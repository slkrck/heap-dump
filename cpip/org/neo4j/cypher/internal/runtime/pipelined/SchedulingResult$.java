package org.neo4j.cypher.internal.runtime.pipelined;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.BoxesRunTime;

public final class SchedulingResult$ implements Serializable
{
    public static SchedulingResult$ MODULE$;

    static
    {
        new SchedulingResult$();
    }

    private SchedulingResult$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SchedulingResult";
    }

    public <T> SchedulingResult<T> apply( final T task, final boolean someTaskWasFilteredOut )
    {
        return new SchedulingResult( task, someTaskWasFilteredOut );
    }

    public <T> Option<Tuple2<T,Object>> unapply( final SchedulingResult<T> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.task(), BoxesRunTime.boxToBoolean( x$0.someTaskWasFilteredOut() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
