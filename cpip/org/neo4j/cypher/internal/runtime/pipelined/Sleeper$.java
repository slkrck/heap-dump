package org.neo4j.cypher.internal.runtime.pipelined;

import java.util.concurrent.TimeUnit;

import scala.concurrent.duration.Duration;
import scala.concurrent.duration.Duration.;

public final class Sleeper$
{
    public static Sleeper$ MODULE$;

    static
    {
        new Sleeper$();
    }

    private Sleeper$()
    {
        MODULE$ = this;
    }

    public Sleeper concurrentSleeper( final int workerId, final Duration sleepDuration )
    {
        return new ConcurrentSleeper( workerId, sleepDuration );
    }

    public Duration concurrentSleeper$default$2()
    {
        return .MODULE$.apply( 1L, TimeUnit.SECONDS );
    }
}
