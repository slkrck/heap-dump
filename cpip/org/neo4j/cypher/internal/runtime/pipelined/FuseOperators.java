package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.cypher.internal.ir.VarPatternLength;
import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.AllNodesScan;
import org.neo4j.cypher.internal.logical.plans.Argument;
import org.neo4j.cypher.internal.logical.plans.Bound;
import org.neo4j.cypher.internal.logical.plans.CacheProperties;
import org.neo4j.cypher.internal.logical.plans.DirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.Distinct;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.logical.plans.IndexedProperty;
import org.neo4j.cypher.internal.logical.plans.InequalitySeekRange;
import org.neo4j.cypher.internal.logical.plans.InequalitySeekRangeWrapper;
import org.neo4j.cypher.internal.logical.plans.Input;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.ManyQueryExpression;
import org.neo4j.cypher.internal.logical.plans.ManySeekableArgs;
import org.neo4j.cypher.internal.logical.plans.NodeByIdSeek;
import org.neo4j.cypher.internal.logical.plans.NodeByLabelScan;
import org.neo4j.cypher.internal.logical.plans.NodeCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.NodeIndexContainsScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexEndsWithScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexSeek;
import org.neo4j.cypher.internal.logical.plans.NodeUniqueIndexSeek;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.logical.plans.QueryExpression;
import org.neo4j.cypher.internal.logical.plans.RangeBetween;
import org.neo4j.cypher.internal.logical.plans.RangeGreaterThan;
import org.neo4j.cypher.internal.logical.plans.RangeLessThan;
import org.neo4j.cypher.internal.logical.plans.RangeQueryExpression;
import org.neo4j.cypher.internal.logical.plans.RelationshipCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.SeekableArgs;
import org.neo4j.cypher.internal.logical.plans.Selection;
import org.neo4j.cypher.internal.logical.plans.SingleQueryExpression;
import org.neo4j.cypher.internal.logical.plans.SingleSeekableArg;
import org.neo4j.cypher.internal.logical.plans.Ties;
import org.neo4j.cypher.internal.logical.plans.UndirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.physicalplanning.NoOutput$;
import org.neo4j.cypher.internal.physicalplanning.OutputDefinition;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PipelineDefinition;
import org.neo4j.cypher.internal.physicalplanning.ProduceResultOutput;
import org.neo4j.cypher.internal.physicalplanning.ReduceOutput;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty$;
import org.neo4j.cypher.internal.physicalplanning.VariablePredicates$;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.compiled.expressions.VariableNamer;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatorFactory;
import org.neo4j.cypher.internal.runtime.pipelined.operators.AggregationMapperOperatorNoGroupingTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.AggregationMapperOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ArgumentOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ArgumentOperatorTaskTemplate$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.CachePropertiesOperatorTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledStreamingOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorselGenerator$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorselTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.DelegateOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.DelegateOperatorTaskTemplate$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ExpandAllOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.FilterOperatorTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.InputLoopTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.InputOperatorTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.InputOperatorTemplate$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ManyNodeByIdsSeekTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ManyQueriesExactNodeIndexSeekTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeCountFromCountStoreOperatorTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeIndexScanTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeIndexStringSearchScanTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.Operator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCodeGenHelperTemplates$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ProduceResultOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ProjectOperatorTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.RelationshipByIdSeekOperator$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.RelationshipCountFromCountStoreOperatorTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SerialTopLevelLimitOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SingleExactSeekQueryNodeIndexSeekTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SingleNodeByIdSeekTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SingleRangeSeekQueryNodeIndexSeekTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SingleThreadedAllNodeScanTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SingleThreadedLabelScanTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.UnwindOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.operators.VarExpandOperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.expressions.SlottedExpressionConverters$;
import org.neo4j.cypher.internal.v4_0.expressions.Ands;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.LabelName;
import org.neo4j.cypher.internal.v4_0.expressions.LabelToken;
import org.neo4j.cypher.internal.v4_0.expressions.ListLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.util.Last;
import org.neo4j.cypher.internal.v4_0.util.Many;
import org.neo4j.cypher.internal.v4_0.util.NonEmptyList;
import org.neo4j.cypher.internal.v4_0.util.One;
import org.neo4j.cypher.internal.v4_0.util.ZeroOneOrMany;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.exceptions.InternalException;
import scala.Function0;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.IterableLike;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable..colon.colon;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.util.Either;
import scala.util.Left;
import scala.util.Right;

@JavaDocToJava
public class FuseOperators
{
    private final OperatorFactory operatorFactory;
    private final TokenContext tokenContext;
    private final boolean parallelExecution;
    private final CodeGenerationMode codeGenerationMode;
    private final PhysicalPlan physicalPlan;
    private final AggregatorFactory aggregatorFactory;

    public FuseOperators( final OperatorFactory operatorFactory, final TokenContext tokenContext, final boolean parallelExecution,
            final CodeGenerationMode codeGenerationMode )
    {
        this.operatorFactory = operatorFactory;
        this.tokenContext = tokenContext;
        this.parallelExecution = parallelExecution;
        this.codeGenerationMode = codeGenerationMode;
        this.physicalPlan = operatorFactory.executionGraphDefinition().physicalPlan();
        this.aggregatorFactory = new AggregatorFactory( this.physicalPlan() );
    }

    private static final Function0 compileExpression$1( final Expression astExpression, final OperatorExpressionCompiler expressionCompiler$1 )
    {
        return () -> {
            return (IntermediateExpression) expressionCompiler$1.intermediateCompileExpression( astExpression ).getOrElse( () -> {
                throw new CantCompileQueryException(
                        (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( astExpression ).toString() );
            } );
        };
    }

    private static final Function0 compileGroupingKey$1( final Map astExpressions, final SlotConfiguration slots, final Seq orderToLeverage,
            final OperatorExpressionCompiler expressionCompiler$1 )
    {
        Seq orderedGroupingExpressions =
                (Seq) SlottedExpressionConverters$.MODULE$.orderGroupingKeyExpressions( astExpressions, orderToLeverage, slots ).map( ( x$4 ) -> {
                    return (Expression) x$4._2();
                }, scala.collection.Seq..MODULE$.canBuildFrom());
        return () -> {
            return (IntermediateExpression) expressionCompiler$1.intermediateCompileGroupingKey( orderedGroupingExpressions ).getOrElse( () -> {
                throw new CantCompileQueryException(
                        (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( astExpressions ).toString() );
            } );
        };
    }

    private static final FusionPlan cantHandle$1( final FusionPlan acc, final LogicalPlan nextPlan, final OutputDefinition output$1,
            final DelegateOperatorTaskTemplate innermostTemplate$1, final List initFusedPlans$1 )
    {
        Some var8 = scala.collection.Seq..MODULE$.unapplySeq( initFusedPlans$1 );
        LogicalPlan var5;
        if ( !var8.isEmpty() && var8.get() != null && ((SeqLike) var8.get()).lengthCompare( 1 ) == 0 )
        {
            LogicalPlan plan = (LogicalPlan) ((SeqLike) var8.get()).apply( 0 );
            var5 = plan;
        }
        else
        {
            var5 = null;
        }

        innermostTemplate$1.reset();
        List var10002 = scala.collection.immutable.List..MODULE$.empty();
        List var10003 = scala.collection.immutable.List..MODULE$.empty();
        List var11 = (List) acc.fusedPlans().filterNot( ( x$7 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$fuseOperators$9( var5, x$7 ) );
        } );
        return acc.copy( innermostTemplate$1, var10002, var10003, acc.unhandledPlans().$colon$colon$colon( var11 ).$colon$colon( nextPlan ), output$1 );
    }

    private PhysicalPlan physicalPlan()
    {
        return this.physicalPlan;
    }

    private AggregatorFactory aggregatorFactory()
    {
        return this.aggregatorFactory;
    }

    public IndexedSeq<ExecutablePipeline> compilePipelines( final ExecutionGraphDefinition executionGraphDefinition )
    {
        Tuple3 var4 = (Tuple3) executionGraphDefinition.pipelines().foldRight( new Tuple3(.MODULE$.IndexedSeq().empty(), BoxesRunTime.boxToBoolean( false ),
                BoxesRunTime.boxToBoolean( false ) ), ( x0$1, x1$1 ) -> {
        Tuple2 var5 = new Tuple2( x0$1, x1$1 );
        if ( var5 != null )
        {
            PipelineDefinition p = (PipelineDefinition) var5._1();
            Tuple3 var7 = (Tuple3) var5._2();
            if ( var7 != null )
            {
                IndexedSeq pipelines = (IndexedSeq) var7._1();
                boolean needsFilteringMorsel = BoxesRunTime.unboxToBoolean( var7._2() );
                boolean nextPipelineFused = BoxesRunTime.unboxToBoolean( var7._3() );
                Tuple2 var12 = this.compilePipeline( p, needsFilteringMorsel, nextPipelineFused );
                if ( var12 == null )
                {
                    throw new MatchError( var12 );
                }

                ExecutablePipeline executablePipeline = (ExecutablePipeline) var12._1();
                boolean upstreamNeedsFilteringMorsel = var12._2$mcZ$sp();
                Tuple2 var4 = new Tuple2( executablePipeline, BoxesRunTime.boxToBoolean( upstreamNeedsFilteringMorsel ) );
                ExecutablePipeline executablePipelinex = (ExecutablePipeline) var4._1();
                boolean upstreamNeedsFilteringMorselx = var4._2$mcZ$sp();
                Tuple3 var3 = new Tuple3( pipelines.$plus$colon( executablePipelinex, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), BoxesRunTime.
                boxToBoolean( needsFilteringMorsel || upstreamNeedsFilteringMorselx ), BoxesRunTime.boxToBoolean( executablePipelinex.isFused() ));
                return var3;
            }
        }

        throw new MatchError( var5 );
    });
        if ( var4 != null )
        {
            IndexedSeq executablePipelines = (IndexedSeq) var4._1();
            return executablePipelines;
        }
        else
        {
            throw new MatchError( var4 );
        }
    }

    private boolean interpretedOperatorRequiresThisPipelineToUseFilteringMorsel( final LogicalPlan plan )
    {
        boolean var2;
        if ( plan instanceof Distinct )
        {
            var2 = true;
        }
        else if ( plan instanceof Limit )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    private boolean requiresUpstreamPipelinesToUseFilteringMorsel( final LogicalPlan plan )
    {
        boolean var2;
        if ( plan instanceof Limit )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public Tuple2<ExecutablePipeline,Object> compilePipeline( final PipelineDefinition p, final boolean needsFilteringMorsel, final boolean nextPipelineFused )
    {
        boolean upstreamsNeedsFilteringMorsel = p.middlePlans().exists( ( plan ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$compilePipeline$1( this, plan ) );
        } ) || p.fusedPlans().exists( ( plan ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$compilePipeline$2( this, plan ) );
        } ) || needsFilteringMorsel;
        Tuple3 var8 = p.fusedPlans().nonEmpty() ? this.fuseOperators( p ) : new Tuple3( scala.None..MODULE$, p.middlePlans(), p.outputDefinition());
        if ( var8 == null )
        {
            throw new MatchError( var8 );
        }
        else
        {
            boolean var4;
            Option maybeHeadOperator;
            Seq unhandledMiddlePlans;
            OutputDefinition unhandledOutput;
            label78:
            {
                Option maybeHeadOperator = (Option) var8._1();
                Seq unhandledMiddlePlans = (Seq) var8._2();
                OutputDefinition unhandledOutput = (OutputDefinition) var8._3();
                Tuple3 var5 = new Tuple3( maybeHeadOperator, unhandledMiddlePlans, unhandledOutput );
                maybeHeadOperator = (Option) var5._1();
                unhandledMiddlePlans = (Seq) var5._2();
                unhandledOutput = (OutputDefinition) var5._3();
                Tuple3 var16 = new Tuple3( p.outputDefinition(), maybeHeadOperator, unhandledMiddlePlans );
                if ( var16 != null )
                {
                    Option var17 = (Option) var16._2();
                    Seq var18 = (Seq) var16._3();
                    if ( var16._1() instanceof ProduceResultOutput && var17 instanceof Some )
                    {
                        Some var19 = scala.collection.Seq..MODULE$.unapplySeq( var18 );
                        if ( !var19.isEmpty() && var19.get() != null && ((SeqLike) var19.get()).lengthCompare( 0 ) == 0 )
                        {
                            var4 = false;
                            break label78;
                        }
                    }
                }

                if ( var16 != null )
                {
                    OutputDefinition var20 = (OutputDefinition) var16._1();
                    Option var21 = (Option) var16._2();
                    Seq var22 = (Seq) var16._3();
                    if ( var20 instanceof ReduceOutput )
                    {
                        ReduceOutput var23 = (ReduceOutput) var20;
                        LogicalPlan var24 = var23.plan();
                        if ( var24 instanceof Aggregation && var21 instanceof Some )
                        {
                            Some var25 = scala.collection.Seq..MODULE$.unapplySeq( var22 );
                            if ( !var25.isEmpty() && var25.get() != null && ((SeqLike) var25.get()).lengthCompare( 0 ) == 0 )
                            {
                                var4 = false;
                                break label78;
                            }
                        }
                    }
                }

                var4 = true;
            }

            boolean thisNeedsFilteringMorsel = needsFilteringMorsel || unhandledMiddlePlans.exists( ( plan ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$compilePipeline$3( this, plan ) );
            } );
            Operator headOperator = (Operator) maybeHeadOperator.getOrElse( () -> {
                return this.operatorFactory.create( p.headPlan(), p.inputBuffer() );
            } );
            MiddleOperator[] middleOperators = this.operatorFactory.createMiddleOperators( unhandledMiddlePlans, headOperator );
            return new Tuple2( new ExecutablePipeline( p.id(), p.lhs(), p.rhs(), headOperator, middleOperators, p.serial(),
                    (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( p.headPlan().id() ), p.inputBuffer(),
                    this.operatorFactory.createOutput( unhandledOutput, nextPipelineFused ), var4, thisNeedsFilteringMorsel ),
                    BoxesRunTime.boxToBoolean( upstreamsNeedsFilteringMorsel ) );
        }
    }

    private Tuple3<Option<Operator>,Seq<LogicalPlan>,OutputDefinition> fuseOperators( final PipelineDefinition pipeline )
    {
        SlotConfiguration inputSlotConfiguration = pipeline.inputBuffer().bufferSlotConfiguration();
        LogicalPlan headPlan = pipeline.headPlan();
        IndexedSeq middlePlans = pipeline.middlePlans();
        OutputDefinition output = pipeline.outputDefinition();
        int id = headPlan.id();
        SlotConfiguration slots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( headPlan.id() );
        VariableNamer namer = new VariableNamer();
        OperatorExpressionCompiler expressionCompiler =
                new OperatorExpressionCompiler( slots, inputSlotConfiguration, this.operatorFactory.readOnly(), this.codeGenerationMode, namer );
        SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( slots );
        DelegateOperatorTaskTemplate innermostTemplate = new DelegateOperatorTaskTemplate( DelegateOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$1(),
                DelegateOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$2(), DelegateOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$3(),
                expressionCompiler );
        Tuple3 var10000;
        if ( middlePlans.nonEmpty() )
        {
            var10000 = new Tuple3( innermostTemplate, scala.collection.immutable.List..MODULE$.empty(), output);
        }
        else
        {
            Tuple3 var3;
            if ( output instanceof ProduceResultOutput )
            {
                ProduceResultOutput var16 = (ProduceResultOutput) output;
                ProduceResult p = var16.plan();
                innermostTemplate.shouldWriteToContext_$eq( false );
                innermostTemplate.shouldCheckDemand_$eq( true );
                ProduceResultOperatorTaskTemplate template =
                        new ProduceResultOperatorTaskTemplate( innermostTemplate, p.id(), p.columns(), slots, expressionCompiler );
                var3 = new Tuple3( template, new colon( p, scala.collection.immutable.Nil..MODULE$ ), NoOutput$.MODULE$);
            }
            else
            {
                label60:
                {
                    if ( output instanceof ReduceOutput )
                    {
                        ReduceOutput var19 = (ReduceOutput) output;
                        int bufferId = var19.bufferId();
                        LogicalPlan p = var19.plan();
                        if ( p instanceof Aggregation )
                        {
                            Aggregation var22 = (Aggregation) p;
                            Map groupingExpressions = var22.groupingExpressions();
                            Map aggregationExpressionsMap = var22.aggregationExpression();
                            innermostTemplate.shouldWriteToContext_$eq( false );
                            innermostTemplate.shouldCheckDemand_$eq( false );
                            innermostTemplate.shouldCheckOutputCounter_$eq( true );
                            int applyPlanId = ((Id) this.physicalPlan().applyPlans().apply( var22.id() )).x();
                            int argumentSlotOffset = slots.getArgumentLongOffsetFor( applyPlanId );
                            SlotConfiguration outputSlots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( var22.id() );
                            ArrayBuilder aggregators = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Aggregator.class ));
                            ArrayBuilder aggregationExpressions = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Expression.class ));
                            aggregationExpressionsMap.foreach( ( x0$2 ) -> {
                                if ( x0$2 != null )
                                {
                                    Expression astExpression = (Expression) x0$2._2();
                                    Tuple2 var9 = this.aggregatorFactory().newAggregator( astExpression );
                                    if ( var9 != null )
                                    {
                                        Aggregator aggregatorx = (Aggregator) var9._1();
                                        Expression innerAstExpression = (Expression) var9._2();
                                        Tuple2 var5 = new Tuple2( aggregatorx, innerAstExpression );
                                        Aggregator aggregator = (Aggregator) var5._1();
                                        Expression innerAstExpressionx = (Expression) var5._2();
                                        aggregators.$plus$eq( aggregator );
                                        ArrayBuilder var4 = (ArrayBuilder) aggregationExpressions.$plus$eq( innerAstExpressionx );
                                        return var4;
                                    }
                                    else
                                    {
                                        throw new MatchError( var9 );
                                    }
                                }
                                else
                                {
                                    throw new MatchError( x0$2 );
                                }
                            } );
                            Expression[] aggregationAstExpressions = (Expression[]) aggregationExpressions.result();
                            Function0 aggregationExpressionsCreator = () -> {
                                return (IntermediateExpression[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) aggregationAstExpressions ))).
                                map( ( e ) -> {
                                    return (IntermediateExpression) compileExpression$1( e, expressionCompiler ).apply();
                                }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateExpression.class )));
                            }; OperatorTaskTemplate template =
                                groupingExpressions.nonEmpty() ? new AggregationMapperOperatorTaskTemplate( innermostTemplate, var22.id(), argumentSlotOffset,
                                        (Aggregator[]) aggregators.result(), bufferId, aggregationExpressionsCreator,
                                        compileGroupingKey$1( groupingExpressions, outputSlots, (Seq) scala.collection.Seq..MODULE$.empty(),
                                        expressionCompiler ), aggregationAstExpressions, expressionCompiler) :
                            new AggregationMapperOperatorNoGroupingTaskTemplate( innermostTemplate, var22.id(), argumentSlotOffset,
                                    (Aggregator[]) aggregators.result(), bufferId, aggregationExpressionsCreator, aggregationAstExpressions,
                                    expressionCompiler );
                            var3 = new Tuple3( template, new colon( var22, scala.collection.immutable.Nil..MODULE$ ), NoOutput$.MODULE$);
                            break label60;
                        }
                    }

                    var3 = new Tuple3( innermostTemplate, scala.collection.immutable.List..MODULE$.empty(), output);
                }
            }

            var10000 = var3;
        }

        Tuple3 var14 = var10000;
        if ( var14 == null )
        {
            throw new MatchError( var14 );
        }
        else
        {
            OperatorTaskTemplate innerTemplate = (OperatorTaskTemplate) var14._1();
            List initFusedPlans = (List) var14._2();
            OutputDefinition initUnhandledOutput = (OutputDefinition) var14._3();
            Tuple3 var2 = new Tuple3( innerTemplate, initFusedPlans, initUnhandledOutput );
            OperatorTaskTemplate innerTemplate = (OperatorTaskTemplate) var2._1();
            List initFusedPlans = (List) var2._2();
            OutputDefinition initUnhandledOutput = (OutputDefinition) var2._3();
            IndexedSeq reversePlans = (IndexedSeq) pipeline.fusedPlans().reverse();
            boolean serialExecutionOnly = !this.parallelExecution || pipeline.serial();
            FusionPlan fusedPipeline =
                    (FusionPlan) reversePlans.foldLeft( new FusionPlan( innerTemplate, initFusedPlans, scala.collection.immutable.List..MODULE$.empty(),
                            middlePlans.toList(), initUnhandledOutput ), ( x0$4, x1$2 ) -> {
            Tuple2 var19 = new Tuple2( x0$4, x1$2 );
            if ( var19 == null )
            {
                throw new MatchError( var19 );
            }
            else
            {
                boolean var167;
                Some var168;
                Option var169;
                label253:
                {
                    FusionPlan acc = (FusionPlan) var19._1();
                    LogicalPlan nextPlan = (LogicalPlan) var19._2();
                    FusionPlan var12;
                    if ( nextPlan instanceof Argument )
                    {
                        Argument var23 = (Argument) nextPlan;
                        ArgumentOperatorTaskTemplate newTemplatexxxxx = new ArgumentOperatorTaskTemplate( acc.template(), var23.id(), innermostTemplate,
                                (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ),
                                ArgumentOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$5(), expressionCompiler );
                        var12 = acc.copy( newTemplatexxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                acc.copy$default$5() );
                    }
                    else if ( nextPlan instanceof AllNodesScan )
                    {
                        AllNodesScan var26 = (AllNodesScan) nextPlan;
                        String nodeVariableName = var26.idName();
                        SlotConfiguration.Size argumentSizexx = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                        SingleThreadedAllNodeScanTaskTemplate newTemplatexxxxxxx =
                                new SingleThreadedAllNodeScanTaskTemplate( acc.template(), var26.id(), innermostTemplate, nodeVariableName,
                                        slots.getLongOffsetFor( nodeVariableName ), argumentSizexx, expressionCompiler );
                        var12 = acc.copy( newTemplatexxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                acc.copy$default$5() );
                    }
                    else if ( nextPlan instanceof NodeByLabelScan )
                    {
                        NodeByLabelScan var31 = (NodeByLabelScan) nextPlan;
                        String node = var31.idName();
                        LabelName label = var31.label();
                        SlotConfiguration.Size argumentSizexxx = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                        Option maybeToken = this.tokenContext.getOptLabelId( label.name() );
                        SingleThreadedLabelScanTaskTemplate newTemplatexxxxxxxx =
                                new SingleThreadedLabelScanTaskTemplate( acc.template(), var31.id(), innermostTemplate, node, slots.getLongOffsetFor( node ),
                                        label.name(), maybeToken, argumentSizexxx, expressionCompiler );
                        var12 = acc.copy( newTemplatexxxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                acc.copy$default$5() );
                    }
                    else if ( nextPlan instanceof NodeIndexScan )
                    {
                        NodeIndexScan var38 = (NodeIndexScan) nextPlan;
                        String nodex = var38.idName();
                        LabelToken labelx = var38.label();
                        Seq properties = var38.properties();
                        IndexOrder indexOrder = var38.indexOrder();
                        NodeIndexScanTaskTemplate newTemplatexxxxxxxxxx =
                                new NodeIndexScanTaskTemplate( acc.template(), var38.id(), innermostTemplate, slots.getLongOffsetFor( nodex ),
                                        (SlottedIndexedProperty[]) ((TraversableOnce) properties.map( ( x$13 ) -> {
                                            return SlottedIndexedProperty$.MODULE$.apply( nodex, x$13, slots );
                                        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply(
                                        SlottedIndexedProperty.class )),
                        this.operatorFactory.indexRegistrator().registerQueryIndex( labelx, properties ), org.neo4j.cypher.internal.runtime.KernelAPISupport..
                        MODULE$.asKernelIndexOrder( indexOrder ), (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler);
                        var12 = acc.copy( newTemplatexxxxxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                acc.copy$default$5() );
                    }
                    else if ( nextPlan instanceof NodeIndexContainsScan )
                    {
                        NodeIndexContainsScan var45 = (NodeIndexContainsScan) nextPlan;
                        String nodexx = var45.idName();
                        LabelToken labelxx = var45.label();
                        IndexedProperty property = var45.property();
                        Expression seekExpression = var45.valueExpr();
                        IndexOrder indexOrderx = var45.indexOrder();
                        NodeIndexStringSearchScanTaskTemplate newTemplatexxxxxxxxxxx =
                                new NodeIndexStringSearchScanTaskTemplate( acc.template(), var45.id(), innermostTemplate, slots.getLongOffsetFor( nodexx ),
                                        SlottedIndexedProperty$.MODULE$.apply( nodexx, property, slots ),
                                        this.operatorFactory.indexRegistrator().registerQueryIndex( labelxx, property ),
                                        org.neo4j.cypher.internal.runtime.KernelAPISupport..MODULE$.asKernelIndexOrder( indexOrderx ), compileExpression$1
                        (seekExpression, expressionCompiler),( prop, expression ) -> {
                        return $anonfun$fuseOperators$16( BoxesRunTime.unboxToInt( prop ), expression );
                    }, (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler);
                        var12 = acc.copy( newTemplatexxxxxxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                acc.copy$default$5() );
                    }
                    else if ( nextPlan instanceof NodeIndexEndsWithScan )
                    {
                        NodeIndexEndsWithScan var53 = (NodeIndexEndsWithScan) nextPlan;
                        String nodexxx = var53.idName();
                        LabelToken labelxxx = var53.label();
                        IndexedProperty propertyx = var53.property();
                        Expression seekExpressionx = var53.valueExpr();
                        IndexOrder indexOrderxx = var53.indexOrder();
                        NodeIndexStringSearchScanTaskTemplate newTemplatexxxxxxxxxxxxx =
                                new NodeIndexStringSearchScanTaskTemplate( acc.template(), var53.id(), innermostTemplate, slots.getLongOffsetFor( nodexxx ),
                                        SlottedIndexedProperty$.MODULE$.apply( nodexxx, propertyx, slots ),
                                        this.operatorFactory.indexRegistrator().registerQueryIndex( labelxxx, propertyx ),
                                        org.neo4j.cypher.internal.runtime.KernelAPISupport..MODULE$.asKernelIndexOrder( indexOrderxx ), compileExpression$1
                        (seekExpressionx, expressionCompiler),( prop, expression ) -> {
                        return $anonfun$fuseOperators$17( BoxesRunTime.unboxToInt( prop ), expression );
                    }, (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler);
                        var12 = acc.copy( newTemplatexxxxxxxxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                acc.copy$default$5() );
                    }
                    else
                    {
                        if ( nextPlan instanceof NodeUniqueIndexSeek )
                        {
                            NodeUniqueIndexSeek var61 = (NodeUniqueIndexSeek) nextPlan;
                            String nodexxxx = var61.idName();
                            LabelToken labelxxxx = var61.label();
                            Seq propertiesx = var61.properties();
                            QueryExpression valueExpr = var61.valueExpr();
                            IndexOrder order = var61.indexOrder();
                            if ( this.operatorFactory.readOnly() )
                            {
                                Option var67 = this.indexSeek$1( nodexxxx, labelxxxx, propertiesx, valueExpr,
                                        org.neo4j.cypher.internal.runtime.KernelAPISupport..MODULE$.asKernelIndexOrder( order ),
                                true, var61, acc, id, slots, expressionCompiler, innermostTemplate);
                                FusionPlan var18;
                                if ( var67 instanceof Some )
                                {
                                    Some var68 = (Some) var67;
                                    InputLoopTaskTemplate seek = (InputLoopTaskTemplate) var68.value();
                                    var18 = acc.copy( seek, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                            acc.copy$default$5() );
                                }
                                else
                                {
                                    if ( !scala.None..MODULE$.equals( var67 )){
                                    throw new MatchError( var67 );
                                }

                                    var18 = cantHandle$1( acc, nextPlan, output, innermostTemplate, initFusedPlans );
                                }

                                var12 = var18;
                                return var12;
                            }
                        }

                        if ( nextPlan instanceof NodeIndexSeek )
                        {
                            NodeIndexSeek var71 = (NodeIndexSeek) nextPlan;
                            String nodexxxxx = var71.idName();
                            LabelToken labelxxxxx = var71.label();
                            Seq propertiesxxx = var71.properties();
                            QueryExpression valueExprx = var71.valueExpr();
                            IndexOrder orderx = var71.indexOrder();
                            Option var77 = this.indexSeek$1( nodexxxxx, labelxxxxx, propertiesxxx, valueExprx,
                                    org.neo4j.cypher.internal.runtime.KernelAPISupport..MODULE$.asKernelIndexOrder( orderx ),
                            false, var71, acc, id, slots, expressionCompiler, innermostTemplate);
                            FusionPlan var17;
                            if ( var77 instanceof Some )
                            {
                                Some var78 = (Some) var77;
                                InputLoopTaskTemplate seekx = (InputLoopTaskTemplate) var78.value();
                                var17 = acc.copy( seekx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                        acc.copy$default$5() );
                            }
                            else
                            {
                                if ( !scala.None..MODULE$.equals( var77 )){
                                throw new MatchError( var77 );
                            }

                                var17 = cantHandle$1( acc, nextPlan, output, innermostTemplate, initFusedPlans );
                            }

                            var12 = var17;
                        }
                        else if ( nextPlan instanceof NodeByIdSeek )
                        {
                            NodeByIdSeek var81 = (NodeByIdSeek) nextPlan;
                            String nodexxxxxx = var81.idName();
                            SeekableArgs nodeIds = var81.nodeIds();
                            Object var14;
                            if ( nodeIds instanceof SingleSeekableArg )
                            {
                                SingleSeekableArg var86 = (SingleSeekableArg) nodeIds;
                                Expression expr = var86.expr();
                                var14 = new SingleNodeByIdSeekTaskTemplate( acc.template(), var81.id(), innermostTemplate, nodexxxxxx,
                                        slots.getLongOffsetFor( nodexxxxxx ), expr, (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ),
                                        expressionCompiler );
                            }
                            else
                            {
                                if ( !(nodeIds instanceof ManySeekableArgs) )
                                {
                                    throw new MatchError( nodeIds );
                                }

                                ManySeekableArgs var88 = (ManySeekableArgs) nodeIds;
                                Expression exprx = var88.expr();
                                Object var15;
                                if ( exprx instanceof ListLiteral )
                                {
                                    ListLiteral var91 = (ListLiteral) exprx;
                                    ZeroOneOrMany var92 = org.neo4j.cypher.internal.v4_0.util.ZeroOneOrMany..MODULE$.apply( var91.expressions() );
                                    Object var16;
                                    if ( org.neo4j.cypher.internal.v4_0.util.Zero..MODULE$.equals( var92 )){
                                    var16 = OperatorTaskTemplate$.MODULE$.empty( var81.id() );
                                } else if ( var92 instanceof One )
                                {
                                    One var93 = (One) var92;
                                    Expression value = (Expression) var93.value();
                                    var16 = new SingleNodeByIdSeekTaskTemplate( acc.template(), var81.id(), innermostTemplate, nodexxxxxx,
                                            slots.getLongOffsetFor( nodexxxxxx ), value,
                                            (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler );
                                }
                                else
                                {
                                    if ( !(var92 instanceof Many) )
                                    {
                                        throw new MatchError( var92 );
                                    }

                                    var16 = new ManyNodeByIdsSeekTaskTemplate( acc.template(), var81.id(), innermostTemplate, nodexxxxxx,
                                            slots.getLongOffsetFor( nodexxxxxx ), exprx,
                                            (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler );
                                }

                                    var15 = var16;
                                }
                                else
                                {
                                    var15 = new ManyNodeByIdsSeekTaskTemplate( acc.template(), var81.id(), innermostTemplate, nodexxxxxx,
                                            slots.getLongOffsetFor( nodexxxxxx ), exprx,
                                            (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler );
                                }

                                var14 = var15;
                            }

                            var12 = acc.copy( (OperatorTaskTemplate) var14, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(),
                                    acc.copy$default$4(), acc.copy$default$5() );
                        }
                        else if ( nextPlan instanceof DirectedRelationshipByIdSeek )
                        {
                            DirectedRelationshipByIdSeek var96 = (DirectedRelationshipByIdSeek) nextPlan;
                            String relationship = var96.idName();
                            SeekableArgs relIds = var96.relIds();
                            String from = var96.startNode();
                            String to = var96.endNode();
                            OperatorTaskTemplate newTemplatexx =
                                    RelationshipByIdSeekOperator$.MODULE$.taskTemplate( true, acc.template(), var96.id(), innermostTemplate,
                                            slots.getLongOffsetFor( relationship ), slots.getLongOffsetFor( from ), slots.getLongOffsetFor( to ), relIds,
                                            (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler );
                            var12 = acc.copy( newTemplatexx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                    acc.copy$default$5() );
                        }
                        else if ( nextPlan instanceof UndirectedRelationshipByIdSeek )
                        {
                            UndirectedRelationshipByIdSeek var103 = (UndirectedRelationshipByIdSeek) nextPlan;
                            String relationshipx = var103.idName();
                            SeekableArgs relIdsx = var103.relIds();
                            String fromx = var103.leftNode();
                            String tox = var103.rightNode();
                            OperatorTaskTemplate newTemplatexxxx =
                                    RelationshipByIdSeekOperator$.MODULE$.taskTemplate( false, acc.template(), var103.id(), innermostTemplate,
                                            slots.getLongOffsetFor( relationshipx ), slots.getLongOffsetFor( fromx ), slots.getLongOffsetFor( tox ), relIdsx,
                                            (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ), expressionCompiler );
                            var12 = acc.copy( newTemplatexxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                    acc.copy$default$5() );
                        }
                        else
                        {
                            if ( nextPlan instanceof Expand )
                            {
                                Expand var110 = (Expand) nextPlan;
                                String fromName = var110.from();
                                SemanticDirection dir = var110.dir();
                                Seq types = var110.types();
                                String toxx = var110.to();
                                String relName = var110.relName();
                                ExpansionMode var116 = var110.mode();
                                if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( var116 )){
                                Slot fromSlot = slots.apply( fromName );
                                int relOffset = slots.getLongOffsetFor( relName );
                                int toOffset = slots.getLongOffsetFor( toxx );
                                Seq tokensOrNames = (Seq) types.map( ( r ) -> {
                                    Option var3 = this.tokenContext.getOptRelTypeId( r.name() );
                                    Object var2;
                                    if ( var3 instanceof Some )
                                    {
                                        Some var4 = (Some) var3;
                                        int token = BoxesRunTime.unboxToInt( var4.value() );
                                        var2 = .MODULE$.Left().apply( BoxesRunTime.boxToInteger( token ) );
                                    }
                                    else
                                    {
                                        if ( !scala.None..MODULE$.equals( var3 )){
                                        throw new MatchError( var3 );
                                    }

                                        var2 = .MODULE$.Right().apply( r.name() );
                                    }

                                    return (Either) var2;
                                }, scala.collection.Seq..MODULE$.canBuildFrom());
                                Seq typeTokens = (Seq) tokensOrNames.collect( new Serializable( (FuseOperators) null )
                                {
                                    public static final long serialVersionUID = 0L;

                                    public final <A1 extends Either<Object,String>, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                                    {
                                        Object var3;
                                        if ( x1 instanceof Left )
                                        {
                                            Left var5 = (Left) x1;
                                            int token = BoxesRunTime.unboxToInt( var5.value() );
                                            var3 = BoxesRunTime.boxToInteger( token );
                                        }
                                        else
                                        {
                                            var3 = var2.apply( x1 );
                                        }

                                        return var3;
                                    }

                                    public final boolean isDefinedAt( final Either<Object,String> x1 )
                                    {
                                        boolean var2;
                                        if ( x1 instanceof Left && true )
                                        {
                                            var2 = true;
                                        }
                                        else
                                        {
                                            var2 = false;
                                        }

                                        return var2;
                                    }
                                }, scala.collection.Seq..MODULE$.canBuildFrom());
                                Seq missingTypes = (Seq) tokensOrNames.collect( new Serializable( (FuseOperators) null )
                                {
                                    public static final long serialVersionUID = 0L;

                                    public final <A1 extends Either<Object,String>, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
                                    {
                                        Object var3;
                                        if ( x2 instanceof Right )
                                        {
                                            Right var5 = (Right) x2;
                                            String name = (String) var5.value();
                                            if ( name != null )
                                            {
                                                var3 = name;
                                                return var3;
                                            }
                                        }

                                        var3 = var2.apply( x2 );
                                        return var3;
                                    }

                                    public final boolean isDefinedAt( final Either<Object,String> x2 )
                                    {
                                        boolean var2;
                                        if ( x2 instanceof Right )
                                        {
                                            Right var4 = (Right) x2;
                                            String name = (String) var4.value();
                                            if ( name != null )
                                            {
                                                var2 = true;
                                                return var2;
                                            }
                                        }

                                        var2 = false;
                                        return var2;
                                    }
                                }, scala.collection.Seq..MODULE$.canBuildFrom());
                                ExpandAllOperatorTaskTemplate newTemplatexxxxxxxxx =
                                        new ExpandAllOperatorTaskTemplate( acc.template(), var110.id(), innermostTemplate, var110 == headPlan, fromSlot,
                                                relOffset, toOffset, dir, (int[]) typeTokens.toArray( scala.reflect.ClassTag..MODULE$.Int() ),
                                (String[]) missingTypes.toArray( scala.reflect.ClassTag..MODULE$.apply( String.class )),expressionCompiler);
                                var12 = acc.copy( newTemplatexxxxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                        acc.copy$default$5() );
                                return var12;
                            }
                            }

                            if ( nextPlan instanceof VarExpand )
                            {
                                VarExpandOperatorTaskTemplate var10000;
                                OperatorTaskTemplate var10002;
                                int var10003;
                                boolean var10005;
                                int[] var10011;
                                String[] var10012;
                                int var10013;
                                int var10014;
                                boolean var10015;
                                SemanticDirection dirx;
                                SemanticDirection projectedDir;
                                Option nodePredicate;
                                Option relationshipPredicate;
                                Slot fromSlotx;
                                int relOffsetx;
                                Slot toSlot;
                                int tempNodeOffset;
                                int tempRelationshipOffset;
                                label185:
                                {
                                    label184:
                                    {
                                        VarExpand var125 = (VarExpand) nextPlan;
                                        String fromNamex = var125.from();
                                        dirx = var125.dir();
                                        projectedDir = var125.projectedDir();
                                        Seq typesx = var125.types();
                                        String toName = var125.to();
                                        String relNamex = var125.relName();
                                        VarPatternLength length = var125.length();
                                        ExpansionMode mode = var125.mode();
                                        nodePredicate = var125.nodePredicate();
                                        relationshipPredicate = var125.relationshipPredicate();
                                        fromSlotx = slots.apply( fromNamex );
                                        relOffsetx = slots.getReferenceOffsetFor( relNamex );
                                        toSlot = slots.apply( toName );
                                        Seq tokensOrNamesx = (Seq) typesx.map( ( r ) -> {
                                            Option var3 = this.tokenContext.getOptRelTypeId( r.name() );
                                            Object var2;
                                            if ( var3 instanceof Some )
                                            {
                                                Some var4 = (Some) var3;
                                                int token = BoxesRunTime.unboxToInt( var4.value() );
                                                var2 = .MODULE$.Left().apply( BoxesRunTime.boxToInteger( token ) );
                                            }
                                            else
                                            {
                                                if ( !scala.None..MODULE$.equals( var3 )){
                                                throw new MatchError( var3 );
                                            }

                                                var2 = .MODULE$.Right().apply( r.name() );
                                            }

                                            return (Either) var2;
                                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                                        Seq typeTokensx = (Seq) tokensOrNamesx.collect( new Serializable( (FuseOperators) null )
                                        {
                                            public static final long serialVersionUID = 0L;

                                            public final <A1 extends Either<Object,String>, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
                                            {
                                                Object var3;
                                                if ( x3 instanceof Left )
                                                {
                                                    Left var5 = (Left) x3;
                                                    int token = BoxesRunTime.unboxToInt( var5.value() );
                                                    var3 = BoxesRunTime.boxToInteger( token );
                                                }
                                                else
                                                {
                                                    var3 = var2.apply( x3 );
                                                }

                                                return var3;
                                            }

                                            public final boolean isDefinedAt( final Either<Object,String> x3 )
                                            {
                                                boolean var2;
                                                if ( x3 instanceof Left && true )
                                                {
                                                    var2 = true;
                                                }
                                                else
                                                {
                                                    var2 = false;
                                                }

                                                return var2;
                                            }
                                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                                        Seq missingTypesx = (Seq) tokensOrNamesx.collect( new Serializable( (FuseOperators) null )
                                        {
                                            public static final long serialVersionUID = 0L;

                                            public final <A1 extends Either<Object,String>, B1> B1 applyOrElse( final A1 x4, final Function1<A1,B1> default )
                                            {
                                                Object var3;
                                                if ( x4 instanceof Right )
                                                {
                                                    Right var5 = (Right) x4;
                                                    String name = (String) var5.value();
                                                    if ( name != null )
                                                    {
                                                        var3 = name;
                                                        return var3;
                                                    }
                                                }

                                                var3 = var2.apply( x4 );
                                                return var3;
                                            }

                                            public final boolean isDefinedAt( final Either<Object,String> x4 )
                                            {
                                                boolean var2;
                                                if ( x4 instanceof Right )
                                                {
                                                    Right var4 = (Right) x4;
                                                    String name = (String) var4.value();
                                                    if ( name != null )
                                                    {
                                                        var2 = true;
                                                        return var2;
                                                    }
                                                }

                                                var2 = false;
                                                return var2;
                                            }
                                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                                        tempNodeOffset = VariablePredicates$.MODULE$.expressionSlotForPredicate( nodePredicate );
                                        tempRelationshipOffset = VariablePredicates$.MODULE$.expressionSlotForPredicate( relationshipPredicate );
                                        var10000 = new VarExpandOperatorTaskTemplate; var10002 = acc.template();
                                        var10003 = var125.id();
                                        var10005 = var125 == headPlan;
                                        var10011 = (int[]) typeTokensx.toArray( scala.reflect.ClassTag..MODULE$.Int());
                                        var10012 = (String[]) missingTypesx.toArray( scala.reflect.ClassTag..MODULE$.apply( String.class ));
                                        var10013 = length.min();
                                        var10014 = BoxesRunTime.unboxToInt( length.max().getOrElse( () -> {
                                            return Integer.MAX_VALUE;
                                        } ) );
                                        org.neo4j.cypher.internal.logical.plans.ExpandAll.var145 = org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$;
                                        if ( mode == null )
                                        {
                                            if ( var145 == null )
                                            {
                                                break label184;
                                            }
                                        }
                                        else if ( mode.equals( var145 ) )
                                        {
                                            break label184;
                                        }

                                        var10015 = false;
                                        break label185;
                                    }

                                    var10015 = true;
                                }

                                var10000.<init>
                                (var10002, var10003, innermostTemplate, var10005, fromSlotx, relOffsetx, toSlot, dirx, projectedDir, var10011, var10012, var10013, var10014, var10015, tempNodeOffset, tempRelationshipOffset, nodePredicate, relationshipPredicate, expressionCompiler)
                                ;
                                VarExpandOperatorTaskTemplate newTemplatexxxxxxxxxxxx = var10000;
                                var12 = acc.copy( newTemplatexxxxxxxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(),
                                        acc.copy$default$4(), acc.copy$default$5() );
                            }
                            else if ( nextPlan instanceof Selection )
                            {
                                Selection var147 = (Selection) nextPlan;
                                Ands predicate = var147.predicate();
                                var12 = acc.copy( new FilterOperatorTemplate( acc.template(), var147.id(), compileExpression$1( predicate, expressionCompiler ),
                                                expressionCompiler ), acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                        acc.copy$default$5() );
                            }
                            else if ( nextPlan instanceof Projection )
                            {
                                Projection var150 = (Projection) nextPlan;
                                Map projections = var150.projectExpressions();
                                var12 = acc.copy( new ProjectOperatorTemplate( acc.template(), var150.id(), projections, expressionCompiler ),
                                        acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(), acc.copy$default$5() );
                            }
                            else if ( nextPlan instanceof CacheProperties )
                            {
                                CacheProperties var153 = (CacheProperties) nextPlan;
                                Set propertiesxx = var153.properties();
                                var12 = acc.copy( new CachePropertiesOperatorTemplate( acc.template(), var153.id(), propertiesxx.toSeq(), expressionCompiler ),
                                        acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(), acc.copy$default$5() );
                            }
                            else if ( nextPlan instanceof Input )
                            {
                                Input var156 = (Input) nextPlan;
                                Seq nodes = var156.nodes();
                                Seq relationships = var156.relationships();
                                Seq variables = var156.variables();
                                boolean nullable = var156.nullable();
                                InputOperatorTemplate newTemplatexxxxxxxxxxxxxx = new InputOperatorTemplate( acc.template(), var156.id(), innermostTemplate,
                                        (int[]) ((TraversableOnce) nodes.map( ( v ) -> {
                                            return BoxesRunTime.boxToInteger( $anonfun$fuseOperators$21( slots, v ) );
                                        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                                (int[]) ((TraversableOnce) relationships.map( ( v ) -> {
                                    return BoxesRunTime.boxToInteger( $anonfun$fuseOperators$22( slots, v ) );
                                }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                                (int[]) ((TraversableOnce) variables.map( ( v ) -> {
                                    return BoxesRunTime.boxToInteger( $anonfun$fuseOperators$23( slots, v ) );
                                }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                                nullable, InputOperatorTemplate$.MODULE$.$lessinit$greater$default$8(), expressionCompiler);
                                var12 = acc.copy( newTemplatexxxxxxxxxxxxxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(),
                                        acc.copy$default$4(), acc.copy$default$5() );
                            }
                            else if ( nextPlan instanceof UnwindCollection )
                            {
                                UnwindCollection var163 = (UnwindCollection) nextPlan;
                                String variable = var163.variable();
                                Expression collection = var163.expression();
                                var167 = false;
                                var168 = null;
                                var169 = slots.get( variable );
                                if ( !(var169 instanceof Some) )
                                {
                                    break label253;
                                }

                                var167 = true;
                                var168 = (Some) var169;
                                Slot var170 = (Slot) var168.value();
                                if ( !(var170 instanceof RefSlot) )
                                {
                                    break label253;
                                }

                                RefSlot var171 = (RefSlot) var170;
                                int idx = var171.offset();
                                UnwindOperatorTaskTemplate newTemplate =
                                        new UnwindOperatorTaskTemplate( acc.template(), var163.id(), innermostTemplate, var163 == headPlan, collection, idx,
                                                expressionCompiler );
                                var12 = acc.copy( newTemplate, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                        acc.copy$default$5() );
                            }
                            else if ( nextPlan instanceof NodeCountFromCountStore )
                            {
                                NodeCountFromCountStore var176 = (NodeCountFromCountStore) nextPlan;
                                String name = var176.idName();
                                List labels = var176.labelNames();
                                SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                                List labelTokenOrNames = (List) labels.map( ( x$29 ) -> {
                                    return x$29.map( ( labelName ) -> {
                                        Option var3 = this.tokenContext.getOptLabelId( labelName.name() );
                                        Object var2;
                                        if ( scala.None..MODULE$.equals( var3 )){
                                            var2 = .MODULE$.Left().apply( labelName.name() );
                                        } else{
                                            if ( !(var3 instanceof Some) )
                                            {
                                                throw new MatchError( var3 );
                                            }

                                            Some var4 = (Some) var3;
                                            int token = BoxesRunTime.unboxToInt( var4.value() );
                                            var2 = .MODULE$.Right().apply( BoxesRunTime.boxToInteger( token ) );
                                        }

                                        return (Either) var2;
                                    } );
                                }, scala.collection.immutable.List..MODULE$.canBuildFrom());
                                NodeCountFromCountStoreOperatorTemplate newTemplatex =
                                        new NodeCountFromCountStoreOperatorTemplate( acc.template(), var176.id(), innermostTemplate,
                                                slots.getReferenceOffsetFor( name ), labelTokenOrNames, argumentSize, expressionCompiler );
                                var12 = acc.copy( newTemplatex, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                        acc.copy$default$5() );
                            }
                            else if ( nextPlan instanceof RelationshipCountFromCountStore )
                            {
                                RelationshipCountFromCountStore var183 = (RelationshipCountFromCountStore) nextPlan;
                                String namex = var183.idName();
                                Option startLabel = var183.startLabel();
                                Seq typeNames = var183.typeNames();
                                Option endLabel = var183.endLabel();
                                SlotConfiguration.Size argumentSizex = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                                Option startNameOrToken = startLabel.map( ( l ) -> {
                                    Option var3 = this.tokenContext.getOptLabelId( l.name() );
                                    Object var2;
                                    if ( scala.None..MODULE$.equals( var3 )){
                                        var2 = .MODULE$.Left().apply( l.name() );
                                    } else{
                                        if ( !(var3 instanceof Some) )
                                        {
                                            throw new MatchError( var3 );
                                        }

                                        Some var4 = (Some) var3;
                                        int token = BoxesRunTime.unboxToInt( var4.value() );
                                        var2 = .MODULE$.Right().apply( BoxesRunTime.boxToInteger( token ) );
                                    }

                                    return (Either) var2;
                                } ); Option endNameOrToken = endLabel.map( ( l ) -> {
                                Option var3 = this.tokenContext.getOptLabelId( l.name() );
                                Object var2;
                                if ( scala.None..MODULE$.equals( var3 )){
                                    var2 = .MODULE$.Left().apply( l.name() );
                                } else{
                                    if ( !(var3 instanceof Some) )
                                    {
                                        throw new MatchError( var3 );
                                    }

                                    Some var4 = (Some) var3;
                                    int token = BoxesRunTime.unboxToInt( var4.value() );
                                    var2 = .MODULE$.Right().apply( BoxesRunTime.boxToInteger( token ) );
                                }

                                return (Either) var2;
                            } ); Seq typeNamesOrTokens = (Seq) typeNames.map( ( t ) -> {
                                Option var3 = this.tokenContext.getOptRelTypeId( t.name() );
                                Object var2;
                                if ( scala.None..MODULE$.equals( var3 )){
                                    var2 = .MODULE$.Left().apply( t.name() );
                                } else{
                                    if ( !(var3 instanceof Some) )
                                    {
                                        throw new MatchError( var3 );
                                    }

                                    Some var4 = (Some) var3;
                                    int token = BoxesRunTime.unboxToInt( var4.value() );
                                    var2 = .MODULE$.Right().apply( BoxesRunTime.boxToInteger( token ) );
                                }

                                return (Either) var2;
                            }, scala.collection.Seq..MODULE$.canBuildFrom());
                                RelationshipCountFromCountStoreOperatorTemplate newTemplatexxx =
                                        new RelationshipCountFromCountStoreOperatorTemplate( acc.template(), var183.id(), innermostTemplate,
                                                slots.getReferenceOffsetFor( namex ), startNameOrToken, typeNamesOrTokens, endNameOrToken, argumentSizex,
                                                expressionCompiler );
                                var12 = acc.copy( newTemplatexxx, acc.fusedPlans().$colon$colon( nextPlan ), acc.copy$default$3(), acc.copy$default$4(),
                                        acc.copy$default$5() );
                            }
                            else
                            {
                                if ( nextPlan instanceof Limit )
                                {
                                    Limit var194 = (Limit) nextPlan;
                                    Expression countExpression = var194.count();
                                    Ties var196 = var194.ties();
                                    if ( org.neo4j.cypher.internal.logical.plans.DoNotIncludeTies..
                                    MODULE$.equals( var196 ) && this.hasNoNestedArguments$1( var194 ) && serialExecutionOnly){
                                    int argumentStateMapId = this.operatorFactory.executionGraphDefinition().findArgumentStateMapForPlan( var194.id() );
                                    SerialTopLevelLimitOperatorTaskTemplate newTemplatexxxxxx =
                                            new SerialTopLevelLimitOperatorTaskTemplate( acc.template(), var194.id(), innermostTemplate, argumentStateMapId,
                                                    compileExpression$1( countExpression, expressionCompiler ), expressionCompiler );
                                    List var201 = acc.fusedPlans().$colon$colon( nextPlan );
                                    Tuple2 var200 = new Tuple2( new ArgumentStateMapId( argumentStateMapId ),
                                            SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitStateFactory$.MODULE$ );
                                    var12 = acc.copy( newTemplatexxxxxx, var201, acc.argumentStates().$colon$colon( var200 ), acc.copy$default$4(),
                                            acc.copy$default$5() );
                                    return var12;
                                }
                                }

                                var12 = cantHandle$1( acc, nextPlan, output, innermostTemplate, initFusedPlans );
                            }
                        }
                    }

                    return var12;
                }

                if ( var167 )
                {
                    Slot slot = (Slot) var168.value();
                    throw new InternalException( (new StringBuilder( 26 )).append( slot ).append( " cannot be used for UNWIND" ).toString() );
                }
                else if ( scala.None..MODULE$.equals( var169 )){
                throw new InternalException( "No slot found for UNWIND" );
            } else{
                throw new MatchError( var169 );
            }
            }
        });
            if ( fusedPipeline.fusedPlans().length() < FuseOperators$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$FuseOperators$$FUSE_LIMIT() )
            {
                var10000 = new Tuple3( scala.None..MODULE$, ((TraversableLike) pipeline.fusedPlans().tail()).$plus$plus( middlePlans,
                        scala.collection.IndexedSeq..MODULE$.canBuildFrom()),output);
            }
            else
            {
                WorkIdentity workIdentity = org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity..MODULE$.fromFusedPlans( fusedPipeline.fusedPlans() );
                ContinuableOperatorTaskWithMorselTemplate operatorTaskWithMorselTemplate = (ContinuableOperatorTaskWithMorselTemplate) fusedPipeline.template();

                try
                {
                    CompiledStreamingOperator compiledOperator =
                            ContinuableOperatorTaskWithMorselGenerator$.MODULE$.compileOperator( operatorTaskWithMorselTemplate, workIdentity,
                                    fusedPipeline.argumentStates(), this.codeGenerationMode );
                    var10000 = new Tuple3( new Some( compiledOperator ), fusedPipeline.unhandledPlans(), fusedPipeline.unhandledOutput() );
                }
                catch ( CantCompileQueryException var45 )
                {
                    var10000 = new Tuple3( scala.None..MODULE$, ((TraversableLike) pipeline.fusedPlans().tail()).$plus$plus( middlePlans,
                            scala.collection.IndexedSeq..MODULE$.canBuildFrom()),output);
                }
            }

            return var10000;
        }
    }

    private final Option indexSeek$1( final String node, final LabelToken label, final Seq properties, final QueryExpression valueExpr,
            final org.neo4j.internal.schema.IndexOrder order, final boolean unique, final LogicalPlan plan, final FusionPlan acc, final int id$1,
            final SlotConfiguration slots$1, final OperatorExpressionCompiler expressionCompiler$1, final DelegateOperatorTaskTemplate innermostTemplate$1 )
    {
        boolean needsLockingUnique = !this.operatorFactory.readOnly() && unique;
        SlottedIndexedProperty property = SlottedIndexedProperty$.MODULE$.apply( node, (IndexedProperty) properties.head(), slots$1 );
        Object var13;
        if ( valueExpr instanceof SingleQueryExpression )
        {
            SingleQueryExpression var18 = (SingleQueryExpression) valueExpr;
            Expression expr = (Expression) var18.expression();
            if ( !needsLockingUnique )
            {
                scala.Predef..MODULE$. assert (properties.length() == 1);
                var13 = new Some(
                        new SingleExactSeekQueryNodeIndexSeekTaskTemplate( acc.template(), plan.id(), innermostTemplate$1, slots$1.getLongOffsetFor( node ),
                                property, compileExpression$1( expr, expressionCompiler$1 ),
                                this.operatorFactory.indexRegistrator().registerQueryIndex( label, properties ),
                                (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id$1 ), expressionCompiler$1 ) );
                return (Option) var13;
            }
        }

        if ( valueExpr instanceof ManyQueryExpression )
        {
            ManyQueryExpression var20 = (ManyQueryExpression) valueExpr;
            Expression expr = (Expression) var20.expression();
            if ( !needsLockingUnique )
            {
                scala.Predef..MODULE$. assert (properties.length() == 1);
                var13 = new Some(
                        new ManyQueriesExactNodeIndexSeekTaskTemplate( acc.template(), plan.id(), innermostTemplate$1, node, slots$1.getLongOffsetFor( node ),
                                property, compileExpression$1( expr, expressionCompiler$1 ),
                                this.operatorFactory.indexRegistrator().registerQueryIndex( label, properties ), order,
                                (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id$1 ), expressionCompiler$1 ) );
                return (Option) var13;
            }
        }

        if ( valueExpr instanceof RangeQueryExpression )
        {
            RangeQueryExpression var22 = (RangeQueryExpression) valueExpr;
            Expression rangeWrapper = (Expression) var22.expression();
            if ( !needsLockingUnique )
            {
                Object var14;
                label106:
                {
                    scala.Predef..MODULE$. assert (properties.length() == 1);
                    boolean var24 = false;
                    InequalitySeekRangeWrapper var25 = null;
                    if ( rangeWrapper instanceof InequalitySeekRangeWrapper )
                    {
                        var24 = true;
                        var25 = (InequalitySeekRangeWrapper) rangeWrapper;
                        InequalitySeekRange var27 = var25.range();
                        if ( var27 instanceof RangeLessThan )
                        {
                            RangeLessThan var28 = (RangeLessThan) var27;
                            NonEmptyList var29 = var28.bounds();
                            if ( var29 instanceof Last )
                            {
                                Last var30 = (Last) var29;
                                Bound bound = (Bound) var30.head();
                                var14 = new Some( new Tuple2( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new Function0[]{
                                                compileExpression$1( (Expression) bound.endPoint(), expressionCompiler$1 )}) ) ), ( in ) -> {
                                    return OperatorCodeGenHelperTemplates$.MODULE$.lessThanSeek( property.propertyKeyId(), bound.isInclusive(),
                                            (IntermediateRepresentation) in.head() );
                                }));
                                break label106;
                            }
                        }
                    }

                    if ( var24 )
                    {
                        InequalitySeekRange var32 = var25.range();
                        if ( var32 instanceof RangeGreaterThan )
                        {
                            RangeGreaterThan var33 = (RangeGreaterThan) var32;
                            NonEmptyList var34 = var33.bounds();
                            if ( var34 instanceof Last )
                            {
                                Last var35 = (Last) var34;
                                Bound bound = (Bound) var35.head();
                                var14 = new Some( new Tuple2( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new Function0[]{
                                                compileExpression$1( (Expression) bound.endPoint(), expressionCompiler$1 )}) ) ), ( in ) -> {
                                    return OperatorCodeGenHelperTemplates$.MODULE$.greaterThanSeek( property.propertyKeyId(), bound.isInclusive(),
                                            (IntermediateRepresentation) in.head() );
                                }));
                                break label106;
                            }
                        }
                    }

                    if ( var24 )
                    {
                        InequalitySeekRange var37 = var25.range();
                        if ( var37 instanceof RangeBetween )
                        {
                            RangeBetween var38 = (RangeBetween) var37;
                            RangeGreaterThan var39 = var38.greaterThan();
                            RangeLessThan var40 = var38.lessThan();
                            if ( var39 != null )
                            {
                                NonEmptyList var41 = var39.bounds();
                                if ( var41 instanceof Last )
                                {
                                    Last var42 = (Last) var41;
                                    Bound greaterThan = (Bound) var42.head();
                                    if ( var40 != null )
                                    {
                                        NonEmptyList var44 = var40.bounds();
                                        if ( var44 instanceof Last )
                                        {
                                            Last var45 = (Last) var44;
                                            Bound lessThan = (Bound) var45.head();
                                            var14 = new Some( new Tuple2( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                                    (Object[]) (new Function0[]{
                                                            compileExpression$1( (Expression) greaterThan.endPoint(), expressionCompiler$1 ),
                                                            compileExpression$1( (Expression) lessThan.endPoint(), expressionCompiler$1 )}) ) ), ( in ) -> {
                                                return OperatorCodeGenHelperTemplates$.MODULE$.rangeBetweenSeek( property.propertyKeyId(),
                                                        greaterThan.isInclusive(), (IntermediateRepresentation) in.head(), lessThan.isInclusive(),
                                                        (IntermediateRepresentation) ((IterableLike) in.tail()).head() );
                                            }));
                                            break label106;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    var14 = scala.None..MODULE$;
                }

                var13 = ((Option) var14).map( ( x0$3 ) -> {
                    if ( x0$3 != null )
                    {
                        Seq generateSeekValues = (Seq) x0$3._1();
                        Function1 generatePredicate = (Function1) x0$3._2();
                        SingleRangeSeekQueryNodeIndexSeekTaskTemplate var13 =
                                new SingleRangeSeekQueryNodeIndexSeekTaskTemplate( acc.template(), plan.id(), innermostTemplate$1,
                                        slots$1.getLongOffsetFor( node ), property, generateSeekValues, generatePredicate,
                                        this.operatorFactory.indexRegistrator().registerQueryIndex( label, properties ), order,
                                        (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id$1 ), expressionCompiler$1 );
                        return var13;
                    }
                    else
                    {
                        throw new MatchError( x0$3 );
                    }
                } );
                return (Option) var13;
            }
        }

        var13 = scala.None..MODULE$;
        return (Option) var13;
    }

    private final boolean hasNoNestedArguments$1( final LogicalPlan plan )
    {
        boolean var3;
        label23:
        {
            Object var10000 = this.physicalPlan().applyPlans().apply( plan.id() );
            Id var2 = new Id( org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID());
            if ( var10000 == null )
            {
                if ( var2 == null )
                {
                    break label23;
                }
            }
            else if ( var10000.equals( var2 ) )
            {
                break label23;
            }

            var3 = false;
            return var3;
        }

        var3 = true;
        return var3;
    }
}
