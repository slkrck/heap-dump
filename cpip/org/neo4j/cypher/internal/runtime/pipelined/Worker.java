package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.RuntimeResourceLeakException;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutingQuery;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryManager;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.MatchError;
import scala.Option;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Worker implements Runnable
{
    private final int workerId;
    private final QueryManager queryManager;
    private final Sleeper sleeper;
    private volatile boolean isTimeToStop;

    public Worker( final int workerId, final QueryManager queryManager, final Sleeper sleeper )
    {
        this.workerId = workerId;
        this.queryManager = queryManager;
        this.sleeper = sleeper;
        this.isTimeToStop = false;
    }

    public static String WORKING_THOUGH_RELEASED( final Worker worker )
    {
        return Worker$.MODULE$.WORKING_THOUGH_RELEASED( var0 );
    }

    public static Seq<WorkUnitEvent> NO_WORK()
    {
        return Worker$.MODULE$.NO_WORK();
    }

    public int workerId()
    {
        return this.workerId;
    }

    public Sleeper sleeper()
    {
        return this.sleeper;
    }

    private boolean isTimeToStop()
    {
        return this.isTimeToStop;
    }

    private void isTimeToStop_$eq( final boolean x$1 )
    {
        this.isTimeToStop = x$1;
    }

    public void reset()
    {
        this.isTimeToStop_$eq( false );
    }

    public void stop()
    {
        this.isTimeToStop_$eq( true );
    }

    public boolean isSleeping()
    {
        return this.sleeper().isSleeping();
    }

    public void run()
    {
      .MODULE$.WORKERS().log( "[WORKER%2d] started", BoxesRunTime.boxToInteger( this.workerId() ) );

        while ( !this.isTimeToStop() )
        {
            try
            {
                ExecutingQuery executingQuery = this.queryManager.nextQueryToWorkOn( this.workerId() );
                if ( executingQuery != null )
                {
                    QueryResources resources = executingQuery.workerResourceProvider().resourcesForWorker( this.workerId() );
                    boolean worked = this.workOnQuery( executingQuery, resources );
                    if ( !worked )
                    {
                        this.sleeper().reportIdle();
                    }
                }
                else
                {
                    this.sleeper().reportIdle();
                }
            }
            catch ( Throwable var5 )
            {
            .MODULE$.WORKERS().log( "[WORKER%2d] crashed horribly", BoxesRunTime.boxToInteger( this.workerId() ) );
                var5.printStackTrace();
                throw var5;
            }
        }

      .MODULE$.WORKERS().log( "[WORKER%2d] stopped", BoxesRunTime.boxToInteger( this.workerId() ) );
    }

    public boolean workOnQuery( final ExecutingQuery executingQuery, final QueryResources resources )
    {
        SchedulingResult schedulingResult = this.scheduleNextTask( executingQuery, resources );
        boolean var10000;
        if ( schedulingResult.task() == null )
        {
            var10000 = schedulingResult.someTaskWasFilteredOut();
        }
        else
        {
            try
            {
                Task var8 = (Task) schedulingResult.task();
                if ( var8 instanceof CleanUpTask )
                {
                    CleanUpTask var9 = (CleanUpTask) var8;
                    PreparedOutput var6 = var9.executeWorkUnit( (QueryResources) resources, (WorkUnitEvent) null, (QueryProfiler) null );
                }
                else
                {
                    if ( !(var8 instanceof PipelineTask) )
                    {
                        throw new MatchError( var8 );
                    }

                    PipelineTask var10 = (PipelineTask) var8;
                    PipelineState state = var10.pipelineState();
                    this.executeTask( executingQuery, var10, resources );
                    BoxedUnit var25;
                    if ( var10.canContinue() )
                    {
                        state.putContinuation( var10, false, resources );
                        var25 = BoxedUnit.UNIT;
                    }
                    else
                    {
                        var10.close( resources );
                        var25 = BoxedUnit.UNIT;
                    }
                }

                var10000 = true;
            }
            catch ( Throwable var24 )
            {
                Option var14 = org.neo4j.cypher.internal.NonFatalCypherError..MODULE$.unapply( var24 );
                if ( var14.isEmpty() )
                {
                    throw var24;
                }

                Throwable throwable = (Throwable) var14.get();

                try
                {
                    Task var16 = (Task) schedulingResult.task();
                    if ( !(var16 instanceof PipelineTask) )
                    {
                        throw new MatchError( var16 );
                    }

                    PipelineTask var17 = (PipelineTask) var16;
                    executingQuery.executionState().failQuery( throwable, resources, var17.pipelineState().pipeline() );
                    var17.close( resources );
                    BoxedUnit var5 = BoxedUnit.UNIT;
                }
                catch ( Throwable var23 )
                {
                    Option var20 = org.neo4j.cypher.internal.NonFatalCypherError..MODULE$.unapply( var23 );
                    if ( var20.isEmpty() )
                    {
                        throw var23;
                    }

                    label57:
                    {
                        Throwable t2 = (Throwable) var20.get();
                        if ( throwable == null )
                        {
                            if ( t2 == null )
                            {
                                break label57;
                            }
                        }
                        else if ( throwable.equals( t2 ) )
                        {
                            break label57;
                        }

                        throwable.addSuppressed( t2 );
                    }

                    throwable.printStackTrace();
                    BoxedUnit var4 = BoxedUnit.UNIT;
                }

                boolean var3 = true;
                var10000 = var3;
            }
        }

        return var10000;
    }

    public void executeTask( final ExecutingQuery executingQuery, final PipelineTask task, final QueryResources resources )
    {
        WorkUnitEvent workUnitEvent = null;
        PreparedOutput preparedOutput = null;

        try
        {
            org.neo4j.cypher.internal.runtime.debug.DebugLog..MODULE$.log( "[WORKER%2d] working on %s", BoxesRunTime.boxToInteger( this.workerId() ), task );
         .MODULE$.WORKERS().log( "[WORKER%2d] working on %s of %s", BoxesRunTime.boxToInteger( this.workerId() ), task, executingQuery );
            this.sleeper().reportStartWorkUnit();
            workUnitEvent = executingQuery.queryExecutionTracer().scheduleWorkUnit( task, this.upstreamWorkUnitEvents( task ) ).start();
            preparedOutput = task.executeWorkUnit( resources, workUnitEvent, executingQuery.workersQueryProfiler().queryProfiler( this.workerId() ) );
        }
        finally
        {
            if ( workUnitEvent != null )
            {
                workUnitEvent.stop();
                this.sleeper().reportStopWorkUnit();
            }
        }

        preparedOutput.produce();
    }

    public SchedulingResult<Task<QueryResources>> scheduleNextTask( final ExecutingQuery executingQuery, final QueryResources resources )
    {
        SchedulingResult var10000;
        try
        {
            var10000 = executingQuery.querySchedulingPolicy().nextTask( resources );
        }
        catch ( Throwable var23 )
        {
            SchedulingResult var3;
            label46:
            {
                boolean var5 = false;
                NextTaskException var6 = null;
                if ( var23 instanceof NextTaskException )
                {
                    var5 = true;
                    var6 = (NextTaskException) var23;
                    ExecutablePipeline pipeline = var6.pipeline();
                    Throwable var9 = var6.cause();
                    if ( var9 instanceof SchedulingInputException )
                    {
                        SchedulingInputException var10 = (SchedulingInputException) var9;
                        Object morsel = var10.input();
                        Throwable cause = var10.cause();
                        if ( morsel instanceof MorselParallelizer )
                        {
                            MorselParallelizer var13 = (MorselParallelizer) morsel;
                            executingQuery.executionState().closeMorselTask( pipeline, var13.originalForClosing() );
                            executingQuery.executionState().failQuery( cause, resources, pipeline );
                            var3 = new SchedulingResult( (Object) null, true );
                            break label46;
                        }
                    }
                }

                if ( var5 )
                {
                    ExecutablePipeline pipeline = var6.pipeline();
                    Throwable var15 = var6.cause();
                    if ( var15 instanceof SchedulingInputException )
                    {
                        SchedulingInputException var16 = (SchedulingInputException) var15;
                        Object acc = var16.input();
                        Throwable cause = var16.cause();
                        if ( acc instanceof ArgumentStateMap.MorselAccumulator )
                        {
                            ArgumentStateMap.MorselAccumulator var19 = (ArgumentStateMap.MorselAccumulator) acc;
                            executingQuery.executionState().closeAccumulatorTask( pipeline, var19 );
                            executingQuery.executionState().failQuery( cause, resources, pipeline );
                            var3 = new SchedulingResult( (Object) null, true );
                            break label46;
                        }
                    }
                }

                if ( var5 )
                {
                    ExecutablePipeline pipeline = var6.pipeline();
                    Throwable cause = var6.cause();
                    executingQuery.executionState().failQuery( cause, resources, pipeline );
                    var3 = new SchedulingResult( (Object) null, false );
                }
                else
                {
                    if ( var23 == null )
                    {
                        throw var23;
                    }

                    executingQuery.executionState().failQuery( var23, resources, (ExecutablePipeline) null );
                    var3 = new SchedulingResult( (Object) null, false );
                }
            }

            var10000 = var3;
        }

        return var10000;
    }

    public void assertIsNotActive()
    {
        if ( this.sleeper().isWorking() )
        {
            throw new RuntimeResourceLeakException( Worker$.MODULE$.WORKING_THOUGH_RELEASED( this ) );
        }
    }

    private Seq<WorkUnitEvent> upstreamWorkUnitEvents( final PipelineTask task )
    {
        WorkUnitEvent upstreamWorkUnitEvent = task.startTask().producingWorkUnitEvent();
        return (Seq) (upstreamWorkUnitEvent != null ? scala.Predef..MODULE$.wrapRefArray( (Object[]) ((Object[]) (new WorkUnitEvent[]{upstreamWorkUnitEvent})) ) :
        Worker$.MODULE$.NO_WORK());
    }

    public String toString()
    {
        return (new StringBuilder( 10 )).append( "Worker[" ).append( this.workerId() ).append( ", " ).append( this.sleeper().statusString() ).append(
                "]" ).toString();
    }
}
