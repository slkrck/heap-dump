package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.physicalplanning.BufferDefinition;
import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledStreamingOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NoOutputOperator$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.Operator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.HasWorkIdentity;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple11;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class ExecutablePipeline implements WorkIdentity, Product, Serializable
{
    private final int id;
    private final int lhs;
    private final int rhs;
    private final Operator start;
    private final MiddleOperator[] middleOperators;
    private final boolean serial;
    private final SlotConfiguration slots;
    private final BufferDefinition inputBuffer;
    private final OutputOperator outputOperator;
    private final boolean needsMorsel;
    private final boolean needsFilteringMorsel;
    private final int workId;
    private final String workDescription;

    public ExecutablePipeline( final int id, final int lhs, final int rhs, final Operator start, final MiddleOperator[] middleOperators, final boolean serial,
            final SlotConfiguration slots, final BufferDefinition inputBuffer, final OutputOperator outputOperator, final boolean needsMorsel,
            final boolean needsFilteringMorsel )
    {
        this.id = id;
        this.lhs = lhs;
        this.rhs = rhs;
        this.start = start;
        this.middleOperators = middleOperators;
        this.serial = serial;
        this.slots = slots;
        this.inputBuffer = inputBuffer;
        this.outputOperator = outputOperator;
        this.needsMorsel = needsMorsel;
        this.needsFilteringMorsel = needsFilteringMorsel;
        WorkIdentity.$init$( this );
        Product.$init$( this );
        this.workId = start.workIdentity().workId();
        this.workDescription = this.composeWorkDescriptions( start, scala.Predef..MODULE$.wrapRefArray( (Object[]) middleOperators ), outputOperator);
    }

    public static boolean apply$default$11()
    {
        return ExecutablePipeline$.MODULE$.apply$default$11();
    }

    public static boolean apply$default$10()
    {
        return ExecutablePipeline$.MODULE$.apply$default$10();
    }

    public static boolean $lessinit$greater$default$11()
    {
        return ExecutablePipeline$.MODULE$.$lessinit$greater$default$11();
    }

    public static boolean $lessinit$greater$default$10()
    {
        return ExecutablePipeline$.MODULE$.$lessinit$greater$default$10();
    }

    public static Option<Tuple11<PipelineId,PipelineId,PipelineId,Operator,MiddleOperator[],Object,SlotConfiguration,BufferDefinition,OutputOperator,Object,Object>> unapply(
            final ExecutablePipeline x$0 )
    {
        return ExecutablePipeline$.MODULE$.unapply( var0 );
    }

    public static ExecutablePipeline apply( final int id, final int lhs, final int rhs, final Operator start, final MiddleOperator[] middleOperators,
            final boolean serial, final SlotConfiguration slots, final BufferDefinition inputBuffer, final OutputOperator outputOperator,
            final boolean needsMorsel, final boolean needsFilteringMorsel )
    {
        return ExecutablePipeline$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10 );
    }

    public static Function1<Tuple11<PipelineId,PipelineId,PipelineId,Operator,MiddleOperator[],Object,SlotConfiguration,BufferDefinition,OutputOperator,Object,Object>,ExecutablePipeline> tupled()
    {
        return ExecutablePipeline$.MODULE$.tupled();
    }

    public static Function1<PipelineId,Function1<PipelineId,Function1<PipelineId,Function1<Operator,Function1<MiddleOperator[],Function1<Object,Function1<SlotConfiguration,Function1<BufferDefinition,Function1<OutputOperator,Function1<Object,Function1<Object,ExecutablePipeline>>>>>>>>>>> curried()
    {
        return ExecutablePipeline$.MODULE$.curried();
    }

    public int id()
    {
        return this.id;
    }

    public int lhs()
    {
        return this.lhs;
    }

    public int rhs()
    {
        return this.rhs;
    }

    public Operator start()
    {
        return this.start;
    }

    public MiddleOperator[] middleOperators()
    {
        return this.middleOperators;
    }

    public boolean serial()
    {
        return this.serial;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public BufferDefinition inputBuffer()
    {
        return this.inputBuffer;
    }

    public OutputOperator outputOperator()
    {
        return this.outputOperator;
    }

    public boolean needsMorsel()
    {
        return this.needsMorsel;
    }

    public boolean needsFilteringMorsel()
    {
        return this.needsFilteringMorsel;
    }

    public boolean isFused()
    {
        Operator var2 = this.start();
        boolean var1;
        if ( var2 instanceof CompiledStreamingOperator )
        {
            var1 = true;
        }
        else
        {
            var1 = false;
        }

        return var1;
    }

    public PipelineState createState( final ExecutionState executionState, final QueryContext queryContext, final QueryState queryState,
            final QueryResources resources, final StateFactory stateFactory )
    {
        OperatorTask[] middleTasks = new OperatorTask[this.middleOperators().length];

        for ( int i = 0; i < middleTasks.length; ++i )
        {
            middleTasks[i] = this.middleOperators()[i].createTask( executionState, stateFactory, queryContext, queryState, resources );
        }

        return new PipelineState( this, this.start().createState( executionState, stateFactory, queryContext, queryState, resources ), middleTasks,
                ( operatorTask ) -> {
                    return this.outputOperatorStateFor$1( operatorTask, executionState );
                }, executionState );
    }

    public int workId()
    {
        return this.workId;
    }

    public String workDescription()
    {
        return this.workDescription;
    }

    private String composeWorkDescriptions( final HasWorkIdentity first, final Seq<HasWorkIdentity> others, final HasWorkIdentity last )
    {
        Seq workIdentities = (Seq) ((TraversableLike) ((TraversableLike).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new HasWorkIdentity[]{first}) ))).$plus$plus( others,.MODULE$.canBuildFrom())).
        $plus$plus(.MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new HasWorkIdentity[]{last}) )), .MODULE$.canBuildFrom());
        return String.valueOf( ((TraversableOnce) workIdentities.map( ( x$1 ) -> {
            return x$1.workIdentity().workDescription();
        },.MODULE$.canBuildFrom()) ).mkString( "," ));
    }

    public String toString()
    {
        return this.workDescription();
    }

    public ExecutablePipeline copy( final int id, final int lhs, final int rhs, final Operator start, final MiddleOperator[] middleOperators,
            final boolean serial, final SlotConfiguration slots, final BufferDefinition inputBuffer, final OutputOperator outputOperator,
            final boolean needsMorsel, final boolean needsFilteringMorsel )
    {
        return new ExecutablePipeline( id, lhs, rhs, start, middleOperators, serial, slots, inputBuffer, outputOperator, needsMorsel, needsFilteringMorsel );
    }

    public int copy$default$1()
    {
        return this.id();
    }

    public boolean copy$default$10()
    {
        return this.needsMorsel();
    }

    public boolean copy$default$11()
    {
        return this.needsFilteringMorsel();
    }

    public int copy$default$2()
    {
        return this.lhs();
    }

    public int copy$default$3()
    {
        return this.rhs();
    }

    public Operator copy$default$4()
    {
        return this.start();
    }

    public MiddleOperator[] copy$default$5()
    {
        return this.middleOperators();
    }

    public boolean copy$default$6()
    {
        return this.serial();
    }

    public SlotConfiguration copy$default$7()
    {
        return this.slots();
    }

    public BufferDefinition copy$default$8()
    {
        return this.inputBuffer();
    }

    public OutputOperator copy$default$9()
    {
        return this.outputOperator();
    }

    public String productPrefix()
    {
        return "ExecutablePipeline";
    }

    public int productArity()
    {
        return 11;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new PipelineId( this.id() );
            break;
        case 1:
            var10000 = new PipelineId( this.lhs() );
            break;
        case 2:
            var10000 = new PipelineId( this.rhs() );
            break;
        case 3:
            var10000 = this.start();
            break;
        case 4:
            var10000 = this.middleOperators();
            break;
        case 5:
            var10000 = BoxesRunTime.boxToBoolean( this.serial() );
            break;
        case 6:
            var10000 = this.slots();
            break;
        case 7:
            var10000 = this.inputBuffer();
            break;
        case 8:
            var10000 = this.outputOperator();
            break;
        case 9:
            var10000 = BoxesRunTime.boxToBoolean( this.needsMorsel() );
            break;
        case 10:
            var10000 = BoxesRunTime.boxToBoolean( this.needsFilteringMorsel() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ExecutablePipeline;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( new PipelineId( this.id() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( new PipelineId( this.lhs() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( new PipelineId( this.rhs() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.start() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.middleOperators() ) );
        var1 = Statics.mix( var1, this.serial() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.inputBuffer() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.outputOperator() ) );
        var1 = Statics.mix( var1, this.needsMorsel() ? 1231 : 1237 );
        var1 = Statics.mix( var1, this.needsFilteringMorsel() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 11 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label95:
            {
                boolean var2;
                if ( x$1 instanceof ExecutablePipeline )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label77:
                    {
                        ExecutablePipeline var4 = (ExecutablePipeline) x$1;
                        if ( this.id() == var4.id() && this.lhs() == var4.lhs() && this.rhs() == var4.rhs() )
                        {
                            label73:
                            {
                                Operator var10000 = this.start();
                                Operator var5 = var4.start();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label73;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label73;
                                }

                                if ( this.middleOperators() == var4.middleOperators() && this.serial() == var4.serial() )
                                {
                                    label86:
                                    {
                                        SlotConfiguration var9 = this.slots();
                                        SlotConfiguration var6 = var4.slots();
                                        if ( var9 == null )
                                        {
                                            if ( var6 != null )
                                            {
                                                break label86;
                                            }
                                        }
                                        else if ( !var9.equals( var6 ) )
                                        {
                                            break label86;
                                        }

                                        BufferDefinition var10 = this.inputBuffer();
                                        BufferDefinition var7 = var4.inputBuffer();
                                        if ( var10 == null )
                                        {
                                            if ( var7 != null )
                                            {
                                                break label86;
                                            }
                                        }
                                        else if ( !var10.equals( var7 ) )
                                        {
                                            break label86;
                                        }

                                        OutputOperator var11 = this.outputOperator();
                                        OutputOperator var8 = var4.outputOperator();
                                        if ( var11 == null )
                                        {
                                            if ( var8 != null )
                                            {
                                                break label86;
                                            }
                                        }
                                        else if ( !var11.equals( var8 ) )
                                        {
                                            break label86;
                                        }

                                        if ( this.needsMorsel() == var4.needsMorsel() && this.needsFilteringMorsel() == var4.needsFilteringMorsel() &&
                                                var4.canEqual( this ) )
                                        {
                                            var12 = true;
                                            break label77;
                                        }
                                    }
                                }
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label95;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }

    private final OutputOperatorState outputOperatorStateFor$1( final OperatorTask operatorTask, final ExecutionState executionState$1 )
    {
        Tuple2 var4 = new Tuple2( operatorTask, this.outputOperator() );
        OutputOperatorState var3;
        if ( var4 != null )
        {
            OperatorTask compiledTask = (OperatorTask) var4._1();
            OutputOperator var6 = (OutputOperator) var4._2();
            if ( compiledTask instanceof CompiledTask )
            {
                CompiledTask var7 = (CompiledTask) compiledTask;
                if ( NoOutputOperator$.MODULE$.equals( var6 ) && (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.middleOperators() )) ).isEmpty())
                {
                    var3 = var7.createState( executionState$1, this.id() );
                    return var3;
                }
            }
        }

        var3 = this.outputOperator().createState( executionState$1, this.id() );
        return var3;
    }
}
