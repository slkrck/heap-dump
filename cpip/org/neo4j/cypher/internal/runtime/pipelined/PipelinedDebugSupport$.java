package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OptionalOperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStream;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.EndOfEmptyStream;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.EndOfNonEmptyStream$;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.NotTheEnd$;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.MatchError;
import scala.Predef.;
import scala.collection.GenTraversableOnce;
import scala.collection.Seq;
import scala.collection.mutable.ArrayOps.ofLong;
import scala.collection.mutable.ArrayOps.ofRef;

public final class PipelinedDebugSupport$
{
    public static PipelinedDebugSupport$ MODULE$;

    static
    {
        new PipelinedDebugSupport$();
    }

    private final String MORSEL_INDENT;

    private PipelinedDebugSupport$()
    {
        MODULE$ = this;
        this.MORSEL_INDENT = "  ";
    }

    private String MORSEL_INDENT()
    {
        return this.MORSEL_INDENT;
    }

    public Seq<String> prettyStartTask( final ContinuableOperatorTask startTask, final WorkIdentity workIdentity )
    {
        Seq var3;
        if ( startTask instanceof ContinuableOperatorTaskWithMorsel )
        {
            ContinuableOperatorTaskWithMorsel var5 = (ContinuableOperatorTaskWithMorsel) startTask;
            var3 = (Seq) this.prettyMorselWithHeader( "INPUT:", var5.inputMorsel() ).$plus$plus(
                    new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{workIdentity.toString()})) ) ), scala.collection.Seq..
            MODULE$.canBuildFrom());
        }
        else if ( startTask instanceof OptionalOperatorTask )
        {
            OptionalOperatorTask var6 = (OptionalOperatorTask) startTask;
            var3 = (Seq) (new ofRef(.MODULE$.refArrayOps(
                    (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{"INPUT:"})) )) ).$plus$plus(
                    this.prettyStreamedData( var6.morselData() ), scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( String.class )))))).
            $plus$plus( new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{workIdentity.toString()})) ) ), scala.Array..
            MODULE$.fallbackCanBuildFrom( scala.Predef.DummyImplicit..MODULE$.dummyImplicit()));
        }
        else
        {
            if ( !(startTask instanceof ContinuableOperatorTaskWithAccumulator) )
            {
                throw new MatchError( startTask );
            }

            ContinuableOperatorTaskWithAccumulator var7 = (ContinuableOperatorTaskWithAccumulator) startTask;
            var3 = (Seq) scala.collection.Seq..
            MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new String[]{"INPUT:", var7.accumulator().toString(), var7.workIdentity().toString()}) ));
        }

        return var3;
    }

    public Seq<String> prettyPostStartTask( final ContinuableOperatorTask startTask )
    {
        Object var2;
        if ( startTask instanceof ContinuableOperatorTaskWithMorsel )
        {
            ContinuableOperatorTaskWithMorsel var4 = (ContinuableOperatorTaskWithMorsel) startTask;
            var2 = this.prettyMorselWithHeader(
                    (new StringBuilder( 27 )).append( "INPUT POST (canContinue: " ).append( startTask.canContinue() ).append( "):" ).toString(),
                    var4.inputMorsel() );
        }
        else
        {
            if ( startTask == null )
            {
                throw new MatchError( startTask );
            }

            var2 = .MODULE$.wrapRefArray( (Object[]) ((Object[]) (new String[]{
                (new StringBuilder( 26 )).append( "INPUT POST (canContinue: " ).append( startTask.canContinue() ).append( ")" ).toString()})) );
        }

        return (Seq) var2;
    }

    public Seq<String> prettyWork( final MorselExecutionContext morsel, final WorkIdentity workIdentity )
    {
        return (Seq) this.prettyMorselWithHeader( "OUTPUT:", morsel ).$plus$plus(
                new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{workIdentity.toString(), ""})) ) ),scala.collection.Seq..
        MODULE$.canBuildFrom());
    }

    public Seq<String> prettyMorselWithHeader( final String header, final MorselExecutionContext morsel )
    {
        return (Seq) (new ofRef(.MODULE$.refArrayOps(
                (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{header})) )) ).$plus$plus( morsel.prettyString(),
                scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( String.class )))))).map( ( row ) -> {
        return (new StringBuilder( 0 )).append( MODULE$.MORSEL_INDENT() ).append( row ).toString();
    }, scala.Array..MODULE$.fallbackCanBuildFrom( scala.Predef.DummyImplicit..MODULE$.dummyImplicit()));
    }

    public Seq<String> prettyWorkDone()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new String[]{"------------------------------------------------------------------------------------"}) ))
        ;
    }

    private Seq<String> prettyStreamedData( final MorselData streamedData )
    {
        ofRef var10000 = new ofRef(.MODULE$.refArrayOps( (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{
                (new StringBuilder( 35 )).append( "MorselData with downstream arg ids " ).append(
                        (new ofLong(.MODULE$.longArrayOps( streamedData.argumentRowIdsForReducers() )) ).toSeq()).toString()}))))).
        $plus$plus( (GenTraversableOnce) streamedData.morsels().flatMap( ( morsel ) -> {
            return MODULE$.prettyMorselWithHeader( "", morsel );
        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( String.class )))));
        ArgumentStream var3 = streamedData.argumentStream();
        Object var2;
        if ( var3 instanceof EndOfEmptyStream )
        {
            EndOfEmptyStream var4 = (EndOfEmptyStream) var3;
            MorselExecutionContext argRow = var4.viewOfArgumentRow();
            var2 = this.prettyMorselWithHeader( "EndOfEmptyStream", argRow );
        }
        else if ( EndOfNonEmptyStream$.MODULE$.equals( var3 ) )
        {
            var2 = new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{"EndOfNonEmptyStream"})) ));
        }
        else
        {
            if ( !NotTheEnd$.MODULE$.equals( var3 ) )
            {
                throw new MatchError( var3 );
            }

            var2 = new ofRef(.MODULE$.refArrayOps( (Object[]) ((Object[]) (new String[]{"NotTheEnd"})) ));
        }

        return (Seq) var10000.$plus$plus( (GenTraversableOnce) var2, scala.Array..MODULE$.fallbackCanBuildFrom(
                scala.Predef.DummyImplicit..MODULE$.dummyImplicit()));
    }
}
