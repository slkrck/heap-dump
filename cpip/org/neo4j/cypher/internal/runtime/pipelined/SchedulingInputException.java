package org.neo4j.cypher.internal.runtime.pipelined;

import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SchedulingInputException<T> extends Exception implements Product, Serializable
{
    private final T input;
    private final Throwable cause;

    public SchedulingInputException( final T input, final Throwable cause )
    {
        super( cause );
        this.input = input;
        this.cause = cause;
        Product.$init$( this );
    }

    public static <T> Option<Tuple2<T,Throwable>> unapply( final SchedulingInputException<T> x$0 )
    {
        return SchedulingInputException$.MODULE$.unapply( var0 );
    }

    public static <T> SchedulingInputException<T> apply( final T input, final Throwable cause )
    {
        return SchedulingInputException$.MODULE$.apply( var0, var1 );
    }

    public T input()
    {
        return this.input;
    }

    public Throwable cause()
    {
        return this.cause;
    }

    public <T> SchedulingInputException<T> copy( final T input, final Throwable cause )
    {
        return new SchedulingInputException( input, cause );
    }

    public <T> T copy$default$1()
    {
        return this.input();
    }

    public <T> Throwable copy$default$2()
    {
        return this.cause();
    }

    public String productPrefix()
    {
        return "SchedulingInputException";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.input();
            break;
        case 1:
            var10000 = this.cause();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SchedulingInputException;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof SchedulingInputException )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        SchedulingInputException var4 = (SchedulingInputException) x$1;
                        if ( BoxesRunTime.equals( this.input(), var4.input() ) )
                        {
                            label36:
                            {
                                Throwable var10000 = this.cause();
                                Throwable var5 = var4.cause();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label36;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label36;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label38;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
