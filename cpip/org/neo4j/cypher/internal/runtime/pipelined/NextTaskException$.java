package org.neo4j.cypher.internal.runtime.pipelined;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class NextTaskException$ extends AbstractFunction2<ExecutablePipeline,Throwable,NextTaskException> implements Serializable
{
    public static NextTaskException$ MODULE$;

    static
    {
        new NextTaskException$();
    }

    private NextTaskException$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NextTaskException";
    }

    public NextTaskException apply( final ExecutablePipeline pipeline, final Throwable cause )
    {
        return new NextTaskException( pipeline, cause );
    }

    public Option<Tuple2<ExecutablePipeline,Throwable>> unapply( final NextTaskException x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.pipeline(), x$0.cause() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
