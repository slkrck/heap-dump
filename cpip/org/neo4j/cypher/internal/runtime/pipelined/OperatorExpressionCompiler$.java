package org.neo4j.cypher.internal.runtime.pipelined;

public final class OperatorExpressionCompiler$
{
    public static OperatorExpressionCompiler$ MODULE$;

    static
    {
        new OperatorExpressionCompiler$();
    }

    private OperatorExpressionCompiler$()
    {
        MODULE$ = this;
    }
}
