package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CsvFileDataWriter extends CsvDataWriter
{
    private final BufferedWriter out;

    public CsvFileDataWriter( final File file )
    {
        this.out = new BufferedWriter( new FileWriter( file ) );
        this.writeRow( this.header() );
    }

    private BufferedWriter out()
    {
        return this.out;
    }

    public void flush()
    {
        this.out().flush();
    }

    public void close()
    {
        this.out().close();
    }

    public void writeRow( final String row )
    {
        this.out().write( row );
    }
}
