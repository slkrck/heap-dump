package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface DataPointWriter
{
    void write( final DataPoint dataPoint );
}
