package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple9;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class DataPoint implements Product, Serializable
{
    private final long id;
    private final Seq<Object> upstreamIds;
    private final int queryId;
    private final long schedulingThreadId;
    private final long scheduledTime;
    private final long executionThreadId;
    private final long startTime;
    private final long stopTime;
    private final WorkIdentity workId;

    public DataPoint( final long id, final Seq<Object> upstreamIds, final int queryId, final long schedulingThreadId, final long scheduledTime,
            final long executionThreadId, final long startTime, final long stopTime, final WorkIdentity workId )
    {
        this.id = id;
        this.upstreamIds = upstreamIds;
        this.queryId = queryId;
        this.schedulingThreadId = schedulingThreadId;
        this.scheduledTime = scheduledTime;
        this.executionThreadId = executionThreadId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.workId = workId;
        Product.$init$( this );
    }

    public static Option<Tuple9<Object,Seq<Object>,Object,Object,Object,Object,Object,Object,WorkIdentity>> unapply( final DataPoint x$0 )
    {
        return DataPoint$.MODULE$.unapply( var0 );
    }

    public static DataPoint apply( final long id, final Seq<Object> upstreamIds, final int queryId, final long schedulingThreadId, final long scheduledTime,
            final long executionThreadId, final long startTime, final long stopTime, final WorkIdentity workId )
    {
        return DataPoint$.MODULE$.apply( var0, var2, var3, var4, var6, var8, var10, var12, var14 );
    }

    public static Function1<Tuple9<Object,Seq<Object>,Object,Object,Object,Object,Object,Object,WorkIdentity>,DataPoint> tupled()
    {
        return DataPoint$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Seq<Object>,Function1<Object,Function1<Object,Function1<Object,Function1<Object,Function1<Object,Function1<Object,Function1<WorkIdentity,DataPoint>>>>>>>>> curried()
    {
        return DataPoint$.MODULE$.curried();
    }

    public long id()
    {
        return this.id;
    }

    public Seq<Object> upstreamIds()
    {
        return this.upstreamIds;
    }

    public int queryId()
    {
        return this.queryId;
    }

    public long schedulingThreadId()
    {
        return this.schedulingThreadId;
    }

    public long scheduledTime()
    {
        return this.scheduledTime;
    }

    public long executionThreadId()
    {
        return this.executionThreadId;
    }

    public long startTime()
    {
        return this.startTime;
    }

    public long stopTime()
    {
        return this.stopTime;
    }

    public WorkIdentity workId()
    {
        return this.workId;
    }

    public DataPoint withTimeZero( final long t0 )
    {
        return new DataPoint( this.id(), this.upstreamIds(), this.queryId(), this.schedulingThreadId(), this.scheduledTime() - t0, this.executionThreadId(),
                this.startTime() - t0, this.stopTime() - t0, this.workId() );
    }

    public DataPoint copy( final long id, final Seq<Object> upstreamIds, final int queryId, final long schedulingThreadId, final long scheduledTime,
            final long executionThreadId, final long startTime, final long stopTime, final WorkIdentity workId )
    {
        return new DataPoint( id, upstreamIds, queryId, schedulingThreadId, scheduledTime, executionThreadId, startTime, stopTime, workId );
    }

    public long copy$default$1()
    {
        return this.id();
    }

    public Seq<Object> copy$default$2()
    {
        return this.upstreamIds();
    }

    public int copy$default$3()
    {
        return this.queryId();
    }

    public long copy$default$4()
    {
        return this.schedulingThreadId();
    }

    public long copy$default$5()
    {
        return this.scheduledTime();
    }

    public long copy$default$6()
    {
        return this.executionThreadId();
    }

    public long copy$default$7()
    {
        return this.startTime();
    }

    public long copy$default$8()
    {
        return this.stopTime();
    }

    public WorkIdentity copy$default$9()
    {
        return this.workId();
    }

    public String productPrefix()
    {
        return "DataPoint";
    }

    public int productArity()
    {
        return 9;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToLong( this.id() );
            break;
        case 1:
            var10000 = this.upstreamIds();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.queryId() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToLong( this.schedulingThreadId() );
            break;
        case 4:
            var10000 = BoxesRunTime.boxToLong( this.scheduledTime() );
            break;
        case 5:
            var10000 = BoxesRunTime.boxToLong( this.executionThreadId() );
            break;
        case 6:
            var10000 = BoxesRunTime.boxToLong( this.startTime() );
            break;
        case 7:
            var10000 = BoxesRunTime.boxToLong( this.stopTime() );
            break;
        case 8:
            var10000 = this.workId();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof DataPoint;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.longHash( this.id() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.upstreamIds() ) );
        var1 = Statics.mix( var1, this.queryId() );
        var1 = Statics.mix( var1, Statics.longHash( this.schedulingThreadId() ) );
        var1 = Statics.mix( var1, Statics.longHash( this.scheduledTime() ) );
        var1 = Statics.mix( var1, Statics.longHash( this.executionThreadId() ) );
        var1 = Statics.mix( var1, Statics.longHash( this.startTime() ) );
        var1 = Statics.mix( var1, Statics.longHash( this.stopTime() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.workId() ) );
        return Statics.finalizeHash( var1, 9 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label76:
            {
                boolean var2;
                if ( x$1 instanceof DataPoint )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label59:
                    {
                        DataPoint var4 = (DataPoint) x$1;
                        if ( this.id() == var4.id() )
                        {
                            label57:
                            {
                                Seq var10000 = this.upstreamIds();
                                Seq var5 = var4.upstreamIds();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label57;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label57;
                                }

                                if ( this.queryId() == var4.queryId() && this.schedulingThreadId() == var4.schedulingThreadId() &&
                                        this.scheduledTime() == var4.scheduledTime() && this.executionThreadId() == var4.executionThreadId() &&
                                        this.startTime() == var4.startTime() && this.stopTime() == var4.stopTime() )
                                {
                                    label46:
                                    {
                                        WorkIdentity var7 = this.workId();
                                        WorkIdentity var6 = var4.workId();
                                        if ( var7 == null )
                                        {
                                            if ( var6 != null )
                                            {
                                                break label46;
                                            }
                                        }
                                        else if ( !var7.equals( var6 ) )
                                        {
                                            break label46;
                                        }

                                        if ( var4.canEqual( this ) )
                                        {
                                            var8 = true;
                                            break label59;
                                        }
                                    }
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label76;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
