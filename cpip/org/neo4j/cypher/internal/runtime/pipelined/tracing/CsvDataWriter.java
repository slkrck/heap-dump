package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import java.util.concurrent.TimeUnit;

import scala.Predef.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class CsvDataWriter implements DataPointFlusher
{
    public final void write( final DataPoint dp )
    {
        this.writeRow( this.serialize( dp ) );
    }

    public abstract void writeRow( final String row );

    public String header()
    {
        return CsvDataWriter$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$HEADER();
    }

    private String serialize( final DataPoint dataPoint )
    {
        return (new StringBuilder( 0 )).append(.MODULE$.genericArrayOps( scala.Array..MODULE$.apply(.MODULE$.genericWrapArray(
                new Object[]{BoxesRunTime.boxToLong( dataPoint.id() ).toString(), dataPoint.upstreamIds().mkString( "[", ";", "]" ),
                        BoxesRunTime.boxToInteger( dataPoint.queryId() ).toString(), BoxesRunTime.boxToLong( dataPoint.schedulingThreadId() ).toString(),
                        BoxesRunTime.boxToLong( TimeUnit.NANOSECONDS.toMicros( dataPoint.scheduledTime() ) ).toString(),
                        BoxesRunTime.boxToLong( dataPoint.executionThreadId() ).toString(),
                        BoxesRunTime.boxToLong( TimeUnit.NANOSECONDS.toMicros( dataPoint.startTime() ) ).toString(),
                        BoxesRunTime.boxToLong( TimeUnit.NANOSECONDS.toMicros( dataPoint.stopTime() ) ).toString(),
                        BoxesRunTime.boxToInteger( dataPoint.workId().workId() ), dataPoint.workId().workDescription()} ),scala.reflect.ClassTag..MODULE$.Any())).
        mkString( CsvDataWriter$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$SEPARATOR() )).
        append( System.lineSeparator() ).toString();
    }
}
