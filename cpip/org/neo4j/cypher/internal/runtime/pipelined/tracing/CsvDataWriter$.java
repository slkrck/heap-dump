package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import scala.Predef.;
import scala.collection.mutable.ArrayOps.ofRef;

public final class CsvDataWriter$
{
    public static CsvDataWriter$ MODULE$;

    static
    {
        new CsvDataWriter$();
    }

    private final String org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$SEPARATOR;
    private final String VERSION;
    private final String org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$HEADER;

    private CsvDataWriter$()
    {
        MODULE$ = this;
        this.org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$SEPARATOR = ",";
        this.VERSION = "1.0";
        this.org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$HEADER = (new StringBuilder( 0 )).append( (new ofRef(.MODULE$.refArrayOps(
                (Object[]) ((Object[]) (new String[]{(new StringBuilder( 3 )).append( "id_" ).append( this.VERSION() ).toString(), "upstreamIds", "queryId",
                        "schedulingThreadId", "schedulingTime(us)", "executionThreadId", "startTime(us)", "stopTime(us)", "pipelineId",
                        "pipelineDescription"})) )) ).mkString( this.org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$SEPARATOR() )).
        append( System.lineSeparator() ).toString();
    }

    public String org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$SEPARATOR()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$SEPARATOR;
    }

    private String VERSION()
    {
        return this.VERSION;
    }

    public String org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$HEADER()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$CsvDataWriter$$HEADER;
    }
}
