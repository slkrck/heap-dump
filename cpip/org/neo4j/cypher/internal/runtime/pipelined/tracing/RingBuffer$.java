package org.neo4j.cypher.internal.runtime.pipelined.tracing;

public final class RingBuffer$
{
    public static RingBuffer$ MODULE$;

    static
    {
        new RingBuffer$();
    }

    private final int defaultBitSize;
    private final int defaultMaxRetries;

    private RingBuffer$()
    {
        MODULE$ = this;
        this.defaultBitSize = 10;
        this.defaultMaxRetries = 1000;
    }

    public int $lessinit$greater$default$1()
    {
        return this.defaultBitSize();
    }

    public int $lessinit$greater$default$2()
    {
        return this.defaultMaxRetries();
    }

    public int defaultBitSize()
    {
        return this.defaultBitSize;
    }

    public int defaultMaxRetries()
    {
        return this.defaultMaxRetries;
    }
}
