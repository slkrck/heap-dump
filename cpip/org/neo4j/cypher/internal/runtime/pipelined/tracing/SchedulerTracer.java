package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SchedulerTracer
{
    static WorkUnitEvent NoWorkUnitEvent()
    {
        return SchedulerTracer$.MODULE$.NoWorkUnitEvent();
    }

    static ScheduledWorkUnitEvent NoScheduledWorkUnitEvent()
    {
        return SchedulerTracer$.MODULE$.NoScheduledWorkUnitEvent();
    }

    static QueryExecutionTracer NoQueryExecutionTracer()
    {
        return SchedulerTracer$.MODULE$.NoQueryExecutionTracer();
    }

    static SchedulerTracer NoSchedulerTracer()
    {
        return SchedulerTracer$.MODULE$.NoSchedulerTracer();
    }

    QueryExecutionTracer traceQuery();
}
