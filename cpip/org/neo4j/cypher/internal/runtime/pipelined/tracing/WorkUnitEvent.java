package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface WorkUnitEvent
{
    long id();

    void stop();
}
