package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

import scala.Predef.;
import scala.collection.TraversableOnce;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.ArrayOps.ofLong;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SingleConsumerDataBuffers implements DataPointWriter
{
    private final int ringBufferBitSize;
    private final int ringBufferMaxRetries;
    private final AtomicLong org$neo4j$cypher$internal$runtime$pipelined$tracing$SingleConsumerDataBuffers$$localIds;
    private final ThreadLocal<Object> localThreadId;
    private final int MAX_CLIENT_THREADS;
    private final RingBuffer[] buffersByThread;
    private long t0;
    private Thread theConsumer;

    public SingleConsumerDataBuffers( final int ringBufferBitSize, final int ringBufferMaxRetries )
    {
        this.ringBufferBitSize = ringBufferBitSize;
        this.ringBufferMaxRetries = ringBufferMaxRetries;
        this.org$neo4j$cypher$internal$runtime$pipelined$tracing$SingleConsumerDataBuffers$$localIds = new AtomicLong();
        this.localThreadId = ThreadLocal.withInitial( new Supplier<Object>( this )
        {
            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                }
            }

            public long get()
            {
                return this.$outer.org$neo4j$cypher$internal$runtime$pipelined$tracing$SingleConsumerDataBuffers$$localIds().getAndIncrement();
            }
        } );
        this.MAX_CLIENT_THREADS = 1024;
        this.buffersByThread =
                (RingBuffer[]) ((TraversableOnce) scala.runtime.RichInt..MODULE$.until$extension0(.MODULE$.intWrapper( 0 ), this.MAX_CLIENT_THREADS()).
        map( ( x$1 ) -> {
            return $anonfun$buffersByThread$1( this, BoxesRunTime.unboxToInt( x$1 ) );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( RingBuffer.class ));
        this.t0 = -1L;
    }

    public static int $lessinit$greater$default$2()
    {
        return SingleConsumerDataBuffers$.MODULE$.$lessinit$greater$default$2();
    }

    public static int $lessinit$greater$default$1()
    {
        return SingleConsumerDataBuffers$.MODULE$.$lessinit$greater$default$1();
    }

    public AtomicLong org$neo4j$cypher$internal$runtime$pipelined$tracing$SingleConsumerDataBuffers$$localIds()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$SingleConsumerDataBuffers$$localIds;
    }

    private ThreadLocal<Object> localThreadId()
    {
        return this.localThreadId;
    }

    private int MAX_CLIENT_THREADS()
    {
        return this.MAX_CLIENT_THREADS;
    }

    private RingBuffer[] buffersByThread()
    {
        return this.buffersByThread;
    }

    private long t0()
    {
        return this.t0;
    }

    private void t0_$eq( final long x$1 )
    {
        this.t0 = x$1;
    }

    private Thread theConsumer()
    {
        return this.theConsumer;
    }

    private void theConsumer_$eq( final Thread x$1 )
    {
        this.theConsumer = x$1;
    }

    public void write( final DataPoint dataPoint )
    {
        long threadLocalId = BoxesRunTime.unboxToLong( this.localThreadId().get() );
        if ( threadLocalId < (long) this.MAX_CLIENT_THREADS() )
        {
            this.buffersByThread()[(int) threadLocalId].produce( dataPoint );
        }
        else
        {
            throw new IllegalArgumentException(
                    (new StringBuilder( 37 )).append( "Thread local ID exceeded maximum: " ).append( threadLocalId ).append( " > " ).append(
                            this.MAX_CLIENT_THREADS() - 1 ).toString() );
        }
    }

    public void consume( final DataPointFlusher dataPointWriter )
    {
        if ( this.theConsumer() == null )
        {
            this.theConsumer_$eq( Thread.currentThread() );
        }
        else
        {
            label30:
            {
                Thread var10000 = this.theConsumer();
                Thread var2 = Thread.currentThread();
                if ( var10000 == null )
                {
                    if ( var2 == null )
                    {
                        break label30;
                    }
                }
                else if ( var10000.equals( var2 ) )
                {
                    break label30;
                }

                throw new IllegalStateException( (new StringOps(.MODULE$.augmentString( (new StringBuilder( 138 )).append(
                        "Tried to consume SingleConsumerThreadSafeDataBuffers from wrong thread\n          |  wanted thread: " ).append(
                        this.theConsumer() ).append( "\n          |  but got thread: " ).append( Thread.currentThread() ).append(
                        "\n        " ).toString() )) ).stripMargin());
            }
        }

        ArrayBuffer[] dataByProducerThread = (ArrayBuffer[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) this.buffersByThread() ))).map( ( threadBuffer ) -> {
        ArrayBuffer dataPoints = new ArrayBuffer();
        threadBuffer.consume( ( dataPoint ) -> {
            $anonfun$consume$2( dataPoints, dataPoint );
            return BoxedUnit.UNIT;
        } );
        return dataPoints;
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( ArrayBuffer.class )));
        ArrayBuffer[] threadsWithData = (ArrayBuffer[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) dataByProducerThread ))).filter( ( x$2 ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$consume$3( x$2 ) );
    } );
        if ( (new ofRef(.MODULE$.refArrayOps( (Object[]) threadsWithData )) ).nonEmpty()){
        if ( this.t0() == -1L )
        {
            this.t0_$eq( BoxesRunTime.unboxToLong(
                    (new ofLong(.MODULE$.longArrayOps( (long[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) threadsWithData )) ).map( ( x$3 ) -> {
                        return BoxesRunTime.boxToLong( $anonfun$consume$4( x$3 ) );
                    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Long()) ) ))).min( scala.math.Ordering.Long..MODULE$)));
        }

        (new ofRef(.MODULE$.refArrayOps( (Object[]) dataByProducerThread ))).foreach( ( threadData ) -> {
            $anonfun$consume$5( this, dataPointWriter, threadData );
            return BoxedUnit.UNIT;
        } );
        dataPointWriter.flush();
    }
    }
}
