package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple6;
import scala.Tuple8;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction1;
import scala.runtime.AbstractFunction6;
import scala.runtime.AbstractFunction8;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class DataPointSchedulerTracer implements SchedulerTracer
{
    public final DataPointWriter org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$dataPointWriter;
    private final AtomicInteger queryCounter;
    private final ThreadLocal<Object> localWorkUnitId;
    private volatile DataPointSchedulerTracer.QueryTracer$ QueryTracer$module;
    private volatile DataPointSchedulerTracer.ScheduledWorkUnit$ ScheduledWorkUnit$module;
    private volatile DataPointSchedulerTracer.WorkUnit$ WorkUnit$module;

    public DataPointSchedulerTracer( final DataPointWriter dataPointWriter )
    {
        this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$dataPointWriter = dataPointWriter;
        this.queryCounter = new AtomicInteger();
        this.localWorkUnitId = ThreadLocal.withInitial( new Supplier<Object>( (DataPointSchedulerTracer) null )
        {
            public long get()
            {
                return 0L;
            }
        } );
    }

    public DataPointSchedulerTracer.QueryTracer$ QueryTracer()
    {
        if ( this.QueryTracer$module == null )
        {
            this.QueryTracer$lzycompute$1();
        }

        return this.QueryTracer$module;
    }

    public DataPointSchedulerTracer.ScheduledWorkUnit$ ScheduledWorkUnit()
    {
        if ( this.ScheduledWorkUnit$module == null )
        {
            this.ScheduledWorkUnit$lzycompute$1();
        }

        return this.ScheduledWorkUnit$module;
    }

    public DataPointSchedulerTracer.WorkUnit$ WorkUnit()
    {
        if ( this.WorkUnit$module == null )
        {
            this.WorkUnit$lzycompute$1();
        }

        return this.WorkUnit$module;
    }

    private AtomicInteger queryCounter()
    {
        return this.queryCounter;
    }

    private ThreadLocal<Object> localWorkUnitId()
    {
        return this.localWorkUnitId;
    }

    public long org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$newWorkUnitId( final long schedulingThreadId )
    {
        long localId = BoxesRunTime.unboxToLong( this.localWorkUnitId().get() );
        this.localWorkUnitId().set( BoxesRunTime.boxToLong( localId + 1L ) );
        return localId | schedulingThreadId << 40;
    }

    public QueryExecutionTracer traceQuery()
    {
        return new DataPointSchedulerTracer.QueryTracer( this, this.queryCounter().incrementAndGet() );
    }

    public long org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$currentTime()
    {
        return System.nanoTime();
    }

    private final void QueryTracer$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.QueryTracer$module == null )
            {
                this.QueryTracer$module = new DataPointSchedulerTracer.QueryTracer$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    private final void ScheduledWorkUnit$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.ScheduledWorkUnit$module == null )
            {
                this.ScheduledWorkUnit$module = new DataPointSchedulerTracer.ScheduledWorkUnit$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    private final void WorkUnit$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.WorkUnit$module == null )
            {
                this.WorkUnit$module = new DataPointSchedulerTracer.WorkUnit$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class QueryTracer implements QueryExecutionTracer, Product, Serializable
    {
        private final int queryId;

        public QueryTracer( final DataPointSchedulerTracer $outer, final int queryId )
        {
            this.queryId = queryId;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
            }
        }

        public int queryId()
        {
            return this.queryId;
        }

        public ScheduledWorkUnitEvent scheduleWorkUnit( final WorkIdentity workId, final Seq<WorkUnitEvent> upstreamWorkUnits )
        {
            long scheduledTime =
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer().org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$currentTime();
            long schedulingThread = Thread.currentThread().getId();
            long workUnitId =
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer().org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$newWorkUnitId(
                            schedulingThread );
            Seq upstreamWorkUnitIds = (Seq) upstreamWorkUnits.map( ( x$1 ) -> {
                return BoxesRunTime.boxToLong( $anonfun$scheduleWorkUnit$1( x$1 ) );
            }, scala.collection.Seq..MODULE$.canBuildFrom());
            return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer().new ScheduledWorkUnit(
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer(), workUnitId, upstreamWorkUnitIds,
                    this.queryId(), scheduledTime, schedulingThread, workId );
        }

        public void stopQuery()
        {
        }

        public DataPointSchedulerTracer.QueryTracer copy( final int queryId )
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer().new QueryTracer(
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer(), queryId );
        }

        public int copy$default$1()
        {
            return this.queryId();
        }

        public String productPrefix()
        {
            return "QueryTracer";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return BoxesRunTime.boxToInteger( this.queryId() );
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof DataPointSchedulerTracer.QueryTracer;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, this.queryId() );
            return Statics.finalizeHash( var1, 1 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label54:
                {
                    boolean var2;
                    if ( x$1 instanceof DataPointSchedulerTracer.QueryTracer &&
                            ((DataPointSchedulerTracer.QueryTracer) x$1).org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer() ==
                                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$QueryTracer$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        DataPointSchedulerTracer.QueryTracer var4 = (DataPointSchedulerTracer.QueryTracer) x$1;
                        if ( this.queryId() == var4.queryId() && var4.canEqual( this ) )
                        {
                            break label54;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public class QueryTracer$ extends AbstractFunction1<Object,DataPointSchedulerTracer.QueryTracer> implements Serializable
    {
        public QueryTracer$( final DataPointSchedulerTracer $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public final String toString()
        {
            return "QueryTracer";
        }

        public DataPointSchedulerTracer.QueryTracer apply( final int queryId )
        {
            return this.$outer.new QueryTracer( this.$outer, queryId );
        }

        public Option<Object> unapply( final DataPointSchedulerTracer.QueryTracer x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.queryId() ) ));
        }
    }

    public class ScheduledWorkUnit implements ScheduledWorkUnitEvent, Product, Serializable
    {
        private final long workUnitId;
        private final Seq<Object> upstreamWorkUnitIds;
        private final int queryId;
        private final long scheduledTime;
        private final long schedulingThreadId;
        private final WorkIdentity workId;

        public ScheduledWorkUnit( final DataPointSchedulerTracer $outer, final long workUnitId, final Seq<Object> upstreamWorkUnitIds, final int queryId,
                final long scheduledTime, final long schedulingThreadId, final WorkIdentity workId )
        {
            this.workUnitId = workUnitId;
            this.upstreamWorkUnitIds = upstreamWorkUnitIds;
            this.queryId = queryId;
            this.scheduledTime = scheduledTime;
            this.schedulingThreadId = schedulingThreadId;
            this.workId = workId;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
            }
        }

        public long workUnitId()
        {
            return this.workUnitId;
        }

        public Seq<Object> upstreamWorkUnitIds()
        {
            return this.upstreamWorkUnitIds;
        }

        public int queryId()
        {
            return this.queryId;
        }

        public long scheduledTime()
        {
            return this.scheduledTime;
        }

        public long schedulingThreadId()
        {
            return this.schedulingThreadId;
        }

        public WorkIdentity workId()
        {
            return this.workId;
        }

        public WorkUnitEvent start()
        {
            long startTime =
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$ScheduledWorkUnit$$$outer().org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$currentTime();
            return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$ScheduledWorkUnit$$$outer().new WorkUnit(
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$ScheduledWorkUnit$$$outer(), this.workUnitId(),
                    this.upstreamWorkUnitIds(), this.queryId(), this.schedulingThreadId(), this.scheduledTime(), Thread.currentThread().getId(), startTime,
                    this.workId() );
        }

        public DataPointSchedulerTracer.ScheduledWorkUnit copy( final long workUnitId, final Seq<Object> upstreamWorkUnitIds, final int queryId,
                final long scheduledTime, final long schedulingThreadId, final WorkIdentity workId )
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$ScheduledWorkUnit$$$outer().new ScheduledWorkUnit(
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$ScheduledWorkUnit$$$outer(), workUnitId,
                    upstreamWorkUnitIds, queryId, scheduledTime, schedulingThreadId, workId );
        }

        public long copy$default$1()
        {
            return this.workUnitId();
        }

        public Seq<Object> copy$default$2()
        {
            return this.upstreamWorkUnitIds();
        }

        public int copy$default$3()
        {
            return this.queryId();
        }

        public long copy$default$4()
        {
            return this.scheduledTime();
        }

        public long copy$default$5()
        {
            return this.schedulingThreadId();
        }

        public WorkIdentity copy$default$6()
        {
            return this.workId();
        }

        public String productPrefix()
        {
            return "ScheduledWorkUnit";
        }

        public int productArity()
        {
            return 6;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = BoxesRunTime.boxToLong( this.workUnitId() );
                break;
            case 1:
                var10000 = this.upstreamWorkUnitIds();
                break;
            case 2:
                var10000 = BoxesRunTime.boxToInteger( this.queryId() );
                break;
            case 3:
                var10000 = BoxesRunTime.boxToLong( this.scheduledTime() );
                break;
            case 4:
                var10000 = BoxesRunTime.boxToLong( this.schedulingThreadId() );
                break;
            case 5:
                var10000 = this.workId();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof DataPointSchedulerTracer.ScheduledWorkUnit;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.longHash( this.workUnitId() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.upstreamWorkUnitIds() ) );
            var1 = Statics.mix( var1, this.queryId() );
            var1 = Statics.mix( var1, Statics.longHash( this.scheduledTime() ) );
            var1 = Statics.mix( var1, Statics.longHash( this.schedulingThreadId() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.workId() ) );
            return Statics.finalizeHash( var1, 6 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label75:
                {
                    boolean var2;
                    if ( x$1 instanceof DataPointSchedulerTracer.ScheduledWorkUnit &&
                            ((DataPointSchedulerTracer.ScheduledWorkUnit) x$1).org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$ScheduledWorkUnit$$$outer() ==
                                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$ScheduledWorkUnit$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label52:
                        {
                            DataPointSchedulerTracer.ScheduledWorkUnit var4 = (DataPointSchedulerTracer.ScheduledWorkUnit) x$1;
                            if ( this.workUnitId() == var4.workUnitId() )
                            {
                                label50:
                                {
                                    Seq var10000 = this.upstreamWorkUnitIds();
                                    Seq var5 = var4.upstreamWorkUnitIds();
                                    if ( var10000 == null )
                                    {
                                        if ( var5 != null )
                                        {
                                            break label50;
                                        }
                                    }
                                    else if ( !var10000.equals( var5 ) )
                                    {
                                        break label50;
                                    }

                                    if ( this.queryId() == var4.queryId() && this.scheduledTime() == var4.scheduledTime() &&
                                            this.schedulingThreadId() == var4.schedulingThreadId() )
                                    {
                                        label42:
                                        {
                                            WorkIdentity var7 = this.workId();
                                            WorkIdentity var6 = var4.workId();
                                            if ( var7 == null )
                                            {
                                                if ( var6 != null )
                                                {
                                                    break label42;
                                                }
                                            }
                                            else if ( !var7.equals( var6 ) )
                                            {
                                                break label42;
                                            }

                                            if ( var4.canEqual( this ) )
                                            {
                                                var8 = true;
                                                break label52;
                                            }
                                        }
                                    }
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label75;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public class ScheduledWorkUnit$ extends AbstractFunction6<Object,Seq<Object>,Object,Object,Object,WorkIdentity,DataPointSchedulerTracer.ScheduledWorkUnit>
            implements Serializable
    {
        public ScheduledWorkUnit$( final DataPointSchedulerTracer $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public final String toString()
        {
            return "ScheduledWorkUnit";
        }

        public DataPointSchedulerTracer.ScheduledWorkUnit apply( final long workUnitId, final Seq<Object> upstreamWorkUnitIds, final int queryId,
                final long scheduledTime, final long schedulingThreadId, final WorkIdentity workId )
        {
            return this.$outer.new ScheduledWorkUnit( this.$outer, workUnitId, upstreamWorkUnitIds, queryId, scheduledTime, schedulingThreadId, workId );
        }

        public Option<Tuple6<Object,Seq<Object>,Object,Object,Object,WorkIdentity>> unapply( final DataPointSchedulerTracer.ScheduledWorkUnit x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
                new Tuple6( BoxesRunTime.boxToLong( x$0.workUnitId() ), x$0.upstreamWorkUnitIds(), BoxesRunTime.boxToInteger( x$0.queryId() ),
                        BoxesRunTime.boxToLong( x$0.scheduledTime() ), BoxesRunTime.boxToLong( x$0.schedulingThreadId() ), x$0.workId() ) ));
        }
    }

    public class WorkUnit implements WorkUnitEvent, Product, Serializable
    {
        private final long id;
        private final Seq<Object> upstreamIds;
        private final int queryId;
        private final long schedulingThreadId;
        private final long scheduledTime;
        private final long executionThreadId;
        private final long startTime;
        private final WorkIdentity workId;

        public WorkUnit( final DataPointSchedulerTracer $outer, final long id, final Seq<Object> upstreamIds, final int queryId, final long schedulingThreadId,
                final long scheduledTime, final long executionThreadId, final long startTime, final WorkIdentity workId )
        {
            this.id = id;
            this.upstreamIds = upstreamIds;
            this.queryId = queryId;
            this.schedulingThreadId = schedulingThreadId;
            this.scheduledTime = scheduledTime;
            this.executionThreadId = executionThreadId;
            this.startTime = startTime;
            this.workId = workId;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
            }
        }

        public long id()
        {
            return this.id;
        }

        public Seq<Object> upstreamIds()
        {
            return this.upstreamIds;
        }

        public int queryId()
        {
            return this.queryId;
        }

        public long schedulingThreadId()
        {
            return this.schedulingThreadId;
        }

        public long scheduledTime()
        {
            return this.scheduledTime;
        }

        public long executionThreadId()
        {
            return this.executionThreadId;
        }

        public long startTime()
        {
            return this.startTime;
        }

        public WorkIdentity workId()
        {
            return this.workId;
        }

        public void stop()
        {
            long stopTime =
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$WorkUnit$$$outer().org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$currentTime();
            this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$WorkUnit$$$outer().org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$$dataPointWriter.write(
                    new DataPoint( this.id(), this.upstreamIds(), this.queryId(), this.schedulingThreadId(), this.scheduledTime(), this.executionThreadId(),
                            this.startTime(), stopTime, this.workId() ) );
        }

        public DataPointSchedulerTracer.WorkUnit copy( final long id, final Seq<Object> upstreamIds, final int queryId, final long schedulingThreadId,
                final long scheduledTime, final long executionThreadId, final long startTime, final WorkIdentity workId )
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$WorkUnit$$$outer().new WorkUnit(
                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$WorkUnit$$$outer(), id, upstreamIds, queryId,
                    schedulingThreadId, scheduledTime, executionThreadId, startTime, workId );
        }

        public long copy$default$1()
        {
            return this.id();
        }

        public Seq<Object> copy$default$2()
        {
            return this.upstreamIds();
        }

        public int copy$default$3()
        {
            return this.queryId();
        }

        public long copy$default$4()
        {
            return this.schedulingThreadId();
        }

        public long copy$default$5()
        {
            return this.scheduledTime();
        }

        public long copy$default$6()
        {
            return this.executionThreadId();
        }

        public long copy$default$7()
        {
            return this.startTime();
        }

        public WorkIdentity copy$default$8()
        {
            return this.workId();
        }

        public String productPrefix()
        {
            return "WorkUnit";
        }

        public int productArity()
        {
            return 8;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = BoxesRunTime.boxToLong( this.id() );
                break;
            case 1:
                var10000 = this.upstreamIds();
                break;
            case 2:
                var10000 = BoxesRunTime.boxToInteger( this.queryId() );
                break;
            case 3:
                var10000 = BoxesRunTime.boxToLong( this.schedulingThreadId() );
                break;
            case 4:
                var10000 = BoxesRunTime.boxToLong( this.scheduledTime() );
                break;
            case 5:
                var10000 = BoxesRunTime.boxToLong( this.executionThreadId() );
                break;
            case 6:
                var10000 = BoxesRunTime.boxToLong( this.startTime() );
                break;
            case 7:
                var10000 = this.workId();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof DataPointSchedulerTracer.WorkUnit;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.longHash( this.id() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.upstreamIds() ) );
            var1 = Statics.mix( var1, this.queryId() );
            var1 = Statics.mix( var1, Statics.longHash( this.schedulingThreadId() ) );
            var1 = Statics.mix( var1, Statics.longHash( this.scheduledTime() ) );
            var1 = Statics.mix( var1, Statics.longHash( this.executionThreadId() ) );
            var1 = Statics.mix( var1, Statics.longHash( this.startTime() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.workId() ) );
            return Statics.finalizeHash( var1, 8 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label79:
                {
                    boolean var2;
                    if ( x$1 instanceof DataPointSchedulerTracer.WorkUnit &&
                            ((DataPointSchedulerTracer.WorkUnit) x$1).org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$WorkUnit$$$outer() ==
                                    this.org$neo4j$cypher$internal$runtime$pipelined$tracing$DataPointSchedulerTracer$WorkUnit$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label56:
                        {
                            DataPointSchedulerTracer.WorkUnit var4 = (DataPointSchedulerTracer.WorkUnit) x$1;
                            if ( this.id() == var4.id() )
                            {
                                label54:
                                {
                                    Seq var10000 = this.upstreamIds();
                                    Seq var5 = var4.upstreamIds();
                                    if ( var10000 == null )
                                    {
                                        if ( var5 != null )
                                        {
                                            break label54;
                                        }
                                    }
                                    else if ( !var10000.equals( var5 ) )
                                    {
                                        break label54;
                                    }

                                    if ( this.queryId() == var4.queryId() && this.schedulingThreadId() == var4.schedulingThreadId() &&
                                            this.scheduledTime() == var4.scheduledTime() && this.executionThreadId() == var4.executionThreadId() &&
                                            this.startTime() == var4.startTime() )
                                    {
                                        label44:
                                        {
                                            WorkIdentity var7 = this.workId();
                                            WorkIdentity var6 = var4.workId();
                                            if ( var7 == null )
                                            {
                                                if ( var6 != null )
                                                {
                                                    break label44;
                                                }
                                            }
                                            else if ( !var7.equals( var6 ) )
                                            {
                                                break label44;
                                            }

                                            if ( var4.canEqual( this ) )
                                            {
                                                var8 = true;
                                                break label56;
                                            }
                                        }
                                    }
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label79;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public class WorkUnit$ extends AbstractFunction8<Object,Seq<Object>,Object,Object,Object,Object,Object,WorkIdentity,DataPointSchedulerTracer.WorkUnit>
            implements Serializable
    {
        public WorkUnit$( final DataPointSchedulerTracer $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public final String toString()
        {
            return "WorkUnit";
        }

        public DataPointSchedulerTracer.WorkUnit apply( final long id, final Seq<Object> upstreamIds, final int queryId, final long schedulingThreadId,
                final long scheduledTime, final long executionThreadId, final long startTime, final WorkIdentity workId )
        {
            return this.$outer.new WorkUnit( this.$outer, id, upstreamIds, queryId, schedulingThreadId, scheduledTime, executionThreadId, startTime, workId );
        }

        public Option<Tuple8<Object,Seq<Object>,Object,Object,Object,Object,Object,WorkIdentity>> unapply( final DataPointSchedulerTracer.WorkUnit x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
                new Tuple8( BoxesRunTime.boxToLong( x$0.id() ), x$0.upstreamIds(), BoxesRunTime.boxToInteger( x$0.queryId() ),
                        BoxesRunTime.boxToLong( x$0.schedulingThreadId() ), BoxesRunTime.boxToLong( x$0.scheduledTime() ),
                        BoxesRunTime.boxToLong( x$0.executionThreadId() ), BoxesRunTime.boxToLong( x$0.startTime() ), x$0.workId() ) ));
        }
    }
}
