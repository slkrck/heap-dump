package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ScheduledWorkUnitEvent
{
    WorkUnitEvent start();
}
