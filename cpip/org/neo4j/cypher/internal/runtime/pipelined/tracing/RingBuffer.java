package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class RingBuffer
{
    private final int maxRetries;
    private final int size;
    private final int mask;
    private final DataPoint[] buffer;
    private volatile int produceCount;
    private volatile int consumeCount;

    public RingBuffer( final int bitSize, final int maxRetries )
    {
        this.maxRetries = maxRetries;
        this.produceCount = 0;
        this.consumeCount = 0;
        this.size = 1 << bitSize;
        this.mask = this.size() - 1;
        this.buffer = new DataPoint[this.size()];
    }

    public static int $lessinit$greater$default$2()
    {
        return RingBuffer$.MODULE$.$lessinit$greater$default$2();
    }

    public static int $lessinit$greater$default$1()
    {
        return RingBuffer$.MODULE$.$lessinit$greater$default$1();
    }

    public static int defaultMaxRetries()
    {
        return RingBuffer$.MODULE$.defaultMaxRetries();
    }

    public static int defaultBitSize()
    {
        return RingBuffer$.MODULE$.defaultBitSize();
    }

    private int maxRetries()
    {
        return this.maxRetries;
    }

    private int produceCount()
    {
        return this.produceCount;
    }

    private void produceCount_$eq( final int x$1 )
    {
        this.produceCount = x$1;
    }

    private int consumeCount()
    {
        return this.consumeCount;
    }

    private void consumeCount_$eq( final int x$1 )
    {
        this.consumeCount = x$1;
    }

    public int size()
    {
        return this.size;
    }

    private int mask()
    {
        return this.mask;
    }

    private DataPoint[] buffer()
    {
        return this.buffer;
    }

    public void produce( final DataPoint dp )
    {
        int claimed = -1;
        int snapshotProduce = this.produceCount();
        int retries = 0;

        while ( claimed == -1 )
        {
            int snapshotConsume = this.consumeCount();
            if ( snapshotProduce - this.size() < snapshotConsume )
            {
                claimed = snapshotProduce & this.mask();
                this.buffer()[claimed] = dp;
                this.produceCount_$eq( this.produceCount() + 1 );
            }
            else
            {
                ++retries;
                if ( retries >= this.maxRetries() )
                {
                    throw new RuntimeException( "Exceeded max retries" );
                }

                Thread.sleep( 0L, 1000 );
            }
        }
    }

    public void consume( final Function1<DataPoint,BoxedUnit> f )
    {
        int snapshotConsume = this.consumeCount();

        for ( int snapshotProduce = this.produceCount(); snapshotConsume < snapshotProduce; ++snapshotConsume )
        {
            f.apply( this.buffer()[snapshotConsume & this.mask()] );
        }

        this.consumeCount_$eq( snapshotConsume );
    }
}
