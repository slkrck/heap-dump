package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import java.io.Closeable;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface DataPointFlusher extends DataPointWriter, Closeable
{
    void flush();
}
