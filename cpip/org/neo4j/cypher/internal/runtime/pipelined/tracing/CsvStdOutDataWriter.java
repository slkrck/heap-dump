package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import scala.Predef.;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CsvStdOutDataWriter extends CsvDataWriter
{
    private final StringBuilder sb = new StringBuilder( this.header() );

    private StringBuilder sb()
    {
        return this.sb;
    }

    public void flush()
    {
        String result = this.sb().result();
        this.sb().clear();
      .MODULE$.print( result );
    }

    public void close()
    {
    }

    public void writeRow( final String row )
    {
        this.sb().$plus$plus$eq( row );
    }
}
