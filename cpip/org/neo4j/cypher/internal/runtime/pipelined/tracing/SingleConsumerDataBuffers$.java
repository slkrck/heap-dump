package org.neo4j.cypher.internal.runtime.pipelined.tracing;

public final class SingleConsumerDataBuffers$
{
    public static SingleConsumerDataBuffers$ MODULE$;

    static
    {
        new SingleConsumerDataBuffers$();
    }

    private SingleConsumerDataBuffers$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$1()
    {
        return RingBuffer$.MODULE$.defaultBitSize();
    }

    public int $lessinit$greater$default$2()
    {
        return RingBuffer$.MODULE$.defaultMaxRetries();
    }
}
