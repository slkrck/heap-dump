package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple9;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction9;
import scala.runtime.BoxesRunTime;

public final class DataPoint$ extends AbstractFunction9<Object,Seq<Object>,Object,Object,Object,Object,Object,Object,WorkIdentity,DataPoint>
        implements Serializable
{
    public static DataPoint$ MODULE$;

    static
    {
        new DataPoint$();
    }

    private DataPoint$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "DataPoint";
    }

    public DataPoint apply( final long id, final Seq<Object> upstreamIds, final int queryId, final long schedulingThreadId, final long scheduledTime,
            final long executionThreadId, final long startTime, final long stopTime, final WorkIdentity workId )
    {
        return new DataPoint( id, upstreamIds, queryId, schedulingThreadId, scheduledTime, executionThreadId, startTime, stopTime, workId );
    }

    public Option<Tuple9<Object,Seq<Object>,Object,Object,Object,Object,Object,Object,WorkIdentity>> unapply( final DataPoint x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple9( BoxesRunTime.boxToLong( x$0.id() ), x$0.upstreamIds(), BoxesRunTime.boxToInteger( x$0.queryId() ),
                    BoxesRunTime.boxToLong( x$0.schedulingThreadId() ), BoxesRunTime.boxToLong( x$0.scheduledTime() ),
                    BoxesRunTime.boxToLong( x$0.executionThreadId() ), BoxesRunTime.boxToLong( x$0.startTime() ), BoxesRunTime.boxToLong( x$0.stopTime() ),
                    x$0.workId() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
