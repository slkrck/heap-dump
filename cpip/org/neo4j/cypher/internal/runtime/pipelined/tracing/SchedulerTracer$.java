package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.collection.Seq;

public final class SchedulerTracer$
{
    public static SchedulerTracer$ MODULE$;

    static
    {
        new SchedulerTracer$();
    }

    private final SchedulerTracer NoSchedulerTracer;
    private final QueryExecutionTracer NoQueryExecutionTracer;
    private final ScheduledWorkUnitEvent NoScheduledWorkUnitEvent;
    private final WorkUnitEvent NoWorkUnitEvent;

    private SchedulerTracer$()
    {
        MODULE$ = this;
        this.NoSchedulerTracer = () -> {
            return MODULE$.NoQueryExecutionTracer();
        };
        this.NoQueryExecutionTracer = new QueryExecutionTracer()
        {
            public ScheduledWorkUnitEvent scheduleWorkUnit( final WorkIdentity workId, final Seq<WorkUnitEvent> upstreamWorkUnitEvents )
            {
                return SchedulerTracer$.MODULE$.NoScheduledWorkUnitEvent();
            }

            public void stopQuery()
            {
            }
        };
        this.NoScheduledWorkUnitEvent = new ScheduledWorkUnitEvent()
        {
            public WorkUnitEvent start()
            {
                return SchedulerTracer$.MODULE$.NoWorkUnitEvent();
            }
        };
        this.NoWorkUnitEvent = new WorkUnitEvent()
        {
            public void stop()
            {
            }

            public long id()
            {
                return -1L;
            }
        };
    }

    public SchedulerTracer NoSchedulerTracer()
    {
        return this.NoSchedulerTracer;
    }

    public QueryExecutionTracer NoQueryExecutionTracer()
    {
        return this.NoQueryExecutionTracer;
    }

    public ScheduledWorkUnitEvent NoScheduledWorkUnitEvent()
    {
        return this.NoScheduledWorkUnitEvent;
    }

    public WorkUnitEvent NoWorkUnitEvent()
    {
        return this.NoWorkUnitEvent;
    }
}
