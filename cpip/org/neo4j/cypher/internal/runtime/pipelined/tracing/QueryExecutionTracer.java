package org.neo4j.cypher.internal.runtime.pipelined.tracing;

import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface QueryExecutionTracer
{
    ScheduledWorkUnitEvent scheduleWorkUnit( final WorkIdentity workId, final Seq<WorkUnitEvent> upstreamWorkUnitEvents );

    void stopQuery();
}
