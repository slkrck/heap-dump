package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.pipelined.execution.LiveCounts;
import org.neo4j.cypher.internal.runtime.pipelined.execution.LiveCounts$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.kernel.lifecycle.Lifecycle;
import scala.Function0;
import scala.Predef.;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class WorkerResourceProvider implements Lifecycle
{
    private final QueryResources[] queryResourcesForWorkers;

    public WorkerResourceProvider( final int numberOfWorkers, final Function0<QueryResources> newWorkerResources )
    {
        this.queryResourcesForWorkers = (QueryResources[]) scala.Array..
        MODULE$.fill( numberOfWorkers, newWorkerResources, scala.reflect.ClassTag..MODULE$.apply( QueryResources.class ));
    }

    private QueryResources[] queryResourcesForWorkers()
    {
        return this.queryResourcesForWorkers;
    }

    public QueryResources resourcesForWorker( final int workerId )
    {
        return this.queryResourcesForWorkers()[workerId];
    }

    public void assertAllReleased()
    {
        LiveCounts liveCounts = new LiveCounts( LiveCounts$.MODULE$.$lessinit$greater$default$1(), LiveCounts$.MODULE$.$lessinit$greater$default$2(),
                LiveCounts$.MODULE$.$lessinit$greater$default$3(), LiveCounts$.MODULE$.$lessinit$greater$default$4(),
                LiveCounts$.MODULE$.$lessinit$greater$default$5(), LiveCounts$.MODULE$.$lessinit$greater$default$6() );
        (new ofRef(.MODULE$.refArrayOps( (Object[]) this.queryResourcesForWorkers() ))).foreach( ( q ) -> {
        $anonfun$assertAllReleased$1( liveCounts, q );
        return BoxedUnit.UNIT;
    } );
        liveCounts.assertAllReleased();
    }

    public void init()
    {
    }

    public void start()
    {
    }

    public void stop()
    {
    }

    public void shutdown()
    {
        for ( int i = 0; i < this.queryResourcesForWorkers().length; ++i )
        {
            this.queryResourcesForWorkers()[i].close();
        }
    }
}
