package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.CypherInterpretedPipesFallbackOption;
import org.neo4j.cypher.internal.logical.plans.EagerLogicalPlan;
import org.neo4j.cypher.internal.logical.plans.ErrorPlan;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.FindShortestPaths;
import org.neo4j.cypher.internal.logical.plans.LoadCSV;
import org.neo4j.cypher.internal.logical.plans.LockNodes;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.OptionalExpand;
import org.neo4j.cypher.internal.logical.plans.ProcedureCall;
import org.neo4j.cypher.internal.logical.plans.ProjectEndpoints;
import org.neo4j.cypher.internal.logical.plans.PruningVarExpand;
import org.neo4j.cypher.internal.logical.plans.ResolvedCall;
import org.neo4j.cypher.internal.logical.plans.Skip;
import org.neo4j.cypher.internal.logical.plans.UpdatingPlan;
import scala.Function1;
import scala.Option;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public interface InterpretedPipesFallbackPolicy
{
    static InterpretedPipesFallbackPolicy apply( final CypherInterpretedPipesFallbackOption interpretedPipesFallbackOption, final boolean parallelExecution )
    {
        return InterpretedPipesFallbackPolicy$.MODULE$.apply( var0, var1 );
    }

    boolean readOnly();

    boolean breakOn( final LogicalPlan lp );

    public static class INTERPRETED_PIPES_FALLBACK_DISABLED$ implements InterpretedPipesFallbackPolicy, Product, Serializable
    {
        public static InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_DISABLED$ MODULE$;

        static
        {
            new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_DISABLED$();
        }

        public INTERPRETED_PIPES_FALLBACK_DISABLED$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public boolean readOnly()
        {
            return true;
        }

        public boolean breakOn( final LogicalPlan lp )
        {
            throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                    lp.getClass().getSimpleName() );
        }

        public String productPrefix()
        {
            return "INTERPRETED_PIPES_FALLBACK_DISABLED";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_DISABLED$;
        }

        public int hashCode()
        {
            return -928430868;
        }

        public String toString()
        {
            return "INTERPRETED_PIPES_FALLBACK_DISABLED";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS implements InterpretedPipesFallbackPolicy, Product, Serializable
    {
        private final boolean parallelExecution;
        private final PartialFunction<LogicalPlan,Object> WHITELIST;
        private final PartialFunction<LogicalPlan,Object> BLACKLIST;

        public INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS( final boolean parallelExecution )
        {
            this.parallelExecution = parallelExecution;
            Product.$init$( this );
            this.WHITELIST = (new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY( parallelExecution )).WHITELIST();
            this.BLACKLIST = new Serializable( this )
            {
                public static final long serialVersionUID = 0L;

                public
                {
                    if ( $outer == null )
                    {
                        throw null;
                    }
                    else
                    {
                        this.$outer = $outer;
                    }
                }

                public final <A1 extends LogicalPlan, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
                {
                    if ( x2 instanceof Skip )
                    {
                        Skip var6 = (Skip) x2;
                        throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                                var6.getClass().getSimpleName() );
                    }
                    else
                    {
                        if ( x2 instanceof ProcedureCall )
                        {
                            ProcedureCall var7 = (ProcedureCall) x2;
                            if ( this.$outer.parallelExecution() )
                            {
                                throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                                        var7.getClass().getSimpleName() );
                            }
                        }

                        if ( x2 instanceof LoadCSV )
                        {
                            LoadCSV var8 = (LoadCSV) x2;
                            if ( this.$outer.parallelExecution() )
                            {
                                throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                                        var8.getClass().getSimpleName() );
                            }
                        }

                        if ( x2 instanceof EagerLogicalPlan )
                        {
                            throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                                    x2.getClass().getSimpleName() );
                        }
                        else if ( x2.isLeaf() )
                        {
                            throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                                    x2.getClass().getSimpleName() );
                        }
                        else if ( x2.lhs().isDefined() && x2.rhs().isDefined() )
                        {
                            throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                                    x2.getClass().getSimpleName() );
                        }
                        else
                        {
                            boolean var4;
                            if ( x2 instanceof UpdatingPlan )
                            {
                                var4 = true;
                            }
                            else if ( x2 instanceof LockNodes )
                            {
                                var4 = true;
                            }
                            else
                            {
                                var4 = false;
                            }

                            if ( var4 && this.$outer.parallelExecution() )
                            {
                                throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                                        x2.getClass().getSimpleName() );
                            }
                            else
                            {
                                Object var3 = var2.apply( x2 );
                                return var3;
                            }
                        }
                    }
                }

                public final boolean isDefinedAt( final LogicalPlan x2 )
                {
                    boolean var2;
                    if ( x2 instanceof Skip )
                    {
                        var2 = true;
                    }
                    else if ( x2 instanceof ProcedureCall && this.$outer.parallelExecution() )
                    {
                        var2 = true;
                    }
                    else if ( x2 instanceof LoadCSV && this.$outer.parallelExecution() )
                    {
                        var2 = true;
                    }
                    else if ( x2 instanceof EagerLogicalPlan )
                    {
                        var2 = true;
                    }
                    else if ( x2.isLeaf() )
                    {
                        var2 = true;
                    }
                    else if ( x2.lhs().isDefined() && x2.rhs().isDefined() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        boolean var3;
                        if ( x2 instanceof UpdatingPlan )
                        {
                            var3 = true;
                        }
                        else if ( x2 instanceof LockNodes )
                        {
                            var3 = true;
                        }
                        else
                        {
                            var3 = false;
                        }

                        if ( var3 && this.$outer.parallelExecution() )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }
                    }

                    return var2;
                }
            };
        }

        public boolean parallelExecution()
        {
            return this.parallelExecution;
        }

        public boolean readOnly()
        {
            return false;
        }

        private PartialFunction<LogicalPlan,Object> WHITELIST()
        {
            return this.WHITELIST;
        }

        public PartialFunction<LogicalPlan,Object> BLACKLIST()
        {
            return this.BLACKLIST;
        }

        public boolean breakOn( final LogicalPlan lp )
        {
            return BoxesRunTime.unboxToBoolean( this.WHITELIST().orElse( this.BLACKLIST() ).applyOrElse( lp, ( x0$1 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$breakOn$2( x0$1 ) );
            } ) );
        }

        public InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS copy( final boolean parallelExecution )
        {
            return new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS( parallelExecution );
        }

        public boolean copy$default$1()
        {
            return this.parallelExecution();
        }

        public String productPrefix()
        {
            return "INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return BoxesRunTime.boxToBoolean( this.parallelExecution() );
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, this.parallelExecution() ? 1231 : 1237 );
            return Statics.finalizeHash( var1, 1 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label49:
                {
                    boolean var2;
                    if ( x$1 instanceof InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS var4 =
                                (InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS) x$1;
                        if ( this.parallelExecution() == var4.parallelExecution() && var4.canEqual( this ) )
                        {
                            break label49;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS$
            extends AbstractFunction1<Object,InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS> implements Serializable
    {
        public static InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS$ MODULE$;

        static
        {
            new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS$();
        }

        public INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS";
        }

        public InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS apply( final boolean parallelExecution )
        {
            return new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS( parallelExecution );
        }

        public Option<Object> unapply( final InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( BoxesRunTime.boxToBoolean( x$0.parallelExecution() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY implements InterpretedPipesFallbackPolicy, Product, Serializable
    {
        private final boolean parallelExecution;
        private final PartialFunction<LogicalPlan,Object> WHITELIST;

        public INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY( final boolean parallelExecution )
        {
            this.parallelExecution = parallelExecution;
            Product.$init$( this );
            this.WHITELIST = new Serializable( this )
            {
                public static final long serialVersionUID = 0L;

                public
                {
                    if ( $outer == null )
                    {
                        throw null;
                    }
                    else
                    {
                        this.$outer = $outer;
                    }
                }

                public final <A1 extends LogicalPlan, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                {
                    Object var3;
                    label79:
                    {
                        if ( x1 instanceof Expand )
                        {
                            Expand var6 = (Expand) x1;
                            ExpansionMode var10000 = var6.mode();
                            org.neo4j.cypher.internal.logical.plans.ExpandInto.var7 = org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$;
                            if ( var10000 == null )
                            {
                                if ( var7 == null )
                                {
                                    break label79;
                                }
                            }
                            else if ( var10000.equals( var7 ) )
                            {
                                break label79;
                            }
                        }

                        boolean var4;
                        if ( x1 instanceof PruningVarExpand )
                        {
                            var4 = true;
                        }
                        else if ( x1 instanceof OptionalExpand )
                        {
                            var4 = true;
                        }
                        else if ( x1 instanceof FindShortestPaths )
                        {
                            var4 = true;
                        }
                        else
                        {
                            var4 = false;
                        }

                        if ( var4 )
                        {
                            var3 = BoxesRunTime.boxToBoolean( true );
                            return var3;
                        }
                        else
                        {
                            if ( x1 instanceof ProcedureCall )
                            {
                                ProcedureCall var8 = (ProcedureCall) x1;
                                ResolvedCall call = var8.call();
                                if ( !this.$outer.parallelExecution() )
                                {
                                    var3 = BoxesRunTime.boxToBoolean( !call.signature().isVoid() );
                                    return var3;
                                }
                            }

                            if ( x1 instanceof ProjectEndpoints )
                            {
                                ProjectEndpoints var10 = (ProjectEndpoints) x1;
                                var3 = BoxesRunTime.boxToBoolean( !var10.directed() );
                                return var3;
                            }
                            else
                            {
                                if ( x1 instanceof ErrorPlan )
                                {
                                    var3 = BoxesRunTime.boxToBoolean( false );
                                }
                                else
                                {
                                    var3 = var2.apply( x1 );
                                }

                                return var3;
                            }
                        }
                    }

                    var3 = BoxesRunTime.boxToBoolean( true );
                    return var3;
                }

                public final boolean isDefinedAt( final LogicalPlan x1 )
                {
                    boolean var2;
                    label61:
                    {
                        if ( x1 instanceof Expand )
                        {
                            Expand var5 = (Expand) x1;
                            ExpansionMode var10000 = var5.mode();
                            org.neo4j.cypher.internal.logical.plans.ExpandInto.var6 = org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$;
                            if ( var10000 == null )
                            {
                                if ( var6 == null )
                                {
                                    break label61;
                                }
                            }
                            else if ( var10000.equals( var6 ) )
                            {
                                break label61;
                            }
                        }

                        boolean var3;
                        if ( x1 instanceof PruningVarExpand )
                        {
                            var3 = true;
                        }
                        else if ( x1 instanceof OptionalExpand )
                        {
                            var3 = true;
                        }
                        else if ( x1 instanceof FindShortestPaths )
                        {
                            var3 = true;
                        }
                        else
                        {
                            var3 = false;
                        }

                        if ( var3 )
                        {
                            var2 = true;
                            return var2;
                        }
                        else if ( x1 instanceof ProcedureCall && !this.$outer.parallelExecution() )
                        {
                            var2 = true;
                            return var2;
                        }
                        else if ( x1 instanceof ProjectEndpoints )
                        {
                            var2 = true;
                            return var2;
                        }
                        else
                        {
                            if ( x1 instanceof ErrorPlan )
                            {
                                var2 = true;
                            }
                            else
                            {
                                var2 = false;
                            }

                            return var2;
                        }
                    }

                    var2 = true;
                    return var2;
                }
            };
        }

        public boolean parallelExecution()
        {
            return this.parallelExecution;
        }

        public boolean readOnly()
        {
            return true;
        }

        public PartialFunction<LogicalPlan,Object> WHITELIST()
        {
            return this.WHITELIST;
        }

        public boolean breakOn( final LogicalPlan lp )
        {
            return BoxesRunTime.unboxToBoolean( this.WHITELIST().applyOrElse( lp, ( x$1 ) -> {
                throw InterpretedPipesFallbackPolicy$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported(
                        lp.getClass().getSimpleName() );
            } ) );
        }

        public InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY copy( final boolean parallelExecution )
        {
            return new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY( parallelExecution );
        }

        public boolean copy$default$1()
        {
            return this.parallelExecution();
        }

        public String productPrefix()
        {
            return "INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return BoxesRunTime.boxToBoolean( this.parallelExecution() );
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, this.parallelExecution() ? 1231 : 1237 );
            return Statics.finalizeHash( var1, 1 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label49:
                {
                    boolean var2;
                    if ( x$1 instanceof InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY var4 =
                                (InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY) x$1;
                        if ( this.parallelExecution() == var4.parallelExecution() && var4.canEqual( this ) )
                        {
                            break label49;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY$
            extends AbstractFunction1<Object,InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY> implements Serializable
    {
        public static InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY$ MODULE$;

        static
        {
            new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY$();
        }

        public INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY";
        }

        public InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY apply( final boolean parallelExecution )
        {
            return new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY( parallelExecution );
        }

        public Option<Object> unapply( final InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( BoxesRunTime.boxToBoolean( x$0.parallelExecution() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
