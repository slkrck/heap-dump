package org.neo4j.cypher.internal.runtime.pipelined;

public final class FuseOperators$
{
    public static FuseOperators$ MODULE$;

    static
    {
        new FuseOperators$();
    }

    private final int org$neo4j$cypher$internal$runtime$pipelined$FuseOperators$$FUSE_LIMIT;

    private FuseOperators$()
    {
        MODULE$ = this;
        this.org$neo4j$cypher$internal$runtime$pipelined$FuseOperators$$FUSE_LIMIT = 2;
    }

    public int org$neo4j$cypher$internal$runtime$pipelined$FuseOperators$$FUSE_LIMIT()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$FuseOperators$$FUSE_LIMIT;
    }
}
