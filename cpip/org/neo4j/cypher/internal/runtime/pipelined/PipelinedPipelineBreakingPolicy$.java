package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class PipelinedPipelineBreakingPolicy$
        extends AbstractFunction2<OperatorFusionPolicy,InterpretedPipesFallbackPolicy,PipelinedPipelineBreakingPolicy> implements Serializable
{
    public static PipelinedPipelineBreakingPolicy$ MODULE$;

    static
    {
        new PipelinedPipelineBreakingPolicy$();
    }

    private PipelinedPipelineBreakingPolicy$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "PipelinedPipelineBreakingPolicy";
    }

    public PipelinedPipelineBreakingPolicy apply( final OperatorFusionPolicy fusionPolicy, final InterpretedPipesFallbackPolicy interpretedPipesPolicy )
    {
        return new PipelinedPipelineBreakingPolicy( fusionPolicy, interpretedPipesPolicy );
    }

    public Option<Tuple2<OperatorFusionPolicy,InterpretedPipesFallbackPolicy>> unapply( final PipelinedPipelineBreakingPolicy x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.fusionPolicy(), x$0.interpretedPipesPolicy() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
