package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.physicalplanning.BufferDefinition;
import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.Operator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple11;
import scala.None.;
import scala.runtime.AbstractFunction11;
import scala.runtime.BoxesRunTime;

public final class ExecutablePipeline$ extends
        AbstractFunction11<PipelineId,PipelineId,PipelineId,Operator,MiddleOperator[],Object,SlotConfiguration,BufferDefinition,OutputOperator,Object,Object,ExecutablePipeline>
        implements Serializable
{
    public static ExecutablePipeline$ MODULE$;

    static
    {
        new ExecutablePipeline$();
    }

    private ExecutablePipeline$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$10()
    {
        return true;
    }

    public boolean $lessinit$greater$default$11()
    {
        return false;
    }

    public final String toString()
    {
        return "ExecutablePipeline";
    }

    public ExecutablePipeline apply( final int id, final int lhs, final int rhs, final Operator start, final MiddleOperator[] middleOperators,
            final boolean serial, final SlotConfiguration slots, final BufferDefinition inputBuffer, final OutputOperator outputOperator,
            final boolean needsMorsel, final boolean needsFilteringMorsel )
    {
        return new ExecutablePipeline( id, lhs, rhs, start, middleOperators, serial, slots, inputBuffer, outputOperator, needsMorsel, needsFilteringMorsel );
    }

    public boolean apply$default$10()
    {
        return true;
    }

    public boolean apply$default$11()
    {
        return false;
    }

    public Option<Tuple11<PipelineId,PipelineId,PipelineId,Operator,MiddleOperator[],Object,SlotConfiguration,BufferDefinition,OutputOperator,Object,Object>> unapply(
            final ExecutablePipeline x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple11( new PipelineId( x$0.id() ), new PipelineId( x$0.lhs() ), new PipelineId( x$0.rhs() ), x$0.start(), x$0.middleOperators(),
                    BoxesRunTime.boxToBoolean( x$0.serial() ), x$0.slots(), x$0.inputBuffer(), x$0.outputOperator(),
                    BoxesRunTime.boxToBoolean( x$0.needsMorsel() ), BoxesRunTime.boxToBoolean( x$0.needsFilteringMorsel() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
