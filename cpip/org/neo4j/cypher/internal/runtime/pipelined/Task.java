package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Task<THREAD_LOCAL_RESOURCE> extends WorkIdentity
{
    static void $init$( final Task $this )
    {
    }

    PreparedOutput executeWorkUnit( final THREAD_LOCAL_RESOURCE threadLocalResource, final WorkUnitEvent workUnitEvent, final QueryProfiler queryProfiler );

    boolean canContinue();

    default String toString()
    {
        return (new StringBuilder( 4 )).append( this.getClass().getSimpleName() ).append( "[" ).append( new Id( this.workId() ) ).append( "](" ).append(
                this.workDescription() ).append( ")" ).toString();
    }
}
