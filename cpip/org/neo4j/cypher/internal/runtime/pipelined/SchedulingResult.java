package org.neo4j.cypher.internal.runtime.pipelined;

import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SchedulingResult<T> implements Product, Serializable
{
    private final T task;
    private final boolean someTaskWasFilteredOut;

    public SchedulingResult( final T task, final boolean someTaskWasFilteredOut )
    {
        this.task = task;
        this.someTaskWasFilteredOut = someTaskWasFilteredOut;
        Product.$init$( this );
    }

    public static <T> Option<Tuple2<T,Object>> unapply( final SchedulingResult<T> x$0 )
    {
        return SchedulingResult$.MODULE$.unapply( var0 );
    }

    public static <T> SchedulingResult<T> apply( final T task, final boolean someTaskWasFilteredOut )
    {
        return SchedulingResult$.MODULE$.apply( var0, var1 );
    }

    public T task()
    {
        return this.task;
    }

    public boolean someTaskWasFilteredOut()
    {
        return this.someTaskWasFilteredOut;
    }

    public <T> SchedulingResult<T> copy( final T task, final boolean someTaskWasFilteredOut )
    {
        return new SchedulingResult( task, someTaskWasFilteredOut );
    }

    public <T> T copy$default$1()
    {
        return this.task();
    }

    public <T> boolean copy$default$2()
    {
        return this.someTaskWasFilteredOut();
    }

    public String productPrefix()
    {
        return "SchedulingResult";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.task();
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.someTaskWasFilteredOut() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SchedulingResult;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.task() ) );
        var1 = Statics.mix( var1, this.someTaskWasFilteredOut() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 2 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label51:
            {
                boolean var2;
                if ( x$1 instanceof SchedulingResult )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    SchedulingResult var4 = (SchedulingResult) x$1;
                    if ( BoxesRunTime.equals( this.task(), var4.task() ) && this.someTaskWasFilteredOut() == var4.someTaskWasFilteredOut() &&
                            var4.canEqual( this ) )
                    {
                        break label51;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
