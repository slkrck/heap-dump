package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.ir.VarPatternLength;
import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.AllNodesScan;
import org.neo4j.cypher.internal.logical.plans.Argument;
import org.neo4j.cypher.internal.logical.plans.CacheProperties;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.DirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.Distinct;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.logical.plans.IndexedProperty;
import org.neo4j.cypher.internal.logical.plans.Input;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.NodeByIdSeek;
import org.neo4j.cypher.internal.logical.plans.NodeByLabelScan;
import org.neo4j.cypher.internal.logical.plans.NodeCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.NodeHashJoin;
import org.neo4j.cypher.internal.logical.plans.NodeIndexContainsScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexEndsWithScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexSeek;
import org.neo4j.cypher.internal.logical.plans.NodeUniqueIndexSeek;
import org.neo4j.cypher.internal.logical.plans.Optional;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.logical.plans.QueryExpression;
import org.neo4j.cypher.internal.logical.plans.RelationshipCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.SeekableArgs;
import org.neo4j.cypher.internal.logical.plans.Selection;
import org.neo4j.cypher.internal.logical.plans.Sort;
import org.neo4j.cypher.internal.logical.plans.Ties;
import org.neo4j.cypher.internal.logical.plans.Top;
import org.neo4j.cypher.internal.logical.plans.UndirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.BufferDefinition;
import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.physicalplanning.LHSAccumulatingRHSStreamingBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.MorselArgumentStateBufferOutput;
import org.neo4j.cypher.internal.physicalplanning.MorselBufferOutput;
import org.neo4j.cypher.internal.physicalplanning.NoOutput$;
import org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy;
import org.neo4j.cypher.internal.physicalplanning.OptionalBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.OutputDefinition;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes;
import org.neo4j.cypher.internal.physicalplanning.ProduceResultOutput;
import org.neo4j.cypher.internal.physicalplanning.ReduceOutput;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration$;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty$;
import org.neo4j.cypher.internal.physicalplanning.VariablePredicates$;
import org.neo4j.cypher.internal.runtime.QueryIndexRegistrator;
import org.neo4j.cypher.internal.runtime.interpreted.CommandProjection;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.True;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.DropResultPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekMode;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekModeFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeMapper;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatorFactory;
import org.neo4j.cypher.internal.runtime.pipelined.operators.AggregationOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.AggregationOperatorNoGrouping;
import org.neo4j.cypher.internal.runtime.pipelined.operators.AllNodeScanOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ArgumentOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.CachePropertiesOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.CartesianProductOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.DirectedRelationshipByIdSeekOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.DistinctOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ExpandAllOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.FilterOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.InputOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.LabelScanOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.LimitOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.MorselArgumentStateBufferOutputOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.MorselBufferOutputOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.MorselFeedPipe;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NoOutputOperator$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeByIdSeekOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeCountFromCountStoreOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeHashJoinOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeHashJoinSingleNodeOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeIndexContainsScanOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeIndexEndsWithScanOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeIndexScanOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NodeIndexSeekOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.Operator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OptionalOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ProduceResultOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ProjectOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.RelationshipCountFromCountStoreOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SlottedPipeHeadOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SlottedPipeMiddleOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SlottedPipeOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SortMergeOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.SortPreOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.TopOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.UndirectedRelationshipByIdSeekOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.UnwindOperator;
import org.neo4j.cypher.internal.runtime.pipelined.operators.VarExpandOperator;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescription;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescriptionImpl;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity.;
import org.neo4j.cypher.internal.runtime.slotted.SlottedPipeMapper$;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.expressions.Ands;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.LabelName;
import org.neo4j.cypher.internal.v4_0.expressions.LabelToken;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import org.neo4j.cypher.internal.v4_0.expressions.RelTypeName;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.exceptions.InternalException;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class OperatorFactory
{
    private final ExecutionGraphDefinition executionGraphDefinition;
    private final ExpressionConverters converters;
    private final boolean readOnly;
    private final QueryIndexRegistrator indexRegistrator;
    private final SemanticTable semanticTable;
    private final InterpretedPipesFallbackPolicy interpretedPipesFallbackPolicy;
    private final Option<PipeMapper> slottedPipeBuilder;
    private final PhysicalPlan physicalPlan;
    private final AggregatorFactory aggregatorFactory;
    private final PipelinedPipelineBreakingPolicy breakingPolicyForInterpretedPipesFallback;

    public OperatorFactory( final ExecutionGraphDefinition executionGraphDefinition, final ExpressionConverters converters, final boolean readOnly,
            final QueryIndexRegistrator indexRegistrator, final SemanticTable semanticTable,
            final InterpretedPipesFallbackPolicy interpretedPipesFallbackPolicy, final Option<PipeMapper> slottedPipeBuilder )
    {
        this.executionGraphDefinition = executionGraphDefinition;
        this.converters = converters;
        this.readOnly = readOnly;
        this.indexRegistrator = indexRegistrator;
        this.semanticTable = semanticTable;
        this.interpretedPipesFallbackPolicy = interpretedPipesFallbackPolicy;
        this.slottedPipeBuilder = slottedPipeBuilder;
        this.physicalPlan = executionGraphDefinition.physicalPlan();
        this.aggregatorFactory = new AggregatorFactory( this.physicalPlan() );
        this.breakingPolicyForInterpretedPipesFallback =
                new PipelinedPipelineBreakingPolicy( OperatorFusionPolicy.OPERATOR_FUSION_DISABLED$.MODULE$, interpretedPipesFallbackPolicy );
    }

    public ExecutionGraphDefinition executionGraphDefinition()
    {
        return this.executionGraphDefinition;
    }

    public ExpressionConverters converters()
    {
        return this.converters;
    }

    public boolean readOnly()
    {
        return this.readOnly;
    }

    public QueryIndexRegistrator indexRegistrator()
    {
        return this.indexRegistrator;
    }

    public InterpretedPipesFallbackPolicy interpretedPipesFallbackPolicy()
    {
        return this.interpretedPipesFallbackPolicy;
    }

    public Option<PipeMapper> slottedPipeBuilder()
    {
        return this.slottedPipeBuilder;
    }

    private PhysicalPlan physicalPlan()
    {
        return this.physicalPlan;
    }

    private AggregatorFactory aggregatorFactory()
    {
        return this.aggregatorFactory;
    }

    private PipelinedPipelineBreakingPolicy breakingPolicyForInterpretedPipesFallback()
    {
        return this.breakingPolicyForInterpretedPipesFallback;
    }

    public Operator create( final LogicalPlan plan, final BufferDefinition inputBuffer )
    {
        boolean var140;
        Some var141;
        Option var142;
        label206:
        {
            int id = plan.id();
            SlotConfiguration slots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( id );
            SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( slots );
            boolean var7 = false;
            Aggregation var8 = null;
            Object var3;
            if ( plan instanceof Input )
            {
                Input var10 = (Input) plan;
                Seq nodes = var10.nodes();
                Seq relationships = var10.relationships();
                Seq variables = var10.variables();
                var3 = new InputOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),(int[]) ((TraversableOnce) nodes.map( ( v ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$create$1( slots, v ) );
            }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                (int[]) ((TraversableOnce) relationships.map( ( v ) -> {
                    return BoxesRunTime.boxToInteger( $anonfun$create$2( slots, v ) );
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                (int[]) ((TraversableOnce) variables.map( ( v ) -> {
                    return BoxesRunTime.boxToInteger( $anonfun$create$3( slots, v ) );
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int()));
            }
            else if ( plan instanceof AllNodesScan )
            {
                AllNodesScan var14 = (AllNodesScan) plan;
                String column = var14.idName();
                SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                var3 = new AllNodeScanOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),slots.getLongOffsetFor( column ), argumentSize);
            }
            else if ( plan instanceof NodeByLabelScan )
            {
                NodeByLabelScan var17 = (NodeByLabelScan) plan;
                String column = var17.idName();
                LabelName label = var17.label();
                SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                this.indexRegistrator().registerLabelScan();
                var3 = new LabelScanOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                slots.getLongOffsetFor( column ), org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..
                MODULE$.apply( label, this.semanticTable ), argumentSize);
            }
            else if ( !(plan instanceof NodeIndexSeek) )
            {
                if ( plan instanceof NodeUniqueIndexSeek )
                {
                    NodeUniqueIndexSeek var30 = (NodeUniqueIndexSeek) plan;
                    String column = var30.idName();
                    LabelToken label = var30.label();
                    Seq properties = var30.properties();
                    QueryExpression valueExpr = var30.valueExpr();
                    IndexOrder indexOrder = var30.indexOrder();
                    SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                    IndexSeekMode indexSeekMode = (new IndexSeekModeFactory( true, this.readOnly() )).fromQueryExpression( valueExpr );
                    var3 = new NodeIndexSeekOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getLongOffsetFor( column ), (SlottedIndexedProperty[]) ((TraversableOnce) properties.map( ( x$3 ) -> {
                        return SlottedIndexedProperty$.MODULE$.apply( column, x$3, slots );
                    }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( SlottedIndexedProperty.class )),
                    this.indexRegistrator().registerQueryIndex( label, properties ), org.neo4j.cypher.internal.runtime.KernelAPISupport..
                    MODULE$.asKernelIndexOrder( indexOrder ), argumentSize, valueExpr.map( ( x$4 ) -> {
                        return this.converters().toCommandExpression( id, x$4 );
                    } ), indexSeekMode);
                }
                else if ( plan instanceof NodeIndexScan )
                {
                    NodeIndexScan var38 = (NodeIndexScan) plan;
                    String column = var38.idName();
                    LabelToken labelToken = var38.label();
                    Seq properties = var38.properties();
                    IndexOrder indexOrder = var38.indexOrder();
                    SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                    var3 = new NodeIndexScanOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getLongOffsetFor( column ), (SlottedIndexedProperty[]) ((TraversableOnce) properties.map( ( x$5 ) -> {
                        return SlottedIndexedProperty$.MODULE$.apply( column, x$5, slots );
                    }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( SlottedIndexedProperty.class )),
                    this.indexRegistrator().registerQueryIndex( labelToken, properties ), org.neo4j.cypher.internal.runtime.KernelAPISupport..
                    MODULE$.asKernelIndexOrder( indexOrder ), argumentSize);
                }
                else if ( plan instanceof NodeIndexContainsScan )
                {
                    NodeIndexContainsScan var44 = (NodeIndexContainsScan) plan;
                    String column = var44.idName();
                    LabelToken labelToken = var44.label();
                    IndexedProperty property = var44.property();
                    Expression valueExpr = var44.valueExpr();
                    IndexOrder indexOrder = var44.indexOrder();
                    SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                    var3 = new NodeIndexContainsScanOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getLongOffsetFor( column ), SlottedIndexedProperty$.MODULE$.apply( column, property,
                            slots ), this.indexRegistrator().registerQueryIndex( labelToken, property ), org.neo4j.cypher.internal.runtime.KernelAPISupport..
                    MODULE$.asKernelIndexOrder( indexOrder ), this.converters().toCommandExpression( id, valueExpr ), argumentSize);
                }
                else if ( plan instanceof NodeIndexEndsWithScan )
                {
                    NodeIndexEndsWithScan var51 = (NodeIndexEndsWithScan) plan;
                    String column = var51.idName();
                    LabelToken labelToken = var51.label();
                    IndexedProperty property = var51.property();
                    Expression valueExpr = var51.valueExpr();
                    IndexOrder indexOrder = var51.indexOrder();
                    SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                    var3 = new NodeIndexEndsWithScanOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getLongOffsetFor( column ), SlottedIndexedProperty$.MODULE$.apply( column, property,
                            slots ), this.indexRegistrator().registerQueryIndex( labelToken, property ), org.neo4j.cypher.internal.runtime.KernelAPISupport..
                    MODULE$.asKernelIndexOrder( indexOrder ), this.converters().toCommandExpression( id, valueExpr ), argumentSize);
                }
                else if ( plan instanceof NodeByIdSeek )
                {
                    NodeByIdSeek var58 = (NodeByIdSeek) plan;
                    String column = var58.idName();
                    SeekableArgs nodeIds = var58.nodeIds();
                    var3 = new NodeByIdSeekOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getLongOffsetFor( column ), this.converters().toCommandSeekArgs( id,
                            nodeIds ), (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ));
                }
                else if ( plan instanceof DirectedRelationshipByIdSeek )
                {
                    DirectedRelationshipByIdSeek var61 = (DirectedRelationshipByIdSeek) plan;
                    String column = var61.idName();
                    SeekableArgs relIds = var61.relIds();
                    String startNode = var61.startNode();
                    String endNode = var61.endNode();
                    var3 = new DirectedRelationshipByIdSeekOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getLongOffsetFor( column ), slots.getLongOffsetFor( startNode ), slots.getLongOffsetFor(
                            endNode ), this.converters().toCommandSeekArgs( id, relIds ), (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply(
                            id ));
                }
                else if ( plan instanceof UndirectedRelationshipByIdSeek )
                {
                    UndirectedRelationshipByIdSeek var66 = (UndirectedRelationshipByIdSeek) plan;
                    String column = var66.idName();
                    SeekableArgs relIds = var66.relIds();
                    String startNode = var66.leftNode();
                    String endNode = var66.rightNode();
                    var3 = new UndirectedRelationshipByIdSeekOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getLongOffsetFor( column ), slots.getLongOffsetFor( startNode ), slots.getLongOffsetFor(
                            endNode ), this.converters().toCommandSeekArgs( id, relIds ), (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply(
                            id ));
                }
                else if ( plan instanceof NodeCountFromCountStore )
                {
                    NodeCountFromCountStore var71 = (NodeCountFromCountStore) plan;
                    String idName = var71.idName();
                    List labelNames = var71.labelNames();
                    List labels = (List) labelNames.map( ( labelx ) -> {
                        return labelx.map( ( x$6 ) -> {
                            return org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.apply( x$6, this.semanticTable );
                        } );
                    }, scala.collection.immutable.List..MODULE$.canBuildFrom());
                    var3 = new NodeCountFromCountStoreOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                    slots.getReferenceOffsetFor( idName ), labels, (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ));
                }
                else if ( plan instanceof RelationshipCountFromCountStore )
                {
                    RelationshipCountFromCountStore var75 = (RelationshipCountFromCountStore) plan;
                    String idName = var75.idName();
                    Option startLabel = var75.startLabel();
                    Seq typeNames = var75.typeNames();
                    Option endLabel = var75.endLabel();
                    Option maybeStartLabel = startLabel.map( ( labelx ) -> {
                        return org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.apply( labelx, this.semanticTable );
                    } ); RelationshipTypes relationshipTypes = org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..
                    MODULE$.apply( (RelTypeName[]) typeNames.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable);
                    Option maybeEndLabel = endLabel.map( ( labelx ) -> {
                        return org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.apply( labelx, this.semanticTable );
                    } ); var3 = new RelationshipCountFromCountStoreOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),slots.getReferenceOffsetFor(
                        idName ), maybeStartLabel, relationshipTypes, maybeEndLabel, (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ));
                }
                else
                {
                    if ( plan instanceof Expand )
                    {
                        Expand var83 = (Expand) plan;
                        String fromName = var83.from();
                        SemanticDirection dir = var83.dir();
                        Seq types = var83.types();
                        String to = var83.to();
                        String relName = var83.relName();
                        ExpansionMode var89 = var83.mode();
                        if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( var89 )){
                        Slot fromSlot = slots.apply( fromName );
                        int relOffset = slots.getLongOffsetFor( relName );
                        int toOffset = slots.getLongOffsetFor( to );
                        RelationshipTypes lazyTypes = org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..
                        MODULE$.apply( (RelTypeName[]) types.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable);
                        var3 = new ExpandAllOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),fromSlot, relOffset, toOffset, dir, lazyTypes);
                        return (Operator) var3;
                    }
                    }

                    if ( plan instanceof VarExpand )
                    {
                        VarExpandOperator var10000;
                        WorkIdentity var10002;
                        int var10009;
                        int var10010;
                        boolean var10011;
                        SemanticDirection dir;
                        SemanticDirection projectedDir;
                        Option nodePredicate;
                        Option relationshipPredicate;
                        Slot fromSlot;
                        int relOffset;
                        Slot toSlot;
                        RelationshipTypes lazyTypes;
                        int tempNodeOffset;
                        int tempRelationshipOffset;
                        label137:
                        {
                            label136:
                            {
                                VarExpand var94 = (VarExpand) plan;
                                String fromName = var94.from();
                                dir = var94.dir();
                                projectedDir = var94.projectedDir();
                                Seq types = var94.types();
                                String toName = var94.to();
                                String relName = var94.relName();
                                VarPatternLength length = var94.length();
                                ExpansionMode mode = var94.mode();
                                nodePredicate = var94.nodePredicate();
                                relationshipPredicate = var94.relationshipPredicate();
                                fromSlot = slots.apply( fromName );
                                relOffset = slots.getReferenceOffsetFor( relName );
                                toSlot = slots.apply( toName );
                                lazyTypes = org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..
                                MODULE$.apply( (RelTypeName[]) types.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable);
                                tempNodeOffset = VariablePredicates$.MODULE$.expressionSlotForPredicate( nodePredicate );
                                tempRelationshipOffset = VariablePredicates$.MODULE$.expressionSlotForPredicate( relationshipPredicate );
                                var10000 = new VarExpandOperator; var10002 = .MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2());
                                var10009 = length.min();
                                var10010 = BoxesRunTime.unboxToInt( length.max().getOrElse( () -> {
                                    return Integer.MAX_VALUE;
                                } ) );
                                org.neo4j.cypher.internal.logical.plans.ExpandAll.var111 = org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$;
                                if ( mode == null )
                                {
                                    if ( var111 == null )
                                    {
                                        break label136;
                                    }
                                }
                                else if ( mode.equals( var111 ) )
                                {
                                    break label136;
                                }

                                var10011 = false;
                                break label137;
                            }

                            var10011 = true;
                        }

                        var10000.<init>
                        (var10002, fromSlot, relOffset, toSlot, dir, projectedDir, lazyTypes, var10009, var10010, var10011, tempNodeOffset, tempRelationshipOffset, (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) nodePredicate.map(
                                ( x ) -> {
                                    return this.converters().toCommandExpression( id, x.predicate() );
                                } ).getOrElse( () -> {
                            return new True();
                        } ), (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) relationshipPredicate.map( ( x ) -> {
                            return this.converters().toCommandExpression( id, x.predicate() );
                        } ).getOrElse( () -> {
                            return new True();
                        } ));
                        var3 = var10000;
                    }
                    else if ( plan instanceof Optional )
                    {
                        Optional var112 = (Optional) plan;
                        LogicalPlan source = var112.source();
                        Set protectedSymbols = var112.protectedSymbols();
                        int argumentStateMapId = ((OptionalBufferVariant) inputBuffer.variant()).argumentStateMapId();
                        Set nullableKeys = (Set) source.availableSymbols().$minus$minus( protectedSymbols );
                        Slot[] nullableSlots = (Slot[]) ((TraversableOnce) nullableKeys.map( ( k ) -> {
                            return (Slot) slots.get( k ).get();
                        }, scala.collection.immutable.Set..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Slot.class ));
                        SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( plan.id() );
                        int argumentDepth = ((Id) this.physicalPlan().applyPlans().apply( id )).x();
                        int argumentSlotOffset = slots.getArgumentLongOffsetFor( argumentDepth );
                        var3 = new OptionalOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),argumentStateMapId, argumentSlotOffset, scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) nullableSlots ), slots, argumentSize);
                    }
                    else if ( plan instanceof NodeHashJoin )
                    {
                        NodeHashJoin var121 = (NodeHashJoin) plan;
                        PhysicalPlanningAttributes.SlotConfigurations slotConfigs = this.physicalPlan().slotConfigurations();
                        SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( plan.id() );
                        String[] nodes = (String[]) var121.nodes().toArray( scala.reflect.ClassTag..MODULE$.apply( String.class ));
                        int[] lhsOffsets = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) nodes ))).map( ( k ) -> {
                        return BoxesRunTime.boxToInteger( $anonfun$create$19( slots, k ) );
                    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
                        SlotConfiguration rhsSlots = (SlotConfiguration) slotConfigs.apply( var121.right().id() );
                        int[] rhsOffsets = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) nodes ))).map( ( k ) -> {
                        return BoxesRunTime.boxToInteger( $anonfun$create$20( rhsSlots, k ) );
                    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
                        ArrayBuilder copyLongsFromRHS = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Tuple2.class ));
                        ArrayBuilder copyRefsFromRHS = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Tuple2.class ));
                        ArrayBuilder copyCachedPropertiesFromRHS = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Tuple2.class ));
                        rhsSlots.foreachSlotOrdered( ( x0$1, x1$1 ) -> {
                            $anonfun$create$21( slots, argumentSize, copyLongsFromRHS, copyRefsFromRHS, x0$1, x1$1 );
                            return BoxedUnit.UNIT;
                        }, ( cnp ) -> {
                            $anonfun$create$22( slots, argumentSize, rhsSlots, copyCachedPropertiesFromRHS, cnp );
                            return BoxedUnit.UNIT;
                        }, rhsSlots.foreachSlotOrdered$default$3(), rhsSlots.foreachSlotOrdered$default$4() );
                        Tuple2[] longsToCopy = (Tuple2[]) copyLongsFromRHS.result();
                        Tuple2[] refsToCopy = (Tuple2[]) copyRefsFromRHS.result();
                        Tuple2[] cachedPropertiesToCopy = (Tuple2[]) copyCachedPropertiesFromRHS.result();
                        LHSAccumulatingRHSStreamingBufferVariant buffer = (LHSAccumulatingRHSStreamingBufferVariant) inputBuffer.variant();
                        var3 = lhsOffsets.length == 1 ? new NodeHashJoinSingleNodeOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                        buffer.lhsArgumentStateMapId(), buffer.rhsArgumentStateMapId(), lhsOffsets[0], rhsOffsets[0], slots, longsToCopy, refsToCopy, cachedPropertiesToCopy) :
                        new NodeHashJoinOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                        buffer.lhsArgumentStateMapId(), buffer.rhsArgumentStateMapId(), lhsOffsets, rhsOffsets, slots, longsToCopy, refsToCopy, cachedPropertiesToCopy)
                        ;
                    }
                    else if ( plan instanceof CartesianProduct )
                    {
                        LHSAccumulatingRHSStreamingBufferVariant buffer = (LHSAccumulatingRHSStreamingBufferVariant) inputBuffer.variant();
                        var3 = new CartesianProductOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                        buffer.lhsArgumentStateMapId(), buffer.rhsArgumentStateMapId(), slots, (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply(
                                plan.id() ));
                    }
                    else if ( plan instanceof UnwindCollection )
                    {
                        UnwindCollection var136 = (UnwindCollection) plan;
                        String variable = var136.variable();
                        Expression collection = var136.expression();
                        var140 = false;
                        var141 = null;
                        var142 = slots.get( variable );
                        if ( !(var142 instanceof Some) )
                        {
                            break label206;
                        }

                        var140 = true;
                        var141 = (Some) var142;
                        Slot var143 = (Slot) var141.value();
                        if ( !(var143 instanceof RefSlot) )
                        {
                            break label206;
                        }

                        RefSlot var144 = (RefSlot) var143;
                        int idx = var144.offset();
                        org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression runtimeExpression =
                                this.converters().toCommandExpression( id, collection );
                        var3 = new UnwindOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),runtimeExpression, idx);
                    }
                    else if ( plan instanceof Sort )
                    {
                        Sort var148 = (Sort) plan;
                        Seq sortItems = var148.sortItems();
                        Seq ordering = (Seq) sortItems.map( ( x$7 ) -> {
                            return SlottedPipeMapper$.MODULE$.translateColumnOrder( slots, x$7 );
                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                        int argumentDepth = ((Id) this.physicalPlan().applyPlans().apply( id )).x();
                        int argumentSlot = slots.getArgumentLongOffsetFor( argumentDepth );
                        int argumentStateMapId = ((ArgumentStateBufferVariant) inputBuffer.variant()).argumentStateMapId();
                        var3 = new SortMergeOperator( argumentStateMapId,.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),ordering, argumentSlot);
                    }
                    else if ( plan instanceof Top )
                    {
                        Top var154 = (Top) plan;
                        Seq sortItems = var154.sortItems();
                        Expression limit = var154.limit();
                        Seq ordering = (Seq) sortItems.map( ( x$8 ) -> {
                            return SlottedPipeMapper$.MODULE$.translateColumnOrder( slots, x$8 );
                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                        int argumentStateMapId = ((ArgumentStateBufferVariant) inputBuffer.variant()).argumentStateMapId();
                        var3 = (new TopOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()), ordering, this.converters().toCommandExpression(
                                plan.id(), limit ))).reducer( argumentStateMapId );
                    }
                    else
                    {
                        if ( plan instanceof Aggregation )
                        {
                            var7 = true;
                            var8 = (Aggregation) plan;
                            Map groupingExpressions = var8.groupingExpressions();
                            Map aggregationExpression = var8.aggregationExpression();
                            if ( groupingExpressions.isEmpty() )
                            {
                                int argumentStateMapId = ((ArgumentStateBufferVariant) inputBuffer.variant()).argumentStateMapId();
                                ArrayBuilder aggregators = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Aggregator.class ));
                                ArrayBuilder outputSlots = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.Int());
                                aggregationExpression.foreach( ( x0$2 ) -> {
                                    if ( x0$2 != null )
                                    {
                                        String key = (String) x0$2._1();
                                        Expression astExpression = (Expression) x0$2._2();
                                        Slot outputSlot = (Slot) slots.get( key ).get();
                                        Tuple2 var12 = this.aggregatorFactory().newAggregator( astExpression );
                                        if ( var12 != null )
                                        {
                                            Aggregator aggregator = (Aggregator) var12._1();
                                            aggregators.$plus$eq( aggregator );
                                            ArrayBuilder var5 = (ArrayBuilder) outputSlots.$plus$eq( BoxesRunTime.boxToInteger( outputSlot.offset() ) );
                                            return var5;
                                        }
                                        else
                                        {
                                            throw new MatchError( var12 );
                                        }
                                    }
                                    else
                                    {
                                        throw new MatchError( x0$2 );
                                    }
                                } );
                                var3 = (new AggregationOperatorNoGrouping(.MODULE$.fromPlan(
                                        plan,.MODULE$.fromPlan$default$2()), (Aggregator[]) aggregators.result())).
                                reducer( argumentStateMapId, (int[]) outputSlots.result() );
                                return (Operator) var3;
                            }
                        }

                        if ( var7 )
                        {
                            Map groupingExpressions = var8.groupingExpressions();
                            Map aggregationExpression = var8.aggregationExpression();
                            int argumentStateMapId = ((ArgumentStateBufferVariant) inputBuffer.variant()).argumentStateMapId();
                            GroupingExpression groupings =
                                    this.converters().toGroupingExpression( id, groupingExpressions, (Seq) scala.collection.Seq..MODULE$.empty());
                            ArrayBuilder aggregators = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Aggregator.class ));
                            ArrayBuilder outputSlots = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.Int());
                            aggregationExpression.foreach( ( x0$3 ) -> {
                                if ( x0$3 != null )
                                {
                                    String key = (String) x0$3._1();
                                    Expression astExpression = (Expression) x0$3._2();
                                    Slot outputSlot = (Slot) slots.get( key ).get();
                                    Tuple2 var12 = this.aggregatorFactory().newAggregator( astExpression );
                                    if ( var12 != null )
                                    {
                                        Aggregator aggregator = (Aggregator) var12._1();
                                        aggregators.$plus$eq( aggregator );
                                        ArrayBuilder var5 = (ArrayBuilder) outputSlots.$plus$eq( BoxesRunTime.boxToInteger( outputSlot.offset() ) );
                                        return var5;
                                    }
                                    else
                                    {
                                        throw new MatchError( var12 );
                                    }
                                }
                                else
                                {
                                    throw new MatchError( x0$3 );
                                }
                            } );
                            var3 = (new AggregationOperator(.MODULE$.fromPlan(
                                    plan,.MODULE$.fromPlan$default$2()), (Aggregator[]) aggregators.result(), groupings)).
                            reducer( argumentStateMapId, (int[]) outputSlots.result() );
                        }
                        else if ( plan instanceof ProduceResult )
                        {
                            ProduceResult var170 = (ProduceResult) plan;
                            var3 = this.createProduceResults( var170 );
                        }
                        else if ( plan instanceof Argument )
                        {
                            var3 = new ArgumentOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                            (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id ));
                        }
                        else
                        {
                            if ( !this.slottedPipeBuilder().isDefined() )
                            {
                                throw new CantCompileQueryException(
                                        (new StringBuilder( 75 )).append( "Pipelined does not yet support the plans including `" ).append( plan ).append(
                                                "`, use another runtime." ).toString() );
                            }

                            this.interpretedPipesFallbackPolicy().breakOn( plan );
                            var3 = this.createSlottedPipeHeadOperator( plan );
                        }
                    }
                }
            }
            else
            {
                String column;
                LabelToken label;
                Seq properties;
                QueryExpression valueExpr;
                IndexOrder indexOrder;
                SlotConfiguration.Size argumentSize;
                IndexSeekMode indexSeekMode;
                label178:
                {
                    NodeIndexSeek var21 = (NodeIndexSeek) plan;
                    column = var21.idName();
                    label = var21.label();
                    properties = var21.properties();
                    valueExpr = var21.valueExpr();
                    indexOrder = var21.indexOrder();
                    argumentSize = (SlotConfiguration.Size) this.physicalPlan().argumentSizes().apply( id );
                    indexSeekMode = (new IndexSeekModeFactory( false, this.readOnly() )).fromQueryExpression( valueExpr );
                    org.neo4j.cypher.internal.runtime.interpreted.pipes.LockingUniqueIndexSeek.var29 =
                            org.neo4j.cypher.internal.runtime.interpreted.pipes.LockingUniqueIndexSeek..MODULE$;
                    if ( indexSeekMode == null )
                    {
                        if ( var29 != null )
                        {
                            break label178;
                        }
                    }
                    else if ( !indexSeekMode.equals( var29 ) )
                    {
                        break label178;
                    }

                    throw new CantCompileQueryException(
                            "Pipelined does not yet support the plans including `NodeUniqueIndexSeek(Locking)`, use another runtime." );
                }

                var3 = new NodeIndexSeekOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),
                slots.getLongOffsetFor( column ), (SlottedIndexedProperty[]) ((TraversableOnce) properties.map( ( x$1 ) -> {
                    return SlottedIndexedProperty$.MODULE$.apply( column, x$1, slots );
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( SlottedIndexedProperty.class )),
                this.indexRegistrator().registerQueryIndex( label, properties ), org.neo4j.cypher.internal.runtime.KernelAPISupport..
                MODULE$.asKernelIndexOrder( indexOrder ), argumentSize, valueExpr.map( ( x$2 ) -> {
                    return this.converters().toCommandExpression( id, x$2 );
                } ), indexSeekMode);
            }

            return (Operator) var3;
        }

        if ( var140 )
        {
            Slot slot = (Slot) var141.value();
            throw new InternalException( (new StringBuilder( 26 )).append( slot ).append( " cannot be used for UNWIND" ).toString() );
        }
        else if ( scala.None..MODULE$.equals( var142 )){
        throw new InternalException( "No slot found for UNWIND" );
    } else{
        throw new MatchError( var142 );
    }
    }

    public MiddleOperator[] createMiddleOperators( final Seq<LogicalPlan> middlePlans, final Operator headOperator )
    {
        Object var3;
        if ( headOperator instanceof SlottedPipeOperator )
        {
            SlottedPipeOperator var6 = (SlottedPipeOperator) headOperator;
            var3 = new Some( var6 );
        }
        else
        {
            var3 = scala.None..MODULE$;
        }

        ArrayBuffer middleOperatorBuilder = new ArrayBuffer();
        middlePlans.foldLeft( new Tuple2( middleOperatorBuilder, var3 ), ( acc, plan ) -> {
            return this.createMiddleFoldFunction( acc, plan );
        } );
        MiddleOperator[] middleOperators =
                (MiddleOperator[]) middleOperatorBuilder.result().toArray( scala.reflect.ClassTag..MODULE$.apply( MiddleOperator.class ));
        return middleOperators;
    }

    private Tuple2<ArrayBuffer<MiddleOperator>,Option<SlottedPipeOperator>> createMiddleFoldFunction(
            final Tuple2<ArrayBuffer<MiddleOperator>,Option<SlottedPipeOperator>> acc, final LogicalPlan plan )
    {
        if ( acc == null )
        {
            throw new MatchError( acc );
        }
        else
        {
            ArrayBuffer middleOperators = (ArrayBuffer) acc._1();
            Option maybeSlottedPipeOperatorToChainOnTo = (Option) acc._2();
            Tuple2 var4 = new Tuple2( middleOperators, maybeSlottedPipeOperatorToChainOnTo );
            ArrayBuffer middleOperators = (ArrayBuffer) var4._1();
            Option maybeSlottedPipeOperatorToChainOnTo = (Option) var4._2();
            Option maybeNewOperator = this.createMiddleOrUpdateSlottedPipeChain( plan, maybeSlottedPipeOperatorToChainOnTo );
            middleOperators.$plus$plus$eq( scala.Option..MODULE$.option2Iterable( maybeNewOperator ));
            boolean var12 = false;
            Some var13 = null;
            Tuple2 var3;
            if ( maybeNewOperator instanceof Some )
            {
                var12 = true;
                var13 = (Some) maybeNewOperator;
                MiddleOperator spo = (MiddleOperator) var13.value();
                if ( spo instanceof SlottedPipeOperator )
                {
                    SlottedPipeOperator var16 = (SlottedPipeOperator) spo;
                    if ( maybeSlottedPipeOperatorToChainOnTo.isEmpty() )
                    {
                        var3 = new Tuple2( middleOperators, new Some( var16 ) );
                        return var3;
                    }
                }
            }

            if ( var12 && maybeSlottedPipeOperatorToChainOnTo.isDefined() )
            {
                var3 = new Tuple2( middleOperators, scala.None..MODULE$);
            }
            else
            {
                var3 = acc;
            }

            return var3;
        }
    }

    public Option<MiddleOperator> createMiddleOrUpdateSlottedPipeChain( final LogicalPlan plan,
            final Option<SlottedPipeOperator> maybeSlottedPipeOperatorToChainOnTo )
    {
        int id = plan.id();
        SlotConfiguration slots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( id );
        SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( slots );
        Object var3;
        if ( plan instanceof Selection )
        {
            Selection var7 = (Selection) plan;
            Ands predicate = var7.predicate();
            var3 = new Some( new FilterOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2() ), this.converters().toCommandExpression( id, predicate )))
            ;
        }
        else
        {
            if ( plan instanceof Limit )
            {
                Limit var9 = (Limit) plan;
                Expression count = var9.count();
                Ties var11 = var9.ties();
                if ( org.neo4j.cypher.internal.logical.plans.DoNotIncludeTies..MODULE$.equals( var11 )){
                int argumentStateMapId = this.executionGraphDefinition().findArgumentStateMapForPlan( id );
                var3 = new Some(
                        new LimitOperator( argumentStateMapId,.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2() ), this.converters().toCommandExpression(
                        plan.id(), count )));
                return (Option) var3;
            }
            }

            if ( plan instanceof Distinct )
            {
                Distinct var13 = (Distinct) plan;
                Map groupingExpressions = var13.groupingExpressions();
                int argumentStateMapId = this.executionGraphDefinition().findArgumentStateMapForPlan( id );
                GroupingExpression groupings = this.converters().toGroupingExpression( id, groupingExpressions, (Seq) scala.collection.Seq..MODULE$.empty());
                var3 = new Some( new DistinctOperator( argumentStateMapId,.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2() ), groupings));
            }
            else if ( plan instanceof Projection )
            {
                Projection var17 = (Projection) plan;
                Map expressions = var17.projectExpressions();
                Map toProject = (Map) expressions.collect( new Serializable( (OperatorFactory) null, slots )
                {
                    public static final long serialVersionUID = 0L;
                    private final SlotConfiguration slots$2;

                    public
                    {
                        this.slots$2 = slots$2;
                    }

                    public final <A1 extends Tuple2<String,Expression>, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                    {
                        Object var3;
                        if ( x1 != null )
                        {
                            String k = (String) x1._1();
                            Expression e = (Expression) x1._2();
                            if ( SlotConfiguration$.MODULE$.isRefSlotAndNotAlias( this.slots$2, k ) )
                            {
                                var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( k ), e);
                                return var3;
                            }
                        }

                        var3 = var2.apply( x1 );
                        return var3;
                    }

                    public final boolean isDefinedAt( final Tuple2<String,Expression> x1 )
                    {
                        boolean var2;
                        if ( x1 != null )
                        {
                            String k = (String) x1._1();
                            if ( SlotConfiguration$.MODULE$.isRefSlotAndNotAlias( this.slots$2, k ) )
                            {
                                var2 = true;
                                return var2;
                            }
                        }

                        var2 = false;
                        return var2;
                    }
                }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                CommandProjection projectionOps = this.converters().toCommandProjection( id, toProject );
                var3 = new Some( new ProjectOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2() ), projectionOps));
            }
            else if ( plan instanceof CacheProperties )
            {
                CacheProperties var21 = (CacheProperties) plan;
                Set properties = var21.properties();
                org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression[] propertyOps =
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                                (Object[]) properties.toArray( scala.reflect.ClassTag..MODULE$.apply( LogicalProperty.class ) )))).map( ( x$10 ) -> {
                return this.converters().toCommandExpression( id, x$10 );
            }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply(
                    org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression.class )));
                var3 = new Some( new CachePropertiesOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2() ), propertyOps));
            }
            else if ( plan instanceof Argument )
            {
                var3 = scala.None..MODULE$;
            }
            else
            {
                if ( !this.slottedPipeBuilder().isDefined() )
                {
                    throw new CantCompileQueryException( (new StringBuilder( 78 )).append( "Pipelined does not yet support using `" ).append( plan ).append(
                            "` as a middle plan, use another runtime." ).toString() );
                }

                this.interpretedPipesFallbackPolicy().breakOn( plan );
                if ( this.breakingPolicyForInterpretedPipesFallback().breakOn( plan ) )
                {
                    throw new CantCompileQueryException( (new StringBuilder( 87 )).append( "Pipelined does not yet support using `" ).append( plan ).append(
                            "` as a fallback middle plan, use another runtime." ).toString() );
                }

                var3 = this.createSlottedPipeMiddleOperator( plan, maybeSlottedPipeOperatorToChainOnTo );
            }
        }

        return (Option) var3;
    }

    public ProduceResultOperator createProduceResults( final ProduceResult plan )
    {
        SlotConfiguration slots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( plan.id() );
        Seq runtimeColumns = SlottedPipeMapper$.MODULE$.createProjectionsForResult( plan.columns(), slots );
        return new ProduceResultOperator(.MODULE$.fromPlan( plan,.MODULE$.fromPlan$default$2()),slots, runtimeColumns);
    }

    public OutputOperator createOutput( final OutputDefinition outputDefinition, final boolean nextPipelineFused )
    {
        Object var3;
        if ( NoOutput$.MODULE$.equals( outputDefinition ) )
        {
            var3 = NoOutputOperator$.MODULE$;
        }
        else if ( outputDefinition instanceof MorselBufferOutput )
        {
            MorselBufferOutput var6 = (MorselBufferOutput) outputDefinition;
            int bufferId = var6.id();
            int planId = var6.nextPipelineHeadPlanId();
            var3 = new MorselBufferOutputOperator( bufferId, planId, nextPipelineFused );
        }
        else if ( outputDefinition instanceof MorselArgumentStateBufferOutput )
        {
            MorselArgumentStateBufferOutput var9 = (MorselArgumentStateBufferOutput) outputDefinition;
            int bufferId = var9.id();
            int argumentSlotOffset = var9.argumentSlotOffset();
            int planId = var9.nextPipelineHeadPlanId();
            var3 = new MorselArgumentStateBufferOutputOperator( bufferId, argumentSlotOffset, planId, nextPipelineFused );
        }
        else if ( outputDefinition instanceof ProduceResultOutput )
        {
            ProduceResultOutput var13 = (ProduceResultOutput) outputDefinition;
            ProduceResult p = var13.plan();
            var3 = this.createProduceResults( p );
        }
        else
        {
            if ( !(outputDefinition instanceof ReduceOutput) )
            {
                throw new MatchError( outputDefinition );
            }

            ReduceOutput var15 = (ReduceOutput) outputDefinition;
            int bufferId = var15.bufferId();
            LogicalPlan plan = var15.plan();
            int id = plan.id();
            SlotConfiguration slots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( id );
            SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( slots );
            int argumentDepth = ((Id) this.physicalPlan().applyPlans().apply( id )).x();
            int argumentSlot = slots.getArgumentLongOffsetFor( argumentDepth );
            boolean var22 = false;
            Aggregation var23 = null;
            Object var4;
            if ( plan instanceof Sort )
            {
                Sort var25 = (Sort) plan;
                Seq sortItems = var25.sortItems();
                Seq ordering = (Seq) sortItems.map( ( x$11 ) -> {
                    return SlottedPipeMapper$.MODULE$.translateColumnOrder( slots, x$11 );
                }, scala.collection.Seq..MODULE$.canBuildFrom());
                var4 = new SortPreOperator(.MODULE$.fromPlan( plan, "Pre" ), argumentSlot, bufferId, ordering);
            }
            else if ( plan instanceof Top )
            {
                Top var28 = (Top) plan;
                Seq sortItems = var28.sortItems();
                Expression limit = var28.limit();
                Seq ordering = (Seq) sortItems.map( ( x$12 ) -> {
                    return SlottedPipeMapper$.MODULE$.translateColumnOrder( slots, x$12 );
                }, scala.collection.Seq..MODULE$.canBuildFrom());
                var4 = (new TopOperator(.MODULE$.fromPlan( plan, "Pre" ), ordering, this.converters().toCommandExpression( plan.id(), limit ))).
                mapper( argumentSlot, bufferId );
            }
            else
            {
                label57:
                {
                    if ( plan instanceof Aggregation )
                    {
                        var22 = true;
                        var23 = (Aggregation) plan;
                        Map groupingExpressions = var23.groupingExpressions();
                        Map aggregationExpression = var23.aggregationExpression();
                        if ( groupingExpressions.isEmpty() )
                        {
                            ArrayBuilder aggregators = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Aggregator.class ));
                            ArrayBuilder expressions = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply(
                                org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression.class ));
                            aggregationExpression.foreach( ( x0$4 ) -> {
                                if ( x0$4 != null )
                                {
                                    Expression astExpression = (Expression) x0$4._2();
                                    Tuple2 var10 = this.aggregatorFactory().newAggregator( astExpression );
                                    if ( var10 != null )
                                    {
                                        Aggregator aggregator = (Aggregator) var10._1();
                                        Expression expression = (Expression) var10._2();
                                        Tuple2 var6 = new Tuple2( aggregator, expression );
                                        Aggregator aggregatorx = (Aggregator) var6._1();
                                        Expression expressionx = (Expression) var6._2();
                                        aggregators.$plus$eq( aggregatorx );
                                        ArrayBuilder var5 = (ArrayBuilder) expressions.$plus$eq( this.converters().toCommandExpression( id, expressionx ) );
                                        return var5;
                                    }
                                    else
                                    {
                                        throw new MatchError( var10 );
                                    }
                                }
                                else
                                {
                                    throw new MatchError( x0$4 );
                                }
                            } );
                            var4 = (new AggregationOperatorNoGrouping(.MODULE$.fromPlan( plan, "Pre" ), (Aggregator[]) aggregators.result())).
                            mapper( argumentSlot, bufferId,
                                    (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression[]) expressions.result() );
                            break label57;
                        }
                    }

                    if ( !var22 )
                    {
                        throw new MatchError( plan );
                    }

                    Map groupingExpressions = var23.groupingExpressions();
                    Map aggregationExpression = var23.aggregationExpression();
                    GroupingExpression groupings = this.converters().toGroupingExpression( id, groupingExpressions, (Seq) scala.collection.Seq..MODULE$.empty())
                    ;
                    ArrayBuilder aggregators = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Aggregator.class ));
                    ArrayBuilder expressions = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply(
                        org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression.class ));
                    aggregationExpression.foreach( ( x0$5 ) -> {
                        if ( x0$5 != null )
                        {
                            Expression astExpression = (Expression) x0$5._2();
                            Tuple2 var10 = this.aggregatorFactory().newAggregator( astExpression );
                            if ( var10 != null )
                            {
                                Aggregator aggregator = (Aggregator) var10._1();
                                Expression expression = (Expression) var10._2();
                                Tuple2 var6 = new Tuple2( aggregator, expression );
                                Aggregator aggregatorx = (Aggregator) var6._1();
                                Expression expressionx = (Expression) var6._2();
                                aggregators.$plus$eq( aggregatorx );
                                ArrayBuilder var5 = (ArrayBuilder) expressions.$plus$eq( this.converters().toCommandExpression( id, expressionx ) );
                                return var5;
                            }
                            else
                            {
                                throw new MatchError( var10 );
                            }
                        }
                        else
                        {
                            throw new MatchError( x0$5 );
                        }
                    } );
                    var4 = (new AggregationOperator(.MODULE$.fromPlan( plan, "Pre" ), (Aggregator[]) aggregators.result(), groupings)).
                    mapper( argumentSlot, bufferId, (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression[]) expressions.result() );
                }
            }

            var3 = var4;
        }

        return (OutputOperator) var3;
    }

    private String workIdentityDescriptionForPipe( final Pipe pipe )
    {
        List pipeNames = this.collectPipeNames$1( pipe, scala.collection.immutable.Nil..MODULE$);
        return pipeNames.mkString( "(", "->", ")" );
    }

    private WorkIdentityMutableDescription workIdentityFromSlottedPipePlan( final String opName, final LogicalPlan plan, final Pipe pipe )
    {
        String prefix = (new StringBuilder( 2 )).append( opName ).append( "[" ).append( plan.getClass().getSimpleName() ).append( "]" ).toString();
        String pipeDescription = this.workIdentityDescriptionForPipe( pipe );
        return new WorkIdentityMutableDescriptionImpl( plan.id(), prefix, pipeDescription );
    }

    public Operator createSlottedPipeHeadOperator( final LogicalPlan plan )
    {
        MorselFeedPipe feedPipe = new MorselFeedPipe( org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID());
        Pipe pipe = ((PipeMapper) this.slottedPipeBuilder().get()).onOneChildPlan( plan, feedPipe );
        WorkIdentityMutableDescription workIdentity = this.workIdentityFromSlottedPipePlan( "SlottedPipeHead", plan, pipe );
        return new SlottedPipeHeadOperator( workIdentity, pipe );
    }

    public Option<MiddleOperator> createSlottedPipeMiddleOperator( final LogicalPlan plan,
            final Option<SlottedPipeOperator> maybeSlottedPipeOperatorToChainOnTo )
    {
        Object var3;
        if ( maybeSlottedPipeOperatorToChainOnTo instanceof Some )
        {
            Some var5 = (Some) maybeSlottedPipeOperatorToChainOnTo;
            SlottedPipeOperator slottedPipeOperator = (SlottedPipeOperator) var5.value();
            Pipe chainedPipe = ((PipeMapper) this.slottedPipeBuilder().get()).onOneChildPlan( plan, slottedPipeOperator.pipe() );
            slottedPipeOperator.setPipe( chainedPipe );
            slottedPipeOperator.workIdentity().updateDescription( this.workIdentityDescriptionForPipe( chainedPipe ) );
            var3 = scala.None..MODULE$;
        }
        else
        {
            if ( !scala.None..MODULE$.equals( maybeSlottedPipeOperatorToChainOnTo )){
            throw new MatchError( maybeSlottedPipeOperatorToChainOnTo );
        }

            MorselFeedPipe feedPipe = new MorselFeedPipe( org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID());
            Pipe pipe = ((PipeMapper) this.slottedPipeBuilder().get()).onOneChildPlan( plan, feedPipe );
            WorkIdentityMutableDescription workIdentity = this.workIdentityFromSlottedPipePlan( "SlottedPipeMiddle", plan, pipe );
            var3 = new Some( new SlottedPipeMiddleOperator( workIdentity, pipe ) );
        }

        return (Option) var3;
    }

    private final List collectPipeNames$1( final Pipe pipe, final List acc )
    {
        while ( true )
        {
            String pipeName = pipe.getClass().getSimpleName();
            Pipe var10000;
            if ( pipe instanceof PipeWithSource )
            {
                PipeWithSource var7 = (PipeWithSource) pipe;
                var10000 = var7.getSource();
                acc = acc.$colon$colon( pipeName );
                pipe = var10000;
            }
            else
            {
                if ( !(pipe instanceof DropResultPipe) )
                {
                    return acc;
                }

                DropResultPipe var9 = (DropResultPipe) pipe;
                var10000 = var9.source();
                acc = acc.$colon$colon( pipeName );
                pipe = var10000;
            }
        }
    }
}
