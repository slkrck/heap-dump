package org.neo4j.cypher.internal.runtime.pipelined.execution;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface WorkerWaker
{
    void wakeOne();
}
