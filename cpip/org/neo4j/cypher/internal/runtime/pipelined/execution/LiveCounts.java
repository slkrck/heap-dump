package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.RuntimeResourceLeakException;
import scala.Option;
import scala.Some;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class LiveCounts
{
    private long nodeCursorPool;
    private long relationshipGroupCursorPool;
    private long relationshipTraversalCursorPool;
    private long relationshipScanCursorPool;
    private long nodeValueIndexCursorPool;
    private long nodeLabelIndexCursorPool;

    public LiveCounts( final long nodeCursorPool, final long relationshipGroupCursorPool, final long relationshipTraversalCursorPool,
            final long relationshipScanCursorPool, final long nodeValueIndexCursorPool, final long nodeLabelIndexCursorPool )
    {
        this.nodeCursorPool = nodeCursorPool;
        this.relationshipGroupCursorPool = relationshipGroupCursorPool;
        this.relationshipTraversalCursorPool = relationshipTraversalCursorPool;
        this.relationshipScanCursorPool = relationshipScanCursorPool;
        this.nodeValueIndexCursorPool = nodeValueIndexCursorPool;
        this.nodeLabelIndexCursorPool = nodeLabelIndexCursorPool;
        super();
    }

    public static long $lessinit$greater$default$6()
    {
        return LiveCounts$.MODULE$.$lessinit$greater$default$6();
    }

    public static long $lessinit$greater$default$5()
    {
        return LiveCounts$.MODULE$.$lessinit$greater$default$5();
    }

    public static long $lessinit$greater$default$4()
    {
        return LiveCounts$.MODULE$.$lessinit$greater$default$4();
    }

    public static long $lessinit$greater$default$3()
    {
        return LiveCounts$.MODULE$.$lessinit$greater$default$3();
    }

    public static long $lessinit$greater$default$2()
    {
        return LiveCounts$.MODULE$.$lessinit$greater$default$2();
    }

    public static long $lessinit$greater$default$1()
    {
        return LiveCounts$.MODULE$.$lessinit$greater$default$1();
    }

    private static final Option reportLeak$1( final long liveCount, final String poolName )
    {
        return (Option) (liveCount != 0L ? new Some(
                (new StringBuilder( 28 )).append( poolName ).append( "s had a total live count of " ).append( liveCount ).toString() ) : scala.None..MODULE$);
    }

    public long nodeCursorPool()
    {
        return this.nodeCursorPool;
    }

    public void nodeCursorPool_$eq( final long x$1 )
    {
        this.nodeCursorPool = x$1;
    }

    public long relationshipGroupCursorPool()
    {
        return this.relationshipGroupCursorPool;
    }

    public void relationshipGroupCursorPool_$eq( final long x$1 )
    {
        this.relationshipGroupCursorPool = x$1;
    }

    public long relationshipTraversalCursorPool()
    {
        return this.relationshipTraversalCursorPool;
    }

    public void relationshipTraversalCursorPool_$eq( final long x$1 )
    {
        this.relationshipTraversalCursorPool = x$1;
    }

    public long relationshipScanCursorPool()
    {
        return this.relationshipScanCursorPool;
    }

    public void relationshipScanCursorPool_$eq( final long x$1 )
    {
        this.relationshipScanCursorPool = x$1;
    }

    public long nodeValueIndexCursorPool()
    {
        return this.nodeValueIndexCursorPool;
    }

    public void nodeValueIndexCursorPool_$eq( final long x$1 )
    {
        this.nodeValueIndexCursorPool = x$1;
    }

    public long nodeLabelIndexCursorPool()
    {
        return this.nodeLabelIndexCursorPool;
    }

    public void nodeLabelIndexCursorPool_$eq( final long x$1 )
    {
        this.nodeLabelIndexCursorPool = x$1;
    }

    public void assertAllReleased()
    {
        Seq resourceLeaks = (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Option[]{reportLeak$1( this.nodeCursorPool(), "nodeCursorPool" ),
                        reportLeak$1( this.relationshipGroupCursorPool(), "relationshipGroupCursorPool" ),
                        reportLeak$1( this.relationshipTraversalCursorPool(), "relationshipTraversalCursorPool" ),
                        reportLeak$1( this.relationshipScanCursorPool(), "relationshipScanCursorPool" ),
                        reportLeak$1( this.nodeValueIndexCursorPool(), "nodeValueIndexCursorPool" ),
                        reportLeak$1( this.nodeLabelIndexCursorPool(), "nodeLabelIndexCursorPool" )}) )).flatten( ( xo ) -> {
        return scala.Option..MODULE$.option2Iterable( xo );
    } ); if ( resourceLeaks.nonEmpty() )
    {
        throw new RuntimeResourceLeakException(
                resourceLeaks.mkString( "Cursors are live even though all cursors should have been released\n  ", "\n  ", "\n" ) );
    }
    }
}
