package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.Worker;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerResourceProvider;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.cypher.internal.v4_0.util.AssertionRunner;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CallingThreadExecutingQuery extends ExecutingQuery
{
    private final Worker worker;
    private final QueryResources workerResources;

    public CallingThreadExecutingQuery( final ExecutionState executionState, final QueryContext queryContext, final QueryState queryState,
            final QueryExecutionTracer queryExecutionTracer, final WorkersQueryProfiler workersQueryProfiler, final Worker worker,
            final WorkerResourceProvider workerResourceProvider, final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy )
    {
        super( executionState, queryContext, queryState, queryExecutionTracer, workersQueryProfiler, workerResourceProvider, executionGraphSchedulingPolicy );
        this.worker = worker;
        this.workerResources = super.workerResourceProvider().resourcesForWorker( worker.workerId() );
    }

    private QueryResources workerResources()
    {
        return this.workerResources;
    }

    public void request( final long numberOfRecords )
    {
        super.request( numberOfRecords );

        while ( !super.executionState().hasEnded() && this.flowControl().hasDemand() )
        {
            boolean worked = this.worker.workOnQuery( this, this.workerResources() );
            if ( !worked && !super.executionState().hasEnded() && this.flowControl().hasDemand() )
            {
                return;
            }
        }
    }

    public void cancel()
    {
        boolean hasEnded = super.executionState().hasEnded();
        this.flowControl().cancel();
        if ( !hasEnded )
        {
            super.executionState().cancelQuery( this.workerResources() );
        }

        try
        {
            if ( AssertionRunner.isAssertionsEnabled() )
            {
                this.worker.assertIsNotActive();
                super.workerResourceProvider().assertAllReleased();
            }
        }
        finally
        {
            super.workerResourceProvider().shutdown();
        }
    }

    public boolean await()
    {
        if ( super.executionState().hasEnded() )
        {
            try
            {
                if ( AssertionRunner.isAssertionsEnabled() )
                {
                    this.worker.assertIsNotActive();
                    super.workerResourceProvider().assertAllReleased();
                }
            }
            finally
            {
                super.workerResourceProvider().shutdown();
            }
        }

        return super.await();
    }
}
