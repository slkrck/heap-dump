package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SchedulingPolicy
{
    ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy( final ExecutionGraphDefinition executionGraphDefinition );
}
