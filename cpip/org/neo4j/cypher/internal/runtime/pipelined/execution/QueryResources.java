package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.io.IOUtils;
import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class QueryResources implements AutoCloseable
{
    private final ExpressionCursors expressionCursors;
    private final CursorPools cursorPools;
    private AnyValue[] _expressionVariables;

    public QueryResources( final CursorFactory cursorFactory )
    {
        this.expressionCursors = new ExpressionCursors( cursorFactory );
        this.cursorPools = new CursorPools( cursorFactory );
        this._expressionVariables = new AnyValue[8];
    }

    public ExpressionCursors expressionCursors()
    {
        return this.expressionCursors;
    }

    public CursorPools cursorPools()
    {
        return this.cursorPools;
    }

    private AnyValue[] _expressionVariables()
    {
        return this._expressionVariables;
    }

    private void _expressionVariables_$eq( final AnyValue[] x$1 )
    {
        this._expressionVariables = x$1;
    }

    public AnyValue[] expressionVariables( final int nExpressionSlots )
    {
        if ( this._expressionVariables().length < nExpressionSlots )
        {
            this._expressionVariables_$eq( new AnyValue[nExpressionSlots] );
        }

        return this._expressionVariables();
    }

    public void setKernelTracer( final KernelReadTracer tracer )
    {
        this.expressionCursors().nodeCursor().setTracer( tracer );
        this.expressionCursors().relationshipScanCursor().setTracer( tracer );
        this.expressionCursors().propertyCursor().setTracer( tracer );
        this.cursorPools().setKernelTracer( tracer );
    }

    public void close()
    {
        IOUtils.closeAll( new AutoCloseable[]{this.expressionCursors(), this.cursorPools()} );
    }
}
