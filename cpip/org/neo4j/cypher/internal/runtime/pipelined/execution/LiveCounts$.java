package org.neo4j.cypher.internal.runtime.pipelined.execution;

public final class LiveCounts$
{
    public static LiveCounts$ MODULE$;

    static
    {
        new LiveCounts$();
    }

    private LiveCounts$()
    {
        MODULE$ = this;
    }

    public long $lessinit$greater$default$1()
    {
        return 0L;
    }

    public long $lessinit$greater$default$2()
    {
        return 0L;
    }

    public long $lessinit$greater$default$3()
    {
        return 0L;
    }

    public long $lessinit$greater$default$4()
    {
        return 0L;
    }

    public long $lessinit$greater$default$5()
    {
        return 0L;
    }

    public long $lessinit$greater$default$6()
    {
        return 0L;
    }
}
