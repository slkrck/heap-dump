package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.MemoryTracking;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.debug.DebugLog.;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerManagement;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerResourceProvider;
import org.neo4j.cypher.internal.runtime.pipelined.state.ConcurrentStateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker;
import org.neo4j.cypher.internal.runtime.pipelined.state.TheExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer;
import org.neo4j.cypher.internal.v4_0.util.AssertionRunner;
import org.neo4j.cypher.result.QueryProfile;
import org.neo4j.exceptions.RuntimeUnsupportedException;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class FixedWorkersQueryExecutor implements QueryExecutor
{
    private final WorkerResourceProvider workerResourceProvider;
    private final WorkerManagement workerManager;

    public FixedWorkersQueryExecutor( final WorkerResourceProvider workerResourceProvider, final WorkerManagement workerManager )
    {
        this.workerResourceProvider = workerResourceProvider;
        this.workerManager = workerManager;
    }

    public WorkerResourceProvider workerResourceProvider()
    {
        return this.workerResourceProvider;
    }

    public WorkerManagement workerManager()
    {
        return this.workerManager;
    }

    public void assertAllReleased()
    {
        AssertionRunner.runUnderAssertion( () -> {
            this.workerResourceProvider().assertAllReleased();
            this.workerManager().assertNoWorkerIsActive();
        } );
    }

    public <E extends Exception> ProfiledQuerySubscription execute( final IndexedSeq<ExecutablePipeline> executablePipelines,
            final ExecutionGraphDefinition executionGraphDefinition, final InputDataStream inputDataStream, final QueryContext queryContext,
            final AnyValue[] params, final SchedulerTracer schedulerTracer, final IndexReadSession[] queryIndexes, final int nExpressionSlots,
            final boolean prePopulateResults, final QuerySubscriber subscriber, final boolean doProfile, final int morselSize,
            final MemoryTracking memoryTracking, final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy )
    {
        if ( queryContext.transactionalContext().dataRead().transactionStateHasChanges() )
        {
            throw new RuntimeUnsupportedException(
                    "The parallel runtime is not supported if there are changes in the transaction state. Use another runtime." );
        }
        else
        {
         .MODULE$.log( "FixedWorkersQueryExecutor.execute()" );
            ConcurrentStateFactory stateFactory = new ConcurrentStateFactory();
            QueryExecutionTracer tracer = schedulerTracer.traceQuery();
            QueryCompletionTracker tracker = stateFactory.newTracker( subscriber, queryContext, tracer );
            QueryState queryState =
                    new QueryState( params, subscriber, tracker, morselSize, queryIndexes, this.workerManager().numberOfWorkers(), nExpressionSlots,
                            prePopulateResults, doProfile, inputDataStream );
            QueryResources initializationResources = this.workerResourceProvider().resourcesForWorker( 0 );
            TheExecutionState executionState =
                    new TheExecutionState( executionGraphDefinition, executablePipelines, stateFactory, this.workerManager(), queryContext, queryState,
                            initializationResources, tracker );
            Tuple2 var10000;
            if ( doProfile )
            {
                FixedWorkersQueryProfiler profiler =
                        new FixedWorkersQueryProfiler( this.workerManager().numberOfWorkers(), executionGraphDefinition.applyRhsPlans() );
                var10000 = new Tuple2( profiler, profiler.Profile() );
            }
            else
            {
                var10000 = new Tuple2( WorkersQueryProfiler.NONE$.MODULE$, QueryProfile.NONE );
            }

            Tuple2 var23 = var10000;
            if ( var23 != null )
            {
                WorkersQueryProfiler workersProfiler = (WorkersQueryProfiler) var23._1();
                QueryProfile queryProfile = (QueryProfile) var23._2();
                Tuple2 var15 = new Tuple2( workersProfiler, queryProfile );
                WorkersQueryProfiler workersProfiler = (WorkersQueryProfiler) var15._1();
                QueryProfile queryProfile = (QueryProfile) var15._2();
                ExecutingQuery executingQuery =
                        new ExecutingQuery( executionState, queryContext, queryState, tracer, workersProfiler, this.workerResourceProvider(),
                                executionGraphSchedulingPolicy );
                queryContext.transactionalContext().transaction().freezeLocks();
                executionState.initializeState();
                this.workerManager().queryManager().addQuery( executingQuery );
                return new ProfiledQuerySubscription( executingQuery, queryProfile, stateFactory.memoryTracker() );
            }
            else
            {
                throw new MatchError( var23 );
            }
        }
    }
}
