package org.neo4j.cypher.internal.runtime.pipelined.execution;

import java.util.BitSet;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.collection.mutable.ArrayOps.ofLong;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.RichInt.;

@JavaDocToJava
public class FilteringMorselExecutionContext extends MorselExecutionContext
{
    private BitSet cancelledRows;

    public FilteringMorselExecutionContext( final Morsel morsel, final SlotConfiguration slots, final int maxNumberOfRows, final int initialCurrentRow,
            final int initialStartRow, final int initialEndRow, final WorkUnitEvent producingWorkUnitEvent, final BitSet initialCancelledRows )
    {
        super( morsel, slots, maxNumberOfRows, initialCurrentRow, initialStartRow, initialEndRow, producingWorkUnitEvent );
        if ( initialCancelledRows != null )
        {
            this.cancelledRows_$eq( (BitSet) initialCancelledRows.clone() );
        }
    }

    public static BitSet $lessinit$greater$default$8()
    {
        return FilteringMorselExecutionContext$.MODULE$.$lessinit$greater$default$8();
    }

    public static WorkUnitEvent $lessinit$greater$default$7()
    {
        return FilteringMorselExecutionContext$.MODULE$.$lessinit$greater$default$7();
    }

    public static FilteringMorselExecutionContext apply( final MorselExecutionContext source )
    {
        return FilteringMorselExecutionContext$.MODULE$.apply( var0 );
    }

    private BitSet cancelledRows()
    {
        return this.cancelledRows;
    }

    private void cancelledRows_$eq( final BitSet x$1 )
    {
        this.cancelledRows = x$1;
    }

    private void ensureCancelledRows()
    {
        if ( this.cancelledRows() == null )
        {
            this.cancelledRows_$eq( new BitSet( super.maxNumberOfRows() ) );
        }
    }

    public void cropCancelledRows()
    {
        if ( this.cancelledRows() != null )
        {
            if ( this.startRow() > 0 )
            {
                this.cancelledRows().clear( 0, this.startRow() );
            }

            if ( this.endRow() < super.maxNumberOfRows() )
            {
                this.cancelledRows().clear( this.endRow(), super.maxNumberOfRows() );
            }
        }
    }

    public boolean isCancelled( final int row )
    {
        return this.cancelledRows() != null && this.cancelledRows().get( row );
    }

    public int numberOfCancelledRows()
    {
        return this.cancelledRows() == null ? 0 : this.cancelledRows().cardinality();
    }

    public boolean hasCancelledRows()
    {
        return this.cancelledRows() != null && !this.cancelledRows().isEmpty();
    }

    public void cancelRow( final int row )
    {
        this.ensureCancelledRows();
        this.cancelledRows().set( row );
    }

    public void cancelCurrentRow()
    {
        this.cancelRow( this.getCurrentRow() );
    }

    public void cancelRows( final BitSet cancelledRows )
    {
        this.ensureCancelledRows();
        this.cancelledRows().or( cancelledRows );
    }

    public FilteringMorselExecutionContext shallowCopy()
    {
        return new FilteringMorselExecutionContext( super.morsel(), super.slots(), super.maxNumberOfRows(), this.currentRow(), this.startRow(), this.endRow(),
                (WorkUnitEvent) null, this.cancelledRows() );
    }

    public void moveToNextRow()
    {
        do
        {
            this.currentRow_$eq( this.currentRow() + 1 );
        }
        while ( this.isCancelled( this.currentRow() ) );
    }

    public int getValidRows()
    {
        return this.numberOfRows() - this.numberOfCancelledRows();
    }

    public int getFirstRow()
    {
        int firstRow;
        for ( firstRow = this.startRow(); this.isCancelled( firstRow ) && firstRow < this.endRow(); ++firstRow )
        {
        }

        return firstRow;
    }

    public int getLastRow()
    {
        int lastRow;
        for ( lastRow = this.endRow() - 1; this.isCancelled( lastRow ) && lastRow > this.startRow(); --lastRow )
        {
        }

        return lastRow;
    }

    public void resetToFirstRow()
    {
        this.currentRow_$eq( this.getFirstRow() );
    }

    public void resetToBeforeFirstRow()
    {
        this.currentRow_$eq( this.getFirstRow() - 1 );
    }

    public void setToAfterLastRow()
    {
        this.currentRow_$eq( this.getLastRow() + 1 );
    }

    public boolean isValidRow()
    {
        return this.currentRow() >= this.startRow() && this.currentRow() < this.endRow() && !this.isCancelled( this.currentRow() );
    }

    public boolean hasNextRow()
    {
        int nextRow = this.currentRow() + 1;
        return nextRow < this.endRow() && (!this.isCancelled( nextRow ) || nextRow < this.getLastRow());
    }

    public MorselExecutionContext view( final int start, final int end )
    {
        FilteringMorselExecutionContext view = this.shallowCopy();
        view.startRow_$eq( start );
        view.currentRow_$eq( start );
        view.endRow_$eq( end );
        view.cropCancelledRows();
        return view;
    }

    public void compactRowsFrom( final MorselExecutionContext input )
    {
        super.compactRowsFrom( input );
        if ( this.cancelledRows() != null )
        {
            this.endRow_$eq( this.endRow() - (this.numberOfRows() - input.numberOfRows()) );
            this.cancelledRows_$eq( (BitSet) null );
        }
    }

    public void finishedWritingUsing( final MorselExecutionContext otherContext )
    {
        int oldEndRow = this.endRow();
        super.finishedWritingUsing( otherContext );
        if ( this.cancelledRows() != null )
        {
            this.cancelledRows().clear( otherContext.currentRow(), oldEndRow );
        }
    }

    public String toString()
    {
        return (new StringBuilder( 104 )).append( "FilteringMorselExecutionContext[0x" ).append(.MODULE$.toHexString$extension(
                scala.Predef..MODULE$.intWrapper( System.identityHashCode( this ) ))).
        append( "](longsPerRow=" ).append( this.longsPerRow() ).append( ", refsPerRow=" ).append( this.refsPerRow() ).append( ", maxRows=" ).append(
                super.maxNumberOfRows() ).append( ", currentRow=" ).append( this.currentRow() ).append( " startRow=" ).append( this.startRow() ).append(
                " endRow=" ).append( this.endRow() ).append( " " ).append( this.prettyCurrentRow() ).append( ")" ).toString();
    }

    public String prettyCurrentRow()
    {
        String var10000;
        if ( super.isValidRow() )
        {
            String cancelled = this.isCancelled( this.currentRow() ) ? "<Cancelled>" : "";
            var10000 = (new StringBuilder( 8 )).append( "longs: " ).append( (new ofLong( scala.Predef..MODULE$.longArrayOps(
                    (long[]) (new ofLong( scala.Predef..MODULE$.longArrayOps( super.morsel().longs() )) ).slice( this.currentRow() * this.longsPerRow(),
                    (this.currentRow() + 1) * this.longsPerRow() )) )).mkString( "[", ", ", "]" )).append( " " ).append(
                (new StringBuilder( 7 )).append( "refs: " ).append( (new ofRef( scala.Predef..MODULE$.refArrayOps(
                        (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) super.morsel().refs() )) ).slice(
                        this.currentRow() * this.refsPerRow(), (this.currentRow() + 1) * this.refsPerRow() )) ) ).mkString( "[", ", ", "]" )).
            append( " " ).append( cancelled ).toString()).toString();
        }
        else
        {
            var10000 = "<Invalid row>";
        }

        return var10000;
    }

    public void addPrettyRowMarker( final scala.collection.mutable.StringBuilder sb, final int row )
    {
        if ( this.isCancelled( row ) )
        {
            sb.$plus$plus$eq( " <Cancelled>" );
        }
    }

    public final void moveToNextRawRow()
    {
        this.currentRow_$eq( this.currentRow() + 1 );
    }

    public final void moveToRawRow( final int row )
    {
        this.currentRow_$eq( row );
    }

    public final void resetToFirstRawRow()
    {
        super.resetToFirstRow();
    }

    public final boolean isValidRawRow()
    {
        return super.isValidRow();
    }
}
