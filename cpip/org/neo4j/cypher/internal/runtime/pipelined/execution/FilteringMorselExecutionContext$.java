package org.neo4j.cypher.internal.runtime.pipelined.execution;

import java.util.BitSet;

import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;

public final class FilteringMorselExecutionContext$
{
    public static FilteringMorselExecutionContext$ MODULE$;

    static
    {
        new FilteringMorselExecutionContext$();
    }

    private FilteringMorselExecutionContext$()
    {
        MODULE$ = this;
    }

    public WorkUnitEvent $lessinit$greater$default$7()
    {
        return null;
    }

    public BitSet $lessinit$greater$default$8()
    {
        return null;
    }

    public FilteringMorselExecutionContext apply( final MorselExecutionContext source )
    {
        return new FilteringMorselExecutionContext( source.morsel(), source.slots(), source.maxNumberOfRows(), source.currentRow(), source.startRow(),
                source.endRow(), source.producingWorkUnitEvent(), this.$lessinit$greater$default$8() );
    }
}
