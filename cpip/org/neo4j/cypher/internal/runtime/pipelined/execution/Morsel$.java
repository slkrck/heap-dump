package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.values.AnyValue;

public final class Morsel$
{
    public static Morsel$ MODULE$;

    static
    {
        new Morsel$();
    }

    private Morsel$()
    {
        MODULE$ = this;
    }

    public Morsel create( final SlotConfiguration slots, final int size )
    {
        long[] longs = new long[slots.numberOfLongs() * size];
        AnyValue[] refs = new AnyValue[slots.numberOfReferences() * size];
        return new Morsel( longs, refs );
    }
}
