package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.result.QueryProfile;
import org.neo4j.kernel.impl.query.QuerySubscription;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class ProfiledQuerySubscription$ extends AbstractFunction3<QuerySubscription,QueryProfile,QueryMemoryTracker,ProfiledQuerySubscription>
        implements Serializable
{
    public static ProfiledQuerySubscription$ MODULE$;

    static
    {
        new ProfiledQuerySubscription$();
    }

    private ProfiledQuerySubscription$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ProfiledQuerySubscription";
    }

    public ProfiledQuerySubscription apply( final QuerySubscription subscription, final QueryProfile profile, final QueryMemoryTracker memoryTracker )
    {
        return new ProfiledQuerySubscription( subscription, profile, memoryTracker );
    }

    public Option<Tuple3<QuerySubscription,QueryProfile,QueryMemoryTracker>> unapply( final ProfiledQuerySubscription x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.subscription(), x$0.profile(), x$0.memoryTracker() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
