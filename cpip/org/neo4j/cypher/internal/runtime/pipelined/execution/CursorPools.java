package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.NodeLabelIndexCursor;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.RelationshipGroupCursor;
import org.neo4j.internal.kernel.api.RelationshipIndexCursor;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.RelationshipTraversalCursor;
import org.neo4j.io.IOUtils;
import scala.reflect.ScalaSignature;
import scala.runtime.Nothing.;

@JavaDocToJava
public class CursorPools implements CursorFactory, AutoCloseable
{
    private final CursorFactory cursorFactory;
    private final CursorPool<NodeCursor> nodeCursorPool;
    private final CursorPool<RelationshipGroupCursor> relationshipGroupCursorPool;
    private final CursorPool<RelationshipTraversalCursor> relationshipTraversalCursorPool;
    private final CursorPool<RelationshipScanCursor> relationshipScanCursorPool;
    private final CursorPool<NodeValueIndexCursor> nodeValueIndexCursorPool;
    private final CursorPool<NodeLabelIndexCursor> nodeLabelIndexCursorPool;

    private fail( final String cursor )
    {
        throw new IllegalStateException( (new StringBuilder( 44 )).append( "This cursor pool doesn't support allocating " ).append( cursor ).toString() );
    }

    public CursorPools( final CursorFactory cursorFactory )
    {
        this.cursorFactory = cursorFactory;
        this.nodeCursorPool = CursorPool$.MODULE$.apply( () -> {
            return this.cursorFactory.allocateNodeCursor();
        } );
        this.relationshipGroupCursorPool = CursorPool$.MODULE$.apply( () -> {
            return this.cursorFactory.allocateRelationshipGroupCursor();
        } );
        this.relationshipTraversalCursorPool = CursorPool$.MODULE$.apply( () -> {
            return this.cursorFactory.allocateRelationshipTraversalCursor();
        } );
        this.relationshipScanCursorPool = CursorPool$.MODULE$.apply( () -> {
            return this.cursorFactory.allocateRelationshipScanCursor();
        } );
        this.nodeValueIndexCursorPool = CursorPool$.MODULE$.apply( () -> {
            return this.cursorFactory.allocateNodeValueIndexCursor();
        } );
        this.nodeLabelIndexCursorPool = CursorPool$.MODULE$.apply( () -> {
            return this.cursorFactory.allocateNodeLabelIndexCursor();
        } );
    }

    public CursorPool<NodeCursor> nodeCursorPool()
    {
        return this.nodeCursorPool;
    }

    public CursorPool<RelationshipGroupCursor> relationshipGroupCursorPool()
    {
        return this.relationshipGroupCursorPool;
    }

    public CursorPool<RelationshipTraversalCursor> relationshipTraversalCursorPool()
    {
        return this.relationshipTraversalCursorPool;
    }

    public CursorPool<RelationshipScanCursor> relationshipScanCursorPool()
    {
        return this.relationshipScanCursorPool;
    }

    public CursorPool<NodeValueIndexCursor> nodeValueIndexCursorPool()
    {
        return this.nodeValueIndexCursorPool;
    }

    public CursorPool<NodeLabelIndexCursor> nodeLabelIndexCursorPool()
    {
        return this.nodeLabelIndexCursorPool;
    }

    public void setKernelTracer( final KernelReadTracer tracer )
    {
        this.nodeCursorPool().setKernelTracer( tracer );
        this.relationshipGroupCursorPool().setKernelTracer( tracer );
        this.relationshipTraversalCursorPool().setKernelTracer( tracer );
        this.relationshipScanCursorPool().setKernelTracer( tracer );
        this.nodeValueIndexCursorPool().setKernelTracer( tracer );
        this.nodeLabelIndexCursorPool().setKernelTracer( tracer );
    }

    public void close()
    {
        IOUtils.closeAll( (AutoCloseable[]) (new CursorPool[]{this.nodeCursorPool(), this.relationshipGroupCursorPool(), this.relationshipTraversalCursorPool(),
                this.relationshipScanCursorPool(), this.nodeValueIndexCursorPool(), this.nodeLabelIndexCursorPool()}) );
    }

    public void collectLiveCounts( final LiveCounts liveCounts )
    {
        liveCounts.nodeCursorPool_$eq( liveCounts.nodeCursorPool() + this.nodeCursorPool().getLiveCount() );
        liveCounts.relationshipGroupCursorPool_$eq( liveCounts.relationshipGroupCursorPool() + this.relationshipGroupCursorPool().getLiveCount() );
        liveCounts.relationshipTraversalCursorPool_$eq( liveCounts.relationshipTraversalCursorPool() + this.relationshipTraversalCursorPool().getLiveCount() );
        liveCounts.relationshipScanCursorPool_$eq( liveCounts.relationshipScanCursorPool() + this.relationshipScanCursorPool().getLiveCount() );
        liveCounts.nodeValueIndexCursorPool_$eq( liveCounts.nodeValueIndexCursorPool() + this.nodeValueIndexCursorPool().getLiveCount() );
        liveCounts.nodeLabelIndexCursorPool_$eq( liveCounts.nodeLabelIndexCursorPool() + this.nodeLabelIndexCursorPool().getLiveCount() );
    }

    public NodeCursor allocateNodeCursor()
    {
        return (NodeCursor) this.nodeCursorPool().allocate();
    }

    public NodeCursor allocateFullAccessNodeCursor()
    {
        throw this.fail( "FullAccessNodeCursor" );
    }

    public RelationshipScanCursor allocateRelationshipScanCursor()
    {
        return (RelationshipScanCursor) this.relationshipScanCursorPool().allocate();
    }

    public RelationshipScanCursor allocateFullAccessRelationshipScanCursor()
    {
        throw this.fail( "FullAccessRelationshipScanCursor" );
    }

    public RelationshipTraversalCursor allocateRelationshipTraversalCursor()
    {
        return (RelationshipTraversalCursor) this.relationshipTraversalCursorPool().allocate();
    }

    public PropertyCursor allocatePropertyCursor()
    {
        throw this.fail( "PropertyCursor" );
    }

    public PropertyCursor allocateFullAccessPropertyCursor()
    {
        throw this.fail( "FullAccessPropertyCursor" );
    }

    public RelationshipGroupCursor allocateRelationshipGroupCursor()
    {
        return (RelationshipGroupCursor) this.relationshipGroupCursorPool().allocate();
    }

    public NodeValueIndexCursor allocateNodeValueIndexCursor()
    {
        return (NodeValueIndexCursor) this.nodeValueIndexCursorPool().allocate();
    }

    public NodeLabelIndexCursor allocateNodeLabelIndexCursor()
    {
        return (NodeLabelIndexCursor) this.nodeLabelIndexCursorPool().allocate();
    }

    public RelationshipIndexCursor allocateRelationshipIndexCursor()
    {
        throw this.fail( "RelationshipIndexCursor" );
    }
}
