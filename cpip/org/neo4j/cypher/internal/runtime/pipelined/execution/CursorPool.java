package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.internal.kernel.api.Cursor;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import scala.Function0;
import scala.collection.Seq;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CursorPool<CURSOR extends Cursor> implements AutoCloseable
{
    private final Function0<CURSOR> cursorFactory;
    private CURSOR cached;
    private KernelReadTracer tracer;

    public CursorPool( final Function0<CURSOR> cursorFactory )
    {
        this.cursorFactory = cursorFactory;
    }

    public static <CURSOR extends Cursor> CursorPool<CURSOR> apply( final Function0<CURSOR> cursorFactory )
    {
        return CursorPool$.MODULE$.apply( var0 );
    }

    private CURSOR cached()
    {
        return this.cached;
    }

    private void cached_$eq( final CURSOR x$1 )
    {
        this.cached = x$1;
    }

    private KernelReadTracer tracer()
    {
        return this.tracer;
    }

    private void tracer_$eq( final KernelReadTracer x$1 )
    {
        this.tracer = x$1;
    }

    public void setKernelTracer( final KernelReadTracer tracer )
    {
        this.tracer_$eq( tracer );
    }

    public CURSOR allocateAndTrace()
    {
        Cursor cursor = this.allocateCursor();
        cursor.setTracer( this.tracer() );
        return cursor;
    }

    public CURSOR allocate()
    {
        return this.allocateCursor();
    }

    public void free( final CURSOR cursor )
    {
        if ( cursor != null )
        {
            if (.MODULE$.CURSORS().enabled()){
            .MODULE$.CURSORS().log(
                    this.stackTraceSlice( 4, 5 ).mkString( (new StringBuilder( 16 )).append( "+ free " ).append( cursor ).append( "\n        " ).toString(),
                            "\n        ", "" ) );
        }

            cursor.removeTracer();
            Cursor c = this.cached();
            if ( c != null )
            {
                c.close();
            }

            this.cached_$eq( cursor );
        }
    }

    private final CURSOR allocateCursor()
    {
        if (.MODULE$.CURSORS().enabled()){
         .MODULE$.CURSORS().log( this.stackTraceSlice( 2, 5 ).mkString( "+ allocate\n        ", "\n        ", "" ) );
    }

        Cursor cursor = this.cached();
        if ( cursor != null )
        {
            this.cached_$eq( (Cursor) null );
        }
        else
        {
            cursor = (Cursor) this.cursorFactory.apply();
        }

        return cursor;
    }

    public void close()
    {
        Cursor c = this.cached();
        if ( c != null )
        {
            c.close();
            this.cached_$eq( (Cursor) null );
        }
    }

    public long getLiveCount()
    {
        throw new UnsupportedOperationException( "use TrackingCursorPool" );
    }

    private Seq<String> stackTraceSlice( final int from, final int to )
    {
        return (Seq) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) (new Exception()).getStackTrace() )) ).slice( from, to )))).
        map( ( traceElement ) -> {
            return (new StringBuilder( 4 )).append( "\tat " ).append( traceElement ).toString();
        }, scala.Array..MODULE$.fallbackCanBuildFrom( scala.Predef.DummyImplicit..MODULE$.dummyImplicit()));
    }
}
