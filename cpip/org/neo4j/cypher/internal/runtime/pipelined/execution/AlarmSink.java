package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryStatus;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class AlarmSink<T> implements Sink<T>
{
    private final Sink<T> inner;
    private final WorkerWaker waker;
    private final QueryStatus queryStatus;

    public AlarmSink( final Sink<T> inner, final WorkerWaker waker, final QueryStatus queryStatus )
    {
        this.inner = inner;
        this.waker = waker;
        this.queryStatus = queryStatus;
    }

    public void put( final T t )
    {
        if ( !this.queryStatus.cancelled() )
        {
            this.inner.put( t );
            this.waker.wakeOne();
        }
        else
        {
         .MODULE$.ERROR_HANDLING().log( "Dropped data %s because of query cancellation", t );
        }
    }

    public boolean canPut()
    {
        return this.inner.canPut();
    }
}
