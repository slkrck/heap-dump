package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.TopLevelArgument$;
import org.neo4j.cypher.internal.runtime.EntityById;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ResourceLinenumber;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.slotted.SlottedCompatible;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import org.neo4j.exceptions.InternalException;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.util.Preconditions;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.Option;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.ArrayOps.ofLong;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class MorselExecutionContext implements ExecutionContext, SlottedCompatible
{
    private final Morsel morsel;
    private final SlotConfiguration slots;
    private final int maxNumberOfRows;
    private final WorkUnitEvent producingWorkUnitEvent;
    private final int longsPerRow;
    private final int refsPerRow;
    private int currentRow;
    private int startRow;
    private int endRow;
    private MorselExecutionContext attachedMorsel;
    private Option<ResourceLinenumber> org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber;

    {
        throw new InternalException( "Tried using a wrong context." );
    }

    public MorselExecutionContext( final Morsel morsel, final SlotConfiguration slots, final int maxNumberOfRows, final int currentRow, final int startRow,
            final int endRow, final WorkUnitEvent producingWorkUnitEvent )
    {
        this.morsel = morsel;
        this.slots = slots;
        this.maxNumberOfRows = maxNumberOfRows;
        this.currentRow = currentRow;
        this.startRow = startRow;
        this.endRow = endRow;
        this.producingWorkUnitEvent = producingWorkUnitEvent;
        super();
        ExecutionContext.$init$( this );
        this.longsPerRow = slots.numberOfLongs();
        this.refsPerRow = slots.numberOfReferences();
    }

    public static WorkUnitEvent $lessinit$greater$default$7()
    {
        return MorselExecutionContext$.MODULE$.$lessinit$greater$default$7();
    }

    public static int $lessinit$greater$default$6()
    {
        return MorselExecutionContext$.MODULE$.$lessinit$greater$default$6();
    }

    public static int $lessinit$greater$default$5()
    {
        return MorselExecutionContext$.MODULE$.$lessinit$greater$default$5();
    }

    public static int $lessinit$greater$default$4()
    {
        return MorselExecutionContext$.MODULE$.$lessinit$greater$default$4();
    }

    public static FilteringMorselExecutionContext createInitialRow()
    {
        return MorselExecutionContext$.MODULE$.createInitialRow();
    }

    public static MorselExecutionContext empty()
    {
        return MorselExecutionContext$.MODULE$.empty();
    }

    public static MorselExecutionContext apply( final Morsel morsel, final SlotConfiguration slots, final int maxNumberOfRows )
    {
        return MorselExecutionContext$.MODULE$.apply( var0, var1, var2 );
    }

    public Option<ResourceLinenumber> org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber()
    {
        return this.org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber;
    }

    public void org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber_$eq( final Option<ResourceLinenumber> x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber = x$1;
    }

    public final Morsel morsel()
    {
        return this.morsel;
    }

    public final SlotConfiguration slots()
    {
        return this.slots;
    }

    public final int maxNumberOfRows()
    {
        return this.maxNumberOfRows;
    }

    public int currentRow()
    {
        return this.currentRow;
    }

    public void currentRow_$eq( final int x$1 )
    {
        this.currentRow = x$1;
    }

    public int startRow()
    {
        return this.startRow;
    }

    public void startRow_$eq( final int x$1 )
    {
        this.startRow = x$1;
    }

    public int endRow()
    {
        return this.endRow;
    }

    public void endRow_$eq( final int x$1 )
    {
        this.endRow = x$1;
    }

    public final WorkUnitEvent producingWorkUnitEvent()
    {
        return this.producingWorkUnitEvent;
    }

    public final int longsPerRow()
    {
        return this.longsPerRow;
    }

    public final int refsPerRow()
    {
        return this.refsPerRow;
    }

    private MorselExecutionContext attachedMorsel()
    {
        return this.attachedMorsel;
    }

    private void attachedMorsel_$eq( final MorselExecutionContext x$1 )
    {
        this.attachedMorsel = x$1;
    }

    public void attach( final MorselExecutionContext morsel )
    {
        Preconditions.checkState( this.attachedMorsel() == null, "Cannot override existing MorselExecutionContext.attachment." );
        this.attachedMorsel_$eq( morsel );
    }

    public MorselExecutionContext detach()
    {
        Preconditions.checkState( this.attachedMorsel() != null, "Cannot detach if no attachment available." );
        MorselExecutionContext temp = this.attachedMorsel();
        this.attachedMorsel_$eq( (MorselExecutionContext) null );
        return temp;
    }

    public MorselExecutionContext shallowCopy()
    {
        return new MorselExecutionContext( this.morsel(), this.slots(), this.maxNumberOfRows(), this.currentRow(), this.startRow(), this.endRow(),
                MorselExecutionContext$.MODULE$.$lessinit$greater$default$7() );
    }

    public int numberOfRows()
    {
        return this.endRow() - this.startRow();
    }

    public void moveToNextRow()
    {
        this.currentRow_$eq( this.currentRow() + 1 );
    }

    public int getValidRows()
    {
        return this.numberOfRows();
    }

    public int getFirstRow()
    {
        return this.startRow();
    }

    public int getLastRow()
    {
        return this.endRow() - 1;
    }

    public int getCurrentRow()
    {
        return this.currentRow();
    }

    public void setCurrentRow( final int row )
    {
        this.currentRow_$eq( row );
    }

    public int getLongsPerRow()
    {
        return this.longsPerRow();
    }

    public int getRefsPerRow()
    {
        return this.refsPerRow();
    }

    public void resetToFirstRow()
    {
        this.currentRow_$eq( this.startRow() );
    }

    public void resetToBeforeFirstRow()
    {
        this.currentRow_$eq( this.startRow() - 1 );
    }

    public void setToAfterLastRow()
    {
        this.currentRow_$eq( this.endRow() );
    }

    public boolean isValidRow()
    {
        return this.currentRow() >= this.startRow() && this.currentRow() < this.endRow();
    }

    public boolean hasNextRow()
    {
        return this.currentRow() < this.endRow() - 1;
    }

    public boolean hasData()
    {
        return this.getValidRows() > 0;
    }

    public boolean isEmpty()
    {
        return !this.hasData();
    }

    public void finishedWriting()
    {
        this.endRow_$eq( this.currentRow() );
    }

    public void finishedWritingUsing( final MorselExecutionContext otherContext )
    {
        if ( this.startRow() != otherContext.startRow() )
        {
            throw new IllegalStateException( "Cannot write to a context from a context with a different first row." );
        }
        else
        {
            this.endRow_$eq( otherContext.currentRow() );
        }
    }

    public MorselExecutionContext view( final int start, final int end )
    {
        MorselExecutionContext view = this.shallowCopy();
        view.startRow_$eq( start );
        view.currentRow_$eq( start );
        view.endRow_$eq( end );
        return view;
    }

    public void compactRowsFrom( final MorselExecutionContext input )
    {
      .MODULE$. assert (!(input instanceof FilteringMorselExecutionContext) && this.numberOfRows() >= input.numberOfRows());
        if ( this.longsPerRow() > 0 )
        {
            System.arraycopy( input.morsel().longs(), input.startRow() * input.longsPerRow(), this.morsel().longs(), this.startRow() * this.longsPerRow(),
                    input.numberOfRows() * this.longsPerRow() );
        }

        if ( this.refsPerRow() > 0 )
        {
            System.arraycopy( input.morsel().refs(), input.startRow() * input.refsPerRow(), this.morsel().refs(), this.startRow() * this.refsPerRow(),
                    input.numberOfRows() * this.refsPerRow() );
        }
    }

    public void copyTo( final ExecutionContext target, final int sourceLongOffset, final int sourceRefOffset, final int targetLongOffset,
            final int targetRefOffset )
    {
        BoxedUnit var6;
        if ( target instanceof MorselExecutionContext )
        {
            MorselExecutionContext var8 = (MorselExecutionContext) target;
            System.arraycopy( this.morsel().longs(), this.longsAtCurrentRow() + sourceLongOffset, var8.morsel().longs(),
                    var8.longsAtCurrentRow() + targetLongOffset, this.longsPerRow() - sourceLongOffset );
            System.arraycopy( this.morsel().refs(), this.refsAtCurrentRow() + sourceRefOffset, var8.morsel().refs(), var8.refsAtCurrentRow() + targetRefOffset,
                    this.refsPerRow() - sourceRefOffset );
            var6 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !(target instanceof SlottedExecutionContext) )
            {
                throw new MatchError( target );
            }

            SlottedExecutionContext var9 = (SlottedExecutionContext) target;
            System.arraycopy( this.morsel().longs(), this.longsAtCurrentRow() + sourceLongOffset, var9.longs(), targetLongOffset,
                    this.longsPerRow() - sourceLongOffset );
            System.arraycopy( this.morsel().refs(), this.refsAtCurrentRow() + sourceRefOffset, var9.refs(), targetRefOffset,
                    this.refsPerRow() - sourceRefOffset );
            var6 = BoxedUnit.UNIT;
        }
    }

    public int copyTo$default$2()
    {
        return 0;
    }

    public int copyTo$default$3()
    {
        return 0;
    }

    public int copyTo$default$4()
    {
        return 0;
    }

    public int copyTo$default$5()
    {
        return 0;
    }

    public void copyFrom( final ExecutionContext input, final int nLongs, final int nRefs )
    {
        BoxedUnit var4;
        if ( input instanceof MorselExecutionContext )
        {
            MorselExecutionContext var6 = (MorselExecutionContext) input;
            if ( nLongs > this.longsPerRow() || nRefs > this.refsPerRow() )
            {
                throw new InternalException( "A bug has occurred in the morsel runtime: The target morsel execution context cannot hold the data to copy." );
            }

            System.arraycopy( var6.morsel().longs(), var6.longsAtCurrentRow(), this.morsel().longs(), this.longsAtCurrentRow(), nLongs );
            System.arraycopy( var6.morsel().refs(), var6.refsAtCurrentRow(), this.morsel().refs(), this.refsAtCurrentRow(), nRefs );
            var4 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !(input instanceof SlottedExecutionContext) )
            {
                throw this.fail();
            }

            SlottedExecutionContext var7 = (SlottedExecutionContext) input;
            if ( nLongs > this.longsPerRow() || nRefs > this.refsPerRow() )
            {
                throw new InternalException( "A bug has occurred in the morsel runtime: The target morsel execution context cannot hold the data to copy." );
            }

            System.arraycopy( var7.longs(), 0, this.morsel().longs(), this.longsAtCurrentRow(), nLongs );
            System.arraycopy( var7.refs(), 0, this.morsel().refs(), this.refsAtCurrentRow(), nRefs );
            var4 = BoxedUnit.UNIT;
        }
    }

    public void copyToSlottedExecutionContext( final SlottedExecutionContext other, final int nLongs, final int nRefs )
    {
        System.arraycopy( this.morsel().longs(), this.longsAtCurrentRow(), other.longs(), 0, nLongs );
        System.arraycopy( this.morsel().refs(), this.refsAtCurrentRow(), other.refs(), 0, nRefs );
    }

    public String toString()
    {
        return (new StringBuilder( 95 )).append( "MorselExecutionContext[0x" ).append( scala.runtime.RichInt..MODULE$.toHexString$extension(.MODULE$.intWrapper(
                System.identityHashCode( this ) ))).
        append( "](longsPerRow=" ).append( this.longsPerRow() ).append( ", refsPerRow=" ).append( this.refsPerRow() ).append( ", maxRows=" ).append(
                this.maxNumberOfRows() ).append( ", currentRow=" ).append( this.currentRow() ).append( " startRow=" ).append( this.startRow() ).append(
                " endRow=" ).append( this.endRow() ).append( " " ).append( this.prettyCurrentRow() ).append( ")" ).toString();
    }

    public String prettyCurrentRow()
    {
        return this.isValidRow() ? (new StringBuilder( 8 )).append( "longs: " ).append(
                (new ofLong(.MODULE$.longArrayOps( (long[]) (new ofLong(.MODULE$.longArrayOps( this.morsel().longs() )) ).slice(
                        this.currentRow() * this.longsPerRow(), (this.currentRow() + 1) * this.longsPerRow() )) )).mkString( "[", ", ", "]" )).
        append( " " ).append( (new StringBuilder( 6 )).append( "refs: " ).append(
                (new ofRef(.MODULE$.refArrayOps( (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) this.morsel().refs() )) ).slice(
                        this.currentRow() * this.refsPerRow(), (this.currentRow() + 1) * this.refsPerRow() )) ) ).mkString( "[", ", ", "]" )).toString()).
        toString() :"<Invalid row>";
    }

    public Seq<String> prettyString()
    {
        String[] longStrings = (String[]) (new ofLong(.MODULE$.longArrayOps( (long[]) (new ofLong(.MODULE$.longArrayOps( this.morsel().longs() )) ).slice(
                this.startRow() * this.longsPerRow(), this.numberOfRows() * this.longsPerRow() )))).map( ( x$1 ) -> {
        return $anonfun$prettyString$1( BoxesRunTime.unboxToLong( x$1 ) );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( String.class )));
        String[] refStrings =
                (String[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) this.morsel().refs() )) ).slice(
                        this.startRow() * this.refsPerRow(), this.numberOfRows() * this.refsPerRow() )))).map( ( x$1 ) -> {
        return String.valueOf( x$1 );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( String.class )));
        int[] longWidths = this.widths$1( longStrings, this.longsPerRow() );
        int[] refWidths = this.widths$1( refStrings, this.refsPerRow() );
        ArrayBuffer rows = new ArrayBuffer();
        scala.collection.mutable.StringBuilder sb = new scala.collection.mutable.StringBuilder();
        scala.runtime.RichInt..MODULE$.until$extension0(.MODULE$.intWrapper( 0 ), this.numberOfRows() - this.startRow()).foreach$mVc$sp( ( row ) -> {
        sb.$plus$plus$eq( this.startRow() + row == this.currentRow() ? " * " : "   " );
        scala.runtime.RichInt..MODULE$.until$extension0(.MODULE$.intWrapper( 0 ), this.longsPerRow()).foreach( ( col ) -> {
            return $anonfun$prettyString$6( this, longStrings, longWidths, sb, row, BoxesRunTime.unboxToInt( col ) );
        } );
        sb.$plus$eq( ' ' );
        sb.$plus$eq( ' ' );
        scala.runtime.RichInt..MODULE$.until$extension0(.MODULE$.intWrapper( 0 ), this.refsPerRow()).foreach( ( col ) -> {
            return $anonfun$prettyString$7( this, refStrings, refWidths, sb, row, BoxesRunTime.unboxToInt( col ) );
        } );
        this.addPrettyRowMarker( sb, this.startRow() + row );
        rows.$plus$eq( sb.result() );
        sb.clear();
    } ); return rows;
    }

    public void addPrettyRowMarker( final scala.collection.mutable.StringBuilder sb, final int row )
    {
    }

    public void copyFrom( final MorselExecutionContext input )
    {
        this.copyFrom( input, input.longsPerRow(), input.refsPerRow() );
    }

    public void setLongAt( final int offset, final long value )
    {
        this.morsel().longs()[this.currentRow() * this.longsPerRow() + offset] = value;
    }

    public long getLongAt( final int offset )
    {
        return this.getLongAt( this.currentRow(), offset );
    }

    public long getLongAt( final int row, final int offset )
    {
        return this.morsel().longs()[row * this.longsPerRow() + offset];
    }

    public long getArgumentAt( final int offset )
    {
        return offset == TopLevelArgument$.MODULE$.SLOT_OFFSET() ? 0L : this.getLongAt( this.currentRow(), offset );
    }

    public void setArgumentAt( final int offset, final long argument )
    {
        if ( offset == TopLevelArgument$.MODULE$.SLOT_OFFSET() )
        {
            TopLevelArgument$.MODULE$.assertTopLevelArgument( argument );
        }
        else
        {
            this.setLongAt( offset, argument );
        }
    }

    public void setRefAt( final int offset, final AnyValue value )
    {
        this.morsel().refs()[this.currentRow() * this.refsPerRow() + offset] = value;
    }

    public AnyValue getRefAt( final int offset )
    {
        return this.morsel().refs()[this.currentRow() * this.refsPerRow() + offset];
    }

    public AnyValue getByName( final String name )
    {
        return (AnyValue) this.slots().maybeGetter( name ).map( ( g ) -> {
            return (AnyValue) g.apply( this );
        } ).getOrElse( () -> {
            throw new NotFoundException( (new StringBuilder( 20 )).append( "Unknown variable `" ).append( name ).append( "`." ).toString() );
        } );
    }

    public boolean containsName( final String name )
    {
        return this.slots().maybeGetter( name ).map( ( g ) -> {
            return (AnyValue) g.apply( this );
        } ).isDefined();
    }

    public int numberOfColumns()
    {
        return this.longsPerRow() + this.refsPerRow();
    }

    public void set( final Seq<Tuple2<String,AnyValue>> newEntries )
    {
        newEntries.foreach( ( x0$1 ) -> {
            $anonfun$set$1( this, x0$1 );
            return BoxedUnit.UNIT;
        } );
    }

    public void set( final String key1, final AnyValue value1 )
    {
        this.setValue( key1, value1 );
    }

    public void set( final String key1, final AnyValue value1, final String key2, final AnyValue value2 )
    {
        this.setValue( key1, value1 );
        this.setValue( key2, value2 );
    }

    public void set( final String key1, final AnyValue value1, final String key2, final AnyValue value2, final String key3, final AnyValue value3 )
    {
        this.setValue( key1, value1 );
        this.setValue( key2, value2 );
        this.setValue( key3, value3 );
    }

    private void setValue( final String key1, final AnyValue value1 )
    {
        ((Function2) this.slots().maybeSetter( key1 ).getOrElse( () -> {
            throw new InternalException(
                    (new StringBuilder( 42 )).append( "Ouch, no suitable slot for key " ).append( key1 ).append( " = " ).append( value1 ).append(
                            "\nSlots: " ).append( this.slots() ).toString() );
        } )).apply( this, value1 );
    }

    public void mergeWith( final ExecutionContext other, final EntityById entityById )
    {
        throw this.fail();
    }

    public ExecutionContext createClone()
    {
        SlottedExecutionContext slottedRow = new SlottedExecutionContext( this.slots() );
        this.copyTo( slottedRow, this.copyTo$default$2(), this.copyTo$default$3(), this.copyTo$default$4(), this.copyTo$default$5() );
        return slottedRow;
    }

    public long estimatedHeapUsage()
    {
        long usage = (long) (this.longsPerRow() * this.maxNumberOfRows()) * 8L;

        for ( int i = this.startRow() * this.refsPerRow(); i < (this.startRow() + this.maxNumberOfRows()) * this.refsPerRow(); ++i )
        {
            AnyValue ref = this.morsel().refs()[i];
            if ( ref != null )
            {
                usage += this.morsel().refs()[i].estimatedHeapUsage();
            }
        }

        return usage;
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1 )
    {
        throw this.fail();
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1, final String key2, final AnyValue value2 )
    {
        throw this.fail();
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1, final String key2, final AnyValue value2, final String key3,
            final AnyValue value3 )
    {
        throw this.fail();
    }

    public ExecutionContext copyWith( final Seq<Tuple2<String,AnyValue>> newEntries )
    {
        throw this.fail();
    }

    public Map<String,AnyValue> boundEntities( final Function1<Object,AnyValue> materializeNode, final Function1<Object,AnyValue> materializeRelationship )
    {
        throw this.fail();
    }

    public boolean isNull( final String key )
    {
        throw this.fail();
    }

    public void setCachedProperty( final ASTCachedProperty key, final Value value )
    {
        throw this.fail();
    }

    public void setCachedPropertyAt( final int offset, final Value value )
    {
        this.setRefAt( offset, value );
    }

    public Value getCachedProperty( final ASTCachedProperty key )
    {
        throw this.fail();
    }

    public Value getCachedPropertyAt( final int offset )
    {
        return (Value) this.getRefAt( offset );
    }

    public void invalidateCachedNodeProperties( final long node )
    {
        this.slots().foreachCachedSlot( ( x0$2 ) -> {
            $anonfun$invalidateCachedNodeProperties$1( this, node, x0$2 );
            return BoxedUnit.UNIT;
        } );
    }

    public void invalidateCachedRelationshipProperties( final long rel )
    {
        this.slots().foreachCachedSlot( ( x0$3 ) -> {
            $anonfun$invalidateCachedRelationshipProperties$1( this, rel, x0$3 );
            return BoxedUnit.UNIT;
        } );
    }

    public void setLinenumber( final String file, final long line, final boolean last )
    {
        throw this.fail();
    }

    public boolean setLinenumber$default$3()
    {
        return false;
    }

    public Option<ResourceLinenumber> getLinenumber()
    {
        throw this.fail();
    }

    public void setLinenumber( final Option<ResourceLinenumber> line )
    {
        throw this.fail();
    }

   private scala.runtime.Nothing.fail()

    public int longsAtCurrentRow()
    {
        return this.currentRow() * this.longsPerRow();
    }

    public int refsAtCurrentRow()
    {
        return this.currentRow() * this.refsPerRow();
    }

    private final int[] widths$1( final String[] strings, final int nCols )
    {
        int[] widths = new int[nCols];
        scala.runtime.RichInt..MODULE$.until$extension0(.MODULE$.intWrapper( 0 ), this.numberOfRows() - this.startRow()).foreach$mVc$sp( ( row ) -> {
        scala.runtime.RichInt..MODULE$.until$extension0(.MODULE$.intWrapper( 0 ), nCols).foreach$mVc$sp( ( col ) -> {
            widths[col] = scala.math.package..MODULE$.max( widths[col], strings[row * nCols + col].length() );
        } );
    } ); return widths;
    }
}
