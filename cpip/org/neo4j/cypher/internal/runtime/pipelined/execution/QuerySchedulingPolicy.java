package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.pipelined.SchedulingResult;
import org.neo4j.cypher.internal.runtime.pipelined.Task;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface QuerySchedulingPolicy
{
    SchedulingResult<Task<QueryResources>> nextTask( final QueryResources queryResources );
}
