package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LazyScheduling
{
    public static ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy( final ExecutionGraphDefinition executionGraphDefinition )
    {
        return LazyScheduling$.MODULE$.executionGraphSchedulingPolicy( var0 );
    }
}
