package org.neo4j.cypher.internal.runtime.pipelined.execution;

public final class WorkersQueryProfiler$
{
    public static WorkersQueryProfiler$ MODULE$;

    static
    {
        new WorkersQueryProfiler$();
    }

    private WorkersQueryProfiler$()
    {
        MODULE$ = this;
    }
}
