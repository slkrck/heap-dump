package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.MemoryTracking;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface QueryExecutor
{
    <E extends Exception> ProfiledQuerySubscription execute( final IndexedSeq<ExecutablePipeline> executablePipelines,
            final ExecutionGraphDefinition executionGraphDefinition, final InputDataStream inputDataStream, final QueryContext queryContext,
            final AnyValue[] params, final SchedulerTracer schedulerTracer, final IndexReadSession[] queryIndexes, final int nExpressionSlots,
            final boolean prePopulateResults, final QuerySubscriber subscriber, final boolean doProfile, final int morselSize,
            final MemoryTracking memoryTracking, final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy );

    void assertAllReleased();
}
