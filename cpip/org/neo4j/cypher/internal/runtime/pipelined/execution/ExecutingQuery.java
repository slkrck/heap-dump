package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerResourceProvider;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.kernel.impl.query.QuerySubscription;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ExecutingQuery implements QuerySubscription
{
    private final ExecutionState executionState;
    private final QueryContext queryContext;
    private final QueryState queryState;
    private final QueryExecutionTracer queryExecutionTracer;
    private final WorkersQueryProfiler workersQueryProfiler;
    private final WorkerResourceProvider workerResourceProvider;
    private final QuerySchedulingPolicy querySchedulingPolicy;
    private final FlowControl flowControl;

    public ExecutingQuery( final ExecutionState executionState, final QueryContext queryContext, final QueryState queryState,
            final QueryExecutionTracer queryExecutionTracer, final WorkersQueryProfiler workersQueryProfiler,
            final WorkerResourceProvider workerResourceProvider, final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy )
    {
        this.executionState = executionState;
        this.queryContext = queryContext;
        this.queryState = queryState;
        this.queryExecutionTracer = queryExecutionTracer;
        this.workersQueryProfiler = workersQueryProfiler;
        this.workerResourceProvider = workerResourceProvider;
        this.querySchedulingPolicy = executionGraphSchedulingPolicy.querySchedulingPolicy( this );
        this.flowControl = queryState.flowControl();
    }

    public void consumeAll() throws Exception
    {
        super.consumeAll();
    }

    public ExecutionState executionState()
    {
        return this.executionState;
    }

    public QueryContext queryContext()
    {
        return this.queryContext;
    }

    public QueryState queryState()
    {
        return this.queryState;
    }

    public QueryExecutionTracer queryExecutionTracer()
    {
        return this.queryExecutionTracer;
    }

    public WorkersQueryProfiler workersQueryProfiler()
    {
        return this.workersQueryProfiler;
    }

    public WorkerResourceProvider workerResourceProvider()
    {
        return this.workerResourceProvider;
    }

    public QuerySchedulingPolicy querySchedulingPolicy()
    {
        return this.querySchedulingPolicy;
    }

    public FlowControl flowControl()
    {
        return this.flowControl;
    }

    public void request( final long numberOfRecords )
    {
        this.flowControl().request( numberOfRecords );
    }

    public void cancel()
    {
        this.flowControl().cancel();
        this.executionState().scheduleCancelQuery();
    }

    public boolean await()
    {
        return this.flowControl().await();
    }

    public String toString()
    {
        return (new StringBuilder( 15 )).append( "ExecutingQuery " ).append( System.identityHashCode( this ) ).toString();
    }
}
