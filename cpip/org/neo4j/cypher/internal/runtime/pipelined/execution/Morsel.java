package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.values.AnyValue;
import scala.collection.mutable.ArrayOps.ofLong;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.RichInt.;

@JavaDocToJava
public class Morsel
{
    private final long[] longs;
    private final AnyValue[] refs;

    public Morsel( final long[] longs, final AnyValue[] refs )
    {
        this.longs = longs;
        this.refs = refs;
    }

    public static Morsel create( final SlotConfiguration slots, final int size )
    {
        return Morsel$.MODULE$.create( var0, var1 );
    }

    public long[] longs()
    {
        return this.longs;
    }

    public AnyValue[] refs()
    {
        return this.refs;
    }

    public String toString()
    {
        return (new StringBuilder( 25 )).append( "Morsel[0x" ).append(.MODULE$.toHexString$extension( scala.Predef..MODULE$.intWrapper(
                System.identityHashCode( this ) ))).
        append( "](longs:" ).append( (new ofLong( scala.Predef..MODULE$.longArrayOps( this.longs() )) ).mkString( ", " )).
        append( ", refs:" ).append( (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.refs() )) ).mkString( ", " )).append( ")" ).toString();
    }
}
