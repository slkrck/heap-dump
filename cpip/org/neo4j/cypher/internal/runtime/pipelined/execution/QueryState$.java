package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple10;
import scala.None.;
import scala.runtime.AbstractFunction10;
import scala.runtime.BoxesRunTime;

public final class QueryState$
        extends AbstractFunction10<AnyValue[],QuerySubscriber,FlowControl,Object,IndexReadSession[],Object,Object,Object,Object,InputDataStream,QueryState>
        implements Serializable
{
    public static QueryState$ MODULE$;

    static
    {
        new QueryState$();
    }

    private QueryState$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "QueryState";
    }

    public QueryState apply( final AnyValue[] params, final QuerySubscriber subscriber, final FlowControl flowControl, final int morselSize,
            final IndexReadSession[] queryIndexes, final int numberOfWorkers, final int nExpressionSlots, final boolean prepopulateResults,
            final boolean doProfile, final InputDataStream input )
    {
        return new QueryState( params, subscriber, flowControl, morselSize, queryIndexes, numberOfWorkers, nExpressionSlots, prepopulateResults, doProfile,
                input );
    }

    public Option<Tuple10<AnyValue[],QuerySubscriber,FlowControl,Object,IndexReadSession[],Object,Object,Object,Object,InputDataStream>> unapply(
            final QueryState x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple10( x$0.params(), x$0.subscriber(), x$0.flowControl(), BoxesRunTime.boxToInteger( x$0.morselSize() ), x$0.queryIndexes(),
                    BoxesRunTime.boxToInteger( x$0.numberOfWorkers() ), BoxesRunTime.boxToInteger( x$0.nExpressionSlots() ),
                    BoxesRunTime.boxToBoolean( x$0.prepopulateResults() ), BoxesRunTime.boxToBoolean( x$0.doProfile() ), x$0.input() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
