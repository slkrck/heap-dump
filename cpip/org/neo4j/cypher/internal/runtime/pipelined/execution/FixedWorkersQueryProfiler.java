package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.profiling.ProfilingTracer;
import org.neo4j.cypher.internal.profiling.ProfilingTracerData;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.result.OperatorProfile;
import org.neo4j.cypher.result.QueryProfile;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class FixedWorkersQueryProfiler implements WorkersQueryProfiler
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$numberOfWorkers;
    public final Map<Object,Object> org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$applyRhsPlans;
    private final ProfilingTracer[] org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers;
    private volatile FixedWorkersQueryProfiler.Profile$ Profile$module;

    public FixedWorkersQueryProfiler( final int numberOfWorkers, final Map<Object,Object> applyRhsPlans )
    {
        this.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$numberOfWorkers = numberOfWorkers;
        this.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$applyRhsPlans = applyRhsPlans;
        this.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers =
                (ProfilingTracer[]) ((TraversableOnce) scala.runtime.RichInt..MODULE$.until$extension0( scala.Predef..MODULE$.intWrapper( 0 ), numberOfWorkers).
        map( ( x$1 ) -> {
            return $anonfun$profilers$1( BoxesRunTime.unboxToInt( x$1 ) );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( ProfilingTracer.class ));
    }

    public FixedWorkersQueryProfiler.Profile$ Profile()
    {
        if ( this.Profile$module == null )
        {
            this.Profile$lzycompute$1();
        }

        return this.Profile$module;
    }

    public ProfilingTracer[] org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers;
    }

    public QueryProfiler queryProfiler( final int workerId )
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers()[workerId];
    }

    private final void Profile$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.Profile$module == null )
            {
                this.Profile$module = new FixedWorkersQueryProfiler.Profile$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class Profile$ implements QueryProfile
    {
        public Profile$( final FixedWorkersQueryProfiler $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public OperatorProfile operatorProfile( final int operatorId )
        {
            Option var3 = this.$outer.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$applyRhsPlans.get(
                    BoxesRunTime.boxToInteger( operatorId ) );
            OperatorProfile var2;
            if ( var3 instanceof Some )
            {
                Some var4 = (Some) var3;
                int applyRhsPlanId = BoxesRunTime.unboxToInt( var4.value() );
                var2 = this.applyOperatorProfile( operatorId, applyRhsPlanId );
            }
            else
            {
                if ( !scala.None..MODULE$.equals( var3 )){
                throw new MatchError( var3 );
            }

                var2 = this.regularOperatorProfile( operatorId );
            }

            return var2;
        }

        private OperatorProfile regularOperatorProfile( final int operatorId )
        {
            int i = 0;

            ProfilingTracerData data;
            for ( data = new ProfilingTracerData();
                    i < this.$outer.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$numberOfWorkers; ++i )
            {
                OperatorProfile workerData =
                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers()[i].operatorProfile(
                                operatorId );
                data.update( workerData.time(), workerData.dbHits(), workerData.rows(), 0L, 0L );
            }

            data.update( 0L, 0L, 0L, -1L, -1L );
            data.sanitize();
            return data;
        }

        private OperatorProfile applyOperatorProfile( final int applyPlanId, final int applyRhsPlanId )
        {
            int i = 0;

            ProfilingTracerData data;
            for ( data = new ProfilingTracerData();
                    i < this.$outer.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$numberOfWorkers; ++i )
            {
                OperatorProfile timeData =
                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers()[i].operatorProfile(
                                applyPlanId );
                OperatorProfile rowData =
                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$execution$FixedWorkersQueryProfiler$$profilers()[i].operatorProfile(
                                applyRhsPlanId );
                data.update( timeData.time(), 0L, 0L, 0L, 0L );
                data.update( 0L, 0L, rowData.rows(), 0L, 0L );
            }

            data.update( 0L, 0L, 0L, -1L, -1L );
            data.sanitize();
            return data;
        }
    }
}
