package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.result.QueryProfile;
import org.neo4j.kernel.impl.query.QuerySubscription;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ProfiledQuerySubscription implements Product, Serializable
{
    private final QuerySubscription subscription;
    private final QueryProfile profile;
    private final QueryMemoryTracker memoryTracker;

    public ProfiledQuerySubscription( final QuerySubscription subscription, final QueryProfile profile, final QueryMemoryTracker memoryTracker )
    {
        this.subscription = subscription;
        this.profile = profile;
        this.memoryTracker = memoryTracker;
        Product.$init$( this );
    }

    public static Option<Tuple3<QuerySubscription,QueryProfile,QueryMemoryTracker>> unapply( final ProfiledQuerySubscription x$0 )
    {
        return ProfiledQuerySubscription$.MODULE$.unapply( var0 );
    }

    public static ProfiledQuerySubscription apply( final QuerySubscription subscription, final QueryProfile profile, final QueryMemoryTracker memoryTracker )
    {
        return ProfiledQuerySubscription$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<QuerySubscription,QueryProfile,QueryMemoryTracker>,ProfiledQuerySubscription> tupled()
    {
        return ProfiledQuerySubscription$.MODULE$.tupled();
    }

    public static Function1<QuerySubscription,Function1<QueryProfile,Function1<QueryMemoryTracker,ProfiledQuerySubscription>>> curried()
    {
        return ProfiledQuerySubscription$.MODULE$.curried();
    }

    public QuerySubscription subscription()
    {
        return this.subscription;
    }

    public QueryProfile profile()
    {
        return this.profile;
    }

    public QueryMemoryTracker memoryTracker()
    {
        return this.memoryTracker;
    }

    public ProfiledQuerySubscription copy( final QuerySubscription subscription, final QueryProfile profile, final QueryMemoryTracker memoryTracker )
    {
        return new ProfiledQuerySubscription( subscription, profile, memoryTracker );
    }

    public QuerySubscription copy$default$1()
    {
        return this.subscription();
    }

    public QueryProfile copy$default$2()
    {
        return this.profile();
    }

    public QueryMemoryTracker copy$default$3()
    {
        return this.memoryTracker();
    }

    public String productPrefix()
    {
        return "ProfiledQuerySubscription";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.subscription();
            break;
        case 1:
            var10000 = this.profile();
            break;
        case 2:
            var10000 = this.memoryTracker();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ProfiledQuerySubscription;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof ProfiledQuerySubscription )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            ProfiledQuerySubscription var4 = (ProfiledQuerySubscription) x$1;
                            QuerySubscription var10000 = this.subscription();
                            QuerySubscription var5 = var4.subscription();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            QueryProfile var8 = this.profile();
                            QueryProfile var6 = var4.profile();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            QueryMemoryTracker var9 = this.memoryTracker();
                            QueryMemoryTracker var7 = var4.memoryTracker();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
