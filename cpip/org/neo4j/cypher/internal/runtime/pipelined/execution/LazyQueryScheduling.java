package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.CleanUpTask;
import org.neo4j.cypher.internal.runtime.pipelined.PipelineState;
import org.neo4j.cypher.internal.runtime.pipelined.SchedulingResult;
import org.neo4j.cypher.internal.runtime.pipelined.Task;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class LazyQueryScheduling implements QuerySchedulingPolicy
{
    private final ExecutingQuery executingQuery;
    private final PipelineId[] pipelinesInLHSDepthFirstOrder;

    public LazyQueryScheduling( final ExecutingQuery executingQuery, final PipelineId[] pipelinesInLHSDepthFirstOrder )
    {
        this.executingQuery = executingQuery;
        this.pipelinesInLHSDepthFirstOrder = pipelinesInLHSDepthFirstOrder;
    }

    public SchedulingResult<Task<QueryResources>> nextTask( final QueryResources queryResources )
    {
        PipelineState[] pipelineStates = this.executingQuery.executionState().pipelineStates();
        CleanUpTask cleanUpTask = this.executingQuery.executionState().cleanUpTask();
        if ( cleanUpTask != null )
        {
            return new SchedulingResult( cleanUpTask, false );
        }
        else
        {
            int i = 0;

            boolean someTaskWasFilteredOut;
            for ( someTaskWasFilteredOut = false; i < this.pipelinesInLHSDepthFirstOrder.length; ++i )
            {
                PipelineState pipelineState = pipelineStates[this.pipelinesInLHSDepthFirstOrder[i].x()];
            .MODULE$.SCHEDULING().log( "[nextTask] probe pipeline (%s)", pipelineState.pipeline() );
                SchedulingResult schedulingResult =
                        pipelineState.nextTask( this.executingQuery.queryContext(), this.executingQuery.queryState(), queryResources );
                if ( schedulingResult.task() != null )
                {
               .MODULE$.SCHEDULING().log( "[nextTask] schedule %s", schedulingResult );
                    return schedulingResult;
                }

                if ( schedulingResult.someTaskWasFilteredOut() )
                {
                    someTaskWasFilteredOut = true;
                }
            }

            return new SchedulingResult( (Object) null, someTaskWasFilteredOut );
        }
    }
}
