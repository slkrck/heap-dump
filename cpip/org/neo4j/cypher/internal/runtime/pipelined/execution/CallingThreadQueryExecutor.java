package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.MEMORY_BOUND;
import org.neo4j.cypher.internal.runtime.MemoryTracking;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.debug.DebugLog.;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline;
import org.neo4j.cypher.internal.runtime.pipelined.Sleeper;
import org.neo4j.cypher.internal.runtime.pipelined.Worker;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerResourceProvider;
import org.neo4j.cypher.internal.runtime.pipelined.state.MemoryTrackingStandardStateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker;
import org.neo4j.cypher.internal.runtime.pipelined.state.StandardStateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.TheExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer;
import org.neo4j.cypher.result.QueryProfile;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CallingThreadQueryExecutor implements QueryExecutor, WorkerWaker
{
    private final CursorFactory cursors;

    public CallingThreadQueryExecutor( final CursorFactory cursors )
    {
        this.cursors = cursors;
    }

    public void wakeOne()
    {
    }

    public void assertAllReleased()
    {
    }

    public <E extends Exception> ProfiledQuerySubscription execute( final IndexedSeq<ExecutablePipeline> executablePipelines,
            final ExecutionGraphDefinition executionGraphDefinition, final InputDataStream inputDataStream, final QueryContext queryContext,
            final AnyValue[] params, final SchedulerTracer schedulerTracer, final IndexReadSession[] queryIndexes, final int nExpressionSlots,
            final boolean prePopulateResults, final QuerySubscriber subscriber, final boolean doProfile, final int morselSize,
            final MemoryTracking memoryTracking, final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy )
    {
      .MODULE$.log( "CallingThreadQueryExecutor.execute()" );
        Object var16;
        if ( org.neo4j.cypher.internal.runtime.NO_TRACKING..MODULE$.equals( memoryTracking )){
        var16 = new StandardStateFactory();
    } else if ( org.neo4j.cypher.internal.runtime.MEMORY_TRACKING..MODULE$.equals( memoryTracking )){
        var16 = new MemoryTrackingStandardStateFactory( Long.MAX_VALUE );
    } else{
        if ( !(memoryTracking instanceof MEMORY_BOUND) )
        {
            throw new MatchError( memoryTracking );
        }

        MEMORY_BOUND var19 = (MEMORY_BOUND) memoryTracking;
        long maxAllocatedBytes = var19.maxAllocatedBytes();
        var16 = new MemoryTrackingStandardStateFactory( maxAllocatedBytes );
    }

        QueryResources resources = new QueryResources( this.cursors );
        QueryExecutionTracer tracer = schedulerTracer.traceQuery();
        QueryCompletionTracker tracker = ((StandardStateFactory) var16).newTracker( subscriber, queryContext, tracer );
        QueryState queryState =
                new QueryState( params, subscriber, tracker, morselSize, queryIndexes, 1, nExpressionSlots, prePopulateResults, doProfile, inputDataStream );
        TheExecutionState executionState =
                new TheExecutionState( executionGraphDefinition, executablePipelines, (StateFactory) var16, this, queryContext, queryState, resources,
                        tracker );
        executionState.initializeState();
        Tuple2 var10000;
        if ( doProfile )
        {
            FixedWorkersQueryProfiler profiler = new FixedWorkersQueryProfiler( 1, executionGraphDefinition.applyRhsPlans() );
            var10000 = new Tuple2( profiler, profiler.Profile() );
        }
        else
        {
            var10000 = new Tuple2( WorkersQueryProfiler.NONE$.MODULE$, QueryProfile.NONE );
        }

        Tuple2 var28 = var10000;
        if ( var28 != null )
        {
            WorkersQueryProfiler workersProfiler = (WorkersQueryProfiler) var28._1();
            QueryProfile queryProfile = (QueryProfile) var28._2();
            Tuple2 var15 = new Tuple2( workersProfiler, queryProfile );
            WorkersQueryProfiler workersProfiler = (WorkersQueryProfiler) var15._1();
            QueryProfile queryProfile = (QueryProfile) var15._2();
            Worker worker = new Worker( 0, (QueryManager) null, Sleeper.noSleep$.MODULE$ );
            WorkerResourceProvider workerResourceProvider = new WorkerResourceProvider( 1, () -> {
                return resources;
            } );
            CallingThreadExecutingQuery executingQuery =
                    new CallingThreadExecutingQuery( executionState, queryContext, queryState, tracer, workersProfiler, worker, workerResourceProvider,
                            executionGraphSchedulingPolicy );
            return new ProfiledQuerySubscription( executingQuery, queryProfile, ((StandardStateFactory) var16).memoryTracker() );
        }
        else
        {
            throw new MatchError( var28 );
        }
    }
}
