package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple10;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class QueryState implements Product, Serializable
{
    private final AnyValue[] params;
    private final QuerySubscriber subscriber;
    private final FlowControl flowControl;
    private final int morselSize;
    private final IndexReadSession[] queryIndexes;
    private final int numberOfWorkers;
    private final int nExpressionSlots;
    private final boolean prepopulateResults;
    private final boolean doProfile;
    private final InputDataStream input;

    public QueryState( final AnyValue[] params, final QuerySubscriber subscriber, final FlowControl flowControl, final int morselSize,
            final IndexReadSession[] queryIndexes, final int numberOfWorkers, final int nExpressionSlots, final boolean prepopulateResults,
            final boolean doProfile, final InputDataStream input )
    {
        this.params = params;
        this.subscriber = subscriber;
        this.flowControl = flowControl;
        this.morselSize = morselSize;
        this.queryIndexes = queryIndexes;
        this.numberOfWorkers = numberOfWorkers;
        this.nExpressionSlots = nExpressionSlots;
        this.prepopulateResults = prepopulateResults;
        this.doProfile = doProfile;
        this.input = input;
        Product.$init$( this );
    }

    public static Option<Tuple10<AnyValue[],QuerySubscriber,FlowControl,Object,IndexReadSession[],Object,Object,Object,Object,InputDataStream>> unapply(
            final QueryState x$0 )
    {
        return QueryState$.MODULE$.unapply( var0 );
    }

    public static QueryState apply( final AnyValue[] params, final QuerySubscriber subscriber, final FlowControl flowControl, final int morselSize,
            final IndexReadSession[] queryIndexes, final int numberOfWorkers, final int nExpressionSlots, final boolean prepopulateResults,
            final boolean doProfile, final InputDataStream input )
    {
        return QueryState$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9 );
    }

    public static Function1<Tuple10<AnyValue[],QuerySubscriber,FlowControl,Object,IndexReadSession[],Object,Object,Object,Object,InputDataStream>,QueryState> tupled()
    {
        return QueryState$.MODULE$.tupled();
    }

    public static Function1<AnyValue[],Function1<QuerySubscriber,Function1<FlowControl,Function1<Object,Function1<IndexReadSession[],Function1<Object,Function1<Object,Function1<Object,Function1<Object,Function1<InputDataStream,QueryState>>>>>>>>>> curried()
    {
        return QueryState$.MODULE$.curried();
    }

    public AnyValue[] params()
    {
        return this.params;
    }

    public QuerySubscriber subscriber()
    {
        return this.subscriber;
    }

    public FlowControl flowControl()
    {
        return this.flowControl;
    }

    public int morselSize()
    {
        return this.morselSize;
    }

    public IndexReadSession[] queryIndexes()
    {
        return this.queryIndexes;
    }

    public int numberOfWorkers()
    {
        return this.numberOfWorkers;
    }

    public int nExpressionSlots()
    {
        return this.nExpressionSlots;
    }

    public boolean prepopulateResults()
    {
        return this.prepopulateResults;
    }

    public boolean doProfile()
    {
        return this.doProfile;
    }

    public InputDataStream input()
    {
        return this.input;
    }

    public QueryState copy( final AnyValue[] params, final QuerySubscriber subscriber, final FlowControl flowControl, final int morselSize,
            final IndexReadSession[] queryIndexes, final int numberOfWorkers, final int nExpressionSlots, final boolean prepopulateResults,
            final boolean doProfile, final InputDataStream input )
    {
        return new QueryState( params, subscriber, flowControl, morselSize, queryIndexes, numberOfWorkers, nExpressionSlots, prepopulateResults, doProfile,
                input );
    }

    public AnyValue[] copy$default$1()
    {
        return this.params();
    }

    public InputDataStream copy$default$10()
    {
        return this.input();
    }

    public QuerySubscriber copy$default$2()
    {
        return this.subscriber();
    }

    public FlowControl copy$default$3()
    {
        return this.flowControl();
    }

    public int copy$default$4()
    {
        return this.morselSize();
    }

    public IndexReadSession[] copy$default$5()
    {
        return this.queryIndexes();
    }

    public int copy$default$6()
    {
        return this.numberOfWorkers();
    }

    public int copy$default$7()
    {
        return this.nExpressionSlots();
    }

    public boolean copy$default$8()
    {
        return this.prepopulateResults();
    }

    public boolean copy$default$9()
    {
        return this.doProfile();
    }

    public String productPrefix()
    {
        return "QueryState";
    }

    public int productArity()
    {
        return 10;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.params();
            break;
        case 1:
            var10000 = this.subscriber();
            break;
        case 2:
            var10000 = this.flowControl();
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.morselSize() );
            break;
        case 4:
            var10000 = this.queryIndexes();
            break;
        case 5:
            var10000 = BoxesRunTime.boxToInteger( this.numberOfWorkers() );
            break;
        case 6:
            var10000 = BoxesRunTime.boxToInteger( this.nExpressionSlots() );
            break;
        case 7:
            var10000 = BoxesRunTime.boxToBoolean( this.prepopulateResults() );
            break;
        case 8:
            var10000 = BoxesRunTime.boxToBoolean( this.doProfile() );
            break;
        case 9:
            var10000 = this.input();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof QueryState;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.params() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.subscriber() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.flowControl() ) );
        var1 = Statics.mix( var1, this.morselSize() );
        var1 = Statics.mix( var1, Statics.anyHash( this.queryIndexes() ) );
        var1 = Statics.mix( var1, this.numberOfWorkers() );
        var1 = Statics.mix( var1, this.nExpressionSlots() );
        var1 = Statics.mix( var1, this.prepopulateResults() ? 1231 : 1237 );
        var1 = Statics.mix( var1, this.doProfile() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.input() ) );
        return Statics.finalizeHash( var1, 10 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label86:
            {
                boolean var2;
                if ( x$1 instanceof QueryState )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label68:
                    {
                        QueryState var4 = (QueryState) x$1;
                        if ( this.params() == var4.params() )
                        {
                            label77:
                            {
                                QuerySubscriber var10000 = this.subscriber();
                                QuerySubscriber var5 = var4.subscriber();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label77;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label77;
                                }

                                FlowControl var8 = this.flowControl();
                                FlowControl var6 = var4.flowControl();
                                if ( var8 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label77;
                                    }
                                }
                                else if ( !var8.equals( var6 ) )
                                {
                                    break label77;
                                }

                                if ( this.morselSize() == var4.morselSize() && this.queryIndexes() == var4.queryIndexes() &&
                                        this.numberOfWorkers() == var4.numberOfWorkers() && this.nExpressionSlots() == var4.nExpressionSlots() &&
                                        this.prepopulateResults() == var4.prepopulateResults() && this.doProfile() == var4.doProfile() )
                                {
                                    label50:
                                    {
                                        InputDataStream var9 = this.input();
                                        InputDataStream var7 = var4.input();
                                        if ( var9 == null )
                                        {
                                            if ( var7 != null )
                                            {
                                                break label50;
                                            }
                                        }
                                        else if ( !var9.equals( var7 ) )
                                        {
                                            break label50;
                                        }

                                        if ( var4.canEqual( this ) )
                                        {
                                            var10 = true;
                                            break label68;
                                        }
                                    }
                                }
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label86;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
