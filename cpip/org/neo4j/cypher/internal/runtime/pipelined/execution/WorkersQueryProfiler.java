package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.profiling.QueryProfiler;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface WorkersQueryProfiler
{
    QueryProfiler queryProfiler( final int workerId );

    public static class NONE$ implements WorkersQueryProfiler
    {
        public static WorkersQueryProfiler.NONE$ MODULE$;

        static
        {
            new WorkersQueryProfiler.NONE$();
        }

        public NONE$()
        {
            MODULE$ = this;
        }

        public QueryProfiler queryProfiler( final int workerId )
        {
            return QueryProfiler.NONE;
        }
    }
}
