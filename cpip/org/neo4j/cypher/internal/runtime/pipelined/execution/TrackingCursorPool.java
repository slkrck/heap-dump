package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.internal.kernel.api.Cursor;
import scala.Function0;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class TrackingCursorPool<CURSOR extends Cursor> extends CursorPool<CURSOR>
{
    private volatile long liveCount = 0L;

    public TrackingCursorPool( final Function0<CURSOR> cursorFactory )
    {
        super( cursorFactory );
    }

    private long liveCount()
    {
        return this.liveCount;
    }

    private void liveCount_$eq( final long x$1 )
    {
        this.liveCount = x$1;
    }

    public CURSOR allocateAndTrace()
    {
        this.liveCount_$eq( this.liveCount() + 1L );
        return super.allocateAndTrace();
    }

    public CURSOR allocate()
    {
        this.liveCount_$eq( this.liveCount() + 1L );
        return super.allocate();
    }

    public void free( final CURSOR cursor )
    {
        if ( cursor != null )
        {
            this.liveCount_$eq( this.liveCount() - 1L );
        }

        super.free( cursor );
    }

    public long getLiveCount()
    {
        return this.liveCount();
    }
}
