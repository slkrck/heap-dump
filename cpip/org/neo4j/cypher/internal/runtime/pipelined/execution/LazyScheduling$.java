package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;

public final class LazyScheduling$ implements SchedulingPolicy
{
    public static LazyScheduling$ MODULE$;

    static
    {
        new LazyScheduling$();
    }

    private LazyScheduling$()
    {
        MODULE$ = this;
    }

    public ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy( final ExecutionGraphDefinition executionGraphDefinition )
    {
        return new LazyExecutionGraphScheduling( executionGraphDefinition );
    }
}
