package org.neo4j.cypher.internal.runtime.pipelined.execution;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class QueryManager
{
    private final ConcurrentLinkedQueue<ExecutingQuery> runningQueries = new ConcurrentLinkedQueue();

    private ConcurrentLinkedQueue<ExecutingQuery> runningQueries()
    {
        return this.runningQueries;
    }

    public void addQuery( final ExecutingQuery query )
    {
      .MODULE$.QUERIES().log( "Adding query %s", query );
        this.runningQueries().add( query );
    }

    public ExecutingQuery nextQueryToWorkOn( final int workerId )
    {
        ExecutingQuery query;
        for ( query = (ExecutingQuery) this.runningQueries().peek(); query != null && query.executionState().hasEnded();
                query = (ExecutingQuery) this.runningQueries().peek() )
        {
         .MODULE$.QUERIES().log( "Removing query %s", query );
            this.runningQueries().remove( query );
        }

        return query;
    }
}
