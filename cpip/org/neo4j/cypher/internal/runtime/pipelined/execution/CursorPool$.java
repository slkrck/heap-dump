package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.v4_0.util.AssertionRunner;
import org.neo4j.internal.kernel.api.Cursor;
import scala.Function0;

public final class CursorPool$
{
    public static CursorPool$ MODULE$;

    static
    {
        new CursorPool$();
    }

    private CursorPool$()
    {
        MODULE$ = this;
    }

    public <CURSOR extends Cursor> CursorPool<CURSOR> apply( final Function0<CURSOR> cursorFactory )
    {
        return (CursorPool) (AssertionRunner.isAssertionsEnabled() ? new TrackingCursorPool( cursorFactory ) : new CursorPool( cursorFactory ));
    }
}
