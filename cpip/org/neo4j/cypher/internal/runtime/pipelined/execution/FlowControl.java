package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.kernel.impl.query.QuerySubscription;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface FlowControl extends QuerySubscription
{
    long getDemand();

    boolean hasDemand();

    void addServed( final long newlyServed );
}
