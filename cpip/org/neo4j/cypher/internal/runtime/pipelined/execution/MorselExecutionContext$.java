package org.neo4j.cypher.internal.runtime.pipelined.execution;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration$;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import scala.Array.;

public final class MorselExecutionContext$
{
    public static MorselExecutionContext$ MODULE$;

    static
    {
        new MorselExecutionContext$();
    }

    private final MorselExecutionContext empty;

    private MorselExecutionContext$()
    {
        MODULE$ = this;
        this.empty = new MorselExecutionContext( new Morsel( ( long[]).MODULE$.empty( scala.reflect.ClassTag..MODULE$.Long()),(AnyValue[]).
        MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( AnyValue.class ))),
        SlotConfiguration$.MODULE$.empty(), 0, this.$lessinit$greater$default$4(), this.$lessinit$greater$default$5(), this.$lessinit$greater$default$6(), this.$lessinit$greater$default$7())
        ;
    }

    public int $lessinit$greater$default$4()
    {
        return 0;
    }

    public int $lessinit$greater$default$5()
    {
        return 0;
    }

    public int $lessinit$greater$default$6()
    {
        return 0;
    }

    public WorkUnitEvent $lessinit$greater$default$7()
    {
        return null;
    }

    public MorselExecutionContext apply( final Morsel morsel, final SlotConfiguration slots, final int maxNumberOfRows )
    {
        return new MorselExecutionContext( morsel, slots, maxNumberOfRows, 0, 0, maxNumberOfRows, this.$lessinit$greater$default$7() );
    }

    public MorselExecutionContext empty()
    {
        return this.empty;
    }

    public FilteringMorselExecutionContext createInitialRow()
    {
        return new FilteringMorselExecutionContext()
        {
            public Value getCachedPropertyAt( final int offset )
            {
                return null;
            }

            public FilteringMorselExecutionContext shallowCopy()
            {
                return MorselExecutionContext$.MODULE$.createInitialRow();
            }
        };
    }
}
