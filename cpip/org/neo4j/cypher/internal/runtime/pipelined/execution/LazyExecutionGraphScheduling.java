package org.neo4j.cypher.internal.runtime.pipelined.execution;

import java.util.BitSet;

import org.eclipse.collections.api.stack.primitive.MutableIntStack;
import org.eclipse.collections.impl.factory.primitive.IntStacks;
import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.physicalplanning.PipelineDefinition;
import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.physicalplanning.PipelineId$;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class LazyExecutionGraphScheduling implements ExecutionGraphSchedulingPolicy
{
    private final PipelineId[] pipelinesInLHSDepthFirstOrder;

    public LazyExecutionGraphScheduling( final ExecutionGraphDefinition executionGraphDefinition )
    {
        IndexedSeq pipelinesInExecutionOrder = executionGraphDefinition.pipelines();
        PipelineId[] result = new PipelineId[pipelinesInExecutionOrder.length()];
        MutableIntStack stack = IntStacks.mutable.empty();
        BitSet visited = new BitSet( pipelinesInExecutionOrder.length() );
        int i = 0;
        stack.push( pipelinesInExecutionOrder.length() - 1 );

        while ( stack.notEmpty() )
        {
            int pipelineId = stack.pop();
            if ( !visited.get( pipelineId ) )
            {
                PipelineDefinition pipelineState = (PipelineDefinition) pipelinesInExecutionOrder.apply( pipelineId );
                result[i] = new PipelineId( pipelineId );
                ++i;
                visited.set( pipelineId );
                if ( pipelineState.rhs() != PipelineId$.MODULE$.NO_PIPELINE() )
                {
                    stack.push( pipelineState.rhs() );
                }

                if ( pipelineState.lhs() != PipelineId$.MODULE$.NO_PIPELINE() )
                {
                    stack.push( pipelineState.lhs() );
                }
            }
        }

        this.pipelinesInLHSDepthFirstOrder = result;
    }

    public PipelineId[] pipelinesInLHSDepthFirstOrder()
    {
        return this.pipelinesInLHSDepthFirstOrder;
    }

    public QuerySchedulingPolicy querySchedulingPolicy( final ExecutingQuery executingQuery )
    {
        return new LazyQueryScheduling( executingQuery, this.pipelinesInLHSDepthFirstOrder() );
    }
}
