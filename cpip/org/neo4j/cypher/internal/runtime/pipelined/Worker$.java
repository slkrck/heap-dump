package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.Predef.;
import scala.collection.Seq;

public final class Worker$
{
    public static Worker$ MODULE$;

    static
    {
        new Worker$();
    }

    private final Seq<WorkUnitEvent> NO_WORK;

    private Worker$()
    {
        MODULE$ = this;
        this.NO_WORK = .MODULE$.wrapRefArray( (Object[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( WorkUnitEvent.class )));
    }

    public Seq<WorkUnitEvent> NO_WORK()
    {
        return this.NO_WORK;
    }

    public String WORKING_THOUGH_RELEASED( final Worker worker )
    {
        return (new StringBuilder( 57 )).append( worker ).append( " is WORKING even though all resources should be released!" ).toString();
    }
}
