package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple6;
import scala.None.;
import scala.runtime.AbstractFunction6;

public final class PipelineTask$
        extends AbstractFunction6<ContinuableOperatorTask,OperatorTask[],OutputOperatorState,QueryContext,QueryState,PipelineState,PipelineTask>
        implements Serializable
{
    public static PipelineTask$ MODULE$;

    static
    {
        new PipelineTask$();
    }

    private PipelineTask$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "PipelineTask";
    }

    public PipelineTask apply( final ContinuableOperatorTask startTask, final OperatorTask[] middleTasks, final OutputOperatorState outputOperatorState,
            final QueryContext queryContext, final QueryState state, final PipelineState pipelineState )
    {
        return new PipelineTask( startTask, middleTasks, outputOperatorState, queryContext, state, pipelineState );
    }

    public Option<Tuple6<ContinuableOperatorTask,OperatorTask[],OutputOperatorState,QueryContext,QueryState,PipelineState>> unapply( final PipelineTask x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple6( x$0.startTask(), x$0.middleTasks(), x$0.outputOperatorState(), x$0.queryContext(), x$0.state(), x$0.pipelineState() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
