package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.NonFatalCypherError.;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.FilteringMorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.FilteringMorselExecutionContext$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.Morsel;
import org.neo4j.cypher.internal.runtime.pipelined.execution.Morsel$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorInput;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.util.Preconditions;
import scala.Function1;
import scala.Option;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;
import scala.runtime.BooleanRef;

@JavaDocToJava
public class PipelineState implements OperatorInput, OperatorCloser
{
    private final ExecutablePipeline pipeline;
    private final OperatorState startState;
    private final OperatorTask[] middleTasks;
    private final Function1<OperatorTask,OutputOperatorState> outputOperatorStateCreator;
    private final ExecutionState executionState;

    public PipelineState( final ExecutablePipeline pipeline, final OperatorState startState, final OperatorTask[] middleTasks,
            final Function1<OperatorTask,OutputOperatorState> outputOperatorStateCreator, final ExecutionState executionState )
    {
        this.pipeline = pipeline;
        this.startState = startState;
        this.middleTasks = middleTasks;
        this.outputOperatorStateCreator = outputOperatorStateCreator;
        this.executionState = executionState;
    }

    private static final boolean taskCancelled$1( final PipelineTask task, final QueryResources resources$1, final BooleanRef someTaskWasFilteredOut$1 )
    {
        boolean cancelled = task.filterCancelledArguments( resources$1 );
        if ( cancelled )
        {
            someTaskWasFilteredOut$1.elem = true;
        }

        return cancelled;
    }

    public ExecutablePipeline pipeline()
    {
        return this.pipeline;
    }

    public SchedulingResult<PipelineTask> nextTask( final QueryContext context, final QueryState state, final QueryResources resources )
    {
        PipelineTask task = null;
        BooleanRef someTaskWasFilteredOut = BooleanRef.create( false );

        try
        {
            do
            {
                PipelineTask var10000;
                if ( this.pipeline().serial() )
                {
                    if ( this.executionState.canContinueOrTake( this.pipeline() ) && this.executionState.tryLock( this.pipeline() ) )
                    {
                        PipelineTask t = this.innerNextTask( context, state, resources );
                        if ( t == null )
                        {
                            this.executionState.unlock( this.pipeline() );
                        }

                        var10000 = t;
                    }
                    else
                    {
                        var10000 = null;
                    }
                }
                else
                {
                    var10000 = this.innerNextTask( context, state, resources );
                }

                task = var10000;
            }
            while ( task != null && taskCancelled$1( task, resources, someTaskWasFilteredOut ) );
        }
        catch ( Throwable var12 )
        {
            Option var10 = .MODULE$.unapply( var12 );
            if ( !var10.isEmpty() )
            {
                Throwable t = (Throwable) var10.get();
                if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.SCHEDULING().enabled()){
                org.neo4j.cypher.internal.runtime.debug.DebugSupport..
                MODULE$.SCHEDULING().log( (new StringBuilder( 23 )).append( "[nextTask] failed with " ).append( t ).toString() );
            } else if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.WORKERS().enabled()){
                org.neo4j.cypher.internal.runtime.debug.DebugSupport..
                MODULE$.WORKERS().log( (new StringBuilder( 23 )).append( "[nextTask] failed with " ).append( t ).toString() );
            }

                throw new NextTaskException( this.pipeline(), t );
            }

            throw var12;
        }

        return new SchedulingResult( task, someTaskWasFilteredOut.elem );
    }

    public MorselExecutionContext allocateMorsel( final WorkUnitEvent producingWorkUnitEvent, final QueryState state )
    {
        Object var10000;
        if ( this.pipeline().needsMorsel() )
        {
            SlotConfiguration slots = this.pipeline().slots();
            Morsel morsel = Morsel$.MODULE$.create( slots, state.morselSize() );
            var10000 =
                    this.pipeline().needsFilteringMorsel() ? new FilteringMorselExecutionContext( morsel, slots, state.morselSize(), 0, 0, state.morselSize(),
                            producingWorkUnitEvent, FilteringMorselExecutionContext$.MODULE$.$lessinit$greater$default$8() )
                                                           : new MorselExecutionContext( morsel, slots, state.morselSize(), 0, 0, state.morselSize(),
                                                                   producingWorkUnitEvent );
        }
        else
        {
            var10000 = MorselExecutionContext$.MODULE$.empty();
        }

        return (MorselExecutionContext) var10000;
    }

    private PipelineTask innerNextTask( final QueryContext context, final QueryState state, final QueryResources resources )
    {
        if ( !this.executionState.canPut( this.pipeline() ) )
        {
            return null;
        }
        else
        {
            PipelineTask task = this.executionState.takeContinuation( this.pipeline() );
            if ( task != null )
            {
                return task;
            }
            else
            {
                int parallelism = this.pipeline().serial() ? 1 : state.numberOfWorkers();
                IndexedSeq startTasks = this.startState.nextTasks( context, state, this, parallelism, resources, this.executionState.argumentStateMaps() );
                PipelineTask var10000;
                if ( startTasks != null )
                {
                    Preconditions.checkArgument( startTasks.nonEmpty(), "If no tasks are available, `null` is expected rather than empty collections" );

                    for ( int i = 1; i < startTasks.size(); ++i )
                    {
                        this.putContinuation( this.asPipelineTask$1( (ContinuableOperatorTask) startTasks.apply( i ), context, state ), true, resources );
                    }

                    var10000 = this.asPipelineTask$1( (ContinuableOperatorTask) startTasks.apply( 0 ), context, state );
                }
                else
                {
                    var10000 = null;
                }

                return var10000;
            }
        }
    }

    public void putContinuation( final PipelineTask task, final boolean wakeUp, final QueryResources resources )
    {
        this.executionState.putContinuation( task, wakeUp, resources );
    }

    public MorselParallelizer takeMorsel()
    {
        return this.executionState.takeMorsel( this.pipeline().inputBuffer().id() );
    }

    public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> ACC takeAccumulator()
    {
        return this.executionState.takeAccumulator( this.pipeline().inputBuffer().id() );
    }

    public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> Buffers.AccumulatorAndMorsel<DATA,ACC> takeAccumulatorAndMorsel()
    {
        return this.executionState.takeAccumulatorAndMorsel( this.pipeline().inputBuffer().id() );
    }

    public <DATA> DATA takeData()
    {
        return this.executionState.takeData( this.pipeline().inputBuffer().id() );
    }

    public void closeMorsel( final MorselExecutionContext morsel )
    {
        this.executionState.closeMorselTask( this.pipeline(), morsel );
    }

    public <DATA> void closeData( final DATA data )
    {
        this.executionState.closeData( this.pipeline(), data );
    }

    public void closeAccumulator( final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        this.executionState.closeAccumulatorTask( this.pipeline(), accumulator );
    }

    public void closeMorselAndAccumulatorTask( final MorselExecutionContext morsel, final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        this.executionState.closeMorselAndAccumulatorTask( this.pipeline(), morsel, accumulator );
    }

    public boolean filterCancelledArguments( final MorselExecutionContext morsel )
    {
        return this.executionState.filterCancelledArguments( this.pipeline(), morsel );
    }

    public boolean filterCancelledArguments( final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        return this.executionState.filterCancelledArguments( this.pipeline(), accumulator );
    }

    public boolean filterCancelledArguments( final MorselExecutionContext morsel, final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        return this.executionState.filterCancelledArguments( this.pipeline(), morsel, accumulator );
    }

    private final PipelineTask asPipelineTask$1( final ContinuableOperatorTask startTask, final QueryContext context$1, final QueryState state$1 )
    {
        return new PipelineTask( startTask, this.middleTasks, (OutputOperatorState) this.outputOperatorStateCreator.apply( startTask ), context$1, state$1,
                this );
    }
}
