package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ExecutionState extends ArgumentStateMapCreator
{
    static void $init$( final ExecutionState $this )
    {
    }

    PipelineState[] pipelineStates();

    default <T> Sink<T> getSinkInt( final int fromPipeline, final int bufferId )
    {
        return this.getSink( fromPipeline, bufferId );
    }

    <T> Sink<T> getSink( final int fromPipeline, final int bufferId );

    void putMorsel( final int fromPipeline, final int bufferId, final MorselExecutionContext morsel );

    MorselParallelizer takeMorsel( final int bufferId );

    <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> ACC takeAccumulator( final int bufferId );

    <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> Buffers.AccumulatorAndMorsel<DATA,ACC> takeAccumulatorAndMorsel( final int bufferId );

    <DATA> DATA takeData( final int bufferId );

    void closeMorselTask( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel );

    <DATA> void closeData( final ExecutablePipeline pipeline, final DATA data );

    void closeWorkUnit( final ExecutablePipeline pipeline );

    void closeAccumulatorTask( final ExecutablePipeline pipeline, final ArgumentStateMap.MorselAccumulator<?> accumulator );

    void closeMorselAndAccumulatorTask( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel,
            final ArgumentStateMap.MorselAccumulator<?> accumulator );

    boolean filterCancelledArguments( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel );

    boolean filterCancelledArguments( final ExecutablePipeline pipeline, final ArgumentStateMap.MorselAccumulator<?> accumulator );

    boolean filterCancelledArguments( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel,
            final ArgumentStateMap.MorselAccumulator<?> accumulator );

    boolean canPut( final ExecutablePipeline pipeline );

    PipelineTask takeContinuation( final ExecutablePipeline p );

    void putContinuation( final PipelineTask task, final boolean wakeUp, final QueryResources resources );

    boolean tryLock( final ExecutablePipeline pipeline );

    void unlock( final ExecutablePipeline pipeline );

    boolean canContinueOrTake( final ExecutablePipeline pipeline );

    void initializeState();

    void failQuery( final Throwable throwable, final QueryResources resources, final ExecutablePipeline failedPipeline );

    void cancelQuery( final QueryResources resources );

    void scheduleCancelQuery();

    CleanUpTask cleanUpTask();

    boolean hasEnded();

    String prettyString( final ExecutablePipeline pipeline );

    ArgumentStateMap.ArgumentStateMaps argumentStateMaps();
}
