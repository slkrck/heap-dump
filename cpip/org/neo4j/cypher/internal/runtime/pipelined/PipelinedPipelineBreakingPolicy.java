package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.AllNodesScan;
import org.neo4j.cypher.internal.logical.plans.Apply;
import org.neo4j.cypher.internal.logical.plans.Argument;
import org.neo4j.cypher.internal.logical.plans.CacheProperties;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.DirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.Distinct;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.Input;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.NodeByIdSeek;
import org.neo4j.cypher.internal.logical.plans.NodeByLabelScan;
import org.neo4j.cypher.internal.logical.plans.NodeCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.NodeHashJoin;
import org.neo4j.cypher.internal.logical.plans.NodeIndexContainsScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexEndsWithScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexSeek;
import org.neo4j.cypher.internal.logical.plans.NodeUniqueIndexSeek;
import org.neo4j.cypher.internal.logical.plans.Optional;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.logical.plans.RelationshipCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.Selection;
import org.neo4j.cypher.internal.logical.plans.Sort;
import org.neo4j.cypher.internal.logical.plans.Top;
import org.neo4j.cypher.internal.logical.plans.UndirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import org.neo4j.cypher.internal.logical.plans.ExpandAll.;
import org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy;
import org.neo4j.cypher.internal.physicalplanning.PipelineBreakingPolicy;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class PipelinedPipelineBreakingPolicy implements PipelineBreakingPolicy, Product, Serializable
{
    private final OperatorFusionPolicy fusionPolicy;
    private final InterpretedPipesFallbackPolicy interpretedPipesPolicy;

    public PipelinedPipelineBreakingPolicy( final OperatorFusionPolicy fusionPolicy, final InterpretedPipesFallbackPolicy interpretedPipesPolicy )
    {
        this.fusionPolicy = fusionPolicy;
        this.interpretedPipesPolicy = interpretedPipesPolicy;
        PipelineBreakingPolicy.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<OperatorFusionPolicy,InterpretedPipesFallbackPolicy>> unapply( final PipelinedPipelineBreakingPolicy x$0 )
    {
        return PipelinedPipelineBreakingPolicy$.MODULE$.unapply( var0 );
    }

    public static PipelinedPipelineBreakingPolicy apply( final OperatorFusionPolicy fusionPolicy, final InterpretedPipesFallbackPolicy interpretedPipesPolicy )
    {
        return PipelinedPipelineBreakingPolicy$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<OperatorFusionPolicy,InterpretedPipesFallbackPolicy>,PipelinedPipelineBreakingPolicy> tupled()
    {
        return PipelinedPipelineBreakingPolicy$.MODULE$.tupled();
    }

    public static Function1<OperatorFusionPolicy,Function1<InterpretedPipesFallbackPolicy,PipelinedPipelineBreakingPolicy>> curried()
    {
        return PipelinedPipelineBreakingPolicy$.MODULE$.curried();
    }

    public SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
    {
        return PipelineBreakingPolicy.invoke$( this, lp, slots, argumentSlots );
    }

    public OperatorFusionPolicy fusionPolicy()
    {
        return this.fusionPolicy;
    }

    public InterpretedPipesFallbackPolicy interpretedPipesPolicy()
    {
        return this.interpretedPipesPolicy;
    }

    public boolean breakOn( final LogicalPlan lp )
    {
        boolean var6;
        if ( lp instanceof AllNodesScan )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeByLabelScan )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeIndexSeek )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeUniqueIndexSeek )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeIndexContainsScan )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeIndexEndsWithScan )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeIndexScan )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeByIdSeek )
        {
            var6 = true;
        }
        else if ( lp instanceof DirectedRelationshipByIdSeek )
        {
            var6 = true;
        }
        else if ( lp instanceof UndirectedRelationshipByIdSeek )
        {
            var6 = true;
        }
        else if ( lp instanceof NodeCountFromCountStore )
        {
            var6 = true;
        }
        else if ( lp instanceof RelationshipCountFromCountStore )
        {
            var6 = true;
        }
        else if ( lp instanceof Input )
        {
            var6 = true;
        }
        else if ( lp instanceof Argument )
        {
            var6 = true;
        }
        else
        {
            var6 = false;
        }

        boolean var2;
        if ( var6 )
        {
            var2 = true;
        }
        else
        {
            Expand var8;
            label195:
            {
                if ( lp instanceof Expand )
                {
                    var8 = (Expand) lp;
                    ExpansionMode var10000 = var8.mode();
                    var9 = .MODULE$;
                    if ( var10000 == null )
                    {
                        if ( var9 == null )
                        {
                            break label195;
                        }
                    }
                    else if ( var10000.equals( var9 ) )
                    {
                        break label195;
                    }
                }

                boolean var5;
                if ( lp instanceof UnwindCollection )
                {
                    var5 = true;
                }
                else if ( lp instanceof Sort )
                {
                    var5 = true;
                }
                else if ( lp instanceof Top )
                {
                    var5 = true;
                }
                else if ( lp instanceof Aggregation )
                {
                    var5 = true;
                }
                else if ( lp instanceof Optional )
                {
                    var5 = true;
                }
                else if ( lp instanceof VarExpand )
                {
                    var5 = true;
                }
                else
                {
                    var5 = false;
                }

                if ( var5 )
                {
                    var2 = !this.canFuseOneChildOperator( lp );
                    return var2;
                }
                else
                {
                    boolean var4;
                    if ( lp instanceof ProduceResult )
                    {
                        var4 = true;
                    }
                    else if ( lp instanceof Limit )
                    {
                        var4 = true;
                    }
                    else if ( lp instanceof Distinct )
                    {
                        var4 = true;
                    }
                    else if ( lp instanceof Projection )
                    {
                        var4 = true;
                    }
                    else if ( lp instanceof CacheProperties )
                    {
                        var4 = true;
                    }
                    else if ( lp instanceof Selection )
                    {
                        var4 = true;
                    }
                    else
                    {
                        var4 = false;
                    }

                    if ( var4 )
                    {
                        var2 = false;
                        return var2;
                    }
                    else
                    {
                        boolean var3;
                        if ( lp instanceof Apply )
                        {
                            var3 = true;
                        }
                        else if ( lp instanceof NodeHashJoin )
                        {
                            var3 = true;
                        }
                        else if ( lp instanceof CartesianProduct )
                        {
                            var3 = true;
                        }
                        else
                        {
                            var3 = false;
                        }

                        if ( var3 )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = this.interpretedPipesPolicy().breakOn( lp ) && !this.canFuseOneChildOperator( lp );
                        }

                        return var2;
                    }
                }
            }

            var2 = !this.canFuseOneChildOperator( var8 );
        }

        return var2;
    }

    public void onNestedPlanBreak()
    {
        throw this.unsupported( "NestedPlanExpression" );
    }

    private CantCompileQueryException unsupported( final String thing )
    {
        return new CantCompileQueryException( (new StringBuilder( 75 )).append( "Pipelined does not yet support the plans including `" ).append( thing ).append(
                "`, use another runtime." ).toString() );
    }

    private boolean canFuseOneChildOperator( final LogicalPlan lp )
    {
        scala.Predef..MODULE$. assert (lp.rhs().isEmpty());
        if ( !this.fusionPolicy().canFuseOverPipeline( lp ) )
        {
            return false;
        }
        else
        {
            for ( LogicalPlan current = (LogicalPlan) lp.lhs().orNull( scala.Predef..MODULE$.$conforms() )
            {
                ;
            } current != null;
            current = (LogicalPlan) current.lhs().orNull( scala.Predef..MODULE$.$conforms())){
            if ( !this.fusionPolicy().canFuse( current ) )
            {
                return false;
            }

            if ( this.breakOn( current ) )
            {
                return true;
            }
        }

            return true;
        }
    }

    public PipelinedPipelineBreakingPolicy copy( final OperatorFusionPolicy fusionPolicy, final InterpretedPipesFallbackPolicy interpretedPipesPolicy )
    {
        return new PipelinedPipelineBreakingPolicy( fusionPolicy, interpretedPipesPolicy );
    }

    public OperatorFusionPolicy copy$default$1()
    {
        return this.fusionPolicy();
    }

    public InterpretedPipesFallbackPolicy copy$default$2()
    {
        return this.interpretedPipesPolicy();
    }

    public String productPrefix()
    {
        return "PipelinedPipelineBreakingPolicy";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.fusionPolicy();
            break;
        case 1:
            var10000 = this.interpretedPipesPolicy();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof PipelinedPipelineBreakingPolicy;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof PipelinedPipelineBreakingPolicy )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            PipelinedPipelineBreakingPolicy var4 = (PipelinedPipelineBreakingPolicy) x$1;
                            OperatorFusionPolicy var10000 = this.fusionPolicy();
                            OperatorFusionPolicy var5 = var4.fusionPolicy();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            InterpretedPipesFallbackPolicy var7 = this.interpretedPipesPolicy();
                            InterpretedPipesFallbackPolicy var6 = var4.interpretedPipesPolicy();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
