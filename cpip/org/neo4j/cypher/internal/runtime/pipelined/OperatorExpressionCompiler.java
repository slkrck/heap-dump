package org.neo4j.cypher.internal.runtime.pipelined;

import java.util.Arrays;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.VariableNamer;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCodeGenHelperTemplates$;
import org.neo4j.cypher.operations.CursorUtils;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple5;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction1;
import scala.runtime.AbstractFunction3;
import scala.runtime.AbstractFunction5;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;
import scala.runtime.ObjectRef;

@JavaDocToJava
public class OperatorExpressionCompiler extends ExpressionCompiler
{
    private final SlotConfiguration inputSlotConfiguration;
    private final OperatorExpressionCompiler.LocalsForSlots locals;
    private int nLongSlotsToCopyFromInput;
    private int nRefSlotsToCopyFromInput;

    public OperatorExpressionCompiler( final SlotConfiguration slots, final SlotConfiguration inputSlotConfiguration, final boolean readOnly,
            final CodeGenerationMode codeGenerationMode, final VariableNamer namer )
    {
        super( slots, readOnly, codeGenerationMode, namer );
        this.inputSlotConfiguration = inputSlotConfiguration;
        this.locals = new OperatorExpressionCompiler.LocalsForSlots( this );
        this.nLongSlotsToCopyFromInput = 0;
        this.nRefSlotsToCopyFromInput = 0;
    }

    private static final IntermediateRepresentation initializeFromStoreIR$1( final IntermediateRepresentation getFromStore$1, final ObjectRef local$1 )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( (String) local$1.elem, getFromStore$1 );
    }

    public SlotConfiguration inputSlotConfiguration()
    {
        return this.inputSlotConfiguration;
    }

    private OperatorExpressionCompiler.LocalsForSlots locals()
    {
        return this.locals;
    }

    private int nLongSlotsToCopyFromInput()
    {
        return this.nLongSlotsToCopyFromInput;
    }

    private void nLongSlotsToCopyFromInput_$eq( final int x$1 )
    {
        this.nLongSlotsToCopyFromInput = x$1;
    }

    private int nRefSlotsToCopyFromInput()
    {
        return this.nRefSlotsToCopyFromInput;
    }

    private void nRefSlotsToCopyFromInput_$eq( final int x$1 )
    {
        this.nRefSlotsToCopyFromInput = x$1;
    }

    public final IntermediateRepresentation getLongAt( final int offset )
    {
        String local = this.locals().getLocalForLongSlot( offset );
        if ( local == null )
        {
            local = this.locals().addLocalForLongSlot( offset );
        }

        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( local );
    }

    public final IntermediateRepresentation getRefAt( final int offset )
    {
        String local = this.locals().getLocalForRefSlot( offset );
        if ( local == null )
        {
            local = this.locals().addLocalForRefSlot( offset );
        }

        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( local );
    }

    public final IntermediateRepresentation setLongAt( final int offset, final IntermediateRepresentation value )
    {
        String local = this.locals().getLocalForLongSlot( offset );
        if ( local == null )
        {
            local = this.locals().addLocalForLongSlot( offset );
            this.locals().markInitializedLocalForLongSlot( offset );
        }

        this.locals().markModifiedLocalForLongSlot( offset );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( local, value );
    }

    public final IntermediateRepresentation setRefAt( final int offset, final IntermediateRepresentation value )
    {
        String local = this.locals().getLocalForRefSlot( offset );
        if ( local == null )
        {
            local = this.locals().addLocalForRefSlot( offset );
            this.locals().markInitializedLocalForRefSlot( offset );
        }

        this.locals().markModifiedLocalForRefSlot( offset );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( local, value );
    }

    public IntermediateRepresentation copyFromInput( final int nLongs, final int nRefs )
    {
        if ( nLongs > this.nLongSlotsToCopyFromInput() )
        {
            this.nLongSlotsToCopyFromInput_$eq( nLongs );
        }

        if ( nRefs > this.nRefSlotsToCopyFromInput() )
        {
            this.nRefSlotsToCopyFromInput_$eq( nRefs );
        }

        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }

    public final IntermediateRepresentation doCopyFromWithExecutionContext( final IntermediateRepresentation context, final IntermediateRepresentation input,
            final int nLongs, final int nRefs )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeSideEffect( context, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "copyFrom",
                scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class ), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{input, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( nLongs ) ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
            BoxesRunTime.boxToInteger( nRefs ) )})));
    }

    public void didInitializeCachedPropertyFromStore()
    {
    }

    public void didInitializeCachedPropertyFromContext()
    {
    }

    public void didLoadLocalCachedProperty()
    {
    }

    public IntermediateRepresentation getCachedPropertyAt( final SlottedCachedProperty property, final IntermediateRepresentation getFromStore )
    {
        int offset = property.cachedPropertyOffset();
        ObjectRef local = ObjectRef.create( this.locals().getLocalForRefSlot( offset ) );
        Option maybeCachedProperty = this.inputSlotConfiguration().getCachedPropertySlot( property );
        this.locals().markInitializedLocalForRefSlot( offset );
        IntermediateRepresentation var10000;
        if ( (String) local.elem == null && maybeCachedProperty.isDefined() )
        {
            this.didInitializeCachedPropertyFromContext();
            local.elem = this.locals().addCachedProperty( offset );
            var10000 = this.initializeFromContextIR$1( getFromStore, local, maybeCachedProperty );
        }
        else if ( (String) local.elem == null )
        {
            this.didInitializeCachedPropertyFromStore();
            local.elem = this.locals().addCachedProperty( offset );
            var10000 = initializeFromStoreIR$1( getFromStore, local );
        }
        else
        {
            this.didLoadLocalCachedProperty();
            var10000 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                    (String) local.elem )),maybeCachedProperty.isDefined() ? this.initializeFromContextIR$1( getFromStore, local, maybeCachedProperty )
                                                                           : initializeFromStoreIR$1( getFromStore, local ));
        }

        IntermediateRepresentation prepareOps = var10000;
        this.locals().markModifiedLocalForRefSlot( offset );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{prepareOps, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( (String) local.elem ), scala.reflect.ManifestFactory..MODULE$.classType(
            Value.class ))})));
    }

    public IntermediateRepresentation setCachedPropertyAt( final int offset, final IntermediateRepresentation value )
    {
        String local = this.locals().getLocalForRefSlot( offset );
        if ( local == null )
        {
            local = this.locals().addCachedProperty( offset );
            this.locals().markInitializedLocalForRefSlot( offset );
        }

        this.locals().markModifiedLocalForRefSlot( offset );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( local, value );
    }

    public IntermediateRepresentation isLabelSetOnNode( final IntermediateRepresentation labelToken, final int offset )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeHasLabel",
                scala.reflect.ManifestFactory..MODULE$.classType( CursorUtils.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() ), ExpressionCompiler$.MODULE$.NODE_CURSOR(), this.getLongAt( offset ), labelToken})));
    }

    public IntermediateRepresentation getNodeProperty( final IntermediateRepresentation propertyToken, final int offset )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeGetProperty",
                scala.reflect.ManifestFactory..MODULE$.classType( CursorUtils.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
        MODULE$.classType( PropertyCursor.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() ), ExpressionCompiler$.MODULE$.NODE_CURSOR(), this.getLongAt( offset ),
            ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(), propertyToken})));
    }

    public IntermediateRepresentation getRelationshipProperty( final IntermediateRepresentation propertyToken, final int offset )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationshipGetProperty",
                scala.reflect.ManifestFactory..MODULE$.classType( CursorUtils.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
        MODULE$.classType( PropertyCursor.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() ), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), this.getLongAt( offset ),
            ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(), propertyToken})));
    }

    public IntermediateRepresentation getProperty( final String key, final IntermediateRepresentation container )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyGet", scala.reflect.ManifestFactory..MODULE$.classType(
                CursorUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( String.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( PropertyCursor.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( key ), container,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() ), ExpressionCompiler$.MODULE$.DB_ACCESS(), ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()})))
        ;
    }

    public IntermediateRepresentation writeLocalsToSlots()
    {
        ArrayBuffer writeOps = new ArrayBuffer();
        ArrayBuffer writeLongSlotOps = new ArrayBuffer();
        ArrayBuffer writeRefSlotOps = new ArrayBuffer();
        int USE_ARRAY_COPY_THRESHOLD = 2;
        OperatorExpressionCompiler.LocalVariableSlotMapper mergedLocals = this.locals().mergeAllScopesCopy();
        IntRef firstModifiedLongSlot = IntRef.create( Integer.MAX_VALUE );
        mergedLocals.foreachLocalForLongSlot().apply( ( x0$8, x1$8, x2$7 ) -> {
            $anonfun$writeLocalsToSlots$1( this, writeLongSlotOps, firstModifiedLongSlot, BoxesRunTime.unboxToInt( x0$8 ), x1$8,
                    BoxesRunTime.unboxToBoolean( x2$7 ) );
            return BoxedUnit.UNIT;
        } );
        IntRef firstModifiedRefSlot = IntRef.create( Integer.MAX_VALUE );
        mergedLocals.foreachLocalForRefSlot().apply( ( x0$9, x1$9, x2$8 ) -> {
            $anonfun$writeLocalsToSlots$2( this, writeRefSlotOps, firstModifiedRefSlot, BoxesRunTime.unboxToInt( x0$9 ), x1$9,
                    BoxesRunTime.unboxToBoolean( x2$8 ) );
            return BoxedUnit.UNIT;
        } );
        int nLongsToCopy = Math.min( this.nLongSlotsToCopyFromInput(), firstModifiedLongSlot.elem );
        int nRefsToCopy = Math.min( this.nRefSlotsToCopyFromInput(), firstModifiedRefSlot.elem );
        if ( nLongsToCopy <= USE_ARRAY_COPY_THRESHOLD && nRefsToCopy <= USE_ARRAY_COPY_THRESHOLD )
        {
            if ( nLongsToCopy > 0 )
            {
                for ( int i = 0; i < nLongsToCopy; ++i )
                {
                    String local = mergedLocals.getLocalForLongSlot( i );
                    IntermediateRepresentation getOp =
                            local == null ? this.getLongFromExecutionContext( i, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                    OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() )) :org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.load( local );
                    writeOps.$plus$eq( this.setLongInExecutionContext( i, getOp ) );
                }
            }

            writeOps.$plus$plus$eq( writeLongSlotOps );
            if ( nRefsToCopy > 0 )
            {
                for ( int i = 0; i < nRefsToCopy; ++i )
                {
                    String local = mergedLocals.getLocalForRefSlot( i );
                    IntermediateRepresentation getOp =
                            local == null ? this.getRefFromExecutionContext( i, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                    OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() )) :org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.load( local );
                    IntermediateRepresentation var16 = this.setRefInExecutionContext( i, getOp );
                    writeRefSlotOps.$plus$eq$colon( var16 );
                }
            }

            writeOps.$plus$plus$eq( writeRefSlotOps );
        }
        else
        {
            writeOps.$plus$eq( this.doCopyFromWithExecutionContext( OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() ), nLongsToCopy,
                    nRefsToCopy ));
            writeOps.$plus$plus$eq( writeLongSlotOps );
            writeOps.$plus$plus$eq( writeRefSlotOps );
        }

        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( writeOps );
    }

    public void beginScope( final String scopeId )
    {
        this.locals().beginScope( scopeId );
    }

    public OperatorExpressionCompiler.ScopeContinuationState endInitializationScope( final boolean mergeIntoParentScope )
    {
        return this.locals().endInitializationScope( mergeIntoParentScope );
    }

    public boolean endInitializationScope$default$1()
    {
        return true;
    }

    public OperatorExpressionCompiler.ScopeLocalsState endScope( final boolean mergeIntoParentScope )
    {
        return this.locals().endScope( mergeIntoParentScope );
    }

    public boolean endScope$default$1()
    {
        return false;
    }

    public Seq<Tuple2<Object,String>> getAllLocalsForLongSlots()
    {
        ArrayBuffer all = new ArrayBuffer();
        this.locals().foreachLocalForLongSlot( ( x0$10, x1$10, x2$9 ) -> {
            $anonfun$getAllLocalsForLongSlots$1( all, BoxesRunTime.unboxToInt( x0$10 ), x1$10, BoxesRunTime.unboxToBoolean( x2$9 ) );
            return BoxedUnit.UNIT;
        } );
        return all;
    }

    public Seq<Tuple2<Object,String>> getAllLocalsForRefSlots()
    {
        ArrayBuffer all = new ArrayBuffer();
        this.locals().foreachLocalForRefSlot( ( x0$11, x1$11, x2$10 ) -> {
            $anonfun$getAllLocalsForRefSlots$1( all, BoxesRunTime.unboxToInt( x0$11 ), x1$11, BoxesRunTime.unboxToBoolean( x2$10 ) );
            return BoxedUnit.UNIT;
        } );
        return all;
    }

    public Seq<Tuple2<Object,String>> getAllLocalsForCachedProperties()
    {
        ArrayBuffer all = new ArrayBuffer();
        this.locals().foreachCachedProperty( ( x0$12, x1$12 ) -> {
            $anonfun$getAllLocalsForCachedProperties$1( all, BoxesRunTime.unboxToInt( x0$12 ), x1$12 );
            return BoxedUnit.UNIT;
        } );
        return all;
    }

    private final IntermediateRepresentation initializeFromContextIR$1( final IntermediateRepresentation getFromStore$1, final ObjectRef local$1,
            final Option maybeCachedProperty$1 )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( (String) local$1.elem,
                    this.getCachedPropertyFromExecutionContext( ((RefSlot) maybeCachedProperty$1.get()).offset(),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNull(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( (String) local$1.elem )),initializeFromStoreIR$1( getFromStore$1, local$1 ))})));
    }

    public static class LocalVariableSlotMapper implements Product, Serializable
    {
        private final String scopeId;
        private final SlotConfiguration slots;
        private final String[] longSlotToLocal;
        private final boolean[] longSlotToLocalModified;
        private final boolean[] longSlotToLocalInitialized;
        private final String[] refSlotToLocal;
        private final boolean[] refSlotToLocalModified;
        private final boolean[] refSlotToLocalInitialized;
        private final String[] cachedProperties;
        private final Function1<Function3<Object,String,Object,BoxedUnit>,BoxedUnit> foreachLocalForLongSlot;
        private final Function1<Function3<Object,String,Object,BoxedUnit>,BoxedUnit> foreachLocalForRefSlot;

        public LocalVariableSlotMapper( final String scopeId, final SlotConfiguration slots, final String[] longSlotToLocal,
                final boolean[] longSlotToLocalModified, final boolean[] longSlotToLocalInitialized, final String[] refSlotToLocal,
                final boolean[] refSlotToLocalModified, final boolean[] refSlotToLocalInitialized, final String[] cachedProperties )
        {
            this.scopeId = scopeId;
            this.slots = slots;
            this.longSlotToLocal = longSlotToLocal;
            this.longSlotToLocalModified = longSlotToLocalModified;
            this.longSlotToLocalInitialized = longSlotToLocalInitialized;
            this.refSlotToLocal = refSlotToLocal;
            this.refSlotToLocalModified = refSlotToLocalModified;
            this.refSlotToLocalInitialized = refSlotToLocalInitialized;
            this.cachedProperties = cachedProperties;
            Product.$init$( this );
            this.foreachLocalForLongSlot = ( f ) -> {
                $anonfun$foreachLocalForLongSlot$1( this, f );
                return BoxedUnit.UNIT;
            };
            this.foreachLocalForRefSlot = ( f ) -> {
                $anonfun$foreachLocalForRefSlot$1( this, f );
                return BoxedUnit.UNIT;
            };
        }

        private static final void addField$1( final Field f, final String local, final ArrayBuffer fields$1, final ArrayBuffer saveOps$1,
                final ArrayBuffer restoreOps$1 )
        {
            fields$1.$plus$eq( f );
            saveOps$1.$plus$eq( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( f,
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( local )));
            restoreOps$1.$plus$eq( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( local,
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f )));
        }

        public String scopeId()
        {
            return this.scopeId;
        }

        public SlotConfiguration slots()
        {
            return this.slots;
        }

        private String[] longSlotToLocal()
        {
            return this.longSlotToLocal;
        }

        private boolean[] longSlotToLocalModified()
        {
            return this.longSlotToLocalModified;
        }

        private boolean[] longSlotToLocalInitialized()
        {
            return this.longSlotToLocalInitialized;
        }

        private String[] refSlotToLocal()
        {
            return this.refSlotToLocal;
        }

        private boolean[] refSlotToLocalModified()
        {
            return this.refSlotToLocalModified;
        }

        private boolean[] refSlotToLocalInitialized()
        {
            return this.refSlotToLocalInitialized;
        }

        private String[] cachedProperties()
        {
            return this.cachedProperties;
        }

        public OperatorExpressionCompiler.LocalVariableSlotMapper copy()
        {
            return new OperatorExpressionCompiler.LocalVariableSlotMapper( this.scopeId(), this.slots(),
                    (String[]) Arrays.copyOf( (Object[]) this.longSlotToLocal(), this.longSlotToLocal().length ),
                    Arrays.copyOf( this.longSlotToLocalModified(), this.longSlotToLocalModified().length ),
                    Arrays.copyOf( this.longSlotToLocalInitialized(), this.longSlotToLocalInitialized().length ),
                    (String[]) Arrays.copyOf( (Object[]) this.refSlotToLocal(), this.refSlotToLocal().length ),
                    Arrays.copyOf( this.refSlotToLocalModified(), this.refSlotToLocalModified().length ),
                    Arrays.copyOf( this.refSlotToLocalInitialized(), this.refSlotToLocalInitialized().length ),
                    (String[]) Arrays.copyOf( (Object[]) this.cachedProperties(), this.cachedProperties().length ) );
        }

        public String addLocalForLongSlot( final int offset )
        {
            String local = (new StringBuilder( 8 )).append( "longSlot" ).append( offset ).toString();
            this.longSlotToLocal()[offset] = local;
            return local;
        }

        public String addLocalForRefSlot( final int offset )
        {
            String local = (new StringBuilder( 7 )).append( "refSlot" ).append( offset ).toString();
            this.refSlotToLocal()[offset] = local;
            return local;
        }

        public String addCachedProperty( final int offset )
        {
            String refslot = this.addLocalForRefSlot( offset );
            this.cachedProperties()[offset] = refslot;
            return refslot;
        }

        public void markModifiedLocalForLongSlot( final int offset )
        {
            this.longSlotToLocalModified()[offset] = true;
        }

        public void markModifiedLocalForRefSlot( final int offset )
        {
            this.refSlotToLocalModified()[offset] = true;
        }

        public void markInitializedLocalForLongSlot( final int offset )
        {
            this.longSlotToLocalInitialized()[offset] = true;
        }

        public void markInitializedLocalForRefSlot( final int offset )
        {
            this.refSlotToLocalInitialized()[offset] = true;
        }

        public String getLocalForLongSlot( final int offset )
        {
            return this.longSlotToLocal()[offset];
        }

        public String getLocalForRefSlot( final int offset )
        {
            return this.refSlotToLocal()[offset];
        }

        public Function1<Function3<Object,String,Object,BoxedUnit>,BoxedUnit> foreachLocalForLongSlot()
        {
            return this.foreachLocalForLongSlot;
        }

        public Function1<Function3<Object,String,Object,BoxedUnit>,BoxedUnit> foreachLocalForRefSlot()
        {
            return this.foreachLocalForRefSlot;
        }

        private void foreachLocalFor( final String[] slotToLocal, final boolean[] slotToLocalModified, final Function3<Object,String,Object,BoxedUnit> f )
        {
            for ( int i = 0; i < slotToLocal.length; ++i )
            {
                String name = slotToLocal[i];
                if ( name != null )
                {
                    boolean modified = slotToLocalModified[i];
                    f.apply( BoxesRunTime.boxToInteger( i ), name, BoxesRunTime.boxToBoolean( modified ) );
                }
                else
                {
                    BoxedUnit var10000 = BoxedUnit.UNIT;
                }
            }
        }

        public void foreachCachedProperty( final Function2<Object,String,BoxedUnit> f )
        {
            for ( int i = 0; i < this.cachedProperties().length; ++i )
            {
                String cp = this.cachedProperties()[i];
                if ( cp != null )
                {
                    f.apply( BoxesRunTime.boxToInteger( i ), cp );
                }
                else
                {
                    BoxedUnit var10000 = BoxedUnit.UNIT;
                }
            }
        }

        public void merge( final OperatorExpressionCompiler.LocalVariableSlotMapper other )
        {
            other.foreachLocalForLongSlot().apply( ( x0$1, x1$1, x2$1 ) -> {
                $anonfun$merge$1( this, BoxesRunTime.unboxToInt( x0$1 ), x1$1, BoxesRunTime.unboxToBoolean( x2$1 ) );
                return BoxedUnit.UNIT;
            } );
            other.foreachLocalForRefSlot().apply( ( x0$2, x1$2, x2$2 ) -> {
                $anonfun$merge$2( this, BoxesRunTime.unboxToInt( x0$2 ), x1$2, BoxesRunTime.unboxToBoolean( x2$2 ) );
                return BoxedUnit.UNIT;
            } );
            other.foreachCachedProperty( ( x0$3, x1$3 ) -> {
                $anonfun$merge$3( this, BoxesRunTime.unboxToInt( x0$3 ), x1$3 );
                return BoxedUnit.UNIT;
            } );
        }

        public OperatorExpressionCompiler.ScopeLocalsState genScopeLocalsState( final ExpressionCompiler codeGen,
                final IntermediateRepresentation inputContext )
        {
            ArrayBuffer locals = new ArrayBuffer();
            this.foreachLocalForLongSlot().apply( ( x0$4, x1$4, x2$3 ) -> {
                $anonfun$genScopeLocalsState$1( this, codeGen, inputContext, locals, BoxesRunTime.unboxToInt( x0$4 ), x1$4,
                        BoxesRunTime.unboxToBoolean( x2$3 ) );
                return BoxedUnit.UNIT;
            } );
            this.foreachLocalForRefSlot().apply( ( x0$5, x1$5, x2$4 ) -> {
                $anonfun$genScopeLocalsState$2( this, codeGen, inputContext, locals, BoxesRunTime.unboxToInt( x0$5 ), x1$5,
                        BoxesRunTime.unboxToBoolean( x2$4 ) );
                return BoxedUnit.UNIT;
            } );
            ArrayBuffer declarations = new ArrayBuffer();
            ArrayBuffer assignments = new ArrayBuffer();
            locals.foreach( ( lv ) -> {
                declarations.$plus$eq( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( lv.typ(), lv.name() ));
                return assignments.$plus$eq( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( lv.name(), lv.value() ));
            } ); return new OperatorExpressionCompiler.ScopeLocalsState( locals, declarations, assignments );
        }

        public OperatorExpressionCompiler.ScopeContinuationState genScopeContinuationState( final ExpressionCompiler codeGen,
                final IntermediateRepresentation inputContext )
        {
            ArrayBuffer fields = new ArrayBuffer();
            ArrayBuffer saveOps = new ArrayBuffer();
            ArrayBuffer restoreOps = new ArrayBuffer();
            this.foreachLocalForLongSlot().apply( ( x0$6, x1$6, x2$5 ) -> {
                $anonfun$genScopeContinuationState$1( fields, saveOps, restoreOps, BoxesRunTime.unboxToInt( x0$6 ), x1$6, BoxesRunTime.unboxToBoolean( x2$5 ) );
                return BoxedUnit.UNIT;
            } );
            this.foreachLocalForRefSlot().apply( ( x0$7, x1$7, x2$6 ) -> {
                $anonfun$genScopeContinuationState$2( fields, saveOps, restoreOps, BoxesRunTime.unboxToInt( x0$7 ), x1$7, BoxesRunTime.unboxToBoolean( x2$6 ) );
                return BoxedUnit.UNIT;
            } );
            if ( fields.nonEmpty() )
            {
                InstanceField hasStateField = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.field( (new StringBuilder( 20 )).append( this.scopeId() ).append( "HasContinuationState" ).toString(),
                        scala.reflect.ManifestFactory..MODULE$.Boolean());
                fields.$plus$eq( hasStateField );
                saveOps.$plus$eq( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( hasStateField,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )));
                restoreOps.$plus$eq( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( hasStateField,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )));
            }
            else
            {
                BoxedUnit var10000 = BoxedUnit.UNIT;
            }

            OperatorExpressionCompiler.ScopeLocalsState scopeLocalsState = this.genScopeLocalsState( codeGen, inputContext );
            return new OperatorExpressionCompiler.ScopeContinuationState( fields, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( saveOps ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( restoreOps ), scopeLocalsState.declarations(), scopeLocalsState.assignments());
        }

        public String productPrefix()
        {
            return "LocalVariableSlotMapper";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.scopeId();
                break;
            case 1:
                var10000 = this.slots();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorExpressionCompiler.LocalVariableSlotMapper;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof OperatorExpressionCompiler.LocalVariableSlotMapper )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                OperatorExpressionCompiler.LocalVariableSlotMapper var4 = (OperatorExpressionCompiler.LocalVariableSlotMapper) x$1;
                                String var10000 = this.scopeId();
                                String var5 = var4.scopeId();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlotConfiguration var7 = this.slots();
                                SlotConfiguration var6 = var4.slots();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class LocalVariableSlotMapper$ implements Serializable
    {
        public static OperatorExpressionCompiler.LocalVariableSlotMapper$ MODULE$;

        static
        {
            new OperatorExpressionCompiler.LocalVariableSlotMapper$();
        }

        public LocalVariableSlotMapper$()
        {
            MODULE$ = this;
        }

        public String[] $lessinit$greater$default$3( final String scopeId, final SlotConfiguration slots )
        {
            return new String[slots.numberOfLongs()];
        }

        public boolean[] $lessinit$greater$default$4( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfLongs()];
        }

        public boolean[] $lessinit$greater$default$5( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfLongs()];
        }

        public String[] $lessinit$greater$default$6( final String scopeId, final SlotConfiguration slots )
        {
            return new String[slots.numberOfReferences()];
        }

        public boolean[] $lessinit$greater$default$7( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfReferences()];
        }

        public boolean[] $lessinit$greater$default$8( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfReferences()];
        }

        public String[] $lessinit$greater$default$9( final String scopeId, final SlotConfiguration slots )
        {
            return new String[slots.numberOfReferences()];
        }

        public final String toString()
        {
            return "LocalVariableSlotMapper";
        }

        public OperatorExpressionCompiler.LocalVariableSlotMapper apply( final String scopeId, final SlotConfiguration slots, final String[] longSlotToLocal,
                final boolean[] longSlotToLocalModified, final boolean[] longSlotToLocalInitialized, final String[] refSlotToLocal,
                final boolean[] refSlotToLocalModified, final boolean[] refSlotToLocalInitialized, final String[] cachedProperties )
        {
            return new OperatorExpressionCompiler.LocalVariableSlotMapper( scopeId, slots, longSlotToLocal, longSlotToLocalModified, longSlotToLocalInitialized,
                    refSlotToLocal, refSlotToLocalModified, refSlotToLocalInitialized, cachedProperties );
        }

        public String[] apply$default$3( final String scopeId, final SlotConfiguration slots )
        {
            return new String[slots.numberOfLongs()];
        }

        public boolean[] apply$default$4( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfLongs()];
        }

        public boolean[] apply$default$5( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfLongs()];
        }

        public String[] apply$default$6( final String scopeId, final SlotConfiguration slots )
        {
            return new String[slots.numberOfReferences()];
        }

        public boolean[] apply$default$7( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfReferences()];
        }

        public boolean[] apply$default$8( final String scopeId, final SlotConfiguration slots )
        {
            return new boolean[slots.numberOfReferences()];
        }

        public String[] apply$default$9( final String scopeId, final SlotConfiguration slots )
        {
            return new String[slots.numberOfReferences()];
        }

        public Option<Tuple2<String,SlotConfiguration>> unapply( final OperatorExpressionCompiler.LocalVariableSlotMapper x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.scopeId(), x$0.slots() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class LocalsForSlots implements Product, Serializable
    {
        private final OperatorExpressionCompiler operatorExpressionCompiler;
        private final SlotConfiguration slots;
        private final OperatorExpressionCompiler.LocalVariableSlotMapper rootScope;
        private List<OperatorExpressionCompiler.LocalVariableSlotMapper> scopeStack;

        public LocalsForSlots( final OperatorExpressionCompiler operatorExpressionCompiler )
        {
            this.operatorExpressionCompiler = operatorExpressionCompiler;
            Product.$init$( this );
            this.slots = operatorExpressionCompiler.slots();
            String x$6 = "root";
            SlotConfiguration x$7 = this.slots();
            String[] x$8 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$3( x$6, x$7 );
            boolean[] x$9 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$4( x$6, x$7 );
            boolean[] x$10 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$5( x$6, x$7 );
            String[] x$11 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$6( x$6, x$7 );
            boolean[] x$12 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$7( x$6, x$7 );
            boolean[] x$13 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$8( x$6, x$7 );
            String[] x$14 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$9( x$6, x$7 );
            this.rootScope = new OperatorExpressionCompiler.LocalVariableSlotMapper( x$6, x$7, x$8, x$9, x$10, x$11, x$12, x$13, x$14 );
            OperatorExpressionCompiler.LocalVariableSlotMapper var11 = this.rootScope();
            this.scopeStack = scala.collection.immutable.Nil..MODULE$.$colon$colon( var11 );
        }

        public OperatorExpressionCompiler operatorExpressionCompiler()
        {
            return this.operatorExpressionCompiler;
        }

        private SlotConfiguration slots()
        {
            return this.slots;
        }

        private OperatorExpressionCompiler.LocalVariableSlotMapper rootScope()
        {
            return this.rootScope;
        }

        private List<OperatorExpressionCompiler.LocalVariableSlotMapper> scopeStack()
        {
            return this.scopeStack;
        }

        private void scopeStack_$eq( final List<OperatorExpressionCompiler.LocalVariableSlotMapper> x$1 )
        {
            this.scopeStack = x$1;
        }

        public void beginScope( final String scopeId )
        {
            SlotConfiguration x$16 = this.slots();
            String[] x$17 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$3( scopeId, x$16 );
            boolean[] x$18 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$4( scopeId, x$16 );
            boolean[] x$19 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$5( scopeId, x$16 );
            String[] x$20 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$6( scopeId, x$16 );
            boolean[] x$21 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$7( scopeId, x$16 );
            boolean[] x$22 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$8( scopeId, x$16 );
            String[] x$23 = OperatorExpressionCompiler.LocalVariableSlotMapper$.MODULE$.apply$default$9( scopeId, x$16 );
            OperatorExpressionCompiler.LocalVariableSlotMapper localVariableSlotMapper =
                    new OperatorExpressionCompiler.LocalVariableSlotMapper( scopeId, x$16, x$17, x$18, x$19, x$20, x$21, x$22, x$23 );
            this.scopeStack_$eq( this.scopeStack().$colon$colon( localVariableSlotMapper ) );
        }

        public OperatorExpressionCompiler.ScopeLocalsState endScope( final boolean mergeIntoParentScope )
        {
            return (OperatorExpressionCompiler.ScopeLocalsState) this.endScope( ( x$3 ) -> {
                return x$3.genScopeLocalsState( this.operatorExpressionCompiler(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() ));
            }, mergeIntoParentScope );
        }

        public OperatorExpressionCompiler.ScopeContinuationState endInitializationScope( final boolean mergeIntoParentScope )
        {
            return (OperatorExpressionCompiler.ScopeContinuationState) this.endScope( ( x$4 ) -> {
                return x$4.genScopeContinuationState( this.operatorExpressionCompiler(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() ));
            }, mergeIntoParentScope );
        }

        private <T> T endScope( final Function1<OperatorExpressionCompiler.LocalVariableSlotMapper,T> genState, final boolean mergeIntoParentScope )
        {
            OperatorExpressionCompiler.LocalVariableSlotMapper endedScope = (OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head();
            this.scopeStack_$eq( (List) this.scopeStack().tail() );
            Object state = genState.apply( endedScope );
            if ( mergeIntoParentScope )
            {
                ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).merge( endedScope );
            }

            return state;
        }

        public OperatorExpressionCompiler.LocalVariableSlotMapper mergeAllScopesCopy()
        {
            List s = this.scopeStack();
            OperatorExpressionCompiler.LocalVariableSlotMapper scope = ((OperatorExpressionCompiler.LocalVariableSlotMapper) s.head()).copy();

            while ( true )
            {
                Object var10000 = s.tail();
                scala.collection.immutable.Nil.var3 = scala.collection.immutable.Nil..MODULE$;
                if ( var10000 == null )
                {
                    if ( var3 == null )
                    {
                        break;
                    }
                }
                else if ( var10000.equals( var3 ) )
                {
                    break;
                }

                s = (List) s.tail();
                scope.merge( (OperatorExpressionCompiler.LocalVariableSlotMapper) s.head() );
            }

            return scope;
        }

        public String addLocalForLongSlot( final int offset )
        {
            return ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).addLocalForLongSlot( offset );
        }

        public String addLocalForRefSlot( final int offset )
        {
            return ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).addLocalForRefSlot( offset );
        }

        public String addCachedProperty( final int offset )
        {
            return ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).addCachedProperty( offset );
        }

        public void markModifiedLocalForLongSlot( final int offset )
        {
            ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).markModifiedLocalForLongSlot( offset );
        }

        public void markModifiedLocalForRefSlot( final int offset )
        {
            ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).markModifiedLocalForRefSlot( offset );
        }

        public void markInitializedLocalForLongSlot( final int offset )
        {
            ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).markInitializedLocalForLongSlot( offset );
        }

        public void markInitializedLocalForRefSlot( final int offset )
        {
            ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).markInitializedLocalForRefSlot( offset );
        }

        public String getLocalForLongSlot( final int offset )
        {
            String local = null;
            List scope = this.scopeStack();

            while ( true )
            {
                local = ((OperatorExpressionCompiler.LocalVariableSlotMapper) scope.head()).getLocalForLongSlot( offset );
                scope = (List) scope.tail();
                if ( local != null )
                {
                    break;
                }

                scala.collection.immutable.Nil.var4 = scala.collection.immutable.Nil..MODULE$;
                if ( scope == null )
                {
                    if ( var4 != null )
                    {
                        continue;
                    }
                }
                else if ( !scope.equals( var4 ) )
                {
                    continue;
                }
                break;
            }

            return local;
        }

        public String getLocalForRefSlot( final int offset )
        {
            String local = null;
            List scope = this.scopeStack();

            while ( true )
            {
                local = ((OperatorExpressionCompiler.LocalVariableSlotMapper) scope.head()).getLocalForRefSlot( offset );
                scope = (List) scope.tail();
                if ( local != null )
                {
                    break;
                }

                scala.collection.immutable.Nil.var4 = scala.collection.immutable.Nil..MODULE$;
                if ( scope == null )
                {
                    if ( var4 != null )
                    {
                        continue;
                    }
                }
                else if ( !scope.equals( var4 ) )
                {
                    continue;
                }
                break;
            }

            return local;
        }

        public void foreachLocalForLongSlot( final Function3<Object,String,Object,BoxedUnit> f )
        {
            ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).foreachLocalForLongSlot().apply( f );
        }

        public void foreachLocalForRefSlot( final Function3<Object,String,Object,BoxedUnit> f )
        {
            ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).foreachLocalForRefSlot().apply( f );
        }

        public void foreachCachedProperty( final Function2<Object,String,BoxedUnit> f )
        {
            ((OperatorExpressionCompiler.LocalVariableSlotMapper) this.scopeStack().head()).foreachCachedProperty( f );
        }

        public OperatorExpressionCompiler.LocalsForSlots copy( final OperatorExpressionCompiler operatorExpressionCompiler )
        {
            return new OperatorExpressionCompiler.LocalsForSlots( operatorExpressionCompiler );
        }

        public OperatorExpressionCompiler copy$default$1()
        {
            return this.operatorExpressionCompiler();
        }

        public String productPrefix()
        {
            return "LocalsForSlots";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.operatorExpressionCompiler();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorExpressionCompiler.LocalsForSlots;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            label47:
            {
                if ( this != x$1 )
                {
                    boolean var2;
                    if ( x$1 instanceof OperatorExpressionCompiler.LocalsForSlots )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( !var2 )
                    {
                        break label47;
                    }

                    label35:
                    {
                        label34:
                        {
                            OperatorExpressionCompiler.LocalsForSlots var4 = (OperatorExpressionCompiler.LocalsForSlots) x$1;
                            OperatorExpressionCompiler var10000 = this.operatorExpressionCompiler();
                            OperatorExpressionCompiler var5 = var4.operatorExpressionCompiler();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label34;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label34;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label35;
                            }
                        }

                        var6 = false;
                    }

                    if ( !var6 )
                    {
                        break label47;
                    }
                }

                var6 = true;
                return var6;
            }

            var6 = false;
            return var6;
        }
    }

    public static class LocalsForSlots$ extends AbstractFunction1<OperatorExpressionCompiler,OperatorExpressionCompiler.LocalsForSlots> implements Serializable
    {
        public static OperatorExpressionCompiler.LocalsForSlots$ MODULE$;

        static
        {
            new OperatorExpressionCompiler.LocalsForSlots$();
        }

        public LocalsForSlots$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "LocalsForSlots";
        }

        public OperatorExpressionCompiler.LocalsForSlots apply( final OperatorExpressionCompiler operatorExpressionCompiler )
        {
            return new OperatorExpressionCompiler.LocalsForSlots( operatorExpressionCompiler );
        }

        public Option<OperatorExpressionCompiler> unapply( final OperatorExpressionCompiler.LocalsForSlots x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( x$0.operatorExpressionCompiler() ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class ScopeContinuationState implements Product, Serializable
    {
        private final Seq<Field> fields;
        private final IntermediateRepresentation saveStateIR;
        private final IntermediateRepresentation restoreStateIR;
        private final Seq<IntermediateRepresentation> declarations;
        private final Seq<IntermediateRepresentation> assignments;

        public ScopeContinuationState( final Seq<Field> fields, final IntermediateRepresentation saveStateIR, final IntermediateRepresentation restoreStateIR,
                final Seq<IntermediateRepresentation> declarations, final Seq<IntermediateRepresentation> assignments )
        {
            this.fields = fields;
            this.saveStateIR = saveStateIR;
            this.restoreStateIR = restoreStateIR;
            this.declarations = declarations;
            this.assignments = assignments;
            Product.$init$( this );
        }

        public Seq<Field> fields()
        {
            return this.fields;
        }

        public IntermediateRepresentation saveStateIR()
        {
            return this.saveStateIR;
        }

        public IntermediateRepresentation restoreStateIR()
        {
            return this.restoreStateIR;
        }

        public Seq<IntermediateRepresentation> declarations()
        {
            return this.declarations;
        }

        public Seq<IntermediateRepresentation> assignments()
        {
            return this.assignments;
        }

        public boolean isEmpty()
        {
            return this.fields().isEmpty();
        }

        public boolean nonEmpty()
        {
            return this.fields().nonEmpty();
        }

        public OperatorExpressionCompiler.ScopeContinuationState copy( final Seq<Field> fields, final IntermediateRepresentation saveStateIR,
                final IntermediateRepresentation restoreStateIR, final Seq<IntermediateRepresentation> declarations,
                final Seq<IntermediateRepresentation> assignments )
        {
            return new OperatorExpressionCompiler.ScopeContinuationState( fields, saveStateIR, restoreStateIR, declarations, assignments );
        }

        public Seq<Field> copy$default$1()
        {
            return this.fields();
        }

        public IntermediateRepresentation copy$default$2()
        {
            return this.saveStateIR();
        }

        public IntermediateRepresentation copy$default$3()
        {
            return this.restoreStateIR();
        }

        public Seq<IntermediateRepresentation> copy$default$4()
        {
            return this.declarations();
        }

        public Seq<IntermediateRepresentation> copy$default$5()
        {
            return this.assignments();
        }

        public String productPrefix()
        {
            return "ScopeContinuationState";
        }

        public int productArity()
        {
            return 5;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.fields();
                break;
            case 1:
                var10000 = this.saveStateIR();
                break;
            case 2:
                var10000 = this.restoreStateIR();
                break;
            case 3:
                var10000 = this.declarations();
                break;
            case 4:
                var10000 = this.assignments();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorExpressionCompiler.ScopeContinuationState;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var11;
            label84:
            {
                if ( this != x$1 )
                {
                    boolean var2;
                    if ( x$1 instanceof OperatorExpressionCompiler.ScopeContinuationState )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( !var2 )
                    {
                        break label84;
                    }

                    label71:
                    {
                        label79:
                        {
                            OperatorExpressionCompiler.ScopeContinuationState var4 = (OperatorExpressionCompiler.ScopeContinuationState) x$1;
                            Seq var10000 = this.fields();
                            Seq var5 = var4.fields();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label79;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label79;
                            }

                            IntermediateRepresentation var10 = this.saveStateIR();
                            IntermediateRepresentation var6 = var4.saveStateIR();
                            if ( var10 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label79;
                                }
                            }
                            else if ( !var10.equals( var6 ) )
                            {
                                break label79;
                            }

                            var10 = this.restoreStateIR();
                            IntermediateRepresentation var7 = var4.restoreStateIR();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label79;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label79;
                            }

                            var10000 = this.declarations();
                            Seq var8 = var4.declarations();
                            if ( var10000 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label79;
                                }
                            }
                            else if ( !var10000.equals( var8 ) )
                            {
                                break label79;
                            }

                            var10000 = this.assignments();
                            Seq var9 = var4.assignments();
                            if ( var10000 == null )
                            {
                                if ( var9 != null )
                                {
                                    break label79;
                                }
                            }
                            else if ( !var10000.equals( var9 ) )
                            {
                                break label79;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label71;
                            }
                        }

                        var11 = false;
                    }

                    if ( !var11 )
                    {
                        break label84;
                    }
                }

                var11 = true;
                return var11;
            }

            var11 = false;
            return var11;
        }
    }

    public static class ScopeContinuationState$ extends
            AbstractFunction5<Seq<Field>,IntermediateRepresentation,IntermediateRepresentation,Seq<IntermediateRepresentation>,Seq<IntermediateRepresentation>,OperatorExpressionCompiler.ScopeContinuationState>
            implements Serializable
    {
        public static OperatorExpressionCompiler.ScopeContinuationState$ MODULE$;

        static
        {
            new OperatorExpressionCompiler.ScopeContinuationState$();
        }

        public ScopeContinuationState$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "ScopeContinuationState";
        }

        public OperatorExpressionCompiler.ScopeContinuationState apply( final Seq<Field> fields, final IntermediateRepresentation saveStateIR,
                final IntermediateRepresentation restoreStateIR, final Seq<IntermediateRepresentation> declarations,
                final Seq<IntermediateRepresentation> assignments )
        {
            return new OperatorExpressionCompiler.ScopeContinuationState( fields, saveStateIR, restoreStateIR, declarations, assignments );
        }

        public Option<Tuple5<Seq<Field>,IntermediateRepresentation,IntermediateRepresentation,Seq<IntermediateRepresentation>,Seq<IntermediateRepresentation>>> unapply(
                final OperatorExpressionCompiler.ScopeContinuationState x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :
            new Some( new Tuple5( x$0.fields(), x$0.saveStateIR(), x$0.restoreStateIR(), x$0.declarations(), x$0.assignments() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class ScopeLocalsState implements Product, Serializable
    {
        private final Seq<LocalVariable> locals;
        private final Seq<IntermediateRepresentation> declarations;
        private final Seq<IntermediateRepresentation> assignments;

        public ScopeLocalsState( final Seq<LocalVariable> locals, final Seq<IntermediateRepresentation> declarations,
                final Seq<IntermediateRepresentation> assignments )
        {
            this.locals = locals;
            this.declarations = declarations;
            this.assignments = assignments;
            Product.$init$( this );
        }

        public Seq<LocalVariable> locals()
        {
            return this.locals;
        }

        public Seq<IntermediateRepresentation> declarations()
        {
            return this.declarations;
        }

        public Seq<IntermediateRepresentation> assignments()
        {
            return this.assignments;
        }

        public boolean isEmpty()
        {
            return this.locals().isEmpty();
        }

        public boolean nonsEmpty()
        {
            return this.locals().nonEmpty();
        }

        public OperatorExpressionCompiler.ScopeLocalsState copy( final Seq<LocalVariable> locals, final Seq<IntermediateRepresentation> declarations,
                final Seq<IntermediateRepresentation> assignments )
        {
            return new OperatorExpressionCompiler.ScopeLocalsState( locals, declarations, assignments );
        }

        public Seq<LocalVariable> copy$default$1()
        {
            return this.locals();
        }

        public Seq<IntermediateRepresentation> copy$default$2()
        {
            return this.declarations();
        }

        public Seq<IntermediateRepresentation> copy$default$3()
        {
            return this.assignments();
        }

        public String productPrefix()
        {
            return "ScopeLocalsState";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Seq var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.locals();
                break;
            case 1:
                var10000 = this.declarations();
                break;
            case 2:
                var10000 = this.assignments();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorExpressionCompiler.ScopeLocalsState;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof OperatorExpressionCompiler.ScopeLocalsState )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                OperatorExpressionCompiler.ScopeLocalsState var4 = (OperatorExpressionCompiler.ScopeLocalsState) x$1;
                                Seq var10000 = this.locals();
                                Seq var5 = var4.locals();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                var10000 = this.declarations();
                                Seq var6 = var4.declarations();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label63;
                                }

                                var10000 = this.assignments();
                                Seq var7 = var4.assignments();
                                if ( var10000 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label54;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label72;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class ScopeLocalsState$ extends
            AbstractFunction3<Seq<LocalVariable>,Seq<IntermediateRepresentation>,Seq<IntermediateRepresentation>,OperatorExpressionCompiler.ScopeLocalsState>
            implements Serializable
    {
        public static OperatorExpressionCompiler.ScopeLocalsState$ MODULE$;

        static
        {
            new OperatorExpressionCompiler.ScopeLocalsState$();
        }

        public ScopeLocalsState$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "ScopeLocalsState";
        }

        public OperatorExpressionCompiler.ScopeLocalsState apply( final Seq<LocalVariable> locals, final Seq<IntermediateRepresentation> declarations,
                final Seq<IntermediateRepresentation> assignments )
        {
            return new OperatorExpressionCompiler.ScopeLocalsState( locals, declarations, assignments );
        }

        public Option<Tuple3<Seq<LocalVariable>,Seq<IntermediateRepresentation>,Seq<IntermediateRepresentation>>> unapply(
                final OperatorExpressionCompiler.ScopeLocalsState x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.locals(), x$0.declarations(), x$0.assignments() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
