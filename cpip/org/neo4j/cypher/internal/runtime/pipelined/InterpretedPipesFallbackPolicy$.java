package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.CypherInterpretedPipesFallbackOption;
import org.neo4j.cypher.CypherInterpretedPipesFallbackOption.disabled.;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.MatchError;

public final class InterpretedPipesFallbackPolicy$
{
    public static InterpretedPipesFallbackPolicy$ MODULE$;

    static
    {
        new InterpretedPipesFallbackPolicy$();
    }

    private InterpretedPipesFallbackPolicy$()
    {
        MODULE$ = this;
    }

    public InterpretedPipesFallbackPolicy apply( final CypherInterpretedPipesFallbackOption interpretedPipesFallbackOption, final boolean parallelExecution )
    {
        Object var3;
        if (.MODULE$.equals( interpretedPipesFallbackOption )){
        var3 = InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_DISABLED$.MODULE$;
    } else if ( org.neo4j.cypher.CypherInterpretedPipesFallbackOption.whitelistedPlansOnly..MODULE$.equals( interpretedPipesFallbackOption )){
        var3 = new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_WHITELISTED_PLANS_ONLY( parallelExecution );
    } else{
        if ( !org.neo4j.cypher.CypherInterpretedPipesFallbackOption.allPossiblePlans..MODULE$.equals( interpretedPipesFallbackOption )){
            throw new MatchError( interpretedPipesFallbackOption );
        }

        var3 = new InterpretedPipesFallbackPolicy.INTERPRETED_PIPES_FALLBACK_FOR_ALL_POSSIBLE_PLANS( parallelExecution );
    }

        return (InterpretedPipesFallbackPolicy) var3;
    }

    public CantCompileQueryException org$neo4j$cypher$internal$runtime$pipelined$InterpretedPipesFallbackPolicy$$unsupported( final String thing )
    {
        return new CantCompileQueryException( (new StringBuilder( 75 )).append( "Pipelined does not yet support the plans including `" ).append( thing ).append(
                "`, use another runtime." ).toString() );
    }
}
