package org.neo4j.cypher.internal.runtime.pipelined;

import java.util.concurrent.locks.LockSupport;

import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import scala.concurrent.duration.Duration;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ConcurrentSleeper implements Sleeper
{
    private final int workerId;
    private final Duration sleepDuration;
    private final long sleepNs;
    private int workStreak;
    private volatile Sleeper.Status status;

    public ConcurrentSleeper( final int workerId, final Duration sleepDuration )
    {
        this.workerId = workerId;
        this.sleepDuration = sleepDuration;
        this.sleepNs = sleepDuration.toNanos();
        this.workStreak = 0;
        this.status = Sleeper.ACTIVE$.MODULE$;
    }

    public static Duration $lessinit$greater$default$2()
    {
        return ConcurrentSleeper$.MODULE$.$lessinit$greater$default$2();
    }

    public int workerId()
    {
        return this.workerId;
    }

    private Duration sleepDuration()
    {
        return this.sleepDuration;
    }

    private long sleepNs()
    {
        return this.sleepNs;
    }

    private int workStreak()
    {
        return this.workStreak;
    }

    private void workStreak_$eq( final int x$1 )
    {
        this.workStreak = x$1;
    }

    private Sleeper.Status status()
    {
        return this.status;
    }

    private void status_$eq( final Sleeper.Status x$1 )
    {
        this.status = x$1;
    }

    public void reportStartWorkUnit()
    {
        this.status_$eq( Sleeper.WORKING$.MODULE$ );
    }

    public void reportStopWorkUnit()
    {
        this.status_$eq( Sleeper.ACTIVE$.MODULE$ );
        this.workStreak_$eq( this.workStreak() + 1 );
    }

    public void reportIdle()
    {
      .MODULE$.WORKERS().log( "[WORKER%2d] parked after working %d times", BoxesRunTime.boxToInteger( this.workerId() ),
            BoxesRunTime.boxToInteger( this.workStreak() ) );
        this.workStreak_$eq( 0 );
        this.status_$eq( Sleeper.SLEEPING$.MODULE$ );
        LockSupport.parkNanos( this.sleepNs() );
      .MODULE$.WORKERS().log( "[WORKER%2d] unparked", BoxesRunTime.boxToInteger( this.workerId() ) );
        this.status_$eq( Sleeper.ACTIVE$.MODULE$ );
    }

    public boolean isSleeping()
    {
        boolean var2;
        label23:
        {
            Sleeper.Status var10000 = this.status();
            Sleeper.SLEEPING$ var1 = Sleeper.SLEEPING$.MODULE$;
            if ( var10000 == null )
            {
                if ( var1 == null )
                {
                    break label23;
                }
            }
            else if ( var10000.equals( var1 ) )
            {
                break label23;
            }

            var2 = false;
            return var2;
        }

        var2 = true;
        return var2;
    }

    public boolean isWorking()
    {
        boolean var2;
        label23:
        {
            Sleeper.Status var10000 = this.status();
            Sleeper.WORKING$ var1 = Sleeper.WORKING$.MODULE$;
            if ( var10000 == null )
            {
                if ( var1 == null )
                {
                    break label23;
                }
            }
            else if ( var10000.equals( var1 ) )
            {
                break label23;
            }

            var2 = false;
            return var2;
        }

        var2 = true;
        return var2;
    }

    public String statusString()
    {
        return this.status().toString();
    }
}
