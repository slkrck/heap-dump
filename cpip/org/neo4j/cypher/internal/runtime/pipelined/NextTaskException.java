package org.neo4j.cypher.internal.runtime.pipelined;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class NextTaskException extends Exception implements Product, Serializable
{
    private final ExecutablePipeline pipeline;
    private final Throwable cause;

    public NextTaskException( final ExecutablePipeline pipeline, final Throwable cause )
    {
        super( cause );
        this.pipeline = pipeline;
        this.cause = cause;
        Product.$init$( this );
    }

    public static Option<Tuple2<ExecutablePipeline,Throwable>> unapply( final NextTaskException x$0 )
    {
        return NextTaskException$.MODULE$.unapply( var0 );
    }

    public static NextTaskException apply( final ExecutablePipeline pipeline, final Throwable cause )
    {
        return NextTaskException$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<ExecutablePipeline,Throwable>,NextTaskException> tupled()
    {
        return NextTaskException$.MODULE$.tupled();
    }

    public static Function1<ExecutablePipeline,Function1<Throwable,NextTaskException>> curried()
    {
        return NextTaskException$.MODULE$.curried();
    }

    public ExecutablePipeline pipeline()
    {
        return this.pipeline;
    }

    public Throwable cause()
    {
        return this.cause;
    }

    public NextTaskException copy( final ExecutablePipeline pipeline, final Throwable cause )
    {
        return new NextTaskException( pipeline, cause );
    }

    public ExecutablePipeline copy$default$1()
    {
        return this.pipeline();
    }

    public Throwable copy$default$2()
    {
        return this.cause();
    }

    public String productPrefix()
    {
        return "NextTaskException";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.pipeline();
            break;
        case 1:
            var10000 = this.cause();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NextTaskException;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof NextTaskException )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            NextTaskException var4 = (NextTaskException) x$1;
                            ExecutablePipeline var10000 = this.pipeline();
                            ExecutablePipeline var5 = var4.pipeline();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Throwable var7 = this.cause();
                            Throwable var6 = var4.cause();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
