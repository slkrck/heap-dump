package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.physicalplanning.OutputDefinition;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.Tuple5;
import scala.collection.Iterator;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public final class FusionPlan implements Product, Serializable
{
    private final OperatorTaskTemplate template;
    private final List<LogicalPlan> fusedPlans;
    private final List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates;
    private final List<LogicalPlan> unhandledPlans;
    private final OutputDefinition unhandledOutput;

    public FusionPlan( final OperatorTaskTemplate template, final List<LogicalPlan> fusedPlans,
            final List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates,
            final List<LogicalPlan> unhandledPlans, final OutputDefinition unhandledOutput )
    {
        this.template = template;
        this.fusedPlans = fusedPlans;
        this.argumentStates = argumentStates;
        this.unhandledPlans = unhandledPlans;
        this.unhandledOutput = unhandledOutput;
        Product.$init$( this );
    }

    public static OutputDefinition apply$default$5()
    {
        return FusionPlan$.MODULE$.apply$default$5();
    }

    public static List<LogicalPlan> apply$default$4()
    {
        return FusionPlan$.MODULE$.apply$default$4();
    }

    public static List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> apply$default$3()
    {
        return FusionPlan$.MODULE$.apply$default$3();
    }

    public static OutputDefinition $lessinit$greater$default$5()
    {
        return FusionPlan$.MODULE$.$lessinit$greater$default$5();
    }

    public static List<LogicalPlan> $lessinit$greater$default$4()
    {
        return FusionPlan$.MODULE$.$lessinit$greater$default$4();
    }

    public static List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> $lessinit$greater$default$3()
    {
        return FusionPlan$.MODULE$.$lessinit$greater$default$3();
    }

    public static Option<Tuple5<OperatorTaskTemplate,List<LogicalPlan>,List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>>,List<LogicalPlan>,OutputDefinition>> unapply(
            final FusionPlan x$0 )
    {
        return FusionPlan$.MODULE$.unapply( var0 );
    }

    public static FusionPlan apply( final OperatorTaskTemplate template, final List<LogicalPlan> fusedPlans,
            final List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates,
            final List<LogicalPlan> unhandledPlans, final OutputDefinition unhandledOutput )
    {
        return FusionPlan$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public static Function1<Tuple5<OperatorTaskTemplate,List<LogicalPlan>,List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>>,List<LogicalPlan>,OutputDefinition>,FusionPlan> tupled()
    {
        return FusionPlan$.MODULE$.tupled();
    }

    public static Function1<OperatorTaskTemplate,Function1<List<LogicalPlan>,Function1<List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>>,Function1<List<LogicalPlan>,Function1<OutputDefinition,FusionPlan>>>>> curried()
    {
        return FusionPlan$.MODULE$.curried();
    }

    public OperatorTaskTemplate template()
    {
        return this.template;
    }

    public List<LogicalPlan> fusedPlans()
    {
        return this.fusedPlans;
    }

    public List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates()
    {
        return this.argumentStates;
    }

    public List<LogicalPlan> unhandledPlans()
    {
        return this.unhandledPlans;
    }

    public OutputDefinition unhandledOutput()
    {
        return this.unhandledOutput;
    }

    public FusionPlan copy( final OperatorTaskTemplate template, final List<LogicalPlan> fusedPlans,
            final List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates,
            final List<LogicalPlan> unhandledPlans, final OutputDefinition unhandledOutput )
    {
        return new FusionPlan( template, fusedPlans, argumentStates, unhandledPlans, unhandledOutput );
    }

    public OperatorTaskTemplate copy$default$1()
    {
        return this.template();
    }

    public List<LogicalPlan> copy$default$2()
    {
        return this.fusedPlans();
    }

    public List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> copy$default$3()
    {
        return this.argumentStates();
    }

    public List<LogicalPlan> copy$default$4()
    {
        return this.unhandledPlans();
    }

    public OutputDefinition copy$default$5()
    {
        return this.unhandledOutput();
    }

    public String productPrefix()
    {
        return "FusionPlan";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.template();
            break;
        case 1:
            var10000 = this.fusedPlans();
            break;
        case 2:
            var10000 = this.argumentStates();
            break;
        case 3:
            var10000 = this.unhandledPlans();
            break;
        case 4:
            var10000 = this.unhandledOutput();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FusionPlan;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label85:
            {
                boolean var2;
                if ( x$1 instanceof FusionPlan )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label67:
                    {
                        label66:
                        {
                            label76:
                            {
                                FusionPlan var4 = (FusionPlan) x$1;
                                OperatorTaskTemplate var10000 = this.template();
                                OperatorTaskTemplate var5 = var4.template();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label76;
                                }

                                List var10 = this.fusedPlans();
                                List var6 = var4.fusedPlans();
                                if ( var10 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10.equals( var6 ) )
                                {
                                    break label76;
                                }

                                var10 = this.argumentStates();
                                List var7 = var4.argumentStates();
                                if ( var10 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10.equals( var7 ) )
                                {
                                    break label76;
                                }

                                var10 = this.unhandledPlans();
                                List var8 = var4.unhandledPlans();
                                if ( var10 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10.equals( var8 ) )
                                {
                                    break label76;
                                }

                                OutputDefinition var11 = this.unhandledOutput();
                                OutputDefinition var9 = var4.unhandledOutput();
                                if ( var11 == null )
                                {
                                    if ( var9 == null )
                                    {
                                        break label66;
                                    }
                                }
                                else if ( var11.equals( var9 ) )
                                {
                                    break label66;
                                }
                            }

                            var12 = false;
                            break label67;
                        }

                        var12 = true;
                    }

                    if ( var12 )
                    {
                        break label85;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
