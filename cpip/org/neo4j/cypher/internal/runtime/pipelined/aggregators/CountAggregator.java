package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class CountAggregator
{
    public static String toString()
    {
        return CountAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return CountAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return CountAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return CountAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return CountAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return CountAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return CountAggregator$.MODULE$.productPrefix();
    }

    public static Reducer newConcurrentReducer()
    {
        return CountAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return CountAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return CountAggregator$.MODULE$.newUpdater();
    }

    public static class CountUpdater extends CountUpdaterBase
    {
        public void update( final AnyValue value )
        {
            if ( value != Values.NO_VALUE )
            {
                this.count_$eq( this.count() + 1L );
            }
        }
    }
}
