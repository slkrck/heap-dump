package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CountDistinctUpdater implements Updater
{
    private final Set<AnyValue> seen = new HashSet();

    public Set<AnyValue> seen()
    {
        return this.seen;
    }

    public void update( final AnyValue value )
    {
        if ( value != Values.NO_VALUE )
        {
            this.seen().add( value );
        }
    }
}
