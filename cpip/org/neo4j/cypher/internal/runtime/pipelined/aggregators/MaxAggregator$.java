package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.values.AnyValue;
import org.neo4j.values.AnyValues;
import org.neo4j.values.storable.Values;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class MaxAggregator$ implements Aggregator, Product, Serializable
{
    public static MaxAggregator$ MODULE$;

    static
    {
        new MaxAggregator$();
    }

    private MaxAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new MaxAggregator.MaxUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new MaxAggregator.MaxStandardReducer();
    }

    public Reducer newConcurrentReducer()
    {
        return new MaxAggregator.MaxConcurrentReducer();
    }

    public boolean shouldUpdate( final AnyValue max, final AnyValue value )
    {
        return (max == Values.NO_VALUE || AnyValues.COMPARATOR.compare( max, value ) < 0) && value != Values.NO_VALUE;
    }

    public String productPrefix()
    {
        return "MaxAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MaxAggregator$;
    }

    public int hashCode()
    {
        return 762118189;
    }

    public String toString()
    {
        return "MaxAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
