package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class CountDistinctConcurrentReducer implements Reducer
{
    private final Set<AnyValue> seen = ConcurrentHashMap.newKeySet();
    private final AtomicLong count = new AtomicLong( 0L );

    private Set<AnyValue> seen()
    {
        return this.seen;
    }

    private AtomicLong count()
    {
        return this.count;
    }

    public void update( final Updater updater )
    {
        if ( updater instanceof CountDistinctUpdater )
        {
            CountDistinctUpdater var4 = (CountDistinctUpdater) updater;
            var4.seen().forEach( ( e ) -> {
                if ( this.seen().add( e ) )
                {
                    this.count().incrementAndGet();
                }
            } );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return Values.longValue( this.count().get() );
    }
}
