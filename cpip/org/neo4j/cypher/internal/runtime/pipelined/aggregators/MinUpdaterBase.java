package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class MinUpdaterBase implements Updater
{
    private AnyValue min;

    public MinUpdaterBase()
    {
        this.min = Values.NO_VALUE;
    }

    public AnyValue min()
    {
        return this.min;
    }

    public void min_$eq( final AnyValue x$1 )
    {
        this.min = x$1;
    }

    public void update( final AnyValue value )
    {
        if ( value != Values.NO_VALUE && MinAggregator$.MODULE$.shouldUpdate( this.min(), value ) )
        {
            this.min_$eq( value );
        }
    }
}
