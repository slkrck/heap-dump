package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.MatchError;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;
import scala.reflect.ClassTag.;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class CollectStandardReducer implements Reducer
{
    private final QueryMemoryTracker memoryTracker;
    private final ArrayBuffer<ListValue> collections;

    public CollectStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        this.memoryTracker = memoryTracker;
        this.collections = new ArrayBuffer();
    }

    private ArrayBuffer<ListValue> collections()
    {
        return this.collections;
    }

    public void update( final Updater updater )
    {
        if ( updater instanceof CollectUpdater )
        {
            CollectUpdater var4 = (CollectUpdater) updater;
            ListValue value = VirtualValues.fromList( var4.collection() );
            this.collections().$plus$eq( value );
            this.memoryTracker.allocated( value );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return VirtualValues.concat( (ListValue[]) this.collections().toArray(.MODULE$.apply( ListValue.class ) ));
    }
}
