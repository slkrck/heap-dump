package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

public class Aggregators
{
    public static final Aggregator COUNT_STAR;
    public static final Aggregator COUNT;
    public static final Aggregator COUNT_DISTINCT;
    public static final Aggregator SUM;
    public static final Aggregator AVG;
    public static final Aggregator MAX;
    public static final Aggregator MIN;
    public static final Aggregator COLLECT;

    static
    {
        COUNT_STAR = CountStarAggregator$.MODULE$;
        COUNT = CountAggregator$.MODULE$;
        COUNT_DISTINCT = CountDistinctAggregator$.MODULE$;
        SUM = SumAggregator$.MODULE$;
        AVG = AvgAggregator$.MODULE$;
        MAX = MaxAggregator$.MODULE$;
        MIN = MinAggregator$.MODULE$;
        COLLECT = CollectAggregator$.MODULE$;
    }
}
