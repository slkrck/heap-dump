package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Updater
{
    void update( final AnyValue value );
}
