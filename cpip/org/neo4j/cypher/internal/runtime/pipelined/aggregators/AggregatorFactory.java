package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.v4_0.expressions.CountStar;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.Null.;
import org.neo4j.cypher.internal.v4_0.expressions.functions.AggregatingFunction;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.exceptions.SyntaxException;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class AggregatorFactory implements Product, Serializable
{
    private final PhysicalPlan physicalPlan;

    public AggregatorFactory( final PhysicalPlan physicalPlan )
    {
        this.physicalPlan = physicalPlan;
        Product.$init$( this );
    }

    public static Option<PhysicalPlan> unapply( final AggregatorFactory x$0 )
    {
        return AggregatorFactory$.MODULE$.unapply( var0 );
    }

    public static AggregatorFactory apply( final PhysicalPlan physicalPlan )
    {
        return AggregatorFactory$.MODULE$.apply( var0 );
    }

    public static <A> Function1<PhysicalPlan,A> andThen( final Function1<AggregatorFactory,A> g )
    {
        return AggregatorFactory$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,AggregatorFactory> compose( final Function1<A,PhysicalPlan> g )
    {
        return AggregatorFactory$.MODULE$.compose( var0 );
    }

    public PhysicalPlan physicalPlan()
    {
        return this.physicalPlan;
    }

    public Tuple2<Aggregator,Expression> newAggregator( final Expression expression )
    {
        if ( expression.arguments().exists( ( x$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$newAggregator$1( x$1 ) );
        } ) )
        {
            throw new SyntaxException( "Can't use aggregate functions inside of aggregate functions." );
        }
        else if ( !expression.isDeterministic() )
        {
            throw new SyntaxException( "Can't use non-deterministic (random) functions inside of aggregate functions." );
        }
        else
        {
            Tuple2 var2;
            if ( expression instanceof CountStar )
            {
                var2 = new Tuple2( CountStarAggregator$.MODULE$,.MODULE$.NULL());
            }
            else
            {
                if ( !(expression instanceof FunctionInvocation) )
                {
                    throw new SyntaxException(
                            (new StringBuilder( 56 )).append( "Unexpected expression in aggregating function position: " ).append( expression ).toString() );
                }

                Tuple2 var3;
                label77:
                {
                    FunctionInvocation var5 = (FunctionInvocation) expression;
                    boolean var6 = false;
                    Object var7 = null;
                    boolean var8 = false;
                    AggregatingFunction var9 = null;
                    Function var10 = var5.function();
                    if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Count..MODULE$.equals( var10 )){
                    var6 = true;
                    if ( var5.distinct() )
                    {
                        var3 = new Tuple2( CountDistinctAggregator$.MODULE$, var5.arguments().head() );
                        break label77;
                    }
                }

                    if ( var10 instanceof AggregatingFunction )
                    {
                        var8 = true;
                        var9 = (AggregatingFunction) var10;
                        if ( var5.distinct() )
                        {
                            throw new CantCompileQueryException( "Morsel does not yet support Distinct aggregating functions, use another runtime." );
                        }
                    }

                    if ( var6 )
                    {
                        var3 = new Tuple2( CountAggregator$.MODULE$, var5.arguments().head() );
                    }
                    else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Sum..MODULE$.equals( var10 )){
                    var3 = new Tuple2( SumAggregator$.MODULE$, var5.arguments().head() );
                } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Avg..MODULE$.equals( var10 )){
                    var3 = new Tuple2( AvgAggregator$.MODULE$, var5.arguments().head() );
                } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Max..MODULE$.equals( var10 )){
                    var3 = new Tuple2( MaxAggregator$.MODULE$, var5.arguments().head() );
                } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Min..MODULE$.equals( var10 )){
                    var3 = new Tuple2( MinAggregator$.MODULE$, var5.arguments().head() );
                } else{
                    if ( !org.neo4j.cypher.internal.v4_0.expressions.functions.Collect..MODULE$.equals( var10 )){
                        if ( var8 )
                        {
                            throw new CantCompileQueryException(
                                    (new StringBuilder( 77 )).append( "Morsel does not yet support the Aggregating function `" ).append( var5.name() ).append(
                                            "`, use another runtime." ).toString() );
                        }

                        throw new SyntaxException(
                                (new StringBuilder( 54 )).append( "Unexpected function in aggregating function position: " ).append( var5.name() ).toString() );
                    }

                    var3 = new Tuple2( CollectAggregator$.MODULE$, var5.arguments().head() );
                }
                }

                var2 = var3;
            }

            return var2;
        }
    }

    public AggregatorFactory copy( final PhysicalPlan physicalPlan )
    {
        return new AggregatorFactory( physicalPlan );
    }

    public PhysicalPlan copy$default$1()
    {
        return this.physicalPlan();
    }

    public String productPrefix()
    {
        return "AggregatorFactory";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.physicalPlan();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AggregatorFactory;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof AggregatorFactory )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        AggregatorFactory var4 = (AggregatorFactory) x$1;
                        PhysicalPlan var10000 = this.physicalPlan();
                        PhysicalPlan var5 = var4.physicalPlan();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
