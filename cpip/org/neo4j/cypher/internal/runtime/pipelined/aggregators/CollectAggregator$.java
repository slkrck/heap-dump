package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class CollectAggregator$ implements Aggregator, Product, Serializable
{
    public static CollectAggregator$ MODULE$;

    static
    {
        new CollectAggregator$();
    }

    private CollectAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new CollectUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new CollectStandardReducer( memoryTracker );
    }

    public Reducer newConcurrentReducer()
    {
        return new CollectConcurrentReducer();
    }

    public String productPrefix()
    {
        return "CollectAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CollectAggregator$;
    }

    public int hashCode()
    {
        return 1650216051;
    }

    public String toString()
    {
        return "CollectAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
