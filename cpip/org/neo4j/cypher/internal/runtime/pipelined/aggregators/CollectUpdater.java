package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.ArrayList;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CollectUpdater implements Updater
{
    private final ArrayList<AnyValue> collection = new ArrayList();

    public ArrayList<AnyValue> collection()
    {
        return this.collection;
    }

    public void update( final AnyValue value )
    {
        if ( value != Values.NO_VALUE )
        {
            this.collection().add( value );
        }
    }
}
