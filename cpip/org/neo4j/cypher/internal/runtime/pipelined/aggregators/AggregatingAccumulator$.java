package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

public final class AggregatingAccumulator$
{
    public static AggregatingAccumulator$ MODULE$;

    static
    {
        new AggregatingAccumulator$();
    }

    private AggregatingAccumulator$()
    {
        MODULE$ = this;
    }
}
