package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.values.AnyValue;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class CountStarAggregator
{
    public static String toString()
    {
        return CountStarAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return CountStarAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return CountStarAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return CountStarAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return CountStarAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return CountStarAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return CountStarAggregator$.MODULE$.productPrefix();
    }

    public static Reducer newConcurrentReducer()
    {
        return CountStarAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return CountStarAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return CountStarAggregator$.MODULE$.newUpdater();
    }

    public static class CountStarUpdater extends CountUpdaterBase
    {
        public void update( final AnyValue value )
        {
            this.count_$eq( this.count() + 1L );
        }
    }
}
