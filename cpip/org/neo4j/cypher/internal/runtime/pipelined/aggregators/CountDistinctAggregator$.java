package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class CountDistinctAggregator$ implements Aggregator, Product, Serializable
{
    public static CountDistinctAggregator$ MODULE$;

    static
    {
        new CountDistinctAggregator$();
    }

    private CountDistinctAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new CountDistinctUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new CountDistinctStandardReducer();
    }

    public Reducer newConcurrentReducer()
    {
        return new CountDistinctConcurrentReducer();
    }

    public String productPrefix()
    {
        return "CountDistinctAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CountDistinctAggregator$;
    }

    public int hashCode()
    {
        return -569009356;
    }

    public String toString()
    {
        return "CountDistinctAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
