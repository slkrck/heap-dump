package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class CountDistinctStandardReducer implements Reducer
{
    private final Set<AnyValue> seen = new HashSet();
    private long count = 0L;

    private Set<AnyValue> seen()
    {
        return this.seen;
    }

    private long count()
    {
        return this.count;
    }

    private void count_$eq( final long x$1 )
    {
        this.count = x$1;
    }

    public void update( final Updater updater )
    {
        if ( updater instanceof CountDistinctUpdater )
        {
            CountDistinctUpdater var4 = (CountDistinctUpdater) updater;
            var4.seen().forEach( ( e ) -> {
                if ( this.seen().add( e ) )
                {
                    this.count_$eq( this.count() + 1L );
                }
            } );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return Values.longValue( this.count() );
    }
}
