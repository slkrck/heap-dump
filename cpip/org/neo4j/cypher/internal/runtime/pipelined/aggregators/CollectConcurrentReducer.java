package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class CollectConcurrentReducer implements Reducer
{
    private final ConcurrentLinkedQueue<ListValue> collections = new ConcurrentLinkedQueue();

    private ConcurrentLinkedQueue<ListValue> collections()
    {
        return this.collections;
    }

    public void update( final Updater updater )
    {
        if ( updater instanceof CollectUpdater )
        {
            CollectUpdater var4 = (CollectUpdater) updater;
            this.collections().add( VirtualValues.fromList( var4.collection() ) );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return VirtualValues.concat( (ListValue[]) this.collections().toArray( (Object[]) (new ListValue[0]) ) );
    }
}
