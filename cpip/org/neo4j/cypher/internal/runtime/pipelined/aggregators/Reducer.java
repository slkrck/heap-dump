package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Reducer
{
    void update( final Updater updater );

    AnyValue result();
}
