package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class MinStandardReducer extends MinUpdaterBase implements Reducer
{
    public void update( final Updater updater )
    {
        if ( updater instanceof MinUpdater )
        {
            MinUpdater var4 = (MinUpdater) updater;
            this.update( var4.min() );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return this.min();
    }
}
