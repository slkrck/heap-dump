package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.concurrent.atomic.AtomicReference;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class MinConcurrentReducer implements Reducer
{
    private final AtomicReference<AnyValue> min;

    public MinConcurrentReducer()
    {
        this.min = new AtomicReference( Values.NO_VALUE );
    }

    private AtomicReference<AnyValue> min()
    {
        return this.min;
    }

    public void update( final Updater updater )
    {
        if ( updater instanceof MinUpdater )
        {
            MinUpdater var4 = (MinUpdater) updater;
            this.min().updateAndGet( ( oldMin ) -> {
                return MinAggregator$.MODULE$.shouldUpdate( oldMin, var4.min() ) ? var4.min() : oldMin;
            } );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return (AnyValue) this.min().get();
    }
}
