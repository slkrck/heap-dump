package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class CountAggregator$ implements Aggregator, Product, Serializable
{
    public static CountAggregator$ MODULE$;

    static
    {
        new CountAggregator$();
    }

    private CountAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new CountAggregator.CountUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new CountStandardReducer();
    }

    public Reducer newConcurrentReducer()
    {
        return new CountConcurrentReducer();
    }

    public String productPrefix()
    {
        return "CountAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CountAggregator$;
    }

    public int hashCode()
    {
        return -168830440;
    }

    public String toString()
    {
        return "CountAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
