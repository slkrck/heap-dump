package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class AggregatorFactory$ extends AbstractFunction1<PhysicalPlan,AggregatorFactory> implements Serializable
{
    public static AggregatorFactory$ MODULE$;

    static
    {
        new AggregatorFactory$();
    }

    private AggregatorFactory$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "AggregatorFactory";
    }

    public AggregatorFactory apply( final PhysicalPlan physicalPlan )
    {
        return new AggregatorFactory( physicalPlan );
    }

    public Option<PhysicalPlan> unapply( final AggregatorFactory x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.physicalPlan() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
