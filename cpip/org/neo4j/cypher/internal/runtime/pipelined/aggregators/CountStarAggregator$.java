package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class CountStarAggregator$ implements Aggregator, Product, Serializable
{
    public static CountStarAggregator$ MODULE$;

    static
    {
        new CountStarAggregator$();
    }

    private CountStarAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new CountStarAggregator.CountStarUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new CountStandardReducer();
    }

    public Reducer newConcurrentReducer()
    {
        return new CountConcurrentReducer();
    }

    public String productPrefix()
    {
        return "CountStarAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CountStarAggregator$;
    }

    public int hashCode()
    {
        return -779895158;
    }

    public String toString()
    {
        return "CountStarAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
