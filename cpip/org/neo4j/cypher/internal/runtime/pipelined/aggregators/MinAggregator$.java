package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.values.AnyValue;
import org.neo4j.values.AnyValues;
import org.neo4j.values.storable.Values;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class MinAggregator$ implements Aggregator, Product, Serializable
{
    public static MinAggregator$ MODULE$;

    static
    {
        new MinAggregator$();
    }

    private MinAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new MinUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new MinStandardReducer();
    }

    public Reducer newConcurrentReducer()
    {
        return new MinConcurrentReducer();
    }

    public boolean shouldUpdate( final AnyValue min, final AnyValue value )
    {
        return (min == Values.NO_VALUE || AnyValues.COMPARATOR.compare( min, value ) > 0) && value != Values.NO_VALUE;
    }

    public String productPrefix()
    {
        return "MinAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MinAggregator$;
    }

    public int hashCode()
    {
        return -1710542949;
    }

    public String toString()
    {
        return "MinAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
