package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class AvgAggregator$ implements Aggregator, Product, Serializable
{
    public static AvgAggregator$ MODULE$;

    static
    {
        new AvgAggregator$();
    }

    private AvgAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new AvgAggregator.AvgUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new AvgAggregator.AvgStandardReducer();
    }

    public Reducer newConcurrentReducer()
    {
        return new AvgAggregator.AvgConcurrentReducer();
    }

    public String productPrefix()
    {
        return "AvgAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AvgAggregator$;
    }

    public int hashCode()
    {
        return -1112841189;
    }

    public String toString()
    {
        return "AvgAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
