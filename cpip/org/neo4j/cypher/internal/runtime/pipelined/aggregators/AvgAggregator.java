package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.DurationValue;
import org.neo4j.values.storable.NumberValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.utils.ValueMath;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple5;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction2;
import scala.runtime.AbstractFunction5;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public final class AvgAggregator
{
    public static String toString()
    {
        return AvgAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return AvgAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return AvgAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return AvgAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return AvgAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return AvgAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return AvgAggregator$.MODULE$.productPrefix();
    }

    public static Reducer newConcurrentReducer()
    {
        return AvgAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return AvgAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return AvgAggregator$.MODULE$.newUpdater();
    }

    public static class AvgConcurrentReducer extends AvgAggregator.AvgStandardBase implements Reducer
    {
        private volatile AvgAggregator.AvgConcurrentReducer.AvgNumber$ AvgNumber$module;
        private final AtomicReference<AvgAggregator.AvgConcurrentReducer.AvgNumber> number = new AtomicReference(
                new AvgAggregator.AvgConcurrentReducer.AvgNumber( this, this.AvgNumber().apply$default$1(), this.AvgNumber().apply$default$2() ) );
        private volatile AvgAggregator.AvgConcurrentReducer.AvgDuration$ AvgDuration$module;
        private final AtomicReference<AvgAggregator.AvgConcurrentReducer.AvgDuration> duration = new AtomicReference(
                new AvgAggregator.AvgConcurrentReducer.AvgDuration( this, this.AvgDuration().apply$default$1(), this.AvgDuration().apply$default$2(),
                        this.AvgDuration().apply$default$3(), this.AvgDuration().apply$default$4(), this.AvgDuration().apply$default$5() ) );
        private volatile boolean seenNumber = false;
        private volatile boolean seenDuration = false;

        public AvgAggregator.AvgConcurrentReducer.AvgNumber$ AvgNumber()
        {
            if ( this.AvgNumber$module == null )
            {
                this.AvgNumber$lzycompute$1();
            }

            return this.AvgNumber$module;
        }

        public AvgAggregator.AvgConcurrentReducer.AvgDuration$ AvgDuration()
        {
            if ( this.AvgDuration$module == null )
            {
                this.AvgDuration$lzycompute$1();
            }

            return this.AvgDuration$module;
        }

        private boolean seenNumber()
        {
            return this.seenNumber;
        }

        private void seenNumber_$eq( final boolean x$1 )
        {
            this.seenNumber = x$1;
        }

        private boolean seenDuration()
        {
            return this.seenDuration;
        }

        private void seenDuration_$eq( final boolean x$1 )
        {
            this.seenDuration = x$1;
        }

        private AtomicReference<AvgAggregator.AvgConcurrentReducer.AvgNumber> number()
        {
            return this.number;
        }

        private AtomicReference<AvgAggregator.AvgConcurrentReducer.AvgDuration> duration()
        {
            return this.duration;
        }

        public void update( final Updater updater )
        {
            if ( updater instanceof AvgAggregator.AvgUpdater )
            {
                AvgAggregator.AvgUpdater var4 = (AvgAggregator.AvgUpdater) updater;
                BoxedUnit var10000;
                if ( var4.seenNumber() )
                {
                    this.seenNumber_$eq( true );
                    if ( this.seenDuration() )
                    {
                        throw this.failMix();
                    }

                    this.number().updateAndGet( ( old ) -> {
                        long newCount = old.count() + var4.count();
                        NumberValue diff = var4.avgNumber().minus( old.avgNumber() );
                        NumberValue next = diff.times( (double) var4.count() / (double) newCount );
                        return this.new AvgNumber( this, newCount, ValueMath.overflowSafeAdd( old.avgNumber(), next ) );
                    } );
                    var10000 = BoxedUnit.UNIT;
                }
                else if ( var4.seenDuration() )
                {
                    this.seenDuration_$eq( true );
                    if ( this.seenNumber() )
                    {
                        throw this.failMix();
                    }

                    this.duration().updateAndGet( ( old ) -> {
                        long newCount = old.count() + var4.count();
                        return this.new AvgDuration( this, newCount,
                                old.monthsRunningAvg() + ((double) var4.sumMonths() - old.monthsRunningAvg() * (double) var4.count()) / (double) newCount,
                                old.daysRunningAvg() + ((double) var4.sumDays() - old.daysRunningAvg() * (double) var4.count()) / (double) newCount,
                                old.secondsRunningAvg() + ((double) var4.sumSeconds() - old.secondsRunningAvg() * (double) var4.count()) / (double) newCount,
                                old.nanosRunningAvg() + ((double) var4.sumNanos() - old.nanosRunningAvg() * (double) var4.count()) / (double) newCount );
                    } );
                    var10000 = BoxedUnit.UNIT;
                }
                else
                {
                    var10000 = BoxedUnit.UNIT;
                }
            }
            else
            {
                throw new MatchError( updater );
            }
        }

        public AnyValue result()
        {
            Object var10000;
            if ( this.seenDuration() )
            {
                AvgAggregator.AvgConcurrentReducer.AvgDuration x = (AvgAggregator.AvgConcurrentReducer.AvgDuration) this.duration().get();
                var10000 = DurationValue.approximate( x.monthsRunningAvg(), x.daysRunningAvg(), x.secondsRunningAvg(), x.nanosRunningAvg() ).normalize();
            }
            else if ( this.seenNumber() )
            {
                AvgAggregator.AvgConcurrentReducer.AvgNumber x = (AvgAggregator.AvgConcurrentReducer.AvgNumber) this.number().get();
                var10000 = x.avgNumber();
            }
            else
            {
                var10000 = Values.NO_VALUE;
            }

            return (AnyValue) var10000;
        }

        private final void AvgNumber$lzycompute$1()
        {
            synchronized ( this )
            {
            }

            try
            {
                if ( this.AvgNumber$module == null )
                {
                    this.AvgNumber$module = new AvgAggregator.AvgConcurrentReducer.AvgNumber$( this );
                }
            }
            catch ( Throwable var3 )
            {
                throw var3;
            }
        }

        private final void AvgDuration$lzycompute$1()
        {
            synchronized ( this )
            {
            }

            try
            {
                if ( this.AvgDuration$module == null )
                {
                    this.AvgDuration$module = new AvgAggregator.AvgConcurrentReducer.AvgDuration$( this );
                }
            }
            catch ( Throwable var3 )
            {
                throw var3;
            }
        }

        public class AvgDuration implements Product, Serializable
        {
            private final long count;
            private final double monthsRunningAvg;
            private final double daysRunningAvg;
            private final double secondsRunningAvg;
            private final double nanosRunningAvg;

            public AvgDuration( final AvgAggregator.AvgConcurrentReducer $outer, final long count, final double monthsRunningAvg, final double daysRunningAvg,
                    final double secondsRunningAvg, final double nanosRunningAvg )
            {
                this.count = count;
                this.monthsRunningAvg = monthsRunningAvg;
                this.daysRunningAvg = daysRunningAvg;
                this.secondsRunningAvg = secondsRunningAvg;
                this.nanosRunningAvg = nanosRunningAvg;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    Product.$init$( this );
                }
            }

            public long count()
            {
                return this.count;
            }

            public double monthsRunningAvg()
            {
                return this.monthsRunningAvg;
            }

            public double daysRunningAvg()
            {
                return this.daysRunningAvg;
            }

            public double secondsRunningAvg()
            {
                return this.secondsRunningAvg;
            }

            public double nanosRunningAvg()
            {
                return this.nanosRunningAvg;
            }

            public AvgAggregator.AvgConcurrentReducer.AvgDuration copy( final long count, final double monthsRunningAvg, final double daysRunningAvg,
                    final double secondsRunningAvg, final double nanosRunningAvg )
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgDuration$$$outer().new AvgDuration(
                        this.org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgDuration$$$outer(), count,
                        monthsRunningAvg, daysRunningAvg, secondsRunningAvg, nanosRunningAvg );
            }

            public long copy$default$1()
            {
                return this.count();
            }

            public double copy$default$2()
            {
                return this.monthsRunningAvg();
            }

            public double copy$default$3()
            {
                return this.daysRunningAvg();
            }

            public double copy$default$4()
            {
                return this.secondsRunningAvg();
            }

            public double copy$default$5()
            {
                return this.nanosRunningAvg();
            }

            public String productPrefix()
            {
                return "AvgDuration";
            }

            public int productArity()
            {
                return 5;
            }

            public Object productElement( final int x$1 )
            {
                Object var10000;
                switch ( x$1 )
                {
                case 0:
                    var10000 = BoxesRunTime.boxToLong( this.count() );
                    break;
                case 1:
                    var10000 = BoxesRunTime.boxToDouble( this.monthsRunningAvg() );
                    break;
                case 2:
                    var10000 = BoxesRunTime.boxToDouble( this.daysRunningAvg() );
                    break;
                case 3:
                    var10000 = BoxesRunTime.boxToDouble( this.secondsRunningAvg() );
                    break;
                case 4:
                    var10000 = BoxesRunTime.boxToDouble( this.nanosRunningAvg() );
                    break;
                default:
                    throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
                }

                return var10000;
            }

            public Iterator<Object> productIterator()
            {
                return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
            }

            public boolean canEqual( final Object x$1 )
            {
                return x$1 instanceof AvgAggregator.AvgConcurrentReducer.AvgDuration;
            }

            public int hashCode()
            {
                int var1 = -889275714;
                var1 = Statics.mix( var1, Statics.longHash( this.count() ) );
                var1 = Statics.mix( var1, Statics.doubleHash( this.monthsRunningAvg() ) );
                var1 = Statics.mix( var1, Statics.doubleHash( this.daysRunningAvg() ) );
                var1 = Statics.mix( var1, Statics.doubleHash( this.secondsRunningAvg() ) );
                var1 = Statics.mix( var1, Statics.doubleHash( this.nanosRunningAvg() ) );
                return Statics.finalizeHash( var1, 5 );
            }

            public String toString()
            {
                return scala.runtime.ScalaRunTime..MODULE$._toString( this );
            }

            public boolean equals( final Object x$1 )
            {
                boolean var10000;
                if ( this != x$1 )
                {
                    label62:
                    {
                        boolean var2;
                        if ( x$1 instanceof AvgAggregator.AvgConcurrentReducer.AvgDuration &&
                                ((AvgAggregator.AvgConcurrentReducer.AvgDuration) x$1).org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgDuration$$$outer() ==
                                        this.org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgDuration$$$outer() )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        if ( var2 )
                        {
                            AvgAggregator.AvgConcurrentReducer.AvgDuration var4 = (AvgAggregator.AvgConcurrentReducer.AvgDuration) x$1;
                            if ( this.count() == var4.count() && this.monthsRunningAvg() == var4.monthsRunningAvg() &&
                                    this.daysRunningAvg() == var4.daysRunningAvg() && this.secondsRunningAvg() == var4.secondsRunningAvg() &&
                                    this.nanosRunningAvg() == var4.nanosRunningAvg() && var4.canEqual( this ) )
                            {
                                break label62;
                            }
                        }

                        var10000 = false;
                        return var10000;
                    }
                }

                var10000 = true;
                return var10000;
            }
        }

        public class AvgDuration$ extends AbstractFunction5<Object,Object,Object,Object,Object,AvgAggregator.AvgConcurrentReducer.AvgDuration>
                implements Serializable
        {
            public AvgDuration$( final AvgAggregator.AvgConcurrentReducer $outer )
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                }
            }

            public long $lessinit$greater$default$1()
            {
                return 0L;
            }

            public double $lessinit$greater$default$2()
            {
                return 0.0D;
            }

            public double $lessinit$greater$default$3()
            {
                return 0.0D;
            }

            public double $lessinit$greater$default$4()
            {
                return 0.0D;
            }

            public double $lessinit$greater$default$5()
            {
                return 0.0D;
            }

            public final String toString()
            {
                return "AvgDuration";
            }

            public AvgAggregator.AvgConcurrentReducer.AvgDuration apply( final long count, final double monthsRunningAvg, final double daysRunningAvg,
                    final double secondsRunningAvg, final double nanosRunningAvg )
            {
                return this.$outer.new AvgDuration( this.$outer, count, monthsRunningAvg, daysRunningAvg, secondsRunningAvg, nanosRunningAvg );
            }

            public long apply$default$1()
            {
                return 0L;
            }

            public double apply$default$2()
            {
                return 0.0D;
            }

            public double apply$default$3()
            {
                return 0.0D;
            }

            public double apply$default$4()
            {
                return 0.0D;
            }

            public double apply$default$5()
            {
                return 0.0D;
            }

            public Option<Tuple5<Object,Object,Object,Object,Object>> unapply( final AvgAggregator.AvgConcurrentReducer.AvgDuration x$0 )
            {
                return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
                    new Tuple5( BoxesRunTime.boxToLong( x$0.count() ), BoxesRunTime.boxToDouble( x$0.monthsRunningAvg() ),
                            BoxesRunTime.boxToDouble( x$0.daysRunningAvg() ), BoxesRunTime.boxToDouble( x$0.secondsRunningAvg() ),
                            BoxesRunTime.boxToDouble( x$0.nanosRunningAvg() ) ) ));
            }
        }

        public class AvgNumber implements Product, Serializable
        {
            private final long count;
            private final NumberValue avgNumber;

            public AvgNumber( final AvgAggregator.AvgConcurrentReducer $outer, final long count, final NumberValue avgNumber )
            {
                this.count = count;
                this.avgNumber = avgNumber;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    Product.$init$( this );
                }
            }

            public long count()
            {
                return this.count;
            }

            public NumberValue avgNumber()
            {
                return this.avgNumber;
            }

            public AvgAggregator.AvgConcurrentReducer.AvgNumber copy( final long count, final NumberValue avgNumber )
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgNumber$$$outer().new AvgNumber(
                        this.org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgNumber$$$outer(), count, avgNumber );
            }

            public long copy$default$1()
            {
                return this.count();
            }

            public NumberValue copy$default$2()
            {
                return this.avgNumber();
            }

            public String productPrefix()
            {
                return "AvgNumber";
            }

            public int productArity()
            {
                return 2;
            }

            public Object productElement( final int x$1 )
            {
                Object var10000;
                switch ( x$1 )
                {
                case 0:
                    var10000 = BoxesRunTime.boxToLong( this.count() );
                    break;
                case 1:
                    var10000 = this.avgNumber();
                    break;
                default:
                    throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
                }

                return var10000;
            }

            public Iterator<Object> productIterator()
            {
                return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
            }

            public boolean canEqual( final Object x$1 )
            {
                return x$1 instanceof AvgAggregator.AvgConcurrentReducer.AvgNumber;
            }

            public int hashCode()
            {
                int var1 = -889275714;
                var1 = Statics.mix( var1, Statics.longHash( this.count() ) );
                var1 = Statics.mix( var1, Statics.anyHash( this.avgNumber() ) );
                return Statics.finalizeHash( var1, 2 );
            }

            public String toString()
            {
                return scala.runtime.ScalaRunTime..MODULE$._toString( this );
            }

            public boolean equals( final Object x$1 )
            {
                boolean var6;
                if ( this != x$1 )
                {
                    label60:
                    {
                        boolean var2;
                        if ( x$1 instanceof AvgAggregator.AvgConcurrentReducer.AvgNumber &&
                                ((AvgAggregator.AvgConcurrentReducer.AvgNumber) x$1).org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgNumber$$$outer() ==
                                        this.org$neo4j$cypher$internal$runtime$pipelined$aggregators$AvgAggregator$AvgConcurrentReducer$AvgNumber$$$outer() )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        if ( var2 )
                        {
                            label37:
                            {
                                AvgAggregator.AvgConcurrentReducer.AvgNumber var4 = (AvgAggregator.AvgConcurrentReducer.AvgNumber) x$1;
                                if ( this.count() == var4.count() )
                                {
                                    label35:
                                    {
                                        NumberValue var10000 = this.avgNumber();
                                        NumberValue var5 = var4.avgNumber();
                                        if ( var10000 == null )
                                        {
                                            if ( var5 != null )
                                            {
                                                break label35;
                                            }
                                        }
                                        else if ( !var10000.equals( var5 ) )
                                        {
                                            break label35;
                                        }

                                        if ( var4.canEqual( this ) )
                                        {
                                            var6 = true;
                                            break label37;
                                        }
                                    }
                                }

                                var6 = false;
                            }

                            if ( var6 )
                            {
                                break label60;
                            }
                        }

                        var6 = false;
                        return var6;
                    }
                }

                var6 = true;
                return var6;
            }
        }

        public class AvgNumber$ extends AbstractFunction2<Object,NumberValue,AvgAggregator.AvgConcurrentReducer.AvgNumber> implements Serializable
        {
            public AvgNumber$( final AvgAggregator.AvgConcurrentReducer $outer )
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                }
            }

            public long $lessinit$greater$default$1()
            {
                return 0L;
            }

            public NumberValue $lessinit$greater$default$2()
            {
                return Values.ZERO_INT;
            }

            public final String toString()
            {
                return "AvgNumber";
            }

            public AvgAggregator.AvgConcurrentReducer.AvgNumber apply( final long count, final NumberValue avgNumber )
            {
                return this.$outer.new AvgNumber( this.$outer, count, avgNumber );
            }

            public long apply$default$1()
            {
                return 0L;
            }

            public NumberValue apply$default$2()
            {
                return Values.ZERO_INT;
            }

            public Option<Tuple2<Object,NumberValue>> unapply( final AvgAggregator.AvgConcurrentReducer.AvgNumber x$0 )
            {
                return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToLong( x$0.count() ), x$0.avgNumber() ) ));
            }
        }
    }

    public abstract static class AvgStandardBase
    {
      public scala.runtime.Nothing.failMix()

final AnyValue value

      public scala.runtime.Nothing.failType(

        {
            throw new CypherTypeException( "avg() cannot mix number and duration" );
        })

        {
            throw new CypherTypeException(
                    (new StringBuilder( 64 )).append( "avg() can only handle numerical values, duration, and null. Got " ).append( value ).toString() );
        }
    }

    public static class AvgStandardReducer extends AvgAggregator.AvgStandardBase implements Reducer
    {
        private boolean seenNumber = false;
        private boolean seenDuration = false;
        private long count = 0L;
        private NumberValue avgNumber;
        private double monthsRunningAvg;
        private double daysRunningAvg;
        private double secondsRunningAvg;
        private double nanosRunningAvg;

        public AvgStandardReducer()
        {
            this.avgNumber = Values.ZERO_INT;
            this.monthsRunningAvg = 0.0D;
            this.daysRunningAvg = 0.0D;
            this.secondsRunningAvg = 0.0D;
            this.nanosRunningAvg = 0.0D;
        }

        public boolean seenNumber()
        {
            return this.seenNumber;
        }

        public void seenNumber_$eq( final boolean x$1 )
        {
            this.seenNumber = x$1;
        }

        public boolean seenDuration()
        {
            return this.seenDuration;
        }

        public void seenDuration_$eq( final boolean x$1 )
        {
            this.seenDuration = x$1;
        }

        public long count()
        {
            return this.count;
        }

        public void count_$eq( final long x$1 )
        {
            this.count = x$1;
        }

        public NumberValue avgNumber()
        {
            return this.avgNumber;
        }

        public void avgNumber_$eq( final NumberValue x$1 )
        {
            this.avgNumber = x$1;
        }

        public double monthsRunningAvg()
        {
            return this.monthsRunningAvg;
        }

        public void monthsRunningAvg_$eq( final double x$1 )
        {
            this.monthsRunningAvg = x$1;
        }

        public double daysRunningAvg()
        {
            return this.daysRunningAvg;
        }

        public void daysRunningAvg_$eq( final double x$1 )
        {
            this.daysRunningAvg = x$1;
        }

        public double secondsRunningAvg()
        {
            return this.secondsRunningAvg;
        }

        public void secondsRunningAvg_$eq( final double x$1 )
        {
            this.secondsRunningAvg = x$1;
        }

        public double nanosRunningAvg()
        {
            return this.nanosRunningAvg;
        }

        public void nanosRunningAvg_$eq( final double x$1 )
        {
            this.nanosRunningAvg = x$1;
        }

        public void update( final Updater updater )
        {
            if ( updater instanceof AvgAggregator.AvgUpdater )
            {
                AvgAggregator.AvgUpdater var4 = (AvgAggregator.AvgUpdater) updater;
                long newCount = this.count() + var4.count();
                if ( var4.seenNumber() )
                {
                    this.seenNumber_$eq( true );
                    if ( this.seenDuration() )
                    {
                        throw this.failMix();
                    }

                    NumberValue diff = var4.avgNumber().minus( this.avgNumber() );
                    NumberValue next = diff.times( (double) var4.count() / (double) newCount );
                    this.avgNumber_$eq( ValueMath.overflowSafeAdd( this.avgNumber(), next ) );
                }
                else if ( var4.seenDuration() )
                {
                    this.seenDuration_$eq( true );
                    if ( this.seenNumber() )
                    {
                        throw this.failMix();
                    }

                    this.monthsRunningAvg_$eq(
                            this.monthsRunningAvg() + ((double) var4.sumMonths() - this.monthsRunningAvg() * (double) var4.count()) / (double) newCount );
                    this.daysRunningAvg_$eq(
                            this.daysRunningAvg() + ((double) var4.sumDays() - this.daysRunningAvg() * (double) var4.count()) / (double) newCount );
                    this.secondsRunningAvg_$eq(
                            this.secondsRunningAvg() + ((double) var4.sumSeconds() - this.secondsRunningAvg() * (double) var4.count()) / (double) newCount );
                    this.nanosRunningAvg_$eq(
                            this.nanosRunningAvg() + ((double) var4.sumNanos() - this.nanosRunningAvg() * (double) var4.count()) / (double) newCount );
                }

                this.count_$eq( newCount );
                BoxedUnit var2 = BoxedUnit.UNIT;
            }
            else
            {
                throw new MatchError( updater );
            }
        }

        public AnyValue result()
        {
            return (AnyValue) (this.seenDuration() ? DurationValue.approximate( this.monthsRunningAvg(), this.daysRunningAvg(), this.secondsRunningAvg(),
                    this.nanosRunningAvg() ).normalize() : (this.seenNumber() ? this.avgNumber() : Values.NO_VALUE));
        }
    }

    public static class AvgUpdater extends AvgAggregator.AvgStandardBase implements Updater
    {
        private boolean seenNumber = false;
        private boolean seenDuration = false;
        private long count = 0L;
        private NumberValue avgNumber;
        private long sumMonths;
        private long sumDays;
        private long sumSeconds;
        private long sumNanos;

        public AvgUpdater()
        {
            this.avgNumber = Values.ZERO_INT;
            this.sumMonths = 0L;
            this.sumDays = 0L;
            this.sumSeconds = 0L;
            this.sumNanos = 0L;
        }

        public boolean seenNumber()
        {
            return this.seenNumber;
        }

        public void seenNumber_$eq( final boolean x$1 )
        {
            this.seenNumber = x$1;
        }

        public boolean seenDuration()
        {
            return this.seenDuration;
        }

        public void seenDuration_$eq( final boolean x$1 )
        {
            this.seenDuration = x$1;
        }

        public long count()
        {
            return this.count;
        }

        public void count_$eq( final long x$1 )
        {
            this.count = x$1;
        }

        public NumberValue avgNumber()
        {
            return this.avgNumber;
        }

        public void avgNumber_$eq( final NumberValue x$1 )
        {
            this.avgNumber = x$1;
        }

        public long sumMonths()
        {
            return this.sumMonths;
        }

        public void sumMonths_$eq( final long x$1 )
        {
            this.sumMonths = x$1;
        }

        public long sumDays()
        {
            return this.sumDays;
        }

        public void sumDays_$eq( final long x$1 )
        {
            this.sumDays = x$1;
        }

        public long sumSeconds()
        {
            return this.sumSeconds;
        }

        public void sumSeconds_$eq( final long x$1 )
        {
            this.sumSeconds = x$1;
        }

        public long sumNanos()
        {
            return this.sumNanos;
        }

        public void sumNanos_$eq( final long x$1 )
        {
            this.sumNanos = x$1;
        }

        public void update( final AnyValue value )
        {
            if ( value != Values.NO_VALUE )
            {
                this.count_$eq( this.count() + 1L );
                BoxedUnit var2;
                if ( value instanceof NumberValue )
                {
                    NumberValue var4 = (NumberValue) value;
                    this.seenNumber_$eq( true );
                    if ( this.seenDuration() )
                    {
                        throw this.failMix();
                    }

                    NumberValue diff = var4.minus( this.avgNumber() );
                    NumberValue next = diff.dividedBy( (double) this.count() );
                    this.avgNumber_$eq( ValueMath.overflowSafeAdd( this.avgNumber(), next ) );
                    var2 = BoxedUnit.UNIT;
                }
                else
                {
                    if ( !(value instanceof DurationValue) )
                    {
                        throw this.failType( value );
                    }

                    DurationValue var7 = (DurationValue) value;
                    this.seenDuration_$eq( true );
                    if ( this.seenNumber() )
                    {
                        throw this.failMix();
                    }

                    this.sumMonths_$eq( this.sumMonths() + var7.get( ChronoUnit.MONTHS ) );
                    this.sumDays_$eq( this.sumDays() + var7.get( ChronoUnit.DAYS ) );
                    this.sumSeconds_$eq( this.sumSeconds() + var7.get( ChronoUnit.SECONDS ) );
                    this.sumNanos_$eq( this.sumNanos() + var7.get( ChronoUnit.NANOS ) );
                    var2 = BoxedUnit.UNIT;
                }
            }
        }
    }
}
