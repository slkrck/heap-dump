package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class CountDistinctAggregator
{
    public static String toString()
    {
        return CountDistinctAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return CountDistinctAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return CountDistinctAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return CountDistinctAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return CountDistinctAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return CountDistinctAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return CountDistinctAggregator$.MODULE$.productPrefix();
    }

    public static Reducer newConcurrentReducer()
    {
        return CountDistinctAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return CountDistinctAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return CountDistinctAggregator$.MODULE$.newUpdater();
    }
}
