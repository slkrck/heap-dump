package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Aggregator
{
    Updater newUpdater();

    Reducer newStandardReducer( final QueryMemoryTracker memoryTracker );

    Reducer newConcurrentReducer();
}
