package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.concurrent.atomic.AtomicReference;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.DurationValue;
import org.neo4j.values.storable.NumberValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.utils.ValueMath;
import scala.MatchError;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.Nothing.;

@JavaDocToJava
public final class SumAggregator
{
    public static String toString()
    {
        return SumAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return SumAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return SumAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return SumAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return SumAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return SumAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return SumAggregator$.MODULE$.productPrefix();
    }

    public static Reducer newConcurrentReducer()
    {
        return SumAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return SumAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return SumAggregator$.MODULE$.newUpdater();
    }

    public static class SumConcurrentReducer implements Reducer
    {
        private final AtomicReference<NumberValue> sumNumber;
        private final AtomicReference<DurationValue> sumDuration;
        private volatile boolean seenNumber = false;
        private volatile boolean seenDuration = false;

        public SumConcurrentReducer()
        {
            this.sumNumber = new AtomicReference( Values.ZERO_INT );
            this.sumDuration = new AtomicReference( DurationValue.ZERO );
        }

        private boolean seenNumber()
        {
            return this.seenNumber;
        }

        private void seenNumber_$eq( final boolean x$1 )
        {
            this.seenNumber = x$1;
        }

        private boolean seenDuration()
        {
            return this.seenDuration;
        }

        private void seenDuration_$eq( final boolean x$1 )
        {
            this.seenDuration = x$1;
        }

        private AtomicReference<NumberValue> sumNumber()
        {
            return this.sumNumber;
        }

        private AtomicReference<DurationValue> sumDuration()
        {
            return this.sumDuration;
        }

        public void update( final Updater updater )
        {
            if ( updater instanceof SumAggregator.SumUpdater )
            {
                SumAggregator.SumUpdater var4 = (SumAggregator.SumUpdater) updater;
                BoxedUnit var10000;
                if ( var4.seenNumber() )
                {
                    this.sumNumber().updateAndGet( ( num ) -> {
                        return ValueMath.overflowSafeAdd( num, var4.sumNumber() );
                    } );
                    this.seenNumber_$eq( true );
                    var10000 = BoxedUnit.UNIT;
                }
                else if ( var4.seenDuration() )
                {
                    this.sumDuration().updateAndGet( ( dur ) -> {
                        return dur.add( var4.sumDuration() );
                    } );
                    this.seenDuration_$eq( true );
                    var10000 = BoxedUnit.UNIT;
                }
                else
                {
                    var10000 = BoxedUnit.UNIT;
                }
            }
            else
            {
                throw new MatchError( updater );
            }
        }

        public AnyValue result()
        {
            if ( this.seenNumber() && this.seenDuration() )
            {
                throw new CypherTypeException( "sum() cannot mix number and duration" );
            }
            else
            {
                return this.seenDuration() ? (AnyValue) this.sumDuration().get() : (AnyValue) this.sumNumber().get();
            }
        }
    }

    public abstract static class SumStandardBase
    {
        private boolean seenNumber = false;
        private boolean seenDuration = false;
        private NumberValue sumNumber;
        private DurationValue sumDuration;

        public failMix()
        {
            throw new CypherTypeException( "sum() cannot mix number and duration" );
        }

        public failType( final AnyValue value )
        {
            throw new CypherTypeException(
                    (new StringBuilder( 64 )).append( "sum() can only handle numerical values, duration, and null. Got " ).append( value ).toString() );
        }

        public SumStandardBase()
        {
            this.sumNumber = Values.ZERO_INT;
            this.sumDuration = DurationValue.ZERO;
        }

        public boolean seenNumber()
        {
            return this.seenNumber;
        }

        public void seenNumber_$eq( final boolean x$1 )
        {
            this.seenNumber = x$1;
        }

        public boolean seenDuration()
        {
            return this.seenDuration;
        }

        public void seenDuration_$eq( final boolean x$1 )
        {
            this.seenDuration = x$1;
        }

        public NumberValue sumNumber()
        {
            return this.sumNumber;
        }

        public void sumNumber_$eq( final NumberValue x$1 )
        {
            this.sumNumber = x$1;
        }

        public DurationValue sumDuration()
        {
            return this.sumDuration;
        }

        public void sumDuration_$eq( final DurationValue x$1 )
        {
            this.sumDuration = x$1;
        }

        public void onNumber( final NumberValue number )
        {
            this.seenNumber_$eq( true );
            if ( !this.seenDuration() )
            {
                this.sumNumber_$eq( ValueMath.overflowSafeAdd( this.sumNumber(), number ) );
            }
            else
            {
                throw this.failMix();
            }
        }

        public void onDuration( final DurationValue dur )
        {
            this.seenDuration_$eq( true );
            if ( !this.seenNumber() )
            {
                this.sumDuration_$eq( this.sumDuration().add( dur ) );
            }
            else
            {
                throw this.failMix();
            }
        }
    }

    public static class SumStandardReducer extends SumAggregator.SumStandardBase implements Reducer
    {
        public void update( final Updater updater )
        {
            if ( updater instanceof SumAggregator.SumUpdater )
            {
                SumAggregator.SumUpdater var4 = (SumAggregator.SumUpdater) updater;
                BoxedUnit var10000;
                if ( var4.seenNumber() )
                {
                    this.onNumber( var4.sumNumber() );
                    var10000 = BoxedUnit.UNIT;
                }
                else if ( var4.seenDuration() )
                {
                    this.onDuration( var4.sumDuration() );
                    var10000 = BoxedUnit.UNIT;
                }
                else
                {
                    var10000 = BoxedUnit.UNIT;
                }
            }
            else
            {
                throw new MatchError( updater );
            }
        }

        public AnyValue result()
        {
            return (AnyValue) (this.seenDuration() ? this.sumDuration() : this.sumNumber());
        }
    }

    public static class SumUpdater extends SumAggregator.SumStandardBase implements Updater
    {
        public void update( final AnyValue value )
        {
            if ( value != Values.NO_VALUE )
            {
                BoxedUnit var2;
                if ( value instanceof NumberValue )
                {
                    NumberValue var4 = (NumberValue) value;
                    this.onNumber( var4 );
                    var2 = BoxedUnit.UNIT;
                }
                else
                {
                    if ( !(value instanceof DurationValue) )
                    {
                        throw this.failType( value );
                    }

                    DurationValue var5 = (DurationValue) value;
                    this.onDuration( var5 );
                    var2 = BoxedUnit.UNIT;
                }
            }
        }
    }
}
