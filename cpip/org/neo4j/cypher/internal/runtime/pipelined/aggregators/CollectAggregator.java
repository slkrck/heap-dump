package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class CollectAggregator
{
    public static String toString()
    {
        return CollectAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return CollectAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return CollectAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return CollectAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return CollectAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return CollectAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return CollectAggregator$.MODULE$.productPrefix();
    }

    public static Reducer newConcurrentReducer()
    {
        return CollectAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return CollectAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return CollectAggregator$.MODULE$.newUpdater();
    }
}
