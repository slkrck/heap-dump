package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.values.AnyValue;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class MinAggregator
{
    public static String toString()
    {
        return MinAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return MinAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return MinAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return MinAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return MinAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return MinAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return MinAggregator$.MODULE$.productPrefix();
    }

    public static boolean shouldUpdate( final AnyValue min, final AnyValue value )
    {
        return MinAggregator$.MODULE$.shouldUpdate( var0, var1 );
    }

    public static Reducer newConcurrentReducer()
    {
        return MinAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return MinAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return MinAggregator$.MODULE$.newUpdater();
    }
}
