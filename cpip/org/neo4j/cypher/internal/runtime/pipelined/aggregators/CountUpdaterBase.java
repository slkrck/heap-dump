package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class CountUpdaterBase implements Updater
{
    private long count = 0L;

    public long count()
    {
        return this.count;
    }

    public void count_$eq( final long x$1 )
    {
        this.count = x$1;
    }
}
