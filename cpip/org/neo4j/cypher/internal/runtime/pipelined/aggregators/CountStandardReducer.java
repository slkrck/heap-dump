package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class CountStandardReducer implements Reducer
{
    private long count = 0L;

    private long count()
    {
        return this.count;
    }

    private void count_$eq( final long x$1 )
    {
        this.count = x$1;
    }

    public void update( final Updater updater )
    {
        if ( updater instanceof CountUpdaterBase )
        {
            CountUpdaterBase var4 = (CountUpdaterBase) updater;
            this.count_$eq( this.count() + var4.count() );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return Values.longValue( this.count() );
    }
}
