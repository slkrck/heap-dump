package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.values.AnyValue;
import scala.Predef.;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class AggregatingAccumulator implements ArgumentStateMap.MorselAccumulator<Updater[]>
{
    private final long argumentRowId;
    private final Reducer[] reducers;
    private final long[] argumentRowIdsForReducers;

    public AggregatingAccumulator( final long argumentRowId, final Reducer[] reducers, final long[] argumentRowIdsForReducers )
    {
        this.argumentRowId = argumentRowId;
        this.reducers = reducers;
        this.argumentRowIdsForReducers = argumentRowIdsForReducers;
    }

    public long argumentRowId()
    {
        return this.argumentRowId;
    }

    public long[] argumentRowIdsForReducers()
    {
        return this.argumentRowIdsForReducers;
    }

    public void update( final Updater[] data )
    {
        for ( int i = 0; i < data.length; ++i )
        {
            this.reducers[i].update( data[i] );
        }
    }

    public AnyValue result( final int offset )
    {
        return this.reducers[offset].result();
    }

    public static class Factory implements ArgumentStateMap.ArgumentStateFactory<AggregatingAccumulator>
    {
        private final Aggregator[] aggregators;
        private final QueryMemoryTracker memoryTracker;

        public Factory( final Aggregator[] aggregators, final QueryMemoryTracker memoryTracker )
        {
            this.aggregators = aggregators;
            this.memoryTracker = memoryTracker;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public AggregatingAccumulator newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new AggregatingAccumulator( argumentRowId, (Reducer[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) this.aggregators )) ).map( ( x$1 ) -> {
                return x$1.newStandardReducer( this.memoryTracker );
            }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Reducer.class ))),argumentRowIdsForReducers);
        }

        public AggregatingAccumulator newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new AggregatingAccumulator( argumentRowId, (Reducer[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) this.aggregators )) ).map( ( x$2 ) -> {
                return x$2.newConcurrentReducer();
            }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Reducer.class ))),argumentRowIdsForReducers);
        }
    }
}
