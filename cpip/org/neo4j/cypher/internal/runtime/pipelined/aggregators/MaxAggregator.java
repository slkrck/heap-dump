package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.concurrent.atomic.AtomicReference;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.MatchError;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public final class MaxAggregator
{
    public static String toString()
    {
        return MaxAggregator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return MaxAggregator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return MaxAggregator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return MaxAggregator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return MaxAggregator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return MaxAggregator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return MaxAggregator$.MODULE$.productPrefix();
    }

    public static boolean shouldUpdate( final AnyValue max, final AnyValue value )
    {
        return MaxAggregator$.MODULE$.shouldUpdate( var0, var1 );
    }

    public static Reducer newConcurrentReducer()
    {
        return MaxAggregator$.MODULE$.newConcurrentReducer();
    }

    public static Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return MaxAggregator$.MODULE$.newStandardReducer( var0 );
    }

    public static Updater newUpdater()
    {
        return MaxAggregator$.MODULE$.newUpdater();
    }

    public static class MaxConcurrentReducer implements Reducer
    {
        private final AtomicReference<AnyValue> max;

        public MaxConcurrentReducer()
        {
            this.max = new AtomicReference( Values.NO_VALUE );
        }

        private AtomicReference<AnyValue> max()
        {
            return this.max;
        }

        public void update( final Updater updater )
        {
            if ( updater instanceof MaxAggregator.MaxUpdater )
            {
                MaxAggregator.MaxUpdater var4 = (MaxAggregator.MaxUpdater) updater;
                this.max().updateAndGet( ( oldMax ) -> {
                    return MaxAggregator$.MODULE$.shouldUpdate( oldMax, var4.max() ) ? var4.max() : oldMax;
                } );
                BoxedUnit var2 = BoxedUnit.UNIT;
            }
            else
            {
                throw new MatchError( updater );
            }
        }

        public AnyValue result()
        {
            return (AnyValue) this.max().get();
        }
    }

    public static class MaxStandardReducer extends MaxAggregator.MaxUpdaterBase implements Reducer
    {
        public void update( final Updater updater )
        {
            if ( updater instanceof MaxAggregator.MaxUpdater )
            {
                MaxAggregator.MaxUpdater var4 = (MaxAggregator.MaxUpdater) updater;
                this.update( var4.max() );
                BoxedUnit var2 = BoxedUnit.UNIT;
            }
            else
            {
                throw new MatchError( updater );
            }
        }

        public AnyValue result()
        {
            return this.max();
        }
    }

    public static class MaxUpdater extends MaxAggregator.MaxUpdaterBase
    {
    }

    public abstract static class MaxUpdaterBase implements Updater
    {
        private AnyValue max;

        public MaxUpdaterBase()
        {
            this.max = Values.NO_VALUE;
        }

        public AnyValue max()
        {
            return this.max;
        }

        public void max_$eq( final AnyValue x$1 )
        {
            this.max = x$1;
        }

        public void update( final AnyValue value )
        {
            if ( value != Values.NO_VALUE && MaxAggregator$.MODULE$.shouldUpdate( this.max(), value ) )
            {
                this.max_$eq( value );
            }
        }
    }
}
