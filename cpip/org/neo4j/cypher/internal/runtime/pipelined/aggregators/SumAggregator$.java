package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class SumAggregator$ implements Aggregator, Product, Serializable
{
    public static SumAggregator$ MODULE$;

    static
    {
        new SumAggregator$();
    }

    private SumAggregator$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Updater newUpdater()
    {
        return new SumAggregator.SumUpdater();
    }

    public Reducer newStandardReducer( final QueryMemoryTracker memoryTracker )
    {
        return new SumAggregator.SumStandardReducer();
    }

    public Reducer newConcurrentReducer()
    {
        return new SumAggregator.SumConcurrentReducer();
    }

    public String productPrefix()
    {
        return "SumAggregator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SumAggregator$;
    }

    public int hashCode()
    {
        return -125017004;
    }

    public String toString()
    {
        return "SumAggregator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
