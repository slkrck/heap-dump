package org.neo4j.cypher.internal.runtime.pipelined.aggregators;

import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class CountConcurrentReducer implements Reducer
{
    private final AtomicLong count = new AtomicLong( 0L );

    private AtomicLong count()
    {
        return this.count;
    }

    public void update( final Updater updater )
    {
        if ( updater instanceof CountUpdaterBase )
        {
            CountUpdaterBase var4 = (CountUpdaterBase) updater;
            this.count().addAndGet( var4.count() );
            BoxedUnit var2 = BoxedUnit.UNIT;
        }
        else
        {
            throw new MatchError( updater );
        }
    }

    public AnyValue result()
    {
        return Values.longValue( this.count().get() );
    }
}
