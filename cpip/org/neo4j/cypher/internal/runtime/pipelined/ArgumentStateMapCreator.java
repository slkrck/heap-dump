package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ArgumentStateMapCreator
{
    <S extends ArgumentStateMap.ArgumentState> ArgumentStateMap<S> createArgumentStateMap( final int argumentStateMapId,
            final ArgumentStateMap.ArgumentStateFactory<S> factory );
}
