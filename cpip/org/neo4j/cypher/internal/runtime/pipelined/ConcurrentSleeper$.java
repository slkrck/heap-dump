package org.neo4j.cypher.internal.runtime.pipelined;

import java.util.concurrent.TimeUnit;

import scala.concurrent.duration.Duration;
import scala.concurrent.duration.Duration.;

public final class ConcurrentSleeper$
{
    public static ConcurrentSleeper$ MODULE$;

    static
    {
        new ConcurrentSleeper$();
    }

    private ConcurrentSleeper$()
    {
        MODULE$ = this;
    }

    public Duration $lessinit$greater$default$2()
    {
        return .MODULE$.apply( 1L, TimeUnit.SECONDS );
    }
}
