package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.operators.NoOutputOperator$;
import org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CleanUpTask implements Task<QueryResources>
{
    private final ExecutionState executionState;

    public CleanUpTask( final ExecutionState executionState )
    {
        this.executionState = executionState;
        WorkIdentity.$init$( this );
        Task.$init$( this );
    }

    public String toString()
    {
        return Task.toString$( this );
    }

    public PreparedOutput executeWorkUnit( final QueryResources resources, final WorkUnitEvent workUnitEvent, final QueryProfiler queryProfiler )
    {
        this.executionState.cancelQuery( resources );
        return NoOutputOperator$.MODULE$;
    }

    public boolean canContinue()
    {
        return false;
    }

    public int workId()
    {
        return .MODULE$.INVALID_ID();
    }

    public String workDescription()
    {
        return "Cleaning up the ExecutionState";
    }
}
