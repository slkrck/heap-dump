package org.neo4j.cypher.internal.runtime.pipelined.expressions;

import org.neo4j.cypher.internal.ir.ProvidedOrder;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.NestedPlanExpression;
import org.neo4j.cypher.internal.logical.plans.ResolvedFunctionInvocation;
import org.neo4j.cypher.internal.planner.spi.PlanningAttributes.ProvidedOrders;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import org.neo4j.cypher.internal.v4_0.util.Foldable.FoldableAny.;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.Function1;
import scala.Serializable;
import scala.collection.immutable.Set;
import scala.runtime.ObjectRef;

public final class PipelinedBlacklist$
{
    public static PipelinedBlacklist$ MODULE$;

    static
    {
        new PipelinedBlacklist$();
    }

    private PipelinedBlacklist$()
    {
        MODULE$ = this;
    }

    public void throwOnUnsupportedPlan( final LogicalPlan logicalPlan, final boolean parallelExecution, final ProvidedOrders providedOrders )
    {
        Set unsupport = (Set).MODULE$.fold$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( logicalPlan ), scala.Predef..
        MODULE$.Set().apply( scala.collection.immutable.Nil..MODULE$),new Serializable( parallelExecution, providedOrders )
    {
        public static final long serialVersionUID = 0L;
        private final boolean parallelExecution$1;
        private final ProvidedOrders providedOrders$1;

        public
        {
            this.parallelExecution$1 = parallelExecution$1;
            this.providedOrders$1 = providedOrders$1;
        }

        public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
        {
            boolean var4 = false;
            ObjectRef var5 = ObjectRef.create( (Object) null );
            Object var3;
            if ( x1 instanceof NestedPlanExpression )
            {
                var3 = ( x$1 ) -> {
                    return (Set) x$1.$plus( "Nested plan expressions" );
                };
            }
            else if ( x1 instanceof ResolvedFunctionInvocation && this.parallelExecution$1 )
            {
                var3 = ( x$2 ) -> {
                    return (Set) x$2.$plus( "User-defined functions" );
                };
            }
            else
            {
                label81:
                {
                    Function var10000;
                    if ( x1 instanceof FunctionInvocation )
                    {
                        var4 = true;
                        var5.elem = (FunctionInvocation) x1;
                        var10000 = ((FunctionInvocation) var5.elem).function();
                        org.neo4j.cypher.internal.v4_0.expressions.functions.Linenumber.var7 = org.neo4j.cypher.internal.v4_0.expressions.functions.Linenumber..
                        MODULE$;
                        if ( var10000 == null )
                        {
                            if ( var7 == null )
                            {
                                break label81;
                            }
                        }
                        else if ( var10000.equals( var7 ) )
                        {
                            break label81;
                        }

                        var10000 = ((FunctionInvocation) var5.elem).function();
                        org.neo4j.cypher.internal.v4_0.expressions.functions.File.var8 = org.neo4j.cypher.internal.v4_0.expressions.functions.File..MODULE$;
                        if ( var10000 == null )
                        {
                            if ( var8 == null )
                            {
                                break label81;
                            }
                        }
                        else if ( var10000.equals( var8 ) )
                        {
                            break label81;
                        }
                    }

                    if ( var4 )
                    {
                        label48:
                        {
                            var10000 = ((FunctionInvocation) var5.elem).function();
                            org.neo4j.cypher.internal.v4_0.expressions.functions.Type.var9 = org.neo4j.cypher.internal.v4_0.expressions.functions.Type..MODULE$;
                            if ( var10000 == null )
                            {
                                if ( var9 != null )
                                {
                                    break label48;
                                }
                            }
                            else if ( !var10000.equals( var9 ) )
                            {
                                break label48;
                            }

                            if ( this.parallelExecution$1 )
                            {
                                var3 = ( x$4 ) -> {
                                    return (Set) x$4.$plus( (new StringBuilder( 2 )).append( ((FunctionInvocation) var5.elem).functionName().name() ).append(
                                            "()" ).toString() );
                                };
                                return var3;
                            }
                        }
                    }

                    if ( x1 instanceof CartesianProduct )
                    {
                        CartesianProduct var10 = (CartesianProduct) x1;
                        if ( !((ProvidedOrder) this.providedOrders$1.getOrElse( var10.left().id(), () -> {
                            return org.neo4j.cypher.internal.ir.ProvidedOrder..MODULE$.empty();
                        } )).isEmpty() )
                        {
                            var3 = ( x$5 ) -> {
                                return (Set) x$5.$plus( "CartesianProduct if the LHS has a provided order" );
                            };
                            return var3;
                        }
                    }

                    var3 = var2.apply( x1 );
                    return var3;
                }

                var3 = ( x$3 ) -> {
                    return (Set) x$3.$plus(
                            (new StringBuilder( 2 )).append( ((FunctionInvocation) var5.elem).functionName().name() ).append( "()" ).toString() );
                };
            }

            return var3;
        }

        public final boolean isDefinedAt( final Object x1 )
        {
            boolean var3 = false;
            FunctionInvocation var4 = null;
            boolean var2;
            if ( x1 instanceof NestedPlanExpression )
            {
                var2 = true;
            }
            else if ( x1 instanceof ResolvedFunctionInvocation && this.parallelExecution$1 )
            {
                var2 = true;
            }
            else
            {
                label81:
                {
                    Function var10000;
                    if ( x1 instanceof FunctionInvocation )
                    {
                        var3 = true;
                        var4 = (FunctionInvocation) x1;
                        var10000 = var4.function();
                        org.neo4j.cypher.internal.v4_0.expressions.functions.Linenumber.var6 = org.neo4j.cypher.internal.v4_0.expressions.functions.Linenumber..
                        MODULE$;
                        if ( var10000 == null )
                        {
                            if ( var6 == null )
                            {
                                break label81;
                            }
                        }
                        else if ( var10000.equals( var6 ) )
                        {
                            break label81;
                        }

                        var10000 = var4.function();
                        org.neo4j.cypher.internal.v4_0.expressions.functions.File.var7 = org.neo4j.cypher.internal.v4_0.expressions.functions.File..MODULE$;
                        if ( var10000 == null )
                        {
                            if ( var7 == null )
                            {
                                break label81;
                            }
                        }
                        else if ( var10000.equals( var7 ) )
                        {
                            break label81;
                        }
                    }

                    if ( var3 )
                    {
                        label48:
                        {
                            var10000 = var4.function();
                            org.neo4j.cypher.internal.v4_0.expressions.functions.Type.var8 = org.neo4j.cypher.internal.v4_0.expressions.functions.Type..MODULE$;
                            if ( var10000 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label48;
                                }
                            }
                            else if ( !var10000.equals( var8 ) )
                            {
                                break label48;
                            }

                            if ( this.parallelExecution$1 )
                            {
                                var2 = true;
                                return var2;
                            }
                        }
                    }

                    if ( x1 instanceof CartesianProduct )
                    {
                        CartesianProduct var9 = (CartesianProduct) x1;
                        if ( !((ProvidedOrder) this.providedOrders$1.getOrElse( var9.left().id(), () -> {
                            return org.neo4j.cypher.internal.ir.ProvidedOrder..MODULE$.empty();
                        } )).isEmpty() )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    var2 = false;
                    return var2;
                }

                var2 = true;
            }

            return var2;
        }
    });
        if ( unsupport.nonEmpty() )
        {
            throw new CantCompileQueryException(
                    (new StringBuilder( 53 )).append( "Pipelined does not yet support " ).append( unsupport.mkString( "`", "`, `", "`" ) ).append(
                            ", use another runtime." ).toString() );
        }
    }
}
