package org.neo4j.cypher.internal.runtime.pipelined.expressions;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.planner.spi.PlanningAttributes.ProvidedOrders;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class PipelinedBlacklist
{
    public static void throwOnUnsupportedPlan( final LogicalPlan logicalPlan, final boolean parallelExecution, final ProvidedOrders providedOrders )
    {
        PipelinedBlacklist$.MODULE$.throwOnUnsupportedPlan( var0, var1, var2 );
    }
}
