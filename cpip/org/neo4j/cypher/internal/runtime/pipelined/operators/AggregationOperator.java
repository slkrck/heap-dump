package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap$;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class AggregationOperator implements Product, Serializable
{
    private final WorkIdentity workIdentity;
    private final Aggregator[] aggregations;
    private final GroupingExpression groupings;
    private final Function<AnyValue,Updater[]> org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$$newUpdaters;
    private volatile AggregationOperator.AggregatingAccumulator$ AggregatingAccumulator$module;

    public AggregationOperator( final WorkIdentity workIdentity, final Aggregator[] aggregations, final GroupingExpression groupings )
    {
        this.workIdentity = workIdentity;
        this.aggregations = aggregations;
        this.groupings = groupings;
        Product.$init$( this );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$$newUpdaters = ( x$1 ) -> {
            return (Updater[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.aggregations() ))).map( ( x$2 ) -> {
                return x$2.newUpdater();
            }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Updater.class )));
        };
    }

    public static Option<Tuple3<WorkIdentity,Aggregator[],GroupingExpression>> unapply( final AggregationOperator x$0 )
    {
        return AggregationOperator$.MODULE$.unapply( var0 );
    }

    public static AggregationOperator apply( final WorkIdentity workIdentity, final Aggregator[] aggregations, final GroupingExpression groupings )
    {
        return AggregationOperator$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<WorkIdentity,Aggregator[],GroupingExpression>,AggregationOperator> tupled()
    {
        return AggregationOperator$.MODULE$.tupled();
    }

    public static Function1<WorkIdentity,Function1<Aggregator[],Function1<GroupingExpression,AggregationOperator>>> curried()
    {
        return AggregationOperator$.MODULE$.curried();
    }

    public AggregationOperator.AggregatingAccumulator$ AggregatingAccumulator()
    {
        if ( this.AggregatingAccumulator$module == null )
        {
            this.AggregatingAccumulator$lzycompute$1();
        }

        return this.AggregatingAccumulator$module;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public Aggregator[] aggregations()
    {
        return this.aggregations;
    }

    public GroupingExpression groupings()
    {
        return this.groupings;
    }

    public Function<AnyValue,Updater[]> org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$$newUpdaters()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$$newUpdaters;
    }

    public AggregationOperator.AggregationMapperOperator mapper( final int argumentSlotOffset, final int outputBufferId, final Expression[] expressionValues )
    {
        return new AggregationOperator.AggregationMapperOperator( this, argumentSlotOffset, outputBufferId, expressionValues );
    }

    public AggregationOperator.AggregationReduceOperator reducer( final int argumentStateMapId, final int[] reducerOutputSlots )
    {
        return new AggregationOperator.AggregationReduceOperator( this, argumentStateMapId, reducerOutputSlots );
    }

    public AggregationOperator copy( final WorkIdentity workIdentity, final Aggregator[] aggregations, final GroupingExpression groupings )
    {
        return new AggregationOperator( workIdentity, aggregations, groupings );
    }

    public WorkIdentity copy$default$1()
    {
        return this.workIdentity();
    }

    public Aggregator[] copy$default$2()
    {
        return this.aggregations();
    }

    public GroupingExpression copy$default$3()
    {
        return this.groupings();
    }

    public String productPrefix()
    {
        return "AggregationOperator";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.workIdentity();
            break;
        case 1:
            var10000 = this.aggregations();
            break;
        case 2:
            var10000 = this.groupings();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AggregationOperator;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label64:
            {
                boolean var2;
                if ( x$1 instanceof AggregationOperator )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        label46:
                        {
                            AggregationOperator var4 = (AggregationOperator) x$1;
                            WorkIdentity var10000 = this.workIdentity();
                            WorkIdentity var5 = var4.workIdentity();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label46;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label46;
                            }

                            if ( this.aggregations() == var4.aggregations() )
                            {
                                label40:
                                {
                                    GroupingExpression var7 = this.groupings();
                                    GroupingExpression var6 = var4.groupings();
                                    if ( var7 == null )
                                    {
                                        if ( var6 != null )
                                        {
                                            break label40;
                                        }
                                    }
                                    else if ( !var7.equals( var6 ) )
                                    {
                                        break label40;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var8 = true;
                                        break label47;
                                    }
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label64;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }

    private final void AggregatingAccumulator$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.AggregatingAccumulator$module == null )
            {
                this.AggregatingAccumulator$module = new AggregationOperator.AggregatingAccumulator$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public abstract class AggregatingAccumulator implements ArgumentStateMap.MorselAccumulator<LinkedHashMap<AnyValue,Updater[]>>
    {
        public AggregatingAccumulator( final AggregationOperator $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public abstract java.util.Iterator<Entry<AnyValue,Reducer[]>> result();
    }

    public class AggregatingAccumulator$
    {
        public AggregatingAccumulator$( final AggregationOperator $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }
    }

    public class AggregationMapperOperator implements OutputOperator
    {
        public final int org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$argumentSlotOffset;
        public final Expression[] org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$expressionValues;
        private final int outputBufferId;

        public AggregationMapperOperator( final AggregationOperator $outer, final int argumentSlotOffset, final int outputBufferId,
                final Expression[] expressionValues )
        {
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$argumentSlotOffset = argumentSlotOffset;
            this.outputBufferId = outputBufferId;
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$expressionValues = expressionValues;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$$outer().workIdentity();
        }

        public Option<BufferId> outputBuffer()
        {
            return new Some( new BufferId( this.outputBufferId ) );
        }

        public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
        {
            return new AggregationOperator.AggregationMapperOperator.State( this, executionState.getSink( pipelineId, this.outputBufferId ) );
        }

        public class PreAggregatedOutput implements PreparedOutput
        {
            private final IndexedSeq<ArgumentStateMap.PerArgument<LinkedHashMap<AnyValue,Updater[]>>> preAggregated;
            private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<LinkedHashMap<AnyValue,Updater[]>>>> sink;

            public PreAggregatedOutput( final AggregationOperator.AggregationMapperOperator $outer,
                    final IndexedSeq<ArgumentStateMap.PerArgument<LinkedHashMap<AnyValue,Updater[]>>> preAggregated,
                    final Sink<IndexedSeq<ArgumentStateMap.PerArgument<LinkedHashMap<AnyValue,Updater[]>>>> sink )
            {
                this.preAggregated = preAggregated;
                this.sink = sink;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                }
            }

            public void produce()
            {
                this.sink.put( this.preAggregated );
            }
        }

        public class State implements OutputOperatorState
        {
            private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<LinkedHashMap<AnyValue,Updater[]>>>> sink;

            public State( final AggregationOperator.AggregationMapperOperator $outer,
                    final Sink<IndexedSeq<ArgumentStateMap.PerArgument<LinkedHashMap<AnyValue,Updater[]>>>> sink )
            {
                this.sink = sink;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    OutputOperatorState.$init$( this );
                }
            }

            public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                    final QueryResources resources, final QueryProfiler queryProfiler )
            {
                return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
            }

            public boolean canContinueOutput()
            {
                return OutputOperatorState.canContinueOutput$( this );
            }

            public boolean trackTime()
            {
                return true;
            }

            public WorkIdentity workIdentity()
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$$outer().workIdentity();
            }

            public AggregationOperator.AggregationMapperOperator.PreAggregatedOutput prepareOutput( final MorselExecutionContext morsel,
                    final QueryContext context, final QueryState state, final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
            {
                SlottedQueryState queryState = new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(),
                        (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
                resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
                MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
                ;
                IndexedSeq preAggregated = ArgumentStateMap$.MODULE$.map(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$argumentSlotOffset,
                        morsel, ( morselx ) -> {
                            return this.preAggregate( queryState, morselx );
                        } );
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer().new PreAggregatedOutput(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer(), preAggregated,
                        this.sink );
            }

            private LinkedHashMap<AnyValue,Updater[]> preAggregate( final SlottedQueryState queryState, final MorselExecutionContext morsel )
            {
                LinkedHashMap result = new LinkedHashMap();

                while ( morsel.isValidRow() )
                {
                    AnyValue groupingValue =
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$$outer().groupings().computeGroupingKey(
                                    morsel, queryState );
                    Updater[] updaters = (Updater[]) result.computeIfAbsent( groupingValue,
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$$newUpdaters() );

                    for ( int i = 0; i <
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$$outer().aggregations().length;
                            ++i )
                    {
                        AnyValue value =
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationMapperOperator$$expressionValues[i].apply(
                                        morsel, queryState );
                        updaters[i].update( value );
                    }

                    morsel.moveToNextRow();
                }

                return result;
            }
        }
    }

    public class AggregationReduceOperator
            implements Operator, ReduceOperatorState<LinkedHashMap<AnyValue,Updater[]>,AggregationOperator.AggregatingAccumulator>
    {
        public final int[] org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$reducerOutputSlots;
        private final int argumentStateMapId;

        public AggregationReduceOperator( final AggregationOperator $outer, final int argumentStateMapId, final int[] reducerOutputSlots )
        {
            this.argumentStateMapId = argumentStateMapId;
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$reducerOutputSlots = reducerOutputSlots;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                ReduceOperatorState.$init$( this );
            }
        }

        public final IndexedSeq<ContinuableOperatorTaskWithAccumulator<LinkedHashMap<AnyValue,Updater[]>,AggregationOperator.AggregatingAccumulator>> nextTasks(
                final QueryContext context, final QueryState state, final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
                final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
        {
            return ReduceOperatorState.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
        }

        public int argumentStateMapId()
        {
            return this.argumentStateMapId;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$$outer().workIdentity();
        }

        public ReduceOperatorState<LinkedHashMap<AnyValue,Updater[]>,AggregationOperator.AggregatingAccumulator> createState(
                final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext, final QueryState state,
                final QueryResources resources )
        {
            argumentStateCreator.createArgumentStateMap( this.argumentStateMapId(), new AggregationOperator$AggregatingAccumulator$Factory(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$$outer().AggregatingAccumulator(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$$outer().aggregations(),
                    stateFactory.memoryTracker() ) );
            return this;
        }

        public IndexedSeq<ContinuableOperatorTaskWithAccumulator<LinkedHashMap<AnyValue,Updater[]>,AggregationOperator.AggregatingAccumulator>> nextTasks(
                final QueryContext queryContext, final QueryState state, final AggregationOperator.AggregatingAccumulator input,
                final QueryResources resources )
        {
            return scala.Predef..MODULE$.wrapRefArray( (Object[]) ((Object[]) (new AggregationOperator.AggregationReduceOperator.OTask[]{
                new AggregationOperator.AggregationReduceOperator.OTask( this, input )})) );
        }

        public class OTask implements ContinuableOperatorTaskWithAccumulator<LinkedHashMap<AnyValue,Updater[]>,AggregationOperator.AggregatingAccumulator>
        {
            private final AggregationOperator.AggregatingAccumulator accumulator;
            private final java.util.Iterator<Entry<AnyValue,Reducer[]>> resultIterator;

            public OTask( final AggregationOperator.AggregationReduceOperator $outer, final AggregationOperator.AggregatingAccumulator accumulator )
            {
                this.accumulator = accumulator;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    OperatorTask.$init$( this );
                    ContinuableOperatorTask.$init$( this );
                    ContinuableOperatorTaskWithAccumulator.$init$( this );
                    this.resultIterator = accumulator.result();
                }
            }

            public void closeInput( final OperatorCloser operatorCloser )
            {
                ContinuableOperatorTaskWithAccumulator.closeInput$( this, operatorCloser );
            }

            public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
            {
                return ContinuableOperatorTaskWithAccumulator.filterCancelledArguments$( this, operatorCloser );
            }

            public WorkUnitEvent producingWorkUnitEvent()
            {
                return ContinuableOperatorTaskWithAccumulator.producingWorkUnitEvent$( this );
            }

            public void setExecutionEvent( final OperatorProfileEvent event )
            {
                ContinuableOperatorTaskWithAccumulator.setExecutionEvent$( this, event );
            }

            public void closeCursors( final QueryResources resources )
            {
                ContinuableOperatorTaskWithAccumulator.closeCursors$( this, resources );
            }

            public long estimatedHeapUsage()
            {
                return ContinuableOperatorTaskWithAccumulator.estimatedHeapUsage$( this );
            }

            public void close( final OperatorCloser operatorCloser, final QueryResources resources )
            {
                ContinuableOperatorTask.close$( this, operatorCloser, resources );
            }

            public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                    final QueryResources resources, final QueryProfiler queryProfiler )
            {
                OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
            }

            public AggregationOperator.AggregatingAccumulator accumulator()
            {
                return this.accumulator;
            }

            public WorkIdentity workIdentity()
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$$outer().workIdentity();
            }

            private java.util.Iterator<Entry<AnyValue,Reducer[]>> resultIterator()
            {
                return this.resultIterator;
            }

            public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
            {
                while ( this.resultIterator().hasNext() && outputRow.isValidRow() )
                {
                    Entry entry = (Entry) this.resultIterator().next();
                    AnyValue key = (AnyValue) entry.getKey();
                    Reducer[] reducers = (Reducer[]) entry.getValue();
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$$outer().groupings().project(
                            outputRow, key );

                    for ( int i = 0; i <
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$$outer().aggregations().length;
                            ++i )
                    {
                        outputRow.setRefAt(
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregationReduceOperator$$reducerOutputSlots[i],
                                reducers[i].result() );
                    }

                    outputRow.moveToNextRow();
                }

                outputRow.finishedWriting();
            }

            public boolean canContinue()
            {
                return this.resultIterator().hasNext();
            }
        }
    }

    public class ConcurrentAggregatingAccumulator extends AggregationOperator.AggregatingAccumulator
    {
        private final long argumentRowId;
        private final Aggregator[] aggregators;
        private final long[] argumentRowIdsForReducers;
        private final ConcurrentHashMap<AnyValue,Reducer[]> reducerMap;

        public ConcurrentAggregatingAccumulator( final AggregationOperator $outer, final long argumentRowId, final Aggregator[] aggregators,
                final long[] argumentRowIdsForReducers )
        {
            super( $outer );
            this.argumentRowId = argumentRowId;
            this.aggregators = aggregators;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.reducerMap = new ConcurrentHashMap();
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        public ConcurrentHashMap<AnyValue,Reducer[]> reducerMap()
        {
            return this.reducerMap;
        }

        public void update( final LinkedHashMap<AnyValue,Updater[]> data )
        {
            java.util.Iterator iterator = data.entrySet().iterator();

            while ( iterator.hasNext() )
            {
                Entry entry = (Entry) iterator.next();
                Reducer[] reducers = (Reducer[]) this.reducerMap().computeIfAbsent( entry.getKey(), ( key ) -> {
                    return (Reducer[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.aggregators ))).map( ( x$5 ) -> {
                        return x$5.newConcurrentReducer();
                    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Reducer.class )));
                } );

                for ( int i = 0; i < reducers.length; ++i )
                {
                    reducers[i].update( ((Updater[]) entry.getValue())[i] );
                }
            }
        }

        public java.util.Iterator<Entry<AnyValue,Reducer[]>> result()
        {
            return this.reducerMap().entrySet().iterator();
        }
    }

    public class StandardAggregatingAccumulator extends AggregationOperator.AggregatingAccumulator
    {
        private final long argumentRowId;
        private final Aggregator[] aggregators;
        private final long[] argumentRowIdsForReducers;
        private final QueryMemoryTracker memoryTracker;
        private final LinkedHashMap<AnyValue,Reducer[]> reducerMap;

        public StandardAggregatingAccumulator( final AggregationOperator $outer, final long argumentRowId, final Aggregator[] aggregators,
                final long[] argumentRowIdsForReducers, final QueryMemoryTracker memoryTracker )
        {
            super( $outer );
            this.argumentRowId = argumentRowId;
            this.aggregators = aggregators;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.memoryTracker = memoryTracker;
            this.reducerMap = new LinkedHashMap();
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        public LinkedHashMap<AnyValue,Reducer[]> reducerMap()
        {
            return this.reducerMap;
        }

        public void update( final LinkedHashMap<AnyValue,Updater[]> data )
        {
            java.util.Iterator iterator = data.entrySet().iterator();

            while ( iterator.hasNext() )
            {
                Entry entry = (Entry) iterator.next();
                Reducer[] reducers = (Reducer[]) this.reducerMap().computeIfAbsent( entry.getKey(), ( x$3 ) -> {
                    this.memoryTracker.allocated( (AnyValue) entry.getKey() );
                    return (Reducer[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.aggregators ))).map( ( x$4 ) -> {
                        return x$4.newStandardReducer( this.memoryTracker );
                    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Reducer.class )));
                } );

                for ( int i = 0; i < reducers.length; ++i )
                {
                    reducers[i].update( ((Updater[]) entry.getValue())[i] );
                }
            }
        }

        public java.util.Iterator<Entry<AnyValue,Reducer[]>> result()
        {
            return this.reducerMap().entrySet().iterator();
        }
    }
}
