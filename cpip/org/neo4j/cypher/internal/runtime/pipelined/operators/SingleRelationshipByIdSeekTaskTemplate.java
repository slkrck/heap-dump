package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class SingleRelationshipByIdSeekTaskTemplate extends RelationshipByIdSeekTaskTemplate
{
    private final Expression relIdExpr;
    private final LocalVariable idVariable;

    public SingleRelationshipByIdSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int relationshipOffset, final int fromOffset, final int toOffset, final Expression relIdExpr, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, relationshipOffset, fromOffset, toOffset, relIdExpr, argumentSize, codeGen );
        this.relIdExpr = relIdExpr;
        this.idVariable = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( super.codeGen().namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                BoxesRunTime.boxToLong( -1L ) ), scala.reflect.ManifestFactory..MODULE$.Long());
    }

    public LocalVariable idVariable()
    {
        return this.idVariable;
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V(), this.idVariable()}) ));
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        this.relationshipExpression_$eq( (IntermediateExpression) super.codeGen().intermediateCompileExpression( this.relIdExpr ).getOrElse( () -> {
            throw new CantCompileQueryException(
                    (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( this.relIdExpr ).toString() );
        } ) );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.cursor(), this.executionEventField(),
                            OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_REL_SCAN_CURSOR() ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( this.idVariable(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( RelationshipByIdSeekOperator$.MODULE$.asIdMethod(),
                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.relationshipExpression(),
                            ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.greaterThanOrEqual(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.idVariable() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToLong( 0L ) )),
        OperatorCodeGenHelperTemplates$.MODULE$.singleRelationship( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                this.idVariable() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.greaterThanOrEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
            this.idVariable() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( 0L ) )),
        OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.cursor() ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class )))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )})));
    }
}
