package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import scala.runtime.BoxesRunTime;

public final class VarExpandPredicate$
{
    public static VarExpandPredicate$ MODULE$;

    static
    {
        new VarExpandPredicate$();
    }

    private final VarExpandPredicate<Object> NO_NODE_PREDICATE;
    private final VarExpandPredicate<RelationshipSelectionCursor> NO_RELATIONSHIP_PREDICATE;

    private VarExpandPredicate$()
    {
        MODULE$ = this;
        this.NO_NODE_PREDICATE = ( x$3 ) -> {
            return $anonfun$NO_NODE_PREDICATE$1( BoxesRunTime.unboxToLong( x$3 ) );
        };
        this.NO_RELATIONSHIP_PREDICATE = ( x$4 ) -> {
            return true;
        };
    }

    public VarExpandPredicate<Object> NO_NODE_PREDICATE()
    {
        return this.NO_NODE_PREDICATE;
    }

    public VarExpandPredicate<RelationshipSelectionCursor> NO_RELATIONSHIP_PREDICATE()
    {
        return this.NO_RELATIONSHIP_PREDICATE;
    }
}
