package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SingleNodeByIdSeekTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int offset;
    private final Expression nodeIdExpr;
    private final SlotConfiguration.Size argumentSize;
    private final LocalVariable idVariable;
    private IntermediateExpression nodeId;

    public SingleNodeByIdSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final String nodeVarName, final int offset, final Expression nodeIdExpr, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.offset = offset;
        this.nodeIdExpr = nodeIdExpr;
        this.argumentSize = argumentSize;
        this.idVariable = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( super.codeGen().namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                BoxesRunTime.boxToLong( -1L ) ), scala.reflect.ManifestFactory..MODULE$.Long());
    }

    private LocalVariable idVariable()
    {
        return this.idVariable;
    }

    private IntermediateExpression nodeId()
    {
        return this.nodeId;
    }

    private void nodeId_$eq( final IntermediateExpression x$1 )
    {
        this.nodeId = x$1;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.empty();
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V(), this.idVariable()}) ));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.nodeId()}) ));
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        this.nodeId_$eq( (IntermediateExpression) super.codeGen().intermediateCompileExpression( this.nodeIdExpr ).getOrElse( () -> {
            throw new CantCompileQueryException(
                    (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( this.nodeIdExpr ).toString() );
        } ) );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( this.idVariable(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( NodeByIdSeekOperator$.MODULE$.asIdMethod(),
                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.nodeId(),
                            ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(),
                NodeByIdSeekOperator$.MODULE$.isValidNode( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.idVariable().name() ) )),
        OperatorCodeGenHelperTemplates$.MODULE$.onNode( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.executionEventField() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.idVariable() )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                        super.codeGen().setLongAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.idVariable() )),
                super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( OperatorCodeGenHelperTemplates$.MODULE$.profileRow( super.id() ) ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }
}
