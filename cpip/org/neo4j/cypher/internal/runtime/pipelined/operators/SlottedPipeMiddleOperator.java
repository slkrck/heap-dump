package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescription;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SlottedPipeMiddleOperator extends SlottedPipeOperator implements MiddleOperator
{
    private final Pipe initialPipe;

    public SlottedPipeMiddleOperator( final WorkIdentityMutableDescription workIdentity, final Pipe initialPipe )
    {
        super( workIdentity, initialPipe );
        this.initialPipe = initialPipe;
    }

    public Pipe initialPipe()
    {
        return this.initialPipe;
    }

    public String toString()
    {
        return super.workIdentity().toString();
    }

    public OperatorTask createTask( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        FeedPipeQueryState inputQueryState =
                SlottedPipeOperator$.MODULE$.createFeedPipeQueryState( (MorselExecutionContext) null, queryContext, state, resources,
                        stateFactory.memoryTracker() );
        return new SlottedPipeMiddleOperator.OMiddleTask( this, inputQueryState );
    }

    public class OMiddleTask implements OperatorWithInterpretedDBHitsProfiling
    {
        private final FeedPipeQueryState feedPipeQueryState;
        private Iterator<ExecutionContext> resultIterator;
        private OperatorProfileEvent profileEvent;
        private boolean _canContinue;

        public OMiddleTask( final SlottedPipeMiddleOperator $outer, final FeedPipeQueryState feedPipeQueryState )
        {
            this.feedPipeQueryState = feedPipeQueryState;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                OperatorWithInterpretedDBHitsProfiling.$init$( this );
                this._canContinue = false;
            }
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorWithInterpretedDBHitsProfiling.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public FeedPipeQueryState feedPipeQueryState()
        {
            return this.feedPipeQueryState;
        }

        private Iterator<ExecutionContext> resultIterator()
        {
            return this.resultIterator;
        }

        private void resultIterator_$eq( final Iterator<ExecutionContext> x$1 )
        {
            this.resultIterator = x$1;
        }

        private OperatorProfileEvent profileEvent()
        {
            return this.profileEvent;
        }

        private void profileEvent_$eq( final OperatorProfileEvent x$1 )
        {
            this.profileEvent = x$1;
        }

        private boolean _canContinue()
        {
            return this._canContinue;
        }

        private void _canContinue_$eq( final boolean x$1 )
        {
            this._canContinue = x$1;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeMiddleOperator$OMiddleTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeMiddleOperator$$super$workIdentity();
        }

        public String toString()
        {
            return (new StringBuilder( 23 )).append( "SlottedPipeMiddleTask(" ).append(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeMiddleOperator$OMiddleTask$$$outer().pipe() ).append(
                    ")" ).toString();
        }

        public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            MorselExecutionContext feedRow = outputRow.shallowCopy();
            this.feedPipeQueryState().morsel_$eq( feedRow );
            if ( this.resultIterator() == null )
            {
                this.resultIterator_$eq(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeMiddleOperator$OMiddleTask$$$outer().pipe().createResults(
                                this.feedPipeQueryState() ) );
            }

            this._canContinue_$eq( this.resultIterator().hasNext() );

            while ( this._canContinue() )
            {
                ExecutionContext resultRow = (ExecutionContext) this.resultIterator().next();
                outputRow.copyFrom( resultRow, outputRow.getLongsPerRow(), outputRow.getRefsPerRow() );
                outputRow.moveToNextRow();
                this._canContinue_$eq( this.resultIterator().hasNext() );
            }

            if ( this.profileEvent() != null && this.feedPipeQueryState().profileInformation() != null )
            {
                SlottedPipeOperator$.MODULE$.updateProfileEvent( this.profileEvent(), this.feedPipeQueryState().profileInformation() );
            }

            if ( !this._canContinue() )
            {
                this.resultIterator_$eq( (Iterator) null );
            }

            outputRow.finishedWriting();
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            this.profileEvent_$eq( event );
        }
    }
}
