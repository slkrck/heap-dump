package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.TypeReference;
import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Constructor;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.MethodDeclaration;
import org.neo4j.codegen.api.Parameter;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.internal.kernel.api.Read;
import scala.Function0;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.IndexedSeq;
import scala.reflect.Manifest;
import scala.runtime.BoxesRunTime;

public final class CompiledStreamingOperator$
{
    public static CompiledStreamingOperator$ MODULE$;

    static
    {
        new CompiledStreamingOperator$();
    }

    private CompiledStreamingOperator$()
    {
        MODULE$ = this;
    }

    private static final String staticFieldName$1( final Object obj )
    {
        return (new StringBuilder( 6 )).append( "FIELD_" ).append( System.identityHashCode( obj ) ).toString();
    }

    public ClassDeclaration<CompiledStreamingOperator> getClassDeclaration( final String packageName, final String className,
            final Class<CompiledTask> taskClazz, final StaticField workIdentityField,
            final Seq<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates )
    {
        IndexedSeq argumentStateFields = ((TraversableOnce) argumentStates.map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                ArgumentStateMap.ArgumentStateFactory factory = (ArgumentStateMap.ArgumentStateFactory) x0$1._2();
                StaticField var1 = .MODULE$.staticConstant( staticFieldName$1( factory ), factory, scala.reflect.ManifestFactory..MODULE$.classType(
                    ArgumentStateMap.ArgumentStateFactory.class, scala.reflect.ManifestFactory..MODULE$.classType(
                    ArgumentStateMap.ArgumentState.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )));
                return var1;
            }
            else
            {
                throw new MatchError( x0$1 );
            }
        }, scala.collection.Seq..MODULE$.canBuildFrom())).toIndexedSeq();
        IntermediateRepresentation createState = .MODULE$.block( (Seq) argumentStates.map( ( x0$2 ) -> {
        if ( x0$2 != null )
        {
            int argumentStateMapId = ((ArgumentStateMapId) x0$2._1()).x();
            ArgumentStateMap.ArgumentStateFactory factory = (ArgumentStateMap.ArgumentStateFactory) x0$2._2();
            IntermediateRepresentation var1 = .MODULE$.invokeSideEffect(.MODULE$.load( "argumentStateCreator" ), .
            MODULE$.method( "createArgumentStateMap", scala.reflect.ManifestFactory..MODULE$.classType(
                    ArgumentStateMapCreator.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( ArgumentStateMap.class, scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.ArgumentState.class ), scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
            MODULE$.classType( ArgumentStateMap.ArgumentStateFactory.class, scala.reflect.ManifestFactory..MODULE$.classType(
                    ArgumentStateMap.ArgumentState.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..
            MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( argumentStateMapId ) ),.MODULE$.getStatic(
                    staticFieldName$1( factory ), scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.ArgumentStateFactory.class,
                    scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.ArgumentState.class ), scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )))})));
            return var1;
        }
        else
        {
            throw new MatchError( x0$2 );
        }
    }, scala.collection.Seq..MODULE$.canBuildFrom() ));
        scala.None.x$8 = scala.None..MODULE$;
        Seq x$9 = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new TypeReference[]{.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( CompiledStreamingOperator.class ))})));
        Seq x$10 = (Seq) scala.collection.Seq..MODULE$.apply( scala.collection.immutable.Nil..MODULE$);
        IntermediateRepresentation x$11 = .MODULE$.noop();
        Seq x$12 = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new MethodDeclaration[]{
            new MethodDeclaration( "compiledNextTask",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                    ContinuableOperatorTaskWithMorsel.class )), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new Parameter[]{.MODULE$.param( "dataRead", scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),.MODULE$.param(
            "inputMorsel", scala.reflect.ManifestFactory..MODULE$.classType( MorselExecutionContext.class )), .
        MODULE$.param( "argumentStateMaps", scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.ArgumentStateMaps.class ))}))), .
        MODULE$.newInstance(
                new Constructor( TypeReference.typeReference( taskClazz ), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[]{TypeReference.typeReference( Read.class ), TypeReference.typeReference( MorselExecutionContext.class ),
                                TypeReference.typeReference( ArgumentStateMap.ArgumentStateMaps.class )}) ) )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.load( "dataRead" ),.MODULE$.load( "inputMorsel" ), .
        MODULE$.load( "argumentStateMaps" )}))),org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7()),
        new MethodDeclaration( "workIdentity",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( WorkIdentity.class )),
        (Seq) scala.collection.Seq..MODULE$.empty(), .
        MODULE$.getStatic( workIdentityField.name(), scala.reflect.ManifestFactory..MODULE$.classType( WorkIdentity.class )),
        org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7()),
        new MethodDeclaration( "createState",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( OperatorState.class )),
        (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new Parameter[]{.MODULE$.param( "argumentStateCreator", scala.reflect.ManifestFactory..MODULE$.classType(
                    ArgumentStateMapCreator.class )),.MODULE$.param( "stateFactory", scala.reflect.ManifestFactory..MODULE$.classType( StateFactory.class )), .
        MODULE$.param( "queryContext", scala.reflect.ManifestFactory..MODULE$.classType( QueryContext.class )), .
        MODULE$.param( "state", scala.reflect.ManifestFactory..MODULE$.classType( QueryState.class )), .
        MODULE$.param( "resources", scala.reflect.ManifestFactory..MODULE$.classType( QueryResources.class ))}))), .
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{createState,.MODULE$.self()}))),
        org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7())})));
        Function0 x$13 = () -> {
            return (Seq) argumentStateFields.$colon$plus( workIdentityField, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
        }; return new ClassDeclaration( packageName, className, x$8, x$9, x$10, x$11, x$13, x$12 );
    }
}
