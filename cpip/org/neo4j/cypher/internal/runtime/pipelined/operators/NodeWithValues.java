package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.values.storable.Value;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class NodeWithValues
{
    private final long nodeId;
    private final Value[] values;

    public NodeWithValues( final long nodeId, final Value[] values )
    {
        this.nodeId = nodeId;
        this.values = values;
    }

    public long nodeId()
    {
        return this.nodeId;
    }

    public Value[] values()
    {
        return this.values;
    }
}
