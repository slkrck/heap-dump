package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.scheduling.HasWorkIdentity;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface OutputOperatorState extends HasWorkIdentity
{
    static void $init$( final OutputOperatorState $this )
    {
    }

    boolean trackTime();

    default PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
            final QueryResources resources, final QueryProfiler queryProfiler )
    {
        OperatorProfileEvent operatorExecutionEvent = queryProfiler.executeOperator( this.workIdentity().workId(), this.trackTime() );
        resources.setKernelTracer( operatorExecutionEvent );

        PreparedOutput var10000;
        try
        {
            var10000 = this.prepareOutput( output, context, state, resources, operatorExecutionEvent );
        }
        finally
        {
            resources.setKernelTracer( (KernelReadTracer) null );
            if ( operatorExecutionEvent != null )
            {
                operatorExecutionEvent.close();
            }
        }

        return var10000;
    }

    PreparedOutput prepareOutput( final MorselExecutionContext outputMorsel, final QueryContext context, final QueryState state, final QueryResources resources,
            final OperatorProfileEvent operatorExecutionEvent );

    default boolean canContinueOutput()
    {
        return false;
    }
}
