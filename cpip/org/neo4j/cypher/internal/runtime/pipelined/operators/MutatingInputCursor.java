package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.InputCursor;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class MutatingInputCursor
{
    private final InputDataStream input;
    private boolean _canContinue;
    private InputCursor cursor;

    public MutatingInputCursor( final InputDataStream input )
    {
        this.input = input;
        this._canContinue = true;
    }

    private boolean _canContinue()
    {
        return this._canContinue;
    }

    private void _canContinue_$eq( final boolean x$1 )
    {
        this._canContinue = x$1;
    }

    private InputCursor cursor()
    {
        return this.cursor;
    }

    private void cursor_$eq( final InputCursor x$1 )
    {
        this.cursor = x$1;
    }

    public boolean canContinue()
    {
        return this._canContinue();
    }

    public AnyValue value( final int offset )
    {
        return this.cursor().value( offset );
    }

    public boolean nextInput()
    {
        while ( true )
        {
            if ( this.cursor() == null )
            {
                this.cursor_$eq( this.input.nextInputBatch() );
                if ( this.cursor() == null )
                {
                    this._canContinue_$eq( false );
                    return false;
                }
            }

            if ( this.cursor().next() )
            {
                return true;
            }

            this.cursor().close();
            this.cursor_$eq( (InputCursor) null );
        }
    }

    public void close()
    {
        if ( this.cursor() != null )
        {
            this.cursor().close();
            this.cursor_$eq( (InputCursor) null );
        }
    }
}
