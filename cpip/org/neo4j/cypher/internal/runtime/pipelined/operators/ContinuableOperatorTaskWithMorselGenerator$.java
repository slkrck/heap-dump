package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.codegen.CodeGenerator;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Tuple2;
import scala.collection.Seq;

public final class ContinuableOperatorTaskWithMorselGenerator$
{
    public static ContinuableOperatorTaskWithMorselGenerator$ MODULE$;

    static
    {
        new ContinuableOperatorTaskWithMorselGenerator$();
    }

    private final String PACKAGE_NAME;
    private final AtomicLong COUNTER;

    private ContinuableOperatorTaskWithMorselGenerator$()
    {
        MODULE$ = this;
        this.PACKAGE_NAME = "org.neo4j.codegen";
        this.COUNTER = new AtomicLong( 0L );
    }

    private String PACKAGE_NAME()
    {
        return this.PACKAGE_NAME;
    }

    private AtomicLong COUNTER()
    {
        return this.COUNTER;
    }

    public CompiledStreamingOperator compileOperator( final ContinuableOperatorTaskWithMorselTemplate template, final WorkIdentity workIdentity,
            final Seq<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates,
            final CodeGenerationMode codeGenerationMode )
    {
        StaticField staticWorkIdentity = .MODULE$.staticConstant( OperatorCodeGenHelperTemplates$.MODULE$.WORK_IDENTITY_STATIC_FIELD_NAME(), workIdentity,
            scala.reflect.ManifestFactory..MODULE$.classType( WorkIdentity.class ));
        long operatorId = this.COUNTER().getAndIncrement();
        CodeGenerator generator = org.neo4j.codegen.api.CodeGeneration..MODULE$.createGenerator( codeGenerationMode );
        Class taskClazz = org.neo4j.codegen.api.CodeGeneration..MODULE$.compileClass(
            template.genClassDeclaration( this.PACKAGE_NAME(), (new StringBuilder( 12 )).append( "OperatorTask" ).append( operatorId ).toString(),
                    (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new StaticField[]{staticWorkIdentity}) ) )),
        generator);
        Class operatorClazz = org.neo4j.codegen.api.CodeGeneration..MODULE$.compileClass(
            CompiledStreamingOperator$.MODULE$.getClassDeclaration( this.PACKAGE_NAME(),
                    (new StringBuilder( 8 )).append( "Operator" ).append( operatorId ).toString(), taskClazz, staticWorkIdentity, argumentStates ), generator );
        return (CompiledStreamingOperator) operatorClazz.getDeclaredConstructor().newInstance();
    }
}
