package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CartesianProductOperator implements Operator, OperatorState
{
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$CartesianProductOperator$$argumentSize;
    private final WorkIdentity workIdentity;
    private final int lhsArgumentStateMapId;
    private final int rhsArgumentStateMapId;

    public CartesianProductOperator( final WorkIdentity workIdentity, final int lhsArgumentStateMapId, final int rhsArgumentStateMapId,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.lhsArgumentStateMapId = lhsArgumentStateMapId;
        this.rhsArgumentStateMapId = rhsArgumentStateMapId;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$CartesianProductOperator$$argumentSize = argumentSize;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        argumentStateCreator.createArgumentStateMap( this.lhsArgumentStateMapId, new CartesianProductOperator$LHSMorsel$Factory( stateFactory ) );
        argumentStateCreator.createArgumentStateMap( this.rhsArgumentStateMapId, new ArgumentStateBuffer.Factory( stateFactory ) );
        return this;
    }

    public IndexedSeq<ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,CartesianProductOperator.LHSMorsel>> nextTasks( final QueryContext context,
            final QueryState state, final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        Buffers.AccumulatorAndMorsel accAndMorsel = operatorInput.takeAccumulatorAndMorsel();
        return accAndMorsel != null ? scala.Predef..MODULE$.wrapRefArray( (Object[]) ((Object[]) (new CartesianProductOperator.OTask[]{
            new CartesianProductOperator.OTask( this, (CartesianProductOperator.LHSMorsel) accAndMorsel.acc(), accAndMorsel.morsel() )})) ) :null;
    }

    public static class LHSMorsel implements ArgumentStateMap.MorselAccumulator<MorselExecutionContext>
    {
        private final long argumentRowId;
        private final MorselExecutionContext lhsMorsel;
        private final long[] argumentRowIdsForReducers;

        public LHSMorsel( final long argumentRowId, final MorselExecutionContext lhsMorsel, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.lhsMorsel = lhsMorsel;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public MorselExecutionContext lhsMorsel()
        {
            return this.lhsMorsel;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        public void update( final MorselExecutionContext morsel )
        {
            throw new IllegalStateException( "LHSMorsel is complete on construction, and cannot be further updated." );
        }

        public String toString()
        {
            return (new StringBuilder( 25 )).append( "LHSMorsel(argumentRowId=" ).append( this.argumentRowId() ).append( ")" ).toString();
        }
    }

    public static class LHSMorsel$
    {
        public static CartesianProductOperator.LHSMorsel$ MODULE$;

        static
        {
            new CartesianProductOperator.LHSMorsel$();
        }

        public LHSMorsel$()
        {
            MODULE$ = this;
        }
    }

    public class OTask extends InputLoopTask
            implements ContinuableOperatorTaskWithMorselAndAccumulator<MorselExecutionContext,CartesianProductOperator.LHSMorsel>
    {
        private final CartesianProductOperator.LHSMorsel accumulator;
        private final MorselExecutionContext rhsRow;
        private final MorselExecutionContext inputMorsel;
        private final SlotConfiguration lhsSlots;

        public OTask( final CartesianProductOperator $outer, final CartesianProductOperator.LHSMorsel accumulator, final MorselExecutionContext rhsRow )
        {
            this.accumulator = accumulator;
            this.rhsRow = rhsRow;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                ContinuableOperatorTaskWithAccumulator.$init$( this );
                ContinuableOperatorTaskWithMorselAndAccumulator.$init$( this );
                this.inputMorsel = accumulator.lhsMorsel().shallowCopy();
                this.lhsSlots = this.inputMorsel().slots();
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorselAndAccumulator.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorselAndAccumulator.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorselAndAccumulator.producingWorkUnitEvent$( this );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            ContinuableOperatorTaskWithAccumulator.setExecutionEvent$( this, event );
        }

        public void closeCursors( final QueryResources resources )
        {
            ContinuableOperatorTaskWithAccumulator.closeCursors$( this, resources );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithAccumulator.estimatedHeapUsage$( this );
        }

        public CartesianProductOperator.LHSMorsel accumulator()
        {
            return this.accumulator;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$CartesianProductOperator$OTask$$$outer().workIdentity();
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        private SlotConfiguration lhsSlots()
        {
            return this.lhsSlots;
        }

        public String toString()
        {
            return "CartesianProductTask";
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            this.rhsRow.resetToBeforeFirstRow();
            return true;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.rhsRow.hasNextRow() )
            {
                this.rhsRow.moveToNextRow();
                this.inputMorsel().copyTo( outputRow, this.inputMorsel().copyTo$default$2(), this.inputMorsel().copyTo$default$3(),
                        this.inputMorsel().copyTo$default$4(), this.inputMorsel().copyTo$default$5() );
                this.rhsRow.copyTo( outputRow,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$CartesianProductOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$CartesianProductOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$CartesianProductOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$CartesianProductOperator$$argumentSize.nReferences(),
                        this.lhsSlots().numberOfLongs(), this.lhsSlots().numberOfReferences() );
                outputRow.moveToNextRow();
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
        }
    }
}
