package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import scala.Function0;
import scala.Function1;
import scala.Option;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class FilterOperatorTemplate implements OperatorTaskTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final Function0<IntermediateExpression> generatePredicate;
    private final OperatorExpressionCompiler codeGen;
    private IntermediateExpression predicate;

    public FilterOperatorTemplate( final OperatorTaskTemplate inner, final int id, final Function0<IntermediateExpression> generatePredicate,
            final OperatorExpressionCompiler codeGen )
    {
        this.inner = inner;
        this.id = id;
        this.generatePredicate = generatePredicate;
        this.codeGen = codeGen;
        OperatorTaskTemplate.$init$( this );
    }

    public String scopeId()
    {
        return OperatorTaskTemplate.scopeId$( this );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return OperatorTaskTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return OperatorTaskTemplate.genOperateEnter$( this );
    }

    public IntermediateRepresentation genOperateExit()
    {
        return OperatorTaskTemplate.genOperateExit$( this );
    }

    public IntermediateRepresentation genProduce()
    {
        return OperatorTaskTemplate.genProduce$( this );
    }

    public IntermediateRepresentation genCreateState()
    {
        return OperatorTaskTemplate.genCreateState$( this );
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return OperatorTaskTemplate.genOutputBuffer$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    public IntermediateRepresentation genInit()
    {
        return this.inner().genInit();
    }

    private IntermediateExpression predicate()
    {
        return this.predicate;
    }

    private void predicate_$eq( final IntermediateExpression x$1 )
    {
        this.predicate = x$1;
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.predicate()}) ));
    }

    public IntermediateRepresentation genOperate()
    {
        if ( this.predicate() != null )
        {
            throw new IllegalStateException( "genOperate must be called first!!" );
        }
        else
        {
            this.predicate_$eq( (IntermediateExpression) this.generatePredicate.apply() );
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.predicate(), ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.profileRow( this.id() ),
                            this.inner().genOperateWithExpressions()}) )));
        }
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.empty();
    }

    public Seq<Field> genFields()
    {
        return (Seq).MODULE$.empty();
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue();
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return this.inner().genCloseCursors();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return this.inner().genSetExecutionEvent( event );
    }
}
