package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.exceptions.InternalException;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import scala.Function1;
import scala.Option;
import scala.Some;
import scala.collection.GenSeq;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface OperatorTaskTemplate
{
    static OperatorTaskTemplate empty( final int withId )
    {
        return OperatorTaskTemplate$.MODULE$.empty( var0 );
    }

    static void $init$( final OperatorTaskTemplate $this )
    {
    }

    OperatorTaskTemplate inner();

    int id();

    default String scopeId()
    {
        return (new StringBuilder( 8 )).append( "operator" ).append( this.id() ).toString();
    }

    OperatorExpressionCompiler codeGen();

    default <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        OperatorTaskTemplate var3 = this.inner();
        List var2;
        if ( var3 == null )
        {
            Object var4 = f.apply( this );
            var2 = .MODULE$.$colon$colon( var4 );
        }
        else
        {
            Object var5 = f.apply( this );
            var2 = var3.map( f ).$colon$colon( var5 );
        }

        return var2;
    }

    default <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        OperatorTaskTemplate var3 = this.inner();
        Seq var2;
        if ( var3 == null )
        {
            var2 = (Seq) ((TraversableLike) f.apply( this )).$plus$plus( scala.collection.Seq..MODULE$.empty(), scala.collection.Seq..MODULE$.canBuildFrom());
        }
        else
        {
            var2 = (Seq) ((TraversableLike) f.apply( this )).$plus$plus( var3.flatMap( f ), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        return var2;
    }

    default ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        throw new InternalException( "Illegal start operator template" );
    }

    IntermediateRepresentation genInit();

    default InstanceField executionEventField()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 23 )).append( "operatorExecutionEvent_" ).append( this.id() ).toString(),
                scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class ));
    }

    default Option<Field> genProfileEventField()
    {
        return new Some( this.executionEventField() );
    }

    default IntermediateRepresentation genInitializeProfileEvents()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.executionEventField(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.QUERY_PROFILER(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "executeOperator", scala.reflect.ManifestFactory..MODULE$.classType(
                    QueryProfiler.class ), scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class ), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( this.id() ) ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
            BoxesRunTime.boxToBoolean( false ) )})))),
        this.genSetExecutionEvent( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )),
        this.inner().genInitializeProfileEvents()})));
    }

    IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event );

    default IntermediateRepresentation genCloseProfileEvents()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.genSetExecutionEvent( OperatorCodeGenHelperTemplates$.MODULE$.NO_OPERATOR_PROFILE_EVENT() ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect( OperatorCodeGenHelperTemplates$.MODULE$.QUERY_RESOURCES(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "setKernelTracer", scala.reflect.ManifestFactory..MODULE$.classType(
                    QueryResources.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
        MODULE$.classType( KernelReadTracer.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.NO_KERNEL_TRACER()}) )),
        OperatorCodeGenHelperTemplates$.MODULE$.closeEvent( this.id() ), this.inner().genCloseProfileEvents()})));
    }

    default IntermediateRepresentation genOperateWithExpressions()
    {
        this.codeGen().beginScope( this.scopeId() );
        IntermediateRepresentation body = this.genOperate();
        OperatorExpressionCompiler qual$1 = this.codeGen();
        boolean x$14 = qual$1.endScope$default$1();
        OperatorExpressionCompiler.ScopeLocalsState localState = qual$1.endScope( x$14 );
        Seq declarations = localState.declarations();
        Seq assignments = localState.assignments();
        Seq expressionCursors = (Seq) ((SeqLike) this.genExpressions().flatMap( ( x$3 ) -> {
            return x$3.variables();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).intersect( (GenSeq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR(), ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR(),
                    ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR()}) )));
        Seq setTracerCalls = expressionCursors.nonEmpty() ? (Seq) expressionCursors.map( ( cursor ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                    cursor ), OperatorCodeGenHelperTemplates$.MODULE$.SET_TRACER(), scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.executionEventField() )})));
        }, scala.collection.Seq..MODULE$.canBuildFrom()) :(Seq) scala.collection.Seq..MODULE$.empty();
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
            (Seq) ((SeqLike) ((TraversableLike) setTracerCalls.$plus$plus( declarations, scala.collection.Seq..MODULE$.canBuildFrom())).$plus$plus( assignments,
                    scala.collection.Seq..MODULE$.canBuildFrom() )).$colon$plus( body, scala.collection.Seq..MODULE$.canBuildFrom()));
    }

    default IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return (IntermediateRepresentation) this.inner().genCanContinue().map( ( innerCanContinue ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not( innerCanContinue ), op);
        } ).getOrElse( () -> {
            return op;
        } );
    }

    IntermediateRepresentation genOperate();

    default IntermediateRepresentation genOperateEnter()
    {
        return this.inner().genOperateEnter();
    }

    default IntermediateRepresentation genOperateExit()
    {
        return this.inner().genOperateExit();
    }

    default IntermediateRepresentation genProduce()
    {
        return this.inner().genProduce();
    }

    default IntermediateRepresentation genCreateState()
    {
        return this.inner().genCreateState();
    }

    default Option<IntermediateRepresentation> genOutputBuffer()
    {
        return this.inner().genOutputBuffer();
    }

    Seq<IntermediateExpression> genExpressions();

    Seq<Field> genFields();

    Seq<LocalVariable> genLocalVariables();

    Option<IntermediateRepresentation> genCanContinue();

    IntermediateRepresentation genCloseCursors();
}
