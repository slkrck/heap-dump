package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Comparator;

import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.runtime.pipelined.execution.Morsel;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext$;
import org.neo4j.cypher.internal.runtime.slotted.ColumnOrder;
import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;

public final class MorselSorting$
{
    public static MorselSorting$ MODULE$;

    static
    {
        new MorselSorting$();
    }

    private MorselSorting$()
    {
        MODULE$ = this;
    }

    public Comparator<MorselExecutionContext> createComparator( final Seq<ColumnOrder> orderBy )
    {
        return (Comparator) ((TraversableOnce) orderBy.map( ( order ) -> {
            return MODULE$.createMorselComparator( order );
        },.MODULE$.canBuildFrom())).reduce( ( a, b ) -> {
        return a.thenComparing( b );
    } );
    }

    public Comparator<Integer> compareMorselIndexesByColumnOrder( final MorselExecutionContext row, final ColumnOrder order )
    {
        boolean var4 = false;
        LongSlot var5 = null;
        Slot var6 = order.slot();
        Comparator var3;
        if ( var6 instanceof LongSlot )
        {
            var4 = true;
            var5 = (LongSlot) var6;
            int offset = var5.offset();
            boolean var8 = var5.nullable();
            if ( var8 )
            {
                var3 = ( idx1, idx2 ) -> {
                    row.setCurrentRow( scala.Predef..MODULE$.Integer2int( idx1 ));
                    long aVal = row.getLongAt( offset );
                    row.setCurrentRow( scala.Predef..MODULE$.Integer2int( idx2 ));
                    long bVal = row.getLongAt( offset );
                    return order.compareNullableLongs( aVal, bVal );
                }; return var3;
            }
        }

        if ( var4 )
        {
            int offset = var5.offset();
            boolean var10 = var5.nullable();
            if ( !var10 )
            {
                var3 = ( idx1, idx2 ) -> {
                    row.setCurrentRow( scala.Predef..MODULE$.Integer2int( idx1 ));
                    long aVal = row.getLongAt( offset );
                    row.setCurrentRow( scala.Predef..MODULE$.Integer2int( idx2 ));
                    long bVal = row.getLongAt( offset );
                    return order.compareLongs( aVal, bVal );
                }; return var3;
            }
        }

        if ( !(var6 instanceof RefSlot) )
        {
            throw new MatchError( var6 );
        }
        else
        {
            RefSlot var11 = (RefSlot) var6;
            int offset = var11.offset();
            var3 = ( idx1, idx2 ) -> {
                row.setCurrentRow( scala.Predef..MODULE$.Integer2int( idx1 ));
                AnyValue aVal = row.getRefAt( offset );
                row.setCurrentRow( scala.Predef..MODULE$.Integer2int( idx2 ));
                AnyValue bVal = row.getRefAt( offset );
                return order.compareValues( aVal, bVal );
            }; return var3;
        }
    }

    public Integer[] createMorselIndexesArray( final MorselExecutionContext row )
    {
        int currentRow = row.getCurrentRow();
        int rows = row.getValidRows();
        Integer[] indexes = new Integer[rows];
        int i = 0;
        row.resetToFirstRow();

        while ( i < rows )
        {
            scala.Predef..MODULE$. assert (row.isValidRow());
            indexes[i] = scala.Predef..MODULE$.int2Integer( row.getCurrentRow() );
            row.moveToNextRow();
            ++i;
        }

        scala.Predef..MODULE$. assert (!row.isValidRow());
        row.setCurrentRow( currentRow );
        return indexes;
    }

    public void createSortedMorselData( final MorselExecutionContext inputRow, final Integer[] outputToInputIndexes )
    {
        int numInputRows = inputRow.getValidRows();
        Morsel tempMorsel = new Morsel( new long[numInputRows * inputRow.getLongsPerRow()], new AnyValue[numInputRows * inputRow.getRefsPerRow()] );
        MorselExecutionContext outputRow = MorselExecutionContext$.MODULE$.apply( tempMorsel, inputRow.slots(), numInputRows );

        while ( outputRow.isValidRow() )
        {
            Integer fromIndex = outputToInputIndexes[outputRow.getCurrentRow()];
            inputRow.setCurrentRow( scala.Predef..MODULE$.Integer2int( fromIndex ));
            outputRow.copyFrom( inputRow );
            outputRow.moveToNextRow();
        }

        inputRow.compactRowsFrom( outputRow );
    }

    public Comparator<MorselExecutionContext> createMorselComparator( final ColumnOrder order )
    {
        boolean var3 = false;
        LongSlot var4 = null;
        Slot var5 = order.slot();
        Comparator var2;
        if ( var5 instanceof LongSlot )
        {
            var3 = true;
            var4 = (LongSlot) var5;
            int offset = var4.offset();
            boolean var7 = var4.nullable();
            if ( var7 )
            {
                var2 = ( m1, m2 ) -> {
                    long aVal = m1.getLongAt( offset );
                    long bVal = m2.getLongAt( offset );
                    return order.compareNullableLongs( aVal, bVal );
                };
                return var2;
            }
        }

        if ( var3 )
        {
            int offset = var4.offset();
            boolean var9 = var4.nullable();
            if ( !var9 )
            {
                var2 = ( m1, m2 ) -> {
                    long aVal = m1.getLongAt( offset );
                    long bVal = m2.getLongAt( offset );
                    return order.compareLongs( aVal, bVal );
                };
                return var2;
            }
        }

        if ( !(var5 instanceof RefSlot) )
        {
            throw new MatchError( var5 );
        }
        else
        {
            RefSlot var10 = (RefSlot) var5;
            int offset = var10.offset();
            var2 = ( m1, m2 ) -> {
                AnyValue aVal = m1.getRefAt( offset );
                AnyValue bVal = m2.getRefAt( offset );
                return order.compareValues( aVal, bVal );
            };
            return var2;
        }
    }
}
