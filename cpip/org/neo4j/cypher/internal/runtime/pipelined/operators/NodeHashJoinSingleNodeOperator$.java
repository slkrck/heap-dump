package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class NodeHashJoinSingleNodeOperator$
{
    public static NodeHashJoinSingleNodeOperator$ MODULE$;

    static
    {
        new NodeHashJoinSingleNodeOperator$();
    }

    private NodeHashJoinSingleNodeOperator$()
    {
        MODULE$ = this;
    }
}
