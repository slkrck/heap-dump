package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Comparator;
import java.util.PriorityQueue;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.ColumnOrder;
import scala.Predef.;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SortMergeOperator implements Operator, ReduceOperatorState<MorselExecutionContext,ArgumentStateBuffer>
{
    private final int argumentStateMapId;
    private final WorkIdentity workIdentity;
    private final Comparator<MorselExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$SortMergeOperator$$comparator;

    public SortMergeOperator( final int argumentStateMapId, final WorkIdentity workIdentity, final Seq<ColumnOrder> orderBy, final int argumentSlotOffset )
    {
        this.argumentStateMapId = argumentStateMapId;
        this.workIdentity = workIdentity;
        ReduceOperatorState.$init$( this );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortMergeOperator$$comparator = MorselSorting$.MODULE$.createComparator( orderBy );
    }

    public final IndexedSeq<ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,ArgumentStateBuffer>> nextTasks( final QueryContext context,
            final QueryState state, final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return ReduceOperatorState.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public int argumentStateMapId()
    {
        return this.argumentStateMapId;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public String toString()
    {
        return "SortMerge";
    }

    public Comparator<MorselExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$SortMergeOperator$$comparator()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortMergeOperator$$comparator;
    }

    public ReduceOperatorState<MorselExecutionContext,ArgumentStateBuffer> createState( final ArgumentStateMapCreator argumentStateCreator,
            final StateFactory stateFactory, final QueryContext queryContext, final QueryState state, final QueryResources resources )
    {
        argumentStateCreator.createArgumentStateMap( this.argumentStateMapId(), new ArgumentStateBuffer.Factory( stateFactory ) );
        return this;
    }

    public IndexedSeq<ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,ArgumentStateBuffer>> nextTasks( final QueryContext queryContext,
            final QueryState state, final ArgumentStateBuffer input, final QueryResources resources )
    {
        return .MODULE$.wrapRefArray( (Object[]) ((Object[]) (new SortMergeOperator.OTask[]{new SortMergeOperator.OTask( this, input )})) );
    }

    public class OTask implements ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,ArgumentStateBuffer>
    {
        private final ArgumentStateBuffer accumulator;
        private PriorityQueue<MorselExecutionContext> sortedInputPerArgument;

        public OTask( final SortMergeOperator $outer, final ArgumentStateBuffer accumulator )
        {
            this.accumulator = accumulator;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                ContinuableOperatorTask.$init$( this );
                ContinuableOperatorTaskWithAccumulator.$init$( this );
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithAccumulator.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithAccumulator.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithAccumulator.producingWorkUnitEvent$( this );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            ContinuableOperatorTaskWithAccumulator.setExecutionEvent$( this, event );
        }

        public void closeCursors( final QueryResources resources )
        {
            ContinuableOperatorTaskWithAccumulator.closeCursors$( this, resources );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithAccumulator.estimatedHeapUsage$( this );
        }

        public void close( final OperatorCloser operatorCloser, final QueryResources resources )
        {
            ContinuableOperatorTask.close$( this, operatorCloser, resources );
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public ArgumentStateBuffer accumulator()
        {
            return this.accumulator;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortMergeOperator$OTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "SortMergeTask";
        }

        public PriorityQueue<MorselExecutionContext> sortedInputPerArgument()
        {
            return this.sortedInputPerArgument;
        }

        public void sortedInputPerArgument_$eq( final PriorityQueue<MorselExecutionContext> x$1 )
        {
            this.sortedInputPerArgument = x$1;
        }

        public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            if ( this.sortedInputPerArgument() == null )
            {
                this.sortedInputPerArgument_$eq( new PriorityQueue(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortMergeOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$SortMergeOperator$$comparator() ) );
                this.accumulator().foreach( ( morsel ) -> {
                    $anonfun$operate$1( this, morsel );
                    return BoxedUnit.UNIT;
                } );
            }

            while ( outputRow.isValidRow() && this.canContinue() )
            {
                MorselExecutionContext nextRow = (MorselExecutionContext) this.sortedInputPerArgument().poll();
                outputRow.copyFrom( nextRow );
                nextRow.moveToNextRow();
                outputRow.moveToNextRow();
                if ( nextRow.isValidRow() )
                {
                    BoxesRunTime.boxToBoolean( this.sortedInputPerArgument().add( nextRow ) );
                }
                else
                {
                    BoxedUnit var10000 = BoxedUnit.UNIT;
                }
            }

            outputRow.finishedWriting();
        }

        public boolean canContinue()
        {
            return !this.sortedInputPerArgument().isEmpty();
        }
    }
}
