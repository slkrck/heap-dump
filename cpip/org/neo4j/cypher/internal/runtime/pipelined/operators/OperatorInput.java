package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface OperatorInput
{
    MorselParallelizer takeMorsel();

    <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> ACC takeAccumulator();

    <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> Buffers.AccumulatorAndMorsel<DATA,ACC> takeAccumulatorAndMorsel();

    <DATA> DATA takeData();
}
