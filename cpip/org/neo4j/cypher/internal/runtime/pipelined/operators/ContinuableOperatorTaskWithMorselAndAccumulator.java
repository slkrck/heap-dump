package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ContinuableOperatorTaskWithMorselAndAccumulator<DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>>
        extends ContinuableOperatorTaskWithMorsel, ContinuableOperatorTaskWithAccumulator<DATA,ACC>
{
    static void $init$( final ContinuableOperatorTaskWithMorselAndAccumulator $this )
    {
    }

    default void closeInput( final OperatorCloser operatorCloser )
    {
        operatorCloser.closeMorselAndAccumulatorTask( this.inputMorsel(), this.accumulator() );
    }

    default boolean filterCancelledArguments( final OperatorCloser operatorCloser )
    {
        return operatorCloser.filterCancelledArguments( this.inputMorsel(), this.accumulator() );
    }

    default WorkUnitEvent producingWorkUnitEvent()
    {
        return this.inputMorsel().producingWorkUnitEvent();
    }
}
