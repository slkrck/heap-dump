package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class EMIT$ implements ExpandStatus, Product, Serializable
{
    public static EMIT$ MODULE$;

    static
    {
        new EMIT$();
    }

    private EMIT$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "EMIT";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EMIT$;
    }

    public int hashCode()
    {
        return 2131923;
    }

    public String toString()
    {
        return "EMIT";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
