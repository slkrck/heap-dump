package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.AssignToLocalVariable;
import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class DelegateOperatorTaskTemplate implements OperatorTaskTemplate
{
    private final OperatorExpressionCompiler codeGen;
    private final int id;
    private boolean shouldWriteToContext;
    private boolean shouldCheckDemand;
    private boolean shouldCheckOutputCounter;

    public DelegateOperatorTaskTemplate( final boolean shouldWriteToContext, final boolean shouldCheckDemand, final boolean shouldCheckOutputCounter,
            final OperatorExpressionCompiler codeGen )
    {
        this.shouldWriteToContext = shouldWriteToContext;
        this.shouldCheckDemand = shouldCheckDemand;
        this.shouldCheckOutputCounter = shouldCheckOutputCounter;
        this.codeGen = codeGen;
        super();
        OperatorTaskTemplate.$init$( this );
        this.id = org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID();
    }

    public static boolean $lessinit$greater$default$3()
    {
        return DelegateOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$3();
    }

    public static boolean $lessinit$greater$default$2()
    {
        return DelegateOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$2();
    }

    public static boolean $lessinit$greater$default$1()
    {
        return DelegateOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$1();
    }

    public String scopeId()
    {
        return OperatorTaskTemplate.scopeId$( this );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return OperatorTaskTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public boolean shouldWriteToContext()
    {
        return this.shouldWriteToContext;
    }

    public void shouldWriteToContext_$eq( final boolean x$1 )
    {
        this.shouldWriteToContext = x$1;
    }

    public boolean shouldCheckDemand()
    {
        return this.shouldCheckDemand;
    }

    public void shouldCheckDemand_$eq( final boolean x$1 )
    {
        this.shouldCheckDemand = x$1;
    }

    public boolean shouldCheckOutputCounter()
    {
        return this.shouldCheckOutputCounter;
    }

    public void shouldCheckOutputCounter_$eq( final boolean x$1 )
    {
        this.shouldCheckOutputCounter = x$1;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    public void reset()
    {
        this.shouldWriteToContext_$eq( true );
        this.shouldCheckDemand_$eq( false );
        this.shouldCheckOutputCounter_$eq( false );
    }

    public OperatorTaskTemplate inner()
    {
        return null;
    }

    public int id()
    {
        return this.id;
    }

    public IntermediateRepresentation genInit()
    {
        return .MODULE$.noop();
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return .MODULE$.noop();
    }

    public Option<Field> genProfileEventField()
    {
        return scala.None..MODULE$;
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return .MODULE$.noop();
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return .MODULE$.noop();
    }

    public IntermediateRepresentation genOperate()
    {
        ArrayBuffer ops = new ArrayBuffer();
        BoxedUnit var10000;
        if ( this.shouldWriteToContext() )
        {
            ops.$plus$eq(.MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.codeGen().writeLocalsToSlots(),
                    OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW_MOVE_TO_NEXT()}) )));
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        if ( this.shouldCheckOutputCounter() )
        {
            ops.$plus$eq( OperatorCodeGenHelperTemplates$.MODULE$.UPDATE_OUTPUT_COUNTER() );
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        return ops.nonEmpty() ? .MODULE$.block( ops ) : .MODULE$.noop();
    }

    public IntermediateRepresentation resetCachedPropertyVariables()
    {
        return .MODULE$.block( (Seq) this.codeGen().getAllLocalsForCachedProperties().map( ( x0$4 ) -> {
        if ( x0$4 != null )
        {
            String name = (String) x0$4._2();
            AssignToLocalVariable var1 = .MODULE$.assign( name,.MODULE$.constant( (Object) null ));
            return var1;
        }
        else
        {
            throw new MatchError( x0$4 );
        }
    }, scala.collection.Seq..MODULE$.canBuildFrom() ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public IntermediateRepresentation predicate()
    {
        ArrayBuffer conditions = new ArrayBuffer();
        BoxedUnit var10000;
        if ( this.shouldWriteToContext() )
        {
            conditions.$plus$eq( OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW_IS_VALID() );
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        if ( this.shouldCheckDemand() )
        {
            conditions.$plus$eq( OperatorCodeGenHelperTemplates$.MODULE$.HAS_DEMAND() );
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        if ( this.shouldCheckOutputCounter() )
        {
            conditions.$plus$eq( OperatorCodeGenHelperTemplates$.MODULE$.HAS_REMAINING_OUTPUT() );
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        return .MODULE$.and( conditions );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return .MODULE$.noop();
    }

    public IntermediateRepresentation genOperateExit()
    {
        ArrayBuffer updates = new ArrayBuffer();
        BoxedUnit var10000;
        if ( this.shouldWriteToContext() )
        {
            updates.$plus$eq( OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW_FINISHED_WRITING() );
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        if ( this.shouldCheckDemand() )
        {
            updates.$plus$eq( OperatorCodeGenHelperTemplates$.MODULE$.UPDATE_DEMAND() );
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        return updates.nonEmpty() ? .MODULE$.block( updates ) : .MODULE$.noop();
    }

    public Seq<Field> genFields()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return this.shouldCheckOutputCounter() ? (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_COUNTER()}) )) :
        (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return scala.None..MODULE$;
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return .MODULE$.block( scala.collection.immutable.Nil..MODULE$);
    }

    public IntermediateRepresentation genProduce()
    {
        return .MODULE$.noop();
    }

    public IntermediateRepresentation genCreateState()
    {
        return .MODULE$.noop();
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return scala.None..MODULE$;
    }
}
