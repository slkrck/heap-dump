package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.ValuePopulation;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Tuple2;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class ProduceResultOperator implements StreamingOperator, OutputOperator
{
    private final WorkIdentity workIdentity;
    private final Expression[] expressions;

    public ProduceResultOperator( final WorkIdentity workIdentity, final SlotConfiguration slots, final Seq<Tuple2<String,Expression>> columns )
    {
        this.workIdentity = workIdentity;
        StreamingOperator.$init$( this );
        this.expressions = (Expression[]) ((TraversableOnce) columns.map( ( x$1 ) -> {
            return (Expression) x$1._2();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ));
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    private Expression[] expressions()
    {
        return this.expressions;
    }

    public String toString()
    {
        return "ProduceResult";
    }

    public Option<BufferId> outputBuffer()
    {
        return .MODULE$;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext context, final QueryState state, final MorselParallelizer inputMorsel,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return scala.Predef..MODULE$.wrapRefArray(
            (Object[]) ((Object[]) (new ProduceResultOperator.InputOTask[]{new ProduceResultOperator.InputOTask( this, inputMorsel.nextCopy() )})) );
    }

    public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
    {
        return new ProduceResultOperator.OutputOOperatorState( this );
    }

    public void produceOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
            final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
    {
        int numberOfOutputedRows = this.produceOutput( output, context, state, resources );
        if ( operatorExecutionEvent != null )
        {
            operatorExecutionEvent.rows( numberOfOutputedRows );
        }
    }

    public int produceOutput( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources )
    {
        scala.runtime.Null.x$3 = null;
        AnyValue[] x$4 = state.params();
        ExpressionCursors x$5 = resources.expressionCursors();
        IndexReadSession[] x$6 = (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class ));
        AnyValue[] x$7 = resources.expressionVariables( state.nExpressionSlots() );
        QuerySubscriber x$8 = state.subscriber();
        org.neo4j.cypher.internal.runtime.NoMemoryTracker.x$9 = org.neo4j.cypher.internal.runtime.NoMemoryTracker..MODULE$;
        boolean x$10 = state.prepopulateResults();
        PipeDecorator x$11 = SlottedQueryState$.MODULE$.$lessinit$greater$default$9();
        Option x$12 = SlottedQueryState$.MODULE$.$lessinit$greater$default$10();
        SingleThreadedLRUCache x$13 = SlottedQueryState$.MODULE$.$lessinit$greater$default$11();
        boolean x$14 = SlottedQueryState$.MODULE$.$lessinit$greater$default$12();
        InputDataStream x$15 = SlottedQueryState$.MODULE$.$lessinit$greater$default$14();
        SlottedQueryState queryState =
                new SlottedQueryState( context, (ExternalCSVResource) null, x$4, x$5, x$6, x$7, x$8, x$9, x$11, x$12, x$13, x$14, x$10, x$15 );
        QuerySubscriber subscriber = state.subscriber();
        int served = 0;
        long demand = state.flowControl().getDemand();

        while ( output.isValidRow() && (long) served < demand )
        {
            subscriber.onRecord();

            for ( int i = 0; i < this.expressions().length; ++i )
            {
                AnyValue value = this.expressions()[i].apply( output, queryState );
                if ( state.prepopulateResults() )
                {
                    ValuePopulation.populate( value );
                }
                else
                {
                    BoxedUnit var10000 = BoxedUnit.UNIT;
                }

                subscriber.onField( value );
            }

            subscriber.onRecordCompleted();
            ++served;
            output.moveToNextRow();
        }

        state.flowControl().addServed( (long) served );
        return served;
    }

    public class InputOTask implements ContinuableOperatorTaskWithMorsel
    {
        private final MorselExecutionContext inputMorsel;

        public InputOTask( final ProduceResultOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                ContinuableOperatorTask.$init$( this );
                ContinuableOperatorTaskWithMorsel.$init$( this );
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorsel.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorsel.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorsel.producingWorkUnitEvent$( this );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithMorsel.estimatedHeapUsage$( this );
        }

        public void close( final OperatorCloser operatorCloser, final QueryResources resources )
        {
            ContinuableOperatorTask.close$( this, operatorCloser, resources );
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$ProduceResultOperator$InputOTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "ProduceResultInputTask";
        }

        public boolean canContinue()
        {
            return this.inputMorsel().isValidRow();
        }

        public void operateWithProfile( final MorselExecutionContext outputIgnore, final QueryContext context, final QueryState state,
                final QueryResources resources, final QueryProfiler queryProfiler )
        {
            OperatorProfileEvent operatorExecutionEvent = queryProfiler.executeOperator( new Id( this.workIdentity().workId() ) );

            try
            {
                this.org$neo4j$cypher$internal$runtime$pipelined$operators$ProduceResultOperator$InputOTask$$$outer().produceOutputWithProfile(
                        this.inputMorsel(), context, state, resources, operatorExecutionEvent );
            }
            finally
            {
                if ( operatorExecutionEvent != null )
                {
                    operatorExecutionEvent.close();
                }
            }
        }

        public void operate( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            throw new UnsupportedOperationException( "ProduceResults should be called via operateWithProfile" );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
        }

        public void closeCursors( final QueryResources resources )
        {
        }
    }

    public class OutputOOperatorState implements OutputOperatorState, PreparedOutput
    {
        private boolean _canContinue;

        public OutputOOperatorState( final ProduceResultOperator $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OutputOperatorState.$init$( this );
                this._canContinue = false;
            }
        }

        public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                final QueryResources resources, final QueryProfiler queryProfiler )
        {
            return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        private boolean _canContinue()
        {
            return this._canContinue;
        }

        private void _canContinue_$eq( final boolean x$1 )
        {
            this._canContinue = x$1;
        }

        public String toString()
        {
            return "ProduceResultOutputTask";
        }

        public boolean canContinueOutput()
        {
            return this._canContinue();
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$ProduceResultOperator$OutputOOperatorState$$$outer().workIdentity();
        }

        public PreparedOutput prepareOutput( final MorselExecutionContext outputMorsel, final QueryContext context, final QueryState state,
                final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
        {
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$ProduceResultOperator$OutputOOperatorState$$$outer().produceOutputWithProfile(
                    outputMorsel, context, state, resources, operatorExecutionEvent );
            this._canContinue_$eq( outputMorsel.isValidRow() );
            return this;
        }

        public void produce()
        {
        }

        public boolean trackTime()
        {
            return true;
        }
    }
}
