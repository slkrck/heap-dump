package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.values.AnyValue;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ManyDirectedRelationshipByIdsSeekTaskTemplate extends ManyRelationshipByIdsSeekTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int relationshipOffset;
    private final int fromOffset;
    private final int toOffset;
    private final SlotConfiguration.Size argumentSize;

    public ManyDirectedRelationshipByIdsSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int relationshipOffset, final int fromOffset, final int toOffset, final Expression relIdsExpr, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, relationshipOffset, fromOffset, toOffset, relIdsExpr, argumentSize, codeGen );
        this.innermost = innermost;
        this.relationshipOffset = relationshipOffset;
        this.fromOffset = fromOffset;
        this.toOffset = toOffset;
        this.argumentSize = argumentSize;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.cursor(), this.idCursor()}) ));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        String idVariable = super.codeGen().namer().nextVariableName();
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()), idVariable,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( RelationshipByIdSeekOperator$.MODULE$.asIdMethod(),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.idCursor() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "value", scala.reflect.ManifestFactory..MODULE$.classType(
                IteratorCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.greaterThanOrEqual(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( idVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToLong( 0L ) )),
        OperatorCodeGenHelperTemplates$.MODULE$.singleRelationship( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                idVariable ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.greaterThanOrEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
            idVariable ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( 0L ) )),
        OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.cursor() ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                    super.codeGen().setLongAt( this.relationshipOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( idVariable )),
            super.codeGen().setLongAt( this.fromOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "sourceNodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
            RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.codeGen().setLongAt( this.toOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "targetNodeReference", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( OperatorCodeGenHelperTemplates$.MODULE$.profileRow( super.id() ) )})))),
        this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.idCursor() ),
                scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ))))}))));
    }
}
