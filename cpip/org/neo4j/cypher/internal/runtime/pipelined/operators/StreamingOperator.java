package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.NonFatalCypherError.;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.SchedulingInputException;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import scala.Option;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface StreamingOperator extends Operator, OperatorState
{
    static void $init$( final StreamingOperator $this )
    {
    }

    default IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        MorselParallelizer input = operatorInput.takeMorsel();
        IndexedSeq var10000;
        if ( input != null )
        {
            try
            {
                var10000 = this.nextTasks( context, state, input, parallelism, resources, argumentStateMaps );
            }
            catch ( Throwable var13 )
            {
                Option var11 = .MODULE$.unapply( var13 );
                if ( !var11.isEmpty() )
                {
                    Throwable t = (Throwable) var11.get();
                    throw new SchedulingInputException( input, t );
                }

                throw var13;
            }
        }
        else
        {
            var10000 = null;
        }

        return var10000;
    }

    IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext context, final QueryState state, final MorselParallelizer inputMorsel,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps );

    default OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return this;
    }
}
