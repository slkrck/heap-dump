package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class SerialTopLevelLimitOperatorTaskTemplate$
{
    public static SerialTopLevelLimitOperatorTaskTemplate$ MODULE$;

    static
    {
        new SerialTopLevelLimitOperatorTaskTemplate$();
    }

    private SerialTopLevelLimitOperatorTaskTemplate$()
    {
        MODULE$ = this;
    }
}
