package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface VarExpandPredicate<ENTITY>
{
    static VarExpandPredicate<RelationshipSelectionCursor> NO_RELATIONSHIP_PREDICATE()
    {
        return VarExpandPredicate$.MODULE$.NO_RELATIONSHIP_PREDICATE();
    }

    static VarExpandPredicate<Object> NO_NODE_PREDICATE()
    {
        return VarExpandPredicate$.MODULE$.NO_NODE_PREDICATE();
    }

    boolean isTrue( final ENTITY entity );
}
