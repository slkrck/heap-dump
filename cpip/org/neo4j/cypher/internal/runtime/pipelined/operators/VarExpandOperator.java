package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.physicalplanning.VariablePredicates$;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class VarExpandOperator implements StreamingOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$relOffset;
    public final SemanticDirection org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$dir;
    public final RelationshipTypes org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$types;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$minLength;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$maxLength;
    public final boolean org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$shouldExpandAll;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempNodeOffset;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempRelationshipOffset;
    public final Expression org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$nodePredicate;
    public final Expression org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$relationshipPredicate;
    private final WorkIdentity workIdentity;
    private final ToLongFunction<ExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getFromNodeFunction;
    private final ToLongFunction<ExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getToNodeFunction;
    private final int org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$toOffset;
    private final boolean org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$projectBackwards;

    public VarExpandOperator( final WorkIdentity workIdentity, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int minLength, final int maxLength, final boolean shouldExpandAll,
            final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate, final Expression relationshipPredicate )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$relOffset = relOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$dir = dir;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$types = types;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$minLength = minLength;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$maxLength = maxLength;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$shouldExpandAll = shouldExpandAll;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempNodeOffset = tempNodeOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempRelationshipOffset = tempRelationshipOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$nodePredicate = nodePredicate;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$relationshipPredicate = relationshipPredicate;
        StreamingOperator.$init$( this );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getFromNodeFunction =
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( fromSlot, false );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getToNodeFunction =
                shouldExpandAll ? SlotConfigurationUtils$.MODULE$.NO_ENTITY_FUNCTION()
                                : SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( toSlot, false );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$toOffset = toSlot.offset();
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$projectBackwards =
                org.neo4j.cypher.internal.runtime.interpreted.pipes.VarLengthExpandPipe..MODULE$.projectBackwards( dir, projectedDir );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public ToLongFunction<ExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getFromNodeFunction()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getFromNodeFunction;
    }

    public ToLongFunction<ExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getToNodeFunction()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getToNodeFunction;
    }

    public int org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$toOffset()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$toOffset;
    }

    public boolean org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$projectBackwards()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$projectBackwards;
    }

    public String toString()
    {
        return "VarExpand";
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new VarExpandOperator.OTask[]{new VarExpandOperator.OTask( this, inputMorsel.nextCopy() )}) ));
    }

    public class OTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private VarExpandCursor varExpandCursor;
        private SlottedQueryState org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState;
        private OperatorProfileEvent executionEvent;

        public OTask( final VarExpandOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "VarExpandTask";
        }

        private VarExpandCursor varExpandCursor()
        {
            return this.varExpandCursor;
        }

        private void varExpandCursor_$eq( final VarExpandCursor x$1 )
        {
            this.varExpandCursor = x$1;
        }

        public SlottedQueryState org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState;
        }

        private void org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState_$eq( final SlottedQueryState x$1 )
        {
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState = x$1;
        }

        private OperatorProfileEvent executionEvent()
        {
            return this.executionEvent;
        }

        private void executionEvent_$eq( final OperatorProfileEvent x$1 )
        {
            this.executionEvent = x$1;
        }

        public void enterOperate( final QueryContext context, final QueryState state, final QueryResources resources )
        {
            if ( this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempNodeOffset !=
                    VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() ||
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempRelationshipOffset !=
                            VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() )
            {
                this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState_$eq(
                        new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(),
                                (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply(
                                IndexReadSession.class ) ), resources.expressionVariables(
                        state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
                MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14()))
                ;
            }

            if ( this.varExpandCursor() != null )
            {
                this.varExpandCursor().enterWorkUnit( resources.cursorPools() );
            }
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            long fromNode =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getFromNodeFunction().applyAsLong(
                            this.inputMorsel() );
            long toNode =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$getToNodeFunction().applyAsLong(
                            this.inputMorsel() );
            VarExpandPredicate nodeVarExpandPredicate =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempNodeOffset !=
                            VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() ? new VarExpandPredicate<Object>( this, context )
                    {
                        private final QueryContext context$1;

                        public
                        {
                            if ( $outer == null )
                            {
                                throw null;
                            }
                            else
                            {
                                this.$outer = $outer;
                                this.context$1 = context$1;
                            }
                        }

                        public boolean isTrue( final long nodeId )
                        {
                            NodeValue value = this.context$1.nodeById( nodeId );
                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState().expressionVariables()[this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempNodeOffset] =
                                    value;
                            return this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$nodePredicate.apply(
                                    this.$outer.inputMorsel(),
                                    this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState() ) ==
                                    Values.TRUE;
                        }
                    } : VarExpandPredicate$.MODULE$.NO_NODE_PREDICATE();
            VarExpandPredicate relVarExpandPredicate =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempRelationshipOffset !=
                            VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() ? new VarExpandPredicate<RelationshipSelectionCursor>( this, context )
                    {
                        private final QueryContext context$1;

                        public
                        {
                            if ( $outer == null )
                            {
                                throw null;
                            }
                            else
                            {
                                this.$outer = $outer;
                                this.context$1 = context$1;
                            }
                        }

                        public boolean isTrue( final RelationshipSelectionCursor cursor )
                        {
                            RelationshipValue value = VarExpandCursor$.MODULE$.relationshipFromCursor( this.context$1, cursor );
                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState().expressionVariables()[this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$tempRelationshipOffset] =
                                    value;
                            return this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$relationshipPredicate.apply(
                                    this.$outer.inputMorsel(),
                                    this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$predicateState() ) ==
                                    Values.TRUE;
                        }
                    } : VarExpandPredicate$.MODULE$.NO_RELATIONSHIP_PREDICATE();
            boolean var10000;
            if ( !NullChecker$.MODULE$.entityIsNull( fromNode ) && nodeVarExpandPredicate.isTrue( BoxesRunTime.boxToLong( fromNode ) ) &&
                    (this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$shouldExpandAll ||
                            !NullChecker$.MODULE$.entityIsNull( toNode )) )
            {
                this.varExpandCursor_$eq( VarExpandCursor$.MODULE$.apply(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$dir,
                        fromNode, toNode, (NodeCursor) resources.cursorPools().nodeCursorPool().allocateAndTrace(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$projectBackwards(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$types.types(
                                context ),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$minLength,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$maxLength,
                        context.transactionalContext().dataRead(), context, nodeVarExpandPredicate, relVarExpandPredicate ) );
                this.varExpandCursor().enterWorkUnit( resources.cursorPools() );
                this.varExpandCursor().setTracer( this.executionEvent() );
                var10000 = true;
            }
            else
            {
                var10000 = false;
            }

            return var10000;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.varExpandCursor().next() )
            {
                outputRow.copyFrom( this.inputMorsel() );
                if ( this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$shouldExpandAll )
                {
                    outputRow.setLongAt(
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$toOffset(),
                            this.varExpandCursor().toNode() );
                }

                outputRow.setRefAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperator$$relOffset,
                        this.varExpandCursor().relationships() );
                outputRow.moveToNextRow();
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            this.executionEvent_$eq( event );
            if ( this.varExpandCursor() != null )
            {
                this.varExpandCursor().setTracer( event );
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            if ( this.varExpandCursor() != null )
            {
                this.varExpandCursor().free( resources.cursorPools() );
                this.varExpandCursor_$eq( (VarExpandCursor) null );
            }
        }
    }
}
