package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.IndexedSeq;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class NodeIndexOperatorWithValues<CURSOR extends NodeValueIndexCursor> implements StreamingOperator
{
    private final int nodeOffset;
    private final int[] indexPropertyIndices;
    private final int[] indexPropertySlotOffsets;

    public NodeIndexOperatorWithValues( final int nodeOffset, final SlottedIndexedProperty[] properties )
    {
        this.nodeOffset = nodeOffset;
        StreamingOperator.$init$( this );
        this.indexPropertyIndices = (int[]) (new ofRef(.MODULE$.refArrayOps(
                (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) properties )) ).zipWithIndex(
                        scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )) )))).filter( ( x$1 ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$indexPropertyIndices$1( x$1 ) );
    } )))).map( ( x$2 ) -> {
        return BoxesRunTime.boxToInteger( $anonfun$indexPropertyIndices$2( x$2 ) );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
        this.indexPropertySlotOffsets = (int[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) properties ))).flatMap( ( x$3 ) -> {
        return scala.Option..MODULE$.option2Iterable( x$3.maybeCachedNodePropertySlot() );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public int[] indexPropertyIndices()
    {
        return this.indexPropertyIndices;
    }

    private int[] indexPropertySlotOffsets()
    {
        return this.indexPropertySlotOffsets;
    }

    public void iterate( final MorselExecutionContext inputRow, final MorselExecutionContext outputRow, final CURSOR cursor,
            final SlotConfiguration.Size argumentSize )
    {
        while ( outputRow.isValidRow() && cursor.next() )
        {
            outputRow.copyFrom( inputRow, argumentSize.nLongs(), argumentSize.nReferences() );
            outputRow.setLongAt( this.nodeOffset, cursor.nodeReference() );

            for ( int i = 0; i < this.indexPropertyIndices().length; ++i )
            {
                outputRow.setCachedPropertyAt( this.indexPropertySlotOffsets()[i], cursor.propertyValue( this.indexPropertyIndices()[i] ) );
            }

            outputRow.moveToNextRow();
        }
    }
}
