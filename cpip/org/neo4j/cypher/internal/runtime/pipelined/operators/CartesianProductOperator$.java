package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class CartesianProductOperator$
{
    public static CartesianProductOperator$ MODULE$;

    static
    {
        new CartesianProductOperator$();
    }

    private CartesianProductOperator$()
    {
        MODULE$ = this;
    }
}
