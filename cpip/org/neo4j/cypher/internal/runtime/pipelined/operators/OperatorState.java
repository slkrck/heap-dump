package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface OperatorState
{
    IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput, final int parallelism,
            final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps );
}
