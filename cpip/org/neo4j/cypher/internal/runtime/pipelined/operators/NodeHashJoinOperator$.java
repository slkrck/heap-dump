package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class NodeHashJoinOperator$
{
    public static NodeHashJoinOperator$ MODULE$;

    static
    {
        new NodeHashJoinOperator$();
    }

    private NodeHashJoinOperator$()
    {
        MODULE$ = this;
    }
}
