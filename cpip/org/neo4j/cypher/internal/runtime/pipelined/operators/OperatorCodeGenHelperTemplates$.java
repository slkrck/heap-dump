package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.GetStatic;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.Method;
import org.neo4j.codegen.api.Parameter;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool;
import org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPools;
import org.neo4j.cypher.internal.runtime.pipelined.execution.FlowControl;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.operations.CypherCoercions;
import org.neo4j.cypher.operations.CypherFunctions;
import org.neo4j.internal.kernel.api.Cursor;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.NodeLabelIndexCursor;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.IndexQuery.ExactPredicate;
import org.neo4j.internal.kernel.api.IndexQuery.RangePredicate;
import org.neo4j.internal.kernel.api.IndexQuery.StringContainsPredicate;
import org.neo4j.internal.kernel.api.IndexQuery.StringSuffixPredicate;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.token.api.TokenConstants;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.virtual.ListValue;
import scala.MatchError;
import scala.reflect.Manifest;
import scala.runtime.BoxesRunTime;

public final class OperatorCodeGenHelperTemplates$
{
    public static OperatorCodeGenHelperTemplates$ MODULE$;

    static
    {
        new OperatorCodeGenHelperTemplates$();
    }

    private final IntermediateRepresentation UNINITIALIZED_LONG_SLOT_VALUE;
    private final IntermediateRepresentation UNINITIALIZED_REF_SLOT_VALUE;
    private final Parameter DATA_READ_CONSTRUCTOR_PARAMETER;
    private final Parameter INPUT_MORSEL_CONSTRUCTOR_PARAMETER;
    private final Parameter ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER;
    private final Parameter QUERY_RESOURCE_PARAMETER;
    private final Parameter OPERATOR_CLOSER_PARAMETER;
    private final String WORK_IDENTITY_STATIC_FIELD_NAME;
    private final InstanceField DATA_READ;
    private final InstanceField INPUT_MORSEL;
    private final IntermediateRepresentation QUERY_PROFILER;
    private final IntermediateRepresentation QUERY_STATE;
    private final IntermediateRepresentation QUERY_RESOURCES;
    private final LocalVariable CURSOR_POOL_V;
    private final IntermediateRepresentation CURSOR_POOL;
    private final IntermediateRepresentation OUTPUT_ROW;
    private final IntermediateRepresentation OUTPUT_ROW_MOVE_TO_NEXT;
    private final IntermediateRepresentation DB_ACCESS;
    private final IntermediateRepresentation PARAMS;
    private final IntermediateRepresentation EXPRESSION_CURSORS;
    private final IntermediateRepresentation EXPRESSION_VARIABLES;
    private final IntermediateRepresentation EXECUTION_STATE;
    private final IntermediateRepresentation PIPELINE_ID;
    private final LocalVariable SUBSCRIBER;
    private final LocalVariable SUBSCRIPTION;
    private final LocalVariable DEMAND;
    private final LocalVariable SERVED;
    private final IntermediateRepresentation HAS_DEMAND;
    private final LocalVariable PRE_POPULATE_RESULTS_V;
    private final IntermediateRepresentation PRE_POPULATE_RESULTS;
    private final IntermediateRepresentation ALLOCATE_NODE_CURSOR;
    private final IntermediateRepresentation ALLOCATE_NODE_LABEL_CURSOR;
    private final IntermediateRepresentation ALLOCATE_NODE_INDEX_CURSOR;
    private final IntermediateRepresentation ALLOCATE_GROUP_CURSOR;
    private final IntermediateRepresentation ALLOCATE_TRAVERSAL_CURSOR;
    private final IntermediateRepresentation ALLOCATE_REL_SCAN_CURSOR;
    private final String OUTER_LOOP_LABEL_NAME;
    private final IntermediateRepresentation INPUT_ROW_IS_VALID;
    private final IntermediateRepresentation OUTPUT_ROW_IS_VALID;
    private final IntermediateRepresentation OUTPUT_ROW_FINISHED_WRITING;
    private final IntermediateRepresentation INPUT_ROW_MOVE_TO_NEXT;
    private final IntermediateRepresentation UPDATE_DEMAND;
    private final LocalVariable OUTPUT_COUNTER;
    private final IntermediateRepresentation UPDATE_OUTPUT_COUNTER;
    private final IntermediateRepresentation HAS_REMAINING_OUTPUT;
    private final GetStatic NO_TOKEN;
    private final Method SET_TRACER;
    private final IntermediateRepresentation NO_KERNEL_TRACER;
    private final IntermediateRepresentation NO_OPERATOR_PROFILE_EVENT;
    private final Method TRACE_ON_NODE;
    private final Method TRACE_DB_HIT;
    private final Method TRACE_DB_HITS;
    private final IntermediateRepresentation CALL_CAN_CONTINUE;

    private OperatorCodeGenHelperTemplates$()
    {
        MODULE$ = this;
        this.UNINITIALIZED_LONG_SLOT_VALUE = .MODULE$.constant( BoxesRunTime.boxToLong( -2L ) );
        this.UNINITIALIZED_REF_SLOT_VALUE = .MODULE$.constant( (Object) null );
        this.DATA_READ_CONSTRUCTOR_PARAMETER = .MODULE$.param( "dataRead", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ));
        this.INPUT_MORSEL_CONSTRUCTOR_PARAMETER = .
        MODULE$.param( "inputMorsel", scala.reflect.ManifestFactory..MODULE$.classType( MorselExecutionContext.class ));
        this.ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER = .
        MODULE$.param( "argumentStateMaps", scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.ArgumentStateMaps.class ));
        this.QUERY_RESOURCE_PARAMETER = .MODULE$.param( "resources", scala.reflect.ManifestFactory..MODULE$.classType( QueryResources.class ));
        this.OPERATOR_CLOSER_PARAMETER = .MODULE$.param( "operatorCloser", scala.reflect.ManifestFactory..MODULE$.classType( OperatorCloser.class ));
        this.WORK_IDENTITY_STATIC_FIELD_NAME = "_workIdentity";
        this.DATA_READ = .MODULE$.field( "dataRead",.MODULE$.load( this.DATA_READ_CONSTRUCTOR_PARAMETER().name() ), scala.reflect.ManifestFactory..
        MODULE$.classType( Read.class ));
        this.INPUT_MORSEL = .MODULE$.field( "inputMorsel",.MODULE$.load( this.INPUT_MORSEL_CONSTRUCTOR_PARAMETER().name() ), scala.reflect.ManifestFactory..
        MODULE$.classType( MorselExecutionContext.class ));
        this.QUERY_PROFILER = .MODULE$.load( "queryProfiler" );
        this.QUERY_STATE = .MODULE$.load( "state" );
        this.QUERY_RESOURCES = .MODULE$.load( "resources" );
        this.CURSOR_POOL_V = .MODULE$.variable( "cursorPools",.MODULE$.invoke( this.QUERY_RESOURCES(),.MODULE$.method( "cursorPools",
            scala.reflect.ManifestFactory..MODULE$.classType( QueryResources.class ), scala.reflect.ManifestFactory..MODULE$.classType( CursorPools.class )),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
        MODULE$.classType( CursorPools.class ));
        this.CURSOR_POOL = .MODULE$.load( this.CURSOR_POOL_V() );
        this.OUTPUT_ROW = .MODULE$.load( "context" );
        this.OUTPUT_ROW_MOVE_TO_NEXT = .
        MODULE$.invokeSideEffect( this.OUTPUT_ROW(),.MODULE$.method( "moveToNextRow", scala.reflect.ManifestFactory..MODULE$.classType(
                MorselExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        this.DB_ACCESS = .MODULE$.load( "dbAccess" );
        this.PARAMS = .MODULE$.load( "params" );
        this.EXPRESSION_CURSORS = .MODULE$.load( "cursors" );
        this.EXPRESSION_VARIABLES = .MODULE$.load( "expressionVariables" );
        this.EXECUTION_STATE = .MODULE$.load( "executionState" );
        this.PIPELINE_ID = .MODULE$.load( "pipelineId" );
        this.SUBSCRIBER = .
        MODULE$.variable( "subscriber",.MODULE$.invoke( this.QUERY_STATE(),.MODULE$.method( "subscriber", scala.reflect.ManifestFactory..MODULE$.classType(
                QueryState.class ), scala.reflect.ManifestFactory..MODULE$.classType( QuerySubscriber.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.classType( QuerySubscriber.class ));
        this.SUBSCRIPTION = .
        MODULE$.variable( "subscription",.MODULE$.invoke( this.QUERY_STATE(),.MODULE$.method( "flowControl", scala.reflect.ManifestFactory..MODULE$.classType(
                QueryState.class ), scala.reflect.ManifestFactory..MODULE$.classType( FlowControl.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.classType( FlowControl.class ));
        this.DEMAND = .MODULE$.variable( "demand",.MODULE$.invoke(.MODULE$.load( this.SUBSCRIPTION() ), .
        MODULE$.method( "getDemand", scala.reflect.ManifestFactory..MODULE$.classType( FlowControl.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.Long());
        this.SERVED = .MODULE$.variable( "served",.MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.HAS_DEMAND = .MODULE$.lessThan(.MODULE$.load( this.SERVED() ), .MODULE$.load( this.DEMAND() ));
        this.PRE_POPULATE_RESULTS_V = .MODULE$.variable( "prePopulateResults",.MODULE$.invoke( this.QUERY_STATE(),.MODULE$.method( "prepopulateResults",
            scala.reflect.ManifestFactory..MODULE$.classType( QueryState.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.PRE_POPULATE_RESULTS = .MODULE$.load( this.PRE_POPULATE_RESULTS_V() );
        this.ALLOCATE_NODE_CURSOR = this.allocateCursor( OperatorCodeGenHelperTemplates.NodeCursorPool$.MODULE$ );
        this.ALLOCATE_NODE_LABEL_CURSOR = this.allocateCursor( OperatorCodeGenHelperTemplates.NodeLabelIndexCursorPool$.MODULE$ );
        this.ALLOCATE_NODE_INDEX_CURSOR = this.allocateCursor( OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$.MODULE$ );
        this.ALLOCATE_GROUP_CURSOR = this.allocateCursor( OperatorCodeGenHelperTemplates.GroupCursorPool$.MODULE$ );
        this.ALLOCATE_TRAVERSAL_CURSOR = this.allocateCursor( OperatorCodeGenHelperTemplates.TraversalCursorPool$.MODULE$ );
        this.ALLOCATE_REL_SCAN_CURSOR = this.allocateCursor( OperatorCodeGenHelperTemplates.RelScanCursorPool$.MODULE$ );
        this.OUTER_LOOP_LABEL_NAME = "outerLoop";
        this.INPUT_ROW_IS_VALID = .MODULE$.invoke(.MODULE$.loadField( this.INPUT_MORSEL() ), .
        MODULE$.method( "isValidRow", scala.reflect.ManifestFactory..MODULE$.classType( MorselExecutionContext.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        this.OUTPUT_ROW_IS_VALID = .MODULE$.invoke( this.OUTPUT_ROW(),.MODULE$.method( "isValidRow", scala.reflect.ManifestFactory..MODULE$.classType(
            MorselExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        this.OUTPUT_ROW_FINISHED_WRITING = .
        MODULE$.invokeSideEffect( this.OUTPUT_ROW(),.MODULE$.method( "finishedWriting", scala.reflect.ManifestFactory..MODULE$.classType(
                MorselExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        this.INPUT_ROW_MOVE_TO_NEXT = .MODULE$.invokeSideEffect(.MODULE$.loadField( this.INPUT_MORSEL() ), .
        MODULE$.method( "moveToNextRow", scala.reflect.ManifestFactory..MODULE$.classType( MorselExecutionContext.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        this.UPDATE_DEMAND = .MODULE$.invokeSideEffect(.MODULE$.load( this.SUBSCRIPTION() ), .
        MODULE$.method( "addServed", scala.reflect.ManifestFactory..MODULE$.classType( FlowControl.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.load( this.SERVED() )})));
        this.OUTPUT_COUNTER = .
        MODULE$.variable( "outputCounter",.MODULE$.invoke( this.QUERY_STATE(),.MODULE$.method( "morselSize", scala.reflect.ManifestFactory..MODULE$.classType(
                QueryState.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.Int());
        this.UPDATE_OUTPUT_COUNTER = .MODULE$.assign( this.OUTPUT_COUNTER(),.MODULE$.subtract(.MODULE$.load( this.OUTPUT_COUNTER() ), .
        MODULE$.constant( BoxesRunTime.boxToInteger( 1 ) )));
        this.HAS_REMAINING_OUTPUT = .MODULE$.greaterThan(.MODULE$.load( this.OUTPUT_COUNTER() ), .MODULE$.constant( BoxesRunTime.boxToInteger( 0 ) ));
        this.NO_TOKEN = .MODULE$.getStatic( "NO_TOKEN", scala.reflect.ManifestFactory..MODULE$.classType( TokenConstants.class ), scala.reflect.ManifestFactory..
        MODULE$.Int());
        this.SET_TRACER = .MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( Cursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class ));
        this.NO_KERNEL_TRACER = .MODULE$.constant( (Object) null );
        this.NO_OPERATOR_PROFILE_EVENT = .MODULE$.constant( (Object) null );
        this.TRACE_ON_NODE = .
        MODULE$.method( "onNode", scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Long());
        this.TRACE_DB_HIT = .
        MODULE$.method( "dbHit", scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit());
        this.TRACE_DB_HITS = .
        MODULE$.method( "dbHits", scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Int());
        this.CALL_CAN_CONTINUE = .MODULE$.invoke(.MODULE$.self(), .
        MODULE$.method( "canContinue", scala.reflect.ManifestFactory..MODULE$.classType( ContinuableOperatorTask.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
    }

    public IntermediateRepresentation UNINITIALIZED_LONG_SLOT_VALUE()
    {
        return this.UNINITIALIZED_LONG_SLOT_VALUE;
    }

    public IntermediateRepresentation UNINITIALIZED_REF_SLOT_VALUE()
    {
        return this.UNINITIALIZED_REF_SLOT_VALUE;
    }

    public Parameter DATA_READ_CONSTRUCTOR_PARAMETER()
    {
        return this.DATA_READ_CONSTRUCTOR_PARAMETER;
    }

    public Parameter INPUT_MORSEL_CONSTRUCTOR_PARAMETER()
    {
        return this.INPUT_MORSEL_CONSTRUCTOR_PARAMETER;
    }

    public Parameter ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER()
    {
        return this.ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER;
    }

    public Parameter QUERY_RESOURCE_PARAMETER()
    {
        return this.QUERY_RESOURCE_PARAMETER;
    }

    public Parameter OPERATOR_CLOSER_PARAMETER()
    {
        return this.OPERATOR_CLOSER_PARAMETER;
    }

    public String WORK_IDENTITY_STATIC_FIELD_NAME()
    {
        return this.WORK_IDENTITY_STATIC_FIELD_NAME;
    }

    public InstanceField DATA_READ()
    {
        return this.DATA_READ;
    }

    public InstanceField INPUT_MORSEL()
    {
        return this.INPUT_MORSEL;
    }

    public IntermediateRepresentation QUERY_PROFILER()
    {
        return this.QUERY_PROFILER;
    }

    public IntermediateRepresentation QUERY_STATE()
    {
        return this.QUERY_STATE;
    }

    public IntermediateRepresentation QUERY_RESOURCES()
    {
        return this.QUERY_RESOURCES;
    }

    public LocalVariable CURSOR_POOL_V()
    {
        return this.CURSOR_POOL_V;
    }

    public IntermediateRepresentation CURSOR_POOL()
    {
        return this.CURSOR_POOL;
    }

    public IntermediateRepresentation OUTPUT_ROW()
    {
        return this.OUTPUT_ROW;
    }

    public IntermediateRepresentation OUTPUT_ROW_MOVE_TO_NEXT()
    {
        return this.OUTPUT_ROW_MOVE_TO_NEXT;
    }

    public IntermediateRepresentation DB_ACCESS()
    {
        return this.DB_ACCESS;
    }

    public IntermediateRepresentation PARAMS()
    {
        return this.PARAMS;
    }

    public IntermediateRepresentation EXPRESSION_CURSORS()
    {
        return this.EXPRESSION_CURSORS;
    }

    public IntermediateRepresentation EXPRESSION_VARIABLES()
    {
        return this.EXPRESSION_VARIABLES;
    }

    public IntermediateRepresentation EXECUTION_STATE()
    {
        return this.EXECUTION_STATE;
    }

    public IntermediateRepresentation PIPELINE_ID()
    {
        return this.PIPELINE_ID;
    }

    public LocalVariable SUBSCRIBER()
    {
        return this.SUBSCRIBER;
    }

    public LocalVariable SUBSCRIPTION()
    {
        return this.SUBSCRIPTION;
    }

    public LocalVariable DEMAND()
    {
        return this.DEMAND;
    }

    public LocalVariable SERVED()
    {
        return this.SERVED;
    }

    public IntermediateRepresentation HAS_DEMAND()
    {
        return this.HAS_DEMAND;
    }

    public LocalVariable PRE_POPULATE_RESULTS_V()
    {
        return this.PRE_POPULATE_RESULTS_V;
    }

    public IntermediateRepresentation PRE_POPULATE_RESULTS()
    {
        return this.PRE_POPULATE_RESULTS;
    }

    public IntermediateRepresentation ALLOCATE_NODE_CURSOR()
    {
        return this.ALLOCATE_NODE_CURSOR;
    }

    public IntermediateRepresentation ALLOCATE_NODE_LABEL_CURSOR()
    {
        return this.ALLOCATE_NODE_LABEL_CURSOR;
    }

    public IntermediateRepresentation ALLOCATE_NODE_INDEX_CURSOR()
    {
        return this.ALLOCATE_NODE_INDEX_CURSOR;
    }

    public IntermediateRepresentation ALLOCATE_GROUP_CURSOR()
    {
        return this.ALLOCATE_GROUP_CURSOR;
    }

    public IntermediateRepresentation ALLOCATE_TRAVERSAL_CURSOR()
    {
        return this.ALLOCATE_TRAVERSAL_CURSOR;
    }

    public IntermediateRepresentation ALLOCATE_REL_SCAN_CURSOR()
    {
        return this.ALLOCATE_REL_SCAN_CURSOR;
    }

    public String OUTER_LOOP_LABEL_NAME()
    {
        return this.OUTER_LOOP_LABEL_NAME;
    }

    public IntermediateRepresentation INPUT_ROW_IS_VALID()
    {
        return this.INPUT_ROW_IS_VALID;
    }

    public IntermediateRepresentation OUTPUT_ROW_IS_VALID()
    {
        return this.OUTPUT_ROW_IS_VALID;
    }

    public IntermediateRepresentation OUTPUT_ROW_FINISHED_WRITING()
    {
        return this.OUTPUT_ROW_FINISHED_WRITING;
    }

    public IntermediateRepresentation INPUT_ROW_MOVE_TO_NEXT()
    {
        return this.INPUT_ROW_MOVE_TO_NEXT;
    }

    public IntermediateRepresentation UPDATE_DEMAND()
    {
        return this.UPDATE_DEMAND;
    }

    public LocalVariable OUTPUT_COUNTER()
    {
        return this.OUTPUT_COUNTER;
    }

    public IntermediateRepresentation UPDATE_OUTPUT_COUNTER()
    {
        return this.UPDATE_OUTPUT_COUNTER;
    }

    public IntermediateRepresentation HAS_REMAINING_OUTPUT()
    {
        return this.HAS_REMAINING_OUTPUT;
    }

    public GetStatic NO_TOKEN()
    {
        return this.NO_TOKEN;
    }

    public Method SET_TRACER()
    {
        return this.SET_TRACER;
    }

    public IntermediateRepresentation NO_KERNEL_TRACER()
    {
        return this.NO_KERNEL_TRACER;
    }

    public IntermediateRepresentation NO_OPERATOR_PROFILE_EVENT()
    {
        return this.NO_OPERATOR_PROFILE_EVENT;
    }

    private Method TRACE_ON_NODE()
    {
        return this.TRACE_ON_NODE;
    }

    private Method TRACE_DB_HIT()
    {
        return this.TRACE_DB_HIT;
    }

    private Method TRACE_DB_HITS()
    {
        return this.TRACE_DB_HITS;
    }

    public IntermediateRepresentation CALL_CAN_CONTINUE()
    {
        return this.CALL_CAN_CONTINUE;
    }

    public IntermediateRepresentation allocateCursor( final OperatorCodeGenHelperTemplates.CursorPoolsType cursorPools )
    {
        return .MODULE$.invoke(.MODULE$.invoke( this.CURSOR_POOL(),.MODULE$.method( cursorPools.name(), scala.reflect.ManifestFactory..MODULE$.classType(
            CursorPools.class ), scala.reflect.ManifestFactory..MODULE$.classType( CursorPool.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )), .
        MODULE$.method( "allocate", scala.reflect.ManifestFactory..MODULE$.classType( CursorPool.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.classType( Cursor.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
    }

    public IntermediateRepresentation allNodeScan( final IntermediateRepresentation cursor )
    {
        return .MODULE$.invokeSideEffect(.MODULE$.loadField( this.DATA_READ() ), .
        MODULE$.method( "allNodesScan", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{cursor}) ));
    }

    public IntermediateRepresentation nodeLabelScan( final IntermediateRepresentation label, final IntermediateRepresentation cursor )
    {
        return .MODULE$.invokeSideEffect(.MODULE$.loadField( this.DATA_READ() ), .
        MODULE$.method( "nodeLabelScan", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class )),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{label, cursor}) ));
    }

    public IntermediateRepresentation nodeIndexScan( final IntermediateRepresentation indexReadSession, final IntermediateRepresentation cursor,
            final IndexOrder order, final boolean needsValues )
    {
        return .MODULE$.invokeSideEffect(.MODULE$.loadField( this.DATA_READ() ), .
        MODULE$.method( "nodeIndexScan", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( IndexReadSession.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( IndexOrder.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{indexReadSession, cursor, this.indexOrder( order ),.MODULE$.constant(
                    BoxesRunTime.boxToBoolean( needsValues ) )})));
    }

    public IntermediateRepresentation nodeIndexSeek( final IntermediateRepresentation indexReadSession, final IntermediateRepresentation cursor,
            final IntermediateRepresentation query, final IndexOrder order, final boolean needsValues )
    {
        return .MODULE$.invokeSideEffect(.MODULE$.loadField( this.DATA_READ() ), .
        MODULE$.method( "nodeIndexSeek", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( IndexReadSession.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( IndexOrder.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class ))),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{indexReadSession, cursor, this.indexOrder( order ),.MODULE$.constant(
            BoxesRunTime.boxToBoolean( needsValues ) ),.MODULE$.arrayOf( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{query}) ), scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class ))})));
    }

    public IntermediateRepresentation indexOrder( final IndexOrder indexOrder )
    {
        GetStatic var2;
        if ( IndexOrder.ASCENDING.equals( indexOrder ) )
        {
            var2 = .MODULE$.getStatic( "ASCENDING", scala.reflect.ManifestFactory..MODULE$.classType( IndexOrder.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( IndexOrder.class ));
        }
        else if ( IndexOrder.DESCENDING.equals( indexOrder ) )
        {
            var2 = .MODULE$.getStatic( "DESCENDING", scala.reflect.ManifestFactory..MODULE$.classType( IndexOrder.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( IndexOrder.class ));
        }
        else
        {
            if ( !IndexOrder.NONE.equals( indexOrder ) )
            {
                throw new MatchError( indexOrder );
            }

            var2 = .MODULE$.getStatic( "NONE", scala.reflect.ManifestFactory..MODULE$.classType( IndexOrder.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( IndexOrder.class ));
        }

        return var2;
    }

    public IntermediateRepresentation exactSeek( final int prop, final IntermediateRepresentation expression )
    {
        return .
        MODULE$.invokeStatic(.MODULE$.method( "exact", scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( ExactPredicate.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( prop ) ), expression})));
    }

    public IntermediateRepresentation lessThanSeek( final int prop, final boolean inclusive, final IntermediateRepresentation expression )
    {
        return .
        MODULE$.invokeStatic(.MODULE$.method( "range", scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RangePredicate.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( prop ) ),.MODULE$.constant(
                (Object) null ), .MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ), expression, .
        MODULE$.constant( BoxesRunTime.boxToBoolean( inclusive ) )})));
    }

    public IntermediateRepresentation greaterThanSeek( final int prop, final boolean inclusive, final IntermediateRepresentation expression )
    {
        return .
        MODULE$.invokeStatic(.MODULE$.method( "range", scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RangePredicate.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( prop ) ), expression,.MODULE$.constant(
                BoxesRunTime.boxToBoolean( inclusive ) ), .MODULE$.constant( (Object) null ), .MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )})));
    }

    public IntermediateRepresentation rangeBetweenSeek( final int prop, final boolean fromInclusive, final IntermediateRepresentation fromExpression,
            final boolean toInclusive, final IntermediateRepresentation toExpression )
    {
        return .
        MODULE$.invokeStatic(.MODULE$.method( "range", scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RangePredicate.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( prop ) ),
                fromExpression,.MODULE$.constant( BoxesRunTime.boxToBoolean( fromInclusive ) ), toExpression, .
        MODULE$.constant( BoxesRunTime.boxToBoolean( toInclusive ) )})));
    }

    public IntermediateRepresentation stringContainsScan( final int prop, final IntermediateRepresentation expression )
    {
        return .MODULE$.invokeStatic(.MODULE$.method( "stringContains", scala.reflect.ManifestFactory..MODULE$.classType(
            IndexQuery.class ), scala.reflect.ManifestFactory..MODULE$.classType( StringContainsPredicate.class ), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( prop ) ), expression})));
    }

    public IntermediateRepresentation stringEndsWithScan( final int prop, final IntermediateRepresentation expression )
    {
        return .MODULE$.invokeStatic(.MODULE$.method( "stringSuffix", scala.reflect.ManifestFactory..MODULE$.classType(
            IndexQuery.class ), scala.reflect.ManifestFactory..MODULE$.classType( StringSuffixPredicate.class ), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( prop ) ), expression})));
    }

    public IntermediateRepresentation singleNode( final IntermediateRepresentation node, final IntermediateRepresentation cursor )
    {
        return .MODULE$.invokeSideEffect(.MODULE$.loadField( this.DATA_READ() ), .
        MODULE$.method( "singleNode", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{node, cursor}) ));
    }

    public IntermediateRepresentation singleRelationship( final IntermediateRepresentation relationship, final IntermediateRepresentation cursor )
    {
        return .MODULE$.invokeSideEffect(.MODULE$.loadField( this.DATA_READ() ), .
        MODULE$.method( "singleRelationship", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class )),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{relationship, cursor}) ));
    }

    public IntermediateRepresentation allocateAndTraceCursor( final InstanceField cursorField, final InstanceField executionEventField,
            final IntermediateRepresentation allocate )
    {
        return .MODULE$.condition(.MODULE$.isNull(.MODULE$.loadField( cursorField )), .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.setField( cursorField, allocate ),.MODULE$.invokeSideEffect(.MODULE$.loadField(
            cursorField ), this.SET_TRACER(), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.loadField( executionEventField )})))}))));
    }

    public <CURSOR> IntermediateRepresentation freeCursor( final IntermediateRepresentation cursor,
            final OperatorCodeGenHelperTemplates.CursorPoolsType cursorPools, final Manifest<CURSOR> out )
    {
        return .
        MODULE$.invokeSideEffect(.MODULE$.invoke( this.CURSOR_POOL(),.MODULE$.method( cursorPools.name(), scala.reflect.ManifestFactory..MODULE$.classType(
                CursorPools.class ), scala.reflect.ManifestFactory..MODULE$.classType( CursorPool.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )), .
        MODULE$.method( "free", scala.reflect.ManifestFactory..MODULE$.classType( CursorPool.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
        MODULE$.classType( Cursor.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{cursor}) ));
    }

    public <CURSOR> IntermediateRepresentation cursorNext( final IntermediateRepresentation cursor, final Manifest<CURSOR> out )
    {
        return .MODULE$.invoke( cursor,.MODULE$.method( "next", out, scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
    }

    public IntermediateRepresentation nodeLabelId( final String labelName )
    {
        return .MODULE$.invoke( this.DB_ACCESS(),.MODULE$.method( "nodeLabel", scala.reflect.ManifestFactory..MODULE$.classType(
            DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( labelName )})));
    }

    public IntermediateRepresentation relationshipTypeId( final String typeName )
    {
        return .MODULE$.invoke( this.DB_ACCESS(),.MODULE$.method( "relationshipType", scala.reflect.ManifestFactory..MODULE$.classType(
            DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( typeName )})));
    }

    private IntermediateRepresentation event( final int id )
    {
        return .MODULE$.loadField(.MODULE$.field( (new StringBuilder( 23 )).append( "operatorExecutionEvent_" ).append( id ).toString(),
            scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class )));
    }

    public <CURSOR> IntermediateRepresentation profilingCursorNext( final IntermediateRepresentation cursor, final int id, final Manifest<CURSOR> out )
    {
        String hasNext = (new StringBuilder( 4 )).append( "tmp_" ).append( id ).toString();
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.declareAndAssign(.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Boolean()),
            hasNext,.MODULE$.invoke( cursor,.MODULE$.method( "next", out, scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))), .MODULE$.condition(.MODULE$.isNotNull( this.event( id ) ), .
        MODULE$.invokeSideEffect( this.event( id ),.MODULE$.method( "row", scala.reflect.ManifestFactory..MODULE$.classType(
                OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.load( hasNext )})))), .MODULE$.load( hasNext )})));
    }

    public IntermediateRepresentation profileRow( final int id )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( this.event( id ) ), .
        MODULE$.invokeSideEffect( this.event( id ),.MODULE$.method( "row", scala.reflect.ManifestFactory..MODULE$.classType(
                OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )));
    }

    public IntermediateRepresentation profileRow( final int id, final IntermediateRepresentation hasRow )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( this.event( id ) ), .
        MODULE$.invokeSideEffect( this.event( id ),.MODULE$.method( "row", scala.reflect.ManifestFactory..MODULE$.classType(
                OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{hasRow}) )));
    }

    public IntermediateRepresentation profileRows( final int id, final int nRows )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( this.event( id ) ), .
        MODULE$.invokeSideEffect( this.event( id ),.MODULE$.method( "rows", scala.reflect.ManifestFactory..MODULE$.classType(
                OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( nRows ) )}))));
    }

    public IntermediateRepresentation profileRows( final int id, final IntermediateRepresentation nRows )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( this.event( id ) ), .
        MODULE$.invokeSideEffect( this.event( id ),.MODULE$.method( "rows", scala.reflect.ManifestFactory..MODULE$.classType(
                OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nRows}) )));
    }

    public IntermediateRepresentation closeEvent( final int id )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( this.event( id ) ), .
        MODULE$.invokeSideEffect( this.event( id ),.MODULE$.method( "close", scala.reflect.ManifestFactory..MODULE$.classType(
                OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )));
    }

    public IntermediateRepresentation dbHit( final IntermediateRepresentation event )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( event ), .
        MODULE$.invoke( event, this.TRACE_DB_HIT(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )));
    }

    public IntermediateRepresentation dbHits( final IntermediateRepresentation event, final IntermediateRepresentation nHits )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( event ), .
        MODULE$.invoke( event, this.TRACE_DB_HITS(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nHits}) )));
    }

    public IntermediateRepresentation onNode( final IntermediateRepresentation event, final IntermediateRepresentation node )
    {
        return .MODULE$.condition(.MODULE$.isNotNull( event ), .
        MODULE$.invoke( event, this.TRACE_ON_NODE(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{node}) )));
    }

    public IntermediateRepresentation indexReadSession( final int offset )
    {
        return .MODULE$.arrayLoad(.MODULE$.invoke( this.QUERY_STATE(),.MODULE$.method( "queryIndexes", scala.reflect.ManifestFactory..MODULE$.classType(
            QueryState.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( IndexReadSession.class ))),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),offset);
    }

    public IntermediateRepresentation asStorableValue( final IntermediateRepresentation in )
    {
        return .MODULE$.invokeStatic(.MODULE$.method( "asStorableValue", scala.reflect.ManifestFactory..MODULE$.classType(
            CypherCoercions.class ), scala.reflect.ManifestFactory..MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in}) ));
    }

    public IntermediateRepresentation asListValue( final IntermediateRepresentation in )
    {
        return .MODULE$.invokeStatic(.MODULE$.method( "asList", scala.reflect.ManifestFactory..MODULE$.classType(
            CypherFunctions.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in}) ));
    }
}
