package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipGroupCursor;
import org.neo4j.internal.kernel.api.RelationshipTraversalCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelections;
import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class OutgoingVarExpandCursor extends VarExpandCursor
{
    public OutgoingVarExpandCursor( final long fromNode, final long targetToNode, final NodeCursor nodeCursor, final boolean projectBackwards,
            final int[] relTypes, final int minLength, final int maxLength, final Read read, final ExecutionContext executionContext, final DbAccess dbAccess,
            final AnyValue[] params, final ExpressionCursors cursors, final AnyValue[] expressionVariables )
    {
        super( fromNode, targetToNode, nodeCursor, projectBackwards, relTypes, minLength, maxLength, read, executionContext, dbAccess, params, cursors,
                expressionVariables );
    }

    public long fromNode()
    {
        return super.fromNode();
    }

    public RelationshipSelectionCursor selectionCursor( final RelationshipGroupCursor groupCursor, final RelationshipTraversalCursor traversalCursor,
            final NodeCursor node, final int[] types )
    {
        return RelationshipSelections.outgoingCursor( groupCursor, traversalCursor, node, types );
    }
}
