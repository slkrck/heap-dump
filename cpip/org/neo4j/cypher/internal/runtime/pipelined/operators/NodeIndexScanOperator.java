package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.schema.IndexOrder;
import scala.collection.IndexedSeq;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class NodeIndexScanOperator extends NodeIndexOperatorWithValues<NodeValueIndexCursor>
{
    public final IndexOrder org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$indexOrder;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$argumentSize;
    private final WorkIdentity workIdentity;
    private final int queryIndexId;
    private final boolean org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$needsValues;

    public NodeIndexScanOperator( final WorkIdentity workIdentity, final int nodeOffset, final SlottedIndexedProperty[] properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration.Size argumentSize )
    {
        super( nodeOffset, properties );
        this.workIdentity = workIdentity;
        this.queryIndexId = queryIndexId;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$indexOrder = indexOrder;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$argumentSize = argumentSize;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$needsValues =
                (new ofInt( scala.Predef..MODULE$.intArrayOps( this.indexPropertyIndices() ))).nonEmpty();
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public boolean org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$needsValues()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$needsValues;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        IndexReadSession indexSession = state.queryIndexes()[this.queryIndexId];
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new NodeIndexScanOperator.OTask[]{new NodeIndexScanOperator.OTask( this, inputMorsel.nextCopy(), indexSession )}) ));
    }

    public class OTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private final IndexReadSession index;
        private NodeValueIndexCursor cursor;

        public OTask( final NodeIndexScanOperator $outer, final MorselExecutionContext inputMorsel, final IndexReadSession index )
        {
            this.inputMorsel = inputMorsel;
            this.index = index;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$OTask$$$outer().workIdentity();
        }

        private NodeValueIndexCursor cursor()
        {
            return this.cursor;
        }

        private void cursor_$eq( final NodeValueIndexCursor x$1 )
        {
            this.cursor = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            this.cursor_$eq( (NodeValueIndexCursor) resources.cursorPools().nodeValueIndexCursorPool().allocateAndTrace() );
            Read read = context.transactionalContext().dataRead();
            read.nodeIndexScan( this.index, this.cursor(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$indexOrder,
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$needsValues() );
            return true;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$OTask$$$outer().iterate( this.inputMorsel(), outputRow,
                    this.cursor(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexScanOperator$$argumentSize );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.cursor() != null )
            {
                this.cursor().setTracer( event );
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            resources.cursorPools().nodeValueIndexCursorPool().free( this.cursor() );
            this.cursor_$eq( (NodeValueIndexCursor) null );
        }
    }
}
