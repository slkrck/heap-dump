package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class EXPAND$ implements ExpandStatus, Product, Serializable
{
    public static EXPAND$ MODULE$;

    static
    {
        new EXPAND$();
    }

    private EXPAND$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "EXPAND";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EXPAND$;
    }

    public int hashCode()
    {
        return 2059129498;
    }

    public String toString()
    {
        return "EXPAND";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
