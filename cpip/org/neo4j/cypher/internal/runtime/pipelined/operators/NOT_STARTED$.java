package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class NOT_STARTED$ implements ExpandStatus, Product, Serializable
{
    public static NOT_STARTED$ MODULE$;

    static
    {
        new NOT_STARTED$();
    }

    private NOT_STARTED$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "NOT_STARTED";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NOT_STARTED$;
    }

    public int hashCode()
    {
        return -1391247659;
    }

    public String toString()
    {
        return "NOT_STARTED";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
