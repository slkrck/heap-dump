package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface OperatorWithInterpretedDBHitsProfiling extends OperatorTask
{
    static void $init$( final OperatorWithInterpretedDBHitsProfiling $this )
    {
    }

    default void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
            final QueryProfiler queryProfiler )
    {
        OperatorProfileEvent operatorExecutionEvent = queryProfiler.executeOperator( new Id( this.workIdentity().workId() ) );
        this.setExecutionEvent( operatorExecutionEvent );

        try
        {
            this.operate( output, context, state, resources );
            if ( operatorExecutionEvent != null )
            {
                operatorExecutionEvent.rows( output.getValidRows() );
            }
        }
        finally
        {
            this.setExecutionEvent( (OperatorProfileEvent) null );
            if ( operatorExecutionEvent != null )
            {
                operatorExecutionEvent.close();
            }
        }
    }
}
