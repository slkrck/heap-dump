package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.TextValue;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class NodeIndexStringSearchScanOperator extends NodeIndexOperatorWithValues<NodeValueIndexCursor>
{
    public final SlottedIndexedProperty org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$property;
    public final IndexOrder org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$indexOrder;
    public final Expression org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$valueExpr;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$argumentSize;
    private final WorkIdentity workIdentity;
    private final int queryIndexId;

    public NodeIndexStringSearchScanOperator( final WorkIdentity workIdentity, final int nodeOffset, final SlottedIndexedProperty property,
            final int queryIndexId, final IndexOrder indexOrder, final Expression valueExpr, final SlotConfiguration.Size argumentSize )
    {
        super( nodeOffset, (SlottedIndexedProperty[]) ((Object[]) (new SlottedIndexedProperty[]{property})) );
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$property = property;
        this.queryIndexId = queryIndexId;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$indexOrder = indexOrder;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$valueExpr = valueExpr;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$argumentSize = argumentSize;
    }

    public static Method isValidOrThrowMethod()
    {
        return NodeIndexStringSearchScanOperator$.MODULE$.isValidOrThrowMethod();
    }

    public static boolean isValidOrThrow( final AnyValue value )
    {
        return NodeIndexStringSearchScanOperator$.MODULE$.isValidOrThrow( var0 );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        IndexReadSession indexSession = state.queryIndexes()[this.queryIndexId];
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new NodeIndexStringSearchScanOperator.OTask[]{
                new NodeIndexStringSearchScanOperator.OTask( this, inputMorsel.nextCopy(), indexSession )}) ));
    }

    public abstract IndexQuery computeIndexQuery( final int property, final TextValue value );

    public class OTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private final IndexReadSession index;
        private NodeValueIndexCursor cursor;

        public OTask( final NodeIndexStringSearchScanOperator $outer, final MorselExecutionContext inputMorsel, final IndexReadSession index )
        {
            this.inputMorsel = inputMorsel;
            this.index = index;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().workIdentity();
        }

        private NodeValueIndexCursor cursor()
        {
            return this.cursor;
        }

        private void cursor_$eq( final NodeValueIndexCursor x$1 )
        {
            this.cursor = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            Read read = context.transactionalContext().dataRead();
            SlottedQueryState queryState = new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(),
                    (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
            resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
            MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
            ;
            initExecutionContext.copyFrom( this.inputMorsel(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$argumentSize.nLongs(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$argumentSize.nReferences() );
            AnyValue value =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$valueExpr.apply(
                            initExecutionContext, queryState );
            boolean var5;
            if ( value instanceof TextValue )
            {
                TextValue var10 = (TextValue) value;
                IndexQuery indexQuery =
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().computeIndexQuery(
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$property.propertyKeyId(),
                                var10 );
                this.cursor_$eq( (NodeValueIndexCursor) resources.cursorPools().nodeValueIndexCursorPool().allocateAndTrace() );
                read.nodeIndexSeek( this.index, this.cursor(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$indexOrder,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$property.maybeCachedNodePropertySlot().isDefined(),
                        new IndexQuery[]{indexQuery} );
                var5 = true;
            }
            else
            {
                if ( !org.neo4j.cypher.internal.runtime.IsNoValue..MODULE$.unapply( value )){
                throw new CypherTypeException( (new StringBuilder( 33 )).append( "Expected a string value, but got " ).append( value ).toString() );
            }

                var5 = false;
            }

            return var5;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().iterate( this.inputMorsel(), outputRow,
                    this.cursor(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexStringSearchScanOperator$$argumentSize );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.cursor() != null )
            {
                this.cursor().setTracer( event );
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            resources.cursorPools().nodeValueIndexCursorPool().free( this.cursor() );
            this.cursor_$eq( (NodeValueIndexCursor) null );
        }
    }
}
