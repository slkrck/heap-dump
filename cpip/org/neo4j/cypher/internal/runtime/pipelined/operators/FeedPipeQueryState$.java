package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.InCheckContainer;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.NullPipeDecorator.;
import org.neo4j.cypher.internal.runtime.interpreted.profiler.InterpretedProfileInformation;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Option;

public final class FeedPipeQueryState$
{
    public static FeedPipeQueryState$ MODULE$;

    static
    {
        new FeedPipeQueryState$();
    }

    private FeedPipeQueryState$()
    {
        MODULE$ = this;
    }

    public PipeDecorator $lessinit$greater$default$9()
    {
        return .MODULE$;
    }

    public Option<ExecutionContext> $lessinit$greater$default$10()
    {
        return scala.None..MODULE$;
    }

    public SingleThreadedLRUCache<Object,InCheckContainer> $lessinit$greater$default$11()
    {
        return new SingleThreadedLRUCache( 16 );
    }

    public boolean $lessinit$greater$default$12()
    {
        return false;
    }

    public boolean $lessinit$greater$default$13()
    {
        return false;
    }

    public InputDataStream $lessinit$greater$default$14()
    {
        return org.neo4j.cypher.internal.runtime.NoInput..MODULE$;
    }

    public InterpretedProfileInformation $lessinit$greater$default$15()
    {
        return null;
    }

    public MorselExecutionContext $lessinit$greater$default$16()
    {
        return null;
    }
}
