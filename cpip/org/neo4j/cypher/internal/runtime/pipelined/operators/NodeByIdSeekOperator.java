package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.SeekArgs;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.values.AnyValue;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class NodeByIdSeekOperator implements StreamingOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$offset;
    public final SeekArgs org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$nodeIdsExpr;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$argumentSize;
    private final WorkIdentity workIdentity;

    public NodeByIdSeekOperator( final WorkIdentity workIdentity, final int offset, final SeekArgs nodeIdsExpr, final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$offset = offset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$nodeIdsExpr = nodeIdsExpr;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$argumentSize = argumentSize;
        StreamingOperator.$init$( this );
    }

    public static IntermediateRepresentation isValidNode( final IntermediateRepresentation nodeId )
    {
        return NodeByIdSeekOperator$.MODULE$.isValidNode( var0 );
    }

    public static Method asIdMethod()
    {
        return NodeByIdSeekOperator$.MODULE$.asIdMethod();
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new NodeByIdSeekOperator.NodeByIdTask[]{new NodeByIdSeekOperator.NodeByIdTask( this, inputMorsel.nextCopy() )}) ));
    }

    public class NodeByIdTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private Iterator<AnyValue> ids;
        private KernelReadTracer tracer;

        public NodeByIdTask( final NodeByIdSeekOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public String toString()
        {
            return "NodeByIdTask";
        }

        private Iterator<AnyValue> ids()
        {
            return this.ids;
        }

        private void ids_$eq( final Iterator<AnyValue> x$1 )
        {
            this.ids = x$1;
        }

        private KernelReadTracer tracer()
        {
            return this.tracer;
        }

        private void tracer_$eq( final KernelReadTracer x$1 )
        {
            this.tracer = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            SlottedQueryState queryState = new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(),
                    (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
            resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
            MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
            ;
            initExecutionContext.copyFrom( this.inputMorsel(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$NodeByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$argumentSize.nLongs(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$NodeByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$argumentSize.nReferences() );
            this.ids_$eq(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$NodeByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$nodeIdsExpr.expressions(
                            initExecutionContext, queryState ).iterator() );
            return true;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$NodeByIdTask$$$outer().workIdentity();
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.ids().hasNext() )
            {
                long nextId = org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.NumericHelper..
                MODULE$.asLongEntityIdPrimitive( (AnyValue) this.ids().next() );
                if ( this.tracer() != null )
                {
                    this.tracer().onNode( nextId );
                }

                if ( nextId >= 0L && context.transactionalContext().dataRead().nodeExists( nextId ) )
                {
                    outputRow.copyFrom( this.inputMorsel(),
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$NodeByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$argumentSize.nLongs(),
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$NodeByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$argumentSize.nReferences() );
                    outputRow.setLongAt(
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$NodeByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeByIdSeekOperator$$offset,
                            nextId );
                    outputRow.moveToNextRow();
                }
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            this.tracer_$eq( event );
        }

        public void closeInnerLoop( final QueryResources resources )
        {
        }
    }
}
