package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface PreparedOutput
{
    void produce();
}
