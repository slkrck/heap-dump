package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class AggregationOperator$ extends AbstractFunction3<WorkIdentity,Aggregator[],GroupingExpression,AggregationOperator> implements Serializable
{
    public static AggregationOperator$ MODULE$;

    static
    {
        new AggregationOperator$();
    }

    private AggregationOperator$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "AggregationOperator";
    }

    public AggregationOperator apply( final WorkIdentity workIdentity, final Aggregator[] aggregations, final GroupingExpression groupings )
    {
        return new AggregationOperator( workIdentity, aggregations, groupings );
    }

    public Option<Tuple3<WorkIdentity,Aggregator[],GroupingExpression>> unapply( final AggregationOperator x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.workIdentity(), x$0.aggregations(), x$0.groupings() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
