package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.Values;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.util.Either;
import scala.util.Left;
import scala.util.Right;

@JavaDocToJava
public class RelationshipCountFromCountStoreOperatorTemplate extends InputLoopTaskTemplate
{
    private final int offset;
    private final Option<Either<String,Object>> startLabel;
    private final Seq<Either<String,Object>> relationshipTypes;
    private final Option<Either<String,Object>> endLabel;
    private final SlotConfiguration.Size argumentSize;
    private final Map<String,InstanceField> startLabelField;
    private final Map<String,InstanceField> relTypesFields;
    private final Map<String,InstanceField> endLabelField;

    public RelationshipCountFromCountStoreOperatorTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int offset, final Option<Either<String,Object>> startLabel, final Seq<Either<String,Object>> relationshipTypes,
            final Option<Either<String,Object>> endLabel, final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.offset = offset;
        this.startLabel = startLabel;
        this.relationshipTypes = relationshipTypes;
        this.endLabel = endLabel;
        this.argumentSize = argumentSize;
        this.startLabelField = scala.Option..MODULE$.option2Iterable( startLabel.collect( new Serializable( this )
    {
        public static final long serialVersionUID = 0L;

        public
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
            }
        }

        public final <A1 extends Either<String,Object>, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
        {
            Object var3;
            if ( x1 instanceof Left )
            {
                Left var5 = (Left) x1;
                String labelName = (String) var5.value();
                var3 = scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( labelName ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.field(
                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperatorTemplate$$super$codeGen().namer().variableName(
                                labelName ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN(), scala.reflect.ManifestFactory..MODULE$.Int()));
            }
            else
            {
                var3 = var2.apply( x1 );
            }

            return var3;
        }

        public final boolean isDefinedAt( final Either<String,Object> x1 )
        {
            boolean var2;
            if ( x1 instanceof Left )
            {
                var2 = true;
            }
            else
            {
                var2 = false;
            }

            return var2;
        }
    } ) ).toMap( scala.Predef..MODULE$.$conforms());
        this.relTypesFields = ((TraversableOnce) relationshipTypes.collect( new Serializable( this )
        {
            public static final long serialVersionUID = 0L;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                }
            }

            public final <A1 extends Either<String,Object>, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x2 instanceof Left )
                {
                    Left var5 = (Left) x2;
                    String typeName = (String) var5.value();
                    var3 = scala.Predef.ArrowAssoc..
                    MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( typeName ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.field(
                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperatorTemplate$$super$codeGen().namer().nextVariableName(),
                            OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN(), scala.reflect.ManifestFactory..MODULE$.Int()));
                }
                else
                {
                    var3 = var2.apply( x2 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final Either<String,Object> x2 )
            {
                boolean var2;
                if ( x2 instanceof Left )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        },.MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
        this.endLabelField = scala.Option..MODULE$.option2Iterable( endLabel.collect( new Serializable( this )
    {
        public static final long serialVersionUID = 0L;

        public
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
            }
        }

        public final <A1 extends Either<String,Object>, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
        {
            Object var3;
            if ( x3 instanceof Left )
            {
                Left var5 = (Left) x3;
                String labelName = (String) var5.value();
                var3 = scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( labelName ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.field(
                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperatorTemplate$$super$codeGen().namer().variableName(
                                labelName ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN(), scala.reflect.ManifestFactory..MODULE$.Int()));
            }
            else
            {
                var3 = var2.apply( x3 );
            }

            return var3;
        }

        public final boolean isDefinedAt( final Either<String,Object> x3 )
        {
            boolean var2;
            if ( x3 instanceof Left )
            {
                var2 = true;
            }
            else
            {
                var2 = false;
            }

            return var2;
        }
    } ) ).toMap( scala.Predef..MODULE$.$conforms());
    }

    public OperatorTaskTemplate inner()
    {
        return super.inner();
    }

    private Map<String,InstanceField> startLabelField()
    {
        return this.startLabelField;
    }

    private Map<String,InstanceField> relTypesFields()
    {
        return this.relTypesFields;
    }

    private Map<String,InstanceField> endLabelField()
    {
        return this.endLabelField;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq) ((TraversableLike) this.startLabelField().values().toSeq().$plus$plus( this.relTypesFields().values(),.MODULE$.canBuildFrom())).
        $plus$plus( this.endLabelField().values(),.MODULE$.canBuildFrom());
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.empty();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        IntermediateRepresentation startLabelOps = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( ((TraversableOnce) this.startLabelField().map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                String name = (String) x0$1._1();
                InstanceField field = (InstanceField) x0$1._2();
                IntermediateRepresentation var1 = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( field ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN()),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( field, OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelId( name ) ));
                return var1;
            }
            else
            {
                throw new MatchError( x0$1 );
            }
        }, scala.collection.immutable.Iterable..MODULE$.canBuildFrom()) ).toSeq());
        IntermediateRepresentation endLabelOps = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( ((TraversableOnce) this.endLabelField().map( ( x0$2 ) -> {
            if ( x0$2 != null )
            {
                String name = (String) x0$2._1();
                InstanceField field = (InstanceField) x0$2._2();
                IntermediateRepresentation var1 = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( field ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN()),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( field, OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelId( name ) ));
                return var1;
            }
            else
            {
                throw new MatchError( x0$2 );
            }
        }, scala.collection.immutable.Iterable..MODULE$.canBuildFrom()) ).toSeq());
        IntermediateRepresentation relTypesOps = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( (Seq) this.relTypesFields().toSeq().map( ( x0$3 ) -> {
            if ( x0$3 != null )
            {
                String name = (String) x0$3._1();
                InstanceField field = (InstanceField) x0$3._2();
                IntermediateRepresentation var1 = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( field ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN()),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( field, OperatorCodeGenHelperTemplates$.MODULE$.relationshipTypeId( name ) ))
                ;
                return var1;
            }
            else
            {
                throw new MatchError( x0$3 );
            }
        },.MODULE$.canBuildFrom() ));
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{startLabelOps, endLabelOps, relTypesOps,
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        IntermediateRepresentation var3;
        label110:
        {
            boolean var5 = false;
            Some var6 = null;
            Option var7 = this.startLabel;
            if ( var7 instanceof Some )
            {
                var5 = true;
                var6 = (Some) var7;
                Either var8 = (Either) var6.value();
                if ( var8 instanceof Left )
                {
                    Left var9 = (Left) var8;
                    String name = (String) var9.value();
                    var3 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( (Field) this.startLabelField().apply( name ) );
                    break label110;
                }
            }

            if ( var5 )
            {
                Either var11 = (Either) var6.value();
                if ( var11 instanceof Right )
                {
                    Right var12 = (Right) var11;
                    int token = BoxesRunTime.unboxToInt( var12.value() );
                    var3 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( token ) );
                    break label110;
                }
            }

            if ( !scala.None..MODULE$.equals( var7 )){
            throw new MatchError( var7 );
        }

            var3 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) );
        }

        IntermediateRepresentation var2;
        label111:
        {
            boolean var15 = false;
            Some var16 = null;
            Option var17 = this.endLabel;
            if ( var17 instanceof Some )
            {
                var15 = true;
                var16 = (Some) var17;
                Either var18 = (Either) var16.value();
                if ( var18 instanceof Left )
                {
                    Left var19 = (Left) var18;
                    String name = (String) var19.value();
                    var2 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( (Field) this.endLabelField().apply( name ) );
                    break label111;
                }
            }

            if ( var15 )
            {
                Either var21 = (Either) var16.value();
                if ( var21 instanceof Right )
                {
                    Right var22 = (Right) var21;
                    int token = BoxesRunTime.unboxToInt( var22.value() );
                    var2 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( token ) );
                    break label111;
                }
            }

            if ( !scala.None..MODULE$.equals( var17 )){
            throw new MatchError( var17 );
        }

            var2 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) );
        }

        Function1 var1;
        IntermediateRepresentation countOps;
        label112:
        {
            countOps = this.relationshipTypes.isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                    "relationshipCountByCountStore", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
            MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
            MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{var3, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                        BoxesRunTime.boxToInteger( -1 ) ), var2}))) :(IntermediateRepresentation) ((TraversableOnce) this.relationshipTypes.map( ( x0$4 ) -> {
            IntermediateRepresentation var4;
            if ( x0$4 instanceof Left )
            {
                Left var6 = (Left) x0$4;
                String name = (String) var6.value();
                var4 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    (Field) this.relTypesFields().apply( name ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                        "relationshipCountByCountStore", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{var3, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            (Field) this.relTypesFields().apply( name ) ), var2}))));
            }
            else
            {
                if ( !(x0$4 instanceof Right) )
                {
                    throw new MatchError( x0$4 );
                }

                Right var8 = (Right) x0$4;
                int token = BoxesRunTime.unboxToInt( var8.value() );
                var4 = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                        "relationshipCountByCountStore", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{var3, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                            BoxesRunTime.boxToInteger( token ) ), var2})));
            }

            return var4;
        },.MODULE$.canBuildFrom())).reduceLeft( ( lhs, rhs ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.add( lhs, rhs );
        } ); Tuple2 var26 = new Tuple2( this.startLabel, this.endLabel );
            if ( var26 != null )
            {
                Option var27 = (Option) var26._1();
                Option var28 = (Option) var26._2();
                if ( var27 instanceof Some )
                {
                    Some var29 = (Some) var27;
                    Either var30 = (Either) var29.value();
                    if ( var30 instanceof Left )
                    {
                        Left var31 = (Left) var30;
                        String start = (String) var31.value();
                        if ( var28 instanceof Some )
                        {
                            Some var33 = (Some) var28;
                            Either var34 = (Either) var33.value();
                            if ( var34 instanceof Left )
                            {
                                Left var35 = (Left) var34;
                                String end = (String) var35.value();
                                var1 = ( x$1 ) -> {
                                    return org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.or(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                            (Field) this.startLabelField().apply( start ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                            (Field) this.endLabelField().apply( end ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ), x$1);
                                }; break label112;
                            }
                        }
                    }
                }
            }

            if ( var26 != null )
            {
                Option var37 = (Option) var26._1();
                if ( var37 instanceof Some )
                {
                    Some var38 = (Some) var37;
                    Either var39 = (Either) var38.value();
                    if ( var39 instanceof Left )
                    {
                        Left var40 = (Left) var39;
                        String start = (String) var40.value();
                        var1 = ( x$2 ) -> {
                            return org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                    (Field) this.startLabelField().apply( start ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ), x$2);
                        }; break label112;
                    }
                }
            }

            if ( var26 != null )
            {
                Option var42 = (Option) var26._2();
                if ( var42 instanceof Some )
                {
                    Some var43 = (Some) var42;
                    Either var44 = (Either) var43.value();
                    if ( var44 instanceof Left )
                    {
                        Left var45 = (Left) var44;
                        String end = (String) var45.value();
                        var1 = ( x$3 ) -> {
                            return org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                    (Field) this.endLabelField().apply( end ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ), x$3);
                        }; break label112;
                    }
                }
            }

            var1 = ( ops ) -> {
                return ops;
            };
        }

        IntermediateRepresentation dbHitsRepresentation = this.relationshipTypes.size() <= 1 ? OperatorCodeGenHelperTemplates$.MODULE$.dbHit(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )) :
        OperatorCodeGenHelperTemplates$.MODULE$.dbHits( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.executionEventField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToInteger( this.relationshipTypes.size() ) ));
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                    super.codeGen().setRefAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "longValue", scala.reflect.ManifestFactory..MODULE$.classType(
                            Values.class ), scala.reflect.ManifestFactory..MODULE$.classType( LongValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{(IntermediateRepresentation) var1.apply( countOps )}) ))),
        dbHitsRepresentation, OperatorCodeGenHelperTemplates$.MODULE$.profileRow(
                super.id() ), this.inner().genOperateWithExpressions(), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ))})));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }
}
