package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.InCheckContainer;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator;
import org.neo4j.cypher.internal.runtime.interpreted.profiler.InterpretedProfileInformation;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Some;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class FeedPipeQueryState extends SlottedQueryState
{
    private final InterpretedProfileInformation profileInformation;
    private MorselExecutionContext morsel;

    public FeedPipeQueryState( final QueryContext query, final ExternalCSVResource resources, final AnyValue[] params, final ExpressionCursors cursors,
            final IndexReadSession[] queryIndexes, final AnyValue[] expressionVariables, final QuerySubscriber subscriber,
            final QueryMemoryTracker memoryTracker, final PipeDecorator decorator, final Option<ExecutionContext> initialContext,
            final SingleThreadedLRUCache<Object,InCheckContainer> cachedIn, final boolean lenientCreateRelationship, final boolean prePopulateResults,
            final InputDataStream input, final InterpretedProfileInformation profileInformation, final MorselExecutionContext morsel )
    {
        this.profileInformation = profileInformation;
        this.morsel = morsel;
        super( query, resources, params, cursors, queryIndexes, expressionVariables, subscriber, memoryTracker, decorator, initialContext, cachedIn,
                lenientCreateRelationship, prePopulateResults, input );
    }

    public static MorselExecutionContext $lessinit$greater$default$16()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$16();
    }

    public static InterpretedProfileInformation $lessinit$greater$default$15()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$15();
    }

    public static InputDataStream $lessinit$greater$default$14()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$14();
    }

    public static boolean $lessinit$greater$default$13()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$13();
    }

    public static boolean $lessinit$greater$default$12()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$12();
    }

    public static SingleThreadedLRUCache<Object,InCheckContainer> $lessinit$greater$default$11()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$11();
    }

    public static Option<ExecutionContext> $lessinit$greater$default$10()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$10();
    }

    public static PipeDecorator $lessinit$greater$default$9()
    {
        return FeedPipeQueryState$.MODULE$.$lessinit$greater$default$9();
    }

    public InterpretedProfileInformation profileInformation()
    {
        return this.profileInformation;
    }

    public MorselExecutionContext morsel()
    {
        return this.morsel;
    }

    public void morsel_$eq( final MorselExecutionContext x$1 )
    {
        this.morsel = x$1;
    }

    public FeedPipeQueryState withDecorator( final PipeDecorator decorator )
    {
        return new FeedPipeQueryState( super.query(), super.resources(), super.params(), super.cursors(), super.queryIndexes(), super.expressionVariables(),
                super.subscriber(), super.memoryTracker(), decorator, super.initialContext(), super.cachedIn(), super.lenientCreateRelationship(),
                super.prePopulateResults(), super.input(), this.profileInformation(), this.morsel() );
    }

    public FeedPipeQueryState withInitialContext( final ExecutionContext initialContext )
    {
        return new FeedPipeQueryState( super.query(), super.resources(), super.params(), super.cursors(), super.queryIndexes(), super.expressionVariables(),
                super.subscriber(), super.memoryTracker(), super.decorator(), new Some( initialContext ), super.cachedIn(), super.lenientCreateRelationship(),
                super.prePopulateResults(), super.input(), this.profileInformation(), this.morsel() );
    }

    public FeedPipeQueryState withQueryContext( final QueryContext query )
    {
        return new FeedPipeQueryState( query, super.resources(), super.params(), super.cursors(), super.queryIndexes(), super.expressionVariables(),
                super.subscriber(), super.memoryTracker(), super.decorator(), super.initialContext(), super.cachedIn(), super.lenientCreateRelationship(),
                super.prePopulateResults(), super.input(), this.profileInformation(), this.morsel() );
    }
}
