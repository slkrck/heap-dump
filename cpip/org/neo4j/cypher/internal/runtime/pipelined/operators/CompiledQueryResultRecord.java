package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.result.QueryResult.Record;
import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CompiledQueryResultRecord implements Record
{
    private final AnyValue[] fields;

    public CompiledQueryResultRecord( final AnyValue[] fields )
    {
        this.fields = fields;
    }

    public void release()
    {
        super.release();
    }

    public AnyValue[] fields()
    {
        return this.fields;
    }
}
