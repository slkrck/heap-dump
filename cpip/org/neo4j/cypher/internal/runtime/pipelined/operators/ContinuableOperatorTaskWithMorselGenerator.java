package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Tuple2;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class ContinuableOperatorTaskWithMorselGenerator
{
    public static CompiledStreamingOperator compileOperator( final ContinuableOperatorTaskWithMorselTemplate template, final WorkIdentity workIdentity,
            final Seq<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates,
            final CodeGenerationMode codeGenerationMode )
    {
        return ContinuableOperatorTaskWithMorselGenerator$.MODULE$.compileOperator( var0, var1, var2, var3 );
    }
}
