package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.Scan;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class AllNodeScanOperator implements StreamingOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$offset;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$argumentSize;
    private final WorkIdentity workIdentity;

    public AllNodeScanOperator( final WorkIdentity workIdentity, final int offset, final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$offset = offset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$argumentSize = argumentSize;
        StreamingOperator.$init$( this );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        Object var10000;
        if ( parallelism == 1 )
        {
            var10000 = (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new AllNodeScanOperator.SingleThreadedScanTask[]{
                            new AllNodeScanOperator.SingleThreadedScanTask( this, inputMorsel.nextCopy() )}) ));
        }
        else
        {
            Scan scan = queryContext.transactionalContext().dataRead().allNodesScan();
            ContinuableOperatorTaskWithMorsel[] tasks = new ContinuableOperatorTaskWithMorsel[parallelism];

            for ( int i = 0; i < parallelism; ++i )
            {
                NodeCursor cursor = (NodeCursor) resources.cursorPools().nodeCursorPool().allocateAndTrace();
                MorselExecutionContext rowForTask = inputMorsel.nextCopy();
                tasks[i] = new AllNodeScanOperator.ParallelScanTask( this, rowForTask, scan, cursor, state.morselSize() );
            }

            var10000 = scala.Predef..MODULE$.wrapRefArray( (Object[]) tasks );
        }

        return (IndexedSeq) var10000;
    }

    public class ParallelScanTask implements ContinuableOperatorTaskWithMorsel
    {
        private final MorselExecutionContext inputMorsel;
        private final Scan<NodeCursor> scan;
        private final int batchSizeHint;
        private NodeCursor cursor;
        private boolean _canContinue;
        private boolean deferredRow;

        public ParallelScanTask( final AllNodeScanOperator $outer, final MorselExecutionContext inputMorsel, final Scan<NodeCursor> scan,
                final NodeCursor cursor, final int batchSizeHint )
        {
            this.inputMorsel = inputMorsel;
            this.scan = scan;
            this.cursor = cursor;
            this.batchSizeHint = batchSizeHint;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                ContinuableOperatorTask.$init$( this );
                ContinuableOperatorTaskWithMorsel.$init$( this );
                this._canContinue = true;
                this.deferredRow = false;
                scan.reserveBatch( this.cursor(), batchSizeHint );
                inputMorsel.setToAfterLastRow();
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorsel.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorsel.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorsel.producingWorkUnitEvent$( this );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithMorsel.estimatedHeapUsage$( this );
        }

        public void close( final OperatorCloser operatorCloser, final QueryResources resources )
        {
            ContinuableOperatorTask.close$( this, operatorCloser, resources );
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public NodeCursor cursor()
        {
            return this.cursor;
        }

        public void cursor_$eq( final NodeCursor x$1 )
        {
            this.cursor = x$1;
        }

        public int batchSizeHint()
        {
            return this.batchSizeHint;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$ParallelScanTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "AllNodeScanParallelTask";
        }

        private boolean _canContinue()
        {
            return this._canContinue;
        }

        private void _canContinue_$eq( final boolean x$1 )
        {
            this._canContinue = x$1;
        }

        private boolean deferredRow()
        {
            return this.deferredRow;
        }

        private void deferredRow_$eq( final boolean x$1 )
        {
            this.deferredRow = x$1;
        }

        public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState queryState, final QueryResources resources )
        {
            while ( this.next( queryState, resources ) && outputRow.isValidRow() )
            {
                outputRow.copyFrom( this.inputMorsel(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$ParallelScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$ParallelScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$argumentSize.nReferences() );
                outputRow.setLongAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$ParallelScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$offset,
                        this.cursor().nodeReference() );
                outputRow.moveToNextRow();
            }

            if ( !outputRow.isValidRow() && this._canContinue() )
            {
                this.deferredRow_$eq( true );
            }

            outputRow.finishedWriting();
        }

        private boolean next( final QueryState queryState, final QueryResources resources )
        {
            while ( !this.deferredRow() )
            {
                if ( this.inputMorsel().hasNextRow() )
                {
                    this.inputMorsel().moveToNextRow();
                    return true;
                }

                if ( this.cursor().next() )
                {
                    this.inputMorsel().resetToBeforeFirstRow();
                }
                else if ( !this.scan.reserveBatch( this.cursor(), this.batchSizeHint() ) )
                {
                    this._canContinue_$eq( false );
                    return false;
                }
            }

            this.deferredRow_$eq( false );
            return true;
        }

        public boolean canContinue()
        {
            return this._canContinue();
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.cursor() != null )
            {
                this.cursor().setTracer( event );
            }
        }

        public void closeCursors( final QueryResources resources )
        {
            resources.cursorPools().nodeCursorPool().free( this.cursor() );
            this.cursor_$eq( (NodeCursor) null );
        }
    }

    public class SingleThreadedScanTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private NodeCursor cursor;

        public SingleThreadedScanTask( final AllNodeScanOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$SingleThreadedScanTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "AllNodeScanSerialTask";
        }

        private NodeCursor cursor()
        {
            return this.cursor;
        }

        private void cursor_$eq( final NodeCursor x$1 )
        {
            this.cursor = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            this.cursor_$eq( (NodeCursor) resources.cursorPools().nodeCursorPool().allocateAndTrace() );
            context.transactionalContext().dataRead().allNodesScan( this.cursor() );
            return true;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.cursor().next() )
            {
                outputRow.copyFrom( this.inputMorsel(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$SingleThreadedScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$SingleThreadedScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$argumentSize.nReferences() );
                outputRow.setLongAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$SingleThreadedScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AllNodeScanOperator$$offset,
                        this.cursor().nodeReference() );
                outputRow.moveToNextRow();
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.cursor() != null )
            {
                this.cursor().setTracer( event );
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            resources.cursorPools().nodeCursorPool().free( this.cursor() );
            this.cursor_$eq( (NodeCursor) null );
        }
    }
}
