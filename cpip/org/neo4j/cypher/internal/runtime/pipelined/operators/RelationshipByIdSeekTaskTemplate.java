package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class RelationshipByIdSeekTaskTemplate extends InputLoopTaskTemplate
{
    private final InstanceField cursor;
    private IntermediateExpression relationshipExpression;

    public RelationshipByIdSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int relationshipOffset, final int fromOffset, final int toOffset, final Expression relIdExpr, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.cursor = .
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ));
    }

    public InstanceField cursor()
    {
        return this.cursor;
    }

    public IntermediateExpression relationshipExpression()
    {
        return this.relationshipExpression;
    }

    public void relationshipExpression_$eq( final IntermediateExpression x$1 )
    {
        this.relationshipExpression = x$1;
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.condition(.MODULE$.isNotNull(.MODULE$.loadField(
                    this.cursor() )),.MODULE$.invokeSideEffect(.MODULE$.loadField( this.cursor() ), .
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{event}) ))),super.inner().genSetExecutionEvent( event )})));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.relationshipExpression()}) ));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.freeCursor(.MODULE$.loadField( this.cursor() ),
                    OperatorCodeGenHelperTemplates.RelScanCursorPool$.MODULE$, scala.reflect.ManifestFactory..MODULE$.classType(
                    RelationshipScanCursor.class )),.MODULE$.setField( this.cursor(),.MODULE$.constant( (Object) null ))})));
    }
}
