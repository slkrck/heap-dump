package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.SeekArgs;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.values.AnyValue;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class UndirectedRelationshipByIdSeekOperator extends RelationshipByIdSeekOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$relationship;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$startNode;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$endNode;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$argumentSize;
    private boolean org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection;

    public UndirectedRelationshipByIdSeekOperator( final WorkIdentity workIdentity, final int relationship, final int startNode, final int endNode,
            final SeekArgs relId, final SlotConfiguration.Size argumentSize )
    {
        super( workIdentity, relationship, startNode, endNode, relId, argumentSize );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$relationship = relationship;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$startNode = startNode;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$endNode = endNode;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$argumentSize = argumentSize;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection = true;
    }

    public boolean org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection;
    }

    public void org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection_$eq( final boolean x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection = x$1;
    }

    public String toString()
    {
        return "UndirectedRelationshipByIdTask";
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new RelationshipByIdSeekOperator.RelationshipByIdTask[]{new RelationshipByIdSeekOperator.RelationshipByIdTask( this, inputMorsel )
                {
                    public
                    {
                        if ( $outer == null )
                        {
                            throw null;
                        }
                        else
                        {
                            this.$outer = $outer;
                        }
                    }

                    public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
                    {
                        while ( outputRow.isValidRow() &&
                                (!this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection() ||
                                        this.ids().hasNext()) )
                        {
                            if ( this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection() )
                            {
                                long nextId = org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.NumericHelper..
                                MODULE$.asLongEntityIdPrimitive( (AnyValue) this.ids().next() );
                                if ( nextId >= 0L )
                                {
                                    context.transactionalContext().dataRead().singleRelationship( nextId, this.cursor() );
                                    if ( this.cursor().next() )
                                    {
                                        outputRow.copyFrom( this.inputMorsel(),
                                                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$argumentSize.nLongs(),
                                                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$argumentSize.nReferences() );
                                        outputRow.setLongAt(
                                                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$relationship,
                                                nextId );
                                        outputRow.setLongAt(
                                                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$startNode,
                                                this.cursor().sourceNodeReference() );
                                        outputRow.setLongAt(
                                                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$endNode,
                                                this.cursor().targetNodeReference() );
                                        outputRow.moveToNextRow();
                                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection_$eq(
                                                false );
                                    }
                                }
                            }
                            else
                            {
                                outputRow.copyFrom( this.inputMorsel(),
                                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$argumentSize.nLongs(),
                                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$argumentSize.nReferences() );
                                outputRow.setLongAt(
                                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$relationship,
                                        this.cursor().relationshipReference() );
                                outputRow.setLongAt(
                                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$startNode,
                                        this.cursor().targetNodeReference() );
                                outputRow.setLongAt(
                                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$endNode,
                                        this.cursor().sourceNodeReference() );
                                outputRow.moveToNextRow();
                                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$UndirectedRelationshipByIdSeekOperator$$forwardDirection_$eq(
                                        true );
                            }
                        }
                    }
                }}) ));
    }
}
