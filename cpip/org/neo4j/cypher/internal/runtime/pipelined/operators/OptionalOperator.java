package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStream;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.EndOfEmptyStream;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.OptionalArgumentStateBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Function1;
import scala.MatchError;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class OptionalOperator implements Operator
{
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$argumentSize;
    private final WorkIdentity workIdentity;
    private final int argumentStateMapId;
    private final Seq<Function1<ExecutionContext,BoxedUnit>>
            org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$setNullableSlotToNullFunctions;

    public OptionalOperator( final WorkIdentity workIdentity, final int argumentStateMapId, final int argumentSlotOffset, final Seq<Slot> nullableSlots,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.argumentStateMapId = argumentStateMapId;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$argumentSize = argumentSize;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$setNullableSlotToNullFunctions = (Seq) nullableSlots.map( ( x0$1 ) -> {
            Function1 var1;
            if ( x0$1 instanceof LongSlot )
            {
                LongSlot var3 = (LongSlot) x0$1;
                int offsetx = var3.offset();
                var1 = ( context ) -> {
                    $anonfun$setNullableSlotToNullFunctions$2( offsetx, context );
                    return BoxedUnit.UNIT;
                };
            }
            else
            {
                if ( !(x0$1 instanceof RefSlot) )
                {
                    throw new MatchError( x0$1 );
                }

                RefSlot var5 = (RefSlot) x0$1;
                int offset = var5.offset();
                var1 = ( context ) -> {
                    $anonfun$setNullableSlotToNullFunctions$3( offset, context );
                    return BoxedUnit.UNIT;
                };
            }

            return var1;
        },.MODULE$.canBuildFrom());
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public Seq<Function1<ExecutionContext,BoxedUnit>> org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$setNullableSlotToNullFunctions()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$setNullableSlotToNullFunctions;
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        argumentStateCreator.createArgumentStateMap( this.argumentStateMapId, new OptionalArgumentStateBuffer.Factory( stateFactory ) );
        return new OptionalOperator.OptionalOperatorState( this );
    }

    public class OTask implements OptionalOperatorTask
    {
        private final MorselData morselData;
        private final Iterator<MorselExecutionContext> morselIterator;
        private boolean consumedArgumentStream;
        private MorselExecutionContext currentMorsel;

        public OTask( final OptionalOperator $outer, final MorselData morselData )
        {
            this.morselData = morselData;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                ContinuableOperatorTask.$init$( this );
                OptionalOperatorTask.$init$( this );
                this.morselIterator = morselData.morsels().iterator();
                this.consumedArgumentStream = false;
            }
        }

        public long estimatedHeapUsage()
        {
            return OptionalOperatorTask.estimatedHeapUsage$( this );
        }

        public void close( final OperatorCloser operatorCloser, final QueryResources resources )
        {
            ContinuableOperatorTask.close$( this, operatorCloser, resources );
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public MorselData morselData()
        {
            return this.morselData;
        }

        private Iterator<MorselExecutionContext> morselIterator()
        {
            return this.morselIterator;
        }

        private boolean consumedArgumentStream()
        {
            return this.consumedArgumentStream;
        }

        private void consumedArgumentStream_$eq( final boolean x$1 )
        {
            this.consumedArgumentStream = x$1;
        }

        private MorselExecutionContext currentMorsel()
        {
            return this.currentMorsel;
        }

        private void currentMorsel_$eq( final MorselExecutionContext x$1 )
        {
            this.currentMorsel = x$1;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$OTask$$$outer().workIdentity();
        }

        public void operate( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            while ( output.isValidRow() && this.canContinue() )
            {
                if ( this.currentMorsel() == null || !this.currentMorsel().isValidRow() )
                {
                    if ( this.morselIterator().hasNext() )
                    {
                        this.currentMorsel_$eq( (MorselExecutionContext) this.morselIterator().next() );
                    }
                    else
                    {
                        this.currentMorsel_$eq( (MorselExecutionContext) null );
                    }
                }

                if ( this.currentMorsel() != null )
                {
                    while ( output.isValidRow() && this.currentMorsel().isValidRow() )
                    {
                        output.copyFrom( this.currentMorsel() );
                        output.moveToNextRow();
                        this.currentMorsel().moveToNextRow();
                    }
                }
                else if ( !this.consumedArgumentStream() )
                {
                    ArgumentStream var6 = this.morselData().argumentStream();
                    BoxedUnit var5;
                    if ( var6 instanceof EndOfEmptyStream )
                    {
                        EndOfEmptyStream var7 = (EndOfEmptyStream) var6;
                        MorselExecutionContext viewOfArgumentRow = var7.viewOfArgumentRow();
                        output.copyFrom( viewOfArgumentRow,
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$argumentSize.nLongs(),
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$argumentSize.nReferences() );
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$$setNullableSlotToNullFunctions().foreach(
                                ( f ) -> {
                                    $anonfun$operate$1( output, f );
                                    return BoxedUnit.UNIT;
                                } );
                        output.moveToNextRow();
                        var5 = BoxedUnit.UNIT;
                    }
                    else
                    {
                        var5 = BoxedUnit.UNIT;
                    }

                    this.consumedArgumentStream_$eq( true );
                }
            }

            output.finishedWriting();
        }

        public void closeCursors( final QueryResources resources )
        {
        }

        public boolean canContinue()
        {
            return this.currentMorsel() != null && this.currentMorsel().isValidRow() || this.morselIterator().hasNext() || !this.consumedArgumentStream();
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            operatorCloser.closeData( this.morselData() );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return false;
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return null;
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
        }
    }

    public class OptionalOperatorState implements OperatorState
    {
        public OptionalOperatorState( final OptionalOperator $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
                final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
        {
            MorselData input = (MorselData) operatorInput.takeData();
            return input != null ? (IndexedSeq) scala.package..MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new OptionalOperator.OTask[]{
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$OptionalOperatorState$$$outer().new OTask(
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$OptionalOperator$OptionalOperatorState$$$outer(), input )}) )) :null;
        }
    }
}
