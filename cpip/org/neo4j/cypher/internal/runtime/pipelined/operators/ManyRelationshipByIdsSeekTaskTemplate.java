package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class ManyRelationshipByIdsSeekTaskTemplate extends RelationshipByIdSeekTaskTemplate
{
    private final Expression relIdsExpr;
    private final InstanceField idCursor;

    public ManyRelationshipByIdsSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int relationshipOffset, final int fromOffset, final int toOffset, final Expression relIdsExpr, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, relationshipOffset, fromOffset, toOffset, relIdsExpr, argumentSize, codeGen );
        this.relIdsExpr = relIdsExpr;
        this.idCursor = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ));
    }

    public InstanceField idCursor()
    {
        return this.idCursor;
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        this.relationshipExpression_$eq( (IntermediateExpression) super.codeGen().intermediateCompileExpression( this.relIdsExpr ).getOrElse( () -> {
            throw new CantCompileQueryException(
                    (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( this.relIdsExpr ).toString() );
        } ) );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.cursor(),
                    OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_REL_SCAN_CURSOR() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
            this.idCursor(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "apply", scala.reflect.ManifestFactory..MODULE$.classType(
            IteratorCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.relationshipExpression(),
                            ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType(
            ListValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
            OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.idCursor() ),
            scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loadField( this.canContinue() )})));
    }
}
