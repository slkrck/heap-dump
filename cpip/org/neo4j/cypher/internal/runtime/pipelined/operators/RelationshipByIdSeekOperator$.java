package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.logical.plans.ManySeekableArgs;
import org.neo4j.cypher.internal.logical.plans.SeekableArgs;
import org.neo4j.cypher.internal.logical.plans.SingleSeekableArg;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.NumericHelper;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.ListLiteral;
import org.neo4j.cypher.internal.v4_0.util.Many;
import org.neo4j.cypher.internal.v4_0.util.One;
import org.neo4j.cypher.internal.v4_0.util.ZeroOneOrMany;
import org.neo4j.cypher.internal.v4_0.util.ZeroOneOrMany.;
import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.Tuple2.mcZZ.sp;

public final class RelationshipByIdSeekOperator$
{
    public static RelationshipByIdSeekOperator$ MODULE$;

    static
    {
        new RelationshipByIdSeekOperator$();
    }

    private final Method asIdMethod;

    private RelationshipByIdSeekOperator$()
    {
        MODULE$ = this;
        this.asIdMethod = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "asLongEntityIdPrimitive", scala.reflect.ManifestFactory..MODULE$.classType( NumericHelper.class ), scala.reflect.ManifestFactory..
        MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
    }

    private static final RelationshipByIdSeekTaskTemplate factory$1( final Expression expr, final boolean isSingle, final boolean isDirected$1,
            final OperatorTaskTemplate inner$1, final int id$1, final DelegateOperatorTaskTemplate innermost$1, final int relationshipOffset$1,
            final int fromOffset$1, final int toOffset$1, final SlotConfiguration.Size argumentSize$1, final OperatorExpressionCompiler codeGen$1 )
    {
        sp var12 = new sp( isDirected$1, isSingle );
        Object var11;
        if ( var12 != null )
        {
            boolean var13 = var12._1$mcZ$sp();
            boolean var14 = var12._2$mcZ$sp();
            if ( var13 && var14 )
            {
                var11 = new SingleDirectedRelationshipByIdSeekTaskTemplate( inner$1, id$1, innermost$1, relationshipOffset$1, fromOffset$1, toOffset$1, expr,
                        argumentSize$1, codeGen$1 );
                return (RelationshipByIdSeekTaskTemplate) var11;
            }
        }

        if ( var12 != null )
        {
            boolean var15 = var12._1$mcZ$sp();
            boolean var16 = var12._2$mcZ$sp();
            if ( var15 && !var16 )
            {
                var11 = new ManyDirectedRelationshipByIdsSeekTaskTemplate( inner$1, id$1, innermost$1, relationshipOffset$1, fromOffset$1, toOffset$1, expr,
                        argumentSize$1, codeGen$1 );
                return (RelationshipByIdSeekTaskTemplate) var11;
            }
        }

        if ( var12 != null )
        {
            boolean var17 = var12._1$mcZ$sp();
            boolean var18 = var12._2$mcZ$sp();
            if ( !var17 && var18 )
            {
                var11 = new SingleUndirectedRelationshipByIdSeekTaskTemplate( inner$1, id$1, innermost$1, relationshipOffset$1, fromOffset$1, toOffset$1, expr,
                        argumentSize$1, codeGen$1 );
                return (RelationshipByIdSeekTaskTemplate) var11;
            }
        }

        if ( var12 == null )
        {
            throw new MatchError( var12 );
        }
        else
        {
            boolean var19 = var12._1$mcZ$sp();
            boolean var20 = var12._2$mcZ$sp();
            if ( var19 || var20 )
            {
                throw new MatchError( var12 );
            }
            else
            {
                var11 = new ManyUndirectedRelationshipByIdsSeekTaskTemplate( inner$1, id$1, innermost$1, relationshipOffset$1, fromOffset$1, toOffset$1, expr,
                        argumentSize$1, codeGen$1 );
                return (RelationshipByIdSeekTaskTemplate) var11;
            }
        }
    }

    public Method asIdMethod()
    {
        return this.asIdMethod;
    }

    public OperatorTaskTemplate taskTemplate( final boolean isDirected, final OperatorTaskTemplate inner, final int id,
            final DelegateOperatorTaskTemplate innermost, final int relationshipOffset, final int fromOffset, final int toOffset, final SeekableArgs relIds,
            final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        Object var11;
        if ( relIds instanceof SingleSeekableArg )
        {
            SingleSeekableArg var15 = (SingleSeekableArg) relIds;
            Expression expr = var15.expr();
            var11 = factory$1( expr, true, isDirected, inner, id, innermost, relationshipOffset, fromOffset, toOffset, argumentSize, codeGen );
        }
        else
        {
            if ( !(relIds instanceof ManySeekableArgs) )
            {
                throw new MatchError( relIds );
            }

            ManySeekableArgs var17 = (ManySeekableArgs) relIds;
            Expression expr = var17.expr();
            Object var12;
            if ( expr instanceof ListLiteral )
            {
                ListLiteral var20 = (ListLiteral) expr;
                ZeroOneOrMany var21 = .MODULE$.apply( var20.expressions() );
                Object var13;
                if ( org.neo4j.cypher.internal.v4_0.util.Zero..MODULE$.equals( var21 )){
                var13 = OperatorTaskTemplate$.MODULE$.empty( id );
            } else if ( var21 instanceof One )
            {
                One var22 = (One) var21;
                Expression value = (Expression) var22.value();
                var13 = factory$1( value, true, isDirected, inner, id, innermost, relationshipOffset, fromOffset, toOffset, argumentSize, codeGen );
            }
            else
            {
                if ( !(var21 instanceof Many) )
                {
                    throw new MatchError( var21 );
                }

                var13 = factory$1( expr, false, isDirected, inner, id, innermost, relationshipOffset, fromOffset, toOffset, argumentSize, codeGen );
            }

                var12 = var13;
            }
            else
            {
                var12 = factory$1( expr, false, isDirected, inner, id, innermost, relationshipOffset, fromOffset, toOffset, argumentSize, codeGen );
            }

            var11 = var12;
        }

        return (OperatorTaskTemplate) var11;
    }
}
