package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData;
import scala.collection.TraversableOnce;
import scala.collection.IndexedSeq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface OptionalOperatorTask extends ContinuableOperatorTask
{
    static void $init$( final OptionalOperatorTask $this )
    {
    }

    MorselData morselData();

    default long estimatedHeapUsage()
    {
        return BoxesRunTime.unboxToLong( ((TraversableOnce) this.morselData().morsels().map( ( x$1 ) -> {
            return BoxesRunTime.boxToLong( $anonfun$estimatedHeapUsage$1( x$1 ) );
        },.MODULE$.canBuildFrom()) ).sum( scala.math.Numeric.LongIsIntegral..MODULE$));
    }
}
