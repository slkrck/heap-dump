package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.NodeLabelIndexCursor;
import org.neo4j.util.Preconditions;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.Values;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Serializable;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.util.Either;
import scala.util.Left;
import scala.util.Right;

@JavaDocToJava
public class NodeCountFromCountStoreOperatorTemplate extends InputLoopTaskTemplate
{
    private final int offset;
    private final SlotConfiguration.Size argumentSize;
    private final int wildCards;
    private final List<Object> knownLabels;
    private final InstanceField nodeLabelCursorField;
    private final Map<String,InstanceField> labelFields;

    public NodeCountFromCountStoreOperatorTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int offset, final List<Option<Either<String,Object>>> labels, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.offset = offset;
        this.argumentSize = argumentSize;
        this.wildCards = labels.count( ( x$2 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$wildCards$2( x$2 ) );
        } );
        this.knownLabels = (List) ((List) labels.flatten( ( xo ) -> {
            return scala.Option..MODULE$.option2Iterable( xo );
        } )).collect( new Serializable( (NodeCountFromCountStoreOperatorTemplate) null )
        {
            public static final long serialVersionUID = 0L;

            public final <A1 extends Either<String,Object>, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x1 instanceof Right )
                {
                    Right var5 = (Right) x1;
                    int token = BoxesRunTime.unboxToInt( var5.value() );
                    var3 = BoxesRunTime.boxToInteger( token );
                }
                else
                {
                    var3 = var2.apply( x1 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final Either<String,Object> x1 )
            {
                boolean var2;
                if ( x1 instanceof Right )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        }, scala.collection.immutable.List..MODULE$.canBuildFrom());
        Preconditions.checkState( this.knownLabels().forall( ( x$3 ) -> {
            return x$3 >= 0;
        } ), "Expect all label tokens to be positive" );
        this.nodeLabelCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ));
        this.labelFields = ((TraversableOnce) ((List) labels.flatten( ( xo ) -> {
            return scala.Option..MODULE$.option2Iterable( xo );
        } )).collect( new Serializable( this )
        {
            public static final long serialVersionUID = 0L;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                }
            }

            public final <A1 extends Either<String,Object>, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x2 instanceof Left )
                {
                    Left var5 = (Left) x2;
                    String labelName = (String) var5.value();
                    var3 = scala.Predef.ArrowAssoc..
                    MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( labelName ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.field(
                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperatorTemplate$$super$codeGen().namer().variableName(
                                    labelName ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN(), scala.reflect.ManifestFactory..MODULE$.Int()));
                }
                else
                {
                    var3 = var2.apply( x2 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final Either<String,Object> x2 )
            {
                boolean var2;
                if ( x2 instanceof Left )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        }, scala.collection.immutable.List..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
    }

    public OperatorTaskTemplate inner()
    {
        return super.inner();
    }

    private int wildCards()
    {
        return this.wildCards;
    }

    private List<Object> knownLabels()
    {
        return this.knownLabels;
    }

    private InstanceField nodeLabelCursorField()
    {
        return this.nodeLabelCursorField;
    }

    private Map<String,InstanceField> labelFields()
    {
        return this.labelFields;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq) this.labelFields().values().toSeq().$colon$plus( this.nodeLabelCursorField(),.MODULE$.canBuildFrom());
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.empty();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        Seq setLabelIds = (Seq) this.labelFields().toSeq().map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                String name = (String) x0$1._1();
                InstanceField field = (InstanceField) x0$1._2();
                IntermediateRepresentation var1 = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( field ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN()),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( field, OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelId( name ) ));
                return var1;
            }
            else
            {
                throw new MatchError( x0$1 );
            }
        },.MODULE$.canBuildFrom());
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( (Seq) setLabelIds.$plus$plus(.MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )}))), .MODULE$.canBuildFrom()));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        String countVar = super.codeGen().namer().nextVariableName();
        IntermediateRepresentation unknownLabelOps = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( (Seq) this.labelFields().values().toSeq().map( ( field ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ifElse( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( field ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.assign( countVar, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( 0L ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( countVar,
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.multiply( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                            countVar ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeCountByCountStore", scala.reflect.ManifestFactory..MODULE$.classType(
                    DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( field )}))))),
            OperatorCodeGenHelperTemplates$.MODULE$.dbHit( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() ))}))))
            ;
        },.MODULE$.canBuildFrom() ));
        IntermediateRepresentation knownLabelOps = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( (Seq) ((SeqLike) this.knownLabels().map( ( token ) -> {
            return $anonfun$genInnerLoop$2( countVar, BoxesRunTime.unboxToInt( token ) );
        }, scala.collection.immutable.List..MODULE$.canBuildFrom()) ).$colon$plus(
                OperatorCodeGenHelperTemplates$.MODULE$.dbHits( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.executionEventField() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                BoxesRunTime.boxToInteger( this.knownLabels().size() ) )),scala.collection.immutable.List..MODULE$.canBuildFrom()));
        IntermediateRepresentation var10000;
        if ( this.wildCards() > 0 )
        {
            String wildCardCount = super.codeGen().namer().nextVariableName();
            IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.block( (Seq) scala.runtime.RichInt..MODULE$.to$extension0( scala.Predef..MODULE$.intWrapper( 1 ), this.wildCards()).map( ( x$4 ) -> {
            return $anonfun$genInnerLoop$3( countVar, wildCardCount, BoxesRunTime.unboxToInt( x$4 ) );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom()));
            var10000 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()), wildCardCount,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeCountByCountStore", scala.reflect.ManifestFactory..MODULE$.classType(
                DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( org.neo4j.cypher.internal.v4_0.util.NameId..MODULE$.WILDCARD() ))})))),
            OperatorCodeGenHelperTemplates$.MODULE$.dbHit( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )),
            ops})));
        }
        else
        {
            var10000 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
        }

        IntermediateRepresentation wildCardOps = var10000;
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()), countVar,
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( 1L ) )),
        unknownLabelOps, knownLabelOps, wildCardOps, super.codeGen().copyFromInput( this.argumentSize.nLongs(),
                this.argumentSize.nReferences() ), super.codeGen().setRefAt( this.offset,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                "longValue", scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( LongValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( countVar )})))),
        OperatorCodeGenHelperTemplates$.MODULE$.profileRow(
                super.id() ), this.inner().genOperateWithExpressions(), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ))})));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }
}
