package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class MorselBufferOutputOperator$ extends AbstractFunction3<BufferId,Id,Object,MorselBufferOutputOperator> implements Serializable
{
    public static MorselBufferOutputOperator$ MODULE$;

    static
    {
        new MorselBufferOutputOperator$();
    }

    private MorselBufferOutputOperator$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselBufferOutputOperator";
    }

    public MorselBufferOutputOperator apply( final int bufferId, final int nextPipelineHeadPlanId, final boolean nextPipelineFused )
    {
        return new MorselBufferOutputOperator( bufferId, nextPipelineHeadPlanId, nextPipelineFused );
    }

    public Option<Tuple3<BufferId,Id,Object>> unapply( final MorselBufferOutputOperator x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( new BufferId( x$0.bufferId() ), new Id( x$0.nextPipelineHeadPlanId() ), BoxesRunTime.boxToBoolean( x$0.nextPipelineFused() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
