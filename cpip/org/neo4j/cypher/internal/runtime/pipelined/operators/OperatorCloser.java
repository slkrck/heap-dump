package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface OperatorCloser
{
    void closeMorsel( final MorselExecutionContext morsel );

    <DATA> void closeData( final DATA data );

    void closeAccumulator( final ArgumentStateMap.MorselAccumulator<?> accumulator );

    void closeMorselAndAccumulatorTask( final MorselExecutionContext morsel, final ArgumentStateMap.MorselAccumulator<?> accumulator );

    boolean filterCancelledArguments( final MorselExecutionContext morsel );

    boolean filterCancelledArguments( final ArgumentStateMap.MorselAccumulator<?> accumulator );

    boolean filterCancelledArguments( final MorselExecutionContext morsel, final ArgumentStateMap.MorselAccumulator<?> accumulator );
}
