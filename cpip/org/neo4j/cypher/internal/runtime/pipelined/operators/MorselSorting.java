package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Comparator;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.slotted.ColumnOrder;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class MorselSorting
{
    public static Comparator<MorselExecutionContext> createMorselComparator( final ColumnOrder order )
    {
        return MorselSorting$.MODULE$.createMorselComparator( var0 );
    }

    public static void createSortedMorselData( final MorselExecutionContext inputRow, final Integer[] outputToInputIndexes )
    {
        MorselSorting$.MODULE$.createSortedMorselData( var0, var1 );
    }

    public static Integer[] createMorselIndexesArray( final MorselExecutionContext row )
    {
        return MorselSorting$.MODULE$.createMorselIndexesArray( var0 );
    }

    public static Comparator<Integer> compareMorselIndexesByColumnOrder( final MorselExecutionContext row, final ColumnOrder order )
    {
        return MorselSorting$.MODULE$.compareMorselIndexesByColumnOrder( var0, var1 );
    }

    public static Comparator<MorselExecutionContext> createComparator( final Seq<ColumnOrder> orderBy )
    {
        return MorselSorting$.MODULE$.createComparator( var0 );
    }
}
