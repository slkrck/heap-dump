package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.storable.Value;
import scala.Function0;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SingleExactSeekQueryNodeIndexSeekTaskTemplate extends SingleQueryNodeIndexSeekTaskTemplate
{
    private final SlottedIndexedProperty property;
    private final Function0<IntermediateExpression> generateSeekValue;
    private final LocalVariable seekValueVariable;
    private IntermediateExpression seekValue;

    public SingleExactSeekQueryNodeIndexSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int offset, final SlottedIndexedProperty property, final Function0<IntermediateExpression> generateSeekValue, final int queryIndexId,
            final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, offset, property, IndexOrder.NONE, false, queryIndexId, argumentSize, codeGen );
        this.property = property;
        this.generateSeekValue = generateSeekValue;
        this.seekValueVariable = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( super.codeGen().namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                (Object) null ), scala.reflect.ManifestFactory..MODULE$.classType( Value.class ));
    }

    private IntermediateExpression seekValue()
    {
        return this.seekValue;
    }

    private void seekValue_$eq( final IntermediateExpression x$1 )
    {
        this.seekValue = x$1;
    }

    private LocalVariable seekValueVariable()
    {
        return this.seekValueVariable;
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V(), this.seekValueVariable()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.seekValue()}) ));
    }

    public IntermediateRepresentation getPropertyValue()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.seekValueVariable() );
    }

    public IntermediateRepresentation beginInnerLoop()
    {
        IntermediateExpression value = (IntermediateExpression) this.generateSeekValue.apply();
        this.seekValue_$eq( value.copy( OperatorCodeGenHelperTemplates$.MODULE$.asStorableValue(
                ExpressionCompiler$.MODULE$.nullCheckIfRequired( value, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ), value.copy$default$2(),
                value.copy$default$3(), value.copy$default$4(), value.copy$default$5() ) );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( this.seekValueVariable(), this.seekValue().ir() );
    }

    public IntermediateRepresentation isPredicatePossible()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.and( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                this.seekValueVariable() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.not( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNaN( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                this.seekValueVariable() ))));
    }

    public IntermediateRepresentation predicate()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.exactSeek( this.property.propertyKeyId(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                this.seekValueVariable() ));
    }
}
