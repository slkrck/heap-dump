package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.TypeReference;
import org.neo4j.codegen.api.BooleanAnd;
import org.neo4j.codegen.api.ExtendClass;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.MethodDeclaration;
import org.neo4j.codegen.api.Parameter;
import org.neo4j.cypher.internal.logical.plans.VariablePredicate;
import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.VariablePredicates$;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledHelpers;
import org.neo4j.cypher.internal.runtime.compiled.expressions.DefaultExpressionCompiler;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPools;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.exceptions.InternalException;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.BufferLike;
import scala.collection.mutable.ArrayBuffer.;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class VarExpandOperatorTaskTemplate extends InputLoopTaskTemplate
{
    public final Slot org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$fromSlot;
    public final Slot org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$toSlot;
    private final DelegateOperatorTaskTemplate innermost;
    private final int relOffset;
    private final SemanticDirection dir;
    private final int[] types;
    private final String[] missingTypes;
    private final int minLength;
    private final int maxLength;
    private final boolean shouldExpandAll;
    private final int tempNodeOffset;
    private final int tempRelOffset;
    private final Option<VariablePredicate> maybeNodeVariablePredicate;
    private final Option<VariablePredicate> maybeRelVariablePredicate;
    private final InstanceField typeField;
    private final InstanceField missingTypeField;
    private final InstanceField varExpandCursorField;
    private final int toOffset;
    private final boolean projectBackwards;
    private Option<IntermediateExpression> startNodePredicate;
    private Option<IntermediateExpression> nodePredicate;
    private Option<IntermediateExpression> relPredicate;

    public VarExpandOperatorTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost, final boolean isHead,
            final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir, final SemanticDirection projectedDir, final int[] types,
            final String[] missingTypes, final int minLength, final int maxLength, final boolean shouldExpandAll, final int tempNodeOffset,
            final int tempRelOffset, final Option<VariablePredicate> maybeNodeVariablePredicate, final Option<VariablePredicate> maybeRelVariablePredicate,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, isHead );
        this.innermost = innermost;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$toSlot = toSlot;
        this.dir = dir;
        this.types = types;
        this.missingTypes = missingTypes;
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.shouldExpandAll = shouldExpandAll;
        this.tempNodeOffset = tempNodeOffset;
        this.tempRelOffset = tempRelOffset;
        this.maybeNodeVariablePredicate = maybeNodeVariablePredicate;
        this.maybeRelVariablePredicate = maybeRelVariablePredicate;
        this.typeField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 4 )).append( super.codeGen().namer().nextVariableName() ).append( "type" ).toString(),
                (new ofInt( scala.Predef..MODULE$.intArrayOps( types )) ).isEmpty() && (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) missingTypes ))).
        isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ) :org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.arrayOf( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new ofInt( scala.Predef..MODULE$.intArrayOps( types )) ).map( ( value ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( value );
        }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )))),scala.reflect.ManifestFactory..
        MODULE$.Int()),scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()));
        this.missingTypeField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 11 )).append( super.codeGen().namer().nextVariableName() ).append( "missingType" ).toString(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) missingTypes )) ).map( ( value ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( value );
        }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )))),scala.reflect.ManifestFactory..
        MODULE$.classType( String.class )),scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( String.class )));
        this.varExpandCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 15 )).append( super.codeGen().namer().nextVariableName() ).append( "varExpandCursor" ).toString(),
                scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ));
        this.toOffset = toSlot.offset();
        this.projectBackwards = org.neo4j.cypher.internal.runtime.interpreted.pipes.VarLengthExpandPipe..MODULE$.projectBackwards( dir, projectedDir );
    }

    private InstanceField typeField()
    {
        return this.typeField;
    }

    private InstanceField missingTypeField()
    {
        return this.missingTypeField;
    }

    private InstanceField varExpandCursorField()
    {
        return this.varExpandCursorField;
    }

    private int toOffset()
    {
        return this.toOffset;
    }

    private boolean projectBackwards()
    {
        return this.projectBackwards;
    }

    private Option<IntermediateExpression> startNodePredicate()
    {
        return this.startNodePredicate;
    }

    private void startNodePredicate_$eq( final Option<IntermediateExpression> x$1 )
    {
        this.startNodePredicate = x$1;
    }

    private Option<IntermediateExpression> nodePredicate()
    {
        return this.nodePredicate;
    }

    private void nodePredicate_$eq( final Option<IntermediateExpression> x$1 )
    {
        this.nodePredicate = x$1;
    }

    private Option<IntermediateExpression> relPredicate()
    {
        return this.relPredicate;
    }

    private void relPredicate_$eq( final Option<IntermediateExpression> x$1 )
    {
        this.relPredicate = x$1;
    }

    public final String scopeId()
    {
        return (new StringBuilder( 9 )).append( "varExpand" ).append( super.id() ).toString();
    }

    public Seq<Field> genMoreFields()
    {
        ArrayBuffer localFields = (ArrayBuffer).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new InstanceField[]{this.typeField(), this.varExpandCursorField()}) ));
        if ( (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.missingTypes )) ).nonEmpty()){
        localFields.$plus$eq( this.missingTypeField() );
    } else{
        BoxedUnit var10000 = BoxedUnit.UNIT;
    }

        return ((BufferLike) localFields.$plus$plus( (GenTraversableOnce) this.nodePredicate().map( ( x$1 ) -> {
            return x$1.fields();
        } ).getOrElse( () -> {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        } ),.MODULE$.canBuildFrom())).$plus$plus( (GenTraversableOnce) this.relPredicate().map( ( x$2 ) -> {
        return x$2.fields();
    } ).getOrElse( () -> {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    } ) );
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return scala.Option..MODULE$.option2Iterable( this.startNodePredicate() ).toSeq();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        String resultBoolean = super.codeGen().namer().nextVariableName();
        String fromNode = (new StringBuilder( 8 )).append( super.codeGen().namer().nextVariableName() ).append( "fromNode" ).toString();
        String toNode = (new StringBuilder( 6 )).append( super.codeGen().namer().nextVariableName() ).append( "toNode" ).toString();
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Boolean()), resultBoolean,
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.declareAndAssign( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()),
        fromNode, this.getNodeIdFromSlot( this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$fromSlot )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.declareAndAssign( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()),
        toNode, this.shouldExpandAll ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ) :
        this.getNodeIdFromSlot( this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$toSlot )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.condition( this.generateStartNodePredicate$1( fromNode, toNode ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{this.loadTypes(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                        this.varExpandCursorField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.newInstance( this.createInnerClass( this.dir ),
                        scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( fromNode ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( toNode ),
                OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_CURSOR(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                BoxesRunTime.boxToBoolean( this.projectBackwards() ) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loadField( this.typeField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToInteger( this.minLength ) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToInteger( this.maxLength ) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() ), OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(), OperatorCodeGenHelperTemplates$.MODULE$.PARAMS(), OperatorCodeGenHelperTemplates$.MODULE$.EXPRESSION_CURSORS(), OperatorCodeGenHelperTemplates$.MODULE$.EXPRESSION_VARIABLES()})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.varExpandCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "enterWorkUnit", scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( CursorPools.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL()}) )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.varExpandCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )}))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
            OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.varExpandCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( resultBoolean, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( resultBoolean )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                super.codeGen().copyFromInput( Math.min( super.codeGen().inputSlotConfiguration().numberOfLongs(), super.codeGen().slots().numberOfLongs() ),
                        Math.min( super.codeGen().inputSlotConfiguration().numberOfReferences(), super.codeGen().slots().numberOfReferences() ) ),
                super.codeGen().setRefAt( this.relOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.varExpandCursorField() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationships", scala.reflect.ManifestFactory..MODULE$.classType(
                VarExpandCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        this.shouldExpandAll ? super.codeGen().setLongAt( this.toOffset(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.varExpandCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "toNode", scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))) :org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.noop(), super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.varExpandCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class )))),
        this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.varExpandCursorField() )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.varExpandCursorField() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "free", scala.reflect.ManifestFactory..MODULE$.classType(
            VarExpandCursor.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( CursorPools.class )),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL()}) )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.varExpandCursorField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))}))));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.varExpandCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.varExpandCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )})))),
        super.inner().genSetExecutionEvent( event )})));
    }

    private IntermediateRepresentation generatePredicate()
    {
        Tuple2 var2 = new Tuple2( this.generateNodePredicate(), this.generateRelationshipPredicate() );
        Object var1;
        if ( var2 != null )
        {
            Option var3 = (Option) var2._1();
            Option var4 = (Option) var2._2();
            if ( var3 instanceof Some )
            {
                Some var5 = (Some) var3;
                IntermediateRepresentation np = (IntermediateRepresentation) var5.value();
                if ( var4 instanceof Some )
                {
                    Some var7 = (Some) var4;
                    IntermediateRepresentation rp = (IntermediateRepresentation) var7.value();
                    var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( np, rp );
                    return (IntermediateRepresentation) var1;
                }
            }
        }

        if ( var2 != null )
        {
            Option var9 = (Option) var2._1();
            Option var10 = (Option) var2._2();
            if ( var9 instanceof Some )
            {
                Some var11 = (Some) var9;
                IntermediateRepresentation np = (IntermediateRepresentation) var11.value();
                if ( scala.None..MODULE$.equals( var10 )){
                var1 = np;
                return (IntermediateRepresentation) var1;
            }
            }
        }

        if ( var2 != null )
        {
            Option var13 = (Option) var2._1();
            Option var14 = (Option) var2._2();
            if ( scala.None..MODULE$.equals( var13 ) && var14 instanceof Some){
            Some var15 = (Some) var14;
            IntermediateRepresentation rp = (IntermediateRepresentation) var15.value();
            var1 = rp;
            return (IntermediateRepresentation) var1;
        }
        }

        if ( var2 == null )
        {
            throw new MatchError( var2 );
        }
        else
        {
            Option var17 = (Option) var2._1();
            Option var18 = (Option) var2._2();
            if ( !scala.None..MODULE$.equals( var17 ) || !scala.None..MODULE$.equals( var18 )){
            throw new MatchError( var2 );
        } else{
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) );
            return (IntermediateRepresentation) var1;
        }
        }
    }

    private Option<IntermediateRepresentation> generateNodePredicate()
    {
        return (Option) (this.tempNodeOffset == VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() ? scala.None..MODULE$ :this.nodePredicate().map( ( pred ) -> {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.oneTime(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arraySet( OperatorCodeGenHelperTemplates$.MODULE$.EXPRESSION_VARIABLES(),
                        this.tempNodeOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeById",
                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                NodeValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( "selectionCursor" ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "otherNodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))}))))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), ExpressionCompiler$.MODULE$.nullCheckIfRequired( pred,
                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ))})));
    } ));
    }

    private Option<IntermediateRepresentation> generateRelationshipPredicate()
    {
        return (Option) (this.tempRelOffset == VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() ? scala.None..MODULE$ :this.relPredicate().map( ( pred ) -> {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.oneTime(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arraySet( OperatorCodeGenHelperTemplates$.MODULE$.EXPRESSION_VARIABLES(),
                        this.tempRelOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationshipFromCursor",
                        scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( "selectionCursor" )}))))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), ExpressionCompiler$.MODULE$.nullCheckIfRequired( pred,
                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ))})));
    } ));
    }

    private ExtendClass createInnerClass( final SemanticDirection dir )
    {
        DefaultExpressionCompiler newScopeExpressionCompiler = new DefaultExpressionCompiler( this )
        {
            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                }
            }

            public IntermediateRepresentation getLongAt( final int offset )
            {
                return this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$fromSlot.isLongSlot() &&
                               offset == this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$fromSlot.offset()
                       ? org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.self(), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "fromNode", scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ), scala.reflect.ManifestFactory..
                MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )) :
                (this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$toSlot.isLongSlot() &&
                         offset == this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$toSlot.offset()
                 ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.self(), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "targetToNode", scala.reflect.ManifestFactory..MODULE$.classType( VarExpandCursor.class ), scala.reflect.ManifestFactory..
                MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )) :super.getLongAt( offset ));
            }

            public IntermediateRepresentation getRefAt( final int offset )
            {
                String tmp =
                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$super$codeGen().namer().nextVariableName();
                return !this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$fromSlot.isLongSlot() &&
                               offset == this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$fromSlot.offset()
                       ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.oneTime(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            AnyValue.class )), tmp, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeOrNoValue", scala.reflect.ManifestFactory..MODULE$.classType(
                    CompiledHelpers.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.self(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "fromNode", scala.reflect.ManifestFactory..MODULE$.classType(
                    VarExpandCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))}))))),org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.load( tmp )}))) :
                (!this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$toSlot.isLongSlot() &&
                         offset == this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$toSlot.offset()
                 ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.oneTime(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                AnyValue.class )), tmp, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeOrNoValue", scala.reflect.ManifestFactory..MODULE$.classType(
                        CompiledHelpers.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.self(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "targetToNode", scala.reflect.ManifestFactory..MODULE$.classType(
                    VarExpandCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))}))))),org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.load( tmp )}))) :super.getRefAt( offset ));
            }
        }; this.nodePredicate_$eq( this.maybeNodeVariablePredicate.map( ( p ) -> {
        return (IntermediateExpression) newScopeExpressionCompiler.intermediateCompileExpression( p.predicate() ).getOrElse( () -> {
            throw new CantCompileQueryException(
                    (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( p.predicate() ).toString() );
        } );
    } ) );
        this.relPredicate_$eq( this.maybeRelVariablePredicate.map( ( p ) -> {
            return (IntermediateExpression) newScopeExpressionCompiler.intermediateCompileExpression( p.predicate() ).getOrElse( () -> {
                throw new CantCompileQueryException(
                        (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( p.predicate() ).toString() );
            } );
        } ) );
        TypeReference var2;
        if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( dir )){
        var2 = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( OutgoingVarExpandCursor.class ));
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( dir )){
        var2 = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( IncomingVarExpandCursor.class ));
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( dir )){
            throw new MatchError( dir );
        }

        var2 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( AllVarExpandCursor.class ))
        ;
    }

        Seq fields = (Seq) ((TraversableLike) this.nodePredicate().map( ( x$3 ) -> {
            return x$3.fields();
        } ).getOrElse( () -> {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        } )).$plus$plus( (GenTraversableOnce) this.relPredicate().map( ( x$4 ) -> {
            return x$4.fields();
        } ).getOrElse( () -> {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        } ), scala.collection.Seq..MODULE$.canBuildFrom());
        Iterable locals = (Iterable) ((TraversableLike) this.nodePredicate().map( ( x$5 ) -> {
            return x$5.variables();
        } ).getOrElse( () -> {
            return scala.Predef..MODULE$.Set().empty();
        } )).$plus$plus( (GenTraversableOnce) this.relPredicate().map( ( x$6 ) -> {
            return x$6.variables();
        } ).getOrElse( () -> {
            return scala.Predef..MODULE$.Set().empty();
        } ), scala.collection.Iterable..MODULE$.canBuildFrom());
        return new ExtendClass(
                (new StringBuilder( 19 )).append( super.codeGen().namer().nextVariableName().toUpperCase() ).append( "VarExpandCursorImpl" ).toString(), var2,
                (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Parameter[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "fromNode",
                        scala.reflect.ManifestFactory..MODULE$.Long()), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "toNode",
                scala.reflect.ManifestFactory..MODULE$.Long()),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.param( "nodeCursor", scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.param( "projectBackwards", scala.reflect.ManifestFactory..MODULE$.Boolean()),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.param( "types", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int())),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "minLength", scala.reflect.ManifestFactory..MODULE$.Int()),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "maxLength", scala.reflect.ManifestFactory..MODULE$.Int()),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "read", scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "context", scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "dbAccess", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.param( "params", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "cursors", scala.reflect.ManifestFactory..MODULE$.classType( ExpressionCursors.class )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.param( "expressionVariables", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType(
                AnyValue.class )))}))),(Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new MethodDeclaration[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.methodDeclaration( "satisfyPredicates",
                    this.generatePredicate(), () -> {
                        return locals.toSeq();
                    }, scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Parameter[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "context",
                            scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class )),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "dbAccess", scala.reflect.ManifestFactory..MODULE$.classType(
                    DbAccess.class )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "params",
            scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "cursors", scala.reflect.ManifestFactory..MODULE$.classType( ExpressionCursors.class )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.param( "expressionVariables", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType(
                AnyValue.class ))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.param( "selectionCursor", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ))})),
        scala.reflect.ManifestFactory..MODULE$.Boolean())}))),fields);
    }

    private IntermediateRepresentation loadTypes()
    {
        return (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.missingTypes ))).isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.noop() :org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayLength( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.typeField() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToInteger( this.types.length + this.missingTypes.length ) )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.typeField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "computeTypes", scala.reflect.ManifestFactory..MODULE$.classType(
                ExpandAllOperatorTaskTemplate.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()),
        scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()),scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class )),
        scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.typeField() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.missingTypeField() ), OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS()})))))
        ;
    }

    private IntermediateRepresentation getNodeIdFromSlot( final Slot slot )
    {
        boolean var3 = false;
        RefSlot var4 = null;
        IntermediateRepresentation var2;
        if ( slot instanceof LongSlot )
        {
            LongSlot var6 = (LongSlot) slot;
            int offset = var6.offset();
            var2 = super.codeGen().getLongAt( offset );
        }
        else
        {
            if ( slot instanceof RefSlot )
            {
                var3 = true;
                var4 = (RefSlot) slot;
                int offset = var4.offset();
                boolean var9 = var4.nullable();
                if ( !var9 )
                {
                    var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeIdOrNullFromAnyValue",
                            scala.reflect.ManifestFactory..MODULE$.classType( CompiledHelpers.class ), scala.reflect.ManifestFactory..
                    MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{super.codeGen().getRefAt( offset )}) ));
                    return var2;
                }
            }

            if ( !var3 )
            {
                throw new InternalException( (new StringBuilder( 42 )).append( "Do not know how to get a node id for slot " ).append( slot ).toString() );
            }

            int offset = var4.offset();
            boolean var11 = var4.nullable();
            if ( !var11 )
            {
                throw new InternalException( (new StringBuilder( 42 )).append( "Do not know how to get a node id for slot " ).append( slot ).toString() );
            }

            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( super.codeGen().getRefAt( offset ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeIdOrNullFromAnyValue",
                    scala.reflect.ManifestFactory..MODULE$.classType( CompiledHelpers.class ), scala.reflect.ManifestFactory..
            MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{super.codeGen().getRefAt( offset )}) )));
        }

        return var2;
    }

    private final IntermediateRepresentation generateStartNodePredicate$1( final String fromNode$1, final String toNode$1 )
    {
        this.startNodePredicate_$eq( this.maybeNodeVariablePredicate.map( ( p ) -> {
            return (IntermediateExpression) this.org$neo4j$cypher$internal$runtime$pipelined$operators$VarExpandOperatorTaskTemplate$$super$codeGen().intermediateCompileExpression(
                    p.predicate() ).getOrElse( () -> {
                throw new CantCompileQueryException(
                        (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( p.predicate() ).toString() );
            } );
        } ) );
        Option fromNodePredicate = this.tempNodeOffset == VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() ? scala.None..MODULE$:
    this.startNodePredicate().map( ( pred ) -> {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arraySet(
                        OperatorCodeGenHelperTemplates$.MODULE$.EXPRESSION_VARIABLES(), this.tempNodeOffset,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeById", scala.reflect.ManifestFactory..MODULE$.classType(
                        DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( fromNode$1 )})))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), ExpressionCompiler$.MODULE$.nullCheckIfRequired( pred,
                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ))})));
    } ); IntermediateRepresentation predicateOnFromNode = (IntermediateRepresentation) scala.Option..
        MODULE$.option2Iterable( (Option) fromNodePredicate ).foldLeft( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( fromNode$1 ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),( x0$1, x1$1 ) -> {
        Tuple2 var3 = new Tuple2( x0$1, x1$1 );
        if ( var3 != null )
        {
            IntermediateRepresentation acc = (IntermediateRepresentation) var3._1();
            IntermediateRepresentation current = (IntermediateRepresentation) var3._2();
            BooleanAnd var2 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( acc, current );
            return var2;
        }
        else
        {
            throw new MatchError( var3 );
        }
    });
        IntermediateRepresentation predicate = this.shouldExpandAll ? predicateOnFromNode : org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.and( predicateOnFromNode, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( toNode$1 ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )));
        return (IntermediateRepresentation) predicate;
    }
}
