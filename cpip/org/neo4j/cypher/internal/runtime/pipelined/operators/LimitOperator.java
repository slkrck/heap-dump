package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.values.AnyValue;
import scala.math.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class LimitOperator implements MiddleOperator
{
    private final int argumentStateMapId;
    private final WorkIdentity workIdentity;
    private final Expression countExpression;

    public LimitOperator( final int argumentStateMapId, final WorkIdentity workIdentity, final Expression countExpression )
    {
        this.argumentStateMapId = argumentStateMapId;
        this.workIdentity = workIdentity;
        this.countExpression = countExpression;
    }

    public static long evaluateCountValue( final AnyValue countValue )
    {
        return LimitOperator$.MODULE$.evaluateCountValue( var0 );
    }

    public static long evaluateCountValue( final QueryContext queryContext, final QueryState state, final QueryResources resources,
            final Expression countExpression )
    {
        return LimitOperator$.MODULE$.evaluateCountValue( var0, var1, var2, var3 );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public OperatorTask createTask( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        long limit = LimitOperator$.MODULE$.evaluateCountValue( queryContext, state, resources, this.countExpression );
        return new LimitOperator.LimitOperatorTask( this,
                argumentStateCreator.createArgumentStateMap( this.argumentStateMapId, new LimitOperator.LimitStateFactory( limit ) ) );
    }

    public static class ConcurrentLimitState extends LimitOperator.LimitState
    {
        private final long argumentRowId;
        private final long[] argumentRowIdsForReducers;
        private final AtomicLong countLeft;

        public ConcurrentLimitState( final long argumentRowId, final long countTotal, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.countLeft = new AtomicLong( countTotal );
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private AtomicLong countLeft()
        {
            return this.countLeft;
        }

        public long reserve( final long wanted )
        {
            long var10000;
            if ( this.countLeft().get() <= 0L )
            {
                var10000 = 0L;
            }
            else
            {
                long newCountLeft = this.countLeft().addAndGet( -wanted );
                var10000 = newCountLeft >= 0L ? wanted : .MODULE$.max( 0L, wanted + newCountLeft );
            }

            return var10000;
        }

        public boolean isCancelled()
        {
            return this.countLeft().get() <= 0L;
        }

        public String toString()
        {
            return (new StringBuilder( 34 )).append( "ConcurrentLimitState(" ).append( this.argumentRowId() ).append( ", countLeft=" ).append(
                    this.countLeft().get() ).append( ")" ).toString();
        }
    }

    public abstract static class LimitState implements ArgumentStateMap.WorkCanceller
    {
        public abstract long reserve( final long wanted );
    }

    public static class LimitStateFactory implements ArgumentStateMap.ArgumentStateFactory<LimitOperator.LimitState>
    {
        private final long count;

        public LimitStateFactory( final long count )
        {
            this.count = count;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public LimitOperator.LimitState newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new LimitOperator.StandardLimitState( argumentRowId, this.count, argumentRowIdsForReducers );
        }

        public LimitOperator.LimitState newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new LimitOperator.ConcurrentLimitState( argumentRowId, this.count, argumentRowIdsForReducers );
        }
    }

    public static class StandardLimitState extends LimitOperator.LimitState
    {
        private final long argumentRowId;
        private final long[] argumentRowIdsForReducers;
        private long countLeft;

        public StandardLimitState( final long argumentRowId, final long countTotal, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.countLeft = countTotal;
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private long countLeft()
        {
            return this.countLeft;
        }

        private void countLeft_$eq( final long x$1 )
        {
            this.countLeft = x$1;
        }

        public long reserve( final long wanted )
        {
            long got = .MODULE$.min( this.countLeft(), wanted );
            this.countLeft_$eq( this.countLeft() - got );
            return got;
        }

        public boolean isCancelled()
        {
            return this.countLeft() == 0L;
        }

        public String toString()
        {
            return (new StringBuilder( 32 )).append( "StandardLimitState(" ).append( this.argumentRowId() ).append( ", countLeft=" ).append(
                    this.countLeft() ).append( ")" ).toString();
        }
    }

    public class FilterState
    {
        private long countLeft;

        public FilterState( final LimitOperator $outer, final long countLeft )
        {
            this.countLeft = countLeft;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public long countLeft()
        {
            return this.countLeft;
        }

        public void countLeft_$eq( final long x$1 )
        {
            this.countLeft = x$1;
        }

        public boolean next()
        {
            boolean var10000;
            if ( this.countLeft() > 0L )
            {
                this.countLeft_$eq( this.countLeft() - 1L );
                var10000 = true;
            }
            else
            {
                var10000 = false;
            }

            return var10000;
        }
    }

    public class LimitOperatorTask implements OperatorTask
    {
        private final ArgumentStateMap<LimitOperator.LimitState> argumentStateMap;

        public LimitOperatorTask( final LimitOperator $outer, final ArgumentStateMap<LimitOperator.LimitState> argumentStateMap )
        {
            this.argumentStateMap = argumentStateMap;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
            }
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$LimitOperator$LimitOperatorTask$$$outer().workIdentity();
        }

        public void operate( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            this.argumentStateMap.filter( output, ( rowCount, nRows ) -> {
                return $anonfun$operate$1( this, rowCount, BoxesRunTime.unboxToLong( nRows ) );
            }, ( x, x$1 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$operate$2( x, x$1 ) );
            } );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
        }
    }
}
