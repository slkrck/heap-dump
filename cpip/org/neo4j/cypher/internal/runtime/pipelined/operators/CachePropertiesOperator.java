package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexReadSession;
import scala.Array.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CachePropertiesOperator implements StatelessOperator
{
    private final WorkIdentity workIdentity;
    private final Expression[] properties;

    public CachePropertiesOperator( final WorkIdentity workIdentity, final Expression[] properties )
    {
        this.workIdentity = workIdentity;
        this.properties = properties;
        OperatorTask.$init$( this );
        StatelessOperator.$init$( this );
    }

    public final OperatorTask createTask( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StatelessOperator.createTask$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public final void setExecutionEvent( final OperatorProfileEvent event )
    {
        StatelessOperator.setExecutionEvent$( this, event );
    }

    public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
            final QueryProfiler queryProfiler )
    {
        OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public Expression[] properties()
    {
        return this.properties;
    }

    public void operate( final MorselExecutionContext currentRow, final QueryContext context, final QueryState state, final QueryResources resources )
    {
        SlottedQueryState queryState =
                new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(), (IndexReadSession[]).
        MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
        resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
        MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
        ;

        while ( currentRow.isValidRow() )
        {
            for ( int i = 0; i < this.properties().length; ++i )
            {
                this.properties()[i].apply( currentRow, queryState );
            }

            currentRow.moveToNextRow();
        }
    }
}
