package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.scheduling.HasWorkIdentity;
import scala.Option;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface OutputOperator extends HasWorkIdentity
{
    Option<BufferId> outputBuffer();

    OutputOperatorState createState( final ExecutionState executionState, final int pipelineId );
}
