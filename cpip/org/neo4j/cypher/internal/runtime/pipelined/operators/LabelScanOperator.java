package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.internal.kernel.api.NodeLabelIndexCursor;
import org.neo4j.internal.kernel.api.Read;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class LabelScanOperator implements StreamingOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$offset;
    public final LazyLabel org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$label;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$argumentSize;
    private final WorkIdentity workIdentity;

    public LabelScanOperator( final WorkIdentity workIdentity, final int offset, final LazyLabel label, final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$offset = offset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$label = label;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$argumentSize = argumentSize;
        StreamingOperator.$init$( this );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LabelScanOperator.SingleThreadedScanTask[]{new LabelScanOperator.SingleThreadedScanTask( this, inputMorsel.nextCopy() )}) ));
    }

    public class SingleThreadedScanTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private NodeLabelIndexCursor cursor;

        public SingleThreadedScanTask( final LabelScanOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$SingleThreadedScanTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "LabelScanSerialTask";
        }

        private NodeLabelIndexCursor cursor()
        {
            return this.cursor;
        }

        private void cursor_$eq( final NodeLabelIndexCursor x$1 )
        {
            this.cursor = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            int id =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$SingleThreadedScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$label.getId(
                            context );
            boolean var10000;
            if ( id == org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.UNKNOWN()){
            var10000 = false;
        } else{
            this.cursor_$eq( (NodeLabelIndexCursor) resources.cursorPools().nodeLabelIndexCursorPool().allocateAndTrace() );
            Read read = context.transactionalContext().dataRead();
            read.nodeLabelScan( id, this.cursor() );
            var10000 = true;
        }

            return var10000;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.cursor().next() )
            {
                outputRow.copyFrom( this.inputMorsel(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$SingleThreadedScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$SingleThreadedScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$argumentSize.nReferences() );
                outputRow.setLongAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$SingleThreadedScanTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$LabelScanOperator$$offset,
                        this.cursor().nodeReference() );
                outputRow.moveToNextRow();
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.cursor() != null )
            {
                this.cursor().setTracer( event );
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            resources.cursorPools().nodeLabelIndexCursorPool().free( this.cursor() );
            this.cursor_$eq( (NodeLabelIndexCursor) null );
        }
    }
}
