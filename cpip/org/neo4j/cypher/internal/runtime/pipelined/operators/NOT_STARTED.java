package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class NOT_STARTED
{
    public static String toString()
    {
        return NOT_STARTED$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return NOT_STARTED$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return NOT_STARTED$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return NOT_STARTED$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return NOT_STARTED$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return NOT_STARTED$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return NOT_STARTED$.MODULE$.productPrefix();
    }
}
