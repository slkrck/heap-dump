package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.runtime.AbstractFunction5;
import scala.runtime.BoxesRunTime;

public final class MorselBufferOutputState$ extends AbstractFunction5<WorkIdentity,Object,BufferId,ExecutionState,PipelineId,MorselBufferOutputState>
        implements Serializable
{
    public static MorselBufferOutputState$ MODULE$;

    static
    {
        new MorselBufferOutputState$();
    }

    private MorselBufferOutputState$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselBufferOutputState";
    }

    public MorselBufferOutputState apply( final WorkIdentity workIdentity, final boolean trackTime, final int bufferId, final ExecutionState executionState,
            final int pipelineId )
    {
        return new MorselBufferOutputState( workIdentity, trackTime, bufferId, executionState, pipelineId );
    }

    public Option<Tuple5<WorkIdentity,Object,BufferId,ExecutionState,PipelineId>> unapply( final MorselBufferOutputState x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple5( x$0.workIdentity(), BoxesRunTime.boxToBoolean( x$0.trackTime() ), new BufferId( x$0.bufferId() ), x$0.executionState(),
                    new PipelineId( x$0.pipelineId() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
