package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.values.AnyValue;
import scala.Function0;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class InputOperatorTemplate implements ContinuableOperatorTaskWithMorselTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final DelegateOperatorTaskTemplate innermost;
    private final int[] nodeOffsets;
    private final int[] relationshipOffsets;
    private final int[] refOffsets;
    private final boolean isHead;
    private final OperatorExpressionCompiler codeGen;
    private final InstanceField inputCursorField;
    private final InstanceField canContinue;

    public InputOperatorTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost, final int[] nodeOffsets,
            final int[] relationshipOffsets, final int[] refOffsets, final boolean nullable, final boolean isHead, final OperatorExpressionCompiler codeGen )
    {
        this.inner = inner;
        this.id = id;
        this.innermost = innermost;
        this.nodeOffsets = nodeOffsets;
        this.relationshipOffsets = relationshipOffsets;
        this.refOffsets = refOffsets;
        this.isHead = isHead;
        this.codeGen = codeGen;
        OperatorTaskTemplate.$init$( this );
        ContinuableOperatorTaskWithMorselTemplate.$init$( this );
        this.inputCursorField = .
        MODULE$.field( codeGen.namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( MutatingInputCursor.class ));
        this.canContinue = .MODULE$.field( codeGen.namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.Boolean());
    }

    public static boolean $lessinit$greater$default$8()
    {
        return InputOperatorTemplate$.MODULE$.$lessinit$greater$default$8();
    }

    public final IntermediateRepresentation genOperate()
    {
        return ContinuableOperatorTaskWithMorselTemplate.genOperate$( this );
    }

    public IntermediateRepresentation genScopeWithLocalDeclarations( final String scopeId, final Function0<IntermediateRepresentation> genBody )
    {
        return ContinuableOperatorTaskWithMorselTemplate.genScopeWithLocalDeclarations$( this, scopeId, genBody );
    }

    public IntermediateRepresentation genAdvanceOnCancelledRow()
    {
        return ContinuableOperatorTaskWithMorselTemplate.genAdvanceOnCancelledRow$( this );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return ContinuableOperatorTaskWithMorselTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return OperatorTaskTemplate.genOperateEnter$( this );
    }

    public IntermediateRepresentation genOperateExit()
    {
        return OperatorTaskTemplate.genOperateExit$( this );
    }

    public IntermediateRepresentation genProduce()
    {
        return OperatorTaskTemplate.genProduce$( this );
    }

    public IntermediateRepresentation genCreateState()
    {
        return OperatorTaskTemplate.genCreateState$( this );
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return OperatorTaskTemplate.genOutputBuffer$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public final boolean isHead()
    {
        return this.isHead;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    private InstanceField inputCursorField()
    {
        return this.inputCursorField;
    }

    private InstanceField canContinue()
    {
        return this.canContinue;
    }

    public String scopeId()
    {
        return (new StringBuilder( 5 )).append( "input" ).append( this.id() ).toString();
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue().map( ( x$2 ) -> {
            return .MODULE$.or( x$2,.MODULE$.loadField( this.canContinue() ));
        } ).orElse( () -> {
            return new Some(.MODULE$.loadField( this.canContinue() ));
        } );
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.condition(.MODULE$.isNotNull(.MODULE$.loadField(
                    this.inputCursorField() )),.MODULE$.invoke(.MODULE$.loadField( this.inputCursorField() ), .
        MODULE$.method( "close", scala.reflect.ManifestFactory..MODULE$.classType( MutatingInputCursor.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),this.inner().genCloseCursors()})));
    }

    public final IntermediateRepresentation genOperateHead()
    {
        IntermediateRepresentation[] setNodes = (IntermediateRepresentation[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                (Object[]) (new ofInt( scala.Predef..MODULE$.intArrayOps( this.nodeOffsets )) ).zipWithIndex( scala.Array..MODULE$.canBuildFrom(
                scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )))))).map( ( x0$1 ) -> {
        if ( x0$1 != null )
        {
            int nodeOffset = x0$1._1$mcI$sp();
            int i = x0$1._2$mcI$sp();
            IntermediateRepresentation var2 = this.codeGen().setLongAt( nodeOffset,.MODULE$.invokeStatic(.MODULE$.method( "nodeOrNoValue",
                    scala.reflect.ManifestFactory..MODULE$.classType( InputOperator.class ), scala.reflect.ManifestFactory..
            MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.invoke(.MODULE$.loadField( this.inputCursorField() ),.MODULE$.method(
                    "value", scala.reflect.ManifestFactory..MODULE$.classType( MutatingInputCursor.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( i ) )})))}))));
            return var2;
        }
        else
        {
            throw new MatchError( x0$1 );
        }
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )));
        IntermediateRepresentation[] setRelationships = (IntermediateRepresentation[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                (Object[]) (new ofInt( scala.Predef..MODULE$.intArrayOps( this.relationshipOffsets )) ).zipWithIndex( scala.Array..MODULE$.canBuildFrom(
                scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )))))).map( ( x0$2 ) -> {
        if ( x0$2 != null )
        {
            int relationshipOffset = x0$2._1$mcI$sp();
            int i = x0$2._2$mcI$sp();
            IntermediateRepresentation var2 = this.codeGen().setLongAt( relationshipOffset,.MODULE$.invokeStatic(.MODULE$.method( "relationshipOrNoValue",
                    scala.reflect.ManifestFactory..MODULE$.classType( InputOperator.class ), scala.reflect.ManifestFactory..
            MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.invoke(.MODULE$.loadField( this.inputCursorField() ),.MODULE$.method(
                    "value", scala.reflect.ManifestFactory..MODULE$.classType( MutatingInputCursor.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( BoxesRunTime.boxToInteger( this.nodeOffsets.length + i ) )})))}))))
            ;
            return var2;
        }
        else
        {
            throw new MatchError( x0$2 );
        }
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )));
        IntermediateRepresentation[] setRefs = (IntermediateRepresentation[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                (Object[]) (new ofInt( scala.Predef..MODULE$.intArrayOps( this.refOffsets )) ).zipWithIndex( scala.Array..MODULE$.canBuildFrom(
                scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )))))).map( ( x0$3 ) -> {
        if ( x0$3 != null )
        {
            int refOffset = x0$3._1$mcI$sp();
            int i = x0$3._2$mcI$sp();
            IntermediateRepresentation var2 = this.codeGen().setRefAt( refOffset,.MODULE$.invoke(.MODULE$.loadField( this.inputCursorField() ), .
            MODULE$.method( "value", scala.reflect.ManifestFactory..MODULE$.classType( MutatingInputCursor.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant(
                        BoxesRunTime.boxToInteger( this.nodeOffsets.length + this.relationshipOffsets.length + i ) )}))));
            return var2;
        }
        else
        {
            throw new MatchError( x0$3 );
        }
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )));
        IntermediateRepresentation setters = .MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
            (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) setNodes )) ).$plus$plus(
            new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) setRelationships ) ), scala.Array..MODULE$.canBuildFrom(
            scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )))))).
        $plus$plus( new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) setRefs ) ), scala.Array..
        MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )))));
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.condition(.MODULE$.isNull(.MODULE$.loadField( this.inputCursorField() )),.MODULE$.setField(
            this.inputCursorField(),.MODULE$.newInstance(.MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType(
            MutatingInputCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( InputDataStream.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.QUERY_STATE(),.MODULE$.method( "input",
                    scala.reflect.ManifestFactory..MODULE$.classType( QueryState.class ), scala.reflect.ManifestFactory..MODULE$.classType(
            InputDataStream.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))}))))), .
        MODULE$.condition(.MODULE$.not(.MODULE$.loadField( this.canContinue() )), .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.setField( this.canContinue(),.MODULE$.invoke(.MODULE$.loadField(
                    this.inputCursorField() ),.MODULE$.method( "nextInput", scala.reflect.ManifestFactory..MODULE$.classType(
            MutatingInputCursor.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        OperatorCodeGenHelperTemplates$.MODULE$.profileRow( this.id(),.MODULE$.loadField( this.canContinue() ))})))), .
        MODULE$.labeledLoop( OperatorCodeGenHelperTemplates$.MODULE$.OUTER_LOOP_LABEL_NAME(),.MODULE$.and( this.innermost.predicate(),.MODULE$.loadField(
                this.canContinue() )), .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{setters, this.inner().genOperateWithExpressions(),
                    this.doIfInnerCantContinue(.MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{.MODULE$.setField( this.canContinue(),.MODULE$.invoke(.MODULE$.loadField(
                                    this.inputCursorField() ),.MODULE$.method( "nextInput", scala.reflect.ManifestFactory..MODULE$.classType(
                            MutatingInputCursor.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()), scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[0]) ))),
        OperatorCodeGenHelperTemplates$.MODULE$.profileRow( this.id(),.MODULE$.loadField( this.canContinue() ))})))),
        this.innermost.resetCachedPropertyVariables()}))))})));
    }

    public IntermediateRepresentation genOperateMiddle()
    {
        throw new CantCompileQueryException( "Cannot compile Input as middle operator" );
    }

    public IntermediateRepresentation genInit()
    {
        return this.inner().genInit();
    }

    public Seq<Field> genFields()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.inputCursorField(), this.canContinue()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return this.inner().genSetExecutionEvent( event );
    }

    public IntermediateRepresentation genClearStateOnCancelledRow()
    {
        return .MODULE$.noop();
    }
}
