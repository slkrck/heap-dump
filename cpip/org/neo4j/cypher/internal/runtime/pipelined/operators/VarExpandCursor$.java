package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING.;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.RelationshipValue;
import scala.MatchError;
import scala.runtime.BoxesRunTime;

public final class VarExpandCursor$
{
    public static VarExpandCursor$ MODULE$;

    static
    {
        new VarExpandCursor$();
    }

    private VarExpandCursor$()
    {
        MODULE$ = this;
    }

    public VarExpandCursor apply( final SemanticDirection direction, final long fromNode, final long targetToNode, final NodeCursor nodeCursor,
            final boolean projectBackwards, final int[] relTypes, final int minLength, final int maxLength, final Read read, final DbAccess dbAccess,
            final VarExpandPredicate<Object> nodePredicate, final VarExpandPredicate<RelationshipSelectionCursor> relationshipPredicate )
    {
        Object var15;
        if (.MODULE$.equals( direction )){
        var15 = new OutgoingVarExpandCursor( fromNode, targetToNode, nodeCursor, projectBackwards, relTypes, minLength, maxLength, read, dbAccess,
                nodePredicate, relationshipPredicate )
        {
            private final VarExpandPredicate nodePredicate$1;
            private final VarExpandPredicate relationshipPredicate$1;

            public
            {
                this.nodePredicate$1 = nodePredicate$1;
                this.relationshipPredicate$1 = relationshipPredicate$1;
            }

            public boolean satisfyPredicates( final ExecutionContext executionContext, final DbAccess dbAccess, final AnyValue[] params,
                    final ExpressionCursors cursors, final AnyValue[] expressionVariables, final RelationshipSelectionCursor selectionCursor )
            {
                return this.relationshipPredicate$1.isTrue( selectionCursor ) &&
                        this.nodePredicate$1.isTrue( BoxesRunTime.boxToLong( selectionCursor.otherNodeReference() ) );
            }
        };
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( direction )){
        var15 = new IncomingVarExpandCursor( fromNode, targetToNode, nodeCursor, projectBackwards, relTypes, minLength, maxLength, read, dbAccess,
                nodePredicate, relationshipPredicate )
        {
            private final VarExpandPredicate nodePredicate$1;
            private final VarExpandPredicate relationshipPredicate$1;

            public
            {
                this.nodePredicate$1 = nodePredicate$1;
                this.relationshipPredicate$1 = relationshipPredicate$1;
            }

            public boolean satisfyPredicates( final ExecutionContext executionContext, final DbAccess dbAccess, final AnyValue[] params,
                    final ExpressionCursors cursors, final AnyValue[] expressionVariables, final RelationshipSelectionCursor selectionCursor )
            {
                return this.relationshipPredicate$1.isTrue( selectionCursor ) &&
                        this.nodePredicate$1.isTrue( BoxesRunTime.boxToLong( selectionCursor.otherNodeReference() ) );
            }
        };
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( direction )){
            throw new MatchError( direction );
        }

        var15 = new AllVarExpandCursor( fromNode, targetToNode, nodeCursor, projectBackwards, relTypes, minLength, maxLength, read, dbAccess, nodePredicate,
                relationshipPredicate )
        {
            private final VarExpandPredicate nodePredicate$1;
            private final VarExpandPredicate relationshipPredicate$1;

            public
            {
                this.nodePredicate$1 = nodePredicate$1;
                this.relationshipPredicate$1 = relationshipPredicate$1;
            }

            public boolean satisfyPredicates( final ExecutionContext executionContext, final DbAccess dbAccess, final AnyValue[] params,
                    final ExpressionCursors cursors, final AnyValue[] expressionVariables, final RelationshipSelectionCursor selectionCursor )
            {
                return this.relationshipPredicate$1.isTrue( selectionCursor ) &&
                        this.nodePredicate$1.isTrue( BoxesRunTime.boxToLong( selectionCursor.otherNodeReference() ) );
            }
        };
    }

        return (VarExpandCursor) var15;
    }

    public RelationshipValue relationshipFromCursor( final DbAccess dbAccess, final RelationshipSelectionCursor cursor )
    {
        return dbAccess.relationshipById( cursor.relationshipReference(), cursor.sourceNodeReference(), cursor.targetNodeReference(), cursor.type() );
    }
}
