package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class InputLoopTaskTemplate$
{
    public static InputLoopTaskTemplate$ MODULE$;

    static
    {
        new InputLoopTaskTemplate$();
    }

    private InputLoopTaskTemplate$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$5()
    {
        return true;
    }
}
