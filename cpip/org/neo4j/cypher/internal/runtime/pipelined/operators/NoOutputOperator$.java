package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityImpl;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.None.;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;

public final class NoOutputOperator$ implements OutputOperator, OutputOperatorState, PreparedOutput, Product, Serializable
{
    public static NoOutputOperator$ MODULE$;

    static
    {
        new NoOutputOperator$();
    }

    private NoOutputOperator$()
    {
        MODULE$ = this;
        OutputOperatorState.$init$( this );
        Product.$init$( this );
    }

    public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
            final QueryResources resources, final QueryProfiler queryProfiler )
    {
        return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
    }

    public boolean canContinueOutput()
    {
        return OutputOperatorState.canContinueOutput$( this );
    }

    public Option<BufferId> outputBuffer()
    {
        return .MODULE$;
    }

    public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
    {
        return this;
    }

    public PreparedOutput prepareOutput( final MorselExecutionContext outputMorsel, final QueryContext context, final QueryState state,
            final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
    {
        return this;
    }

    public void produce()
    {
    }

    public WorkIdentity workIdentity()
    {
        return new WorkIdentityImpl( org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID(),"Perform no output");
    }

    public boolean trackTime()
    {
        return true;
    }

    public String productPrefix()
    {
        return "NoOutputOperator";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NoOutputOperator$;
    }

    public int hashCode()
    {
        return -1056142074;
    }

    public String toString()
    {
        return "NoOutputOperator";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
