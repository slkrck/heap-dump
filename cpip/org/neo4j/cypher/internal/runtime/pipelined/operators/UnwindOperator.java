package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ListSupport;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.ListSupport.RichSeq;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import scala.Option;
import scala.PartialFunction;
import scala.collection.IndexedSeq;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class UnwindOperator implements StreamingOperator, ListSupport
{
    public final Expression org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$$collection;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$$offset;
    private final WorkIdentity workIdentity;

    public UnwindOperator( final WorkIdentity workIdentity, final Expression collection, final int offset )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$$collection = collection;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$$offset = offset;
        StreamingOperator.$init$( this );
        ListSupport.$init$( this );
    }

    public boolean isList( final AnyValue x )
    {
        return ListSupport.isList$( this, x );
    }

    public <T> Option<Iterable<T>> asListOf( final PartialFunction<AnyValue,T> test, final Iterable<AnyValue> input )
    {
        return ListSupport.asListOf$( this, test, input );
    }

    public ListValue makeTraversable( final AnyValue z )
    {
        return ListSupport.makeTraversable$( this, z );
    }

    public PartialFunction<AnyValue,ListValue> castToList()
    {
        return ListSupport.castToList$( this );
    }

    public <T> RichSeq<T> RichSeq( final Seq<T> inner )
    {
        return ListSupport.RichSeq$( this, inner );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext context, final QueryState state, final MorselParallelizer inputMorsel,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new UnwindOperator.OTask[]{new UnwindOperator.OTask( this, inputMorsel.nextCopy() )}) ));
    }

    public class OTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private Iterator<AnyValue> unwoundValues;

        public OTask( final UnwindOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$OTask$$$outer().workIdentity();
        }

        private Iterator<AnyValue> unwoundValues()
        {
            return this.unwoundValues;
        }

        private void unwoundValues_$eq( final Iterator<AnyValue> x$1 )
        {
            this.unwoundValues = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            SlottedQueryState queryState = new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(),
                    (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
            resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
            MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
            ;
            initExecutionContext.copyFrom( this.inputMorsel(), this.inputMorsel().getLongsPerRow(), this.inputMorsel().getRefsPerRow() );
            AnyValue value =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$$collection.apply(
                            initExecutionContext, queryState );
            this.unwoundValues_$eq(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$OTask$$$outer().makeTraversable( value ).iterator() );
            return true;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( this.unwoundValues().hasNext() && outputRow.isValidRow() )
            {
                AnyValue thisValue = (AnyValue) this.unwoundValues().next();
                outputRow.copyFrom( this.inputMorsel() );
                outputRow.setRefAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$UnwindOperator$$offset,
                        thisValue );
                outputRow.moveToNextRow();
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            this.unwoundValues_$eq( (Iterator) null );
        }

        public boolean canContinue()
        {
            return this.unwoundValues() != null || this.inputMorsel().isValidRow();
        }
    }
}
