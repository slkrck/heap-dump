package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class DelegateOperatorTaskTemplate$
{
    public static DelegateOperatorTaskTemplate$ MODULE$;

    static
    {
        new DelegateOperatorTaskTemplate$();
    }

    private DelegateOperatorTaskTemplate$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$1()
    {
        return true;
    }

    public boolean $lessinit$greater$default$2()
    {
        return false;
    }

    public boolean $lessinit$greater$default$3()
    {
        return false;
    }
}
