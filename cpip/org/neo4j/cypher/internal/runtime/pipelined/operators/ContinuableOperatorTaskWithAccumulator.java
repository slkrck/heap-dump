package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ContinuableOperatorTaskWithAccumulator<DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> extends ContinuableOperatorTask
{
    static void $init$( final ContinuableOperatorTaskWithAccumulator $this )
    {
    }

    ACC accumulator();

    default void closeInput( final OperatorCloser operatorCloser )
    {
        operatorCloser.closeAccumulator( this.accumulator() );
    }

    default boolean filterCancelledArguments( final OperatorCloser operatorCloser )
    {
        return operatorCloser.filterCancelledArguments( this.accumulator() );
    }

    default WorkUnitEvent producingWorkUnitEvent()
    {
        return null;
    }

    default void setExecutionEvent( final OperatorProfileEvent event )
    {
    }

    default void closeCursors( final QueryResources resources )
    {
    }

    default long estimatedHeapUsage()
    {
        return 0L;
    }
}
