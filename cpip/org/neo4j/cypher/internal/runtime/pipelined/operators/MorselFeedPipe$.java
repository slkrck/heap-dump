package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Serializable;

public final class MorselFeedPipe$ implements Serializable
{
    public static MorselFeedPipe$ MODULE$;

    static
    {
        new MorselFeedPipe$();
    }

    private MorselFeedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$1()
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "MorselFeedPipe";
    }

    public MorselFeedPipe apply( final int id )
    {
        return new MorselFeedPipe( id );
    }

    public int apply$default$1()
    {
        return .MODULE$.INVALID_ID();
    }

    public boolean unapply( final MorselFeedPipe x$0 )
    {
        return x$0 != null;
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
