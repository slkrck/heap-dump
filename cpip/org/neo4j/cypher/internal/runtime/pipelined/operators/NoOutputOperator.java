package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Option;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class NoOutputOperator
{
    public static boolean canContinueOutput()
    {
        return NoOutputOperator$.MODULE$.canContinueOutput();
    }

    public static PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
            final QueryResources resources, final QueryProfiler queryProfiler )
    {
        return NoOutputOperator$.MODULE$.prepareOutputWithProfile( var0, var1, var2, var3, var4 );
    }

    public static String toString()
    {
        return NoOutputOperator$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return NoOutputOperator$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return NoOutputOperator$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return NoOutputOperator$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return NoOutputOperator$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return NoOutputOperator$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return NoOutputOperator$.MODULE$.productPrefix();
    }

    public static boolean trackTime()
    {
        return NoOutputOperator$.MODULE$.trackTime();
    }

    public static WorkIdentity workIdentity()
    {
        return NoOutputOperator$.MODULE$.workIdentity();
    }

    public static void produce()
    {
        NoOutputOperator$.MODULE$.produce();
    }

    public static PreparedOutput prepareOutput( final MorselExecutionContext outputMorsel, final QueryContext context, final QueryState state,
            final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
    {
        return NoOutputOperator$.MODULE$.prepareOutput( var0, var1, var2, var3, var4 );
    }

    public static OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
    {
        return NoOutputOperator$.MODULE$.createState( var0, var1 );
    }

    public static Option<BufferId> outputBuffer()
    {
        return NoOutputOperator$.MODULE$.outputBuffer();
    }
}
