package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.GetStatic;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregators;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.AvgAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.CollectAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.CountAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.CountDistinctAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.CountStarAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.MaxAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.MinAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.SumAggregator$;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater;
import org.neo4j.exceptions.SyntaxException;
import scala.Predef.;
import scala.collection.immutable.IndexedSeq;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.runtime.BoxesRunTime;

public final class AggregationMapperOperatorTaskTemplate$
{
    public static AggregationMapperOperatorTaskTemplate$ MODULE$;

    static
    {
        new AggregationMapperOperatorTaskTemplate$();
    }

    private AggregationMapperOperatorTaskTemplate$()
    {
        MODULE$ = this;
    }

    public IntermediateRepresentation createAggregators( final Aggregator[] aggregators )
    {
        GetStatic[] newAggregators = (GetStatic[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) aggregators ))).map( ( x0$1 ) -> {
        GetStatic var1;
        if ( CountStarAggregator$.MODULE$.equals( x0$1 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "COUNT_STAR", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }
        else if ( CountAggregator$.MODULE$.equals( x0$1 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "COUNT", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }
        else if ( CountDistinctAggregator$.MODULE$.equals( x0$1 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "COUNT_DISTINCT", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }
        else if ( SumAggregator$.MODULE$.equals( x0$1 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "SUM", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }
        else if ( AvgAggregator$.MODULE$.equals( x0$1 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "AVG", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }
        else if ( MaxAggregator$.MODULE$.equals( x0$1 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "MAX", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }
        else if ( MinAggregator$.MODULE$.equals( x0$1 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "MIN", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }
        else
        {
            if ( !CollectAggregator$.MODULE$.equals( x0$1 ) )
            {
                throw new SyntaxException( (new StringBuilder( 23 )).append( "Unexpected Aggregator: " ).append( x0$1.getClass().getName() ).toString() );
            }

            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "COLLECT", scala.reflect.ManifestFactory..MODULE$.classType( Aggregators.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( Aggregator.class ));
        }

        return var1;
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( GetStatic.class )));
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.arrayOf(.MODULE$.wrapRefArray( (Object[]) ((IntermediateRepresentation[]) newAggregators) ), scala.reflect.ManifestFactory..
        MODULE$.classType( Aggregator.class ));
    }

    public IntermediateRepresentation createUpdaters( final Aggregator[] aggregators, final IntermediateRepresentation aggregatorsVar )
    {
        IndexedSeq newUpdaters = (IndexedSeq) (new ofRef(.MODULE$.refArrayOps( (Object[]) aggregators ))).indices().map( ( i ) -> {
        return $anonfun$createUpdaters$1( aggregatorsVar, BoxesRunTime.unboxToInt( i ) );
    }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( newUpdaters, scala.reflect.ManifestFactory..MODULE$.classType( Updater.class ))
        ;
    }
}
