package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.collections.api.map.primitive.MutableLongObjectMap;
import org.eclipse.collections.impl.factory.primitive.LongObjectMaps;
import org.eclipse.collections.impl.list.mutable.FastList;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.runtime.slotted.pipes.NodeHashJoinSlottedPipe$;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeHashJoinSingleNodeOperator implements Operator, OperatorState
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$rhsOffset;
    public final Tuple2<Object,Object>[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$longsToCopy;
    public final Tuple2<Object,Object>[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$refsToCopy;
    public final Tuple2<Object,Object>[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$cachedPropertiesToCopy;
    private final WorkIdentity workIdentity;
    private final int lhsArgumentStateMapId;
    private final int rhsArgumentStateMapId;
    private final int lhsOffset;

    public NodeHashJoinSingleNodeOperator( final WorkIdentity workIdentity, final int lhsArgumentStateMapId, final int rhsArgumentStateMapId,
            final int lhsOffset, final int rhsOffset, final SlotConfiguration slots, final Tuple2<Object,Object>[] longsToCopy,
            final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy )
    {
        this.workIdentity = workIdentity;
        this.lhsArgumentStateMapId = lhsArgumentStateMapId;
        this.rhsArgumentStateMapId = rhsArgumentStateMapId;
        this.lhsOffset = lhsOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$rhsOffset = rhsOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$longsToCopy = longsToCopy;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$refsToCopy = refsToCopy;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$cachedPropertiesToCopy = cachedPropertiesToCopy;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        argumentStateCreator.createArgumentStateMap( this.lhsArgumentStateMapId,
                new NodeHashJoinSingleNodeOperator.HashTableFactory( this.lhsOffset, stateFactory.memoryTracker() ) );
        argumentStateCreator.createArgumentStateMap( this.rhsArgumentStateMapId, new ArgumentStateBuffer.Factory( stateFactory ) );
        return this;
    }

    public IndexedSeq<ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,NodeHashJoinSingleNodeOperator.HashTable>> nextTasks(
            final QueryContext context, final QueryState state, final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        Buffers.AccumulatorAndMorsel accAndMorsel = operatorInput.takeAccumulatorAndMorsel();
        return accAndMorsel != null ? .MODULE$.wrapRefArray( (Object[]) ((Object[]) (new NodeHashJoinSingleNodeOperator.OTask[]{
            new NodeHashJoinSingleNodeOperator.OTask( this, (NodeHashJoinSingleNodeOperator.HashTable) accAndMorsel.acc(), accAndMorsel.morsel() )})) ) :null;
    }

    public static class ConcurrentHashTable extends NodeHashJoinSingleNodeOperator.HashTable
    {
        private final long argumentRowId;
        private final int lhsOffset;
        private final long[] argumentRowIdsForReducers;
        private final ConcurrentHashMap<Object,ConcurrentLinkedQueue<MorselExecutionContext>> table;

        public ConcurrentHashTable( final long argumentRowId, final int lhsOffset, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.lhsOffset = lhsOffset;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.table = new ConcurrentHashMap();
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private ConcurrentHashMap<Object,ConcurrentLinkedQueue<MorselExecutionContext>> table()
        {
            return this.table;
        }

        public void update( final MorselExecutionContext morsel )
        {
            for ( ; morsel.isValidRow(); morsel.moveToNextRow() )
            {
                long key = morsel.getLongAt( this.lhsOffset );
                if ( !NullChecker$.MODULE$.entityIsNull( key ) )
                {
                    ConcurrentLinkedQueue lhsRows = (ConcurrentLinkedQueue) this.table().computeIfAbsent( BoxesRunTime.boxToLong( key ), ( x$1 ) -> {
                        return $anonfun$update$1( BoxesRunTime.unboxToLong( x$1 ) );
                    } );
                    BoxesRunTime.boxToBoolean( lhsRows.add( morsel.shallowCopy() ) );
                }
                else
                {
                    BoxedUnit var10000 = BoxedUnit.UNIT;
                }
            }
        }

        public Iterator<MorselExecutionContext> lhsRows( final long nodeId )
        {
            ConcurrentLinkedQueue lhsRows = (ConcurrentLinkedQueue) this.table().get( BoxesRunTime.boxToLong( nodeId ) );
            return lhsRows == null ? Collections.emptyIterator() : lhsRows.iterator();
        }
    }

    public abstract static class HashTable implements ArgumentStateMap.MorselAccumulator<MorselExecutionContext>
    {
        public abstract Iterator<MorselExecutionContext> lhsRows( final long nodeId );
    }

    public static class HashTableFactory implements ArgumentStateMap.ArgumentStateFactory<NodeHashJoinSingleNodeOperator.HashTable>
    {
        private final int lhsOffset;
        private final QueryMemoryTracker memoryTracker;

        public HashTableFactory( final int lhsOffset, final QueryMemoryTracker memoryTracker )
        {
            this.lhsOffset = lhsOffset;
            this.memoryTracker = memoryTracker;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public NodeHashJoinSingleNodeOperator.HashTable newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new NodeHashJoinSingleNodeOperator.StandardHashTable( argumentRowId, this.lhsOffset, argumentRowIdsForReducers, this.memoryTracker );
        }

        public NodeHashJoinSingleNodeOperator.HashTable newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new NodeHashJoinSingleNodeOperator.ConcurrentHashTable( argumentRowId, this.lhsOffset, argumentRowIdsForReducers );
        }
    }

    public static class StandardHashTable extends NodeHashJoinSingleNodeOperator.HashTable
    {
        private final long argumentRowId;
        private final int lhsOffset;
        private final long[] argumentRowIdsForReducers;
        private final QueryMemoryTracker memoryTracker;
        private final MutableLongObjectMap<FastList<MorselExecutionContext>> table;

        public StandardHashTable( final long argumentRowId, final int lhsOffset, final long[] argumentRowIdsForReducers,
                final QueryMemoryTracker memoryTracker )
        {
            this.argumentRowId = argumentRowId;
            this.lhsOffset = lhsOffset;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.memoryTracker = memoryTracker;
            this.table = LongObjectMaps.mutable.empty();
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private MutableLongObjectMap<FastList<MorselExecutionContext>> table()
        {
            return this.table;
        }

        public void update( final MorselExecutionContext morsel )
        {
            for ( ; morsel.isValidRow(); morsel.moveToNextRow() )
            {
                long key = morsel.getLongAt( this.lhsOffset );
                if ( !NullChecker$.MODULE$.entityIsNull( key ) )
                {
                    MorselExecutionContext view = morsel.view( morsel.getCurrentRow(), morsel.getCurrentRow() + 1 );
                    FastList list = (FastList) this.table().getIfAbsentPut( key, new FastList( 1 ) );
                    list.add( view );
                    this.memoryTracker.allocated( view );
                }
            }
        }

        public Iterator<MorselExecutionContext> lhsRows( final long nodeId )
        {
            FastList lhsRows = (FastList) this.table().get( nodeId );
            return lhsRows == null ? Collections.emptyIterator() : lhsRows.iterator();
        }
    }

    public class OTask extends InputLoopTask
            implements ContinuableOperatorTaskWithMorselAndAccumulator<MorselExecutionContext,NodeHashJoinSingleNodeOperator.HashTable>
    {
        private final NodeHashJoinSingleNodeOperator.HashTable accumulator;
        private final MorselExecutionContext rhsRow;
        private final MorselExecutionContext inputMorsel;
        private Iterator<MorselExecutionContext> lhsRows;

        public OTask( final NodeHashJoinSingleNodeOperator $outer, final NodeHashJoinSingleNodeOperator.HashTable accumulator,
                final MorselExecutionContext rhsRow )
        {
            this.accumulator = accumulator;
            this.rhsRow = rhsRow;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                ContinuableOperatorTaskWithAccumulator.$init$( this );
                ContinuableOperatorTaskWithMorselAndAccumulator.$init$( this );
                this.inputMorsel = rhsRow;
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorselAndAccumulator.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorselAndAccumulator.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorselAndAccumulator.producingWorkUnitEvent$( this );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            ContinuableOperatorTaskWithAccumulator.setExecutionEvent$( this, event );
        }

        public void closeCursors( final QueryResources resources )
        {
            ContinuableOperatorTaskWithAccumulator.closeCursors$( this, resources );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithAccumulator.estimatedHeapUsage$( this );
        }

        public NodeHashJoinSingleNodeOperator.HashTable accumulator()
        {
            return this.accumulator;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$OTask$$$outer().workIdentity();
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public String toString()
        {
            return "NodeHashJoinSingleNodeTask";
        }

        private Iterator<MorselExecutionContext> lhsRows()
        {
            return this.lhsRows;
        }

        private void lhsRows_$eq( final Iterator<MorselExecutionContext> x$1 )
        {
            this.lhsRows = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            long key = this.rhsRow.getLongAt(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$rhsOffset );
            this.lhsRows_$eq( this.accumulator().lhsRows( key ) );
            return true;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.lhsRows().hasNext() )
            {
                outputRow.copyFrom( (MorselExecutionContext) this.lhsRows().next() );
                NodeHashJoinSlottedPipe$.MODULE$.copyDataFromRhs(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$longsToCopy,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$refsToCopy,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinSingleNodeOperator$$cachedPropertiesToCopy,
                        outputRow, this.rhsRow );
                outputRow.moveToNextRow();
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
        }
    }
}
