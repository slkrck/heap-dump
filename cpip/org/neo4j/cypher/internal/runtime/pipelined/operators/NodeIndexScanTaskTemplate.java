package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.schema.IndexOrder;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.IndexedSeq;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeIndexScanTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int offset;
    private final SlottedIndexedProperty[] properties;
    private final int queryIndexId;
    private final IndexOrder indexOrder;
    private final SlotConfiguration.Size argumentSize;
    private final InstanceField nodeIndexCursorField;
    private final boolean needsValues;

    public NodeIndexScanTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost, final int offset,
            final SlottedIndexedProperty[] properties, final int queryIndexId, final IndexOrder indexOrder, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.offset = offset;
        this.properties = properties;
        this.queryIndexId = queryIndexId;
        this.indexOrder = indexOrder;
        this.argumentSize = argumentSize;
        this.nodeIndexCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ));
        this.needsValues = (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) properties ))).exists( ( x$1 ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$needsValues$1( x$1 ) );
    } );
    }

    private InstanceField nodeIndexCursorField()
    {
        return this.nodeIndexCursorField;
    }

    private boolean needsValues()
    {
        return this.needsValues;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.nodeIndexCursorField()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeIndexCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.nodeIndexCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{event}) ))),super.inner().genSetExecutionEvent( event )})));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.empty();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.nodeIndexCursorField(), this.executionEventField(),
                            OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_INDEX_CURSOR() ),
                    OperatorCodeGenHelperTemplates$.MODULE$.nodeIndexScan( OperatorCodeGenHelperTemplates$.MODULE$.indexReadSession( this.queryIndexId ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ), this.indexOrder,
                    this.needsValues()), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
            OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeIndexCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        int[] indexPropertyIndices = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.properties )) ).zipWithIndex( scala.Array..MODULE$.canBuildFrom(
                scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )) )))).filter( ( x$2 ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$genInnerLoop$1( x$2 ) );
    } )))).map( ( x$3 ) -> {
        return BoxesRunTime.boxToInteger( $anonfun$genInnerLoop$2( x$3 ) );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
        int[] indexPropertySlotOffsets = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.properties ))).flatMap( ( x$4 ) -> {
        return scala.Option..MODULE$.option2Iterable( x$4.maybeCachedNodePropertySlot() );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
        IndexedSeq cacheProperties = (IndexedSeq) (new ofInt( scala.Predef..MODULE$.intArrayOps( indexPropertyIndices ))).indices().map( ( i ) -> {
        return $anonfun$genInnerLoop$4( this, indexPropertyIndices, indexPropertySlotOffsets, BoxesRunTime.unboxToInt( i ) );
    }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                        super.codeGen().setLongAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                        NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( cacheProperties ), super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeIndexCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class )))),
        this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeIndexCursorField() ), OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$.MODULE$,
                    scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.nodeIndexCursorField(),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))})));
    }
}
