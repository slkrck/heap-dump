package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.runtime.AbstractFunction2;

public final class MorselArgumentStateBufferPreparedOutput$ extends
        AbstractFunction2<Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>,MorselArgumentStateBufferPreparedOutput>
        implements Serializable
{
    public static MorselArgumentStateBufferPreparedOutput$ MODULE$;

    static
    {
        new MorselArgumentStateBufferPreparedOutput$();
    }

    private MorselArgumentStateBufferPreparedOutput$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselArgumentStateBufferPreparedOutput";
    }

    public MorselArgumentStateBufferPreparedOutput apply( final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink,
            final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data )
    {
        return new MorselArgumentStateBufferPreparedOutput( sink, data );
    }

    public Option<Tuple2<Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>> unapply(
            final MorselArgumentStateBufferPreparedOutput x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.sink(), x$0.data() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
