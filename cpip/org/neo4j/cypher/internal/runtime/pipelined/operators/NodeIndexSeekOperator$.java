package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekMode;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeek.;

public final class NodeIndexSeekOperator$
{
    public static NodeIndexSeekOperator$ MODULE$;

    static
    {
        new NodeIndexSeekOperator$();
    }

    private NodeIndexSeekOperator$()
    {
        MODULE$ = this;
    }

    public IndexSeekMode $lessinit$greater$default$8()
    {
        return .MODULE$;
    }
}
