package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.operations.CypherFunctions;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class UnwindOperatorTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final Expression rawListExpression;
    private final int offset;
    private final InstanceField cursorField;
    private IntermediateExpression listExpression;

    public UnwindOperatorTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost, final boolean isHead,
            final Expression rawListExpression, final int offset, final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, isHead );
        this.innermost = innermost;
        this.rawListExpression = rawListExpression;
        this.offset = offset;
        this.cursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ));
    }

    private InstanceField cursorField()
    {
        return this.cursorField;
    }

    private IntermediateExpression listExpression()
    {
        return this.listExpression;
    }

    private void listExpression_$eq( final IntermediateExpression x$1 )
    {
        this.listExpression = x$1;
    }

    public final String scopeId()
    {
        return (new StringBuilder( 6 )).append( "unwind" ).append( super.id() ).toString();
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.cursorField()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return super.inner().genSetExecutionEvent( event );
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.listExpression()}) ));
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        this.listExpression_$eq( (IntermediateExpression) super.codeGen().intermediateCompileExpression( this.rawListExpression ).getOrElse( () -> {
            throw new CantCompileQueryException(
                    (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( this.rawListExpression ).toString() );
        } ) );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.cursorField(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                    "apply", scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType(
            IteratorCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                    "asList", scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..MODULE$.classType(
            ListValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.listExpression(), ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
            OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.cursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loop(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                    super.codeGen().copyFromInput( super.codeGen().inputSlotConfiguration().numberOfLongs(),
                            super.codeGen().inputSlotConfiguration().numberOfReferences() ),
                    super.codeGen().setRefAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursorField() ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "value", scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ),
            scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.cursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class )))),this.endInnerLoop()}))))})))
        ;
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }
}
