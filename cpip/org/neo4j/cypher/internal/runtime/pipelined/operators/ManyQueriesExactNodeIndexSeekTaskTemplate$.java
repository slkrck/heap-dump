package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.runtime.makeValueNeoSafe.;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.IndexQuery.ExactPredicate;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.storable.FloatingPointValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;

public final class ManyQueriesExactNodeIndexSeekTaskTemplate$
{
    public static ManyQueriesExactNodeIndexSeekTaskTemplate$ MODULE$;

    static
    {
        new ManyQueriesExactNodeIndexSeekTaskTemplate$();
    }

    private final Comparator<ExactPredicate> ASCENDING;
    private final Comparator<ExactPredicate> DESCENDING;
    private final Method nextMethod;

    private ManyQueriesExactNodeIndexSeekTaskTemplate$()
    {
        MODULE$ = this;
        this.ASCENDING = new Comparator<ExactPredicate>()
        {
            public Comparator<ExactPredicate> reversed()
            {
                return super.reversed();
            }

            public Comparator<ExactPredicate> thenComparing( final Comparator<? super ExactPredicate> x$1 )
            {
                return super.thenComparing( x$1 );
            }

            public <U> Comparator<ExactPredicate> thenComparing( final Function<? super ExactPredicate,? extends U> x$1, final Comparator<? super U> x$2 )
            {
                return super.thenComparing( x$1, x$2 );
            }

            public <U extends Comparable<? super U>> Comparator<ExactPredicate> thenComparing( final Function<? super ExactPredicate,? extends U> x$1 )
            {
                return super.thenComparing( x$1 );
            }

            public Comparator<ExactPredicate> thenComparingInt( final ToIntFunction<? super ExactPredicate> x$1 )
            {
                return super.thenComparingInt( x$1 );
            }

            public Comparator<ExactPredicate> thenComparingLong( final ToLongFunction<? super ExactPredicate> x$1 )
            {
                return super.thenComparingLong( x$1 );
            }

            public Comparator<ExactPredicate> thenComparingDouble( final ToDoubleFunction<? super ExactPredicate> x$1 )
            {
                return super.thenComparingDouble( x$1 );
            }

            public int compare( final ExactPredicate o1, final ExactPredicate o2 )
            {
                return Values.COMPARATOR.compare( o1.value(), o2.value() );
            }
        };
        this.DESCENDING = new Comparator<ExactPredicate>()
        {
            public Comparator<ExactPredicate> reversed()
            {
                return super.reversed();
            }

            public Comparator<ExactPredicate> thenComparing( final Comparator<? super ExactPredicate> x$1 )
            {
                return super.thenComparing( x$1 );
            }

            public <U> Comparator<ExactPredicate> thenComparing( final Function<? super ExactPredicate,? extends U> x$1, final Comparator<? super U> x$2 )
            {
                return super.thenComparing( x$1, x$2 );
            }

            public <U extends Comparable<? super U>> Comparator<ExactPredicate> thenComparing( final Function<? super ExactPredicate,? extends U> x$1 )
            {
                return super.thenComparing( x$1 );
            }

            public Comparator<ExactPredicate> thenComparingInt( final ToIntFunction<? super ExactPredicate> x$1 )
            {
                return super.thenComparingInt( x$1 );
            }

            public Comparator<ExactPredicate> thenComparingLong( final ToLongFunction<? super ExactPredicate> x$1 )
            {
                return super.thenComparingLong( x$1 );
            }

            public Comparator<ExactPredicate> thenComparingDouble( final ToDoubleFunction<? super ExactPredicate> x$1 )
            {
                return super.thenComparingDouble( x$1 );
            }

            public int compare( final ExactPredicate o1, final ExactPredicate o2 )
            {
                return -Values.COMPARATOR.compare( o1.value(), o2.value() );
            }
        };
        this.nextMethod = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType(
            ManyQueriesExactNodeIndexSeekTaskTemplate.class ), scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
        MODULE$.classType( IndexReadSession.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( ExactPredicateIterator.class ), scala.reflect.ManifestFactory..MODULE$.classType( Read.class ));
    }

    public boolean next( final IndexReadSession index, final NodeValueIndexCursor cursor, final ExactPredicateIterator queries, final Read read )
    {
        label33:
        while ( !cursor.next() )
        {
            if ( !queries.hasNext() )
            {
                return false;
            }

            boolean var5 = true;

            while ( true )
            {
                while ( true )
                {
                    if ( !var5 )
                    {
                        continue label33;
                    }

                    ExactPredicate indexQuery = queries.next();
                    if ( indexQuery.value() != Values.NO_VALUE &&
                            (!(indexQuery.value() instanceof FloatingPointValue) || !((FloatingPointValue) indexQuery.value()).isNaN()) )
                    {
                        read.nodeIndexSeek( index, cursor, IndexOrder.NONE, false, new IndexQuery[]{indexQuery} );
                        var5 = false;
                    }
                    else
                    {
                        var5 = queries.hasNext();
                    }
                }
            }
        }

        return true;
    }

    private Comparator<ExactPredicate> ASCENDING()
    {
        return this.ASCENDING;
    }

    private Comparator<ExactPredicate> DESCENDING()
    {
        return this.DESCENDING;
    }

    private ExactPredicate[] computePredicates( final int propertyKey, final ListValue list )
    {
        ListValue itemsToLookFor = list.distinct();
        int size = itemsToLookFor.size();
        ExactPredicate[] predicates = new ExactPredicate[size];

        for ( int i = 0; i < size; ++i )
        {
            predicates[i] = IndexQuery.exact( propertyKey,.MODULE$.apply( itemsToLookFor.value( i ) ));
        }

        return predicates;
    }

    public ExactPredicateIterator unorderedQueryIterator( final int propertyKey, final ListValue list )
    {
        return new ExactPredicateIterator( propertyKey, this.computePredicates( propertyKey, list ) );
    }

    public ExactPredicateIterator ascendingQueryIterator( final int propertyKey, final ListValue list )
    {
        ExactPredicate[] predicates = this.computePredicates( propertyKey, list );
        Arrays.sort( (Object[]) predicates, this.ASCENDING() );
        return new ExactPredicateIterator( propertyKey, predicates );
    }

    public ExactPredicateIterator descendingQueryIterator( final int propertyKey, final ListValue list )
    {
        ExactPredicate[] predicates = this.computePredicates( propertyKey, list );
        Arrays.sort( (Object[]) predicates, this.DESCENDING() );
        return new ExactPredicateIterator( propertyKey, predicates );
    }

    public Method nextMethod()
    {
        return this.nextMethod;
    }
}
