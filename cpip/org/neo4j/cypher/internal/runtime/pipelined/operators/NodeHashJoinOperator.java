package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.collections.api.multimap.list.MutableListMultimap;
import org.eclipse.collections.impl.factory.Multimaps;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.runtime.slotted.pipes.NodeHashJoinSlottedPipe$;
import org.neo4j.values.storable.LongArray;
import org.neo4j.values.storable.Values;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeHashJoinOperator implements Operator, OperatorState
{
    public final int[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$rhsOffsets;
    public final Tuple2<Object,Object>[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$longsToCopy;
    public final Tuple2<Object,Object>[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$refsToCopy;
    public final Tuple2<Object,Object>[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$cachedPropertiesToCopy;
    private final WorkIdentity workIdentity;
    private final int lhsArgumentStateMapId;
    private final int rhsArgumentStateMapId;
    private final int[] lhsOffsets;

    public NodeHashJoinOperator( final WorkIdentity workIdentity, final int lhsArgumentStateMapId, final int rhsArgumentStateMapId, final int[] lhsOffsets,
            final int[] rhsOffsets, final SlotConfiguration slots, final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy,
            final Tuple2<Object,Object>[] cachedPropertiesToCopy )
    {
        this.workIdentity = workIdentity;
        this.lhsArgumentStateMapId = lhsArgumentStateMapId;
        this.rhsArgumentStateMapId = rhsArgumentStateMapId;
        this.lhsOffsets = lhsOffsets;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$rhsOffsets = rhsOffsets;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$longsToCopy = longsToCopy;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$refsToCopy = refsToCopy;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$cachedPropertiesToCopy = cachedPropertiesToCopy;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        argumentStateCreator.createArgumentStateMap( this.lhsArgumentStateMapId,
                new NodeHashJoinOperator.HashTableFactory( this.lhsOffsets, stateFactory.memoryTracker() ) );
        argumentStateCreator.createArgumentStateMap( this.rhsArgumentStateMapId, new ArgumentStateBuffer.Factory( stateFactory ) );
        return this;
    }

    public IndexedSeq<ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,NodeHashJoinOperator.HashTable>> nextTasks( final QueryContext context,
            final QueryState state, final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        Buffers.AccumulatorAndMorsel accAndMorsel = operatorInput.takeAccumulatorAndMorsel();
        return accAndMorsel != null ? .MODULE$.wrapRefArray( (Object[]) ((Object[]) (new NodeHashJoinOperator.OTask[]{
            new NodeHashJoinOperator.OTask( this, (NodeHashJoinOperator.HashTable) accAndMorsel.acc(), accAndMorsel.morsel() )})) ) :null;
    }

    public static class ConcurrentHashTable extends NodeHashJoinOperator.HashTable
    {
        private final long argumentRowId;
        private final int[] lhsOffsets;
        private final long[] argumentRowIdsForReducers;
        private final ConcurrentHashMap<LongArray,ConcurrentLinkedQueue<MorselExecutionContext>> table;

        public ConcurrentHashTable( final long argumentRowId, final int[] lhsOffsets, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.lhsOffsets = lhsOffsets;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.table = new ConcurrentHashMap();
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private ConcurrentHashMap<LongArray,ConcurrentLinkedQueue<MorselExecutionContext>> table()
        {
            return this.table;
        }

        public void update( final MorselExecutionContext morsel )
        {
            for ( ; morsel.isValidRow(); morsel.moveToNextRow() )
            {
                long[] key = new long[this.lhsOffsets.length];
                NodeHashJoinSlottedPipe$.MODULE$.fillKeyArray( morsel, key, this.lhsOffsets );
                if ( !NullChecker$.MODULE$.entityIsNull( key[0] ) )
                {
                    ConcurrentLinkedQueue lhsRows = (ConcurrentLinkedQueue) this.table().computeIfAbsent( Values.longArray( key ), ( x$1 ) -> {
                        return new ConcurrentLinkedQueue();
                    } );
                    BoxesRunTime.boxToBoolean( lhsRows.add( morsel.shallowCopy() ) );
                }
                else
                {
                    BoxedUnit var10000 = BoxedUnit.UNIT;
                }
            }
        }

        public Iterator<MorselExecutionContext> lhsRows( final LongArray nodeIds )
        {
            ConcurrentLinkedQueue lhsRows = (ConcurrentLinkedQueue) this.table().get( nodeIds );
            return lhsRows == null ? Collections.emptyIterator() : lhsRows.iterator();
        }
    }

    public abstract static class HashTable implements ArgumentStateMap.MorselAccumulator<MorselExecutionContext>
    {
        public abstract Iterator<MorselExecutionContext> lhsRows( final LongArray nodeIds );
    }

    public static class HashTableFactory implements ArgumentStateMap.ArgumentStateFactory<NodeHashJoinOperator.HashTable>
    {
        private final int[] lhsOffsets;
        private final QueryMemoryTracker memoryTracker;

        public HashTableFactory( final int[] lhsOffsets, final QueryMemoryTracker memoryTracker )
        {
            this.lhsOffsets = lhsOffsets;
            this.memoryTracker = memoryTracker;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public NodeHashJoinOperator.HashTable newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new NodeHashJoinOperator.StandardHashTable( argumentRowId, this.lhsOffsets, argumentRowIdsForReducers, this.memoryTracker );
        }

        public NodeHashJoinOperator.HashTable newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new NodeHashJoinOperator.ConcurrentHashTable( argumentRowId, this.lhsOffsets, argumentRowIdsForReducers );
        }
    }

    public static class StandardHashTable extends NodeHashJoinOperator.HashTable
    {
        private final long argumentRowId;
        private final int[] lhsOffsets;
        private final long[] argumentRowIdsForReducers;
        private final QueryMemoryTracker memoryTracker;
        private final MutableListMultimap<LongArray,MorselExecutionContext> table;

        public StandardHashTable( final long argumentRowId, final int[] lhsOffsets, final long[] argumentRowIdsForReducers,
                final QueryMemoryTracker memoryTracker )
        {
            this.argumentRowId = argumentRowId;
            this.lhsOffsets = lhsOffsets;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.memoryTracker = memoryTracker;
            this.table = Multimaps.mutable.list.empty();
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private MutableListMultimap<LongArray,MorselExecutionContext> table()
        {
            return this.table;
        }

        public void update( final MorselExecutionContext morsel )
        {
            for ( ; morsel.isValidRow(); morsel.moveToNextRow() )
            {
                long[] key = new long[this.lhsOffsets.length];
                NodeHashJoinSlottedPipe$.MODULE$.fillKeyArray( morsel, key, this.lhsOffsets );
                if ( !NullChecker$.MODULE$.entityIsNull( key[0] ) )
                {
                    MorselExecutionContext view = morsel.view( morsel.getCurrentRow(), morsel.getCurrentRow() + 1 );
                    this.table().put( Values.longArray( key ), view );
                    this.memoryTracker.allocated( view );
                }
            }
        }

        public Iterator<MorselExecutionContext> lhsRows( final LongArray nodeIds )
        {
            return this.table().get( nodeIds ).iterator();
        }
    }

    public class OTask extends InputLoopTask implements ContinuableOperatorTaskWithMorselAndAccumulator<MorselExecutionContext,NodeHashJoinOperator.HashTable>
    {
        private final NodeHashJoinOperator.HashTable accumulator;
        private final MorselExecutionContext rhsRow;
        private final MorselExecutionContext inputMorsel;
        private final long[] key;
        private Iterator<MorselExecutionContext> lhsRows;

        public OTask( final NodeHashJoinOperator $outer, final NodeHashJoinOperator.HashTable accumulator, final MorselExecutionContext rhsRow )
        {
            this.accumulator = accumulator;
            this.rhsRow = rhsRow;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                ContinuableOperatorTaskWithAccumulator.$init$( this );
                ContinuableOperatorTaskWithMorselAndAccumulator.$init$( this );
                this.inputMorsel = rhsRow;
                this.key = new long[$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$rhsOffsets.length];
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorselAndAccumulator.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorselAndAccumulator.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorselAndAccumulator.producingWorkUnitEvent$( this );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            ContinuableOperatorTaskWithAccumulator.setExecutionEvent$( this, event );
        }

        public void closeCursors( final QueryResources resources )
        {
            ContinuableOperatorTaskWithAccumulator.closeCursors$( this, resources );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithAccumulator.estimatedHeapUsage$( this );
        }

        public NodeHashJoinOperator.HashTable accumulator()
        {
            return this.accumulator;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$OTask$$$outer().workIdentity();
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public String toString()
        {
            return "NodeHashJoinTask";
        }

        private Iterator<MorselExecutionContext> lhsRows()
        {
            return this.lhsRows;
        }

        private void lhsRows_$eq( final Iterator<MorselExecutionContext> x$1 )
        {
            this.lhsRows = x$1;
        }

        private long[] key()
        {
            return this.key;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            NodeHashJoinSlottedPipe$.MODULE$.fillKeyArray( this.rhsRow, this.key(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$rhsOffsets );
            this.lhsRows_$eq( this.accumulator().lhsRows( Values.longArray( this.key() ) ) );
            return true;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.lhsRows().hasNext() )
            {
                outputRow.copyFrom( (MorselExecutionContext) this.lhsRows().next() );
                NodeHashJoinSlottedPipe$.MODULE$.copyDataFromRhs(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$longsToCopy,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$refsToCopy,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeHashJoinOperator$$cachedPropertiesToCopy,
                        outputRow, this.rhsRow );
                outputRow.moveToNextRow();
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
        }
    }
}
