package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.CSVResources;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.NullPipeDecorator.;
import org.neo4j.cypher.internal.runtime.interpreted.profiler.InterpretedProfileInformation;
import org.neo4j.cypher.internal.runtime.interpreted.profiler.Profiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.Option;
import scala.Tuple2;
import scala.collection.TraversableOnce;
import scala.runtime.BoxesRunTime;

public final class SlottedPipeOperator$
{
    public static SlottedPipeOperator$ MODULE$;

    static
    {
        new SlottedPipeOperator$();
    }

    private SlottedPipeOperator$()
    {
        MODULE$ = this;
    }

    public FeedPipeQueryState createFeedPipeQueryState( final MorselExecutionContext inputMorsel, final QueryContext queryContext,
            final QueryState morselQueryState, final QueryResources resources, final QueryMemoryTracker memoryTracker )
    {
        Tuple2 var10000;
        if ( morselQueryState.doProfile() )
        {
            InterpretedProfileInformation profileInformation = new InterpretedProfileInformation();
            var10000 = new Tuple2( new Profiler( queryContext.transactionalContext().databaseInfo(), profileInformation ), profileInformation );
        }
        else
        {
            var10000 = new Tuple2(.MODULE$, (Object) null);
        }

        Tuple2 var8 = var10000;
        if ( var8 != null )
        {
            PipeDecorator pipeDecorator = (PipeDecorator) var8._1();
            InterpretedProfileInformation profileInformation = (InterpretedProfileInformation) var8._2();
            Tuple2 var6 = new Tuple2( pipeDecorator, profileInformation );
            PipeDecorator pipeDecorator = (PipeDecorator) var6._1();
            InterpretedProfileInformation profileInformation = (InterpretedProfileInformation) var6._2();
            ExternalCSVResource externalResource = new CSVResources( queryContext.resources() );
            AnyValue[] x$5 = morselQueryState.params();
            ExpressionCursors x$6 = resources.expressionCursors();
            IndexReadSession[] x$7 = morselQueryState.queryIndexes();
            AnyValue[] x$8 = resources.expressionVariables( morselQueryState.nExpressionSlots() );
            QuerySubscriber x$9 = morselQueryState.subscriber();
            Option x$14 = FeedPipeQueryState$.MODULE$.$lessinit$greater$default$10();
            SingleThreadedLRUCache x$15 = FeedPipeQueryState$.MODULE$.$lessinit$greater$default$11();
            boolean x$16 = FeedPipeQueryState$.MODULE$.$lessinit$greater$default$12();
            boolean x$17 = FeedPipeQueryState$.MODULE$.$lessinit$greater$default$13();
            InputDataStream x$18 = FeedPipeQueryState$.MODULE$.$lessinit$greater$default$14();
            return new FeedPipeQueryState( queryContext, externalResource, x$5, x$6, x$7, x$8, x$9, memoryTracker, pipeDecorator, x$14, x$15, x$16, x$17, x$18,
                    profileInformation, inputMorsel );
        }
        else
        {
            throw new MatchError( var8 );
        }
    }

    public void updateProfileEvent( final OperatorProfileEvent profileEvent, final InterpretedProfileInformation profileInformation )
    {
        if ( profileEvent != null )
        {
            long dbHits = BoxesRunTime.unboxToLong( ((TraversableOnce) profileInformation.dbHitsMap().values().map( ( x$2 ) -> {
                return BoxesRunTime.boxToLong( $anonfun$updateProfileEvent$1( x$2 ) );
            }, scala.collection.Iterable..MODULE$.canBuildFrom()) ).sum( scala.math.Numeric.LongIsIntegral..MODULE$));
            profileEvent.dbHits( (int) dbHits );
        }
    }
}
