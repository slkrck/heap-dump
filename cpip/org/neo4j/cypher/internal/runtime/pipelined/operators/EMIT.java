package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class EMIT
{
    public static String toString()
    {
        return EMIT$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return EMIT$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return EMIT$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return EMIT$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return EMIT$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return EMIT$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return EMIT$.MODULE$.productPrefix();
    }
}
