package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.logical.plans.SeekableArgs;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.SeekArgs;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.values.AnyValue;
import scala.Array.;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class RelationshipByIdSeekOperator implements StreamingOperator
{
    public final SeekArgs org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$$relId;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$$argumentSize;
    private final WorkIdentity workIdentity;

    public RelationshipByIdSeekOperator( final WorkIdentity workIdentity, final int relationship, final int startNode, final int endNode, final SeekArgs relId,
            final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$$relId = relId;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$$argumentSize = argumentSize;
        StreamingOperator.$init$( this );
    }

    public static OperatorTaskTemplate taskTemplate( final boolean isDirected, final OperatorTaskTemplate inner, final int id,
            final DelegateOperatorTaskTemplate innermost, final int relationshipOffset, final int fromOffset, final int toOffset, final SeekableArgs relIds,
            final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        return RelationshipByIdSeekOperator$.MODULE$.taskTemplate( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9 );
    }

    public static Method asIdMethod()
    {
        return RelationshipByIdSeekOperator$.MODULE$.asIdMethod();
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public abstract class RelationshipByIdTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private Iterator<AnyValue> ids;
        private RelationshipScanCursor cursor;

        public RelationshipByIdTask( final RelationshipByIdSeekOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public Iterator<AnyValue> ids()
        {
            return this.ids;
        }

        public void ids_$eq( final Iterator<AnyValue> x$1 )
        {
            this.ids = x$1;
        }

        public RelationshipScanCursor cursor()
        {
            return this.cursor;
        }

        public void cursor_$eq( final RelationshipScanCursor x$1 )
        {
            this.cursor = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            this.cursor_$eq( (RelationshipScanCursor) resources.cursorPools().relationshipScanCursorPool().allocateAndTrace() );
            SlottedQueryState queryState =
                    new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(), (IndexReadSession[]).
            MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
            resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
            MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
            ;
            initExecutionContext.copyFrom( this.inputMorsel(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$RelationshipByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$$argumentSize.nLongs(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$RelationshipByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$$argumentSize.nReferences() );
            this.ids_$eq(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$RelationshipByIdTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$$relId.expressions(
                            initExecutionContext, queryState ).iterator() );
            return true;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipByIdSeekOperator$RelationshipByIdTask$$$outer().workIdentity();
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            resources.cursorPools().relationshipScanCursorPool().free( this.cursor() );
            this.cursor_$eq( (RelationshipScanCursor) null );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.cursor() != null )
            {
                this.cursor().setTracer( event );
            }
        }
    }
}
