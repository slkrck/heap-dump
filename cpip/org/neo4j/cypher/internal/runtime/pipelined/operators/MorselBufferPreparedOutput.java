package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselBufferPreparedOutput implements PreparedOutput, Product, Serializable
{
    private final int bufferId;
    private final ExecutionState executionState;
    private final int pipelineId;
    private final MorselExecutionContext outputMorsel;

    public MorselBufferPreparedOutput( final int bufferId, final ExecutionState executionState, final int pipelineId,
            final MorselExecutionContext outputMorsel )
    {
        this.bufferId = bufferId;
        this.executionState = executionState;
        this.pipelineId = pipelineId;
        this.outputMorsel = outputMorsel;
        Product.$init$( this );
    }

    public static Option<Tuple4<BufferId,ExecutionState,PipelineId,MorselExecutionContext>> unapply( final MorselBufferPreparedOutput x$0 )
    {
        return MorselBufferPreparedOutput$.MODULE$.unapply( var0 );
    }

    public static MorselBufferPreparedOutput apply( final int bufferId, final ExecutionState executionState, final int pipelineId,
            final MorselExecutionContext outputMorsel )
    {
        return MorselBufferPreparedOutput$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<BufferId,ExecutionState,PipelineId,MorselExecutionContext>,MorselBufferPreparedOutput> tupled()
    {
        return MorselBufferPreparedOutput$.MODULE$.tupled();
    }

    public static Function1<BufferId,Function1<ExecutionState,Function1<PipelineId,Function1<MorselExecutionContext,MorselBufferPreparedOutput>>>> curried()
    {
        return MorselBufferPreparedOutput$.MODULE$.curried();
    }

    public int bufferId()
    {
        return this.bufferId;
    }

    public ExecutionState executionState()
    {
        return this.executionState;
    }

    public int pipelineId()
    {
        return this.pipelineId;
    }

    public MorselExecutionContext outputMorsel()
    {
        return this.outputMorsel;
    }

    public void produce()
    {
        this.executionState().putMorsel( this.pipelineId(), this.bufferId(), this.outputMorsel() );
    }

    public MorselBufferPreparedOutput copy( final int bufferId, final ExecutionState executionState, final int pipelineId,
            final MorselExecutionContext outputMorsel )
    {
        return new MorselBufferPreparedOutput( bufferId, executionState, pipelineId, outputMorsel );
    }

    public int copy$default$1()
    {
        return this.bufferId();
    }

    public ExecutionState copy$default$2()
    {
        return this.executionState();
    }

    public int copy$default$3()
    {
        return this.pipelineId();
    }

    public MorselExecutionContext copy$default$4()
    {
        return this.outputMorsel();
    }

    public String productPrefix()
    {
        return "MorselBufferPreparedOutput";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new BufferId( this.bufferId() );
            break;
        case 1:
            var10000 = this.executionState();
            break;
        case 2:
            var10000 = new PipelineId( this.pipelineId() );
            break;
        case 3:
            var10000 = this.outputMorsel();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselBufferPreparedOutput;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label66:
            {
                boolean var2;
                if ( x$1 instanceof MorselBufferPreparedOutput )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label49:
                    {
                        MorselBufferPreparedOutput var4 = (MorselBufferPreparedOutput) x$1;
                        if ( this.bufferId() == var4.bufferId() )
                        {
                            label47:
                            {
                                ExecutionState var10000 = this.executionState();
                                ExecutionState var5 = var4.executionState();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label47;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label47;
                                }

                                if ( this.pipelineId() == var4.pipelineId() )
                                {
                                    label41:
                                    {
                                        MorselExecutionContext var7 = this.outputMorsel();
                                        MorselExecutionContext var6 = var4.outputMorsel();
                                        if ( var7 == null )
                                        {
                                            if ( var6 != null )
                                            {
                                                break label41;
                                            }
                                        }
                                        else if ( !var7.equals( var6 ) )
                                        {
                                            break label41;
                                        }

                                        if ( var4.canEqual( this ) )
                                        {
                                            var8 = true;
                                            break label49;
                                        }
                                    }
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label66;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
