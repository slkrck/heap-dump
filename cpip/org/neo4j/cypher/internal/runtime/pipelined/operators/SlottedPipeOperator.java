package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.profiler.InterpretedProfileInformation;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescription;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class SlottedPipeOperator
{
    private final WorkIdentityMutableDescription workIdentity;
    private Pipe _pipe;

    public SlottedPipeOperator( final WorkIdentityMutableDescription workIdentity, final Pipe initialPipe )
    {
        this.workIdentity = workIdentity;
        this._pipe = initialPipe;
    }

    public static void updateProfileEvent( final OperatorProfileEvent profileEvent, final InterpretedProfileInformation profileInformation )
    {
        SlottedPipeOperator$.MODULE$.updateProfileEvent( var0, var1 );
    }

    public static FeedPipeQueryState createFeedPipeQueryState( final MorselExecutionContext inputMorsel, final QueryContext queryContext,
            final QueryState morselQueryState, final QueryResources resources, final QueryMemoryTracker memoryTracker )
    {
        return SlottedPipeOperator$.MODULE$.createFeedPipeQueryState( var0, var1, var2, var3, var4 );
    }

    public WorkIdentityMutableDescription workIdentity()
    {
        return this.workIdentity;
    }

    private Pipe _pipe()
    {
        return this._pipe;
    }

    private void _pipe_$eq( final Pipe x$1 )
    {
        this._pipe = x$1;
    }

    public Pipe pipe()
    {
        return this._pipe();
    }

    public void setPipe( final Pipe newPipe )
    {
        this._pipe_$eq( newPipe );
    }
}
