package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Arrays;
import java.util.Comparator;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap$;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.ColumnOrder;
import scala.Option;
import scala.Some;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SortPreOperator implements OutputOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$$argumentSlotOffset;
    public final Seq<ColumnOrder> org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$$orderBy;
    private final WorkIdentity workIdentity;
    private final int outputBufferId;

    public SortPreOperator( final WorkIdentity workIdentity, final int argumentSlotOffset, final int outputBufferId, final Seq<ColumnOrder> orderBy )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$$argumentSlotOffset = argumentSlotOffset;
        this.outputBufferId = outputBufferId;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$$orderBy = orderBy;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public String toString()
    {
        return "SortPre";
    }

    public Option<BufferId> outputBuffer()
    {
        return new Some( new BufferId( this.outputBufferId ) );
    }

    public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
    {
        return new SortPreOperator.State( this, executionState.getSink( pipelineId, this.outputBufferId ) );
    }

    public class PreSortedOutput implements PreparedOutput
    {
        private final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> preSorted;
        private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink;

        public PreSortedOutput( final SortPreOperator $outer, final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> preSorted,
                final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink )
        {
            this.preSorted = preSorted;
            this.sink = sink;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public void produce()
        {
            this.sink.put( this.preSorted );
        }
    }

    public class State implements OutputOperatorState
    {
        private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink;

        public State( final SortPreOperator $outer, final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink )
        {
            this.sink = sink;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OutputOperatorState.$init$( this );
            }
        }

        public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                final QueryResources resources, final QueryProfiler queryProfiler )
        {
            return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public boolean canContinueOutput()
        {
            return OutputOperatorState.canContinueOutput$( this );
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$State$$$outer().workIdentity();
        }

        public boolean trackTime()
        {
            return true;
        }

        public SortPreOperator.PreSortedOutput prepareOutput( final MorselExecutionContext morsel, final QueryContext context, final QueryState state,
                final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
        {
            MorselExecutionContext rowCloneForComparators = morsel.shallowCopy();
            Comparator comparator =
                    (Comparator) ((TraversableOnce) this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$$orderBy.map(
                            ( order ) -> {
                                return MorselSorting$.MODULE$.compareMorselIndexesByColumnOrder( rowCloneForComparators, order );
                            },.MODULE$.canBuildFrom())).reduce( ( a, b ) -> {
            return a.thenComparing( b );
        } );
            IndexedSeq preSorted = ArgumentStateMap$.MODULE$.map(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$$argumentSlotOffset,
                    morsel, ( morselView ) -> {
                        return this.sortInPlace( morselView, comparator );
                    } );
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$State$$$outer().new PreSortedOutput(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$SortPreOperator$State$$$outer(), preSorted, this.sink );
        }

        private MorselExecutionContext sortInPlace( final MorselExecutionContext morsel, final Comparator<Integer> comparator )
        {
            Integer[] outputToInputIndexes = MorselSorting$.MODULE$.createMorselIndexesArray( morsel );
            Arrays.sort( (Object[]) outputToInputIndexes, comparator );
            MorselSorting$.MODULE$.createSortedMorselData( morsel, outputToInputIndexes );
            return morsel;
        }
    }
}
