package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.cypher.internal.DefaultComparatorTopTable;
import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap$;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.ColumnOrder;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class TopOperator implements Product, Serializable
{
    private final WorkIdentity workIdentity;
    private final Seq<ColumnOrder> orderBy;
    private final Expression countExpression;
    private final Comparator<MorselExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$$comparator;

    public TopOperator( final WorkIdentity workIdentity, final Seq<ColumnOrder> orderBy, final Expression countExpression )
    {
        this.workIdentity = workIdentity;
        this.orderBy = orderBy;
        this.countExpression = countExpression;
        Product.$init$( this );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$$comparator = MorselSorting$.MODULE$.createComparator( orderBy );
    }

    public static Option<Tuple3<WorkIdentity,Seq<ColumnOrder>,Expression>> unapply( final TopOperator x$0 )
    {
        return TopOperator$.MODULE$.unapply( var0 );
    }

    public static TopOperator apply( final WorkIdentity workIdentity, final Seq<ColumnOrder> orderBy, final Expression countExpression )
    {
        return TopOperator$.MODULE$.apply( var0, var1, var2 );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public Seq<ColumnOrder> orderBy()
    {
        return this.orderBy;
    }

    public Expression countExpression()
    {
        return this.countExpression;
    }

    public String toString()
    {
        return "TopMerge";
    }

    public Comparator<MorselExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$$comparator()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$$comparator;
    }

    public TopOperator.TopMapperOperator mapper( final int argumentSlotOffset, final int outputBufferId )
    {
        return new TopOperator.TopMapperOperator( this, argumentSlotOffset, outputBufferId );
    }

    public TopOperator.TopReduceOperator reducer( final int argumentStateMapId )
    {
        return new TopOperator.TopReduceOperator( this, argumentStateMapId );
    }

    public TopOperator copy( final WorkIdentity workIdentity, final Seq<ColumnOrder> orderBy, final Expression countExpression )
    {
        return new TopOperator( workIdentity, orderBy, countExpression );
    }

    public WorkIdentity copy$default$1()
    {
        return this.workIdentity();
    }

    public Seq<ColumnOrder> copy$default$2()
    {
        return this.orderBy();
    }

    public Expression copy$default$3()
    {
        return this.countExpression();
    }

    public String productPrefix()
    {
        return "TopOperator";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.workIdentity();
            break;
        case 1:
            var10000 = this.orderBy();
            break;
        case 2:
            var10000 = this.countExpression();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof TopOperator;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof TopOperator )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            TopOperator var4 = (TopOperator) x$1;
                            WorkIdentity var10000 = this.workIdentity();
                            WorkIdentity var5 = var4.workIdentity();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            Seq var8 = this.orderBy();
                            Seq var6 = var4.orderBy();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            Expression var9 = this.countExpression();
                            Expression var7 = var4.countExpression();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }

    public static class ConcurrentTopTable extends TopOperator.TopTable
    {
        private final long argumentRowId;
        private final long[] argumentRowIdsForReducers;
        private final Comparator<MorselExecutionContext> comparator;
        private final int limit;
        private final ConcurrentHashMap<Object,DefaultComparatorTopTable<MorselExecutionContext>> topTableByThread;

        public ConcurrentTopTable( final long argumentRowId, final long[] argumentRowIdsForReducers, final Comparator<MorselExecutionContext> comparator,
                final int limit )
        {
            this.argumentRowId = argumentRowId;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.comparator = comparator;
            this.limit = limit;
            this.topTableByThread = new ConcurrentHashMap();
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private ConcurrentHashMap<Object,DefaultComparatorTopTable<MorselExecutionContext>> topTableByThread()
        {
            return this.topTableByThread;
        }

        public void update( final MorselExecutionContext morsel )
        {
            long threadId = Thread.currentThread().getId();
            DefaultComparatorTopTable topTable =
                    (DefaultComparatorTopTable) this.topTableByThread().computeIfAbsent( BoxesRunTime.boxToLong( threadId ), ( x$1 ) -> {
                        return $anonfun$update$1( this, BoxesRunTime.unboxToLong( x$1 ) );
                    } );

            while ( morsel.isValidRow() )
            {
                topTable.add( morsel.shallowCopy() );
                morsel.moveToNextRow();
            }
        }

        public void deallocateMemory()
        {
        }

        public java.util.Iterator<MorselExecutionContext> getTopRows()
        {
            java.util.Iterator topTables = this.topTableByThread().values().iterator();
            java.util.Iterator var10000;
            if ( !topTables.hasNext() )
            {
                var10000 = Collections.emptyIterator();
            }
            else
            {
                DefaultComparatorTopTable mergedTopTable = (DefaultComparatorTopTable) topTables.next();

                while ( topTables.hasNext() )
                {
                    DefaultComparatorTopTable topTable = (DefaultComparatorTopTable) topTables.next();
                    topTable.unorderedIterator().forEachRemaining( ( x$1 ) -> {
                        mergedTopTable.add( x$1 );
                    } );
                }

                mergedTopTable.sort();
                var10000 = mergedTopTable.iterator();
            }

            return var10000;
        }
    }

    public static class Factory implements ArgumentStateMap.ArgumentStateFactory<TopOperator.TopTable>
    {
        private final QueryMemoryTracker memoryTracker;
        private final Comparator<MorselExecutionContext> comparator;
        private final int limit;

        public Factory( final QueryMemoryTracker memoryTracker, final Comparator<MorselExecutionContext> comparator, final int limit )
        {
            this.memoryTracker = memoryTracker;
            this.comparator = comparator;
            this.limit = limit;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public TopOperator.TopTable newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return (TopOperator.TopTable) (this.limit <= 0 ? new TopOperator.ZeroTable( argumentRowId, argumentRowIdsForReducers )
                                                           : new TopOperator.StandardTopTable( argumentRowId, argumentRowIdsForReducers, this.memoryTracker,
                                                                   this.comparator, this.limit ));
        }

        public TopOperator.TopTable newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return (TopOperator.TopTable) (this.limit <= 0 ? new TopOperator.ZeroTable( argumentRowId, argumentRowIdsForReducers )
                                                           : new TopOperator.ConcurrentTopTable( argumentRowId, argumentRowIdsForReducers, this.comparator,
                                                                   this.limit ));
        }
    }

    public static class StandardTopTable extends TopOperator.TopTable
    {
        private final long argumentRowId;
        private final long[] argumentRowIdsForReducers;
        private final QueryMemoryTracker memoryTracker;
        private final int limit;
        private final DefaultComparatorTopTable<MorselExecutionContext> topTable;
        private long totalTopHeapUsage;
        private int morselCount;
        private long maxMorselHeapUsage;

        public StandardTopTable( final long argumentRowId, final long[] argumentRowIdsForReducers, final QueryMemoryTracker memoryTracker,
                final Comparator<MorselExecutionContext> comparator, final int limit )
        {
            this.argumentRowId = argumentRowId;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.memoryTracker = memoryTracker;
            this.limit = limit;
            this.topTable = new DefaultComparatorTopTable( comparator, limit );
            this.totalTopHeapUsage = 0L;
            this.morselCount = 0;
            this.maxMorselHeapUsage = 0L;
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private DefaultComparatorTopTable<MorselExecutionContext> topTable()
        {
            return this.topTable;
        }

        private long totalTopHeapUsage()
        {
            return this.totalTopHeapUsage;
        }

        private void totalTopHeapUsage_$eq( final long x$1 )
        {
            this.totalTopHeapUsage = x$1;
        }

        private int morselCount()
        {
            return this.morselCount;
        }

        private void morselCount_$eq( final int x$1 )
        {
            this.morselCount = x$1;
        }

        private long maxMorselHeapUsage()
        {
            return this.maxMorselHeapUsage;
        }

        private void maxMorselHeapUsage_$eq( final long x$1 )
        {
            this.maxMorselHeapUsage = x$1;
        }

        public void update( final MorselExecutionContext morsel )
        {
            for ( boolean hasAddedRow = false; morsel.isValidRow(); morsel.moveToNextRow() )
            {
                if ( this.topTable().add( morsel.shallowCopy() ) && this.memoryTracker.isEnabled() && !hasAddedRow )
                {
                    hasAddedRow = true;
                    this.morselCount_$eq( Math.min( this.morselCount() + 1, this.limit ) );
                    this.maxMorselHeapUsage_$eq( Math.max( this.maxMorselHeapUsage(), morsel.estimatedHeapUsage() ) );
                    this.memoryTracker.deallocated( this.totalTopHeapUsage() );
                    this.totalTopHeapUsage_$eq( this.maxMorselHeapUsage() * (long) this.morselCount() );
                    this.memoryTracker.allocated( this.totalTopHeapUsage() );
                }
            }
        }

        public void deallocateMemory()
        {
            this.memoryTracker.deallocated( this.totalTopHeapUsage() );
        }

        public java.util.Iterator<MorselExecutionContext> getTopRows()
        {
            this.topTable().sort();
            return this.topTable().iterator();
        }
    }

    public abstract static class TopTable implements ArgumentStateMap.MorselAccumulator<MorselExecutionContext>
    {
        private boolean sorted = false;

        private boolean sorted()
        {
            return this.sorted;
        }

        private void sorted_$eq( final boolean x$1 )
        {
            this.sorted = x$1;
        }

        public final java.util.Iterator<MorselExecutionContext> topRows()
        {
            if ( this.sorted() )
            {
                throw new IllegalArgumentException( "Method should not be called more than once, per top table instance" );
            }
            else
            {
                java.util.Iterator rows = this.getTopRows();
                this.sorted_$eq( true );
                return rows;
            }
        }

        public abstract void deallocateMemory();

        public abstract java.util.Iterator<MorselExecutionContext> getTopRows();
    }

    public static class ZeroTable extends TopOperator.TopTable implements Product, Serializable
    {
        private final long argumentRowId;
        private final long[] argumentRowIdsForReducers;

        {
            throw new IllegalStateException( "Top table method should never be called with LIMIT 0" );
        }

        public ZeroTable( final long argumentRowId, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            Product.$init$( this );
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        public java.util.Iterator<MorselExecutionContext> getTopRows()
        {
            return Collections.emptyIterator();
        }

      private scala.runtime.Nothing.error()

        public void update( final MorselExecutionContext data )
        {
            throw this.error();
        }

        public void deallocateMemory()
        {
        }

        public TopOperator.ZeroTable copy( final long argumentRowId, final long[] argumentRowIdsForReducers )
        {
            return new TopOperator.ZeroTable( argumentRowId, argumentRowIdsForReducers );
        }

        public long copy$default$1()
        {
            return this.argumentRowId();
        }

        public long[] copy$default$2()
        {
            return this.argumentRowIdsForReducers();
        }

        public String productPrefix()
        {
            return "ZeroTable";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = BoxesRunTime.boxToLong( this.argumentRowId() );
                break;
            case 1:
                var10000 = this.argumentRowIdsForReducers();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof TopOperator.ZeroTable;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.longHash( this.argumentRowId() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.argumentRowIdsForReducers() ) );
            return Statics.finalizeHash( var1, 2 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label51:
                {
                    boolean var2;
                    if ( x$1 instanceof TopOperator.ZeroTable )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        TopOperator.ZeroTable var4 = (TopOperator.ZeroTable) x$1;
                        if ( this.argumentRowId() == var4.argumentRowId() && this.argumentRowIdsForReducers() == var4.argumentRowIdsForReducers() &&
                                var4.canEqual( this ) )
                        {
                            break label51;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class ZeroTable$ extends AbstractFunction2<Object,long[],TopOperator.ZeroTable> implements Serializable
    {
        public static TopOperator.ZeroTable$ MODULE$;

        static
        {
            new TopOperator.ZeroTable$();
        }

        public ZeroTable$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "ZeroTable";
        }

        public TopOperator.ZeroTable apply( final long argumentRowId, final long[] argumentRowIdsForReducers )
        {
            return new TopOperator.ZeroTable( argumentRowId, argumentRowIdsForReducers );
        }

        public Option<Tuple2<Object,long[]>> unapply( final TopOperator.ZeroTable x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :
            new Some( new Tuple2( BoxesRunTime.boxToLong( x$0.argumentRowId() ), x$0.argumentRowIdsForReducers() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public class TopMapperOperator implements OutputOperator
    {
        public final int org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$$argumentSlotOffset;
        private final int outputBufferId;

        public TopMapperOperator( final TopOperator $outer, final int argumentSlotOffset, final int outputBufferId )
        {
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$$argumentSlotOffset = argumentSlotOffset;
            this.outputBufferId = outputBufferId;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$$$outer().workIdentity();
        }

        public Option<BufferId> outputBuffer()
        {
            return new Some( new BufferId( this.outputBufferId ) );
        }

        public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
        {
            return new TopOperator.TopMapperOperator.State( this, executionState.getSink( pipelineId, this.outputBufferId ) );
        }

        public class PreTopOutput implements PreparedOutput
        {
            private final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> preTopped;
            private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink;

            public PreTopOutput( final TopOperator.TopMapperOperator $outer, final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> preTopped,
                    final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink )
            {
                this.preTopped = preTopped;
                this.sink = sink;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                }
            }

            public void produce()
            {
                this.sink.put( this.preTopped );
            }
        }

        public class State implements OutputOperatorState
        {
            private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink;

            public State( final TopOperator.TopMapperOperator $outer, final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink )
            {
                this.sink = sink;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    OutputOperatorState.$init$( this );
                }
            }

            public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                    final QueryResources resources, final QueryProfiler queryProfiler )
            {
                return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
            }

            public boolean canContinueOutput()
            {
                return OutputOperatorState.canContinueOutput$( this );
            }

            public WorkIdentity workIdentity()
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$$$outer().workIdentity();
            }

            public boolean trackTime()
            {
                return true;
            }

            public TopOperator.TopMapperOperator.PreTopOutput prepareOutput( final MorselExecutionContext morsel, final QueryContext queryContext,
                    final QueryState state, final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
            {
                long limit = LimitOperator$.MODULE$.evaluateCountValue( queryContext, state, resources,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$$$outer().countExpression() );
                IndexedSeq perArguments = limit <= 0L ? (IndexedSeq) scala.package..MODULE$.IndexedSeq().empty() :ArgumentStateMap$.MODULE$.map(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$$argumentSlotOffset,
                    morsel, ( argumentMorsel ) -> {
                        return argumentMorsel;
                    } );
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$State$$$outer().new PreTopOutput(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopMapperOperator$State$$$outer(), perArguments, this.sink );
            }
        }
    }

    public class TopReduceOperator implements Operator, ReduceOperatorState<MorselExecutionContext,TopOperator.TopTable>
    {
        private final int argumentStateMapId;

        public TopReduceOperator( final TopOperator $outer, final int argumentStateMapId )
        {
            this.argumentStateMapId = argumentStateMapId;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                ReduceOperatorState.$init$( this );
            }
        }

        public final IndexedSeq<ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,TopOperator.TopTable>> nextTasks( final QueryContext context,
                final QueryState state, final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
                final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
        {
            return ReduceOperatorState.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopReduceOperator$$$outer().workIdentity();
        }

        public ReduceOperatorState<MorselExecutionContext,TopOperator.TopTable> createState( final ArgumentStateMapCreator argumentStateCreator,
                final StateFactory stateFactory, final QueryContext queryContext, final QueryState state, final QueryResources resources )
        {
            int limit = (int) Math.min( LimitOperator$.MODULE$.evaluateCountValue( queryContext, state, resources,
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopReduceOperator$$$outer().countExpression() ), 2147483647L );
            argumentStateCreator.createArgumentStateMap( this.argumentStateMapId, new TopOperator.Factory( stateFactory.memoryTracker(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopReduceOperator$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$$comparator(),
                    limit ) );
            return this;
        }

        public IndexedSeq<ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,TopOperator.TopTable>> nextTasks( final QueryContext queryContext,
                final QueryState state, final TopOperator.TopTable input, final QueryResources resources )
        {
            return scala.Predef..
            MODULE$.wrapRefArray( (Object[]) ((Object[]) (new TopOperator.TopReduceOperator.OTask[]{new TopOperator.TopReduceOperator.OTask( this, input )})) );
        }

        public class OTask implements ContinuableOperatorTaskWithAccumulator<MorselExecutionContext,TopOperator.TopTable>
        {
            private final TopOperator.TopTable accumulator;
            private java.util.Iterator<MorselExecutionContext> sortedInputPerArgument;

            public OTask( final TopOperator.TopReduceOperator $outer, final TopOperator.TopTable accumulator )
            {
                this.accumulator = accumulator;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    OperatorTask.$init$( this );
                    ContinuableOperatorTask.$init$( this );
                    ContinuableOperatorTaskWithAccumulator.$init$( this );
                }
            }

            public void closeInput( final OperatorCloser operatorCloser )
            {
                ContinuableOperatorTaskWithAccumulator.closeInput$( this, operatorCloser );
            }

            public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
            {
                return ContinuableOperatorTaskWithAccumulator.filterCancelledArguments$( this, operatorCloser );
            }

            public WorkUnitEvent producingWorkUnitEvent()
            {
                return ContinuableOperatorTaskWithAccumulator.producingWorkUnitEvent$( this );
            }

            public void setExecutionEvent( final OperatorProfileEvent event )
            {
                ContinuableOperatorTaskWithAccumulator.setExecutionEvent$( this, event );
            }

            public void closeCursors( final QueryResources resources )
            {
                ContinuableOperatorTaskWithAccumulator.closeCursors$( this, resources );
            }

            public long estimatedHeapUsage()
            {
                return ContinuableOperatorTaskWithAccumulator.estimatedHeapUsage$( this );
            }

            public void close( final OperatorCloser operatorCloser, final QueryResources resources )
            {
                ContinuableOperatorTask.close$( this, operatorCloser, resources );
            }

            public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                    final QueryResources resources, final QueryProfiler queryProfiler )
            {
                OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
            }

            public TopOperator.TopTable accumulator()
            {
                return this.accumulator;
            }

            public WorkIdentity workIdentity()
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopReduceOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$TopOperator$TopReduceOperator$$$outer().workIdentity();
            }

            public String toString()
            {
                return "TopMergeTask";
            }

            public java.util.Iterator<MorselExecutionContext> sortedInputPerArgument()
            {
                return this.sortedInputPerArgument;
            }

            public void sortedInputPerArgument_$eq( final java.util.Iterator<MorselExecutionContext> x$1 )
            {
                this.sortedInputPerArgument = x$1;
            }

            public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
            {
                if ( this.sortedInputPerArgument() == null )
                {
                    this.sortedInputPerArgument_$eq( this.accumulator().topRows() );
                }

                while ( outputRow.isValidRow() && this.canContinue() )
                {
                    MorselExecutionContext nextRow = (MorselExecutionContext) this.sortedInputPerArgument().next();
                    outputRow.copyFrom( nextRow );
                    nextRow.moveToNextRow();
                    outputRow.moveToNextRow();
                }

                if ( !this.sortedInputPerArgument().hasNext() )
                {
                    this.accumulator().deallocateMemory();
                }

                outputRow.finishedWriting();
            }

            public boolean canContinue()
            {
                return this.sortedInputPerArgument().hasNext();
            }
        }
    }
}
