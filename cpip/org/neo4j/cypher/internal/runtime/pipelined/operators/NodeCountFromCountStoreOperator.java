package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.values.storable.Values;
import scala.Option;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeCountFromCountStoreOperator implements StreamingOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$offset;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$argumentSize;
    private final WorkIdentity workIdentity;
    private final LazyLabel[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$lazyLabels;
    private final int org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$wildCards;

    public NodeCountFromCountStoreOperator( final WorkIdentity workIdentity, final int offset, final Seq<Option<LazyLabel>> labels,
            final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$offset = offset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$argumentSize = argumentSize;
        StreamingOperator.$init$( this );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$lazyLabels =
                (LazyLabel[]) ((TraversableOnce) labels.flatten( ( xo ) -> {
                    return scala.Option..MODULE$.option2Iterable( xo );
                } )).toArray( scala.reflect.ClassTag..MODULE$.apply( LazyLabel.class ));
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$wildCards = labels.count( ( x$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$wildCards$1( x$1 ) );
        } );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public LazyLabel[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$lazyLabels()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$lazyLabels;
    }

    public int org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$wildCards()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$wildCards;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new NodeCountFromCountStoreOperator.NodeFromCountStoreTask[]{
                        new NodeCountFromCountStoreOperator.NodeFromCountStoreTask( this, inputMorsel.nextCopy() )}) ));
    }

    public class NodeFromCountStoreTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private boolean hasNext;
        private OperatorProfileEvent executionEvent;

        public NodeFromCountStoreTask( final NodeCountFromCountStoreOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                this.hasNext = false;
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public String toString()
        {
            return "NodeFromCountStoreTask";
        }

        private boolean hasNext()
        {
            return this.hasNext;
        }

        private void hasNext_$eq( final boolean x$1 )
        {
            this.hasNext = x$1;
        }

        private OperatorProfileEvent executionEvent()
        {
            return this.executionEvent;
        }

        private void executionEvent_$eq( final OperatorProfileEvent x$1 )
        {
            this.executionEvent = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            this.hasNext_$eq( true );
            return true;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().workIdentity();
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            if ( this.hasNext() )
            {
                long count = 1L;

                int i;
                for ( i = 0; i <
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$lazyLabels().length;
                        ++i )
                {
                    int idOfLabel =
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$lazyLabels()[i].getId(
                                    context );
                    if ( idOfLabel == org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.UNKNOWN()){
                    count = 0L;
                } else{
                    if ( this.executionEvent() != null )
                    {
                        this.executionEvent().dbHit();
                    }

                    count *= context.nodeCountByCountStore( idOfLabel );
                }
                }

                if ( this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$wildCards() >
                        0 )
                {
                    i = 0;
                    long wildCardCount = context.nodeCountByCountStore( org.neo4j.cypher.internal.v4_0.util.NameId..MODULE$.WILDCARD());
                    if ( this.executionEvent() != null )
                    {
                        this.executionEvent().dbHit();
                    }

                    while ( i <
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$wildCards() )
                    {
                        count *= wildCardCount;
                        ++i;
                    }
                }

                outputRow.copyFrom( this.inputMorsel(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$argumentSize.nReferences() );
                outputRow.setRefAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$NodeFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeCountFromCountStoreOperator$$offset,
                        Values.longValue( count ) );
                outputRow.moveToNextRow();
                this.hasNext_$eq( false );
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            this.executionEvent_$eq( event );
        }

        public void closeInnerLoop( final QueryResources resources )
        {
        }
    }
}
