package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityImpl;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselArgumentStateBufferOutputOperator implements OutputOperator, Product, Serializable
{
    private final int bufferId;
    private final int argumentSlotOffset;
    private final int nextPipelineHeadPlanId;
    private final boolean nextPipelineFused;
    private final WorkIdentity workIdentity;

    public MorselArgumentStateBufferOutputOperator( final int bufferId, final int argumentSlotOffset, final int nextPipelineHeadPlanId,
            final boolean nextPipelineFused )
    {
        this.bufferId = bufferId;
        this.argumentSlotOffset = argumentSlotOffset;
        this.nextPipelineHeadPlanId = nextPipelineHeadPlanId;
        this.nextPipelineFused = nextPipelineFused;
        Product.$init$( this );
        this.workIdentity = new WorkIdentityImpl( nextPipelineHeadPlanId,
                (new StringBuilder( 42 )).append( "Output morsel grouped by argumentSlot " ).append( argumentSlotOffset ).append( " to " ).append(
                        new BufferId( bufferId ) ).toString() );
    }

    public static Option<Tuple4<BufferId,Object,Id,Object>> unapply( final MorselArgumentStateBufferOutputOperator x$0 )
    {
        return MorselArgumentStateBufferOutputOperator$.MODULE$.unapply( var0 );
    }

    public static MorselArgumentStateBufferOutputOperator apply( final int bufferId, final int argumentSlotOffset, final int nextPipelineHeadPlanId,
            final boolean nextPipelineFused )
    {
        return MorselArgumentStateBufferOutputOperator$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<BufferId,Object,Id,Object>,MorselArgumentStateBufferOutputOperator> tupled()
    {
        return MorselArgumentStateBufferOutputOperator$.MODULE$.tupled();
    }

    public static Function1<BufferId,Function1<Object,Function1<Id,Function1<Object,MorselArgumentStateBufferOutputOperator>>>> curried()
    {
        return MorselArgumentStateBufferOutputOperator$.MODULE$.curried();
    }

    public int bufferId()
    {
        return this.bufferId;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public int nextPipelineHeadPlanId()
    {
        return this.nextPipelineHeadPlanId;
    }

    public boolean nextPipelineFused()
    {
        return this.nextPipelineFused;
    }

    public Option<BufferId> outputBuffer()
    {
        return new Some( new BufferId( this.bufferId() ) );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
    {
        return new MorselArgumentStateBufferOutputState( this.workIdentity(), executionState.getSink( pipelineId, this.bufferId() ), this.argumentSlotOffset(),
                !this.nextPipelineFused() );
    }

    public MorselArgumentStateBufferOutputOperator copy( final int bufferId, final int argumentSlotOffset, final int nextPipelineHeadPlanId,
            final boolean nextPipelineFused )
    {
        return new MorselArgumentStateBufferOutputOperator( bufferId, argumentSlotOffset, nextPipelineHeadPlanId, nextPipelineFused );
    }

    public int copy$default$1()
    {
        return this.bufferId();
    }

    public int copy$default$2()
    {
        return this.argumentSlotOffset();
    }

    public int copy$default$3()
    {
        return this.nextPipelineHeadPlanId();
    }

    public boolean copy$default$4()
    {
        return this.nextPipelineFused();
    }

    public String productPrefix()
    {
        return "MorselArgumentStateBufferOutputOperator";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new BufferId( this.bufferId() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToInteger( this.argumentSlotOffset() );
            break;
        case 2:
            var10000 = new Id( this.nextPipelineHeadPlanId() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToBoolean( this.nextPipelineFused() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselArgumentStateBufferOutputOperator;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( new BufferId( this.bufferId() ) ) );
        var1 = Statics.mix( var1, this.argumentSlotOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( new Id( this.nextPipelineHeadPlanId() ) ) );
        var1 = Statics.mix( var1, this.nextPipelineFused() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 4 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        label49:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof MorselArgumentStateBufferOutputOperator )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label49;
                }

                MorselArgumentStateBufferOutputOperator var4 = (MorselArgumentStateBufferOutputOperator) x$1;
                if ( this.bufferId() != var4.bufferId() || this.argumentSlotOffset() != var4.argumentSlotOffset() ||
                        this.nextPipelineHeadPlanId() != var4.nextPipelineHeadPlanId() || this.nextPipelineFused() != var4.nextPipelineFused() ||
                        !var4.canEqual( this ) )
                {
                    break label49;
                }
            }

            var10000 = true;
            return var10000;
        }

        var10000 = false;
        return var10000;
    }
}
