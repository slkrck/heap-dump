package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Value;
import scala.Function0;
import scala.Function2;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeIndexStringSearchScanTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int offset;
    private final SlottedIndexedProperty property;
    private final int queryIndexId;
    private final IndexOrder indexOrder;
    private final Function0<IntermediateExpression> generateExpression;
    private final Function2<Object,IntermediateRepresentation,IntermediateRepresentation> searchPredicate;
    private final SlotConfiguration.Size argumentSize;
    private final InstanceField nodeIndexCursorField;
    private final boolean needsValues;
    private final LocalVariable seekVariable;
    private IntermediateExpression seekExpression;

    public NodeIndexStringSearchScanTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int offset, final SlottedIndexedProperty property, final int queryIndexId, final IndexOrder indexOrder,
            final Function0<IntermediateExpression> generateExpression,
            final Function2<Object,IntermediateRepresentation,IntermediateRepresentation> searchPredicate, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.offset = offset;
        this.property = property;
        this.queryIndexId = queryIndexId;
        this.indexOrder = indexOrder;
        this.generateExpression = generateExpression;
        this.searchPredicate = searchPredicate;
        this.argumentSize = argumentSize;
        this.nodeIndexCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ));
        this.needsValues = property.getValueFromIndex();
        this.seekVariable = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( super.codeGen().namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                (Object) null ), scala.reflect.ManifestFactory..MODULE$.classType( Value.class ));
    }

    private InstanceField nodeIndexCursorField()
    {
        return this.nodeIndexCursorField;
    }

    private boolean needsValues()
    {
        return this.needsValues;
    }

    private IntermediateExpression seekExpression()
    {
        return this.seekExpression;
    }

    private void seekExpression_$eq( final IntermediateExpression x$1 )
    {
        this.seekExpression = x$1;
    }

    private LocalVariable seekVariable()
    {
        return this.seekVariable;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.nodeIndexCursorField()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V(), this.seekVariable()}) ));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeIndexCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.nodeIndexCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{event}) ))),super.inner().genSetExecutionEvent( event )})));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.seekExpression()}) ));
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        this.seekExpression_$eq( (IntermediateExpression) this.generateExpression.apply() );
        String hasInnerLoop = super.codeGen().namer().nextVariableName();
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( this.seekVariable(),
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.seekExpression(), ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
            scala.reflect.ManifestFactory..MODULE$.Boolean()),hasInnerLoop, org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
            NodeIndexStringSearchScanOperator$.MODULE$.isValidOrThrowMethod(), scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.seekVariable() )}))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
            OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.nodeIndexCursorField(), this.executionEventField(),
                    OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_INDEX_CURSOR() ),
            OperatorCodeGenHelperTemplates$.MODULE$.nodeIndexSeek( OperatorCodeGenHelperTemplates$.MODULE$.indexReadSession( this.queryIndexId ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ),
            (IntermediateRepresentation) this.searchPredicate.apply( BoxesRunTime.boxToInteger( this.property.propertyKeyId() ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                    this.seekVariable() ), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )) ), this.indexOrder, this.needsValues()),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
            OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeIndexCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( hasInnerLoop, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( hasInnerLoop )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                        super.codeGen().setLongAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                        NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        (IntermediateRepresentation) this.property.maybeCachedNodePropertySlot().map( ( x$1 ) -> {
            return $anonfun$genInnerLoop$1( this, BoxesRunTime.unboxToInt( x$1 ) );
        } ).getOrElse( () -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
        } ), super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeIndexCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ))))}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeIndexCursorField() ), OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$.MODULE$,
                    scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.nodeIndexCursorField(),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))})));
    }
}
