package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.SeekArgs;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.values.AnyValue;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class DirectedRelationshipByIdSeekOperator extends RelationshipByIdSeekOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$relationship;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$startNode;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$endNode;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$argumentSize;

    public DirectedRelationshipByIdSeekOperator( final WorkIdentity workIdentity, final int relationship, final int startNode, final int endNode,
            final SeekArgs relId, final SlotConfiguration.Size argumentSize )
    {
        super( workIdentity, relationship, startNode, endNode, relId, argumentSize );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$relationship = relationship;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$startNode = startNode;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$endNode = endNode;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$argumentSize = argumentSize;
    }

    public String toString()
    {
        return "DirectedRelationshipByIdTask";
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new RelationshipByIdSeekOperator.RelationshipByIdTask[]{new RelationshipByIdSeekOperator.RelationshipByIdTask( this, inputMorsel )
                {
                    public
                    {
                        if ( $outer == null )
                        {
                            throw null;
                        }
                        else
                        {
                            this.$outer = $outer;
                        }
                    }

                    public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
                    {
                        while ( outputRow.isValidRow() && this.ids().hasNext() )
                        {
                            long nextId = org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.NumericHelper..
                            MODULE$.asLongEntityIdPrimitive( (AnyValue) this.ids().next() );
                            Read read = context.transactionalContext().dataRead();
                            if ( nextId >= 0L )
                            {
                                read.singleRelationship( nextId, this.cursor() );
                                if ( this.cursor().next() )
                                {
                                    outputRow.copyFrom( this.inputMorsel(),
                                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$argumentSize.nLongs(),
                                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$argumentSize.nReferences() );
                                    outputRow.setLongAt(
                                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$relationship,
                                            nextId );
                                    outputRow.setLongAt(
                                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$startNode,
                                            this.cursor().sourceNodeReference() );
                                    outputRow.setLongAt(
                                            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$operators$DirectedRelationshipByIdSeekOperator$$endNode,
                                            this.cursor().targetNodeReference() );
                                    outputRow.moveToNextRow();
                                }
                            }
                        }
                    }
                }}) ));
    }
}
