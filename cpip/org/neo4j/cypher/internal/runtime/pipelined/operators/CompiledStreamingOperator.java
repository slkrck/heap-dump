package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.internal.kernel.api.Read;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CompiledStreamingOperator extends StreamingOperator
{
    static ClassDeclaration<CompiledStreamingOperator> getClassDeclaration( final String packageName, final String className,
            final Class<CompiledTask> taskClazz, final StaticField workIdentityField,
            final Seq<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates )
    {
        return CompiledStreamingOperator$.MODULE$.getClassDeclaration( var0, var1, var2, var3, var4 );
    }

    static void $init$( final CompiledStreamingOperator $this )
    {
    }

    default IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext context, final QueryState state, final MorselParallelizer inputMorsel,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return .MODULE$.wrapRefArray( (Object[]) ((Object[]) (new ContinuableOperatorTaskWithMorsel[]{
            this.compiledNextTask( context.transactionalContext().dataRead(), inputMorsel.nextCopy(), argumentStateMaps )})) );
    }

    ContinuableOperatorTaskWithMorsel compiledNextTask( final Read dataRead, final MorselExecutionContext inputMorsel,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps );
}
