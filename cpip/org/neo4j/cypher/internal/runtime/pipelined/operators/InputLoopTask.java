package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class InputLoopTask implements ContinuableOperatorTaskWithMorsel
{
    private boolean innerLoop;

    public InputLoopTask()
    {
        OperatorTask.$init$( this );
        ContinuableOperatorTask.$init$( this );
        ContinuableOperatorTaskWithMorsel.$init$( this );
        this.innerLoop = false;
    }

    public void closeInput( final OperatorCloser operatorCloser )
    {
        ContinuableOperatorTaskWithMorsel.closeInput$( this, operatorCloser );
    }

    public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
    {
        return ContinuableOperatorTaskWithMorsel.filterCancelledArguments$( this, operatorCloser );
    }

    public WorkUnitEvent producingWorkUnitEvent()
    {
        return ContinuableOperatorTaskWithMorsel.producingWorkUnitEvent$( this );
    }

    public long estimatedHeapUsage()
    {
        return ContinuableOperatorTaskWithMorsel.estimatedHeapUsage$( this );
    }

    public void close( final OperatorCloser operatorCloser, final QueryResources resources )
    {
        ContinuableOperatorTask.close$( this, operatorCloser, resources );
    }

    public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
            final QueryProfiler queryProfiler )
    {
        OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
    }

    public abstract boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
            final ExecutionContext initExecutionContext );

    public abstract void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state );

    public abstract void closeInnerLoop( final QueryResources resources );

    public void enterOperate( final QueryContext context, final QueryState state, final QueryResources resources )
    {
    }

    public void exitOperate()
    {
    }

    private boolean innerLoop()
    {
        return this.innerLoop;
    }

    private void innerLoop_$eq( final boolean x$1 )
    {
        this.innerLoop = x$1;
    }

    public final void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
    {
        this.advanceOnCancelledRows( resources );
        this.enterOperate( context, state, resources );

        while ( (this.inputMorsel().isValidRow() || this.innerLoop()) && outputRow.isValidRow() )
        {
            if ( !this.innerLoop() )
            {
                this.innerLoop_$eq( this.initializeInnerLoop( context, state, resources, outputRow ) );
            }

            if ( this.innerLoop() )
            {
                this.innerLoop( outputRow, context, state );
                if ( outputRow.isValidRow() )
                {
                    this.closeInnerLoop( resources );
                    this.innerLoop_$eq( false );
                    this.inputMorsel().moveToNextRow();
                }
            }
            else
            {
                this.inputMorsel().moveToNextRow();
            }
        }

        outputRow.finishedWriting();
        this.exitOperate();
    }

    private void advanceOnCancelledRows( final QueryResources resources )
    {
        if ( !this.inputMorsel().isValidRow() )
        {
            this.inputMorsel().moveToNextRow();
            if ( this.innerLoop() )
            {
                this.closeInnerLoop( resources );
                this.innerLoop_$eq( false );
            }
        }
    }

    public boolean canContinue()
    {
        return this.inputMorsel().isValidRow() || this.innerLoop();
    }

    public void closeCursors( final QueryResources resources )
    {
        this.closeInnerLoop( resources );
    }
}
