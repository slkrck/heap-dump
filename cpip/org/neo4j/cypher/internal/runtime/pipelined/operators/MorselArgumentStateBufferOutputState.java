package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap$;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselArgumentStateBufferOutputState implements OutputOperatorState, Product, Serializable
{
    private final WorkIdentity workIdentity;
    private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink;
    private final int argumentSlotOffset;
    private final boolean trackTime;

    public MorselArgumentStateBufferOutputState( final WorkIdentity workIdentity,
            final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink, final int argumentSlotOffset, final boolean trackTime )
    {
        this.workIdentity = workIdentity;
        this.sink = sink;
        this.argumentSlotOffset = argumentSlotOffset;
        this.trackTime = trackTime;
        OutputOperatorState.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<WorkIdentity,Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,Object,Object>> unapply(
            final MorselArgumentStateBufferOutputState x$0 )
    {
        return MorselArgumentStateBufferOutputState$.MODULE$.unapply( var0 );
    }

    public static MorselArgumentStateBufferOutputState apply( final WorkIdentity workIdentity,
            final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink, final int argumentSlotOffset, final boolean trackTime )
    {
        return MorselArgumentStateBufferOutputState$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<WorkIdentity,Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,Object,Object>,MorselArgumentStateBufferOutputState> tupled()
    {
        return MorselArgumentStateBufferOutputState$.MODULE$.tupled();
    }

    public static Function1<WorkIdentity,Function1<Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,Function1<Object,Function1<Object,MorselArgumentStateBufferOutputState>>>> curried()
    {
        return MorselArgumentStateBufferOutputState$.MODULE$.curried();
    }

    public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
            final QueryResources resources, final QueryProfiler queryProfiler )
    {
        return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
    }

    public boolean canContinueOutput()
    {
        return OutputOperatorState.canContinueOutput$( this );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink()
    {
        return this.sink;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public boolean trackTime()
    {
        return this.trackTime;
    }

    public PreparedOutput prepareOutput( final MorselExecutionContext outputMorsel, final QueryContext context, final QueryState state,
            final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
    {
        IndexedSeq viewsPerArgument = ArgumentStateMap$.MODULE$.map( this.argumentSlotOffset(), outputMorsel, ( morselView ) -> {
            return morselView;
        } );
        return new MorselArgumentStateBufferPreparedOutput( this.sink(), viewsPerArgument );
    }

    public MorselArgumentStateBufferOutputState copy( final WorkIdentity workIdentity,
            final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink, final int argumentSlotOffset, final boolean trackTime )
    {
        return new MorselArgumentStateBufferOutputState( workIdentity, sink, argumentSlotOffset, trackTime );
    }

    public WorkIdentity copy$default$1()
    {
        return this.workIdentity();
    }

    public Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> copy$default$2()
    {
        return this.sink();
    }

    public int copy$default$3()
    {
        return this.argumentSlotOffset();
    }

    public boolean copy$default$4()
    {
        return this.trackTime();
    }

    public String productPrefix()
    {
        return "MorselArgumentStateBufferOutputState";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.workIdentity();
            break;
        case 1:
            var10000 = this.sink();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.argumentSlotOffset() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToBoolean( this.trackTime() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselArgumentStateBufferOutputState;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.workIdentity() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.sink() ) );
        var1 = Statics.mix( var1, this.argumentSlotOffset() );
        var1 = Statics.mix( var1, this.trackTime() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 4 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label67:
            {
                boolean var2;
                if ( x$1 instanceof MorselArgumentStateBufferOutputState )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label49:
                    {
                        label58:
                        {
                            MorselArgumentStateBufferOutputState var4 = (MorselArgumentStateBufferOutputState) x$1;
                            WorkIdentity var10000 = this.workIdentity();
                            WorkIdentity var5 = var4.workIdentity();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label58;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label58;
                            }

                            Sink var7 = this.sink();
                            Sink var6 = var4.sink();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label58;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label58;
                            }

                            if ( this.argumentSlotOffset() == var4.argumentSlotOffset() && this.trackTime() == var4.trackTime() && var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label49;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label67;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
