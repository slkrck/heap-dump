package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.DbAccess;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.ArrayBuffer.;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.runtime.BoxedUnit;

public final class ExpandAllOperatorTaskTemplate$
{
    public static ExpandAllOperatorTaskTemplate$ MODULE$;

    static
    {
        new ExpandAllOperatorTaskTemplate$();
    }

    private ExpandAllOperatorTaskTemplate$()
    {
        MODULE$ = this;
    }

    public int[] computeTypes( final int[] computed, final String[] missing, final DbAccess dbAccess )
    {
        ArrayBuffer newTokens = (ArrayBuffer).MODULE$.apply( scala.Predef..MODULE$.wrapIntArray( computed ));
        (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) missing ))).foreach( ( s ) -> {
        $anonfun$computeTypes$1( dbAccess, newTokens, s );
        return BoxedUnit.UNIT;
    } );
        return (int[]) newTokens.toArray( scala.reflect.ClassTag..MODULE$.Int());
    }
}
