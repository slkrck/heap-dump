package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeCursor;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SingleThreadedAllNodeScanTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final String nodeVarName;
    private final int offset;
    private final SlotConfiguration.Size argumentSize;
    private final InstanceField nodeCursorField;

    public SingleThreadedAllNodeScanTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final String nodeVarName, final int offset, final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.nodeVarName = nodeVarName;
        this.offset = offset;
        this.argumentSize = argumentSize;
        this.nodeCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ));
    }

    public DelegateOperatorTaskTemplate innermost()
    {
        return this.innermost;
    }

    public String nodeVarName()
    {
        return this.nodeVarName;
    }

    public int offset()
    {
        return this.offset;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    private InstanceField nodeCursorField()
    {
        return this.nodeCursorField;
    }

    public final String scopeId()
    {
        return (new StringBuilder( 12 )).append( "allNodesScan" ).append( super.id() ).toString();
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.nodeCursorField()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.empty();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.nodeCursorField(), this.executionEventField(),
                            OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_CURSOR() ),
                    OperatorCodeGenHelperTemplates$.MODULE$.allNodeScan( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
            OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost().predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize().nLongs(), this.argumentSize().nReferences() ),
                        super.codeGen().setLongAt( this.offset(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeCursorField() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                        NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )))),this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeCursorField() ), OperatorCodeGenHelperTemplates.NodeCursorPool$.MODULE$, scala.reflect.ManifestFactory..MODULE$.classType(
                    NodeCursor.class )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.nodeCursorField(),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))})));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )})))),
        super.inner().genSetExecutionEvent( event )})));
    }
}
