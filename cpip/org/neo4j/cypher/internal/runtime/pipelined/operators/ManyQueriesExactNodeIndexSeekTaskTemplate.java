package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function0;
import scala.MatchError;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ManyQueriesExactNodeIndexSeekTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int offset;
    private final SlottedIndexedProperty property;
    private final Function0<IntermediateExpression> generatePredicate;
    private final int queryIndexId;
    private final IndexOrder order;
    private final SlotConfiguration.Size argumentSize;
    private final String queriesVariable;
    private final InstanceField nodeIndexCursorField;
    private final InstanceField queryIteratorField;
    private IntermediateExpression queries;

    public ManyQueriesExactNodeIndexSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final String nodeVarName, final int offset, final SlottedIndexedProperty property, final Function0<IntermediateExpression> generatePredicate,
            final int queryIndexId, final IndexOrder order, final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.offset = offset;
        this.property = property;
        this.generatePredicate = generatePredicate;
        this.queryIndexId = queryIndexId;
        this.order = order;
        this.argumentSize = argumentSize;
        this.queriesVariable = super.codeGen().namer().nextVariableName();
        this.nodeIndexCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ));
        this.queryIteratorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( ExactPredicateIterator.class ));
    }

    public static Method nextMethod()
    {
        return ManyQueriesExactNodeIndexSeekTaskTemplate$.MODULE$.nextMethod();
    }

    public static ExactPredicateIterator descendingQueryIterator( final int propertyKey, final ListValue list )
    {
        return ManyQueriesExactNodeIndexSeekTaskTemplate$.MODULE$.descendingQueryIterator( var0, var1 );
    }

    public static ExactPredicateIterator ascendingQueryIterator( final int propertyKey, final ListValue list )
    {
        return ManyQueriesExactNodeIndexSeekTaskTemplate$.MODULE$.ascendingQueryIterator( var0, var1 );
    }

    public static ExactPredicateIterator unorderedQueryIterator( final int propertyKey, final ListValue list )
    {
        return ManyQueriesExactNodeIndexSeekTaskTemplate$.MODULE$.unorderedQueryIterator( var0, var1 );
    }

    public static boolean next( final IndexReadSession index, final NodeValueIndexCursor cursor, final ExactPredicateIterator queries, final Read read )
    {
        return ManyQueriesExactNodeIndexSeekTaskTemplate$.MODULE$.next( var0, var1, var2, var3 );
    }

    public OperatorTaskTemplate inner()
    {
        return super.inner();
    }

    private IntermediateExpression queries()
    {
        return this.queries;
    }

    private void queries_$eq( final IntermediateExpression x$1 )
    {
        this.queries = x$1;
    }

    private String queriesVariable()
    {
        return this.queriesVariable;
    }

    private InstanceField nodeIndexCursorField()
    {
        return this.nodeIndexCursorField;
    }

    private InstanceField queryIteratorField()
    {
        return this.queryIteratorField;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new InstanceField[]{this.nodeIndexCursorField(), this.queryIteratorField()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.queries()}) ));
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        IndexOrder var3 = this.order;
        Method var1;
        if ( IndexOrder.NONE.equals( var3 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "unorderedQueryIterator", scala.reflect.ManifestFactory..MODULE$.classType(
                ManyQueriesExactNodeIndexSeekTaskTemplate.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( ExactPredicateIterator.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
            MODULE$.classType( ListValue.class ));
        }
        else if ( IndexOrder.ASCENDING.equals( var3 ) )
        {
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "ascendingQueryIterator", scala.reflect.ManifestFactory..MODULE$.classType(
                ManyQueriesExactNodeIndexSeekTaskTemplate.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( ExactPredicateIterator.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
            MODULE$.classType( ListValue.class ));
        }
        else
        {
            if ( !IndexOrder.DESCENDING.equals( var3 ) )
            {
                throw new MatchError( var3 );
            }

            var1 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.method( "descendingQueryIterator", scala.reflect.ManifestFactory..MODULE$.classType(
                    ManyQueriesExactNodeIndexSeekTaskTemplate.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( ExactPredicateIterator.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
            MODULE$.classType( ListValue.class ));
        }

        IntermediateExpression compiled = (IntermediateExpression) this.generatePredicate.apply();
        this.queries_$eq(
                compiled.copy( OperatorCodeGenHelperTemplates$.MODULE$.asListValue( compiled.ir() ), compiled.copy$default$2(), compiled.copy$default$3(),
                        compiled.copy$default$4(), compiled.copy$default$5() ) );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
            this.queriesVariable(),
            ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.queries(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( "EMPTY_LIST",
                    scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.queryIteratorField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( var1,
                scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                        BoxesRunTime.boxToInteger( this.property.propertyKeyId() ) ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                this.queriesVariable() )})))),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.nodeIndexCursorField(),
            OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_INDEX_CURSOR() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                ManyQueriesExactNodeIndexSeekTaskTemplate$.MODULE$.nextMethod(), scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.indexReadSession( this.queryIndexId ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.queryIteratorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() )})))),
        OperatorCodeGenHelperTemplates$.MODULE$.profileRow( super.id(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                        super.codeGen().setLongAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                        NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        (IntermediateRepresentation) this.property.maybeCachedNodePropertySlot().map( ( x$10 ) -> {
            return $anonfun$genInnerLoop$3( this, BoxesRunTime.unboxToInt( x$10 ) );
        } ).getOrElse( () -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
        } ), this.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( ManyQueriesExactNodeIndexSeekTaskTemplate$.MODULE$.nextMethod(),
                        scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.indexReadSession( this.queryIndexId ),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.queryIteratorField() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() )})))),
        OperatorCodeGenHelperTemplates$.MODULE$.profileRow( super.id(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.canContinue() ))})))),this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeIndexCursorField() ), OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$.MODULE$,
                    scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.nodeIndexCursorField(),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))})));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeIndexCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.nodeIndexCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{event}) ))),this.inner().genSetExecutionEvent( event )})));
    }
}
