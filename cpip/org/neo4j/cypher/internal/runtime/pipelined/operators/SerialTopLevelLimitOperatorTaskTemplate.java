package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.cypher.internal.physicalplanning.TopLevelArgument$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.util.Preconditions;
import org.neo4j.values.AnyValue;
import scala.Function0;
import scala.Function1;
import scala.Option;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SerialTopLevelLimitOperatorTaskTemplate implements OperatorTaskTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final DelegateOperatorTaskTemplate innermost;
    private final Function0<IntermediateExpression> generateCountExpression;
    private final OperatorExpressionCompiler codeGen;
    private final LocalVariable countLeftVar;
    private final LocalVariable reservedVar;
    private final InstanceField limitStateField;
    private IntermediateExpression countExpression;

    public SerialTopLevelLimitOperatorTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int argumentStateMapId, final Function0<IntermediateExpression> generateCountExpression, final OperatorExpressionCompiler codeGen )
    {
        this.inner = inner;
        this.id = id;
        this.innermost = innermost;
        this.generateCountExpression = generateCountExpression;
        this.codeGen = codeGen;
        OperatorTaskTemplate.$init$( this );
        this.countLeftVar = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( (new StringBuilder( 10 )).append( codeGen.namer().nextVariableName() ).append( "_countLeft" ).toString(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ), scala.reflect.ManifestFactory..
        MODULE$.Long());
        this.reservedVar = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( (new StringBuilder( 9 )).append( codeGen.namer().nextVariableName() ).append( "_reserved" ).toString(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ), scala.reflect.ManifestFactory..
        MODULE$.Long());
        this.limitStateField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 11 )).append( codeGen.namer().nextVariableName() ).append( "_limitState" ).toString(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                OperatorCodeGenHelperTemplates$.MODULE$.ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER().name() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "applyByIntId", scala.reflect.ManifestFactory..MODULE$.classType(
                ArgumentStateMap.ArgumentStateMaps.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( ArgumentStateMap.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.ArgumentState.class )),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( argumentStateMapId ) )}))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "peek", scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.ArgumentState.class )),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..
        MODULE$.classType( ArgumentStateMap.ArgumentState.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToLong( TopLevelArgument$.MODULE$.VALUE() ) )}))),scala.reflect.ManifestFactory..
        MODULE$.classType( SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState.class )),scala.reflect.ManifestFactory..
        MODULE$.classType( SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState.class ));
    }

    public String scopeId()
    {
        return OperatorTaskTemplate.scopeId$( this );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return OperatorTaskTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genProduce()
    {
        return OperatorTaskTemplate.genProduce$( this );
    }

    public IntermediateRepresentation genCreateState()
    {
        return OperatorTaskTemplate.genCreateState$( this );
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return OperatorTaskTemplate.genOutputBuffer$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    private IntermediateExpression countExpression()
    {
        return this.countExpression;
    }

    private void countExpression_$eq( final IntermediateExpression x$1 )
    {
        this.countExpression = x$1;
    }

    private LocalVariable countLeftVar()
    {
        return this.countLeftVar;
    }

    private LocalVariable reservedVar()
    {
        return this.reservedVar;
    }

    private InstanceField limitStateField()
    {
        return this.limitStateField;
    }

    public IntermediateRepresentation genInit()
    {
        return this.inner().genInit();
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.countExpression()}) ));
    }

    public IntermediateRepresentation genOperateEnter()
    {
        if ( this.countExpression() == null )
        {
            this.countExpression_$eq( (IntermediateExpression) this.generateCountExpression.apply() );
        }

        IntermediateRepresentation howMuchToReserve = this.innermost.shouldWriteToContext() ? org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.cast( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "getValidRows", scala.reflect.ManifestFactory..MODULE$.classType(
                MorselExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.Long()) :
        (this.innermost.shouldCheckOutputCounter() ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_COUNTER() ), scala.reflect.ManifestFactory..MODULE$.Long()) :
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( Long.MAX_VALUE ) ));
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.limitStateField() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "isUninitialized",
            scala.reflect.ManifestFactory..MODULE$.classType(
            SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.limitStateField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "initialize", scala.reflect.ManifestFactory..MODULE$.classType(
                SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "evaluateCountValue", scala.reflect.ManifestFactory..MODULE$.classType(
                    LimitOperator.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.countExpression().ir()}) ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( this.reservedVar(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.limitStateField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "reserve", scala.reflect.ManifestFactory..MODULE$.classType( LimitOperator.LimitState.class ), scala.reflect.ManifestFactory..
        MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{(IntermediateRepresentation) howMuchToReserve}) ))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( this.countLeftVar(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.reservedVar() )),
        this.inner().genOperateEnter()})));
    }

    public IntermediateRepresentation genOperate()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.lessThanOrEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                    this.countLeftVar() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( 0L ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$. break (OperatorCodeGenHelperTemplates$.MODULE$.OUTER_LOOP_LABEL_NAME())),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( this.countLeftVar(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.subtract(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.countLeftVar() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToLong( 1L ) ))),this.inner().genOperateWithExpressions()})));
    }

    public IntermediateRepresentation genOperateExit()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.limitStateField() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "update", scala.reflect.ManifestFactory..MODULE$.classType(
            SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.subtract(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.reservedVar() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.countLeftVar() ))}))),
        OperatorCodeGenHelperTemplates$.MODULE$.profileRows( this.id(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.subtract( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                this.reservedVar() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.countLeftVar() )),scala.reflect.ManifestFactory..
        MODULE$.Int())),this.inner().genOperateExit()})));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{this.countLeftVar(), this.reservedVar()}) ));
    }

    public Seq<Field> genFields()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.limitStateField()}) ));
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue();
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return this.inner().genCloseCursors();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return this.inner().genSetExecutionEvent( event );
    }

    public abstract static class SerialTopLevelLimitState extends LimitOperator.LimitState
    {
        public abstract long getCount();

        public abstract void setCount( final long count );

        public boolean isUninitialized()
        {
            return this.getCount() == -1L;
        }

        public void initialize( final long count )
        {
            Preconditions.checkState( this.isUninitialized(), "Can only call initialize once" );
            this.setCount( count );
        }

        public void update( final long usedCount )
        {
            Preconditions.checkState( usedCount >= 0L, "Can not have used a negative number of rows" );
            long newCount = this.getCount() - usedCount;
            Preconditions.checkState( newCount >= 0L, "Used more rows than had count left" );
            this.setCount( newCount );
        }

        public long reserve( final long wanted )
        {
            long count = this.getCount();
            Preconditions.checkState( count != -1L, "SerialTopLevelLimitState has not been initialized" );
            return scala.math.package..MODULE$.min( count, wanted );
        }

        public boolean isCancelled()
        {
            return this.getCount() == 0L;
        }
    }

    public static class SerialTopLevelLimitStateFactory$
            implements ArgumentStateMap.ArgumentStateFactory<SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState>
    {
        public static SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitStateFactory$ MODULE$;

        static
        {
            new SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitStateFactory$();
        }

        public SerialTopLevelLimitStateFactory$()
        {
            MODULE$ = this;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState newStandardArgumentState( final long argumentRowId,
                final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers )
        {
            return new SerialTopLevelLimitOperatorTaskTemplate.StandardSerialTopLevelLimitState( argumentRowId, argumentRowIdsForReducers );
        }

        public SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState newConcurrentArgumentState( final long argumentRowId,
                final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers )
        {
            return new SerialTopLevelLimitOperatorTaskTemplate.VolatileSerialTopLevelLimitState( argumentRowId, argumentRowIdsForReducers );
        }
    }

    public static class StandardSerialTopLevelLimitState extends SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState
    {
        private final long argumentRowId;
        private final long[] argumentRowIdsForReducers;
        private long countLeft;

        public StandardSerialTopLevelLimitState( final long argumentRowId, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.countLeft = -1L;
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private long countLeft()
        {
            return this.countLeft;
        }

        private void countLeft_$eq( final long x$1 )
        {
            this.countLeft = x$1;
        }

        public long getCount()
        {
            return this.countLeft();
        }

        public void setCount( final long count )
        {
            this.countLeft_$eq( count );
        }

        public String toString()
        {
            return (new StringBuilder( 46 )).append( "StandardSerialTopLevelLimitState(" ).append( this.argumentRowId() ).append( ", countLeft=" ).append(
                    this.countLeft() ).append( ")" ).toString();
        }
    }

    public static class VolatileSerialTopLevelLimitState extends SerialTopLevelLimitOperatorTaskTemplate.SerialTopLevelLimitState
    {
        private final long argumentRowId;
        private final long[] argumentRowIdsForReducers;
        private volatile long countLeft;

        public VolatileSerialTopLevelLimitState( final long argumentRowId, final long[] argumentRowIdsForReducers )
        {
            this.argumentRowId = argumentRowId;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.countLeft = -1L;
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        private long countLeft()
        {
            return this.countLeft;
        }

        private void countLeft_$eq( final long x$1 )
        {
            this.countLeft = x$1;
        }

        public long getCount()
        {
            return this.countLeft();
        }

        public void setCount( final long count )
        {
            this.countLeft_$eq( count );
        }

        public String toString()
        {
            return (new StringBuilder( 46 )).append( "VolatileSerialTopLevelLimitState(" ).append( this.argumentRowId() ).append( ", countLeft=" ).append(
                    this.countLeft() ).append( ")" ).toString();
        }
    }
}
