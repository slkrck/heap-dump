package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledHelpers;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.storable.Value;
import scala.Function0;
import scala.Function1;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SingleRangeSeekQueryNodeIndexSeekTaskTemplate extends SingleQueryNodeIndexSeekTaskTemplate
{
    private final Seq<Function0<IntermediateExpression>> generateSeekValues;
    private final Function1<Seq<IntermediateRepresentation>,IntermediateRepresentation> generatePredicate;
    private final LocalVariable predicateVar;
    private Seq<IntermediateExpression> seekValues;

    public SingleRangeSeekQueryNodeIndexSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int offset, final SlottedIndexedProperty property, final Seq<Function0<IntermediateExpression>> generateSeekValues,
            final Function1<Seq<IntermediateRepresentation>,IntermediateRepresentation> generatePredicate, final int queryIndexId, final IndexOrder order,
            final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, offset, property, order, property.getValueFromIndex(), queryIndexId, argumentSize, codeGen );
        this.generateSeekValues = generateSeekValues;
        this.generatePredicate = generatePredicate;
        this.predicateVar = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( super.codeGen().namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                (Object) null ), scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class ));
    }

    private Seq<IntermediateExpression> seekValues()
    {
        return this.seekValues;
    }

    private void seekValues_$eq( final Seq<IntermediateExpression> x$1 )
    {
        this.seekValues = x$1;
    }

    private LocalVariable predicateVar()
    {
        return this.predicateVar;
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V(), this.predicateVar()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return this.seekValues();
    }

    public IntermediateRepresentation getPropertyValue()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.nodeIndexCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "propertyValue", scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( 0 ) )})))
        ;
    }

    public IntermediateRepresentation beginInnerLoop()
    {
        this.seekValues_$eq( (Seq) ((TraversableLike) this.generateSeekValues.map( ( x$8 ) -> {
            return (IntermediateExpression) x$8.apply();
        },.MODULE$.canBuildFrom()) ).map( ( v ) -> {
            return v.copy( OperatorCodeGenHelperTemplates$.MODULE$.asStorableValue(
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( v, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ), v.copy$default$2(),
                    v.copy$default$3(), v.copy$default$4(), v.copy$default$5() );
        },.MODULE$.canBuildFrom()));
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( this.predicateVar(), (IntermediateRepresentation) this.generatePredicate.apply( this.seekValues().map( ( x$9 ) -> {
            return x$9.ir();
        },.MODULE$.canBuildFrom() ) ));
    }

    public IntermediateRepresentation isPredicatePossible()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "possibleRangePredicate",
                scala.reflect.ManifestFactory..MODULE$.classType( CompiledHelpers.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.classType( IndexQuery.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.predicate()}) ));
    }

    public IntermediateRepresentation predicate()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( this.predicateVar() );
    }
}
