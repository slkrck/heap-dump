package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;

public class CartesianProductOperator$LHSMorsel$Factory implements ArgumentStateMap.ArgumentStateFactory<CartesianProductOperator.LHSMorsel>
{
    public CartesianProductOperator$LHSMorsel$Factory( final StateFactory stateFactory )
    {
        ArgumentStateMap.ArgumentStateFactory.$init$( this );
    }

    public CartesianProductOperator.LHSMorsel newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
            final long[] argumentRowIdsForReducers )
    {
        return new CartesianProductOperator.LHSMorsel( argumentRowId, argumentMorsel.detach(), argumentRowIdsForReducers );
    }

    public CartesianProductOperator.LHSMorsel newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
            final long[] argumentRowIdsForReducers )
    {
        return new CartesianProductOperator.LHSMorsel( argumentRowId, argumentMorsel.detach(), argumentRowIdsForReducers );
    }

    public boolean completeOnConstruction()
    {
        return true;
    }
}
