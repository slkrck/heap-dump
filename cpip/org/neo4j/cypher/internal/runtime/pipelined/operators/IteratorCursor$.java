package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.values.AnyValue;

public final class IteratorCursor$
{
    public static IteratorCursor$ MODULE$;

    static
    {
        new IteratorCursor$();
    }

    private IteratorCursor$()
    {
        MODULE$ = this;
    }

    public IteratorCursor apply( final Iterator<AnyValue> iterator )
    {
        return new IteratorCursor( iterator );
    }
}
