package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import scala.Function1;
import scala.Option;
import scala.collection.Seq;
import scala.collection.immutable.List;

public final class OperatorTaskTemplate$
{
    public static OperatorTaskTemplate$ MODULE$;

    static
    {
        new OperatorTaskTemplate$();
    }

    private OperatorTaskTemplate$()
    {
        MODULE$ = this;
    }

    public OperatorTaskTemplate empty( final int withId )
    {
        return new OperatorTaskTemplate( withId )
        {
            private final int withId$1;

            public
            {
                this.withId$1 = withId$1;
                OperatorTaskTemplate.$init$( this );
            }

            public String scopeId()
            {
                return OperatorTaskTemplate.scopeId$( this );
            }

            public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
            {
                return OperatorTaskTemplate.map$( this, f );
            }

            public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
            {
                return OperatorTaskTemplate.flatMap$( this, f );
            }

            public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
            {
                return OperatorTaskTemplate.genClassDeclaration$( this, packageName, className, staticFields );
            }

            public InstanceField executionEventField()
            {
                return OperatorTaskTemplate.executionEventField$( this );
            }

            public Option<Field> genProfileEventField()
            {
                return OperatorTaskTemplate.genProfileEventField$( this );
            }

            public IntermediateRepresentation genInitializeProfileEvents()
            {
                return OperatorTaskTemplate.genInitializeProfileEvents$( this );
            }

            public IntermediateRepresentation genCloseProfileEvents()
            {
                return OperatorTaskTemplate.genCloseProfileEvents$( this );
            }

            public final IntermediateRepresentation genOperateWithExpressions()
            {
                return OperatorTaskTemplate.genOperateWithExpressions$( this );
            }

            public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
            {
                return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
            }

            public IntermediateRepresentation genOperateEnter()
            {
                return OperatorTaskTemplate.genOperateEnter$( this );
            }

            public IntermediateRepresentation genOperateExit()
            {
                return OperatorTaskTemplate.genOperateExit$( this );
            }

            public OperatorTaskTemplate inner()
            {
                return null;
            }

            public OperatorExpressionCompiler codeGen()
            {
                return null;
            }

            public int id()
            {
                return this.withId$1;
            }

            public IntermediateRepresentation genOperate()
            {
                return .MODULE$.noop();
            }

            public IntermediateRepresentation genProduce()
            {
                return .MODULE$.noop();
            }

            public IntermediateRepresentation genCreateState()
            {
                return .MODULE$.noop();
            }

            public Seq<Field> genFields()
            {
                return (Seq) scala.collection.Seq..MODULE$.empty();
            }

            public Seq<LocalVariable> genLocalVariables()
            {
                return (Seq) scala.collection.Seq..MODULE$.empty();
            }

            public Seq<IntermediateExpression> genExpressions()
            {
                return (Seq) scala.collection.Seq..MODULE$.empty();
            }

            public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
            {
                return .MODULE$.noop();
            }

            public Option<IntermediateRepresentation> genCanContinue()
            {
                return scala.None..MODULE$;
            }

            public IntermediateRepresentation genCloseCursors()
            {
                return .MODULE$.noop();
            }

            public Option<IntermediateRepresentation> genOutputBuffer()
            {
                return scala.None..MODULE$;
            }

            public IntermediateRepresentation genInit()
            {
                return .MODULE$.noop();
            }
        };
    }
}
