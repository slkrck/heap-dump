package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeLabelIndexCursor;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SingleThreadedLabelScanTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int offset;
    private final String labelName;
    private final Option<Object> maybeLabelId;
    private final SlotConfiguration.Size argumentSize;
    private final InstanceField nodeLabelCursorField;
    private final InstanceField labelField;

    public SingleThreadedLabelScanTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final String nodeVarName, final int offset, final String labelName, final Option<Object> maybeLabelId, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.offset = offset;
        this.labelName = labelName;
        this.maybeLabelId = maybeLabelId;
        this.argumentSize = argumentSize;
        this.nodeLabelCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ));
        this.labelField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN(),
                scala.reflect.ManifestFactory..MODULE$.Int());
    }

    private InstanceField nodeLabelCursorField()
    {
        return this.nodeLabelCursorField;
    }

    private InstanceField labelField()
    {
        return this.labelField;
    }

    public final String scopeId()
    {
        return (new StringBuilder( 9 )).append( "labelScan" ).append( super.id() ).toString();
    }

    public Seq<Field> genMoreFields()
    {
        return this.maybeLabelId.isDefined() ? (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new InstanceField[]{this.nodeLabelCursorField()}) )) :
        (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.nodeLabelCursorField(), this.labelField()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.empty();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        Option var2 = this.maybeLabelId;
        IntermediateRepresentation var1;
        if ( var2 instanceof Some )
        {
            Some var3 = (Some) var2;
            int labelId = BoxesRunTime.unboxToInt( var3.value() );
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{
                        OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.nodeLabelCursorField(), this.executionEventField(),
                                OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_LABEL_CURSOR() ),
                        OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelScan( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                BoxesRunTime.boxToInteger( labelId ) ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeLabelCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeLabelCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ))),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
        }
        else
        {
            if ( !scala.None..MODULE$.equals( var2 )){
            throw new MatchError( var2 );
        }

            String hasInnerLoop = super.codeGen().namer().nextVariableName();
            var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.labelField() ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN() ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.setField( this.labelField(), OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelId( this.labelName ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.declareAndAssign( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Boolean()),
            hasInnerLoop, org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.labelField() ), OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN())),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( hasInnerLoop )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( hasInnerLoop ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                            OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.nodeLabelCursorField(), this.executionEventField(),
                                    OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_LABEL_CURSOR() ),
                            OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelScan( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                    this.labelField() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeLabelCursorField() )),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                    OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeLabelCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class )))})))),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( hasInnerLoop )})));
        }

        return var1;
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                        super.codeGen().setLongAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeLabelCursorField() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                        NodeLabelIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeLabelCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class )))),
        this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeLabelCursorField() ), OperatorCodeGenHelperTemplates.NodeLabelIndexCursorPool$.MODULE$,
                    scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.nodeLabelCursorField(),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))})));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeLabelCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.nodeLabelCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{event}) ))),super.inner().genSetExecutionEvent( event )})));
    }
}
