package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.values.storable.Values;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class RelationshipCountFromCountStoreOperator implements StreamingOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$offset;
    public final Option<LazyLabel> org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$startLabel;
    public final RelationshipTypes org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$relationshipTypes;
    public final Option<LazyLabel> org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$endLabel;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$argumentSize;
    private final WorkIdentity workIdentity;
    private volatile RelationshipCountFromCountStoreOperator.Unknown$ Unknown$module;
    private volatile RelationshipCountFromCountStoreOperator.Wildcard$ Wildcard$module;
    private volatile RelationshipCountFromCountStoreOperator.Known$ Known$module;

    public RelationshipCountFromCountStoreOperator( final WorkIdentity workIdentity, final int offset, final Option<LazyLabel> startLabel,
            final RelationshipTypes relationshipTypes, final Option<LazyLabel> endLabel, final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$offset = offset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$startLabel = startLabel;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$relationshipTypes = relationshipTypes;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$endLabel = endLabel;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$argumentSize = argumentSize;
        StreamingOperator.$init$( this );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public RelationshipCountFromCountStoreOperator.Unknown$ Unknown()
    {
        if ( this.Unknown$module == null )
        {
            this.Unknown$lzycompute$1();
        }

        return this.Unknown$module;
    }

    public RelationshipCountFromCountStoreOperator.Wildcard$ Wildcard()
    {
        if ( this.Wildcard$module == null )
        {
            this.Wildcard$lzycompute$1();
        }

        return this.Wildcard$module;
    }

    public RelationshipCountFromCountStoreOperator.Known$ Known()
    {
        if ( this.Known$module == null )
        {
            this.Known$lzycompute$1();
        }

        return this.Known$module;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq) scala.package..MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new RelationshipCountFromCountStoreOperator.RelationshipFromCountStoreTask[]{
                    new RelationshipCountFromCountStoreOperator.RelationshipFromCountStoreTask( this, inputMorsel.nextCopy() )}) ));
    }

    private final void Unknown$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.Unknown$module == null )
            {
                this.Unknown$module = new RelationshipCountFromCountStoreOperator.Unknown$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    private final void Wildcard$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.Wildcard$module == null )
            {
                this.Wildcard$module = new RelationshipCountFromCountStoreOperator.Wildcard$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    private final void Known$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.Known$module == null )
            {
                this.Known$module = new RelationshipCountFromCountStoreOperator.Known$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public interface RelId
    {
        int id();
    }

    public class Known implements RelationshipCountFromCountStoreOperator.RelId, Product, Serializable
    {
        private final int id;

        public Known( final RelationshipCountFromCountStoreOperator $outer, final int id )
        {
            this.id = id;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
            }
        }

        public int id()
        {
            return this.id;
        }

        public RelationshipCountFromCountStoreOperator.Known copy( final int id )
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$Known$$$outer().new Known(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$Known$$$outer(), id );
        }

        public int copy$default$1()
        {
            return this.id();
        }

        public String productPrefix()
        {
            return "Known";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return BoxesRunTime.boxToInteger( this.id() );
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof RelationshipCountFromCountStoreOperator.Known;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, this.id() );
            return Statics.finalizeHash( var1, 1 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label54:
                {
                    boolean var2;
                    if ( x$1 instanceof RelationshipCountFromCountStoreOperator.Known &&
                            ((RelationshipCountFromCountStoreOperator.Known) x$1).org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$Known$$$outer() ==
                                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$Known$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        RelationshipCountFromCountStoreOperator.Known var4 = (RelationshipCountFromCountStoreOperator.Known) x$1;
                        if ( this.id() == var4.id() && var4.canEqual( this ) )
                        {
                            break label54;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public class Known$ extends AbstractFunction1<Object,RelationshipCountFromCountStoreOperator.Known> implements Serializable
    {
        public Known$( final RelationshipCountFromCountStoreOperator $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public final String toString()
        {
            return "Known";
        }

        public RelationshipCountFromCountStoreOperator.Known apply( final int id )
        {
            return this.$outer.new Known( this.$outer, id );
        }

        public Option<Object> unapply( final RelationshipCountFromCountStoreOperator.Known x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.id() ) ));
        }
    }

    public class RelationshipFromCountStoreTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private boolean hasNext;
        private OperatorProfileEvent executionEvent;

        public RelationshipFromCountStoreTask( final RelationshipCountFromCountStoreOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                this.hasNext = false;
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public String toString()
        {
            return "RelationshipFromCountStoreTask";
        }

        private boolean hasNext()
        {
            return this.hasNext;
        }

        private void hasNext_$eq( final boolean x$1 )
        {
            this.hasNext = x$1;
        }

        private OperatorProfileEvent executionEvent()
        {
            return this.executionEvent;
        }

        private void executionEvent_$eq( final OperatorProfileEvent x$1 )
        {
            this.executionEvent = x$1;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().workIdentity();
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            this.hasNext_$eq( true );
            return true;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            if ( this.hasNext() )
            {
                long var10000;
                label27:
                {
                    label31:
                    {
                        RelationshipCountFromCountStoreOperator.RelId startLabelId = this.getLabelId(
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$startLabel,
                                context );
                        RelationshipCountFromCountStoreOperator.RelId endLabelId = this.getLabelId(
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$endLabel,
                                context );
                        RelationshipCountFromCountStoreOperator.Unknown$ var8 =
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().Unknown();
                        if ( startLabelId == null )
                        {
                            if ( var8 == null )
                            {
                                break label31;
                            }
                        }
                        else if ( startLabelId.equals( var8 ) )
                        {
                            break label31;
                        }

                        RelationshipCountFromCountStoreOperator.Unknown$ var9 =
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().Unknown();
                        if ( endLabelId == null )
                        {
                            if ( var9 == null )
                            {
                                break label31;
                            }
                        }
                        else if ( endLabelId.equals( var9 ) )
                        {
                            break label31;
                        }

                        var10000 = this.countOneDirection( context, startLabelId.id(), endLabelId.id() );
                        break label27;
                    }

                    var10000 = 0L;
                }

                long count = var10000;
                outputRow.copyFrom( this.inputMorsel(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$argumentSize.nReferences() );
                outputRow.setRefAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$offset,
                        Values.longValue( count ) );
                outputRow.moveToNextRow();
                this.hasNext_$eq( false );
            }
        }

        private RelationshipCountFromCountStoreOperator.RelId getLabelId( final Option<LazyLabel> lazyLabel, final QueryContext context )
        {
            Object var3;
            if ( lazyLabel instanceof Some )
            {
                Some var5 = (Some) lazyLabel;
                LazyLabel label = (LazyLabel) var5.value();
                int id = label.getId( context );
                var3 = id == org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.UNKNOWN()
                                                                                            ? this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().Unknown()
                                                                                            : this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().new Known(
                                                                                                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer(),
                                                                                                    id );
            }
            else
            {
                var3 =
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().Wildcard();
            }

            return (RelationshipCountFromCountStoreOperator.RelId) var3;
        }

        private long countOneDirection( final QueryContext context, final int startLabelId, final int endLabelId )
        {
            int[] relationshipTypeIds =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$RelationshipFromCountStoreTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$RelationshipCountFromCountStoreOperator$$relationshipTypes.types(
                            context );
            long var10000;
            if ( relationshipTypeIds == null )
            {
                if ( this.executionEvent() != null )
                {
                    this.executionEvent().dbHit();
                }

                var10000 = context.relationshipCountByCountStore( startLabelId, org.neo4j.cypher.internal.v4_0.util.NameId..MODULE$.WILDCARD(), endLabelId);
            }
            else
            {
                int i = 0;

                long count;
                for ( count = 0L; i < relationshipTypeIds.length; ++i )
                {
                    if ( this.executionEvent() != null )
                    {
                        this.executionEvent().dbHit();
                    }

                    count += context.relationshipCountByCountStore( startLabelId, relationshipTypeIds[i], endLabelId );
                }

                var10000 = count;
            }

            return var10000;
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            this.executionEvent_$eq( event );
        }

        public void closeInnerLoop( final QueryResources resources )
        {
        }
    }

    public class Unknown$ implements RelationshipCountFromCountStoreOperator.RelId, Product, Serializable
    {
        public Unknown$( final RelationshipCountFromCountStoreOperator $outer )
        {
            Product.$init$( this );
        }

        public int id()
        {
            throw new UnsupportedOperationException();
        }

        public String productPrefix()
        {
            return "Unknown";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof RelationshipCountFromCountStoreOperator.Unknown$;
        }

        public int hashCode()
        {
            return 1379812394;
        }

        public String toString()
        {
            return "Unknown";
        }
    }

    public class Wildcard$ implements RelationshipCountFromCountStoreOperator.RelId, Product, Serializable
    {
        public Wildcard$( final RelationshipCountFromCountStoreOperator $outer )
        {
            Product.$init$( this );
        }

        public int id()
        {
            return org.neo4j.cypher.internal.v4_0.util.NameId..MODULE$.WILDCARD();
        }

        public String productPrefix()
        {
            return "Wildcard";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof RelationshipCountFromCountStoreOperator.Wildcard$;
        }

        public int hashCode()
        {
            return -1108370950;
        }

        public String toString()
        {
            return "Wildcard";
        }
    }
}
