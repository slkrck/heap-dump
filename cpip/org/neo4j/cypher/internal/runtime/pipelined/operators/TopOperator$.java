package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.ColumnOrder;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.Seq;

public final class TopOperator$ implements Serializable
{
    public static TopOperator$ MODULE$;

    static
    {
        new TopOperator$();
    }

    private TopOperator$()
    {
        MODULE$ = this;
    }

    public TopOperator apply( final WorkIdentity workIdentity, final Seq<ColumnOrder> orderBy, final Expression countExpression )
    {
        return new TopOperator( workIdentity, orderBy, countExpression );
    }

    public Option<Tuple3<WorkIdentity,Seq<ColumnOrder>,Expression>> unapply( final TopOperator x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.workIdentity(), x$0.orderBy(), x$0.countExpression() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
