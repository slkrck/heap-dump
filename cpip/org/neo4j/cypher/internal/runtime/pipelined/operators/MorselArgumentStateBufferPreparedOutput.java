package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselArgumentStateBufferPreparedOutput implements PreparedOutput, Product, Serializable
{
    private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink;
    private final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data;

    public MorselArgumentStateBufferPreparedOutput( final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink,
            final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data )
    {
        this.sink = sink;
        this.data = data;
        Product.$init$( this );
    }

    public static Option<Tuple2<Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>> unapply(
            final MorselArgumentStateBufferPreparedOutput x$0 )
    {
        return MorselArgumentStateBufferPreparedOutput$.MODULE$.unapply( var0 );
    }

    public static MorselArgumentStateBufferPreparedOutput apply( final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink,
            final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data )
    {
        return MorselArgumentStateBufferPreparedOutput$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,MorselArgumentStateBufferPreparedOutput> tupled()
    {
        return MorselArgumentStateBufferPreparedOutput$.MODULE$.tupled();
    }

    public static Function1<Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,Function1<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>,MorselArgumentStateBufferPreparedOutput>> curried()
    {
        return MorselArgumentStateBufferPreparedOutput$.MODULE$.curried();
    }

    public Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink()
    {
        return this.sink;
    }

    public IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data()
    {
        return this.data;
    }

    public void produce()
    {
        this.sink().put( this.data() );
    }

    public MorselArgumentStateBufferPreparedOutput copy( final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink,
            final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data )
    {
        return new MorselArgumentStateBufferPreparedOutput( sink, data );
    }

    public Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> copy$default$1()
    {
        return this.sink();
    }

    public IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> copy$default$2()
    {
        return this.data();
    }

    public String productPrefix()
    {
        return "MorselArgumentStateBufferPreparedOutput";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.sink();
            break;
        case 1:
            var10000 = this.data();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselArgumentStateBufferPreparedOutput;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof MorselArgumentStateBufferPreparedOutput )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            MorselArgumentStateBufferPreparedOutput var4 = (MorselArgumentStateBufferPreparedOutput) x$1;
                            Sink var10000 = this.sink();
                            Sink var5 = var4.sink();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            IndexedSeq var7 = this.data();
                            IndexedSeq var6 = var4.data();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
