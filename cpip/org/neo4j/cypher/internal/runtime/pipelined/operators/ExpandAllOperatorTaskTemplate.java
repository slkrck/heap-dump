package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledHelpers;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.exceptions.InternalException;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.RelationshipGroupCursor;
import org.neo4j.internal.kernel.api.RelationshipTraversalCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelections;
import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.ArrayBuffer.;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ExpandAllOperatorTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final Slot fromSlot;
    private final int relOffset;
    private final int toOffset;
    private final SemanticDirection dir;
    private final int[] types;
    private final String[] missingTypes;
    private final InstanceField nodeCursorField;
    private final InstanceField groupCursorField;
    private final InstanceField traversalCursorField;
    private final InstanceField relationshipsField;
    private final InstanceField typeField;
    private final InstanceField missingTypeField;

    public ExpandAllOperatorTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost, final boolean isHead,
            final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir, final int[] types, final String[] missingTypes,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, isHead );
        this.innermost = innermost;
        this.fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.toOffset = toOffset;
        this.dir = dir;
        this.types = types;
        this.missingTypes = missingTypes;
        this.nodeCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 10 )).append( super.codeGen().namer().nextVariableName() ).append( "nodeCursor" ).toString(),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ));
        this.groupCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 8 )).append( super.codeGen().namer().nextVariableName() ).append( "relGroup" ).toString(),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipGroupCursor.class ));
        this.traversalCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 12 )).append( super.codeGen().namer().nextVariableName() ).append( "relTraversal" ).toString(),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipTraversalCursor.class ));
        this.relationshipsField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 12 )).append( super.codeGen().namer().nextVariableName() ).append( "relSelection" ).toString(),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ));
        this.typeField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 4 )).append( super.codeGen().namer().nextVariableName() ).append( "type" ).toString(),
                (new ofInt( scala.Predef..MODULE$.intArrayOps( types )) ).isEmpty() && (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) missingTypes ))).
        isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ) :org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.arrayOf( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new ofInt( scala.Predef..MODULE$.intArrayOps( types )) ).map( ( value ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( value );
        }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )))),scala.reflect.ManifestFactory..
        MODULE$.Int()),scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()));
        this.missingTypeField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 11 )).append( super.codeGen().namer().nextVariableName() ).append( "missingType" ).toString(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) missingTypes )) ).map( ( value ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( value );
        }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class )))),scala.reflect.ManifestFactory..
        MODULE$.classType( String.class )),scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( String.class )));
    }

    public static int[] computeTypes( final int[] computed, final String[] missing, final DbAccess dbAccess )
    {
        return ExpandAllOperatorTaskTemplate$.MODULE$.computeTypes( var0, var1, var2 );
    }

    private InstanceField nodeCursorField()
    {
        return this.nodeCursorField;
    }

    private InstanceField groupCursorField()
    {
        return this.groupCursorField;
    }

    private InstanceField traversalCursorField()
    {
        return this.traversalCursorField;
    }

    private InstanceField relationshipsField()
    {
        return this.relationshipsField;
    }

    private InstanceField typeField()
    {
        return this.typeField;
    }

    private InstanceField missingTypeField()
    {
        return this.missingTypeField;
    }

    public final String scopeId()
    {
        return (new StringBuilder( 9 )).append( "expandAll" ).append( super.id() ).toString();
    }

    public Seq<Field> genMoreFields()
    {
        ArrayBuffer localFields = (ArrayBuffer).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new InstanceField[]{this.nodeCursorField(), this.groupCursorField(), this.traversalCursorField(), this.relationshipsField(),
                        this.typeField()}) ));
        if ( (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.missingTypes )) ).nonEmpty()){
        localFields.$plus$eq( this.missingTypeField() );
    } else{
        BoxedUnit var10000 = BoxedUnit.UNIT;
    }

        return localFields;
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        SemanticDirection var5 = this.dir;
        Tuple2 var2;
        if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( var5 )){
        var2 = new Tuple2( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "outgoingDenseCursor",
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelections.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipGroupCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipTraversalCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int())),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "outgoingSparseCursor", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelections.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipTraversalCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int())));
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( var5 )){
        var2 = new Tuple2( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "incomingDenseCursor",
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelections.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipGroupCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipTraversalCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int())),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "incomingSparseCursor", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelections.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipTraversalCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int())));
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( var5 )){
            throw new MatchError( var5 );
        }

        var2 = new Tuple2( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "allDenseCursor", scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelections.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipGroupCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipTraversalCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int())),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "allSparseCursor", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelections.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipTraversalCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int())));
    }

        if ( var2 != null )
        {
            Method denseMethod = (Method) var2._1();
            Method sparseMethod = (Method) var2._2();
            Tuple2 var1 = new Tuple2( denseMethod, sparseMethod );
            Method denseMethod = (Method) var1._1();
            Method sparseMethod = (Method) var1._2();
            String resultBoolean = super.codeGen().namer().nextVariableName();
            String fromNode = (new StringBuilder( 8 )).append( super.codeGen().namer().nextVariableName() ).append( "fromNode" ).toString();
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Boolean()), resultBoolean,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.declareAndAssign( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()),
            fromNode, this.getNodeIdFromSlot( this.fromSlot )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( fromNode ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.loadTypes(),
                            OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.nodeCursorField(), this.executionEventField(),
                                    OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_CURSOR() ),
                            OperatorCodeGenHelperTemplates$.MODULE$.singleNode( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( fromNode ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeCursorField() )),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ifElse(
                    OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeCursorField() ), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ifElse( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.method( "isDense", scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),
            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                            OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.groupCursorField(), this.executionEventField(),
                                    OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_GROUP_CURSOR() ),
                            OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.traversalCursorField(), this.executionEventField(),
                                    OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_TRAVERSAL_CURSOR() ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.relationshipsField(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( denseMethod, scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.groupCursorField() ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.traversalCursorField() ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.typeField() )}))))}))),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                        OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.traversalCursorField(), this.executionEventField(),
                                OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_TRAVERSAL_CURSOR() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.relationshipsField(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( sparseMethod, scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.traversalCursorField() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeCursorField() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.typeField() )}))))})))),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.setField( this.relationshipsField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( "EMPTY",
                    scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( RelationshipSelectionCursor.class )))),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.relationshipsField() ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
            MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )}))),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.assign( resultBoolean, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.relationshipsField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class )))})))),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( resultBoolean )})));
        }
        else
        {
            throw new MatchError( var2 );
        }
    }

    private IntermediateRepresentation loadTypes()
    {
        return (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.missingTypes ))).isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.noop() :org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayLength( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.typeField() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToInteger( this.types.length + this.missingTypes.length ) )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.typeField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "computeTypes", scala.reflect.ManifestFactory..MODULE$.classType(
                ExpandAllOperatorTaskTemplate.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()),
        scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()),scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class )),
        scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.typeField() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.missingTypeField() ), OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS()})))))
        ;
    }

    public IntermediateRepresentation genInnerLoop()
    {
        SemanticDirection var3 = this.dir;
        Method var1;
        if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( var3 )){
        var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "targetNodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long());
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( var3 )){
        var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "sourceNodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long());
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( var3 )){
            throw new MatchError( var3 );
        }

        var1 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "otherNodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long());
    }

        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                super.codeGen().copyFromInput( Math.min( super.codeGen().inputSlotConfiguration().numberOfLongs(), super.codeGen().slots().numberOfLongs() ),
                        Math.min( super.codeGen().inputSlotConfiguration().numberOfReferences(), super.codeGen().slots().numberOfReferences() ) ),
                super.codeGen().setLongAt( this.relOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.relationshipsField() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationshipReference", scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.codeGen().setLongAt( this.toOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.relationshipsField() ), var1, scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.relationshipsField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class )))),
        this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeCursorField() ), OperatorCodeGenHelperTemplates.NodeCursorPool$.MODULE$, scala.reflect.ManifestFactory..MODULE$.classType(
                    NodeCursor.class )),
            OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.groupCursorField() ),
            OperatorCodeGenHelperTemplates.GroupCursorPool$.MODULE$, scala.reflect.ManifestFactory..MODULE$.classType( RelationshipGroupCursor.class )),
        OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.traversalCursorField() ), OperatorCodeGenHelperTemplates.TraversalCursorPool$.MODULE$, scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipTraversalCursor.class )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.nodeCursorField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.groupCursorField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.traversalCursorField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.relationshipsField(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))})));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.relationshipsField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeCursorField() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType(
            NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),
        scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )}))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.relationshipsField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.executionEventField() )})))})))),
        super.inner().genSetExecutionEvent( event )})));
    }

    private IntermediateRepresentation getNodeIdFromSlot( final Slot slot )
    {
        boolean var3 = false;
        RefSlot var4 = null;
        IntermediateRepresentation var2;
        if ( slot instanceof LongSlot )
        {
            LongSlot var6 = (LongSlot) slot;
            int offset = var6.offset();
            var2 = super.codeGen().getLongAt( offset );
        }
        else
        {
            if ( slot instanceof RefSlot )
            {
                var3 = true;
                var4 = (RefSlot) slot;
                int offset = var4.offset();
                boolean var9 = var4.nullable();
                if ( !var9 )
                {
                    var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeFromAnyValue",
                            scala.reflect.ManifestFactory..MODULE$.classType( CompiledHelpers.class ), scala.reflect.ManifestFactory..
                    MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{super.codeGen().getRefAt( offset )}) ));
                    return var2;
                }
            }

            if ( !var3 )
            {
                throw new InternalException( (new StringBuilder( 42 )).append( "Do not know how to get a node id for slot " ).append( slot ).toString() );
            }

            int offset = var4.offset();
            boolean var11 = var4.nullable();
            if ( !var11 )
            {
                throw new InternalException( (new StringBuilder( 42 )).append( "Do not know how to get a node id for slot " ).append( slot ).toString() );
            }

            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( super.codeGen().getRefAt( offset ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeFromAnyValue",
                    scala.reflect.ManifestFactory..MODULE$.classType( CompiledHelpers.class ), scala.reflect.ManifestFactory..
            MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{super.codeGen().getRefAt( offset )}) )));
        }

        return var2;
    }
}
