package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class EXPAND
{
    public static String toString()
    {
        return EXPAND$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return EXPAND$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return EXPAND$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return EXPAND$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return EXPAND$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return EXPAND$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return EXPAND$.MODULE$.productPrefix();
    }
}
