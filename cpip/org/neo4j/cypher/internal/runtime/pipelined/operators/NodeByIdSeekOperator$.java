package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.Method;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.NumericHelper;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.values.AnyValue;
import scala.runtime.BoxesRunTime;

public final class NodeByIdSeekOperator$
{
    public static NodeByIdSeekOperator$ MODULE$;

    static
    {
        new NodeByIdSeekOperator$();
    }

    private final Method asIdMethod;

    private NodeByIdSeekOperator$()
    {
        MODULE$ = this;
        this.asIdMethod = .
        MODULE$.method( "asLongEntityIdPrimitive", scala.reflect.ManifestFactory..MODULE$.classType( NumericHelper.class ), scala.reflect.ManifestFactory..
        MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
    }

    public Method asIdMethod()
    {
        return this.asIdMethod;
    }

    public IntermediateRepresentation isValidNode( final IntermediateRepresentation nodeId )
    {
        return .MODULE$.and(.MODULE$.greaterThanOrEqual( nodeId,.MODULE$.constant( BoxesRunTime.boxToLong( 0L ) )), .
        MODULE$.invoke(.MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() ), .
        MODULE$.method( "nodeExists", scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nodeId}) )));
    }
}
