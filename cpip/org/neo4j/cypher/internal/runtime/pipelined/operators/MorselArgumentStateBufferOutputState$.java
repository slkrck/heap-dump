package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class MorselArgumentStateBufferOutputState$ extends
        AbstractFunction4<WorkIdentity,Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,Object,Object,MorselArgumentStateBufferOutputState>
        implements Serializable
{
    public static MorselArgumentStateBufferOutputState$ MODULE$;

    static
    {
        new MorselArgumentStateBufferOutputState$();
    }

    private MorselArgumentStateBufferOutputState$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselArgumentStateBufferOutputState";
    }

    public MorselArgumentStateBufferOutputState apply( final WorkIdentity workIdentity,
            final Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>> sink, final int argumentSlotOffset, final boolean trackTime )
    {
        return new MorselArgumentStateBufferOutputState( workIdentity, sink, argumentSlotOffset, trackTime );
    }

    public Option<Tuple4<WorkIdentity,Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>,Object,Object>> unapply(
            final MorselArgumentStateBufferOutputState x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple4( x$0.workIdentity(), x$0.sink(), BoxesRunTime.boxToInteger( x$0.argumentSlotOffset() ), BoxesRunTime.boxToBoolean( x$0.trackTime() ) ) ))
        ;
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
