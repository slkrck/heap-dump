package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPools;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipGroupCursor;
import org.neo4j.internal.kernel.api.RelationshipTraversalCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelections;
import scala.MatchError;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ExpandAllOperator implements StreamingOperator
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$relOffset;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$toOffset;
    public final SemanticDirection org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$dir;
    public final RelationshipTypes org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$types;
    private final WorkIdentity workIdentity;
    private final ToLongFunction<ExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$getFromNodeFunction;

    public ExpandAllOperator( final WorkIdentity workIdentity, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$relOffset = relOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$toOffset = toOffset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$dir = dir;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$types = types;
        StreamingOperator.$init$( this );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$getFromNodeFunction =
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( fromSlot,
                        SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public ToLongFunction<ExecutionContext> org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$getFromNodeFunction()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$getFromNodeFunction;
    }

    public String toString()
    {
        return "ExpandAll";
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new ExpandAllOperator.OTask[]{new ExpandAllOperator.OTask( this, inputMorsel.nextCopy() )}) ));
    }

    public class OTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private NodeCursor nodeCursor;
        private RelationshipGroupCursor groupCursor;
        private RelationshipTraversalCursor traversalCursor;
        private RelationshipSelectionCursor relationships;

        public OTask( final ExpandAllOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$OTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "ExpandAllTask";
        }

        private NodeCursor nodeCursor()
        {
            return this.nodeCursor;
        }

        private void nodeCursor_$eq( final NodeCursor x$1 )
        {
            this.nodeCursor = x$1;
        }

        private RelationshipGroupCursor groupCursor()
        {
            return this.groupCursor;
        }

        private void groupCursor_$eq( final RelationshipGroupCursor x$1 )
        {
            this.groupCursor = x$1;
        }

        private RelationshipTraversalCursor traversalCursor()
        {
            return this.traversalCursor;
        }

        private void traversalCursor_$eq( final RelationshipTraversalCursor x$1 )
        {
            this.traversalCursor = x$1;
        }

        private RelationshipSelectionCursor relationships()
        {
            return this.relationships;
        }

        private void relationships_$eq( final RelationshipSelectionCursor x$1 )
        {
            this.relationships = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final QueryState state, final QueryResources resources,
                final ExecutionContext initExecutionContext )
        {
            long fromNode =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$getFromNodeFunction().applyAsLong(
                            this.inputMorsel() );
            boolean var10000;
            if ( NullChecker$.MODULE$.entityIsNull( fromNode ) )
            {
                var10000 = false;
            }
            else
            {
                CursorPools pools = resources.cursorPools();
                this.nodeCursor_$eq( (NodeCursor) pools.nodeCursorPool().allocateAndTrace() );
                this.relationships_$eq( this.getRelationshipsCursor( context, pools, fromNode,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$dir,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$types.types(
                                context ) ) );
                var10000 = true;
            }

            return var10000;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state )
        {
            while ( outputRow.isValidRow() && this.relationships().next() )
            {
                long relId = this.relationships().relationshipReference();
                long otherSide = this.relationships().otherNodeReference();
                outputRow.copyFrom( this.inputMorsel() );
                outputRow.setLongAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$relOffset,
                        relId );
                outputRow.setLongAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$ExpandAllOperator$$toOffset,
                        otherSide );
                outputRow.moveToNextRow();
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.relationships() != null )
            {
                this.nodeCursor().setTracer( event );
                this.relationships().setTracer( event );
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            CursorPools pools = resources.cursorPools();
            pools.nodeCursorPool().free( this.nodeCursor() );
            pools.relationshipGroupCursorPool().free( this.groupCursor() );
            pools.relationshipTraversalCursorPool().free( this.traversalCursor() );
            this.nodeCursor_$eq( (NodeCursor) null );
            this.groupCursor_$eq( (RelationshipGroupCursor) null );
            this.traversalCursor_$eq( (RelationshipTraversalCursor) null );
            this.relationships_$eq( (RelationshipSelectionCursor) null );
        }

        private RelationshipSelectionCursor getRelationshipsCursor( final QueryContext context, final CursorPools pools, final long node,
                final SemanticDirection dir, final int[] types )
        {
            Read read = context.transactionalContext().dataRead();
            read.singleNode( node, this.nodeCursor() );
            RelationshipSelectionCursor var10000;
            if ( !this.nodeCursor().next() )
            {
                var10000 = RelationshipSelectionCursor.EMPTY;
            }
            else
            {
                RelationshipSelectionCursor var7;
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( dir )){
                if ( this.nodeCursor().isDense() )
                {
                    this.groupCursor_$eq( (RelationshipGroupCursor) pools.relationshipGroupCursorPool().allocateAndTrace() );
                    this.traversalCursor_$eq( (RelationshipTraversalCursor) pools.relationshipTraversalCursorPool().allocateAndTrace() );
                    var10000 = RelationshipSelections.outgoingDenseCursor( this.groupCursor(), this.traversalCursor(), this.nodeCursor(), types );
                }
                else
                {
                    this.traversalCursor_$eq( (RelationshipTraversalCursor) pools.relationshipTraversalCursorPool().allocateAndTrace() );
                    var10000 = RelationshipSelections.outgoingSparseCursor( this.traversalCursor(), this.nodeCursor(), types );
                }

                var7 = var10000;
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( dir )){
                if ( this.nodeCursor().isDense() )
                {
                    this.groupCursor_$eq( (RelationshipGroupCursor) pools.relationshipGroupCursorPool().allocateAndTrace() );
                    this.traversalCursor_$eq( (RelationshipTraversalCursor) pools.relationshipTraversalCursorPool().allocateAndTrace() );
                    var10000 = RelationshipSelections.incomingDenseCursor( this.groupCursor(), this.traversalCursor(), this.nodeCursor(), types );
                }
                else
                {
                    this.traversalCursor_$eq( (RelationshipTraversalCursor) pools.relationshipTraversalCursorPool().allocateAndTrace() );
                    var10000 = RelationshipSelections.incomingSparseCursor( this.traversalCursor(), this.nodeCursor(), types );
                }

                var7 = var10000;
            } else{
                if ( !org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( dir )){
                    throw new MatchError( dir );
                }

                if ( this.nodeCursor().isDense() )
                {
                    this.groupCursor_$eq( (RelationshipGroupCursor) pools.relationshipGroupCursorPool().allocateAndTrace() );
                    this.traversalCursor_$eq( (RelationshipTraversalCursor) pools.relationshipTraversalCursorPool().allocateAndTrace() );
                    var10000 = RelationshipSelections.allDenseCursor( this.groupCursor(), this.traversalCursor(), this.nodeCursor(), types );
                }
                else
                {
                    this.traversalCursor_$eq( (RelationshipTraversalCursor) pools.relationshipTraversalCursorPool().allocateAndTrace() );
                    var10000 = RelationshipSelections.allSparseCursor( this.traversalCursor(), this.nodeCursor(), types );
                }

                var7 = var10000;
            }

                var10000 = var7;
            }

            return var10000;
        }
    }
}
