package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.exceptions.InvalidArgumentException;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.FloatingPointValue;
import org.neo4j.values.storable.NumberValue;
import scala.Array.;

public final class LimitOperator$
{
    public static LimitOperator$ MODULE$;

    static
    {
        new LimitOperator$();
    }

    private LimitOperator$()
    {
        MODULE$ = this;
    }

    public long evaluateCountValue( final QueryContext queryContext, final QueryState state, final QueryResources resources, final Expression countExpression )
    {
        SlottedQueryState queryState =
                new SlottedQueryState( queryContext, (ExternalCSVResource) null, state.params(), resources.expressionCursors(), (IndexReadSession[]).
        MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
        resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
        MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
        ;
        AnyValue countValue = countExpression.apply( org.neo4j.cypher.internal.runtime.ExecutionContext..MODULE$.empty(), queryState);
        return this.evaluateCountValue( countValue );
    }

    public long evaluateCountValue( final AnyValue countValue )
    {
        NumberValue limitNumber = org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.NumericHelper..MODULE$.asNumber( countValue );
        if ( limitNumber instanceof FloatingPointValue )
        {
            double limit = limitNumber.doubleValue();
            throw new InvalidArgumentException( (new StringBuilder( 78 )).append( "LIMIT: Invalid input. '" ).append( limit ).append(
                    "' is not a valid value. Must be a non-negative integer." ).toString() );
        }
        else
        {
            long limit = limitNumber.longValue();
            if ( limit < 0L )
            {
                throw new InvalidArgumentException( (new StringBuilder( 78 )).append( "LIMIT: Invalid input. '" ).append( limit ).append(
                        "' is not a valid value. Must be a non-negative integer." ).toString() );
            }
            else
            {
                return limit;
            }
        }
    }
}
