package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Method;
import org.neo4j.cypher.internal.runtime.IsNoValue.;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.TextValue;

public final class NodeIndexStringSearchScanOperator$
{
    public static NodeIndexStringSearchScanOperator$ MODULE$;

    static
    {
        new NodeIndexStringSearchScanOperator$();
    }

    private final Method isValidOrThrowMethod;

    private NodeIndexStringSearchScanOperator$()
    {
        MODULE$ = this;
        this.isValidOrThrowMethod = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "isValidOrThrow", scala.reflect.ManifestFactory..MODULE$.classType(
                NodeIndexStringSearchScanOperator.class ), scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
        MODULE$.classType( AnyValue.class ));
    }

    public boolean isValidOrThrow( final AnyValue value )
    {
        boolean var2;
        if (.MODULE$.unapply( value )){
        var2 = false;
    } else{
        if ( !(value instanceof TextValue) )
        {
            throw new CypherTypeException( (new StringBuilder( 33 )).append( "Expected a string value, but got " ).append( value ).toString() );
        }

        var2 = true;
    }

        return var2;
    }

    public Method isValidOrThrowMethod()
    {
        return this.isValidOrThrowMethod;
    }
}
