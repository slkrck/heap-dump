package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.GetStatic;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.Method;
import org.neo4j.codegen.api.Parameter;
import org.neo4j.internal.schema.IndexOrder;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public final class OperatorCodeGenHelperTemplates
{
    public static IntermediateRepresentation asListValue( final IntermediateRepresentation in )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.asListValue( var0 );
    }

    public static IntermediateRepresentation asStorableValue( final IntermediateRepresentation in )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.asStorableValue( var0 );
    }

    public static IntermediateRepresentation indexReadSession( final int offset )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.indexReadSession( var0 );
    }

    public static IntermediateRepresentation onNode( final IntermediateRepresentation event, final IntermediateRepresentation node )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.onNode( var0, var1 );
    }

    public static IntermediateRepresentation dbHits( final IntermediateRepresentation event, final IntermediateRepresentation nHits )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.dbHits( var0, var1 );
    }

    public static IntermediateRepresentation dbHit( final IntermediateRepresentation event )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.dbHit( var0 );
    }

    public static IntermediateRepresentation closeEvent( final int id )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.closeEvent( var0 );
    }

    public static IntermediateRepresentation profileRows( final int id, final IntermediateRepresentation nRows )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.profileRows( var0, var1 );
    }

    public static IntermediateRepresentation profileRows( final int id, final int nRows )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.profileRows( var0, var1 );
    }

    public static IntermediateRepresentation profileRow( final int id, final IntermediateRepresentation hasRow )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.profileRow( var0, var1 );
    }

    public static IntermediateRepresentation profileRow( final int id )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.profileRow( var0 );
    }

    public static <CURSOR> IntermediateRepresentation profilingCursorNext( final IntermediateRepresentation cursor, final int id, final Manifest<CURSOR> out )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( var0, var1, var2 );
    }

    public static IntermediateRepresentation relationshipTypeId( final String typeName )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.relationshipTypeId( var0 );
    }

    public static IntermediateRepresentation nodeLabelId( final String labelName )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelId( var0 );
    }

    public static <CURSOR> IntermediateRepresentation cursorNext( final IntermediateRepresentation cursor, final Manifest<CURSOR> out )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( var0, var1 );
    }

    public static <CURSOR> IntermediateRepresentation freeCursor( final IntermediateRepresentation cursor,
            final OperatorCodeGenHelperTemplates.CursorPoolsType cursorPools, final Manifest<CURSOR> out )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( var0, var1, var2 );
    }

    public static IntermediateRepresentation allocateAndTraceCursor( final InstanceField cursorField, final InstanceField executionEventField,
            final IntermediateRepresentation allocate )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( var0, var1, var2 );
    }

    public static IntermediateRepresentation singleRelationship( final IntermediateRepresentation relationship, final IntermediateRepresentation cursor )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.singleRelationship( var0, var1 );
    }

    public static IntermediateRepresentation singleNode( final IntermediateRepresentation node, final IntermediateRepresentation cursor )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.singleNode( var0, var1 );
    }

    public static IntermediateRepresentation stringEndsWithScan( final int prop, final IntermediateRepresentation expression )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.stringEndsWithScan( var0, var1 );
    }

    public static IntermediateRepresentation stringContainsScan( final int prop, final IntermediateRepresentation expression )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.stringContainsScan( var0, var1 );
    }

    public static IntermediateRepresentation rangeBetweenSeek( final int prop, final boolean fromInclusive, final IntermediateRepresentation fromExpression,
            final boolean toInclusive, final IntermediateRepresentation toExpression )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.rangeBetweenSeek( var0, var1, var2, var3, var4 );
    }

    public static IntermediateRepresentation greaterThanSeek( final int prop, final boolean inclusive, final IntermediateRepresentation expression )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.greaterThanSeek( var0, var1, var2 );
    }

    public static IntermediateRepresentation lessThanSeek( final int prop, final boolean inclusive, final IntermediateRepresentation expression )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.lessThanSeek( var0, var1, var2 );
    }

    public static IntermediateRepresentation exactSeek( final int prop, final IntermediateRepresentation expression )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.exactSeek( var0, var1 );
    }

    public static IntermediateRepresentation indexOrder( final IndexOrder indexOrder )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.indexOrder( var0 );
    }

    public static IntermediateRepresentation nodeIndexSeek( final IntermediateRepresentation indexReadSession, final IntermediateRepresentation cursor,
            final IntermediateRepresentation query, final IndexOrder order, final boolean needsValues )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.nodeIndexSeek( var0, var1, var2, var3, var4 );
    }

    public static IntermediateRepresentation nodeIndexScan( final IntermediateRepresentation indexReadSession, final IntermediateRepresentation cursor,
            final IndexOrder order, final boolean needsValues )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.nodeIndexScan( var0, var1, var2, var3 );
    }

    public static IntermediateRepresentation nodeLabelScan( final IntermediateRepresentation label, final IntermediateRepresentation cursor )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.nodeLabelScan( var0, var1 );
    }

    public static IntermediateRepresentation allNodeScan( final IntermediateRepresentation cursor )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.allNodeScan( var0 );
    }

    public static IntermediateRepresentation allocateCursor( final OperatorCodeGenHelperTemplates.CursorPoolsType cursorPools )
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.allocateCursor( var0 );
    }

    public static IntermediateRepresentation CALL_CAN_CONTINUE()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.CALL_CAN_CONTINUE();
    }

    public static IntermediateRepresentation NO_OPERATOR_PROFILE_EVENT()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.NO_OPERATOR_PROFILE_EVENT();
    }

    public static IntermediateRepresentation NO_KERNEL_TRACER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.NO_KERNEL_TRACER();
    }

    public static Method SET_TRACER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.SET_TRACER();
    }

    public static GetStatic NO_TOKEN()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.NO_TOKEN();
    }

    public static IntermediateRepresentation HAS_REMAINING_OUTPUT()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.HAS_REMAINING_OUTPUT();
    }

    public static IntermediateRepresentation UPDATE_OUTPUT_COUNTER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.UPDATE_OUTPUT_COUNTER();
    }

    public static LocalVariable OUTPUT_COUNTER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_COUNTER();
    }

    public static IntermediateRepresentation UPDATE_DEMAND()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.UPDATE_DEMAND();
    }

    public static IntermediateRepresentation INPUT_ROW_MOVE_TO_NEXT()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_MOVE_TO_NEXT();
    }

    public static IntermediateRepresentation OUTPUT_ROW_FINISHED_WRITING()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW_FINISHED_WRITING();
    }

    public static IntermediateRepresentation OUTPUT_ROW_IS_VALID()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW_IS_VALID();
    }

    public static IntermediateRepresentation INPUT_ROW_IS_VALID()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_IS_VALID();
    }

    public static String OUTER_LOOP_LABEL_NAME()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.OUTER_LOOP_LABEL_NAME();
    }

    public static IntermediateRepresentation ALLOCATE_REL_SCAN_CURSOR()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_REL_SCAN_CURSOR();
    }

    public static IntermediateRepresentation ALLOCATE_TRAVERSAL_CURSOR()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_TRAVERSAL_CURSOR();
    }

    public static IntermediateRepresentation ALLOCATE_GROUP_CURSOR()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_GROUP_CURSOR();
    }

    public static IntermediateRepresentation ALLOCATE_NODE_INDEX_CURSOR()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_INDEX_CURSOR();
    }

    public static IntermediateRepresentation ALLOCATE_NODE_LABEL_CURSOR()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_LABEL_CURSOR();
    }

    public static IntermediateRepresentation ALLOCATE_NODE_CURSOR()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_CURSOR();
    }

    public static IntermediateRepresentation PRE_POPULATE_RESULTS()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.PRE_POPULATE_RESULTS();
    }

    public static LocalVariable PRE_POPULATE_RESULTS_V()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.PRE_POPULATE_RESULTS_V();
    }

    public static IntermediateRepresentation HAS_DEMAND()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.HAS_DEMAND();
    }

    public static LocalVariable SERVED()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.SERVED();
    }

    public static LocalVariable DEMAND()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.DEMAND();
    }

    public static LocalVariable SUBSCRIPTION()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.SUBSCRIPTION();
    }

    public static LocalVariable SUBSCRIBER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.SUBSCRIBER();
    }

    public static IntermediateRepresentation PIPELINE_ID()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.PIPELINE_ID();
    }

    public static IntermediateRepresentation EXECUTION_STATE()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.EXECUTION_STATE();
    }

    public static IntermediateRepresentation EXPRESSION_VARIABLES()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.EXPRESSION_VARIABLES();
    }

    public static IntermediateRepresentation EXPRESSION_CURSORS()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.EXPRESSION_CURSORS();
    }

    public static IntermediateRepresentation PARAMS()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.PARAMS();
    }

    public static IntermediateRepresentation DB_ACCESS()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS();
    }

    public static IntermediateRepresentation OUTPUT_ROW_MOVE_TO_NEXT()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW_MOVE_TO_NEXT();
    }

    public static IntermediateRepresentation OUTPUT_ROW()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.OUTPUT_ROW();
    }

    public static IntermediateRepresentation CURSOR_POOL()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL();
    }

    public static LocalVariable CURSOR_POOL_V()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V();
    }

    public static IntermediateRepresentation QUERY_RESOURCES()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.QUERY_RESOURCES();
    }

    public static IntermediateRepresentation QUERY_STATE()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.QUERY_STATE();
    }

    public static IntermediateRepresentation QUERY_PROFILER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.QUERY_PROFILER();
    }

    public static InstanceField INPUT_MORSEL()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL();
    }

    public static InstanceField DATA_READ()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ();
    }

    public static String WORK_IDENTITY_STATIC_FIELD_NAME()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.WORK_IDENTITY_STATIC_FIELD_NAME();
    }

    public static Parameter OPERATOR_CLOSER_PARAMETER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.OPERATOR_CLOSER_PARAMETER();
    }

    public static Parameter QUERY_RESOURCE_PARAMETER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.QUERY_RESOURCE_PARAMETER();
    }

    public static Parameter ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER();
    }

    public static Parameter INPUT_MORSEL_CONSTRUCTOR_PARAMETER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL_CONSTRUCTOR_PARAMETER();
    }

    public static Parameter DATA_READ_CONSTRUCTOR_PARAMETER()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ_CONSTRUCTOR_PARAMETER();
    }

    public static IntermediateRepresentation UNINITIALIZED_REF_SLOT_VALUE()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.UNINITIALIZED_REF_SLOT_VALUE();
    }

    public static IntermediateRepresentation UNINITIALIZED_LONG_SLOT_VALUE()
    {
        return OperatorCodeGenHelperTemplates$.MODULE$.UNINITIALIZED_LONG_SLOT_VALUE();
    }

    public interface CursorPoolsType
    {
        String name();
    }

    public static class GroupCursorPool$ implements OperatorCodeGenHelperTemplates.CursorPoolsType, Product, Serializable
    {
        public static OperatorCodeGenHelperTemplates.GroupCursorPool$ MODULE$;

        static
        {
            new OperatorCodeGenHelperTemplates.GroupCursorPool$();
        }

        public GroupCursorPool$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String name()
        {
            return "relationshipGroupCursorPool";
        }

        public String productPrefix()
        {
            return "GroupCursorPool";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorCodeGenHelperTemplates.GroupCursorPool$;
        }

        public int hashCode()
        {
            return 263535697;
        }

        public String toString()
        {
            return "GroupCursorPool";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class NodeCursorPool$ implements OperatorCodeGenHelperTemplates.CursorPoolsType, Product, Serializable
    {
        public static OperatorCodeGenHelperTemplates.NodeCursorPool$ MODULE$;

        static
        {
            new OperatorCodeGenHelperTemplates.NodeCursorPool$();
        }

        public NodeCursorPool$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String name()
        {
            return "nodeCursorPool";
        }

        public String productPrefix()
        {
            return "NodeCursorPool";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorCodeGenHelperTemplates.NodeCursorPool$;
        }

        public int hashCode()
        {
            return 2013970516;
        }

        public String toString()
        {
            return "NodeCursorPool";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class NodeLabelIndexCursorPool$ implements OperatorCodeGenHelperTemplates.CursorPoolsType, Product, Serializable
    {
        public static OperatorCodeGenHelperTemplates.NodeLabelIndexCursorPool$ MODULE$;

        static
        {
            new OperatorCodeGenHelperTemplates.NodeLabelIndexCursorPool$();
        }

        public NodeLabelIndexCursorPool$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String name()
        {
            return "nodeLabelIndexCursorPool";
        }

        public String productPrefix()
        {
            return "NodeLabelIndexCursorPool";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorCodeGenHelperTemplates.NodeLabelIndexCursorPool$;
        }

        public int hashCode()
        {
            return -1896804686;
        }

        public String toString()
        {
            return "NodeLabelIndexCursorPool";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class NodeValueIndexCursorPool$ implements OperatorCodeGenHelperTemplates.CursorPoolsType, Product, Serializable
    {
        public static OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$ MODULE$;

        static
        {
            new OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$();
        }

        public NodeValueIndexCursorPool$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String name()
        {
            return "nodeValueIndexCursorPool";
        }

        public String productPrefix()
        {
            return "NodeValueIndexCursorPool";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$;
        }

        public int hashCode()
        {
            return 680989333;
        }

        public String toString()
        {
            return "NodeValueIndexCursorPool";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class RelScanCursorPool$ implements OperatorCodeGenHelperTemplates.CursorPoolsType, Product, Serializable
    {
        public static OperatorCodeGenHelperTemplates.RelScanCursorPool$ MODULE$;

        static
        {
            new OperatorCodeGenHelperTemplates.RelScanCursorPool$();
        }

        public RelScanCursorPool$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String name()
        {
            return "relationshipScanCursorPool";
        }

        public String productPrefix()
        {
            return "RelScanCursorPool";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorCodeGenHelperTemplates.RelScanCursorPool$;
        }

        public int hashCode()
        {
            return -1783155768;
        }

        public String toString()
        {
            return "RelScanCursorPool";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class TraversalCursorPool$ implements OperatorCodeGenHelperTemplates.CursorPoolsType, Product, Serializable
    {
        public static OperatorCodeGenHelperTemplates.TraversalCursorPool$ MODULE$;

        static
        {
            new OperatorCodeGenHelperTemplates.TraversalCursorPool$();
        }

        public TraversalCursorPool$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String name()
        {
            return "relationshipTraversalCursorPool";
        }

        public String productPrefix()
        {
            return "TraversalCursorPool";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorCodeGenHelperTemplates.TraversalCursorPool$;
        }

        public int hashCode()
        {
            return -226739248;
        }

        public String toString()
        {
            return "TraversalCursorPool";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
