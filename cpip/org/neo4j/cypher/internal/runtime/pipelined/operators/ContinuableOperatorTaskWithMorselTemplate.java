package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.TypeReference;
import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.MethodDeclaration;
import org.neo4j.codegen.api.Parameter;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.values.AnyValue;
import scala.Function0;
import scala.Option;
import scala.Some;
import scala.collection.GenTraversableOnce;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface ContinuableOperatorTaskWithMorselTemplate extends OperatorTaskTemplate
{
    static void $init$( final ContinuableOperatorTaskWithMorselTemplate $this )
    {
    }

    boolean isHead();

    IntermediateRepresentation genOperateHead();

    IntermediateRepresentation genOperateMiddle();

    default IntermediateRepresentation genOperate()
    {
        return this.isHead() ? this.genOperateHead() : this.genOperateMiddle();
    }

    default IntermediateRepresentation genScopeWithLocalDeclarations( final String scopeId, final Function0<IntermediateRepresentation> genBody )
    {
        this.codeGen().beginScope( scopeId );
        IntermediateRepresentation body = (IntermediateRepresentation) genBody.apply();
        OperatorExpressionCompiler qual$2 = this.codeGen();
        boolean x$15 = qual$2.endScope$default$1();
        OperatorExpressionCompiler.ScopeLocalsState localsState = qual$2.endScope( x$15 );
        return .MODULE$.block(
            (Seq) ((SeqLike) localsState.declarations().$plus$plus( localsState.assignments(), scala.collection.Seq..MODULE$.canBuildFrom()) ).$colon$plus(
            body, scala.collection.Seq..MODULE$.canBuildFrom()));
    }

    IntermediateRepresentation genClearStateOnCancelledRow();

    default IntermediateRepresentation genAdvanceOnCancelledRow()
    {
        return .MODULE$.condition(.MODULE$.not( OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_IS_VALID() ), .
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.clearStateForEachOperator(),
                OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_MOVE_TO_NEXT()}) )));
    }

    private default IntermediateRepresentation clearStateForEachOperator()
    {
        List clearStateCalls = this.map( ( x0$3 ) -> {
            IntermediateRepresentation var1;
            if ( x0$3 instanceof ContinuableOperatorTaskWithMorselTemplate )
            {
                ContinuableOperatorTaskWithMorselTemplate var3 = (ContinuableOperatorTaskWithMorselTemplate) x0$3;
                var1 = var3.genClearStateOnCancelledRow();
            }
            else
            {
                var1 = .MODULE$.noop();
            }

            return var1;
        } ); return .MODULE$.block( clearStateCalls.reverse() );
    }

    default ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        scala.None.x$32 = scala.None..MODULE$;
        Seq x$33 = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new TypeReference[]{.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( CompiledTask.class ))})));
        Seq x$34 = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new Parameter[]{OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ_CONSTRUCTOR_PARAMETER(),
                    OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL_CONSTRUCTOR_PARAMETER(),
                    OperatorCodeGenHelperTemplates$.MODULE$.ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER()}) ));
        IntermediateRepresentation x$35 = this.genInit();
        scala.collection.Seq.var10000 = scala.collection.Seq..MODULE$;
        scala.Predef.var10001 = scala.Predef..MODULE$;
        MethodDeclaration[] var10002 =
                new MethodDeclaration[]{new MethodDeclaration( "initializeProfileEvents",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Unit()),
        (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new Parameter[]{.MODULE$.param( "queryProfiler", scala.reflect.ManifestFactory..MODULE$.classType( QueryProfiler.class ))}))),
        this.genInitializeProfileEvents(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7()),
        new MethodDeclaration( "compiledOperate",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Unit()),(Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Parameter[]{.MODULE$.param( "context", scala.reflect.ManifestFactory..MODULE$.classType(
                        MorselExecutionContext.class )),.MODULE$.param( "dbAccess", scala.reflect.ManifestFactory..MODULE$.classType( QueryContext.class )), .
        MODULE$.param( "state", scala.reflect.ManifestFactory..MODULE$.classType( QueryState.class )), .
        MODULE$.param( "resources", scala.reflect.ManifestFactory..MODULE$.classType( QueryResources.class )), .
        MODULE$.param( "queryProfiler", scala.reflect.ManifestFactory..MODULE$.classType( QueryProfiler.class ))}))), .
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{this.genOperateEnter(), this.genOperateWithExpressions(), this.genOperateExit()}) )),() -> {
        return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{.MODULE$.variable( "params",.MODULE$.invoke(
                        OperatorCodeGenHelperTemplates$.MODULE$.QUERY_STATE(),.MODULE$.method( "params", scala.reflect.ManifestFactory..MODULE$.classType(
                        QueryState.class ), scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))), .
        MODULE$.variable( "cursors",.MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.QUERY_RESOURCES(),.MODULE$.method( "expressionCursors",
                scala.reflect.ManifestFactory..MODULE$.classType( QueryResources.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( ExpressionCursors.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),
        scala.reflect.ManifestFactory..MODULE$.classType( ExpressionCursors.class )), .
        MODULE$.variable( "expressionVariables",.MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.QUERY_RESOURCES(),.MODULE$.method(
                "expressionVariables", scala.reflect.ManifestFactory..MODULE$.classType( QueryResources.class ), scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..
        MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{.MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.QUERY_STATE(),.MODULE$.method(
                        "nExpressionSlots", scala.reflect.ManifestFactory..MODULE$.classType( QueryState.class ), scala.reflect.ManifestFactory..MODULE$.Int()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))}))),scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )))})))).$plus$plus( this.flatMap( ( op ) -> {
            return (Seq) op.genLocalVariables().$plus$plus( (GenTraversableOnce) op.genExpressions().flatMap( ( x$4 ) -> {
                return x$4.variables();
            }, scala.collection.Seq..MODULE$.canBuildFrom() ),scala.collection.Seq..MODULE$.canBuildFrom());
        } ), scala.collection.Seq..MODULE$.canBuildFrom());
    }, scala.None..MODULE$, new Some(.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( Exception.class )))),
        null, null, null, null, null, null, null, null, null};
        String x$16 = "compiledProduce";
        TypeReference x$17 = .MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Unit());
        Seq x$18 = (Seq) scala.collection.Seq..MODULE$.empty();
        IntermediateRepresentation x$19 = this.genProduce();
        Function0 x$20 = () -> {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        }; Some x$21 = new Some(.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( Exception.class )));
        Option x$22 = org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$6();
        var10002[2] = new MethodDeclaration( x$16, x$17, x$18, x$19, x$20, x$22, x$21 );
        String x$23 = "compiledCreateState";
        TypeReference x$24 = .MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Unit());
        Seq x$25 = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new Parameter[]{.MODULE$.param( "executionState", scala.reflect.ManifestFactory..MODULE$.classType(
                    ExecutionState.class )),.MODULE$.param( "pipelineId", scala.reflect.ManifestFactory..MODULE$.Int())})));
        IntermediateRepresentation x$26 = this.genCreateState();
        Function0 x$27 = () -> {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        }; Some x$28 = new Some(.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( Exception.class )));
        Option x$29 = org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$6();
        var10002[3] = new MethodDeclaration( x$23, x$24, x$25, x$26, x$27, x$29, x$28 );
        var10002[4] = new MethodDeclaration( "compiledOutputBuffer",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Int()),
        (Seq) scala.collection.Seq..MODULE$.empty(), (IntermediateRepresentation) this.genOutputBuffer().getOrElse( () -> {
        return .MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) );
    } ), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7());
        var10002[5] = new MethodDeclaration( "closeProfileEvents",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Unit()),(Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Parameter[]{OperatorCodeGenHelperTemplates$.MODULE$.QUERY_RESOURCE_PARAMETER()}) )),
        this.genCloseProfileEvents(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7());
        var10002[6] = new MethodDeclaration( "canContinue",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Boolean()),(Seq) scala.collection.Seq..
        MODULE$.empty(), (IntermediateRepresentation) this.genCanContinue().getOrElse( () -> {
            return .MODULE$.constant( BoxesRunTime.boxToBoolean( false ) );
        } ), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7());
        var10002[7] = new MethodDeclaration( "closeCursors",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Unit()),(Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Parameter[]{OperatorCodeGenHelperTemplates$.MODULE$.QUERY_RESOURCE_PARAMETER()}) )),
        this.genCloseCursors(), () -> {
            return (Seq) scala.collection.Seq..
            MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
        }, org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7());
        var10002[8] = new MethodDeclaration( "workIdentity",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( WorkIdentity.class )),
        (Seq) scala.collection.Seq..MODULE$.empty(), .
        MODULE$.getStatic( OperatorCodeGenHelperTemplates$.MODULE$.WORK_IDENTITY_STATIC_FIELD_NAME(), scala.reflect.ManifestFactory..MODULE$.classType(
                WorkIdentity.class )),org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7());
        var10002[9] = new MethodDeclaration( "dataRead",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),
        (Seq) scala.collection.Seq..MODULE$.empty(), .
        MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ() ), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$7());
        var10002[10] = new MethodDeclaration( "inputMorsel",.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( MorselExecutionContext.class )),
        (Seq) scala.collection.Seq..MODULE$.empty(), .
        MODULE$.loadField( OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL() ), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..
        MODULE$.apply$default$7());
        Seq x$36 = (Seq) var10000.apply( var10001.wrapRefArray( (Object[]) var10002 ) );
        Function0 x$37 = () -> {
            return (Seq) ((TraversableLike) staticFields.$plus$plus( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new InstanceField[]{OperatorCodeGenHelperTemplates$.MODULE$.DATA_READ(),
                            OperatorCodeGenHelperTemplates$.MODULE$.INPUT_MORSEL()}) )),scala.collection.Seq..MODULE$.canBuildFrom())).
            $plus$plus( this.flatMap( ( op ) -> {
                return (Seq) ((TraversableLike) op.genFields().$plus$plus( scala.Option..MODULE$.option2Iterable( op.genProfileEventField() ),
                scala.collection.Seq..MODULE$.canBuildFrom())).$plus$plus( (GenTraversableOnce) op.genExpressions().flatMap( ( x$5 ) -> {
                    return x$5.fields();
                }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom());
            } ), scala.collection.Seq..MODULE$.canBuildFrom());
        }; return new ClassDeclaration( packageName, className, x$32, x$33, x$34, x$35, x$37, x$36 );
    }
}
