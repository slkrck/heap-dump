package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.Function1;
import scala.Option;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.Seq.;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CachePropertiesOperatorTemplate implements OperatorTaskTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final Seq<Expression> propertyOps;
    private final OperatorExpressionCompiler codeGen;
    private Seq<IntermediateExpression> properties;

    public CachePropertiesOperatorTemplate( final OperatorTaskTemplate inner, final int id, final Seq<Expression> propertyOps,
            final OperatorExpressionCompiler codeGen )
    {
        this.inner = inner;
        this.id = id;
        this.propertyOps = propertyOps;
        this.codeGen = codeGen;
        OperatorTaskTemplate.$init$( this );
    }

    public String scopeId()
    {
        return OperatorTaskTemplate.scopeId$( this );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return OperatorTaskTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return OperatorTaskTemplate.genOperateEnter$( this );
    }

    public IntermediateRepresentation genOperateExit()
    {
        return OperatorTaskTemplate.genOperateExit$( this );
    }

    public IntermediateRepresentation genProduce()
    {
        return OperatorTaskTemplate.genProduce$( this );
    }

    public IntermediateRepresentation genCreateState()
    {
        return OperatorTaskTemplate.genCreateState$( this );
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return OperatorTaskTemplate.genOutputBuffer$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    public IntermediateRepresentation genInit()
    {
        return this.inner().genInit();
    }

    private Seq<IntermediateExpression> properties()
    {
        return this.properties;
    }

    private void properties_$eq( final Seq<IntermediateExpression> x$1 )
    {
        this.properties = x$1;
    }

    public IntermediateRepresentation genOperate()
    {
        this.properties_$eq( (Seq) this.propertyOps.map( ( p ) -> {
            return (IntermediateExpression) this.codeGen().intermediateCompileExpression( p ).getOrElse( () -> {
                throw new CantCompileQueryException( (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( p ).toString() );
            } );
        },.MODULE$.canBuildFrom() ));
        Seq irs = (Seq) ((TraversableLike) this.properties().map( ( x$1 ) -> {
            return x$1.ir();
        },.MODULE$.canBuildFrom())).$plus$plus(.MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.profileRow( this.id() ),
                    this.inner().genOperateWithExpressions()}) )), .MODULE$.canBuildFrom());
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( irs );
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return this.inner().genSetExecutionEvent( event );
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return this.properties();
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.empty();
    }

    public Seq<Field> genFields()
    {
        return (Seq).MODULE$.empty();
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue();
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return this.inner().genCloseCursors();
    }
}
