package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.logical.plans.MinMaxOrdering;
import org.neo4j.cypher.internal.logical.plans.QueryExpression;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekMode;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.NodeIndexSeeker;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.IndexQuery.ExactPredicate;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.storable.Value;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeIndexSeekOperator implements StreamingOperator, NodeIndexSeeker
{
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$offset;
    public final int org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$queryIndexId;
    public final IndexOrder org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexOrder;
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$argumentSize;
    private final WorkIdentity workIdentity;
    private final QueryExpression<Expression> valueExpr;
    private final IndexSeekMode indexMode;
    private final int[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertyIndices;
    private final int[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertySlotOffsets;
    private final boolean org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$needsValues;
    private final int[] propertyIds;
    private final MinMaxOrdering<Value> org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE;

    public NodeIndexSeekOperator( final WorkIdentity workIdentity, final int offset, final SlottedIndexedProperty[] properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration.Size argumentSize, final QueryExpression<Expression> valueExpr, final IndexSeekMode indexMode )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$offset = offset;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$queryIndexId = queryIndexId;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexOrder = indexOrder;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$argumentSize = argumentSize;
        this.valueExpr = valueExpr;
        this.indexMode = indexMode;
        StreamingOperator.$init$( this );
        NodeIndexSeeker.$init$( this );
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertyIndices =
                (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                        (Object[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) properties )) ).zipWithIndex( scala.Array..MODULE$.canBuildFrom(
                        scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )) )))).filter( ( x$1 ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$indexPropertyIndices$1( x$1 ) );
    } )))).map( ( x$2 ) -> {
        return BoxesRunTime.boxToInteger( $anonfun$indexPropertyIndices$2( x$2 ) );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertySlotOffsets =
                (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) properties ))).flatMap( ( x$3 ) -> {
        return scala.Option..MODULE$.option2Iterable( x$3.maybeCachedNodePropertySlot() );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$needsValues = (new ofInt( scala.Predef..MODULE$.intArrayOps(
                this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertyIndices() ))).nonEmpty();
        this.propertyIds = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) properties ))).map( ( x$4 ) -> {
        return BoxesRunTime.boxToInteger( $anonfun$propertyIds$1( x$4 ) );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
    }

    public static IndexSeekMode $lessinit$greater$default$8()
    {
        return NodeIndexSeekOperator$.MODULE$.$lessinit$greater$default$8();
    }

    public <RESULT> NodeValueIndexCursor indexSeek( final QueryState state, final IndexReadSession index, final boolean needsValues,
            final org.neo4j.cypher.internal.logical.plans.IndexOrder indexOrder, final ExecutionContext baseContext )
    {
        return NodeIndexSeeker.indexSeek$( this, state, index, needsValues, indexOrder, baseContext );
    }

    public Seq<Seq<IndexQuery>> computeIndexQueries( final QueryState state, final ExecutionContext row )
    {
        return NodeIndexSeeker.computeIndexQueries$( this, state, row );
    }

    public Seq<Seq<ExactPredicate>> computeExactQueries( final QueryState state, final ExecutionContext row )
    {
        return NodeIndexSeeker.computeExactQueries$( this, state, row );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context,
            final org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState state, final OperatorInput operatorInput, final int parallelism,
            final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public MinMaxOrdering<Value> org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE()
    {
        return this.org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE;
    }

    public final void org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$_setter_$org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE_$eq(
            final MinMaxOrdering<Value> x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE = x$1;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public QueryExpression<Expression> valueExpr()
    {
        return this.valueExpr;
    }

    public IndexSeekMode indexMode()
    {
        return this.indexMode;
    }

    public int[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertyIndices()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertyIndices;
    }

    public int[] org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertySlotOffsets()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertySlotOffsets;
    }

    public boolean org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$needsValues()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$needsValues;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext,
            final org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState state, final MorselParallelizer inputMorsel, final int parallelism,
            final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new NodeIndexSeekOperator.OTask[]{new NodeIndexSeekOperator.OTask( this, inputMorsel.nextCopy() )}) ));
    }

    public int[] propertyIds()
    {
        return this.propertyIds;
    }

    public boolean impossiblePredicates( final Seq<IndexQuery> predicates )
    {
        return predicates.exists( ( x0$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$impossiblePredicates$1( predicates, x0$1 ) );
        } );
    }

    public class OTask extends InputLoopTask
    {
        private final MorselExecutionContext inputMorsel;
        private NodeValueIndexCursor nodeCursor;
        private NodeValueIndexCursor[] cursorsToClose;
        private Value[] exactSeekValues;

        public OTask( final NodeIndexSeekOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().workIdentity();
        }

        private NodeValueIndexCursor nodeCursor()
        {
            return this.nodeCursor;
        }

        private void nodeCursor_$eq( final NodeValueIndexCursor x$1 )
        {
            this.nodeCursor = x$1;
        }

        private NodeValueIndexCursor[] cursorsToClose()
        {
            return this.cursorsToClose;
        }

        private void cursorsToClose_$eq( final NodeValueIndexCursor[] x$1 )
        {
            this.cursorsToClose = x$1;
        }

        private Value[] exactSeekValues()
        {
            return this.exactSeekValues;
        }

        private void exactSeekValues_$eq( final Value[] x$1 )
        {
            this.exactSeekValues = x$1;
        }

        public boolean initializeInnerLoop( final QueryContext context, final org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState state,
                final QueryResources resources, final ExecutionContext initExecutionContext )
        {
            SlottedQueryState queryState = new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(),
                    (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
            resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
            MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
            ;
            initExecutionContext.copyFrom( this.inputMorsel(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$argumentSize.nLongs(),
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$argumentSize.nReferences() );
            Seq indexQueries = this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().computeIndexQueries( queryState,
                    initExecutionContext );
            Read read = context.transactionalContext().transaction().dataRead();
            if ( indexQueries.size() == 1 )
            {
                this.nodeCursor_$eq( (NodeValueIndexCursor) resources.cursorPools().nodeValueIndexCursorPool().allocateAndTrace() );
                this.cursorsToClose_$eq( (NodeValueIndexCursor[]) ((Object[]) (new NodeValueIndexCursor[]{this.nodeCursor()})) );
                this.seek(
                        state.queryIndexes()[this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$queryIndexId],
                        this.nodeCursor(), read, (Seq) indexQueries.head() );
            }
            else
            {
                this.cursorsToClose_$eq( (NodeValueIndexCursor[]) ((TraversableOnce) ((TraversableLike) indexQueries.filterNot( ( predicates ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$initializeInnerLoop$1( this, predicates ) );
                } )).map( ( query ) -> {
                    IndexReadSession var10001;
                    IndexOrder var10003;
                    NodeValueIndexCursor cursor;
                    boolean var7;
                    label24:
                    {
                        cursor = (NodeValueIndexCursor) resources.cursorPools().nodeValueIndexCursorPool().allocateAndTrace();
                        var10001 =
                                state.queryIndexes()[this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$queryIndexId];
                        var10003 =
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexOrder;
                        if ( !this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$needsValues() )
                        {
                            label23:
                            {
                                IndexOrder var10004 =
                                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexOrder;
                                IndexOrder var6 = IndexOrder.NONE;
                                if ( var10004 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label23;
                                    }
                                }
                                else if ( !var10004.equals( var6 ) )
                                {
                                    break label23;
                                }

                                var7 = false;
                                break label24;
                            }
                        }

                        var7 = true;
                    }

                    read.nodeIndexSeek( var10001, cursor, var10003, var7,
                            (IndexQuery[]) query.toArray( scala.reflect.ClassTag..MODULE$.apply( IndexQuery.class ) ));
                    return cursor;
                }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( NodeValueIndexCursor.class )));
                this.nodeCursor_$eq( this.orderedCursor(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexOrder,
                        this.cursorsToClose() ) );
            }

            return true;
        }

        private NodeValueIndexCursor orderedCursor( final IndexOrder indexOrder, final NodeValueIndexCursor[] cursors )
        {
            NodeValueIndexCursor var3;
            if ( IndexOrder.NONE.equals( indexOrder ) )
            {
                var3 = org.neo4j.cypher.internal.runtime.CompositeValueIndexCursor..MODULE$.unordered( cursors );
            }
            else if ( IndexOrder.ASCENDING.equals( indexOrder ) )
            {
                var3 = org.neo4j.cypher.internal.runtime.CompositeValueIndexCursor..MODULE$.ascending( cursors );
            }
            else
            {
                if ( !IndexOrder.DESCENDING.equals( indexOrder ) )
                {
                    throw new MatchError( indexOrder );
                }

                var3 = org.neo4j.cypher.internal.runtime.CompositeValueIndexCursor..MODULE$.descending( cursors );
            }

            return var3;
        }

        public void innerLoop( final MorselExecutionContext outputRow, final QueryContext context,
                final org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState state )
        {
            while ( outputRow.isValidRow() && this.nodeCursor() != null && this.nodeCursor().next() )
            {
                outputRow.copyFrom( this.inputMorsel(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$argumentSize.nReferences() );
                outputRow.setLongAt(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$offset,
                        this.nodeCursor().nodeReference() );

                for ( int i = 0; i <
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertyIndices().length;
                        ++i )
                {
                    int indexPropertyIndex =
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertyIndices()[i];
                    Value value =
                            this.exactSeekValues() != null ? this.exactSeekValues()[indexPropertyIndex] : this.nodeCursor().propertyValue( indexPropertyIndex );
                    outputRow.setCachedPropertyAt(
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexPropertySlotOffsets()[i],
                            value );
                }

                outputRow.moveToNextRow();
            }
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            if ( this.nodeCursor() != null )
            {
                this.nodeCursor().setTracer( event );
            }
        }

        public void closeInnerLoop( final QueryResources resources )
        {
            if ( this.cursorsToClose() != null )
            {
                (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.cursorsToClose() ))).foreach( ( cursor ) -> {
                $anonfun$closeInnerLoop$1( resources, cursor );
                return BoxedUnit.UNIT;
            } );
                this.cursorsToClose_$eq( (NodeValueIndexCursor[]) null );
            }

            this.nodeCursor_$eq( (NodeValueIndexCursor) null );
        }

        private <RESULT> void seek( final IndexReadSession index, final NodeValueIndexCursor nodeCursor, final Read read, final Seq<IndexQuery> predicates )
        {
            if ( !this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().impossiblePredicates( predicates ) )
            {
                this.exactSeekValues_$eq(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$needsValues() &&
                                predicates.forall( ( x$5 ) -> {
                                    return BoxesRunTime.boxToBoolean( $anonfun$seek$1( x$5 ) );
                                } ) ? (Value[]) ((TraversableOnce) predicates.map( ( x$6 ) -> {
                            return ((ExactPredicate) x$6).value();
                        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( Value.class )) :null);
                boolean needsValuesFromIndexSeek = this.exactSeekValues() == null &&
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$needsValues();
                read.nodeIndexSeek( index, nodeCursor,
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$NodeIndexSeekOperator$$indexOrder,
                        needsValuesFromIndexSeek, (IndexQuery[]) predicates.toArray( scala.reflect.ClassTag..MODULE$.apply( IndexQuery.class ) ));
            }
        }
    }
}
