package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class ArgumentOperatorTaskTemplate$
{
    public static ArgumentOperatorTaskTemplate$ MODULE$;

    static
    {
        new ArgumentOperatorTaskTemplate$();
    }

    private ArgumentOperatorTaskTemplate$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$5()
    {
        return true;
    }
}
