package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SingleUndirectedRelationshipByIdSeekTaskTemplate extends SingleRelationshipByIdSeekTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int relationshipOffset;
    private final int fromOffset;
    private final int toOffset;
    private final SlotConfiguration.Size argumentSize;
    private final Field forwardDirection;

    public SingleUndirectedRelationshipByIdSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final int relationshipOffset, final int fromOffset, final int toOffset, final Expression relIdExpr, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, relationshipOffset, fromOffset, toOffset, relIdExpr, argumentSize, codeGen );
        this.innermost = innermost;
        this.relationshipOffset = relationshipOffset;
        this.fromOffset = fromOffset;
        this.toOffset = toOffset;
        this.argumentSize = argumentSize;
        this.forwardDirection = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                BoxesRunTime.boxToBoolean( true ) ), scala.reflect.ManifestFactory..MODULE$.Boolean());
    }

    private Field forwardDirection()
    {
        return this.forwardDirection;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Field[]{this.cursor(), this.forwardDirection()}) ));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                        super.codeGen().setLongAt( this.relationshipOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                this.idVariable() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ifElse(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.forwardDirection() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    super.codeGen().setLongAt( this.fromOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "sourceNodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                    RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.codeGen().setLongAt( this.toOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "targetNodeReference", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.forwardDirection(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ))}))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
            super.codeGen().setLongAt( this.fromOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "targetNodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
            RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        super.codeGen().setLongAt( this.toOffset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.cursor() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "sourceNodeReference", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.forwardDirection(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) ))})))),
        super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue(
                OperatorCodeGenHelperTemplates$.MODULE$.profileRow( super.id() ) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.forwardDirection() )))}))));
    }
}
