package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import scala.Function0;
import scala.Function1;
import scala.Option;
import scala.Some;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.Seq.;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class InputLoopTaskTemplate implements ContinuableOperatorTaskWithMorselTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final DelegateOperatorTaskTemplate innermost;
    private final OperatorExpressionCompiler codeGen;
    private final boolean isHead;
    private final InstanceField canContinue;
    private final InstanceField innerLoop;

    public InputLoopTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final OperatorExpressionCompiler codeGen, final boolean isHead )
    {
        this.inner = inner;
        this.id = id;
        this.innermost = innermost;
        this.codeGen = codeGen;
        this.isHead = isHead;
        OperatorTaskTemplate.$init$( this );
        ContinuableOperatorTaskWithMorselTemplate.$init$( this );
        this.canContinue = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 11 )).append( this.scopeId() ).append( "CanContinue" ).toString(), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.innerLoop = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( (new StringBuilder( 9 )).append( this.scopeId() ).append( "InnerLoop" ).toString(), scala.reflect.ManifestFactory..MODULE$.Boolean());
    }

    public static boolean $lessinit$greater$default$5()
    {
        return InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5();
    }

    public final IntermediateRepresentation genOperate()
    {
        return ContinuableOperatorTaskWithMorselTemplate.genOperate$( this );
    }

    public IntermediateRepresentation genScopeWithLocalDeclarations( final String scopeId, final Function0<IntermediateRepresentation> genBody )
    {
        return ContinuableOperatorTaskWithMorselTemplate.genScopeWithLocalDeclarations$( this, scopeId, genBody );
    }

    public IntermediateRepresentation genAdvanceOnCancelledRow()
    {
        return ContinuableOperatorTaskWithMorselTemplate.genAdvanceOnCancelledRow$( this );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return ContinuableOperatorTaskWithMorselTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return OperatorTaskTemplate.genOperateEnter$( this );
    }

    public IntermediateRepresentation genOperateExit()
    {
        return OperatorTaskTemplate.genOperateExit$( this );
    }

    public IntermediateRepresentation genProduce()
    {
        return OperatorTaskTemplate.genProduce$( this );
    }

    public IntermediateRepresentation genCreateState()
    {
        return OperatorTaskTemplate.genCreateState$( this );
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return OperatorTaskTemplate.genOutputBuffer$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    public boolean isHead()
    {
        return this.isHead;
    }

    public InstanceField canContinue()
    {
        return this.canContinue;
    }

    public InstanceField innerLoop()
    {
        return this.innerLoop;
    }

    public String scopeId()
    {
        return (new StringBuilder( 12 )).append( "leafOperator" ).append( this.id() ).toString();
    }

    public final Seq<Field> genFields()
    {
        return (Seq) ((TraversableLike).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new InstanceField[]{this.canContinue(), this.innerLoop()}) ))).$plus$plus( this.genMoreFields(),.MODULE$.canBuildFrom());
    }

    public abstract Seq<Field> genMoreFields();

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue().map( ( x$1 ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.or( x$1, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() ));
        } ).orElse( () -> {
            return new Some( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() ));
        } );
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.genCloseInnerLoop(), this.inner().genCloseCursors()}) ));
    }

    public IntermediateRepresentation genInit()
    {
        return this.inner().genInit();
    }

    public final IntermediateRepresentation genOperateHead()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.genAdvanceOnCancelledRow(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.labeledLoop(
                    OperatorCodeGenHelperTemplates$.MODULE$.OUTER_LOOP_LABEL_NAME(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.or( OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_IS_VALID(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.innerLoop() )),
            this.innermost.predicate() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.doInitializeInnerLoopOrRestoreContinuationState(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ifElse( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.innerLoop() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    this.genScopeWithLocalDeclarations( (new StringBuilder( 9 )).append( this.scopeId() ).append( "innerLoop" ).toString(), () -> {
                        return this.genInnerLoop();
                    } ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.genCloseInnerLoop(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                    this.innerLoop(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
            this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_MOVE_TO_NEXT(),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                            OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_IS_VALID() )}))))}))))}))),
        this.doIfInnerCantContinue( OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_MOVE_TO_NEXT() )),this.innermost.resetCachedPropertyVariables()}))))})));
    }

    public final IntermediateRepresentation genOperateMiddle()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                    OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_IS_VALID() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loop(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.or(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loadField( this.innerLoop() )),this.innermost.predicate()),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.doInitializeInnerLoopOrRestoreContinuationState(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.innerLoop() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{
                        this.genScopeWithLocalDeclarations( (new StringBuilder( 9 )).append( this.scopeId() ).append( "innerLoop" ).toString(), () -> {
                            return this.genInnerLoop();
                        } ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{this.genCloseInnerLoop(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                        this.innerLoop(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ))}))))})))),
        this.innermost.resetCachedPropertyVariables()}))))})));
    }

    public IntermediateRepresentation genClearStateOnCancelledRow()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.innerLoop() ),
            this.genCloseInnerLoop() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.innerLoop(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ))})));
    }

    private IntermediateRepresentation doInitializeInnerLoopOrRestoreContinuationState()
    {
        this.codeGen().beginScope( (new StringBuilder( 4 )).append( this.scopeId() ).append( "init" ).toString() );
        IntermediateRepresentation body = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.innerLoop() )),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.innerLoop(),
                    this.genInitializeInnerLoop() )}))));
        OperatorExpressionCompiler.ScopeLocalsState localsState = this.codeGen().endScope( true );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( (Seq) localsState.assignments().$colon$plus( body,.MODULE$.canBuildFrom() ));
    }

    public abstract IntermediateRepresentation genInitializeInnerLoop();

    public abstract IntermediateRepresentation genInnerLoop();

    public abstract IntermediateRepresentation genCloseInnerLoop();

    public IntermediateRepresentation endInnerLoop()
    {
        return this.innermost.resetCachedPropertyVariables();
    }
}
