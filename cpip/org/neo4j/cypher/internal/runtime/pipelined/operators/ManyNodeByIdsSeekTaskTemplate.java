package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ManyNodeByIdsSeekTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int offset;
    private final Expression nodeIdsExpr;
    private final SlotConfiguration.Size argumentSize;
    private final InstanceField idCursor;
    private IntermediateExpression nodeIds;

    public ManyNodeByIdsSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final String nodeVarName, final int offset, final Expression nodeIdsExpr, final SlotConfiguration.Size argumentSize,
            final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.offset = offset;
        this.nodeIdsExpr = nodeIdsExpr;
        this.argumentSize = argumentSize;
        this.idCursor = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ));
    }

    private InstanceField idCursor()
    {
        return this.idCursor;
    }

    private IntermediateExpression nodeIds()
    {
        return this.nodeIds;
    }

    private void nodeIds_$eq( final IntermediateExpression x$1 )
    {
        this.nodeIds = x$1;
    }

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.idCursor()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.CURSOR_POOL_V()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{this.nodeIds()}) ));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        this.nodeIds_$eq( (IntermediateExpression) super.codeGen().intermediateCompileExpression( this.nodeIdsExpr ).getOrElse( () -> {
            throw new CantCompileQueryException(
                    (new StringBuilder( 42 )).append( "The expression compiler could not compile " ).append( this.nodeIdsExpr ).toString() );
        } ) );
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.idCursor(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                    "apply", scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType(
            IteratorCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( this.nodeIds(), ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ),
                    scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
            "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
            OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.idCursor() ),
            scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class ))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        String idVariable = super.codeGen().namer().nextVariableName();
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()), idVariable,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic( NodeByIdSeekOperator$.MODULE$.asIdMethod(),
                scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.idCursor() ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "value", scala.reflect.ManifestFactory..MODULE$.classType(
                IteratorCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))})))),
        OperatorCodeGenHelperTemplates$.MODULE$.onNode( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                this.executionEventField() ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( idVariable )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
            NodeByIdSeekOperator$.MODULE$.isValidNode( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                    idVariable ) ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                    super.codeGen().setLongAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( idVariable )),
            super.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( OperatorCodeGenHelperTemplates$.MODULE$.profileRow( super.id() ) )})))),
        this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                        OperatorCodeGenHelperTemplates$.MODULE$.cursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                this.idCursor() ), scala.reflect.ManifestFactory..MODULE$.classType( IteratorCursor.class )) )})))),this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
    }
}
