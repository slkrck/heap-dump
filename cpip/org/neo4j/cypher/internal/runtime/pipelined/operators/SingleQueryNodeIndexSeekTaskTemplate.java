package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.internal.kernel.api.KernelReadTracer;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.schema.IndexOrder;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class SingleQueryNodeIndexSeekTaskTemplate extends InputLoopTaskTemplate
{
    private final DelegateOperatorTaskTemplate innermost;
    private final int offset;
    private final SlottedIndexedProperty property;
    private final IndexOrder order;
    private final boolean needsValues;
    private final int queryIndexId;
    private final SlotConfiguration.Size argumentSize;
    private final InstanceField nodeIndexCursorField;

    public SingleQueryNodeIndexSeekTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost, final int offset,
            final SlottedIndexedProperty property, final IndexOrder order, final boolean needsValues, final int queryIndexId,
            final SlotConfiguration.Size argumentSize, final OperatorExpressionCompiler codeGen )
    {
        super( inner, id, innermost, codeGen, InputLoopTaskTemplate$.MODULE$.$lessinit$greater$default$5() );
        this.innermost = innermost;
        this.offset = offset;
        this.property = property;
        this.order = order;
        this.needsValues = needsValues;
        this.queryIndexId = queryIndexId;
        this.argumentSize = argumentSize;
        this.nodeIndexCursorField = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.field( super.codeGen().namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ));
    }

    public OperatorTaskTemplate inner()
    {
        return super.inner();
    }

    public InstanceField nodeIndexCursorField()
    {
        return this.nodeIndexCursorField;
    }

    public abstract IntermediateRepresentation getPropertyValue();

    public abstract IntermediateRepresentation beginInnerLoop();

    public abstract IntermediateRepresentation isPredicatePossible();

    public abstract IntermediateRepresentation predicate();

    public Seq<Field> genMoreFields()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{this.nodeIndexCursorField()}) ));
    }

    public IntermediateRepresentation genInitializeInnerLoop()
    {
        String hasInnerLoopVar = super.codeGen().namer().nextVariableName();
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.beginInnerLoop(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Boolean()), hasInnerLoopVar,
            this.isPredicatePossible() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.setField( this.canContinue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( hasInnerLoopVar ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                OperatorCodeGenHelperTemplates$.MODULE$.allocateAndTraceCursor( this.nodeIndexCursorField(), this.executionEventField(),
                        OperatorCodeGenHelperTemplates$.MODULE$.ALLOCATE_NODE_INDEX_CURSOR() ),
                OperatorCodeGenHelperTemplates$.MODULE$.nodeIndexSeek( OperatorCodeGenHelperTemplates$.MODULE$.indexReadSession( this.queryIndexId ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ), this.predicate(), this.order,
                this.needsValues), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeIndexCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class )))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( hasInnerLoopVar )})));
    }

    public IntermediateRepresentation genInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( this.innermost.predicate(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.canContinue() )),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{super.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                        super.codeGen().setLongAt( this.offset, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( this.nodeIndexCursorField() ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeReference", scala.reflect.ManifestFactory..MODULE$.classType(
                        NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
        (IntermediateRepresentation) this.property.maybeCachedNodePropertySlot().map( ( x$7 ) -> {
            return $anonfun$genInnerLoop$1( this, BoxesRunTime.unboxToInt( x$7 ) );
        } ).getOrElse( () -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop();
        } ), this.inner().genOperateWithExpressions(), this.doIfInnerCantContinue( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField(
                this.canContinue(),
                OperatorCodeGenHelperTemplates$.MODULE$.profilingCursorNext( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                        this.nodeIndexCursorField() ), super.id(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class )))),
        this.endInnerLoop()}))));
    }

    public IntermediateRepresentation genCloseInnerLoop()
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{
                    OperatorCodeGenHelperTemplates$.MODULE$.freeCursor( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                            this.nodeIndexCursorField() ), OperatorCodeGenHelperTemplates.NodeValueIndexCursorPool$.MODULE$,
                    scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( this.nodeIndexCursorField(),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ))})));
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNotNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                    this.nodeIndexCursorField() )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
            this.nodeIndexCursorField() ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "setTracer", scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( KernelReadTracer.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{event}) ))),this.inner().genSetExecutionEvent( event )})));
    }
}
