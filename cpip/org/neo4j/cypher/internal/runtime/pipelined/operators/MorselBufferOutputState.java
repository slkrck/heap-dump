package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselBufferOutputState implements OutputOperatorState, Product, Serializable
{
    private final WorkIdentity workIdentity;
    private final boolean trackTime;
    private final int bufferId;
    private final ExecutionState executionState;
    private final int pipelineId;

    public MorselBufferOutputState( final WorkIdentity workIdentity, final boolean trackTime, final int bufferId, final ExecutionState executionState,
            final int pipelineId )
    {
        this.workIdentity = workIdentity;
        this.trackTime = trackTime;
        this.bufferId = bufferId;
        this.executionState = executionState;
        this.pipelineId = pipelineId;
        OutputOperatorState.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple5<WorkIdentity,Object,BufferId,ExecutionState,PipelineId>> unapply( final MorselBufferOutputState x$0 )
    {
        return MorselBufferOutputState$.MODULE$.unapply( var0 );
    }

    public static MorselBufferOutputState apply( final WorkIdentity workIdentity, final boolean trackTime, final int bufferId,
            final ExecutionState executionState, final int pipelineId )
    {
        return MorselBufferOutputState$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public static Function1<Tuple5<WorkIdentity,Object,BufferId,ExecutionState,PipelineId>,MorselBufferOutputState> tupled()
    {
        return MorselBufferOutputState$.MODULE$.tupled();
    }

    public static Function1<WorkIdentity,Function1<Object,Function1<BufferId,Function1<ExecutionState,Function1<PipelineId,MorselBufferOutputState>>>>> curried()
    {
        return MorselBufferOutputState$.MODULE$.curried();
    }

    public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
            final QueryResources resources, final QueryProfiler queryProfiler )
    {
        return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
    }

    public boolean canContinueOutput()
    {
        return OutputOperatorState.canContinueOutput$( this );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public boolean trackTime()
    {
        return this.trackTime;
    }

    public int bufferId()
    {
        return this.bufferId;
    }

    public ExecutionState executionState()
    {
        return this.executionState;
    }

    public int pipelineId()
    {
        return this.pipelineId;
    }

    public PreparedOutput prepareOutput( final MorselExecutionContext outputMorsel, final QueryContext context, final QueryState state,
            final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
    {
        return new MorselBufferPreparedOutput( this.bufferId(), this.executionState(), this.pipelineId(), outputMorsel );
    }

    public MorselBufferOutputState copy( final WorkIdentity workIdentity, final boolean trackTime, final int bufferId, final ExecutionState executionState,
            final int pipelineId )
    {
        return new MorselBufferOutputState( workIdentity, trackTime, bufferId, executionState, pipelineId );
    }

    public WorkIdentity copy$default$1()
    {
        return this.workIdentity();
    }

    public boolean copy$default$2()
    {
        return this.trackTime();
    }

    public int copy$default$3()
    {
        return this.bufferId();
    }

    public ExecutionState copy$default$4()
    {
        return this.executionState();
    }

    public int copy$default$5()
    {
        return this.pipelineId();
    }

    public String productPrefix()
    {
        return "MorselBufferOutputState";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.workIdentity();
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.trackTime() );
            break;
        case 2:
            var10000 = new BufferId( this.bufferId() );
            break;
        case 3:
            var10000 = this.executionState();
            break;
        case 4:
            var10000 = new PipelineId( this.pipelineId() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselBufferOutputState;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.workIdentity() ) );
        var1 = Statics.mix( var1, this.trackTime() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( new BufferId( this.bufferId() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.executionState() ) );
        var1 = Statics.mix( var1, Statics.anyHash( new PipelineId( this.pipelineId() ) ) );
        return Statics.finalizeHash( var1, 5 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label68:
            {
                boolean var2;
                if ( x$1 instanceof MorselBufferOutputState )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label51:
                    {
                        label50:
                        {
                            MorselBufferOutputState var4 = (MorselBufferOutputState) x$1;
                            WorkIdentity var10000 = this.workIdentity();
                            WorkIdentity var5 = var4.workIdentity();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label50;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label50;
                            }

                            if ( this.trackTime() == var4.trackTime() && this.bufferId() == var4.bufferId() )
                            {
                                label43:
                                {
                                    ExecutionState var7 = this.executionState();
                                    ExecutionState var6 = var4.executionState();
                                    if ( var7 == null )
                                    {
                                        if ( var6 != null )
                                        {
                                            break label43;
                                        }
                                    }
                                    else if ( !var7.equals( var6 ) )
                                    {
                                        break label43;
                                    }

                                    if ( this.pipelineId() == var4.pipelineId() && var4.canEqual( this ) )
                                    {
                                        var8 = true;
                                        break label51;
                                    }
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label68;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
