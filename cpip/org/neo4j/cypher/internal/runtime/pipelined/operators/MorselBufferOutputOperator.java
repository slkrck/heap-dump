package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityImpl;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselBufferOutputOperator implements OutputOperator, Product, Serializable
{
    private final int bufferId;
    private final int nextPipelineHeadPlanId;
    private final boolean nextPipelineFused;
    private final WorkIdentity workIdentity;

    public MorselBufferOutputOperator( final int bufferId, final int nextPipelineHeadPlanId, final boolean nextPipelineFused )
    {
        this.bufferId = bufferId;
        this.nextPipelineHeadPlanId = nextPipelineHeadPlanId;
        this.nextPipelineFused = nextPipelineFused;
        Product.$init$( this );
        this.workIdentity = new WorkIdentityImpl( nextPipelineHeadPlanId,
                (new StringBuilder( 17 )).append( "Output morsel to " ).append( new BufferId( bufferId ) ).toString() );
    }

    public static Option<Tuple3<BufferId,Id,Object>> unapply( final MorselBufferOutputOperator x$0 )
    {
        return MorselBufferOutputOperator$.MODULE$.unapply( var0 );
    }

    public static MorselBufferOutputOperator apply( final int bufferId, final int nextPipelineHeadPlanId, final boolean nextPipelineFused )
    {
        return MorselBufferOutputOperator$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<BufferId,Id,Object>,MorselBufferOutputOperator> tupled()
    {
        return MorselBufferOutputOperator$.MODULE$.tupled();
    }

    public static Function1<BufferId,Function1<Id,Function1<Object,MorselBufferOutputOperator>>> curried()
    {
        return MorselBufferOutputOperator$.MODULE$.curried();
    }

    public int bufferId()
    {
        return this.bufferId;
    }

    public int nextPipelineHeadPlanId()
    {
        return this.nextPipelineHeadPlanId;
    }

    public boolean nextPipelineFused()
    {
        return this.nextPipelineFused;
    }

    public Option<BufferId> outputBuffer()
    {
        return new Some( new BufferId( this.bufferId() ) );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
    {
        return new MorselBufferOutputState( this.workIdentity(), !this.nextPipelineFused(), this.bufferId(), executionState, pipelineId );
    }

    public MorselBufferOutputOperator copy( final int bufferId, final int nextPipelineHeadPlanId, final boolean nextPipelineFused )
    {
        return new MorselBufferOutputOperator( bufferId, nextPipelineHeadPlanId, nextPipelineFused );
    }

    public int copy$default$1()
    {
        return this.bufferId();
    }

    public int copy$default$2()
    {
        return this.nextPipelineHeadPlanId();
    }

    public boolean copy$default$3()
    {
        return this.nextPipelineFused();
    }

    public String productPrefix()
    {
        return "MorselBufferOutputOperator";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new BufferId( this.bufferId() );
            break;
        case 1:
            var10000 = new Id( this.nextPipelineHeadPlanId() );
            break;
        case 2:
            var10000 = BoxesRunTime.boxToBoolean( this.nextPipelineFused() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselBufferOutputOperator;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( new BufferId( this.bufferId() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( new Id( this.nextPipelineHeadPlanId() ) ) );
        var1 = Statics.mix( var1, this.nextPipelineFused() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label53:
            {
                boolean var2;
                if ( x$1 instanceof MorselBufferOutputOperator )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    MorselBufferOutputOperator var4 = (MorselBufferOutputOperator) x$1;
                    if ( this.bufferId() == var4.bufferId() && this.nextPipelineHeadPlanId() == var4.nextPipelineHeadPlanId() &&
                            this.nextPipelineFused() == var4.nextPipelineFused() && var4.canEqual( this ) )
                    {
                        break label53;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
