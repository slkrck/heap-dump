package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPools;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipGroupCursor;
import org.neo4j.internal.kernel.api.RelationshipTraversalCursor;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.RelationshipValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.MatchError;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public abstract class VarExpandCursor
{
    private final long fromNode;
    private final long targetToNode;
    private final NodeCursor nodeCursor;
    private final boolean projectBackwards;
    private final int[] relTypes;
    private final int minLength;
    private final int maxLength;
    private final Read read;
    private final ExecutionContext executionContext;
    private final DbAccess dbAccess;
    private final AnyValue[] params;
    private final ExpressionCursors cursors;
    private final AnyValue[] expressionVariables;
    private final GrowingArray<RelationshipTraversalCursor> relTraCursors;
    private final GrowingArray<RelationshipGroupCursor> relGroupCursors;
    private final GrowingArray<RelationshipSelectionCursor> selectionCursors;
    private ExpandStatus expandStatus;
    private int pathLength;
    private OperatorProfileEvent event;
    private CursorPools cursorPools;

    public VarExpandCursor( final long fromNode, final long targetToNode, final NodeCursor nodeCursor, final boolean projectBackwards, final int[] relTypes,
            final int minLength, final int maxLength, final Read read, final ExecutionContext executionContext, final DbAccess dbAccess,
            final AnyValue[] params, final ExpressionCursors cursors, final AnyValue[] expressionVariables )
    {
        this.fromNode = fromNode;
        this.targetToNode = targetToNode;
        this.nodeCursor = nodeCursor;
        this.projectBackwards = projectBackwards;
        this.relTypes = relTypes;
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.read = read;
        this.executionContext = executionContext;
        this.dbAccess = dbAccess;
        this.params = params;
        this.cursors = cursors;
        this.expressionVariables = expressionVariables;
        this.expandStatus = NOT_STARTED$.MODULE$;
        this.pathLength = 0;
        this.relTraCursors = new GrowingArray();
        this.relGroupCursors = new GrowingArray();
        this.selectionCursors = new GrowingArray();
    }

    public static RelationshipValue relationshipFromCursor( final DbAccess dbAccess, final RelationshipSelectionCursor cursor )
    {
        return VarExpandCursor$.MODULE$.relationshipFromCursor( var0, var1 );
    }

    public static VarExpandCursor apply( final SemanticDirection direction, final long fromNode, final long targetToNode, final NodeCursor nodeCursor,
            final boolean projectBackwards, final int[] relTypes, final int minLength, final int maxLength, final Read read, final DbAccess dbAccess,
            final VarExpandPredicate<Object> nodePredicate, final VarExpandPredicate<RelationshipSelectionCursor> relationshipPredicate )
    {
        return VarExpandCursor$.MODULE$.apply( var0, var1, var3, var5, var6, var7, var8, var9, var10, var11, var12, var13 );
    }

    public long fromNode()
    {
        return this.fromNode;
    }

    public long targetToNode()
    {
        return this.targetToNode;
    }

    private ExpandStatus expandStatus()
    {
        return this.expandStatus;
    }

    private void expandStatus_$eq( final ExpandStatus x$1 )
    {
        this.expandStatus = x$1;
    }

    private int pathLength()
    {
        return this.pathLength;
    }

    private void pathLength_$eq( final int x$1 )
    {
        this.pathLength = x$1;
    }

    private OperatorProfileEvent event()
    {
        return this.event;
    }

    private void event_$eq( final OperatorProfileEvent x$1 )
    {
        this.event = x$1;
    }

    private GrowingArray<RelationshipTraversalCursor> relTraCursors()
    {
        return this.relTraCursors;
    }

    private GrowingArray<RelationshipGroupCursor> relGroupCursors()
    {
        return this.relGroupCursors;
    }

    private GrowingArray<RelationshipSelectionCursor> selectionCursors()
    {
        return this.selectionCursors;
    }

    private CursorPools cursorPools()
    {
        return this.cursorPools;
    }

    private void cursorPools_$eq( final CursorPools x$1 )
    {
        this.cursorPools = x$1;
    }

    public abstract RelationshipSelectionCursor selectionCursor( final RelationshipGroupCursor groupCursor, final RelationshipTraversalCursor traversalCursor,
            final NodeCursor node, final int[] types );

    public abstract boolean satisfyPredicates( final ExecutionContext executionContext, final DbAccess dbAccess, final AnyValue[] params,
            final ExpressionCursors cursors, final AnyValue[] expressionVariables, final RelationshipSelectionCursor selectionCursor );

    public void enterWorkUnit( final CursorPools cursorPools )
    {
        this.cursorPools_$eq( cursorPools );
    }

    public boolean next()
    {
        ExpandStatus var10000;
        label81:
        {
            var10000 = this.expandStatus();
            NOT_STARTED$ var2 = NOT_STARTED$.MODULE$;
            if ( var10000 == null )
            {
                if ( var2 != null )
                {
                    break label81;
                }
            }
            else if ( !var10000.equals( var2 ) )
            {
                break label81;
            }

            this.expandStatus_$eq( (ExpandStatus) (this.pathLength() < this.maxLength ? EXPAND$.MODULE$ : EMIT$.MODULE$) );
            if ( this.minLength == 0 && this.validToNode() )
            {
                return true;
            }
        }

        while ( true )
        {
            if ( this.pathLength() <= 0 )
            {
                var10000 = this.expandStatus();
                EXPAND$ var3 = EXPAND$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var3 != null )
                    {
                        break;
                    }
                }
                else if ( !var10000.equals( var3 ) )
                {
                    break;
                }
            }

            ExpandStatus var4 = this.expandStatus();
            if ( EXPAND$.MODULE$.equals( var4 ) )
            {
                this.expand( this.toNode() );
                this.pathLength_$eq( this.pathLength() + 1 );
                this.expandStatus_$eq( EMIT$.MODULE$ );
                BoxedUnit var1 = BoxedUnit.UNIT;
            }
            else
            {
                if ( !EMIT$.MODULE$.equals( var4 ) )
                {
                    throw new MatchError( var4 );
                }

                int r = this.pathLength() - 1;
                RelationshipSelectionCursor selectionCursor = (RelationshipSelectionCursor) this.selectionCursors().get( r );
                boolean hasNext = false;

                do
                {
                    hasNext = selectionCursor.next();
                }
                while ( hasNext && (!this.relationshipIsUniqueInPath( r, selectionCursor.relationshipReference() ) ||
                        !this.satisfyPredicates( this.executionContext, this.dbAccess, this.params, this.cursors, this.expressionVariables,
                                selectionCursor )) );

                BoxedUnit var8;
                if ( hasNext )
                {
                    if ( this.pathLength() < this.maxLength )
                    {
                        this.expandStatus_$eq( EXPAND$.MODULE$ );
                    }

                    if ( this.pathLength() >= this.minLength && this.validToNode() )
                    {
                        return true;
                    }

                    var8 = BoxedUnit.UNIT;
                }
                else
                {
                    this.expandStatus_$eq( EMIT$.MODULE$ );
                    this.pathLength_$eq( this.pathLength() - 1 );
                    var8 = BoxedUnit.UNIT;
                }
            }
        }

        return false;
    }

    private boolean validToNode()
    {
        return this.targetToNode() < 0L || this.targetToNode() == this.toNode();
    }

    private boolean relationshipIsUniqueInPath( final int relInPath, final long relationshipId )
    {
        for ( int i = relInPath - 1; i >= 0; --i )
        {
            if ( ((RelationshipSelectionCursor) this.selectionCursors().get( i )).relationshipReference() == relationshipId )
            {
                return false;
            }
        }

        return true;
    }

    private void expand( final long node )
    {
        this.read.singleNode( node, this.nodeCursor );
        RelationshipSelectionCursor var10000;
        if ( !this.nodeCursor.next() )
        {
            var10000 = RelationshipSelectionCursor.EMPTY;
        }
        else
        {
            RelationshipGroupCursor groupCursor = (RelationshipGroupCursor) this.relGroupCursors().computeIfAbsent( this.pathLength(), () -> {
                RelationshipGroupCursor cursor = (RelationshipGroupCursor) this.cursorPools().relationshipGroupCursorPool().allocateAndTrace();
                cursor.setTracer( this.event() );
                return cursor;
            } );
            RelationshipTraversalCursor traversalCursor = (RelationshipTraversalCursor) this.relTraCursors().computeIfAbsent( this.pathLength(), () -> {
                RelationshipTraversalCursor cursor = (RelationshipTraversalCursor) this.cursorPools().relationshipTraversalCursorPool().allocateAndTrace();
                cursor.setTracer( this.event() );
                return cursor;
            } );
            var10000 = this.selectionCursor( groupCursor, traversalCursor, this.nodeCursor, this.relTypes );
        }

        RelationshipSelectionCursor cursor = var10000;
        this.selectionCursors().set( this.pathLength(), cursor );
    }

    public long toNode()
    {
        return this.selectionCursors().hasNeverSeenData() ? this.fromNode() : ((RelationshipSelectionCursor) this.selectionCursors().get(
                this.pathLength() - 1 )).otherNodeReference();
    }

    public ListValue relationships()
    {
        AnyValue[] r = new AnyValue[this.pathLength()];
        if ( this.projectBackwards )
        {
            for ( int i = this.pathLength() - 1; i >= 0; --i )
            {
                RelationshipSelectionCursor cursor = (RelationshipSelectionCursor) this.selectionCursors().get( i );
                r[this.pathLength() - 1 - i] = VarExpandCursor$.MODULE$.relationshipFromCursor( this.dbAccess, cursor );
            }
        }
        else
        {
            for ( int i = 0; i < this.pathLength(); ++i )
            {
                RelationshipSelectionCursor cursor = (RelationshipSelectionCursor) this.selectionCursors().get( i );
                r[i] = VarExpandCursor$.MODULE$.relationshipFromCursor( this.dbAccess, cursor );
            }
        }

        return VirtualValues.list( r );
    }

    public void setTracer( final OperatorProfileEvent event )
    {
        this.event_$eq( event );
        this.nodeCursor.setTracer( event );
        this.relTraCursors().foreach( ( x$1 ) -> {
            $anonfun$setTracer$1( event, x$1 );
            return BoxedUnit.UNIT;
        } );
        this.relGroupCursors().foreach( ( x$2 ) -> {
            $anonfun$setTracer$2( event, x$2 );
            return BoxedUnit.UNIT;
        } );
    }

    public void free( final CursorPools cursorPools )
    {
        cursorPools.nodeCursorPool().free( this.nodeCursor );
        this.relTraCursors().foreach( ( cursor ) -> {
            $anonfun$free$1( cursorPools, cursor );
            return BoxedUnit.UNIT;
        } );
        this.relGroupCursors().foreach( ( cursor ) -> {
            $anonfun$free$2( cursorPools, cursor );
            return BoxedUnit.UNIT;
        } );
    }
}
