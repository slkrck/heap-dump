package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ContinuableOperatorTask extends OperatorTask, WithHeapUsageEstimation
{
    static void $init$( final ContinuableOperatorTask $this )
    {
    }

    boolean canContinue();

    default void close( final OperatorCloser operatorCloser, final QueryResources resources )
    {
        this.closeCursors( resources );
        this.closeInput( operatorCloser );
    }

    void closeInput( final OperatorCloser operatorCloser );

    void closeCursors( final QueryResources resources );

    WorkUnitEvent producingWorkUnitEvent();

    boolean filterCancelledArguments( final OperatorCloser operatorCloser );
}
