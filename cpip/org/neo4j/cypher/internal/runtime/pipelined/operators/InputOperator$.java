package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.VirtualNodeValue;
import org.neo4j.values.virtual.VirtualRelationshipValue;

public final class InputOperator$
{
    public static InputOperator$ MODULE$;

    static
    {
        new InputOperator$();
    }

    private InputOperator$()
    {
        MODULE$ = this;
    }

    public long nodeOrNoValue( final AnyValue value )
    {
        return value == Values.NO_VALUE ? NullChecker$.MODULE$.NULL_ENTITY() : ((VirtualNodeValue) value).id();
    }

    public long relationshipOrNoValue( final AnyValue value )
    {
        return value == Values.NO_VALUE ? NullChecker$.MODULE$.NULL_ENTITY() : ((VirtualRelationshipValue) value).id();
    }
}
