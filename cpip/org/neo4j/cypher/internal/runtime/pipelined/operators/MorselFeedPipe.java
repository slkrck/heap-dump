package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.Predef..less.colon.less;
import scala.collection.BufferedIterator;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Traversable;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Vector;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassTag;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing.;

@JavaDocToJava
public class MorselFeedPipe implements Pipe, Product, Serializable
{
    private final int id;
    private ExecutionContextFactory executionContextFactory;

    public MorselFeedPipe( final int id )
    {
        this.id = id;
        Pipe.$init$( this );
        Product.$init$( this );
    }

    public static int $lessinit$greater$default$1()
    {
        return MorselFeedPipe$.MODULE$.$lessinit$greater$default$1();
    }

    public static int apply$default$1()
    {
        return MorselFeedPipe$.MODULE$.apply$default$1();
    }

    public static boolean unapply( final MorselFeedPipe x$0 )
    {
        return MorselFeedPipe$.MODULE$.unapply( var0 );
    }

    public static MorselFeedPipe apply( final int id )
    {
        return MorselFeedPipe$.MODULE$.apply( var0 );
    }

    public Iterator<ExecutionContext> createResults( final QueryState state )
    {
        return Pipe.createResults$( this, state );
    }

    public ExecutionContextFactory executionContextFactory()
    {
        return this.executionContextFactory;
    }

    public void executionContextFactory_$eq( final ExecutionContextFactory x$1 )
    {
        this.executionContextFactory = x$1;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final QueryState state )
    {
        FeedPipeQueryState feedPipeQueryState = (FeedPipeQueryState) state;
        MorselExecutionContext morsel = feedPipeQueryState.morsel();
        morsel.resetToBeforeFirstRow();
        return new Iterator<ExecutionContext>( (MorselFeedPipe) null, morsel )
        {
            private final MorselExecutionContext morsel$1;

            public
            {
                this.morsel$1 = morsel$1;
                GenTraversableOnce.$init$( this );
                TraversableOnce.$init$( this );
                Iterator.$init$( this );
            }

            public Iterator<ExecutionContext> seq()
            {
                return Iterator.seq$( this );
            }

            public boolean isEmpty()
            {
                return Iterator.isEmpty$( this );
            }

            public boolean isTraversableAgain()
            {
                return Iterator.isTraversableAgain$( this );
            }

            public boolean hasDefiniteSize()
            {
                return Iterator.hasDefiniteSize$( this );
            }

            public Iterator<ExecutionContext> take( final int n )
            {
                return Iterator.take$( this, n );
            }

            public Iterator<ExecutionContext> drop( final int n )
            {
                return Iterator.drop$( this, n );
            }

            public Iterator<ExecutionContext> slice( final int from, final int until )
            {
                return Iterator.slice$( this, from, until );
            }

            public Iterator<ExecutionContext> sliceIterator( final int from, final int until )
            {
                return Iterator.sliceIterator$( this, from, until );
            }

            public <B> Iterator<B> map( final Function1<ExecutionContext,B> f )
            {
                return Iterator.map$( this, f );
            }

            public <B> Iterator<B> $plus$plus( final Function0<GenTraversableOnce<B>> that )
            {
                return Iterator.$plus$plus$( this, that );
            }

            public <B> Iterator<B> flatMap( final Function1<ExecutionContext,GenTraversableOnce<B>> f )
            {
                return Iterator.flatMap$( this, f );
            }

            public Iterator<ExecutionContext> filter( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.filter$( this, p );
            }

            public <B> boolean corresponds( final GenTraversableOnce<B> that, final Function2<ExecutionContext,B,Object> p )
            {
                return Iterator.corresponds$( this, that, p );
            }

            public Iterator<ExecutionContext> withFilter( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.withFilter$( this, p );
            }

            public Iterator<ExecutionContext> filterNot( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.filterNot$( this, p );
            }

            public <B> Iterator<B> collect( final PartialFunction<ExecutionContext,B> pf )
            {
                return Iterator.collect$( this, pf );
            }

            public <B> Iterator<B> scanLeft( final B z, final Function2<B,ExecutionContext,B> op )
            {
                return Iterator.scanLeft$( this, z, op );
            }

            public <B> Iterator<B> scanRight( final B z, final Function2<ExecutionContext,B,B> op )
            {
                return Iterator.scanRight$( this, z, op );
            }

            public Iterator<ExecutionContext> takeWhile( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.takeWhile$( this, p );
            }

            public Tuple2<Iterator<ExecutionContext>,Iterator<ExecutionContext>> partition( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.partition$( this, p );
            }

            public Tuple2<Iterator<ExecutionContext>,Iterator<ExecutionContext>> span( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.span$( this, p );
            }

            public Iterator<ExecutionContext> dropWhile( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.dropWhile$( this, p );
            }

            public <B> Iterator<Tuple2<ExecutionContext,B>> zip( final Iterator<B> that )
            {
                return Iterator.zip$( this, that );
            }

            public <A1> Iterator<A1> padTo( final int len, final A1 elem )
            {
                return Iterator.padTo$( this, len, elem );
            }

            public Iterator<Tuple2<ExecutionContext,Object>> zipWithIndex()
            {
                return Iterator.zipWithIndex$( this );
            }

            public <B, A1, B1> Iterator<Tuple2<A1,B1>> zipAll( final Iterator<B> that, final A1 thisElem, final B1 thatElem )
            {
                return Iterator.zipAll$( this, that, thisElem, thatElem );
            }

            public <U> void foreach( final Function1<ExecutionContext,U> f )
            {
                Iterator.foreach$( this, f );
            }

            public boolean forall( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.forall$( this, p );
            }

            public boolean exists( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.exists$( this, p );
            }

            public boolean contains( final Object elem )
            {
                return Iterator.contains$( this, elem );
            }

            public Option<ExecutionContext> find( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.find$( this, p );
            }

            public int indexWhere( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.indexWhere$( this, p );
            }

            public int indexWhere( final Function1<ExecutionContext,Object> p, final int from )
            {
                return Iterator.indexWhere$( this, p, from );
            }

            public <B> int indexOf( final B elem )
            {
                return Iterator.indexOf$( this, elem );
            }

            public <B> int indexOf( final B elem, final int from )
            {
                return Iterator.indexOf$( this, elem, from );
            }

            public BufferedIterator<ExecutionContext> buffered()
            {
                return Iterator.buffered$( this );
            }

            public <B> Iterator<ExecutionContext>.GroupedIterator<B> grouped( final int size )
            {
                return Iterator.grouped$( this, size );
            }

            public <B> Iterator<ExecutionContext>.GroupedIterator<B> sliding( final int size, final int step )
            {
                return Iterator.sliding$( this, size, step );
            }

            public int length()
            {
                return Iterator.length$( this );
            }

            public Tuple2<Iterator<ExecutionContext>,Iterator<ExecutionContext>> duplicate()
            {
                return Iterator.duplicate$( this );
            }

            public <B> Iterator<B> patch( final int from, final Iterator<B> patchElems, final int replaced )
            {
                return Iterator.patch$( this, from, patchElems, replaced );
            }

            public <B> void copyToArray( final Object xs, final int start, final int len )
            {
                Iterator.copyToArray$( this, xs, start, len );
            }

            public boolean sameElements( final Iterator<?> that )
            {
                return Iterator.sameElements$( this, that );
            }

            public Traversable<ExecutionContext> toTraversable()
            {
                return Iterator.toTraversable$( this );
            }

            public Iterator<ExecutionContext> toIterator()
            {
                return Iterator.toIterator$( this );
            }

            public Stream<ExecutionContext> toStream()
            {
                return Iterator.toStream$( this );
            }

            public String toString()
            {
                return Iterator.toString$( this );
            }

            public <B> int sliding$default$2()
            {
                return Iterator.sliding$default$2$( this );
            }

            public List<ExecutionContext> reversed()
            {
                return TraversableOnce.reversed$( this );
            }

            public int size()
            {
                return TraversableOnce.size$( this );
            }

            public boolean nonEmpty()
            {
                return TraversableOnce.nonEmpty$( this );
            }

            public int count( final Function1<ExecutionContext,Object> p )
            {
                return TraversableOnce.count$( this, p );
            }

            public <B> Option<B> collectFirst( final PartialFunction<ExecutionContext,B> pf )
            {
                return TraversableOnce.collectFirst$( this, pf );
            }

            public <B> B $div$colon( final B z, final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.$div$colon$( this, z, op );
            }

            public <B> B $colon$bslash( final B z, final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.$colon$bslash$( this, z, op );
            }

            public <B> B foldLeft( final B z, final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.foldLeft$( this, z, op );
            }

            public <B> B foldRight( final B z, final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.foldRight$( this, z, op );
            }

            public <B> B reduceLeft( final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.reduceLeft$( this, op );
            }

            public <B> B reduceRight( final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.reduceRight$( this, op );
            }

            public <B> Option<B> reduceLeftOption( final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.reduceLeftOption$( this, op );
            }

            public <B> Option<B> reduceRightOption( final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.reduceRightOption$( this, op );
            }

            public <A1> A1 reduce( final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.reduce$( this, op );
            }

            public <A1> Option<A1> reduceOption( final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.reduceOption$( this, op );
            }

            public <A1> A1 fold( final A1 z, final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.fold$( this, z, op );
            }

            public <B> B aggregate( final Function0<B> z, final Function2<B,ExecutionContext,B> seqop, final Function2<B,B,B> combop )
            {
                return TraversableOnce.aggregate$( this, z, seqop, combop );
            }

            public <B> B sum( final Numeric<B> num )
            {
                return TraversableOnce.sum$( this, num );
            }

            public <B> B product( final Numeric<B> num )
            {
                return TraversableOnce.product$( this, num );
            }

            public Object min( final Ordering cmp )
            {
                return TraversableOnce.min$( this, cmp );
            }

            public Object max( final Ordering cmp )
            {
                return TraversableOnce.max$( this, cmp );
            }

            public Object maxBy( final Function1 f, final Ordering cmp )
            {
                return TraversableOnce.maxBy$( this, f, cmp );
            }

            public Object minBy( final Function1 f, final Ordering cmp )
            {
                return TraversableOnce.minBy$( this, f, cmp );
            }

            public <B> void copyToBuffer( final Buffer<B> dest )
            {
                TraversableOnce.copyToBuffer$( this, dest );
            }

            public <B> void copyToArray( final Object xs, final int start )
            {
                TraversableOnce.copyToArray$( this, xs, start );
            }

            public <B> void copyToArray( final Object xs )
            {
                TraversableOnce.copyToArray$( this, xs );
            }

            public <B> Object toArray( final ClassTag<B> evidence$1 )
            {
                return TraversableOnce.toArray$( this, evidence$1 );
            }

            public List<ExecutionContext> toList()
            {
                return TraversableOnce.toList$( this );
            }

            public Iterable<ExecutionContext> toIterable()
            {
                return TraversableOnce.toIterable$( this );
            }

            public Seq<ExecutionContext> toSeq()
            {
                return TraversableOnce.toSeq$( this );
            }

            public IndexedSeq<ExecutionContext> toIndexedSeq()
            {
                return TraversableOnce.toIndexedSeq$( this );
            }

            public <B> Buffer<B> toBuffer()
            {
                return TraversableOnce.toBuffer$( this );
            }

            public <B> Set<B> toSet()
            {
                return TraversableOnce.toSet$( this );
            }

            public Vector<ExecutionContext> toVector()
            {
                return TraversableOnce.toVector$( this );
            }

            public <Col> Col to( final CanBuildFrom<,ExecutionContext,Col> cbf )
            {
                return TraversableOnce.to$( this, cbf );
            }

            public <T, U> Map<T,U> toMap( final less<ExecutionContext,Tuple2<T,U>> ev )
            {
                return TraversableOnce.toMap$( this, ev );
            }

            public String mkString( final String start, final String sep, final String end )
            {
                return TraversableOnce.mkString$( this, start, sep, end );
            }

            public String mkString( final String sep )
            {
                return TraversableOnce.mkString$( this, sep );
            }

            public String mkString()
            {
                return TraversableOnce.mkString$( this );
            }

            public StringBuilder addString( final StringBuilder b, final String start, final String sep, final String end )
            {
                return TraversableOnce.addString$( this, b, start, sep, end );
            }

            public StringBuilder addString( final StringBuilder b, final String sep )
            {
                return TraversableOnce.addString$( this, b, sep );
            }

            public StringBuilder addString( final StringBuilder b )
            {
                return TraversableOnce.addString$( this, b );
            }

            public int sizeHintIfCheap()
            {
                return GenTraversableOnce.sizeHintIfCheap$( this );
            }

            public boolean hasNext()
            {
                return this.morsel$1.hasNextRow();
            }

            public ExecutionContext next()
            {
                this.morsel$1.moveToNextRow();
                return this.morsel$1;
            }
        };
    }

    public MorselFeedPipe copy( final int id )
    {
        return new MorselFeedPipe( id );
    }

    public String productPrefix()
    {
        return "MorselFeedPipe";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselFeedPipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var2;
        if ( x$1 instanceof MorselFeedPipe )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2 && ((MorselFeedPipe) x$1).canEqual( this );
    }
}
