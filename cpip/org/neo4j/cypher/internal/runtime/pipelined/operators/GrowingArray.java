package org.neo4j.cypher.internal.runtime.pipelined.operators;

import scala.Function0;
import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class GrowingArray<T>
{
    private Object[] array = new Object[4];
    private int highWaterMark = 0;

    private Object[] array()
    {
        return this.array;
    }

    private void array_$eq( final Object[] x$1 )
    {
        this.array = x$1;
    }

    private int highWaterMark()
    {
        return this.highWaterMark;
    }

    private void highWaterMark_$eq( final int x$1 )
    {
        this.highWaterMark = x$1;
    }

    public void set( final int index, final T t )
    {
        this.ensureCapacity( index + 1 );
        this.array()[index] = t;
    }

    public T get( final int index )
    {
        return this.array()[index];
    }

    public T computeIfAbsent( final int index, final Function0<T> compute )
    {
        this.ensureCapacity( index + 1 );
        Object t = this.array()[index];
        if ( t == null )
        {
            t = compute.apply();
            this.array()[index] = t;
        }

        return t;
    }

    public void foreach( final Function1<T,BoxedUnit> f )
    {
        for ( int i = 0; i < this.highWaterMark(); ++i )
        {
            f.apply( this.get( i ) );
        }
    }

    public boolean hasNeverSeenData()
    {
        return this.highWaterMark() == 0;
    }

    private void ensureCapacity( final int size )
    {
        if ( this.highWaterMark() < size )
        {
            this.highWaterMark_$eq( size );
        }

        if ( this.array().length < size )
        {
            Object[] temp = this.array();
            this.array_$eq( new Object[this.array().length * 2] );
            System.arraycopy( temp, 0, this.array(), 0, temp.length );
        }
    }
}
