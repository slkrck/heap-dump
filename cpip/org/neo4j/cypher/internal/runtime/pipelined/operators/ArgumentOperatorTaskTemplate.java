package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.Function0;
import scala.Function1;
import scala.Option;
import scala.Some;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ArgumentOperatorTaskTemplate implements ContinuableOperatorTaskWithMorselTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final DelegateOperatorTaskTemplate innermost;
    private final SlotConfiguration.Size argumentSize;
    private final boolean isHead;
    private final OperatorExpressionCompiler codeGen;

    public ArgumentOperatorTaskTemplate( final OperatorTaskTemplate inner, final int id, final DelegateOperatorTaskTemplate innermost,
            final SlotConfiguration.Size argumentSize, final boolean isHead, final OperatorExpressionCompiler codeGen )
    {
        this.inner = inner;
        this.id = id;
        this.innermost = innermost;
        this.argumentSize = argumentSize;
        this.isHead = isHead;
        this.codeGen = codeGen;
        OperatorTaskTemplate.$init$( this );
        ContinuableOperatorTaskWithMorselTemplate.$init$( this );
    }

    public static boolean $lessinit$greater$default$5()
    {
        return ArgumentOperatorTaskTemplate$.MODULE$.$lessinit$greater$default$5();
    }

    public final IntermediateRepresentation genOperate()
    {
        return ContinuableOperatorTaskWithMorselTemplate.genOperate$( this );
    }

    public IntermediateRepresentation genScopeWithLocalDeclarations( final String scopeId, final Function0<IntermediateRepresentation> genBody )
    {
        return ContinuableOperatorTaskWithMorselTemplate.genScopeWithLocalDeclarations$( this, scopeId, genBody );
    }

    public IntermediateRepresentation genAdvanceOnCancelledRow()
    {
        return ContinuableOperatorTaskWithMorselTemplate.genAdvanceOnCancelledRow$( this );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return ContinuableOperatorTaskWithMorselTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return OperatorTaskTemplate.genOperateEnter$( this );
    }

    public IntermediateRepresentation genOperateExit()
    {
        return OperatorTaskTemplate.genOperateExit$( this );
    }

    public IntermediateRepresentation genProduce()
    {
        return OperatorTaskTemplate.genProduce$( this );
    }

    public IntermediateRepresentation genCreateState()
    {
        return OperatorTaskTemplate.genCreateState$( this );
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return OperatorTaskTemplate.genOutputBuffer$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public final boolean isHead()
    {
        return this.isHead;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    public String scopeId()
    {
        return (new StringBuilder( 8 )).append( "argument" ).append( this.id() ).toString();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return this.inner().genSetExecutionEvent( event );
    }

    public IntermediateRepresentation genInit()
    {
        return this.inner().genInit();
    }

    public IntermediateRepresentation genOperateHead()
    {
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.genAdvanceOnCancelledRow(),.MODULE$.labeledLoop(
                    OperatorCodeGenHelperTemplates$.MODULE$.OUTER_LOOP_LABEL_NAME(),.MODULE$.and(.MODULE$.invoke(.MODULE$.self(),.MODULE$.method( "canContinue",
            scala.reflect.ManifestFactory..MODULE$.classType( ContinuableOperatorTask.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),this.innermost.predicate()), .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.codeGen().copyFromInput( this.argumentSize.nLongs(), this.argumentSize.nReferences() ),
                    this.inner().genOperateWithExpressions(), this.doIfInnerCantContinue(.MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_MOVE_TO_NEXT(),
                            OperatorCodeGenHelperTemplates$.MODULE$.profileRow( this.id() )}) )) ), this.innermost.resetCachedPropertyVariables()}))))})));
    }

    public IntermediateRepresentation genOperateMiddle()
    {
        throw new CantCompileQueryException( "Cannot compile Input as middle operator" );
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Seq<Field> genFields()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue().map( ( x$1 ) -> {
            return .MODULE$.or( x$1, OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_IS_VALID() );
        } ).orElse( () -> {
            return new Some( OperatorCodeGenHelperTemplates$.MODULE$.INPUT_ROW_IS_VALID() );
        } );
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return this.inner().genCloseCursors();
    }

    public IntermediateRepresentation genClearStateOnCancelledRow()
    {
        return .MODULE$.noop();
    }
}
