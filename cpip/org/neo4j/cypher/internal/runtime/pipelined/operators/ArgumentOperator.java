package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ArgumentOperator implements StreamingOperator
{
    public final SlotConfiguration.Size org$neo4j$cypher$internal$runtime$pipelined$operators$ArgumentOperator$$argumentSize;
    private final WorkIdentity workIdentity;

    public ArgumentOperator( final WorkIdentity workIdentity, final SlotConfiguration.Size argumentSize )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ArgumentOperator$$argumentSize = argumentSize;
        StreamingOperator.$init$( this );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public String toString()
    {
        return "Argument";
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new ArgumentOperator.OTask[]{new ArgumentOperator.OTask( this, inputMorsel.nextCopy() )}) ));
    }

    public class OTask implements ContinuableOperatorTaskWithMorsel
    {
        private final MorselExecutionContext inputMorsel;

        public OTask( final ArgumentOperator $outer, final MorselExecutionContext inputMorsel )
        {
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                ContinuableOperatorTask.$init$( this );
                ContinuableOperatorTaskWithMorsel.$init$( this );
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorsel.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorsel.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorsel.producingWorkUnitEvent$( this );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithMorsel.estimatedHeapUsage$( this );
        }

        public void close( final OperatorCloser operatorCloser, final QueryResources resources )
        {
            ContinuableOperatorTask.close$( this, operatorCloser, resources );
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$ArgumentOperator$OTask$$$outer().workIdentity();
        }

        public String toString()
        {
            return "ArgumentTask";
        }

        public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            while ( outputRow.isValidRow() && this.inputMorsel().isValidRow() )
            {
                outputRow.copyFrom( this.inputMorsel(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ArgumentOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$ArgumentOperator$$argumentSize.nLongs(),
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$ArgumentOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$ArgumentOperator$$argumentSize.nReferences() );
                this.inputMorsel().moveToNextRow();
                outputRow.moveToNextRow();
            }

            outputRow.finishedWriting();
        }

        public boolean canContinue()
        {
            return false;
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
        }

        public void closeCursors( final QueryResources resources )
        {
        }
    }
}
