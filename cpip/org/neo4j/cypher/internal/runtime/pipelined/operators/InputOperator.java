package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.values.AnyValue;
import scala.collection.IndexedSeq;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class InputOperator implements StreamingOperator
{
    public final int[] org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$nodeOffsets;
    public final int[] org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$relationshipOffsets;
    public final int[] org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$refOffsets;
    private final WorkIdentity workIdentity;

    public InputOperator( final WorkIdentity workIdentity, final int[] nodeOffsets, final int[] relationshipOffsets, final int[] refOffsets )
    {
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$nodeOffsets = nodeOffsets;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$relationshipOffsets = relationshipOffsets;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$refOffsets = refOffsets;
        StreamingOperator.$init$( this );
    }

    public static long relationshipOrNoValue( final AnyValue value )
    {
        return InputOperator$.MODULE$.relationshipOrNoValue( var0 );
    }

    public static long nodeOrNoValue( final AnyValue value )
    {
        return InputOperator$.MODULE$.nodeOrNoValue( var0 );
    }

    public final IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
            final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return StreamingOperator.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return StreamingOperator.createState$( this, argumentStateCreator, stateFactory, queryContext, state, resources );
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public IndexedSeq<ContinuableOperatorTaskWithMorsel> nextTasks( final QueryContext queryContext, final QueryState state,
            final MorselParallelizer inputMorsel, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        return parallelism == 1 ? (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InputOperator.InputTask[]{
                new InputOperator.InputTask( this, new MutatingInputCursor( state.input() ), inputMorsel.nextCopy() )}) )) :
        (IndexedSeq) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) (new InputOperator.InputTask[parallelism]) ))).map( ( x$1 ) -> {
        return this.new InputTask( this, new MutatingInputCursor( state.input() ), inputMorsel.nextCopy() );
    }, scala.Array..MODULE$.fallbackCanBuildFrom( scala.Predef.DummyImplicit..MODULE$.dummyImplicit()));
    }

    public class InputTask implements ContinuableOperatorTaskWithMorsel
    {
        private final MutatingInputCursor input;
        private final MorselExecutionContext inputMorsel;

        public InputTask( final InputOperator $outer, final MutatingInputCursor input, final MorselExecutionContext inputMorsel )
        {
            this.input = input;
            this.inputMorsel = inputMorsel;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                ContinuableOperatorTask.$init$( this );
                ContinuableOperatorTaskWithMorsel.$init$( this );
            }
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorsel.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorsel.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorsel.producingWorkUnitEvent$( this );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithMorsel.estimatedHeapUsage$( this );
        }

        public void close( final OperatorCloser operatorCloser, final QueryResources resources )
        {
            ContinuableOperatorTask.close$( this, operatorCloser, resources );
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().workIdentity();
        }

        public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState queryState, final QueryResources resources )
        {
            while ( outputRow.isValidRow() && this.input.nextInput() )
            {
                int i;
                for ( i = 0; i <
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$nodeOffsets.length;
                        ++i )
                {
                    outputRow.setLongAt(
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$nodeOffsets[i],
                            InputOperator$.MODULE$.nodeOrNoValue( this.input.value( i ) ) );
                }

                for ( i = 0; i <
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$relationshipOffsets.length;
                        ++i )
                {
                    outputRow.setLongAt(
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$relationshipOffsets[i],
                            InputOperator$.MODULE$.relationshipOrNoValue( this.input.value(
                                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$nodeOffsets.length +
                                            i ) ) );
                }

                for ( i = 0; i <
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$refOffsets.length;
                        ++i )
                {
                    outputRow.setRefAt(
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$refOffsets[i],
                            this.input.value(
                                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$nodeOffsets.length +
                                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$InputTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$InputOperator$$relationshipOffsets.length +
                                            i ) );
                }

                outputRow.moveToNextRow();
            }

            outputRow.finishedWriting();
        }

        public boolean canContinue()
        {
            return this.input.canContinue();
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
        }

        public void closeCursors( final QueryResources resources )
        {
            this.input.close();
        }
    }
}
