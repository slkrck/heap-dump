package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;

public class AggregationOperator$AggregatingAccumulator$Factory implements ArgumentStateMap.ArgumentStateFactory<AggregationOperator.AggregatingAccumulator>
{
    private final Aggregator[] aggregators;
    private final QueryMemoryTracker memoryTracker;

    public AggregationOperator$AggregatingAccumulator$Factory( final AggregationOperator.AggregatingAccumulator$ $outer, final Aggregator[] aggregators,
            final QueryMemoryTracker memoryTracker )
    {
        this.aggregators = aggregators;
        this.memoryTracker = memoryTracker;
        if ( $outer == null )
        {
            throw null;
        }
        else
        {
            this.$outer = $outer;
            super();
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }
    }

    public boolean completeOnConstruction()
    {
        return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
    }

    public AggregationOperator.AggregatingAccumulator newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
            final long[] argumentRowIdsForReducers )
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregatingAccumulator$Factory$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregatingAccumulator$$$outer().new StandardAggregatingAccumulator(
                argumentRowId, this.aggregators, argumentRowIdsForReducers, this.memoryTracker );
    }

    public AggregationOperator.AggregatingAccumulator newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
            final long[] argumentRowIdsForReducers )
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregatingAccumulator$Factory$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperator$AggregatingAccumulator$$$outer().new ConcurrentAggregatingAccumulator(
                argumentRowId, this.aggregators, argumentRowIdsForReducers );
    }
}
