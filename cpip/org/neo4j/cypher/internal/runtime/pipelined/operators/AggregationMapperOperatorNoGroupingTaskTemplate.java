package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import scala.Function0;
import scala.Function1;
import scala.Option;
import scala.Some;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class AggregationMapperOperatorNoGroupingTaskTemplate implements OperatorTaskTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final int argumentSlotOffset;
    private final Aggregator[] aggregators;
    private final int outputBufferId;
    private final Function0<IntermediateExpression[]> aggregationExpressionsCreator;
    private final OperatorExpressionCompiler codeGen;
    private final Field perArgsField;
    private final Field sinkField;
    private final Field bufferIdField;
    private final LocalVariable aggregatorsVar;
    private final LocalVariable argVar;
    private final LocalVariable updatersVar;
    private IntermediateExpression[] compiledAggregationExpressions;

    public AggregationMapperOperatorNoGroupingTaskTemplate( final OperatorTaskTemplate inner, final int id, final int argumentSlotOffset,
            final Aggregator[] aggregators, final int outputBufferId, final Function0<IntermediateExpression[]> aggregationExpressionsCreator,
            final Expression[] aggregationExpressions, final OperatorExpressionCompiler codeGen )
    {
        this.inner = inner;
        this.id = id;
        this.argumentSlotOffset = argumentSlotOffset;
        this.aggregators = aggregators;
        this.outputBufferId = outputBufferId;
        this.aggregationExpressionsCreator = aggregationExpressionsCreator;
        this.codeGen = codeGen;
        OperatorTaskTemplate.$init$( this );
        this.perArgsField = .MODULE$.field( codeGen.namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( ArrayBuffer.class,
            scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.PerArgument.class, scala.reflect.ManifestFactory..MODULE$.arrayType(
            scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )));
        this.sinkField = .MODULE$.field( codeGen.namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( Sink.class,
            scala.reflect.ManifestFactory..MODULE$.classType( IndexedSeq.class, scala.reflect.ManifestFactory..MODULE$.classType(
            ArgumentStateMap.PerArgument.class, scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )));
        this.bufferIdField = .MODULE$.field( codeGen.namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.Int());
        this.aggregatorsVar = .
        MODULE$.variable( codeGen.namer().nextVariableName(), AggregationMapperOperatorTaskTemplate$.MODULE$.createAggregators( aggregators ),
                scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( Aggregator.class )));
        this.argVar = .MODULE$.variable( codeGen.namer().nextVariableName(),.MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ), scala.reflect.ManifestFactory..
        MODULE$.Long());
        this.updatersVar = .MODULE$.variable( codeGen.namer().nextVariableName(),.MODULE$.constant( (Object) null ), scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Any()));
    }

    public String scopeId()
    {
        return OperatorTaskTemplate.scopeId$( this );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return OperatorTaskTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return OperatorTaskTemplate.genOperateEnter$( this );
    }

    public IntermediateRepresentation genOperateExit()
    {
        return OperatorTaskTemplate.genOperateExit$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    public String toString()
    {
        return "AggregationMapperNoGroupingOperatorTaskTemplate";
    }

    private Field perArgsField()
    {
        return this.perArgsField;
    }

    private Field sinkField()
    {
        return this.sinkField;
    }

    private Field bufferIdField()
    {
        return this.bufferIdField;
    }

    private LocalVariable aggregatorsVar()
    {
        return this.aggregatorsVar;
    }

    private LocalVariable argVar()
    {
        return this.argVar;
    }

    private LocalVariable updatersVar()
    {
        return this.updatersVar;
    }

    private IntermediateExpression[] compiledAggregationExpressions()
    {
        return this.compiledAggregationExpressions;
    }

    private void compiledAggregationExpressions_$eq( final IntermediateExpression[] x$1 )
    {
        this.compiledAggregationExpressions = x$1;
    }

    public IntermediateRepresentation genInit()
    {
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.setField( this.perArgsField(),.MODULE$.newInstance(.MODULE$.constructor(
                    scala.reflect.ManifestFactory..MODULE$.classType( ArrayBuffer.class, scala.reflect.ManifestFactory..MODULE$.classType(
                    ArgumentStateMap.PerArgument.class, scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Any()),
            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))), .
        MODULE$.setField( this.bufferIdField(),.MODULE$.constant( BoxesRunTime.boxToInteger( this.outputBufferId ) )),this.inner().genInit()})));
    }

    public IntermediateRepresentation genOperate()
    {
        if ( this.compiledAggregationExpressions() == null )
        {
            this.compiledAggregationExpressions_$eq( (IntermediateExpression[]) this.aggregationExpressionsCreator.apply() );
        }

        String currentArg = this.codeGen().namer().nextVariableName();
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.declareAndAssign(.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()),
            currentArg, this.codeGen().getArgumentAt( this.argumentSlotOffset ) ), .MODULE$.condition(.MODULE$.notEqual(.MODULE$.load( currentArg ), .
        MODULE$.load( this.argVar() )), .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.assign( this.argVar(),.MODULE$.load( currentArg )),.MODULE$.assign( this.updatersVar(),
            AggregationMapperOperatorTaskTemplate$.MODULE$.createUpdaters( this.aggregators,.MODULE$.load( this.aggregatorsVar() ) )), .
        MODULE$.invokeSideEffect(.MODULE$.loadField( this.perArgsField() ), .
        MODULE$.method( "$plus$eq", scala.reflect.ManifestFactory..MODULE$.classType( ArrayBuffer.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..
        MODULE$.classType( ArrayBuffer.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.newInstance(.MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType(
                    ArgumentStateMap.PerArgument.class, scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Any()),
            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
        MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.load( this.argVar() ),.MODULE$.load( this.updatersVar() )})))})))})))), .
        MODULE$.block( (Seq) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.compiledAggregationExpressions() )) ).indices().map( ( i ) -> {
            return $anonfun$genOperate$1( this, BoxesRunTime.unboxToInt( i ) );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())),this.inner().genOperateWithExpressions()})));
    }

    public IntermediateRepresentation genCreateState()
    {
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.setField( this.sinkField(),.MODULE$.invoke(
                    OperatorCodeGenHelperTemplates$.MODULE$.EXECUTION_STATE(),.MODULE$.method( "getSinkInt", scala.reflect.ManifestFactory..MODULE$.classType(
                    ExecutionState.class ), scala.reflect.ManifestFactory..MODULE$.classType( Sink.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Int()),
        scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{OperatorCodeGenHelperTemplates$.MODULE$.PIPELINE_ID(),.MODULE$.loadField( this.bufferIdField() )}))))})))
        ;
    }

    public IntermediateRepresentation genProduce()
    {
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.invokeSideEffect(.MODULE$.loadField( this.sinkField() ),.MODULE$.method( "put",
            scala.reflect.ManifestFactory..MODULE$.classType( Sink.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.Any()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.loadField( this.perArgsField() )}))), .
        MODULE$.setField( this.perArgsField(),.MODULE$.newInstance(.MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType( ArrayBuffer.class,
                scala.reflect.ManifestFactory..MODULE$.classType( ArgumentStateMap.PerArgument.class, scala.reflect.ManifestFactory..MODULE$.arrayType(
                scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )))})));
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return new Some(.MODULE$.loadField( this.bufferIdField() ));
    }

    public Seq<Field> genFields()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Field[]{this.perArgsField(), this.sinkField(), this.bufferIdField()}) ));
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{this.argVar(), this.aggregatorsVar(), this.updatersVar()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return scala.Predef..MODULE$.wrapRefArray( (Object[]) this.compiledAggregationExpressions() );
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue();
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return this.inner().genCloseCursors();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return this.inner().genSetExecutionEvent( event );
    }
}
