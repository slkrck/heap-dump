package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.schema.IndexOrder;
import org.neo4j.values.storable.TextValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class NodeIndexEndsWithScanOperator extends NodeIndexStringSearchScanOperator
{
    public NodeIndexEndsWithScanOperator( final WorkIdentity workIdentity, final int nodeOffset, final SlottedIndexedProperty property, final int queryIndexId,
            final IndexOrder indexOrder, final Expression valueExpr, final SlotConfiguration.Size argumentSize )
    {
        super( workIdentity, nodeOffset, property, queryIndexId, indexOrder, valueExpr, argumentSize );
    }

    public IndexQuery computeIndexQuery( final int property, final TextValue value )
    {
        return IndexQuery.stringSuffix( property, value );
    }
}
