package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.Iterator;

import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class IteratorCursor
{
    private final Iterator<AnyValue> iterator;
    private AnyValue _value;

    public IteratorCursor( final Iterator<AnyValue> iterator )
    {
        this.iterator = iterator;
    }

    public static IteratorCursor apply( final Iterator<AnyValue> iterator )
    {
        return IteratorCursor$.MODULE$.apply( var0 );
    }

    private AnyValue _value()
    {
        return this._value;
    }

    private void _value_$eq( final AnyValue x$1 )
    {
        this._value = x$1;
    }

    public boolean next()
    {
        boolean var10000;
        if ( this.iterator.hasNext() )
        {
            this._value_$eq( (AnyValue) this.iterator.next() );
            var10000 = true;
        }
        else
        {
            var10000 = false;
        }

        return var10000;
    }

    public AnyValue value()
    {
        return this._value();
    }
}
