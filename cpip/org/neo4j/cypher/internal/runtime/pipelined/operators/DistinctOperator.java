package org.neo4j.cypher.internal.runtime.pipelined.operators;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.values.AnyValue;
import scala.Array.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class DistinctOperator implements MiddleOperator
{
    public final GroupingExpression org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$$groupings;
    private final int argumentStateMapId;
    private final WorkIdentity workIdentity;

    public DistinctOperator( final int argumentStateMapId, final WorkIdentity workIdentity, final GroupingExpression groupings )
    {
        this.argumentStateMapId = argumentStateMapId;
        this.workIdentity = workIdentity;
        this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$$groupings = groupings;
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public OperatorTask createTask( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return new DistinctOperator.DistinctOperatorTask( this, argumentStateCreator.createArgumentStateMap( this.argumentStateMapId,
                new DistinctOperator.DistinctStateFactory( this, stateFactory.memoryTracker() ) ) );
    }

    public class DistinctOperatorTask implements OperatorTask
    {
        private final ArgumentStateMap<DistinctOperator.DistinctState> argumentStateMap;

        public DistinctOperatorTask( final DistinctOperator $outer, final ArgumentStateMap<DistinctOperator.DistinctState> argumentStateMap )
        {
            this.argumentStateMap = argumentStateMap;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
            }
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$DistinctOperatorTask$$$outer().workIdentity();
        }

        public void operate( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            SlottedQueryState queryState =
                    new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(), (IndexReadSession[]).
            MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
            resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
            MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
            ;
            this.argumentStateMap.filter( output, ( distinctState, x$1 ) -> {
                return $anonfun$operate$1( distinctState, BoxesRunTime.unboxToLong( x$1 ) );
            }, ( distinctState, morsel ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$operate$2( queryState, distinctState, morsel ) );
            } );
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
        }
    }

    public class DistinctState implements ArgumentStateMap.ArgumentState
    {
        private final long argumentRowId;
        private final Set<AnyValue> seen;
        private final long[] argumentRowIdsForReducers;
        private final QueryMemoryTracker memoryTracker;

        public DistinctState( final DistinctOperator $outer, final long argumentRowId, final Set<AnyValue> seen, final long[] argumentRowIdsForReducers,
                final QueryMemoryTracker memoryTracker )
        {
            this.argumentRowId = argumentRowId;
            this.seen = seen;
            this.argumentRowIdsForReducers = argumentRowIdsForReducers;
            this.memoryTracker = memoryTracker;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public long[] argumentRowIdsForReducers()
        {
            return this.argumentRowIdsForReducers;
        }

        public boolean filterOrProject( final MorselExecutionContext row, final SlottedQueryState queryState )
        {
            AnyValue groupingKey =
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$DistinctState$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$$groupings.computeGroupingKey(
                            row, queryState );
            boolean var10000;
            if ( this.seen.add( groupingKey ) )
            {
                this.memoryTracker.allocated( groupingKey );
                this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$DistinctState$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$$groupings.project(
                        row, groupingKey );
                var10000 = true;
            }
            else
            {
                var10000 = false;
            }

            return var10000;
        }

        public String toString()
        {
            return (new StringBuilder( 28 )).append( "DistinctState(" ).append( this.argumentRowId() ).append( ", concurrent=" ).append(
                    this.seen.getClass().getPackageName().contains( "concurrent" ) ).append( ")" ).toString();
        }
    }

    public class DistinctStateFactory implements ArgumentStateMap.ArgumentStateFactory<DistinctOperator.DistinctState>
    {
        private final QueryMemoryTracker memoryTracker;

        public DistinctStateFactory( final DistinctOperator $outer, final QueryMemoryTracker memoryTracker )
        {
            this.memoryTracker = memoryTracker;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                ArgumentStateMap.ArgumentStateFactory.$init$( this );
            }
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public DistinctOperator.DistinctState newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$DistinctStateFactory$$$outer().new DistinctState(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$DistinctStateFactory$$$outer(), argumentRowId, new HashSet(),
                    argumentRowIdsForReducers, this.memoryTracker );
        }

        public DistinctOperator.DistinctState newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$DistinctStateFactory$$$outer().new DistinctState(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$DistinctOperator$DistinctStateFactory$$$outer(), argumentRowId,
                    ConcurrentHashMap.newKeySet(), argumentRowIdsForReducers, this.memoryTracker );
        }
    }
}
