package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.PipelineId;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class MorselBufferPreparedOutput$ extends AbstractFunction4<BufferId,ExecutionState,PipelineId,MorselExecutionContext,MorselBufferPreparedOutput>
        implements Serializable
{
    public static MorselBufferPreparedOutput$ MODULE$;

    static
    {
        new MorselBufferPreparedOutput$();
    }

    private MorselBufferPreparedOutput$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselBufferPreparedOutput";
    }

    public MorselBufferPreparedOutput apply( final int bufferId, final ExecutionState executionState, final int pipelineId,
            final MorselExecutionContext outputMorsel )
    {
        return new MorselBufferPreparedOutput( bufferId, executionState, pipelineId, outputMorsel );
    }

    public Option<Tuple4<BufferId,ExecutionState,PipelineId,MorselExecutionContext>> unapply( final MorselBufferPreparedOutput x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple4( new BufferId( x$0.bufferId() ), x$0.executionState(), new PipelineId( x$0.pipelineId() ), x$0.outputMorsel() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
