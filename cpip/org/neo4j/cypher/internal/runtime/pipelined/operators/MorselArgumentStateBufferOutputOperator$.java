package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class MorselArgumentStateBufferOutputOperator$ extends AbstractFunction4<BufferId,Object,Id,Object,MorselArgumentStateBufferOutputOperator>
        implements Serializable
{
    public static MorselArgumentStateBufferOutputOperator$ MODULE$;

    static
    {
        new MorselArgumentStateBufferOutputOperator$();
    }

    private MorselArgumentStateBufferOutputOperator$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselArgumentStateBufferOutputOperator";
    }

    public MorselArgumentStateBufferOutputOperator apply( final int bufferId, final int argumentSlotOffset, final int nextPipelineHeadPlanId,
            final boolean nextPipelineFused )
    {
        return new MorselArgumentStateBufferOutputOperator( bufferId, argumentSlotOffset, nextPipelineHeadPlanId, nextPipelineFused );
    }

    public Option<Tuple4<BufferId,Object,Id,Object>> unapply( final MorselArgumentStateBufferOutputOperator x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple4( new BufferId( x$0.bufferId() ), BoxesRunTime.boxToInteger( x$0.argumentSlotOffset() ), new Id( x$0.nextPipelineHeadPlanId() ),
                    BoxesRunTime.boxToBoolean( x$0.nextPipelineFused() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
