package org.neo4j.cypher.internal.runtime.pipelined.operators;

public final class InputOperatorTemplate$
{
    public static InputOperatorTemplate$ MODULE$;

    static
    {
        new InputOperatorTemplate$();
    }

    private InputOperatorTemplate$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$8()
    {
        return true;
    }
}
