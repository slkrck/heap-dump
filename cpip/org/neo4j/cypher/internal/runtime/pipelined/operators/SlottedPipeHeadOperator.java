package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescription;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SlottedPipeHeadOperator extends SlottedPipeOperator implements Operator
{
    public SlottedPipeHeadOperator( final WorkIdentityMutableDescription workIdentity, final Pipe initialPipe )
    {
        super( workIdentity, initialPipe );
    }

    public String toString()
    {
        return super.workIdentity().toString();
    }

    public OperatorState createState( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return new OperatorState( this, stateFactory )
        {
            private final StateFactory stateFactory$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.stateFactory$1 = stateFactory$1;
                }
            }

            public IndexedSeq<ContinuableOperatorTask> nextTasks( final QueryContext context, final QueryState state, final OperatorInput operatorInput,
                    final int parallelism, final QueryResources resources, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
            {
                MorselParallelizer input = operatorInput.takeMorsel();
                IndexedSeq var10000;
                if ( input != null )
                {
                    MorselExecutionContext inputMorsel = input.nextCopy();
                    FeedPipeQueryState inputQueryState = SlottedPipeOperator$.MODULE$.createFeedPipeQueryState( inputMorsel, context, state, resources,
                            this.stateFactory$1.memoryTracker() );
                    var10000 = (IndexedSeq).MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new SlottedPipeHeadOperator.OTask[]{this.$outer.new OTask( this.$outer, inputMorsel, inputQueryState )}) ));
                }
                else
                {
                    var10000 = null;
                }

                return var10000;
            }
        };
    }

    public class OTask implements ContinuableOperatorTaskWithMorsel, OperatorWithInterpretedDBHitsProfiling
    {
        private final MorselExecutionContext inputMorsel;
        private final FeedPipeQueryState feedPipeQueryState;
        private Iterator<ExecutionContext> resultIterator;
        private OperatorProfileEvent profileEvent;

        public OTask( final SlottedPipeHeadOperator $outer, final MorselExecutionContext inputMorsel, final FeedPipeQueryState feedPipeQueryState )
        {
            this.inputMorsel = inputMorsel;
            this.feedPipeQueryState = feedPipeQueryState;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                OperatorTask.$init$( this );
                ContinuableOperatorTask.$init$( this );
                ContinuableOperatorTaskWithMorsel.$init$( this );
                OperatorWithInterpretedDBHitsProfiling.$init$( this );
            }
        }

        public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
                final QueryProfiler queryProfiler )
        {
            OperatorWithInterpretedDBHitsProfiling.operateWithProfile$( this, output, context, state, resources, queryProfiler );
        }

        public void closeInput( final OperatorCloser operatorCloser )
        {
            ContinuableOperatorTaskWithMorsel.closeInput$( this, operatorCloser );
        }

        public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
        {
            return ContinuableOperatorTaskWithMorsel.filterCancelledArguments$( this, operatorCloser );
        }

        public WorkUnitEvent producingWorkUnitEvent()
        {
            return ContinuableOperatorTaskWithMorsel.producingWorkUnitEvent$( this );
        }

        public long estimatedHeapUsage()
        {
            return ContinuableOperatorTaskWithMorsel.estimatedHeapUsage$( this );
        }

        public void close( final OperatorCloser operatorCloser, final QueryResources resources )
        {
            ContinuableOperatorTask.close$( this, operatorCloser, resources );
        }

        public MorselExecutionContext inputMorsel()
        {
            return this.inputMorsel;
        }

        public FeedPipeQueryState feedPipeQueryState()
        {
            return this.feedPipeQueryState;
        }

        private Iterator<ExecutionContext> resultIterator()
        {
            return this.resultIterator;
        }

        private void resultIterator_$eq( final Iterator<ExecutionContext> x$1 )
        {
            this.resultIterator = x$1;
        }

        private OperatorProfileEvent profileEvent()
        {
            return this.profileEvent;
        }

        private void profileEvent_$eq( final OperatorProfileEvent x$1 )
        {
            this.profileEvent = x$1;
        }

        public WorkIdentity workIdentity()
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeHeadOperator$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeHeadOperator$$super$workIdentity();
        }

        public String toString()
        {
            return (new StringBuilder( 17 )).append( "SlottedPipeTask(" ).append(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeHeadOperator$OTask$$$outer().pipe() ).append( ")" ).toString();
        }

        public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
        {
            if ( this.resultIterator() == null )
            {
                this.resultIterator_$eq(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$SlottedPipeHeadOperator$OTask$$$outer().pipe().createResults(
                                this.feedPipeQueryState() ) );
            }

            while ( outputRow.isValidRow() && this.resultIterator().hasNext() )
            {
                ExecutionContext resultRow = (ExecutionContext) this.resultIterator().next();
                outputRow.copyFrom( resultRow, outputRow.getLongsPerRow(), outputRow.getRefsPerRow() );
                outputRow.moveToNextRow();
            }

            if ( this.profileEvent() != null && this.feedPipeQueryState().profileInformation() != null )
            {
                SlottedPipeOperator$.MODULE$.updateProfileEvent( this.profileEvent(), this.feedPipeQueryState().profileInformation() );
            }

            outputRow.finishedWriting();
        }

        public boolean canContinue()
        {
            return this.resultIterator().hasNext();
        }

        public void setExecutionEvent( final OperatorProfileEvent event )
        {
            this.profileEvent_$eq( event );
        }

        public void closeCursors( final QueryResources resources )
        {
        }
    }
}
