package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ContinuableOperatorTaskWithMorsel extends ContinuableOperatorTask
{
    static void $init$( final ContinuableOperatorTaskWithMorsel $this )
    {
    }

    MorselExecutionContext inputMorsel();

    default void closeInput( final OperatorCloser operatorCloser )
    {
        operatorCloser.closeMorsel( this.inputMorsel() );
    }

    default boolean filterCancelledArguments( final OperatorCloser operatorCloser )
    {
        return operatorCloser.filterCancelledArguments( this.inputMorsel() );
    }

    default WorkUnitEvent producingWorkUnitEvent()
    {
        return this.inputMorsel().producingWorkUnitEvent();
    }

    default long estimatedHeapUsage()
    {
        return this.inputMorsel().estimatedHeapUsage();
    }
}
