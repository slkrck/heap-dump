package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.NonFatalCypherError.;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.SchedulingInputException;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import scala.Option;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ReduceOperatorState<DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> extends OperatorState
{
    static void $init$( final ReduceOperatorState $this )
    {
    }

    default IndexedSeq<ContinuableOperatorTaskWithAccumulator<DATA,ACC>> nextTasks( final QueryContext context, final QueryState state,
            final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
    {
        ArgumentStateMap.MorselAccumulator input = operatorInput.takeAccumulator();
        IndexedSeq var10000;
        if ( input != null )
        {
            try
            {
                var10000 = this.nextTasks( context, state, input, resources );
            }
            catch ( Throwable var13 )
            {
                Option var11 = .MODULE$.unapply( var13 );
                if ( !var11.isEmpty() )
                {
                    Throwable t = (Throwable) var11.get();
                    throw new SchedulingInputException( input, t );
                }

                throw var13;
            }
        }
        else
        {
            var10000 = null;
        }

        return var10000;
    }

    IndexedSeq<ContinuableOperatorTaskWithAccumulator<DATA,ACC>> nextTasks( final QueryContext context, final QueryState state, final ACC input,
            final QueryResources resources );
}
