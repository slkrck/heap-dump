package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ValuePopulation;
import org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.exceptions.InternalException;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import scala.Function1;
import scala.Option;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ProduceResultOperatorTaskTemplate implements OperatorTaskTemplate
{
    private final OperatorTaskTemplate inner;
    private final int id;
    private final Seq<String> columns;
    private final SlotConfiguration slots;
    private final OperatorExpressionCompiler codeGen;

    public ProduceResultOperatorTaskTemplate( final OperatorTaskTemplate inner, final int id, final Seq<String> columns, final SlotConfiguration slots,
            final OperatorExpressionCompiler codeGen )
    {
        this.inner = inner;
        this.id = id;
        this.columns = columns;
        this.slots = slots;
        this.codeGen = codeGen;
        OperatorTaskTemplate.$init$( this );
    }

    public String scopeId()
    {
        return OperatorTaskTemplate.scopeId$( this );
    }

    public final <T> List<T> map( final Function1<OperatorTaskTemplate,T> f )
    {
        return OperatorTaskTemplate.map$( this, f );
    }

    public final <T> Seq<T> flatMap( final Function1<OperatorTaskTemplate,Seq<T>> f )
    {
        return OperatorTaskTemplate.flatMap$( this, f );
    }

    public ClassDeclaration<CompiledTask> genClassDeclaration( final String packageName, final String className, final Seq<StaticField> staticFields )
    {
        return OperatorTaskTemplate.genClassDeclaration$( this, packageName, className, staticFields );
    }

    public InstanceField executionEventField()
    {
        return OperatorTaskTemplate.executionEventField$( this );
    }

    public Option<Field> genProfileEventField()
    {
        return OperatorTaskTemplate.genProfileEventField$( this );
    }

    public IntermediateRepresentation genInitializeProfileEvents()
    {
        return OperatorTaskTemplate.genInitializeProfileEvents$( this );
    }

    public IntermediateRepresentation genCloseProfileEvents()
    {
        return OperatorTaskTemplate.genCloseProfileEvents$( this );
    }

    public final IntermediateRepresentation genOperateWithExpressions()
    {
        return OperatorTaskTemplate.genOperateWithExpressions$( this );
    }

    public IntermediateRepresentation doIfInnerCantContinue( final IntermediateRepresentation op )
    {
        return OperatorTaskTemplate.doIfInnerCantContinue$( this, op );
    }

    public IntermediateRepresentation genOperateEnter()
    {
        return OperatorTaskTemplate.genOperateEnter$( this );
    }

    public IntermediateRepresentation genOperateExit()
    {
        return OperatorTaskTemplate.genOperateExit$( this );
    }

    public IntermediateRepresentation genProduce()
    {
        return OperatorTaskTemplate.genProduce$( this );
    }

    public IntermediateRepresentation genCreateState()
    {
        return OperatorTaskTemplate.genCreateState$( this );
    }

    public Option<IntermediateRepresentation> genOutputBuffer()
    {
        return OperatorTaskTemplate.genOutputBuffer$( this );
    }

    public OperatorTaskTemplate inner()
    {
        return this.inner;
    }

    public int id()
    {
        return this.id;
    }

    public OperatorExpressionCompiler codeGen()
    {
        return this.codeGen;
    }

    public String toString()
    {
        return "ProduceResultTaskTemplate";
    }

    public IntermediateRepresentation genInit()
    {
        return this.inner().genInit();
    }

    public IntermediateRepresentation genOperate()
    {
        IntermediateRepresentation project = .MODULE$.block( (Seq) this.columns.map( ( name ) -> {
        Slot slot = (Slot) this.slots.get( name ).getOrElse( () -> {
            throw new InternalException(
                    (new StringBuilder( 41 )).append( "Did not find `" ).append( name ).append( "` in the slot configuration" ).toString() );
        } );
        return .MODULE$.invokeSideEffect(.MODULE$.load( OperatorCodeGenHelperTemplates$.MODULE$.SUBSCRIBER() ), .
        MODULE$.method( "onField", scala.reflect.ManifestFactory..MODULE$.classType( QuerySubscriber.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getFromSlot$1( slot )}) ));
    }, scala.collection.Seq..MODULE$.canBuildFrom() ));
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.invokeSideEffect(.MODULE$.load(
            OperatorCodeGenHelperTemplates$.MODULE$.SUBSCRIBER() ),.MODULE$.method( "onRecord", scala.reflect.ManifestFactory..MODULE$.classType(
            QuerySubscriber.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),project, .
        MODULE$.invokeSideEffect(.MODULE$.load( OperatorCodeGenHelperTemplates$.MODULE$.SUBSCRIBER() ), .
        MODULE$.method( "onRecordCompleted", scala.reflect.ManifestFactory..MODULE$.classType( QuerySubscriber.class ), scala.reflect.ManifestFactory..
        MODULE$.Unit()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )), .
        MODULE$.assign( OperatorCodeGenHelperTemplates$.MODULE$.SERVED(),.MODULE$.add(.MODULE$.load( OperatorCodeGenHelperTemplates$.MODULE$.SERVED() ), .
        MODULE$.constant( BoxesRunTime.boxToLong( 1L ) ))),
        OperatorCodeGenHelperTemplates$.MODULE$.profileRow( this.id() ), this.inner().genOperateWithExpressions()})));
    }

    public Seq<Field> genFields()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Seq<LocalVariable> genLocalVariables()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new LocalVariable[]{OperatorCodeGenHelperTemplates$.MODULE$.PRE_POPULATE_RESULTS_V(),
                    OperatorCodeGenHelperTemplates$.MODULE$.SUBSCRIBER(), OperatorCodeGenHelperTemplates$.MODULE$.SUBSCRIPTION(),
                    OperatorCodeGenHelperTemplates$.MODULE$.DEMAND(), OperatorCodeGenHelperTemplates$.MODULE$.SERVED()}) ));
    }

    public Seq<IntermediateExpression> genExpressions()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Option<IntermediateRepresentation> genCanContinue()
    {
        return this.inner().genCanContinue();
    }

    public IntermediateRepresentation genCloseCursors()
    {
        return this.inner().genCloseCursors();
    }

    public IntermediateRepresentation genSetExecutionEvent( final IntermediateRepresentation event )
    {
        return this.inner().genSetExecutionEvent( event );
    }

    private final IntermediateRepresentation getLongAt$1( final int offset )
    {
        return this.codeGen().getLongAt( offset );
    }

    private final IntermediateRepresentation getRefAt$1( final int offset )
    {
        IntermediateRepresentation notPopulated = this.codeGen().getRefAt( offset );
        return .MODULE$.ternary( OperatorCodeGenHelperTemplates$.MODULE$.PRE_POPULATE_RESULTS(),.MODULE$.invokeStatic(.MODULE$.method( "populate",
            scala.reflect.ManifestFactory..MODULE$.classType( ValuePopulation.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{notPopulated}) )),notPopulated);
    }

    private final IntermediateRepresentation nodeFromSlot$1( final int offset )
    {
        IntermediateRepresentation notPopulated = .
        MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),.MODULE$.method( "nodeById", scala.reflect.ManifestFactory..MODULE$.classType(
                DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt$1( offset )}) ));
        return .MODULE$.ternary( OperatorCodeGenHelperTemplates$.MODULE$.PRE_POPULATE_RESULTS(),.MODULE$.invokeStatic(.MODULE$.method( "populate",
            scala.reflect.ManifestFactory..MODULE$.classType( ValuePopulation.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{notPopulated}) )),notPopulated);
    }

    private final IntermediateRepresentation relFromSlot$1( final int offset )
    {
        IntermediateRepresentation notPopulated = .MODULE$.invoke( OperatorCodeGenHelperTemplates$.MODULE$.DB_ACCESS(),.MODULE$.method( "relationshipById",
            scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt$1( offset )}) ));
        return .MODULE$.ternary( OperatorCodeGenHelperTemplates$.MODULE$.PRE_POPULATE_RESULTS(),.MODULE$.invokeStatic(.MODULE$.method( "populate",
            scala.reflect.ManifestFactory..MODULE$.classType( ValuePopulation.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{notPopulated}) )),notPopulated);
    }

    private final IntermediateRepresentation getFromSlot$1( final Slot slot )
    {
        IntermediateRepresentation var2;
        int offset;
        label106:
        {
            boolean var3 = false;
            LongSlot var4 = null;
            NodeType var10000;
            if ( slot instanceof LongSlot )
            {
                var3 = true;
                var4 = (LongSlot) slot;
                offset = var4.offset();
                boolean var7 = var4.nullable();
                CypherType var8 = var4.typ();
                if ( var7 )
                {
                    var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var8 == null )
                        {
                            break label106;
                        }
                    }
                    else if ( var10000.equals( var8 ) )
                    {
                        break label106;
                    }
                }
            }

            int offset;
            label107:
            {
                if ( var3 )
                {
                    offset = var4.offset();
                    boolean var11 = var4.nullable();
                    CypherType var12 = var4.typ();
                    if ( !var11 )
                    {
                        var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                        if ( var10000 == null )
                        {
                            if ( var12 == null )
                            {
                                break label107;
                            }
                        }
                        else if ( var10000.equals( var12 ) )
                        {
                            break label107;
                        }
                    }
                }

                RelationshipType var24;
                if ( var3 )
                {
                    int offset = var4.offset();
                    boolean var15 = var4.nullable();
                    CypherType var16 = var4.typ();
                    if ( var15 )
                    {
                        label108:
                        {
                            var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                            if ( var24 == null )
                            {
                                if ( var16 != null )
                                {
                                    break label108;
                                }
                            }
                            else if ( !var24.equals( var16 ) )
                            {
                                break label108;
                            }

                            var2 = .MODULE$.ternary(.MODULE$.equal( this.getLongAt$1( offset ),.MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )), .
                            MODULE$.noValue(), this.relFromSlot$1( offset ));
                            return var2;
                        }
                    }
                }

                int offset;
                label109:
                {
                    if ( var3 )
                    {
                        offset = var4.offset();
                        boolean var19 = var4.nullable();
                        CypherType var20 = var4.typ();
                        if ( !var19 )
                        {
                            var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                            if ( var24 == null )
                            {
                                if ( var20 == null )
                                {
                                    break label109;
                                }
                            }
                            else if ( var24.equals( var20 ) )
                            {
                                break label109;
                            }
                        }
                    }

                    if ( !(slot instanceof RefSlot) )
                    {
                        throw new InternalException( (new StringBuilder( 27 )).append( "Do not know how to project " ).append( slot ).toString() );
                    }

                    RefSlot var22 = (RefSlot) slot;
                    int offset = var22.offset();
                    var2 = this.getRefAt$1( offset );
                    return var2;
                }

                var2 = this.relFromSlot$1( offset );
                return var2;
            }

            var2 = this.nodeFromSlot$1( offset );
            return var2;
        }

        var2 = .MODULE$.ternary(.MODULE$.equal( this.getLongAt$1( offset ),.MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )), .
        MODULE$.noValue(), this.nodeFromSlot$1( offset ));
        return var2;
    }
}
