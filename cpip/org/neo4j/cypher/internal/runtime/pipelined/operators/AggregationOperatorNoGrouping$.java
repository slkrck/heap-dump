package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class AggregationOperatorNoGrouping$ extends AbstractFunction2<WorkIdentity,Aggregator[],AggregationOperatorNoGrouping> implements Serializable
{
    public static AggregationOperatorNoGrouping$ MODULE$;

    static
    {
        new AggregationOperatorNoGrouping$();
    }

    private AggregationOperatorNoGrouping$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "AggregationOperatorNoGrouping";
    }

    public AggregationOperatorNoGrouping apply( final WorkIdentity workIdentity, final Aggregator[] aggregations )
    {
        return new AggregationOperatorNoGrouping( workIdentity, aggregations );
    }

    public Option<Tuple2<WorkIdentity,Aggregator[]>> unapply( final AggregationOperatorNoGrouping x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.workIdentity(), x$0.aggregations() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
