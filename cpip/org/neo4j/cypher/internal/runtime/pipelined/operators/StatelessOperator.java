package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface StatelessOperator extends MiddleOperator, OperatorTask
{
    static void $init$( final StatelessOperator $this )
    {
    }

    default OperatorTask createTask( final ArgumentStateMapCreator argumentStateCreator, final StateFactory stateFactory, final QueryContext queryContext,
            final QueryState state, final QueryResources resources )
    {
        return this;
    }

    default void setExecutionEvent( final OperatorProfileEvent event )
    {
    }
}
