package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.internal.kernel.api.IndexQuery.ExactPredicate;
import org.neo4j.values.storable.Value;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ExactPredicateIterator
{
    private final ExactPredicate[] predicates;
    private int index;

    public ExactPredicateIterator( final int propertyKey, final ExactPredicate[] predicates )
    {
        this.predicates = predicates;
        this.index = 0;
    }

    private int index()
    {
        return this.index;
    }

    private void index_$eq( final int x$1 )
    {
        this.index = x$1;
    }

    public boolean hasNext()
    {
        return this.index() < this.predicates.length;
    }

    public ExactPredicate next()
    {
        ExactPredicate current = this.predicates[this.index()];
        this.index_$eq( this.index() + 1 );
        return current;
    }

    public Value current()
    {
        return this.predicates[this.index() - 1].value();
    }
}
