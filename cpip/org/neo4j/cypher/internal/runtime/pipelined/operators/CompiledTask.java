package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import scala.Option;
import scala.Some;
import scala.None.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CompiledTask extends ContinuableOperatorTaskWithMorsel, OutputOperator, OutputOperatorState, PreparedOutput
{
    static void $init$( final CompiledTask $this )
    {
    }

    default boolean trackTime()
    {
        return false;
    }

    default void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources,
            final QueryProfiler queryProfiler )
    {
        this.initializeProfileEvents( queryProfiler );

        try
        {
            this.compiledOperate( output, context, state, resources, queryProfiler );
        }
        finally
        {
            this.closeProfileEvents( resources );
        }
    }

    default OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
    {
        this.compiledCreateState( executionState, pipelineId );
        return this;
    }

    default Option<BufferId> outputBuffer()
    {
        int var1 = this.compiledOutputBuffer();
        Object var10000;
        switch ( var1 )
        {
        case -1:
            var10000 = .MODULE$;
            break;
        default:
            var10000 = new Some( new BufferId( var1 ) );
        }

        return (Option) var10000;
    }

    default PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
            final QueryResources resources, final QueryProfiler queryProfiler )
    {
        return this;
    }

    default PreparedOutput prepareOutput( final MorselExecutionContext outputMorsel, final QueryContext context, final QueryState state,
            final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
    {
        throw new IllegalStateException( "Fused operators should be called via prepareOutputWithProfile." );
    }

    default void produce()
    {
        this.compiledProduce();
    }

    void initializeProfileEvents( final QueryProfiler queryProfiler );

    void compiledOperate( final MorselExecutionContext context, final QueryContext dbAccess, final QueryState state, final QueryResources resources,
            final QueryProfiler queryProfiler ) throws Exception;

    void compiledProduce() throws Exception;

    void compiledCreateState( final ExecutionState executionState, final int pipelineId ) throws Exception;

    int compiledOutputBuffer() throws Exception;

    void closeProfileEvents( final QueryResources resources );

    default void setExecutionEvent( final OperatorProfileEvent event )
    {
        throw new IllegalStateException( "Fused operators should be called via operateWithProfile." );
    }

    default void operate( final MorselExecutionContext output, final QueryContext context, final QueryState state, final QueryResources resources )
    {
        throw new IllegalStateException( "Fused operators should be called via operateWithProfile." );
    }
}
