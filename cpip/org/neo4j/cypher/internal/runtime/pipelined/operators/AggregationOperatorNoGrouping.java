package org.neo4j.cypher.internal.runtime.pipelined.operators;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatingAccumulator;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator;
import org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap$;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState$;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class AggregationOperatorNoGrouping implements Product, Serializable
{
    private final WorkIdentity workIdentity;
    private final Aggregator[] aggregations;

    public AggregationOperatorNoGrouping( final WorkIdentity workIdentity, final Aggregator[] aggregations )
    {
        this.workIdentity = workIdentity;
        this.aggregations = aggregations;
        Product.$init$( this );
    }

    public static Option<Tuple2<WorkIdentity,Aggregator[]>> unapply( final AggregationOperatorNoGrouping x$0 )
    {
        return AggregationOperatorNoGrouping$.MODULE$.unapply( var0 );
    }

    public static AggregationOperatorNoGrouping apply( final WorkIdentity workIdentity, final Aggregator[] aggregations )
    {
        return AggregationOperatorNoGrouping$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<WorkIdentity,Aggregator[]>,AggregationOperatorNoGrouping> tupled()
    {
        return AggregationOperatorNoGrouping$.MODULE$.tupled();
    }

    public static Function1<WorkIdentity,Function1<Aggregator[],AggregationOperatorNoGrouping>> curried()
    {
        return AggregationOperatorNoGrouping$.MODULE$.curried();
    }

    public WorkIdentity workIdentity()
    {
        return this.workIdentity;
    }

    public Aggregator[] aggregations()
    {
        return this.aggregations;
    }

    public AggregationOperatorNoGrouping.AggregationMapperOperatorNoGrouping mapper( final int argumentSlotOffset, final int outputBufferId,
            final Expression[] expressionValues )
    {
        return new AggregationOperatorNoGrouping.AggregationMapperOperatorNoGrouping( this, this.workIdentity(), argumentSlotOffset, outputBufferId,
                this.aggregations(), expressionValues );
    }

    public AggregationOperatorNoGrouping.AggregationReduceOperatorNoGrouping reducer( final int argumentStateMapId, final int[] reducerOutputSlots )
    {
        return new AggregationOperatorNoGrouping.AggregationReduceOperatorNoGrouping( this, argumentStateMapId, this.workIdentity(), this.aggregations(),
                reducerOutputSlots );
    }

    public AggregationOperatorNoGrouping copy( final WorkIdentity workIdentity, final Aggregator[] aggregations )
    {
        return new AggregationOperatorNoGrouping( workIdentity, aggregations );
    }

    public WorkIdentity copy$default$1()
    {
        return this.workIdentity();
    }

    public Aggregator[] copy$default$2()
    {
        return this.aggregations();
    }

    public String productPrefix()
    {
        return "AggregationOperatorNoGrouping";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.workIdentity();
            break;
        case 1:
            var10000 = this.aggregations();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AggregationOperatorNoGrouping;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof AggregationOperatorNoGrouping )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        label37:
                        {
                            AggregationOperatorNoGrouping var4 = (AggregationOperatorNoGrouping) x$1;
                            WorkIdentity var10000 = this.workIdentity();
                            WorkIdentity var5 = var4.workIdentity();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label37;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label37;
                            }

                            if ( this.aggregations() == var4.aggregations() && var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label38;
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }

    public class AggregationMapperOperatorNoGrouping implements OutputOperator
    {
        public final int
                org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$argumentSlotOffset;
        public final Aggregator[]
                org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$aggregations;
        public final Expression[]
                org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$expressionValues;
        private final WorkIdentity workIdentity;
        private final int outputBufferId;

        public AggregationMapperOperatorNoGrouping( final AggregationOperatorNoGrouping $outer, final WorkIdentity workIdentity, final int argumentSlotOffset,
                final int outputBufferId, final Aggregator[] aggregations, final Expression[] expressionValues )
        {
            this.workIdentity = workIdentity;
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$argumentSlotOffset =
                    argumentSlotOffset;
            this.outputBufferId = outputBufferId;
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$aggregations =
                    aggregations;
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$expressionValues =
                    expressionValues;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public WorkIdentity workIdentity()
        {
            return this.workIdentity;
        }

        public Option<BufferId> outputBuffer()
        {
            return new Some( new BufferId( this.outputBufferId ) );
        }

        public OutputOperatorState createState( final ExecutionState executionState, final int pipelineId )
        {
            return new AggregationOperatorNoGrouping.AggregationMapperOperatorNoGrouping.State( this,
                    executionState.getSink( pipelineId, this.outputBufferId ) );
        }

        public class PreAggregatedOutput implements PreparedOutput
        {
            private final IndexedSeq<ArgumentStateMap.PerArgument<Updater[]>> preAggregated;
            private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<Updater[]>>> sink;

            public PreAggregatedOutput( final AggregationOperatorNoGrouping.AggregationMapperOperatorNoGrouping $outer,
                    final IndexedSeq<ArgumentStateMap.PerArgument<Updater[]>> preAggregated,
                    final Sink<IndexedSeq<ArgumentStateMap.PerArgument<Updater[]>>> sink )
            {
                this.preAggregated = preAggregated;
                this.sink = sink;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                }
            }

            public void produce()
            {
                this.sink.put( this.preAggregated );
            }
        }

        public class State implements OutputOperatorState
        {
            private final Sink<IndexedSeq<ArgumentStateMap.PerArgument<Updater[]>>> sink;

            public State( final AggregationOperatorNoGrouping.AggregationMapperOperatorNoGrouping $outer,
                    final Sink<IndexedSeq<ArgumentStateMap.PerArgument<Updater[]>>> sink )
            {
                this.sink = sink;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    OutputOperatorState.$init$( this );
                }
            }

            public PreparedOutput prepareOutputWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                    final QueryResources resources, final QueryProfiler queryProfiler )
            {
                return OutputOperatorState.prepareOutputWithProfile$( this, output, context, state, resources, queryProfiler );
            }

            public boolean canContinueOutput()
            {
                return OutputOperatorState.canContinueOutput$( this );
            }

            public WorkIdentity workIdentity()
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$State$$$outer().workIdentity();
            }

            public boolean trackTime()
            {
                return true;
            }

            public AggregationOperatorNoGrouping.AggregationMapperOperatorNoGrouping.PreAggregatedOutput prepareOutput( final MorselExecutionContext morsel,
                    final QueryContext context, final QueryState state, final QueryResources resources, final OperatorProfileEvent operatorExecutionEvent )
            {
                SlottedQueryState queryState = new SlottedQueryState( context, (ExternalCSVResource) null, state.params(), resources.expressionCursors(),
                        (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( IndexReadSession.class )),
                resources.expressionVariables( state.nExpressionSlots() ), state.subscriber(), org.neo4j.cypher.internal.runtime.NoMemoryTracker..
                MODULE$, SlottedQueryState$.MODULE$.$lessinit$greater$default$9(), SlottedQueryState$.MODULE$.$lessinit$greater$default$10(), SlottedQueryState$.MODULE$.$lessinit$greater$default$11(), SlottedQueryState$.MODULE$.$lessinit$greater$default$12(), SlottedQueryState$.MODULE$.$lessinit$greater$default$13(), SlottedQueryState$.MODULE$.$lessinit$greater$default$14())
                ;
                IndexedSeq preAggregated = ArgumentStateMap$.MODULE$.map(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$argumentSlotOffset,
                        morsel, ( morselx ) -> {
                            return this.preAggregate( queryState, morselx );
                        } );
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$State$$$outer().new PreAggregatedOutput(
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$State$$$outer(),
                        preAggregated, this.sink );
            }

            private Updater[] preAggregate( final SlottedQueryState queryState, final MorselExecutionContext morsel )
            {
                Updater[] updaters = (Updater[]) (new ofRef( scala.Predef..MODULE$.refArrayOps(
                        (Object[]) this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$aggregations ))).
                map( ( x$1 ) -> {
                    return x$1.newUpdater();
                }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Updater.class )));

                while ( morsel.isValidRow() )
                {
                    for ( int i = 0; i <
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$aggregations.length;
                            ++i )
                    {
                        AnyValue value =
                                this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$State$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationMapperOperatorNoGrouping$$expressionValues[i].apply(
                                        morsel, queryState );
                        updaters[i].update( value );
                    }

                    morsel.moveToNextRow();
                }

                return updaters;
            }
        }
    }

    public class AggregationReduceOperatorNoGrouping implements Operator, ReduceOperatorState<Updater[],AggregatingAccumulator>
    {
        public final Aggregator[]
                org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$$aggregations;
        public final int[]
                org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$$reducerOutputSlots;
        private final int argumentStateMapId;
        private final WorkIdentity workIdentity;

        public AggregationReduceOperatorNoGrouping( final AggregationOperatorNoGrouping $outer, final int argumentStateMapId, final WorkIdentity workIdentity,
                final Aggregator[] aggregations, final int[] reducerOutputSlots )
        {
            this.argumentStateMapId = argumentStateMapId;
            this.workIdentity = workIdentity;
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$$aggregations =
                    aggregations;
            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$$reducerOutputSlots =
                    reducerOutputSlots;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                ReduceOperatorState.$init$( this );
            }
        }

        public final IndexedSeq<ContinuableOperatorTaskWithAccumulator<Updater[],AggregatingAccumulator>> nextTasks( final QueryContext context,
                final QueryState state, final OperatorInput operatorInput, final int parallelism, final QueryResources resources,
                final ArgumentStateMap.ArgumentStateMaps argumentStateMaps )
        {
            return ReduceOperatorState.nextTasks$( this, context, state, operatorInput, parallelism, resources, argumentStateMaps );
        }

        public int argumentStateMapId()
        {
            return this.argumentStateMapId;
        }

        public WorkIdentity workIdentity()
        {
            return this.workIdentity;
        }

        public ReduceOperatorState<Updater[],AggregatingAccumulator> createState( final ArgumentStateMapCreator argumentStateCreator,
                final StateFactory stateFactory, final QueryContext queryContext, final QueryState state, final QueryResources resources )
        {
            argumentStateCreator.createArgumentStateMap( this.argumentStateMapId(), new AggregatingAccumulator.Factory(
                    this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$$aggregations,
                    stateFactory.memoryTracker() ) );
            return this;
        }

        public IndexedSeq<ContinuableOperatorTaskWithAccumulator<Updater[],AggregatingAccumulator>> nextTasks( final QueryContext queryContext,
                final QueryState state, final AggregatingAccumulator input, final QueryResources resources )
        {
            return scala.Predef..MODULE$.wrapRefArray( (Object[]) ((Object[]) (new AggregationOperatorNoGrouping.AggregationReduceOperatorNoGrouping.OTask[]{
                new AggregationOperatorNoGrouping.AggregationReduceOperatorNoGrouping.OTask( this, input )})) );
        }

        public class OTask implements ContinuableOperatorTaskWithAccumulator<Updater[],AggregatingAccumulator>
        {
            private final AggregatingAccumulator accumulator;

            public OTask( final AggregationOperatorNoGrouping.AggregationReduceOperatorNoGrouping $outer, final AggregatingAccumulator accumulator )
            {
                this.accumulator = accumulator;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    OperatorTask.$init$( this );
                    ContinuableOperatorTask.$init$( this );
                    ContinuableOperatorTaskWithAccumulator.$init$( this );
                }
            }

            public void closeInput( final OperatorCloser operatorCloser )
            {
                ContinuableOperatorTaskWithAccumulator.closeInput$( this, operatorCloser );
            }

            public boolean filterCancelledArguments( final OperatorCloser operatorCloser )
            {
                return ContinuableOperatorTaskWithAccumulator.filterCancelledArguments$( this, operatorCloser );
            }

            public WorkUnitEvent producingWorkUnitEvent()
            {
                return ContinuableOperatorTaskWithAccumulator.producingWorkUnitEvent$( this );
            }

            public void setExecutionEvent( final OperatorProfileEvent event )
            {
                ContinuableOperatorTaskWithAccumulator.setExecutionEvent$( this, event );
            }

            public void closeCursors( final QueryResources resources )
            {
                ContinuableOperatorTaskWithAccumulator.closeCursors$( this, resources );
            }

            public long estimatedHeapUsage()
            {
                return ContinuableOperatorTaskWithAccumulator.estimatedHeapUsage$( this );
            }

            public void close( final OperatorCloser operatorCloser, final QueryResources resources )
            {
                ContinuableOperatorTask.close$( this, operatorCloser, resources );
            }

            public void operateWithProfile( final MorselExecutionContext output, final QueryContext context, final QueryState state,
                    final QueryResources resources, final QueryProfiler queryProfiler )
            {
                OperatorTask.operateWithProfile$( this, output, context, state, resources, queryProfiler );
            }

            public AggregatingAccumulator accumulator()
            {
                return this.accumulator;
            }

            public WorkIdentity workIdentity()
            {
                return this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$OTask$$$outer().workIdentity();
            }

            public void operate( final MorselExecutionContext outputRow, final QueryContext context, final QueryState state, final QueryResources resources )
            {
                for ( int i = 0; i <
                        this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$$aggregations.length;
                        ++i )
                {
                    outputRow.setRefAt(
                            this.org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$OTask$$$outer().org$neo4j$cypher$internal$runtime$pipelined$operators$AggregationOperatorNoGrouping$AggregationReduceOperatorNoGrouping$$reducerOutputSlots[i],
                            this.accumulator().result( i ) );
                }

                outputRow.moveToNextRow();
                outputRow.finishedWriting();
            }

            public boolean canContinue()
            {
                return false;
            }
        }
    }
}
