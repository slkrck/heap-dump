package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class PipelinedDebugSupport
{
    public static Seq<String> prettyWorkDone()
    {
        return PipelinedDebugSupport$.MODULE$.prettyWorkDone();
    }

    public static Seq<String> prettyMorselWithHeader( final String header, final MorselExecutionContext morsel )
    {
        return PipelinedDebugSupport$.MODULE$.prettyMorselWithHeader( var0, var1 );
    }

    public static Seq<String> prettyWork( final MorselExecutionContext morsel, final WorkIdentity workIdentity )
    {
        return PipelinedDebugSupport$.MODULE$.prettyWork( var0, var1 );
    }

    public static Seq<String> prettyPostStartTask( final ContinuableOperatorTask startTask )
    {
        return PipelinedDebugSupport$.MODULE$.prettyPostStartTask( var0 );
    }

    public static Seq<String> prettyStartTask( final ContinuableOperatorTask startTask, final WorkIdentity workIdentity )
    {
        return PipelinedDebugSupport$.MODULE$.prettyStartTask( var0, var1 );
    }
}
