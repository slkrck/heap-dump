package org.neo4j.cypher.internal.runtime.pipelined;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

import org.neo4j.cypher.internal.RuntimeResourceLeakException;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryManager;
import org.neo4j.kernel.lifecycle.Lifecycle;
import scala.Predef.;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.concurrent.duration.FiniteDuration;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class WorkerManager implements WorkerManagement, Lifecycle
{
    private final int numberOfWorkers;
    private final ThreadFactory threadFactory;
    private final QueryManager queryManager;
    private final Worker[] _workers;
    private final FiniteDuration threadJoinWait;
    private volatile Thread[] workerThreads;

    public WorkerManager( final int numberOfWorkers, final ThreadFactory threadFactory )
    {
        this.numberOfWorkers = numberOfWorkers;
        this.threadFactory = threadFactory;
        this.queryManager = new QueryManager();
        this._workers = (Worker[]) ((TraversableOnce) scala.runtime.RichInt..MODULE$.until$extension0(.MODULE$.intWrapper( 0 ), numberOfWorkers).
        map( ( workerId ) -> {
            return $anonfun$_workers$1( this, BoxesRunTime.unboxToInt( workerId ) );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Worker.class ));
        this.threadJoinWait = scala.concurrent.duration.Duration..MODULE$.apply( 1L, TimeUnit.MINUTES );
    }

    public int numberOfWorkers()
    {
        return this.numberOfWorkers;
    }

    public QueryManager queryManager()
    {
        return this.queryManager;
    }

    private Worker[] _workers()
    {
        return this._workers;
    }

    public Seq<Worker> workers()
    {
        return .MODULE$.wrapRefArray( (Object[]) this._workers() );
    }

    public void assertNoWorkerIsActive()
    {
        String[] activeWorkers =
                (String[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) this._workers() )) ).filter( ( x$1 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$assertNoWorkerIsActive$1( x$1 ) );
                } )))).map( ( worker ) -> {
        return Worker$.MODULE$.WORKING_THOUGH_RELEASED( worker );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( String.class )));
        if ( (new ofRef(.MODULE$.refArrayOps( (Object[]) activeWorkers )) ).nonEmpty()){
        throw new RuntimeResourceLeakException( (new ofRef(.MODULE$.refArrayOps( (Object[]) activeWorkers )) ).mkString( "\n" ));
    }
    }

    public void wakeOne()
    {
        for ( int i = 0; i < this._workers().length; ++i )
        {
            if ( this._workers()[i].isSleeping() )
            {
                LockSupport.unpark( this.workerThreads()[i] );
                return;
            }
        }
    }

    private FiniteDuration threadJoinWait()
    {
        return this.threadJoinWait;
    }

    private Thread[] workerThreads()
    {
        return this.workerThreads;
    }

    private void workerThreads_$eq( final Thread[] x$1 )
    {
        this.workerThreads = x$1;
    }

    public void init()
    {
    }

    public void start()
    {
        org.neo4j.cypher.internal.runtime.debug.DebugLog..MODULE$.log( "starting worker threads" );
        (new ofRef(.MODULE$.refArrayOps( (Object[]) this._workers() ))).foreach( ( x$2 ) -> {
        $anonfun$start$1( x$2 );
        return BoxedUnit.UNIT;
    } );
        this.workerThreads_$eq( (Thread[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) this._workers() )) ).map( ( x$3 ) -> {
            return this.threadFactory.newThread( x$3 );
        }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Thread.class ))));
        (new ofRef(.MODULE$.refArrayOps( (Object[]) this.workerThreads() ))).foreach( ( x$4 ) -> {
        $anonfun$start$3( x$4 );
        return BoxedUnit.UNIT;
    } );
        org.neo4j.cypher.internal.runtime.debug.DebugLog..MODULE$.logDiff( "done" );
    }

    public void stop()
    {
        org.neo4j.cypher.internal.runtime.debug.DebugLog..MODULE$.log( "stopping worker threads" );
        (new ofRef(.MODULE$.refArrayOps( (Object[]) this._workers() ))).foreach( ( x$5 ) -> {
        $anonfun$stop$1( x$5 );
        return BoxedUnit.UNIT;
    } );
        (new ofRef(.MODULE$.refArrayOps( (Object[]) this.workerThreads() ))).foreach( ( x$1 ) -> {
        $anonfun$stop$2( x$1 );
        return BoxedUnit.UNIT;
    } );
        (new ofRef(.MODULE$.refArrayOps( (Object[]) this.workerThreads() ))).foreach( ( x$6 ) -> {
        $anonfun$stop$3( this, x$6 );
        return BoxedUnit.UNIT;
    } );
        org.neo4j.cypher.internal.runtime.debug.DebugLog..MODULE$.logDiff( "done" );
    }

    public void shutdown()
    {
    }
}
