package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryManager;
import org.neo4j.cypher.internal.runtime.pipelined.execution.WorkerWaker;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface WorkerManagement extends WorkerWaker
{
    QueryManager queryManager();

    Seq<Worker> workers();

    int numberOfWorkers();

    void assertNoWorkerIsActive();
}
