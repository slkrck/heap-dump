package org.neo4j.cypher.internal.runtime.pipelined;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;

public final class SchedulingInputException$ implements Serializable
{
    public static SchedulingInputException$ MODULE$;

    static
    {
        new SchedulingInputException$();
    }

    private SchedulingInputException$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SchedulingInputException";
    }

    public <T> SchedulingInputException<T> apply( final T input, final Throwable cause )
    {
        return new SchedulingInputException( input, cause );
    }

    public <T> Option<Tuple2<T,Throwable>> unapply( final SchedulingInputException<T> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.input(), x$0.cause() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
