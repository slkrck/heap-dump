package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap$;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class MorselAttachBuffer implements Buffers.SinkByOrigin, Sink<MorselExecutionContext>
{
    private final int id;
    private final MorselApplyBuffer delegateApplyBuffer;
    private final SlotConfiguration outputSlots;
    private final int argumentSlotOffset;
    private final int argumentNumLongs;
    private final int argumentNumRefs;

    public MorselAttachBuffer( final int id, final MorselApplyBuffer delegateApplyBuffer, final SlotConfiguration outputSlots, final int argumentSlotOffset,
            final int argumentNumLongs, final int argumentNumRefs )
    {
        this.id = id;
        this.delegateApplyBuffer = delegateApplyBuffer;
        this.outputSlots = outputSlots;
        this.argumentSlotOffset = argumentSlotOffset;
        this.argumentNumLongs = argumentNumLongs;
        this.argumentNumRefs = argumentNumRefs;
    }

    public <T> Sink<T> sinkFor( final int fromPipeline )
    {
        return this;
    }

    public void put( final MorselExecutionContext morsel )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[put]   " ).append( this ).append( " <- " ).append( morsel ).toString() );
    }

        if ( morsel.hasData() )
        {
            ArgumentStateMap$.MODULE$.foreach( this.argumentSlotOffset, morsel, ( x$1, view ) -> {
                $anonfun$put$1( this, morsel, BoxesRunTime.unboxToLong( x$1 ), view );
                return BoxedUnit.UNIT;
            } );
        }
    }

    public boolean canPut()
    {
        return this.delegateApplyBuffer.canPut();
    }

    public String toString()
    {
        return (new StringBuilder( 42 )).append( "MorselAttachBuffer(" ).append( new BufferId( this.id ) ).append( ", delegateApplyBuffer=" ).append(
                this.delegateApplyBuffer ).append( ")" ).toString();
    }
}
