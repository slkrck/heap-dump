package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ClosingSource<T> extends Source<T>
{
    void close( final T data );
}
