package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class EndOfNonEmptyStream$ implements EndOfStream, Product, Serializable
{
    public static EndOfNonEmptyStream$ MODULE$;

    static
    {
        new EndOfNonEmptyStream$();
    }

    private EndOfNonEmptyStream$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "EndOfNonEmptyStream";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EndOfNonEmptyStream$;
    }

    public int hashCode()
    {
        return 101503666;
    }

    public String toString()
    {
        return "EndOfNonEmptyStream";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
