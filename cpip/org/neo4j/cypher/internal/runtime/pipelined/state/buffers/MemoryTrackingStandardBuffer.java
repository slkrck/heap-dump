package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class MemoryTrackingStandardBuffer<T extends WithHeapUsageEstimation> extends StandardBuffer<T>
{
    private final QueryMemoryTracker memoryTracker;

    public MemoryTrackingStandardBuffer( final QueryMemoryTracker memoryTracker )
    {
        this.memoryTracker = memoryTracker;
    }

    public void put( final T t )
    {
        this.memoryTracker.allocated( t );
        super.put( t );
    }

    public T take()
    {
        WithHeapUsageEstimation t = (WithHeapUsageEstimation) super.take();
        if ( t != null )
        {
            this.memoryTracker.deallocated( t.estimatedHeapUsage() );
        }

        return t;
    }
}
