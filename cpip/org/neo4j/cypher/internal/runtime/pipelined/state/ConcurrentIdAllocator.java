package org.neo4j.cypher.internal.runtime.pipelined.state;

import java.util.concurrent.atomic.AtomicLong;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ConcurrentIdAllocator implements IdAllocator
{
    private final AtomicLong nextFreeId = new AtomicLong( 0L );

    private AtomicLong nextFreeId()
    {
        return this.nextFreeId;
    }

    public long allocateIdBatch( final int nIds )
    {
        return this.nextFreeId().getAndAdd( (long) nIds );
    }
}
