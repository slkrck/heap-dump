package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.exceptions.CypherExecutionException;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ReferenceCountingException extends CypherExecutionException
{
    public ReferenceCountingException( final String msg )
    {
        super( msg );
    }
}
