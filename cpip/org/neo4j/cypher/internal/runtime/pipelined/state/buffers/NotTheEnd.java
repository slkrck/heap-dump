package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class NotTheEnd
{
    public static String toString()
    {
        return NotTheEnd$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return NotTheEnd$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return NotTheEnd$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return NotTheEnd$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return NotTheEnd$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return NotTheEnd$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return NotTheEnd$.MODULE$.productPrefix();
    }
}
