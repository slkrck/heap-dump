package org.neo4j.cypher.internal.runtime.pipelined.state;

import java.util.Iterator;
import java.util.Map;

import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.util.Preconditions;
import scala.Function1;
import scala.Function2;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class AbstractArgumentStateMap<STATE extends ArgumentStateMap.ArgumentState, CONTROLLER extends AbstractArgumentStateMap.StateController<STATE>>
        implements ArgumentStateMapWithArgumentIdCounter<STATE>, ArgumentStateMapWithoutArgumentIdCounter<STATE>
{
    public abstract Map<Object,CONTROLLER> controllers();

    public abstract long lastCompletedArgumentId();

    public abstract void lastCompletedArgumentId_$eq( final long x$1 );

    public abstract CONTROLLER newStateController( final long argument, final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers );

    public void update( final long argumentRowId, final Function1<STATE,BoxedUnit> onState )
    {
        onState.apply( ((AbstractArgumentStateMap.StateController) this.controllers().get( BoxesRunTime.boxToLong( argumentRowId ) )).state() );
    }

    public void clearAll( final Function1<STATE,BoxedUnit> f )
    {
        this.controllers().forEach( ( x$1, controller ) -> {
            $anonfun$clearAll$1( f, BoxesRunTime.unboxToLong( x$1 ), controller );
        } );
    }

    public <U> void filter( final MorselExecutionContext readingRow, final Function2<STATE,Object,U> onArgument,
            final Function2<U,MorselExecutionContext,Object> onRow )
    {
        ArgumentStateMap$.MODULE$.filter( this.argumentSlotOffset(), readingRow, ( argumentRowId, nRows ) -> {
            return $anonfun$filter$1( this, onArgument, BoxesRunTime.unboxToLong( argumentRowId ), BoxesRunTime.unboxToLong( nRows ) );
        }, onRow );
    }

    public STATE takeOneCompleted()
    {
        Iterator iterator = this.controllers().values().iterator();

        AbstractArgumentStateMap.StateController controller;
        do
        {
            if ( !iterator.hasNext() )
            {
                return null;
            }

            controller = (AbstractArgumentStateMap.StateController) iterator.next();
        }
        while ( !controller.tryTake() );

        this.controllers().remove( BoxesRunTime.boxToLong( controller.state().argumentRowId() ) );
      .MODULE$.ASM().log( "ASM %s take %03d", new ArgumentStateMapId( this.argumentStateMapId() ),
            BoxesRunTime.boxToLong( controller.state().argumentRowId() ) );
        return controller.state();
    }

    public ArgumentStateMap.ArgumentStateWithCompleted<STATE> takeNextIfCompletedOrElsePeek()
    {
        AbstractArgumentStateMap.StateController controller =
                (AbstractArgumentStateMap.StateController) this.controllers().get( BoxesRunTime.boxToLong( this.lastCompletedArgumentId() + 1L ) );
        ArgumentStateMap.ArgumentStateWithCompleted var10000;
        if ( controller != null )
        {
            if ( controller.tryTake() )
            {
                this.lastCompletedArgumentId_$eq( this.lastCompletedArgumentId() + 1L );
                this.controllers().remove( BoxesRunTime.boxToLong( controller.state().argumentRowId() ) );
                var10000 = new ArgumentStateMap.ArgumentStateWithCompleted( controller.state(), true );
            }
            else
            {
                var10000 = new ArgumentStateMap.ArgumentStateWithCompleted( controller.state(), false );
            }
        }
        else
        {
            var10000 = null;
        }

        return var10000;
    }

    public boolean nextArgumentStateIsCompletedOr( final Function1<STATE,Object> statePredicate )
    {
        AbstractArgumentStateMap.StateController controller =
                (AbstractArgumentStateMap.StateController) this.controllers().get( BoxesRunTime.boxToLong( this.lastCompletedArgumentId() + 1L ) );
        return controller != null && (controller.isZero() || BoxesRunTime.unboxToBoolean( statePredicate.apply( controller.state() ) ));
    }

    public STATE peekNext()
    {
        return this.peek( this.lastCompletedArgumentId() + 1L );
    }

    public scala.collection.Iterator<STATE> peekCompleted()
    {
        return (scala.collection.Iterator) scala.collection.JavaConverters..
        MODULE$.asScalaIteratorConverter( this.controllers().values().stream().filter( ( x$2 ) -> {
            return x$2.isZero();
        } ).map( ( x$3 ) -> {
            return x$3.state();
        } ).iterator() ).asScala();
    }

    public STATE peek( final long argumentId )
    {
        AbstractArgumentStateMap.StateController controller =
                (AbstractArgumentStateMap.StateController) this.controllers().get( BoxesRunTime.boxToLong( argumentId ) );
        return controller != null ? controller.state() : null;
    }

    public boolean hasCompleted()
    {
        Iterator iterator = this.controllers().values().iterator();

        AbstractArgumentStateMap.StateController controller;
        do
        {
            if ( !iterator.hasNext() )
            {
                return false;
            }

            controller = (AbstractArgumentStateMap.StateController) iterator.next();
        }
        while ( !controller.isZero() );

        return true;
    }

    public boolean hasCompleted( final long argument )
    {
        AbstractArgumentStateMap.StateController controller =
                (AbstractArgumentStateMap.StateController) this.controllers().get( BoxesRunTime.boxToLong( argument ) );
        return controller != null && controller.isZero();
    }

    public boolean remove( final long argument )
    {
      .MODULE$.ASM().log( "ASM %s rem %03d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ) );
        return this.controllers().remove( BoxesRunTime.boxToLong( argument ) ) != null;
    }

    public void initiate( final long argument, final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers )
    {
      .MODULE$.ASM().log( "ASM %s init %03d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ) );
        AbstractArgumentStateMap.StateController newController = this.newStateController( argument, argumentMorsel, argumentRowIdsForReducers );
        AbstractArgumentStateMap.StateController previousValue =
                (AbstractArgumentStateMap.StateController) this.controllers().put( BoxesRunTime.boxToLong( argument ), newController );
        Preconditions.checkState( previousValue == null, "ArgumentStateMap cannot re-initiate the same argument (argument: %d)",
                new Object[]{BoxesRunTime.boxToLong( argument )} );
    }

    public void increment( final long argument )
    {
        AbstractArgumentStateMap.StateController controller =
                (AbstractArgumentStateMap.StateController) this.controllers().get( BoxesRunTime.boxToLong( argument ) );
        long newCount = controller.increment();
      .MODULE$.ASM().log( "ASM %s incr %03d to %d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ),
            BoxesRunTime.boxToLong( newCount ) );
    }

    public STATE decrement( final long argument )
    {
        AbstractArgumentStateMap.StateController controller =
                (AbstractArgumentStateMap.StateController) this.controllers().get( BoxesRunTime.boxToLong( argument ) );
        long newCount = controller.decrement();
      .MODULE$.ASM().log( "ASM %s decr %03d to %d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ),
            BoxesRunTime.boxToLong( newCount ) );
        return newCount == 0L ? controller.state() : null;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.$plus$plus$eq( "ArgumentStateMap(\n" );
        this.controllers().forEach( ( argumentRowId, controller ) -> {
            $anonfun$toString$1( sb, BoxesRunTime.unboxToLong( argumentRowId ), controller );
        } );
        sb.$plus$eq( ')' );
        return sb.result();
    }

    public interface StateController<STATE extends ArgumentStateMap.ArgumentState>
    {
        STATE state();

        long increment();

        long decrement();

        boolean isZero();

        boolean take();

        boolean tryTake();
    }

    public static class ImmutableStateController<STATE extends ArgumentStateMap.ArgumentState> implements AbstractArgumentStateMap.StateController<STATE>
    {
        private final STATE state;

        public ImmutableStateController( final STATE state )
        {
            this.state = state;
        }

        public STATE state()
        {
            return this.state;
        }

        public long increment()
        {
            throw new IllegalStateException(
                    (new java.lang.StringBuilder( 14 )).append( "Cannot mutate " ).append( this.getClass().getSimpleName() ).toString() );
        }

        public long decrement()
        {
            throw new IllegalStateException(
                    (new java.lang.StringBuilder( 14 )).append( "Cannot mutate " ).append( this.getClass().getSimpleName() ).toString() );
        }

        public boolean tryTake()
        {
            throw new IllegalStateException(
                    (new java.lang.StringBuilder( 14 )).append( "Cannot mutate " ).append( this.getClass().getSimpleName() ).toString() );
        }

        public boolean take()
        {
            throw new IllegalStateException(
                    (new java.lang.StringBuilder( 14 )).append( "Cannot mutate " ).append( this.getClass().getSimpleName() ).toString() );
        }

        public boolean isZero()
        {
            return true;
        }

        public String toString()
        {
            return (new java.lang.StringBuilder( 20 )).append( "[immutable, state: " ).append( this.state() ).append( "]" ).toString();
        }
    }
}
