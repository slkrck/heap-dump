package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Iterator;

import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class ConcurrentOptionalBuffer<T> implements Buffer<T>, OptionalBuffer
{
    private final Buffer<T> inner;
    private volatile boolean _didReceiveData;

    public ConcurrentOptionalBuffer( final Buffer<T> inner )
    {
        this.inner = inner;
        this._didReceiveData = false;
    }

    private boolean _didReceiveData()
    {
        return this._didReceiveData;
    }

    private void _didReceiveData_$eq( final boolean x$1 )
    {
        this._didReceiveData = x$1;
    }

    public void put( final T t )
    {
        this._didReceiveData_$eq( true );
        this.inner.put( t );
    }

    public boolean didReceiveData()
    {
        return this._didReceiveData();
    }

    public void foreach( final Function1<T,BoxedUnit> f )
    {
        this.inner.foreach( f );
    }

    public boolean hasData()
    {
        return this.inner.hasData();
    }

    public T take()
    {
        return this.inner.take();
    }

    public boolean canPut()
    {
        return this.inner.canPut();
    }

    public Iterator<T> iterator()
    {
        return this.inner.iterator();
    }
}
