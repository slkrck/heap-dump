package org.neo4j.cypher.internal.runtime.pipelined.state;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public class QueryStatus
{
    private volatile boolean cancelled = false;

    public boolean cancelled()
    {
        return this.cancelled;
    }

    public void cancelled_$eq( final boolean x$1 )
    {
        this.cancelled = x$1;
    }
}
