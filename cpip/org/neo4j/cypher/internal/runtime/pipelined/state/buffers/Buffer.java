package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Iterator;

import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public interface Buffer<T> extends Sink<T>, Source<T>
{
    static int MAX_SIZE_HINT()
    {
        return Buffer$.MODULE$.MAX_SIZE_HINT();
    }

    void foreach( final Function1<T,BoxedUnit> f );

    Iterator<T> iterator();
}
