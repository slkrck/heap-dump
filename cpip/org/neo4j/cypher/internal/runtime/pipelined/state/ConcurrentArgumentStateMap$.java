package org.neo4j.cypher.internal.runtime.pipelined.state;

public final class ConcurrentArgumentStateMap$
{
    public static ConcurrentArgumentStateMap$ MODULE$;

    static
    {
        new ConcurrentArgumentStateMap$();
    }

    private final int org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentArgumentStateMap$$TAKEN;

    private ConcurrentArgumentStateMap$()
    {
        MODULE$ = this;
        this.org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentArgumentStateMap$$TAKEN = -1000000;
    }

    public int org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentArgumentStateMap$$TAKEN()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentArgumentStateMap$$TAKEN;
    }
}
