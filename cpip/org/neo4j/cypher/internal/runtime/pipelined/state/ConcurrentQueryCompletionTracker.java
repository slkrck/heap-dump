package org.neo4j.cypher.internal.runtime.pipelined.state;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.cypher.internal.v4_0.util.AssertionRunner.Thunk;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.internal.kernel.api.exceptions.LocksNotFrozenException;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class ConcurrentQueryCompletionTracker implements QueryCompletionTracker
{
    private final QuerySubscriber subscriber;
    private final QueryContext queryContext;
    private final QueryExecutionTracer tracer;
    private final AtomicLong count;
    private final ConcurrentLinkedQueue<Throwable> errors;
    private final AtomicLong requested;
    private final AtomicLong served;
    private final ConcurrentLinkedQueue<ConcurrentQueryCompletionTracker.WaitState> waiters;
    private final ArrayBuffer<Thunk> assertions;
    private final String org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName;
    private volatile ConcurrentQueryCompletionTracker.WaitState$ WaitState$module;
    private volatile boolean _cancelledOrFailed;
    private volatile boolean _hasEnded;

    public ConcurrentQueryCompletionTracker( final QuerySubscriber subscriber, final QueryContext queryContext, final QueryExecutionTracer tracer )
    {
        this.subscriber = subscriber;
        this.queryContext = queryContext;
        this.tracer = tracer;
        QueryCompletionTracker.$init$( this );
        this.count = new AtomicLong( 0L );
        this.errors = new ConcurrentLinkedQueue();
        this._cancelledOrFailed = false;
        this._hasEnded = false;
        this.requested = new AtomicLong( 0L );
        this.served = new AtomicLong( 0L );
        this.waiters = new ConcurrentLinkedQueue();
    }

    public void addCompletionAssertion( final Thunk assertion )
    {
        QueryCompletionTracker.addCompletionAssertion$( this, assertion );
    }

    public void runAssertions()
    {
        QueryCompletionTracker.runAssertions$( this );
    }

    public void debug( final String str )
    {
        QueryCompletionTracker.debug$( this, str );
    }

    public void debug( final String str, final Object x )
    {
        QueryCompletionTracker.debug$( this, str, x );
    }

    public void debug( final String str, final Object x1, final Object x2 )
    {
        QueryCompletionTracker.debug$( this, str, x1, x2 );
    }

    public void debug( final String str, final Object x1, final Object x2, final Object x3 )
    {
        QueryCompletionTracker.debug$( this, str, x1, x2, x3 );
    }

    public ConcurrentQueryCompletionTracker.WaitState$ WaitState()
    {
        if ( this.WaitState$module == null )
        {
            this.WaitState$lzycompute$1();
        }

        return this.WaitState$module;
    }

    public ArrayBuffer<Thunk> assertions()
    {
        return this.assertions;
    }

    public String org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName;
    }

    public void org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$assertions_$eq( final ArrayBuffer<Thunk> x$1 )
    {
        this.assertions = x$1;
    }

    public final void org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName_$eq(
            final String x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName = x$1;
    }

    private AtomicLong count()
    {
        return this.count;
    }

    private ConcurrentLinkedQueue<Throwable> errors()
    {
        return this.errors;
    }

    private boolean _cancelledOrFailed()
    {
        return this._cancelledOrFailed;
    }

    private void _cancelledOrFailed_$eq( final boolean x$1 )
    {
        this._cancelledOrFailed = x$1;
    }

    private boolean _hasEnded()
    {
        return this._hasEnded;
    }

    private void _hasEnded_$eq( final boolean x$1 )
    {
        this._hasEnded = x$1;
    }

    private AtomicLong requested()
    {
        return this.requested;
    }

    private AtomicLong served()
    {
        return this.served;
    }

    private ConcurrentLinkedQueue<ConcurrentQueryCompletionTracker.WaitState> waiters()
    {
        return this.waiters;
    }

    public String toString()
    {
        return (new StringBuilder( 37 )).append( "[ConcurrentQueryCompletionTracker@" ).append( System.identityHashCode( this ) ).append( "](" ).append(
                this.count().get() ).append( ")" ).toString();
    }

    public void increment()
    {
        if ( this._hasEnded() )
        {
            throw new ReferenceCountingException(
                    (new StringBuilder( 85 )).append( "Increment called even though query has ended. That should not happen. Current count: " ).append(
                            this.count().get() ).toString() );
        }
        else
        {
            long newCount = this.count().incrementAndGet();
            this.debug( "Increment to %d", BoxesRunTime.boxToLong( newCount ) );
        }
    }

    public void incrementBy( final long n )
    {
        if ( n != 0L )
        {
            long newCount = this.count().addAndGet( n );
            this.debug( "Increment by %d to %d", BoxesRunTime.boxToLong( n ), BoxesRunTime.boxToLong( newCount ) );
        }
    }

    public void decrement()
    {
        long newCount = this.count().decrementAndGet();
        this.debug( "Decrement to %d", BoxesRunTime.boxToLong( newCount ) );
        this.postDecrement( newCount );
    }

    public void decrementBy( final long n )
    {
        if ( n != 0L )
        {
            long newCount = this.count().addAndGet( -n );
            this.debug( "Decrement by %d to %d", BoxesRunTime.boxToLong( n ), BoxesRunTime.boxToLong( newCount ) );
            this.postDecrement( newCount );
        }
    }

    private void postDecrement( final long newCount )
    {
        if ( newCount <= 0L )
        {
            if ( newCount < 0L )
            {
                BoxesRunTime.boxToBoolean( this.errors().add( new ReferenceCountingException(
                        (new StringBuilder( 36 )).append( "Cannot count below 0, but got count " ).append( newCount ).toString() ) ) );
            }
            else
            {
                BoxedUnit var10000 = BoxedUnit.UNIT;
            }

            if ( !this._hasEnded() )
            {
                this.reportQueryEndToSubscriber();
                this.thawTransactionLocks();
                this._hasEnded_$eq( true );
                this.waiters().forEach( ( waitState ) -> {
                    waitState.latch().countDown();
                } );
                this.waiters().clear();
                this.tracer.stopQuery();
            }
        }
    }

    private void thawTransactionLocks()
    {
        try
        {
            this.queryContext.transactionalContext().transaction().thawLocks();
        }
        catch ( LocksNotFrozenException var2 )
        {
        }
        catch ( Exception var3 )
        {
            this.errors().add( var3 );
        }
    }

    private void reportQueryEndToSubscriber()
    {
        try
        {
            if ( !this.errors().isEmpty() )
            {
                this.subscriber.onError( this.allErrors() );
            }
            else if ( !this._cancelledOrFailed() )
            {
                this.subscriber.onResultCompleted( (QueryStatistics) this.queryContext.getOptStatistics().getOrElse( () -> {
                    return new org.neo4j.cypher.internal.runtime.QueryStatistics( org.neo4j.cypher.internal.runtime.QueryStatistics..MODULE$.apply$default$1(),
                    org.neo4j.cypher.internal.runtime.QueryStatistics..MODULE$.apply$default$2(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$3(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$4(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$5(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$6(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$7(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$8(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$9(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$10(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$11(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$12(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$13(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$14(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$15(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                    MODULE$.apply$default$16(), org.neo4j.cypher.internal.runtime.QueryStatistics..MODULE$.apply$default$17());
                } ) );
            }
        }
        catch ( Exception var2 )
        {
            this.errors().add( var2 );
        }
    }

    public void error( final Throwable throwable )
    {
        this.errors().add( throwable );
        this._cancelledOrFailed_$eq( true );
    }

    public boolean hasEnded()
    {
        return this._hasEnded();
    }

    public long getDemand()
    {
        return this._cancelledOrFailed() ? 0L : this.requested().get() - this.served().get();
    }

    public boolean hasDemand()
    {
        return this.getDemand() > 0L;
    }

    public void addServed( final long newlyServed )
    {
        this.debug( "Served %d rows", BoxesRunTime.boxToLong( newlyServed ) );
        long newServed = this.served().addAndGet( newlyServed );

        for ( ConcurrentQueryCompletionTracker.WaitState waitState = (ConcurrentQueryCompletionTracker.WaitState) this.waiters().peek();
                waitState != null && waitState.requestedRows() <= newServed; waitState = (ConcurrentQueryCompletionTracker.WaitState) this.waiters().peek() )
        {
            this.debug( "Releasing latch %s", waitState.latch() );
            waitState.latch().countDown();
            this.waiters().poll();
        }
    }

    public void cancel()
    {
        this._cancelledOrFailed_$eq( true );
        this.debug( "Cancelled" );
    }

    public void request( final long numberOfRecords )
    {
        if ( numberOfRecords > 0L )
        {
            this.debug( "Request %d rows", BoxesRunTime.boxToLong( numberOfRecords ) );
            this.requested().accumulateAndGet( numberOfRecords, ( oldVal, newVal ) -> {
                long newDemand = oldVal + newVal;
                return newDemand < 0L ? Long.MAX_VALUE : newDemand;
            } );
        }
    }

    public boolean await()
    {
        boolean var10000;
        if ( this.hasEnded() )
        {
            this.debug( "Awaiting query which has ended" );
            var10000 = false;
        }
        else
        {
            long currentRequested = this.requested().get();
            long currentServed = this.served().get();
            if ( currentRequested > currentServed )
            {
                CountDownLatch latch = new CountDownLatch( 1 );
                this.waiters().offer( new ConcurrentQueryCompletionTracker.WaitState( this, latch, currentRequested ) );
                if ( this.served().get() >= currentRequested || this.hasEnded() )
                {
                    latch.countDown();
                }

                this.debug( "Awaiting latch %s ....", latch );
                latch.await();
                this.debug( "Awaiting latch %s done", latch );
            }

            var10000 = !this.hasEnded();
        }

        boolean moreToCome = var10000;
        if ( !this.errors().isEmpty() )
        {
            throw this.allErrors();
        }
        else
        {
            if ( !moreToCome )
            {
                this.runAssertions();
            }

            return moreToCome;
        }
    }

    private Throwable allErrors()
    {
        Throwable first = (Throwable) this.errors().peek();
        this.errors().forEach( ( t ) -> {
            if ( t == null )
            {
                if ( first == null )
                {
                    return;
                }
            }
            else if ( t.equals( first ) )
            {
                return;
            }

            first.addSuppressed( t );
        } );
        return first;
    }

    private final void WaitState$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.WaitState$module == null )
            {
                this.WaitState$module = new ConcurrentQueryCompletionTracker.WaitState$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class WaitState implements Product, Serializable
    {
        private final CountDownLatch latch;
        private final long requestedRows;

        public WaitState( final ConcurrentQueryCompletionTracker $outer, final CountDownLatch latch, final long requestedRows )
        {
            this.latch = latch;
            this.requestedRows = requestedRows;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
            }
        }

        public CountDownLatch latch()
        {
            return this.latch;
        }

        public long requestedRows()
        {
            return this.requestedRows;
        }

        public ConcurrentQueryCompletionTracker.WaitState copy( final CountDownLatch latch, final long requestedRows )
        {
            return this.org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentQueryCompletionTracker$WaitState$$$outer().new WaitState(
                    this.org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentQueryCompletionTracker$WaitState$$$outer(), latch, requestedRows );
        }

        public CountDownLatch copy$default$1()
        {
            return this.latch();
        }

        public long copy$default$2()
        {
            return this.requestedRows();
        }

        public String productPrefix()
        {
            return "WaitState";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.latch();
                break;
            case 1:
                var10000 = BoxesRunTime.boxToLong( this.requestedRows() );
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof ConcurrentQueryCompletionTracker.WaitState;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.anyHash( this.latch() ) );
            var1 = Statics.mix( var1, Statics.longHash( this.requestedRows() ) );
            return Statics.finalizeHash( var1, 2 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            if ( this != x$1 )
            {
                label60:
                {
                    boolean var2;
                    if ( x$1 instanceof ConcurrentQueryCompletionTracker.WaitState &&
                            ((ConcurrentQueryCompletionTracker.WaitState) x$1).org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentQueryCompletionTracker$WaitState$$$outer() ==
                                    this.org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentQueryCompletionTracker$WaitState$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label37:
                        {
                            label36:
                            {
                                ConcurrentQueryCompletionTracker.WaitState var4 = (ConcurrentQueryCompletionTracker.WaitState) x$1;
                                CountDownLatch var10000 = this.latch();
                                CountDownLatch var5 = var4.latch();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label36;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label36;
                                }

                                if ( this.requestedRows() == var4.requestedRows() && var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label37;
                                }
                            }

                            var6 = false;
                        }

                        if ( var6 )
                        {
                            break label60;
                        }
                    }

                    var6 = false;
                    return var6;
                }
            }

            var6 = true;
            return var6;
        }
    }

    public class WaitState$ extends AbstractFunction2<CountDownLatch,Object,ConcurrentQueryCompletionTracker.WaitState> implements Serializable
    {
        public WaitState$( final ConcurrentQueryCompletionTracker $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public final String toString()
        {
            return "WaitState";
        }

        public ConcurrentQueryCompletionTracker.WaitState apply( final CountDownLatch latch, final long requestedRows )
        {
            return this.$outer.new WaitState( this.$outer, latch, requestedRows );
        }

        public Option<Tuple2<CountDownLatch,Object>> unapply( final ConcurrentQueryCompletionTracker.WaitState x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.latch(), BoxesRunTime.boxToLong( x$0.requestedRows() ) ) ));
        }
    }
}
