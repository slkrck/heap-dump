package org.neo4j.cypher.internal.runtime.pipelined.state;

import java.util.LinkedHashMap;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class StandardArgumentStateMap<STATE extends ArgumentStateMap.ArgumentState>
        extends AbstractArgumentStateMap<STATE,AbstractArgumentStateMap.StateController<STATE>>
{
    private final int argumentStateMapId;
    private final int argumentSlotOffset;
    private final ArgumentStateMap.ArgumentStateFactory<STATE> factory;
    private final LinkedHashMap<Object,AbstractArgumentStateMap.StateController<STATE>> controllers;
    private long lastCompletedArgumentId;

    public StandardArgumentStateMap( final int argumentStateMapId, final int argumentSlotOffset, final ArgumentStateMap.ArgumentStateFactory<STATE> factory )
    {
        this.argumentStateMapId = argumentStateMapId;
        this.argumentSlotOffset = argumentSlotOffset;
        this.factory = factory;
        this.controllers = new LinkedHashMap();
        this.lastCompletedArgumentId = -1L;
    }

    public int argumentStateMapId()
    {
        return this.argumentStateMapId;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public LinkedHashMap<Object,AbstractArgumentStateMap.StateController<STATE>> controllers()
    {
        return this.controllers;
    }

    public long lastCompletedArgumentId()
    {
        return this.lastCompletedArgumentId;
    }

    public void lastCompletedArgumentId_$eq( final long x$1 )
    {
        this.lastCompletedArgumentId = x$1;
    }

    public AbstractArgumentStateMap.StateController<STATE> newStateController( final long argument, final MorselExecutionContext argumentMorsel,
            final long[] argumentRowIdsForReducers )
    {
        return (AbstractArgumentStateMap.StateController) (this.factory.completeOnConstruction() ? new AbstractArgumentStateMap.ImmutableStateController(
                this.factory.newStandardArgumentState( argument, argumentMorsel, argumentRowIdsForReducers ) )
                                                                                                 : new StandardArgumentStateMap.StandardStateController(
                                                                                                         this.factory.newStandardArgumentState( argument,
                                                                                                                 argumentMorsel, argumentRowIdsForReducers ) ));
    }

    public static class StandardStateController<STATE extends ArgumentStateMap.ArgumentState> implements AbstractArgumentStateMap.StateController<STATE>
    {
        private final STATE state;
        private long _count;

        public StandardStateController( final STATE state )
        {
            this.state = state;
            this._count = 1L;
        }

        public STATE state()
        {
            return this.state;
        }

        private long _count()
        {
            return this._count;
        }

        private void _count_$eq( final long x$1 )
        {
            this._count = x$1;
        }

        public boolean isZero()
        {
            return this._count() == 0L;
        }

        public long increment()
        {
            this._count_$eq( this._count() + 1L );
            return this._count();
        }

        public long decrement()
        {
            this._count_$eq( this._count() - 1L );
            return this._count();
        }

        public boolean tryTake()
        {
            return this.isZero();
        }

        public boolean take()
        {
            return true;
        }

        public String toString()
        {
            return (new StringBuilder( 18 )).append( "[count: " ).append( this._count() ).append( ", state: " ).append( this.state() ).append( "]" ).toString();
        }
    }
}
