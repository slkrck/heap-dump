package org.neo4j.cypher.internal.runtime.pipelined.state;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ArgumentStateMapWithoutArgumentIdCounter<S extends ArgumentStateMap.ArgumentState> extends ArgumentStateMap<S>
{
    S takeOneCompleted();
}
