package org.neo4j.cypher.internal.runtime.pipelined.state;

import java.util.Arrays;

import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import scala.Function1;
import scala.Function2;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class ArgumentCountUpdater
{
    public abstract ArgumentStateMap.ArgumentStateMaps argumentStateMaps();

    private void morselLoop( final IndexedSeq<Buffers.AccumulatingBuffer> downstreamAccumulatingBuffers, final MorselExecutionContext morsel,
            final Function2<Buffers.AccumulatingBuffer,Object,BoxedUnit> operation )
    {
        int originalRow = morsel.getCurrentRow();
        long[] lastSeenRowIds = new long[downstreamAccumulatingBuffers.size()];
        Arrays.fill( lastSeenRowIds, -1L );
        int i = false;
        morsel.resetToFirstRow();

        while ( morsel.isValidRow() )
        {
            for ( int i = 0; i < downstreamAccumulatingBuffers.length(); ++i )
            {
                Buffers.AccumulatingBuffer buffer = (Buffers.AccumulatingBuffer) downstreamAccumulatingBuffers.apply( i );
                long currentRowId = morsel.getArgumentAt( buffer.argumentSlotOffset() );
                if ( currentRowId != lastSeenRowIds[i] )
                {
                    operation.apply( buffer, BoxesRunTime.boxToLong( currentRowId ) );
                    lastSeenRowIds[i] = currentRowId;
                }
            }

            morsel.moveToNextRow();
        }

        morsel.setCurrentRow( originalRow );
    }

    private <T> void downstreamLoop( final IndexedSeq<T> downstreamAccumulatingBuffers, final MorselExecutionContext morsel,
            final Function1<T,BoxedUnit> operation )
    {
        for ( int i = 0; i < downstreamAccumulatingBuffers.length(); ++i )
        {
            Object buffer = downstreamAccumulatingBuffers.apply( i );
            operation.apply( buffer );
        }
    }

    private void argumentCountLoop( final IndexedSeq<Buffers.AccumulatingBuffer> downstreamAccumulatingBuffers, final IndexedSeq<Object> argumentRowIds,
            final Function2<Buffers.AccumulatingBuffer,Object,BoxedUnit> operation )
    {
        for ( int i = 0; i < downstreamAccumulatingBuffers.length(); ++i )
        {
            Buffers.AccumulatingBuffer buffer = (Buffers.AccumulatingBuffer) downstreamAccumulatingBuffers.apply( i );

            for ( int j = 0; j < argumentRowIds.size(); ++j )
            {
                operation.apply( buffer, argumentRowIds.apply( j ) );
            }
        }
    }

    public void initiateArgumentStatesHere( final IndexedSeq<ArgumentStateMapId> argumentStates, final long argumentRowId, final MorselExecutionContext morsel )
    {
        this.downstreamLoop( argumentStates, morsel, ( id ) -> {
            $anonfun$initiateArgumentStatesHere$1( this, argumentRowId, morsel, ((ArgumentStateMapId) id).x() );
            return BoxedUnit.UNIT;
        } );
    }

    public void initiateArgumentReducersHere( final IndexedSeq<Buffers.AccumulatingBuffer> accumulatingBuffers, final long argumentRowId,
            final MorselExecutionContext morsel )
    {
        this.downstreamLoop( accumulatingBuffers, morsel, ( x$1 ) -> {
            $anonfun$initiateArgumentReducersHere$1( argumentRowId, morsel, x$1 );
            return BoxedUnit.UNIT;
        } );
    }

    public long[] forAllArgumentReducersAndGetArgumentRowIds( final IndexedSeq<Buffers.AccumulatingBuffer> accumulatingBuffers,
            final MorselExecutionContext morsel, final Function2<Buffers.AccumulatingBuffer,Object,BoxedUnit> fun )
    {
        long[] argumentRowIdsForReducers = new long[accumulatingBuffers.size()];

        for ( int i = 0; i < accumulatingBuffers.length(); ++i )
        {
            Buffers.AccumulatingBuffer reducer = (Buffers.AccumulatingBuffer) accumulatingBuffers.apply( i );
            int offset = reducer.argumentSlotOffset();
            long argumentRowIdForReducer = morsel.getArgumentAt( offset );
            argumentRowIdsForReducers[i] = argumentRowIdForReducer;
            fun.apply( reducer, BoxesRunTime.boxToLong( argumentRowIdForReducer ) );
        }

        return argumentRowIdsForReducers;
    }

    public void forAllArgumentReducers( final IndexedSeq<Buffers.AccumulatingBuffer> accumulatingBuffers, final long[] argumentRowIds,
            final Function2<Buffers.AccumulatingBuffer,Object,BoxedUnit> fun )
    {
        for ( int i = 0; i < accumulatingBuffers.length(); ++i )
        {
            Buffers.AccumulatingBuffer reducer = (Buffers.AccumulatingBuffer) accumulatingBuffers.apply( i );
            long argumentRowIdForReducer = argumentRowIds[i];
            fun.apply( reducer, BoxesRunTime.boxToLong( argumentRowIdForReducer ) );
        }
    }

    public void incrementArgumentCounts( final IndexedSeq<Buffers.AccumulatingBuffer> accumulatingBuffers, final MorselExecutionContext morsel )
    {
        this.morselLoop( accumulatingBuffers, morsel, ( x$2, x$3 ) -> {
            $anonfun$incrementArgumentCounts$1( x$2, BoxesRunTime.unboxToLong( x$3 ) );
            return BoxedUnit.UNIT;
        } );
    }

    public void decrementArgumentCounts( final IndexedSeq<Buffers.AccumulatingBuffer> accumulatingBuffers, final MorselExecutionContext morsel )
    {
        this.morselLoop( accumulatingBuffers, morsel, ( x$4, x$5 ) -> {
            $anonfun$decrementArgumentCounts$1( x$4, BoxesRunTime.unboxToLong( x$5 ) );
            return BoxedUnit.UNIT;
        } );
    }

    public void decrementArgumentCounts( final IndexedSeq<Buffers.AccumulatingBuffer> accumulatingBuffers, final IndexedSeq<Object> argumentRowIds )
    {
        this.argumentCountLoop( accumulatingBuffers, argumentRowIds, ( x$6, x$7 ) -> {
            $anonfun$decrementArgumentCounts$2( x$6, BoxesRunTime.unboxToLong( x$7 ) );
            return BoxedUnit.UNIT;
        } );
    }
}
