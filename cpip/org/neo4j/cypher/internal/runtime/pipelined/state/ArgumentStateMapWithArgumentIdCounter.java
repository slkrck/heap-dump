package org.neo4j.cypher.internal.runtime.pipelined.state;

import scala.Function1;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ArgumentStateMapWithArgumentIdCounter<S extends ArgumentStateMap.ArgumentState> extends ArgumentStateMap<S>
{
    ArgumentStateMap.ArgumentStateWithCompleted<S> takeNextIfCompletedOrElsePeek();

    boolean nextArgumentStateIsCompletedOr( final Function1<S,Object> statePredicate );

    S peekNext();
}
