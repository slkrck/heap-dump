package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Sink<T>
{
    void put( final T t );

    boolean canPut();
}
