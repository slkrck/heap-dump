package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class StandardSingletonArgumentStateMap<STATE extends ArgumentStateMap.ArgumentState>
        extends AbstractSingletonArgumentStateMap<STATE,AbstractArgumentStateMap.StateController<STATE>>
{
    private final int argumentStateMapId;
    private final ArgumentStateMap.ArgumentStateFactory<STATE> factory;
    private AbstractArgumentStateMap.StateController<STATE> controller;
    private boolean hasController;
    private long lastCompletedArgumentId;

    public StandardSingletonArgumentStateMap( final int argumentStateMapId, final ArgumentStateMap.ArgumentStateFactory<STATE> factory )
    {
        this.argumentStateMapId = argumentStateMapId;
        this.factory = factory;
        this.hasController = true;
        this.lastCompletedArgumentId = -1L;
    }

    public int argumentStateMapId()
    {
        return this.argumentStateMapId;
    }

    public AbstractArgumentStateMap.StateController<STATE> controller()
    {
        return this.controller;
    }

    public void controller_$eq( final AbstractArgumentStateMap.StateController<STATE> x$1 )
    {
        this.controller = x$1;
    }

    public boolean hasController()
    {
        return this.hasController;
    }

    public void hasController_$eq( final boolean x$1 )
    {
        this.hasController = x$1;
    }

    public long lastCompletedArgumentId()
    {
        return this.lastCompletedArgumentId;
    }

    public void lastCompletedArgumentId_$eq( final long x$1 )
    {
        this.lastCompletedArgumentId = x$1;
    }

    public AbstractArgumentStateMap.StateController<STATE> newStateController( final long argument, final MorselExecutionContext argumentMorsel,
            final long[] argumentRowIdsForReducers )
    {
        return (AbstractArgumentStateMap.StateController) (this.factory.completeOnConstruction() ? new AbstractArgumentStateMap.ImmutableStateController(
                this.factory.newStandardArgumentState( argument, argumentMorsel, argumentRowIdsForReducers ) )
                                                                                                 : new StandardArgumentStateMap.StandardStateController(
                                                                                                         this.factory.newStandardArgumentState( argument,
                                                                                                                 argumentMorsel, argumentRowIdsForReducers ) ));
    }
}
