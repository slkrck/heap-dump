package org.neo4j.cypher.internal.runtime.pipelined.state;

public final class StandardArgumentStateMap$
{
    public static StandardArgumentStateMap$ MODULE$;

    static
    {
        new StandardArgumentStateMap$();
    }

    private StandardArgumentStateMap$()
    {
        MODULE$ = this;
    }
}
