package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

import org.neo4j.internal.helpers.collection.Iterators;
import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class ConcurrentSingletonBuffer<T> implements SingletonBuffer<T>
{
    private final AtomicReference<T> datum = new AtomicReference();

    private AtomicReference<T> datum()
    {
        return this.datum;
    }

    public void put( final T t )
    {
        if ( !this.datum().compareAndSet( (Object) null, t ) )
        {
            throw new IllegalStateException(
                    (new StringBuilder( 64 )).append( "SingletonBuffer is full: tried to put " ).append( t ).append( " but already held element " ).append(
                            this.datum().get() ).toString() );
        }
    }

    public void tryPut( final T t )
    {
        this.datum().compareAndSet( (Object) null, t );
    }

    public boolean canPut()
    {
        return this.datum().get() == null;
    }

    public boolean hasData()
    {
        return this.datum().get() != null;
    }

    public T take()
    {
        return this.datum().getAndSet( (Object) null );
    }

    public void foreach( final Function1<T,BoxedUnit> f )
    {
        Object t = this.datum().get();
        if ( t != null )
        {
            f.apply( t );
        }
    }

    public String toString()
    {
        scala.collection.mutable.StringBuilder sb = new scala.collection.mutable.StringBuilder();
        sb.$plus$plus$eq( "ConcurrentSingletonBuffer(" );
        Object t = this.datum().get();
        if ( t != null )
        {
            sb.$plus$plus$eq( t.toString() );
        }
        else
        {
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        sb.$plus$eq( ')' );
        return sb.result();
    }

    public Iterator<T> iterator()
    {
        return Iterators.iterator( this.datum().get() );
    }
}
