package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SingletonBuffer<T> extends Buffer<T>
{
    void tryPut( final T t );
}
