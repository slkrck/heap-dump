package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.RuntimeResourceLeakException;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateDefinition;
import org.neo4j.cypher.internal.physicalplanning.BufferDefinition;
import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.physicalplanning.PipelineId$;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.CleanUpTask;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutionState;
import org.neo4j.cypher.internal.runtime.pipelined.PipelineState;
import org.neo4j.cypher.internal.runtime.pipelined.PipelineTask;
import org.neo4j.cypher.internal.runtime.pipelined.execution.AlarmSink;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.execution.WorkerWaker;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.LHSAccumulatingRHSStreamingBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.SingletonBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink;
import org.neo4j.cypher.internal.v4_0.util.AssertionRunner;
import org.neo4j.util.Preconditions;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.collection.Seq;
import scala.collection.immutable.StringOps;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class TheExecutionState implements ExecutionState
{
    private final ExecutionGraphDefinition executionGraphDefinition;
    private final Seq<ExecutablePipeline> pipelines;
    private final StateFactory stateFactory;
    private final WorkerWaker workerWaker;
    private final QueryState queryState;
    private final QueryCompletionTracker tracker;
    private final QueryStatus queryStatus;
    private final ArgumentStateMap<? extends ArgumentStateMap.ArgumentState>[] argumentStateMapHolder;
    private final ArgumentStateMap.ArgumentStateMaps argumentStateMaps;
    private final Buffers buffers;
    private final SingletonBuffer<CleanUpTask> cleanUpTaskHolder;
    private final PipelineState[] pipelineStates;
    private final Lock[] pipelineLocks;
    private final Buffer<PipelineTask>[] continuations;

    public TheExecutionState( final ExecutionGraphDefinition executionGraphDefinition, final Seq<ExecutablePipeline> pipelines, final StateFactory stateFactory,
            final WorkerWaker workerWaker, final QueryContext queryContext, final QueryState queryState, final QueryResources initializationResources,
            final QueryCompletionTracker tracker )
    {
        this.executionGraphDefinition = executionGraphDefinition;
        this.pipelines = pipelines;
        this.stateFactory = stateFactory;
        this.workerWaker = workerWaker;
        this.queryState = queryState;
        this.tracker = tracker;
        ExecutionState.$init$( this );
        this.verifyThatIdsAndOffsetsMatch();
        tracker.addCompletionAssertion( () -> {
            this.assertEmpty();
        } );
        this.queryStatus = new QueryStatus();
        this.argumentStateMapHolder = new ArgumentStateMap[executionGraphDefinition.argumentStateMaps().size()];
        this.argumentStateMaps = new ArgumentStateMap.ArgumentStateMaps( this )
        {
            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    ArgumentStateMap.ArgumentStateMaps.$init$( this );
                }
            }

            public ArgumentStateMap<? extends ArgumentStateMap.ArgumentState> applyByIntId( final int argumentStateMapId )
            {
                return ArgumentStateMap.ArgumentStateMaps.applyByIntId$( this, argumentStateMapId );
            }

            public final ArgumentStateMap<? extends ArgumentStateMap.ArgumentState> apply( final int argumentStateMapId )
            {
                return this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$TheExecutionState$$$anonfun$argumentStateMaps$1( argumentStateMapId );
            }
        };
        this.buffers = new Buffers( executionGraphDefinition.buffers().size(), tracker, this.argumentStateMaps(), stateFactory );
        this.cleanUpTaskHolder = stateFactory.newSingletonBuffer();
        PipelineState[] states = new PipelineState[pipelines.length()];

        for ( int i = states.length - 1; i >= 0; --i )
        {
            ExecutablePipeline pipeline = (ExecutablePipeline) pipelines.apply( i );
            pipeline.outputOperator().outputBuffer().foreach( ( bufferId ) -> {
                $anonfun$pipelineStates$1( this, ((BufferId) bufferId).x() );
                return BoxedUnit.UNIT;
            } );
            states[i] = pipeline.createState( this, queryContext, queryState, initializationResources, stateFactory );
            this.buffers().constructBuffer( pipeline.inputBuffer() );
        }

        this.buffers().constructBuffer( (BufferDefinition) executionGraphDefinition.buffers().head() );
        this.pipelineStates = states;
        Lock[] arr = new Lock[pipelines.length()];

        for ( int i = 0; i < arr.length; ++i )
        {
            ExecutablePipeline pipeline = (ExecutablePipeline) pipelines.apply( i );
            arr[i] = stateFactory.newLock( (new StringBuilder( 10 )).append( "Pipeline[" ).append( pipeline.id() ).append( "]" ).toString() );
        }

        this.pipelineLocks = arr;
        Buffer[] arr = new Buffer[pipelines.length()];

        for ( int i = 0; i < arr.length; ++i )
        {
            ExecutablePipeline pipeline = (ExecutablePipeline) pipelines.apply( i );
            arr[i] = (Buffer) (pipeline.serial() ? stateFactory.newSingletonBuffer() : stateFactory.newBuffer());
        }

        this.continuations = arr;
    }

    public <T> Sink<T> getSinkInt( final int fromPipeline, final int bufferId )
    {
        return ExecutionState.getSinkInt$( this, fromPipeline, bufferId );
    }

    private QueryStatus queryStatus()
    {
        return this.queryStatus;
    }

    private ArgumentStateMap<? extends ArgumentStateMap.ArgumentState>[] argumentStateMapHolder()
    {
        return this.argumentStateMapHolder;
    }

    public ArgumentStateMap.ArgumentStateMaps argumentStateMaps()
    {
        return this.argumentStateMaps;
    }

    private Buffers buffers()
    {
        return this.buffers;
    }

    private SingletonBuffer<CleanUpTask> cleanUpTaskHolder()
    {
        return this.cleanUpTaskHolder;
    }

    public PipelineState[] pipelineStates()
    {
        return this.pipelineStates;
    }

    private Lock[] pipelineLocks()
    {
        return this.pipelineLocks;
    }

    private Buffer<PipelineTask>[] continuations()
    {
        return this.continuations;
    }

    public void initializeState()
    {
        this.putMorsel( PipelineId$.MODULE$.NO_PIPELINE(), 0, MorselExecutionContext$.MODULE$.createInitialRow() );
    }

    public <T> Sink<T> getSink( final int fromPipeline, final int bufferId )
    {
        return new AlarmSink( this.buffers().sink( fromPipeline, bufferId ), this.workerWaker, this.queryStatus() );
    }

    public void putMorsel( final int fromPipeline, final int bufferId, final MorselExecutionContext output )
    {
        if ( !this.queryStatus().cancelled() )
        {
            this.buffers().sink( fromPipeline, bufferId ).put( output );
            this.workerWaker.wakeOne();
        }
        else
        {
         .MODULE$.ERROR_HANDLING().log( "Dropped morsel %s because of query cancellation", output );
        }
    }

    public MorselParallelizer takeMorsel( final int bufferId )
    {
        return this.buffers().morselBuffer( bufferId ).take();
    }

    public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> ACC takeAccumulator( final int bufferId )
    {
        return (ArgumentStateMap.MorselAccumulator) this.buffers().source( bufferId ).take();
    }

    public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> Buffers.AccumulatorAndMorsel<DATA,ACC> takeAccumulatorAndMorsel( final int bufferId )
    {
        return (Buffers.AccumulatorAndMorsel) this.buffers().source( bufferId ).take();
    }

    public <DATA> DATA takeData( final int bufferId )
    {
        return this.buffers().source( bufferId ).take();
    }

    public void closeWorkUnit( final ExecutablePipeline pipeline )
    {
        if ( pipeline.serial() )
        {
            this.pipelineLocks()[pipeline.id()].unlock();
        }
    }

    public void closeMorselTask( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel )
    {
        this.closeWorkUnit( pipeline );
        this.buffers().morselBuffer( pipeline.inputBuffer().id() ).close( inputMorsel );
    }

    public <DATA> void closeData( final ExecutablePipeline pipeline, final DATA data )
    {
        this.closeWorkUnit( pipeline );
        this.buffers().closingSource( pipeline.inputBuffer().id() ).close( data );
    }

    public void closeAccumulatorTask( final ExecutablePipeline pipeline, final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        this.closeWorkUnit( pipeline );
        this.buffers().argumentStateBuffer( pipeline.inputBuffer().id() ).close( accumulator );
    }

    public void closeMorselAndAccumulatorTask( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel,
            final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        this.closeWorkUnit( pipeline );
        LHSAccumulatingRHSStreamingBuffer buffer = this.buffers().lhsAccumulatingRhsStreamingBuffer( pipeline.inputBuffer().id() );
        buffer.close( accumulator, inputMorsel );
    }

    public boolean filterCancelledArguments( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel )
    {
        return this.buffers().morselBuffer( pipeline.inputBuffer().id() ).filterCancelledArguments( inputMorsel );
    }

    public boolean filterCancelledArguments( final ExecutablePipeline pipeline, final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        return this.buffers().argumentStateBuffer( pipeline.inputBuffer().id() ).filterCancelledArguments( accumulator );
    }

    public boolean filterCancelledArguments( final ExecutablePipeline pipeline, final MorselExecutionContext inputMorsel,
            final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        LHSAccumulatingRHSStreamingBuffer buffer = this.buffers().lhsAccumulatingRhsStreamingBuffer( pipeline.inputBuffer().id() );
        return buffer.filterCancelledArguments( accumulator, inputMorsel );
    }

    public void putContinuation( final PipelineTask task, final boolean wakeUp, final QueryResources resources )
    {
        if ( this.queryStatus().cancelled() )
        {
         .MODULE$.ERROR_HANDLING().log( "[putContinuation] Closing %s because of query cancellation", task );
            task.close( resources );
        }
        else
        {
         .MODULE$.BUFFERS().log( "[putContinuation]   %s <- %s", this, task );
            this.continuations()[task.pipelineState().pipeline().id()].put( task );
            if ( wakeUp && !task.pipelineState().pipeline().serial() )
            {
                this.workerWaker.wakeOne();
            }

            this.closeWorkUnit( task.pipelineState().pipeline() );
        }
    }

    public boolean canPut( final ExecutablePipeline pipeline )
    {
        Option var3 = pipeline.outputOperator().outputBuffer();
        boolean var2;
        if ( scala.None..MODULE$.equals( var3 )){
        var2 = true;
    } else{
        if ( !(var3 instanceof Some) )
        {
            throw new MatchError( var3 );
        }

        Some var4 = (Some) var3;
        int bufferId = ((BufferId) var4.value()).x();
        var2 = this.buffers().sink( pipeline.id(), bufferId ).canPut();
    }

        return var2;
    }

    public PipelineTask takeContinuation( final ExecutablePipeline pipeline )
    {
        return (PipelineTask) this.continuations()[pipeline.id()].take();
    }

    public boolean tryLock( final ExecutablePipeline pipeline )
    {
        return this.pipelineLocks()[pipeline.id()].tryLock();
    }

    public void unlock( final ExecutablePipeline pipeline )
    {
        this.pipelineLocks()[pipeline.id()].unlock();
    }

    public boolean canContinueOrTake( final ExecutablePipeline pipeline )
    {
        boolean hasWork = this.continuations()[pipeline.id()].hasData() || this.buffers().hasData( pipeline.inputBuffer().id() );
        return hasWork && this.queryState.flowControl().hasDemand();
    }

    public final <S extends ArgumentStateMap.ArgumentState> ArgumentStateMap<S> createArgumentStateMap( final int argumentStateMapId,
            final ArgumentStateMap.ArgumentStateFactory<S> factory )
    {
        int argumentSlotOffset = ((ArgumentStateDefinition) this.executionGraphDefinition.argumentStateMaps().apply( argumentStateMapId )).argumentSlotOffset();
        ArgumentStateMap asm = this.stateFactory.newArgumentStateMap( argumentStateMapId, argumentSlotOffset, factory );
        this.argumentStateMapHolder()[argumentStateMapId] = asm;
        return asm;
    }

    public void failQuery( final Throwable throwable, final QueryResources resources, final ExecutablePipeline failedPipeline )
    {
      .MODULE$.ERROR_HANDLING().log( "Starting ExecutionState.failQuery, because of %s", throwable );
        this.tracker.error( throwable );
        this.closeOutstandingWork( resources, failedPipeline );
    }

    public void cancelQuery( final QueryResources resources )
    {
        this.closeOutstandingWork( resources, (ExecutablePipeline) null );
    }

    public void scheduleCancelQuery()
    {
        this.cleanUpTaskHolder().tryPut( new CleanUpTask( this ) );
    }

    public CleanUpTask cleanUpTask()
    {
        return (CleanUpTask) this.cleanUpTaskHolder().take();
    }

    private void closeOutstandingWork( final QueryResources resources, final ExecutablePipeline failedPipeline )
    {
        this.queryStatus().cancelled_$eq( true );
        this.buffers().clearAll();
        this.pipelines.foreach( ( pipeline ) -> {
            $anonfun$closeOutstandingWork$1( this, resources, failedPipeline, pipeline );
            return BoxedUnit.UNIT;
        } );
    }

    public boolean hasEnded()
    {
        return this.tracker.hasEnded();
    }

    public String prettyString( final ExecutablePipeline pipeline )
    {
        return (new StringOps( scala.Predef..MODULE$.augmentString(
                (new StringBuilder( 24 )).append( "continuations: " ).append( this.continuations()[pipeline.id()] ).append( "\n       |" ).toString() ))).
        stripMargin();
    }

    private void assertEmpty()
    {
        this.buffers().assertAllEmpty();

        for ( int i = 0; i < this.continuations().length; ++i )
        {
            Buffer continuation = this.continuations()[i];
            if ( continuation.hasData() )
            {
                throw new RuntimeResourceLeakException( (new StringBuilder( 57 )).append( "Continuation buffer " ).append( continuation ).append(
                        " is not empty after query completion." ).toString() );
            }
        }
    }

    public String toString()
    {
        return "TheExecutionState";
    }

    private void verifyThatIdsAndOffsetsMatch()
    {
        if ( AssertionRunner.isAssertionsEnabled() )
        {
            int i;
            for ( i = 0; i < this.pipelines.length(); ++i )
            {
                Preconditions.checkState( i == ((ExecutablePipeline) this.pipelines.apply( i )).id(), "Pipeline id does not match offset!" );
            }

            for ( i = 0; i < this.executionGraphDefinition.buffers().size(); ++i )
            {
                Preconditions.checkState( i == ((BufferDefinition) this.executionGraphDefinition.buffers().apply( i )).id(),
                        "Buffer definition id does not match offset!" );
            }
        }
    }
}
