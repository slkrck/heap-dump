package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.pipelined.execution.FilteringMorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Function1;
import scala.Function2;
import scala.collection.IndexedSeq;
import scala.collection.mutable.ArrayBuffer;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class ArgumentStateMap$
{
    public static ArgumentStateMap$ MODULE$;

    static
    {
        new ArgumentStateMap$();
    }

    private ArgumentStateMap$()
    {
        MODULE$ = this;
    }

    public void filter( final MorselExecutionContext morsel, final Function1<MorselExecutionContext,Object> predicate )
    {
        int currentRow = morsel.getCurrentRow();
        FilteringMorselExecutionContext filteringMorsel = (FilteringMorselExecutionContext) morsel;
        if ( filteringMorsel.hasCancelledRows() )
        {
            this.filterLoop( filteringMorsel, predicate );
        }
        else
        {
            this.filterLoopRaw( filteringMorsel, predicate );
        }

        filteringMorsel.moveToRawRow( currentRow );
    }

    private void filterLoop( final FilteringMorselExecutionContext filteringMorsel, final Function1<MorselExecutionContext,Object> predicate )
    {
        filteringMorsel.resetToFirstRow();

        for ( ; filteringMorsel.isValidRow(); filteringMorsel.moveToNextRow() )
        {
            if ( !BoxesRunTime.unboxToBoolean( predicate.apply( filteringMorsel ) ) )
            {
                filteringMorsel.cancelCurrentRow();
            }
        }
    }

    private void filterLoopRaw( final FilteringMorselExecutionContext filteringMorsel, final Function1<MorselExecutionContext,Object> predicate )
    {
        filteringMorsel.resetToFirstRawRow();

        for ( ; filteringMorsel.isValidRawRow(); filteringMorsel.moveToNextRawRow() )
        {
            if ( !BoxesRunTime.unboxToBoolean( predicate.apply( filteringMorsel ) ) )
            {
                filteringMorsel.cancelCurrentRow();
            }
        }
    }

    public <FILTER_STATE> void filter( final int argumentSlotOffset, final MorselExecutionContext morsel,
            final Function2<Object,Object,FILTER_STATE> onArgument, final Function2<FILTER_STATE,MorselExecutionContext,Object> onRow )
    {
        int currentRow = morsel.getCurrentRow();
        FilteringMorselExecutionContext filteringMorsel = (FilteringMorselExecutionContext) morsel;
        if ( filteringMorsel.hasCancelledRows() )
        {
            this.filterLoop( argumentSlotOffset, filteringMorsel, onArgument, onRow );
        }
        else
        {
            this.filterLoopRaw( argumentSlotOffset, filteringMorsel, onArgument, onRow );
        }

        filteringMorsel.moveToRawRow( currentRow );
    }

    private <FILTER_STATE> void filterLoop( final int argumentSlotOffset, final FilteringMorselExecutionContext filteringMorsel,
            final Function2<Object,Object,FILTER_STATE> onArgument, final Function2<FILTER_STATE,MorselExecutionContext,Object> onRow )
    {
        filteringMorsel.resetToFirstRow();

        while ( filteringMorsel.isValidRow() )
        {
            long arg = filteringMorsel.getArgumentAt( argumentSlotOffset );
            int start = filteringMorsel.getCurrentRow();

            while ( filteringMorsel.isValidRow() && filteringMorsel.getArgumentAt( argumentSlotOffset ) == arg )
            {
                filteringMorsel.moveToNextRow();
            }

            int end = filteringMorsel.getCurrentRow();
            filteringMorsel.moveToRawRow( start );

            for ( Object filterState = onArgument.apply( BoxesRunTime.boxToLong( arg ), BoxesRunTime.boxToLong( (long) (end - start) ) );
                    filteringMorsel.getCurrentRow() < end; filteringMorsel.moveToNextRow() )
            {
                if ( !BoxesRunTime.unboxToBoolean( onRow.apply( filterState, filteringMorsel ) ) )
                {
                    filteringMorsel.cancelCurrentRow();
                }
            }
        }
    }

    private <FILTER_STATE> void filterLoopRaw( final int argumentSlotOffset, final FilteringMorselExecutionContext filteringMorsel,
            final Function2<Object,Object,FILTER_STATE> onArgument, final Function2<FILTER_STATE,MorselExecutionContext,Object> onRow )
    {
        filteringMorsel.resetToFirstRawRow();

        while ( filteringMorsel.isValidRawRow() )
        {
            long arg = filteringMorsel.getArgumentAt( argumentSlotOffset );
            int start = filteringMorsel.getCurrentRow();

            while ( filteringMorsel.isValidRawRow() && filteringMorsel.getArgumentAt( argumentSlotOffset ) == arg )
            {
                filteringMorsel.moveToNextRawRow();
            }

            int end = filteringMorsel.getCurrentRow();
            filteringMorsel.moveToRawRow( start );

            for ( Object filterState = onArgument.apply( BoxesRunTime.boxToLong( arg ), BoxesRunTime.boxToLong( (long) (end - start) ) );
                    filteringMorsel.getCurrentRow() < end; filteringMorsel.moveToNextRawRow() )
            {
                if ( !BoxesRunTime.unboxToBoolean( onRow.apply( filterState, filteringMorsel ) ) )
                {
                    filteringMorsel.cancelCurrentRow();
                }
            }
        }
    }

    public <T> IndexedSeq<ArgumentStateMap.PerArgument<T>> map( final int argumentSlotOffset, final MorselExecutionContext morsel,
            final Function1<MorselExecutionContext,T> f )
    {
        ArrayBuffer result = new ArrayBuffer();
        this.foreach( argumentSlotOffset, morsel, ( argumentRowId, view ) -> {
            $anonfun$map$1( f, result, BoxesRunTime.unboxToLong( argumentRowId ), view );
            return BoxedUnit.UNIT;
        } );
        return result;
    }

    public <T> void foreach( final int argumentSlotOffset, final MorselExecutionContext morsel, final Function2<Object,MorselExecutionContext,BoxedUnit> f )
    {
        MorselExecutionContext readingRow = morsel.shallowCopy();
        readingRow.resetToFirstRow();

        while ( readingRow.isValidRow() )
        {
            long arg = readingRow.getArgumentAt( argumentSlotOffset );
            int start = readingRow.getCurrentRow();

            while ( readingRow.isValidRow() && readingRow.getArgumentAt( argumentSlotOffset ) == arg )
            {
                readingRow.moveToNextRow();
            }

            int end = readingRow.getCurrentRow();
            MorselExecutionContext view = readingRow.view( start, end );
            f.apply( BoxesRunTime.boxToLong( arg ), view );
        }
    }
}
