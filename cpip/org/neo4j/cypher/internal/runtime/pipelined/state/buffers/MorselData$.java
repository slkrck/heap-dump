package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.runtime.AbstractFunction3;

public final class MorselData$ extends AbstractFunction3<IndexedSeq<MorselExecutionContext>,ArgumentStream,long[],MorselData> implements Serializable
{
    public static MorselData$ MODULE$;

    static
    {
        new MorselData$();
    }

    private MorselData$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselData";
    }

    public MorselData apply( final IndexedSeq<MorselExecutionContext> morsels, final ArgumentStream argumentStream, final long[] argumentRowIdsForReducers )
    {
        return new MorselData( morsels, argumentStream, argumentRowIdsForReducers );
    }

    public Option<Tuple3<IndexedSeq<MorselExecutionContext>,ArgumentStream,long[]>> unapply( final MorselData x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.morsels(), x$0.argumentStream(), x$0.argumentRowIdsForReducers() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
