package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.runtime.pipelined.PipelinedDebugSupport$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentCountUpdater;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

@JavaDocToJava
public class LHSAccumulatingRHSStreamingBuffer<DATA, LHS_ACC extends ArgumentStateMap.MorselAccumulator<DATA>> extends ArgumentCountUpdater
        implements Source<Buffers.AccumulatorAndMorsel<DATA,LHS_ACC>>, Buffers.SinkByOrigin, Buffers.DataHolder
{
    public final QueryCompletionTracker org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$tracker;
    public final IndexedSeq<Buffers.AccumulatingBuffer>
            org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$downstreamArgumentReducers;
    private final ArgumentStateMap.ArgumentStateMaps argumentStateMaps;
    private final int lhsArgumentStateMapId;
    private final int rhsArgumentStateMapId;
    private final int lhsPipelineId;
    private final int rhsPipelineId;
    private final ArgumentStateMap<LHS_ACC> org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap;
    private final ArgumentStateMap<ArgumentStateBuffer>
            org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap;
    private volatile LHSAccumulatingRHSStreamingBuffer<DATA,LHS_ACC>.LHSSink$ LHSSink$module;
    private volatile LHSAccumulatingRHSStreamingBuffer<DATA,LHS_ACC>.RHSSink$ RHSSink$module;

    public LHSAccumulatingRHSStreamingBuffer( final QueryCompletionTracker tracker, final IndexedSeq<Buffers.AccumulatingBuffer> downstreamArgumentReducers,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps, final int lhsArgumentStateMapId, final int rhsArgumentStateMapId,
            final int lhsPipelineId, final int rhsPipelineId, final StateFactory stateFactory )
    {
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$tracker = tracker;
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$downstreamArgumentReducers =
                downstreamArgumentReducers;
        this.argumentStateMaps = argumentStateMaps;
        this.lhsArgumentStateMapId = lhsArgumentStateMapId;
        this.rhsArgumentStateMapId = rhsArgumentStateMapId;
        this.lhsPipelineId = lhsPipelineId;
        this.rhsPipelineId = rhsPipelineId;
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap =
                argumentStateMaps.apply( lhsArgumentStateMapId );
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap =
                argumentStateMaps.apply( rhsArgumentStateMapId );
    }

    public LHSAccumulatingRHSStreamingBuffer<DATA,LHS_ACC>.LHSSink$ LHSSink()
    {
        if ( this.LHSSink$module == null )
        {
            this.LHSSink$lzycompute$1();
        }

        return this.LHSSink$module;
    }

    public LHSAccumulatingRHSStreamingBuffer<DATA,LHS_ACC>.RHSSink$ RHSSink()
    {
        if ( this.RHSSink$module == null )
        {
            this.RHSSink$lzycompute$1();
        }

        return this.RHSSink$module;
    }

    public ArgumentStateMap.ArgumentStateMaps argumentStateMaps()
    {
        return this.argumentStateMaps;
    }

    public int lhsArgumentStateMapId()
    {
        return this.lhsArgumentStateMapId;
    }

    public int rhsArgumentStateMapId()
    {
        return this.rhsArgumentStateMapId;
    }

    public ArgumentStateMap<LHS_ACC> org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap;
    }

    public ArgumentStateMap<ArgumentStateBuffer> org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap;
    }

    public <T> Sink<T> sinkFor( final int fromPipeline )
    {
        Object var10000;
        if ( fromPipeline == this.lhsPipelineId )
        {
            var10000 = this.LHSSink();
        }
        else
        {
            if ( fromPipeline != this.rhsPipelineId )
            {
                throw new IllegalStateException( "Requesting sink from unexpected pipeline." );
            }

            var10000 = this.RHSSink();
        }

        return (Sink) var10000;
    }

    public boolean hasData()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().peekCompleted().exists(
                ( lhsAcc ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$hasData$1( this, lhsAcc ) );
                } );
    }

    public Buffers.AccumulatorAndMorsel<DATA,LHS_ACC> take()
    {
        Iterator it = this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().peekCompleted();

        while ( it.hasNext() )
        {
            ArgumentStateMap.MorselAccumulator lhsAcc = (ArgumentStateMap.MorselAccumulator) it.next();
            ArgumentStateBuffer rhsBuffer =
                    (ArgumentStateBuffer) this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().peek(
                            lhsAcc.argumentRowId() );
            if ( rhsBuffer != null )
            {
                MorselExecutionContext rhsMorsel = rhsBuffer.take();
                if ( rhsMorsel != null )
                {
                    if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
                    org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().log(
                            (new StringBuilder( 14 )).append( "[take] " ).append( this ).append( " -> " ).append( lhsAcc ).append( " & " ).append(
                                    PipelinedDebugSupport$.MODULE$.prettyMorselWithHeader( "", rhsMorsel ).reduce( ( x$1, x$2 ) -> {
                                        return (new StringBuilder( 0 )).append( x$1 ).append( x$2 ).toString();
                                    } ) ).toString() );
                }

                    return new Buffers.AccumulatorAndMorsel( lhsAcc, rhsMorsel );
                }
            }
        }

        return null;
    }

    public boolean filterCancelledArguments( final ArgumentStateMap.MorselAccumulator<?> accumulator, final MorselExecutionContext rhsMorsel )
    {
        return false;
    }

    public void clearAll()
    {
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().clearAll( ( buffer ) -> {
            $anonfun$clearAll$1( this, buffer );
            return BoxedUnit.UNIT;
        } );
    }

    public void close( final ArgumentStateMap.MorselAccumulator<?> accumulator, final MorselExecutionContext rhsMorsel )
    {
        if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
        org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().log(
                (new StringBuilder( 16 )).append( "[close] " ).append( this ).append( " -X- " ).append( accumulator ).append( " & " ).append(
                        PipelinedDebugSupport$.MODULE$.prettyMorselWithHeader( "", rhsMorsel ).reduce( ( x$5, x$6 ) -> {
                            return (new StringBuilder( 0 )).append( x$5 ).append( x$6 ).toString();
                        } ) ).toString() );
    }

        BoxedUnit var10000;
        if ( this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().hasCompleted(
                accumulator.argumentRowId() ) )
        {
            ArgumentStateBuffer rhsAcc =
                    (ArgumentStateBuffer) this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().peek(
                            accumulator.argumentRowId() );
            if ( rhsAcc != null && !rhsAcc.hasData() &&
                    this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().remove(
                            accumulator.argumentRowId() ) )
            {
                BoxesRunTime.boxToBoolean(
                        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().remove(
                                accumulator.argumentRowId() ) );
            }
            else
            {
                var10000 = BoxedUnit.UNIT;
            }
        }
        else
        {
            var10000 = BoxedUnit.UNIT;
        }

        this.forAllArgumentReducers(
                this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$downstreamArgumentReducers,
                accumulator.argumentRowIdsForReducers(), ( x$7, x$8 ) -> {
                    $anonfun$close$2( x$7, BoxesRunTime.unboxToLong( x$8 ) );
                    return BoxedUnit.UNIT;
                } );
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$tracker.decrement();
    }

    private final void LHSSink$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.LHSSink$module == null )
            {
                this.LHSSink$module = new LHSAccumulatingRHSStreamingBuffer.LHSSink$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    private final void RHSSink$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.RHSSink$module == null )
            {
                this.RHSSink$module = new LHSAccumulatingRHSStreamingBuffer.RHSSink$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class LHSSink$ implements Sink<IndexedSeq<ArgumentStateMap.PerArgument<DATA>>>, Buffers.AccumulatingBuffer
    {
        private final int argumentSlotOffset;

        public LHSSink$( final LHSAccumulatingRHSStreamingBuffer<DATA,LHS_ACC> $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                this.argumentSlotOffset =
                        $outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().argumentSlotOffset();
            }
        }

        public int argumentSlotOffset()
        {
            return this.argumentSlotOffset;
        }

        public void put( final IndexedSeq<ArgumentStateMap.PerArgument<DATA>> data )
        {
            if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
            org.neo4j.cypher.internal.runtime.debug.DebugSupport..
            MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[put]   " ).append( this ).append( " <- " ).append( data.mkString( ", " ) ).toString() );
        }

            for ( IntRef i = IntRef.create( 0 ); i.elem < data.length(); ++i.elem )
            {
                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().update(
                        ((ArgumentStateMap.PerArgument) data.apply( i.elem )).argumentRowId(), ( acc ) -> {
                            $anonfun$put$1( data, i, acc );
                            return BoxedUnit.UNIT;
                        } );
            }
        }

        public boolean canPut()
        {
            return true;
        }

        public void initiate( final long argumentRowId, final MorselExecutionContext argumentMorsel )
        {
            if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
            org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().log(
                    (new StringBuilder( 32 )).append( "[init]  " ).append( this ).append( " <- argumentRowId=" ).append( argumentRowId ).append(
                            " from " ).append( argumentMorsel ).toString() );
        }

            long[] argumentRowIdsForReducers = this.$outer.forAllArgumentReducersAndGetArgumentRowIds(
                    this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$downstreamArgumentReducers,
                    argumentMorsel, ( x$9, x$10 ) -> {
                        $anonfun$initiate$1( x$9, BoxesRunTime.unboxToLong( x$10 ) );
                        return BoxedUnit.UNIT;
                    } );
            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().initiate(
                    argumentRowId, argumentMorsel, argumentRowIdsForReducers );
        }

        public void increment( final long argumentRowId )
        {
            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().increment(
                    argumentRowId );
        }

        public void decrement( final long argumentRowId )
        {
            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$lhsArgumentStateMap().decrement(
                    argumentRowId );
        }
    }

    public class RHSSink$ implements Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>, Buffers.AccumulatingBuffer
    {
        private final int argumentSlotOffset;

        public RHSSink$( final LHSAccumulatingRHSStreamingBuffer<DATA,LHS_ACC> $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                this.argumentSlotOffset =
                        $outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().argumentSlotOffset();
            }
        }

        public int argumentSlotOffset()
        {
            return this.argumentSlotOffset;
        }

        public void put( final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data )
        {
            if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
            org.neo4j.cypher.internal.runtime.debug.DebugSupport..
            MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[put]   " ).append( this ).append( " <- " ).append( data.mkString( ", " ) ).toString() );
        }

            for ( int i = 0; i < data.length(); ++i )
            {
                ArgumentStateMap.PerArgument argumentValue = (ArgumentStateMap.PerArgument) data.apply( i );
                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().update(
                        argumentValue.argumentRowId(), ( acc ) -> {
                            $anonfun$put$2( this, argumentValue, acc );
                            return BoxedUnit.UNIT;
                        } );
                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$tracker.increment();
            }
        }

        public boolean canPut()
        {
            return true;
        }

        public void initiate( final long argumentRowId, final MorselExecutionContext argumentMorsel )
        {
            if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
            org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().log(
                    (new StringBuilder( 32 )).append( "[init]  " ).append( this ).append( " <- argumentRowId=" ).append( argumentRowId ).append(
                            " from " ).append( argumentMorsel ).toString() );
        }

            long[] argumentRowIdsForReducers = this.$outer.forAllArgumentReducersAndGetArgumentRowIds(
                    this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$downstreamArgumentReducers,
                    argumentMorsel, ( x$13, x$14 ) -> {
                        $anonfun$initiate$2( x$13, BoxesRunTime.unboxToLong( x$14 ) );
                        return BoxedUnit.UNIT;
                    } );
            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().initiate(
                    argumentRowId, argumentMorsel, argumentRowIdsForReducers );
            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$tracker.increment();
        }

        public void increment( final long argumentRowId )
        {
            this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().increment(
                    argumentRowId );
        }

        public void decrement( final long argumentRowId )
        {
            ArgumentStateBuffer maybeBuffer =
                    (ArgumentStateBuffer) this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$rhsArgumentStateMap().decrement(
                            argumentRowId );
            if ( maybeBuffer != null )
            {
                long[] argumentRowIdsForReducers = maybeBuffer.argumentRowIdsForReducers();
                this.$outer.forAllArgumentReducers(
                        this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$downstreamArgumentReducers,
                        argumentRowIdsForReducers, ( x$15, x$16 ) -> {
                            $anonfun$decrement$1( x$15, BoxesRunTime.unboxToLong( x$16 ) );
                            return BoxedUnit.UNIT;
                        } );
                this.$outer.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$LHSAccumulatingRHSStreamingBuffer$$tracker.decrement();
            }
        }
    }
}
