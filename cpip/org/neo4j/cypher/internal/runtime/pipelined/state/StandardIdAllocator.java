package org.neo4j.cypher.internal.runtime.pipelined.state;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public class StandardIdAllocator implements IdAllocator
{
    private long nextFreeId = 0L;

    private long nextFreeId()
    {
        return this.nextFreeId;
    }

    private void nextFreeId_$eq( final long x$1 )
    {
        this.nextFreeId = x$1;
    }

    public long allocateIdBatch( final int nIds )
    {
        long firstId = this.nextFreeId();
        this.nextFreeId_$eq( this.nextFreeId() + (long) nIds );
        return firstId;
    }
}
