package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

public final class OptionalArgumentStateBuffer$
{
    public static OptionalArgumentStateBuffer$ MODULE$;

    static
    {
        new OptionalArgumentStateBuffer$();
    }

    private OptionalArgumentStateBuffer$()
    {
        MODULE$ = this;
    }
}
