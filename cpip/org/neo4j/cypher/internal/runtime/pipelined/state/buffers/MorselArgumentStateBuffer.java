package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentCountUpdater;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMapWithoutArgumentIdCounter;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

@JavaDocToJava
public class MorselArgumentStateBuffer<DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> extends ArgumentCountUpdater
        implements Buffers.AccumulatingBuffer, Sink<IndexedSeq<ArgumentStateMap.PerArgument<DATA>>>, Source<ACC>, Buffers.SinkByOrigin, Buffers.DataHolder
{
    private final QueryCompletionTracker tracker;
    private final IndexedSeq<Buffers.AccumulatingBuffer> downstreamArgumentReducers;
    private final ArgumentStateMap.ArgumentStateMaps argumentStateMaps;
    private final int argumentStateMapId;
    private final ArgumentStateMapWithoutArgumentIdCounter<ACC> argumentStateMap;
    private final int argumentSlotOffset;

    public MorselArgumentStateBuffer( final QueryCompletionTracker tracker, final IndexedSeq<Buffers.AccumulatingBuffer> downstreamArgumentReducers,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps, final int argumentStateMapId )
    {
        this.tracker = tracker;
        this.downstreamArgumentReducers = downstreamArgumentReducers;
        this.argumentStateMaps = argumentStateMaps;
        this.argumentStateMapId = argumentStateMapId;
        this.argumentStateMap = (ArgumentStateMapWithoutArgumentIdCounter) argumentStateMaps.apply( argumentStateMapId );
        this.argumentSlotOffset = this.argumentStateMap().argumentSlotOffset();
    }

    public ArgumentStateMap.ArgumentStateMaps argumentStateMaps()
    {
        return this.argumentStateMaps;
    }

    public int argumentStateMapId()
    {
        return this.argumentStateMapId;
    }

    private ArgumentStateMapWithoutArgumentIdCounter<ACC> argumentStateMap()
    {
        return this.argumentStateMap;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public <T> Sink<T> sinkFor( final int fromPipeline )
    {
        return this;
    }

    public void put( final IndexedSeq<ArgumentStateMap.PerArgument<DATA>> data )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[put]   " ).append( this ).append( " <- " ).append( data.mkString( ", " ) ).toString() );
    }

        for ( IntRef i = IntRef.create( 0 ); i.elem < data.length(); ++i.elem )
        {
            this.argumentStateMap().update( ((ArgumentStateMap.PerArgument) data.apply( i.elem )).argumentRowId(), ( acc ) -> {
                $anonfun$put$1( data, i, acc );
                return BoxedUnit.UNIT;
            } );
        }
    }

    public boolean canPut()
    {
        return true;
    }

    public boolean hasData()
    {
        return this.argumentStateMap().hasCompleted();
    }

    public ACC take()
    {
        ArgumentStateMap.MorselAccumulator accumulator = (ArgumentStateMap.MorselAccumulator) this.argumentStateMap().takeOneCompleted();
        if ( accumulator != null && .MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[take]  " ).append( this ).append( " -> " ).append( accumulator ).toString() );
    }

        return accumulator;
    }

    public void initiate( final long argumentRowId, final MorselExecutionContext argumentMorsel )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log(
                (new StringBuilder( 32 )).append( "[init]  " ).append( this ).append( " <- argumentRowId=" ).append( argumentRowId ).append( " from " ).append(
                        argumentMorsel ).toString() );
    }

        long[] argumentRowIdsForReducers = this.forAllArgumentReducersAndGetArgumentRowIds( this.downstreamArgumentReducers, argumentMorsel, ( x$1, x$2 ) -> {
            $anonfun$initiate$1( x$1, BoxesRunTime.unboxToLong( x$2 ) );
            return BoxedUnit.UNIT;
        } );
        this.argumentStateMap().initiate( argumentRowId, argumentMorsel, argumentRowIdsForReducers );
        this.tracker.increment();
    }

    public void increment( final long argumentRowId )
    {
        this.argumentStateMap().increment( argumentRowId );
    }

    public void decrement( final long argumentRowId )
    {
        this.argumentStateMap().decrement( argumentRowId );
    }

    public void clearAll()
    {
        this.argumentStateMap().clearAll( ( acc ) -> {
            $anonfun$clearAll$1( this, acc );
            return BoxedUnit.UNIT;
        } );
    }

    public void close( final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 13 )).append( "[close] " ).append( this ).append( " -X- " ).append( accumulator ).toString() );
    }

        this.forAllArgumentReducers( this.downstreamArgumentReducers, accumulator.argumentRowIdsForReducers(), ( x$3, x$4 ) -> {
            $anonfun$close$1( x$3, BoxesRunTime.unboxToLong( x$4 ) );
            return BoxedUnit.UNIT;
        } );
        this.tracker.decrement();
    }

    public boolean filterCancelledArguments( final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        return false;
    }

    public String toString()
    {
        return (new StringBuilder( 35 )).append( "MorselArgumentStateBuffer(planId: " ).append( new ArgumentStateMapId( this.argumentStateMapId() ) ).append(
                ")" ).append( this.argumentStateMap() ).toString();
    }
}
