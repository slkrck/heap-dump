package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Iterator;

import scala.Function1;
import scala.Predef.;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class StandardBuffer<T> implements Buffer<T>
{
    private final ArrayBuffer<T> data = new ArrayBuffer();

    private ArrayBuffer<T> data()
    {
        return this.data;
    }

    public void put( final T t )
    {
        this.data().append(.MODULE$.wrapRefArray( new Object[]{t} ));
    }

    public boolean canPut()
    {
        return this.data().size() < Buffer$.MODULE$.MAX_SIZE_HINT();
    }

    public boolean hasData()
    {
        return this.data().nonEmpty();
    }

    public T take()
    {
        return this.data().isEmpty() ? null : this.data().remove( 0 );
    }

    public void foreach( final Function1<T,BoxedUnit> f )
    {
        this.data().foreach( f );
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.$plus$plus$eq( "StandardBuffer(" );
        this.data().foreach( ( t ) -> {
            sb.$plus$plus$eq( t.toString() );
            return sb.$plus$eq( ',' );
        } );
        sb.$plus$eq( ')' );
        return sb.result();
    }

    public Iterator<T> iterator()
    {
        return (Iterator) scala.collection.JavaConverters..MODULE$.asJavaIteratorConverter( this.data().iterator() ).asJava();
    }
}
