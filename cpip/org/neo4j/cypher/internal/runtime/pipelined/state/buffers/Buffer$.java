package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

public final class Buffer$
{
    public static Buffer$ MODULE$;

    static
    {
        new Buffer$();
    }

    private final int MAX_SIZE_HINT;

    private Buffer$()
    {
        MODULE$ = this;
        this.MAX_SIZE_HINT = 10;
    }

    public int MAX_SIZE_HINT()
    {
        return this.MAX_SIZE_HINT;
    }
}
