package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class NoLock implements Lock
{
    private final String id;
    private boolean isLocked;

    public NoLock( final String id )
    {
        this.id = id;
        this.isLocked = false;
    }

    public String id()
    {
        return this.id;
    }

    private boolean isLocked()
    {
        return this.isLocked;
    }

    private void isLocked_$eq( final boolean x$1 )
    {
        this.isLocked = x$1;
    }

    public boolean tryLock()
    {
        if ( this.isLocked() )
        {
            throw new IllegalStateException( (new StringBuilder( 18 )).append( this.name() ).append( " is already locked" ).toString() );
        }
        else
        {
            this.isLocked_$eq( true );
            if (.MODULE$.LOCKS().enabled()){
            .MODULE$.LOCKS().log( "Locked %s", this.name() );
        }

            return this.isLocked();
        }
    }

    public void unlock()
    {
        if ( !this.isLocked() )
        {
            throw new IllegalStateException( (new StringBuilder( 14 )).append( this.name() ).append( " is not locked" ).toString() );
        }
        else
        {
            if (.MODULE$.LOCKS().enabled()){
            .MODULE$.LOCKS().log( "Unlocked %s", this.name() );
        }

            this.isLocked_$eq( false );
        }
    }

    private String name()
    {
        return (new StringBuilder( 2 )).append( this.getClass().getSimpleName() ).append( "[" ).append( this.id() ).append( "]" ).toString();
    }

    public String toString()
    {
        return (new StringBuilder( 9 )).append( this.name() ).append( ": Locked=" ).append( this.isLocked() ).toString();
    }
}
