package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentCountUpdater;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMapWithArgumentIdCounter;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker;
import scala.MatchError;
import scala.collection.IndexedSeq;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

@JavaDocToJava
public class OptionalMorselBuffer extends ArgumentCountUpdater
        implements Buffers.AccumulatingBuffer, Sink<IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>>>, ClosingSource<MorselData>,
        Buffers.SinkByOrigin, Buffers.DataHolder
{
    private final QueryCompletionTracker tracker;
    private final IndexedSeq<Buffers.AccumulatingBuffer> downstreamArgumentReducers;
    private final ArgumentStateMap.ArgumentStateMaps argumentStateMaps;
    private final int argumentStateMapId;
    private final ArgumentStateMapWithArgumentIdCounter<OptionalArgumentStateBuffer> argumentStateMap;
    private final int argumentSlotOffset;

    public OptionalMorselBuffer( final int id, final QueryCompletionTracker tracker, final IndexedSeq<Buffers.AccumulatingBuffer> downstreamArgumentReducers,
            final ArgumentStateMap.ArgumentStateMaps argumentStateMaps, final int argumentStateMapId )
    {
        this.tracker = tracker;
        this.downstreamArgumentReducers = downstreamArgumentReducers;
        this.argumentStateMaps = argumentStateMaps;
        this.argumentStateMapId = argumentStateMapId;
        this.argumentStateMap = (ArgumentStateMapWithArgumentIdCounter) argumentStateMaps.apply( argumentStateMapId );
        this.argumentSlotOffset = this.argumentStateMap().argumentSlotOffset();
    }

    public ArgumentStateMap.ArgumentStateMaps argumentStateMaps()
    {
        return this.argumentStateMaps;
    }

    public int argumentStateMapId()
    {
        return this.argumentStateMapId;
    }

    private ArgumentStateMapWithArgumentIdCounter<OptionalArgumentStateBuffer> argumentStateMap()
    {
        return this.argumentStateMap;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public <T> Sink<T> sinkFor( final int fromPipeline )
    {
        return this;
    }

    public MorselData take()
    {
        ArgumentStateMap.ArgumentStateWithCompleted argumentState = this.argumentStateMap().takeNextIfCompletedOrElsePeek();
        MorselData var10000;
        if ( argumentState == null )
        {
            var10000 = null;
        }
        else
        {
            MorselData var1;
            label54:
            {
                if ( argumentState != null )
                {
                    OptionalArgumentStateBuffer completedArgument = (OptionalArgumentStateBuffer) argumentState.argumentState();
                    boolean var6 = argumentState.isCompleted();
                    if ( var6 )
                    {
                        if ( !completedArgument.didReceiveData() )
                        {
                            var10000 = new MorselData( (IndexedSeq).MODULE$.IndexedSeq().empty(),
                                    new EndOfEmptyStream( completedArgument.viewOfArgumentRow( this.argumentSlotOffset() ) ),
                                    completedArgument.argumentRowIdsForReducers() );
                        }
                        else
                        {
                            IndexedSeq morsels = completedArgument.takeAll();
                            var10000 = morsels != null ? new MorselData( morsels, EndOfNonEmptyStream$.MODULE$, completedArgument.argumentRowIdsForReducers() )
                                                       : new MorselData( (IndexedSeq).MODULE$.IndexedSeq().empty(), EndOfNonEmptyStream$.MODULE$,
                                                               completedArgument.argumentRowIdsForReducers() );
                        }

                        var1 = var10000;
                        break label54;
                    }
                }

                if ( argumentState == null )
                {
                    throw new MatchError( argumentState );
                }

                OptionalArgumentStateBuffer incompleteArgument = (OptionalArgumentStateBuffer) argumentState.argumentState();
                boolean var9 = argumentState.isCompleted();
                if ( var9 )
                {
                    throw new MatchError( argumentState );
                }

                IndexedSeq morsels = incompleteArgument.takeAll();
                var1 = morsels != null ? new MorselData( morsels, NotTheEnd$.MODULE$, incompleteArgument.argumentRowIdsForReducers() ) : null;
            }

            var10000 = var1;
        }

        MorselData data = var10000;
        if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
        org.neo4j.cypher.internal.runtime.debug.DebugSupport..
        MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[take]  " ).append( this ).append( " -> " ).append( data ).toString() );
    }

        return data;
    }

    public boolean canPut()
    {
        OptionalArgumentStateBuffer buffer = (OptionalArgumentStateBuffer) this.argumentStateMap().peekNext();
        return buffer != null && buffer.canPut();
    }

    public void put( final IndexedSeq<ArgumentStateMap.PerArgument<MorselExecutionContext>> data )
    {
        if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
        org.neo4j.cypher.internal.runtime.debug.DebugSupport..
        MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[put]   " ).append( this ).append( " <- " ).append( data.mkString( ", " ) ).toString() );
    }

        this.tracker.incrementBy( (long) data.length() );

        for ( IntRef i = IntRef.create( 0 ); i.elem < data.length(); ++i.elem )
        {
            this.argumentStateMap().update( ((ArgumentStateMap.PerArgument) data.apply( i.elem )).argumentRowId(), ( acc ) -> {
                $anonfun$put$1( this, data, i, acc );
                return BoxedUnit.UNIT;
            } );
        }
    }

    public boolean hasData()
    {
        return this.argumentStateMap().nextArgumentStateIsCompletedOr( ( state ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$hasData$1( state ) );
        } );
    }

    public void initiate( final long argumentRowId, final MorselExecutionContext argumentMorsel )
    {
        if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
        org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().log(
                (new StringBuilder( 32 )).append( "[init]  " ).append( this ).append( " <- argumentRowId=" ).append( argumentRowId ).append( " from " ).append(
                        argumentMorsel ).toString() );
    }

        long[] argumentRowIdsForReducers = this.forAllArgumentReducersAndGetArgumentRowIds( this.downstreamArgumentReducers, argumentMorsel, ( x$3, x$4 ) -> {
            $anonfun$initiate$1( x$3, BoxesRunTime.unboxToLong( x$4 ) );
            return BoxedUnit.UNIT;
        } );
        this.argumentStateMap().initiate( argumentRowId, argumentMorsel, argumentRowIdsForReducers );
        this.tracker.increment();
    }

    public void increment( final long argumentRowId )
    {
        this.argumentStateMap().increment( argumentRowId );
    }

    public void decrement( final long argumentRowId )
    {
        this.argumentStateMap().decrement( argumentRowId );
    }

    public void clearAll()
    {
        this.argumentStateMap().clearAll( ( buffer ) -> {
            $anonfun$clearAll$1( this, buffer );
            return BoxedUnit.UNIT;
        } );
    }

    public void close( final MorselData data )
    {
        if ( org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.BUFFERS().enabled()){
        org.neo4j.cypher.internal.runtime.debug.DebugSupport..
        MODULE$.BUFFERS().log( (new StringBuilder( 13 )).append( "[close] " ).append( this ).append( " -X- " ).append( data ).toString() );
    }

        ArgumentStream var3 = data.argumentStream();
        BoxedUnit var2;
        if ( var3 instanceof EndOfStream )
        {
            this.tracker.decrement();
            this.forAllArgumentReducers( this.downstreamArgumentReducers, data.argumentRowIdsForReducers(), ( x$5, x$6 ) -> {
                $anonfun$close$1( x$5, BoxesRunTime.unboxToLong( x$6 ) );
                return BoxedUnit.UNIT;
            } );
            var2 = BoxedUnit.UNIT;
        }
        else
        {
            var2 = BoxedUnit.UNIT;
        }

        int numberOfDecrements = data.morsels().size();
        this.tracker.decrementBy( (long) numberOfDecrements );

        for ( int i = 0; i < numberOfDecrements; ++i )
        {
            this.forAllArgumentReducers( this.downstreamArgumentReducers, data.argumentRowIdsForReducers(), ( x$7, x$8 ) -> {
                $anonfun$close$2( x$7, BoxesRunTime.unboxToLong( x$8 ) );
                return BoxedUnit.UNIT;
            } );
        }
    }

    public boolean filterCancelledArguments( final ArgumentStateMap.MorselAccumulator<?> accumulator )
    {
        return false;
    }

    public String toString()
    {
        return (new StringBuilder( 30 )).append( "OptionalMorselBuffer(planId: " ).append( new ArgumentStateMapId( this.argumentStateMapId() ) ).append(
                ")" ).append( this.argumentStateMap() ).toString();
    }
}
