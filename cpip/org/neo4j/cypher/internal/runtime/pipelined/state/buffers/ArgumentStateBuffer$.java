package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

public final class ArgumentStateBuffer$
{
    public static ArgumentStateBuffer$ MODULE$;

    static
    {
        new ArgumentStateBuffer$();
    }

    private ArgumentStateBuffer$()
    {
        MODULE$ = this;
    }
}
