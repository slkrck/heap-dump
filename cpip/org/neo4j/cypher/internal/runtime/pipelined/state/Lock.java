package org.neo4j.cypher.internal.runtime.pipelined.state;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Lock
{
    boolean tryLock();

    void unlock();
}
