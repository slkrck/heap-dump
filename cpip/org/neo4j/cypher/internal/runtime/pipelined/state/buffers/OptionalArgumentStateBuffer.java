package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import org.neo4j.exceptions.InternalException;
import scala.collection.IndexedSeq;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class OptionalArgumentStateBuffer extends ArgumentStateBuffer
{
    private final MorselExecutionContext argumentMorsel;
    private final Buffer<MorselExecutionContext> inner;

    public OptionalArgumentStateBuffer( final long argumentRowId, final MorselExecutionContext argumentMorsel, final Buffer<MorselExecutionContext> inner,
            final long[] argumentRowIdsForReducers )
    {
        super( argumentRowId, inner, argumentRowIdsForReducers );
        this.argumentMorsel = argumentMorsel;
        this.inner = inner;
    }

    public MorselExecutionContext argumentMorsel()
    {
        return this.argumentMorsel;
    }

    public boolean didReceiveData()
    {
        return ((OptionalBuffer) this.inner).didReceiveData();
    }

    public MorselExecutionContext viewOfArgumentRow( final int argumentSlotOffset )
    {
        MorselExecutionContext view = this.argumentMorsel().shallowCopy();
        view.resetToFirstRow();

        long arg;
        for ( arg = view.getArgumentAt( argumentSlotOffset ); arg < super.argumentRowId() && view.isValidRow(); arg = view.getArgumentAt( argumentSlotOffset ) )
        {
            view.moveToNextRow();
        }

        if ( arg == super.argumentRowId() )
        {
            return view;
        }
        else
        {
            throw new InternalException(
                    (new StringBuilder( 35 )).append( "Could not locate argumentRowId " ).append( super.argumentRowId() ).append( " in " ).append(
                            this.argumentMorsel() ).toString() );
        }
    }

    public IndexedSeq<MorselExecutionContext> takeAll()
    {
        MorselExecutionContext morsel = this.take();
        ArrayBuffer var10000;
        if ( morsel != null )
        {
            ArrayBuffer morsels = new ArrayBuffer();

            while ( true )
            {
                morsels.$plus$eq( morsel );
                morsel = this.take();
                if ( morsel == null )
                {
                    var10000 = morsels;
                    break;
                }
            }
        }
        else
        {
            var10000 = null;
        }

        return var10000;
    }

    public String toString()
    {
        return (new StringBuilder( 88 )).append( "OptionalArgumentStateBuffer(argumentRowId=" ).append( super.argumentRowId() ).append(
                ", argumentRowIdsForReducers=" ).append( super.argumentRowIdsForReducers() ).append( ", argumentMorsel=" ).append(
                this.argumentMorsel() ).append( ")" ).toString();
    }

    public static class Factory implements ArgumentStateMap.ArgumentStateFactory<ArgumentStateBuffer>
    {
        private final StateFactory stateFactory;

        public Factory( final StateFactory stateFactory )
        {
            this.stateFactory = stateFactory;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public ArgumentStateBuffer newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new OptionalArgumentStateBuffer( argumentRowId, argumentMorsel, new StandardOptionalBuffer( this.stateFactory.newBuffer() ),
                    argumentRowIdsForReducers );
        }

        public ArgumentStateBuffer newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new OptionalArgumentStateBuffer( argumentRowId, argumentMorsel, new ConcurrentOptionalBuffer( this.stateFactory.newBuffer() ),
                    argumentRowIdsForReducers );
        }
    }
}
