package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.physicalplanning.TopLevelArgument$;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation;
import org.neo4j.cypher.internal.runtime.NoMemoryTracker.;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ConcurrentBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ConcurrentSingletonBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.SingletonBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ConcurrentStateFactory implements StateFactory
{
    private final QueryMemoryTracker memoryTracker;

    public ConcurrentStateFactory()
    {
        this.memoryTracker = .MODULE$;
    }

    public <T extends WithHeapUsageEstimation> Buffer<T> newBuffer()
    {
        return new ConcurrentBuffer();
    }

    public <T> SingletonBuffer<T> newSingletonBuffer()
    {
        return new ConcurrentSingletonBuffer();
    }

    public QueryCompletionTracker newTracker( final QuerySubscriber subscriber, final QueryContext queryContext, final QueryExecutionTracer tracer )
    {
        return new ConcurrentQueryCompletionTracker( subscriber, queryContext, tracer );
    }

    public IdAllocator newIdAllocator()
    {
        return new ConcurrentIdAllocator();
    }

    public Lock newLock( final String id )
    {
        return new ConcurrentLock( id );
    }

    public <S extends ArgumentStateMap.ArgumentState> ArgumentStateMap<S> newArgumentStateMap( final int argumentStateMapId, final int argumentSlotOffset,
            final ArgumentStateMap.ArgumentStateFactory<S> factory )
    {
        return (ArgumentStateMap) (argumentSlotOffset == TopLevelArgument$.MODULE$.SLOT_OFFSET() ? new ConcurrentSingletonArgumentStateMap( argumentStateMapId,
                factory ) : new ConcurrentArgumentStateMap( argumentStateMapId, argumentSlotOffset, factory ));
    }

    public QueryMemoryTracker memoryTracker()
    {
        return this.memoryTracker;
    }
}
