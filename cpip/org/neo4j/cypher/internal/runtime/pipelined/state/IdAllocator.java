package org.neo4j.cypher.internal.runtime.pipelined.state;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface IdAllocator
{
    long allocateIdBatch( final int nIds );
}
