package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public interface ArgumentStateMap<S extends ArgumentStateMap.ArgumentState>
{
    static <T> void foreach( final int argumentSlotOffset, final MorselExecutionContext morsel, final Function2<Object,MorselExecutionContext,BoxedUnit> f )
    {
        ArgumentStateMap$.MODULE$.foreach( var0, var1, var2 );
    }

    static <T> IndexedSeq<ArgumentStateMap.PerArgument<T>> map( final int argumentSlotOffset, final MorselExecutionContext morsel,
            final Function1<MorselExecutionContext,T> f )
    {
        return ArgumentStateMap$.MODULE$.map( var0, var1, var2 );
    }

    void clearAll( final Function1<S,BoxedUnit> f );

    void update( final long argumentRowId, final Function1<S,BoxedUnit> onState );

    <FILTER_STATE> void filter( final MorselExecutionContext morsel, final Function2<S,Object,FILTER_STATE> onArgument,
            final Function2<FILTER_STATE,MorselExecutionContext,Object> onRow );

    Iterator<S> peekCompleted();

    S peek( final long argumentId );

    boolean hasCompleted();

    boolean hasCompleted( final long argument );

    boolean remove( final long argument );

    void initiate( final long argument, final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers );

    void increment( final long argument );

    S decrement( final long argument );

    int argumentStateMapId();

    int argumentSlotOffset();

    public interface ArgumentState
    {
        long argumentRowId();

        long[] argumentRowIdsForReducers();
    }

    public interface ArgumentStateFactory<S extends ArgumentStateMap.ArgumentState>
    {
        static void $init$( final ArgumentStateMap.ArgumentStateFactory $this )
        {
        }

        S newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers );

        S newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers );

        default boolean completeOnConstruction()
        {
            return false;
        }
    }

    public interface ArgumentStateMaps
    {
        static void $init$( final ArgumentStateMap.ArgumentStateMaps $this )
        {
        }

        ArgumentStateMap<? extends ArgumentStateMap.ArgumentState> apply( final int argumentStateMapId );

        default ArgumentStateMap<? extends ArgumentStateMap.ArgumentState> applyByIntId( final int argumentStateMapId )
        {
            return this.apply( argumentStateMapId );
        }
    }

    public interface MorselAccumulator<DATA> extends ArgumentStateMap.ArgumentState
    {
        void update( final DATA data );
    }

    public interface WorkCanceller extends ArgumentStateMap.ArgumentState
    {
        boolean isCancelled();
    }

    public static class ArgumentStateWithCompleted<S extends ArgumentStateMap.ArgumentState> implements Product, Serializable
    {
        private final S argumentState;
        private final boolean isCompleted;

        public ArgumentStateWithCompleted( final S argumentState, final boolean isCompleted )
        {
            this.argumentState = argumentState;
            this.isCompleted = isCompleted;
            Product.$init$( this );
        }

        public S argumentState()
        {
            return this.argumentState;
        }

        public boolean isCompleted()
        {
            return this.isCompleted;
        }

        public <S extends ArgumentStateMap.ArgumentState> ArgumentStateMap.ArgumentStateWithCompleted<S> copy( final S argumentState,
                final boolean isCompleted )
        {
            return new ArgumentStateMap.ArgumentStateWithCompleted( argumentState, isCompleted );
        }

        public <S extends ArgumentStateMap.ArgumentState> S copy$default$1()
        {
            return this.argumentState();
        }

        public <S extends ArgumentStateMap.ArgumentState> boolean copy$default$2()
        {
            return this.isCompleted();
        }

        public String productPrefix()
        {
            return "ArgumentStateWithCompleted";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.argumentState();
                break;
            case 1:
                var10000 = BoxesRunTime.boxToBoolean( this.isCompleted() );
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof ArgumentStateMap.ArgumentStateWithCompleted;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.anyHash( this.argumentState() ) );
            var1 = Statics.mix( var1, this.isCompleted() ? 1231 : 1237 );
            return Statics.finalizeHash( var1, 2 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            if ( this != x$1 )
            {
                label55:
                {
                    boolean var2;
                    if ( x$1 instanceof ArgumentStateMap.ArgumentStateWithCompleted )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label38:
                        {
                            label37:
                            {
                                ArgumentStateMap.ArgumentStateWithCompleted var4 = (ArgumentStateMap.ArgumentStateWithCompleted) x$1;
                                ArgumentStateMap.ArgumentState var10000 = this.argumentState();
                                ArgumentStateMap.ArgumentState var5 = var4.argumentState();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label37;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label37;
                                }

                                if ( this.isCompleted() == var4.isCompleted() && var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label38;
                                }
                            }

                            var6 = false;
                        }

                        if ( var6 )
                        {
                            break label55;
                        }
                    }

                    var6 = false;
                    return var6;
                }
            }

            var6 = true;
            return var6;
        }
    }

    public static class ArgumentStateWithCompleted$ implements Serializable
    {
        public static ArgumentStateMap.ArgumentStateWithCompleted$ MODULE$;

        static
        {
            new ArgumentStateMap.ArgumentStateWithCompleted$();
        }

        public ArgumentStateWithCompleted$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "ArgumentStateWithCompleted";
        }

        public <S extends ArgumentStateMap.ArgumentState> ArgumentStateMap.ArgumentStateWithCompleted<S> apply( final S argumentState,
                final boolean isCompleted )
        {
            return new ArgumentStateMap.ArgumentStateWithCompleted( argumentState, isCompleted );
        }

        public <S extends ArgumentStateMap.ArgumentState> Option<Tuple2<S,Object>> unapply( final ArgumentStateMap.ArgumentStateWithCompleted<S> x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.argumentState(), BoxesRunTime.boxToBoolean( x$0.isCompleted() ) ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class PerArgument<T> implements Product, Serializable
    {
        private final long argumentRowId;
        private final T value;

        public PerArgument( final long argumentRowId, final T value )
        {
            this.argumentRowId = argumentRowId;
            this.value = value;
            Product.$init$( this );
        }

        public long argumentRowId()
        {
            return this.argumentRowId;
        }

        public T value()
        {
            return this.value;
        }

        public <T> ArgumentStateMap.PerArgument<T> copy( final long argumentRowId, final T value )
        {
            return new ArgumentStateMap.PerArgument( argumentRowId, value );
        }

        public <T> long copy$default$1()
        {
            return this.argumentRowId();
        }

        public <T> T copy$default$2()
        {
            return this.value();
        }

        public String productPrefix()
        {
            return "PerArgument";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = BoxesRunTime.boxToLong( this.argumentRowId() );
                break;
            case 1:
                var10000 = this.value();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof ArgumentStateMap.PerArgument;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.longHash( this.argumentRowId() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.value() ) );
            return Statics.finalizeHash( var1, 2 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label51:
                {
                    boolean var2;
                    if ( x$1 instanceof ArgumentStateMap.PerArgument )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        ArgumentStateMap.PerArgument var4 = (ArgumentStateMap.PerArgument) x$1;
                        if ( this.argumentRowId() == var4.argumentRowId() && BoxesRunTime.equals( this.value(), var4.value() ) && var4.canEqual( this ) )
                        {
                            break label51;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class PerArgument$ implements Serializable
    {
        public static ArgumentStateMap.PerArgument$ MODULE$;

        static
        {
            new ArgumentStateMap.PerArgument$();
        }

        public PerArgument$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "PerArgument";
        }

        public <T> ArgumentStateMap.PerArgument<T> apply( final long argumentRowId, final T value )
        {
            return new ArgumentStateMap.PerArgument( argumentRowId, value );
        }

        public <T> Option<Tuple2<Object,T>> unapply( final ArgumentStateMap.PerArgument<T> x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToLong( x$0.argumentRowId() ), x$0.value() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
