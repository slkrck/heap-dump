package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.SingletonBuffer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface StateFactory
{
    <T extends WithHeapUsageEstimation> Buffer<T> newBuffer();

    <T> SingletonBuffer<T> newSingletonBuffer();

    QueryCompletionTracker newTracker( final QuerySubscriber subscriber, final QueryContext queryContext, final QueryExecutionTracer tracer );

    IdAllocator newIdAllocator();

    Lock newLock( final String id );

    <S extends ArgumentStateMap.ArgumentState> ArgumentStateMap<S> newArgumentStateMap( final int argumentStateMapId, final int argumentSlotOffset,
            final ArgumentStateMap.ArgumentStateFactory<S> factory );

    QueryMemoryTracker memoryTracker();
}
