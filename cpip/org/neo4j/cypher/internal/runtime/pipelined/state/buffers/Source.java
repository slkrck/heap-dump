package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Source<T>
{
    boolean hasData();

    T take();
}
