package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class EndOfEmptyStream$ extends AbstractFunction1<MorselExecutionContext,EndOfEmptyStream> implements Serializable
{
    public static EndOfEmptyStream$ MODULE$;

    static
    {
        new EndOfEmptyStream$();
    }

    private EndOfEmptyStream$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "EndOfEmptyStream";
    }

    public EndOfEmptyStream apply( final MorselExecutionContext viewOfArgumentRow )
    {
        return new EndOfEmptyStream( viewOfArgumentRow );
    }

    public Option<MorselExecutionContext> unapply( final EndOfEmptyStream x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.viewOfArgumentRow() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
