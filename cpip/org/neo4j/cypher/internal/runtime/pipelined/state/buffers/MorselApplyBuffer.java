package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentCountUpdater;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.IdAllocator;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class MorselApplyBuffer extends ArgumentCountUpdater implements Buffers.SinkByOrigin, Sink<MorselExecutionContext>
{
    private final int id;
    private final IndexedSeq<ArgumentStateMapId> argumentStatesOnRHSOfThisApply;
    private final IndexedSeq<Buffers.AccumulatingBuffer> argumentReducersOnRHSOfThisApply;
    private final IndexedSeq<Buffers.AccumulatingBuffer> argumentReducersOnTopOfThisApply;
    private final ArgumentStateMap.ArgumentStateMaps argumentStateMaps;
    private final int argumentSlotOffset;
    private final IdAllocator idAllocator;
    private final IndexedSeq<MorselBuffer> delegates;

    public MorselApplyBuffer( final int id, final IndexedSeq<ArgumentStateMapId> argumentStatesOnRHSOfThisApply,
            final IndexedSeq<Buffers.AccumulatingBuffer> argumentReducersOnRHSOfThisApply,
            final IndexedSeq<Buffers.AccumulatingBuffer> argumentReducersOnTopOfThisApply, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps,
            final int argumentSlotOffset, final IdAllocator idAllocator, final IndexedSeq<MorselBuffer> delegates )
    {
        this.id = id;
        this.argumentStatesOnRHSOfThisApply = argumentStatesOnRHSOfThisApply;
        this.argumentReducersOnRHSOfThisApply = argumentReducersOnRHSOfThisApply;
        this.argumentReducersOnTopOfThisApply = argumentReducersOnTopOfThisApply;
        this.argumentStateMaps = argumentStateMaps;
        this.argumentSlotOffset = argumentSlotOffset;
        this.idAllocator = idAllocator;
        this.delegates = delegates;
    }

    public ArgumentStateMap.ArgumentStateMaps argumentStateMaps()
    {
        return this.argumentStateMaps;
    }

    public <T> Sink<T> sinkFor( final int fromPipeline )
    {
        return this;
    }

    public void put( final MorselExecutionContext morsel )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[put]   " ).append( this ).append( " <- " ).append( morsel ).toString() );
    }

        if ( morsel.hasData() )
        {
            long argumentRowId = this.idAllocator.allocateIdBatch( morsel.getValidRows() );
            morsel.resetToFirstRow();

            while ( morsel.isValidRow() )
            {
                morsel.setArgumentAt( this.argumentSlotOffset, argumentRowId );
                this.initiateArgumentReducersHere( this.argumentReducersOnRHSOfThisApply, argumentRowId, morsel );
                this.initiateArgumentStatesHere( this.argumentStatesOnRHSOfThisApply, argumentRowId, morsel );
                ++argumentRowId;
                morsel.moveToNextRow();
            }

            this.incrementArgumentCounts( this.argumentReducersOnTopOfThisApply, morsel );

            for ( int i = 0; i < this.delegates.size(); ++i )
            {
                ((MorselBuffer) this.delegates.apply( i )).putInDelegate( morsel.shallowCopy() );
            }
        }
    }

    public boolean canPut()
    {
        return this.delegates.forall( ( x$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$canPut$1( x$1 ) );
        } );
    }

    public String toString()
    {
        return (new StringBuilder( 40 )).append( "MorselApplyBuffer(" ).append( new BufferId( this.id ) ).append( ", argumentSlotOffset=" ).append(
                this.argumentSlotOffset ).append( ")" ).toString();
    }
}
