package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.RuntimeResourceLeakException;
import org.neo4j.cypher.internal.physicalplanning.ApplyBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.physicalplanning.AttachBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.BufferDefinition;
import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.physicalplanning.BufferVariant;
import org.neo4j.cypher.internal.physicalplanning.LHSAccumulatingRHSStreamingBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.OptionalBufferVariant;
import org.neo4j.cypher.internal.physicalplanning.RegularBufferVariant$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.mutable.ArrayBuilder;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Buffers
{
    private final QueryCompletionTracker tracker;
    private final ArgumentStateMap.ArgumentStateMaps argumentStateMaps;
    private final StateFactory stateFactory;
    private final Buffers.SinkByOrigin[] buffers;

    public Buffers( final int numBuffers, final QueryCompletionTracker tracker, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps,
            final StateFactory stateFactory )
    {
        this.tracker = tracker;
        this.argumentStateMaps = argumentStateMaps;
        this.stateFactory = stateFactory;
        this.buffers = new Buffers.SinkByOrigin[numBuffers];
    }

    private Buffers.SinkByOrigin[] buffers()
    {
        return this.buffers;
    }

    private Buffers.AccumulatingBuffer[] findRHSAccumulatingStateBuffers( final int initialIndex, final ArgumentStateMapId[] argumentStateMapIds )
    {
        int j = 0;

        ArrayBuilder reducersBuilder;
        for ( reducersBuilder = scala.Array..MODULE$.newBuilder( scala.reflect.ClassTag..MODULE$.apply( Buffers.AccumulatingBuffer.class ));
        j<argumentStateMapIds.length ;
        ++j){
        int asmId = argumentStateMapIds[j].x();
        Buffers.AccumulatingBuffer accumulatingBuffer = this.findRHSAccumulatingStateBuffer( initialIndex, asmId );
        reducersBuilder.$plus$eq( accumulatingBuffer );
    }

        return (Buffers.AccumulatingBuffer[]) reducersBuilder.result();
    }

    private Buffers.AccumulatingBuffer findRHSAccumulatingStateBuffer( final int initialIndex, final int argumentStateMapId )
    {
        for ( int j = initialIndex + 1; j < this.buffers().length; ++j )
        {
            boolean var5 = false;
            LHSAccumulatingRHSStreamingBuffer var6 = null;
            Buffers.SinkByOrigin var7 = this.buffers()[j];
            if ( var7 instanceof LHSAccumulatingRHSStreamingBuffer )
            {
                var5 = true;
                var6 = (LHSAccumulatingRHSStreamingBuffer) var7;
                if ( var6.lhsArgumentStateMapId() == argumentStateMapId )
                {
                    return var6.LHSSink();
                }
            }

            if ( var5 && var6.rhsArgumentStateMapId() == argumentStateMapId )
            {
                return var6.RHSSink();
            }

            if ( var7 instanceof MorselArgumentStateBuffer )
            {
                MorselArgumentStateBuffer var8 = (MorselArgumentStateBuffer) var7;
                if ( var8.argumentStateMapId() == argumentStateMapId )
                {
                    return var8;
                }
            }

            if ( var7 instanceof OptionalMorselBuffer )
            {
                OptionalMorselBuffer var9 = (OptionalMorselBuffer) var7;
                if ( var9.argumentStateMapId() == argumentStateMapId )
                {
                    return var9;
                }
            }

            BoxedUnit var3 = BoxedUnit.UNIT;
        }

        throw new IllegalStateException( (new StringBuilder( 54 )).append( "Could not find downstream argumentStateBuffer with id " ).append(
                new ArgumentStateMapId( argumentStateMapId ) ).toString() );
    }

    public void constructBuffer( final BufferDefinition bufferDefinition )
    {
        int i = bufferDefinition.id();
        if ( this.buffers()[i] == null )
        {
            Buffers.AccumulatingBuffer[] reducers = this.findRHSAccumulatingStateBuffers( i, bufferDefinition.reducers() );
            ArgumentStateMapId[] workCancellers = bufferDefinition.workCancellers();
            ArgumentStateMapId[] downstreamStates = bufferDefinition.downstreamStates();
            Buffers.SinkByOrigin[] var10000 = this.buffers();
            BufferVariant var7 = bufferDefinition.variant();
            Object var2;
            if ( var7 instanceof AttachBufferVariant )
            {
                AttachBufferVariant var8 = (AttachBufferVariant) var7;
                this.constructBuffer( var8.applyBuffer() );
                var2 = new MorselAttachBuffer( bufferDefinition.id(), this.applyBuffer( var8.applyBuffer().id() ), var8.outputSlots(),
                        var8.argumentSlotOffset(), var8.argumentSize().nLongs(), var8.argumentSize().nReferences() );
            }
            else if ( var7 instanceof ApplyBufferVariant )
            {
                ApplyBufferVariant var9 = (ApplyBufferVariant) var7;
                ArgumentStateMapId[] argumentStatesToInitiate = this.concatWithoutCopy( workCancellers, downstreamStates );
                Buffers.AccumulatingBuffer[] reducersOnRHS = this.findRHSAccumulatingStateBuffers( i, var9.reducersOnRHSReversed() );
                var2 = new MorselApplyBuffer( bufferDefinition.id(), scala.Predef..MODULE$.genericWrapArray( argumentStatesToInitiate ), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) reducersOnRHS ), scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) reducers ), this.argumentStateMaps, var9.argumentSlotOffset(), this.stateFactory.newIdAllocator(), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) this.morselBuffers( var9.delegates() ) ));
            }
            else if ( var7 instanceof ArgumentStateBufferVariant )
            {
                ArgumentStateBufferVariant var12 = (ArgumentStateBufferVariant) var7;
                var2 = new MorselArgumentStateBuffer( this.tracker, scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) reducers ), this.argumentStateMaps, var12.argumentStateMapId());
            }
            else if ( var7 instanceof LHSAccumulatingRHSStreamingBufferVariant )
            {
                LHSAccumulatingRHSStreamingBufferVariant var13 = (LHSAccumulatingRHSStreamingBufferVariant) var7;
                var2 = new LHSAccumulatingRHSStreamingBuffer( this.tracker, scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) reducers ), this.argumentStateMaps, var13.lhsArgumentStateMapId(), var13.rhsArgumentStateMapId(), var13.lhsPipelineId(), var13.rhsPipelineId(), this.stateFactory)
                ;
            }
            else if ( var7 instanceof OptionalBufferVariant )
            {
                OptionalBufferVariant var14 = (OptionalBufferVariant) var7;
                var2 = new OptionalMorselBuffer( bufferDefinition.id(), this.tracker, scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) reducers ), this.argumentStateMaps, var14.argumentStateMapId());
            }
            else
            {
                if ( !RegularBufferVariant$.MODULE$.equals( var7 ) )
                {
                    throw new MatchError( var7 );
                }

                var2 = new MorselBuffer( bufferDefinition.id(), this.tracker, scala.Predef..MODULE$.wrapRefArray( (Object[]) reducers ), scala.Predef..
                MODULE$.genericWrapArray( workCancellers ), this.argumentStateMaps, this.stateFactory.newBuffer());
            }

            var10000[i] = (Buffers.SinkByOrigin) var2;
        }
    }

    public <T> Sink<T> sink( final int fromPipeline, final int bufferId )
    {
        return this.buffers()[bufferId].sinkFor( fromPipeline );
    }

    public <S> Source<S> source( final int bufferId )
    {
        return (Source) this.buffers()[bufferId];
    }

    public boolean hasData( final int bufferId )
    {
        return ((Source) this.buffers()[bufferId]).hasData();
    }

    public MorselBuffer morselBuffer( final int bufferId )
    {
        return (MorselBuffer) this.buffers()[bufferId];
    }

    public MorselApplyBuffer applyBuffer( final int bufferId )
    {
        return (MorselApplyBuffer) this.buffers()[bufferId];
    }

    public <S> ClosingSource<S> closingSource( final int bufferId )
    {
        return (ClosingSource) this.buffers()[bufferId];
    }

    public MorselArgumentStateBuffer<?,?> argumentStateBuffer( final int bufferId )
    {
        return (MorselArgumentStateBuffer) this.buffers()[bufferId];
    }

    public LHSAccumulatingRHSStreamingBuffer<?,?> lhsAccumulatingRhsStreamingBuffer( final int bufferId )
    {
        return (LHSAccumulatingRHSStreamingBuffer) this.buffers()[bufferId];
    }

    public void clearAll()
    {
        for ( int i = 0; i < this.buffers().length; ++i )
        {
            Buffers.SinkByOrigin buffer = this.buffers()[i];
            BoxedUnit var1;
            if ( buffer instanceof Buffers.DataHolder )
            {
                org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.ERROR_HANDLING().log( "Clearing %s", buffer );
                ((Buffers.DataHolder) buffer).clearAll();
                var1 = BoxedUnit.UNIT;
            }
            else
            {
                org.neo4j.cypher.internal.runtime.debug.DebugSupport..MODULE$.ERROR_HANDLING().log( "Not clearing %s", buffer );
                var1 = BoxedUnit.UNIT;
            }
        }
    }

    public void assertAllEmpty()
    {
        for ( int i = 0; i < this.buffers().length; ++i )
        {
            Buffers.SinkByOrigin buffer = this.buffers()[i];
            BoxedUnit var1;
            if ( buffer instanceof Source )
            {
                if ( ((Source) buffer).hasData() )
                {
                    throw new RuntimeResourceLeakException(
                            (new StringBuilder( 44 )).append( "Buffer " ).append( buffer ).append( " is not empty after query completion." ).toString() );
                }

                var1 = BoxedUnit.UNIT;
            }
            else
            {
                var1 = BoxedUnit.UNIT;
            }
        }
    }

    private ArgumentStateMapId[] concatWithoutCopy( final ArgumentStateMapId[] a, final ArgumentStateMapId[] b )
    {
        if ( a.length == 0 )
        {
            return b;
        }
        else if ( b.length == 0 )
        {
            return a;
        }
        else
        {
            ArgumentStateMapId[] result = new ArgumentStateMapId[a.length + b.length];
            System.arraycopy( a, 0, result, 0, a.length );
            System.arraycopy( b, 0, result, a.length, b.length );
            return result;
        }
    }

    private MorselBuffer[] morselBuffers( final BufferId[] bufferIds )
    {
        MorselBuffer[] result = new MorselBuffer[bufferIds.length];

        for ( int i = 0; i < result.length; ++i )
        {
            result[i] = this.morselBuffer( bufferIds[i].x() );
        }

        return result;
    }

    public interface AccumulatingBuffer
    {
        int argumentSlotOffset();

        void initiate( final long argumentRowId, final MorselExecutionContext argumentMorsel );

        void increment( final long argumentRowId );

        void decrement( final long argumentRowId );
    }

    public interface DataHolder
    {
        void clearAll();
    }

    public interface SinkByOrigin
    {
        <T> Sink<T> sinkFor( final int fromPipeline );
    }

    public static class AccumulatorAndMorsel<DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> implements Product, Serializable
    {
        private final ACC acc;
        private final MorselExecutionContext morsel;

        public AccumulatorAndMorsel( final ACC acc, final MorselExecutionContext morsel )
        {
            this.acc = acc;
            this.morsel = morsel;
            Product.$init$( this );
        }

        public ACC acc()
        {
            return this.acc;
        }

        public MorselExecutionContext morsel()
        {
            return this.morsel;
        }

        public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> Buffers.AccumulatorAndMorsel<DATA,ACC> copy( final ACC acc,
                final MorselExecutionContext morsel )
        {
            return new Buffers.AccumulatorAndMorsel( acc, morsel );
        }

        public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> ACC copy$default$1()
        {
            return this.acc();
        }

        public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> MorselExecutionContext copy$default$2()
        {
            return this.morsel();
        }

        public String productPrefix()
        {
            return "AccumulatorAndMorsel";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.acc();
                break;
            case 1:
                var10000 = this.morsel();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Buffers.AccumulatorAndMorsel;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof Buffers.AccumulatorAndMorsel )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                Buffers.AccumulatorAndMorsel var4 = (Buffers.AccumulatorAndMorsel) x$1;
                                ArgumentStateMap.MorselAccumulator var10000 = this.acc();
                                ArgumentStateMap.MorselAccumulator var5 = var4.acc();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                MorselExecutionContext var7 = this.morsel();
                                MorselExecutionContext var6 = var4.morsel();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class AccumulatorAndMorsel$ implements Serializable
    {
        public static Buffers.AccumulatorAndMorsel$ MODULE$;

        static
        {
            new Buffers.AccumulatorAndMorsel$();
        }

        public AccumulatorAndMorsel$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "AccumulatorAndMorsel";
        }

        public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> Buffers.AccumulatorAndMorsel<DATA,ACC> apply( final ACC acc,
                final MorselExecutionContext morsel )
        {
            return new Buffers.AccumulatorAndMorsel( acc, morsel );
        }

        public <DATA, ACC extends ArgumentStateMap.MorselAccumulator<DATA>> Option<Tuple2<ACC,MorselExecutionContext>> unapply(
                final Buffers.AccumulatorAndMorsel<DATA,ACC> x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.acc(), x$0.morsel() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
