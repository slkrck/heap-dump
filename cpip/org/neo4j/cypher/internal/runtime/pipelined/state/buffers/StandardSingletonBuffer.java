package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Iterator;

import org.neo4j.internal.helpers.collection.Iterators;
import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class StandardSingletonBuffer<T> implements SingletonBuffer<T>
{
    private T datum;

    private T datum()
    {
        return this.datum;
    }

    private void datum_$eq( final T x$1 )
    {
        this.datum = x$1;
    }

    public void put( final T t )
    {
        if ( this.datum() != null )
        {
            throw new IllegalStateException(
                    (new StringBuilder( 65 )).append( "SingletonBuffer is full: tried to put " ).append( t ).append( ", but already held element " ).append(
                            this.datum() ).toString() );
        }
        else
        {
            this.datum_$eq( t );
        }
    }

    public void tryPut( final T t )
    {
        if ( this.datum() == null )
        {
            this.datum_$eq( t );
        }
    }

    public boolean canPut()
    {
        return this.datum() == null;
    }

    public boolean hasData()
    {
        return this.datum() != null;
    }

    public T take()
    {
        Object t = this.datum();
        this.datum_$eq( (Object) null );
        return t;
    }

    public void foreach( final Function1<T,BoxedUnit> f )
    {
        if ( this.datum() != null )
        {
            f.apply( this.datum() );
        }
    }

    public String toString()
    {
        scala.collection.mutable.StringBuilder sb = new scala.collection.mutable.StringBuilder();
        sb.$plus$plus$eq( "StandardSingletonBuffer(" );
        if ( this.datum() != null )
        {
            sb.$plus$plus$eq( this.datum().toString() );
        }
        else
        {
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        sb.$plus$eq( ')' );
        return sb.result();
    }

    public Iterator<T> iterator()
    {
        return Iterators.iterator( this.datum() );
    }
}
