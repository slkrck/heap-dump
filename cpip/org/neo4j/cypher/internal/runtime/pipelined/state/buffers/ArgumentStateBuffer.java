package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Iterator;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory;
import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class ArgumentStateBuffer implements ArgumentStateMap.MorselAccumulator<MorselExecutionContext>, Buffer<MorselExecutionContext>
{
    private final long argumentRowId;
    private final Buffer<MorselExecutionContext> inner;
    private final long[] argumentRowIdsForReducers;

    public ArgumentStateBuffer( final long argumentRowId, final Buffer<MorselExecutionContext> inner, final long[] argumentRowIdsForReducers )
    {
        this.argumentRowId = argumentRowId;
        this.inner = inner;
        this.argumentRowIdsForReducers = argumentRowIdsForReducers;
    }

    public long argumentRowId()
    {
        return this.argumentRowId;
    }

    public long[] argumentRowIdsForReducers()
    {
        return this.argumentRowIdsForReducers;
    }

    public void update( final MorselExecutionContext morsel )
    {
        this.put( morsel );
    }

    public void put( final MorselExecutionContext morsel )
    {
        morsel.resetToFirstRow();
        this.inner.put( morsel );
    }

    public boolean canPut()
    {
        return this.inner.canPut();
    }

    public boolean hasData()
    {
        return this.inner.hasData();
    }

    public MorselExecutionContext take()
    {
        return (MorselExecutionContext) this.inner.take();
    }

    public void foreach( final Function1<MorselExecutionContext,BoxedUnit> f )
    {
        this.inner.foreach( f );
    }

    public Iterator<MorselExecutionContext> iterator()
    {
        return this.inner.iterator();
    }

    public String toString()
    {
        return (new StringBuilder( 35 )).append( "ArgumentStateBuffer(argumentRowId=" ).append( this.argumentRowId() ).append( ")" ).toString();
    }

    public static class Factory implements ArgumentStateMap.ArgumentStateFactory<ArgumentStateBuffer>
    {
        private final StateFactory stateFactory;

        public Factory( final StateFactory stateFactory )
        {
            this.stateFactory = stateFactory;
            ArgumentStateMap.ArgumentStateFactory.$init$( this );
        }

        public boolean completeOnConstruction()
        {
            return ArgumentStateMap.ArgumentStateFactory.completeOnConstruction$( this );
        }

        public ArgumentStateBuffer newStandardArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new ArgumentStateBuffer( argumentRowId, this.stateFactory.newBuffer(), argumentRowIdsForReducers );
        }

        public ArgumentStateBuffer newConcurrentArgumentState( final long argumentRowId, final MorselExecutionContext argumentMorsel,
                final long[] argumentRowIdsForReducers )
        {
            return new ArgumentStateBuffer( argumentRowId, this.stateFactory.newBuffer(), argumentRowIdsForReducers );
        }
    }
}
