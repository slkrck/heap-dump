package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselData implements Product, Serializable
{
    private final IndexedSeq<MorselExecutionContext> morsels;
    private final ArgumentStream argumentStream;
    private final long[] argumentRowIdsForReducers;

    public MorselData( final IndexedSeq<MorselExecutionContext> morsels, final ArgumentStream argumentStream, final long[] argumentRowIdsForReducers )
    {
        this.morsels = morsels;
        this.argumentStream = argumentStream;
        this.argumentRowIdsForReducers = argumentRowIdsForReducers;
        Product.$init$( this );
    }

    public static Option<Tuple3<IndexedSeq<MorselExecutionContext>,ArgumentStream,long[]>> unapply( final MorselData x$0 )
    {
        return MorselData$.MODULE$.unapply( var0 );
    }

    public static MorselData apply( final IndexedSeq<MorselExecutionContext> morsels, final ArgumentStream argumentStream,
            final long[] argumentRowIdsForReducers )
    {
        return MorselData$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<IndexedSeq<MorselExecutionContext>,ArgumentStream,long[]>,MorselData> tupled()
    {
        return MorselData$.MODULE$.tupled();
    }

    public static Function1<IndexedSeq<MorselExecutionContext>,Function1<ArgumentStream,Function1<long[],MorselData>>> curried()
    {
        return MorselData$.MODULE$.curried();
    }

    public IndexedSeq<MorselExecutionContext> morsels()
    {
        return this.morsels;
    }

    public ArgumentStream argumentStream()
    {
        return this.argumentStream;
    }

    public long[] argumentRowIdsForReducers()
    {
        return this.argumentRowIdsForReducers;
    }

    public MorselData copy( final IndexedSeq<MorselExecutionContext> morsels, final ArgumentStream argumentStream, final long[] argumentRowIdsForReducers )
    {
        return new MorselData( morsels, argumentStream, argumentRowIdsForReducers );
    }

    public IndexedSeq<MorselExecutionContext> copy$default$1()
    {
        return this.morsels();
    }

    public ArgumentStream copy$default$2()
    {
        return this.argumentStream();
    }

    public long[] copy$default$3()
    {
        return this.argumentRowIdsForReducers();
    }

    public String productPrefix()
    {
        return "MorselData";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.morsels();
            break;
        case 1:
            var10000 = this.argumentStream();
            break;
        case 2:
            var10000 = this.argumentRowIdsForReducers();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselData;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof MorselData )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        label56:
                        {
                            MorselData var4 = (MorselData) x$1;
                            IndexedSeq var10000 = this.morsels();
                            IndexedSeq var5 = var4.morsels();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label56;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label56;
                            }

                            ArgumentStream var7 = this.argumentStream();
                            ArgumentStream var6 = var4.argumentStream();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label56;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label56;
                            }

                            if ( this.argumentRowIdsForReducers() == var4.argumentRowIdsForReducers() && var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label47;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label65;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
