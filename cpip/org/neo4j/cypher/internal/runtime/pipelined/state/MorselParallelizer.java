package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface MorselParallelizer
{
    MorselExecutionContext originalForClosing();

    MorselExecutionContext nextCopy();
}
