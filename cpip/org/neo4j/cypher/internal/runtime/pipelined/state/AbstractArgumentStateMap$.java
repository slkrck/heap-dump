package org.neo4j.cypher.internal.runtime.pipelined.state;

public final class AbstractArgumentStateMap$
{
    public static AbstractArgumentStateMap$ MODULE$;

    static
    {
        new AbstractArgumentStateMap$();
    }

    private AbstractArgumentStateMap$()
    {
        MODULE$ = this;
    }
}
