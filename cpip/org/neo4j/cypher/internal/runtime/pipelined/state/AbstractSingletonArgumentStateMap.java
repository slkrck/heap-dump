package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.physicalplanning.TopLevelArgument$;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Function1;
import scala.Function2;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class AbstractSingletonArgumentStateMap<STATE extends ArgumentStateMap.ArgumentState, CONTROLLER extends AbstractArgumentStateMap.StateController<STATE>>
        implements ArgumentStateMapWithArgumentIdCounter<STATE>, ArgumentStateMapWithoutArgumentIdCounter<STATE>
{
    public int argumentSlotOffset()
    {
        return TopLevelArgument$.MODULE$.SLOT_OFFSET();
    }

    public abstract CONTROLLER controller();

    public abstract void controller_$eq( final CONTROLLER x$1 );

    public abstract boolean hasController();

    public abstract void hasController_$eq( final boolean x$1 );

    public abstract long lastCompletedArgumentId();

    public abstract void lastCompletedArgumentId_$eq( final long x$1 );

    public abstract CONTROLLER newStateController( final long argument, final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers );

    public void update( final long argumentRowId, final Function1<STATE,BoxedUnit> onState )
    {
        TopLevelArgument$.MODULE$.assertTopLevelArgument( argumentRowId );
        onState.apply( this.controller().state() );
    }

    public void clearAll( final Function1<STATE,BoxedUnit> f )
    {
        if ( this.hasController() && this.controller().take() )
        {
            f.apply( this.controller().state() );
        }
    }

    public <U> void filter( final MorselExecutionContext morsel, final Function2<STATE,Object,U> onArgument,
            final Function2<U,MorselExecutionContext,Object> onRow )
    {
        Object filterState = onArgument.apply( this.controller().state(), BoxesRunTime.boxToLong( (long) morsel.getValidRows() ) );
        ArgumentStateMap$.MODULE$.filter( morsel, ( row ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$filter$1( onRow, filterState, row ) );
        } );
    }

    public STATE takeOneCompleted()
    {
        if ( this.hasController() && this.controller().tryTake() )
        {
            ArgumentStateMap.ArgumentState completedState = this.controller().state();
            this.hasController_$eq( false );
         .MODULE$.ASM().log( "ASM %s take %03d", new ArgumentStateMapId( this.argumentStateMapId() ),
                BoxesRunTime.boxToLong( completedState.argumentRowId() ) );
            return completedState;
        }
        else
        {
            return null;
        }
    }

    public ArgumentStateMap.ArgumentStateWithCompleted<STATE> takeNextIfCompletedOrElsePeek()
    {
        ArgumentStateMap.ArgumentStateWithCompleted var10000;
        if ( this.hasController() && this.controller().tryTake() )
        {
            this.lastCompletedArgumentId_$eq( TopLevelArgument$.MODULE$.VALUE() );
            ArgumentStateMap.ArgumentState completedState = this.controller().state();
            this.hasController_$eq( false );
            var10000 = new ArgumentStateMap.ArgumentStateWithCompleted( completedState, true );
        }
        else
        {
            var10000 = this.hasController() ? new ArgumentStateMap.ArgumentStateWithCompleted( this.controller().state(), false ) : null;
        }

        return var10000;
    }

    public boolean nextArgumentStateIsCompletedOr( final Function1<STATE,Object> statePredicate )
    {
        return this.hasController() && (this.controller().isZero() || BoxesRunTime.unboxToBoolean( statePredicate.apply( this.controller().state() ) ));
    }

    public STATE peekNext()
    {
        return this.peek( this.lastCompletedArgumentId() + 1L );
    }

    public Iterator<STATE> peekCompleted()
    {
        return this.hasController() && this.controller().isZero() ? scala.package..MODULE$.Iterator().single( this.controller().state() ) :scala.package..
        MODULE$.Iterator().empty();
    }

    public STATE peek( final long argumentId )
    {
        return argumentId == TopLevelArgument$.MODULE$.VALUE() ? this.controller().state() : null;
    }

    public boolean hasCompleted()
    {
        return this.hasController() && this.controller().isZero();
    }

    public boolean hasCompleted( final long argument )
    {
        return argument == TopLevelArgument$.MODULE$.VALUE() && this.controller() != null && this.controller().isZero();
    }

    public boolean remove( final long argument )
    {
      .MODULE$.ASM().log( "ASM %s rem %03d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ) );
        boolean var10000;
        if ( argument == TopLevelArgument$.MODULE$.VALUE() )
        {
            this.hasController_$eq( false );
            var10000 = true;
        }
        else
        {
            var10000 = false;
        }

        return var10000;
    }

    public void initiate( final long argument, final MorselExecutionContext argumentMorsel, final long[] argumentRowIdsForReducers )
    {
        TopLevelArgument$.MODULE$.assertTopLevelArgument( argument );
      .MODULE$.ASM().log( "ASM %s init %03d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ) );
        this.controller_$eq( this.newStateController( argument, argumentMorsel, argumentRowIdsForReducers ) );
    }

    public void increment( final long argument )
    {
        TopLevelArgument$.MODULE$.assertTopLevelArgument( argument );
        long newCount = this.controller().increment();
      .MODULE$.ASM().log( "ASM %s incr %03d to %d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ),
            BoxesRunTime.boxToLong( newCount ) );
    }

    public STATE decrement( final long argument )
    {
        TopLevelArgument$.MODULE$.assertTopLevelArgument( argument );
        long newCount = this.controller().decrement();
      .MODULE$.ASM().log( "ASM %s decr %03d to %d", new ArgumentStateMapId( this.argumentStateMapId() ), BoxesRunTime.boxToLong( argument ),
            BoxesRunTime.boxToLong( newCount ) );
        return newCount == 0L ? this.controller().state() : null;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.$plus$plus$eq( "ArgumentStateMap(\n" );
        sb.$plus$plus$eq(
                (new java.lang.StringBuilder( 6 )).append( "0 -> " ).append( this.hasController() ? this.controller() : null ).append( "\n" ).toString() );
        sb.$plus$eq( ')' );
        return sb.result();
    }
}
