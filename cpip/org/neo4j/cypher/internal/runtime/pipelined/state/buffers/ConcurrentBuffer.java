package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import scala.Function1;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ConcurrentBuffer<T> implements Buffer<T>
{
    private final ConcurrentLinkedQueue<T> data = new ConcurrentLinkedQueue();
    private final AtomicInteger size = new AtomicInteger( 0 );

    private ConcurrentLinkedQueue<T> data()
    {
        return this.data;
    }

    private AtomicInteger size()
    {
        return this.size;
    }

    public boolean hasData()
    {
        return !this.data().isEmpty();
    }

    public void put( final T t )
    {
        this.size().incrementAndGet();
        this.data().add( t );
    }

    public boolean canPut()
    {
        return this.size().get() < Buffer$.MODULE$.MAX_SIZE_HINT();
    }

    public T take()
    {
        Object taken = this.data().poll();
        if ( taken != null )
        {
            BoxesRunTime.boxToInteger( this.size().decrementAndGet() );
        }
        else
        {
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        return taken;
    }

    public void foreach( final Function1<T,BoxedUnit> f )
    {
        this.data().forEach( ( t ) -> {
            f.apply( t );
        } );
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.$plus$plus$eq( "ConcurrentBuffer(" );
        this.data().forEach( ( t ) -> {
            sb.$plus$plus$eq( t.toString() );
            sb.$plus$eq( ',' );
        } );
        sb.$plus$eq( ')' );
        return sb.result();
    }

    public Iterator<T> iterator()
    {
        return this.data().iterator();
    }
}
