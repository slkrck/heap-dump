package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation;
import org.neo4j.exceptions.TransactionOutOfMemoryException;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class BoundedConcurrentBuffer<T extends WithHeapUsageEstimation> extends ConcurrentBuffer<T>
{
    private final int bound;
    private final AtomicLong heapSize;

    public BoundedConcurrentBuffer( final int bound )
    {
        this.bound = bound;
        this.heapSize = new AtomicLong( 0L );
    }

    private AtomicLong heapSize()
    {
        return this.heapSize;
    }

    public void put( final T t )
    {
        long _size = this.heapSize().addAndGet( t.estimatedHeapUsage() );
        if ( _size > (long) this.bound )
        {
            throw new TransactionOutOfMemoryException();
        }
        else
        {
            super.put( t );
        }
    }

    public T take()
    {
        WithHeapUsageEstimation t = (WithHeapUsageEstimation) super.take();
        if ( t != null )
        {
            BoxesRunTime.boxToLong( this.heapSize().addAndGet( -t.estimatedHeapUsage() ) );
        }
        else
        {
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        return t;
    }
}
