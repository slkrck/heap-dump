package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

public final class MorselBuffer$
{
    public static MorselBuffer$ MODULE$;

    static
    {
        new MorselBuffer$();
    }

    private final long org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID;
    private final int org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_SLOT_OFFSET;

    private MorselBuffer$()
    {
        MODULE$ = this;
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID = -1L;
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_SLOT_OFFSET = -2;
    }

    public long org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID;
    }

    public int org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_SLOT_OFFSET()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_SLOT_OFFSET;
    }
}
