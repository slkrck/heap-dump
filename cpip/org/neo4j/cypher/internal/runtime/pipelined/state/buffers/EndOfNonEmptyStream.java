package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class EndOfNonEmptyStream
{
    public static String toString()
    {
        return EndOfNonEmptyStream$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return EndOfNonEmptyStream$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return EndOfNonEmptyStream$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return EndOfNonEmptyStream$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return EndOfNonEmptyStream$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return EndOfNonEmptyStream$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return EndOfNonEmptyStream$.MODULE$.productPrefix();
    }
}
