package org.neo4j.cypher.internal.runtime.pipelined.state;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class ConcurrentArgumentStateMap<STATE extends ArgumentStateMap.ArgumentState>
        extends AbstractArgumentStateMap<STATE,AbstractArgumentStateMap.StateController<STATE>>
{
    private final int argumentStateMapId;
    private final int argumentSlotOffset;
    private final ArgumentStateMap.ArgumentStateFactory<STATE> factory;
    private final ConcurrentHashMap<Object,AbstractArgumentStateMap.StateController<STATE>> controllers;
    private volatile long lastCompletedArgumentId;

    public ConcurrentArgumentStateMap( final int argumentStateMapId, final int argumentSlotOffset, final ArgumentStateMap.ArgumentStateFactory<STATE> factory )
    {
        this.argumentStateMapId = argumentStateMapId;
        this.argumentSlotOffset = argumentSlotOffset;
        this.factory = factory;
        this.controllers = new ConcurrentHashMap();
        this.lastCompletedArgumentId = -1L;
    }

    public int argumentStateMapId()
    {
        return this.argumentStateMapId;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public ConcurrentHashMap<Object,AbstractArgumentStateMap.StateController<STATE>> controllers()
    {
        return this.controllers;
    }

    public long lastCompletedArgumentId()
    {
        return this.lastCompletedArgumentId;
    }

    public void lastCompletedArgumentId_$eq( final long x$1 )
    {
        this.lastCompletedArgumentId = x$1;
    }

    public AbstractArgumentStateMap.StateController<STATE> newStateController( final long argument, final MorselExecutionContext argumentMorsel,
            final long[] argumentRowIdsForReducers )
    {
        return (AbstractArgumentStateMap.StateController) (this.factory.completeOnConstruction() ? new AbstractArgumentStateMap.ImmutableStateController(
                this.factory.newConcurrentArgumentState( argument, argumentMorsel, argumentRowIdsForReducers ) )
                                                                                                 : new ConcurrentArgumentStateMap.ConcurrentStateController(
                                                                                                         this.factory.newConcurrentArgumentState( argument,
                                                                                                                 argumentMorsel, argumentRowIdsForReducers ) ));
    }

    public static class ConcurrentStateController<STATE extends ArgumentStateMap.ArgumentState> implements AbstractArgumentStateMap.StateController<STATE>
    {
        private final STATE state;
        private final AtomicLong count;

        public ConcurrentStateController( final STATE state )
        {
            this.state = state;
            this.count = new AtomicLong( 1L );
        }

        public STATE state()
        {
            return this.state;
        }

        private AtomicLong count()
        {
            return this.count;
        }

        public long increment()
        {
            return this.count().incrementAndGet();
        }

        public long decrement()
        {
            return this.count().decrementAndGet();
        }

        public boolean tryTake()
        {
            return this.count().compareAndSet( 0L,
                    (long) ConcurrentArgumentStateMap$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentArgumentStateMap$$TAKEN() );
        }

        public boolean take()
        {
            return this.count().getAndSet(
                    (long) ConcurrentArgumentStateMap$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$ConcurrentArgumentStateMap$$TAKEN() ) >= 0L;
        }

        public boolean isZero()
        {
            return this.count().get() == 0L;
        }

        public String toString()
        {
            return (new StringBuilder( 18 )).append( "[count: " ).append( this.count().get() ).append( ", state: " ).append( this.state() ).append(
                    "]" ).toString();
        }
    }
}
