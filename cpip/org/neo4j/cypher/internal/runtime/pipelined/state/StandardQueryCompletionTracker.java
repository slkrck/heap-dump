package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryStatistics.;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer;
import org.neo4j.cypher.internal.v4_0.util.AssertionRunner.Thunk;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import scala.Option;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class StandardQueryCompletionTracker implements QueryCompletionTracker
{
    private final QuerySubscriber subscriber;
    private final QueryContext queryContext;
    private final QueryExecutionTracer tracer;
    private final ArrayBuffer<Thunk> assertions;
    private final String org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName;
    private long count;
    private Throwable throwable;
    private long demand;
    private boolean cancelled;
    private boolean _hasEnded;

    public StandardQueryCompletionTracker( final QuerySubscriber subscriber, final QueryContext queryContext, final QueryExecutionTracer tracer )
    {
        this.subscriber = subscriber;
        this.queryContext = queryContext;
        this.tracer = tracer;
        QueryCompletionTracker.$init$( this );
        this.count = 0L;
        this.demand = 0L;
        this.cancelled = false;
        this._hasEnded = false;
    }

    public void addCompletionAssertion( final Thunk assertion )
    {
        QueryCompletionTracker.addCompletionAssertion$( this, assertion );
    }

    public void runAssertions()
    {
        QueryCompletionTracker.runAssertions$( this );
    }

    public void debug( final String str )
    {
        QueryCompletionTracker.debug$( this, str );
    }

    public void debug( final String str, final Object x )
    {
        QueryCompletionTracker.debug$( this, str, x );
    }

    public void debug( final String str, final Object x1, final Object x2 )
    {
        QueryCompletionTracker.debug$( this, str, x1, x2 );
    }

    public void debug( final String str, final Object x1, final Object x2, final Object x3 )
    {
        QueryCompletionTracker.debug$( this, str, x1, x2, x3 );
    }

    public ArrayBuffer<Thunk> assertions()
    {
        return this.assertions;
    }

    public String org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName()
    {
        return this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName;
    }

    public void org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$assertions_$eq( final ArrayBuffer<Thunk> x$1 )
    {
        this.assertions = x$1;
    }

    public final void org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName_$eq(
            final String x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName = x$1;
    }

    private long count()
    {
        return this.count;
    }

    private void count_$eq( final long x$1 )
    {
        this.count = x$1;
    }

    private Throwable throwable()
    {
        return this.throwable;
    }

    private void throwable_$eq( final Throwable x$1 )
    {
        this.throwable = x$1;
    }

    private long demand()
    {
        return this.demand;
    }

    private void demand_$eq( final long x$1 )
    {
        this.demand = x$1;
    }

    private boolean cancelled()
    {
        return this.cancelled;
    }

    private void cancelled_$eq( final boolean x$1 )
    {
        this.cancelled = x$1;
    }

    private boolean _hasEnded()
    {
        return this._hasEnded;
    }

    private void _hasEnded_$eq( final boolean x$1 )
    {
        this._hasEnded = x$1;
    }

    public void increment()
    {
        this.count_$eq( this.count() + 1L );
        this.debug( "Incremented to %d", BoxesRunTime.boxToLong( this.count() ) );
    }

    public void incrementBy( final long n )
    {
        if ( n != 0L )
        {
            this.count_$eq( this.count() + n );
            this.debug( "Incremented by %d to %d", BoxesRunTime.boxToLong( n ), BoxesRunTime.boxToLong( this.count() ) );
        }
    }

    public void decrement()
    {
        this.count_$eq( this.count() - 1L );
        this.debug( "Decremented to %d", BoxesRunTime.boxToLong( this.count() ) );
        if ( this.count() < 0L )
        {
            throw new IllegalStateException( (new StringBuilder( 33 )).append( "Should not decrement below zero: " ).append( this.count() ).toString() );
        }
        else
        {
            this.postDecrement();
        }
    }

    public void decrementBy( final long n )
    {
        if ( n != 0L )
        {
            this.count_$eq( this.count() - n );
            this.debug( "Decremented by %d to %d", BoxesRunTime.boxToLong( n ), BoxesRunTime.boxToLong( this.count() ) );
            this.postDecrement();
        }
    }

    private void postDecrement()
    {
        if ( this.count() <= 0L )
        {
            if ( this.count() < 0L )
            {
                this.error( new ReferenceCountingException(
                        (new StringBuilder( 36 )).append( "Cannot count below 0, but got count " ).append( this.count() ).toString() ) );
            }

            if ( !this._hasEnded() )
            {
                try
                {
                    if ( this.throwable() != null )
                    {
                        this.subscriber.onError( this.throwable() );
                    }
                    else if ( !this.cancelled() )
                    {
                        this.subscriber.onResultCompleted( (QueryStatistics) this.queryContext.getOptStatistics().getOrElse( () -> {
                            return new org.neo4j.cypher.internal.runtime.QueryStatistics(.MODULE$.apply$default$1(), .MODULE$.apply$default$2(), .
                            MODULE$.apply$default$3(), .MODULE$.apply$default$4(), .MODULE$.apply$default$5(), .MODULE$.apply$default$6(), .
                            MODULE$.apply$default$7(), .MODULE$.apply$default$8(), .MODULE$.apply$default$9(), .MODULE$.apply$default$10(), .
                            MODULE$.apply$default$11(), .MODULE$.apply$default$12(), .MODULE$.apply$default$13(), .MODULE$.apply$default$14(), .
                            MODULE$.apply$default$15(), .MODULE$.apply$default$16(), .MODULE$.apply$default$17());
                        } ) );
                    }
                }
                catch ( Throwable var6 )
                {
                    Option var4 = org.neo4j.cypher.internal.NonFatalCypherError..MODULE$.unapply( var6 );
                    if ( var4.isEmpty() )
                    {
                        throw var6;
                    }

                    Throwable reportError = (Throwable) var4.get();
                    this.error( reportError );
                    BoxedUnit var1 = BoxedUnit.UNIT;
                }

                this.tracer.stopQuery();
                this._hasEnded_$eq( true );
            }
        }
    }

    public void error( final Throwable throwable )
    {
        if ( this.throwable() == null )
        {
            this.throwable_$eq( throwable );
        }
        else
        {
            Throwable var10000 = this.throwable();
            if ( var10000 == null )
            {
                if ( throwable == null )
                {
                    return;
                }
            }
            else if ( var10000.equals( throwable ) )
            {
                return;
            }

            this.throwable().addSuppressed( throwable );
        }
    }

    public long getDemand()
    {
        return this.demand();
    }

    public boolean hasDemand()
    {
        return this.getDemand() > 0L;
    }

    public void addServed( final long newlyServed )
    {
        this.demand_$eq( this.demand() - newlyServed );
    }

    public boolean hasEnded()
    {
        return this._hasEnded();
    }

    public void request( final long numberOfRecords )
    {
        long newDemand = this.demand() + numberOfRecords;
        this.demand_$eq( newDemand < 0L ? Long.MAX_VALUE : newDemand );
    }

    public void cancel()
    {
        this.cancelled_$eq( true );
    }

    public boolean await()
    {
        if ( this.throwable() != null )
        {
            throw this.throwable();
        }
        else if ( this.count() != 0L && !this.cancelled() && this.demand() > 0L )
        {
            throw new IllegalStateException( (new StringOps( scala.Predef..MODULE$.augmentString( (new StringBuilder( 121 )).append(
                    "Should not reach await until tracking is complete, cancelled or out-of demand,\n           |count: " ).append( this.count() ).append(
                    ", cancelled: " ).append( this.cancelled() ).append( ", demand: " ).append( this.demand() ).toString() )) ).stripMargin());
        }
        else
        {
            boolean moreToCome = this.count() > 0L && !this.cancelled();
            if ( !moreToCome )
            {
                this.runAssertions();
            }

            return moreToCome;
        }
    }

    public String toString()
    {
        return (new StringBuilder( 32 )).append( "StandardQueryCompletionTracker(" ).append( this.count() ).append( ")" ).toString();
    }
}
