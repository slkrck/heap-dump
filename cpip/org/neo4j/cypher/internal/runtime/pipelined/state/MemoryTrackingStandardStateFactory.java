package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.BoundedMemoryTracker;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer;
import org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MemoryTrackingStandardBuffer;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class MemoryTrackingStandardStateFactory extends StandardStateFactory
{
    private final QueryMemoryTracker memoryTracker;

    public MemoryTrackingStandardStateFactory( final long transactionMaxMemory )
    {
        this.memoryTracker = new BoundedMemoryTracker( transactionMaxMemory );
    }

    public QueryMemoryTracker memoryTracker()
    {
        return this.memoryTracker;
    }

    public <T extends WithHeapUsageEstimation> Buffer<T> newBuffer()
    {
        return new MemoryTrackingStandardBuffer( this.memoryTracker() );
    }
}
