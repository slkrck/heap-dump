package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

public final class Buffers$
{
    public static Buffers$ MODULE$;

    static
    {
        new Buffers$();
    }

    private Buffers$()
    {
        MODULE$ = this;
    }
}
