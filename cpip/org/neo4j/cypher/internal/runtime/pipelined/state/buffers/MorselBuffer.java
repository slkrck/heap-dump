package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import java.util.Arrays;
import java.util.BitSet;

import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.physicalplanning.BufferId;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.FilteringMorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentCountUpdater;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer;
import org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker;
import org.neo4j.util.Preconditions;
import scala.collection.IndexedSeq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class MorselBuffer extends ArgumentCountUpdater
        implements Sink<MorselExecutionContext>, Source<MorselParallelizer>, Buffers.SinkByOrigin, Buffers.DataHolder
{
    public final QueryCompletionTracker org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$tracker;
    public final IndexedSeq<Buffers.AccumulatingBuffer> org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers;
    private final int id;
    private final IndexedSeq<ArgumentStateMapId> workCancellers;
    private final ArgumentStateMap.ArgumentStateMaps argumentStateMaps;
    private final Buffer<MorselExecutionContext> inner;
    private final ArgumentStateMap<ArgumentStateMap.WorkCanceller>[] cancellerASMs;

    public MorselBuffer( final int id, final QueryCompletionTracker tracker, final IndexedSeq<Buffers.AccumulatingBuffer> downstreamArgumentReducers,
            final IndexedSeq<ArgumentStateMapId> workCancellers, final ArgumentStateMap.ArgumentStateMaps argumentStateMaps,
            final Buffer<MorselExecutionContext> inner )
    {
        this.id = id;
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$tracker = tracker;
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers = downstreamArgumentReducers;
        this.workCancellers = workCancellers;
        this.argumentStateMaps = argumentStateMaps;
        this.inner = inner;
        ArgumentStateMap[] x = new ArgumentStateMap[workCancellers.size()];

        for ( int i = 0; i < workCancellers.size(); ++i )
        {
            x[i] = argumentStateMaps.apply( ((ArgumentStateMapId) workCancellers.apply( i )).x() );
        }

        this.cancellerASMs = x;
    }

    public ArgumentStateMap.ArgumentStateMaps argumentStateMaps()
    {
        return this.argumentStateMaps;
    }

    private ArgumentStateMap<ArgumentStateMap.WorkCanceller>[] cancellerASMs()
    {
        return this.cancellerASMs;
    }

    public <T> Sink<T> sinkFor( final int fromPipeline )
    {
        return this;
    }

    public void put( final MorselExecutionContext morsel )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[put]   " ).append( this ).append( " <- " ).append( morsel ).toString() );
    }

        if ( morsel.hasData() )
        {
            morsel.resetToFirstRow();
            this.incrementArgumentCounts( this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers, morsel );
            this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$tracker.increment();
            this.inner.put( morsel );
        }
    }

    public boolean canPut()
    {
        return this.inner.canPut();
    }

    public void putInDelegate( final MorselExecutionContext morsel )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 20 )).append( "[putInDelegate] " ).append( this ).append( " <- " ).append( morsel ).toString() );
    }

        if ( morsel.hasData() )
        {
            morsel.resetToFirstRow();
            this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$tracker.increment();
            this.inner.put( morsel );
        }
    }

    public boolean hasData()
    {
        return this.inner.hasData();
    }

    public MorselParallelizer take()
    {
        MorselExecutionContext morsel = (MorselExecutionContext) this.inner.take();
        MorselBuffer.Parallelizer var10000;
        if ( morsel == null )
        {
            var10000 = null;
        }
        else
        {
            if (.MODULE$.BUFFERS().enabled()){
            .MODULE$.BUFFERS().log( (new StringBuilder( 12 )).append( "[take]  " ).append( this ).append( " -> " ).append( morsel ).toString() );
        }

            var10000 = new MorselBuffer.Parallelizer( this, morsel );
        }

        return var10000;
    }

    public void clearAll()
    {
        for ( MorselExecutionContext morsel = (MorselExecutionContext) this.inner.take(); morsel != null; morsel = (MorselExecutionContext) this.inner.take() )
        {
            this.close( morsel );
        }
    }

    public boolean filterCancelledArguments( final MorselExecutionContext morsel )
    {
        if ( this.workCancellers.nonEmpty() )
        {
            int currentRow = morsel.getCurrentRow();
            Preconditions.checkArgument( morsel instanceof FilteringMorselExecutionContext,
                    (new StringBuilder( 75 )).append( "Expected filtering morsel for filterCancelledArguments in buffer " ).append(
                            new BufferId( this.id ) ).append( ", but got " ).append( morsel.getClass() ).toString() );
            FilteringMorselExecutionContext filteringMorsel = (FilteringMorselExecutionContext) morsel;
            filteringMorsel.resetToFirstRow();
            BitSet rowsToCancel = this.determineCancelledRows( filteringMorsel );
            if ( !rowsToCancel.isEmpty() )
            {
                this.decrementReducers( filteringMorsel, rowsToCancel );
                filteringMorsel.cancelRows( rowsToCancel );
            }

            filteringMorsel.moveToRawRow( currentRow );
        }

        return morsel.isEmpty();
    }

    private void decrementReducers( final FilteringMorselExecutionContext filteringMorsel, final BitSet rowsToCancel )
    {
        filteringMorsel.resetToFirstRow();
        long[] reducerArgumentRowIds = new long[this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers.size()];
        Arrays.fill( reducerArgumentRowIds,
                MorselBuffer$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID() );
        BitSet reducerDecrementFlags =
                new BitSet( this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers.size() );

        while ( filteringMorsel.isValidRow() )
        {
            for ( int j = 0; j < this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers.size(); ++j )
            {
                Buffers.AccumulatingBuffer reducer =
                        (Buffers.AccumulatingBuffer) this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers.apply(
                                j );
                int reducerArgumentSlotOffset = reducer.argumentSlotOffset();
                long currentReducerArgumentRowId = reducerArgumentRowIds[j];
                long nextReducerArgumentRowId = filteringMorsel.isValidRow() ? filteringMorsel.getArgumentAt( reducerArgumentSlotOffset )
                                                                             : MorselBuffer$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID();
                reducerArgumentRowIds[j] = nextReducerArgumentRowId;
                if ( !filteringMorsel.isValidRow() || currentReducerArgumentRowId !=
                        MorselBuffer$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID() &&
                        currentReducerArgumentRowId != nextReducerArgumentRowId )
                {
                    if ( !reducerDecrementFlags.get( j ) )
                    {
                        reducer.decrement( currentReducerArgumentRowId );
                    }

                    reducerDecrementFlags.clear( j );
                }

                if ( !rowsToCancel.get( filteringMorsel.getCurrentRow() ) )
                {
                    reducerDecrementFlags.set( j );
                }
            }

            filteringMorsel.moveToNextRow();
        }

        for ( int j = 0; j < this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers.size(); ++j )
        {
            Buffers.AccumulatingBuffer reducer =
                    (Buffers.AccumulatingBuffer) this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers.apply(
                            j );
            if ( !reducerDecrementFlags.get( j ) && reducerArgumentRowIds[j] !=
                    MorselBuffer$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID() )
            {
                reducer.decrement( reducerArgumentRowIds[j] );
            }
        }
    }

    private BitSet determineCancelledRows( final FilteringMorselExecutionContext filteringMorsel )
    {
        BitSet rowsToCancel = new BitSet( filteringMorsel.maxNumberOfRows() );

        while ( true )
        {
            while ( filteringMorsel.isValidRow() )
            {
                int i = 0;
                boolean isCancelled = false;
                long cancellerArgumentRowId =
                        -MorselBuffer$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_ROW_ID();

                int cancellerArgumentSlotOffset;
                for ( cancellerArgumentSlotOffset =
                        MorselBuffer$.MODULE$.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$INVALID_ARG_SLOT_OFFSET();
                        i < this.workCancellers.size() && !isCancelled; ++i )
                {
                    ArgumentStateMap cancellerASM = this.cancellerASMs()[i];
                    cancellerArgumentRowId = filteringMorsel.getArgumentAt( cancellerASM.argumentSlotOffset() );
                    if ( ((ArgumentStateMap.WorkCanceller) cancellerASM.peek( cancellerArgumentRowId )).isCancelled() )
                    {
                        cancellerArgumentSlotOffset = cancellerASM.argumentSlotOffset();
                        isCancelled = true;
                    }
                }

                if ( isCancelled )
                {
                    while ( true )
                    {
                        rowsToCancel.set( filteringMorsel.getCurrentRow() );
                        filteringMorsel.moveToNextRow();
                        if ( !filteringMorsel.isValidRow() || filteringMorsel.getArgumentAt( cancellerArgumentSlotOffset ) != cancellerArgumentRowId )
                        {
                            break;
                        }
                    }
                }
                else
                {
                    filteringMorsel.moveToNextRow();
                }
            }

            return rowsToCancel;
        }
    }

    public void close( final MorselExecutionContext morsel )
    {
        if (.MODULE$.BUFFERS().enabled()){
         .MODULE$.BUFFERS().log( (new StringBuilder( 13 )).append( "[close] " ).append( this ).append( " -X- " ).append( morsel ).toString() );
    }

        this.decrementArgumentCounts( this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers, morsel );
        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$tracker.decrement();
    }

    public String toString()
    {
        return (new StringBuilder( 16 )).append( "MorselBuffer(" ).append( new BufferId( this.id ) ).append( ", " ).append( this.inner ).append(
                ")" ).toString();
    }

    public class Parallelizer implements MorselParallelizer
    {
        private final MorselExecutionContext original;
        private boolean usedOriginal;

        public Parallelizer( final MorselBuffer $outer, final MorselExecutionContext original )
        {
            this.original = original;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                this.usedOriginal = false;
            }
        }

        private boolean usedOriginal()
        {
            return this.usedOriginal;
        }

        private void usedOriginal_$eq( final boolean x$1 )
        {
            this.usedOriginal = x$1;
        }

        public MorselExecutionContext originalForClosing()
        {
            return this.original;
        }

        public MorselExecutionContext nextCopy()
        {
            MorselExecutionContext var10000;
            if ( !this.usedOriginal() )
            {
                this.usedOriginal_$eq( true );
                var10000 = this.original;
            }
            else
            {
                MorselExecutionContext copy = this.original.shallowCopy();
                this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$Parallelizer$$$outer().incrementArgumentCounts(
                        this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$Parallelizer$$$outer().org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$downstreamArgumentReducers,
                        copy );
                this.org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$Parallelizer$$$outer().org$neo4j$cypher$internal$runtime$pipelined$state$buffers$MorselBuffer$$tracker.increment();
                var10000 = copy;
            }

            return var10000;
        }
    }
}
