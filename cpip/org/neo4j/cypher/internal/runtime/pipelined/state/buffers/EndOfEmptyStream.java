package org.neo4j.cypher.internal.runtime.pipelined.state.buffers;

import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class EndOfEmptyStream implements EndOfStream, Product, Serializable
{
    private final MorselExecutionContext viewOfArgumentRow;

    public EndOfEmptyStream( final MorselExecutionContext viewOfArgumentRow )
    {
        this.viewOfArgumentRow = viewOfArgumentRow;
        Product.$init$( this );
    }

    public static Option<MorselExecutionContext> unapply( final EndOfEmptyStream x$0 )
    {
        return EndOfEmptyStream$.MODULE$.unapply( var0 );
    }

    public static EndOfEmptyStream apply( final MorselExecutionContext viewOfArgumentRow )
    {
        return EndOfEmptyStream$.MODULE$.apply( var0 );
    }

    public static <A> Function1<MorselExecutionContext,A> andThen( final Function1<EndOfEmptyStream,A> g )
    {
        return EndOfEmptyStream$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,EndOfEmptyStream> compose( final Function1<A,MorselExecutionContext> g )
    {
        return EndOfEmptyStream$.MODULE$.compose( var0 );
    }

    public MorselExecutionContext viewOfArgumentRow()
    {
        return this.viewOfArgumentRow;
    }

    public EndOfEmptyStream copy( final MorselExecutionContext viewOfArgumentRow )
    {
        return new EndOfEmptyStream( viewOfArgumentRow );
    }

    public MorselExecutionContext copy$default$1()
    {
        return this.viewOfArgumentRow();
    }

    public String productPrefix()
    {
        return "EndOfEmptyStream";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.viewOfArgumentRow();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EndOfEmptyStream;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof EndOfEmptyStream )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        EndOfEmptyStream var4 = (EndOfEmptyStream) x$1;
                        MorselExecutionContext var10000 = this.viewOfArgumentRow();
                        MorselExecutionContext var5 = var4.viewOfArgumentRow();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
