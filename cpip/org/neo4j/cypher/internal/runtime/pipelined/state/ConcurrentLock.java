package org.neo4j.cypher.internal.runtime.pipelined.state;

import java.util.concurrent.atomic.AtomicBoolean;

import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ConcurrentLock implements Lock
{
    private final String id;
    private final AtomicBoolean isLocked;

    public ConcurrentLock( final String id )
    {
        this.id = id;
        this.isLocked = new AtomicBoolean( false );
    }

    public String id()
    {
        return this.id;
    }

    private AtomicBoolean isLocked()
    {
        return this.isLocked;
    }

    public boolean tryLock()
    {
        boolean success = this.isLocked().compareAndSet( false, true );
        if (.MODULE$.LOCKS().enabled()){
         .MODULE$.LOCKS().log( "%s tried locking %s. Success: %s", BoxesRunTime.boxToLong( Thread.currentThread().getId() ), this.name(),
                BoxesRunTime.boxToBoolean( success ) );
    }

        return success;
    }

    public void unlock()
    {
        if ( !this.isLocked().compareAndSet( true, false ) )
        {
            throw new IllegalStateException(
                    (new StringBuilder( 41 )).append( Thread.currentThread().getId() ).append( " tried to release " ).append( this.name() ).append(
                            " that was not acquired." ).toString() );
        }
        else
        {
            if (.MODULE$.LOCKS().enabled()){
            .MODULE$.LOCKS().log( "%s unlocked %s.", BoxesRunTime.boxToLong( Thread.currentThread().getId() ), this.name() );
        }
        }
    }

    private String name()
    {
        return (new StringBuilder( 2 )).append( this.getClass().getSimpleName() ).append( "[" ).append( this.id() ).append( "]" ).toString();
    }

    public String toString()
    {
        return (new StringBuilder( 9 )).append( this.name() ).append( ": Locked=" ).append( this.isLocked().get() ).toString();
    }
}
