package org.neo4j.cypher.internal.runtime.pipelined.state;

import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.FlowControl;
import org.neo4j.cypher.internal.v4_0.util.AssertionRunner;
import org.neo4j.cypher.internal.v4_0.util.AssertionRunner.Thunk;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public interface QueryCompletionTracker extends FlowControl
{
    static void $init$( final QueryCompletionTracker $this )
    {
        $this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$assertions_$eq(
                AssertionRunner.isAssertionsEnabled() ? new ArrayBuffer() : null );
        $this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName_$eq(.MODULE$.TRACKER().enabled()
        ? (new StringBuilder( 4 )).append( "[" ).append( $this.getClass().getSimpleName() ).append( "@" ).append( System.identityHashCode( $this ) ).append(
                "] " ).toString() : "");
    }

    void org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$assertions_$eq( final ArrayBuffer<Thunk> x$1 );

    void org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$_setter_$org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName_$eq(
            final String x$1 );

    void increment();

    void incrementBy( final long n );

    void decrement();

    void decrementBy( final long n );

    void error( final Throwable throwable );

    boolean hasEnded();

    default void addCompletionAssertion( final Thunk assertion )
    {
        if ( AssertionRunner.isAssertionsEnabled() )
        {
            this.assertions().$plus$eq( assertion );
        }
    }

    ArrayBuffer<Thunk> assertions();

    default void runAssertions()
    {
        if ( AssertionRunner.isAssertionsEnabled() )
        {
            this.assertions().foreach( ( x$1 ) -> {
                $anonfun$runAssertions$1( x$1 );
                return BoxedUnit.UNIT;
            } );
        }
    }

    String org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName();

    default void debug( final String str )
    {
        if (.MODULE$.TRACKER().enabled()){
         .MODULE$.TRACKER().log(
                (new StringBuilder( 0 )).append( this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName() ).append(
                        str ).toString() );
    }
    }

    default void debug( final String str, final Object x )
    {
        if (.MODULE$.TRACKER().enabled()){
         .MODULE$.TRACKER().log(
                (new StringBuilder( 0 )).append( this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName() ).append(
                        str ).toString(), x );
    }
    }

    default void debug( final String str, final Object x1, final Object x2 )
    {
        if (.MODULE$.TRACKER().enabled()){
         .MODULE$.TRACKER().log(
                (new StringBuilder( 0 )).append( this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName() ).append(
                        str ).toString(), x1, x2 );
    }
    }

    default void debug( final String str, final Object x1, final Object x2, final Object x3 )
    {
        if (.MODULE$.TRACKER().enabled()){
         .MODULE$.TRACKER().log(
                (new StringBuilder( 0 )).append( this.org$neo4j$cypher$internal$runtime$pipelined$state$QueryCompletionTracker$$instanceName() ).append(
                        str ).toString(), x1, x2, x3 );
    }
    }
}
