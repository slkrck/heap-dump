package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation;
import org.neo4j.cypher.internal.runtime.debug.DebugSupport.;
import org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState;
import org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState;
import org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent;
import org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple6;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

@JavaDocToJava
public class PipelineTask implements Task<QueryResources>, WithHeapUsageEstimation, Product, Serializable
{
    private final ContinuableOperatorTask startTask;
    private final OperatorTask[] middleTasks;
    private final OutputOperatorState outputOperatorState;
    private final QueryContext queryContext;
    private final QueryState state;
    private final PipelineState pipelineState;
    private MorselExecutionContext _output;

    public PipelineTask( final ContinuableOperatorTask startTask, final OperatorTask[] middleTasks, final OutputOperatorState outputOperatorState,
            final QueryContext queryContext, final QueryState state, final PipelineState pipelineState )
    {
        this.startTask = startTask;
        this.middleTasks = middleTasks;
        this.outputOperatorState = outputOperatorState;
        this.queryContext = queryContext;
        this.state = state;
        this.pipelineState = pipelineState;
        WorkIdentity.$init$( this );
        Task.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple6<ContinuableOperatorTask,OperatorTask[],OutputOperatorState,QueryContext,QueryState,PipelineState>> unapply(
            final PipelineTask x$0 )
    {
        return PipelineTask$.MODULE$.unapply( var0 );
    }

    public static PipelineTask apply( final ContinuableOperatorTask startTask, final OperatorTask[] middleTasks, final OutputOperatorState outputOperatorState,
            final QueryContext queryContext, final QueryState state, final PipelineState pipelineState )
    {
        return PipelineTask$.MODULE$.apply( var0, var1, var2, var3, var4, var5 );
    }

    public static Function1<Tuple6<ContinuableOperatorTask,OperatorTask[],OutputOperatorState,QueryContext,QueryState,PipelineState>,PipelineTask> tupled()
    {
        return PipelineTask$.MODULE$.tupled();
    }

    public static Function1<ContinuableOperatorTask,Function1<OperatorTask[],Function1<OutputOperatorState,Function1<QueryContext,Function1<QueryState,Function1<PipelineState,PipelineTask>>>>>> curried()
    {
        return PipelineTask$.MODULE$.curried();
    }

    public String toString()
    {
        return Task.toString$( this );
    }

    public ContinuableOperatorTask startTask()
    {
        return this.startTask;
    }

    public OperatorTask[] middleTasks()
    {
        return this.middleTasks;
    }

    public OutputOperatorState outputOperatorState()
    {
        return this.outputOperatorState;
    }

    public QueryContext queryContext()
    {
        return this.queryContext;
    }

    public QueryState state()
    {
        return this.state;
    }

    public PipelineState pipelineState()
    {
        return this.pipelineState;
    }

    private MorselExecutionContext _output()
    {
        return this._output;
    }

    private void _output_$eq( final MorselExecutionContext x$1 )
    {
        this._output = x$1;
    }

    public PreparedOutput executeWorkUnit( final QueryResources resources, final WorkUnitEvent workUnitEvent, final QueryProfiler queryProfiler )
    {
        if ( this._output() == null )
        {
            this._output_$eq( this.pipelineState().allocateMorsel( workUnitEvent, this.state() ) );
            this.executeOperators( resources, queryProfiler );
        }

        PreparedOutput output = this.executeOutputOperator( resources, queryProfiler );
      .MODULE$.logPipelines( () -> {
        return PipelinedDebugSupport$.MODULE$.prettyWorkDone();
    } );
        return output;
    }

    private void executeOperators( final QueryResources resources, final QueryProfiler queryProfiler )
    {
      .MODULE$.logPipelines( () -> {
        return PipelinedDebugSupport$.MODULE$.prettyStartTask( this.startTask(), this.pipelineState().pipeline().start().workIdentity() );
    } );
        this.startTask().operateWithProfile( this._output(), this.queryContext(), this.state(), resources, queryProfiler );
        this._output().resetToFirstRow();
      .MODULE$.logPipelines( () -> {
        return PipelinedDebugSupport$.MODULE$.prettyPostStartTask( this.startTask() );
    } );

        for ( IntRef i = IntRef.create( 0 ); i.elem < this.middleTasks().length; ++i.elem )
        {
            OperatorTask op = this.middleTasks()[i.elem];
         .MODULE$.logPipelines( () -> {
            return PipelinedDebugSupport$.MODULE$.prettyWork( this._output(), this.pipelineState().pipeline().middleOperators()[i.elem].workIdentity() );
        } );
            op.operateWithProfile( this._output(), this.queryContext(), this.state(), resources, queryProfiler );
            this._output().resetToFirstRow();
        }
    }

    private PreparedOutput executeOutputOperator( final QueryResources resources, final QueryProfiler queryProfiler )
    {
      .MODULE$.logPipelines( () -> {
        return PipelinedDebugSupport$.MODULE$.prettyWork( this._output(), this.pipelineState().pipeline().outputOperator().workIdentity() );
    } );
        PreparedOutput preparedOutput =
                this.outputOperatorState().prepareOutputWithProfile( this._output(), this.queryContext(), this.state(), resources, queryProfiler );
        if ( !this.outputOperatorState().canContinueOutput() )
        {
            this._output_$eq( (MorselExecutionContext) null );
        }

        return preparedOutput;
    }

    public boolean filterCancelledArguments( final QueryResources resources )
    {
        boolean var10000;
        if ( this._output() == null )
        {
            boolean isCancelled = this.startTask().filterCancelledArguments( this.pipelineState() );
            if ( isCancelled )
            {
                this.close( resources );
            }

            var10000 = isCancelled;
        }
        else
        {
            var10000 = false;
        }

        return var10000;
    }

    public void close( final QueryResources resources )
    {
        this.startTask().close( this.pipelineState(), resources );
    }

    public int workId()
    {
        return this.pipelineState().pipeline().workId();
    }

    public String workDescription()
    {
        return this.pipelineState().pipeline().workDescription();
    }

    public boolean canContinue()
    {
        return this.startTask().canContinue() || this.outputOperatorState().canContinueOutput();
    }

    public long estimatedHeapUsage()
    {
        return this.startTask().estimatedHeapUsage();
    }

    public PipelineTask copy( final ContinuableOperatorTask startTask, final OperatorTask[] middleTasks, final OutputOperatorState outputOperatorState,
            final QueryContext queryContext, final QueryState state, final PipelineState pipelineState )
    {
        return new PipelineTask( startTask, middleTasks, outputOperatorState, queryContext, state, pipelineState );
    }

    public ContinuableOperatorTask copy$default$1()
    {
        return this.startTask();
    }

    public OperatorTask[] copy$default$2()
    {
        return this.middleTasks();
    }

    public OutputOperatorState copy$default$3()
    {
        return this.outputOperatorState();
    }

    public QueryContext copy$default$4()
    {
        return this.queryContext();
    }

    public QueryState copy$default$5()
    {
        return this.state();
    }

    public PipelineState copy$default$6()
    {
        return this.pipelineState();
    }

    public String productPrefix()
    {
        return "PipelineTask";
    }

    public int productArity()
    {
        return 6;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.startTask();
            break;
        case 1:
            var10000 = this.middleTasks();
            break;
        case 2:
            var10000 = this.outputOperatorState();
            break;
        case 3:
            var10000 = this.queryContext();
            break;
        case 4:
            var10000 = this.state();
            break;
        case 5:
            var10000 = this.pipelineState();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof PipelineTask;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var14;
        if ( this != x$1 )
        {
            label92:
            {
                boolean var2;
                if ( x$1 instanceof PipelineTask )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label74:
                    {
                        label73:
                        {
                            PipelineTask var4 = (PipelineTask) x$1;
                            ContinuableOperatorTask var10000 = this.startTask();
                            ContinuableOperatorTask var5 = var4.startTask();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label73;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label73;
                            }

                            if ( this.middleTasks() == var4.middleTasks() )
                            {
                                label83:
                                {
                                    OutputOperatorState var10 = this.outputOperatorState();
                                    OutputOperatorState var6 = var4.outputOperatorState();
                                    if ( var10 == null )
                                    {
                                        if ( var6 != null )
                                        {
                                            break label83;
                                        }
                                    }
                                    else if ( !var10.equals( var6 ) )
                                    {
                                        break label83;
                                    }

                                    QueryContext var11 = this.queryContext();
                                    QueryContext var7 = var4.queryContext();
                                    if ( var11 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label83;
                                        }
                                    }
                                    else if ( !var11.equals( var7 ) )
                                    {
                                        break label83;
                                    }

                                    QueryState var12 = this.state();
                                    QueryState var8 = var4.state();
                                    if ( var12 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label83;
                                        }
                                    }
                                    else if ( !var12.equals( var8 ) )
                                    {
                                        break label83;
                                    }

                                    PipelineState var13 = this.pipelineState();
                                    PipelineState var9 = var4.pipelineState();
                                    if ( var13 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label83;
                                        }
                                    }
                                    else if ( !var13.equals( var9 ) )
                                    {
                                        break label83;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var14 = true;
                                        break label74;
                                    }
                                }
                            }
                        }

                        var14 = false;
                    }

                    if ( var14 )
                    {
                        break label92;
                    }
                }

                var14 = false;
                return var14;
            }
        }

        var14 = true;
        return var14;
    }
}
