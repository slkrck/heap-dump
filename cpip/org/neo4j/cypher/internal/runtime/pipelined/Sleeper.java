package org.neo4j.cypher.internal.runtime.pipelined;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.concurrent.duration.Duration;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface Sleeper
{
    static Duration concurrentSleeper$default$2()
    {
        return Sleeper$.MODULE$.concurrentSleeper$default$2();
    }

    static Sleeper concurrentSleeper( final int workerId, final Duration sleepDuration )
    {
        return Sleeper$.MODULE$.concurrentSleeper( var0, var1 );
    }

    void reportStartWorkUnit();

    void reportStopWorkUnit();

    void reportIdle();

    boolean isSleeping();

    boolean isWorking();

    String statusString();

    public interface Status
    {
    }

    public static class ACTIVE$ implements Sleeper.Status, Product, Serializable
    {
        public static Sleeper.ACTIVE$ MODULE$;

        static
        {
            new Sleeper.ACTIVE$();
        }

        public ACTIVE$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String productPrefix()
        {
            return "ACTIVE";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Sleeper.ACTIVE$;
        }

        public int hashCode()
        {
            return 1925346054;
        }

        public String toString()
        {
            return "ACTIVE";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class SLEEPING$ implements Sleeper.Status, Product, Serializable
    {
        public static Sleeper.SLEEPING$ MODULE$;

        static
        {
            new Sleeper.SLEEPING$();
        }

        public SLEEPING$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String productPrefix()
        {
            return "SLEEPING";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Sleeper.SLEEPING$;
        }

        public int hashCode()
        {
            return -603236949;
        }

        public String toString()
        {
            return "SLEEPING";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class WORKING$ implements Sleeper.Status, Product, Serializable
    {
        public static Sleeper.WORKING$ MODULE$;

        static
        {
            new Sleeper.WORKING$();
        }

        public WORKING$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public String productPrefix()
        {
            return "WORKING";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Sleeper.WORKING$;
        }

        public int hashCode()
        {
            return -2051819759;
        }

        public String toString()
        {
            return "WORKING";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class noSleep$ implements Sleeper, Product, Serializable
    {
        public static Sleeper.noSleep$ MODULE$;

        static
        {
            new Sleeper.noSleep$();
        }

        public noSleep$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public void reportStartWorkUnit()
        {
        }

        public void reportStopWorkUnit()
        {
        }

        public void reportIdle()
        {
        }

        public boolean isSleeping()
        {
            return false;
        }

        public boolean isWorking()
        {
            return false;
        }

        public String statusString()
        {
            return Sleeper.ACTIVE$.MODULE$.toString();
        }

        public String productPrefix()
        {
            return "noSleep";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Sleeper.noSleep$;
        }

        public int hashCode()
        {
            return 2098962838;
        }

        public String toString()
        {
            return "noSleep";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
