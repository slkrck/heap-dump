package org.neo4j.cypher.internal.runtime.pipelined;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId;
import org.neo4j.cypher.internal.physicalplanning.NoOutput$;
import org.neo4j.cypher.internal.physicalplanning.OutputDefinition;
import org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate;
import org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple5;
import scala.collection.immutable.List;
import scala.collection.immutable.List.;
import scala.runtime.AbstractFunction5;

public final class FusionPlan$ extends
        AbstractFunction5<OperatorTaskTemplate,List<LogicalPlan>,List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>>,List<LogicalPlan>,OutputDefinition,FusionPlan>
        implements Serializable
{
    public static FusionPlan$ MODULE$;

    static
    {
        new FusionPlan$();
    }

    private FusionPlan$()
    {
        MODULE$ = this;
    }

    public List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> $lessinit$greater$default$3()
    {
        return .MODULE$.empty();
    }

    public List<LogicalPlan> $lessinit$greater$default$4()
    {
        return .MODULE$.empty();
    }

    public OutputDefinition $lessinit$greater$default$5()
    {
        return NoOutput$.MODULE$;
    }

    public final String toString()
    {
        return "FusionPlan";
    }

    public FusionPlan apply( final OperatorTaskTemplate template, final List<LogicalPlan> fusedPlans,
            final List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> argumentStates,
            final List<LogicalPlan> unhandledPlans, final OutputDefinition unhandledOutput )
    {
        return new FusionPlan( template, fusedPlans, argumentStates, unhandledPlans, unhandledOutput );
    }

    public List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>> apply$default$3()
    {
        return .MODULE$.empty();
    }

    public List<LogicalPlan> apply$default$4()
    {
        return .MODULE$.empty();
    }

    public OutputDefinition apply$default$5()
    {
        return NoOutput$.MODULE$;
    }

    public Option<Tuple5<OperatorTaskTemplate,List<LogicalPlan>,List<Tuple2<ArgumentStateMapId,ArgumentStateMap.ArgumentStateFactory<? extends ArgumentStateMap.ArgumentState>>>,List<LogicalPlan>,OutputDefinition>> unapply(
            final FusionPlan x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :
        new Some( new Tuple5( x$0.template(), x$0.fusedPlans(), x$0.argumentStates(), x$0.unhandledPlans(), x$0.unhandledOutput() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
