package com.neo4j.fabric.functions;

import com.neo4j.fabric.config.FabricConfig;

import java.util.Collections;
import java.util.List;

import org.neo4j.internal.kernel.api.procs.FieldSignature;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.QualifiedName;
import org.neo4j.internal.kernel.api.procs.UserFunctionSignature;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.ListType;
import org.neo4j.kernel.api.procedure.CallableUserFunction;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;

public class GraphIdsFunction implements CallableUserFunction
{
    private static final String NAME = "graphIds";
    private static final String DESCRIPTION = "List all fabric graph ids";
    private static final List<FieldSignature> ARGUMENT_TYPES = Collections.emptyList();
    private static final ListType RESULT_TYPE;

    static
    {
        RESULT_TYPE = Neo4jTypes.NTList( Neo4jTypes.NTInteger );
    }

    private final UserFunctionSignature signature;
    private final FabricConfig fabricConfig;

    public GraphIdsFunction( FabricConfig fabricConfig )
    {
        this.fabricConfig = fabricConfig;
        String namespace = fabricConfig.getDatabase().getName().name();
        this.signature =
                new UserFunctionSignature( new QualifiedName( new String[]{namespace}, "graphIds" ), ARGUMENT_TYPES, RESULT_TYPE, (String) null, new String[0],
                        "List all fabric graph ids", true );
    }

    public UserFunctionSignature signature()
    {
        return this.signature;
    }

    public boolean threadSafe()
    {
        return true;
    }

    public AnyValue apply( Context ctx, AnyValue[] input )
    {
        return Values.longArray( this.fabricConfig.getDatabase().getGraphs().stream().mapToLong( FabricConfig.Graph::getId ).toArray() );
    }
}
