package com.neo4j.fabric.cache;

import com.neo4j.fabric.planning.FabricPlan;
import org.neo4j.cypher.internal.QueryCache.;
import org.neo4j.cypher.internal.cache.LFUCache;
import org.neo4j.values.virtual.MapValue;
import scala.Function2;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class FabricQueryCache
{
    private final LFUCache<Tuple2<String,Map<String,Class<?>>>,FabricPlan> cache;
    private long hits;
    private long misses;

    public FabricQueryCache( final int size )
    {
        this.cache = new LFUCache( size );
        this.hits = 0L;
        this.misses = 0L;
    }

    private LFUCache<Tuple2<String,Map<String,Class<?>>>,FabricPlan> cache()
    {
        return this.cache;
    }

    private long hits()
    {
        return this.hits;
    }

    private void hits_$eq( final long x$1 )
    {
        this.hits = x$1;
    }

    private long misses()
    {
        return this.misses;
    }

    private void misses_$eq( final long x$1 )
    {
        this.misses = x$1;
    }

    public FabricPlan computeIfAbsent( final String query, final MapValue params, final Function2<String,MapValue,FabricPlan> compute )
    {
        Map paramTypes = .MODULE$.extractParameterTypeMap( params );
        Tuple2 key = new Tuple2( query, paramTypes );
        Option var7 = this.cache().get( key );
        FabricPlan var4;
        if ( scala.None..MODULE$.equals( var7 )){
        FabricPlan result = (FabricPlan) compute.apply( query, params );
        this.cache().put( key, result );
        this.misses_$eq( this.misses() + 1L );
        var4 = result;
    } else{
        if ( !(var7 instanceof Some) )
        {
            throw new MatchError( var7 );
        }

        Some var9 = (Some) var7;
        FabricPlan result = (FabricPlan) var9.value();
        this.hits_$eq( this.hits() + 1L );
        var4 = result;
    }

        return var4;
    }

    public long getHits()
    {
        return this.hits();
    }

    public long getMisses()
    {
        return this.misses();
    }
}
