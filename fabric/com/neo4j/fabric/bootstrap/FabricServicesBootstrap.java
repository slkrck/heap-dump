package com.neo4j.fabric.bootstrap;

import com.neo4j.fabric.auth.CredentialsProvider;
import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.driver.DriverPool;
import com.neo4j.fabric.eval.Catalog;
import com.neo4j.fabric.eval.UseEvaluation;
import com.neo4j.fabric.executor.FabricExecutor;
import com.neo4j.fabric.executor.FabricLocalExecutor;
import com.neo4j.fabric.executor.FabricQueryMonitoring;
import com.neo4j.fabric.executor.FabricRemoteExecutor;
import com.neo4j.fabric.functions.GraphIdsFunction;
import com.neo4j.fabric.localdb.FabricDatabaseManager;
import com.neo4j.fabric.pipeline.SignatureResolver;
import com.neo4j.fabric.planning.FabricPlanner;
import com.neo4j.fabric.transaction.TransactionManager;

import java.time.Clock;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.cypher.internal.CypherConfiguration;
import org.neo4j.exceptions.KernelException;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.config.SslPolicyLoader;

public class FabricServicesBootstrap
{
    private final FabricConfig fabricConfig;

    public FabricServicesBootstrap( LifeSupport lifeSupport, Dependencies dependencies, LogService logService )
    {
        FabricServicesBootstrap.ServiceBootstrapper serviceBootstrapper = new FabricServicesBootstrap.ServiceBootstrapper( lifeSupport, dependencies );
        LogProvider internalLogProvider = logService.getInternalLogProvider();
        Config config = (Config) dependencies.resolveDependency( Config.class );
        this.fabricConfig = (FabricConfig) serviceBootstrapper.registerService( FabricConfig.from( config ), FabricConfig.class );
        FabricDatabaseManager fabricDatabaseManager =
                (FabricDatabaseManager) serviceBootstrapper.registerService( new FabricDatabaseManager( this.fabricConfig, dependencies, internalLogProvider ),
                        FabricDatabaseManager.class );
        if ( this.fabricConfig.isEnabled() )
        {
            JobScheduler jobScheduler = (JobScheduler) dependencies.resolveDependency( JobScheduler.class );
            Monitors monitors = (Monitors) dependencies.resolveDependency( Monitors.class );
            Supplier<GlobalProcedures> proceduresSupplier = dependencies.provideDependency( GlobalProcedures.class );
            CredentialsProvider credentialsProvider =
                    (CredentialsProvider) serviceBootstrapper.registerService( new CredentialsProvider(), CredentialsProvider.class );
            SslPolicyLoader sslPolicyLoader = (SslPolicyLoader) dependencies.resolveDependency( SslPolicyLoader.class );
            DriverPool driverPool = (DriverPool) serviceBootstrapper.registerService(
                    new DriverPool( jobScheduler, this.fabricConfig, config, Clock.systemUTC(), credentialsProvider, sslPolicyLoader ), DriverPool.class );
            serviceBootstrapper.registerService( new FabricRemoteExecutor( driverPool ), FabricRemoteExecutor.class );
            serviceBootstrapper.registerService( new FabricLocalExecutor( this.fabricConfig, fabricDatabaseManager ), FabricLocalExecutor.class );
            serviceBootstrapper.registerService( new TransactionManager( dependencies ), TransactionManager.class );
            CypherConfiguration cypherConfig = CypherConfiguration.fromConfig( config );
            Catalog catalog = Catalog.fromConfig( this.fabricConfig );
            SignatureResolver signatureResolver = new SignatureResolver( proceduresSupplier );
            FabricQueryMonitoring monitoring = new FabricQueryMonitoring( dependencies, monitors );
            FabricPlanner planner =
                    (FabricPlanner) serviceBootstrapper.registerService( new FabricPlanner( this.fabricConfig, cypherConfig, monitors, signatureResolver ),
                            FabricPlanner.class );
            UseEvaluation useEvaluation =
                    (UseEvaluation) serviceBootstrapper.registerService( new UseEvaluation( catalog, proceduresSupplier, signatureResolver ),
                            UseEvaluation.class );
            FabricReactorHooks.register( internalLogProvider );
            Executor fabricWorkerExecutor = jobScheduler.executor( Group.FABRIC_WORKER );
            FabricExecutor fabricExecutor =
                    new FabricExecutor( this.fabricConfig, planner, useEvaluation, internalLogProvider, monitoring, fabricWorkerExecutor );
            serviceBootstrapper.registerService( fabricExecutor, FabricExecutor.class );
        }
    }

    public void registerProcedures( GlobalProcedures globalProcedures ) throws KernelException
    {
        if ( this.fabricConfig.isEnabled() )
        {
            globalProcedures.register( new GraphIdsFunction( this.fabricConfig ) );
        }
    }

    private static class ServiceBootstrapper
    {
        private final LifeSupport lifeSupport;
        private final Dependencies dependencies;

        ServiceBootstrapper( LifeSupport lifeSupport, Dependencies dependencies )
        {
            this.lifeSupport = lifeSupport;
            this.dependencies = dependencies;
        }

        <T> T registerService( T dependency, Class<T> dependencyType )
        {
            this.dependencies.satisfyDependency( dependency );
            if ( LifecycleAdapter.class.isAssignableFrom( dependencyType ) )
            {
                this.lifeSupport.add( (LifecycleAdapter) dependency );
            }

            return this.dependencies.resolveDependency( dependencyType );
        }
    }
}
