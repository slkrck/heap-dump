package com.neo4j.fabric.bootstrap;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import reactor.core.publisher.Hooks;

public class FabricReactorHooks
{
    static void register( LogProvider internalLogProvider )
    {
        Log log = internalLogProvider.getLog( FabricReactorHooks.class );
        Hooks.onErrorDropped( ( throwable ) -> {
            log.error( "Dropped error in stream", throwable );
        } );
    }
}
