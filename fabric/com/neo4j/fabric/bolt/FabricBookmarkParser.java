package com.neo4j.fabric.bolt;

import com.neo4j.fabric.driver.RemoteBookmark;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.neo4j.bolt.dbapi.CustomBookmarkFormatParser;
import org.neo4j.bolt.runtime.Bookmark;

public class FabricBookmarkParser implements CustomBookmarkFormatParser
{
    public boolean isCustomBookmark( String string )
    {
        return string.startsWith( "FB:" );
    }

    public List<Bookmark> parse( List<String> customBookmarks )
    {
        return (List) customBookmarks.stream().map( this::parse ).collect( Collectors.toList() );
    }

    private Bookmark parse( String bookmarkString )
    {
        if ( !this.isCustomBookmark( bookmarkString ) )
        {
            throw new IllegalArgumentException( String.format( "'%s' is not a valid Fabric bookmark", bookmarkString ) );
        }
        else
        {
            String content = bookmarkString.substring( "FB:".length() );
            if ( content.isEmpty() )
            {
                return new FabricBookmark( List.of() );
            }
            else
            {
                String[] graphParts = content.split( "-" );
                List<FabricBookmark.GraphState> remoteStates = (List) Arrays.stream( graphParts ).map( ( rawGraphState ) -> {
                    return this.parseGraphState( bookmarkString, rawGraphState );
                } ).collect( Collectors.toList() );
                return new FabricBookmark( remoteStates );
            }
        }
    }

    private FabricBookmark.GraphState parseGraphState( String bookmarkString, String rawGraphState )
    {
        String[] parts = rawGraphState.split( ":" );
        if ( parts.length != 2 )
        {
            throw new IllegalArgumentException( String.format( "Bookmark '%s' not valid", bookmarkString ) );
        }
        else
        {
            long graphId;
            try
            {
                graphId = Long.parseLong( parts[0] );
            }
            catch ( NumberFormatException var7 )
            {
                throw new IllegalArgumentException( String.format( "Could not parse graph ID in '%s'", bookmarkString ), var7 );
            }

            List<RemoteBookmark> remoteBookmarks =
                    (List) Arrays.stream( parts[1].split( "," ) ).map( this::decodeRemoteBookmark ).collect( Collectors.toList() );
            return new FabricBookmark.GraphState( graphId, remoteBookmarks );
        }
    }

    private RemoteBookmark decodeRemoteBookmark( String encodedBookmark )
    {
        Set<String> decodedBookmarkState = (Set) Arrays.stream( encodedBookmark.split( "\\|" ) ).map( ( bookmarkPart ) -> {
            return Base64.getDecoder().decode( bookmarkPart );
        } ).map( ( decodedPart ) -> {
            return new String( decodedPart, StandardCharsets.UTF_8 );
        } ).collect( Collectors.toSet() );
        return new RemoteBookmark( decodedBookmarkState );
    }
}
