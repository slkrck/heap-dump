package com.neo4j.fabric.bolt;

import com.neo4j.fabric.driver.RemoteBookmark;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.neo4j.bolt.dbapi.BookmarkMetadata;
import org.neo4j.bolt.runtime.BoltResponseHandler;
import org.neo4j.bolt.runtime.Bookmark;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.values.storable.Values;

public class FabricBookmark extends BookmarkMetadata implements Bookmark
{
    public static final String PREFIX = "FB:";
    private final List<FabricBookmark.GraphState> graphStates;

    public FabricBookmark( List<FabricBookmark.GraphState> graphStates )
    {
        super( -1L, (NamedDatabaseId) null );
        this.graphStates = graphStates;
    }

    public long txId()
    {
        return this.getTxId();
    }

    public NamedDatabaseId databaseId()
    {
        return this.getNamedDatabaseId();
    }

    public void attachTo( BoltResponseHandler state )
    {
        state.onMetadata( "bookmark", Values.stringValue( this.serialize() ) );
    }

    public List<FabricBookmark.GraphState> getGraphStates()
    {
        return this.graphStates;
    }

    public Bookmark toBookmark( BiFunction<Long,NamedDatabaseId,Bookmark> defaultBookmarkFormat )
    {
        return this;
    }

    public String toString()
    {
        return "FabricBookmark{graphStates=" + this.graphStates + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FabricBookmark that = (FabricBookmark) o;
            return this.graphStates.equals( that.graphStates );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.graphStates} );
    }

    public String serialize()
    {
        return (String) this.graphStates.stream().map( ( rec$ ) -> {
            return ((FabricBookmark.GraphState) rec$).serialize();
        } ).collect( Collectors.joining( "-", "FB:", "" ) );
    }

    public static class GraphState
    {
        private final long remoteGraphId;
        private final List<RemoteBookmark> bookmarks;

        public GraphState( long remoteGraphId, List<RemoteBookmark> bookmarks )
        {
            this.remoteGraphId = remoteGraphId;
            this.bookmarks = bookmarks;
        }

        public long getRemoteGraphId()
        {
            return this.remoteGraphId;
        }

        public List<RemoteBookmark> getBookmarks()
        {
            return this.bookmarks;
        }

        public String toString()
        {
            return "GraphState{remoteGraphId=" + this.remoteGraphId + ", bookmarks=" + this.bookmarks + "}";
        }

        private String serialize()
        {
            return (String) this.bookmarks.stream().map( this::serialize ).collect( Collectors.joining( ",", this.remoteGraphId + ":", "" ) );
        }

        private String serialize( RemoteBookmark remoteBookmark )
        {
            return (String) remoteBookmark.getSerialisedState().stream().map( ( bookmarkPart ) -> {
                return Base64.getEncoder().encodeToString( bookmarkPart.getBytes( StandardCharsets.UTF_8 ) );
            } ).collect( Collectors.joining( "|" ) );
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                FabricBookmark.GraphState that = (FabricBookmark.GraphState) o;
                return this.remoteGraphId == that.remoteGraphId && this.bookmarks.equals( that.bookmarks );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return Objects.hash( new Object[]{this.remoteGraphId, this.bookmarks} );
        }
    }
}
