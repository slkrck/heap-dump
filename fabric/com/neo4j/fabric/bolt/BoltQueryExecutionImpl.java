package com.neo4j.fabric.bolt;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.executor.Exceptions;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.stream.Rx2SyncStream;
import com.neo4j.fabric.stream.StatementResult;
import com.neo4j.fabric.stream.summary.Summary;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import org.neo4j.bolt.dbapi.BoltQueryExecution;
import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.graphdb.Result.ResultVisitor;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.neo4j.kernel.impl.query.QueryExecution;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class BoltQueryExecutionImpl implements BoltQueryExecution
{
    private final BoltQueryExecutionImpl.QueryExecutionImpl queryExecution;

    public BoltQueryExecutionImpl( StatementResult statementResult, QuerySubscriber subscriber, FabricConfig fabricConfig )
    {
        FabricConfig.DataStream config = fabricConfig.getDataStream();
        Rx2SyncStream rx2SyncStream = new Rx2SyncStream( statementResult.records(), config.getBatchSize() );
        this.queryExecution = new BoltQueryExecutionImpl.QueryExecutionImpl( rx2SyncStream, subscriber, statementResult.columns(), statementResult.summary() );
    }

    public QueryExecution getQueryExecution()
    {
        return this.queryExecution;
    }

    public void close()
    {
        this.queryExecution.cancel();
    }

    public void terminate()
    {
        this.queryExecution.cancel();
    }

    private static class QueryExecutionImpl implements QueryExecution
    {
        private final Rx2SyncStream rx2SyncStream;
        private final QuerySubscriber subscriber;
        private final Mono<Summary> summary;
        private final Supplier<List<String>> columns;
        private boolean hasMore = true;
        private boolean initialised;

        private QueryExecutionImpl( Rx2SyncStream rx2SyncStream, QuerySubscriber subscriber, Flux<String> columns, Mono<Summary> summary )
        {
            this.rx2SyncStream = rx2SyncStream;
            this.subscriber = subscriber;
            this.summary = summary;
            AtomicReference<List<String>> columnsStore = new AtomicReference();
            this.columns = () -> {
                if ( columnsStore.get() == null )
                {
                    columnsStore.compareAndSet( (Object) null, (List) columns.collectList().block() );
                }

                return (List) columnsStore.get();
            };
        }

        private Summary getSummary()
        {
            return (Summary) this.summary.block();
        }

        public QueryExecutionType executionType()
        {
            return this.getSummary().executionType();
        }

        public ExecutionPlanDescription executionPlanDescription()
        {
            return this.getSummary().executionPlanDescription();
        }

        public Iterable<Notification> getNotifications()
        {
            return this.getSummary().getNotifications();
        }

        public String[] fieldNames()
        {
            return (String[]) ((List) this.columns.get()).toArray( new String[0] );
        }

        public void request( long numberOfRecords ) throws Exception
        {
            if ( this.hasMore )
            {
                if ( !this.initialised )
                {
                    this.initialised = true;
                    this.subscriber.onResult( ((List) this.columns.get()).size() );
                }

                try
                {
                    for ( int i = 0; (long) i < numberOfRecords; ++i )
                    {
                        Record record = this.rx2SyncStream.readRecord();
                        if ( record == null )
                        {
                            this.hasMore = false;
                            this.subscriber.onResultCompleted( this.getSummary().getQueryStatistics() );
                            return;
                        }

                        this.subscriber.onRecord();
                        this.publishFields( record );
                        this.subscriber.onRecordCompleted();
                    }
                }
                catch ( Exception var5 )
                {
                    throw Exceptions.transform( Statement.ExecutionFailed, var5 );
                }
            }
        }

        private void publishFields( Record record ) throws Exception
        {
            for ( int i = 0; i < ((List) this.columns.get()).size(); ++i )
            {
                this.subscriber.onField( record.getValue( i ) );
            }
        }

        public void cancel()
        {
            this.rx2SyncStream.close();
        }

        public boolean await()
        {
            return this.hasMore;
        }

        public boolean isVisitable()
        {
            return false;
        }

        public <VisitationException extends Exception> QueryStatistics accept( ResultVisitor<VisitationException> visitor )
        {
            throw new IllegalStateException( "Results are not visitable" );
        }
    }
}
