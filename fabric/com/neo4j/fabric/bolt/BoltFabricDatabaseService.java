package com.neo4j.fabric.bolt;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.executor.FabricExecutor;
import com.neo4j.fabric.stream.StatementResult;
import com.neo4j.fabric.transaction.FabricTransaction;
import com.neo4j.fabric.transaction.FabricTransactionInfo;
import com.neo4j.fabric.transaction.TransactionBookmarkManager;
import com.neo4j.fabric.transaction.TransactionManager;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.neo4j.bolt.dbapi.BoltGraphDatabaseServiceSPI;
import org.neo4j.bolt.dbapi.BoltQueryExecution;
import org.neo4j.bolt.dbapi.BoltTransaction;
import org.neo4j.bolt.dbapi.BookmarkMetadata;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.bolt.runtime.Bookmark;
import org.neo4j.bolt.txtracking.TransactionIdTracker;
import org.neo4j.internal.kernel.api.connectioninfo.ClientConnectionInfo;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.internal.kernel.api.security.LoginContext;
import org.neo4j.kernel.api.KernelTransaction.Type;
import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Transaction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.query.QueryExecutionKernelException;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.virtual.MapValue;

public class BoltFabricDatabaseService implements BoltGraphDatabaseServiceSPI
{
    private final FabricExecutor fabricExecutor;
    private final NamedDatabaseId namedDatabaseId;
    private final FabricConfig config;
    private final TransactionManager transactionManager;
    private final Duration bookmarkTimeout;
    private final TransactionIdTracker transactionIdTracker;

    public BoltFabricDatabaseService( NamedDatabaseId namedDatabaseId, FabricExecutor fabricExecutor, FabricConfig config,
            TransactionManager transactionManager, Duration bookmarkTimeout, TransactionIdTracker transactionIdTracker )
    {
        this.namedDatabaseId = namedDatabaseId;
        this.config = config;
        this.transactionManager = transactionManager;
        this.fabricExecutor = fabricExecutor;
        this.bookmarkTimeout = bookmarkTimeout;
        this.transactionIdTracker = transactionIdTracker;
    }

    public BoltTransaction beginTransaction( Type type, LoginContext loginContext, ClientConnectionInfo clientInfo, List<Bookmark> bookmarks,
            Duration txTimeout, AccessMode accessMode, Map<String,Object> txMetadata )
    {
        if ( txTimeout == null )
        {
            txTimeout = this.config.getTransactionTimeout();
        }

        FabricTransactionInfo transactionInfo =
                new FabricTransactionInfo( accessMode, loginContext, clientInfo, this.namedDatabaseId.name(), false, txTimeout, txMetadata );
        TransactionBookmarkManager transactionBookmarkManager = new TransactionBookmarkManager( this.config, this.transactionIdTracker, this.bookmarkTimeout );
        transactionBookmarkManager.processSubmittedByClient( bookmarks );
        FabricTransaction fabricTransaction = this.transactionManager.begin( transactionInfo, transactionBookmarkManager );
        return new BoltFabricDatabaseService.BoltTransactionImpl( transactionInfo, fabricTransaction );
    }

    public boolean isPeriodicCommit( String query )
    {
        return false;
    }

    public NamedDatabaseId getNamedDatabaseId()
    {
        return this.namedDatabaseId;
    }

    private class BoltTransactionImpl implements BoltTransaction
    {
        private final FabricTransactionInfo transactionInfo;
        private final FabricTransaction fabricTransaction;

        BoltTransactionImpl( FabricTransactionInfo transactionInfo, FabricTransaction fabricTransaction )
        {
            this.transactionInfo = transactionInfo;
            this.fabricTransaction = fabricTransaction;
        }

        public void commit() throws TransactionFailureException
        {
            this.fabricTransaction.commit();
        }

        public void rollback() throws TransactionFailureException
        {
            this.fabricTransaction.rollback();
        }

        public void markForTermination( Status reason )
        {
            this.fabricTransaction.markForTermination( reason );
        }

        public void markForTermination()
        {
            this.fabricTransaction.markForTermination( Transaction.Terminated );
        }

        public Optional<Status> getReasonIfTerminated()
        {
            return this.fabricTransaction.getReasonIfTerminated();
        }

        public BookmarkMetadata getBookmarkMetadata()
        {
            return this.fabricTransaction.getBookmarkManager().constructFinalBookmark();
        }

        public BoltQueryExecution executeQuery( String query, MapValue parameters, boolean prePopulate, QuerySubscriber subscriber )
                throws QueryExecutionKernelException
        {
            StatementResult statementResult = BoltFabricDatabaseService.this.fabricExecutor.run( this.fabricTransaction, query, parameters );
            return new BoltQueryExecutionImpl( statementResult, subscriber, BoltFabricDatabaseService.this.config );
        }
    }
}
