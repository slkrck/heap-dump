package com.neo4j.fabric.config;

import java.net.URI;
import java.time.Duration;
import java.util.List;

import org.neo4j.configuration.Description;
import org.neo4j.configuration.DocumentedDefaultValue;
import org.neo4j.configuration.GroupSetting;
import org.neo4j.configuration.Internal;
import org.neo4j.configuration.SettingConstraints;
import org.neo4j.configuration.SettingImpl;
import org.neo4j.configuration.SettingValueParsers;
import org.neo4j.configuration.SettingsDeclaration;
import org.neo4j.configuration.helpers.NormalizedGraphName;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.graphdb.config.Setting;
import org.neo4j.logging.Level;

public class FabricSettings implements SettingsDeclaration
{
    private static final String DRIVER_LOGGING_LEVEL = "driver.logging.level";
    private static final String DRIVER_LOG_LEAKED_SESSIONS = "driver.logging.leaked_sessions";
    private static final String DRIVER_MAX_CONNECTION_POOL_SIZE = "driver.connection.pool.max_size";
    private static final String DRIVER_IDLE_TIME_BEFORE_CONNECTION_TEST = "driver.connection.pool.idle_test";
    private static final String DRIVER_CONNECTION_ACQUISITION_TIMEOUT = "driver.connection.pool.acquisition_timeout";
    private static final String DRIVER_MAX_CONNECTION_LIFETIME = "driver.connection.max_lifetime";
    private static final String DRIVER_CONNECT_TIMEOUT = "driver.connection.connect_timeout";
    private static final String DRIVER_API = "driver.api";
    @Description( "A comma-separated list of Fabric instances that form a routing group. A driver will route transactions to available routing group members.\nA Fabric instance is represented by its Bolt connector address." )
    public static Setting<List<SocketAddress>> fabricServersSetting;
    @Description( "Name of the Fabric database. Only one Fabric database is currently supported per Neo4j instance." )
    public static Setting<String> databaseName;
    @Description( "The time to live (TTL) of a routing table for fabric routing group." )
    public static Setting<Duration> routingTtlSetting;
    @Description( "Time interval of inactivity after which a driver will be closed." )
    @Internal
    public static Setting<Duration> driverIdleTimeout;
    @Description( "Time interval between driver idleness check." )
    @Internal
    public static Setting<Duration> driverIdleCheckInterval;
    @Description( " Number of event loops used by drivers. Event loops are shard between drivers, so this is the total number of event loops created." )
    @DocumentedDefaultValue( "Number of available processors" )
    @Internal
    public static Setting<Integer> driverEventLoopCount;
    @Description( "Sets level for driver internal logging." )
    @DocumentedDefaultValue( "Value of dbms.logs.debug.level" )
    public static Setting<Level> driverLoggingLevel;
    @Description( "Enables logging of leaked driver session" )
    @Internal
    public static Setting<Boolean> driverLogLeakedSessions;
    @Description( "Maximum total number of connections to be managed by a connection pool.\nThe limit is enforced for a combination of a host and user. Negative values are allowed and result in unlimited pool. Value of 0is not allowed." )
    @DocumentedDefaultValue( "Unlimited" )
    public static Setting<Integer> driverMaxConnectionPoolSize;
    @Description( "Pooled connections that have been idle in the pool for longer than this timeout will be tested before they are used again, to ensure they are still alive.\nIf this option is set too low, an additional network call will be incurred when acquiring a connection, which causes a performance hit.\nIf this is set high, no longer live connections might be used which might lead to errors.\nHence, this parameter tunes a balance between the likelihood of experiencing connection problems and performance\nNormally, this parameter should not need tuning.\nValue 0 means connections will always be tested for validity" )
    @DocumentedDefaultValue( "No connection liveliness check is done by default." )
    public static Setting<Duration> driverIdleTimeBeforeConnectionTest;
    @Description( "Pooled connections older than this threshold will be closed and removed from the pool.\nSetting this option to a low value will cause a high connection churn and might result in a performance hit.\nIt is recommended to set maximum lifetime to a slightly smaller value than the one configured in network\nequipment (load balancer, proxy, firewall, etc. can also limit maximum connection lifetime).\nZero and negative values result in lifetime not being checked." )
    public static Setting<Duration> driverMaxConnectionLifetime;
    @Description( "Maximum amount of time spent attempting to acquire a connection from the connection pool.\nThis timeout only kicks in when all existing connections are being used and no new connections can be created because maximum connection pool size has been reached.\nError is raised when connection can't be acquired within configured time.\nNegative values are allowed and result in unlimited acquisition timeout. Value of 0 is allowed and results in no timeout and immediate failure when connection is unavailable" )
    public static Setting<Duration> driverConnectionAcquisitionTimeout;
    @Description( "Socket connection timeout.\nA timeout of zero is treated as an infinite timeout and will be bound by the timeout configured on the\noperating system level." )
    public static Setting<Duration> driverConnectTimeout;
    @Description( "Maximal size of a buffer used for pre-fetching result records of remote queries.\nTo compensate for latency to remote databases, the Fabric execution engine pre-fetches records needed for local executions.\nThis limit is enforced per fabric query. If a fabric query uses multiple remote stream at the same time, this setting represents the maximal number of pre-fetched records counted together for all such remote streams" )
    public static Setting<Integer> bufferSizeSetting;
    @Description( "Number of records in prefetching buffer that will trigger prefetching again. This is strongly related to fabric.stream.buffer.size" )
    public static Setting<Integer> bufferLowWatermarkSetting;
    @Description( "Batch size used when requesting records from local Cypher engine." )
    @Internal
    public static Setting<Integer> batchSizeSetting;
    @Description( "Maximal concurrency within Fabric queries.\nLimits the number of iterations of each subquery that are executed concurrently. Higher concurrency may consume more memory and network resources simultaneously, while lower concurrency may force sequential execution, requiring more time." )
    @DocumentedDefaultValue( "The number of remote graphs" )
    public static Setting<Integer> concurrency;
    @Description( "Determines which driver API will be used. ASYNC must be used when the remote instance is 3.5" )
    public static Setting<FabricSettings.DriverApi> driverApi;

    static
    {
        fabricServersSetting = SettingImpl.newBuilder( "fabric.routing.servers", SettingValueParsers.listOf( SettingValueParsers.SOCKET_ADDRESS ),
                List.of( new SocketAddress( "localhost", 7687 ) ) ).dynamic().build();
        databaseName = SettingImpl.newBuilder( "fabric.database.name", SettingValueParsers.DATABASENAME, (Object) null ).build();
        routingTtlSetting = SettingImpl.newBuilder( "fabric.routing.ttl", SettingValueParsers.DURATION, Duration.ofMinutes( 1L ) ).build();
        driverIdleTimeout = SettingImpl.newBuilder( "fabric.driver.timeout", SettingValueParsers.DURATION, Duration.ofMinutes( 1L ) ).build();
        driverIdleCheckInterval = SettingImpl.newBuilder( "fabric.driver.idle_check_interval", SettingValueParsers.DURATION, Duration.ofMinutes( 1L ) ).build();
        driverEventLoopCount =
                SettingImpl.newBuilder( "fabric.driver.event_loop_count", SettingValueParsers.INT, Runtime.getRuntime().availableProcessors() ).build();
        driverLoggingLevel = SettingImpl.newBuilder( "fabric.driver.logging.level", SettingValueParsers.ofEnum( Level.class ), (Object) null ).build();
        driverLogLeakedSessions = SettingImpl.newBuilder( "fabric.driver.logging.leaked_sessions", SettingValueParsers.BOOL, false ).build();
        driverMaxConnectionPoolSize = SettingImpl.newBuilder( "fabric.driver.connection.pool.max_size", SettingValueParsers.INT, -1 ).build();
        driverIdleTimeBeforeConnectionTest =
                SettingImpl.newBuilder( "fabric.driver.connection.pool.idle_test", SettingValueParsers.DURATION, (Object) null ).build();
        driverMaxConnectionLifetime =
                SettingImpl.newBuilder( "fabric.driver.connection.max_lifetime", SettingValueParsers.DURATION, Duration.ofHours( 1L ) ).build();
        driverConnectionAcquisitionTimeout =
                SettingImpl.newBuilder( "fabric.driver.connection.pool.acquisition_timeout", SettingValueParsers.DURATION, Duration.ofSeconds( 60L ) ).build();
        driverConnectTimeout =
                SettingImpl.newBuilder( "fabric.driver.connection.connect_timeout", SettingValueParsers.DURATION, Duration.ofSeconds( 5L ) ).build();
        bufferSizeSetting =
                SettingImpl.newBuilder( "fabric.stream.buffer.size", SettingValueParsers.INT, 1000 ).addConstraint( SettingConstraints.min( 1 ) ).build();
        bufferLowWatermarkSetting = SettingImpl.newBuilder( "fabric.stream.buffer.low_watermark", SettingValueParsers.INT, 300 ).addConstraint(
                SettingConstraints.min( 0 ) ).build();
        batchSizeSetting =
                SettingImpl.newBuilder( "fabric.stream.batch_size", SettingValueParsers.INT, 50 ).addConstraint( SettingConstraints.min( 1 ) ).build();
        concurrency = SettingImpl.newBuilder( "fabric.stream.concurrency", SettingValueParsers.INT, (Object) null ).addConstraint(
                SettingConstraints.min( 1 ) ).build();
        driverApi = SettingImpl.newBuilder( "fabric.driver.api", SettingValueParsers.ofEnum( FabricSettings.DriverApi.class ),
                FabricSettings.DriverApi.RX ).build();
    }

    public static enum DriverApi
    {
        RX,
        ASYNC;
    }

    public static class GraphSetting extends GroupSetting
    {
        @Description( "URI of the Neo4j DBMS hosting the database associated to the Fabric graph. Example: neo4j://somewhere:7687 \nA comma separated list of URIs is acceptable. This is useful when the Fabric graph is hosted on a clusterand more that one bootstrap address needs to be provided in order to avoid a single point of failure.The provided addresses will be considered as an initial source of a routing table.Example: neo4j://core-1:1111,neo4j://core-2:2222" )
        public final Setting<List<URI>> uris;
        @Description( "Name of the database associated to the Fabric graph." )
        @DocumentedDefaultValue( "The default database on the target DBMS. Typically 'Neo4j'" )
        public final Setting<String> database;
        @Description( "Name assigned to the Fabric graph. The name can be used in Fabric queries." )
        public final Setting<NormalizedGraphName> name;
        public final Setting<Level> driverLoggingLevel;
        public final Setting<Boolean> driverLogLeakedSessions;
        public final Setting<Integer> driverMaxConnectionPoolSize;
        public final Setting<Duration> driverIdleTimeBeforeConnectionTest;
        public final Setting<Duration> driverMaxConnectionLifetime;
        public final Setting<Duration> driverConnectionAcquisitionTimeout;
        public final Setting<Duration> driverConnectTimeout;
        public final Setting<FabricSettings.DriverApi> driverApi;
        @Description( "SSL for Fabric drivers is configured using 'fabric' SSL policy.This setting can be used to instruct the driver not to use SSL even though 'fabric' SSL policy is configured.The driver will use SSL if 'fabric' SSL policy is configured and this setting is set to 'true'" )
        public final Setting<Boolean> sslEnabled;

        protected GraphSetting( String name )
        {
            super( name );
            this.uris = this.getBuilder( "uri", SettingValueParsers.listOf( SettingValueParsers.URI ), (Object) null ).build();
            this.database = this.getBuilder( "database", SettingValueParsers.STRING, (Object) null ).build();
            this.name = this.getBuilder( "name", SettingValueParsers.GRAPHNAME, (Object) null ).build();
            this.driverLoggingLevel = this.getBuilder( "driver.logging.level", SettingValueParsers.ofEnum( Level.class ), (Object) null ).build();
            this.driverLogLeakedSessions = this.getBuilder( "driver.logging.leaked_sessions", SettingValueParsers.BOOL, (Object) null ).build();
            this.driverMaxConnectionPoolSize = this.getBuilder( "driver.connection.pool.max_size", SettingValueParsers.INT, (Object) null ).build();
            this.driverIdleTimeBeforeConnectionTest =
                    this.getBuilder( "driver.connection.pool.idle_test", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverMaxConnectionLifetime = this.getBuilder( "driver.connection.max_lifetime", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverConnectionAcquisitionTimeout =
                    this.getBuilder( "driver.connection.pool.acquisition_timeout", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverConnectTimeout = this.getBuilder( "driver.connection.connect_timeout", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverApi = this.getBuilder( "driver.api", SettingValueParsers.ofEnum( FabricSettings.DriverApi.class ), (Object) null ).build();
            this.sslEnabled = this.getBuilder( "driver.ssl_enabled", SettingValueParsers.BOOL, true ).build();
        }

        public GraphSetting()
        {
            super( (String) null );
            this.uris = this.getBuilder( "uri", SettingValueParsers.listOf( SettingValueParsers.URI ), (Object) null ).build();
            this.database = this.getBuilder( "database", SettingValueParsers.STRING, (Object) null ).build();
            this.name = this.getBuilder( "name", SettingValueParsers.GRAPHNAME, (Object) null ).build();
            this.driverLoggingLevel = this.getBuilder( "driver.logging.level", SettingValueParsers.ofEnum( Level.class ), (Object) null ).build();
            this.driverLogLeakedSessions = this.getBuilder( "driver.logging.leaked_sessions", SettingValueParsers.BOOL, (Object) null ).build();
            this.driverMaxConnectionPoolSize = this.getBuilder( "driver.connection.pool.max_size", SettingValueParsers.INT, (Object) null ).build();
            this.driverIdleTimeBeforeConnectionTest =
                    this.getBuilder( "driver.connection.pool.idle_test", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverMaxConnectionLifetime = this.getBuilder( "driver.connection.max_lifetime", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverConnectionAcquisitionTimeout =
                    this.getBuilder( "driver.connection.pool.acquisition_timeout", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverConnectTimeout = this.getBuilder( "driver.connection.connect_timeout", SettingValueParsers.DURATION, (Object) null ).build();
            this.driverApi = this.getBuilder( "driver.api", SettingValueParsers.ofEnum( FabricSettings.DriverApi.class ), (Object) null ).build();
            this.sslEnabled = this.getBuilder( "driver.ssl_enabled", SettingValueParsers.BOOL, true ).build();
        }

        public static FabricSettings.GraphSetting of( String name )
        {
            return new FabricSettings.GraphSetting( name );
        }

        public String getPrefix()
        {
            return "fabric.graph";
        }
    }
}
