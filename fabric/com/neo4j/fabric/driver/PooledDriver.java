package com.neo4j.fabric.driver;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Driver;
import com.neo4j.fabric.shaded.driver.SessionConfig;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.transaction.FabricTransactionInfo;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Mono;

public abstract class PooledDriver
{
    private final Driver driver;
    private final AtomicInteger referenceCounter = new AtomicInteger();
    private final Consumer<PooledDriver> releaseCallback;
    private Instant lastUsedTimestamp;

    PooledDriver( Driver driver, Consumer<PooledDriver> releaseCallback )
    {
        this.driver = driver;
        this.releaseCallback = releaseCallback;
    }

    public void release()
    {
        this.releaseCallback.accept( this );
    }

    public abstract AutoCommitStatementResult run( String var1, MapValue var2, FabricConfig.Graph var3, AccessMode var4, FabricTransactionInfo var5,
            List<RemoteBookmark> var6 );

    public abstract Mono<FabricDriverTransaction> beginTransaction( FabricConfig.Graph var1, AccessMode var2, FabricTransactionInfo var3,
            List<RemoteBookmark> var4 );

    AtomicInteger getReferenceCounter()
    {
        return this.referenceCounter;
    }

    Instant getLastUsedTimestamp()
    {
        return this.lastUsedTimestamp;
    }

    void setLastUsedTimestamp( Instant lastUsedTimestamp )
    {
        this.lastUsedTimestamp = lastUsedTimestamp;
    }

    void close()
    {
        this.driver.close();
    }

    protected SessionConfig createSessionConfig( FabricConfig.Graph location, AccessMode accessMode, List<RemoteBookmark> bookmarks )
    {
        SessionConfig.Builder builder = SessionConfig.builder().withDefaultAccessMode( this.translateAccessMode( accessMode ) );
        HashSet<String> mergedBookmarks = new HashSet();
        bookmarks.forEach( ( remoteBookmark ) -> {
            mergedBookmarks.addAll( remoteBookmark.getSerialisedState() );
        } );
        builder.withBookmarks( Bookmark.from( mergedBookmarks ) );
        if ( location.getDatabase() != null )
        {
            builder.withDatabase( location.getDatabase() );
        }

        return builder.build();
    }

    protected TransactionConfig getTransactionConfig( FabricTransactionInfo transactionInfo )
    {
        return transactionInfo.getTxTimeout().equals( Duration.ZERO ) ? TransactionConfig.empty()
                                                                      : TransactionConfig.builder().withTimeout( transactionInfo.getTxTimeout() ).build();
    }

    private com.neo4j.fabric.shaded.driver.AccessMode translateAccessMode( AccessMode accessMode )
    {
        return accessMode == AccessMode.READ ? com.neo4j.fabric.shaded.driver.AccessMode.READ : com.neo4j.fabric.shaded.driver.AccessMode.WRITE;
    }
}
