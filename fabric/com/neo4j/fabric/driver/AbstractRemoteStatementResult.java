package com.neo4j.fabric.driver;

import com.neo4j.fabric.executor.FabricException;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.Neo4jException;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.stream.Records;
import com.neo4j.fabric.stream.StatementResult;
import com.neo4j.fabric.stream.summary.Summary;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Code;
import org.neo4j.kernel.api.exceptions.Status.Fabric;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

abstract class AbstractRemoteStatementResult implements StatementResult
{
    private final Flux<String> columns;
    private final Mono<ResultSummary> summary;
    private final RecordConverter recordConverter;
    private final Runnable completionListener;
    private boolean completionInvoked;

    AbstractRemoteStatementResult( Flux<String> columns, Mono<ResultSummary> summary, long sourceTag )
    {
        this( columns, summary, sourceTag, () -> {
        } );
    }

    AbstractRemoteStatementResult( Flux<String> columns, Mono<ResultSummary> summary, long sourceTag, Runnable completionListener )
    {
        this.columns = columns;
        this.summary = summary;
        this.recordConverter = new RecordConverter( sourceTag );
        this.completionListener = completionListener;
    }

    public Flux<String> columns()
    {
        return this.columns.onErrorMap( Neo4jException.class, this::translateError ).doOnError( ( ignored ) -> {
            this.invokeCompletionListener();
        } );
    }

    public Flux<Record> records()
    {
        return this.doGetRecords().onErrorMap( Neo4jException.class, this::translateError ).doFinally( ( signalType ) -> {
            this.invokeCompletionListener();
        } );
    }

    public Mono<Summary> summary()
    {
        return this.summary.onErrorMap( Neo4jException.class, this::translateError ).map( ResultSummaryWrapper::new );
    }

    protected abstract Flux<Record> doGetRecords();

    protected Flux<Record> convertRxRecords( Flux<com.neo4j.fabric.shaded.driver.Record> records )
    {
        return records.map( ( driverRecord ) -> {
            return Records.lazy( driverRecord.size(), () -> {
                Stream var10000 = driverRecord.values().stream();
                RecordConverter var10001 = this.recordConverter;
                Objects.requireNonNull( var10001 );
                return Records.of( (List) var10000.map( var10001::convertValue ).collect( Collectors.toList() ) );
            } );
        } );
    }

    private void invokeCompletionListener()
    {
        if ( !this.completionInvoked )
        {
            this.completionListener.run();
            this.completionInvoked = true;
        }
    }

    private FabricException translateError( Neo4jException driverException )
    {
        if ( driverException instanceof ClientException )
        {
            Optional<Status> serverCode = Code.all().stream().filter( ( code ) -> {
                return code.code().serialize().equals( driverException.code() );
            } ).findAny();
            return serverCode.isEmpty() ? this.genericRemoteFailure( driverException )
                                        : new FabricException( (Status) serverCode.get(), driverException.getMessage(), driverException );
        }
        else
        {
            return this.genericRemoteFailure( driverException );
        }
    }

    private FabricException genericRemoteFailure( Neo4jException driverException )
    {
        throw new FabricException( Fabric.RemoteExecutionFailed,
                String.format( "Remote execution failed with code %s and message '%s'", driverException.code(), driverException.getMessage() ),
                driverException );
    }
}
