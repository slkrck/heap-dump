package com.neo4j.fabric.driver;

import com.neo4j.fabric.auth.CredentialsProvider;
import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.config.FabricSettings;
import com.neo4j.fabric.shaded.driver.AuthToken;
import com.neo4j.fabric.shaded.driver.Driver;
import com.neo4j.fabric.shaded.driver.internal.DriverFactory;
import com.neo4j.fabric.shaded.driver.internal.async.connection.EventLoopGroupFactory;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingSettings;
import com.neo4j.fabric.shaded.driver.internal.retry.RetrySettings;
import com.neo4j.fabric.shaded.driver.internal.security.SecurityPlan;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Clock;
import java.time.Duration;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.config.SslPolicyLoader;

public class DriverPool extends LifecycleAdapter
{
    private final ConcurrentHashMap<DriverPool.Key,PooledDriver> driversInUse = new ConcurrentHashMap();
    private final ConcurrentHashMap<DriverPool.Key,PooledDriver> idleDrivers = new ConcurrentHashMap();
    private final CredentialsProvider credentialsProvider;
    private final JobScheduler jobScheduler;
    private final FabricConfig fabricConfig;
    private final Clock clock;
    private final DriverConfigFactory driverConfigFactory;
    private final EventLoopGroup eventLoopGroup;

    public DriverPool( JobScheduler jobScheduler, FabricConfig fabricConfig, Config serverConfig, Clock clock, CredentialsProvider credentialsProvider,
            SslPolicyLoader sslPolicyLoader )
    {
        this.jobScheduler = jobScheduler;
        this.fabricConfig = fabricConfig;
        this.clock = clock;
        this.credentialsProvider = credentialsProvider;
        this.driverConfigFactory = new DriverConfigFactory( fabricConfig, serverConfig, sslPolicyLoader );
        int eventLoopCount = fabricConfig.getGlobalDriverConfig().getEventLoopCount();
        this.eventLoopGroup = EventLoopGroupFactory.newEventLoopGroup( eventLoopCount );
    }

    public PooledDriver getDriver( FabricConfig.Graph location, AuthSubject subject )
    {
        AuthToken authToken = this.credentialsProvider.credentialsFor( subject );
        DriverPool.Key key = new DriverPool.Key( location.getUri(), authToken );
        return (PooledDriver) this.driversInUse.compute( key, ( k, presentValue ) -> {
            if ( presentValue != null )
            {
                presentValue.getReferenceCounter().incrementAndGet();
                return presentValue;
            }
            else
            {
                AtomicReference<PooledDriver> idleDriverRef = new AtomicReference();
                this.idleDrivers.computeIfPresent( key, ( k2, oldValue ) -> {
                    idleDriverRef.set( oldValue );
                    return null;
                } );
                PooledDriver pooledDriver;
                if ( idleDriverRef.get() != null )
                {
                    pooledDriver = (PooledDriver) idleDriverRef.get();
                }
                else
                {
                    pooledDriver = this.createDriver( key, location, authToken );
                }

                pooledDriver.getReferenceCounter().incrementAndGet();
                return pooledDriver;
            }
        } );
    }

    private void release( DriverPool.Key key, PooledDriver pooledDriver )
    {
        this.driversInUse.computeIfPresent( key, ( k, value ) -> {
            if ( pooledDriver.getReferenceCounter().decrementAndGet() != 0 )
            {
                return pooledDriver;
            }
            else
            {
                this.idleDrivers.put( key, pooledDriver );
                pooledDriver.setLastUsedTimestamp( this.clock.instant() );
                return null;
            }
        } );
    }

    public void start()
    {
        long checkInterval = this.fabricConfig.getGlobalDriverConfig().getDriverIdleCheckInterval().toSeconds();
        Duration idleTimeout = this.fabricConfig.getGlobalDriverConfig().getIdleTimeout();
        this.jobScheduler.schedule( Group.FABRIC_IDLE_DRIVER_MONITOR, () -> {
            List<DriverPool.Key> timeoutCandidates = (List) this.idleDrivers.entrySet().stream().filter( ( entry ) -> {
                return Duration.between( ((PooledDriver) entry.getValue()).getLastUsedTimestamp(), this.clock.instant() ).compareTo( idleTimeout ) > 0;
            } ).map( Entry::getKey ).collect( Collectors.toList() );
            timeoutCandidates.forEach( ( key ) -> {
                this.idleDrivers.computeIfPresent( key, ( k, pooledDriver ) -> {
                    pooledDriver.close();
                    return null;
                } );
            } );
        }, checkInterval, TimeUnit.SECONDS );
    }

    public void stop()
    {
        this.idleDrivers.values().forEach( PooledDriver::close );
        this.driversInUse.values().forEach( PooledDriver::close );
        this.eventLoopGroup.shutdownGracefully( 1L, 4L, TimeUnit.SECONDS );
    }

    private PooledDriver createDriver( DriverPool.Key key, FabricConfig.Graph location, AuthToken token )
    {
        com.neo4j.fabric.shaded.driver.Config config = this.driverConfigFactory.createConfig( location );
        SecurityPlan securityPlan = this.driverConfigFactory.createSecurityPlan( location );
        DriverFactory driverFactory = new DriverFactory();
        URI driverUri = this.constructDriverUri( location.getUri() );
        Driver databaseDriver =
                driverFactory.newInstance( driverUri, token, RoutingSettings.DEFAULT, RetrySettings.DEFAULT, config, this.eventLoopGroup, securityPlan );
        FabricSettings.DriverApi driverApi =
                (FabricSettings.DriverApi) this.driverConfigFactory.getProperty( location, FabricConfig.DriverConfig::getDriverApi );
        switch ( driverApi )
        {
        case RX:
            return new RxPooledDriver( databaseDriver, ( pd ) -> {
                this.release( key, pd );
            } );
        case ASYNC:
            return new AsyncPooledDriver( databaseDriver, ( pd ) -> {
                this.release( key, pd );
            } );
        default:
            throw new IllegalArgumentException( "Unexpected Driver API value: " + driverApi );
        }
    }

    private URI constructDriverUri( FabricConfig.RemoteUri uri )
    {
        SocketAddress address = (SocketAddress) uri.getAddresses().get( 0 );

        try
        {
            return new URI( uri.getScheme(), (String) null, address.getHostname(), address.getPort(), (String) null, uri.getQuery(), (String) null );
        }
        catch ( URISyntaxException var4 )
        {
            throw new IllegalArgumentException( var4.getMessage(), var4 );
        }
    }

    private class Key
    {
        private final FabricConfig.RemoteUri uri;
        private final AuthToken auth;

        Key( FabricConfig.RemoteUri uri, AuthToken auth )
        {
            this.uri = uri;
            this.auth = auth;
        }

        public boolean equals( Object that )
        {
            return EqualsBuilder.reflectionEquals( this, that, new String[0] );
        }

        public int hashCode()
        {
            return HashCodeBuilder.reflectionHashCode( this, new String[0] );
        }
    }
}
