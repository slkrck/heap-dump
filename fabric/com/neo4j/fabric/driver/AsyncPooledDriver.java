package com.neo4j.fabric.driver;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.shaded.driver.Driver;
import com.neo4j.fabric.shaded.driver.SessionConfig;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.async.AsyncSession;
import com.neo4j.fabric.shaded.driver.async.AsyncTransaction;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.transaction.FabricTransactionInfo;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class AsyncPooledDriver extends PooledDriver
{
    private final Driver driver;

    AsyncPooledDriver( Driver driver, Consumer<PooledDriver> releaseCallback )
    {
        super( driver, releaseCallback );
        this.driver = driver;
    }

    public AutoCommitStatementResult run( String query, MapValue params, FabricConfig.Graph location, AccessMode accessMode,
            FabricTransactionInfo transactionInfo, List<RemoteBookmark> bookmarks )
    {
        SessionConfig sessionConfig = this.createSessionConfig( location, accessMode, bookmarks );
        AsyncSession session = this.driver.asyncSession( sessionConfig );
        ParameterConverter parameterConverter = new ParameterConverter();
        Map<String,Object> paramMap = (Map) parameterConverter.convertValue( params );
        TransactionConfig transactionConfig = this.getTransactionConfig( transactionInfo );
        Mono<ResultCursor> resultCursor = Mono.fromFuture( session.runAsync( query, paramMap, transactionConfig ).toCompletableFuture() );
        return new AsyncPooledDriver.StatementResultImpl( session, resultCursor, location.getId() );
    }

    public Mono<FabricDriverTransaction> beginTransaction( FabricConfig.Graph location, AccessMode accessMode, FabricTransactionInfo transactionInfo,
            List<RemoteBookmark> bookmarks )
    {
        SessionConfig sessionConfig = this.createSessionConfig( location, accessMode, bookmarks );
        AsyncSession session = this.driver.asyncSession( sessionConfig );
        CompletionStage<AsyncTransaction> driverTransaction = this.getDriverTransaction( session, transactionInfo );
        return Mono.fromFuture( driverTransaction.toCompletableFuture() ).map( ( tx ) -> {
            return new FabricDriverAsyncTransaction( tx, session, location );
        } );
    }

    private CompletionStage<AsyncTransaction> getDriverTransaction( AsyncSession session, FabricTransactionInfo transactionInfo )
    {
        TransactionConfig transactionConfig = this.getTransactionConfig( transactionInfo );
        return session.beginTransactionAsync( transactionConfig );
    }

    private static class StatementResultImpl extends AbstractRemoteStatementResult implements AutoCommitStatementResult
    {
        private final AsyncSession session;
        private final Mono<ResultCursor> statementResultCursor;
        private final RecordConverter recordConverter;
        private final CompletableFuture<RemoteBookmark> bookmarkFuture;

        StatementResultImpl( AsyncSession session, Mono<ResultCursor> statementResultCursor, long sourceTag )
        {
            Flux var10001 = statementResultCursor.map( ResultCursor::keys ).flatMapMany( Flux::fromIterable );
            Mono var10002 = statementResultCursor.map( ResultCursor::consumeAsync ).flatMap( Mono::fromCompletionStage );
            Objects.requireNonNull( session );
            super( var10001, var10002, sourceTag, session::closeAsync );
            this.bookmarkFuture = new CompletableFuture();
            this.session = session;
            this.statementResultCursor = statementResultCursor;
            this.recordConverter = new RecordConverter( sourceTag );
        }

        protected Flux<Record> doGetRecords()
        {
            return this.statementResultCursor.flatMapMany( ( cursor ) -> {
                return Flux.from( new RecordPublisher( cursor, this.recordConverter ) );
            } ).doOnComplete( () -> {
                RemoteBookmark bookmark = Utils.convertBookmark( this.session.lastBookmark() );
                this.bookmarkFuture.complete( bookmark );
            } );
        }

        public Mono<RemoteBookmark> getBookmark()
        {
            return Mono.fromFuture( this.bookmarkFuture );
        }
    }
}
