package com.neo4j.fabric.driver;

import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.stream.Records;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

class RecordPublisher implements Publisher<Record>
{
    private final ResultCursor statementResultCursor;
    private final RecordConverter recordConverter;
    private Subscriber<? super Record> subscriber;
    private AtomicBoolean producing = new AtomicBoolean( false );
    private AtomicLong pendingRequests = new AtomicLong();

    RecordPublisher( ResultCursor statementResultCursor, RecordConverter recordConverter )
    {
        this.statementResultCursor = statementResultCursor;
        this.recordConverter = recordConverter;
    }

    public void subscribe( Subscriber<? super Record> subscriber )
    {
        this.subscriber = subscriber;
        subscriber.onSubscribe( new Subscription()
        {
            public void request( long l )
            {
                RecordPublisher.this.pendingRequests.addAndGet( l );
                RecordPublisher.this.maybeProduce();
            }

            public void cancel()
            {
                RecordPublisher.this.statementResultCursor.consumeAsync();
            }
        } );
    }

    private void maybeProduce()
    {
        if ( this.pendingRequests.get() != 0L && this.producing.compareAndSet( false, true ) )
        {
            this.produce();
        }
    }

    private void produce()
    {
        CompletionStage<com.neo4j.fabric.shaded.driver.Record> recordFuture = this.statementResultCursor.nextAsync();
        recordFuture.whenComplete( ( record, completionError ) -> {
            Throwable error = Futures.completionExceptionCause( completionError );
            if ( error != null )
            {
                this.subscriber.onError( error );
            }
            else if ( record == null )
            {
                this.subscriber.onComplete();
            }
            else
            {
                long pending = this.pendingRequests.decrementAndGet();

                try
                {
                    Record convertedRecord = Records.lazy( record.size(), () -> {
                        Stream var10000 = record.values().stream();
                        RecordConverter var10001 = this.recordConverter;
                        Objects.requireNonNull( var10001 );
                        return Records.of( (List) var10000.map( var10001::convertValue ).collect( Collectors.toList() ) );
                    } );
                    this.subscriber.onNext( convertedRecord );
                }
                catch ( Throwable var7 )
                {
                    this.subscriber.onError( var7 );
                    return;
                }

                if ( pending > 0L )
                {
                    this.produce();
                }
                else
                {
                    this.producing.set( false );
                    this.maybeProduce();
                }
            }
        } );
    }
}
