package com.neo4j.fabric.driver;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.shaded.driver.async.AsyncSession;
import com.neo4j.fabric.shaded.driver.async.AsyncTransaction;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.stream.StatementResult;

import java.util.Map;

import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

class FabricDriverAsyncTransaction implements FabricDriverTransaction
{
    private final ParameterConverter parameterConverter = new ParameterConverter();
    private final AsyncTransaction asyncTransaction;
    private final AsyncSession asyncSession;
    private final FabricConfig.Graph location;

    FabricDriverAsyncTransaction( AsyncTransaction asyncTransaction, AsyncSession asyncSession, FabricConfig.Graph location )
    {
        this.asyncTransaction = asyncTransaction;
        this.asyncSession = asyncSession;
        this.location = location;
    }

    public Mono<RemoteBookmark> commit()
    {
        return Mono.fromFuture( this.asyncTransaction.commitAsync().toCompletableFuture() ).then( Mono.fromSupplier( () -> {
            return Utils.convertBookmark( this.asyncSession.lastBookmark() );
        } ) ).doFinally( ( s ) -> {
            this.asyncSession.closeAsync();
        } );
    }

    public Mono<Void> rollback()
    {
        return Mono.fromFuture( this.asyncTransaction.rollbackAsync().toCompletableFuture() ).then().doFinally( ( s ) -> {
            this.asyncSession.closeAsync();
        } );
    }

    public StatementResult run( String query, MapValue params )
    {
        Map<String,Object> paramMap = (Map) this.parameterConverter.convertValue( params );
        Mono<ResultCursor> statementResultCursor = Mono.fromFuture( this.asyncTransaction.runAsync( query, paramMap ).toCompletableFuture() );
        return new FabricDriverAsyncTransaction.StatementResultImpl( statementResultCursor, this.location.getId() );
    }

    private static class StatementResultImpl extends AbstractRemoteStatementResult
    {
        private final Mono<ResultCursor> statementResultCursor;
        private final RecordConverter recordConverter;

        StatementResultImpl( Mono<ResultCursor> statementResultCursor, long sourceTag )
        {
            super( statementResultCursor.map( ResultCursor::keys ).flatMapMany( Flux::fromIterable ),
                    statementResultCursor.map( ResultCursor::consumeAsync ).flatMap( Mono::fromCompletionStage ), sourceTag );
            this.statementResultCursor = statementResultCursor;
            this.recordConverter = new RecordConverter( sourceTag );
        }

        protected Flux<Record> doGetRecords()
        {
            return this.statementResultCursor.flatMapMany( ( cursor ) -> {
                return Flux.from( new RecordPublisher( cursor, this.recordConverter ) );
            } );
        }
    }
}
