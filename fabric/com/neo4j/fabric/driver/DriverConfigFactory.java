package com.neo4j.fabric.driver;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.security.SecurityPlan;
import com.neo4j.fabric.shaded.driver.net.ServerAddress;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.ssl.SslPolicyScope;
import org.neo4j.logging.Level;
import org.neo4j.ssl.SslPolicy;
import org.neo4j.ssl.config.SslPolicyLoader;

class DriverConfigFactory
{
    private final FabricConfig fabricConfig;
    private final Level serverLogLevel;
    private final SSLContext sslContext;
    private final SslPolicy sslPolicy;

    DriverConfigFactory( FabricConfig fabricConfig, Config serverConfig, SslPolicyLoader sslPolicyLoader )
    {
        this.fabricConfig = fabricConfig;
        this.serverLogLevel = (Level) serverConfig.get( GraphDatabaseSettings.store_internal_log_level );
        if ( sslPolicyLoader.hasPolicyForSource( SslPolicyScope.FABRIC ) )
        {
            this.sslPolicy = sslPolicyLoader.getPolicy( SslPolicyScope.FABRIC );
            this.sslContext = this.createSslContext( this.sslPolicy );
        }
        else
        {
            this.sslPolicy = null;
            this.sslContext = null;
        }
    }

    com.neo4j.fabric.shaded.driver.Config createConfig( FabricConfig.Graph graph )
    {
        com.neo4j.fabric.shaded.driver.Config.ConfigBuilder builder = com.neo4j.fabric.shaded.driver.Config.builder();
        Boolean logLeakedSessions = (Boolean) this.getProperty( graph, FabricConfig.DriverConfig::getLogLeakedSessions );
        if ( logLeakedSessions )
        {
            builder.withLeakedSessionsLogging();
        }

        Duration idleTimeBeforeConnectionTest = (Duration) this.getProperty( graph, FabricConfig.DriverConfig::getIdleTimeBeforeConnectionTest );
        if ( idleTimeBeforeConnectionTest != null )
        {
            builder.withConnectionLivenessCheckTimeout( idleTimeBeforeConnectionTest.toMillis(), TimeUnit.MILLISECONDS );
        }
        else
        {
            builder.withConnectionLivenessCheckTimeout( -1L, TimeUnit.MILLISECONDS );
        }

        Duration maxConnectionLifetime = (Duration) this.getProperty( graph, FabricConfig.DriverConfig::getMaxConnectionLifetime );
        if ( maxConnectionLifetime != null )
        {
            builder.withMaxConnectionLifetime( maxConnectionLifetime.toMillis(), TimeUnit.MILLISECONDS );
        }

        Duration connectionAcquisitionTimeout = (Duration) this.getProperty( graph, FabricConfig.DriverConfig::getConnectionAcquisitionTimeout );
        if ( connectionAcquisitionTimeout != null )
        {
            builder.withConnectionAcquisitionTimeout( connectionAcquisitionTimeout.toMillis(), TimeUnit.MILLISECONDS );
        }

        Duration connectTimeout = (Duration) this.getProperty( graph, FabricConfig.DriverConfig::getConnectTimeout );
        if ( connectTimeout != null )
        {
            builder.withConnectionTimeout( connectTimeout.toMillis(), TimeUnit.MILLISECONDS );
        }

        Integer maxConnectionPoolSize = (Integer) this.getProperty( graph, FabricConfig.DriverConfig::getMaxConnectionPoolSize );
        if ( maxConnectionPoolSize != null )
        {
            builder.withMaxConnectionPoolSize( maxConnectionPoolSize );
        }

        Set<ServerAddress> serverAddresses = (Set) graph.getUri().getAddresses().stream().map( ( address ) -> {
            return ServerAddress.of( address.getHostname(), address.getPort() );
        } ).collect( Collectors.toSet() );
        return builder.withResolver( ( mainAddress ) -> {
            return serverAddresses;
        } ).withLogging( Logging.javaUtilLogging( this.getLoggingLevel( graph ) ) ).build();
    }

    SecurityPlan createSecurityPlan( FabricConfig.Graph graph )
    {
        return this.sslPolicy != null && graph.getDriverConfig().isSslEnabled() ? new DriverConfigFactory.SecurityPlanImpl( true, this.sslContext,
                this.sslPolicy.isVerifyHostname() ) : new DriverConfigFactory.SecurityPlanImpl( false, (SSLContext) null, false );
    }

    <T> T getProperty( FabricConfig.Graph graph, Function<FabricConfig.DriverConfig,T> extractor )
    {
        FabricConfig.GraphDriverConfig graphDriverConfig = graph.getDriverConfig();
        if ( graphDriverConfig != null )
        {
            T configValue = extractor.apply( graphDriverConfig );
            if ( configValue != null )
            {
                return configValue;
            }
        }

        return extractor.apply( this.fabricConfig.getGlobalDriverConfig().getDriverConfig() );
    }

    private java.util.logging.Level getLoggingLevel( FabricConfig.Graph graph )
    {
        Level loggingLevel = (Level) this.getProperty( graph, FabricConfig.DriverConfig::getLoggingLevel );
        if ( loggingLevel == null )
        {
            loggingLevel = this.serverLogLevel;
        }

        switch ( loggingLevel )
        {
        case NONE:
            return java.util.logging.Level.OFF;
        case ERROR:
            return java.util.logging.Level.SEVERE;
        case WARN:
            return java.util.logging.Level.WARNING;
        case INFO:
            return java.util.logging.Level.INFO;
        case DEBUG:
            return java.util.logging.Level.FINE;
        default:
            throw new IllegalArgumentException( "Unexpected logging level: " + loggingLevel );
        }
    }

    private SSLContext createSslContext( SslPolicy sslPolicy )
    {
        try
        {
            KeyManagerFactory keyManagerFactory = null;
            if ( sslPolicy.privateKey() != null && sslPolicy.certificateChain() != null )
            {
                KeyStore ks = KeyStore.getInstance( KeyStore.getDefaultType() );
                ks.load( (InputStream) null, (char[]) null );
                ks.setKeyEntry( "client-private-key", sslPolicy.privateKey(), (char[]) null, sslPolicy.certificateChain() );
                keyManagerFactory = KeyManagerFactory.getInstance( KeyManagerFactory.getDefaultAlgorithm() );
                keyManagerFactory.init( ks, (char[]) null );
            }

            TrustManagerFactory trustManagerFactory = sslPolicy.getTrustManagerFactory();
            SSLContext ctx = SSLContext.getInstance( "TLS" );
            KeyManager[] keyManagers = keyManagerFactory == null ? null : keyManagerFactory.getKeyManagers();
            TrustManager[] trustManagers = trustManagerFactory == null ? null : trustManagerFactory.getTrustManagers();
            ctx.init( keyManagers, trustManagers, (SecureRandom) null );
            return ctx;
        }
        catch ( IOException | GeneralSecurityException var7 )
        {
            throw new IllegalArgumentException( "Failed to build SSL context", var7 );
        }
    }

    private final class SecurityPlanImpl implements SecurityPlan
    {
        private final boolean requiresEncryption;
        private final SSLContext sslContext;
        private final boolean requiresHostnameVerification;

        SecurityPlanImpl( boolean requiresEncryption, SSLContext sslContext, boolean requiresHostnameVerification )
        {
            this.requiresEncryption = requiresEncryption;
            this.sslContext = sslContext;
            this.requiresHostnameVerification = requiresHostnameVerification;
        }

        public boolean requiresEncryption()
        {
            return this.requiresEncryption;
        }

        public SSLContext sslContext()
        {
            return this.sslContext;
        }

        public boolean requiresHostnameVerification()
        {
            return this.requiresHostnameVerification;
        }
    }
}
