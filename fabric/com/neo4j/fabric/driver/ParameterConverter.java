package com.neo4j.fabric.driver;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.spatial.CRS;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.ByteArray;
import org.neo4j.values.storable.DateTimeValue;
import org.neo4j.values.storable.DateValue;
import org.neo4j.values.storable.DoubleValue;
import org.neo4j.values.storable.DurationValue;
import org.neo4j.values.storable.LocalDateTimeValue;
import org.neo4j.values.storable.LocalTimeValue;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.NoValue;
import org.neo4j.values.storable.PointValue;
import org.neo4j.values.storable.StringValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.TimeValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.MapValue;

class ParameterConverter
{
    Object convertValue( AnyValue serverValue )
    {
        if ( serverValue instanceof NoValue )
        {
            return null;
        }
        else if ( serverValue instanceof LongValue )
        {
            return this.convertLong( (LongValue) serverValue );
        }
        else if ( serverValue instanceof DoubleValue )
        {
            return this.convertDouble( (DoubleValue) serverValue );
        }
        else if ( serverValue instanceof BooleanValue )
        {
            return this.convertBoolean( (BooleanValue) serverValue );
        }
        else if ( serverValue instanceof StringValue )
        {
            return this.convertString( (StringValue) serverValue );
        }
        else if ( serverValue instanceof MapValue )
        {
            return this.convertMap( (MapValue) serverValue );
        }
        else if ( serverValue instanceof ListValue )
        {
            return this.convertList( (ListValue) serverValue );
        }
        else if ( serverValue instanceof ByteArray )
        {
            return this.convertBytes( (ByteArray) serverValue );
        }
        else if ( serverValue instanceof DateValue )
        {
            return this.convertDate( (DateValue) serverValue );
        }
        else if ( serverValue instanceof LocalTimeValue )
        {
            return this.convertLocalTime( (LocalTimeValue) serverValue );
        }
        else if ( serverValue instanceof LocalDateTimeValue )
        {
            return this.convertLocalDateTime( (LocalDateTimeValue) serverValue );
        }
        else if ( serverValue instanceof TimeValue )
        {
            return this.convertTime( (TimeValue) serverValue );
        }
        else if ( serverValue instanceof DateTimeValue )
        {
            return this.convertDateTime( (DateTimeValue) serverValue );
        }
        else if ( serverValue instanceof DurationValue )
        {
            return this.convertDuration( (DurationValue) serverValue );
        }
        else if ( serverValue instanceof PointValue )
        {
            return this.convertPoint( (PointValue) serverValue );
        }
        else
        {
            throw new IllegalStateException( "Unsupported type encountered: " + serverValue.getClass() );
        }
    }

    private Long convertLong( LongValue serverValue )
    {
        return serverValue.asObjectCopy();
    }

    private Double convertDouble( DoubleValue serverValue )
    {
        return serverValue.asObjectCopy();
    }

    private boolean convertBoolean( BooleanValue serverValue )
    {
        return serverValue.booleanValue();
    }

    private byte[] convertBytes( ByteArray serverValue )
    {
        return serverValue.asObjectCopy();
    }

    private Map<String,Object> convertMap( MapValue serverValue )
    {
        Map<String,Object> driverValue = new HashMap();
        serverValue.foreach( ( key, value ) -> {
            driverValue.put( key, this.convertValue( value ) );
        } );
        return driverValue;
    }

    private List<Object> convertList( ListValue serverValue )
    {
        List<Object> driverValue = new ArrayList();
        serverValue.forEach( ( value ) -> {
            driverValue.add( this.convertValue( value ) );
        } );
        return driverValue;
    }

    private LocalDate convertDate( DateValue serverValue )
    {
        return (LocalDate) serverValue.asObjectCopy();
    }

    private LocalTime convertLocalTime( LocalTimeValue serverValue )
    {
        return (LocalTime) serverValue.asObjectCopy();
    }

    private LocalDateTime convertLocalDateTime( LocalDateTimeValue serverValue )
    {
        return (LocalDateTime) serverValue.asObjectCopy();
    }

    private OffsetTime convertTime( TimeValue serverValue )
    {
        return (OffsetTime) serverValue.asObjectCopy();
    }

    private ZonedDateTime convertDateTime( DateTimeValue serverValue )
    {
        return (ZonedDateTime) serverValue.asObjectCopy();
    }

    private Value convertDuration( DurationValue serverValue )
    {
        return Values.isoDuration( serverValue.get( ChronoUnit.MONTHS ), serverValue.get( ChronoUnit.DAYS ), serverValue.get( ChronoUnit.SECONDS ),
                (int) serverValue.get( ChronoUnit.NANOS ) );
    }

    private Value convertPoint( PointValue point )
    {
        double[] coordinate = point.coordinate();
        CRS crs = point.getCRS();
        return coordinate.length == 2 ? Values.point( crs.getCode(), coordinate[0], coordinate[1] )
                                      : Values.point( crs.getCode(), coordinate[0], coordinate[1], coordinate[2] );
    }

    private String convertString( TextValue serverValue )
    {
        return serverValue.stringValue();
    }
}
