package com.neo4j.fabric.driver;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.types.IsoDuration;
import com.neo4j.fabric.shaded.driver.types.MapAccessor;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Point;
import com.neo4j.fabric.shaded.driver.types.Relationship;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.ByteArray;
import org.neo4j.values.storable.CoordinateReferenceSystem;
import org.neo4j.values.storable.DateTimeValue;
import org.neo4j.values.storable.DateValue;
import org.neo4j.values.storable.DoubleValue;
import org.neo4j.values.storable.DurationValue;
import org.neo4j.values.storable.LocalDateTimeValue;
import org.neo4j.values.storable.LocalTimeValue;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.PointValue;
import org.neo4j.values.storable.TextArray;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.TimeValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.PathValue;
import org.neo4j.values.virtual.RelationshipValue;
import org.neo4j.values.virtual.VirtualValues;

class RecordConverter
{
    private static final int ID_MAX_BITS = 50;
    private static final long TAG_MAX_VALUE = 16383L;
    private final boolean hasSourceTag;
    private final long sourceTagValue;

    RecordConverter()
    {
        this.hasSourceTag = true;
        this.sourceTagValue = 0L;
    }

    RecordConverter( long sourceTag )
    {
        if ( sourceTag >= 0L && 16383L >= sourceTag )
        {
            this.hasSourceTag = true;
            this.sourceTagValue = this.shiftToMsb( sourceTag );
        }
        else
        {
            throw new IllegalArgumentException( "Source tags must be in range 0-16383. Got: " + sourceTag );
        }
    }

    private long shiftToMsb( long value )
    {
        return value << 50;
    }

    AnyValue convertValue( Value driverValue )
    {
        Object value = driverValue.asObject();
        return this.convertValue( value );
    }

    private AnyValue convertValue( Object driverValue )
    {
        if ( driverValue == null )
        {
            return Values.NO_VALUE;
        }
        else if ( driverValue instanceof Node )
        {
            return this.convertNode( (Node) driverValue );
        }
        else if ( driverValue instanceof Relationship )
        {
            return this.convertRelationship( (Relationship) driverValue );
        }
        else if ( driverValue instanceof Path )
        {
            return this.convertPath( (Path) driverValue );
        }
        else if ( driverValue instanceof Long )
        {
            return this.convertLong( (Long) driverValue );
        }
        else if ( driverValue instanceof Double )
        {
            return this.convertDouble( (Double) driverValue );
        }
        else if ( driverValue instanceof Boolean )
        {
            return this.convertBoolean( (Boolean) driverValue );
        }
        else if ( driverValue instanceof String )
        {
            return this.convertString( (String) driverValue );
        }
        else if ( driverValue instanceof Map )
        {
            return this.convertMap( (Map) driverValue );
        }
        else if ( driverValue instanceof List )
        {
            return this.convertList( (List) driverValue );
        }
        else if ( driverValue instanceof byte[] )
        {
            return this.convertBytes( (byte[]) driverValue );
        }
        else if ( driverValue instanceof LocalDate )
        {
            return this.convertDate( (LocalDate) driverValue );
        }
        else if ( driverValue instanceof LocalTime )
        {
            return this.convertLocalTime( (LocalTime) driverValue );
        }
        else if ( driverValue instanceof LocalDateTime )
        {
            return this.convertLocalDateTime( (LocalDateTime) driverValue );
        }
        else if ( driverValue instanceof OffsetTime )
        {
            return this.convertTime( (OffsetTime) driverValue );
        }
        else if ( driverValue instanceof ZonedDateTime )
        {
            return this.convertDateTime( (ZonedDateTime) driverValue );
        }
        else if ( driverValue instanceof IsoDuration )
        {
            return this.convertDuration( (IsoDuration) driverValue );
        }
        else if ( driverValue instanceof Point )
        {
            return this.convertPoint( (Point) driverValue );
        }
        else
        {
            throw new IllegalStateException( "Unsupported type encountered: " + driverValue.getClass() );
        }
    }

    private LongValue convertLong( Long driverValue )
    {
        return Values.longValue( driverValue );
    }

    private DoubleValue convertDouble( Double driverValue )
    {
        return Values.doubleValue( driverValue );
    }

    private BooleanValue convertBoolean( Boolean driverValue )
    {
        return Values.booleanValue( driverValue );
    }

    private ByteArray convertBytes( byte[] driverValue )
    {
        return Values.byteArray( driverValue );
    }

    private long convertId( long driverValue )
    {
        return this.hasSourceTag ? driverValue | this.sourceTagValue : driverValue;
    }

    private NodeValue convertNode( Node driverValue )
    {
        String[] labels = (String[]) Iterables.asArray( String.class, driverValue.labels() );
        TextArray serverLabels = this.convertStringArray( labels );
        MapValue properties = this.convertMap( (MapAccessor) driverValue );
        return VirtualValues.nodeValue( this.convertId( driverValue.id() ), serverLabels, properties );
    }

    private RelationshipValue convertRelationship( Relationship driverValue )
    {
        NodeValue startNode = this.convertNodeReference( this.convertId( driverValue.startNodeId() ) );
        NodeValue endNode = this.convertNodeReference( this.convertId( driverValue.endNodeId() ) );
        return this.convertRelationship( driverValue, startNode, endNode );
    }

    private RelationshipValue convertRelationship( Relationship driverValue, NodeValue startNode, NodeValue endNode )
    {
        TextValue type = this.convertString( driverValue.type() );
        MapValue properties = this.convertMap( (MapAccessor) driverValue );
        return VirtualValues.relationshipValue( this.convertId( driverValue.id() ), startNode, endNode, type, properties );
    }

    private MapValue convertMap( MapAccessor driverValue )
    {
        String[] keys = new String[driverValue.size()];
        AnyValue[] values = new AnyValue[driverValue.size()];
        int i = 0;

        for ( Iterator var5 = driverValue.keys().iterator(); var5.hasNext(); ++i )
        {
            String key = (String) var5.next();
            keys[i] = key;
            values[i] = this.convertValue( driverValue.get( key ) );
        }

        return VirtualValues.map( keys, values );
    }

    private PathValue convertPath( Path driverValue )
    {
        Map<Long,NodeValue> encounteredNodes = new HashMap( driverValue.length() + 1 );
        NodeValue[] nodes = new NodeValue[driverValue.length() + 1];
        RelationshipValue[] relationships = new RelationshipValue[driverValue.length()];
        int i = 0;

        Iterator var6;
        NodeValue startNode;
        for ( var6 = driverValue.nodes().iterator(); var6.hasNext(); ++i )
        {
            Node driverNode = (Node) var6.next();
            startNode = this.convertNode( driverNode );
            nodes[i] = startNode;
            encounteredNodes.put( driverNode.id(), startNode );
        }

        i = 0;

        for ( var6 = driverValue.relationships().iterator(); var6.hasNext(); ++i )
        {
            Relationship driverRelationship = (Relationship) var6.next();
            startNode = (NodeValue) encounteredNodes.get( driverRelationship.startNodeId() );
            NodeValue endNode = (NodeValue) encounteredNodes.get( driverRelationship.endNodeId() );
            RelationshipValue relationship = this.convertRelationship( driverRelationship, startNode, endNode );
            relationships[i] = relationship;
        }

        return VirtualValues.path( nodes, relationships );
    }

    private NodeValue convertNodeReference( long nodeId )
    {
        return VirtualValues.nodeValue( nodeId, this.convertStringArray( new String[0] ), this.convertMap( Collections.emptyMap() ) );
    }

    private TextArray convertStringArray( String[] driverValue )
    {
        return Values.stringArray( driverValue );
    }

    private MapValue convertMap( Map<String,Object> driverValue )
    {
        String[] keys = new String[driverValue.size()];
        AnyValue[] values = new AnyValue[driverValue.size()];
        int i = 0;

        for ( Iterator var5 = driverValue.entrySet().iterator(); var5.hasNext(); ++i )
        {
            Entry<String,Object> e = (Entry) var5.next();
            keys[i] = (String) e.getKey();
            values[i] = this.convertValue( e.getValue() );
        }

        return VirtualValues.map( keys, values );
    }

    private ListValue convertList( List<Object> driverValue )
    {
        AnyValue[] listValues = new AnyValue[driverValue.size()];

        for ( int i = 0; i < driverValue.size(); ++i )
        {
            listValues[i] = this.convertValue( driverValue.get( i ) );
        }

        return VirtualValues.list( listValues );
    }

    private DateValue convertDate( LocalDate driverValue )
    {
        return DateValue.date( driverValue );
    }

    private LocalTimeValue convertLocalTime( LocalTime driverValue )
    {
        return LocalTimeValue.localTime( driverValue );
    }

    private LocalDateTimeValue convertLocalDateTime( LocalDateTime driverValue )
    {
        return LocalDateTimeValue.localDateTime( driverValue );
    }

    private TimeValue convertTime( OffsetTime driverValue )
    {
        return TimeValue.time( driverValue );
    }

    private DateTimeValue convertDateTime( ZonedDateTime driverValue )
    {
        return DateTimeValue.datetime( driverValue );
    }

    private DurationValue convertDuration( IsoDuration driverValue )
    {
        return DurationValue.duration( driverValue.months(), driverValue.days(), driverValue.seconds(), (long) driverValue.nanoseconds() );
    }

    private PointValue convertPoint( Point point )
    {
        CoordinateReferenceSystem coordinateReferenceSystem = CoordinateReferenceSystem.get( point.srid() );
        return Double.isNaN( point.z() ) ? Values.pointValue( coordinateReferenceSystem, new double[]{point.x(), point.y()} )
                                         : Values.pointValue( coordinateReferenceSystem, new double[]{point.x(), point.y(), point.z()} );
    }

    private TextValue convertString( String driverValue )
    {
        return Values.stringValue( driverValue );
    }
}
