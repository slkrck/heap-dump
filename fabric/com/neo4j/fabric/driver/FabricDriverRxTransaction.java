package com.neo4j.fabric.driver;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.shaded.driver.reactive.RxResult;
import com.neo4j.fabric.shaded.driver.reactive.RxSession;
import com.neo4j.fabric.shaded.driver.reactive.RxTransaction;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.stream.StatementResult;

import java.util.Map;

import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

class FabricDriverRxTransaction implements FabricDriverTransaction
{
    private final ParameterConverter parameterConverter = new ParameterConverter();
    private final RxTransaction rxTransaction;
    private final RxSession rxSession;
    private final FabricConfig.Graph location;

    FabricDriverRxTransaction( RxTransaction rxTransaction, RxSession rxSession, FabricConfig.Graph location )
    {
        this.rxTransaction = rxTransaction;
        this.rxSession = rxSession;
        this.location = location;
    }

    public Mono<RemoteBookmark> commit()
    {
        return Mono.from( this.rxTransaction.commit() ).then( Mono.fromSupplier( () -> {
            return Utils.convertBookmark( this.rxSession.lastBookmark() );
        } ) ).doFinally( ( s ) -> {
            this.rxSession.close();
        } );
    }

    public Mono<Void> rollback()
    {
        return Mono.from( this.rxTransaction.rollback() ).then().doFinally( ( s ) -> {
            this.rxSession.close();
        } );
    }

    public StatementResult run( String query, MapValue params )
    {
        Map<String,Object> paramMap = (Map) this.parameterConverter.convertValue( params );
        RxResult rxStatementResult = this.rxTransaction.run( query, paramMap );
        return new FabricDriverRxTransaction.StatementResultImpl( rxStatementResult, this.location.getId() );
    }

    private static class StatementResultImpl extends AbstractRemoteStatementResult
    {
        private final RxResult rxStatementResult;

        StatementResultImpl( RxResult rxStatementResult, long sourceTag )
        {
            super( Mono.from( rxStatementResult.keys() ).flatMapMany( Flux::fromIterable ), Mono.from( rxStatementResult.consume() ), sourceTag );
            this.rxStatementResult = rxStatementResult;
        }

        protected Flux<Record> doGetRecords()
        {
            return this.convertRxRecords( Flux.from( this.rxStatementResult.records() ) );
        }
    }
}
