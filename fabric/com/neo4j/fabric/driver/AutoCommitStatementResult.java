package com.neo4j.fabric.driver;

import com.neo4j.fabric.stream.StatementResult;
import reactor.core.publisher.Mono;

public interface AutoCommitStatementResult extends StatementResult
{
    Mono<RemoteBookmark> getBookmark();
}
