package com.neo4j.fabric.driver;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.shaded.driver.Driver;
import com.neo4j.fabric.shaded.driver.SessionConfig;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.reactive.RxResult;
import com.neo4j.fabric.shaded.driver.reactive.RxSession;
import com.neo4j.fabric.shaded.driver.reactive.RxTransaction;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.transaction.FabricTransactionInfo;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

class RxPooledDriver extends PooledDriver
{
    private final Driver driver;

    RxPooledDriver( Driver driver, Consumer<PooledDriver> releaseCallback )
    {
        super( driver, releaseCallback );
        this.driver = driver;
    }

    public AutoCommitStatementResult run( String query, MapValue params, FabricConfig.Graph location, AccessMode accessMode,
            FabricTransactionInfo transactionInfo, List<RemoteBookmark> bookmarks )
    {
        SessionConfig sessionConfig = this.createSessionConfig( location, accessMode, bookmarks );
        RxSession session = this.driver.rxSession( sessionConfig );
        ParameterConverter parameterConverter = new ParameterConverter();
        Map<String,Object> paramMap = (Map) parameterConverter.convertValue( params );
        TransactionConfig transactionConfig = this.getTransactionConfig( transactionInfo );
        RxResult rxResult = session.run( query, paramMap, transactionConfig );
        return new RxPooledDriver.StatementResultImpl( session, rxResult, location.getId() );
    }

    public Mono<FabricDriverTransaction> beginTransaction( FabricConfig.Graph location, AccessMode accessMode, FabricTransactionInfo transactionInfo,
            List<RemoteBookmark> bookmarks )
    {
        SessionConfig sessionConfig = this.createSessionConfig( location, accessMode, bookmarks );
        RxSession session = this.driver.rxSession( sessionConfig );
        Mono<RxTransaction> driverTransaction = this.getDriverTransaction( session, transactionInfo );
        return driverTransaction.map( ( tx ) -> {
            return new FabricDriverRxTransaction( tx, session, location );
        } );
    }

    private Mono<RxTransaction> getDriverTransaction( RxSession session, FabricTransactionInfo transactionInfo )
    {
        TransactionConfig transactionConfig = this.getTransactionConfig( transactionInfo );
        return Mono.from( session.beginTransaction( transactionConfig ) ).cache();
    }

    private static class StatementResultImpl extends AbstractRemoteStatementResult implements AutoCommitStatementResult
    {
        private final RxSession session;
        private final RxResult rxResult;
        private final CompletableFuture<RemoteBookmark> bookmarkFuture;

        StatementResultImpl( RxSession session, RxResult rxResult, long sourceTag )
        {
            Flux var10001 = Mono.from( rxResult.keys() ).flatMapMany( Flux::fromIterable );
            Mono var10002 = Mono.from( rxResult.consume() );
            Objects.requireNonNull( session );
            super( var10001, var10002, sourceTag, session::close );
            this.bookmarkFuture = new CompletableFuture();
            this.session = session;
            this.rxResult = rxResult;
        }

        public Mono<RemoteBookmark> getBookmark()
        {
            return Mono.fromFuture( this.bookmarkFuture );
        }

        protected Flux<Record> doGetRecords()
        {
            return this.convertRxRecords( Flux.from( this.rxResult.records() ) ).doOnComplete( () -> {
                RemoteBookmark bookmark = Utils.convertBookmark( this.session.lastBookmark() );
                this.bookmarkFuture.complete( bookmark );
            } );
        }
    }
}
