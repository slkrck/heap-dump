package com.neo4j.fabric.driver;

import com.neo4j.fabric.stream.StatementResult;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Mono;

public interface FabricDriverTransaction
{
    Mono<RemoteBookmark> commit();

    Mono<Void> rollback();

    StatementResult run( String var1, MapValue var2 );
}
