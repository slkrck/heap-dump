package com.neo4j.fabric.driver;

import com.neo4j.fabric.shaded.driver.summary.ResultSummary;
import com.neo4j.fabric.shaded.driver.summary.SummaryCounters;
import com.neo4j.fabric.stream.summary.EmptySummary;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.neo4j.graphdb.InputPosition;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.graphdb.SeverityLevel;

public class ResultSummaryWrapper extends EmptySummary
{
    private final QueryStatistics statistics;
    private final List<Notification> notifications;

    public ResultSummaryWrapper( ResultSummary summary )
    {
        this.statistics = new ResultSummaryWrapper.SummaryCountersWrapper( summary.counters() );
        this.notifications = (List) summary.notifications().stream().map( ResultSummaryWrapper.NotificationWrapper::new ).collect( Collectors.toList() );
    }

    public Collection<Notification> getNotifications()
    {
        return this.notifications;
    }

    public QueryStatistics getQueryStatistics()
    {
        return this.statistics;
    }

    public static class SummaryCountersWrapper implements QueryStatistics
    {
        private final SummaryCounters counters;

        SummaryCountersWrapper( SummaryCounters counters )
        {
            this.counters = counters;
        }

        public int getNodesCreated()
        {
            return this.counters.nodesCreated();
        }

        public int getNodesDeleted()
        {
            return this.counters.nodesDeleted();
        }

        public int getRelationshipsCreated()
        {
            return this.counters.relationshipsCreated();
        }

        public int getRelationshipsDeleted()
        {
            return this.counters.relationshipsDeleted();
        }

        public int getPropertiesSet()
        {
            return this.counters.propertiesSet();
        }

        public int getLabelsAdded()
        {
            return this.counters.labelsAdded();
        }

        public int getLabelsRemoved()
        {
            return this.counters.labelsRemoved();
        }

        public int getIndexesAdded()
        {
            return this.counters.indexesAdded();
        }

        public int getIndexesRemoved()
        {
            return this.counters.indexesRemoved();
        }

        public int getConstraintsAdded()
        {
            return this.counters.constraintsAdded();
        }

        public int getConstraintsRemoved()
        {
            return this.counters.constraintsRemoved();
        }

        public int getSystemUpdates()
        {
            return 0;
        }

        public boolean containsUpdates()
        {
            return this.counters.containsUpdates();
        }

        public boolean containsSystemUpdates()
        {
            return false;
        }
    }

    static class NotificationWrapper implements Notification
    {
        private final com.neo4j.fabric.shaded.driver.summary.Notification notification;

        NotificationWrapper( com.neo4j.fabric.shaded.driver.summary.Notification notification )
        {
            this.notification = notification;
        }

        public String getCode()
        {
            return this.notification.code();
        }

        public String getTitle()
        {
            return this.notification.title();
        }

        public String getDescription()
        {
            return this.notification.description();
        }

        public SeverityLevel getSeverity()
        {
            return SeverityLevel.valueOf( this.notification.severity() );
        }

        public InputPosition getPosition()
        {
            com.neo4j.fabric.shaded.driver.summary.InputPosition pos = this.notification.position();
            return new InputPosition( pos.offset(), pos.line(), pos.column() );
        }
    }
}
