package com.neo4j.fabric.driver;

import com.neo4j.fabric.shaded.driver.Bookmark;

import java.util.Set;

public class Utils
{
    static RemoteBookmark convertBookmark( Bookmark bookmark )
    {
        Set<String> serialisedBookmark = bookmark == null ? Set.of() : bookmark.values();
        return new RemoteBookmark( serialisedBookmark );
    }
}
