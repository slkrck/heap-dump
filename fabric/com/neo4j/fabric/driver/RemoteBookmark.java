package com.neo4j.fabric.driver;

import java.util.Objects;
import java.util.Set;

public class RemoteBookmark
{
    private final Set<String> serialisedState;

    public RemoteBookmark( Set<String> serialisedState )
    {
        this.serialisedState = serialisedState;
    }

    public Set<String> getSerialisedState()
    {
        return this.serialisedState;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            RemoteBookmark that = (RemoteBookmark) o;
            return this.serialisedState.equals( that.serialisedState );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.serialisedState} );
    }
}
