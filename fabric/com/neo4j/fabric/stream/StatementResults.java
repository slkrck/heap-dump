package com.neo4j.fabric.stream;

import com.neo4j.fabric.executor.FabricException;
import com.neo4j.fabric.stream.summary.Summary;

import java.util.function.Function;

import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.kernel.api.exceptions.Status.General;
import org.neo4j.kernel.impl.query.QueryExecution;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class StatementResults
{
    public static StatementResult map( StatementResult statementResult, Function<Flux<Record>,Flux<Record>> func )
    {
        return new StatementResults.BasicStatementResult( statementResult.columns(), (Flux) func.apply( statementResult.records() ),
                statementResult.summary() );
    }

    public static StatementResult initial()
    {
        return new StatementResults.BasicStatementResult( Flux.empty(), Flux.just( Records.empty() ), Mono.empty() );
    }

    public static StatementResult create( Function<QuerySubscriber,QueryExecution> execution )
    {
        try
        {
            StatementResults.QuerySubject querySubject = new StatementResults.QuerySubject();
            QueryExecution queryExecution = (QueryExecution) execution.apply( querySubject );
            querySubject.setQueryExecution( queryExecution );
            return create( Flux.fromArray( queryExecution.fieldNames() ), Flux.from( querySubject ), Mono.empty() );
        }
        catch ( RuntimeException var3 )
        {
            return error( var3 );
        }
    }

    public static StatementResult create( Flux<String> columns, Flux<Record> records, Mono<Summary> summary )
    {
        return new StatementResults.BasicStatementResult( columns, records, summary );
    }

    public static StatementResult error( Throwable err )
    {
        return new StatementResults.BasicStatementResult( Flux.error( err ), Flux.error( err ), Mono.error( err ) );
    }

    public static StatementResult trace( StatementResult input )
    {
        return new StatementResults.BasicStatementResult( input.columns(), input.records().doOnEach( ( signal ) -> {
            if ( signal.hasValue() )
            {
                System.out.println( String.join( ", ", signal.getType().toString(), Records.show( (Record) signal.get() ) ) );
            }
            else if ( signal.hasError() )
            {
                System.out.println( String.join( ", ", signal.getType().toString(), signal.getThrowable().toString() ) );
            }
            else
            {
                System.out.println( String.join( ", ", signal.getType().toString() ) );
            }
        } ), input.summary() );
    }

    private abstract static class RecordQuerySubscriber implements QuerySubscriber
    {
        private int numberOfFields;
        private AnyValue[] fields;
        private int fieldIndex;

        public void onResult( int numberOfFields )
        {
            this.numberOfFields = numberOfFields;
        }

        public void onRecord()
        {
            this.fields = new AnyValue[this.numberOfFields];
            this.fieldIndex = 0;
        }

        public void onField( AnyValue value )
        {
            this.fields[this.fieldIndex] = value;
            ++this.fieldIndex;
        }

        public void onRecordCompleted()
        {
            this.onNext( Records.of( this.fields ) );
        }

        abstract void onNext( Record var1 );
    }

    private static class QuerySubject extends StatementResults.RecordQuerySubscriber implements Publisher<Record>
    {
        private Subscriber<? super Record> subscriber;
        private QueryExecution queryExecution;
        private Throwable cachedError;
        private boolean cachedCompleted;
        private boolean errorReceived;

        void setQueryExecution( QueryExecution queryExecution )
        {
            this.queryExecution = queryExecution;
        }

        public void onNext( Record record )
        {
            this.subscriber.onNext( record );
        }

        public void onError( Throwable throwable )
        {
            this.errorReceived = true;
            if ( this.subscriber == null )
            {
                this.cachedError = throwable;
            }
            else
            {
                this.subscriber.onError( throwable );
            }
        }

        public void onResultCompleted( QueryStatistics statistics )
        {
            if ( this.subscriber == null )
            {
                this.cachedCompleted = true;
            }
            else
            {
                this.subscriber.onComplete();
            }
        }

        public void subscribe( final Subscriber<? super Record> subscriber )
        {
            if ( this.subscriber != null )
            {
                throw new FabricException( General.UnknownError, "Already subscribed", new Object[0] );
            }
            else
            {
                this.subscriber = subscriber;
                Subscription subscription = new Subscription()
                {
                    private final Object requestLock = new Object();
                    private long pendingRequests;
                    private boolean producing;

                    public void request( long size )
                    {
                        synchronized ( this.requestLock )
                        {
                            this.pendingRequests += size;
                            if ( this.producing )
                            {
                                return;
                            }

                            this.producing = true;
                        }

                        while ( true )
                        {
                            boolean var17 = false;

                            try
                            {
                                var17 = true;
                                long toRequest;
                                synchronized ( this.requestLock )
                                {
                                    toRequest = this.pendingRequests;
                                    if ( toRequest == 0L )
                                    {
                                        var17 = false;
                                        break;
                                    }

                                    this.pendingRequests = 0L;
                                }

                                this.doRequest( toRequest );
                            }
                            finally
                            {
                                if ( var17 )
                                {
                                    synchronized ( this.requestLock )
                                    {
                                        this.producing = false;
                                    }
                                }
                            }
                        }

                        synchronized ( this.requestLock )
                        {
                            this.producing = false;
                        }
                    }

                    private void doRequest( long size )
                    {
                        QuerySubject.this.maybeSendCachedEvents();

                        try
                        {
                            QuerySubject.this.queryExecution.request( size );
                            if ( !QuerySubject.this.errorReceived )
                            {
                                QuerySubject.this.queryExecution.await();
                            }
                        }
                        catch ( Exception var4 )
                        {
                            subscriber.onError( var4 );
                        }
                    }

                    public void cancel()
                    {
                        QuerySubject.this.queryExecution.cancel();
                    }
                };
                subscriber.onSubscribe( subscription );
                this.maybeSendCachedEvents();
            }
        }

        private void maybeSendCachedEvents()
        {
            if ( this.cachedError != null )
            {
                this.subscriber.onError( this.cachedError );
                this.cachedError = null;
            }
            else if ( this.cachedCompleted )
            {
                this.subscriber.onComplete();
                this.cachedCompleted = false;
            }
        }
    }

    private static class BasicStatementResult implements StatementResult
    {
        private final Flux<String> columns;
        private final Flux<Record> records;
        private final Mono<Summary> summary;

        BasicStatementResult( Flux<String> columns, Flux<Record> records, Mono<Summary> summary )
        {
            this.columns = columns;
            this.records = records;
            this.summary = summary;
        }

        public Flux<String> columns()
        {
            return this.columns;
        }

        public Flux<Record> records()
        {
            return this.records;
        }

        public Mono<Summary> summary()
        {
            return this.summary;
        }
    }
}
