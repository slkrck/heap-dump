package com.neo4j.fabric.stream;

import com.neo4j.fabric.executor.Exceptions;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;

public class Rx2SyncStream
{
    private static final Rx2SyncStream.RecordOrError END = new Rx2SyncStream.RecordOrError( (Record) null, (Throwable) null );
    private final Rx2SyncStream.RecordSubscriber recordSubscriber;
    private final BlockingQueue<Rx2SyncStream.RecordOrError> buffer;
    private final int batchSize;

    public Rx2SyncStream( Flux<Record> records, int batchSize )
    {
        this.batchSize = batchSize;
        this.buffer = new ArrayBlockingQueue( batchSize + 1 );
        this.recordSubscriber = new Rx2SyncStream.RecordSubscriber();
        records.subscribeWith( this.recordSubscriber );
    }

    public Record readRecord()
    {
        this.maybeRequest();

        Rx2SyncStream.RecordOrError recordOrError;
        try
        {
            recordOrError = (Rx2SyncStream.RecordOrError) this.buffer.take();
        }
        catch ( InterruptedException var3 )
        {
            this.recordSubscriber.close();
            throw new IllegalStateException( var3 );
        }

        if ( recordOrError == END )
        {
            return null;
        }
        else if ( recordOrError.error != null )
        {
            throw Exceptions.transform( Statement.ExecutionFailed, recordOrError.error );
        }
        else
        {
            return recordOrError.record;
        }
    }

    public void close()
    {
        this.recordSubscriber.close();
    }

    private void maybeRequest()
    {
        int buffered = this.buffer.size();
        long pendingRequested = this.recordSubscriber.pendingRequested.get();
        if ( pendingRequested + (long) buffered == 0L )
        {
            this.recordSubscriber.request( (long) this.batchSize );
        }
    }

    private static class RecordOrError
    {
        private final Record record;
        private final Throwable error;

        RecordOrError( Record record, Throwable error )
        {
            this.record = record;
            this.error = error;
        }
    }

    private class RecordSubscriber implements Subscriber<Record>
    {
        private volatile Subscription subscription;
        private AtomicLong pendingRequested = new AtomicLong( 0L );

        public void onSubscribe( Subscription subscription )
        {
            this.subscription = subscription;
        }

        public void onNext( Record record )
        {
            this.pendingRequested.decrementAndGet();
            Rx2SyncStream.this.buffer.add( new Rx2SyncStream.RecordOrError( record, (Throwable) null ) );
        }

        public void onError( Throwable throwable )
        {
            Rx2SyncStream.this.buffer.add( new Rx2SyncStream.RecordOrError( (Record) null, throwable ) );
        }

        public void onComplete()
        {
            Rx2SyncStream.this.buffer.add( Rx2SyncStream.END );
        }

        void request( long numberOfRecords )
        {
            this.pendingRequested.addAndGet( numberOfRecords );
            this.subscription.request( numberOfRecords );
        }

        void close()
        {
            this.subscription.cancel();
        }
    }
}
