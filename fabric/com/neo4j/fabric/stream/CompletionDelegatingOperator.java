package com.neo4j.fabric.stream;

import java.util.Objects;
import java.util.concurrent.Executor;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxOperator;

public class CompletionDelegatingOperator extends FluxOperator<Record,Record>
{
    private final Flux<Record> recordStream;
    private final Executor executor;

    public CompletionDelegatingOperator( Flux<Record> recordStream, Executor executor )
    {
        super( recordStream );
        this.recordStream = recordStream;
        this.executor = executor;
    }

    public void subscribe( CoreSubscriber downstreamSubscriber )
    {
        this.recordStream.subscribeWith( new CompletionDelegatingOperator.UpstreamSubscriber( downstreamSubscriber ) );
    }

    private class UpstreamSubscriber implements Subscriber<Record>
    {
        private final Subscriber<Record> downstreamSubscriber;

        UpstreamSubscriber( Subscriber<Record> downstreamSubscriber )
        {
            this.downstreamSubscriber = downstreamSubscriber;
        }

        public void onSubscribe( Subscription subscription )
        {
            this.downstreamSubscriber.onSubscribe( subscription );
        }

        public void onNext( Record record )
        {
            this.downstreamSubscriber.onNext( record );
        }

        public void onError( Throwable throwable )
        {
            CompletionDelegatingOperator.this.executor.execute( () -> {
                this.downstreamSubscriber.onError( throwable );
            } );
        }

        public void onComplete()
        {
            Executor var10000 = CompletionDelegatingOperator.this.executor;
            Subscriber var10001 = this.downstreamSubscriber;
            Objects.requireNonNull( var10001 );
            var10000.execute( var10001::onComplete );
        }
    }
}
