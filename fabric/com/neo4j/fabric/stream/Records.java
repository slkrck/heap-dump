package com.neo4j.fabric.stream;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.neo4j.values.AnyValue;

public class Records
{
    public static Record empty()
    {
        return of( List.of() );
    }

    public static Record of( List<AnyValue> values )
    {
        return new Records.ListRecord( values );
    }

    public static Record of( AnyValue[] values )
    {
        return new Records.ListRecord( List.of( values ) );
    }

    public static Record join( Record lhs, Record rhs )
    {
        return new Records.JoinedRecord( lhs, rhs );
    }

    public static Record lazy( int size, Supplier<Record> recordSupplier )
    {
        return new Records.LazyConvertingRecord( size, recordSupplier );
    }

    public static Map<String,AnyValue> asMap( Record record, List<String> columns )
    {
        HashMap<String,AnyValue> map = new HashMap();

        for ( int i = 0; i < columns.size(); ++i )
        {
            map.put( (String) columns.get( i ), record.getValue( i ) );
        }

        return map;
    }

    public static Iterator<AnyValue> iterator( final Record record )
    {
        return new Iterator<AnyValue>()
        {
            private int i;

            public boolean hasNext()
            {
                return this.i < record.size();
            }

            public AnyValue next()
            {
                if ( !this.hasNext() )
                {
                    throw new NoSuchElementException();
                }
                else
                {
                    return record.getValue( this.i++ );
                }
            }
        };
    }

    public static Iterable<AnyValue> iterable( Record record )
    {
        return () -> {
            return iterator( record );
        };
    }

    public static Stream<AnyValue> stream( Record record )
    {
        return StreamSupport.stream( iterable( record ).spliterator(), false );
    }

    public static String show( Record record )
    {
        return (String) stream( record ).map( Object::toString ).collect( Collectors.joining( ", ", "[", "]" ) );
    }

    private static class LazyConvertingRecord extends Record
    {
        private final int size;
        private final Supplier<Record> recordSupplier;
        private Record convertedRecord;

        LazyConvertingRecord( int size, Supplier<Record> recordSupplier )
        {
            this.size = size;
            this.recordSupplier = recordSupplier;
        }

        public AnyValue getValue( int offset )
        {
            this.maybeConvert();
            return this.convertedRecord.getValue( offset );
        }

        public int size()
        {
            return this.size;
        }

        private void maybeConvert()
        {
            if ( this.convertedRecord == null )
            {
                this.convertedRecord = (Record) this.recordSupplier.get();
            }
        }
    }

    private static class JoinedRecord extends Record
    {
        private final Record lhs;
        private final Record rhs;

        private JoinedRecord( Record lhs, Record rhs )
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }

        public AnyValue getValue( int offset )
        {
            return offset < this.lhs.size() ? this.lhs.getValue( offset ) : this.rhs.getValue( offset - this.lhs.size() );
        }

        public int size()
        {
            return this.lhs.size() + this.rhs.size();
        }
    }

    private static class ListRecord extends Record
    {
        private final List<AnyValue> values;

        private ListRecord( List<AnyValue> values )
        {
            this.values = values;
        }

        public AnyValue getValue( int offset )
        {
            return (AnyValue) this.values.get( offset );
        }

        public int size()
        {
            return this.values.size();
        }
    }
}
