package com.neo4j.fabric.stream;

import com.neo4j.fabric.stream.summary.Summary;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StatementResult
{
    Flux<String> columns();

    Flux<Record> records();

    Mono<Summary> summary();
}
