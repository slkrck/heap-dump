package com.neo4j.fabric.stream.summary;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.ExecutionPlanDescription.ProfilerStatistics;

public class EmptyExecutionPlanDescription implements ExecutionPlanDescription
{
    public String getName()
    {
        return "";
    }

    public List<ExecutionPlanDescription> getChildren()
    {
        return Collections.emptyList();
    }

    public Map<String,Object> getArguments()
    {
        return Collections.emptyMap();
    }

    public Set<String> getIdentifiers()
    {
        return Collections.emptySet();
    }

    public boolean hasProfilerStatistics()
    {
        return false;
    }

    public ProfilerStatistics getProfilerStatistics()
    {
        return null;
    }
}
