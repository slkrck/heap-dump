package com.neo4j.fabric.stream.summary;

import java.util.concurrent.atomic.AtomicInteger;

import org.neo4j.graphdb.QueryStatistics;

public class MergedQueryStatistics implements QueryStatistics
{
    private final AtomicInteger nodesCreated = new AtomicInteger( 0 );
    private final AtomicInteger nodesDeleted = new AtomicInteger( 0 );
    private final AtomicInteger relationshipsCreated = new AtomicInteger( 0 );
    private final AtomicInteger relationshipsDeleted = new AtomicInteger( 0 );
    private final AtomicInteger propertiesSet = new AtomicInteger( 0 );
    private final AtomicInteger labelsAdded = new AtomicInteger( 0 );
    private final AtomicInteger labelsRemoved = new AtomicInteger( 0 );
    private final AtomicInteger indexesAdded = new AtomicInteger( 0 );
    private final AtomicInteger indexesRemoved = new AtomicInteger( 0 );
    private final AtomicInteger constraintsAdded = new AtomicInteger( 0 );
    private final AtomicInteger constraintsRemoved = new AtomicInteger( 0 );
    private final AtomicInteger systemUpdates = new AtomicInteger( 0 );
    private boolean containsUpdates;
    private boolean containsSystemUpdates;

    public void add( QueryStatistics delta )
    {
        this.nodesCreated.addAndGet( delta.getNodesCreated() );
        this.nodesDeleted.addAndGet( delta.getNodesDeleted() );
        this.relationshipsCreated.addAndGet( delta.getRelationshipsCreated() );
        this.relationshipsDeleted.addAndGet( delta.getRelationshipsDeleted() );
        this.propertiesSet.addAndGet( delta.getPropertiesSet() );
        this.labelsAdded.addAndGet( delta.getLabelsAdded() );
        this.labelsRemoved.addAndGet( delta.getLabelsRemoved() );
        this.indexesAdded.addAndGet( delta.getIndexesAdded() );
        this.indexesRemoved.addAndGet( delta.getIndexesRemoved() );
        this.constraintsAdded.addAndGet( delta.getConstraintsAdded() );
        this.constraintsRemoved.addAndGet( delta.getConstraintsRemoved() );
        this.systemUpdates.addAndGet( delta.getSystemUpdates() );
        if ( delta.containsUpdates() )
        {
            this.containsUpdates = true;
        }

        if ( delta.containsSystemUpdates() )
        {
            this.containsSystemUpdates = true;
        }
    }

    public int getNodesCreated()
    {
        return this.nodesCreated.get();
    }

    public int getNodesDeleted()
    {
        return this.nodesDeleted.get();
    }

    public int getRelationshipsCreated()
    {
        return this.relationshipsCreated.get();
    }

    public int getRelationshipsDeleted()
    {
        return this.relationshipsDeleted.get();
    }

    public int getPropertiesSet()
    {
        return this.propertiesSet.get();
    }

    public int getLabelsAdded()
    {
        return this.labelsAdded.get();
    }

    public int getLabelsRemoved()
    {
        return this.labelsRemoved.get();
    }

    public int getIndexesAdded()
    {
        return this.indexesAdded.get();
    }

    public int getIndexesRemoved()
    {
        return this.indexesRemoved.get();
    }

    public int getConstraintsAdded()
    {
        return this.constraintsAdded.get();
    }

    public int getConstraintsRemoved()
    {
        return this.constraintsRemoved.get();
    }

    public int getSystemUpdates()
    {
        return this.systemUpdates.get();
    }

    public boolean containsUpdates()
    {
        return this.containsUpdates;
    }

    public boolean containsSystemUpdates()
    {
        return this.containsSystemUpdates;
    }
}
