package com.neo4j.fabric.stream.summary;

import java.util.Collection;
import java.util.List;

import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryStatistics;

public class PartialSummary extends EmptySummary
{
    private final QueryStatistics queryStatistics;
    private final QueryExecutionType executionType;
    private final List<Notification> notifications;

    public PartialSummary( QueryStatistics queryStatistics, QueryExecutionType executionType, List<Notification> notifications )
    {
        this.queryStatistics = queryStatistics;
        this.executionType = executionType;
        this.notifications = notifications;
    }

    public QueryStatistics getQueryStatistics()
    {
        return this.queryStatistics;
    }

    public QueryExecutionType executionType()
    {
        return this.executionType;
    }

    public Collection<Notification> getNotifications()
    {
        return this.notifications;
    }
}
