package com.neo4j.fabric.stream.summary;

import java.util.Collection;
import java.util.Collections;

import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.graphdb.QueryExecutionType.QueryType;

public class EmptySummary implements Summary
{
    public QueryExecutionType executionType()
    {
        return QueryExecutionType.query( QueryType.READ_WRITE );
    }

    public ExecutionPlanDescription executionPlanDescription()
    {
        return new EmptyExecutionPlanDescription();
    }

    public Collection<Notification> getNotifications()
    {
        return Collections.emptyList();
    }

    public QueryStatistics getQueryStatistics()
    {
        return QueryStatistics.EMPTY;
    }
}
