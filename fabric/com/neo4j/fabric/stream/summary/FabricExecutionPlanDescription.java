package com.neo4j.fabric.stream.summary;

import com.neo4j.fabric.planning.FabricQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.ExecutionPlanDescription.ProfilerStatistics;
import scala.collection.JavaConverters;

public class FabricExecutionPlanDescription implements ExecutionPlanDescription
{
    private final FabricQuery query;

    public FabricExecutionPlanDescription( FabricQuery query )
    {
        this.query = query;
    }

    public String getName()
    {
        return this.query.getClass().getSimpleName();
    }

    public List<ExecutionPlanDescription> getChildren()
    {
        return (List) JavaConverters.seqAsJavaList( this.query.children() ).stream().map( FabricExecutionPlanDescription::new ).collect( Collectors.toList() );
    }

    public Map<String,Object> getArguments()
    {
        HashMap<String,Object> arguments = new HashMap();
        if ( this.query instanceof FabricQuery.RemoteQuery )
        {
            FabricQuery.RemoteQuery rq = (FabricQuery.RemoteQuery) this.query;
            arguments.put( "query", rq.queryString() );
        }

        return arguments;
    }

    public Set<String> getIdentifiers()
    {
        return JavaConverters.setAsJavaSet( this.query.columns().output().toSet() );
    }

    public boolean hasProfilerStatistics()
    {
        return false;
    }

    public ProfilerStatistics getProfilerStatistics()
    {
        return null;
    }
}
