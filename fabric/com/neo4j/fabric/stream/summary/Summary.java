package com.neo4j.fabric.stream.summary;

import java.util.Collection;

import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryStatistics;

public interface Summary
{
    QueryExecutionType executionType();

    ExecutionPlanDescription executionPlanDescription();

    Collection<Notification> getNotifications();

    QueryStatistics getQueryStatistics();
}
