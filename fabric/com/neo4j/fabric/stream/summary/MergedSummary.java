package com.neo4j.fabric.stream.summary;

import com.neo4j.fabric.executor.EffectiveQueryType;
import com.neo4j.fabric.planning.FabricPlan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.graphdb.QueryExecutionType.QueryType;

public class MergedSummary implements Summary
{
    private final MergedQueryStatistics statistics;
    private final List<Notification> notifications;
    private final QueryExecutionType executionType;
    private final FabricExecutionPlanDescription executionPlanDescription;

    public MergedSummary( FabricPlan plan, AccessMode accessMode )
    {
        this.executionType = this.queryExecutionType( plan, accessMode );
        this.statistics = new MergedQueryStatistics();
        this.notifications = new ArrayList();
        if ( plan.executionType() == FabricPlan.EXPLAIN() )
        {
            this.executionPlanDescription = new FabricExecutionPlanDescription( plan.query() );
        }
        else
        {
            this.executionPlanDescription = null;
        }
    }

    public void add( QueryStatistics delta )
    {
        this.statistics.add( delta );
    }

    public void add( Collection<Notification> delta )
    {
        this.notifications.addAll( delta );
    }

    public QueryExecutionType executionType()
    {
        return this.executionType;
    }

    public ExecutionPlanDescription executionPlanDescription()
    {
        return this.executionPlanDescription;
    }

    public Collection<Notification> getNotifications()
    {
        return this.notifications;
    }

    public QueryStatistics getQueryStatistics()
    {
        return this.statistics;
    }

    private QueryExecutionType queryExecutionType( FabricPlan plan, AccessMode accessMode )
    {
        if ( plan.executionType() == FabricPlan.EXECUTE() )
        {
            return QueryExecutionType.query( this.queryType( plan, accessMode ) );
        }
        else if ( plan.executionType() == FabricPlan.EXPLAIN() )
        {
            return QueryExecutionType.explained( this.queryType( plan, accessMode ) );
        }
        else if ( plan.executionType() == FabricPlan.PROFILE() )
        {
            return QueryExecutionType.profiled( this.queryType( plan, accessMode ) );
        }
        else
        {
            throw this.unexpected( "execution type", plan.executionType().toString() );
        }
    }

    private QueryType queryType( FabricPlan plan, AccessMode accessMode )
    {
        return EffectiveQueryType.effectiveQueryType( accessMode, plan.queryType() );
    }

    private IllegalArgumentException unexpected( String type, String got )
    {
        return new IllegalArgumentException( "Unexpected " + type + ": " + got );
    }
}
