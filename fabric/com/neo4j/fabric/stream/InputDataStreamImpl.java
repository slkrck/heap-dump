package com.neo4j.fabric.stream;

import org.neo4j.cypher.internal.runtime.InputCursor;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.values.AnyValue;

public class InputDataStreamImpl implements InputDataStream
{
    private final Rx2SyncStream wrappedStream;
    private InputCursor inputCursor;

    public InputDataStreamImpl( Rx2SyncStream wrappedStream )
    {
        this.wrappedStream = wrappedStream;
        this.inputCursor = new InputDataStreamImpl.Cursor();
    }

    public InputCursor nextInputBatch()
    {
        return this.inputCursor;
    }

    private class Cursor implements InputCursor
    {
        private Record currentRecord;

        public boolean next()
        {
            this.currentRecord = InputDataStreamImpl.this.wrappedStream.readRecord();
            if ( this.currentRecord != null )
            {
                return true;
            }
            else
            {
                InputDataStreamImpl.this.inputCursor = null;
                return false;
            }
        }

        public AnyValue value( int offset )
        {
            return this.currentRecord.getValue( offset );
        }

        public void close() throws Exception
        {
        }
    }
}
