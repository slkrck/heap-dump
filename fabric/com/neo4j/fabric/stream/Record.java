package com.neo4j.fabric.stream;

import org.neo4j.values.AnyValue;

public abstract class Record
{
    public abstract AnyValue getValue( int var1 );

    public abstract int size();

    public final int hashCode()
    {
        int hashCode = 1;

        for ( int i = 0; i < this.size(); ++i )
        {
            hashCode = 31 * hashCode + this.getValue( i ).hashCode();
        }

        return hashCode;
    }

    public final boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( !(o instanceof Record) )
        {
            return false;
        }
        else
        {
            Record that = (Record) o;
            if ( this.size() != that.size() )
            {
                return false;
            }
            else
            {
                for ( int i = 0; i < this.size(); ++i )
                {
                    if ( !this.getValue( i ).equals( that.getValue( i ) ) )
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
