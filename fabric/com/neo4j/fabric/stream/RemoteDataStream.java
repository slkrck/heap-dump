package com.neo4j.fabric.stream;

import java.util.List;

import org.neo4j.cypher.internal.runtime.InputDataStream;

public class RemoteDataStream
{
    private final List<String> columns;
    private final InputDataStream inputDataStream;

    public RemoteDataStream( List<String> columns, InputDataStream inputDataStream )
    {
        this.columns = columns;
        this.inputDataStream = inputDataStream;
    }

    public List<String> getColumns()
    {
        return this.columns;
    }

    public InputDataStream getInputDataStream()
    {
        return this.inputDataStream;
    }
}
