package com.neo4j.fabric.transaction;

import com.neo4j.fabric.bolt.FabricBookmark;
import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.driver.RemoteBookmark;
import com.neo4j.fabric.executor.FabricException;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.neo4j.bolt.runtime.Bookmark;
import org.neo4j.bolt.txtracking.TransactionIdTracker;
import org.neo4j.kernel.api.exceptions.Status.Transaction;
import org.neo4j.kernel.database.DatabaseIdRepository;

public class TransactionBookmarkManager
{
    private final FabricConfig fabricConfig;
    private final TransactionIdTracker transactionIdTracker;
    private final Duration bookmarkTimeout;
    private final Map<FabricConfig.Graph,TransactionBookmarkManager.GraphBookmarkData> graphBookmarkData = new ConcurrentHashMap();

    public TransactionBookmarkManager( FabricConfig fabricConfig, TransactionIdTracker transactionIdTracker, Duration bookmarkTimeout )
    {
        this.fabricConfig = fabricConfig;
        this.transactionIdTracker = transactionIdTracker;
        this.bookmarkTimeout = bookmarkTimeout;
    }

    public void processSubmittedByClient( List<Bookmark> bookmarks )
    {
        if ( !bookmarks.isEmpty() )
        {
            TransactionBookmarkManager.BookmarksByType bookmarksByType = this.sortOutByType( bookmarks );
            this.processSystemDatabase( bookmarksByType );
            this.processFabricBookmarks( bookmarksByType );
        }
    }

    public List<RemoteBookmark> getBookmarksForGraph( FabricConfig.Graph graph )
    {
        TransactionBookmarkManager.GraphBookmarkData bookmarkData = (TransactionBookmarkManager.GraphBookmarkData) this.graphBookmarkData.get( graph );
        return bookmarkData != null ? bookmarkData.bookmarksSubmittedByClient : List.of();
    }

    public void recordBookmarkReceivedFromGraph( FabricConfig.Graph graph, RemoteBookmark bookmark )
    {
        TransactionBookmarkManager.GraphBookmarkData bookmarkData =
                (TransactionBookmarkManager.GraphBookmarkData) this.graphBookmarkData.computeIfAbsent( graph, ( g ) -> {
                    return new TransactionBookmarkManager.GraphBookmarkData();
                } );
        bookmarkData.bookmarkReceivedFromGraph = bookmark;
    }

    public FabricBookmark constructFinalBookmark()
    {
        List<FabricBookmark.GraphState> remoteStates = (List) this.graphBookmarkData.entrySet().stream().map( ( entry ) -> {
            TransactionBookmarkManager.GraphBookmarkData bookmarkData = (TransactionBookmarkManager.GraphBookmarkData) entry.getValue();
            List graphBookmarks;
            if ( bookmarkData.bookmarkReceivedFromGraph != null )
            {
                graphBookmarks = List.of( bookmarkData.bookmarkReceivedFromGraph );
            }
            else
            {
                graphBookmarks = bookmarkData.bookmarksSubmittedByClient;
            }

            return new FabricBookmark.GraphState( ((FabricConfig.Graph) entry.getKey()).getId(), graphBookmarks );
        } ).collect( Collectors.toList() );
        return new FabricBookmark( remoteStates );
    }

    private TransactionBookmarkManager.BookmarksByType sortOutByType( List<Bookmark> bookmarks )
    {
        long systemDbTxId = -1L;
        List<FabricBookmark> fabricBookmarks = new ArrayList( bookmarks.size() );
        Iterator var5 = bookmarks.iterator();

        while ( var5.hasNext() )
        {
            Bookmark bookmark = (Bookmark) var5.next();
            if ( bookmark instanceof FabricBookmark )
            {
                fabricBookmarks.add( (FabricBookmark) bookmark );
            }
            else
            {
                if ( !bookmark.databaseId().equals( DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID ) )
                {
                    throw new FabricException( Transaction.InvalidBookmarkMixture, "Bookmark for unexpected database encountered: " + bookmark, new Object[0] );
                }

                systemDbTxId = Math.max( systemDbTxId, bookmark.txId() );
            }
        }

        return new TransactionBookmarkManager.BookmarksByType( systemDbTxId, fabricBookmarks );
    }

    private void processSystemDatabase( TransactionBookmarkManager.BookmarksByType bookmarksByType )
    {
        if ( bookmarksByType.systemDbTxId != -1L )
        {
            this.transactionIdTracker.awaitUpToDate( DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID, bookmarksByType.systemDbTxId, this.bookmarkTimeout );
        }
    }

    private void processFabricBookmarks( TransactionBookmarkManager.BookmarksByType bookmarksByType )
    {
        Map<Long,FabricConfig.Graph> graphsById =
                (Map) this.fabricConfig.getDatabase().getGraphs().stream().collect( Collectors.toMap( FabricConfig.Graph::getId, Function.identity() ) );
        Iterator var3 = bookmarksByType.fabricBookmarks.iterator();

        while ( var3.hasNext() )
        {
            FabricBookmark bookmark = (FabricBookmark) var3.next();
            bookmark.getGraphStates().forEach( ( graphState ) -> {
                FabricConfig.Graph graph = (FabricConfig.Graph) graphsById.get( graphState.getRemoteGraphId() );
                if ( graph == null )
                {
                    throw new FabricException( Transaction.InvalidBookmark, "Bookmark with non-existent remote graph ID database encountered: " + bookmark,
                            new Object[0] );
                }
                else
                {
                    TransactionBookmarkManager.GraphBookmarkData bookmarkData =
                            (TransactionBookmarkManager.GraphBookmarkData) this.graphBookmarkData.computeIfAbsent( graph, ( g ) -> {
                                return new TransactionBookmarkManager.GraphBookmarkData();
                            } );
                    bookmarkData.bookmarksSubmittedByClient.addAll( graphState.getBookmarks() );
                }
            } );
        }
    }

    private static class GraphBookmarkData
    {
        private final List<RemoteBookmark> bookmarksSubmittedByClient = new ArrayList();
        private RemoteBookmark bookmarkReceivedFromGraph;
    }

    private static class BookmarksByType
    {
        private final long systemDbTxId;
        private final List<FabricBookmark> fabricBookmarks;

        BookmarksByType( Long systemDbTxId, List<FabricBookmark> fabricBookmarks )
        {
            this.systemDbTxId = systemDbTxId;
            this.fabricBookmarks = fabricBookmarks;
        }
    }
}
