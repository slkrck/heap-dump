package com.neo4j.fabric.transaction;

import java.time.Duration;
import java.util.Map;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.internal.kernel.api.connectioninfo.ClientConnectionInfo;
import org.neo4j.internal.kernel.api.security.LoginContext;

public class FabricTransactionInfo
{
    private final AccessMode accessMode;
    private final LoginContext loginContext;
    private final ClientConnectionInfo clientConnectionInfo;
    private final String databaseName;
    private final boolean implicitTransaction;
    private final Duration txTimeout;
    private final Map<String,Object> txMetadata;

    public FabricTransactionInfo( AccessMode accessMode, LoginContext loginContext, ClientConnectionInfo clientConnectionInfo, String databaseName,
            boolean implicitTransaction, Duration txTimeout, Map<String,Object> txMetadata )
    {
        this.accessMode = accessMode;
        this.loginContext = loginContext;
        this.clientConnectionInfo = clientConnectionInfo;
        this.databaseName = databaseName;
        this.implicitTransaction = implicitTransaction;
        this.txTimeout = txTimeout;
        this.txMetadata = txMetadata;
    }

    public AccessMode getAccessMode()
    {
        return this.accessMode;
    }

    public LoginContext getLoginContext()
    {
        return this.loginContext;
    }

    public ClientConnectionInfo getClientConnectionInfo()
    {
        return this.clientConnectionInfo;
    }

    public String getDatabaseName()
    {
        return this.databaseName;
    }

    public boolean isImplicitTransaction()
    {
        return this.implicitTransaction;
    }

    public Duration getTxTimeout()
    {
        return this.txTimeout;
    }

    public Map<String,Object> getTxMetadata()
    {
        return this.txMetadata;
    }
}
