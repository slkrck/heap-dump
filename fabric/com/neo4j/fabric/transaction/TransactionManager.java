package com.neo4j.fabric.transaction;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.executor.FabricLocalExecutor;
import com.neo4j.fabric.executor.FabricRemoteExecutor;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.common.DependencyResolver;
import org.neo4j.internal.kernel.api.security.LoginContext.IdLookup;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.internal.LogService;
import org.neo4j.scheduler.JobScheduler;

public class TransactionManager extends LifecycleAdapter
{
    private final FabricRemoteExecutor remoteExecutor;
    private final FabricLocalExecutor localExecutor;
    private final LogService logService;
    private final JobScheduler jobScheduler;
    private final FabricConfig fabricConfig;
    private final Set<FabricTransactionImpl> openTransactions = ConcurrentHashMap.newKeySet();

    public TransactionManager( DependencyResolver dependencyResolver )
    {
        this.remoteExecutor = (FabricRemoteExecutor) dependencyResolver.resolveDependency( FabricRemoteExecutor.class );
        this.localExecutor = (FabricLocalExecutor) dependencyResolver.resolveDependency( FabricLocalExecutor.class );
        this.logService = (LogService) dependencyResolver.resolveDependency( LogService.class );
        this.jobScheduler = (JobScheduler) dependencyResolver.resolveDependency( JobScheduler.class );
        this.fabricConfig = (FabricConfig) dependencyResolver.resolveDependency( FabricConfig.class );
    }

    public FabricTransaction begin( FabricTransactionInfo transactionInfo, TransactionBookmarkManager transactionBookmarkManager )
    {
        transactionInfo.getLoginContext().authorize( IdLookup.EMPTY, transactionInfo.getDatabaseName() );
        FabricTransactionImpl fabricTransaction =
                new FabricTransactionImpl( transactionInfo, transactionBookmarkManager, this.remoteExecutor, this.localExecutor, this.logService, this,
                        this.jobScheduler, this.fabricConfig );
        fabricTransaction.begin();
        this.openTransactions.add( fabricTransaction );
        return fabricTransaction;
    }

    public void stop()
    {
        this.openTransactions.forEach( FabricTransactionImpl::doRollback );
    }

    void removeTransaction( FabricTransactionImpl transaction )
    {
        this.openTransactions.remove( transaction );
    }

    public Set<FabricTransactionImpl> getOpenTransactions()
    {
        return this.openTransactions;
    }
}
