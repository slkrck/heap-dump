package com.neo4j.fabric.transaction;

import com.neo4j.fabric.executor.FabricLocalExecutor;
import com.neo4j.fabric.executor.FabricRemoteExecutor;
import com.neo4j.fabric.stream.StatementResult;

import java.util.Optional;
import java.util.function.Function;

import org.neo4j.kernel.api.exceptions.Status;

public interface FabricTransaction
{
    void begin();

    void commit();

    void rollback();

    StatementResult execute( Function<FabricTransaction.FabricExecutionContext,StatementResult> var1 );

    void markForTermination( Status var1 );

    Optional<Status> getReasonIfTerminated();

    FabricTransactionInfo getTransactionInfo();

    TransactionBookmarkManager getBookmarkManager();

    public interface FabricExecutionContext
    {
        FabricRemoteExecutor.FabricRemoteTransaction getRemote();

        FabricLocalExecutor.FabricLocalTransaction getLocal();
    }
}
