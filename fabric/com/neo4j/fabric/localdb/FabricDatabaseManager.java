package com.neo4j.fabric.localdb;

import com.neo4j.fabric.config.FabricConfig;

import java.util.UUID;
import java.util.function.Function;

import org.neo4j.common.DependencyResolver;
import org.neo4j.configuration.helpers.NormalizedDatabaseName;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.dbms.api.DatabaseNotFoundException;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.SystemGraphDbmsModel;
import org.neo4j.graphdb.ConstraintViolationException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.availability.UnavailableException;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class FabricDatabaseManager extends LifecycleAdapter
{
    private final FabricConfig fabricConfig;
    private final DependencyResolver dependencyResolver;
    private final Log log;
    private DatabaseManager<DatabaseContext> databaseManager;
    private DatabaseIdRepository databaseIdRepository;
    private DatabaseManagementService managementService;

    public FabricDatabaseManager( FabricConfig fabricConfig, DependencyResolver dependencyResolver, LogProvider logProvider )
    {
        this.dependencyResolver = dependencyResolver;
        this.fabricConfig = fabricConfig;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void start()
    {
        this.databaseManager = (DatabaseManager) this.dependencyResolver.resolveDependency( DatabaseManager.class );
        this.databaseIdRepository = this.databaseManager.databaseIdRepository();
    }

    public DatabaseIdRepository databaseIdRepository()
    {
        return this.databaseIdRepository;
    }

    public void manageFabricDatabases( GraphDatabaseService system, boolean update )
    {
        Transaction tx = system.beginTx();

        try
        {
            boolean exists = false;
            if ( update )
            {
                exists = this.checkExisting( tx );
            }

            if ( this.fabricConfig.isEnabled() )
            {
                NormalizedDatabaseName dbName = this.fabricConfig.getDatabase().getName();
                if ( exists )
                {
                    this.log.info( "Using existing Fabric virtual database '%s'", new Object[]{dbName.name()} );
                }
                else
                {
                    this.log.info( "Creating Fabric virtual database '%s'", new Object[]{dbName.name()} );
                    this.newFabricDb( tx, dbName );
                }
            }

            tx.commit();
        }
        catch ( Throwable var7 )
        {
            if ( tx != null )
            {
                try
                {
                    tx.close();
                }
                catch ( Throwable var6 )
                {
                    var7.addSuppressed( var6 );
                }
            }

            throw var7;
        }

        if ( tx != null )
        {
            tx.close();
        }
    }

    public boolean isFabricDatabase( String databaseNameRaw )
    {
        NormalizedDatabaseName databaseName = new NormalizedDatabaseName( databaseNameRaw );
        return this.fabricConfig.isEnabled() && this.fabricConfig.getDatabase().getName().equals( databaseName );
    }

    public GraphDatabaseFacade getDatabase( String databaseNameRaw ) throws UnavailableException
    {
        GraphDatabaseFacade graphDatabaseFacade = ((DatabaseContext) this.databaseIdRepository.getByName( databaseNameRaw ).flatMap( ( databaseId ) -> {
            return this.databaseManager.getDatabaseContext( databaseId );
        } ).orElseThrow( () -> {
            return new DatabaseNotFoundException( "Database " + databaseNameRaw + " not found" );
        } )).databaseFacade();
        if ( !graphDatabaseFacade.isAvailable( 0L ) )
        {
            throw new UnavailableException( "Database %s not available " + databaseNameRaw );
        }
        else
        {
            return graphDatabaseFacade;
        }
    }

    private boolean checkExisting( Transaction tx )
    {
        Function<ResourceIterator<Node>,Boolean> iterator = ( nodes ) -> {
            boolean found = false;

            while ( true )
            {
                while ( nodes.hasNext() )
                {
                    Node fabricDb = (Node) nodes.next();
                    Object dbName = fabricDb.getProperty( "name" );
                    if ( this.fabricConfig.isEnabled() && this.fabricConfig.getDatabase().getName().name().equals( dbName ) )
                    {
                        this.log.info( "Setting Fabric virtual database '%s' status to online", new Object[]{dbName} );
                        fabricDb.setProperty( "status", "online" );
                        found = true;
                    }
                    else
                    {
                        this.log.info( "Setting Fabric virtual database '%s' status to offline", new Object[]{dbName} );
                        fabricDb.setProperty( "status", "offline" );
                    }
                }

                nodes.close();
                return found;
            }
        };
        return (Boolean) iterator.apply( tx.findNodes( SystemGraphDbmsModel.DATABASE_LABEL, "fabric", true ) );
    }

    private void newFabricDb( Transaction tx, NormalizedDatabaseName dbName )
    {
        try
        {
            Node node = tx.createNode( new Label[]{SystemGraphDbmsModel.DATABASE_LABEL} );
            node.setProperty( "name", dbName.name() );
            node.setProperty( "uuid", UUID.randomUUID().toString() );
            node.setProperty( "status", "online" );
            node.setProperty( "default", false );
            node.setProperty( "fabric", true );
        }
        catch ( ConstraintViolationException var4 )
        {
            throw new IllegalStateException( "The specified database '" + dbName.name() + "' already exists." );
        }
    }
}
