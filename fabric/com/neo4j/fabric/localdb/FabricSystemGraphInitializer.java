package com.neo4j.fabric.localdb;

import com.neo4j.dbms.EnterpriseSystemGraphInitializer;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.graphdb.GraphDatabaseService;

public class FabricSystemGraphInitializer extends EnterpriseSystemGraphInitializer
{
    private final FabricDatabaseManager fabricDatabaseManager;

    public FabricSystemGraphInitializer( DatabaseManager<?> databaseManager, Config config, FabricDatabaseManager fabricDatabaseManager )
    {
        super( databaseManager, config );
        this.fabricDatabaseManager = fabricDatabaseManager;
    }

    protected void manageDatabases( GraphDatabaseService system, boolean update )
    {
        this.fabricDatabaseManager.manageFabricDatabases( system, update );
    }
}
