package com.neo4j.fabric.auth;

import com.neo4j.kernel.enterprise.api.security.EnterpriseLoginContext;
import com.neo4j.kernel.enterprise.api.security.EnterpriseSecurityContext;

import java.util.Set;

import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.internal.kernel.api.security.LoginContext.IdLookup;

public class FabricLoginContext implements EnterpriseLoginContext
{
    private final EnterpriseLoginContext wrappedLoginContext;
    private final FabricAuthSubject authSubject;

    public FabricLoginContext( EnterpriseLoginContext wrappedLoginContext, FabricAuthSubject authSubject )
    {
        this.wrappedLoginContext = wrappedLoginContext;
        this.authSubject = authSubject;
    }

    public Set<String> roles()
    {
        return this.wrappedLoginContext.roles();
    }

    public AuthSubject subject()
    {
        return this.authSubject;
    }

    public EnterpriseSecurityContext authorize( IdLookup idLookup, String dbName )
    {
        return this.wrappedLoginContext.authorize( idLookup, dbName );
    }
}
