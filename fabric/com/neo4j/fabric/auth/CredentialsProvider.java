package com.neo4j.fabric.auth;

import com.neo4j.fabric.shaded.driver.AuthToken;
import com.neo4j.fabric.shaded.driver.AuthTokens;
import org.neo4j.internal.kernel.api.security.AuthSubject;

public class CredentialsProvider
{
    public AuthToken credentialsFor( AuthSubject subject )
    {
        Credentials credentials = FabricAuthManagerWrapper.getCredentials( subject );
        return credentials.getProvided() ? AuthTokens.basic( credentials.getUsername(), new String( credentials.getPassword() ) ) : AuthTokens.none();
    }
}
