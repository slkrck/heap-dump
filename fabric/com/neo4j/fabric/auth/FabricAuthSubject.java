package com.neo4j.fabric.auth;

import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.internal.kernel.api.security.AuthenticationResult;

public class FabricAuthSubject implements AuthSubject
{
    private final AuthSubject wrappedAuthSubject;
    private final Credentials credentials;

    public FabricAuthSubject( AuthSubject wrappedAuthSubject, Credentials credentials )
    {
        this.wrappedAuthSubject = wrappedAuthSubject;
        this.credentials = credentials;
    }

    public void logout()
    {
        this.wrappedAuthSubject.logout();
    }

    public AuthenticationResult getAuthenticationResult()
    {
        return this.wrappedAuthSubject.getAuthenticationResult();
    }

    public void setPasswordChangeNoLongerRequired()
    {
        this.wrappedAuthSubject.setPasswordChangeNoLongerRequired();
    }

    public boolean hasUsername( String username )
    {
        return this.wrappedAuthSubject.hasUsername( username );
    }

    public String username()
    {
        return this.wrappedAuthSubject.username();
    }

    public Credentials getCredentials()
    {
        return this.credentials;
    }
}
