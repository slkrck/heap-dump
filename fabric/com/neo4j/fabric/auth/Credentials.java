package com.neo4j.fabric.auth;

public class Credentials
{
    private final String username;
    private final byte[] password;
    private final boolean provided;

    public Credentials( String username, byte[] password, boolean provided )
    {
        this.username = username;
        this.password = password;
        this.provided = provided;
    }

    public String getUsername()
    {
        return this.username;
    }

    public byte[] getPassword()
    {
        return this.password;
    }

    public boolean getProvided()
    {
        return this.provided;
    }
}
