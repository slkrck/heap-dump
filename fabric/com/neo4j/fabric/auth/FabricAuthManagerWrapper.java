package com.neo4j.fabric.auth;

import com.neo4j.kernel.enterprise.api.security.EnterpriseAuthManager;
import com.neo4j.kernel.enterprise.api.security.EnterpriseLoginContext;

import java.util.Arrays;
import java.util.Map;

import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.api.security.AuthToken;
import org.neo4j.kernel.api.security.exception.InvalidAuthTokenException;

public class FabricAuthManagerWrapper implements EnterpriseAuthManager
{
    private final EnterpriseAuthManager wrappedAuthManager;

    public FabricAuthManagerWrapper( EnterpriseAuthManager wrappedAuthManager )
    {
        this.wrappedAuthManager = wrappedAuthManager;
    }

    public static Credentials getCredentials( AuthSubject authSubject )
    {
        if ( !(authSubject instanceof FabricAuthSubject) )
        {
            throw new IllegalArgumentException( "The submitted subject was not created by Fabric Authentication manager: " + authSubject );
        }
        else
        {
            return ((FabricAuthSubject) authSubject).getCredentials();
        }
    }

    public EnterpriseLoginContext login( Map<String,Object> authToken ) throws InvalidAuthTokenException
    {
        boolean authProvided = !authToken.get( "scheme" ).equals( "none" );
        String username = null;
        byte[] password = null;
        if ( authToken.containsKey( "principal" ) && authToken.containsKey( "credentials" ) )
        {
            username = AuthToken.safeCast( "principal", authToken );
            byte[] originalPassword = AuthToken.safeCastCredentials( "credentials", authToken );
            password = Arrays.copyOf( originalPassword, originalPassword.length );
        }

        EnterpriseLoginContext wrappedLoginContext = this.wrappedAuthManager.login( authToken );
        Credentials credentials = new Credentials( username, password, authProvided );
        FabricAuthSubject fabricAuthSubject = new FabricAuthSubject( wrappedLoginContext.subject(), credentials );
        return new FabricLoginContext( wrappedLoginContext, fabricAuthSubject );
    }

    public void clearAuthCache()
    {
        this.wrappedAuthManager.clearAuthCache();
    }

    public void log( String message, SecurityContext securityContext )
    {
        this.wrappedAuthManager.log( message, securityContext );
    }

    public void init()
    {
    }

    public void start()
    {
    }

    public void stop()
    {
    }

    public void shutdown()
    {
    }
}
