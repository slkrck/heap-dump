package com.neo4j.fabric.executor;

import com.neo4j.fabric.localdb.FabricDatabaseManager;
import com.neo4j.fabric.transaction.FabricTransactionInfo;

import java.util.Map;
import java.util.function.Supplier;

import org.neo4j.common.DependencyResolver;
import org.neo4j.graphdb.QueryExecutionType.QueryType;
import org.neo4j.kernel.api.query.CompilerInfo;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.query.QueryExecutionMonitor;
import org.neo4j.kernel.impl.util.MonotonicCounter;
import org.neo4j.memory.OptionalMemoryTracker;
import org.neo4j.monitoring.Monitors;
import org.neo4j.resources.CpuClock;
import org.neo4j.time.Clocks;
import org.neo4j.values.virtual.MapValue;

public class FabricQueryMonitoring
{
    private final DependencyResolver dependencyResolver;
    private final Monitors monitors;
    private DatabaseIdRepository databaseIdRepository;

    public FabricQueryMonitoring( DependencyResolver dependencyResolver, Monitors monitors )
    {
        this.dependencyResolver = dependencyResolver;
        this.monitors = monitors;
    }

    FabricQueryMonitoring.QueryMonitor queryMonitor( FabricTransactionInfo transactionInfo, String statement, MapValue params, Thread thread )
    {
        return new FabricQueryMonitoring.QueryMonitor( this.executingQuery( transactionInfo, statement, params, thread ),
                (QueryExecutionMonitor) this.monitors.newMonitor( QueryExecutionMonitor.class, new String[0] ) );
    }

    private ExecutingQuery executingQuery( FabricTransactionInfo transactionInfo, String statement, MapValue params, Thread thread )
    {
        NamedDatabaseId namedDatabaseId = (NamedDatabaseId) this.getDatabaseIdRepository().getByName( transactionInfo.getDatabaseName() ).get();
        return new FabricQueryMonitoring.FabricExecutingQuery( transactionInfo, statement, params, thread, namedDatabaseId );
    }

    private DatabaseIdRepository getDatabaseIdRepository()
    {
        if ( this.databaseIdRepository == null )
        {
            this.databaseIdRepository =
                    ((FabricDatabaseManager) this.dependencyResolver.resolveDependency( FabricDatabaseManager.class )).databaseIdRepository();
        }

        return this.databaseIdRepository;
    }

    private static class FabricExecutingQuery extends ExecutingQuery
    {
        private static final MonotonicCounter internalFabricQueryIdGenerator = MonotonicCounter.newAtomicMonotonicCounter();
        private String internalFabricId;

        private FabricExecutingQuery( FabricTransactionInfo transactionInfo, String statement, MapValue params, Thread thread, NamedDatabaseId namedDatabaseId )
        {
            super( -1L, transactionInfo.getClientConnectionInfo(), namedDatabaseId, transactionInfo.getLoginContext().subject().username(), statement, params,
                    Map.of(), () -> {
                        return 0L;
                    }, () -> {
                        return 0L;
                    }, () -> {
                        return 0L;
                    }, thread.getId(), thread.getName(), Clocks.nanoClock(), CpuClock.NOT_AVAILABLE );
            this.internalFabricId = "Fabric-" + internalFabricQueryIdGenerator.incrementAndGet();
        }

        public String id()
        {
            return this.internalFabricId;
        }
    }

    static class QueryMonitor
    {
        private final ExecutingQuery executingQuery;
        private final QueryExecutionMonitor monitor;

        private QueryMonitor( ExecutingQuery executingQuery, QueryExecutionMonitor monitor )
        {
            this.executingQuery = executingQuery;
            this.monitor = monitor;
        }

        void start()
        {
            this.monitor.start( this.executingQuery );
        }

        void startExecution()
        {
            this.executingQuery.onCompilationCompleted( (CompilerInfo) null, (QueryType) null, (Supplier) null );
            this.executingQuery.onExecutionStarted( OptionalMemoryTracker.NONE );
        }

        void endSuccess()
        {
            this.monitor.endSuccess( this.executingQuery );
        }

        void endFailure( Throwable failure )
        {
            this.monitor.endFailure( this.executingQuery, failure );
        }

        ExecutingQuery getMonitoredQuery()
        {
            return this.executingQuery;
        }
    }
}
