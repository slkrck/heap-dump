package com.neo4j.fabric.executor;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.stream.InputDataStreamImpl;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.stream.Rx2SyncStream;
import com.neo4j.fabric.stream.StatementResult;
import com.neo4j.fabric.stream.StatementResults;
import org.neo4j.cypher.internal.FullyParsedQuery;
import org.neo4j.cypher.internal.javacompat.ExecutionEngine;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.api.KernelTransaction;
import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.impl.coreapi.InternalTransaction;
import org.neo4j.kernel.impl.query.QueryExecution;
import org.neo4j.kernel.impl.query.QueryExecutionKernelException;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.kernel.impl.query.TransactionalContext;
import org.neo4j.kernel.impl.query.TransactionalContextFactory;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;

public class SingleStatementKernelTransaction
{
    private final ExecutingQuery parentQuery;
    private final ExecutionEngine queryExecutionEngine;
    private final TransactionalContextFactory transactionalContextFactory;
    private final KernelTransaction kernelTransaction;
    private final InternalTransaction internalTransaction;
    private final FabricConfig config;
    private TransactionalContext executionContext;

    SingleStatementKernelTransaction( ExecutingQuery parentQuery, ExecutionEngine queryExecutionEngine, TransactionalContextFactory transactionalContextFactory,
            KernelTransaction kernelTransaction, InternalTransaction internalTransaction, FabricConfig config )
    {
        this.parentQuery = parentQuery;
        this.queryExecutionEngine = queryExecutionEngine;
        this.transactionalContextFactory = transactionalContextFactory;
        this.kernelTransaction = kernelTransaction;
        this.internalTransaction = internalTransaction;
        this.config = config;
    }

    public StatementResult run( FullyParsedQuery query, MapValue params, Flux<Record> input )
    {
        if ( this.executionContext != null )
        {
            throw new IllegalStateException( "This transaction can be used to execute only one statement" );
        }
        else
        {
            return StatementResults.create( ( subscriber ) -> {
                return this.execute( query, params, this.convert( input ), subscriber );
            } );
        }
    }

    private QueryExecution execute( FullyParsedQuery query, MapValue params, InputDataStream input, QuerySubscriber subscriber )
    {
        try
        {
            String queryText = "Internal query for Fabric query id:" + this.parentQuery.id();
            this.executionContext = this.transactionalContextFactory.newContext( this.internalTransaction, queryText, params );
            return this.queryExecutionEngine.executeQuery( query, params, this.executionContext, true, input, subscriber );
        }
        catch ( QueryExecutionKernelException var6 )
        {
            if ( var6.getCause() == null )
            {
                throw Exceptions.transform( Statement.ExecutionFailed, var6 );
            }
            else
            {
                throw Exceptions.transform( Statement.ExecutionFailed, var6.getCause() );
            }
        }
    }

    private InputDataStream convert( Flux<Record> input )
    {
        return new InputDataStreamImpl( new Rx2SyncStream( input, this.config.getDataStream().getBatchSize() ) );
    }

    public void commit()
    {
        synchronized ( this.kernelTransaction )
        {
            if ( this.kernelTransaction.isOpen() )
            {
                try
                {
                    this.closeContext();
                    this.kernelTransaction.commit();
                }
                catch ( TransactionFailureException var4 )
                {
                    throw new FabricException( var4.status(), var4 );
                }
            }
        }
    }

    public void rollback()
    {
        synchronized ( this.kernelTransaction )
        {
            if ( this.kernelTransaction.isOpen() )
            {
                try
                {
                    this.closeContext();
                    this.kernelTransaction.rollback();
                }
                catch ( TransactionFailureException var4 )
                {
                    throw new FabricException( var4.status(), var4 );
                }
            }
        }
    }

    private void closeContext()
    {
        if ( this.executionContext != null )
        {
            this.executionContext.close();
        }
    }

    public void markForTermination( Status reason )
    {
        this.kernelTransaction.markForTermination( reason );
    }
}
