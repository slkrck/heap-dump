package com.neo4j.fabric.executor;

import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.HasStatus;

public class FabricException extends RuntimeException implements HasStatus
{
    private final Status statusCode;

    public FabricException( Status statusCode, Throwable cause )
    {
        super( cause );
        this.statusCode = statusCode;
    }

    public FabricException( Status statusCode, String message, Object... parameters )
    {
        super( String.format( message, parameters ) );
        this.statusCode = statusCode;
    }

    public FabricException( Status statusCode, String message, Throwable cause )
    {
        super( message, cause );
        this.statusCode = statusCode;
    }

    public Status status()
    {
        return this.statusCode;
    }
}
