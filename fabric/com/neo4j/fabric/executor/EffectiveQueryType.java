package com.neo4j.fabric.executor;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.graphdb.QueryExecutionType.QueryType;

public class EffectiveQueryType
{
    public static QueryType effectiveQueryType( AccessMode requested, com.neo4j.fabric.planning.QueryType queryType )
    {
        if ( queryType == com.neo4j.fabric.planning.QueryType.READ() )
        {
            return QueryType.READ_ONLY;
        }
        else if ( queryType == com.neo4j.fabric.planning.QueryType.READ_PLUS_UNRESOLVED() )
        {
            switch ( requested )
            {
            case READ:
                return QueryType.READ_ONLY;
            case WRITE:
                return QueryType.READ_WRITE;
            default:
                throw new IllegalArgumentException( "Unexpected access mode: " + requested );
            }
        }
        else if ( queryType == com.neo4j.fabric.planning.QueryType.WRITE() )
        {
            return QueryType.READ_WRITE;
        }
        else
        {
            throw new IllegalArgumentException( "Unexpected query type: " + queryType );
        }
    }

    public static AccessMode effectiveAccessMode( AccessMode requested, com.neo4j.fabric.planning.QueryType queryType )
    {
        if ( queryType == com.neo4j.fabric.planning.QueryType.READ() )
        {
            return AccessMode.READ;
        }
        else if ( queryType == com.neo4j.fabric.planning.QueryType.READ_PLUS_UNRESOLVED() )
        {
            return requested;
        }
        else if ( queryType == com.neo4j.fabric.planning.QueryType.WRITE() )
        {
            return AccessMode.WRITE;
        }
        else
        {
            throw new IllegalArgumentException( "Unexpected query type: " + queryType );
        }
    }
}
