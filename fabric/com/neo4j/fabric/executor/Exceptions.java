package com.neo4j.fabric.executor;

import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.HasStatus;

public class Exceptions
{
    public static RuntimeException transform( Status defaultStatus, Throwable t )
    {
        Throwable unwrapped = reactor.core.Exceptions.unwrap( t );
        if ( unwrapped instanceof HasStatus )
        {
            return (RuntimeException) (unwrapped instanceof RuntimeException ? (RuntimeException) unwrapped
                                                                             : new FabricException( ((HasStatus) unwrapped).status(), unwrapped ));
        }
        else
        {
            return new FabricException( defaultStatus, unwrapped );
        }
    }
}
