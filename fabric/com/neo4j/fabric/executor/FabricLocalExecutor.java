package com.neo4j.fabric.executor;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.localdb.FabricDatabaseManager;
import com.neo4j.fabric.stream.Record;
import com.neo4j.fabric.stream.StatementResult;
import com.neo4j.fabric.stream.summary.Summary;
import com.neo4j.fabric.transaction.FabricTransactionInfo;
import com.neo4j.kernel.enterprise.api.security.EnterpriseLoginContext;
import com.neo4j.kernel.enterprise.api.security.EnterpriseSecurityContext;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.neo4j.common.DependencyResolver;
import org.neo4j.cypher.internal.FullyParsedQuery;
import org.neo4j.cypher.internal.javacompat.ExecutionEngine;
import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.internal.kernel.api.security.AccessMode.Static;
import org.neo4j.internal.kernel.api.security.LoginContext.IdLookup;
import org.neo4j.kernel.GraphDatabaseQueryService;
import org.neo4j.kernel.api.KernelTransaction;
import org.neo4j.kernel.api.KernelTransaction.Type;
import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Database;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.availability.UnavailableException;
import org.neo4j.kernel.impl.api.security.RestrictedAccessMode;
import org.neo4j.kernel.impl.coreapi.InternalTransaction;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;
import org.neo4j.kernel.impl.query.Neo4jTransactionalContextFactory;
import org.neo4j.kernel.impl.query.TransactionalContextFactory;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FabricLocalExecutor
{
    private final FabricConfig config;
    private final FabricDatabaseManager dbms;

    public FabricLocalExecutor( FabricConfig config, FabricDatabaseManager dbms )
    {
        this.config = config;
        this.dbms = dbms;
    }

    public FabricLocalExecutor.FabricLocalTransaction begin( FabricTransactionInfo transactionInfo )
    {
        return new FabricLocalExecutor.FabricLocalTransaction( transactionInfo );
    }

    private Type getKernelTransactionType( FabricTransactionInfo fabricTransactionInfo )
    {
        return fabricTransactionInfo.isImplicitTransaction() ? Type.implicit : Type.explicit;
    }

    private static class ResultInterceptor implements StatementResult
    {
        private final StatementResult wrappedResult;
        private final Runnable commit;
        private final Runnable rollback;

        ResultInterceptor( StatementResult wrappedResult, Runnable commit, Runnable rollback )
        {
            this.wrappedResult = wrappedResult;
            this.commit = commit;
            this.rollback = rollback;
        }

        public Flux<String> columns()
        {
            return this.wrappedResult.columns().doOnError( ( error ) -> {
                this.rollback.run();
            } );
        }

        public Flux<Record> records()
        {
            return this.wrappedResult.records().doOnError( ( error ) -> {
                this.rollback.run();
            } ).doOnComplete( this.commit ).doOnCancel( this.rollback );
        }

        public Mono<Summary> summary()
        {
            return this.wrappedResult.summary();
        }
    }

    private static class FabricLocalLoginContext implements EnterpriseLoginContext
    {
        private final EnterpriseLoginContext inner;

        private FabricLocalLoginContext( EnterpriseLoginContext inner )
        {
            this.inner = inner;
        }

        public AuthSubject subject()
        {
            return this.inner.subject();
        }

        public Set<String> roles()
        {
            return this.inner.roles();
        }

        public EnterpriseSecurityContext authorize( IdLookup idLookup, String dbName )
        {
            EnterpriseSecurityContext originalSecurityContext = this.inner.authorize( idLookup, dbName );
            RestrictedAccessMode restrictedAccessMode = new RestrictedAccessMode( originalSecurityContext.mode(), Static.ACCESS );
            return new EnterpriseSecurityContext( this.inner.subject(), restrictedAccessMode, this.inner.roles(), ( action ) -> {
                return false;
            } );
        }
    }

    public class FabricLocalTransaction
    {
        private final FabricTransactionInfo transactionInfo;
        private final Set<SingleStatementKernelTransaction> kernelTransactions = Collections.newSetFromMap( new ConcurrentHashMap() );

        FabricLocalTransaction( FabricTransactionInfo transactionInfo )
        {
            this.transactionInfo = transactionInfo;
        }

        public StatementResult run( ExecutingQuery parentQuery, FullyParsedQuery query, MapValue params, Flux<Record> input )
        {
            SingleStatementKernelTransaction kernelTransaction = this.beginKernelTransaction( parentQuery );
            this.kernelTransactions.add( kernelTransaction );

            try
            {
                StatementResult result = kernelTransaction.run( query, params, input );
                return new FabricLocalExecutor.ResultInterceptor( result, () -> {
                    kernelTransaction.commit();
                    this.kernelTransactions.remove( kernelTransaction );
                }, () -> {
                    kernelTransaction.rollback();
                    this.kernelTransactions.remove( kernelTransaction );
                } );
            }
            catch ( RuntimeException var7 )
            {
                kernelTransaction.rollback();
                this.kernelTransactions.remove( kernelTransaction );
                throw var7;
            }
        }

        public void commit()
        {
            this.kernelTransactions.forEach( SingleStatementKernelTransaction::commit );
        }

        public void rollback()
        {
            this.kernelTransactions.forEach( SingleStatementKernelTransaction::rollback );
        }

        public void markForTermination( Status reason )
        {
            this.kernelTransactions.forEach( ( tx ) -> {
                tx.markForTermination( reason );
            } );
        }

        private SingleStatementKernelTransaction beginKernelTransaction( ExecutingQuery parentQuery )
        {
            String databaseName = this.transactionInfo.getDatabaseName();

            GraphDatabaseFacade databaseFacade;
            try
            {
                databaseFacade = FabricLocalExecutor.this.dbms.getDatabase( databaseName );
            }
            catch ( UnavailableException var10 )
            {
                throw new FabricException( Database.DatabaseUnavailable, var10 );
            }

            DependencyResolver dependencyResolver = databaseFacade.getDependencyResolver();
            ExecutionEngine executionEngine = (ExecutionEngine) dependencyResolver.resolveDependency( ExecutionEngine.class );
            InternalTransaction internalTransaction = this.beginInternalTransaction( databaseFacade, this.transactionInfo );
            KernelTransaction kernelTransaction = internalTransaction.kernelTransaction();
            GraphDatabaseQueryService queryService = (GraphDatabaseQueryService) dependencyResolver.resolveDependency( GraphDatabaseQueryService.class );
            TransactionalContextFactory transactionalContextFactory = Neo4jTransactionalContextFactory.create( queryService );
            return new SingleStatementKernelTransaction( parentQuery, executionEngine, transactionalContextFactory, kernelTransaction, internalTransaction,
                    FabricLocalExecutor.this.config );
        }

        private InternalTransaction beginInternalTransaction( GraphDatabaseFacade databaseFacade, FabricTransactionInfo transactionInfo )
        {
            Type kernelTransactionType = FabricLocalExecutor.this.getKernelTransactionType( transactionInfo );
            FabricLocalExecutor.FabricLocalLoginContext loginContext =
                    new FabricLocalExecutor.FabricLocalLoginContext( (EnterpriseLoginContext) transactionInfo.getLoginContext() );
            InternalTransaction internalTransaction;
            if ( transactionInfo.getTxTimeout() == null )
            {
                internalTransaction = databaseFacade.beginTransaction( kernelTransactionType, loginContext, transactionInfo.getClientConnectionInfo() );
            }
            else
            {
                internalTransaction = databaseFacade.beginTransaction( kernelTransactionType, loginContext, transactionInfo.getClientConnectionInfo(),
                        transactionInfo.getTxTimeout().toMillis(), TimeUnit.MILLISECONDS );
            }

            if ( transactionInfo.getTxMetadata() != null )
            {
                internalTransaction.setMetaData( transactionInfo.getTxMetadata() );
            }

            return internalTransaction;
        }
    }
}
