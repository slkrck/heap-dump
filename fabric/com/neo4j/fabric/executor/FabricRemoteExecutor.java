package com.neo4j.fabric.executor;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.driver.AutoCommitStatementResult;
import com.neo4j.fabric.driver.DriverPool;
import com.neo4j.fabric.driver.FabricDriverTransaction;
import com.neo4j.fabric.driver.PooledDriver;
import com.neo4j.fabric.driver.RemoteBookmark;
import com.neo4j.fabric.planning.QueryType;
import com.neo4j.fabric.stream.StatementResult;
import com.neo4j.fabric.transaction.FabricTransactionInfo;
import com.neo4j.fabric.transaction.TransactionBookmarkManager;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.kernel.api.exceptions.Status.Fabric;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Mono;

public class FabricRemoteExecutor
{
    private final DriverPool driverPool;

    public FabricRemoteExecutor( DriverPool driverPool )
    {
        this.driverPool = driverPool;
    }

    public FabricRemoteExecutor.FabricRemoteTransaction begin( FabricTransactionInfo transactionInfo, TransactionBookmarkManager bookmarkManager )
    {
        return new FabricRemoteExecutor.FabricRemoteTransaction( transactionInfo, bookmarkManager );
    }

    public class FabricRemoteTransaction
    {
        private final Object writeTransactionStartLock = new Object();
        private final FabricTransactionInfo transactionInfo;
        private final TransactionBookmarkManager bookmarkManager;
        private final Map<Long,PooledDriver> usedDrivers = new ConcurrentHashMap();
        private volatile FabricConfig.Graph writingTo;
        private volatile Mono<FabricDriverTransaction> writeTransaction;

        private FabricRemoteTransaction( FabricTransactionInfo transactionInfo, TransactionBookmarkManager bookmarkManager )
        {
            this.transactionInfo = transactionInfo;
            this.bookmarkManager = bookmarkManager;
        }

        public Mono<StatementResult> run( FabricConfig.Graph location, String query, QueryType queryType, MapValue params )
        {
            if ( location.equals( this.writingTo ) )
            {
                return this.runInWriteTransaction( query, params );
            }
            else
            {
                AccessMode requestedMode = this.transactionInfo.getAccessMode();
                AccessMode effectiveMode = EffectiveQueryType.effectiveAccessMode( requestedMode, queryType );
                if ( effectiveMode == AccessMode.READ )
                {
                    return this.runInAutoCommitReadTransaction( location, query, params );
                }
                else if ( requestedMode == AccessMode.READ && effectiveMode == AccessMode.WRITE )
                {
                    throw this.writeInReadError( location );
                }
                else
                {
                    synchronized ( this.writeTransactionStartLock )
                    {
                        if ( this.writingTo != null && !this.writingTo.equals( location ) )
                        {
                            throw this.multipleWriteError( location, this.writingTo );
                        }

                        if ( this.writingTo == null )
                        {
                            this.beginWriteTransaction( location );
                        }
                    }

                    return this.runInWriteTransaction( query, params );
                }
            }
        }

        public Mono<Void> commit()
        {
            if ( this.writeTransaction == null )
            {
                this.releaseTransactionResources();
                return Mono.empty();
            }
            else
            {
                return this.writeTransaction.flatMap( FabricDriverTransaction::commit ).doOnSuccess( ( bookmark ) -> {
                    this.bookmarkManager.recordBookmarkReceivedFromGraph( this.writingTo, bookmark );
                } ).then().doFinally( ( signal ) -> {
                    this.releaseTransactionResources();
                } );
            }
        }

        public Mono<Void> rollback()
        {
            if ( this.writeTransaction == null )
            {
                this.releaseTransactionResources();
                return Mono.empty();
            }
            else
            {
                return this.writeTransaction.flatMap( FabricDriverTransaction::rollback ).doFinally( ( signal ) -> {
                    this.releaseTransactionResources();
                } );
            }
        }

        private void beginWriteTransaction( FabricConfig.Graph location )
        {
            this.writingTo = location;
            PooledDriver driver = this.getDriver( location );
            this.writeTransaction = driver.beginTransaction( location, AccessMode.WRITE, this.transactionInfo, List.of() );
        }

        private Mono<StatementResult> runInAutoCommitReadTransaction( FabricConfig.Graph location, String query, MapValue params )
        {
            PooledDriver driver = this.getDriver( location );
            List<RemoteBookmark> bookmarks = this.bookmarkManager.getBookmarksForGraph( location );
            AutoCommitStatementResult autoCommitStatementResult = driver.run( query, params, location, AccessMode.READ, this.transactionInfo, bookmarks );
            autoCommitStatementResult.getBookmark().subscribe( ( bookmark ) -> {
                this.bookmarkManager.recordBookmarkReceivedFromGraph( location, bookmark );
            } );
            return Mono.just( autoCommitStatementResult );
        }

        private Mono<StatementResult> runInWriteTransaction( String query, MapValue params )
        {
            return this.writeTransaction.map( ( rxTransaction ) -> {
                return rxTransaction.run( query, params );
            } );
        }

        private PooledDriver getDriver( FabricConfig.Graph location )
        {
            return (PooledDriver) this.usedDrivers.computeIfAbsent( location.getId(), ( gid ) -> {
                return FabricRemoteExecutor.this.driverPool.getDriver( location, this.transactionInfo.getLoginContext().subject() );
            } );
        }

        private FabricException writeInReadError( FabricConfig.Graph attempt )
        {
            return new FabricException( Fabric.AccessMode, "Writing in read access mode not allowed. Attempted write to %s", new Object[]{attempt} );
        }

        private FabricException multipleWriteError( FabricConfig.Graph attempt, FabricConfig.Graph writingTo )
        {
            return new FabricException( Fabric.AccessMode, "Multi-shard writes not allowed. Attempted write to %s, currently writing to %s",
                    new Object[]{attempt, writingTo} );
        }

        private void releaseTransactionResources()
        {
            this.usedDrivers.values().forEach( PooledDriver::release );
        }
    }
}
