package com.neo4j.fabric.executor;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class FabricMultiException extends RuntimeException
{
    private final List<RuntimeException> errors;

    public FabricMultiException( List<RuntimeException> errors )
    {
        super( getFirst( errors ).getMessage(), getFirst( errors ) );
        this.errors = errors;
    }

    private static RuntimeException getFirst( List<RuntimeException> errors )
    {
        if ( errors.isEmpty() )
        {
            throw new IllegalArgumentException( "The submitted error list cannot be empty" );
        }
        else
        {
            return (RuntimeException) errors.get( 0 );
        }
    }

    public List<RuntimeException> getErros()
    {
        return this.errors;
    }

    public String getMessage()
    {
        return (String) this.errors.stream().map( ( e ) -> {
            String var10000 = e.getClass().getName();
            return var10000 + (e.getMessage() != null ? ": " + e.getMessage() : "");
        } ).collect( Collectors.joining( "\n", "A MultiException has the following " + this.errors.size() + " exceptions:\n", "" ) );
    }

    public void printStackTrace( PrintStream s )
    {
        Iterator var2 = this.errors.iterator();

        while ( var2.hasNext() )
        {
            Throwable error = (Throwable) var2.next();
            error.printStackTrace( s );
        }
    }

    public void printStackTrace( PrintWriter s )
    {
        Iterator var2 = this.errors.iterator();

        while ( var2.hasNext() )
        {
            Throwable error = (Throwable) var2.next();
            error.printStackTrace( s );
        }
    }
}
