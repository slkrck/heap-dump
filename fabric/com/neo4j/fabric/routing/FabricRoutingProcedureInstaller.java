package com.neo4j.fabric.routing;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.localdb.FabricDatabaseManager;

import java.util.List;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.api.procedure.CallableProcedure;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.BaseRoutingProcedureInstaller;

public class FabricRoutingProcedureInstaller extends BaseRoutingProcedureInstaller
{
    private final DatabaseManager<?> databaseManager;
    private final ConnectorPortRegister portRegister;
    private final Config config;
    private final FabricDatabaseManager fabricDatabaseManager;
    private final FabricConfig fabricConfig;
    private final LogProvider logProvider;

    public FabricRoutingProcedureInstaller( DatabaseManager<?> databaseManager, ConnectorPortRegister portRegister, Config config,
            FabricDatabaseManager fabricDatabaseManager, FabricConfig fabricConfig, LogProvider logProvider )
    {
        this.databaseManager = databaseManager;
        this.portRegister = portRegister;
        this.config = config;
        this.fabricDatabaseManager = fabricDatabaseManager;
        this.fabricConfig = fabricConfig;
        this.logProvider = logProvider;
    }

    protected CallableProcedure createProcedure( List<String> namespace )
    {
        return new FabricSingleInstanceGetRoutingTableProcedure( namespace, this.databaseManager, this.portRegister, this.config, this.fabricDatabaseManager,
                this.fabricConfig, this.logProvider );
    }
}
