package com.neo4j.fabric.pipeline;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import org.neo4j.cypher.internal.logical.plans.CypherValue;
import org.neo4j.cypher.internal.logical.plans.FieldSignature;
import org.neo4j.cypher.internal.logical.plans.ProcedureAccessMode;
import org.neo4j.cypher.internal.logical.plans.ProcedureDbmsAccess;
import org.neo4j.cypher.internal.logical.plans.ProcedureReadOnlyAccess;
import org.neo4j.cypher.internal.logical.plans.ProcedureReadWriteAccess;
import org.neo4j.cypher.internal.logical.plans.ProcedureSchemaWriteAccess;
import org.neo4j.cypher.internal.logical.plans.ProcedureSignature;
import org.neo4j.cypher.internal.logical.plans.QualifiedName;
import org.neo4j.cypher.internal.logical.plans.UserFunctionSignature;
import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.exceptions.CypherExecutionException;
import org.neo4j.internal.kernel.api.procs.DefaultParameterValue;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.ProcedureHandle;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.AnyType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.BooleanType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.ByteArrayType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.DateTimeType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.DateType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.DurationType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.FloatType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.GeometryType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.IntegerType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.ListType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.LocalDateTimeType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.LocalTimeType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.MapType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.NodeType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.NumberType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.PathType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.PointType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.RelationshipType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.TextType;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.TimeType;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.procedure.Mode;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Option.;
import scala.collection.IndexedSeq;
import scala.collection.TraversableOnce;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SignatureResolver implements ProcedureSignatureResolver
{
    private final Supplier<GlobalProcedures> registrySupplier;

    public SignatureResolver( final Supplier<GlobalProcedures> registrySupplier )
    {
        this.registrySupplier = registrySupplier;
    }

    public Option<UserFunctionSignature> functionSignature( final QualifiedName name )
    {
        return .MODULE$.apply( ((GlobalProcedures) this.registrySupplier.get()).function( this.asKernelQualifiedName( name ) ) ).map( ( fcn ) -> {
        org.neo4j.internal.kernel.api.procs.UserFunctionSignature signature = fcn.signature();
        return new UserFunctionSignature( name, (IndexedSeq) ((TraversableOnce) scala.collection.JavaConverters..MODULE$.asScalaBufferConverter(
                signature.inputSignature() ).asScala() ).toIndexedSeq().map( ( s ) -> {
            return new FieldSignature( s.name(), this.asCypherType( s.neo4jType() ), this.OptionalOps( s.defaultValue() ).asScala().map( ( neo4jValue ) -> {
                return this.asCypherValue( neo4jValue );
            } ), org.neo4j.cypher.internal.logical.plans.FieldSignature..MODULE$.apply$default$4());
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom()),
        this.asCypherType( signature.outputType() ), this.OptionalOps( signature.deprecated() ).asScala(), signature.allowed(), this.OptionalOps(
                signature.description() ).asScala(), false, fcn.id(), fcn.threadSafe());
    } );
    }

    public ProcedureSignature procedureSignature( final QualifiedName name )
    {
        org.neo4j.internal.kernel.api.procs.QualifiedName kn =
                new org.neo4j.internal.kernel.api.procs.QualifiedName( (List) scala.collection.JavaConverters..MODULE$.seqAsJavaListConverter(
                        name.namespace() ).asJava(), name.name());
        ProcedureHandle handle = ((GlobalProcedures) this.registrySupplier.get()).procedure( kn );
        org.neo4j.internal.kernel.api.procs.ProcedureSignature signature = handle.signature();
        return new ProcedureSignature( name, (IndexedSeq) ((TraversableOnce) scala.collection.JavaConverters..MODULE$.asScalaBufferConverter(
                signature.inputSignature() ).asScala() ).toIndexedSeq().map( ( s ) -> {
            return new FieldSignature( s.name(), this.asCypherType( s.neo4jType() ), this.OptionalOps( s.defaultValue() ).asScala().map( ( neo4jValue ) -> {
                return this.asCypherValue( neo4jValue );
            } ), org.neo4j.cypher.internal.logical.plans.FieldSignature..MODULE$.apply$default$4());
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom()),(Option) (signature.isVoid() ? scala.None..MODULE$ :new Some(
            ((TraversableOnce) scala.collection.JavaConverters..MODULE$.asScalaBufferConverter( signature.outputSignature() ).asScala() ).toIndexedSeq().map(
            ( s ) -> {
                String x$1 = s.name();
                CypherType x$2 = this.asCypherType( s.neo4jType() );
                boolean x$3 = s.isDeprecated();
                Option x$4 = org.neo4j.cypher.internal.logical.plans.FieldSignature..MODULE$.apply$default$3();
                return new FieldSignature( x$1, x$2, x$4, x$3 );
            }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom()))),
        this.OptionalOps( signature.deprecated() ).asScala(), this.asCypherProcMode( signature.mode(), signature.allowed() ), this.OptionalOps(
                signature.description() ).asScala(), this.OptionalOps(
                signature.warning() ).asScala(), signature.eager(), handle.id(), signature.systemProcedure());
    }

    private org.neo4j.internal.kernel.api.procs.QualifiedName asKernelQualifiedName( final QualifiedName name )
    {
        return new org.neo4j.internal.kernel.api.procs.QualifiedName(
                (String[]) name.namespace().toArray( scala.reflect.ClassTag..MODULE$.apply( String.class ) ),name.name());
    }

    private CypherValue asCypherValue( final DefaultParameterValue neo4jValue )
    {
        return new CypherValue( neo4jValue.value(), this.asCypherType( neo4jValue.neo4jType() ) );
    }

    private CypherType asCypherType( final AnyType neoType )
    {
        Object var2;
        label329:
        {
            TextType var10000 = Neo4jTypes.NTString;
            if ( var10000 == null )
            {
                if ( neoType == null )
                {
                    break label329;
                }
            }
            else if ( var10000.equals( neoType ) )
            {
                break label329;
            }

            label330:
            {
                IntegerType var28 = Neo4jTypes.NTInteger;
                if ( var28 == null )
                {
                    if ( neoType != null )
                    {
                        break label330;
                    }
                }
                else if ( !var28.equals( neoType ) )
                {
                    break label330;
                }

                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                return (CypherType) var2;
            }

            label331:
            {
                FloatType var29 = Neo4jTypes.NTFloat;
                if ( var29 == null )
                {
                    if ( neoType == null )
                    {
                        break label331;
                    }
                }
                else if ( var29.equals( neoType ) )
                {
                    break label331;
                }

                label332:
                {
                    NumberType var30 = Neo4jTypes.NTNumber;
                    if ( var30 == null )
                    {
                        if ( neoType != null )
                        {
                            break label332;
                        }
                    }
                    else if ( !var30.equals( neoType ) )
                    {
                        break label332;
                    }

                    var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNumber();
                    return (CypherType) var2;
                }

                label333:
                {
                    BooleanType var31 = Neo4jTypes.NTBoolean;
                    if ( var31 == null )
                    {
                        if ( neoType == null )
                        {
                            break label333;
                        }
                    }
                    else if ( var31.equals( neoType ) )
                    {
                        break label333;
                    }

                    if ( neoType instanceof ListType )
                    {
                        ListType var9 = (ListType) neoType;
                        var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTList( this.asCypherType( var9.innerType() ) );
                        return (CypherType) var2;
                    }

                    label334:
                    {
                        ByteArrayType var32 = Neo4jTypes.NTByteArray;
                        if ( var32 == null )
                        {
                            if ( neoType != null )
                            {
                                break label334;
                            }
                        }
                        else if ( !var32.equals( neoType ) )
                        {
                            break label334;
                        }

                        var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTList( org.neo4j.cypher.internal.v4_0.util.symbols.package..
                        MODULE$.CTAny());
                        return (CypherType) var2;
                    }

                    label335:
                    {
                        DateTimeType var33 = Neo4jTypes.NTDateTime;
                        if ( var33 == null )
                        {
                            if ( neoType == null )
                            {
                                break label335;
                            }
                        }
                        else if ( var33.equals( neoType ) )
                        {
                            break label335;
                        }

                        label336:
                        {
                            LocalDateTimeType var34 = Neo4jTypes.NTLocalDateTime;
                            if ( var34 == null )
                            {
                                if ( neoType == null )
                                {
                                    break label336;
                                }
                            }
                            else if ( var34.equals( neoType ) )
                            {
                                break label336;
                            }

                            label337:
                            {
                                DateType var35 = Neo4jTypes.NTDate;
                                if ( var35 == null )
                                {
                                    if ( neoType != null )
                                    {
                                        break label337;
                                    }
                                }
                                else if ( !var35.equals( neoType ) )
                                {
                                    break label337;
                                }

                                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTDate();
                                return (CypherType) var2;
                            }

                            label338:
                            {
                                TimeType var36 = Neo4jTypes.NTTime;
                                if ( var36 == null )
                                {
                                    if ( neoType != null )
                                    {
                                        break label338;
                                    }
                                }
                                else if ( !var36.equals( neoType ) )
                                {
                                    break label338;
                                }

                                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTTime();
                                return (CypherType) var2;
                            }

                            label339:
                            {
                                LocalTimeType var37 = Neo4jTypes.NTLocalTime;
                                if ( var37 == null )
                                {
                                    if ( neoType != null )
                                    {
                                        break label339;
                                    }
                                }
                                else if ( !var37.equals( neoType ) )
                                {
                                    break label339;
                                }

                                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTLocalTime();
                                return (CypherType) var2;
                            }

                            label340:
                            {
                                DurationType var38 = Neo4jTypes.NTDuration;
                                if ( var38 == null )
                                {
                                    if ( neoType == null )
                                    {
                                        break label340;
                                    }
                                }
                                else if ( var38.equals( neoType ) )
                                {
                                    break label340;
                                }

                                label341:
                                {
                                    PointType var39 = Neo4jTypes.NTPoint;
                                    if ( var39 == null )
                                    {
                                        if ( neoType == null )
                                        {
                                            break label341;
                                        }
                                    }
                                    else if ( var39.equals( neoType ) )
                                    {
                                        break label341;
                                    }

                                    label342:
                                    {
                                        NodeType var40 = Neo4jTypes.NTNode;
                                        if ( var40 == null )
                                        {
                                            if ( neoType == null )
                                            {
                                                break label342;
                                            }
                                        }
                                        else if ( var40.equals( neoType ) )
                                        {
                                            break label342;
                                        }

                                        label343:
                                        {
                                            RelationshipType var41 = Neo4jTypes.NTRelationship;
                                            if ( var41 == null )
                                            {
                                                if ( neoType != null )
                                                {
                                                    break label343;
                                                }
                                            }
                                            else if ( !var41.equals( neoType ) )
                                            {
                                                break label343;
                                            }

                                            var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                            return (CypherType) var2;
                                        }

                                        label344:
                                        {
                                            PathType var24 = Neo4jTypes.NTPath;
                                            if ( var24 == null )
                                            {
                                                if ( neoType == null )
                                                {
                                                    break label344;
                                                }
                                            }
                                            else if ( var24.equals( neoType ) )
                                            {
                                                break label344;
                                            }

                                            label345:
                                            {
                                                GeometryType var25 = Neo4jTypes.NTGeometry;
                                                if ( var25 == null )
                                                {
                                                    if ( neoType != null )
                                                    {
                                                        break label345;
                                                    }
                                                }
                                                else if ( !var25.equals( neoType ) )
                                                {
                                                    break label345;
                                                }

                                                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTGeometry();
                                                return (CypherType) var2;
                                            }

                                            label346:
                                            {
                                                MapType var26 = Neo4jTypes.NTMap;
                                                if ( var26 == null )
                                                {
                                                    if ( neoType == null )
                                                    {
                                                        break label346;
                                                    }
                                                }
                                                else if ( var26.equals( neoType ) )
                                                {
                                                    break label346;
                                                }

                                                AnyType var27 = Neo4jTypes.NTAny;
                                                if ( var27 == null )
                                                {
                                                    if ( neoType != null )
                                                    {
                                                        throw new MatchError( neoType );
                                                    }
                                                }
                                                else if ( !var27.equals( neoType ) )
                                                {
                                                    throw new MatchError( neoType );
                                                }

                                                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
                                                return (CypherType) var2;
                                            }

                                            var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTMap();
                                            return (CypherType) var2;
                                        }

                                        var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTPath();
                                        return (CypherType) var2;
                                    }

                                    var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                    return (CypherType) var2;
                                }

                                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTPoint();
                                return (CypherType) var2;
                            }

                            var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTDuration();
                            return (CypherType) var2;
                        }

                        var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTLocalDateTime();
                        return (CypherType) var2;
                    }

                    var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTDateTime();
                    return (CypherType) var2;
                }

                var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                return (CypherType) var2;
            }

            var2 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
            return (CypherType) var2;
        }

        var2 = (CypherType) org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
        return (CypherType) var2;
    }

    public ProcedureAccessMode asCypherProcMode( final Mode mode, final String[] allowed )
    {
        Object var3;
        if ( Mode.READ.equals( mode ) )
        {
            var3 = new ProcedureReadOnlyAccess( allowed );
        }
        else if ( Mode.DEFAULT.equals( mode ) )
        {
            var3 = new ProcedureReadOnlyAccess( allowed );
        }
        else if ( Mode.WRITE.equals( mode ) )
        {
            var3 = new ProcedureReadWriteAccess( allowed );
        }
        else if ( Mode.SCHEMA.equals( mode ) )
        {
            var3 = new ProcedureSchemaWriteAccess( allowed );
        }
        else
        {
            if ( !Mode.DBMS.equals( mode ) )
            {
                throw new CypherExecutionException(
                        (new StringBuilder( 81 )).append( "Unable to execute procedure, because it requires an unrecognized execution mode: " ).append(
                                mode.name() ).toString(), (Throwable) null );
            }

            var3 = new ProcedureDbmsAccess( allowed );
        }

        return (ProcedureAccessMode) var3;
    }

    private <T> SignatureResolver.OptionalOps<T> OptionalOps( final Optional<T> optional )
    {
        return new SignatureResolver.OptionalOps( this, optional );
    }

    public class OptionalOps<T>
    {
        private final Optional<T> optional;

        public OptionalOps( final SignatureResolver $outer, final Optional<T> optional )
        {
            this.optional = optional;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public Option<T> asScala()
        {
            return (Option) (this.optional.isPresent() ? new Some( this.optional.get() ) : scala.None..MODULE$);
        }
    }
}
