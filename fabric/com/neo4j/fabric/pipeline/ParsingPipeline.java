package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.v4_0.frontend.PlannerName;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Transformer;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ParsingPipeline implements Pipeline, Product, Serializable
{
    private final Seq<Transformer<BaseContext,BaseState,BaseState>> parts;
    private final BaseContext context;
    private final Transformer<BaseContext,BaseState,BaseState> transformer;

    public ParsingPipeline( final Seq<Transformer<BaseContext,BaseState,BaseState>> parts, final BaseContext context )
    {
        this.parts = parts;
        this.context = context;
        Pipeline.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<Seq<Transformer<BaseContext,BaseState,BaseState>>,BaseContext>> unapply( final ParsingPipeline x$0 )
    {
        return ParsingPipeline$.MODULE$.unapply( var0 );
    }

    public static ParsingPipeline apply( final Seq<Transformer<BaseContext,BaseState,BaseState>> parts, final BaseContext context )
    {
        return ParsingPipeline$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<Seq<Transformer<BaseContext,BaseState,BaseState>>,BaseContext>,ParsingPipeline> tupled()
    {
        return ParsingPipeline$.MODULE$.tupled();
    }

    public static Function1<Seq<Transformer<BaseContext,BaseState,BaseState>>,Function1<BaseContext,ParsingPipeline>> curried()
    {
        return ParsingPipeline$.MODULE$.curried();
    }

    public Transformer<BaseContext,BaseState,BaseState> transformer()
    {
        return this.transformer;
    }

    public void com$neo4j$fabric$pipeline$Pipeline$_setter_$transformer_$eq( final Transformer<BaseContext,BaseState,BaseState> x$1 )
    {
        this.transformer = x$1;
    }

    public Seq<Transformer<BaseContext,BaseState,BaseState>> parts()
    {
        return this.parts;
    }

    public BaseContext context()
    {
        return this.context;
    }

    public BaseState process( final String query )
    {
        return (BaseState) this.transformer().transform( new InitialState( query, scala.None..MODULE$, (PlannerName) null,
                org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState..MODULE$.apply$default$4(),
        org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState..MODULE$.apply$default$5(), org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState..
        MODULE$.apply$default$6(), org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState..
        MODULE$.apply$default$7(), org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState..
        MODULE$.apply$default$8(), org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState..
        MODULE$.apply$default$9(), org.neo4j.cypher.internal.v4_0.frontend.phases.InitialState..MODULE$.apply$default$10()),this.context());
    }

    public ParsingPipeline copy( final Seq<Transformer<BaseContext,BaseState,BaseState>> parts, final BaseContext context )
    {
        return new ParsingPipeline( parts, context );
    }

    public Seq<Transformer<BaseContext,BaseState,BaseState>> copy$default$1()
    {
        return this.parts();
    }

    public BaseContext copy$default$2()
    {
        return this.context();
    }

    public String productPrefix()
    {
        return "ParsingPipeline";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.parts();
            break;
        case 1:
            var10000 = this.context();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ParsingPipeline;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof ParsingPipeline )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            ParsingPipeline var4 = (ParsingPipeline) x$1;
                            Seq var10000 = this.parts();
                            Seq var5 = var4.parts();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            BaseContext var7 = this.context();
                            BaseContext var6 = var4.context();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
