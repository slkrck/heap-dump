package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class FabricPreparatoryRewriting$ extends AbstractFunction1<ProcedureSignatureResolver,FabricPreparatoryRewriting> implements Serializable
{
    public static FabricPreparatoryRewriting$ MODULE$;

    static
    {
        new FabricPreparatoryRewriting$();
    }

    private FabricPreparatoryRewriting$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "FabricPreparatoryRewriting";
    }

    public FabricPreparatoryRewriting apply( final ProcedureSignatureResolver signatures )
    {
        return new FabricPreparatoryRewriting( signatures );
    }

    public Option<ProcedureSignatureResolver> unapply( final FabricPreparatoryRewriting x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.signatures() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
