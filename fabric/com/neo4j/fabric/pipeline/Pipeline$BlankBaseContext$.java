package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.v4_0.frontend.phases.CompilationPhaseTracer;
import org.neo4j.cypher.internal.v4_0.frontend.phases.InternalNotificationLogger;
import org.neo4j.cypher.internal.v4_0.frontend.phases.devNullLogger.;

public class Pipeline$BlankBaseContext$
{
    public static Pipeline$BlankBaseContext$ MODULE$;

    static
    {
        new Pipeline$BlankBaseContext$();
    }

    public Pipeline$BlankBaseContext$()
    {
        MODULE$ = this;
    }

    public CompilationPhaseTracer $lessinit$greater$default$3()
    {
        return CompilationPhaseTracer.NO_TRACING;
    }

    public InternalNotificationLogger $lessinit$greater$default$4()
    {
        return .MODULE$;
    }
}
