package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Condition;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Phase;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Transformer;
import org.neo4j.cypher.internal.v4_0.frontend.phases.CompilationPhaseTracer.CompilationPhase;
import org.neo4j.cypher.internal.v4_0.rewriting.rewriters.expandStar;
import org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class FabricPreparatoryRewriting implements Phase<BaseContext,BaseState,BaseState>, Product, Serializable
{
    private final ProcedureSignatureResolver signatures;
    private final CompilationPhase phase;
    private final String description;

    public FabricPreparatoryRewriting( final ProcedureSignatureResolver signatures )
    {
        this.signatures = signatures;
        Transformer.$init$( this );
        Phase.$init$( this );
        Product.$init$( this );
        this.phase = CompilationPhase.AST_REWRITE;
        this.description = "rewrite the AST into a shape that the fabric planner can act on";
    }

    public static Option<ProcedureSignatureResolver> unapply( final FabricPreparatoryRewriting x$0 )
    {
        return FabricPreparatoryRewriting$.MODULE$.unapply( var0 );
    }

    public static FabricPreparatoryRewriting apply( final ProcedureSignatureResolver signatures )
    {
        return FabricPreparatoryRewriting$.MODULE$.apply( var0 );
    }

    public static <A> Function1<A,FabricPreparatoryRewriting> compose( final Function1<A,ProcedureSignatureResolver> g )
    {
        return FabricPreparatoryRewriting$.MODULE$.compose( var0 );
    }

    public Object transform( final Object from, final BaseContext context )
    {
        return Phase.transform$( this, from, context );
    }

    public String name()
    {
        return Phase.name$( this );
    }

    public <D extends BaseContext, TO2> Transformer<D,BaseState,TO2> andThen( final Transformer<D,BaseState,TO2> other )
    {
        return Transformer.andThen$( this, other );
    }

    public Transformer<BaseContext,BaseState,BaseState> adds( final Condition condition )
    {
        return Transformer.adds$( this, condition );
    }

    public ProcedureSignatureResolver signatures()
    {
        return this.signatures;
    }

    public CompilationPhase phase()
    {
        return this.phase;
    }

    public String description()
    {
        return this.description;
    }

    public BaseState process( final BaseState from, final BaseContext context )
    {
        return from.withStatement(
                (Statement).MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( from.statement() ),
                this.chain( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new Function1[]{new expandStar( from.semantics() ), new TryResolveProcedures( this.signatures() )}) ) )));
    }

    private <T> Function1<T,T> chain( final Seq<Function1<T,T>> funcs )
    {
        return (Function1) funcs.reduceLeft( ( x$3, x$4 ) -> {
            return x$3.andThen( x$4 );
        } );
    }

    public Set<Condition> postConditions()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.collection.immutable.Nil..MODULE$);
    }

    public FabricPreparatoryRewriting copy( final ProcedureSignatureResolver signatures )
    {
        return new FabricPreparatoryRewriting( signatures );
    }

    public ProcedureSignatureResolver copy$default$1()
    {
        return this.signatures();
    }

    public String productPrefix()
    {
        return "FabricPreparatoryRewriting";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.signatures();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FabricPreparatoryRewriting;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof FabricPreparatoryRewriting )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        FabricPreparatoryRewriting var4 = (FabricPreparatoryRewriting) x$1;
                        ProcedureSignatureResolver var10000 = this.signatures();
                        ProcedureSignatureResolver var5 = var4.signatures();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
