package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Transformer;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction2;

public final class ParsingPipeline$ extends AbstractFunction2<Seq<Transformer<BaseContext,BaseState,BaseState>>,BaseContext,ParsingPipeline>
        implements Serializable
{
    public static ParsingPipeline$ MODULE$;

    static
    {
        new ParsingPipeline$();
    }

    private ParsingPipeline$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ParsingPipeline";
    }

    public ParsingPipeline apply( final Seq<Transformer<BaseContext,BaseState,BaseState>> parts, final BaseContext context )
    {
        return new ParsingPipeline( parts, context );
    }

    public Option<Tuple2<Seq<Transformer<BaseContext,BaseState,BaseState>>,BaseContext>> unapply( final ParsingPipeline x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.parts(), x$0.context() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
