package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.AstRewriting;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContains;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.PreparatoryRewriting;
import org.neo4j.cypher.internal.v4_0.frontend.phases.SemanticAnalysis;
import org.neo4j.cypher.internal.v4_0.frontend.phases.SyntaxDeprecationWarnings;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Transformer;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Namespacer.;
import org.neo4j.cypher.internal.v4_0.rewriting.rewriters.GeneratingNamer;
import scala.Function1;
import scala.Product;
import scala.collection.Seq;

public final class Pipeline$
{
    public static Pipeline$ MODULE$;

    static
    {
        new Pipeline$();
    }

    private final Seq<Product> features;
    private final Transformer<BaseContext,BaseState,BaseState> com$neo4j$fabric$pipeline$Pipeline$$parse;
    private final SyntaxDeprecationWarnings com$neo4j$fabric$pipeline$Pipeline$$deprecations;
    private final Transformer<BaseContext,BaseState,BaseState> com$neo4j$fabric$pipeline$Pipeline$$semantics;
    private final com$neo4j$fabric$pipeline$Pipeline$$namespace;
    private final PreparatoryRewriting com$neo4j$fabric$pipeline$Pipeline$$prepare;
    private final AstRewriting com$neo4j$fabric$pipeline$Pipeline$$rewrite;

    public com$neo4j$fabric$pipeline$Pipeline$$namespace()
    {
        return this.com$neo4j$fabric$pipeline$Pipeline$$namespace;
    }

    private Pipeline$()
    {
        MODULE$ = this;
        this.features = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new Product[]{org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticFeature.Cypher9Comparability..MODULE$,
            org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticFeature.MultipleDatabases..MODULE$, org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticFeature.MultipleGraphs..
        MODULE$, org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticFeature.UseGraphSelector..
        MODULE$, org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticFeature.CorrelatedSubQueries..
        MODULE$, org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticFeature.ExpressionsInViewInvocations..MODULE$})));
        this.com$neo4j$fabric$pipeline$Pipeline$$parse = org.neo4j.cypher.internal.v4_0.frontend.phases.Parsing..
        MODULE$.adds( new BaseContains( scala.reflect.ClassTag..MODULE$.apply( Statement.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                Statement.class )));
        this.com$neo4j$fabric$pipeline$Pipeline$$deprecations = new SyntaxDeprecationWarnings( org.neo4j.cypher.internal.v4_0.rewriting.Deprecations.V2..MODULE$)
        ;
        this.com$neo4j$fabric$pipeline$Pipeline$$semantics =
                (new SemanticAnalysis( true, this.features() )).adds( new BaseContains( scala.reflect.ClassTag..MODULE$.apply( SemanticState.class ),
                        scala.reflect.ManifestFactory..MODULE$.classType( SemanticState.class )));
        this.com$neo4j$fabric$pipeline$Pipeline$$prepare = new PreparatoryRewriting( org.neo4j.cypher.internal.v4_0.rewriting.Deprecations.V2..MODULE$);
        this.com$neo4j$fabric$pipeline$Pipeline$$namespace = .MODULE$;
        Function1 x$5 = ( sequenceName ) -> {
            return org.neo4j.cypher.internal.v4_0.rewriting.RewriterStepSequencer..MODULE$.newPlain( sequenceName );
        }; org.neo4j.cypher.internal.v4_0.rewriting.rewriters.Never.x$6 = org.neo4j.cypher.internal.v4_0.rewriting.rewriters.Never..MODULE$;
        GeneratingNamer x$7 = new GeneratingNamer();
        boolean x$8 = org.neo4j.cypher.internal.v4_0.frontend.phases.AstRewriting..MODULE$.apply$default$3();
        this.com$neo4j$fabric$pipeline$Pipeline$$rewrite = new AstRewriting( x$5, x$6, x$8, x$7 );
    }

    private Seq<Product> features()
    {
        return this.features;
    }

    public Transformer<BaseContext,BaseState,BaseState> com$neo4j$fabric$pipeline$Pipeline$$parse()
    {
        return this.com$neo4j$fabric$pipeline$Pipeline$$parse;
    }

    public SyntaxDeprecationWarnings com$neo4j$fabric$pipeline$Pipeline$$deprecations()
    {
        return this.com$neo4j$fabric$pipeline$Pipeline$$deprecations;
    }

    public Transformer<BaseContext,BaseState,BaseState> com$neo4j$fabric$pipeline$Pipeline$$semantics()
    {
        return this.com$neo4j$fabric$pipeline$Pipeline$$semantics;
    }

    public PreparatoryRewriting com$neo4j$fabric$pipeline$Pipeline$$prepare()
    {
        return this.com$neo4j$fabric$pipeline$Pipeline$$prepare;
    }

    public FabricPreparatoryRewriting com$neo4j$fabric$pipeline$Pipeline$$fabricPrepare( final ProcedureSignatureResolver signatures )
    {
        return new FabricPreparatoryRewriting( signatures );
    }

    public AstRewriting com$neo4j$fabric$pipeline$Pipeline$$rewrite()
    {
        return this.com$neo4j$fabric$pipeline$Pipeline$$rewrite;
    }
}
