package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.compiler.Neo4jCypherExceptionFactory;
import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.cypher.internal.planning.WrappedMonitors;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.CompilationPhaseTracer;
import org.neo4j.cypher.internal.v4_0.frontend.phases.InternalNotificationLogger;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Monitors;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Transformer;
import org.neo4j.cypher.internal.v4_0.util.CypherExceptionFactory;
import scala.Function1;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface Pipeline
{
    static void $init$( final Pipeline $this )
    {
        $this.com$neo4j$fabric$pipeline$Pipeline$_setter_$transformer_$eq( (Transformer) $this.parts().reduce( ( x$1, x$2 ) -> {
            return x$1.andThen( x$2 );
        } ) );
    }

    void com$neo4j$fabric$pipeline$Pipeline$_setter_$transformer_$eq( final Transformer<BaseContext,BaseState,BaseState> x$1 );

    Seq<Transformer<BaseContext,BaseState,BaseState>> parts();

    BaseContext context();

    Transformer<BaseContext,BaseState,BaseState> transformer();

    public static class BlankBaseContext implements BaseContext
    {
        private final CypherExceptionFactory cypherExceptionFactory;
        private final Monitors monitors;
        private final CompilationPhaseTracer tracer;
        private final InternalNotificationLogger notificationLogger;
        private final Function1<Seq<SemanticErrorDef>,BoxedUnit> errorHandler;

        public BlankBaseContext( final CypherExceptionFactory cypherExceptionFactory, final Monitors monitors, final CompilationPhaseTracer tracer,
                final InternalNotificationLogger notificationLogger )
        {
            this.cypherExceptionFactory = cypherExceptionFactory;
            this.monitors = monitors;
            this.tracer = tracer;
            this.notificationLogger = notificationLogger;
            this.errorHandler = ( errors ) -> {
                $anonfun$errorHandler$1( this, errors );
                return BoxedUnit.UNIT;
            };
        }

        public CypherExceptionFactory cypherExceptionFactory()
        {
            return this.cypherExceptionFactory;
        }

        public Monitors monitors()
        {
            return this.monitors;
        }

        public CompilationPhaseTracer tracer()
        {
            return this.tracer;
        }

        public InternalNotificationLogger notificationLogger()
        {
            return this.notificationLogger;
        }

        public Function1<Seq<SemanticErrorDef>,BoxedUnit> errorHandler()
        {
            return this.errorHandler;
        }
    }

    public static class Instance implements Product, Serializable
    {
        private final org.neo4j.monitoring.Monitors kernelMonitors;
        private final String queryText;
        private final ProcedureSignatureResolver signatures;
        private final WrappedMonitors monitors;
        private final Neo4jCypherExceptionFactory exceptionFactory;
        private final BaseContext context;
        private final ParsingPipeline parseAndPrepare;
        private final AnalysisPipeline checkAndFinalize;

        public Instance( final org.neo4j.monitoring.Monitors kernelMonitors, final String queryText, final ProcedureSignatureResolver signatures )
        {
            this.kernelMonitors = kernelMonitors;
            this.queryText = queryText;
            this.signatures = signatures;
            Product.$init$( this );
            this.monitors = new WrappedMonitors( kernelMonitors );
            this.exceptionFactory = new Neo4jCypherExceptionFactory( queryText, scala.None..MODULE$);
            this.context =
                    new Pipeline.BlankBaseContext( this.exceptionFactory(), this.monitors(), Pipeline$BlankBaseContext$.MODULE$.$lessinit$greater$default$3(),
                            Pipeline$BlankBaseContext$.MODULE$.$lessinit$greater$default$4() );
            this.parseAndPrepare = new ParsingPipeline( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Transformer[]{Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$parse(),
                            Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$deprecations(),
                            Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$prepare(), Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$semantics(),
                            Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$fabricPrepare( signatures ),
                            Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$semantics()}) )),this.context());
            this.checkAndFinalize = new AnalysisPipeline( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Transformer[]{Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$prepare(),
                            Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$semantics(),
                            Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$namespace(),
                            Pipeline$.MODULE$.com$neo4j$fabric$pipeline$Pipeline$$rewrite()}) )),this.context());
        }

        public org.neo4j.monitoring.Monitors kernelMonitors()
        {
            return this.kernelMonitors;
        }

        public String queryText()
        {
            return this.queryText;
        }

        public ProcedureSignatureResolver signatures()
        {
            return this.signatures;
        }

        private WrappedMonitors monitors()
        {
            return this.monitors;
        }

        private Neo4jCypherExceptionFactory exceptionFactory()
        {
            return this.exceptionFactory;
        }

        private BaseContext context()
        {
            return this.context;
        }

        public ParsingPipeline parseAndPrepare()
        {
            return this.parseAndPrepare;
        }

        public AnalysisPipeline checkAndFinalize()
        {
            return this.checkAndFinalize;
        }

        public Pipeline.Instance copy( final org.neo4j.monitoring.Monitors kernelMonitors, final String queryText, final ProcedureSignatureResolver signatures )
        {
            return new Pipeline.Instance( kernelMonitors, queryText, signatures );
        }

        public org.neo4j.monitoring.Monitors copy$default$1()
        {
            return this.kernelMonitors();
        }

        public String copy$default$2()
        {
            return this.queryText();
        }

        public ProcedureSignatureResolver copy$default$3()
        {
            return this.signatures();
        }

        public String productPrefix()
        {
            return "Instance";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.kernelMonitors();
                break;
            case 1:
                var10000 = this.queryText();
                break;
            case 2:
                var10000 = this.signatures();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Pipeline.Instance;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof Pipeline.Instance )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                Pipeline.Instance var4 = (Pipeline.Instance) x$1;
                                org.neo4j.monitoring.Monitors var10000 = this.kernelMonitors();
                                org.neo4j.monitoring.Monitors var5 = var4.kernelMonitors();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                String var8 = this.queryText();
                                String var6 = var4.queryText();
                                if ( var8 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var6 ) )
                                {
                                    break label63;
                                }

                                ProcedureSignatureResolver var9 = this.signatures();
                                ProcedureSignatureResolver var7 = var4.signatures();
                                if ( var9 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var9.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var10 = true;
                                    break label54;
                                }
                            }

                            var10 = false;
                        }

                        if ( var10 )
                        {
                            break label72;
                        }
                    }

                    var10 = false;
                    return var10;
                }
            }

            var10 = true;
            return var10;
        }
    }
}
