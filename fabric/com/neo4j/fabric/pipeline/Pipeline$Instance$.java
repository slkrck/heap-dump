package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.monitoring.Monitors;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public class Pipeline$Instance$ extends AbstractFunction3<Monitors,String,ProcedureSignatureResolver,Pipeline.Instance> implements Serializable
{
    public static Pipeline$Instance$ MODULE$;

    static
    {
        new Pipeline$Instance$();
    }

    public Pipeline$Instance$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Instance";
    }

    public Pipeline.Instance apply( final Monitors kernelMonitors, final String queryText, final ProcedureSignatureResolver signatures )
    {
        return new Pipeline.Instance( kernelMonitors, queryText, signatures );
    }

    public Option<Tuple3<Monitors,String,ProcedureSignatureResolver>> unapply( final Pipeline.Instance x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.kernelMonitors(), x$0.queryText(), x$0.signatures() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
