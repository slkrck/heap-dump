package com.neo4j.fabric.pipeline;

import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class TryResolveProcedures$ extends AbstractFunction1<ProcedureSignatureResolver,TryResolveProcedures> implements Serializable
{
    public static TryResolveProcedures$ MODULE$;

    static
    {
        new TryResolveProcedures$();
    }

    private TryResolveProcedures$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "TryResolveProcedures";
    }

    public TryResolveProcedures apply( final ProcedureSignatureResolver signatures )
    {
        return new TryResolveProcedures( signatures );
    }

    public Option<ProcedureSignatureResolver> unapply( final TryResolveProcedures x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.signatures() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
