package com.neo4j.fabric.pipeline;

import com.neo4j.fabric.util.Rewritten$;
import com.neo4j.fabric.util.Rewritten$RewritingOps$;
import org.neo4j.cypher.internal.logical.plans.QualifiedName;
import org.neo4j.cypher.internal.logical.plans.ResolvedCall;
import org.neo4j.cypher.internal.logical.plans.ResolvedFunctionInvocation;
import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.cypher.internal.v4_0.ast.AliasedReturnItem;
import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.GraphSelection;
import org.neo4j.cypher.internal.v4_0.ast.Query;
import org.neo4j.cypher.internal.v4_0.ast.QueryPart;
import org.neo4j.cypher.internal.v4_0.ast.Return;
import org.neo4j.cypher.internal.v4_0.ast.ReturnItems;
import org.neo4j.cypher.internal.v4_0.ast.SingleQuery;
import org.neo4j.cypher.internal.v4_0.ast.UnresolvedCall;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.Variable;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.util.Try.;

@JavaDocToJava
public class TryResolveProcedures implements Function1<Object,Object>, Product, Serializable
{
    private final ProcedureSignatureResolver signatures;

    public TryResolveProcedures( final ProcedureSignatureResolver signatures )
    {
        this.signatures = signatures;
        Function1.$init$( this );
        Product.$init$( this );
    }

    public static Option<ProcedureSignatureResolver> unapply( final TryResolveProcedures x$0 )
    {
        return TryResolveProcedures$.MODULE$.unapply( var0 );
    }

    public boolean apply$mcZD$sp( final double v1 )
    {
        return Function1.apply$mcZD$sp$( this, v1 );
    }

    public double apply$mcDD$sp( final double v1 )
    {
        return Function1.apply$mcDD$sp$( this, v1 );
    }

    public float apply$mcFD$sp( final double v1 )
    {
        return Function1.apply$mcFD$sp$( this, v1 );
    }

    public int apply$mcID$sp( final double v1 )
    {
        return Function1.apply$mcID$sp$( this, v1 );
    }

    public long apply$mcJD$sp( final double v1 )
    {
        return Function1.apply$mcJD$sp$( this, v1 );
    }

    public void apply$mcVD$sp( final double v1 )
    {
        Function1.apply$mcVD$sp$( this, v1 );
    }

    public boolean apply$mcZF$sp( final float v1 )
    {
        return Function1.apply$mcZF$sp$( this, v1 );
    }

    public double apply$mcDF$sp( final float v1 )
    {
        return Function1.apply$mcDF$sp$( this, v1 );
    }

    public float apply$mcFF$sp( final float v1 )
    {
        return Function1.apply$mcFF$sp$( this, v1 );
    }

    public int apply$mcIF$sp( final float v1 )
    {
        return Function1.apply$mcIF$sp$( this, v1 );
    }

    public long apply$mcJF$sp( final float v1 )
    {
        return Function1.apply$mcJF$sp$( this, v1 );
    }

    public void apply$mcVF$sp( final float v1 )
    {
        Function1.apply$mcVF$sp$( this, v1 );
    }

    public boolean apply$mcZI$sp( final int v1 )
    {
        return Function1.apply$mcZI$sp$( this, v1 );
    }

    public double apply$mcDI$sp( final int v1 )
    {
        return Function1.apply$mcDI$sp$( this, v1 );
    }

    public float apply$mcFI$sp( final int v1 )
    {
        return Function1.apply$mcFI$sp$( this, v1 );
    }

    public int apply$mcII$sp( final int v1 )
    {
        return Function1.apply$mcII$sp$( this, v1 );
    }

    public long apply$mcJI$sp( final int v1 )
    {
        return Function1.apply$mcJI$sp$( this, v1 );
    }

    public void apply$mcVI$sp( final int v1 )
    {
        Function1.apply$mcVI$sp$( this, v1 );
    }

    public boolean apply$mcZJ$sp( final long v1 )
    {
        return Function1.apply$mcZJ$sp$( this, v1 );
    }

    public double apply$mcDJ$sp( final long v1 )
    {
        return Function1.apply$mcDJ$sp$( this, v1 );
    }

    public float apply$mcFJ$sp( final long v1 )
    {
        return Function1.apply$mcFJ$sp$( this, v1 );
    }

    public int apply$mcIJ$sp( final long v1 )
    {
        return Function1.apply$mcIJ$sp$( this, v1 );
    }

    public long apply$mcJJ$sp( final long v1 )
    {
        return Function1.apply$mcJJ$sp$( this, v1 );
    }

    public void apply$mcVJ$sp( final long v1 )
    {
        Function1.apply$mcVJ$sp$( this, v1 );
    }

    public <A> Function1<A,Object> compose( final Function1<A,Object> g )
    {
        return Function1.compose$( this, g );
    }

    public <A> Function1<Object,A> andThen( final Function1<Object,A> g )
    {
        return Function1.andThen$( this, g );
    }

    public String toString()
    {
        return Function1.toString$( this );
    }

    public ProcedureSignatureResolver signatures()
    {
        return this.signatures;
    }

    public Object apply( final Object input )
    {
        return Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps(
                Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps( input ) ).bottomUp( new Serializable( this )
                {
                    public static final long serialVersionUID = 0L;

                    public
                    {
                        if ( $outer == null )
                        {
                            throw null;
                        }
                        else
                        {
                            this.$outer = $outer;
                        }
                    }

                    public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                    {
                        Object var3;
                        if ( x1 instanceof UnresolvedCall )
                        {
                            UnresolvedCall var5 = (UnresolvedCall) x1;
                            var3 = .MODULE$.apply( () -> {
                            return org.neo4j.cypher.internal.logical.plans.ResolvedCall..MODULE$.apply( ( name ) -> {
                                return this.$outer.signatures().procedureSignature( name );
                            }, var5 );
                        } ).getOrElse( () -> {
                            return var5;
                        } );
                        }
                        else
                        {
                            if ( x1 instanceof FunctionInvocation )
                            {
                                FunctionInvocation var6 = (FunctionInvocation) x1;
                                if ( var6.needsToBeResolved() )
                                {
                                    QualifiedName name = org.neo4j.cypher.internal.logical.plans.QualifiedName..MODULE$.apply( var6 );
                                    var3 = this.$outer.signatures().functionSignature( name ).map( ( sig ) -> {
                                        return new ResolvedFunctionInvocation( name, new Some( sig ), var6.args(), var6.position() );
                                    } ).getOrElse( () -> {
                                        return var6;
                                    } );
                                    return var3;
                                }
                            }

                            var3 = var2.apply( x1 );
                        }

                        return var3;
                    }

                    public final boolean isDefinedAt( final Object x1 )
                    {
                        boolean var2;
                        if ( x1 instanceof UnresolvedCall )
                        {
                            var2 = true;
                        }
                        else
                        {
                            if ( x1 instanceof FunctionInvocation )
                            {
                                FunctionInvocation var4 = (FunctionInvocation) x1;
                                if ( var4.needsToBeResolved() )
                                {
                                    var2 = true;
                                    return var2;
                                }
                            }

                            var2 = false;
                        }

                        return var2;
                    }
                } ) ) ).bottomUp( new Serializable( this )
        {
            public static final long serialVersionUID = 0L;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                }
            }

            public final <A1, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
            {
                boolean var6 = false;
                Query var7 = null;
                Object var3;
                if ( x2 instanceof Query )
                {
                    var6 = true;
                    var7 = (Query) x2;
                    Option var9 = var7.periodicCommitHint();
                    QueryPart partx = var7.part();
                    if ( scala.None..MODULE$.equals( var9 ) && partx instanceof SingleQuery){
                    SingleQuery var11 = (SingleQuery) partx;
                    Seq var12 = var11.clauses();
                    Some var13 = scala.collection.Seq..MODULE$.unapplySeq( var12 );
                    if ( !var13.isEmpty() && var13.get() != null && ((SeqLike) var13.get()).lengthCompare( 1 ) == 0 )
                    {
                        Clause resolvedx = (Clause) ((SeqLike) var13.get()).apply( 0 );
                        if ( resolvedx instanceof ResolvedCall )
                        {
                            ResolvedCall var15 = (ResolvedCall) resolvedx;
                            Tuple2 var17 = this.$outer.com$neo4j$fabric$pipeline$TryResolveProcedures$$getExpandedAndProjection( var15 );
                            if ( var17 == null )
                            {
                                throw new MatchError( var17 );
                            }

                            ResolvedCall expandedxx = (ResolvedCall) var17._1();
                            Return projectionxx = (Return) var17._2();
                            Tuple2 var5 = new Tuple2( expandedxx, projectionxx );
                            ResolvedCall expandedxxx = (ResolvedCall) var5._1();
                            Return projectionxxx = (Return) var5._2();
                            SingleQuery x$3 = var11.copy( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new Clause[]{expandedxxx, projectionxxx}) )),var11.position());
                            Option x$4 = var7.copy$default$1();
                            InputPosition x$5 = var7.position();
                            var3 = var7.copy( x$4, x$3, x$5 );
                            return var3;
                        }
                    }
                }
                }

                if ( var6 )
                {
                    Option var25 = var7.periodicCommitHint();
                    QueryPart part = var7.part();
                    if ( scala.None..MODULE$.equals( var25 ) && part instanceof SingleQuery){
                    SingleQuery var27 = (SingleQuery) part;
                    Seq var28 = var27.clauses();
                    Some var29 = scala.collection.Seq..MODULE$.unapplySeq( var28 );
                    if ( !var29.isEmpty() && var29.get() != null && ((SeqLike) var29.get()).lengthCompare( 2 ) == 0 )
                    {
                        Clause graph = (Clause) ((SeqLike) var29.get()).apply( 0 );
                        Clause resolved = (Clause) ((SeqLike) var29.get()).apply( 1 );
                        if ( graph instanceof GraphSelection )
                        {
                            GraphSelection var32 = (GraphSelection) graph;
                            if ( resolved instanceof ResolvedCall )
                            {
                                ResolvedCall var33 = (ResolvedCall) resolved;
                                Tuple2 var35 = this.$outer.com$neo4j$fabric$pipeline$TryResolveProcedures$$getExpandedAndProjection( var33 );
                                if ( var35 == null )
                                {
                                    throw new MatchError( var35 );
                                }

                                ResolvedCall expanded = (ResolvedCall) var35._1();
                                Return projection = (Return) var35._2();
                                Tuple2 var4 = new Tuple2( expanded, projection );
                                ResolvedCall expandedx = (ResolvedCall) var4._1();
                                Return projectionx = (Return) var4._2();
                                SingleQuery x$6 = var27.copy( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new Clause[]{var32, expandedx, projectionx}) )),var27.position());
                                Option x$7 = var7.copy$default$1();
                                InputPosition x$8 = var7.position();
                                var3 = var7.copy( x$7, x$6, x$8 );
                                return var3;
                            }
                        }
                    }
                }
                }

                var3 = var2.apply( x2 );
                return var3;
            }

            public final boolean isDefinedAt( final Object x2 )
            {
                boolean var3 = false;
                Query var4 = null;
                boolean var2;
                if ( x2 instanceof Query )
                {
                    var3 = true;
                    var4 = (Query) x2;
                    Option var6 = var4.periodicCommitHint();
                    QueryPart partx = var4.part();
                    if ( scala.None..MODULE$.equals( var6 ) && partx instanceof SingleQuery){
                    SingleQuery var8 = (SingleQuery) partx;
                    Seq var9 = var8.clauses();
                    Some var10 = scala.collection.Seq..MODULE$.unapplySeq( var9 );
                    if ( !var10.isEmpty() && var10.get() != null && ((SeqLike) var10.get()).lengthCompare( 1 ) == 0 )
                    {
                        Clause resolved = (Clause) ((SeqLike) var10.get()).apply( 0 );
                        if ( resolved instanceof ResolvedCall )
                        {
                            var2 = true;
                            return var2;
                        }
                    }
                }
                }

                if ( var3 )
                {
                    Option var12 = var4.periodicCommitHint();
                    QueryPart part = var4.part();
                    if ( scala.None..MODULE$.equals( var12 ) && part instanceof SingleQuery){
                    SingleQuery var14 = (SingleQuery) part;
                    Seq var15 = var14.clauses();
                    Some var16 = scala.collection.Seq..MODULE$.unapplySeq( var15 );
                    if ( !var16.isEmpty() && var16.get() != null && ((SeqLike) var16.get()).lengthCompare( 2 ) == 0 )
                    {
                        Clause graph = (Clause) ((SeqLike) var16.get()).apply( 0 );
                        Clause resolvedx = (Clause) ((SeqLike) var16.get()).apply( 1 );
                        if ( graph instanceof GraphSelection && resolvedx instanceof ResolvedCall )
                        {
                            var2 = true;
                            return var2;
                        }
                    }
                }
                }

                var2 = false;
                return var2;
            }
        } );
    }

    public Tuple2<ResolvedCall,Return> com$neo4j$fabric$pipeline$TryResolveProcedures$$getExpandedAndProjection( final ResolvedCall resolved )
    {
        ResolvedCall expanded = resolved.withFakedFullDeclarations();
        IndexedSeq aliases = (IndexedSeq) expanded.callResults().map( ( item ) -> {
            Variable copy1 = new Variable( item.variable().name(), item.variable().position() );
            Variable copy2 = new Variable( item.variable().name(), item.variable().position() );
            return new AliasedReturnItem( copy1, copy2, resolved.position() );
        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
        Return projection = new Return( false, new ReturnItems( false, aliases, resolved.position() ), scala.None..MODULE$, scala.None..MODULE$, scala.None..
        MODULE$, org.neo4j.cypher.internal.v4_0.ast.Return..MODULE$.apply$default$6(), resolved.position());
        return new Tuple2( expanded, projection );
    }

    public TryResolveProcedures copy( final ProcedureSignatureResolver signatures )
    {
        return new TryResolveProcedures( signatures );
    }

    public ProcedureSignatureResolver copy$default$1()
    {
        return this.signatures();
    }

    public String productPrefix()
    {
        return "TryResolveProcedures";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.signatures();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof TryResolveProcedures;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof TryResolveProcedures )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        TryResolveProcedures var4 = (TryResolveProcedures) x$1;
                        ProcedureSignatureResolver var10000 = this.signatures();
                        ProcedureSignatureResolver var5 = var4.signatures();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
