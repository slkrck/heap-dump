package com.neo4j.fabric.eval;

import java.util.function.Supplier;

import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class UseEvaluation$ extends AbstractFunction3<Catalog,Supplier<GlobalProcedures>,ProcedureSignatureResolver,UseEvaluation> implements Serializable
{
    public static UseEvaluation$ MODULE$;

    static
    {
        new UseEvaluation$();
    }

    private UseEvaluation$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "UseEvaluation";
    }

    public UseEvaluation apply( final Catalog catalog, final Supplier<GlobalProcedures> proceduresSupplier, final ProcedureSignatureResolver signatureResolver )
    {
        return new UseEvaluation( catalog, proceduresSupplier, signatureResolver );
    }

    public Option<Tuple3<Catalog,Supplier<GlobalProcedures>,ProcedureSignatureResolver>> unapply( final UseEvaluation x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.catalog(), x$0.proceduresSupplier(), x$0.signatureResolver() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
