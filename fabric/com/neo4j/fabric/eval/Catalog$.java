package com.neo4j.fabric.eval;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.util.Errors$;
import org.neo4j.cypher.internal.v4_0.ast.CatalogName;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.IntegralValue;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.TraversableOnce;
import scala.collection.JavaConverters.;
import scala.collection.immutable.Map;
import scala.collection.mutable.Set;
import scala.runtime.BoxesRunTime;

public final class Catalog$ implements Serializable
{
    public static Catalog$ MODULE$;

    static
    {
        new Catalog$();
    }

    private Catalog$()
    {
        MODULE$ = this;
    }

    public Catalog fromConfig( final FabricConfig config )
    {
        return this.fromDatabase( config.getDatabase() );
    }

    private Catalog fromDatabase( final FabricConfig.Database database )
    {
        String namespace = database.getName().name();
        Set graphs = (Set).MODULE$.asScalaSetConverter( database.getGraphs() ).asScala();
        Catalog byName = new Catalog( ((TraversableOnce) graphs.flatMap( ( graph ) -> {
            return scala.Option..MODULE$.option2Iterable( scala.Option..MODULE$.apply( graph.getName() ).map( ( x$1 ) -> {
                return x$1.name();
            } ).map( ( name ) -> {
                return scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( org.neo4j.cypher.internal.v4_0.ast.CatalogName..MODULE$.apply(
                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{namespace, name}) ))),new Catalog.RemoteGraph( graph ));
            } ));
        }, scala.collection.mutable.Set..MODULE$.canBuildFrom()) ).toMap( scala.Predef..MODULE$.$conforms()));
        Catalog byId = new Catalog( (Map) scala.Predef..MODULE$.Map().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                        org.neo4j.cypher.internal.v4_0.ast.CatalogName..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new String[]{namespace, "graph"}) )) ), new Catalog.View1( new Catalog.Arg( "gid", IntegralValue.class ), ( sid ) -> {
        return new Catalog.RemoteGraph( (FabricConfig.Graph) graphs.find( ( x$2 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$fromDatabase$5( sid, x$2 ) );
        } ).getOrElse( () -> {
            return Errors$.MODULE$.entityNotFound( "Graph", Errors$.MODULE$.show( (AnyValue) sid ) );
        } ) );
    } ))}))));
        return byName.$plus$plus( byId );
    }

    public Catalog apply( final Map<CatalogName,Catalog.Entry> entries )
    {
        return new Catalog( entries );
    }

    public Option<Map<CatalogName,Catalog.Entry>> unapply( final Catalog x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( x$0.entries() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
