package com.neo4j.fabric.eval;

import com.neo4j.fabric.config.FabricConfig;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public class Catalog$RemoteGraph$ extends AbstractFunction1<FabricConfig.Graph,Catalog.RemoteGraph> implements Serializable
{
    public static Catalog$RemoteGraph$ MODULE$;

    static
    {
        new Catalog$RemoteGraph$();
    }

    public Catalog$RemoteGraph$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RemoteGraph";
    }

    public Catalog.RemoteGraph apply( final FabricConfig.Graph graph )
    {
        return new Catalog.RemoteGraph( graph );
    }

    public Option<FabricConfig.Graph> unapply( final Catalog.RemoteGraph x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.graph() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
