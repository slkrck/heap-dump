package com.neo4j.fabric.eval;

import com.neo4j.fabric.util.Errors$;
import com.neo4j.fabric.util.Rewritten$;
import com.neo4j.fabric.util.Rewritten$RewritingOps$;

import java.util.Map;
import java.util.function.Supplier;

import org.neo4j.cypher.internal.logical.plans.ResolvedFunctionInvocation;
import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.cypher.internal.runtime.MapExecutionContext;
import org.neo4j.cypher.internal.v4_0.ast.CatalogName;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.Property;
import org.neo4j.cypher.internal.v4_0.expressions.Variable;
import org.neo4j.cypher.internal.v4_0.util.ASTNode;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.MapValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.TraversableLike;
import scala.collection.JavaConverters.;
import scala.collection.immutable.List;
import scala.collection.immutable..colon.colon;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.NonLocalReturnControl;

@JavaDocToJava
public class UseEvaluation implements Product, Serializable
{
    private final Catalog catalog;
    private final Supplier<GlobalProcedures> proceduresSupplier;
    private final ProcedureSignatureResolver signatureResolver;
    private final StaticEvaluation.StaticEvaluator evaluator;

    public UseEvaluation( final Catalog catalog, final Supplier<GlobalProcedures> proceduresSupplier, final ProcedureSignatureResolver signatureResolver )
    {
        this.catalog = catalog;
        this.proceduresSupplier = proceduresSupplier;
        this.signatureResolver = signatureResolver;
        Product.$init$( this );
        this.evaluator = new StaticEvaluation.StaticEvaluator( proceduresSupplier );
    }

    public static Option<Tuple3<Catalog,Supplier<GlobalProcedures>,ProcedureSignatureResolver>> unapply( final UseEvaluation x$0 )
    {
        return UseEvaluation$.MODULE$.unapply( var0 );
    }

    public static UseEvaluation apply( final Catalog catalog, final Supplier<GlobalProcedures> proceduresSupplier,
            final ProcedureSignatureResolver signatureResolver )
    {
        return UseEvaluation$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Catalog,Supplier<GlobalProcedures>,ProcedureSignatureResolver>,UseEvaluation> tupled()
    {
        return UseEvaluation$.MODULE$.tupled();
    }

    public static Function1<Catalog,Function1<Supplier<GlobalProcedures>,Function1<ProcedureSignatureResolver,UseEvaluation>>> curried()
    {
        return UseEvaluation$.MODULE$.curried();
    }

    private static final List parts$1( final Expression expr )
    {
        Object var1;
        if ( expr instanceof Property )
        {
            Property var3 = (Property) expr;
            var1 = (List) parts$1( var3.map() ).$colon$plus( var3.propertyKey().name(), scala.collection.immutable.List..MODULE$.canBuildFrom());
        }
        else
        {
            if ( !(expr instanceof Variable) )
            {
                throw Errors$.MODULE$.openCypherUnexpected( "Graph name segment", (ASTNode) expr );
            }

            Variable var4 = (Variable) expr;
            String name = var4.name();
            var1 = new colon( name, scala.collection.immutable.Nil..MODULE$);
        }

        return (List) var1;
    }

    public Catalog catalog()
    {
        return this.catalog;
    }

    public Supplier<GlobalProcedures> proceduresSupplier()
    {
        return this.proceduresSupplier;
    }

    public ProcedureSignatureResolver signatureResolver()
    {
        return this.signatureResolver;
    }

    private StaticEvaluation.StaticEvaluator evaluator()
    {
        return this.evaluator;
    }

    public Catalog.Graph evaluate( final String originalStatement, final UseGraph use, final MapValue parameters, final Map<String,AnyValue> context )
    {
        return this.evaluate( originalStatement, use, parameters, (scala.collection.mutable.Map).MODULE$.mapAsScalaMapConverter( context ).asScala() );
    }

    public Catalog.Graph evaluate( final String originalStatement, final UseGraph use, final MapValue parameters,
            final scala.collection.mutable.Map<String,AnyValue> context )
    {
        return (Catalog.Graph) Errors$.MODULE$.errorContext( originalStatement, use, () -> {
            Expression var5 = use.expression();
            Catalog.Graph var4;
            if ( var5 instanceof Variable )
            {
                Variable var6 = (Variable) var5;
                var4 = this.catalog().resolve( this.nameFromVar( var6 ) );
            }
            else if ( var5 instanceof Property )
            {
                Property var7 = (Property) var5;
                var4 = this.catalog().resolve( this.nameFromProp( var7 ) );
            }
            else
            {
                if ( !(var5 instanceof FunctionInvocation) )
                {
                    throw Errors$.MODULE$.openCypherUnexpected( "graph or view reference", (ASTNode) var5 );
                }

                FunctionInvocation var8 = (FunctionInvocation) var5;
                MapExecutionContext ctx = org.neo4j.cypher.internal.runtime.ExecutionContext..MODULE$.apply( context );
                IndexedSeq argValues = (IndexedSeq) ((TraversableLike) var8.args().map( ( expr ) -> {
                    return this.resolveFunctions( expr );
                }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).map( ( expr ) -> {
                return this.evaluator().evaluate( expr, parameters, ctx );
            }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                var4 = this.catalog().resolve( this.nameFromFunc( var8 ), argValues );
            }

            return var4;
        } );
    }

    public Expression resolveFunctions( final Expression expr )
    {
        Object var2 = new Object();

        Expression var10000;
        try
        {
            var10000 = (Expression) Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps( expr ) ).bottomUp(
                    new Serializable( this, var2 )
                    {
                        public static final long serialVersionUID = 0L;
                        private final Object nonLocalReturnKey1$1;

                        public
                        {
                            if ( $outer == null )
                            {
                                throw null;
                            }
                            else
                            {
                                this.$outer = $outer;
                                this.nonLocalReturnKey1$1 = nonLocalReturnKey1$1;
                            }
                        }

                        public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                        {
                            if ( x1 instanceof FunctionInvocation )
                            {
                                FunctionInvocation var5 = (FunctionInvocation) x1;
                                if ( var5.needsToBeResolved() )
                                {
                                    ResolvedFunctionInvocation resolved = org.neo4j.cypher.internal.logical.plans.ResolvedFunctionInvocation..
                                    MODULE$.apply( ( name ) -> {
                                        return this.$outer.signatureResolver().functionSignature( name );
                                    }, var5 ).coerceArguments();
                                    if ( resolved.fcnSignature().isEmpty() )
                                    {
                                        throw Errors$.MODULE$.openCypherFailure( (SemanticErrorDef) Errors$.MODULE$.openCypherSemantic(
                                                (new StringBuilder( 19 )).append( "Unknown function '" ).append( resolved.qualifiedName() ).append(
                                                        "'" ).toString(), resolved ) );
                                    }

                                    throw new NonLocalReturnControl( this.nonLocalReturnKey1$1, resolved );
                                }
                            }

                            Object var3 = var2.apply( x1 );
                            return var3;
                        }

                        public final boolean isDefinedAt( final Object x1 )
                        {
                            boolean var2;
                            if ( x1 instanceof FunctionInvocation )
                            {
                                FunctionInvocation var4 = (FunctionInvocation) x1;
                                if ( var4.needsToBeResolved() )
                                {
                                    var2 = true;
                                    return var2;
                                }
                            }

                            var2 = false;
                            return var2;
                        }
                    } );
        }
        catch ( NonLocalReturnControl var4 )
        {
            if ( var4.key() != var2 )
            {
                throw var4;
            }

            var10000 = (Expression) var4.value();
        }

        return var10000;
    }

    private CatalogName nameFromVar( final Variable variable )
    {
        return org.neo4j.cypher.internal.v4_0.ast.CatalogName..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{variable.name()}) ));
    }

    private CatalogName nameFromProp( final Property property )
    {
        return new CatalogName( parts$1( property ) );
    }

    private CatalogName nameFromFunc( final FunctionInvocation func )
    {
        return new CatalogName(
                (List) func.namespace().parts().$colon$plus( func.functionName().name(), scala.collection.immutable.List..MODULE$.canBuildFrom() ));
    }

    public UseEvaluation copy( final Catalog catalog, final Supplier<GlobalProcedures> proceduresSupplier, final ProcedureSignatureResolver signatureResolver )
    {
        return new UseEvaluation( catalog, proceduresSupplier, signatureResolver );
    }

    public Catalog copy$default$1()
    {
        return this.catalog();
    }

    public Supplier<GlobalProcedures> copy$default$2()
    {
        return this.proceduresSupplier();
    }

    public ProcedureSignatureResolver copy$default$3()
    {
        return this.signatureResolver();
    }

    public String productPrefix()
    {
        return "UseEvaluation";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.catalog();
            break;
        case 1:
            var10000 = this.proceduresSupplier();
            break;
        case 2:
            var10000 = this.signatureResolver();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof UseEvaluation;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof UseEvaluation )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            UseEvaluation var4 = (UseEvaluation) x$1;
                            Catalog var10000 = this.catalog();
                            Catalog var5 = var4.catalog();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            Supplier var8 = this.proceduresSupplier();
                            Supplier var6 = var4.proceduresSupplier();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            ProcedureSignatureResolver var9 = this.signatureResolver();
                            ProcedureSignatureResolver var7 = var4.signatureResolver();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
