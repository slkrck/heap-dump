package com.neo4j.fabric.eval;

import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;

public class Catalog$Arg$ implements Serializable
{
    public static Catalog$Arg$ MODULE$;

    static
    {
        new Catalog$Arg$();
    }

    public Catalog$Arg$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Arg";
    }

    public <T extends AnyValue> Catalog.Arg<T> apply( final String name, final Class<T> tpe )
    {
        return new Catalog.Arg( name, tpe );
    }

    public <T extends AnyValue> Option<Tuple2<String,Class<T>>> unapply( final Catalog.Arg<T> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.name(), x$0.tpe() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
