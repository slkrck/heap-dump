package com.neo4j.fabric.eval;

import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.util.Errors$;
import org.neo4j.configuration.helpers.NormalizedGraphName;
import org.neo4j.cypher.internal.v4_0.ast.CatalogName;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Catalog implements Product, Serializable
{
    private final Map<CatalogName,Catalog.Entry> entries;

    public Catalog( final Map<CatalogName,Catalog.Entry> entries )
    {
        this.entries = entries;
        Product.$init$( this );
    }

    public static Option<Map<CatalogName,Catalog.Entry>> unapply( final Catalog x$0 )
    {
        return Catalog$.MODULE$.unapply( var0 );
    }

    public static Catalog apply( final Map<CatalogName,Catalog.Entry> entries )
    {
        return Catalog$.MODULE$.apply( var0 );
    }

    public static Catalog fromConfig( final FabricConfig config )
    {
        return Catalog$.MODULE$.fromConfig( var0 );
    }

    public Map<CatalogName,Catalog.Entry> entries()
    {
        return this.entries;
    }

    public Catalog.Graph resolve( final CatalogName name )
    {
        return this.resolve( name, (Seq) scala.collection.Seq..MODULE$.apply( scala.collection.immutable.Nil..MODULE$));
    }

    public Catalog.Graph resolve( final CatalogName name, final Seq<AnyValue> args )
    {
        CatalogName normalizedName = new CatalogName( (List) name.parts().map( ( namex ) -> {
            return this.normalize( namex );
        }, scala.collection.immutable.List..MODULE$.canBuildFrom() ));
        boolean var5 = false;
        Some var6 = null;
        Option var7 = this.entries().get( normalizedName );
        if ( scala.None..MODULE$.equals( var7 )){
        throw Errors$.MODULE$.entityNotFound( "Catalog entry", Errors$.MODULE$.show( name ) );
    } else{
        Catalog.Graph var3;
        if ( var7 instanceof Some )
        {
            var5 = true;
            var6 = (Some) var7;
            Catalog.Entry g = (Catalog.Entry) var6.value();
            if ( g instanceof Catalog.Graph )
            {
                Catalog.Graph var9 = (Catalog.Graph) g;
                if ( args.nonEmpty() )
                {
                    throw Errors$.MODULE$.wrongArity( 0, args.size(), org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE());
                }

                var3 = var9;
                return var3;
            }
        }

        if ( !var5 )
        {
            throw new MatchError( var7 );
        }
        else
        {
            Catalog.Entry v = (Catalog.Entry) var6.value();
            if ( !(v instanceof Catalog.View) )
            {
                throw new MatchError( var7 );
            }
            else
            {
                Catalog.View var11 = (Catalog.View) v;
                var3 = var11.eval( args );
                return var3;
            }
        }
    }
    }

    private String normalize( final String name )
    {
        return (new NormalizedGraphName( name )).name();
    }

    public Catalog $plus$plus( final Catalog that )
    {
        return new Catalog( this.entries().$plus$plus( that.entries() ) );
    }

    public Catalog copy( final Map<CatalogName,Catalog.Entry> entries )
    {
        return new Catalog( entries );
    }

    public Map<CatalogName,Catalog.Entry> copy$default$1()
    {
        return this.entries();
    }

    public String productPrefix()
    {
        return "Catalog";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.entries();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Catalog;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof Catalog )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        Catalog var4 = (Catalog) x$1;
                        Map var10000 = this.entries();
                        Map var5 = var4.entries();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }

    public interface Entry
    {
    }

    public interface Graph extends Catalog.Entry
    {
    }

    public interface View extends Catalog.Entry
    {
        static void $init$( final Catalog.View $this )
        {
        }

        int arity();

        Seq<Catalog.Arg<?>> signature();

        Catalog.Graph eval( final Seq<AnyValue> args );

        default void checkArity( final Seq<AnyValue> args )
        {
            if ( args.size() != this.arity() )
            {
                throw Errors$.MODULE$.wrongArity( this.arity(), args.size(), org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE());
            }
        }

        default <T extends AnyValue> T cast( final Catalog.Arg<T> a, final AnyValue v, final Seq<AnyValue> args )
        {
            try
            {
                return (AnyValue) a.tpe().cast( v );
            }
            catch ( ClassCastException var4 )
            {
                throw Errors$.MODULE$.wrongType( Errors$.MODULE$.show( this.signature() ), Errors$.MODULE$.show( args ) );
            }
        }
    }

    public static class Arg<T extends AnyValue> implements Product, Serializable
    {
        private final String name;
        private final Class<T> tpe;

        public Arg( final String name, final Class<T> tpe )
        {
            this.name = name;
            this.tpe = tpe;
            Product.$init$( this );
        }

        public String name()
        {
            return this.name;
        }

        public Class<T> tpe()
        {
            return this.tpe;
        }

        public <T extends AnyValue> Catalog.Arg<T> copy( final String name, final Class<T> tpe )
        {
            return new Catalog.Arg( name, tpe );
        }

        public <T extends AnyValue> String copy$default$1()
        {
            return this.name();
        }

        public <T extends AnyValue> Class<T> copy$default$2()
        {
            return this.tpe();
        }

        public String productPrefix()
        {
            return "Arg";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.name();
                break;
            case 1:
                var10000 = this.tpe();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Catalog.Arg;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof Catalog.Arg )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                Catalog.Arg var4 = (Catalog.Arg) x$1;
                                String var10000 = this.name();
                                String var5 = var4.name();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                Class var7 = this.tpe();
                                Class var6 = var4.tpe();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class RemoteGraph implements Catalog.Graph, Product, Serializable
    {
        private final FabricConfig.Graph graph;

        public RemoteGraph( final FabricConfig.Graph graph )
        {
            this.graph = graph;
            Product.$init$( this );
        }

        public FabricConfig.Graph graph()
        {
            return this.graph;
        }

        public Catalog.RemoteGraph copy( final FabricConfig.Graph graph )
        {
            return new Catalog.RemoteGraph( graph );
        }

        public FabricConfig.Graph copy$default$1()
        {
            return this.graph();
        }

        public String productPrefix()
        {
            return "RemoteGraph";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.graph();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Catalog.RemoteGraph;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            label47:
            {
                if ( this != x$1 )
                {
                    boolean var2;
                    if ( x$1 instanceof Catalog.RemoteGraph )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( !var2 )
                    {
                        break label47;
                    }

                    label35:
                    {
                        label34:
                        {
                            Catalog.RemoteGraph var4 = (Catalog.RemoteGraph) x$1;
                            FabricConfig.Graph var10000 = this.graph();
                            FabricConfig.Graph var5 = var4.graph();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label34;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label34;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label35;
                            }
                        }

                        var6 = false;
                    }

                    if ( !var6 )
                    {
                        break label47;
                    }
                }

                var6 = true;
                return var6;
            }

            var6 = false;
            return var6;
        }
    }

    public static class View1<A1 extends AnyValue> implements Catalog.View, Product, Serializable
    {
        private final Catalog.Arg<A1> a1;
        private final Function1<A1,Catalog.Graph> f;
        private final int arity;
        private final Seq<Catalog.Arg<A1>> signature;

        public View1( final Catalog.Arg<A1> a1, final Function1<A1,Catalog.Graph> f )
        {
            this.a1 = a1;
            this.f = f;
            Catalog.View.$init$( this );
            Product.$init$( this );
            this.arity = 1;
            this.signature = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Catalog.Arg[]{a1}) ));
        }

        public void checkArity( final Seq<AnyValue> args )
        {
            Catalog.View.super.checkArity( args );
        }

        public <T extends AnyValue> T cast( final Catalog.Arg<T> a, final AnyValue v, final Seq<AnyValue> args )
        {
            return Catalog.View.super.cast( a, v, args );
        }

        public Catalog.Arg<A1> a1()
        {
            return this.a1;
        }

        public int arity()
        {
            return this.arity;
        }

        public Seq<Catalog.Arg<A1>> signature()
        {
            return this.signature;
        }

        public Catalog.Graph eval( final Seq<AnyValue> args )
        {
            this.checkArity( args );
            return (Catalog.Graph) this.f.apply( this.cast( this.a1(), (AnyValue) args.apply( 0 ), args ) );
        }

        public <A1 extends AnyValue> Catalog.View1<A1> copy( final Catalog.Arg<A1> a1, final Function1<A1,Catalog.Graph> f )
        {
            return new Catalog.View1( a1, f );
        }

        public <A1 extends AnyValue> Catalog.Arg<A1> copy$default$1()
        {
            return this.a1();
        }

        public String productPrefix()
        {
            return "View1";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.a1();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Catalog.View1;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            label47:
            {
                if ( this != x$1 )
                {
                    boolean var2;
                    if ( x$1 instanceof Catalog.View1 )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( !var2 )
                    {
                        break label47;
                    }

                    label35:
                    {
                        label34:
                        {
                            Catalog.View1 var4 = (Catalog.View1) x$1;
                            Catalog.Arg var10000 = this.a1();
                            Catalog.Arg var5 = var4.a1();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label34;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label34;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label35;
                            }
                        }

                        var6 = false;
                    }

                    if ( !var6 )
                    {
                        break label47;
                    }
                }

                var6 = true;
                return var6;
            }

            var6 = false;
            return var6;
        }
    }
}
