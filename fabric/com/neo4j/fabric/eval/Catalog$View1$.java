package com.neo4j.fabric.eval;

import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;

public class Catalog$View1$ implements Serializable
{
    public static Catalog$View1$ MODULE$;

    static
    {
        new Catalog$View1$();
    }

    public Catalog$View1$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "View1";
    }

    public <A1 extends AnyValue> Catalog.View1<A1> apply( final Catalog.Arg<A1> a1, final Function1<A1,Catalog.Graph> f )
    {
        return new Catalog.View1( a1, f );
    }

    public <A1 extends AnyValue> Option<Catalog.Arg<A1>> unapply( final Catalog.View1<A1> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.a1() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
