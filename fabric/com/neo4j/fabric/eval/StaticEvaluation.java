package com.neo4j.fabric.eval;

import java.net.URL;
import java.time.Clock;
import java.util.Optional;
import java.util.function.Supplier;

import org.eclipse.collections.api.iterator.LongIterator;
import org.neo4j.common.DependencyResolver;
import org.neo4j.cypher.internal.evaluator.EvaluationException;
import org.neo4j.cypher.internal.evaluator.SimpleInternalExpressionEvaluator;
import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.Expander;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.KernelPredicate;
import org.neo4j.cypher.internal.runtime.NodeOperations;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.QueryStatistics;
import org.neo4j.cypher.internal.runtime.QueryTransactionalContext;
import org.neo4j.cypher.internal.runtime.RelationshipIterator;
import org.neo4j.cypher.internal.runtime.RelationshipOperations;
import org.neo4j.cypher.internal.runtime.ResourceManager;
import org.neo4j.cypher.internal.runtime.UserDefinedAggregator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.graphdb.Entity;
import org.neo4j.graphdb.Path;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.IndexQuery.ExactPredicate;
import org.neo4j.internal.kernel.api.procs.ProcedureCallContext;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.internal.schema.IndexDescriptor;
import org.neo4j.kernel.api.exceptions.Status.HasStatus;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.impl.core.TransactionalEntityFactory;
import org.neo4j.kernel.impl.coreapi.InternalTransaction;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.values.AnyValue;
import org.neo4j.values.ValueMapper;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import scala.Option;
import scala.None.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.util.Either;

@JavaDocToJava
public final class StaticEvaluation
{
    public interface EmptyProcedureContext extends Context
    {
        static void $init$( final StaticEvaluation.EmptyProcedureContext $this )
        {
        }

        default ProcedureCallContext procedureCallContext()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default ValueMapper<Object> valueMapper()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default SecurityContext securityContext()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default DependencyResolver dependencyResolver()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default GraphDatabaseAPI graphDatabaseAPI()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Thread thread()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Clock systemClock()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Clock statementClock()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Clock transactionClock()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default InternalTransaction internalTransaction()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default InternalTransaction internalTransactionOrNull()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }
    }

    public interface EmptyQueryContext extends QueryContext
    {
        static void $init$( final StaticEvaluation.EmptyQueryContext $this )
        {
        }

        default TransactionalEntityFactory entityAccessor()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default QueryTransactionalContext transactionalContext()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default ResourceManager resources()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default NodeOperations nodeOps()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default RelationshipOperations relationshipOps()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default NodeValue createNode( final int[] labels )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default long createNodeId( final int[] labels )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default RelationshipValue createRelationship( final long start, final long end, final int relType )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int getOrCreateRelTypeId( final String relTypeName )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<RelationshipValue> getRelationshipsForIds( final long node, final SemanticDirection dir, final int[] types )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default RelationshipIterator getRelationshipsForIdsPrimitive( final long node, final SemanticDirection dir, final int[] types )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default RelationshipValue relationshipById( final long id, final long startNode, final long endNode, final int type )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int getOrCreateLabelId( final String labelName )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int setLabelsOnNode( final long node, final Iterator<Object> labelIds )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int removeLabelsFromNode( final long node, final Iterator<Object> labelIds )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int getOrCreatePropertyKeyId( final String propertyKey )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int[] getOrCreatePropertyKeyIds( final String[] propertyKeys )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default IndexDescriptor addIndexRule( final int labelId, final Seq<Object> propertyKeyIds, final Option<String> name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void dropIndexRule( final int labelId, final Seq<Object> propertyKeyIds )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void dropIndexRule( final String name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default IndexDescriptor indexReference( final int label, final Seq<Object> properties )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default <RESULT> NodeValueIndexCursor indexSeek( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder,
                final Seq<IndexQuery> queries )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default <RESULT> NodeValueIndexCursor indexSeekByContains( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder,
                final TextValue value )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default <RESULT> NodeValueIndexCursor indexSeekByEndsWith( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder,
                final TextValue value )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default <RESULT> NodeValueIndexCursor indexScan( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default <RESULT> NodeValueIndexCursor lockingUniqueIndexSeek( final IndexDescriptor index, final Seq<ExactPredicate> queries )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<NodeValue> getNodesByLabel( final int id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default LongIterator getNodesByLabelPrimitive( final int id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void createNodeKeyConstraint( final int labelId, final Seq<Object> propertyKeyIds, final Option<String> name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void dropNodeKeyConstraint( final int labelId, final Seq<Object> propertyKeyIds )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void createUniqueConstraint( final int labelId, final Seq<Object> propertyKeyIds, final Option<String> name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void dropUniqueConstraint( final int labelId, final Seq<Object> propertyKeyIds )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void createNodePropertyExistenceConstraint( final int labelId, final int propertyKeyId, final Option<String> name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void dropNodePropertyExistenceConstraint( final int labelId, final int propertyKeyId )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void createRelationshipPropertyExistenceConstraint( final int relTypeId, final int propertyKeyId, final Option<String> name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void dropRelationshipPropertyExistenceConstraint( final int relTypeId, final int propertyKeyId )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void dropNamedConstraint( final String name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Either<String,URL> getImportURL( final URL url )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default boolean nodeIsDense( final long node, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Object asObject( final AnyValue value )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<Path> variableLengthPathExpand( final long realNode, final Option<Object> minHops, final Option<Object> maxHops,
                final SemanticDirection direction, final Seq<String> relTypes )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Option<Path> singleShortestPath( final long left, final long right, final int depth, final Expander expander,
                final KernelPredicate<Path> pathPredicate, final Seq<KernelPredicate<Entity>> filters )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<Path> allShortestPath( final long left, final long right, final int depth, final Expander expander,
                final KernelPredicate<Path> pathPredicate, final Seq<KernelPredicate<Entity>> filters )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default long nodeCountByCountStore( final int labelId )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default long relationshipCountByCountStore( final int startLabelId, final int typeId, final int endLabelId )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void lockNodes( final Seq<Object> nodeIds )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void lockRelationships( final Seq<Object> relIds )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<AnyValue[]> callReadOnlyProcedure( final int id, final Seq<AnyValue> args, final String[] allowed, final ProcedureCallContext context )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<AnyValue[]> callReadWriteProcedure( final int id, final Seq<AnyValue> args, final String[] allowed,
                final ProcedureCallContext context )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<AnyValue[]> callSchemaWriteProcedure( final int id, final Seq<AnyValue> args, final String[] allowed,
                final ProcedureCallContext context )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Iterator<AnyValue[]> callDbmsProcedure( final int id, final Seq<AnyValue> args, final String[] allowed, final ProcedureCallContext context )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default UserDefinedAggregator aggregateFunction( final int id, final String[] allowed )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int detachDeleteNode( final long id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void assertSchemaWritesAllowed()
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default String getLabelName( final int id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Option<Object> getOptLabelId( final String labelName )
        {
            return .MODULE$;
        }

        default int getLabelId( final String labelName )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default String getPropertyKeyName( final int id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Option<Object> getOptPropertyKeyId( final String propertyKeyName )
        {
            return .MODULE$;
        }

        default int getPropertyKeyId( final String propertyKeyName )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default String getRelTypeName( final int id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Option<Object> getOptRelTypeId( final String relType )
        {
            return .MODULE$;
        }

        default int getRelTypeId( final String relType )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default NodeValue nodeById( final long id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default RelationshipValue relationshipById( final long id )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int[] nodePropertyIds( final long node, final NodeCursor nodeCursor, final PropertyCursor propertyCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int propertyKey( final String name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int nodeLabel( final String name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int relationshipType( final String name )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default TextValue getTypeForRelationship( final long id, final RelationshipScanCursor relationshipCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default boolean nodeHasProperty( final long node, final int property, final NodeCursor nodeCursor, final PropertyCursor propertyCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int[] relationshipPropertyIds( final long node, final RelationshipScanCursor relationshipScanCursor, final PropertyCursor propertyCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default boolean relationshipHasProperty( final long node, final int property, final RelationshipScanCursor relationshipScanCursor,
                final PropertyCursor propertyCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int nodeGetOutgoingDegree( final long node, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int nodeGetOutgoingDegree( final long node, final int relationship, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int nodeGetIncomingDegree( final long node, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int nodeGetIncomingDegree( final long node, final int relationship, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int nodeGetTotalDegree( final long node, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default int nodeGetTotalDegree( final long node, final int relationship, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default void singleRelationship( final long id, final RelationshipScanCursor cursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default ListValue getLabelsForNode( final long id, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default boolean isLabelSetOnNode( final int label, final long id, final NodeCursor nodeCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default MapValue nodeAsMap( final long id, final NodeCursor nodeCursor, final PropertyCursor propertyCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default MapValue relationshipAsMap( final long id, final RelationshipScanCursor relationshipCursor, final PropertyCursor propertyCursor )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default AnyValue callFunction( final int id, final AnyValue[] args, final String[] allowed )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Value getTxStateNodePropertyOrNull( final long nodeId, final int propertyKey )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }

        default Value getTxStateRelationshipPropertyOrNull( final long relId, final int propertyKey )
        {
            throw StaticEvaluation$.MODULE$.com$neo4j$fabric$eval$StaticEvaluation$$notAvailable();
        }
    }

    public static class StaticEvaluator extends SimpleInternalExpressionEvaluator
    {
        private final Supplier<GlobalProcedures> proceduresSupplier;

        public StaticEvaluator( final Supplier<GlobalProcedures> proceduresSupplier )
        {
            this.proceduresSupplier = proceduresSupplier;
        }

        public QueryState queryState( final int nExpressionSlots, final AnyValue[] slottedParams )
        {
            return new QueryState( new StaticEvaluation.StaticQueryContext( (GlobalProcedures) this.proceduresSupplier.get() ), (ExternalCSVResource) null,
                    slottedParams, (ExpressionCursors) null, (IndexReadSession[]) scala.Array..MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply(
                    IndexReadSession.class )),
            new AnyValue[nExpressionSlots], QuerySubscriber.DO_NOTHING_SUBSCRIBER, (QueryMemoryTracker) null, org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState..
            MODULE$.$lessinit$greater$default$9(), org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState..
            MODULE$.$lessinit$greater$default$10(), org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState..
            MODULE$.$lessinit$greater$default$11(), org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState..
            MODULE$.$lessinit$greater$default$12(), org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState..
            MODULE$.$lessinit$greater$default$13(), org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState..MODULE$.$lessinit$greater$default$14());
        }

        public AnyValue evaluate( final Expression expression, final MapValue params, final ExecutionContext context )
        {
            try
            {
                return super.evaluate( expression, params, context );
            }
            catch ( EvaluationException var6 )
            {
                Object unwrapped;
                for ( unwrapped = var6; unwrapped instanceof EvaluationException; unwrapped = ((Throwable) unwrapped).getCause() )
                {
                }

                if ( unwrapped != null && unwrapped instanceof HasStatus )
                {
                    throw unwrapped;
                }
                else
                {
                    throw var6;
                }
            }
        }
    }

    public static class StaticProcedureContext implements StaticEvaluation.EmptyProcedureContext
    {
        public StaticProcedureContext()
        {
            StaticEvaluation.EmptyProcedureContext.$init$( this );
        }

        public ProcedureCallContext procedureCallContext()
        {
            return StaticEvaluation.EmptyProcedureContext.super.procedureCallContext();
        }

        public ValueMapper<Object> valueMapper()
        {
            return StaticEvaluation.EmptyProcedureContext.super.valueMapper();
        }

        public SecurityContext securityContext()
        {
            return StaticEvaluation.EmptyProcedureContext.super.securityContext();
        }

        public DependencyResolver dependencyResolver()
        {
            return StaticEvaluation.EmptyProcedureContext.super.dependencyResolver();
        }

        public GraphDatabaseAPI graphDatabaseAPI()
        {
            return StaticEvaluation.EmptyProcedureContext.super.graphDatabaseAPI();
        }

        public Thread thread()
        {
            return StaticEvaluation.EmptyProcedureContext.super.thread();
        }

        public Clock systemClock()
        {
            return StaticEvaluation.EmptyProcedureContext.super.systemClock();
        }

        public Clock statementClock()
        {
            return StaticEvaluation.EmptyProcedureContext.super.statementClock();
        }

        public Clock transactionClock()
        {
            return StaticEvaluation.EmptyProcedureContext.super.transactionClock();
        }

        public InternalTransaction internalTransaction()
        {
            return StaticEvaluation.EmptyProcedureContext.super.internalTransaction();
        }

        public InternalTransaction internalTransactionOrNull()
        {
            return StaticEvaluation.EmptyProcedureContext.super.internalTransactionOrNull();
        }
    }

    public static class StaticQueryContext implements StaticEvaluation.EmptyQueryContext
    {
        private final GlobalProcedures procedures;

        public StaticQueryContext( final GlobalProcedures procedures )
        {
            this.procedures = procedures;
            QueryContext.$init$( this );
            StaticEvaluation.EmptyQueryContext.$init$( this );
        }

        public TransactionalEntityFactory entityAccessor()
        {
            return StaticEvaluation.EmptyQueryContext.super.entityAccessor();
        }

        public QueryTransactionalContext transactionalContext()
        {
            return StaticEvaluation.EmptyQueryContext.super.transactionalContext();
        }

        public ResourceManager resources()
        {
            return StaticEvaluation.EmptyQueryContext.super.resources();
        }

        public NodeOperations nodeOps()
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeOps();
        }

        public RelationshipOperations relationshipOps()
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipOps();
        }

        public NodeValue createNode( final int[] labels )
        {
            return StaticEvaluation.EmptyQueryContext.super.createNode( labels );
        }

        public long createNodeId( final int[] labels )
        {
            return StaticEvaluation.EmptyQueryContext.super.createNodeId( labels );
        }

        public RelationshipValue createRelationship( final long start, final long end, final int relType )
        {
            return StaticEvaluation.EmptyQueryContext.super.createRelationship( start, end, relType );
        }

        public int getOrCreateRelTypeId( final String relTypeName )
        {
            return StaticEvaluation.EmptyQueryContext.super.getOrCreateRelTypeId( relTypeName );
        }

        public Iterator<RelationshipValue> getRelationshipsForIds( final long node, final SemanticDirection dir, final int[] types )
        {
            return StaticEvaluation.EmptyQueryContext.super.getRelationshipsForIds( node, dir, types );
        }

        public RelationshipIterator getRelationshipsForIdsPrimitive( final long node, final SemanticDirection dir, final int[] types )
        {
            return StaticEvaluation.EmptyQueryContext.super.getRelationshipsForIdsPrimitive( node, dir, types );
        }

        public RelationshipValue relationshipById( final long id, final long startNode, final long endNode, final int type )
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipById( id, startNode, endNode, type );
        }

        public int getOrCreateLabelId( final String labelName )
        {
            return StaticEvaluation.EmptyQueryContext.super.getOrCreateLabelId( labelName );
        }

        public int setLabelsOnNode( final long node, final Iterator<Object> labelIds )
        {
            return StaticEvaluation.EmptyQueryContext.super.setLabelsOnNode( node, labelIds );
        }

        public int removeLabelsFromNode( final long node, final Iterator<Object> labelIds )
        {
            return StaticEvaluation.EmptyQueryContext.super.removeLabelsFromNode( node, labelIds );
        }

        public int getOrCreatePropertyKeyId( final String propertyKey )
        {
            return StaticEvaluation.EmptyQueryContext.super.getOrCreatePropertyKeyId( propertyKey );
        }

        public int[] getOrCreatePropertyKeyIds( final String[] propertyKeys )
        {
            return StaticEvaluation.EmptyQueryContext.super.getOrCreatePropertyKeyIds( propertyKeys );
        }

        public IndexDescriptor addIndexRule( final int labelId, final Seq<Object> propertyKeyIds, final Option<String> name )
        {
            return StaticEvaluation.EmptyQueryContext.super.addIndexRule( labelId, propertyKeyIds, name );
        }

        public void dropIndexRule( final int labelId, final Seq<Object> propertyKeyIds )
        {
            StaticEvaluation.EmptyQueryContext.super.dropIndexRule( labelId, propertyKeyIds );
        }

        public void dropIndexRule( final String name )
        {
            StaticEvaluation.EmptyQueryContext.super.dropIndexRule( name );
        }

        public IndexDescriptor indexReference( final int label, final Seq<Object> properties )
        {
            return StaticEvaluation.EmptyQueryContext.super.indexReference( label, properties );
        }

        public <RESULT> NodeValueIndexCursor indexSeek( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder,
                final Seq<IndexQuery> queries )
        {
            return StaticEvaluation.EmptyQueryContext.super.indexSeek( index, needsValues, indexOrder, queries );
        }

        public <RESULT> NodeValueIndexCursor indexSeekByContains( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder,
                final TextValue value )
        {
            return StaticEvaluation.EmptyQueryContext.super.indexSeekByContains( index, needsValues, indexOrder, value );
        }

        public <RESULT> NodeValueIndexCursor indexSeekByEndsWith( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder,
                final TextValue value )
        {
            return StaticEvaluation.EmptyQueryContext.super.indexSeekByEndsWith( index, needsValues, indexOrder, value );
        }

        public <RESULT> NodeValueIndexCursor indexScan( final IndexReadSession index, final boolean needsValues, final IndexOrder indexOrder )
        {
            return StaticEvaluation.EmptyQueryContext.super.indexScan( index, needsValues, indexOrder );
        }

        public <RESULT> NodeValueIndexCursor lockingUniqueIndexSeek( final IndexDescriptor index, final Seq<ExactPredicate> queries )
        {
            return StaticEvaluation.EmptyQueryContext.super.lockingUniqueIndexSeek( index, queries );
        }

        public Iterator<NodeValue> getNodesByLabel( final int id )
        {
            return StaticEvaluation.EmptyQueryContext.super.getNodesByLabel( id );
        }

        public LongIterator getNodesByLabelPrimitive( final int id )
        {
            return StaticEvaluation.EmptyQueryContext.super.getNodesByLabelPrimitive( id );
        }

        public void createNodeKeyConstraint( final int labelId, final Seq<Object> propertyKeyIds, final Option<String> name )
        {
            StaticEvaluation.EmptyQueryContext.super.createNodeKeyConstraint( labelId, propertyKeyIds, name );
        }

        public void dropNodeKeyConstraint( final int labelId, final Seq<Object> propertyKeyIds )
        {
            StaticEvaluation.EmptyQueryContext.super.dropNodeKeyConstraint( labelId, propertyKeyIds );
        }

        public void createUniqueConstraint( final int labelId, final Seq<Object> propertyKeyIds, final Option<String> name )
        {
            StaticEvaluation.EmptyQueryContext.super.createUniqueConstraint( labelId, propertyKeyIds, name );
        }

        public void dropUniqueConstraint( final int labelId, final Seq<Object> propertyKeyIds )
        {
            StaticEvaluation.EmptyQueryContext.super.dropUniqueConstraint( labelId, propertyKeyIds );
        }

        public void createNodePropertyExistenceConstraint( final int labelId, final int propertyKeyId, final Option<String> name )
        {
            StaticEvaluation.EmptyQueryContext.super.createNodePropertyExistenceConstraint( labelId, propertyKeyId, name );
        }

        public void dropNodePropertyExistenceConstraint( final int labelId, final int propertyKeyId )
        {
            StaticEvaluation.EmptyQueryContext.super.dropNodePropertyExistenceConstraint( labelId, propertyKeyId );
        }

        public void createRelationshipPropertyExistenceConstraint( final int relTypeId, final int propertyKeyId, final Option<String> name )
        {
            StaticEvaluation.EmptyQueryContext.super.createRelationshipPropertyExistenceConstraint( relTypeId, propertyKeyId, name );
        }

        public void dropRelationshipPropertyExistenceConstraint( final int relTypeId, final int propertyKeyId )
        {
            StaticEvaluation.EmptyQueryContext.super.dropRelationshipPropertyExistenceConstraint( relTypeId, propertyKeyId );
        }

        public void dropNamedConstraint( final String name )
        {
            StaticEvaluation.EmptyQueryContext.super.dropNamedConstraint( name );
        }

        public Either<String,URL> getImportURL( final URL url )
        {
            return StaticEvaluation.EmptyQueryContext.super.getImportURL( url );
        }

        public boolean nodeIsDense( final long node, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeIsDense( node, nodeCursor );
        }

        public Object asObject( final AnyValue value )
        {
            return StaticEvaluation.EmptyQueryContext.super.asObject( value );
        }

        public Iterator<Path> variableLengthPathExpand( final long realNode, final Option<Object> minHops, final Option<Object> maxHops,
                final SemanticDirection direction, final Seq<String> relTypes )
        {
            return StaticEvaluation.EmptyQueryContext.super.variableLengthPathExpand( realNode, minHops, maxHops, direction, relTypes );
        }

        public Option<Path> singleShortestPath( final long left, final long right, final int depth, final Expander expander,
                final KernelPredicate<Path> pathPredicate, final Seq<KernelPredicate<Entity>> filters )
        {
            return StaticEvaluation.EmptyQueryContext.super.singleShortestPath( left, right, depth, expander, pathPredicate, filters );
        }

        public Iterator<Path> allShortestPath( final long left, final long right, final int depth, final Expander expander,
                final KernelPredicate<Path> pathPredicate, final Seq<KernelPredicate<Entity>> filters )
        {
            return StaticEvaluation.EmptyQueryContext.super.allShortestPath( left, right, depth, expander, pathPredicate, filters );
        }

        public long nodeCountByCountStore( final int labelId )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeCountByCountStore( labelId );
        }

        public long relationshipCountByCountStore( final int startLabelId, final int typeId, final int endLabelId )
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipCountByCountStore( startLabelId, typeId, endLabelId );
        }

        public void lockNodes( final Seq<Object> nodeIds )
        {
            StaticEvaluation.EmptyQueryContext.super.lockNodes( nodeIds );
        }

        public void lockRelationships( final Seq<Object> relIds )
        {
            StaticEvaluation.EmptyQueryContext.super.lockRelationships( relIds );
        }

        public Iterator<AnyValue[]> callReadOnlyProcedure( final int id, final Seq<AnyValue> args, final String[] allowed, final ProcedureCallContext context )
        {
            return StaticEvaluation.EmptyQueryContext.super.callReadOnlyProcedure( id, args, allowed, context );
        }

        public Iterator<AnyValue[]> callReadWriteProcedure( final int id, final Seq<AnyValue> args, final String[] allowed, final ProcedureCallContext context )
        {
            return StaticEvaluation.EmptyQueryContext.super.callReadWriteProcedure( id, args, allowed, context );
        }

        public Iterator<AnyValue[]> callSchemaWriteProcedure( final int id, final Seq<AnyValue> args, final String[] allowed,
                final ProcedureCallContext context )
        {
            return StaticEvaluation.EmptyQueryContext.super.callSchemaWriteProcedure( id, args, allowed, context );
        }

        public Iterator<AnyValue[]> callDbmsProcedure( final int id, final Seq<AnyValue> args, final String[] allowed, final ProcedureCallContext context )
        {
            return StaticEvaluation.EmptyQueryContext.super.callDbmsProcedure( id, args, allowed, context );
        }

        public UserDefinedAggregator aggregateFunction( final int id, final String[] allowed )
        {
            return StaticEvaluation.EmptyQueryContext.super.aggregateFunction( id, allowed );
        }

        public int detachDeleteNode( final long id )
        {
            return StaticEvaluation.EmptyQueryContext.super.detachDeleteNode( id );
        }

        public void assertSchemaWritesAllowed()
        {
            StaticEvaluation.EmptyQueryContext.super.assertSchemaWritesAllowed();
        }

        public String getLabelName( final int id )
        {
            return StaticEvaluation.EmptyQueryContext.super.getLabelName( id );
        }

        public Option<Object> getOptLabelId( final String labelName )
        {
            return StaticEvaluation.EmptyQueryContext.super.getOptLabelId( labelName );
        }

        public int getLabelId( final String labelName )
        {
            return StaticEvaluation.EmptyQueryContext.super.getLabelId( labelName );
        }

        public String getPropertyKeyName( final int id )
        {
            return StaticEvaluation.EmptyQueryContext.super.getPropertyKeyName( id );
        }

        public Option<Object> getOptPropertyKeyId( final String propertyKeyName )
        {
            return StaticEvaluation.EmptyQueryContext.super.getOptPropertyKeyId( propertyKeyName );
        }

        public int getPropertyKeyId( final String propertyKeyName )
        {
            return StaticEvaluation.EmptyQueryContext.super.getPropertyKeyId( propertyKeyName );
        }

        public String getRelTypeName( final int id )
        {
            return StaticEvaluation.EmptyQueryContext.super.getRelTypeName( id );
        }

        public Option<Object> getOptRelTypeId( final String relType )
        {
            return StaticEvaluation.EmptyQueryContext.super.getOptRelTypeId( relType );
        }

        public int getRelTypeId( final String relType )
        {
            return StaticEvaluation.EmptyQueryContext.super.getRelTypeId( relType );
        }

        public NodeValue nodeById( final long id )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeById( id );
        }

        public RelationshipValue relationshipById( final long id )
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipById( id );
        }

        public int[] nodePropertyIds( final long node, final NodeCursor nodeCursor, final PropertyCursor propertyCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodePropertyIds( node, nodeCursor, propertyCursor );
        }

        public int propertyKey( final String name )
        {
            return StaticEvaluation.EmptyQueryContext.super.propertyKey( name );
        }

        public int nodeLabel( final String name )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeLabel( name );
        }

        public int relationshipType( final String name )
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipType( name );
        }

        public TextValue getTypeForRelationship( final long id, final RelationshipScanCursor relationshipCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.getTypeForRelationship( id, relationshipCursor );
        }

        public boolean nodeHasProperty( final long node, final int property, final NodeCursor nodeCursor, final PropertyCursor propertyCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeHasProperty( node, property, nodeCursor, propertyCursor );
        }

        public int[] relationshipPropertyIds( final long node, final RelationshipScanCursor relationshipScanCursor, final PropertyCursor propertyCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipPropertyIds( node, relationshipScanCursor, propertyCursor );
        }

        public boolean relationshipHasProperty( final long node, final int property, final RelationshipScanCursor relationshipScanCursor,
                final PropertyCursor propertyCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipHasProperty( node, property, relationshipScanCursor, propertyCursor );
        }

        public int nodeGetOutgoingDegree( final long node, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeGetOutgoingDegree( node, nodeCursor );
        }

        public int nodeGetOutgoingDegree( final long node, final int relationship, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeGetOutgoingDegree( node, relationship, nodeCursor );
        }

        public int nodeGetIncomingDegree( final long node, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeGetIncomingDegree( node, nodeCursor );
        }

        public int nodeGetIncomingDegree( final long node, final int relationship, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeGetIncomingDegree( node, relationship, nodeCursor );
        }

        public int nodeGetTotalDegree( final long node, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeGetTotalDegree( node, nodeCursor );
        }

        public int nodeGetTotalDegree( final long node, final int relationship, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeGetTotalDegree( node, relationship, nodeCursor );
        }

        public void singleRelationship( final long id, final RelationshipScanCursor cursor )
        {
            StaticEvaluation.EmptyQueryContext.super.singleRelationship( id, cursor );
        }

        public ListValue getLabelsForNode( final long id, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.getLabelsForNode( id, nodeCursor );
        }

        public boolean isLabelSetOnNode( final int label, final long id, final NodeCursor nodeCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.isLabelSetOnNode( label, id, nodeCursor );
        }

        public MapValue nodeAsMap( final long id, final NodeCursor nodeCursor, final PropertyCursor propertyCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.nodeAsMap( id, nodeCursor, propertyCursor );
        }

        public MapValue relationshipAsMap( final long id, final RelationshipScanCursor relationshipCursor, final PropertyCursor propertyCursor )
        {
            return StaticEvaluation.EmptyQueryContext.super.relationshipAsMap( id, relationshipCursor, propertyCursor );
        }

        public Value getTxStateNodePropertyOrNull( final long nodeId, final int propertyKey )
        {
            return StaticEvaluation.EmptyQueryContext.super.getTxStateNodePropertyOrNull( nodeId, propertyKey );
        }

        public Value getTxStateRelationshipPropertyOrNull( final long relId, final int propertyKey )
        {
            return StaticEvaluation.EmptyQueryContext.super.getTxStateRelationshipPropertyOrNull( relId, propertyKey );
        }

        public Option<QueryStatistics> getOptStatistics()
        {
            return QueryContext.getOptStatistics$( this );
        }

        public int nodeGetDegree( final long node, final SemanticDirection dir, final NodeCursor nodeCursor )
        {
            return QueryContext.nodeGetDegree$( this, node, dir, nodeCursor );
        }

        public int nodeGetDegree( final long node, final SemanticDirection dir, final int relTypeId, final NodeCursor nodeCursor )
        {
            return QueryContext.nodeGetDegree$( this, node, dir, relTypeId, nodeCursor );
        }

        public Value nodeProperty( final long node, final int property, final NodeCursor nodeCursor, final PropertyCursor propertyCursor,
                final boolean throwOnDeleted )
        {
            return QueryContext.nodeProperty$( this, node, property, nodeCursor, propertyCursor, throwOnDeleted );
        }

        public Value relationshipProperty( final long relationship, final int property, final RelationshipScanCursor relationshipScanCursor,
                final PropertyCursor propertyCursor, final boolean throwOnDeleted )
        {
            return QueryContext.relationshipProperty$( this, relationship, property, relationshipScanCursor, propertyCursor, throwOnDeleted );
        }

        public Optional<Boolean> hasTxStatePropertyForCachedNodeProperty( final long nodeId, final int propertyKeyId )
        {
            return QueryContext.hasTxStatePropertyForCachedNodeProperty$( this, nodeId, propertyKeyId );
        }

        public Optional<Boolean> hasTxStatePropertyForCachedRelationshipProperty( final long relId, final int propertyKeyId )
        {
            return QueryContext.hasTxStatePropertyForCachedRelationshipProperty$( this, relId, propertyKeyId );
        }

        public AnyValue callFunction( final int id, final AnyValue[] args, final String[] allowed )
        {
            return this.procedures.callFunction( new StaticEvaluation.StaticProcedureContext(), id, args );
        }
    }
}
