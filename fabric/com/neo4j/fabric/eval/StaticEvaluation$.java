package com.neo4j.fabric.eval;

import scala.runtime.Nothing.;

public final class StaticEvaluation$
{
    public static StaticEvaluation$ MODULE$;

    static
    {
        new StaticEvaluation$();
    }

    public com$neo4j$fabric$eval$StaticEvaluation$$notAvailable()
    {
        throw new RuntimeException( "Operation not available in static context." );
    }

    private StaticEvaluation$()
    {
        MODULE$ = this;
    }
}
