package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.AsValue;
import com.neo4j.fabric.shaded.driver.internal.InternalIsoDuration;
import com.neo4j.fabric.shaded.driver.internal.InternalPoint2D;
import com.neo4j.fabric.shaded.driver.internal.InternalPoint3D;
import com.neo4j.fabric.shaded.driver.internal.util.Extract;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;
import com.neo4j.fabric.shaded.driver.internal.value.BooleanValue;
import com.neo4j.fabric.shaded.driver.internal.value.BytesValue;
import com.neo4j.fabric.shaded.driver.internal.value.DateTimeValue;
import com.neo4j.fabric.shaded.driver.internal.value.DateValue;
import com.neo4j.fabric.shaded.driver.internal.value.DurationValue;
import com.neo4j.fabric.shaded.driver.internal.value.FloatValue;
import com.neo4j.fabric.shaded.driver.internal.value.IntegerValue;
import com.neo4j.fabric.shaded.driver.internal.value.ListValue;
import com.neo4j.fabric.shaded.driver.internal.value.LocalDateTimeValue;
import com.neo4j.fabric.shaded.driver.internal.value.LocalTimeValue;
import com.neo4j.fabric.shaded.driver.internal.value.MapValue;
import com.neo4j.fabric.shaded.driver.internal.value.NullValue;
import com.neo4j.fabric.shaded.driver.internal.value.PointValue;
import com.neo4j.fabric.shaded.driver.internal.value.StringValue;
import com.neo4j.fabric.shaded.driver.internal.value.TimeValue;
import com.neo4j.fabric.shaded.driver.types.Entity;
import com.neo4j.fabric.shaded.driver.types.IsoDuration;
import com.neo4j.fabric.shaded.driver.types.MapAccessor;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Point;
import com.neo4j.fabric.shaded.driver.types.Relationship;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Stream;

public abstract class Values
{
    public static final Value EmptyMap = value( Collections.emptyMap() );
    public static final Value NULL;

    static
    {
        NULL = NullValue.NULL;
    }

    private Values()
    {
        throw new UnsupportedOperationException();
    }

    public static Value value( Object value )
    {
        if ( value == null )
        {
            return NullValue.NULL;
        }
        else if ( value instanceof AsValue )
        {
            return ((AsValue) value).asValue();
        }
        else if ( value instanceof Boolean )
        {
            return value( (Boolean) value );
        }
        else if ( value instanceof String )
        {
            return value( (String) value );
        }
        else if ( value instanceof Character )
        {
            return value( (Character) value );
        }
        else if ( value instanceof Long )
        {
            return value( (Long) value );
        }
        else if ( value instanceof Short )
        {
            return value( (int) (Short) value );
        }
        else if ( value instanceof Byte )
        {
            return value( (int) (Byte) value );
        }
        else if ( value instanceof Integer )
        {
            return value( (Integer) value );
        }
        else if ( value instanceof Double )
        {
            return value( (Double) value );
        }
        else if ( value instanceof Float )
        {
            return value( (double) (Float) value );
        }
        else if ( value instanceof LocalDate )
        {
            return value( (LocalDate) value );
        }
        else if ( value instanceof OffsetTime )
        {
            return value( (OffsetTime) value );
        }
        else if ( value instanceof LocalTime )
        {
            return value( (LocalTime) value );
        }
        else if ( value instanceof LocalDateTime )
        {
            return value( (LocalDateTime) value );
        }
        else if ( value instanceof OffsetDateTime )
        {
            return value( (OffsetDateTime) value );
        }
        else if ( value instanceof ZonedDateTime )
        {
            return value( (ZonedDateTime) value );
        }
        else if ( value instanceof IsoDuration )
        {
            return value( (IsoDuration) value );
        }
        else if ( value instanceof Period )
        {
            return value( (Period) value );
        }
        else if ( value instanceof Duration )
        {
            return value( (Duration) value );
        }
        else if ( value instanceof Point )
        {
            return value( (Point) value );
        }
        else if ( value instanceof List )
        {
            return value( (List) value );
        }
        else if ( value instanceof Map )
        {
            return value( (Map) value );
        }
        else if ( value instanceof Iterable )
        {
            return value( (Iterable) value );
        }
        else if ( value instanceof Iterator )
        {
            return value( (Iterator) value );
        }
        else if ( value instanceof Stream )
        {
            return value( (Stream) value );
        }
        else if ( value instanceof byte[] )
        {
            return value( (byte[]) ((byte[]) value) );
        }
        else if ( value instanceof boolean[] )
        {
            return value( (boolean[]) ((boolean[]) value) );
        }
        else if ( value instanceof String[] )
        {
            return value( (String[]) ((String[]) value) );
        }
        else if ( value instanceof long[] )
        {
            return value( (long[]) ((long[]) value) );
        }
        else if ( value instanceof int[] )
        {
            return value( (int[]) ((int[]) value) );
        }
        else if ( value instanceof double[] )
        {
            return value( (double[]) ((double[]) value) );
        }
        else if ( value instanceof float[] )
        {
            return value( (float[]) ((float[]) value) );
        }
        else if ( value instanceof Value[] )
        {
            return value( (Value[]) ((Value[]) value) );
        }
        else if ( value instanceof Object[] )
        {
            return value( Arrays.asList( (Object[]) ((Object[]) value) ) );
        }
        else
        {
            throw new ClientException( "Unable to convert " + value.getClass().getName() + " to Neo4j Value." );
        }
    }

    public static Value[] values( Object... input )
    {
        Value[] values = new Value[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = value( input[i] );
        }

        return values;
    }

    public static Value value( Value... input )
    {
        int size = input.length;
        Value[] values = new Value[size];
        System.arraycopy( input, 0, values, 0, size );
        return new ListValue( values );
    }

    public static BytesValue value( byte... input )
    {
        return new BytesValue( input );
    }

    public static Value value( String... input )
    {
        StringValue[] values = new StringValue[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = new StringValue( input[i] );
        }

        return new ListValue( values );
    }

    public static Value value( boolean... input )
    {
        Value[] values = new Value[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = value( input[i] );
        }

        return new ListValue( values );
    }

    public static Value value( char... input )
    {
        Value[] values = new Value[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = value( input[i] );
        }

        return new ListValue( values );
    }

    public static Value value( long... input )
    {
        Value[] values = new Value[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = value( input[i] );
        }

        return new ListValue( values );
    }

    public static Value value( int... input )
    {
        Value[] values = new Value[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = value( input[i] );
        }

        return new ListValue( values );
    }

    public static Value value( double... input )
    {
        Value[] values = new Value[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = value( input[i] );
        }

        return new ListValue( values );
    }

    public static Value value( float... input )
    {
        Value[] values = new Value[input.length];

        for ( int i = 0; i < input.length; ++i )
        {
            values[i] = value( (double) input[i] );
        }

        return new ListValue( values );
    }

    public static Value value( List<Object> vals )
    {
        Value[] values = new Value[vals.size()];
        int i = 0;

        Object val;
        for ( Iterator var3 = vals.iterator(); var3.hasNext(); values[i++] = value( val ) )
        {
            val = var3.next();
        }

        return new ListValue( values );
    }

    public static Value value( Iterable<Object> val )
    {
        return value( val.iterator() );
    }

    public static Value value( Iterator<Object> val )
    {
        ArrayList values = new ArrayList();

        while ( val.hasNext() )
        {
            values.add( value( val.next() ) );
        }

        return new ListValue( (Value[]) values.toArray( new Value[0] ) );
    }

    public static Value value( Stream<Object> stream )
    {
        Value[] values = (Value[]) stream.map( Values::value ).toArray( ( x$0 ) -> {
            return new Value[x$0];
        } );
        return new ListValue( values );
    }

    public static Value value( char val )
    {
        return new StringValue( String.valueOf( val ) );
    }

    public static Value value( String val )
    {
        return new StringValue( val );
    }

    public static Value value( long val )
    {
        return new IntegerValue( val );
    }

    public static Value value( int val )
    {
        return new IntegerValue( (long) val );
    }

    public static Value value( double val )
    {
        return new FloatValue( val );
    }

    public static Value value( boolean val )
    {
        return BooleanValue.fromBoolean( val );
    }

    public static Value value( Map<String,Object> val )
    {
        Map<String,Value> asValues = Iterables.newHashMapWithSize( val.size() );
        Iterator var2 = val.entrySet().iterator();

        while ( var2.hasNext() )
        {
            Entry<String,Object> entry = (Entry) var2.next();
            asValues.put( entry.getKey(), value( entry.getValue() ) );
        }

        return new MapValue( asValues );
    }

    public static Value value( LocalDate localDate )
    {
        return new DateValue( localDate );
    }

    public static Value value( OffsetTime offsetTime )
    {
        return new TimeValue( offsetTime );
    }

    public static Value value( LocalTime localTime )
    {
        return new LocalTimeValue( localTime );
    }

    public static Value value( LocalDateTime localDateTime )
    {
        return new LocalDateTimeValue( localDateTime );
    }

    public static Value value( OffsetDateTime offsetDateTime )
    {
        return new DateTimeValue( offsetDateTime.toZonedDateTime() );
    }

    public static Value value( ZonedDateTime zonedDateTime )
    {
        return new DateTimeValue( zonedDateTime );
    }

    public static Value value( Period period )
    {
        return value( (IsoDuration) (new InternalIsoDuration( period )) );
    }

    public static Value value( Duration duration )
    {
        return value( (IsoDuration) (new InternalIsoDuration( duration )) );
    }

    public static Value isoDuration( long months, long days, long seconds, int nanoseconds )
    {
        return value( (IsoDuration) (new InternalIsoDuration( months, days, seconds, nanoseconds )) );
    }

    private static Value value( IsoDuration duration )
    {
        return new DurationValue( duration );
    }

    public static Value point( int srid, double x, double y )
    {
        return value( (Point) (new InternalPoint2D( srid, x, y )) );
    }

    private static Value value( Point point )
    {
        return new PointValue( point );
    }

    public static Value point( int srid, double x, double y, double z )
    {
        return value( (Point) (new InternalPoint3D( srid, x, y, z )) );
    }

    public static Value parameters( Object... keysAndValues )
    {
        if ( keysAndValues.length % 2 != 0 )
        {
            throw new ClientException(
                    "Parameters function requires an even number of arguments, alternating key and value. Arguments were: " + Arrays.toString( keysAndValues ) +
                            "." );
        }
        else
        {
            HashMap<String,Value> map = Iterables.newHashMapWithSize( keysAndValues.length / 2 );

            for ( int i = 0; i < keysAndValues.length; i += 2 )
            {
                Object value = keysAndValues[i + 1];
                Extract.assertParameter( value );
                map.put( keysAndValues[i].toString(), value( value ) );
            }

            return value( (Object) map );
        }
    }

    public static Function<Value,Value> ofValue()
    {
        return ( val ) -> {
            return val;
        };
    }

    public static Function<Value,Object> ofObject()
    {
        return Value::asObject;
    }

    public static Function<Value,Number> ofNumber()
    {
        return Value::asNumber;
    }

    public static Function<Value,String> ofString()
    {
        return Value::asString;
    }

    public static Function<Value,String> ofToString()
    {
        return Value::toString;
    }

    public static Function<Value,Integer> ofInteger()
    {
        return Value::asInt;
    }

    public static Function<Value,Long> ofLong()
    {
        return Value::asLong;
    }

    public static Function<Value,Float> ofFloat()
    {
        return Value::asFloat;
    }

    public static Function<Value,Double> ofDouble()
    {
        return Value::asDouble;
    }

    public static Function<Value,Boolean> ofBoolean()
    {
        return Value::asBoolean;
    }

    public static Function<Value,Map<String,Object>> ofMap()
    {
        return MapAccessor::asMap;
    }

    public static <T> Function<Value,Map<String,T>> ofMap( Function<Value,T> valueConverter )
    {
        return ( val ) -> {
            return val.asMap( valueConverter );
        };
    }

    public static Function<Value,Entity> ofEntity()
    {
        return Value::asEntity;
    }

    public static Function<Value,Long> ofEntityId()
    {
        return ( val ) -> {
            return val.asEntity().id();
        };
    }

    public static Function<Value,Node> ofNode()
    {
        return Value::asNode;
    }

    public static Function<Value,Relationship> ofRelationship()
    {
        return Value::asRelationship;
    }

    public static Function<Value,Path> ofPath()
    {
        return Value::asPath;
    }

    public static Function<Value,LocalDate> ofLocalDate()
    {
        return Value::asLocalDate;
    }

    public static Function<Value,OffsetTime> ofOffsetTime()
    {
        return Value::asOffsetTime;
    }

    public static Function<Value,LocalTime> ofLocalTime()
    {
        return Value::asLocalTime;
    }

    public static Function<Value,LocalDateTime> ofLocalDateTime()
    {
        return Value::asLocalDateTime;
    }

    public static Function<Value,OffsetDateTime> ofOffsetDateTime()
    {
        return Value::asOffsetDateTime;
    }

    public static Function<Value,ZonedDateTime> ofZonedDateTime()
    {
        return Value::asZonedDateTime;
    }

    public static Function<Value,IsoDuration> ofIsoDuration()
    {
        return Value::asIsoDuration;
    }

    public static Function<Value,Point> ofPoint()
    {
        return Value::asPoint;
    }

    public static Function<Value,List<Object>> ofList()
    {
        return Value::asList;
    }

    public static <T> Function<Value,List<T>> ofList( Function<Value,T> innerMap )
    {
        return ( value ) -> {
            return value.asList( innerMap );
        };
    }
}
