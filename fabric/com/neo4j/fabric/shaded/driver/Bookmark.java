package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.internal.InternalBookmark;

import java.util.Set;

public interface Bookmark
{
    static Bookmark from( Set<String> values )
    {
        return InternalBookmark.parse( values );
    }

    Set<String> values();

    boolean isEmpty();
}
