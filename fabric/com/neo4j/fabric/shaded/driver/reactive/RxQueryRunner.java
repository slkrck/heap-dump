package com.neo4j.fabric.shaded.driver.reactive;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;

import java.util.Map;

public interface RxQueryRunner
{
    RxResult run( String var1, Value var2 );

    RxResult run( String var1, Map<String,Object> var2 );

    RxResult run( String var1, Record var2 );

    RxResult run( String var1 );

    RxResult run( Query var1 );
}
