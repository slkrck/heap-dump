package com.neo4j.fabric.shaded.driver.reactive;

import org.reactivestreams.Publisher;

public interface RxTransaction extends RxQueryRunner
{
    <T> Publisher<T> commit();

    <T> Publisher<T> rollback();
}
