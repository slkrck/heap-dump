package com.neo4j.fabric.shaded.driver.reactive;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;

import java.util.Map;

import org.reactivestreams.Publisher;

public interface RxSession extends RxQueryRunner
{
    Publisher<RxTransaction> beginTransaction();

    Publisher<RxTransaction> beginTransaction( TransactionConfig var1 );

    <T> Publisher<T> readTransaction( RxTransactionWork<? extends Publisher<T>> var1 );

    <T> Publisher<T> readTransaction( RxTransactionWork<? extends Publisher<T>> var1, TransactionConfig var2 );

    <T> Publisher<T> writeTransaction( RxTransactionWork<? extends Publisher<T>> var1 );

    <T> Publisher<T> writeTransaction( RxTransactionWork<? extends Publisher<T>> var1, TransactionConfig var2 );

    RxResult run( String var1, TransactionConfig var2 );

    RxResult run( String var1, Map<String,Object> var2, TransactionConfig var3 );

    RxResult run( Query var1, TransactionConfig var2 );

    Bookmark lastBookmark();

    <T> Publisher<T> close();
}
