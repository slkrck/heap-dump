package com.neo4j.fabric.shaded.driver.reactive;

public interface RxTransactionWork<T>
{
    T execute( RxTransaction var1 );
}
