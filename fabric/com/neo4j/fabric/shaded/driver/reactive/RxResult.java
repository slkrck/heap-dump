package com.neo4j.fabric.shaded.driver.reactive;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;

import org.reactivestreams.Publisher;

public interface RxResult
{
    Publisher<List<String>> keys();

    Publisher<Record> records();

    Publisher<ResultSummary> consume();
}
