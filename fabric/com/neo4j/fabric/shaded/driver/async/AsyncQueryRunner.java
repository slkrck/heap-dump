package com.neo4j.fabric.shaded.driver.async;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;

import java.util.Map;
import java.util.concurrent.CompletionStage;

public interface AsyncQueryRunner
{
    CompletionStage<ResultCursor> runAsync( String var1, Value var2 );

    CompletionStage<ResultCursor> runAsync( String var1, Map<String,Object> var2 );

    CompletionStage<ResultCursor> runAsync( String var1, Record var2 );

    CompletionStage<ResultCursor> runAsync( String var1 );

    CompletionStage<ResultCursor> runAsync( Query var1 );
}
