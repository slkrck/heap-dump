package com.neo4j.fabric.shaded.driver.async;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import java.util.function.Function;

public interface ResultCursor
{
    List<String> keys();

    CompletionStage<ResultSummary> consumeAsync();

    CompletionStage<Record> nextAsync();

    CompletionStage<Record> peekAsync();

    CompletionStage<Record> singleAsync();

    CompletionStage<ResultSummary> forEachAsync( Consumer<Record> var1 );

    CompletionStage<List<Record>> listAsync();

    <T> CompletionStage<List<T>> listAsync( Function<Record,T> var1 );
}
