package com.neo4j.fabric.shaded.driver.async;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;

import java.util.Map;
import java.util.concurrent.CompletionStage;

public interface AsyncSession extends AsyncQueryRunner
{
    CompletionStage<AsyncTransaction> beginTransactionAsync();

    CompletionStage<AsyncTransaction> beginTransactionAsync( TransactionConfig var1 );

    <T> CompletionStage<T> readTransactionAsync( AsyncTransactionWork<CompletionStage<T>> var1 );

    <T> CompletionStage<T> readTransactionAsync( AsyncTransactionWork<CompletionStage<T>> var1, TransactionConfig var2 );

    <T> CompletionStage<T> writeTransactionAsync( AsyncTransactionWork<CompletionStage<T>> var1 );

    <T> CompletionStage<T> writeTransactionAsync( AsyncTransactionWork<CompletionStage<T>> var1, TransactionConfig var2 );

    CompletionStage<ResultCursor> runAsync( String var1, TransactionConfig var2 );

    CompletionStage<ResultCursor> runAsync( String var1, Map<String,Object> var2, TransactionConfig var3 );

    CompletionStage<ResultCursor> runAsync( Query var1, TransactionConfig var2 );

    Bookmark lastBookmark();

    CompletionStage<Void> closeAsync();
}
