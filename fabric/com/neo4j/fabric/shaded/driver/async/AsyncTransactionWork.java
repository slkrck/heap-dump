package com.neo4j.fabric.shaded.driver.async;

public interface AsyncTransactionWork<T>
{
    T execute( AsyncTransaction var1 );
}
