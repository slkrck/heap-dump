package com.neo4j.fabric.shaded.driver.async;

import java.util.concurrent.CompletionStage;

public interface AsyncTransaction extends AsyncQueryRunner
{
    CompletionStage<Void> commitAsync();

    CompletionStage<Void> rollbackAsync();
}
