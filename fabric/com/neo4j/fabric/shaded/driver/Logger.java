package com.neo4j.fabric.shaded.driver;

public interface Logger
{
    void error( String var1, Throwable var2 );

    void info( String var1, Object... var2 );

    void warn( String var1, Object... var2 );

    void warn( String var1, Throwable var2 );

    void debug( String var1, Object... var2 );

    void trace( String var1, Object... var2 );

    boolean isTraceEnabled();

    boolean isDebugEnabled();
}
