package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.exceptions.NoSuchRecordException;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Result extends Iterator<Record>
{
    List<String> keys();

    boolean hasNext();

    Record next();

    Record single() throws NoSuchRecordException;

    Record peek();

    Stream<Record> stream();

    List<Record> list();

    <T> List<T> list( Function<Record,T> var1 );

    ResultSummary consume();
}
