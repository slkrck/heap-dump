package com.neo4j.fabric.shaded.driver.exceptions;

public class UntrustedServerException extends RuntimeException
{
    public UntrustedServerException( String message )
    {
        super( message );
    }
}
