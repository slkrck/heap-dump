package com.neo4j.fabric.shaded.driver.exceptions;

public class DatabaseException extends Neo4jException
{
    public DatabaseException( String code, String message )
    {
        super( code, message );
    }
}
