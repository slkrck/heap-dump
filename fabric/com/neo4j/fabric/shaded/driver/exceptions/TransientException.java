package com.neo4j.fabric.shaded.driver.exceptions;

public class TransientException extends Neo4jException
{
    public TransientException( String code, String message )
    {
        super( code, message );
    }
}
