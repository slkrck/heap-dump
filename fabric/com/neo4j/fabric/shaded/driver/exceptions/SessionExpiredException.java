package com.neo4j.fabric.shaded.driver.exceptions;

public class SessionExpiredException extends Neo4jException
{
    public SessionExpiredException( String message )
    {
        super( message );
    }

    public SessionExpiredException( String message, Throwable throwable )
    {
        super( message, throwable );
    }
}
