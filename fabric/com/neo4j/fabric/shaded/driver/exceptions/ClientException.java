package com.neo4j.fabric.shaded.driver.exceptions;

public class ClientException extends Neo4jException
{
    public ClientException( String message )
    {
        super( message );
    }

    public ClientException( String message, Throwable cause )
    {
        super( message, cause );
    }

    public ClientException( String code, String message )
    {
        super( code, message );
    }
}
