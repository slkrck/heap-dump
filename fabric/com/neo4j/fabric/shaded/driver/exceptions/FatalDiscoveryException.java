package com.neo4j.fabric.shaded.driver.exceptions;

public class FatalDiscoveryException extends ClientException
{
    public FatalDiscoveryException( String message )
    {
        super( message );
    }

    public FatalDiscoveryException( String code, String message )
    {
        super( code, message );
    }
}
