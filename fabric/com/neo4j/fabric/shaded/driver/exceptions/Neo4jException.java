package com.neo4j.fabric.shaded.driver.exceptions;

public abstract class Neo4jException extends RuntimeException
{
    private static final long serialVersionUID = -80579062276712566L;
    private final String code;

    public Neo4jException( String message )
    {
        this( "N/A", message );
    }

    public Neo4jException( String message, Throwable cause )
    {
        this( "N/A", message, cause );
    }

    public Neo4jException( String code, String message )
    {
        this( code, message, (Throwable) null );
    }

    public Neo4jException( String code, String message, Throwable cause )
    {
        super( message, cause );
        this.code = code;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public String neo4jErrorCode()
    {
        return this.code;
    }

    public String code()
    {
        return this.code;
    }
}
