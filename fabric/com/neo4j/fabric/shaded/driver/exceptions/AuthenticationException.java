package com.neo4j.fabric.shaded.driver.exceptions;

public class AuthenticationException extends SecurityException
{
    public AuthenticationException( String code, String message )
    {
        super( code, message );
    }
}
