package com.neo4j.fabric.shaded.driver.exceptions;

public class ProtocolException extends Neo4jException
{
    private static final String CODE = "Protocol violation: ";

    public ProtocolException( String message )
    {
        super( "Protocol violation: " + message );
    }

    public ProtocolException( String message, Throwable e )
    {
        super( "Protocol violation: " + message, e );
    }
}
