package com.neo4j.fabric.shaded.driver.exceptions;

public class DiscoveryException extends Neo4jException
{
    public DiscoveryException( String message, Throwable cause )
    {
        super( message, cause );
    }
}
