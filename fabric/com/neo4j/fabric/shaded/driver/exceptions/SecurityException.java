package com.neo4j.fabric.shaded.driver.exceptions;

public class SecurityException extends ClientException
{
    public SecurityException( String code, String message )
    {
        super( code, message );
    }

    public SecurityException( String message, Throwable t )
    {
        super( message, t );
    }
}
