package com.neo4j.fabric.shaded.driver.exceptions.value;

import com.neo4j.fabric.shaded.driver.exceptions.ClientException;

public class ValueException extends ClientException
{
    private static final long serialVersionUID = -1269336313727174998L;

    public ValueException( String message )
    {
        super( message );
    }
}
