package com.neo4j.fabric.shaded.driver.exceptions.value;

public class Unsizable extends ValueException
{
    private static final long serialVersionUID = 741487155344252339L;

    public Unsizable( String message )
    {
        super( message );
    }
}
