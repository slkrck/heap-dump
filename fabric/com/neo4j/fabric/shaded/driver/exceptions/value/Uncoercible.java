package com.neo4j.fabric.shaded.driver.exceptions.value;

public class Uncoercible extends ValueException
{
    private static final long serialVersionUID = -6259981390929065201L;

    public Uncoercible( String sourceTypeName, String destinationTypeName )
    {
        super( String.format( "Cannot coerce %s to %s", sourceTypeName, destinationTypeName ) );
    }
}
