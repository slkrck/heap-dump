package com.neo4j.fabric.shaded.driver.exceptions.value;

public class NotMultiValued extends ValueException
{
    private static final long serialVersionUID = -7380569883011364090L;

    public NotMultiValued( String message )
    {
        super( message );
    }
}
