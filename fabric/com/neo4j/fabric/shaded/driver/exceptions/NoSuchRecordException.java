package com.neo4j.fabric.shaded.driver.exceptions;

import java.util.NoSuchElementException;

public class NoSuchRecordException extends NoSuchElementException
{
    private static final long serialVersionUID = 9091962868264042491L;

    public NoSuchRecordException( String message )
    {
        super( message );
    }
}
