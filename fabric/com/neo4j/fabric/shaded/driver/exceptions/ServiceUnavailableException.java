package com.neo4j.fabric.shaded.driver.exceptions;

public class ServiceUnavailableException extends Neo4jException
{
    public ServiceUnavailableException( String message )
    {
        super( message );
    }

    public ServiceUnavailableException( String message, Throwable throwable )
    {
        super( message, throwable );
    }
}
