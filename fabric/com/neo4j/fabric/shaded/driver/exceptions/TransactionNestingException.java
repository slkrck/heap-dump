package com.neo4j.fabric.shaded.driver.exceptions;

public class TransactionNestingException extends ClientException
{
    public TransactionNestingException( String message )
    {
        super( message );
    }
}
