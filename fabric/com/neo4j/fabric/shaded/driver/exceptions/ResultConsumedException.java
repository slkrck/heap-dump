package com.neo4j.fabric.shaded.driver.exceptions;

public class ResultConsumedException extends ClientException
{
    public ResultConsumedException( String message )
    {
        super( message );
    }
}
