package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.internal.async.pool.PoolSettings;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingSettings;
import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.FetchSizeUtil;
import com.neo4j.fabric.shaded.driver.internal.retry.RetrySettings;
import com.neo4j.fabric.shaded.driver.net.ServerAddressResolver;
import com.neo4j.fabric.shaded.driver.util.Immutable;

import java.io.File;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@Immutable
public class Config
{
    private static final Config EMPTY = builder().build();
    private final Logging logging;
    private final boolean logLeakedSessions;
    private final int maxConnectionPoolSize;
    private final long idleTimeBeforeConnectionTest;
    private final long maxConnectionLifetimeMillis;
    private final long connectionAcquisitionTimeoutMillis;
    private final boolean encrypted;
    private final Config.TrustStrategy trustStrategy;
    private final int routingFailureLimit;
    private final long routingRetryDelayMillis;
    private final long fetchSize;
    private final long routingTablePurgeDelayMillis;
    private final int connectionTimeoutMillis;
    private final RetrySettings retrySettings;
    private final ServerAddressResolver resolver;
    private final boolean isMetricsEnabled;
    private final int eventLoopThreads;

    private Config( Config.ConfigBuilder builder )
    {
        this.logging = builder.logging;
        this.logLeakedSessions = builder.logLeakedSessions;
        this.idleTimeBeforeConnectionTest = builder.idleTimeBeforeConnectionTest;
        this.maxConnectionLifetimeMillis = builder.maxConnectionLifetimeMillis;
        this.maxConnectionPoolSize = builder.maxConnectionPoolSize;
        this.connectionAcquisitionTimeoutMillis = builder.connectionAcquisitionTimeoutMillis;
        this.encrypted = builder.encrypted;
        this.trustStrategy = builder.trustStrategy;
        this.routingFailureLimit = builder.routingFailureLimit;
        this.routingRetryDelayMillis = builder.routingRetryDelayMillis;
        this.connectionTimeoutMillis = builder.connectionTimeoutMillis;
        this.routingTablePurgeDelayMillis = builder.routingTablePurgeDelayMillis;
        this.retrySettings = builder.retrySettings;
        this.resolver = builder.resolver;
        this.fetchSize = builder.fetchSize;
        this.eventLoopThreads = builder.eventLoopThreads;
        this.isMetricsEnabled = builder.isMetricsEnabled;
    }

    public static Config.ConfigBuilder builder()
    {
        return new Config.ConfigBuilder();
    }

    public static Config defaultConfig()
    {
        return EMPTY;
    }

    public Logging logging()
    {
        return this.logging;
    }

    public boolean logLeakedSessions()
    {
        return this.logLeakedSessions;
    }

    public long idleTimeBeforeConnectionTest()
    {
        return this.idleTimeBeforeConnectionTest;
    }

    public long maxConnectionLifetimeMillis()
    {
        return this.maxConnectionLifetimeMillis;
    }

    public int connectionTimeoutMillis()
    {
        return this.connectionTimeoutMillis;
    }

    public int maxConnectionPoolSize()
    {
        return this.maxConnectionPoolSize;
    }

    public long connectionAcquisitionTimeoutMillis()
    {
        return this.connectionAcquisitionTimeoutMillis;
    }

    public boolean encrypted()
    {
        return this.encrypted;
    }

    public Config.TrustStrategy trustStrategy()
    {
        return this.trustStrategy;
    }

    public ServerAddressResolver resolver()
    {
        return this.resolver;
    }

    RoutingSettings routingSettings()
    {
        return new RoutingSettings( this.routingFailureLimit, this.routingRetryDelayMillis, this.routingTablePurgeDelayMillis );
    }

    RetrySettings retrySettings()
    {
        return this.retrySettings;
    }

    public long fetchSize()
    {
        return this.fetchSize;
    }

    public int eventLoopThreads()
    {
        return this.eventLoopThreads;
    }

    public boolean isMetricsEnabled()
    {
        return this.isMetricsEnabled;
    }

    public static class TrustStrategy
    {
        private final Config.TrustStrategy.Strategy strategy;
        private final File certFile;
        private boolean hostnameVerificationEnabled;

        private TrustStrategy( Config.TrustStrategy.Strategy strategy )
        {
            this( strategy, (File) null );
        }

        private TrustStrategy( Config.TrustStrategy.Strategy strategy, File certFile )
        {
            this.hostnameVerificationEnabled = true;
            this.strategy = strategy;
            this.certFile = certFile;
        }

        public static Config.TrustStrategy trustCustomCertificateSignedBy( File certFile )
        {
            return new Config.TrustStrategy( Config.TrustStrategy.Strategy.TRUST_CUSTOM_CA_SIGNED_CERTIFICATES, certFile );
        }

        public static Config.TrustStrategy trustSystemCertificates()
        {
            return new Config.TrustStrategy( Config.TrustStrategy.Strategy.TRUST_SYSTEM_CA_SIGNED_CERTIFICATES );
        }

        public static Config.TrustStrategy trustAllCertificates()
        {
            return new Config.TrustStrategy( Config.TrustStrategy.Strategy.TRUST_ALL_CERTIFICATES );
        }

        public Config.TrustStrategy.Strategy strategy()
        {
            return this.strategy;
        }

        public File certFile()
        {
            return this.certFile;
        }

        public boolean isHostnameVerificationEnabled()
        {
            return this.hostnameVerificationEnabled;
        }

        public Config.TrustStrategy withHostnameVerification()
        {
            this.hostnameVerificationEnabled = true;
            return this;
        }

        public Config.TrustStrategy withoutHostnameVerification()
        {
            this.hostnameVerificationEnabled = false;
            return this;
        }

        public static enum Strategy
        {
            TRUST_ALL_CERTIFICATES,
            TRUST_CUSTOM_CA_SIGNED_CERTIFICATES,
            TRUST_SYSTEM_CA_SIGNED_CERTIFICATES;
        }
    }

    public static class ConfigBuilder
    {
        private Logging logging;
        private boolean logLeakedSessions;
        private int maxConnectionPoolSize;
        private long idleTimeBeforeConnectionTest;
        private long maxConnectionLifetimeMillis;
        private long connectionAcquisitionTimeoutMillis;
        private boolean encrypted;
        private Config.TrustStrategy trustStrategy;
        private int routingFailureLimit;
        private long routingRetryDelayMillis;
        private long routingTablePurgeDelayMillis;
        private int connectionTimeoutMillis;
        private RetrySettings retrySettings;
        private ServerAddressResolver resolver;
        private boolean isMetricsEnabled;
        private long fetchSize;
        private int eventLoopThreads;

        private ConfigBuilder()
        {
            this.logging = Logging.javaUtilLogging( Level.INFO );
            this.maxConnectionPoolSize = 100;
            this.idleTimeBeforeConnectionTest = -1L;
            this.maxConnectionLifetimeMillis = PoolSettings.DEFAULT_MAX_CONNECTION_LIFETIME;
            this.connectionAcquisitionTimeoutMillis = PoolSettings.DEFAULT_CONNECTION_ACQUISITION_TIMEOUT;
            this.encrypted = false;
            this.trustStrategy = Config.TrustStrategy.trustSystemCertificates();
            this.routingFailureLimit = RoutingSettings.DEFAULT.maxRoutingFailures();
            this.routingRetryDelayMillis = RoutingSettings.DEFAULT.retryTimeoutDelay();
            this.routingTablePurgeDelayMillis = RoutingSettings.DEFAULT.routingTablePurgeDelayMs();
            this.connectionTimeoutMillis = (int) TimeUnit.SECONDS.toMillis( 30L );
            this.retrySettings = RetrySettings.DEFAULT;
            this.isMetricsEnabled = false;
            this.fetchSize = 1000L;
            this.eventLoopThreads = 0;
        }

        public Config.ConfigBuilder withLogging( Logging logging )
        {
            this.logging = logging;
            return this;
        }

        public Config.ConfigBuilder withLeakedSessionsLogging()
        {
            this.logLeakedSessions = true;
            return this;
        }

        public Config.ConfigBuilder withConnectionLivenessCheckTimeout( long value, TimeUnit unit )
        {
            this.idleTimeBeforeConnectionTest = unit.toMillis( value );
            return this;
        }

        public Config.ConfigBuilder withMaxConnectionLifetime( long value, TimeUnit unit )
        {
            this.maxConnectionLifetimeMillis = unit.toMillis( value );
            return this;
        }

        public Config.ConfigBuilder withMaxConnectionPoolSize( int value )
        {
            if ( value == 0 )
            {
                throw new IllegalArgumentException( "Zero value is not supported" );
            }
            else
            {
                if ( value < 0 )
                {
                    this.maxConnectionPoolSize = Integer.MAX_VALUE;
                }
                else
                {
                    this.maxConnectionPoolSize = value;
                }

                return this;
            }
        }

        public Config.ConfigBuilder withConnectionAcquisitionTimeout( long value, TimeUnit unit )
        {
            long valueInMillis = unit.toMillis( value );
            if ( value >= 0L )
            {
                this.connectionAcquisitionTimeoutMillis = valueInMillis;
            }
            else
            {
                this.connectionAcquisitionTimeoutMillis = -1L;
            }

            return this;
        }

        public Config.ConfigBuilder withEncryption()
        {
            this.encrypted = true;
            return this;
        }

        public Config.ConfigBuilder withoutEncryption()
        {
            this.encrypted = false;
            return this;
        }

        public Config.ConfigBuilder withTrustStrategy( Config.TrustStrategy trustStrategy )
        {
            this.trustStrategy = trustStrategy;
            return this;
        }

        /**
         * @deprecated
         */
        @Deprecated
        public Config.ConfigBuilder withRoutingFailureLimit( int routingFailureLimit )
        {
            if ( routingFailureLimit < 1 )
            {
                throw new IllegalArgumentException( "The failure limit may not be smaller than 1, but was: " + routingFailureLimit );
            }
            else
            {
                this.routingFailureLimit = routingFailureLimit;
                return this;
            }
        }

        /**
         * @deprecated
         */
        @Deprecated
        public Config.ConfigBuilder withRoutingRetryDelay( long delay, TimeUnit unit )
        {
            long routingRetryDelayMillis = unit.toMillis( delay );
            if ( routingRetryDelayMillis < 0L )
            {
                throw new IllegalArgumentException( String.format( "The retry delay may not be smaller than 0, but was %d %s.", delay, unit ) );
            }
            else
            {
                this.routingRetryDelayMillis = routingRetryDelayMillis;
                return this;
            }
        }

        public Config.ConfigBuilder withRoutingTablePurgeDelay( long delay, TimeUnit unit )
        {
            long routingTablePurgeDelayMillis = unit.toMillis( delay );
            if ( routingTablePurgeDelayMillis < 0L )
            {
                throw new IllegalArgumentException( String.format( "The routing table purge delay may not be smaller than 0, but was %d %s.", delay, unit ) );
            }
            else
            {
                this.routingTablePurgeDelayMillis = routingTablePurgeDelayMillis;
                return this;
            }
        }

        public Config.ConfigBuilder withFetchSize( long size )
        {
            this.fetchSize = FetchSizeUtil.assertValidFetchSize( size );
            return this;
        }

        public Config.ConfigBuilder withConnectionTimeout( long value, TimeUnit unit )
        {
            long connectionTimeoutMillis = unit.toMillis( value );
            if ( connectionTimeoutMillis < 0L )
            {
                throw new IllegalArgumentException( String.format( "The connection timeout may not be smaller than 0, but was %d %s.", value, unit ) );
            }
            else
            {
                int connectionTimeoutMillisInt = (int) connectionTimeoutMillis;
                if ( (long) connectionTimeoutMillisInt != connectionTimeoutMillis )
                {
                    throw new IllegalArgumentException(
                            String.format( "The connection timeout must represent int value when converted to milliseconds %d.", connectionTimeoutMillis ) );
                }
                else
                {
                    this.connectionTimeoutMillis = connectionTimeoutMillisInt;
                    return this;
                }
            }
        }

        public Config.ConfigBuilder withMaxTransactionRetryTime( long value, TimeUnit unit )
        {
            long maxRetryTimeMs = unit.toMillis( value );
            if ( maxRetryTimeMs < 0L )
            {
                throw new IllegalArgumentException( String.format( "The max retry time may not be smaller than 0, but was %d %s.", value, unit ) );
            }
            else
            {
                this.retrySettings = new RetrySettings( maxRetryTimeMs );
                return this;
            }
        }

        public Config.ConfigBuilder withResolver( ServerAddressResolver resolver )
        {
            this.resolver = (ServerAddressResolver) Objects.requireNonNull( resolver, "resolver" );
            return this;
        }

        public Config.ConfigBuilder withDriverMetrics()
        {
            this.isMetricsEnabled = true;
            return this;
        }

        public Config.ConfigBuilder withoutDriverMetrics()
        {
            this.isMetricsEnabled = false;
            return this;
        }

        public Config.ConfigBuilder withEventLoopThreads( int size )
        {
            if ( size < 1 )
            {
                throw new IllegalArgumentException( String.format( "The event loop thread may not be smaller than 1, but was %d.", size ) );
            }
            else
            {
                this.eventLoopThreads = size;
                return this;
            }
        }

        public Config build()
        {
            return new Config( this );
        }
    }
}
