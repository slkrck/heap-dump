package com.neo4j.fabric.shaded.driver;

public interface TransactionWork<T>
{
    T execute( Transaction var1 );
}
