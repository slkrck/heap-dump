package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.types.Entity;
import com.neo4j.fabric.shaded.driver.types.IsoDuration;
import com.neo4j.fabric.shaded.driver.types.MapAccessor;
import com.neo4j.fabric.shaded.driver.types.MapAccessorWithDefaultValue;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Point;
import com.neo4j.fabric.shaded.driver.types.Relationship;
import com.neo4j.fabric.shaded.driver.types.Type;
import com.neo4j.fabric.shaded.driver.util.Experimental;
import com.neo4j.fabric.shaded.driver.util.Immutable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Immutable
public interface Value extends MapAccessor, MapAccessorWithDefaultValue
{
    int size();

    boolean isEmpty();

    Iterable<String> keys();

    Value get( int var1 );

    @Experimental
    Type type();

    @Experimental
    boolean hasType( Type var1 );

    boolean isTrue();

    boolean isFalse();

    boolean isNull();

    Object asObject();

    <T> T computeOrDefault( Function<Value,T> var1, T var2 );

    boolean asBoolean();

    boolean asBoolean( boolean var1 );

    byte[] asByteArray();

    byte[] asByteArray( byte[] var1 );

    String asString();

    String asString( String var1 );

    Number asNumber();

    long asLong();

    long asLong( long var1 );

    int asInt();

    int asInt( int var1 );

    double asDouble();

    double asDouble( double var1 );

    float asFloat();

    float asFloat( float var1 );

    List<Object> asList();

    List<Object> asList( List<Object> var1 );

    <T> List<T> asList( Function<Value,T> var1 );

    <T> List<T> asList( Function<Value,T> var1, List<T> var2 );

    Entity asEntity();

    Node asNode();

    Relationship asRelationship();

    Path asPath();

    LocalDate asLocalDate();

    OffsetTime asOffsetTime();

    LocalTime asLocalTime();

    LocalDateTime asLocalDateTime();

    OffsetDateTime asOffsetDateTime();

    ZonedDateTime asZonedDateTime();

    IsoDuration asIsoDuration();

    Point asPoint();

    LocalDate asLocalDate( LocalDate var1 );

    OffsetTime asOffsetTime( OffsetTime var1 );

    LocalTime asLocalTime( LocalTime var1 );

    LocalDateTime asLocalDateTime( LocalDateTime var1 );

    OffsetDateTime asOffsetDateTime( OffsetDateTime var1 );

    ZonedDateTime asZonedDateTime( ZonedDateTime var1 );

    IsoDuration asIsoDuration( IsoDuration var1 );

    Point asPoint( Point var1 );

    Map<String,Object> asMap( Map<String,Object> var1 );

    <T> Map<String,T> asMap( Function<Value,T> var1, Map<String,T> var2 );

    boolean equals( Object var1 );

    int hashCode();

    String toString();
}
