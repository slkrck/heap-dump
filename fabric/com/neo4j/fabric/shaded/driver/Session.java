package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.util.Resource;

import java.util.Map;

public interface Session extends Resource, QueryRunner
{
    Transaction beginTransaction();

    Transaction beginTransaction( TransactionConfig var1 );

    <T> T readTransaction( TransactionWork<T> var1 );

    <T> T readTransaction( TransactionWork<T> var1, TransactionConfig var2 );

    <T> T writeTransaction( TransactionWork<T> var1 );

    <T> T writeTransaction( TransactionWork<T> var1, TransactionConfig var2 );

    Result run( String var1, TransactionConfig var2 );

    Result run( String var1, Map<String,Object> var2, TransactionConfig var3 );

    Result run( Query var1, TransactionConfig var2 );

    Bookmark lastBookmark();

    /**
     * @deprecated
     */
    @Deprecated
    void reset();

    void close();
}
