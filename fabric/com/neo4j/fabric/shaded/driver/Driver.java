package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.async.AsyncSession;
import com.neo4j.fabric.shaded.driver.reactive.RxSession;
import com.neo4j.fabric.shaded.driver.types.TypeSystem;
import com.neo4j.fabric.shaded.driver.util.Experimental;

import java.util.concurrent.CompletionStage;

public interface Driver extends AutoCloseable
{
    boolean isEncrypted();

    Session session();

    Session session( SessionConfig var1 );

    RxSession rxSession();

    RxSession rxSession( SessionConfig var1 );

    AsyncSession asyncSession();

    AsyncSession asyncSession( SessionConfig var1 );

    void close();

    CompletionStage<Void> closeAsync();

    @Experimental
    Metrics metrics();

    @Experimental
    boolean isMetricsEnabled();

    @Experimental
    TypeSystem defaultTypeSystem();

    void verifyConnectivity();

    CompletionStage<Void> verifyConnectivityAsync();

    boolean supportsMultiDb();

    CompletionStage<Boolean> supportsMultiDbAsync();
}
