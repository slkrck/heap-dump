package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.internal.util.Iterables;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;
import com.neo4j.fabric.shaded.driver.internal.value.MapValue;
import com.neo4j.fabric.shaded.driver.util.Immutable;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

@Immutable
public class Query
{
    private final String text;
    private final Value parameters;

    public Query( String text, Value parameters )
    {
        this.text = validateQueryText( text );
        if ( parameters == null )
        {
            this.parameters = Values.EmptyMap;
        }
        else
        {
            if ( !(parameters instanceof MapValue) )
            {
                throw new IllegalArgumentException( "The parameters should be provided as Map type. Unsupported parameters type: " + parameters.type().name() );
            }

            this.parameters = parameters;
        }
    }

    public Query( String text, Map<String,Object> parameters )
    {
        this( text, Values.value( parameters ) );
    }

    public Query( String text )
    {
        this( text, Values.EmptyMap );
    }

    private static String validateQueryText( String query )
    {
        Preconditions.checkArgument( query != null, "Cypher query text should not be null" );
        Preconditions.checkArgument( !query.isEmpty(), "Cypher query text should not be an empty string" );
        return query;
    }

    public String text()
    {
        return this.text;
    }

    public Value parameters()
    {
        return this.parameters;
    }

    public Query withText( String newText )
    {
        return new Query( newText, this.parameters );
    }

    public Query withParameters( Value newParameters )
    {
        return new Query( this.text, newParameters );
    }

    public Query withParameters( Map<String,Object> newParameters )
    {
        return new Query( this.text, newParameters );
    }

    public Query withUpdatedParameters( Value updates )
    {
        if ( updates != null && !updates.isEmpty() )
        {
            Map<String,Value> newParameters = Iterables.newHashMapWithSize( Math.max( this.parameters.size(), updates.size() ) );
            newParameters.putAll( this.parameters.asMap( Values.ofValue() ) );
            Iterator var3 = updates.asMap( Values.ofValue() ).entrySet().iterator();

            while ( var3.hasNext() )
            {
                Entry<String,Value> entry = (Entry) var3.next();
                Value value = (Value) entry.getValue();
                if ( value.isNull() )
                {
                    newParameters.remove( entry.getKey() );
                }
                else
                {
                    newParameters.put( entry.getKey(), value );
                }
            }

            return this.withParameters( Values.value( (Object) newParameters ) );
        }
        else
        {
            return this;
        }
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            Query query = (Query) o;
            return this.text.equals( query.text ) && this.parameters.equals( query.parameters );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.text.hashCode();
        result = 31 * result + this.parameters.hashCode();
        return result;
    }

    public String toString()
    {
        return String.format( "Query{text='%s', parameters=%s}", this.text, this.parameters );
    }
}
