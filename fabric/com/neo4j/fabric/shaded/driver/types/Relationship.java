package com.neo4j.fabric.shaded.driver.types;

public interface Relationship extends Entity
{
    long startNodeId();

    long endNodeId();

    String type();

    boolean hasType( String var1 );
}
