package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.Value;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface MapAccessorWithDefaultValue
{
    Value get( String var1, Value var2 );

    Object get( String var1, Object var2 );

    Number get( String var1, Number var2 );

    Entity get( String var1, Entity var2 );

    Node get( String var1, Node var2 );

    Path get( String var1, Path var2 );

    Relationship get( String var1, Relationship var2 );

    List<Object> get( String var1, List<Object> var2 );

    <T> List<T> get( String var1, List<T> var2, Function<Value,T> var3 );

    Map<String,Object> get( String var1, Map<String,Object> var2 );

    <T> Map<String,T> get( String var1, Map<String,T> var2, Function<Value,T> var3 );

    int get( String var1, int var2 );

    long get( String var1, long var2 );

    boolean get( String var1, boolean var2 );

    String get( String var1, String var2 );

    float get( String var1, float var2 );

    double get( String var1, double var2 );
}
