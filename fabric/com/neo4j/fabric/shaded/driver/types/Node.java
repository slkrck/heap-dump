package com.neo4j.fabric.shaded.driver.types;

public interface Node extends Entity
{
    Iterable<String> labels();

    boolean hasLabel( String var1 );
}
