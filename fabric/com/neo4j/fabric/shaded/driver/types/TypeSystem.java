package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.util.Experimental;
import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
@Experimental
public interface TypeSystem
{
    Type ANY();

    Type BOOLEAN();

    Type BYTES();

    Type STRING();

    Type NUMBER();

    Type INTEGER();

    Type FLOAT();

    Type LIST();

    Type MAP();

    Type NODE();

    Type RELATIONSHIP();

    Type PATH();

    Type POINT();

    Type DATE();

    Type TIME();

    Type LOCAL_TIME();

    Type LOCAL_DATE_TIME();

    Type DATE_TIME();

    Type DURATION();

    Type NULL();
}
