package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
public interface Point
{
    int srid();

    double x();

    double y();

    double z();
}
