package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
public interface Path extends Iterable<Path.Segment>
{
    Node start();

    Node end();

    int length();

    boolean contains( Node var1 );

    boolean contains( Relationship var1 );

    Iterable<Node> nodes();

    Iterable<Relationship> relationships();

    public interface Segment
    {
        Relationship relationship();

        Node start();

        Node end();
    }
}
