package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
public interface Entity extends MapAccessor
{
    long id();
}
