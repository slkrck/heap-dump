package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.Value;

import java.util.Map;
import java.util.function.Function;

public interface MapAccessor
{
    Iterable<String> keys();

    boolean containsKey( String var1 );

    Value get( String var1 );

    int size();

    Iterable<Value> values();

    <T> Iterable<T> values( Function<Value,T> var1 );

    Map<String,Object> asMap();

    <T> Map<String,T> asMap( Function<Value,T> var1 );
}
