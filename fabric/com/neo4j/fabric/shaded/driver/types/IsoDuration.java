package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.util.Immutable;

import java.time.temporal.TemporalAmount;

@Immutable
public interface IsoDuration extends TemporalAmount
{
    long months();

    long days();

    long seconds();

    int nanoseconds();
}
