package com.neo4j.fabric.shaded.driver.types;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.util.Experimental;
import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
@Experimental
public interface Type
{
    String name();

    boolean isTypeOf( Value var1 );
}
