package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.util.Experimental;

@Experimental
public interface ConnectionPoolMetrics
{
    String id();

    int inUse();

    int idle();

    int creating();

    long created();

    long failedToCreate();

    long closed();

    int acquiring();

    long acquired();

    long timedOutToAcquire();

    long totalAcquisitionTime();

    long totalConnectionTime();

    long totalInUseTime();

    long totalInUseCount();
}
