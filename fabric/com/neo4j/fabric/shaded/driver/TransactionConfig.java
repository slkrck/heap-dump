package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.internal.util.Extract;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class TransactionConfig
{
    private static final TransactionConfig EMPTY = builder().build();
    private final Duration timeout;
    private final Map<String,Value> metadata;

    private TransactionConfig( TransactionConfig.Builder builder )
    {
        this.timeout = builder.timeout;
        this.metadata = Collections.unmodifiableMap( builder.metadata );
    }

    public static TransactionConfig empty()
    {
        return EMPTY;
    }

    public static TransactionConfig.Builder builder()
    {
        return new TransactionConfig.Builder();
    }

    public Duration timeout()
    {
        return this.timeout;
    }

    public Map<String,Value> metadata()
    {
        return this.metadata;
    }

    public boolean isEmpty()
    {
        return this.timeout == null && (this.metadata == null || this.metadata.isEmpty());
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            TransactionConfig that = (TransactionConfig) o;
            return Objects.equals( this.timeout, that.timeout ) && Objects.equals( this.metadata, that.metadata );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.timeout, this.metadata} );
    }

    public String toString()
    {
        return "TransactionConfig{timeout=" + this.timeout + ", metadata=" + this.metadata + '}';
    }

    public static class Builder
    {
        private Duration timeout;
        private Map<String,Value> metadata;

        private Builder()
        {
            this.metadata = Collections.emptyMap();
        }

        public TransactionConfig.Builder withTimeout( Duration timeout )
        {
            Objects.requireNonNull( timeout, "Transaction timeout should not be null" );
            Preconditions.checkArgument( !timeout.isZero(), "Transaction timeout should not be zero" );
            Preconditions.checkArgument( !timeout.isNegative(), "Transaction timeout should not be negative" );
            this.timeout = timeout;
            return this;
        }

        public TransactionConfig.Builder withMetadata( Map<String,Object> metadata )
        {
            Objects.requireNonNull( metadata, "Transaction metadata should not be null" );
            this.metadata = Extract.mapOfValues( metadata );
            return this;
        }

        public TransactionConfig build()
        {
            return new TransactionConfig( this );
        }
    }
}
