package com.neo4j.fabric.shaded.driver.internal.retry;

public final class RetrySettings
{
    public static final RetrySettings DEFAULT;

    static
    {
        DEFAULT = new RetrySettings( ExponentialBackoffRetryLogic.DEFAULT_MAX_RETRY_TIME_MS );
    }

    private final long maxRetryTimeMs;

    public RetrySettings( long maxRetryTimeMs )
    {
        this.maxRetryTimeMs = maxRetryTimeMs;
    }

    public long maxRetryTimeMs()
    {
        return this.maxRetryTimeMs;
    }
}
