package com.neo4j.fabric.shaded.driver.internal.retry;

import java.util.concurrent.CompletionStage;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;

public interface RetryLogic
{
    <T> T retry( Supplier<T> var1 );

    <T> CompletionStage<T> retryAsync( Supplier<CompletionStage<T>> var1 );

    <T> Publisher<T> retryRx( Publisher<T> var1 );
}
