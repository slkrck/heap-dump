package com.neo4j.fabric.shaded.driver.internal.retry;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.exceptions.SessionExpiredException;
import com.neo4j.fabric.shaded.driver.exceptions.TransientException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutorGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher.Flux;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher.Mono;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Schedulers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuples;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;

public class ExponentialBackoffRetryLogic implements RetryLogic
{
    static final long DEFAULT_MAX_RETRY_TIME_MS;
    private static final String RETRY_LOGIC_LOG_NAME = "RetryLogic";
    private static final long INITIAL_RETRY_DELAY_MS;
    private static final double RETRY_DELAY_MULTIPLIER = 2.0D;
    private static final double RETRY_DELAY_JITTER_FACTOR = 0.2D;
    private static final long MAX_RETRY_DELAY = 4611686018427387903L;

    static
    {
        DEFAULT_MAX_RETRY_TIME_MS = TimeUnit.SECONDS.toMillis( 30L );
        INITIAL_RETRY_DELAY_MS = TimeUnit.SECONDS.toMillis( 1L );
    }

    private final long maxRetryTimeMs;
    private final long initialRetryDelayMs;
    private final double multiplier;
    private final double jitterFactor;
    private final EventExecutorGroup eventExecutorGroup;
    private final Clock clock;
    private final Logger log;

    public ExponentialBackoffRetryLogic( RetrySettings settings, EventExecutorGroup eventExecutorGroup, Clock clock, Logging logging )
    {
        this( settings.maxRetryTimeMs(), INITIAL_RETRY_DELAY_MS, 2.0D, 0.2D, eventExecutorGroup, clock, logging );
    }

    ExponentialBackoffRetryLogic( long maxRetryTimeMs, long initialRetryDelayMs, double multiplier, double jitterFactor, EventExecutorGroup eventExecutorGroup,
            Clock clock, Logging logging )
    {
        this.maxRetryTimeMs = maxRetryTimeMs;
        this.initialRetryDelayMs = initialRetryDelayMs;
        this.multiplier = multiplier;
        this.jitterFactor = jitterFactor;
        this.eventExecutorGroup = eventExecutorGroup;
        this.clock = clock;
        this.log = logging.getLog( "RetryLogic" );
        this.verifyAfterConstruction();
    }

    private static boolean isTransientError( Throwable error )
    {
        if ( error instanceof TransientException )
        {
            String code = ((TransientException) error).code();
            return !"Neo.TransientError.Transaction.Terminated".equals( code ) && !"Neo.TransientError.Transaction.LockClientStopped".equals( code );
        }
        else
        {
            return false;
        }
    }

    private static List<Throwable> recordError( Throwable error, List<Throwable> errors )
    {
        if ( errors == null )
        {
            errors = new ArrayList();
        }

        ((List) errors).add( error );
        return (List) errors;
    }

    private static void addSuppressed( Throwable error, List<Throwable> suppressedErrors )
    {
        if ( suppressedErrors != null )
        {
            Iterator var2 = suppressedErrors.iterator();

            while ( var2.hasNext() )
            {
                Throwable suppressedError = (Throwable) var2.next();
                if ( error != suppressedError )
                {
                    error.addSuppressed( suppressedError );
                }
            }
        }
    }

    public <T> T retry( Supplier<T> work )
    {
        List<Throwable> errors = null;
        long startTime = -1L;
        long nextDelayMs = this.initialRetryDelayMs;

        while ( true )
        {
            try
            {
                return work.get();
            }
            catch ( Throwable var14 )
            {
                if ( this.canRetryOn( var14 ) )
                {
                    long currentTime = this.clock.millis();
                    if ( startTime == -1L )
                    {
                        startTime = currentTime;
                    }

                    long elapsedTime = currentTime - startTime;
                    if ( elapsedTime < this.maxRetryTimeMs )
                    {
                        long delayWithJitterMs = this.computeDelayWithJitter( nextDelayMs );
                        this.log.warn( "Transaction failed and will be retried in " + delayWithJitterMs + "ms", var14 );
                        this.sleep( delayWithJitterMs );
                        nextDelayMs = (long) ((double) nextDelayMs * this.multiplier);
                        errors = recordError( var14, errors );
                        continue;
                    }
                }

                addSuppressed( var14, errors );
                throw var14;
            }
        }
    }

    public <T> CompletionStage<T> retryAsync( Supplier<CompletionStage<T>> work )
    {
        CompletableFuture<T> resultFuture = new CompletableFuture();
        this.executeWorkInEventLoop( resultFuture, work );
        return resultFuture;
    }

    public <T> Publisher<T> retryRx( Publisher<T> work )
    {
        return Flux.from( work ).retryWhen( this.retryRxCondition() );
    }

    protected boolean canRetryOn( Throwable error )
    {
        return error instanceof SessionExpiredException || error instanceof ServiceUnavailableException || isTransientError( error );
    }

    private Function<Flux<Throwable>,Publisher<Context>> retryRxCondition()
    {
        return ( errorCurrentAttempt ) -> {
            return errorCurrentAttempt.flatMap( ( e ) -> {
                return Mono.subscriberContext().map( ( ctx ) -> {
                    return Tuples.of( e, ctx );
                } );
            } ).flatMap( ( t2 ) -> {
                Throwable lastError = (Throwable) t2.getT1();
                Context ctx = (Context) t2.getT2();
                List<Throwable> errors = (List) ctx.getOrDefault( "errors", (Object) null );
                long startTime = (Long) ctx.getOrDefault( "startTime", -1L );
                long nextDelayMs = (Long) ctx.getOrDefault( "nextDelayMs", this.initialRetryDelayMs );
                if ( !this.canRetryOn( lastError ) )
                {
                    addSuppressed( lastError, errors );
                    return Mono.error( lastError );
                }
                else
                {
                    long currentTime = this.clock.millis();
                    if ( startTime == -1L )
                    {
                        startTime = currentTime;
                    }

                    long elapsedTime = currentTime - startTime;
                    if ( elapsedTime < this.maxRetryTimeMs )
                    {
                        long delayWithJitterMs = this.computeDelayWithJitter( nextDelayMs );
                        this.log.warn( "Reactive transaction failed and is scheduled to retry in " + delayWithJitterMs + "ms", lastError );
                        nextDelayMs = (long) ((double) nextDelayMs * this.multiplier);
                        errors = recordError( lastError, errors );
                        EventExecutor eventExecutor = this.eventExecutorGroup.next();
                        return Mono.just( ctx.put( "errors", errors ).put( "startTime", startTime ).put( "nextDelayMs", nextDelayMs ) ).delayElement(
                                Duration.ofMillis( delayWithJitterMs ), Schedulers.fromExecutorService( eventExecutor ) );
                    }
                    else
                    {
                        addSuppressed( lastError, errors );
                        return Mono.error( lastError );
                    }
                }
            } );
        };
    }

    private <T> void executeWorkInEventLoop( CompletableFuture<T> resultFuture, Supplier<CompletionStage<T>> work )
    {
        EventExecutor eventExecutor = this.eventExecutorGroup.next();
        eventExecutor.execute( () -> {
            this.executeWork( resultFuture, work, -1L, this.initialRetryDelayMs, (List) null );
        } );
    }

    private <T> void retryWorkInEventLoop( CompletableFuture<T> resultFuture, Supplier<CompletionStage<T>> work, Throwable error, long startTime, long delayMs,
            List<Throwable> errors )
    {
        EventExecutor eventExecutor = this.eventExecutorGroup.next();
        long delayWithJitterMs = this.computeDelayWithJitter( delayMs );
        this.log.warn( "Async transaction failed and is scheduled to retry in " + delayWithJitterMs + "ms", error );
        eventExecutor.schedule( () -> {
            long newRetryDelayMs = (long) ((double) delayMs * this.multiplier);
            this.executeWork( resultFuture, work, startTime, newRetryDelayMs, errors );
        }, delayWithJitterMs, TimeUnit.MILLISECONDS );
    }

    private <T> void executeWork( CompletableFuture<T> resultFuture, Supplier<CompletionStage<T>> work, long startTime, long retryDelayMs,
            List<Throwable> errors )
    {
        CompletionStage workStage;
        try
        {
            workStage = (CompletionStage) work.get();
        }
        catch ( Throwable var10 )
        {
            this.retryOnError( resultFuture, work, startTime, retryDelayMs, var10, errors );
            return;
        }

        workStage.whenComplete( ( result, completionError ) -> {
            Throwable error = Futures.completionExceptionCause( completionError );
            if ( error != null )
            {
                this.retryOnError( resultFuture, work, startTime, retryDelayMs, error, errors );
            }
            else
            {
                resultFuture.complete( result );
            }
        } );
    }

    private <T> void retryOnError( CompletableFuture<T> resultFuture, Supplier<CompletionStage<T>> work, long startTime, long retryDelayMs, Throwable error,
            List<Throwable> errors )
    {
        if ( this.canRetryOn( error ) )
        {
            long currentTime = this.clock.millis();
            if ( startTime == -1L )
            {
                startTime = currentTime;
            }

            long elapsedTime = currentTime - startTime;
            if ( elapsedTime < this.maxRetryTimeMs )
            {
                errors = recordError( error, errors );
                this.retryWorkInEventLoop( resultFuture, work, error, startTime, retryDelayMs, errors );
                return;
            }
        }

        addSuppressed( error, errors );
        resultFuture.completeExceptionally( error );
    }

    private long computeDelayWithJitter( long delayMs )
    {
        if ( delayMs > 4611686018427387903L )
        {
            delayMs = 4611686018427387903L;
        }

        long jitter = (long) ((double) delayMs * this.jitterFactor);
        long min = delayMs - jitter;
        long max = delayMs + jitter;
        return ThreadLocalRandom.current().nextLong( min, max + 1L );
    }

    private void sleep( long delayMs )
    {
        try
        {
            this.clock.sleep( delayMs );
        }
        catch ( InterruptedException var4 )
        {
            Thread.currentThread().interrupt();
            throw new IllegalStateException( "Retries interrupted", var4 );
        }
    }

    private void verifyAfterConstruction()
    {
        if ( this.maxRetryTimeMs < 0L )
        {
            throw new IllegalArgumentException( "Max retry time should be >= 0: " + this.maxRetryTimeMs );
        }
        else if ( this.initialRetryDelayMs < 0L )
        {
            throw new IllegalArgumentException( "Initial retry delay should >= 0: " + this.initialRetryDelayMs );
        }
        else if ( this.multiplier < 1.0D )
        {
            throw new IllegalArgumentException( "Multiplier should be >= 1.0: " + this.multiplier );
        }
        else if ( this.jitterFactor >= 0.0D && this.jitterFactor <= 1.0D )
        {
            if ( this.clock == null )
            {
                throw new IllegalArgumentException( "Clock should not be null" );
            }
        }
        else
        {
            throw new IllegalArgumentException( "Jitter factor should be in [0.0, 1.0]: " + this.jitterFactor );
        }
    }
}
