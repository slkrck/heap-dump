package com.neo4j.fabric.shaded.driver.internal.svm;

import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

import javax.net.ssl.SSLEngine;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.SslHandler$SslEngineType" )
final class Target_io_netty_handler_ssl_SslHandler$SslEngineType
{
    @Alias
    public static Target_io_netty_handler_ssl_SslHandler$SslEngineType JDK;

    @Substitute
    static Target_io_netty_handler_ssl_SslHandler$SslEngineType forEngine( SSLEngine engine )
    {
        return JDK;
    }
}
