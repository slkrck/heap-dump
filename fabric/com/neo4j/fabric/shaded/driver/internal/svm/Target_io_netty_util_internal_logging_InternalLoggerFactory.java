package com.neo4j.fabric.shaded.driver.internal.svm;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.JdkLoggerFactory;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory" )
final class Target_io_netty_util_internal_logging_InternalLoggerFactory
{
    @Substitute
    private static InternalLoggerFactory newDefaultFactory( String name )
    {
        return JdkLoggerFactory.INSTANCE;
    }
}
