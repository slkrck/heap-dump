package com.neo4j.fabric.shaded.driver.internal.svm;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.AbstractBootstrapConfig;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.ChannelFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.DefaultChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.GlobalEventExecutor;
import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.AbstractBootstrap" )
final class Target_io_netty_bootstrap_AbstractBootstrap
{
    @Alias
    private ChannelFactory channelFactory;

    @Alias
    void init( Channel channel ) throws Exception
    {
    }

    @Alias
    public AbstractBootstrapConfig config()
    {
        return null;
    }

    @Substitute
    final ChannelFuture initAndRegister()
    {
        Channel channel = null;

        try
        {
            channel = this.channelFactory.newChannel();
            this.init( channel );
        }
        catch ( Throwable var3 )
        {
            var3.printStackTrace();
            if ( channel != null )
            {
                channel.unsafe().closeForcibly();
            }

            return (new DefaultChannelPromise( channel, GlobalEventExecutor.INSTANCE )).setFailure( var3 );
        }

        ChannelFuture regFuture = this.config().group().register( channel );
        if ( regFuture.cause() != null )
        {
            if ( channel.isRegistered() )
            {
                channel.close();
            }
            else
            {
                channel.unsafe().closeForcibly();
            }
        }

        return regFuture;
    }
}
