package com.neo4j.fabric.shaded.driver.internal.svm;

import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.TargetClass;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.JdkDefaultApplicationProtocolNegotiator" )
final class Target_io_netty_handler_ssl_JdkDefaultApplicationProtocolNegotiator
{
    @Alias
    public static Target_io_netty_handler_ssl_JdkDefaultApplicationProtocolNegotiator INSTANCE;
}
