package com.neo4j.fabric.shaded.driver.internal.svm;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.JdkApplicationProtocolNegotiator;
import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.TargetClass;
import com.oracle.svm.core.jdk.JDK11OrLater;

import javax.net.ssl.SSLEngine;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.Java9SslEngine", onlyWith = {JDK11OrLater.class} )
final class Target_io_netty_handler_ssl_Java9SslEngine
{
    @Alias
    Target_io_netty_handler_ssl_Java9SslEngine( SSLEngine engine, JdkApplicationProtocolNegotiator applicationNegotiator, boolean isServer )
    {
    }
}
