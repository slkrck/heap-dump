package com.neo4j.fabric.shaded.driver.internal.svm;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.ApplicationProtocolConfig;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.JdkAlpnApplicationProtocolNegotiator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.JdkApplicationProtocolNegotiator;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.JdkSslContext" )
final class Target_io_netty_handler_ssl_JdkSslContext
{
    @Substitute
    static JdkApplicationProtocolNegotiator toNegotiator( ApplicationProtocolConfig config, boolean isServer )
    {
        if ( config == null )
        {
            return (JdkApplicationProtocolNegotiator) Target_io_netty_handler_ssl_JdkDefaultApplicationProtocolNegotiator.INSTANCE;
        }
        else
        {
            switch ( config.protocol() )
            {
            case NONE:
                return (JdkApplicationProtocolNegotiator) Target_io_netty_handler_ssl_JdkDefaultApplicationProtocolNegotiator.INSTANCE;
            case ALPN:
                if ( isServer )
                {
                    ApplicationProtocolConfig.SelectorFailureBehavior behavior = config.selectorFailureBehavior();
                    if ( behavior == ApplicationProtocolConfig.SelectorFailureBehavior.FATAL_ALERT )
                    {
                        return new JdkAlpnApplicationProtocolNegotiator( true, config.supportedProtocols() );
                    }
                    else
                    {
                        if ( behavior == ApplicationProtocolConfig.SelectorFailureBehavior.NO_ADVERTISE )
                        {
                            return new JdkAlpnApplicationProtocolNegotiator( false, config.supportedProtocols() );
                        }

                        throw new UnsupportedOperationException( "JDK provider does not support " + config.selectorFailureBehavior() + " failure behavior" );
                    }
                }
                else
                {
                    switch ( config.selectedListenerFailureBehavior() )
                    {
                    case ACCEPT:
                        return new JdkAlpnApplicationProtocolNegotiator( false, config.supportedProtocols() );
                    case FATAL_ALERT:
                        return new JdkAlpnApplicationProtocolNegotiator( true, config.supportedProtocols() );
                    default:
                        throw new UnsupportedOperationException(
                                "JDK provider does not support " + config.selectedListenerFailureBehavior() + " failure behavior" );
                    }
                }
            default:
                throw new UnsupportedOperationException( "JDK provider does not support " + config.protocol() + " protocol" );
            }
        }
    }
}
