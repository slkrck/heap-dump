package com.neo4j.fabric.shaded.driver.internal.svm;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.ApplicationProtocolConfig;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.CipherSuiteFilter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.ClientAuth;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.SslContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.SslProvider;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManagerFactory;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.SslContext" )
final class Target_io_netty_handler_ssl_SslContext
{
    @Substitute
    static SslContext newServerContextInternal( SslProvider provider, Provider sslContextProvider, X509Certificate[] trustCertCollection,
            TrustManagerFactory trustManagerFactory, X509Certificate[] keyCertChain, PrivateKey key, String keyPassword, KeyManagerFactory keyManagerFactory,
            Iterable<String> ciphers, CipherSuiteFilter cipherFilter, ApplicationProtocolConfig apn, long sessionCacheSize, long sessionTimeout,
            ClientAuth clientAuth, String[] protocols, boolean startTls, boolean enableOcsp, String keyStoreType ) throws SSLException
    {
        if ( enableOcsp )
        {
            throw new IllegalArgumentException( "OCSP is not supported with this SslProvider: " + provider );
        }
        else
        {
            return (SslContext) (new Target_io_netty_handler_ssl_JdkSslServerContext( sslContextProvider, trustCertCollection, trustManagerFactory,
                    keyCertChain, key, keyPassword, keyManagerFactory, ciphers, cipherFilter, apn, sessionCacheSize, sessionTimeout, clientAuth, protocols,
                    startTls, keyStoreType ));
        }
    }

    @Substitute
    static SslContext newClientContextInternal( SslProvider provider, Provider sslContextProvider, X509Certificate[] trustCert,
            TrustManagerFactory trustManagerFactory, X509Certificate[] keyCertChain, PrivateKey key, String keyPassword, KeyManagerFactory keyManagerFactory,
            Iterable<String> ciphers, CipherSuiteFilter cipherFilter, ApplicationProtocolConfig apn, String[] protocols, long sessionCacheSize,
            long sessionTimeout, boolean enableOcsp, String keyStoreType ) throws SSLException
    {
        if ( enableOcsp )
        {
            throw new IllegalArgumentException( "OCSP is not supported with this SslProvider: " + provider );
        }
        else
        {
            return (SslContext) (new Target_io_netty_handler_ssl_JdkSslClientContext( sslContextProvider, trustCert, trustManagerFactory, keyCertChain, key,
                    keyPassword, keyManagerFactory, ciphers, cipherFilter, apn, protocols, sessionCacheSize, sessionTimeout, keyStoreType ));
        }
    }
}
