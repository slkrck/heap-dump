package com.neo4j.fabric.shaded.driver.internal.svm;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression.JdkZlibDecoder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression.JdkZlibEncoder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression.ZlibDecoder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression.ZlibEncoder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression.ZlibWrapper;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression.ZlibCodecFactory" )
final class Target_org_neo4j_driver_internal_shaded_io_netty_handler_codec_compression_ZlibCodecFactory
{
    @Substitute
    public static ZlibEncoder newZlibEncoder( int compressionLevel )
    {
        return new JdkZlibEncoder( compressionLevel );
    }

    @Substitute
    public static ZlibEncoder newZlibEncoder( ZlibWrapper wrapper )
    {
        return new JdkZlibEncoder( wrapper );
    }

    @Substitute
    public static ZlibEncoder newZlibEncoder( ZlibWrapper wrapper, int compressionLevel )
    {
        return new JdkZlibEncoder( wrapper, compressionLevel );
    }

    @Substitute
    public static ZlibEncoder newZlibEncoder( ZlibWrapper wrapper, int compressionLevel, int windowBits, int memLevel )
    {
        return new JdkZlibEncoder( wrapper, compressionLevel );
    }

    @Substitute
    public static ZlibEncoder newZlibEncoder( byte[] dictionary )
    {
        return new JdkZlibEncoder( dictionary );
    }

    @Substitute
    public static ZlibEncoder newZlibEncoder( int compressionLevel, byte[] dictionary )
    {
        return new JdkZlibEncoder( compressionLevel, dictionary );
    }

    @Substitute
    public static ZlibEncoder newZlibEncoder( int compressionLevel, int windowBits, int memLevel, byte[] dictionary )
    {
        return new JdkZlibEncoder( compressionLevel, dictionary );
    }

    @Substitute
    public static ZlibDecoder newZlibDecoder()
    {
        return new JdkZlibDecoder();
    }

    @Substitute
    public static ZlibDecoder newZlibDecoder( ZlibWrapper wrapper )
    {
        return new JdkZlibDecoder( wrapper );
    }

    @Substitute
    public static ZlibDecoder newZlibDecoder( byte[] dictionary )
    {
        return new JdkZlibDecoder( dictionary );
    }
}
