package com.neo4j.fabric.shaded.driver.internal.svm;

import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.nio.NioEventLoop" )
final class Target_io_netty_channel_nio_NioEventLoop
{
    @Substitute
    private static Queue<Runnable> newTaskQueue0( int maxPendingTasks )
    {
        return new LinkedBlockingDeque();
    }
}
