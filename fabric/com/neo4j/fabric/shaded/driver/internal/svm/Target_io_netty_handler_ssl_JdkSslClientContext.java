package com.neo4j.fabric.shaded.driver.internal.svm;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.ApplicationProtocolConfig;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.CipherSuiteFilter;
import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.TargetClass;

import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManagerFactory;

@TargetClass( className = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.JdkSslClientContext" )
final class Target_io_netty_handler_ssl_JdkSslClientContext
{
    @Alias
    Target_io_netty_handler_ssl_JdkSslClientContext( Provider sslContextProvider, X509Certificate[] trustCertCollection,
            TrustManagerFactory trustManagerFactory, X509Certificate[] keyCertChain, PrivateKey key, String keyPassword, KeyManagerFactory keyManagerFactory,
            Iterable<String> ciphers, CipherSuiteFilter cipherFilter, ApplicationProtocolConfig apn, String[] protocols, long sessionCacheSize,
            long sessionTimeout, String keyStoreType ) throws SSLException
    {
    }
}
