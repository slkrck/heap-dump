package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.net.ServerAddress;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BoltServerAddress implements ServerAddress
{
    public static final int DEFAULT_PORT = 7687;
    public static final BoltServerAddress LOCAL_DEFAULT = new BoltServerAddress( "localhost", 7687 );
    private final String host;
    private final int port;
    private final String stringValue;
    private InetAddress resolved;

    public BoltServerAddress( String address )
    {
        this( uriFrom( address ) );
    }

    public BoltServerAddress( URI uri )
    {
        this( hostFrom( uri ), portFrom( uri ) );
    }

    public BoltServerAddress( String host, int port )
    {
        this( host, (InetAddress) null, port );
    }

    private BoltServerAddress( String host, InetAddress resolved, int port )
    {
        this.host = (String) Objects.requireNonNull( host, "host" );
        this.resolved = resolved;
        this.port = requireValidPort( port );
        this.stringValue = resolved != null ? String.format( "%s(%s):%d", host, resolved.getHostAddress(), port ) : String.format( "%s:%d", host, port );
    }

    public static BoltServerAddress from( ServerAddress address )
    {
        return address instanceof BoltServerAddress ? (BoltServerAddress) address : new BoltServerAddress( address.host(), address.port() );
    }

    private static String hostFrom( URI uri )
    {
        String host = uri.getHost();
        if ( host == null )
        {
            throw invalidAddressFormat( uri );
        }
        else
        {
            return host;
        }
    }

    private static int portFrom( URI uri )
    {
        int port = uri.getPort();
        return port == -1 ? 7687 : port;
    }

    private static URI uriFrom( String address )
    {
        String[] schemeSplit = address.split( "://" );
        String scheme;
        String hostPort;
        if ( schemeSplit.length == 1 )
        {
            scheme = "bolt://";
            hostPort = hostPortFrom( schemeSplit[0] );
        }
        else
        {
            if ( schemeSplit.length != 2 )
            {
                throw invalidAddressFormat( address );
            }

            scheme = schemeSplit[0] + "://";
            hostPort = hostPortFrom( schemeSplit[1] );
        }

        return URI.create( scheme + hostPort );
    }

    private static String hostPortFrom( String address )
    {
        if ( address.startsWith( "[" ) )
        {
            return address;
        }
        else
        {
            boolean containsSingleColon = address.indexOf( ":" ) == address.lastIndexOf( ":" );
            return containsSingleColon ? address : "[" + address + "]";
        }
    }

    private static RuntimeException invalidAddressFormat( URI uri )
    {
        return invalidAddressFormat( uri.toString() );
    }

    private static RuntimeException invalidAddressFormat( String address )
    {
        return new IllegalArgumentException( "Invalid address format `" + address + "`" );
    }

    private static int requireValidPort( int port )
    {
        if ( port >= 0 && port <= 65535 )
        {
            return port;
        }
        else
        {
            throw new IllegalArgumentException( "Illegal port: " + port );
        }
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            BoltServerAddress that = (BoltServerAddress) o;
            return this.port == that.port && this.host.equals( that.host );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.host, this.port} );
    }

    public String toString()
    {
        return this.stringValue;
    }

    public SocketAddress toSocketAddress()
    {
        return this.resolved == null ? new InetSocketAddress( this.host, this.port ) : new InetSocketAddress( this.resolved, this.port );
    }

    public BoltServerAddress resolve() throws UnknownHostException
    {
        return new BoltServerAddress( this.host, InetAddress.getByName( this.host ), this.port );
    }

    public List<BoltServerAddress> resolveAll() throws UnknownHostException
    {
        return (List) Stream.of( InetAddress.getAllByName( this.host ) ).map( ( address ) -> {
            return new BoltServerAddress( this.host, address, this.port );
        } ).collect( Collectors.toList() );
    }

    public String host()
    {
        return this.host;
    }

    public int port()
    {
        return this.port;
    }
}
