package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.summary.ProfiledPlan;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class InternalProfiledPlan extends InternalPlan<ProfiledPlan> implements ProfiledPlan
{
    public static final InternalPlan.PlanCreator<ProfiledPlan> PROFILED_PLAN = new InternalPlan.PlanCreator<ProfiledPlan>()
    {
        public ProfiledPlan create( String operatorType, Map<String,Value> arguments, List<String> identifiers, List<ProfiledPlan> children,
                Value originalPlanValue )
        {
            return new InternalProfiledPlan( operatorType, arguments, identifiers, children, originalPlanValue.get( "dbHits" ).asLong( 0L ),
                    originalPlanValue.get( "rows" ).asLong( 0L ), originalPlanValue.get( "pageCacheHits" ).asLong( 0L ),
                    originalPlanValue.get( "pageCacheMisses" ).asLong( 0L ), originalPlanValue.get( "pageCacheHitRatio" ).asDouble( 0.0D ),
                    originalPlanValue.get( "time" ).asLong( 0L ) );
        }
    };
    public static final Function<Value,ProfiledPlan> PROFILED_PLAN_FROM_VALUE;

    static
    {
        PROFILED_PLAN_FROM_VALUE = new InternalPlan.Converter( PROFILED_PLAN );
    }

    private final long dbHits;
    private final long records;
    private final long pageCacheHits;
    private final long pageCacheMisses;
    private final double pageCacheHitRatio;
    private final long time;

    protected InternalProfiledPlan( String operatorType, Map<String,Value> arguments, List<String> identifiers, List<ProfiledPlan> children, long dbHits,
            long records, long pageCacheHits, long pageCacheMisses, double pageCacheHitRatio, long time )
    {
        super( operatorType, arguments, identifiers, children );
        this.dbHits = dbHits;
        this.records = records;
        this.pageCacheHits = pageCacheHits;
        this.pageCacheMisses = pageCacheMisses;
        this.pageCacheHitRatio = pageCacheHitRatio;
        this.time = time;
    }

    public long dbHits()
    {
        return this.dbHits;
    }

    public long records()
    {
        return this.records;
    }

    public boolean hasPageCacheStats()
    {
        return this.pageCacheHits > 0L || this.pageCacheMisses > 0L || this.pageCacheHitRatio > 0.0D;
    }

    public long pageCacheHits()
    {
        return this.pageCacheHits;
    }

    public long pageCacheMisses()
    {
        return this.pageCacheMisses;
    }

    public double pageCacheHitRatio()
    {
        return this.pageCacheHitRatio;
    }

    public long time()
    {
        return this.time;
    }
}
