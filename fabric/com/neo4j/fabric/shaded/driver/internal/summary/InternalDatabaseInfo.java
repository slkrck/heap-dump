package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.summary.DatabaseInfo;

import java.util.Objects;

public class InternalDatabaseInfo implements DatabaseInfo
{
    public static DatabaseInfo DEFAULT_DATABASE_INFO = new InternalDatabaseInfo( (String) null );
    private final String name;

    public InternalDatabaseInfo( String name )
    {
        this.name = name;
    }

    public String name()
    {
        return this.name;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalDatabaseInfo that = (InternalDatabaseInfo) o;
            return Objects.equals( this.name, that.name );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.name} );
    }

    public String toString()
    {
        return "InternalDatabaseInfo{name='" + this.name + '\'' + '}';
    }
}
