package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.summary.InputPosition;

import java.util.Objects;

public class InternalInputPosition implements InputPosition
{
    private final int offset;
    private final int line;
    private final int column;

    public InternalInputPosition( int offset, int line, int column )
    {
        this.offset = offset;
        this.line = line;
        this.column = column;
    }

    public int offset()
    {
        return this.offset;
    }

    public int line()
    {
        return this.line;
    }

    public int column()
    {
        return this.column;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalInputPosition that = (InternalInputPosition) o;
            return this.offset == that.offset && this.line == that.line && this.column == that.column;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.offset, this.line, this.column} );
    }

    public String toString()
    {
        return "offset=" + this.offset + ", line=" + this.line + ", column=" + this.column;
    }
}
