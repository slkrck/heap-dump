package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.summary.DatabaseInfo;
import com.neo4j.fabric.shaded.driver.summary.Notification;
import com.neo4j.fabric.shaded.driver.summary.Plan;
import com.neo4j.fabric.shaded.driver.summary.ProfiledPlan;
import com.neo4j.fabric.shaded.driver.summary.QueryType;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;
import com.neo4j.fabric.shaded.driver.summary.ServerInfo;
import com.neo4j.fabric.shaded.driver.summary.SummaryCounters;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class InternalResultSummary implements ResultSummary
{
    private final Query query;
    private final ServerInfo serverInfo;
    private final QueryType queryType;
    private final SummaryCounters counters;
    private final Plan plan;
    private final ProfiledPlan profile;
    private final List<Notification> notifications;
    private final long resultAvailableAfter;
    private final long resultConsumedAfter;
    private final DatabaseInfo databaseInfo;

    public InternalResultSummary( Query query, ServerInfo serverInfo, DatabaseInfo databaseInfo, QueryType queryType, SummaryCounters counters, Plan plan,
            ProfiledPlan profile, List<Notification> notifications, long resultAvailableAfter, long resultConsumedAfter )
    {
        this.query = query;
        this.serverInfo = serverInfo;
        this.databaseInfo = databaseInfo;
        this.queryType = queryType;
        this.counters = counters;
        this.plan = resolvePlan( plan, profile );
        this.profile = profile;
        this.notifications = notifications;
        this.resultAvailableAfter = resultAvailableAfter;
        this.resultConsumedAfter = resultConsumedAfter;
    }

    private static Plan resolvePlan( Plan plan, ProfiledPlan profiledPlan )
    {
        return (Plan) (plan == null ? profiledPlan : plan);
    }

    public Query query()
    {
        return this.query;
    }

    public SummaryCounters counters()
    {
        return (SummaryCounters) (this.counters == null ? InternalSummaryCounters.EMPTY_STATS : this.counters);
    }

    public QueryType queryType()
    {
        return this.queryType;
    }

    public boolean hasPlan()
    {
        return this.plan != null;
    }

    public boolean hasProfile()
    {
        return this.profile != null;
    }

    public Plan plan()
    {
        return this.plan;
    }

    public ProfiledPlan profile()
    {
        return this.profile;
    }

    public List<Notification> notifications()
    {
        return this.notifications == null ? Collections.emptyList() : this.notifications;
    }

    public long resultAvailableAfter( TimeUnit unit )
    {
        return this.resultAvailableAfter == -1L ? this.resultAvailableAfter : unit.convert( this.resultAvailableAfter, TimeUnit.MILLISECONDS );
    }

    public long resultConsumedAfter( TimeUnit unit )
    {
        return this.resultConsumedAfter == -1L ? this.resultConsumedAfter : unit.convert( this.resultConsumedAfter, TimeUnit.MILLISECONDS );
    }

    public ServerInfo server()
    {
        return this.serverInfo;
    }

    public DatabaseInfo database()
    {
        return this.databaseInfo;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalResultSummary that = (InternalResultSummary) o;
            return this.resultAvailableAfter == that.resultAvailableAfter && this.resultConsumedAfter == that.resultConsumedAfter &&
                    Objects.equals( this.query, that.query ) && Objects.equals( this.serverInfo, that.serverInfo ) && this.queryType == that.queryType &&
                    Objects.equals( this.counters, that.counters ) && Objects.equals( this.plan, that.plan ) && Objects.equals( this.profile, that.profile ) &&
                    Objects.equals( this.notifications, that.notifications );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash(
                new Object[]{this.query, this.serverInfo, this.queryType, this.counters, this.plan, this.profile, this.notifications, this.resultAvailableAfter,
                        this.resultConsumedAfter} );
    }

    public String toString()
    {
        return "InternalResultSummary{query=" + this.query + ", serverInfo=" + this.serverInfo + ", databaseInfo=" + this.databaseInfo + ", queryType=" +
                this.queryType + ", counters=" + this.counters + ", plan=" + this.plan + ", profile=" + this.profile + ", notifications=" + this.notifications +
                ", resultAvailableAfter=" + this.resultAvailableAfter + ", resultConsumedAfter=" + this.resultConsumedAfter + '}';
    }
}
