package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.value.NullValue;
import com.neo4j.fabric.shaded.driver.summary.InputPosition;
import com.neo4j.fabric.shaded.driver.summary.Notification;

import java.util.function.Function;

public class InternalNotification implements Notification
{
    public static final Function<Value,Notification> VALUE_TO_NOTIFICATION = new Function<Value,Notification>()
    {
        public Notification apply( Value value )
        {
            String code = value.get( "code" ).asString();
            String title = value.get( "title" ).asString();
            String description = value.get( "description" ).asString();
            String severity = value.containsKey( "severity" ) ? value.get( "severity" ).asString() : "N/A";
            Value posValue = value.get( "position" );
            InputPosition position = null;
            if ( posValue != NullValue.NULL )
            {
                position = new InternalInputPosition( posValue.get( "offset" ).asInt(), posValue.get( "line" ).asInt(), posValue.get( "column" ).asInt() );
            }

            return new InternalNotification( code, title, description, severity, position );
        }
    };
    private final String code;
    private final String title;
    private final String description;
    private final String severity;
    private final InputPosition position;

    public InternalNotification( String code, String title, String description, String severity, InputPosition position )
    {
        this.code = code;
        this.title = title;
        this.description = description;
        this.severity = severity;
        this.position = position;
    }

    public String code()
    {
        return this.code;
    }

    public String title()
    {
        return this.title;
    }

    public String description()
    {
        return this.description;
    }

    public InputPosition position()
    {
        return this.position;
    }

    public String severity()
    {
        return this.severity;
    }

    public String toString()
    {
        String info = "code=" + this.code + ", title=" + this.title + ", description=" + this.description + ", severity=" + this.severity;
        return this.position == null ? info : info + ", position={" + this.position + "}";
    }
}
