package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.summary.Plan;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class InternalPlan<T extends Plan> implements Plan
{
    public static final InternalPlan.PlanCreator<Plan> EXPLAIN_PLAN = new InternalPlan.PlanCreator<Plan>()
    {
        public Plan create( String operatorType, Map<String,Value> arguments, List<String> identifiers, List<Plan> children, Value originalPlanValue )
        {
            return new InternalPlan( operatorType, arguments, identifiers, children );
        }
    };
    public static final Function<Value,Plan> EXPLAIN_PLAN_FROM_VALUE;

    static
    {
        EXPLAIN_PLAN_FROM_VALUE = new InternalPlan.Converter( EXPLAIN_PLAN );
    }

    private final String operatorType;
    private final List<String> identifiers;
    private final Map<String,Value> arguments;
    private final List<T> children;

    protected InternalPlan( String operatorType, Map<String,Value> arguments, List<String> identifiers, List<T> children )
    {
        this.operatorType = operatorType;
        this.identifiers = identifiers;
        this.arguments = arguments;
        this.children = children;
    }

    public static Plan plan( String operatorType, Map<String,Value> arguments, List<String> identifiers, List<Plan> children )
    {
        return EXPLAIN_PLAN.create( operatorType, arguments, identifiers, children, (Value) null );
    }

    public String operatorType()
    {
        return this.operatorType;
    }

    public List<String> identifiers()
    {
        return this.identifiers;
    }

    public Map<String,Value> arguments()
    {
        return this.arguments;
    }

    public List<T> children()
    {
        return this.children;
    }

    public String toString()
    {
        return String.format( "SimplePlanTreeNode{operatorType='%s', arguments=%s, identifiers=%s, children=%s}", this.operatorType, this.arguments,
                this.identifiers, this.children );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalPlan that = (InternalPlan) o;
            return this.operatorType.equals( that.operatorType ) && this.arguments.equals( that.arguments ) && this.identifiers.equals( that.identifiers ) &&
                    this.children.equals( that.children );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.operatorType.hashCode();
        result = 31 * result + this.identifiers.hashCode();
        result = 31 * result + this.arguments.hashCode();
        result = 31 * result + this.children.hashCode();
        return result;
    }

    interface PlanCreator<T extends Plan>
    {
        T create( String var1, Map<String,Value> var2, List<String> var3, List<T> var4, Value var5 );
    }

    static class Converter<T extends Plan> implements Function<Value,T>
    {
        private final InternalPlan.PlanCreator<T> planCreator;

        public Converter( InternalPlan.PlanCreator<T> planCreator )
        {
            this.planCreator = planCreator;
        }

        public T apply( Value plan )
        {
            String operatorType = plan.get( "operatorType" ).asString();
            Value argumentsValue = plan.get( "args" );
            Map<String,Value> arguments = argumentsValue.isNull() ? Collections.emptyMap() : argumentsValue.asMap( Values.ofValue() );
            Value identifiersValue = plan.get( "identifiers" );
            List<String> identifiers = identifiersValue.isNull() ? Collections.emptyList() : identifiersValue.asList( Values.ofString() );
            Value childrenValue = plan.get( "children" );
            List<T> children = childrenValue.isNull() ? Collections.emptyList() : childrenValue.asList( (Function) this );
            return this.planCreator.create( operatorType, arguments, identifiers, children, plan );
        }
    }
}
