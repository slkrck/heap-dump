package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.summary.SummaryCounters;

public class InternalSummaryCounters implements SummaryCounters
{
    public static final InternalSummaryCounters EMPTY_STATS = new InternalSummaryCounters( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
    private final int nodesCreated;
    private final int nodesDeleted;
    private final int relationshipsCreated;
    private final int relationshipsDeleted;
    private final int propertiesSet;
    private final int labelsAdded;
    private final int labelsRemoved;
    private final int indexesAdded;
    private final int indexesRemoved;
    private final int constraintsAdded;
    private final int constraintsRemoved;
    private final int systemUpdates;

    public InternalSummaryCounters( int nodesCreated, int nodesDeleted, int relationshipsCreated, int relationshipsDeleted, int propertiesSet, int labelsAdded,
            int labelsRemoved, int indexesAdded, int indexesRemoved, int constraintsAdded, int constraintsRemoved, int systemUpdates )
    {
        this.nodesCreated = nodesCreated;
        this.nodesDeleted = nodesDeleted;
        this.relationshipsCreated = relationshipsCreated;
        this.relationshipsDeleted = relationshipsDeleted;
        this.propertiesSet = propertiesSet;
        this.labelsAdded = labelsAdded;
        this.labelsRemoved = labelsRemoved;
        this.indexesAdded = indexesAdded;
        this.indexesRemoved = indexesRemoved;
        this.constraintsAdded = constraintsAdded;
        this.constraintsRemoved = constraintsRemoved;
        this.systemUpdates = systemUpdates;
    }

    public boolean containsUpdates()
    {
        return this.isPositive( this.nodesCreated ) || this.isPositive( this.nodesDeleted ) || this.isPositive( this.relationshipsCreated ) ||
                this.isPositive( this.relationshipsDeleted ) || this.isPositive( this.propertiesSet ) || this.isPositive( this.labelsAdded ) ||
                this.isPositive( this.labelsRemoved ) || this.isPositive( this.indexesAdded ) || this.isPositive( this.indexesRemoved ) ||
                this.isPositive( this.constraintsAdded ) || this.isPositive( this.constraintsRemoved );
    }

    public int nodesCreated()
    {
        return this.nodesCreated;
    }

    public int nodesDeleted()
    {
        return this.nodesDeleted;
    }

    public int relationshipsCreated()
    {
        return this.relationshipsCreated;
    }

    public int relationshipsDeleted()
    {
        return this.relationshipsDeleted;
    }

    public int propertiesSet()
    {
        return this.propertiesSet;
    }

    public int labelsAdded()
    {
        return this.labelsAdded;
    }

    public int labelsRemoved()
    {
        return this.labelsRemoved;
    }

    public int indexesAdded()
    {
        return this.indexesAdded;
    }

    public int indexesRemoved()
    {
        return this.indexesRemoved;
    }

    public int constraintsAdded()
    {
        return this.constraintsAdded;
    }

    public int constraintsRemoved()
    {
        return this.constraintsRemoved;
    }

    public boolean containsSystemUpdates()
    {
        return this.isPositive( this.systemUpdates );
    }

    public int systemUpdates()
    {
        return this.systemUpdates;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalSummaryCounters that = (InternalSummaryCounters) o;
            return this.nodesCreated == that.nodesCreated && this.nodesDeleted == that.nodesDeleted && this.relationshipsCreated == that.relationshipsCreated &&
                    this.relationshipsDeleted == that.relationshipsDeleted && this.propertiesSet == that.propertiesSet &&
                    this.labelsAdded == that.labelsAdded && this.labelsRemoved == that.labelsRemoved && this.indexesAdded == that.indexesAdded &&
                    this.indexesRemoved == that.indexesRemoved && this.constraintsAdded == that.constraintsAdded &&
                    this.constraintsRemoved == that.constraintsRemoved && this.systemUpdates == that.systemUpdates;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.nodesCreated;
        result = 31 * result + this.nodesDeleted;
        result = 31 * result + this.relationshipsCreated;
        result = 31 * result + this.relationshipsDeleted;
        result = 31 * result + this.propertiesSet;
        result = 31 * result + this.labelsAdded;
        result = 31 * result + this.labelsRemoved;
        result = 31 * result + this.indexesAdded;
        result = 31 * result + this.indexesRemoved;
        result = 31 * result + this.constraintsAdded;
        result = 31 * result + this.constraintsRemoved;
        result = 31 * result + this.systemUpdates;
        return result;
    }

    private boolean isPositive( int value )
    {
        return value > 0;
    }
}
