package com.neo4j.fabric.shaded.driver.internal.summary;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;
import com.neo4j.fabric.shaded.driver.summary.ServerInfo;

import java.util.Objects;

public class InternalServerInfo implements ServerInfo
{
    private final String address;
    private final String version;

    public InternalServerInfo( BoltServerAddress address, ServerVersion version )
    {
        this.address = address.toString();
        this.version = version.toString();
    }

    public String address()
    {
        return this.address;
    }

    public String version()
    {
        return this.version;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalServerInfo that = (InternalServerInfo) o;
            return Objects.equals( this.address, that.address ) && Objects.equals( this.version, that.version );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.address, this.version} );
    }

    public String toString()
    {
        return "InternalServerInfo{address='" + this.address + '\'' + ", version='" + this.version + '\'' + '}';
    }
}
