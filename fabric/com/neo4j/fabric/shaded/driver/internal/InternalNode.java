package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.value.NodeValue;
import com.neo4j.fabric.shaded.driver.types.Node;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class InternalNode extends InternalEntity implements Node
{
    private final Collection<String> labels;

    public InternalNode( long id )
    {
        this( id, Collections.emptyList(), Collections.emptyMap() );
    }

    public InternalNode( long id, Collection<String> labels, Map<String,Value> properties )
    {
        super( id, properties );
        this.labels = labels;
    }

    public Collection<String> labels()
    {
        return this.labels;
    }

    public boolean hasLabel( String label )
    {
        return this.labels.contains( label );
    }

    public Value asValue()
    {
        return new NodeValue( this );
    }

    public String toString()
    {
        return String.format( "node<%s>", this.id() );
    }
}
