package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Value;

public interface AsValue
{
    Value asValue();
}
