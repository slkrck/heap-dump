package com.neo4j.fabric.shaded.driver.internal.reactive;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.cursor.RxResultCursor;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.reactive.RxResult;
import com.neo4j.fabric.shaded.driver.reactive.RxTransaction;

import java.util.concurrent.CompletableFuture;

import org.reactivestreams.Publisher;

public class InternalRxTransaction extends AbstractRxQueryRunner implements RxTransaction
{
    private final UnmanagedTransaction tx;

    public InternalRxTransaction( UnmanagedTransaction tx )
    {
        this.tx = tx;
    }

    public RxResult run( Query query )
    {
        return new InternalRxResult( () -> {
            CompletableFuture<RxResultCursor> cursorFuture = new CompletableFuture();
            this.tx.runRx( query ).whenComplete( ( cursor, completionError ) -> {
                if ( cursor != null )
                {
                    cursorFuture.complete( cursor );
                }
                else
                {
                    Throwable error = Futures.completionExceptionCause( completionError );
                    this.tx.markTerminated();
                    cursorFuture.completeExceptionally( error );
                }
            } );
            return cursorFuture;
        } );
    }

    public <T> Publisher<T> commit()
    {
        return this.close( true );
    }

    public <T> Publisher<T> rollback()
    {
        return this.close( false );
    }

    private <T> Publisher<T> close( boolean commit )
    {
        return RxUtils.createEmptyPublisher( () -> {
            return commit ? this.tx.commitAsync() : this.tx.rollbackAsync();
        } );
    }
}
