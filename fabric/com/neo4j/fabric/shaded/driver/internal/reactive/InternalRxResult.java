package com.neo4j.fabric.shaded.driver.internal.reactive;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.internal.cursor.RxResultCursor;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher.Flux;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher.FluxSink;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher.Mono;
import com.neo4j.fabric.shaded.driver.internal.util.ErrorUtil;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.reactive.RxResult;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;

public class InternalRxResult implements RxResult
{
    private Supplier<CompletionStage<RxResultCursor>> cursorFutureSupplier;
    private volatile CompletionStage<RxResultCursor> cursorFuture;

    public InternalRxResult( Supplier<CompletionStage<RxResultCursor>> cursorFuture )
    {
        this.cursorFutureSupplier = cursorFuture;
    }

    public Publisher<List<String>> keys()
    {
        return Mono.defer( () -> {
            return Mono.fromCompletionStage( this.getCursorFuture() ).map( RxResultCursor::keys ).onErrorMap( Futures::completionExceptionCause );
        } );
    }

    public Publisher<Record> records()
    {
        return Flux.create( ( sink ) -> {
            this.getCursorFuture().whenComplete( ( cursor, completionError ) -> {
                if ( cursor != null )
                {
                    if ( cursor.isDone() )
                    {
                        sink.error( ErrorUtil.newResultConsumedError() );
                    }
                    else
                    {
                        cursor.installRecordConsumer( this.createRecordConsumer( sink ) );
                        sink.onCancel( cursor::cancel );
                        sink.onRequest( cursor::request );
                    }
                }
                else
                {
                    Throwable error = Futures.completionExceptionCause( completionError );
                    sink.error( error );
                }
            } );
        }, FluxSink.OverflowStrategy.IGNORE );
    }

    private BiConsumer<Record,Throwable> createRecordConsumer( FluxSink<Record> sink )
    {
        return ( r, e ) -> {
            if ( r != null )
            {
                sink.next( r );
            }
            else if ( e != null )
            {
                sink.error( e );
            }
            else
            {
                sink.complete();
            }
        };
    }

    private CompletionStage<RxResultCursor> getCursorFuture()
    {
        return this.cursorFuture != null ? this.cursorFuture : this.initCursorFuture();
    }

    synchronized CompletionStage<RxResultCursor> initCursorFuture()
    {
        if ( this.cursorFuture != null )
        {
            return this.cursorFuture;
        }
        else
        {
            this.cursorFuture = (CompletionStage) this.cursorFutureSupplier.get();
            this.cursorFutureSupplier = null;
            return this.cursorFuture;
        }
    }

    public Publisher<ResultSummary> consume()
    {
        return Mono.create( ( sink ) -> {
            this.getCursorFuture().whenComplete( ( cursor, completionError ) -> {
                if ( cursor != null )
                {
                    cursor.summaryAsync().whenComplete( ( summary, summaryCompletionError ) -> {
                        Throwable error = Futures.completionExceptionCause( summaryCompletionError );
                        if ( summary != null )
                        {
                            sink.success( summary );
                        }
                        else
                        {
                            sink.error( error );
                        }
                    } );
                }
                else
                {
                    Throwable error = Futures.completionExceptionCause( completionError );
                    sink.error( error );
                }
            } );
        } );
    }

    Supplier<CompletionStage<RxResultCursor>> cursorFutureSupplier()
    {
        return this.cursorFutureSupplier;
    }
}
