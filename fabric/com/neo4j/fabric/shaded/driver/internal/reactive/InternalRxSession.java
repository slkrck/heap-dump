package com.neo4j.fabric.shaded.driver.internal.reactive;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.exceptions.TransactionNestingException;
import com.neo4j.fabric.shaded.driver.internal.async.NetworkSession;
import com.neo4j.fabric.shaded.driver.internal.cursor.RxResultCursor;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher.Flux;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.reactive.RxResult;
import com.neo4j.fabric.shaded.driver.reactive.RxSession;
import com.neo4j.fabric.shaded.driver.reactive.RxTransaction;
import com.neo4j.fabric.shaded.driver.reactive.RxTransactionWork;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.reactivestreams.Publisher;

public class InternalRxSession extends AbstractRxQueryRunner implements RxSession
{
    private final NetworkSession session;

    public InternalRxSession( NetworkSession session )
    {
        this.session = session;
    }

    public Publisher<RxTransaction> beginTransaction()
    {
        return this.beginTransaction( TransactionConfig.empty() );
    }

    public Publisher<RxTransaction> beginTransaction( TransactionConfig config )
    {
        return RxUtils.createMono( () -> {
            CompletableFuture<RxTransaction> txFuture = new CompletableFuture();
            this.session.beginTransactionAsync( config ).whenComplete( ( tx, completionError ) -> {
                if ( tx != null )
                {
                    txFuture.complete( new InternalRxTransaction( tx ) );
                }
                else
                {
                    this.releaseConnectionBeforeReturning( txFuture, completionError );
                }
            } );
            return txFuture;
        } );
    }

    private Publisher<RxTransaction> beginTransaction( AccessMode mode, TransactionConfig config )
    {
        return RxUtils.createMono( () -> {
            CompletableFuture<RxTransaction> txFuture = new CompletableFuture();
            this.session.beginTransactionAsync( mode, config ).whenComplete( ( tx, completionError ) -> {
                if ( tx != null )
                {
                    txFuture.complete( new InternalRxTransaction( tx ) );
                }
                else
                {
                    this.releaseConnectionBeforeReturning( txFuture, completionError );
                }
            } );
            return txFuture;
        } );
    }

    public <T> Publisher<T> readTransaction( RxTransactionWork<? extends Publisher<T>> work )
    {
        return this.readTransaction( work, TransactionConfig.empty() );
    }

    public <T> Publisher<T> readTransaction( RxTransactionWork<? extends Publisher<T>> work, TransactionConfig config )
    {
        return this.runTransaction( AccessMode.READ, work, config );
    }

    public <T> Publisher<T> writeTransaction( RxTransactionWork<? extends Publisher<T>> work )
    {
        return this.writeTransaction( work, TransactionConfig.empty() );
    }

    public <T> Publisher<T> writeTransaction( RxTransactionWork<? extends Publisher<T>> work, TransactionConfig config )
    {
        return this.runTransaction( AccessMode.WRITE, work, config );
    }

    private <T> Publisher<T> runTransaction( AccessMode mode, RxTransactionWork<? extends Publisher<T>> work, TransactionConfig config )
    {
        Flux<T> repeatableWork = Flux.usingWhen( this.beginTransaction( mode, config ), work::execute, RxTransaction::commit, (BiFunction) (( tx, error ) -> {
            return tx.rollback();
        }), (Function) null );
        return this.session.retryLogic().retryRx( repeatableWork );
    }

    public RxResult run( String query, TransactionConfig config )
    {
        return this.run( new Query( query ), config );
    }

    public RxResult run( String query, Map<String,Object> parameters, TransactionConfig config )
    {
        return this.run( new Query( query, parameters ), config );
    }

    public RxResult run( Query query )
    {
        return this.run( query, TransactionConfig.empty() );
    }

    public RxResult run( Query query, TransactionConfig config )
    {
        return new InternalRxResult( () -> {
            CompletableFuture<RxResultCursor> resultCursorFuture = new CompletableFuture();
            this.session.runRx( query, config ).whenComplete( ( cursor, completionError ) -> {
                if ( cursor != null )
                {
                    resultCursorFuture.complete( cursor );
                }
                else
                {
                    this.releaseConnectionBeforeReturning( resultCursorFuture, completionError );
                }
            } );
            return resultCursorFuture;
        } );
    }

    private <T> void releaseConnectionBeforeReturning( CompletableFuture<T> returnFuture, Throwable completionError )
    {
        Throwable error = Futures.completionExceptionCause( completionError );
        if ( error instanceof TransactionNestingException )
        {
            returnFuture.completeExceptionally( error );
        }
        else
        {
            this.session.releaseConnectionAsync().whenComplete( ( ignored, closeError ) -> {
                returnFuture.completeExceptionally( Futures.combineErrors( error, closeError ) );
            } );
        }
    }

    public Bookmark lastBookmark()
    {
        return this.session.lastBookmark();
    }

    public Publisher<Void> reset()
    {
        NetworkSession var10000 = this.session;
        var10000.getClass();
        return RxUtils.createEmptyPublisher( var10000::resetAsync );
    }

    public <T> Publisher<T> close()
    {
        NetworkSession var10000 = this.session;
        var10000.getClass();
        return RxUtils.createEmptyPublisher( var10000::closeAsync );
    }
}
