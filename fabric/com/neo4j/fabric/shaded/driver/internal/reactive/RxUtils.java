package com.neo4j.fabric.shaded.driver.internal.reactive;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher.Mono;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.concurrent.CompletionStage;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;

public class RxUtils
{
    public static <T> Publisher<T> createEmptyPublisher( Supplier<CompletionStage<Void>> supplier )
    {
        return Mono.create( ( sink ) -> {
            ((CompletionStage) supplier.get()).whenComplete( ( ignore, completionError ) -> {
                Throwable error = Futures.completionExceptionCause( completionError );
                if ( error != null )
                {
                    sink.error( error );
                }
                else
                {
                    sink.success();
                }
            } );
        } );
    }

    public static <T> Publisher<T> createMono( Supplier<CompletionStage<T>> supplier )
    {
        return Mono.create( ( sink ) -> {
            ((CompletionStage) supplier.get()).whenComplete( ( item, completionError ) -> {
                Throwable error = Futures.completionExceptionCause( completionError );
                if ( item != null )
                {
                    sink.success( item );
                }

                if ( error != null )
                {
                    sink.error( error );
                }
                else
                {
                    sink.success();
                }
            } );
        } );
    }
}
