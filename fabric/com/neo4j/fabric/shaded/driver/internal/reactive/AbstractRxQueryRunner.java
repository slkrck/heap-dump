package com.neo4j.fabric.shaded.driver.internal.reactive;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.AbstractQueryRunner;
import com.neo4j.fabric.shaded.driver.reactive.RxQueryRunner;
import com.neo4j.fabric.shaded.driver.reactive.RxResult;

import java.util.Map;

public abstract class AbstractRxQueryRunner implements RxQueryRunner
{
    public final RxResult run( String query, Value parameters )
    {
        return this.run( new Query( query, parameters ) );
    }

    public final RxResult run( String query, Map<String,Object> parameters )
    {
        return this.run( query, AbstractQueryRunner.parameters( parameters ) );
    }

    public final RxResult run( String query, Record parameters )
    {
        return this.run( query, AbstractQueryRunner.parameters( parameters ) );
    }

    public final RxResult run( String query )
    {
        return this.run( new Query( query ) );
    }
}
