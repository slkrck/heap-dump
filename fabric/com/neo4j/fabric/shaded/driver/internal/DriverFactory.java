package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.AuthToken;
import com.neo4j.fabric.shaded.driver.AuthTokens;
import com.neo4j.fabric.shaded.driver.Config;
import com.neo4j.fabric.shaded.driver.Driver;
import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.async.connection.BootstrapFactory;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelConnector;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelConnectorImpl;
import com.neo4j.fabric.shaded.driver.internal.async.pool.ConnectionPoolImpl;
import com.neo4j.fabric.shaded.driver.internal.async.pool.PoolSettings;
import com.neo4j.fabric.shaded.driver.internal.cluster.IdentityResolver;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingContext;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingSettings;
import com.neo4j.fabric.shaded.driver.internal.cluster.loadbalancing.LeastConnectedLoadBalancingStrategy;
import com.neo4j.fabric.shaded.driver.internal.cluster.loadbalancing.LoadBalancer;
import com.neo4j.fabric.shaded.driver.internal.cluster.loadbalancing.LoadBalancingStrategy;
import com.neo4j.fabric.shaded.driver.internal.logging.NettyLogging;
import com.neo4j.fabric.shaded.driver.internal.metrics.InternalMetricsProvider;
import com.neo4j.fabric.shaded.driver.internal.metrics.MetricsProvider;
import com.neo4j.fabric.shaded.driver.internal.retry.ExponentialBackoffRetryLogic;
import com.neo4j.fabric.shaded.driver.internal.retry.RetryLogic;
import com.neo4j.fabric.shaded.driver.internal.retry.RetrySettings;
import com.neo4j.fabric.shaded.driver.internal.security.SecurityPlan;
import com.neo4j.fabric.shaded.driver.internal.security.SecurityPlanImpl;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.Bootstrap;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutorGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionProvider;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;
import com.neo4j.fabric.shaded.driver.internal.util.ErrorUtil;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.net.ServerAddressResolver;

import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;

public class DriverFactory
{
    public static final String BOLT_URI_SCHEME = "bolt";
    public static final String BOLT_ROUTING_URI_SCHEME = "neo4j";

    protected static MetricsProvider createDriverMetrics( Config config, Clock clock )
    {
        return (MetricsProvider) (config.isMetricsEnabled() ? new InternalMetricsProvider( clock, config.logging() )
                                                            : MetricsProvider.METRICS_DISABLED_PROVIDER);
    }

    private static ServerAddressResolver createResolver( Config config )
    {
        ServerAddressResolver configuredResolver = config.resolver();
        return (ServerAddressResolver) (configuredResolver != null ? configuredResolver : IdentityResolver.IDENTITY_RESOLVER);
    }

    private static SecurityPlan createSecurityPlan( BoltServerAddress address, Config config )
    {
        try
        {
            return createSecurityPlanImpl( config );
        }
        catch ( IOException | GeneralSecurityException var3 )
        {
            throw new ClientException( "Unable to establish SSL parameters", var3 );
        }
    }

    private static SecurityPlan createSecurityPlanImpl( Config config ) throws GeneralSecurityException, IOException
    {
        if ( config.encrypted() )
        {
            Config.TrustStrategy trustStrategy = config.trustStrategy();
            boolean hostnameVerificationEnabled = trustStrategy.isHostnameVerificationEnabled();
            switch ( trustStrategy.strategy() )
            {
            case TRUST_CUSTOM_CA_SIGNED_CERTIFICATES:
                return SecurityPlanImpl.forCustomCASignedCertificates( trustStrategy.certFile(), hostnameVerificationEnabled );
            case TRUST_SYSTEM_CA_SIGNED_CERTIFICATES:
                return SecurityPlanImpl.forSystemCASignedCertificates( hostnameVerificationEnabled );
            case TRUST_ALL_CERTIFICATES:
                return SecurityPlanImpl.forAllCertificates( hostnameVerificationEnabled );
            default:
                throw new ClientException( "Unknown TLS authentication strategy: " + trustStrategy.strategy().name() );
            }
        }
        else
        {
            return SecurityPlanImpl.insecure();
        }
    }

    private static void assertNoRoutingContext( URI uri, RoutingSettings routingSettings )
    {
        RoutingContext routingContext = routingSettings.routingContext();
        if ( routingContext.isDefined() )
        {
            throw new IllegalArgumentException( "Routing parameters are not supported with scheme 'bolt'. Given URI: '" + uri + "'" );
        }
    }

    private static void closeConnectionPoolAndSuppressError( ConnectionPool connectionPool, Throwable mainError )
    {
        try
        {
            Futures.blockingGet( connectionPool.close() );
        }
        catch ( Throwable var3 )
        {
            ErrorUtil.addSuppressed( mainError, var3 );
        }
    }

    public final Driver newInstance( URI uri, AuthToken authToken, RoutingSettings routingSettings, RetrySettings retrySettings, Config config )
    {
        return this.newInstance( uri, authToken, routingSettings, retrySettings, config, (EventLoopGroup) null, (SecurityPlan) null );
    }

    public final Driver newInstance( URI uri, AuthToken authToken, RoutingSettings routingSettings, RetrySettings retrySettings, Config config,
            EventLoopGroup eventLoopGroup, SecurityPlan customSecurityPlan )
    {
        Bootstrap bootstrap;
        boolean ownsEventLoopGroup;
        if ( eventLoopGroup == null )
        {
            bootstrap = this.createBootstrap( config.eventLoopThreads() );
            ownsEventLoopGroup = true;
        }
        else
        {
            bootstrap = this.createBootstrap( eventLoopGroup );
            ownsEventLoopGroup = false;
        }

        authToken = authToken == null ? AuthTokens.none() : authToken;
        BoltServerAddress address = new BoltServerAddress( uri );
        RoutingSettings newRoutingSettings = routingSettings.withRoutingContext( new RoutingContext( uri ) );
        SecurityPlan securityPlan;
        if ( customSecurityPlan != null )
        {
            securityPlan = customSecurityPlan;
        }
        else
        {
            securityPlan = createSecurityPlan( address, config );
        }

        InternalLoggerFactory.setDefaultFactory( new NettyLogging( config.logging() ) );
        EventExecutorGroup eventExecutorGroup = bootstrap.config().group();
        RetryLogic retryLogic = this.createRetryLogic( retrySettings, eventExecutorGroup, config.logging() );
        MetricsProvider metricsProvider = createDriverMetrics( config, this.createClock() );
        ConnectionPool connectionPool = this.createConnectionPool( authToken, securityPlan, bootstrap, metricsProvider, config, ownsEventLoopGroup );
        return this.createDriver( uri, securityPlan, address, connectionPool, eventExecutorGroup, newRoutingSettings, retryLogic, metricsProvider, config );
    }

    protected ConnectionPool createConnectionPool( AuthToken authToken, SecurityPlan securityPlan, Bootstrap bootstrap, MetricsProvider metricsProvider,
            Config config, boolean ownsEventLoopGroup )
    {
        Clock clock = this.createClock();
        ConnectionSettings settings = new ConnectionSettings( authToken, config.connectionTimeoutMillis() );
        ChannelConnector connector = this.createConnector( settings, securityPlan, config, clock );
        PoolSettings poolSettings =
                new PoolSettings( config.maxConnectionPoolSize(), config.connectionAcquisitionTimeoutMillis(), config.maxConnectionLifetimeMillis(),
                        config.idleTimeBeforeConnectionTest() );
        return new ConnectionPoolImpl( connector, bootstrap, poolSettings, metricsProvider.metricsListener(), config.logging(), clock, ownsEventLoopGroup );
    }

    protected ChannelConnector createConnector( ConnectionSettings settings, SecurityPlan securityPlan, Config config, Clock clock )
    {
        return new ChannelConnectorImpl( settings, securityPlan, config.logging(), clock );
    }

    private InternalDriver createDriver( URI uri, SecurityPlan securityPlan, BoltServerAddress address, ConnectionPool connectionPool,
            EventExecutorGroup eventExecutorGroup, RoutingSettings routingSettings, RetryLogic retryLogic, MetricsProvider metricsProvider, Config config )
    {
        try
        {
            String scheme = uri.getScheme().toLowerCase();
            byte var12 = -1;
            switch ( scheme.hashCode() )
            {
            case 3029653:
                if ( scheme.equals( "bolt" ) )
                {
                    var12 = 0;
                }
                break;
            case 104704590:
                if ( scheme.equals( "neo4j" ) )
                {
                    var12 = 1;
                }
            }

            switch ( var12 )
            {
            case 0:
                assertNoRoutingContext( uri, routingSettings );
                return this.createDirectDriver( securityPlan, address, connectionPool, retryLogic, metricsProvider, config );
            case 1:
                return this.createRoutingDriver( securityPlan, address, connectionPool, eventExecutorGroup, routingSettings, retryLogic, metricsProvider,
                        config );
            default:
                throw new ClientException( String.format( "Unsupported URI scheme: %s", scheme ) );
            }
        }
        catch ( Throwable var13 )
        {
            closeConnectionPoolAndSuppressError( connectionPool, var13 );
            throw var13;
        }
    }

    protected InternalDriver createDirectDriver( SecurityPlan securityPlan, BoltServerAddress address, ConnectionPool connectionPool, RetryLogic retryLogic,
            MetricsProvider metricsProvider, Config config )
    {
        ConnectionProvider connectionProvider = new DirectConnectionProvider( address, connectionPool );
        SessionFactory sessionFactory = this.createSessionFactory( connectionProvider, retryLogic, config );
        InternalDriver driver = this.createDriver( securityPlan, sessionFactory, metricsProvider, config );
        Logger log = config.logging().getLog( Driver.class.getSimpleName() );
        log.info( "Direct driver instance %s created for server address %s", driver.hashCode(), address );
        return driver;
    }

    protected InternalDriver createRoutingDriver( SecurityPlan securityPlan, BoltServerAddress address, ConnectionPool connectionPool,
            EventExecutorGroup eventExecutorGroup, RoutingSettings routingSettings, RetryLogic retryLogic, MetricsProvider metricsProvider, Config config )
    {
        ConnectionProvider connectionProvider = this.createLoadBalancer( address, connectionPool, eventExecutorGroup, config, routingSettings );
        SessionFactory sessionFactory = this.createSessionFactory( connectionProvider, retryLogic, config );
        InternalDriver driver = this.createDriver( securityPlan, sessionFactory, metricsProvider, config );
        Logger log = config.logging().getLog( Driver.class.getSimpleName() );
        log.info( "Routing driver instance %s created for server address %s", driver.hashCode(), address );
        return driver;
    }

    protected InternalDriver createDriver( SecurityPlan securityPlan, SessionFactory sessionFactory, MetricsProvider metricsProvider, Config config )
    {
        return new InternalDriver( securityPlan, sessionFactory, metricsProvider, config.logging() );
    }

    protected LoadBalancer createLoadBalancer( BoltServerAddress address, ConnectionPool connectionPool, EventExecutorGroup eventExecutorGroup, Config config,
            RoutingSettings routingSettings )
    {
        LoadBalancingStrategy loadBalancingStrategy = new LeastConnectedLoadBalancingStrategy( connectionPool, config.logging() );
        ServerAddressResolver resolver = createResolver( config );
        return new LoadBalancer( address, routingSettings, connectionPool, eventExecutorGroup, this.createClock(), config.logging(), loadBalancingStrategy,
                resolver );
    }

    protected Clock createClock()
    {
        return Clock.SYSTEM;
    }

    protected SessionFactory createSessionFactory( ConnectionProvider connectionProvider, RetryLogic retryLogic, Config config )
    {
        return new SessionFactoryImpl( connectionProvider, retryLogic, config );
    }

    protected RetryLogic createRetryLogic( RetrySettings settings, EventExecutorGroup eventExecutorGroup, Logging logging )
    {
        return new ExponentialBackoffRetryLogic( settings, eventExecutorGroup, this.createClock(), logging );
    }

    protected Bootstrap createBootstrap( int size )
    {
        return BootstrapFactory.newBootstrap( size );
    }

    protected Bootstrap createBootstrap( EventLoopGroup eventLoopGroup )
    {
        return BootstrapFactory.newBootstrap( eventLoopGroup );
    }
}
