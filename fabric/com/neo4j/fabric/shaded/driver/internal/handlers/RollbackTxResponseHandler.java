package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class RollbackTxResponseHandler implements ResponseHandler
{
    private final CompletableFuture<Void> rollbackFuture;

    public RollbackTxResponseHandler( CompletableFuture<Void> rollbackFuture )
    {
        this.rollbackFuture = (CompletableFuture) Objects.requireNonNull( rollbackFuture );
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        this.rollbackFuture.complete( (Object) null );
    }

    public void onFailure( Throwable error )
    {
        this.rollbackFuture.completeExceptionally( error );
    }

    public void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException( "Transaction rollback is not expected to receive records: " + Arrays.toString( fields ) );
    }
}
