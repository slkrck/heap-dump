package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.exceptions.SessionExpiredException;
import com.neo4j.fabric.shaded.driver.exceptions.TransientException;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.RoutingErrorHandler;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.Map;
import java.util.Objects;

public class RoutingResponseHandler implements ResponseHandler
{
    private final ResponseHandler delegate;
    private final BoltServerAddress address;
    private final AccessMode accessMode;
    private final RoutingErrorHandler errorHandler;

    public RoutingResponseHandler( ResponseHandler delegate, BoltServerAddress address, AccessMode accessMode, RoutingErrorHandler errorHandler )
    {
        this.delegate = delegate;
        this.address = address;
        this.accessMode = accessMode;
        this.errorHandler = errorHandler;
    }

    private static boolean isFailureToWrite( ClientException e )
    {
        String errorCode = e.code();
        return Objects.equals( errorCode, "Neo.ClientError.Cluster.NotALeader" ) ||
                Objects.equals( errorCode, "Neo.ClientError.General.ForbiddenOnReadOnlyDatabase" );
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        this.delegate.onSuccess( metadata );
    }

    public void onFailure( Throwable error )
    {
        Throwable newError = this.handledError( error );
        this.delegate.onFailure( newError );
    }

    public void onRecord( Value[] fields )
    {
        this.delegate.onRecord( fields );
    }

    public boolean canManageAutoRead()
    {
        return this.delegate.canManageAutoRead();
    }

    public void disableAutoReadManagement()
    {
        this.delegate.disableAutoReadManagement();
    }

    private Throwable handledError( Throwable receivedError )
    {
        Throwable error = Futures.completionExceptionCause( receivedError );
        if ( error instanceof ServiceUnavailableException )
        {
            return this.handledServiceUnavailableException( (ServiceUnavailableException) error );
        }
        else if ( error instanceof ClientException )
        {
            return this.handledClientException( (ClientException) error );
        }
        else
        {
            return error instanceof TransientException ? this.handledTransientException( (TransientException) error ) : error;
        }
    }

    private Throwable handledServiceUnavailableException( ServiceUnavailableException e )
    {
        this.errorHandler.onConnectionFailure( this.address );
        return new SessionExpiredException( String.format( "Server at %s is no longer available", this.address ), e );
    }

    private Throwable handledTransientException( TransientException e )
    {
        String errorCode = e.code();
        if ( Objects.equals( errorCode, "Neo.TransientError.General.DatabaseUnavailable" ) )
        {
            this.errorHandler.onConnectionFailure( this.address );
        }

        return e;
    }

    private Throwable handledClientException( ClientException e )
    {
        if ( isFailureToWrite( e ) )
        {
            switch ( this.accessMode )
            {
            case READ:
                return new ClientException( "Write queries cannot be performed in READ access mode." );
            case WRITE:
                this.errorHandler.onWriteFailure( this.address );
                return new SessionExpiredException( String.format( "Server at %s no longer accepts writes", this.address ) );
            default:
                throw new IllegalArgumentException( this.accessMode + " not supported." );
            }
        }
        else
        {
            return e;
        }
    }
}
