package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Promise;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;

import java.util.Map;

public class PingResponseHandler implements ResponseHandler
{
    private final Promise<Boolean> result;
    private final Channel channel;
    private final Logger log;

    public PingResponseHandler( Promise<Boolean> result, Channel channel, Logger log )
    {
        this.result = result;
        this.channel = channel;
        this.log = log;
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        this.log.trace( "Channel %s pinged successfully", this.channel );
        this.result.setSuccess( true );
    }

    public void onFailure( Throwable error )
    {
        this.log.trace( "Channel %s failed ping %s", this.channel, error );
        this.result.setSuccess( false );
    }

    public void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException();
    }
}
