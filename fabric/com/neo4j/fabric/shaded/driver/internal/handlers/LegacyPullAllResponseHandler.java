package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.InternalRecord;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.PullAllMessage;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class LegacyPullAllResponseHandler implements PullAllResponseHandler
{
    static final int RECORD_BUFFER_LOW_WATERMARK = Integer.getInteger( "recordBufferLowWatermark", 300 );
    static final int RECORD_BUFFER_HIGH_WATERMARK = Integer.getInteger( "recordBufferHighWatermark", 1000 );
    private static final Queue<Record> UNINITIALIZED_RECORDS = Iterables.emptyQueue();
    protected final MetadataExtractor metadataExtractor;
    protected final Connection connection;
    private final Query query;
    private final RunResponseHandler runResponseHandler;
    private final PullResponseCompletionListener completionListener;
    private Queue<Record> records;
    private boolean autoReadManagementEnabled;
    private boolean finished;
    private Throwable failure;
    private ResultSummary summary;
    private boolean ignoreRecords;
    private CompletableFuture<Record> recordFuture;
    private CompletableFuture<Throwable> failureFuture;

    public LegacyPullAllResponseHandler( Query query, RunResponseHandler runResponseHandler, Connection connection, MetadataExtractor metadataExtractor,
            PullResponseCompletionListener completionListener )
    {
        this.records = UNINITIALIZED_RECORDS;
        this.autoReadManagementEnabled = true;
        this.query = (Query) Objects.requireNonNull( query );
        this.runResponseHandler = (RunResponseHandler) Objects.requireNonNull( runResponseHandler );
        this.metadataExtractor = (MetadataExtractor) Objects.requireNonNull( metadataExtractor );
        this.connection = (Connection) Objects.requireNonNull( connection );
        this.completionListener = (PullResponseCompletionListener) Objects.requireNonNull( completionListener );
    }

    public boolean canManageAutoRead()
    {
        return true;
    }

    public synchronized void onSuccess( Map<String,Value> metadata )
    {
        this.finished = true;
        this.summary = this.extractResultSummary( metadata );
        this.completionListener.afterSuccess( metadata );
        this.completeRecordFuture( (Record) null );
        this.completeFailureFuture( (Throwable) null );
    }

    public synchronized void onFailure( Throwable error )
    {
        this.finished = true;
        this.summary = this.extractResultSummary( Collections.emptyMap() );
        this.completionListener.afterFailure( error );
        boolean failedRecordFuture = this.failRecordFuture( error );
        if ( failedRecordFuture )
        {
            this.completeFailureFuture( (Throwable) null );
        }
        else
        {
            boolean completedFailureFuture = this.completeFailureFuture( error );
            if ( !completedFailureFuture )
            {
                this.failure = error;
            }
        }
    }

    public synchronized void onRecord( Value[] fields )
    {
        if ( this.ignoreRecords )
        {
            this.completeRecordFuture( (Record) null );
        }
        else
        {
            Record record = new InternalRecord( this.runResponseHandler.queryKeys(), fields );
            this.enqueueRecord( record );
            this.completeRecordFuture( record );
        }
    }

    public synchronized void disableAutoReadManagement()
    {
        this.autoReadManagementEnabled = false;
    }

    public synchronized CompletionStage<Record> peekAsync()
    {
        Record record = (Record) this.records.peek();
        if ( record == null )
        {
            if ( this.failure != null )
            {
                return Futures.failedFuture( this.extractFailure() );
            }
            else if ( !this.ignoreRecords && !this.finished )
            {
                if ( this.recordFuture == null )
                {
                    this.recordFuture = new CompletableFuture();
                }

                return this.recordFuture;
            }
            else
            {
                return Futures.completedWithNull();
            }
        }
        else
        {
            return CompletableFuture.completedFuture( record );
        }
    }

    public synchronized CompletionStage<Record> nextAsync()
    {
        return this.peekAsync().thenApply( ( ignore ) -> {
            return this.dequeueRecord();
        } );
    }

    public synchronized CompletionStage<ResultSummary> consumeAsync()
    {
        this.ignoreRecords = true;
        this.records.clear();
        return this.pullAllFailureAsync().thenApply( ( error ) -> {
            if ( error != null )
            {
                throw Futures.asCompletionException( error );
            }
            else
            {
                return this.summary;
            }
        } );
    }

    public synchronized <T> CompletionStage<List<T>> listAsync( Function<Record,T> mapFunction )
    {
        return this.pullAllFailureAsync().thenApply( ( error ) -> {
            if ( error != null )
            {
                throw Futures.asCompletionException( error );
            }
            else
            {
                return this.recordsAsList( mapFunction );
            }
        } );
    }

    public void prePopulateRecords()
    {
        this.connection.writeAndFlush( PullAllMessage.PULL_ALL, this );
    }

    public synchronized CompletionStage<Throwable> pullAllFailureAsync()
    {
        if ( this.failure != null )
        {
            return CompletableFuture.completedFuture( this.extractFailure() );
        }
        else if ( this.finished )
        {
            return Futures.completedWithNull();
        }
        else
        {
            if ( this.failureFuture == null )
            {
                this.enableAutoRead();
                this.failureFuture = new CompletableFuture();
            }

            return this.failureFuture;
        }
    }

    private void enqueueRecord( Record record )
    {
        if ( this.records == UNINITIALIZED_RECORDS )
        {
            this.records = new ArrayDeque();
        }

        this.records.add( record );
        boolean shouldBufferAllRecords = this.failureFuture != null;
        if ( !shouldBufferAllRecords && this.records.size() > RECORD_BUFFER_HIGH_WATERMARK )
        {
            this.disableAutoRead();
        }
    }

    private Record dequeueRecord()
    {
        Record record = (Record) this.records.poll();
        if ( this.records.size() < RECORD_BUFFER_LOW_WATERMARK )
        {
            this.enableAutoRead();
        }

        return record;
    }

    private <T> List<T> recordsAsList( Function<Record,T> mapFunction )
    {
        if ( !this.finished )
        {
            throw new IllegalStateException( "Can't get records as list because SUCCESS or FAILURE did not arrive" );
        }
        else
        {
            ArrayList result = new ArrayList( this.records.size() );

            while ( !this.records.isEmpty() )
            {
                Record record = (Record) this.records.poll();
                result.add( mapFunction.apply( record ) );
            }

            return result;
        }
    }

    private Throwable extractFailure()
    {
        if ( this.failure == null )
        {
            throw new IllegalStateException( "Can't extract failure because it does not exist" );
        }
        else
        {
            Throwable error = this.failure;
            this.failure = null;
            return error;
        }
    }

    private void completeRecordFuture( Record record )
    {
        if ( this.recordFuture != null )
        {
            CompletableFuture<Record> future = this.recordFuture;
            this.recordFuture = null;
            future.complete( record );
        }
    }

    private boolean failRecordFuture( Throwable error )
    {
        if ( this.recordFuture != null )
        {
            CompletableFuture<Record> future = this.recordFuture;
            this.recordFuture = null;
            future.completeExceptionally( error );
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean completeFailureFuture( Throwable error )
    {
        if ( this.failureFuture != null )
        {
            CompletableFuture<Throwable> future = this.failureFuture;
            this.failureFuture = null;
            future.complete( error );
            return true;
        }
        else
        {
            return false;
        }
    }

    private ResultSummary extractResultSummary( Map<String,Value> metadata )
    {
        long resultAvailableAfter = this.runResponseHandler.resultAvailableAfter();
        return this.metadataExtractor.extractSummary( this.query, this.connection, resultAvailableAfter, metadata );
    }

    private void enableAutoRead()
    {
        if ( this.autoReadManagementEnabled )
        {
            this.connection.enableAutoRead();
        }
    }

    private void disableAutoRead()
    {
        if ( this.autoReadManagementEnabled )
        {
            this.connection.disableAutoRead();
        }
    }
}
