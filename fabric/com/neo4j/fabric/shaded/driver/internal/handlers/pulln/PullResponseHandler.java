package com.neo4j.fabric.shaded.driver.internal.handlers.pulln;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.function.BiConsumer;

import org.reactivestreams.Subscription;

public interface PullResponseHandler extends ResponseHandler, Subscription
{
    void installRecordConsumer( BiConsumer<Record,Throwable> var1 );

    void installSummaryConsumer( BiConsumer<ResultSummary,Throwable> var1 );

    public static enum Status
    {
        SUCCEEDED,
        FAILED,
        CANCELED,
        STREAMING,
        READY;
    }
}
