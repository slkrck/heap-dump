package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;

import java.util.Map;

public interface PullResponseCompletionListener
{
    void afterSuccess( Map<String,Value> var1 );

    void afterFailure( Throwable var1 );
}
