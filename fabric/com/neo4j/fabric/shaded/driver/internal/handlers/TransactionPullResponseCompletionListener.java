package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;

import java.util.Map;
import java.util.Objects;

public class TransactionPullResponseCompletionListener implements PullResponseCompletionListener
{
    private final UnmanagedTransaction tx;

    public TransactionPullResponseCompletionListener( UnmanagedTransaction tx )
    {
        this.tx = (UnmanagedTransaction) Objects.requireNonNull( tx );
    }

    public void afterSuccess( Map<String,Value> metadata )
    {
    }

    public void afterFailure( Throwable error )
    {
        this.tx.markTerminated();
    }
}
