package com.neo4j.fabric.shaded.driver.internal.handlers.pulln;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.InternalRecord;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullResponseCompletionListener;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.DiscardMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.PullMessage;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;
import com.neo4j.fabric.shaded.driver.internal.value.BooleanValue;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;

public class BasicPullResponseHandler implements PullResponseHandler
{
    protected final RunResponseHandler runResponseHandler;
    protected final MetadataExtractor metadataExtractor;
    protected final Connection connection;
    private final Query query;
    private final PullResponseCompletionListener completionListener;
    private PullResponseHandler.Status status;
    private long toRequest;
    private BiConsumer<Record,Throwable> recordConsumer;
    private BiConsumer<ResultSummary,Throwable> summaryConsumer;

    public BasicPullResponseHandler( Query query, RunResponseHandler runResponseHandler, Connection connection, MetadataExtractor metadataExtractor,
            PullResponseCompletionListener completionListener )
    {
        this.status = PullResponseHandler.Status.READY;
        this.recordConsumer = null;
        this.summaryConsumer = null;
        this.query = (Query) Objects.requireNonNull( query );
        this.runResponseHandler = (RunResponseHandler) Objects.requireNonNull( runResponseHandler );
        this.metadataExtractor = (MetadataExtractor) Objects.requireNonNull( metadataExtractor );
        this.connection = (Connection) Objects.requireNonNull( connection );
        this.completionListener = (PullResponseCompletionListener) Objects.requireNonNull( completionListener );
    }

    public synchronized void onSuccess( Map<String,Value> metadata )
    {
        this.assertRecordAndSummaryConsumerInstalled();
        if ( ((Value) metadata.getOrDefault( "has_more", BooleanValue.FALSE )).asBoolean() )
        {
            this.handleSuccessWithHasMore();
        }
        else
        {
            this.handleSuccessWithSummary( metadata );
        }
    }

    public synchronized void onFailure( Throwable error )
    {
        this.assertRecordAndSummaryConsumerInstalled();
        this.status = PullResponseHandler.Status.FAILED;
        this.completionListener.afterFailure( error );
        this.complete( this.extractResultSummary( Collections.emptyMap() ), error );
    }

    public synchronized void onRecord( Value[] fields )
    {
        this.assertRecordAndSummaryConsumerInstalled();
        if ( this.isStreaming() )
        {
            Record record = new InternalRecord( this.runResponseHandler.queryKeys(), fields );
            this.recordConsumer.accept( record, (Object) null );
        }
    }

    public synchronized void request( long size )
    {
        this.assertRecordAndSummaryConsumerInstalled();
        if ( this.isStreamingPaused() )
        {
            this.status = PullResponseHandler.Status.STREAMING;
            this.connection.writeAndFlush( new PullMessage( size, this.runResponseHandler.queryId() ), this );
        }
        else if ( this.isStreaming() )
        {
            this.addToRequest( size );
        }
    }

    public synchronized void cancel()
    {
        this.assertRecordAndSummaryConsumerInstalled();
        if ( this.isStreamingPaused() )
        {
            this.status = PullResponseHandler.Status.CANCELED;
            this.connection.writeAndFlush( DiscardMessage.newDiscardAllMessage( this.runResponseHandler.queryId() ), this );
        }
        else if ( this.isStreaming() )
        {
            this.status = PullResponseHandler.Status.CANCELED;
        }
    }

    public synchronized void installSummaryConsumer( BiConsumer<ResultSummary,Throwable> summaryConsumer )
    {
        if ( this.summaryConsumer != null )
        {
            throw new IllegalStateException( "Summary consumer already installed." );
        }
        else
        {
            this.summaryConsumer = summaryConsumer;
        }
    }

    public synchronized void installRecordConsumer( BiConsumer<Record,Throwable> recordConsumer )
    {
        if ( this.recordConsumer != null )
        {
            throw new IllegalStateException( "Record consumer already installed." );
        }
        else
        {
            this.recordConsumer = recordConsumer;
        }
    }

    private boolean isStreaming()
    {
        return this.status == PullResponseHandler.Status.STREAMING;
    }

    private boolean isStreamingPaused()
    {
        return this.status == PullResponseHandler.Status.READY;
    }

    protected boolean isDone()
    {
        return this.status == PullResponseHandler.Status.SUCCEEDED || this.status == PullResponseHandler.Status.FAILED;
    }

    private void handleSuccessWithSummary( Map<String,Value> metadata )
    {
        this.status = PullResponseHandler.Status.SUCCEEDED;
        this.completionListener.afterSuccess( metadata );
        ResultSummary summary = this.extractResultSummary( metadata );
        this.complete( summary, (Throwable) null );
    }

    private void handleSuccessWithHasMore()
    {
        if ( this.status == PullResponseHandler.Status.CANCELED )
        {
            this.status = PullResponseHandler.Status.READY;
            this.cancel();
        }
        else if ( this.status == PullResponseHandler.Status.STREAMING )
        {
            this.status = PullResponseHandler.Status.READY;
            if ( this.toRequest > 0L || this.toRequest == -1L )
            {
                this.request( this.toRequest );
                this.toRequest = 0L;
            }

            this.summaryConsumer.accept( (Object) null, (Object) null );
        }
    }

    private ResultSummary extractResultSummary( Map<String,Value> metadata )
    {
        long resultAvailableAfter = this.runResponseHandler.resultAvailableAfter();
        return this.metadataExtractor.extractSummary( this.query, this.connection, resultAvailableAfter, metadata );
    }

    private void addToRequest( long toAdd )
    {
        if ( this.toRequest != -1L )
        {
            if ( toAdd == -1L )
            {
                this.toRequest = -1L;
            }
            else if ( toAdd <= 0L )
            {
                throw new IllegalArgumentException( "Cannot request record amount that is less than or equal to 0. Request amount: " + toAdd );
            }
            else
            {
                this.toRequest += toAdd;
                if ( this.toRequest <= 0L )
                {
                    this.toRequest = Long.MAX_VALUE;
                }
            }
        }
    }

    private void assertRecordAndSummaryConsumerInstalled()
    {
        if ( !this.isDone() )
        {
            if ( this.recordConsumer == null || this.summaryConsumer == null )
            {
                throw new IllegalStateException(
                        String.format( "Access record stream without record consumer and/or summary consumer. Record consumer=%s, Summary consumer=%s",
                                this.recordConsumer, this.summaryConsumer ) );
            }
        }
    }

    private void complete( ResultSummary summary, Throwable error )
    {
        this.summaryConsumer.accept( summary, error );
        this.recordConsumer.accept( (Object) null, error );
        this.dispose();
    }

    private void dispose()
    {
        this.recordConsumer = null;
        this.summaryConsumer = null;
    }

    protected PullResponseHandler.Status status()
    {
        return this.status;
    }

    protected void status( PullResponseHandler.Status status )
    {
        this.status = status;
    }
}
