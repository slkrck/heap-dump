package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;

import java.util.Map;
import java.util.Objects;

public class SessionPullResponseCompletionListener implements PullResponseCompletionListener
{
    private final BookmarkHolder bookmarkHolder;
    private final Connection connection;

    public SessionPullResponseCompletionListener( Connection connection, BookmarkHolder bookmarkHolder )
    {
        this.connection = (Connection) Objects.requireNonNull( connection );
        this.bookmarkHolder = (BookmarkHolder) Objects.requireNonNull( bookmarkHolder );
    }

    public void afterSuccess( Map<String,Value> metadata )
    {
        this.releaseConnection();
        this.bookmarkHolder.setBookmark( MetadataExtractor.extractBookmarks( metadata ) );
    }

    public void afterFailure( Throwable error )
    {
        this.releaseConnection();
    }

    private void releaseConnection()
    {
        this.connection.release();
    }
}
