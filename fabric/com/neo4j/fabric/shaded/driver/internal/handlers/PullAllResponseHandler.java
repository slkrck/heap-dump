package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public interface PullAllResponseHandler extends ResponseHandler
{
    CompletionStage<ResultSummary> consumeAsync();

    CompletionStage<Record> nextAsync();

    CompletionStage<Record> peekAsync();

    <T> CompletionStage<List<T>> listAsync( Function<Record,T> var1 );

    CompletionStage<Throwable> pullAllFailureAsync();

    void prePopulateRecords();
}
