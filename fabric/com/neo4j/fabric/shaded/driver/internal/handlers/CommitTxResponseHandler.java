package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.InternalBookmark;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class CommitTxResponseHandler implements ResponseHandler
{
    private final CompletableFuture<Bookmark> commitFuture;

    public CommitTxResponseHandler( CompletableFuture<Bookmark> commitFuture )
    {
        this.commitFuture = (CompletableFuture) Objects.requireNonNull( commitFuture );
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        Value bookmarkValue = (Value) metadata.get( "bookmark" );
        if ( bookmarkValue == null )
        {
            this.commitFuture.complete( (Object) null );
        }
        else
        {
            this.commitFuture.complete( InternalBookmark.parse( bookmarkValue.asString() ) );
        }
    }

    public void onFailure( Throwable error )
    {
        this.commitFuture.completeExceptionally( error );
    }

    public void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException( "Transaction commit is not expected to receive records: " + Arrays.toString( fields ) );
    }
}
