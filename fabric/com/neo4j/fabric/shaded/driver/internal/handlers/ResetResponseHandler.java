package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.InboundMessageDispatcher;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class ResetResponseHandler implements ResponseHandler
{
    private final InboundMessageDispatcher messageDispatcher;
    private final CompletableFuture<Void> completionFuture;

    public ResetResponseHandler( InboundMessageDispatcher messageDispatcher )
    {
        this( messageDispatcher, (CompletableFuture) null );
    }

    public ResetResponseHandler( InboundMessageDispatcher messageDispatcher, CompletableFuture<Void> completionFuture )
    {
        this.messageDispatcher = messageDispatcher;
        this.completionFuture = completionFuture;
    }

    public final void onSuccess( Map<String,Value> metadata )
    {
        this.resetCompleted( true );
    }

    public final void onFailure( Throwable error )
    {
        this.resetCompleted( false );
    }

    public final void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException();
    }

    private void resetCompleted( boolean success )
    {
        this.messageDispatcher.clearCurrentError();
        if ( this.completionFuture != null )
        {
            this.resetCompleted( this.completionFuture, success );
        }
    }

    protected void resetCompleted( CompletableFuture<Void> completionFuture, boolean success )
    {
        completionFuture.complete( (Object) null );
    }
}
