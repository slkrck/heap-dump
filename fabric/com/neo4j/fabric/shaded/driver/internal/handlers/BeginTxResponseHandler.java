package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class BeginTxResponseHandler implements ResponseHandler
{
    private final CompletableFuture<Void> beginTxFuture;

    public BeginTxResponseHandler( CompletableFuture<Void> beginTxFuture )
    {
        this.beginTxFuture = (CompletableFuture) Objects.requireNonNull( beginTxFuture );
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        this.beginTxFuture.complete( (Object) null );
    }

    public void onFailure( Throwable error )
    {
        this.beginTxFuture.completeExceptionally( error );
    }

    public void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException( "Transaction begin is not expected to receive records: " + Arrays.toString( fields ) );
    }
}
