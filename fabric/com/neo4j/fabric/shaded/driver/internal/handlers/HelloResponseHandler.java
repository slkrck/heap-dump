package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.Map;

public class HelloResponseHandler implements ResponseHandler
{
    private static final String CONNECTION_ID_METADATA_KEY = "connection_id";
    private final ChannelPromise connectionInitializedPromise;
    private final Channel channel;

    public HelloResponseHandler( ChannelPromise connectionInitializedPromise )
    {
        this.connectionInitializedPromise = connectionInitializedPromise;
        this.channel = connectionInitializedPromise.channel();
    }

    private static String extractConnectionId( Map<String,Value> metadata )
    {
        Value value = (Value) metadata.get( "connection_id" );
        if ( value != null && !value.isNull() )
        {
            return value.asString();
        }
        else
        {
            throw new IllegalStateException( "Unable to extract connection_id from a response to HELLO message. Received metadata: " + metadata );
        }
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        try
        {
            ServerVersion serverVersion = MetadataExtractor.extractNeo4jServerVersion( metadata );
            ChannelAttributes.setServerVersion( this.channel, serverVersion );
            String connectionId = extractConnectionId( metadata );
            ChannelAttributes.setConnectionId( this.channel, connectionId );
            this.connectionInitializedPromise.setSuccess();
        }
        catch ( Throwable var4 )
        {
            this.onFailure( var4 );
            throw var4;
        }
    }

    public void onFailure( Throwable error )
    {
        this.channel.close().addListener( ( future ) -> {
            this.connectionInitializedPromise.setFailure( error );
        } );
    }

    public void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException();
    }
}
