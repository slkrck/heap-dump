package com.neo4j.fabric.shaded.driver.internal.handlers.pulln;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullAllResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullResponseCompletionListener;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class AutoPullResponseHandler extends BasicPullResponseHandler implements PullAllResponseHandler
{
    private static final Queue<Record> UNINITIALIZED_RECORDS = Iterables.emptyQueue();
    private final long fetchSize;
    private Queue<Record> records;
    private ResultSummary summary;
    private Throwable failure;
    private CompletableFuture<Record> recordFuture;
    private CompletableFuture<ResultSummary> summaryFuture;

    public AutoPullResponseHandler( Query query, RunResponseHandler runResponseHandler, Connection connection, MetadataExtractor metadataExtractor,
            PullResponseCompletionListener completionListener, long fetchSize )
    {
        super( query, runResponseHandler, connection, metadataExtractor, completionListener );
        this.records = UNINITIALIZED_RECORDS;
        this.fetchSize = fetchSize;
        this.installRecordAndSummaryConsumers();
    }

    private void installRecordAndSummaryConsumers()
    {
        this.installRecordConsumer( ( record, error ) -> {
            if ( record != null )
            {
                this.enqueueRecord( record );
                this.completeRecordFuture( record );
            }

            if ( record == null && error == null )
            {
                this.completeRecordFuture( (Record) null );
            }
        } );
        this.installSummaryConsumer( ( summary, error ) -> {
            if ( error != null )
            {
                this.handleFailure( error );
            }

            if ( summary != null )
            {
                this.summary = summary;
                this.completeSummaryFuture( summary );
            }

            if ( error == null && summary == null )
            {
                this.request( this.fetchSize );
            }
        } );
    }

    private void handleFailure( Throwable error )
    {
        if ( !this.failRecordFuture( error ) && !this.failSummaryFuture( error ) )
        {
            this.failure = error;
        }
    }

    public synchronized CompletionStage<Record> peekAsync()
    {
        Record record = (Record) this.records.peek();
        if ( record == null )
        {
            if ( this.isDone() )
            {
                return this.completedWithValueIfNoFailure( (Object) null );
            }
            else
            {
                if ( this.recordFuture == null )
                {
                    this.recordFuture = new CompletableFuture();
                }

                return this.recordFuture;
            }
        }
        else
        {
            return CompletableFuture.completedFuture( record );
        }
    }

    public synchronized CompletionStage<Record> nextAsync()
    {
        return this.peekAsync().thenApply( ( ignore ) -> {
            return this.dequeueRecord();
        } );
    }

    public synchronized CompletionStage<ResultSummary> consumeAsync()
    {
        this.records.clear();
        if ( this.isDone() )
        {
            return this.completedWithValueIfNoFailure( this.summary );
        }
        else
        {
            this.cancel();
            if ( this.summaryFuture == null )
            {
                this.summaryFuture = new CompletableFuture();
            }

            return this.summaryFuture;
        }
    }

    public synchronized <T> CompletionStage<List<T>> listAsync( Function<Record,T> mapFunction )
    {
        return this.pullAllAsync().thenApply( ( summary ) -> {
            return this.recordsAsList( mapFunction );
        } );
    }

    public synchronized CompletionStage<Throwable> pullAllFailureAsync()
    {
        return this.pullAllAsync().handle( ( ignore, error ) -> {
            return error;
        } );
    }

    public void prePopulateRecords()
    {
        this.request( this.fetchSize );
    }

    private synchronized CompletionStage<ResultSummary> pullAllAsync()
    {
        if ( this.isDone() )
        {
            return this.completedWithValueIfNoFailure( this.summary );
        }
        else
        {
            this.request( -1L );
            if ( this.summaryFuture == null )
            {
                this.summaryFuture = new CompletableFuture();
            }

            return this.summaryFuture;
        }
    }

    private void enqueueRecord( Record record )
    {
        if ( this.records == UNINITIALIZED_RECORDS )
        {
            this.records = new ArrayDeque();
        }

        this.records.add( record );
    }

    private Record dequeueRecord()
    {
        return (Record) this.records.poll();
    }

    private <T> List<T> recordsAsList( Function<Record,T> mapFunction )
    {
        if ( !this.isDone() )
        {
            throw new IllegalStateException( "Can't get records as list because SUCCESS or FAILURE did not arrive" );
        }
        else
        {
            ArrayList result = new ArrayList( this.records.size() );

            while ( !this.records.isEmpty() )
            {
                Record record = (Record) this.records.poll();
                result.add( mapFunction.apply( record ) );
            }

            return result;
        }
    }

    private Throwable extractFailure()
    {
        if ( this.failure == null )
        {
            throw new IllegalStateException( "Can't extract failure because it does not exist" );
        }
        else
        {
            Throwable error = this.failure;
            this.failure = null;
            return error;
        }
    }

    private void completeRecordFuture( Record record )
    {
        if ( this.recordFuture != null )
        {
            CompletableFuture<Record> future = this.recordFuture;
            this.recordFuture = null;
            future.complete( record );
        }
    }

    private void completeSummaryFuture( ResultSummary summary )
    {
        if ( this.summaryFuture != null )
        {
            CompletableFuture<ResultSummary> future = this.summaryFuture;
            this.summaryFuture = null;
            future.complete( summary );
        }
    }

    private boolean failRecordFuture( Throwable error )
    {
        if ( this.recordFuture != null )
        {
            CompletableFuture<Record> future = this.recordFuture;
            this.recordFuture = null;
            future.completeExceptionally( error );
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean failSummaryFuture( Throwable error )
    {
        if ( this.summaryFuture != null )
        {
            CompletableFuture<ResultSummary> future = this.summaryFuture;
            this.summaryFuture = null;
            future.completeExceptionally( error );
            return true;
        }
        else
        {
            return false;
        }
    }

    private <T> CompletionStage<T> completedWithValueIfNoFailure( T value )
    {
        if ( this.failure != null )
        {
            return Futures.failedFuture( this.extractFailure() );
        }
        else
        {
            return value == null ? Futures.completedWithNull() : CompletableFuture.completedFuture( value );
        }
    }
}
