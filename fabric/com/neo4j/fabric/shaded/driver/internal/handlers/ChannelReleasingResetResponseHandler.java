package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.InboundMessageDispatcher;
import com.neo4j.fabric.shaded.driver.internal.async.pool.ExtendedChannelPool;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ChannelReleasingResetResponseHandler extends ResetResponseHandler
{
    private final Channel channel;
    private final ExtendedChannelPool pool;
    private final Clock clock;

    public ChannelReleasingResetResponseHandler( Channel channel, ExtendedChannelPool pool, InboundMessageDispatcher messageDispatcher, Clock clock,
            CompletableFuture<Void> releaseFuture )
    {
        super( messageDispatcher, releaseFuture );
        this.channel = channel;
        this.pool = pool;
        this.clock = clock;
    }

    protected void resetCompleted( CompletableFuture<Void> completionFuture, boolean success )
    {
        if ( success )
        {
            ChannelAttributes.setLastUsedTimestamp( this.channel, this.clock.millis() );
        }
        else
        {
            this.channel.close();
        }

        CompletionStage<Void> released = this.pool.release( this.channel );
        released.whenComplete( ( ignore, error ) -> {
            completionFuture.complete( (Object) null );
        } );
    }
}
