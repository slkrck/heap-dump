package com.neo4j.fabric.shaded.driver.internal.handlers.pulln;

public class FetchSizeUtil
{
    public static final long UNLIMITED_FETCH_SIZE = -1L;
    public static final long DEFAULT_FETCH_SIZE = 1000L;

    public static long assertValidFetchSize( long size )
    {
        if ( size <= 0L && size != -1L )
        {
            throw new IllegalArgumentException( String.format( "The record fetch size may not be 0 or negative. Illegal record fetch size: %s.", size ) );
        }
        else
        {
            return size;
        }
    }
}
