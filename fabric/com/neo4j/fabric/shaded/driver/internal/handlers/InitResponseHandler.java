package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.Map;

public class InitResponseHandler implements ResponseHandler
{
    private final ChannelPromise connectionInitializedPromise;
    private final Channel channel;

    public InitResponseHandler( ChannelPromise connectionInitializedPromise )
    {
        this.connectionInitializedPromise = connectionInitializedPromise;
        this.channel = connectionInitializedPromise.channel();
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        try
        {
            ServerVersion serverVersion = MetadataExtractor.extractNeo4jServerVersion( metadata );
            ChannelAttributes.setServerVersion( this.channel, serverVersion );
            this.connectionInitializedPromise.setSuccess();
        }
        catch ( Throwable var3 )
        {
            this.connectionInitializedPromise.setFailure( var3 );
            throw var3;
        }
    }

    public void onFailure( Throwable error )
    {
        this.channel.close().addListener( ( future ) -> {
            this.connectionInitializedPromise.setFailure( error );
        } );
    }

    public void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException();
    }
}
