package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.AutoPullResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.BasicPullResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.PullResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.v1.BoltProtocolV1;
import com.neo4j.fabric.shaded.driver.internal.messaging.v3.BoltProtocolV3;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;

public class PullHandlers
{
    public static PullAllResponseHandler newBoltV1PullAllHandler( Query query, RunResponseHandler runHandler, Connection connection, UnmanagedTransaction tx )
    {
        PullResponseCompletionListener completionListener = createPullResponseCompletionListener( connection, BookmarkHolder.NO_OP, tx );
        return new LegacyPullAllResponseHandler( query, runHandler, connection, BoltProtocolV1.METADATA_EXTRACTOR, completionListener );
    }

    public static PullAllResponseHandler newBoltV3PullAllHandler( Query query, RunResponseHandler runHandler, Connection connection,
            BookmarkHolder bookmarkHolder, UnmanagedTransaction tx )
    {
        PullResponseCompletionListener completionListener = createPullResponseCompletionListener( connection, bookmarkHolder, tx );
        return new LegacyPullAllResponseHandler( query, runHandler, connection, BoltProtocolV3.METADATA_EXTRACTOR, completionListener );
    }

    public static PullAllResponseHandler newBoltV4AutoPullHandler( Query query, RunResponseHandler runHandler, Connection connection,
            BookmarkHolder bookmarkHolder, UnmanagedTransaction tx, long fetchSize )
    {
        PullResponseCompletionListener completionListener = createPullResponseCompletionListener( connection, bookmarkHolder, tx );
        return new AutoPullResponseHandler( query, runHandler, connection, BoltProtocolV3.METADATA_EXTRACTOR, completionListener, fetchSize );
    }

    public static PullResponseHandler newBoltV4BasicPullHandler( Query query, RunResponseHandler runHandler, Connection connection,
            BookmarkHolder bookmarkHolder, UnmanagedTransaction tx )
    {
        PullResponseCompletionListener completionListener = createPullResponseCompletionListener( connection, bookmarkHolder, tx );
        return new BasicPullResponseHandler( query, runHandler, connection, BoltProtocolV3.METADATA_EXTRACTOR, completionListener );
    }

    private static PullResponseCompletionListener createPullResponseCompletionListener( Connection connection, BookmarkHolder bookmarkHolder,
            UnmanagedTransaction tx )
    {
        return (PullResponseCompletionListener) (tx != null ? new TransactionPullResponseCompletionListener( tx )
                                                            : new SessionPullResponseCompletionListener( connection, bookmarkHolder ));
    }
}
