package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class RunResponseHandler implements ResponseHandler
{
    private final CompletableFuture<Throwable> runCompletedFuture;
    private final MetadataExtractor metadataExtractor;
    private long queryId;
    private List<String> queryKeys;
    private long resultAvailableAfter;

    public RunResponseHandler( MetadataExtractor metadataExtractor )
    {
        this( new CompletableFuture(), metadataExtractor );
    }

    public RunResponseHandler( CompletableFuture<Throwable> runCompletedFuture, MetadataExtractor metadataExtractor )
    {
        this.queryId = -1L;
        this.queryKeys = Collections.emptyList();
        this.resultAvailableAfter = -1L;
        this.runCompletedFuture = runCompletedFuture;
        this.metadataExtractor = metadataExtractor;
    }

    public void onSuccess( Map<String,Value> metadata )
    {
        this.queryKeys = this.metadataExtractor.extractQueryKeys( metadata );
        this.resultAvailableAfter = this.metadataExtractor.extractResultAvailableAfter( metadata );
        this.queryId = this.metadataExtractor.extractQueryId( metadata );
        this.completeRunFuture( (Throwable) null );
    }

    public void onFailure( Throwable error )
    {
        this.completeRunFuture( error );
    }

    public void onRecord( Value[] fields )
    {
        throw new UnsupportedOperationException();
    }

    public List<String> queryKeys()
    {
        return this.queryKeys;
    }

    public long resultAvailableAfter()
    {
        return this.resultAvailableAfter;
    }

    public long queryId()
    {
        return this.queryId;
    }

    private void completeRunFuture( Throwable error )
    {
        this.runCompletedFuture.complete( error );
    }

    public CompletableFuture<Throwable> runFuture()
    {
        return this.runCompletedFuture;
    }
}
