package com.neo4j.fabric.shaded.driver.internal.handlers;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;

import java.util.Map;

public class NoOpResponseHandler implements ResponseHandler
{
    public static final NoOpResponseHandler INSTANCE = new NoOpResponseHandler();

    public void onSuccess( Map<String,Value> metadata )
    {
    }

    public void onFailure( Throwable error )
    {
    }

    public void onRecord( Value[] fields )
    {
    }
}
