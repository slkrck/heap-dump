package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.AuthToken;
import com.neo4j.fabric.shaded.driver.Session;

public class ConnectionSettings
{
    private static final String DEFAULT_USER_AGENT = String.format( "neo4j-java/%s", driverVersion() );
    private final AuthToken authToken;
    private final String userAgent;
    private final int connectTimeoutMillis;

    public ConnectionSettings( AuthToken authToken, String userAgent, int connectTimeoutMillis )
    {
        this.authToken = authToken;
        this.userAgent = userAgent;
        this.connectTimeoutMillis = connectTimeoutMillis;
    }

    public ConnectionSettings( AuthToken authToken, int connectTimeoutMillis )
    {
        this( authToken, DEFAULT_USER_AGENT, connectTimeoutMillis );
    }

    private static String driverVersion()
    {
        Package pkg = Session.class.getPackage();
        return pkg != null && pkg.getImplementationVersion() != null ? pkg.getImplementationVersion() : "dev";
    }

    public AuthToken authToken()
    {
        return this.authToken;
    }

    public String userAgent()
    {
        return this.userAgent;
    }

    public int connectTimeoutMillis()
    {
        return this.connectTimeoutMillis;
    }
}
