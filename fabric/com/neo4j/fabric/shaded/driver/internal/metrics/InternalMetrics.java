package com.neo4j.fabric.shaded.driver.internal.metrics;

import com.neo4j.fabric.shaded.driver.ConnectionPoolMetrics;
import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.pool.ConnectionPoolImpl;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class InternalMetrics extends InternalAbstractMetrics
{
    private final Map<String,ConnectionPoolMetrics> connectionPoolMetrics;
    private final Clock clock;
    private final Logger log;

    InternalMetrics( Clock clock, Logging logging )
    {
        Objects.requireNonNull( clock );
        this.connectionPoolMetrics = new ConcurrentHashMap();
        this.clock = clock;
        this.log = logging.getLog( this.getClass().getSimpleName() );
    }

    public void putPoolMetrics( String poolId, BoltServerAddress serverAddress, ConnectionPoolImpl pool )
    {
        this.connectionPoolMetrics.put( poolId, new InternalConnectionPoolMetrics( poolId, serverAddress, pool ) );
    }

    public void removePoolMetrics( String id )
    {
        this.connectionPoolMetrics.remove( id );
    }

    public void beforeCreating( String poolId, ListenerEvent creatingEvent )
    {
        this.poolMetrics( poolId ).beforeCreating( creatingEvent );
    }

    public void afterCreated( String poolId, ListenerEvent creatingEvent )
    {
        this.poolMetrics( poolId ).afterCreated( creatingEvent );
    }

    public void afterFailedToCreate( String poolId )
    {
        this.poolMetrics( poolId ).afterFailedToCreate();
    }

    public void afterClosed( String poolId )
    {
        this.poolMetrics( poolId ).afterClosed();
    }

    public void beforeAcquiringOrCreating( String poolId, ListenerEvent acquireEvent )
    {
        this.poolMetrics( poolId ).beforeAcquiringOrCreating( acquireEvent );
    }

    public void afterAcquiringOrCreating( String poolId )
    {
        this.poolMetrics( poolId ).afterAcquiringOrCreating();
    }

    public void afterAcquiredOrCreated( String poolId, ListenerEvent acquireEvent )
    {
        this.poolMetrics( poolId ).afterAcquiredOrCreated( acquireEvent );
    }

    public void afterConnectionCreated( String poolId, ListenerEvent inUseEvent )
    {
        this.poolMetrics( poolId ).acquired( inUseEvent );
    }

    public void afterConnectionReleased( String poolId, ListenerEvent inUseEvent )
    {
        this.poolMetrics( poolId ).released( inUseEvent );
    }

    public void afterTimedOutToAcquireOrCreate( String poolId )
    {
        this.poolMetrics( poolId ).afterTimedOutToAcquireOrCreate();
    }

    public ListenerEvent createListenerEvent()
    {
        return new TimeRecorderListenerEvent( this.clock );
    }

    public Collection<ConnectionPoolMetrics> connectionPoolMetrics()
    {
        return Collections.unmodifiableCollection( this.connectionPoolMetrics.values() );
    }

    public String toString()
    {
        return String.format( "PoolMetrics=%s", this.connectionPoolMetrics );
    }

    private ConnectionPoolMetricsListener poolMetrics( String poolId )
    {
        InternalConnectionPoolMetrics poolMetrics = (InternalConnectionPoolMetrics) this.connectionPoolMetrics.get( poolId );
        if ( poolMetrics == null )
        {
            this.log.warn( String.format( "Failed to find pool metrics with id `%s` in %s.", poolId, this.connectionPoolMetrics ) );
            return ConnectionPoolMetricsListener.DEV_NULL_POOL_METRICS_LISTENER;
        }
        else
        {
            return poolMetrics;
        }
    }
}
