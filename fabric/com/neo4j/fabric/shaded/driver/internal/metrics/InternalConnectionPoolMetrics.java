package com.neo4j.fabric.shaded.driver.internal.metrics;

import com.neo4j.fabric.shaded.driver.ConnectionPoolMetrics;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class InternalConnectionPoolMetrics implements ConnectionPoolMetrics, ConnectionPoolMetricsListener
{
    private final BoltServerAddress address;
    private final ConnectionPool pool;
    private final AtomicLong closed = new AtomicLong();
    private final AtomicInteger creating = new AtomicInteger();
    private final AtomicLong created = new AtomicLong();
    private final AtomicLong failedToCreate = new AtomicLong();
    private final AtomicInteger acquiring = new AtomicInteger();
    private final AtomicLong acquired = new AtomicLong();
    private final AtomicLong timedOutToAcquire = new AtomicLong();
    private final AtomicLong totalAcquisitionTime = new AtomicLong();
    private final AtomicLong totalConnectionTime = new AtomicLong();
    private final AtomicLong totalInUseTime = new AtomicLong();
    private final AtomicLong totalInUseCount = new AtomicLong();
    private final String id;

    InternalConnectionPoolMetrics( String poolId, BoltServerAddress address, ConnectionPool pool )
    {
        Objects.requireNonNull( address );
        Objects.requireNonNull( pool );
        this.id = poolId;
        this.address = address;
        this.pool = pool;
    }

    public void beforeCreating( ListenerEvent connEvent )
    {
        this.creating.incrementAndGet();
        connEvent.start();
    }

    public void afterFailedToCreate()
    {
        this.failedToCreate.incrementAndGet();
        this.creating.decrementAndGet();
    }

    public void afterCreated( ListenerEvent connEvent )
    {
        this.created.incrementAndGet();
        this.creating.decrementAndGet();
        long elapsed = connEvent.elapsed();
        this.totalConnectionTime.addAndGet( elapsed );
    }

    public void afterClosed()
    {
        this.closed.incrementAndGet();
    }

    public void beforeAcquiringOrCreating( ListenerEvent acquireEvent )
    {
        acquireEvent.start();
        this.acquiring.incrementAndGet();
    }

    public void afterAcquiringOrCreating()
    {
        this.acquiring.decrementAndGet();
    }

    public void afterAcquiredOrCreated( ListenerEvent acquireEvent )
    {
        this.acquired.incrementAndGet();
        long elapsed = acquireEvent.elapsed();
        this.totalAcquisitionTime.addAndGet( elapsed );
    }

    public void afterTimedOutToAcquireOrCreate()
    {
        this.timedOutToAcquire.incrementAndGet();
    }

    public void acquired( ListenerEvent inUseEvent )
    {
        inUseEvent.start();
    }

    public void released( ListenerEvent inUseEvent )
    {
        this.totalInUseCount.incrementAndGet();
        long elapsed = inUseEvent.elapsed();
        this.totalInUseTime.addAndGet( elapsed );
    }

    public String id()
    {
        return this.id;
    }

    public int inUse()
    {
        return this.pool.inUseConnections( this.address );
    }

    public int idle()
    {
        return this.pool.idleConnections( this.address );
    }

    public int creating()
    {
        return this.creating.get();
    }

    public long created()
    {
        return this.created.get();
    }

    public long failedToCreate()
    {
        return this.failedToCreate.get();
    }

    public long timedOutToAcquire()
    {
        return this.timedOutToAcquire.get();
    }

    public long totalAcquisitionTime()
    {
        return this.totalAcquisitionTime.get();
    }

    public long totalConnectionTime()
    {
        return this.totalConnectionTime.get();
    }

    public long totalInUseTime()
    {
        return this.totalInUseTime.get();
    }

    public long totalInUseCount()
    {
        return this.totalInUseCount.get();
    }

    public long closed()
    {
        return this.closed.get();
    }

    public int acquiring()
    {
        return this.acquiring.get();
    }

    public long acquired()
    {
        return this.acquired.get();
    }

    public String toString()
    {
        return String.format(
                "%s=[created=%s, closed=%s, creating=%s, failedToCreate=%s, acquiring=%s, acquired=%s, timedOutToAcquire=%s, inUse=%s, idle=%s, totalAcquisitionTime=%s, totalConnectionTime=%s, totalInUseTime=%s, totalInUseCount=%s]",
                this.id(), this.created(), this.closed(), this.creating(), this.failedToCreate(), this.acquiring(), this.acquired(), this.timedOutToAcquire(),
                this.inUse(), this.idle(), this.totalAcquisitionTime(), this.totalConnectionTime(), this.totalInUseTime(), this.totalInUseCount() );
    }
}
