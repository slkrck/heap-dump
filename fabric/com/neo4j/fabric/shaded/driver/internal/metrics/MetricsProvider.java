package com.neo4j.fabric.shaded.driver.internal.metrics;

import com.neo4j.fabric.shaded.driver.Metrics;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;

public interface MetricsProvider
{
    MetricsProvider METRICS_DISABLED_PROVIDER = new MetricsProvider()
    {
        public Metrics metrics()
        {
            throw new ClientException(
                    "Driver metrics not enabled. To access driver metrics, you need to enabled driver metrics in the driver's configuration." );
        }

        public MetricsListener metricsListener()
        {
            return InternalAbstractMetrics.DEV_NULL_METRICS;
        }

        public boolean isMetricsEnabled()
        {
            return false;
        }
    };

    Metrics metrics();

    MetricsListener metricsListener();

    boolean isMetricsEnabled();
}
