package com.neo4j.fabric.shaded.driver.internal.metrics;

public interface ListenerEvent
{
    ListenerEvent DEV_NULL_LISTENER_EVENT = new ListenerEvent()
    {
        public void start()
        {
        }

        public long elapsed()
        {
            return 0L;
        }
    };

    void start();

    long elapsed();
}
