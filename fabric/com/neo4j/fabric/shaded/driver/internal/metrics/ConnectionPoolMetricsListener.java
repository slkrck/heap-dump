package com.neo4j.fabric.shaded.driver.internal.metrics;

public interface ConnectionPoolMetricsListener
{
    ConnectionPoolMetricsListener DEV_NULL_POOL_METRICS_LISTENER = new ConnectionPoolMetricsListener()
    {
        public void beforeCreating( ListenerEvent listenerEvent )
        {
        }

        public void afterCreated( ListenerEvent listenerEvent )
        {
        }

        public void afterFailedToCreate()
        {
        }

        public void afterClosed()
        {
        }

        public void beforeAcquiringOrCreating( ListenerEvent acquireEvent )
        {
        }

        public void afterAcquiringOrCreating()
        {
        }

        public void afterAcquiredOrCreated( ListenerEvent acquireEvent )
        {
        }

        public void afterTimedOutToAcquireOrCreate()
        {
        }

        public void acquired( ListenerEvent inUseEvent )
        {
        }

        public void released( ListenerEvent inUseEvent )
        {
        }
    };

    void beforeCreating( ListenerEvent var1 );

    void afterCreated( ListenerEvent var1 );

    void afterFailedToCreate();

    void afterClosed();

    void beforeAcquiringOrCreating( ListenerEvent var1 );

    void afterAcquiringOrCreating();

    void afterAcquiredOrCreated( ListenerEvent var1 );

    void afterTimedOutToAcquireOrCreate();

    void acquired( ListenerEvent var1 );

    void released( ListenerEvent var1 );
}
