package com.neo4j.fabric.shaded.driver.internal.metrics;

import com.neo4j.fabric.shaded.driver.ConnectionPoolMetrics;
import com.neo4j.fabric.shaded.driver.Metrics;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.pool.ConnectionPoolImpl;

import java.util.Collection;
import java.util.Collections;

public abstract class InternalAbstractMetrics implements Metrics, MetricsListener
{
    public static final InternalAbstractMetrics DEV_NULL_METRICS = new InternalAbstractMetrics()
    {
        public void beforeCreating( String poolId, ListenerEvent creatingEvent )
        {
        }

        public void afterCreated( String poolId, ListenerEvent creatingEvent )
        {
        }

        public void afterFailedToCreate( String poolId )
        {
        }

        public void afterClosed( String poolId )
        {
        }

        public void beforeAcquiringOrCreating( String poolId, ListenerEvent acquireEvent )
        {
        }

        public void afterAcquiringOrCreating( String poolId )
        {
        }

        public void afterAcquiredOrCreated( String poolId, ListenerEvent acquireEvent )
        {
        }

        public void afterTimedOutToAcquireOrCreate( String poolId )
        {
        }

        public void afterConnectionCreated( String poolId, ListenerEvent inUseEvent )
        {
        }

        public void afterConnectionReleased( String poolId, ListenerEvent inUseEvent )
        {
        }

        public ListenerEvent createListenerEvent()
        {
            return ListenerEvent.DEV_NULL_LISTENER_EVENT;
        }

        public void putPoolMetrics( String id, BoltServerAddress address, ConnectionPoolImpl connectionPool )
        {
        }

        public void removePoolMetrics( String poolId )
        {
        }

        public Collection<ConnectionPoolMetrics> connectionPoolMetrics()
        {
            return Collections.emptySet();
        }

        public String toString()
        {
            return "Driver metrics not available while driver metrics is not enabled.";
        }
    };
}
