package com.neo4j.fabric.shaded.driver.internal.metrics;

import com.neo4j.fabric.shaded.driver.internal.util.Clock;

public class TimeRecorderListenerEvent implements ListenerEvent
{
    private final Clock clock;
    private long startTime;

    TimeRecorderListenerEvent( Clock clock )
    {
        this.clock = clock;
    }

    public void start()
    {
        this.startTime = this.clock.millis();
    }

    public long elapsed()
    {
        return this.clock.millis() - this.startTime;
    }
}
