package com.neo4j.fabric.shaded.driver.internal.metrics;

import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.Metrics;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

public class InternalMetricsProvider implements MetricsProvider
{
    private final InternalMetrics metrics;

    public InternalMetricsProvider( Clock clock, Logging logging )
    {
        this.metrics = new InternalMetrics( clock, logging );
    }

    public Metrics metrics()
    {
        return this.metrics;
    }

    public MetricsListener metricsListener()
    {
        return this.metrics;
    }

    public boolean isMetricsEnabled()
    {
        return true;
    }
}
