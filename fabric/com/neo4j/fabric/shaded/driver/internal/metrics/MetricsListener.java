package com.neo4j.fabric.shaded.driver.internal.metrics;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.pool.ConnectionPoolImpl;

public interface MetricsListener
{
    void beforeCreating( String var1, ListenerEvent var2 );

    void afterCreated( String var1, ListenerEvent var2 );

    void afterFailedToCreate( String var1 );

    void afterClosed( String var1 );

    void beforeAcquiringOrCreating( String var1, ListenerEvent var2 );

    void afterAcquiringOrCreating( String var1 );

    void afterAcquiredOrCreated( String var1, ListenerEvent var2 );

    void afterTimedOutToAcquireOrCreate( String var1 );

    void afterConnectionCreated( String var1, ListenerEvent var2 );

    void afterConnectionReleased( String var1, ListenerEvent var2 );

    ListenerEvent createListenerEvent();

    void putPoolMetrics( String var1, BoltServerAddress var2, ConnectionPoolImpl var3 );

    void removePoolMetrics( String var1 );
}
