package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

public final class InternalBookmark implements Bookmark
{
    private static final InternalBookmark EMPTY = new InternalBookmark( Collections.emptySet() );
    private final Set<String> values;

    private InternalBookmark( Set<String> values )
    {
        Objects.requireNonNull( values );
        this.values = values;
    }

    public static Bookmark empty()
    {
        return EMPTY;
    }

    public static Bookmark from( Iterable<Bookmark> bookmarks )
    {
        if ( bookmarks == null )
        {
            return empty();
        }
        else
        {
            int size = Iterables.count( bookmarks );
            if ( size == 0 )
            {
                return empty();
            }
            else if ( size == 1 )
            {
                return from( (Bookmark) bookmarks.iterator().next() );
            }
            else
            {
                Set<String> newValues = new HashSet();
                Iterator var3 = bookmarks.iterator();

                while ( var3.hasNext() )
                {
                    Bookmark value = (Bookmark) var3.next();
                    if ( value != null )
                    {
                        newValues.addAll( value.values() );
                    }
                }

                return new InternalBookmark( newValues );
            }
        }
    }

    private static Bookmark from( Bookmark bookmark )
    {
        return bookmark == null ? empty() : bookmark;
    }

    public static Bookmark parse( String value )
    {
        return (Bookmark) (value == null ? empty() : new InternalBookmark( Collections.singleton( value ) ));
    }

    public static Bookmark parse( Set<String> values )
    {
        return (Bookmark) (values == null ? empty() : new InternalBookmark( values ));
    }

    public boolean isEmpty()
    {
        return this.values.isEmpty();
    }

    public Set<String> values()
    {
        return Collections.unmodifiableSet( this.values );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalBookmark bookmark = (InternalBookmark) o;
            return Objects.equals( this.values, bookmark.values );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.values} );
    }

    public String toString()
    {
        return "Bookmark{values=" + this.values + "}";
    }
}
