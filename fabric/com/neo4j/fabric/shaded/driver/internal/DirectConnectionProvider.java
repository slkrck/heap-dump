package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.internal.async.ConnectionContext;
import com.neo4j.fabric.shaded.driver.internal.async.connection.DirectConnection;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.MultiDatabaseUtil;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionProvider;

import java.util.concurrent.CompletionStage;

public class DirectConnectionProvider implements ConnectionProvider
{
    private final BoltServerAddress address;
    private final ConnectionPool connectionPool;

    DirectConnectionProvider( BoltServerAddress address, ConnectionPool connectionPool )
    {
        this.address = address;
        this.connectionPool = connectionPool;
    }

    public CompletionStage<Connection> acquireConnection( ConnectionContext context )
    {
        return this.acquireConnection().thenApply( ( connection ) -> {
            return new DirectConnection( connection, context.databaseName(), context.mode() );
        } );
    }

    public CompletionStage<Void> verifyConnectivity()
    {
        return this.acquireConnection().thenCompose( Connection::release );
    }

    public CompletionStage<Void> close()
    {
        return this.connectionPool.close();
    }

    public CompletionStage<Boolean> supportsMultiDb()
    {
        return this.acquireConnection().thenCompose( ( conn ) -> {
            boolean supportsMultiDatabase = MultiDatabaseUtil.supportsMultiDatabase( conn );
            return conn.release().thenApply( ( ignored ) -> {
                return supportsMultiDatabase;
            } );
        } );
    }

    public BoltServerAddress getAddress()
    {
        return this.address;
    }

    private CompletionStage<Connection> acquireConnection()
    {
        return this.connectionPool.acquire( this.address );
    }
}
