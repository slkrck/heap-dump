package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Bookmark;

public class DefaultBookmarkHolder implements BookmarkHolder
{
    private volatile Bookmark bookmark;

    public DefaultBookmarkHolder()
    {
        this( InternalBookmark.empty() );
    }

    public DefaultBookmarkHolder( Bookmark bookmark )
    {
        this.bookmark = bookmark;
    }

    public Bookmark getBookmark()
    {
        return this.bookmark;
    }

    public void setBookmark( Bookmark bookmark )
    {
        if ( bookmark != null && !bookmark.isEmpty() )
        {
            this.bookmark = bookmark;
        }
    }
}
