package com.neo4j.fabric.shaded.driver.internal.util;

import com.neo4j.fabric.shaded.driver.Driver;
import com.neo4j.fabric.shaded.driver.Session;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerVersion
{
    public static final String NEO4J_PRODUCT = "Neo4j";
    public static final ServerVersion v4_0_0 = new ServerVersion( "Neo4j", 4, 0, 0 );
    public static final ServerVersion v3_5_0 = new ServerVersion( "Neo4j", 3, 5, 0 );
    public static final ServerVersion v3_4_0 = new ServerVersion( "Neo4j", 3, 4, 0 );
    public static final ServerVersion vInDev = new ServerVersion( "Neo4j", Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE );
    private static final String NEO4J_IN_DEV_VERSION_STRING = "Neo4j/dev";
    private static final Pattern PATTERN = Pattern.compile( "([^/]+)/(\\d+)\\.(\\d+)(?:\\.)?(\\d*)(\\.|-|\\+)?([0-9A-Za-z-.]*)?" );
    private final String product;
    private final int major;
    private final int minor;
    private final int patch;
    private final String stringValue;

    private ServerVersion( String product, int major, int minor, int patch )
    {
        this.product = product;
        this.major = major;
        this.minor = minor;
        this.patch = patch;
        this.stringValue = stringValue( product, major, minor, patch );
    }

    public static ServerVersion version( Driver driver )
    {
        Session session = driver.session();
        Throwable var2 = null;

        ServerVersion var4;
        try
        {
            String versionString = (String) session.readTransaction( ( tx ) -> {
                return tx.run( "RETURN 1" ).consume().server().version();
            } );
            var4 = version( versionString );
        }
        catch ( Throwable var13 )
        {
            var2 = var13;
            throw var13;
        }
        finally
        {
            if ( session != null )
            {
                if ( var2 != null )
                {
                    try
                    {
                        session.close();
                    }
                    catch ( Throwable var12 )
                    {
                        var2.addSuppressed( var12 );
                    }
                }
                else
                {
                    session.close();
                }
            }
        }

        return var4;
    }

    public static ServerVersion version( String server )
    {
        Matcher matcher = PATTERN.matcher( server );
        if ( matcher.matches() )
        {
            String product = matcher.group( 1 );
            int major = Integer.valueOf( matcher.group( 2 ) );
            int minor = Integer.valueOf( matcher.group( 3 ) );
            String patchString = matcher.group( 4 );
            int patch = 0;
            if ( patchString != null && !patchString.isEmpty() )
            {
                patch = Integer.valueOf( patchString );
            }

            return new ServerVersion( product, major, minor, patch );
        }
        else if ( server.equalsIgnoreCase( "Neo4j/dev" ) )
        {
            return vInDev;
        }
        else
        {
            throw new IllegalArgumentException( "Cannot parse " + server );
        }
    }

    private static String stringValue( String product, int major, int minor, int patch )
    {
        return major == Integer.MAX_VALUE && minor == Integer.MAX_VALUE && patch == Integer.MAX_VALUE ? "Neo4j/dev"
                                                                                                      : String.format( "%s/%s.%s.%s", product, major, minor,
                                                                                                              patch );
    }

    public String product()
    {
        return this.product;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ServerVersion that = (ServerVersion) o;
            if ( !this.product.equals( that.product ) )
            {
                return false;
            }
            else if ( this.major != that.major )
            {
                return false;
            }
            else if ( this.minor != that.minor )
            {
                return false;
            }
            else
            {
                return this.patch == that.patch;
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.product, this.major, this.minor, this.patch} );
    }

    public boolean greaterThan( ServerVersion other )
    {
        return this.compareTo( other ) > 0;
    }

    public boolean greaterThanOrEqual( ServerVersion other )
    {
        return this.compareTo( other ) >= 0;
    }

    public boolean lessThan( ServerVersion other )
    {
        return this.compareTo( other ) < 0;
    }

    public boolean lessThanOrEqual( ServerVersion other )
    {
        return this.compareTo( other ) <= 0;
    }

    private int compareTo( ServerVersion o )
    {
        if ( !this.product.equals( o.product ) )
        {
            throw new IllegalArgumentException( "Comparing different products '" + this.product + "' with '" + o.product + "'" );
        }
        else
        {
            int c = Integer.compare( this.major, o.major );
            if ( c == 0 )
            {
                c = Integer.compare( this.minor, o.minor );
                if ( c == 0 )
                {
                    c = Integer.compare( this.patch, o.patch );
                }
            }

            return c;
        }
    }

    public String toString()
    {
        return this.stringValue;
    }
}
