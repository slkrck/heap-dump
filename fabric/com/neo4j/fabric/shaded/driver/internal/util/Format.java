package com.neo4j.fabric.shaded.driver.internal.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class Format
{
    private Format()
    {
        throw new UnsupportedOperationException();
    }

    public static <V> String formatPairs( Map<String,V> entries )
    {
        Iterator<Entry<String,V>> iterator = entries.entrySet().iterator();
        switch ( entries.size() )
        {
        case 0:
            return "{}";
        case 1:
            return String.format( "{%s}", keyValueString( (Entry) iterator.next() ) );
        default:
            StringBuilder builder = new StringBuilder();
            builder.append( "{" );
            builder.append( keyValueString( (Entry) iterator.next() ) );

            while ( iterator.hasNext() )
            {
                builder.append( ',' );
                builder.append( ' ' );
                builder.append( keyValueString( (Entry) iterator.next() ) );
            }

            builder.append( "}" );
            return builder.toString();
        }
    }

    private static <V> String keyValueString( Entry<String,V> entry )
    {
        return String.format( "%s: %s", entry.getKey(), String.valueOf( entry.getValue() ) );
    }

    public static String valueOrEmpty( String value )
    {
        return value != null ? value : "";
    }
}
