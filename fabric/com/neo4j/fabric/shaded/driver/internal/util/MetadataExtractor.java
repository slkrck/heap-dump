package com.neo4j.fabric.shaded.driver.internal.util;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.exceptions.UntrustedServerException;
import com.neo4j.fabric.shaded.driver.internal.InternalBookmark;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.summary.InternalDatabaseInfo;
import com.neo4j.fabric.shaded.driver.internal.summary.InternalNotification;
import com.neo4j.fabric.shaded.driver.internal.summary.InternalPlan;
import com.neo4j.fabric.shaded.driver.internal.summary.InternalProfiledPlan;
import com.neo4j.fabric.shaded.driver.internal.summary.InternalResultSummary;
import com.neo4j.fabric.shaded.driver.internal.summary.InternalServerInfo;
import com.neo4j.fabric.shaded.driver.internal.summary.InternalSummaryCounters;
import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.summary.DatabaseInfo;
import com.neo4j.fabric.shaded.driver.summary.Notification;
import com.neo4j.fabric.shaded.driver.summary.Plan;
import com.neo4j.fabric.shaded.driver.summary.ProfiledPlan;
import com.neo4j.fabric.shaded.driver.summary.QueryType;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;
import com.neo4j.fabric.shaded.driver.summary.ServerInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MetadataExtractor
{
    public static final int ABSENT_QUERY_ID = -1;
    private final String resultAvailableAfterMetadataKey;
    private final String resultConsumedAfterMetadataKey;

    public MetadataExtractor( String resultAvailableAfterMetadataKey, String resultConsumedAfterMetadataKey )
    {
        this.resultAvailableAfterMetadataKey = resultAvailableAfterMetadataKey;
        this.resultConsumedAfterMetadataKey = resultConsumedAfterMetadataKey;
    }

    public static DatabaseInfo extractDatabaseInfo( Map<String,Value> metadata )
    {
        Value dbValue = (Value) metadata.get( "db" );
        return (DatabaseInfo) (dbValue != null && !dbValue.isNull() ? new InternalDatabaseInfo( dbValue.asString() )
                                                                    : InternalDatabaseInfo.DEFAULT_DATABASE_INFO);
    }

    public static Bookmark extractBookmarks( Map<String,Value> metadata )
    {
        Value bookmarkValue = (Value) metadata.get( "bookmark" );
        return bookmarkValue != null && !bookmarkValue.isNull() && bookmarkValue.hasType( InternalTypeSystem.TYPE_SYSTEM.STRING() ) ? InternalBookmark.parse(
                bookmarkValue.asString() ) : InternalBookmark.empty();
    }

    public static ServerVersion extractNeo4jServerVersion( Map<String,Value> metadata )
    {
        Value versionValue = (Value) metadata.get( "server" );
        if ( versionValue != null && !versionValue.isNull() )
        {
            ServerVersion server = ServerVersion.version( versionValue.asString() );
            if ( "Neo4j".equalsIgnoreCase( server.product() ) )
            {
                return server;
            }
            else
            {
                throw new UntrustedServerException( "Server does not identify as a genuine Neo4j instance: '" + server.product() + "'" );
            }
        }
        else
        {
            throw new UntrustedServerException( "Server provides no product identifier" );
        }
    }

    private static QueryType extractQueryType( Map<String,Value> metadata )
    {
        Value typeValue = (Value) metadata.get( "type" );
        return typeValue != null ? QueryType.fromCode( typeValue.asString() ) : null;
    }

    private static InternalSummaryCounters extractCounters( Map<String,Value> metadata )
    {
        Value countersValue = (Value) metadata.get( "stats" );
        return countersValue != null ? new InternalSummaryCounters( counterValue( countersValue, "nodes-created" ),
                counterValue( countersValue, "nodes-deleted" ), counterValue( countersValue, "relationships-created" ),
                counterValue( countersValue, "relationships-deleted" ), counterValue( countersValue, "properties-set" ),
                counterValue( countersValue, "labels-added" ), counterValue( countersValue, "labels-removed" ), counterValue( countersValue, "indexes-added" ),
                counterValue( countersValue, "indexes-removed" ), counterValue( countersValue, "constraints-added" ),
                counterValue( countersValue, "constraints-removed" ), counterValue( countersValue, "system-updates" ) ) : null;
    }

    private static int counterValue( Value countersValue, String name )
    {
        Value value = countersValue.get( name );
        return value.isNull() ? 0 : value.asInt();
    }

    private static Plan extractPlan( Map<String,Value> metadata )
    {
        Value planValue = (Value) metadata.get( "plan" );
        return planValue != null ? (Plan) InternalPlan.EXPLAIN_PLAN_FROM_VALUE.apply( planValue ) : null;
    }

    private static ProfiledPlan extractProfiledPlan( Map<String,Value> metadata )
    {
        Value profiledPlanValue = (Value) metadata.get( "profile" );
        return profiledPlanValue != null ? (ProfiledPlan) InternalProfiledPlan.PROFILED_PLAN_FROM_VALUE.apply( profiledPlanValue ) : null;
    }

    private static List<Notification> extractNotifications( Map<String,Value> metadata )
    {
        Value notificationsValue = (Value) metadata.get( "notifications" );
        return notificationsValue != null ? notificationsValue.asList( InternalNotification.VALUE_TO_NOTIFICATION ) : Collections.emptyList();
    }

    private static long extractResultConsumedAfter( Map<String,Value> metadata, String key )
    {
        Value resultConsumedAfterValue = (Value) metadata.get( key );
        return resultConsumedAfterValue != null ? resultConsumedAfterValue.asLong() : -1L;
    }

    public List<String> extractQueryKeys( Map<String,Value> metadata )
    {
        Value keysValue = (Value) metadata.get( "fields" );
        if ( keysValue != null && !keysValue.isEmpty() )
        {
            List<String> keys = new ArrayList( keysValue.size() );
            Iterator var4 = keysValue.values().iterator();

            while ( var4.hasNext() )
            {
                Value value = (Value) var4.next();
                keys.add( value.asString() );
            }

            return keys;
        }
        else
        {
            return Collections.emptyList();
        }
    }

    public long extractQueryId( Map<String,Value> metadata )
    {
        Value queryId = (Value) metadata.get( "qid" );
        return queryId != null ? queryId.asLong() : -1L;
    }

    public long extractResultAvailableAfter( Map<String,Value> metadata )
    {
        Value resultAvailableAfterValue = (Value) metadata.get( this.resultAvailableAfterMetadataKey );
        return resultAvailableAfterValue != null ? resultAvailableAfterValue.asLong() : -1L;
    }

    public ResultSummary extractSummary( Query query, Connection connection, long resultAvailableAfter, Map<String,Value> metadata )
    {
        ServerInfo serverInfo = new InternalServerInfo( connection.serverAddress(), connection.serverVersion() );
        DatabaseInfo dbInfo = extractDatabaseInfo( metadata );
        return new InternalResultSummary( query, serverInfo, dbInfo, extractQueryType( metadata ), extractCounters( metadata ), extractPlan( metadata ),
                extractProfiledPlan( metadata ), extractNotifications( metadata ), resultAvailableAfter,
                extractResultConsumedAfter( metadata, this.resultConsumedAfterMetadataKey ) );
    }
}
