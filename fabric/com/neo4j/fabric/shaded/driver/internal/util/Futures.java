package com.neo4j.fabric.shaded.driver.internal.util;

import com.neo4j.fabric.shaded.driver.internal.async.connection.EventLoopGroupFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

public final class Futures
{
    private static final CompletableFuture<?> COMPLETED_WITH_NULL = CompletableFuture.completedFuture( (Object) null );

    private Futures()
    {
    }

    public static <T> CompletableFuture<T> completedWithNull()
    {
        return COMPLETED_WITH_NULL;
    }

    public static <T> CompletableFuture<T> completeWithNullIfNoError( CompletableFuture<T> future, Throwable error )
    {
        if ( error != null )
        {
            future.completeExceptionally( error );
        }
        else
        {
            future.complete( (Object) null );
        }

        return future;
    }

    public static <T> CompletionStage<T> asCompletionStage( Future<T> future )
    {
        CompletableFuture<T> result = new CompletableFuture();
        return asCompletionStage( future, result );
    }

    public static <T> CompletionStage<T> asCompletionStage( Future<T> future, CompletableFuture<T> result )
    {
        if ( future.isCancelled() )
        {
            result.cancel( true );
        }
        else if ( future.isSuccess() )
        {
            result.complete( future.getNow() );
        }
        else if ( future.cause() != null )
        {
            result.completeExceptionally( future.cause() );
        }
        else
        {
            future.addListener( ( ignore ) -> {
                if ( future.isCancelled() )
                {
                    result.cancel( true );
                }
                else if ( future.isSuccess() )
                {
                    result.complete( future.getNow() );
                }
                else
                {
                    result.completeExceptionally( future.cause() );
                }
            } );
        }

        return result;
    }

    public static <T> CompletableFuture<T> failedFuture( Throwable error )
    {
        CompletableFuture<T> result = new CompletableFuture();
        result.completeExceptionally( error );
        return result;
    }

    public static <V> V blockingGet( CompletionStage<V> stage )
    {
        return blockingGet( stage, Futures::noOpInterruptHandler );
    }

    public static <V> V blockingGet( CompletionStage<V> stage, Runnable interruptHandler )
    {
        EventLoopGroupFactory.assertNotInEventLoopThread();
        java.util.concurrent.Future<V> future = stage.toCompletableFuture();
        boolean interrupted = false;

        try
        {
            while ( true )
            {
                try
                {
                    Object var4 = future.get();
                    return var4;
                }
                catch ( InterruptedException var9 )
                {
                    interrupted = true;
                    safeRun( interruptHandler );
                }
                catch ( ExecutionException var10 )
                {
                    ErrorUtil.rethrowAsyncException( var10 );
                }
            }
        }
        finally
        {
            if ( interrupted )
            {
                Thread.currentThread().interrupt();
            }
        }
    }

    public static <T> T getNow( CompletionStage<T> stage )
    {
        return stage.toCompletableFuture().getNow( (Object) null );
    }

    public static Throwable completionExceptionCause( Throwable error )
    {
        return error instanceof CompletionException ? error.getCause() : error;
    }

    public static CompletionException asCompletionException( Throwable error )
    {
        return error instanceof CompletionException ? (CompletionException) error : new CompletionException( error );
    }

    public static CompletionException combineErrors( Throwable error1, Throwable error2 )
    {
        if ( error1 != null && error2 != null )
        {
            Throwable cause1 = completionExceptionCause( error1 );
            Throwable cause2 = completionExceptionCause( error2 );
            ErrorUtil.addSuppressed( cause1, cause2 );
            return asCompletionException( cause1 );
        }
        else if ( error1 != null )
        {
            return asCompletionException( error1 );
        }
        else
        {
            return error2 != null ? asCompletionException( error2 ) : null;
        }
    }

    public static <T> CompletableFuture<T> onErrorContinue( CompletableFuture<T> future, Throwable errorRecorder,
            Function<Throwable,? extends CompletionStage<T>> onErrorAction )
    {
        Objects.requireNonNull( future );
        return future.handle( ( value, error ) -> {
            if ( error != null )
            {
                combineErrors( errorRecorder, error );
                return new Futures.CompletionResult( (Object) null, error );
            }
            else
            {
                return new Futures.CompletionResult( value, (Throwable) null );
            }
        } ).thenCompose( ( result ) -> {
            return (CompletionStage) (result.value != null ? CompletableFuture.completedFuture( result.value )
                                                           : (CompletionStage) onErrorAction.apply( result.error ));
        } );
    }

    private static void safeRun( Runnable runnable )
    {
        try
        {
            runnable.run();
        }
        catch ( Throwable var2 )
        {
        }
    }

    private static void noOpInterruptHandler()
    {
    }

    private static class CompletionResult<T>
    {
        T value;
        Throwable error;

        CompletionResult( T value, Throwable error )
        {
            this.value = value;
            this.error = error;
        }
    }
}
