package com.neo4j.fabric.shaded.driver.internal.util;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Base64;

public class CertificateTool
{
    private static final String BEGIN_CERT = "-----BEGIN CERTIFICATE-----";
    private static final String END_CERT = "-----END CERTIFICATE-----";

    public static void saveX509Cert( String certStr, File certFile ) throws IOException
    {
        BufferedWriter writer = new BufferedWriter( new FileWriter( certFile ) );
        Throwable var3 = null;

        try
        {
            writer.write( "-----BEGIN CERTIFICATE-----" );
            writer.newLine();
            writer.write( certStr );
            writer.newLine();
            writer.write( "-----END CERTIFICATE-----" );
            writer.newLine();
        }
        catch ( Throwable var12 )
        {
            var3 = var12;
            throw var12;
        }
        finally
        {
            if ( writer != null )
            {
                if ( var3 != null )
                {
                    try
                    {
                        writer.close();
                    }
                    catch ( Throwable var11 )
                    {
                        var3.addSuppressed( var11 );
                    }
                }
                else
                {
                    writer.close();
                }
            }
        }
    }

    public static void saveX509Cert( Certificate cert, File certFile ) throws GeneralSecurityException, IOException
    {
        saveX509Cert( new Certificate[]{cert}, certFile );
    }

    public static void saveX509Cert( Certificate[] certs, File certFile ) throws GeneralSecurityException, IOException
    {
        BufferedWriter writer = new BufferedWriter( new FileWriter( certFile ) );
        Throwable var3 = null;

        try
        {
            Certificate[] var4 = certs;
            int var5 = certs.length;

            for ( int var6 = 0; var6 < var5; ++var6 )
            {
                Certificate cert = var4[var6];
                String certStr = Base64.getEncoder().encodeToString( cert.getEncoded() ).replaceAll( "(.{64})", "$1\n" );
                writer.write( "-----BEGIN CERTIFICATE-----" );
                writer.newLine();
                writer.write( certStr );
                writer.newLine();
                writer.write( "-----END CERTIFICATE-----" );
                writer.newLine();
            }
        }
        catch ( Throwable var16 )
        {
            var3 = var16;
            throw var16;
        }
        finally
        {
            if ( writer != null )
            {
                if ( var3 != null )
                {
                    try
                    {
                        writer.close();
                    }
                    catch ( Throwable var15 )
                    {
                        var3.addSuppressed( var15 );
                    }
                }
                else
                {
                    writer.close();
                }
            }
        }
    }

    public static void loadX509Cert( File certFile, KeyStore keyStore ) throws GeneralSecurityException, IOException
    {
        BufferedInputStream inputStream = new BufferedInputStream( new FileInputStream( certFile ) );
        Throwable var3 = null;

        try
        {
            CertificateFactory certFactory = CertificateFactory.getInstance( "X.509" );
            int certCount = 0;

            while ( inputStream.available() > 0 )
            {
                try
                {
                    Certificate cert = certFactory.generateCertificate( inputStream );
                    ++certCount;
                    loadX509Cert( cert, "neo4j.javadriver.trustedcert." + certCount, keyStore );
                }
                catch ( CertificateException var17 )
                {
                    if ( var17.getCause() != null && var17.getCause().getMessage().equals( "Empty input" ) )
                    {
                        return;
                    }

                    throw new IOException( "Failed to load certificate from `" + certFile.getAbsolutePath() + "`: " + certCount + " : " + var17.getMessage(),
                            var17 );
                }
            }
        }
        catch ( Throwable var18 )
        {
            var3 = var18;
            throw var18;
        }
        finally
        {
            if ( inputStream != null )
            {
                if ( var3 != null )
                {
                    try
                    {
                        inputStream.close();
                    }
                    catch ( Throwable var16 )
                    {
                        var3.addSuppressed( var16 );
                    }
                }
                else
                {
                    inputStream.close();
                }
            }
        }
    }

    public static void loadX509Cert( Certificate cert, String certAlias, KeyStore keyStore ) throws KeyStoreException
    {
        keyStore.setCertificateEntry( certAlias, cert );
    }

    public static String X509CertToString( String cert )
    {
        String cert64CharPerLine = cert.replaceAll( "(.{64})", "$1\n" );
        return "-----BEGIN CERTIFICATE-----\n" + cert64CharPerLine + "\n" + "-----END CERTIFICATE-----" + "\n";
    }
}
