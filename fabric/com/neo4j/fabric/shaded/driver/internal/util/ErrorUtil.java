package com.neo4j.fabric.shaded.driver.internal.util;

import com.neo4j.fabric.shaded.driver.exceptions.AuthenticationException;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.DatabaseException;
import com.neo4j.fabric.shaded.driver.exceptions.FatalDiscoveryException;
import com.neo4j.fabric.shaded.driver.exceptions.Neo4jException;
import com.neo4j.fabric.shaded.driver.exceptions.ResultConsumedException;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.exceptions.TransientException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

public final class ErrorUtil
{
    private ErrorUtil()
    {
    }

    public static ServiceUnavailableException newConnectionTerminatedError( String reason )
    {
        return reason == null ? newConnectionTerminatedError() : new ServiceUnavailableException( "Connection to the database terminated. " + reason );
    }

    public static ServiceUnavailableException newConnectionTerminatedError()
    {
        return new ServiceUnavailableException(
                "Connection to the database terminated. Please ensure that your database is listening on the correct host and port and that you have compatible encryption settings both on Neo4j server and driver. Note that the default encryption setting has changed in Neo4j 4.0." );
    }

    public static ResultConsumedException newResultConsumedError()
    {
        return new ResultConsumedException(
                "Cannot access records on this result any more as the result has already been consumed or the query runner where the result is created has already been closed." );
    }

    public static Neo4jException newNeo4jError( String code, String message )
    {
        String classification = extractClassification( code );
        byte var4 = -1;
        switch ( classification.hashCode() )
        {
        case 1165693374:
            if ( classification.equals( "TransientError" ) )
            {
                var4 = 1;
            }
            break;
        case 1470119133:
            if ( classification.equals( "ClientError" ) )
            {
                var4 = 0;
            }
        }

        switch ( var4 )
        {
        case 0:
            if ( code.equalsIgnoreCase( "Neo.ClientError.Security.Unauthorized" ) )
            {
                return new AuthenticationException( code, message );
            }
            else
            {
                if ( code.equalsIgnoreCase( "Neo.ClientError.Database.DatabaseNotFound" ) )
                {
                    return new FatalDiscoveryException( code, message );
                }

                return new ClientException( code, message );
            }
        case 1:
            return new TransientException( code, message );
        default:
            return new DatabaseException( code, message );
        }
    }

    public static boolean isFatal( Throwable error )
    {
        if ( error instanceof Neo4jException )
        {
            if ( isProtocolViolationError( (Neo4jException) error ) )
            {
                return true;
            }

            if ( isClientOrTransientError( (Neo4jException) error ) )
            {
                return false;
            }
        }

        return true;
    }

    public static void rethrowAsyncException( ExecutionException e )
    {
        Throwable error = e.getCause();
        ErrorUtil.InternalExceptionCause internalCause = new ErrorUtil.InternalExceptionCause( error.getStackTrace() );
        error.addSuppressed( internalCause );
        StackTraceElement[] currentStackTrace = (StackTraceElement[]) Stream.of( Thread.currentThread().getStackTrace() ).skip( 2L ).toArray( ( x$0 ) -> {
            return new StackTraceElement[x$0];
        } );
        error.setStackTrace( currentStackTrace );
        PlatformDependent.throwException( error );
    }

    private static boolean isProtocolViolationError( Neo4jException error )
    {
        String errorCode = error.code();
        return errorCode != null && errorCode.startsWith( "Neo.ClientError.Request" );
    }

    private static boolean isClientOrTransientError( Neo4jException error )
    {
        String errorCode = error.code();
        return errorCode != null && (errorCode.contains( "ClientError" ) || errorCode.contains( "TransientError" ));
    }

    private static String extractClassification( String code )
    {
        String[] parts = code.split( "\\." );
        return parts.length < 2 ? "" : parts[1];
    }

    public static void addSuppressed( Throwable mainError, Throwable error )
    {
        if ( mainError != error )
        {
            mainError.addSuppressed( error );
        }
    }

    private static class InternalExceptionCause extends RuntimeException
    {
        InternalExceptionCause( StackTraceElement[] stackTrace )
        {
            this.setStackTrace( stackTrace );
        }

        public synchronized Throwable fillInStackTrace()
        {
            return this;
        }
    }
}
