package com.neo4j.fabric.shaded.driver.internal.util;

public final class Preconditions
{
    private Preconditions()
    {
    }

    public static void checkArgument( boolean expression, String message )
    {
        if ( !expression )
        {
            throw new IllegalArgumentException( message );
        }
    }

    public static void checkArgument( Object argument, Class<?> expectedClass )
    {
        if ( !expectedClass.isInstance( argument ) )
        {
            throw new IllegalArgumentException( "Argument expected to be of type: " + expectedClass.getName() + " but was: " + argument );
        }
    }
}
