package com.neo4j.fabric.shaded.driver.internal.util;

public interface Clock
{
    Clock SYSTEM = new Clock()
    {
        public long millis()
        {
            return System.currentTimeMillis();
        }

        public void sleep( long millis ) throws InterruptedException
        {
            Thread.sleep( millis );
        }
    };

    long millis();

    void sleep( long var1 ) throws InterruptedException;
}
