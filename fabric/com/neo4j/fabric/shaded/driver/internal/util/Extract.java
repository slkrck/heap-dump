package com.neo4j.fabric.shaded.driver.internal.util;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.InternalPair;
import com.neo4j.fabric.shaded.driver.internal.value.NodeValue;
import com.neo4j.fabric.shaded.driver.internal.value.PathValue;
import com.neo4j.fabric.shaded.driver.internal.value.RelationshipValue;
import com.neo4j.fabric.shaded.driver.types.MapAccessor;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Relationship;
import com.neo4j.fabric.shaded.driver.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

public final class Extract
{
    private Extract()
    {
        throw new UnsupportedOperationException();
    }

    public static List<Value> list( Value[] values )
    {
        switch ( values.length )
        {
        case 0:
            return Collections.emptyList();
        case 1:
            return Collections.singletonList( values[0] );
        default:
            return Collections.unmodifiableList( Arrays.asList( values ) );
        }
    }

    public static <T> List<T> list( Value[] data, Function<Value,T> mapFunction )
    {
        int size = data.length;
        switch ( size )
        {
        case 0:
            return Collections.emptyList();
        case 1:
            return Collections.singletonList( mapFunction.apply( data[0] ) );
        default:
            List<T> result = new ArrayList( size );
            Value[] var4 = data;
            int var5 = data.length;

            for ( int var6 = 0; var6 < var5; ++var6 )
            {
                Value value = var4[var6];
                result.add( mapFunction.apply( value ) );
            }

            return Collections.unmodifiableList( result );
        }
    }

    public static <T> Map<String,T> map( Map<String,Value> data, Function<Value,T> mapFunction )
    {
        if ( data.isEmpty() )
        {
            return Collections.emptyMap();
        }
        else
        {
            int size = data.size();
            if ( size == 1 )
            {
                Entry<String,Value> head = (Entry) data.entrySet().iterator().next();
                return Collections.singletonMap( head.getKey(), mapFunction.apply( head.getValue() ) );
            }
            else
            {
                Map<String,T> map = Iterables.newLinkedHashMapWithSize( size );
                Iterator var4 = data.entrySet().iterator();

                while ( var4.hasNext() )
                {
                    Entry<String,Value> entry = (Entry) var4.next();
                    map.put( entry.getKey(), mapFunction.apply( entry.getValue() ) );
                }

                return Collections.unmodifiableMap( map );
            }
        }
    }

    public static <T> Map<String,T> map( Record record, Function<Value,T> mapFunction )
    {
        int size = record.size();
        switch ( size )
        {
        case 0:
            return Collections.emptyMap();
        case 1:
            return Collections.singletonMap( record.keys().get( 0 ), mapFunction.apply( record.get( 0 ) ) );
        default:
            Map<String,T> map = Iterables.newLinkedHashMapWithSize( size );
            List<String> keys = record.keys();

            for ( int i = 0; i < size; ++i )
            {
                map.put( keys.get( i ), mapFunction.apply( record.get( i ) ) );
            }

            return Collections.unmodifiableMap( map );
        }
    }

    public static <V> Iterable<Pair<String,V>> properties( MapAccessor map, Function<Value,V> mapFunction )
    {
        int size = map.size();
        switch ( size )
        {
        case 0:
            return Collections.emptyList();
        case 1:
            String key = (String) map.keys().iterator().next();
            Value value = map.get( key );
            return Collections.singletonList( InternalPair.of( key, mapFunction.apply( value ) ) );
        default:
            List<Pair<String,V>> list = new ArrayList( size );
            Iterator var8 = map.keys().iterator();

            while ( var8.hasNext() )
            {
                String key = (String) var8.next();
                Value value = map.get( key );
                list.add( InternalPair.of( key, mapFunction.apply( value ) ) );
            }

            return Collections.unmodifiableList( list );
        }
    }

    public static <V> List<Pair<String,V>> fields( Record map, Function<Value,V> mapFunction )
    {
        int size = map.keys().size();
        switch ( size )
        {
        case 0:
            return Collections.emptyList();
        case 1:
            String key = (String) map.keys().iterator().next();
            Value value = map.get( key );
            return Collections.singletonList( InternalPair.of( key, mapFunction.apply( value ) ) );
        default:
            List<Pair<String,V>> list = new ArrayList( size );
            List<String> keys = map.keys();

            for ( int i = 0; i < size; ++i )
            {
                String key = (String) keys.get( i );
                Value value = map.get( i );
                list.add( InternalPair.of( key, mapFunction.apply( value ) ) );
            }

            return Collections.unmodifiableList( list );
        }
    }

    public static Map<String,Value> mapOfValues( Map<String,Object> map )
    {
        if ( map != null && !map.isEmpty() )
        {
            Map<String,Value> result = Iterables.newHashMapWithSize( map.size() );
            Iterator var2 = map.entrySet().iterator();

            while ( var2.hasNext() )
            {
                Entry<String,Object> entry = (Entry) var2.next();
                Object value = entry.getValue();
                assertParameter( value );
                result.put( entry.getKey(), Values.value( value ) );
            }

            return result;
        }
        else
        {
            return Collections.emptyMap();
        }
    }

    public static void assertParameter( Object value )
    {
        if ( !(value instanceof Node) && !(value instanceof NodeValue) )
        {
            if ( !(value instanceof Relationship) && !(value instanceof RelationshipValue) )
            {
                if ( value instanceof Path || value instanceof PathValue )
                {
                    throw new ClientException( "Paths can't be used as parameters." );
                }
            }
            else
            {
                throw new ClientException( "Relationships can't be used as parameters." );
            }
        }
        else
        {
            throw new ClientException( "Nodes can't be used as parameters." );
        }
    }
}
