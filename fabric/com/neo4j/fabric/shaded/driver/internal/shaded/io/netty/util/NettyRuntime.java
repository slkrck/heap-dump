package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.SystemPropertyUtil;

import java.util.Locale;

public final class NettyRuntime
{
    private static final NettyRuntime.AvailableProcessorsHolder holder = new NettyRuntime.AvailableProcessorsHolder();

    private NettyRuntime()
    {
    }

    public static void setAvailableProcessors( int availableProcessors )
    {
        holder.setAvailableProcessors( availableProcessors );
    }

    public static int availableProcessors()
    {
        return holder.availableProcessors();
    }

    static class AvailableProcessorsHolder
    {
        private int availableProcessors;

        synchronized void setAvailableProcessors( int availableProcessors )
        {
            ObjectUtil.checkPositive( availableProcessors, "availableProcessors" );
            if ( this.availableProcessors != 0 )
            {
                String message = String.format( Locale.ROOT, "availableProcessors is already set to [%d], rejecting [%d]", this.availableProcessors,
                        availableProcessors );
                throw new IllegalStateException( message );
            }
            else
            {
                this.availableProcessors = availableProcessors;
            }
        }

        @SuppressForbidden( reason = "to obtain default number of available processors" )
        synchronized int availableProcessors()
        {
            if ( this.availableProcessors == 0 )
            {
                int availableProcessors = SystemPropertyUtil.getInt( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.availableProcessors",
                        Runtime.getRuntime().availableProcessors() );
                this.setAvailableProcessors( availableProcessors );
            }

            return this.availableProcessors;
        }
    }
}
