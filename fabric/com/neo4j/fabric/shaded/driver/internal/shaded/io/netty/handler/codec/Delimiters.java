package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.Unpooled;

public final class Delimiters
{
    private Delimiters()
    {
    }

    public static ByteBuf[] nulDelimiter()
    {
        return new ByteBuf[]{Unpooled.wrappedBuffer( new byte[]{0} )};
    }

    public static ByteBuf[] lineDelimiter()
    {
        return new ByteBuf[]{Unpooled.wrappedBuffer( new byte[]{13, 10} ), Unpooled.wrappedBuffer( new byte[]{10} )};
    }
}
