package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.atomic.AtomicLongFieldUpdater;

import org.reactivestreams.Subscriber;

final class FluxRange extends Flux<Integer> implements Fuseable, SourceProducer<Integer>
{
    final long start;
    final long end;

    FluxRange( int start, int count )
    {
        if ( count < 0 )
        {
            throw new IllegalArgumentException( "count >= required but it was " + count );
        }
        else
        {
            long e = (long) start + (long) count;
            if ( e - 1L > 2147483647L )
            {
                throw new IllegalArgumentException( "start + count must be less than Integer.MAX_VALUE + 1" );
            }
            else
            {
                this.start = (long) start;
                this.end = e;
            }
        }
    }

    public void subscribe( CoreSubscriber<? super Integer> actual )
    {
        long st = this.start;
        long en = this.end;
        if ( st == en )
        {
            Operators.complete( actual );
        }
        else if ( st + 1L == en )
        {
            actual.onSubscribe( Operators.scalarSubscription( actual, (int) st ) );
        }
        else if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            actual.onSubscribe( new FluxRange.RangeSubscriptionConditional( (Fuseable.ConditionalSubscriber) actual, st, en ) );
        }
        else
        {
            actual.onSubscribe( new FluxRange.RangeSubscription( actual, st, en ) );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class RangeSubscriptionConditional implements InnerProducer<Integer>, Fuseable.SynchronousSubscription<Integer>
    {
        static final AtomicLongFieldUpdater<FluxRange.RangeSubscriptionConditional> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxRange.RangeSubscriptionConditional.class, "requested" );
        final Fuseable.ConditionalSubscriber<? super Integer> actual;
        final long end;
        volatile boolean cancelled;
        long index;
        volatile long requested;

        RangeSubscriptionConditional( Fuseable.ConditionalSubscriber<? super Integer> actual, long start, long end )
        {
            this.actual = actual;
            this.index = start;
            this.end = end;
        }

        public CoreSubscriber<? super Integer> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && Operators.addCap( REQUESTED, this, n ) == 0L )
            {
                if ( n == Long.MAX_VALUE )
                {
                    this.fastPath();
                }
                else
                {
                    this.slowPath( n );
                }
            }
        }

        public void cancel()
        {
            this.cancelled = true;
        }

        void fastPath()
        {
            long e = this.end;
            Fuseable.ConditionalSubscriber<? super Integer> a = this.actual;

            for ( long i = this.index; i != e; ++i )
            {
                if ( this.cancelled )
                {
                    return;
                }

                a.tryOnNext( (int) i );
            }

            if ( !this.cancelled )
            {
                a.onComplete();
            }
        }

        void slowPath( long n )
        {
            Fuseable.ConditionalSubscriber<? super Integer> a = this.actual;
            long f = this.end;
            long e = 0L;
            long i = this.index;

            while ( !this.cancelled )
            {
                for ( ; e != n && i != f; ++i )
                {
                    boolean b = a.tryOnNext( (int) i );
                    if ( this.cancelled )
                    {
                        return;
                    }

                    if ( b )
                    {
                        ++e;
                    }
                }

                if ( this.cancelled )
                {
                    return;
                }

                if ( i == f )
                {
                    a.onComplete();
                    return;
                }

                n = this.requested;
                if ( n == e )
                {
                    this.index = i;
                    n = REQUESTED.addAndGet( this, -e );
                    if ( n == 0L )
                    {
                        return;
                    }

                    e = 0L;
                }
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.isEmpty() : InnerProducer.super.scanUnsafe( key );
            }
        }

        @Nullable
        public Integer poll()
        {
            long i = this.index;
            if ( i == this.end )
            {
                return null;
            }
            else
            {
                this.index = i + 1L;
                return (int) i;
            }
        }

        public boolean isEmpty()
        {
            return this.index == this.end;
        }

        public void clear()
        {
            this.index = this.end;
        }

        public int size()
        {
            return (int) (this.end - this.index);
        }
    }

    static final class RangeSubscription implements InnerProducer<Integer>, Fuseable.SynchronousSubscription<Integer>
    {
        static final AtomicLongFieldUpdater<FluxRange.RangeSubscription> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxRange.RangeSubscription.class, "requested" );
        final CoreSubscriber<? super Integer> actual;
        final long end;
        volatile boolean cancelled;
        long index;
        volatile long requested;

        RangeSubscription( CoreSubscriber<? super Integer> actual, long start, long end )
        {
            this.actual = actual;
            this.index = start;
            this.end = end;
        }

        public CoreSubscriber<? super Integer> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && Operators.addCap( REQUESTED, this, n ) == 0L )
            {
                if ( n == Long.MAX_VALUE )
                {
                    this.fastPath();
                }
                else
                {
                    this.slowPath( n );
                }
            }
        }

        public void cancel()
        {
            this.cancelled = true;
        }

        void fastPath()
        {
            long e = this.end;
            Subscriber<? super Integer> a = this.actual;

            for ( long i = this.index; i != e; ++i )
            {
                if ( this.cancelled )
                {
                    return;
                }

                a.onNext( (int) i );
            }

            if ( !this.cancelled )
            {
                a.onComplete();
            }
        }

        void slowPath( long n )
        {
            Subscriber<? super Integer> a = this.actual;
            long f = this.end;
            long e = 0L;
            long i = this.index;

            while ( !this.cancelled )
            {
                while ( e != n && i != f )
                {
                    a.onNext( (int) i );
                    if ( this.cancelled )
                    {
                        return;
                    }

                    ++e;
                    ++i;
                }

                if ( this.cancelled )
                {
                    return;
                }

                if ( i == f )
                {
                    a.onComplete();
                    return;
                }

                n = this.requested;
                if ( n == e )
                {
                    this.index = i;
                    n = REQUESTED.addAndGet( this, -e );
                    if ( n == 0L )
                    {
                        return;
                    }

                    e = 0L;
                }
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.isEmpty() : InnerProducer.super.scanUnsafe( key );
            }
        }

        @Nullable
        public Integer poll()
        {
            long i = this.index;
            if ( i == this.end )
            {
                return null;
            }
            else
            {
                this.index = i + 1L;
                return (int) i;
            }
        }

        public boolean isEmpty()
        {
            return this.index == this.end;
        }

        public void clear()
        {
            this.index = this.end;
        }

        public int size()
        {
            return (int) (this.end - this.index);
        }
    }
}
