package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

import org.reactivestreams.Subscription;

final class MonoToCompletableFuture<T> extends CompletableFuture<T> implements CoreSubscriber<T>
{
    final AtomicReference<Subscription> ref = new AtomicReference();

    public boolean cancel( boolean mayInterruptIfRunning )
    {
        boolean cancelled = super.cancel( mayInterruptIfRunning );
        if ( cancelled )
        {
            Subscription s = (Subscription) this.ref.getAndSet( (Object) null );
            if ( s != null )
            {
                s.cancel();
            }
        }

        return cancelled;
    }

    public void onSubscribe( Subscription s )
    {
        if ( Operators.validate( (Subscription) this.ref.getAndSet( s ), s ) )
        {
            s.request( Long.MAX_VALUE );
        }
        else
        {
            s.cancel();
        }
    }

    public void onNext( T t )
    {
        Subscription s = (Subscription) this.ref.getAndSet( (Object) null );
        if ( s != null )
        {
            this.complete( t );
            s.cancel();
        }
        else
        {
            Operators.onNextDropped( t, this.currentContext() );
        }
    }

    public void onError( Throwable t )
    {
        if ( this.ref.getAndSet( (Object) null ) != null )
        {
            this.completeExceptionally( t );
        }
    }

    public void onComplete()
    {
        if ( this.ref.getAndSet( (Object) null ) != null )
        {
            this.complete( (Object) null );
        }
    }
}
