package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Publisher;

final class FluxConcatArray<T> extends Flux<T> implements SourceProducer<T>
{
    final Publisher<? extends T>[] array;
    final boolean delayError;

    @SafeVarargs
    FluxConcatArray( boolean delayError, Publisher<? extends T>... array )
    {
        this.array = (Publisher[]) Objects.requireNonNull( array, "array" );
        this.delayError = delayError;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Publisher<? extends T>[] a = this.array;
        if ( a.length == 0 )
        {
            Operators.complete( actual );
        }
        else if ( a.length == 1 )
        {
            Publisher<? extends T> p = a[0];
            if ( p == null )
            {
                Operators.error( actual, new NullPointerException( "The single source Publisher is null" ) );
            }
            else
            {
                p.subscribe( actual );
            }
        }
        else if ( this.delayError )
        {
            FluxConcatArray.ConcatArrayDelayErrorSubscriber<T> parent = new FluxConcatArray.ConcatArrayDelayErrorSubscriber( actual, a );
            actual.onSubscribe( parent );
            if ( !parent.isCancelled() )
            {
                parent.onComplete();
            }
        }
        else
        {
            FluxConcatArray.ConcatArraySubscriber<T> parent = new FluxConcatArray.ConcatArraySubscriber( actual, a );
            actual.onSubscribe( parent );
            if ( !parent.isCancelled() )
            {
                parent.onComplete();
            }
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.DELAY_ERROR ? this.delayError : null;
    }

    FluxConcatArray<T> concatAdditionalSourceLast( Publisher<? extends T> source )
    {
        int n = this.array.length;
        Publisher<? extends T>[] newArray = new Publisher[n + 1];
        System.arraycopy( this.array, 0, newArray, 0, n );
        newArray[n] = source;
        return new FluxConcatArray( this.delayError, newArray );
    }

    <V> FluxConcatArray<V> concatAdditionalIgnoredLast( Publisher<? extends V> source )
    {
        int n = this.array.length;
        Publisher<? extends V>[] newArray = new Publisher[n + 1];
        System.arraycopy( this.array, 0, newArray, 0, n );
        newArray[n - 1] = Mono.ignoreElements( newArray[n - 1] );
        newArray[n] = source;
        return new FluxConcatArray( this.delayError, newArray );
    }

    FluxConcatArray<T> concatAdditionalSourceFirst( Publisher<? extends T> source )
    {
        int n = this.array.length;
        Publisher<? extends T>[] newArray = new Publisher[n + 1];
        System.arraycopy( this.array, 0, newArray, 1, n );
        newArray[0] = source;
        return new FluxConcatArray( this.delayError, newArray );
    }

    static final class ConcatArrayDelayErrorSubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxConcatArray.ConcatArrayDelayErrorSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxConcatArray.ConcatArrayDelayErrorSubscriber.class, "wip" );
        static final AtomicReferenceFieldUpdater<FluxConcatArray.ConcatArrayDelayErrorSubscriber,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( FluxConcatArray.ConcatArrayDelayErrorSubscriber.class, Throwable.class, "error" );
        final Publisher<? extends T>[] sources;
        int index;
        volatile int wip;
        volatile Throwable error;
        long produced;

        ConcatArrayDelayErrorSubscriber( CoreSubscriber<? super T> actual, Publisher<? extends T>[] sources )
        {
            super( actual );
            this.sources = sources;
        }

        public void onNext( T t )
        {
            ++this.produced;
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            if ( Exceptions.addThrowable( ERROR, this, t ) )
            {
                this.onComplete();
            }
            else
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.DELAY_ERROR )
            {
                return true;
            }
            else
            {
                return key == Scannable.Attr.ERROR ? this.error : super.scanUnsafe( key );
            }
        }

        public void onComplete()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                Publisher[] a = this.sources;

                do
                {
                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    int i = this.index;
                    if ( i == a.length )
                    {
                        Throwable e = Exceptions.terminate( ERROR, this );
                        if ( e != null )
                        {
                            this.actual.onError( e );
                        }
                        else
                        {
                            this.actual.onComplete();
                        }

                        return;
                    }

                    Publisher<? extends T> p = a[i];
                    if ( p == null )
                    {
                        this.actual.onError( new NullPointerException( "Source Publisher at index " + i + " is null" ) );
                        return;
                    }

                    long c = this.produced;
                    if ( c != 0L )
                    {
                        this.produced = 0L;
                        this.produced( c );
                    }

                    p.subscribe( this );
                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    ++i;
                    this.index = i;
                }
                while ( WIP.decrementAndGet( this ) != 0 );
            }
        }
    }

    static final class ConcatArraySubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxConcatArray.ConcatArraySubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxConcatArray.ConcatArraySubscriber.class, "wip" );
        final Publisher<? extends T>[] sources;
        int index;
        volatile int wip;
        long produced;

        ConcatArraySubscriber( CoreSubscriber<? super T> actual, Publisher<? extends T>[] sources )
        {
            super( actual );
            this.sources = sources;
        }

        public void onNext( T t )
        {
            ++this.produced;
            this.actual.onNext( t );
        }

        public void onComplete()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                Publisher[] a = this.sources;

                do
                {
                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    int i = this.index;
                    if ( i == a.length )
                    {
                        this.actual.onComplete();
                        return;
                    }

                    Publisher<? extends T> p = a[i];
                    if ( p == null )
                    {
                        this.actual.onError( new NullPointerException( "Source Publisher at index " + i + " is null" ) );
                        return;
                    }

                    long c = this.produced;
                    if ( c != 0L )
                    {
                        this.produced = 0L;
                        this.produced( c );
                    }

                    p.subscribe( this );
                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    ++i;
                    this.index = i;
                }
                while ( WIP.decrementAndGet( this ) != 0 );
            }
        }
    }
}
