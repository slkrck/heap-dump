package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;

final class MonoSubscribeOnValue<T> extends Mono<T> implements Scannable
{
    final T value;
    final Scheduler scheduler;

    MonoSubscribeOnValue( @Nullable T value, Scheduler scheduler )
    {
        this.value = value;
        this.scheduler = (Scheduler) Objects.requireNonNull( scheduler, "scheduler" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        T v = this.value;
        if ( v == null )
        {
            FluxSubscribeOnValue.ScheduledEmpty parent = new FluxSubscribeOnValue.ScheduledEmpty( actual );
            actual.onSubscribe( parent );

            try
            {
                parent.setFuture( this.scheduler.schedule( parent ) );
            }
            catch ( RejectedExecutionException var5 )
            {
                if ( parent.future != OperatorDisposables.DISPOSED )
                {
                    actual.onError( Operators.onRejectedExecution( var5, actual.currentContext() ) );
                }
            }
        }
        else
        {
            actual.onSubscribe( new FluxSubscribeOnValue.ScheduledScalar( actual, v, this.scheduler ) );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : null;
    }
}
