package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

final class ExecutorServiceWorker implements Scheduler.Worker, Disposable, Scannable
{
    final ScheduledExecutorService exec;
    final Disposable.Composite tasks;

    ExecutorServiceWorker( ScheduledExecutorService exec )
    {
        this.exec = exec;
        this.tasks = Disposables.composite();
    }

    public Disposable schedule( Runnable task )
    {
        return Schedulers.workerSchedule( this.exec, this.tasks, task, 0L, TimeUnit.MILLISECONDS );
    }

    public Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        return Schedulers.workerSchedule( this.exec, this.tasks, task, delay, unit );
    }

    public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        return Schedulers.workerSchedulePeriodically( this.exec, this.tasks, task, initialDelay, period, unit );
    }

    public void dispose()
    {
        this.tasks.dispose();
    }

    public boolean isDisposed()
    {
        return this.tasks.isDisposed();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.BUFFERED )
        {
            return this.tasks.size();
        }
        else if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            return key == Scannable.Attr.NAME ? "ExecutorServiceWorker" : Schedulers.scanExecutor( this.exec, key );
        }
        else
        {
            return this.isDisposed();
        }
    }
}
