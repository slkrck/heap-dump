package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutorGroup;

public interface EventLoopGroup extends EventExecutorGroup
{
    EventLoop next();

    ChannelFuture register( Channel var1 );

    ChannelFuture register( ChannelPromise var1 );

    /**
     * @deprecated
     */
    @Deprecated
    ChannelFuture register( Channel var1, ChannelPromise var2 );
}
