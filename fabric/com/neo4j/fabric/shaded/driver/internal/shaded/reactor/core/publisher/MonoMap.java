package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Objects;
import java.util.function.Function;

final class MonoMap<T, R> extends InternalMonoOperator<T,R>
{
    final Function<? super T,? extends R> mapper;

    MonoMap( Mono<? extends T> source, Function<? super T,? extends R> mapper )
    {
        super( source );
        this.mapper = (Function) Objects.requireNonNull( mapper, "mapper" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<? super R> cs = (Fuseable.ConditionalSubscriber) actual;
            return new FluxMap.MapConditionalSubscriber( cs, this.mapper );
        }
        else
        {
            return new FluxMap.MapSubscriber( actual, this.mapper );
        }
    }
}
