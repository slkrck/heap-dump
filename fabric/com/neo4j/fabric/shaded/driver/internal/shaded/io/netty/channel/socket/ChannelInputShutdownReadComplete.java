package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket;

public final class ChannelInputShutdownReadComplete
{
    public static final ChannelInputShutdownReadComplete INSTANCE = new ChannelInputShutdownReadComplete();

    private ChannelInputShutdownReadComplete()
    {
    }
}
