package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

final class FluxMapSignal<T, R> extends InternalFluxOperator<T,R>
{
    final Function<? super T,? extends R> mapperNext;
    final Function<? super Throwable,? extends R> mapperError;
    final Supplier<? extends R> mapperComplete;

    FluxMapSignal( Flux<? extends T> source, @Nullable Function<? super T,? extends R> mapperNext,
            @Nullable Function<? super Throwable,? extends R> mapperError, @Nullable Supplier<? extends R> mapperComplete )
    {
        super( source );
        if ( mapperNext == null && mapperError == null && mapperComplete == null )
        {
            throw new IllegalArgumentException( "Map Signal needs at least one valid mapper" );
        }
        else
        {
            this.mapperNext = mapperNext;
            this.mapperError = mapperError;
            this.mapperComplete = mapperComplete;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        return new FluxMapSignal.FluxMapSignalSubscriber( actual, this.mapperNext, this.mapperError, this.mapperComplete );
    }

    static final class FluxMapSignalSubscriber<T, R> extends AbstractQueue<R> implements InnerOperator<T,R>, BooleanSupplier
    {
        static final AtomicLongFieldUpdater<FluxMapSignal.FluxMapSignalSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxMapSignal.FluxMapSignalSubscriber.class, "requested" );
        final CoreSubscriber<? super R> actual;
        final Function<? super T,? extends R> mapperNext;
        final Function<? super Throwable,? extends R> mapperError;
        final Supplier<? extends R> mapperComplete;
        boolean done;
        Subscription s;
        R value;
        volatile long requested;
        volatile boolean cancelled;
        long produced;

        FluxMapSignalSubscriber( CoreSubscriber<? super R> actual, @Nullable Function<? super T,? extends R> mapperNext,
                @Nullable Function<? super Throwable,? extends R> mapperError, @Nullable Supplier<? extends R> mapperComplete )
        {
            this.actual = actual;
            this.mapperNext = mapperNext;
            this.mapperError = mapperError;
            this.mapperComplete = mapperComplete;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                if ( this.mapperNext == null )
                {
                    s.request( Long.MAX_VALUE );
                }
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else if ( this.mapperNext != null )
            {
                Object v;
                try
                {
                    v = Objects.requireNonNull( this.mapperNext.apply( t ), "The mapper returned a null value." );
                }
                catch ( Throwable var4 )
                {
                    this.done = true;
                    this.actual.onError( Operators.onOperatorError( this.s, var4, t, this.actual.currentContext() ) );
                    return;
                }

                ++this.produced;
                this.actual.onNext( v );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                if ( this.mapperError == null )
                {
                    this.actual.onError( t );
                }
                else
                {
                    Object v;
                    try
                    {
                        v = Objects.requireNonNull( this.mapperError.apply( t ), "The mapper returned a null value." );
                    }
                    catch ( Throwable var5 )
                    {
                        this.done = true;
                        this.actual.onError( Operators.onOperatorError( this.s, var5, t, this.actual.currentContext() ) );
                        return;
                    }

                    this.value = v;
                    long p = this.produced;
                    if ( p != 0L )
                    {
                        Operators.addCap( REQUESTED, this, -p );
                    }

                    DrainUtils.postComplete( this.actual, this, REQUESTED, this, this );
                }
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                if ( this.mapperComplete == null )
                {
                    this.actual.onComplete();
                }
                else
                {
                    Object v;
                    try
                    {
                        v = Objects.requireNonNull( this.mapperComplete.get(), "The mapper returned a null value." );
                    }
                    catch ( Throwable var4 )
                    {
                        this.done = true;
                        this.actual.onError( Operators.onOperatorError( this.s, var4, this.actual.currentContext() ) );
                        return;
                    }

                    this.value = v;
                    long p = this.produced;
                    if ( p != 0L )
                    {
                        Operators.addCap( REQUESTED, this, -p );
                    }

                    DrainUtils.postComplete( this.actual, this, REQUESTED, this, this );
                }
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && !DrainUtils.postCompleteRequest( n, this.actual, this, REQUESTED, this, this ) )
            {
                this.s.request( n );
            }
        }

        public boolean offer( R e )
        {
            throw new UnsupportedOperationException();
        }

        @Nullable
        public R poll()
        {
            R v = this.value;
            if ( v != null )
            {
                this.value = null;
                return v;
            }
            else
            {
                return null;
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.getAsBoolean();
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.size() : InnerOperator.super.scanUnsafe( key );
            }
        }

        @Nullable
        public R peek()
        {
            return this.value;
        }

        public boolean getAsBoolean()
        {
            return this.cancelled;
        }

        public void cancel()
        {
            this.cancelled = true;
            this.s.cancel();
        }

        public Iterator<R> iterator()
        {
            throw new UnsupportedOperationException();
        }

        public int size()
        {
            return this.value == null ? 0 : 1;
        }
    }
}
