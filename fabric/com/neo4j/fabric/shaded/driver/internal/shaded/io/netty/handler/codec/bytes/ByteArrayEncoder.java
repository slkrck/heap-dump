package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.bytes;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.Unpooled;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

@ChannelHandler.Sharable
public class ByteArrayEncoder extends MessageToMessageEncoder<byte[]>
{
    protected void encode( ChannelHandlerContext ctx, byte[] msg, List<Object> out ) throws Exception
    {
        out.add( Unpooled.wrappedBuffer( msg ) );
    }
}
