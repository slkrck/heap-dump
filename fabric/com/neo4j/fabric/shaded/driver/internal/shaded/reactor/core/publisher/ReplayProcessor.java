package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Schedulers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Subscription;

public final class ReplayProcessor<T> extends FluxProcessor<T,T> implements Fuseable
{
    static final AtomicReferenceFieldUpdater<ReplayProcessor,FluxReplay.ReplaySubscription[]> SUBSCRIBERS =
            AtomicReferenceFieldUpdater.newUpdater( ReplayProcessor.class, FluxReplay.ReplaySubscription[].class, "subscribers" );
    final FluxReplay.ReplayBuffer<T> buffer;
    Subscription subscription;
    volatile FluxReplay.ReplaySubscription<T>[] subscribers;

    ReplayProcessor( FluxReplay.ReplayBuffer<T> buffer )
    {
        this.buffer = buffer;
        SUBSCRIBERS.lazySet( this, FluxReplay.ReplaySubscriber.EMPTY );
    }

    public static <T> ReplayProcessor<T> cacheLast()
    {
        return cacheLastOrDefault( (Object) null );
    }

    public static <T> ReplayProcessor<T> cacheLastOrDefault( @Nullable T value )
    {
        ReplayProcessor<T> b = create( 1 );
        if ( value != null )
        {
            b.onNext( value );
        }

        return b;
    }

    public static <E> ReplayProcessor<E> create()
    {
        return create( Queues.SMALL_BUFFER_SIZE, true );
    }

    public static <E> ReplayProcessor<E> create( int historySize )
    {
        return create( historySize, false );
    }

    public static <E> ReplayProcessor<E> create( int historySize, boolean unbounded )
    {
        Object buffer;
        if ( unbounded )
        {
            buffer = new FluxReplay.UnboundedReplayBuffer( historySize );
        }
        else
        {
            buffer = new FluxReplay.SizeBoundReplayBuffer( historySize );
        }

        return new ReplayProcessor( (FluxReplay.ReplayBuffer) buffer );
    }

    public static <T> ReplayProcessor<T> createTimeout( Duration maxAge )
    {
        return createTimeout( maxAge, Schedulers.parallel() );
    }

    public static <T> ReplayProcessor<T> createTimeout( Duration maxAge, Scheduler scheduler )
    {
        return createSizeAndTimeout( Integer.MAX_VALUE, maxAge, scheduler );
    }

    public static <T> ReplayProcessor<T> createSizeAndTimeout( int size, Duration maxAge )
    {
        return createSizeAndTimeout( size, maxAge, Schedulers.parallel() );
    }

    public static <T> ReplayProcessor<T> createSizeAndTimeout( int size, Duration maxAge, Scheduler scheduler )
    {
        Objects.requireNonNull( scheduler, "scheduler is null" );
        if ( size <= 0 )
        {
            throw new IllegalArgumentException( "size > 0 required but it was " + size );
        }
        else
        {
            return new ReplayProcessor( new FluxReplay.SizeAndTimeBoundReplayBuffer( size, maxAge.toMillis(), scheduler ) );
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Objects.requireNonNull( actual, "subscribe" );
        FluxReplay.ReplaySubscription<T> rs = new ReplayProcessor.ReplayInner( actual, this );
        actual.onSubscribe( rs );
        if ( this.add( rs ) && rs.isCancelled() )
        {
            this.remove( rs );
        }
        else
        {
            this.buffer.replay( rs );
        }
    }

    @Nullable
    public Throwable getError()
    {
        return this.buffer.getError();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.subscription;
        }
        else
        {
            return key == Scannable.Attr.CAPACITY ? this.buffer.capacity() : super.scanUnsafe( key );
        }
    }

    public Stream<? extends Scannable> inners()
    {
        return Stream.of( this.subscribers );
    }

    public long downstreamCount()
    {
        return (long) this.subscribers.length;
    }

    public boolean isTerminated()
    {
        return this.buffer.isDone();
    }

    boolean add( FluxReplay.ReplaySubscription<T> rs )
    {
        FluxReplay.ReplaySubscription[] a;
        ReplayProcessor.ReplayInner[] b;
        do
        {
            a = this.subscribers;
            if ( a == FluxReplay.ReplaySubscriber.TERMINATED )
            {
                return false;
            }

            int n = a.length;
            b = new ReplayProcessor.ReplayInner[n + 1];
            System.arraycopy( a, 0, b, 0, n );
            b[n] = rs;
        }
        while ( !SUBSCRIBERS.compareAndSet( this, a, b ) );

        return true;
    }

    void remove( FluxReplay.ReplaySubscription<T> rs )
    {
        label31:
        while ( true )
        {
            FluxReplay.ReplaySubscription<T>[] a = this.subscribers;
            if ( a != FluxReplay.ReplaySubscriber.TERMINATED && a != FluxReplay.ReplaySubscriber.EMPTY )
            {
                int n = a.length;

                for ( int i = 0; i < n; ++i )
                {
                    if ( a[i] == rs )
                    {
                        Object b;
                        if ( n == 1 )
                        {
                            b = FluxReplay.ReplaySubscriber.EMPTY;
                        }
                        else
                        {
                            b = new ReplayProcessor.ReplayInner[n - 1];
                            System.arraycopy( a, 0, b, 0, i );
                            System.arraycopy( a, i + 1, b, i, n - i - 1 );
                        }

                        if ( !SUBSCRIBERS.compareAndSet( this, a, b ) )
                        {
                            continue label31;
                        }

                        return;
                    }
                }

                return;
            }

            return;
        }
    }

    public void onSubscribe( Subscription s )
    {
        if ( this.buffer.isDone() )
        {
            s.cancel();
        }
        else if ( Operators.validate( this.subscription, s ) )
        {
            this.subscription = s;
            s.request( Long.MAX_VALUE );
        }
    }

    public Context currentContext()
    {
        return Operators.multiSubscribersContext( this.subscribers );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public void onNext( T t )
    {
        FluxReplay.ReplayBuffer<T> b = this.buffer;
        if ( b.isDone() )
        {
            Operators.onNextDropped( t, this.currentContext() );
        }
        else
        {
            b.add( t );
            FluxReplay.ReplaySubscription[] var3 = this.subscribers;
            int var4 = var3.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                FluxReplay.ReplaySubscription<T> rs = var3[var5];
                b.replay( rs );
            }
        }
    }

    public void onError( Throwable t )
    {
        FluxReplay.ReplayBuffer<T> b = this.buffer;
        if ( b.isDone() )
        {
            Operators.onErrorDroppedMulticast( t );
        }
        else
        {
            b.onError( t );
            FluxReplay.ReplaySubscription<T>[] a = (FluxReplay.ReplaySubscription[]) SUBSCRIBERS.getAndSet( this, FluxReplay.ReplaySubscriber.TERMINATED );
            FluxReplay.ReplaySubscription[] var4 = a;
            int var5 = a.length;

            for ( int var6 = 0; var6 < var5; ++var6 )
            {
                FluxReplay.ReplaySubscription<T> rs = var4[var6];
                b.replay( rs );
            }
        }
    }

    public void onComplete()
    {
        FluxReplay.ReplayBuffer<T> b = this.buffer;
        if ( !b.isDone() )
        {
            b.onComplete();
            FluxReplay.ReplaySubscription<T>[] a = (FluxReplay.ReplaySubscription[]) SUBSCRIBERS.getAndSet( this, FluxReplay.ReplaySubscriber.TERMINATED );
            FluxReplay.ReplaySubscription[] var3 = a;
            int var4 = a.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                FluxReplay.ReplaySubscription<T> rs = var3[var5];
                b.replay( rs );
            }
        }
    }

    static final class ReplayInner<T> implements FluxReplay.ReplaySubscription<T>
    {
        static final AtomicIntegerFieldUpdater<ReplayProcessor.ReplayInner> WIP =
                AtomicIntegerFieldUpdater.newUpdater( ReplayProcessor.ReplayInner.class, "wip" );
        static final AtomicLongFieldUpdater<ReplayProcessor.ReplayInner> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( ReplayProcessor.ReplayInner.class, "requested" );
        final CoreSubscriber<? super T> actual;
        final ReplayProcessor<T> parent;
        final FluxReplay.ReplayBuffer<T> buffer;
        int index;
        int tailIndex;
        Object node;
        volatile int wip;
        volatile long requested;
        int fusionMode;

        ReplayInner( CoreSubscriber<? super T> actual, ReplayProcessor<T> parent )
        {
            this.actual = actual;
            this.parent = parent;
            this.buffer = parent.buffer;
        }

        public long requested()
        {
            return this.requested;
        }

        public boolean isCancelled()
        {
            return this.requested == Long.MIN_VALUE;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 2) != 0 )
            {
                this.fusionMode = 2;
                return 2;
            }
            else
            {
                return 0;
            }
        }

        @Nullable
        public T poll()
        {
            return this.buffer.poll( this );
        }

        public void clear()
        {
            this.buffer.clear( this );
        }

        public boolean isEmpty()
        {
            return this.buffer.isEmpty( this );
        }

        public int size()
        {
            return this.buffer.size( this );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( this.fusionMode() == 0 )
                {
                    Operators.addCapCancellable( REQUESTED, this, n );
                }

                this.buffer.replay( this );
            }
        }

        public void cancel()
        {
            if ( REQUESTED.getAndSet( this, Long.MIN_VALUE ) != Long.MIN_VALUE )
            {
                this.parent.remove( this );
                if ( this.enter() )
                {
                    this.node = null;
                }
            }
        }

        public void node( @Nullable Object node )
        {
            this.node = node;
        }

        public int fusionMode()
        {
            return this.fusionMode;
        }

        @Nullable
        public Object node()
        {
            return this.node;
        }

        public int index()
        {
            return this.index;
        }

        public void index( int index )
        {
            this.index = index;
        }

        public int tailIndex()
        {
            return this.tailIndex;
        }

        public void tailIndex( int tailIndex )
        {
            this.tailIndex = tailIndex;
        }

        public boolean enter()
        {
            return WIP.getAndIncrement( this ) == 0;
        }

        public int leave( int missed )
        {
            return WIP.addAndGet( this, -missed );
        }

        public void produced( long n )
        {
            REQUESTED.addAndGet( this, -n );
        }
    }
}
