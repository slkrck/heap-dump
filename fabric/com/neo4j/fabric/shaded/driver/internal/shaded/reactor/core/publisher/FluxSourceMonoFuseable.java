package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

final class FluxSourceMonoFuseable<I> extends FluxFromMonoOperator<I,I> implements Fuseable
{
    FluxSourceMonoFuseable( Mono<? extends I> source )
    {
        super( source );
    }

    public CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super I> actual )
    {
        return actual;
    }
}
