package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.stream;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;

public interface ChunkedInput<B>
{
    boolean isEndOfInput() throws Exception;

    void close() throws Exception;

    /**
     * @deprecated
     */
    @Deprecated
    B readChunk( ChannelHandlerContext var1 ) throws Exception;

    B readChunk( ByteBufAllocator var1 ) throws Exception;

    long length();

    long progress();
}
