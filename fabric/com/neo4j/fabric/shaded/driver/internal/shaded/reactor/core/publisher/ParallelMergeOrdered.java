package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Comparator;
import java.util.Queue;
import java.util.function.Supplier;

final class ParallelMergeOrdered<T> extends Flux<T> implements Scannable
{
    final ParallelFlux<? extends T> source;
    final int prefetch;
    final Supplier<Queue<T>> queueSupplier;
    final Comparator<? super T> valueComparator;

    ParallelMergeOrdered( ParallelFlux<? extends T> source, int prefetch, Supplier<Queue<T>> queueSupplier, Comparator<? super T> valueComparator )
    {
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.source = source;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
            this.valueComparator = valueComparator;
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.prefetch : null;
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        FluxMergeOrdered.MergeOrderedMainProducer<T> main =
                new FluxMergeOrdered.MergeOrderedMainProducer( actual, this.valueComparator, this.prefetch, this.source.parallelism() );
        actual.onSubscribe( main );
        this.source.subscribe( (CoreSubscriber[]) main.subscribers );
    }
}
