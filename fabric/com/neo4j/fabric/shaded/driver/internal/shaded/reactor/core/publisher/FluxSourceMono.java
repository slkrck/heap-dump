package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class FluxSourceMono<I> extends FluxFromMonoOperator<I,I>
{
    FluxSourceMono( Mono<? extends I> source )
    {
        super( source );
    }

    public CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super I> actual )
    {
        return actual;
    }
}
