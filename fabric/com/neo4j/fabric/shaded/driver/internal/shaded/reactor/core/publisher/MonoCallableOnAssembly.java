package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.time.Duration;
import java.util.concurrent.Callable;

final class MonoCallableOnAssembly<T> extends InternalMonoOperator<T,T> implements Callable<T>, AssemblyOp
{
    final FluxOnAssembly.AssemblySnapshot stacktrace;

    MonoCallableOnAssembly( Mono<? extends T> source, FluxOnAssembly.AssemblySnapshot stacktrace )
    {
        super( source );
        this.stacktrace = stacktrace;
    }

    @Nullable
    public T block()
    {
        return this.block( Duration.ZERO );
    }

    @Nullable
    public T block( Duration timeout )
    {
        try
        {
            return ((Callable) this.source).call();
        }
        catch ( Throwable var3 )
        {
            throw Exceptions.propagate( var3 );
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<? super T> cs = (Fuseable.ConditionalSubscriber) actual;
            return new FluxOnAssembly.OnAssemblyConditionalSubscriber( cs, this.stacktrace, this.source );
        }
        else
        {
            return new FluxOnAssembly.OnAssemblySubscriber( actual, this.stacktrace, this.source );
        }
    }

    @Nullable
    public T call() throws Exception
    {
        return ((Callable) this.source).call();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.ACTUAL_METADATA ? !this.stacktrace.checkpointed : super.scanUnsafe( key );
    }

    public String stepName()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }

    public String toString()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }
}
