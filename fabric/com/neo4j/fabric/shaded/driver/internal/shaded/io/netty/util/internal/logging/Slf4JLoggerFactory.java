package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.NOPLoggerFactory;
import org.slf4j.spi.LocationAwareLogger;

public class Slf4JLoggerFactory extends InternalLoggerFactory
{
    public static final InternalLoggerFactory INSTANCE = new Slf4JLoggerFactory();

    /**
     * @deprecated
     */
    @Deprecated
    public Slf4JLoggerFactory()
    {
    }

    Slf4JLoggerFactory( boolean failIfNOP )
    {
        assert failIfNOP;

        if ( LoggerFactory.getILoggerFactory() instanceof NOPLoggerFactory )
        {
            throw new NoClassDefFoundError( "NOPLoggerFactory not supported" );
        }
    }

    static InternalLogger wrapLogger( Logger logger )
    {
        return (InternalLogger) (logger instanceof LocationAwareLogger ? new LocationAwareSlf4JLogger( (LocationAwareLogger) logger )
                                                                       : new Slf4JLogger( logger ));
    }

    public InternalLogger newInstance( String name )
    {
        return wrapLogger( LoggerFactory.getLogger( name ) );
    }
}
