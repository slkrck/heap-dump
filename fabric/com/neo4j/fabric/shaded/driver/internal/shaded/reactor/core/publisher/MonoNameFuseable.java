package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;

import java.util.Set;

final class MonoNameFuseable<T> extends InternalMonoOperator<T,T> implements Fuseable
{
    final String name;
    final Set<Tuple2<String,String>> tags;

    MonoNameFuseable( Mono<? extends T> source, @Nullable String name, @Nullable Set<Tuple2<String,String>> tags )
    {
        super( source );
        this.name = name;
        this.tags = tags;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return actual;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.NAME )
        {
            return this.name;
        }
        else
        {
            return key == Scannable.Attr.TAGS && this.tags != null ? this.tags.stream() : super.scanUnsafe( key );
        }
    }
}
