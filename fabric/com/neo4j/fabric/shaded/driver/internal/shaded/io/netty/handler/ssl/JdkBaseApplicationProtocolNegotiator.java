package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLHandshakeException;

class JdkBaseApplicationProtocolNegotiator implements JdkApplicationProtocolNegotiator
{
    static final JdkApplicationProtocolNegotiator.ProtocolSelectorFactory FAIL_SELECTOR_FACTORY = new JdkApplicationProtocolNegotiator.ProtocolSelectorFactory()
    {
        public JdkApplicationProtocolNegotiator.ProtocolSelector newSelector( SSLEngine engine, Set<String> supportedProtocols )
        {
            return new JdkBaseApplicationProtocolNegotiator.FailProtocolSelector( (JdkSslEngine) engine, supportedProtocols );
        }
    };
    static final JdkApplicationProtocolNegotiator.ProtocolSelectorFactory NO_FAIL_SELECTOR_FACTORY =
            new JdkApplicationProtocolNegotiator.ProtocolSelectorFactory()
            {
                public JdkApplicationProtocolNegotiator.ProtocolSelector newSelector( SSLEngine engine, Set<String> supportedProtocols )
                {
                    return new JdkBaseApplicationProtocolNegotiator.NoFailProtocolSelector( (JdkSslEngine) engine, supportedProtocols );
                }
            };
    static final JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory FAIL_SELECTION_LISTENER_FACTORY =
            new JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory()
            {
                public JdkApplicationProtocolNegotiator.ProtocolSelectionListener newListener( SSLEngine engine, List<String> supportedProtocols )
                {
                    return new JdkBaseApplicationProtocolNegotiator.FailProtocolSelectionListener( (JdkSslEngine) engine, supportedProtocols );
                }
            };
    static final JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory NO_FAIL_SELECTION_LISTENER_FACTORY =
            new JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory()
            {
                public JdkApplicationProtocolNegotiator.ProtocolSelectionListener newListener( SSLEngine engine, List<String> supportedProtocols )
                {
                    return new JdkBaseApplicationProtocolNegotiator.NoFailProtocolSelectionListener( (JdkSslEngine) engine, supportedProtocols );
                }
            };
    private final List<String> protocols;
    private final JdkApplicationProtocolNegotiator.ProtocolSelectorFactory selectorFactory;
    private final JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory listenerFactory;
    private final JdkApplicationProtocolNegotiator.SslEngineWrapperFactory wrapperFactory;

    JdkBaseApplicationProtocolNegotiator( JdkApplicationProtocolNegotiator.SslEngineWrapperFactory wrapperFactory,
            JdkApplicationProtocolNegotiator.ProtocolSelectorFactory selectorFactory,
            JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory listenerFactory, Iterable<String> protocols )
    {
        this( wrapperFactory, selectorFactory, listenerFactory, ApplicationProtocolUtil.toList( protocols ) );
    }

    JdkBaseApplicationProtocolNegotiator( JdkApplicationProtocolNegotiator.SslEngineWrapperFactory wrapperFactory,
            JdkApplicationProtocolNegotiator.ProtocolSelectorFactory selectorFactory,
            JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory listenerFactory, String... protocols )
    {
        this( wrapperFactory, selectorFactory, listenerFactory, ApplicationProtocolUtil.toList( protocols ) );
    }

    private JdkBaseApplicationProtocolNegotiator( JdkApplicationProtocolNegotiator.SslEngineWrapperFactory wrapperFactory,
            JdkApplicationProtocolNegotiator.ProtocolSelectorFactory selectorFactory,
            JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory listenerFactory, List<String> protocols )
    {
        this.wrapperFactory = (JdkApplicationProtocolNegotiator.SslEngineWrapperFactory) ObjectUtil.checkNotNull( wrapperFactory, "wrapperFactory" );
        this.selectorFactory = (JdkApplicationProtocolNegotiator.ProtocolSelectorFactory) ObjectUtil.checkNotNull( selectorFactory, "selectorFactory" );
        this.listenerFactory =
                (JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory) ObjectUtil.checkNotNull( listenerFactory, "listenerFactory" );
        this.protocols = Collections.unmodifiableList( (List) ObjectUtil.checkNotNull( protocols, "protocols" ) );
    }

    public List<String> protocols()
    {
        return this.protocols;
    }

    public JdkApplicationProtocolNegotiator.ProtocolSelectorFactory protocolSelectorFactory()
    {
        return this.selectorFactory;
    }

    public JdkApplicationProtocolNegotiator.ProtocolSelectionListenerFactory protocolListenerFactory()
    {
        return this.listenerFactory;
    }

    public JdkApplicationProtocolNegotiator.SslEngineWrapperFactory wrapperFactory()
    {
        return this.wrapperFactory;
    }

    private static final class FailProtocolSelectionListener extends JdkBaseApplicationProtocolNegotiator.NoFailProtocolSelectionListener
    {
        FailProtocolSelectionListener( JdkSslEngine engineWrapper, List<String> supportedProtocols )
        {
            super( engineWrapper, supportedProtocols );
        }

        protected void noSelectedMatchFound( String protocol ) throws Exception
        {
            throw new SSLHandshakeException( "No compatible protocols found" );
        }
    }

    private static class NoFailProtocolSelectionListener implements JdkApplicationProtocolNegotiator.ProtocolSelectionListener
    {
        private final JdkSslEngine engineWrapper;
        private final List<String> supportedProtocols;

        NoFailProtocolSelectionListener( JdkSslEngine engineWrapper, List<String> supportedProtocols )
        {
            this.engineWrapper = engineWrapper;
            this.supportedProtocols = supportedProtocols;
        }

        public void unsupported()
        {
            this.engineWrapper.setNegotiatedApplicationProtocol( (String) null );
        }

        public void selected( String protocol ) throws Exception
        {
            if ( this.supportedProtocols.contains( protocol ) )
            {
                this.engineWrapper.setNegotiatedApplicationProtocol( protocol );
            }
            else
            {
                this.noSelectedMatchFound( protocol );
            }
        }

        protected void noSelectedMatchFound( String protocol ) throws Exception
        {
        }
    }

    private static final class FailProtocolSelector extends JdkBaseApplicationProtocolNegotiator.NoFailProtocolSelector
    {
        FailProtocolSelector( JdkSslEngine engineWrapper, Set<String> supportedProtocols )
        {
            super( engineWrapper, supportedProtocols );
        }

        public String noSelectMatchFound() throws Exception
        {
            throw new SSLHandshakeException( "Selected protocol is not supported" );
        }
    }

    static class NoFailProtocolSelector implements JdkApplicationProtocolNegotiator.ProtocolSelector
    {
        private final JdkSslEngine engineWrapper;
        private final Set<String> supportedProtocols;

        NoFailProtocolSelector( JdkSslEngine engineWrapper, Set<String> supportedProtocols )
        {
            this.engineWrapper = engineWrapper;
            this.supportedProtocols = supportedProtocols;
        }

        public void unsupported()
        {
            this.engineWrapper.setNegotiatedApplicationProtocol( (String) null );
        }

        public String select( List<String> protocols ) throws Exception
        {
            Iterator var2 = this.supportedProtocols.iterator();

            String p;
            do
            {
                if ( !var2.hasNext() )
                {
                    return this.noSelectMatchFound();
                }

                p = (String) var2.next();
            }
            while ( !protocols.contains( p ) );

            this.engineWrapper.setNegotiatedApplicationProtocol( p );
            return p;
        }

        public String noSelectMatchFound() throws Exception
        {
            this.engineWrapper.setNegotiatedApplicationProtocol( (String) null );
            return null;
        }
    }
}
