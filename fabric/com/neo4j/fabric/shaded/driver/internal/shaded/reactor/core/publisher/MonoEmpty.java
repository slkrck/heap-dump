package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.time.Duration;

import org.reactivestreams.Publisher;

final class MonoEmpty extends Mono<Object> implements Fuseable.ScalarCallable<Object>, SourceProducer<Object>
{
    static final Publisher<Object> INSTANCE = new MonoEmpty();

    static <T> Mono<T> instance()
    {
        return (Mono) INSTANCE;
    }

    public void subscribe( CoreSubscriber<? super Object> actual )
    {
        Operators.complete( actual );
    }

    @Nullable
    public Object call() throws Exception
    {
        return null;
    }

    @Nullable
    public Object block( Duration m )
    {
        return null;
    }

    @Nullable
    public Object block()
    {
        return null;
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
