package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuples;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

final class ParallelFluxName<T> extends ParallelFlux<T> implements Scannable
{
    final ParallelFlux<T> source;
    final String name;
    final Set<Tuple2<String,String>> tags;

    ParallelFluxName( ParallelFlux<T> source, @Nullable String name, @Nullable Set<Tuple2<String,String>> tags )
    {
        this.source = source;
        this.name = name;
        this.tags = tags;
    }

    static <T> ParallelFlux<T> createOrAppend( ParallelFlux<T> source, String name )
    {
        Objects.requireNonNull( name, "name" );
        if ( source instanceof ParallelFluxName )
        {
            ParallelFluxName<T> s = (ParallelFluxName) source;
            return new ParallelFluxName( s.source, name, s.tags );
        }
        else
        {
            return new ParallelFluxName( source, name, (Set) null );
        }
    }

    static <T> ParallelFlux<T> createOrAppend( ParallelFlux<T> source, String tagName, String tagValue )
    {
        Objects.requireNonNull( tagName, "tagName" );
        Objects.requireNonNull( tagValue, "tagValue" );
        Set<Tuple2<String,String>> tags = Collections.singleton( Tuples.of( tagName, tagValue ) );
        if ( source instanceof ParallelFluxName )
        {
            ParallelFluxName<T> s = (ParallelFluxName) source;
            if ( s.tags != null )
            {
                tags = new HashSet( (Collection) tags );
                ((Set) tags).addAll( s.tags );
            }

            return new ParallelFluxName( s.source, s.name, (Set) tags );
        }
        else
        {
            return new ParallelFluxName( source, (String) null, (Set) tags );
        }
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.NAME )
        {
            return this.name;
        }
        else if ( key == Scannable.Attr.TAGS && this.tags != null )
        {
            return this.tags.stream();
        }
        else if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        this.source.subscribe( subscribers );
    }
}
