package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.ByteToMessageDecoder;

public abstract class ZlibDecoder extends ByteToMessageDecoder
{
    public abstract boolean isClosed();
}
