package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.Predicate;

import org.reactivestreams.Subscription;

final class FluxFilterFuseable<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    final Predicate<? super T> predicate;

    FluxFilterFuseable( Flux<? extends T> source, Predicate<? super T> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return (CoreSubscriber) (actual instanceof Fuseable.ConditionalSubscriber ? new FluxFilterFuseable.FilterFuseableConditionalSubscriber(
                (Fuseable.ConditionalSubscriber) actual, this.predicate ) : new FluxFilterFuseable.FilterFuseableSubscriber( actual, this.predicate ));
    }

    static final class FilterFuseableConditionalSubscriber<T> implements InnerOperator<T,T>, Fuseable.ConditionalSubscriber<T>, Fuseable.QueueSubscription<T>
    {
        final Fuseable.ConditionalSubscriber<? super T> actual;
        final Context ctx;
        final Predicate<? super T> predicate;
        Fuseable.QueueSubscription<T> s;
        boolean done;
        int sourceMode;

        FilterFuseableConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, Predicate<? super T> predicate )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.predicate = predicate;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = (Fuseable.QueueSubscription) s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.sourceMode == 2 )
            {
                this.actual.onNext( (Object) null );
            }
            else
            {
                if ( this.done )
                {
                    Operators.onNextDropped( t, this.ctx );
                    return;
                }

                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.ctx, this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }
                    else
                    {
                        this.s.request( 1L );
                    }

                    Operators.onDiscard( t, this.ctx );
                    return;
                }

                if ( b )
                {
                    this.actual.onNext( t );
                }
                else
                {
                    this.s.request( 1L );
                    Operators.onDiscard( t, this.ctx );
                }
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
                return false;
            }
            else
            {
                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.ctx, this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }

                    Operators.onDiscard( t, this.ctx );
                    return false;
                }

                if ( b )
                {
                    return this.actual.tryOnNext( t );
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    return false;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }

        @Nullable
        public T poll()
        {
            if ( this.sourceMode == 2 )
            {
                long dropped = 0L;

                while ( true )
                {
                    Object v = this.s.poll();

                    try
                    {
                        if ( v == null || this.predicate.test( v ) )
                        {
                            if ( dropped != 0L )
                            {
                                this.request( dropped );
                            }

                            return v;
                        }

                        Operators.onDiscard( v, this.ctx );
                        ++dropped;
                    }
                    catch ( Throwable var6 )
                    {
                        RuntimeException e_ = Operators.onNextPollError( v, var6, this.ctx );
                        Operators.onDiscard( v, this.ctx );
                        if ( e_ != null )
                        {
                            throw e_;
                        }
                    }
                }
            }
            else
            {
                while ( true )
                {
                    Object v = this.s.poll();

                    try
                    {
                        if ( v == null || this.predicate.test( v ) )
                        {
                            return v;
                        }

                        Operators.onDiscard( v, this.ctx );
                    }
                    catch ( Throwable var7 )
                    {
                        RuntimeException e_ = Operators.onNextPollError( v, var7, this.ctx );
                        Operators.onDiscard( v, this.ctx );
                        if ( e_ != null )
                        {
                            throw e_;
                        }
                    }
                }
            }
        }

        public boolean isEmpty()
        {
            return this.s.isEmpty();
        }

        public void clear()
        {
            this.s.clear();
        }

        public int size()
        {
            return this.s.size();
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 4) != 0 )
            {
                return 0;
            }
            else
            {
                int m = this.s.requestFusion( requestedMode );
                this.sourceMode = m;
                return m;
            }
        }
    }

    static final class FilterFuseableSubscriber<T> implements InnerOperator<T,T>, Fuseable.QueueSubscription<T>, Fuseable.ConditionalSubscriber<T>
    {
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        final Predicate<? super T> predicate;
        Fuseable.QueueSubscription<T> s;
        boolean done;
        int sourceMode;

        FilterFuseableSubscriber( CoreSubscriber<? super T> actual, Predicate<? super T> predicate )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.predicate = predicate;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = (Fuseable.QueueSubscription) s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.sourceMode == 2 )
            {
                this.actual.onNext( (Object) null );
            }
            else
            {
                if ( this.done )
                {
                    Operators.onNextDropped( t, this.ctx );
                    return;
                }

                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.ctx, this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }
                    else
                    {
                        this.s.request( 1L );
                    }

                    Operators.onDiscard( t, this.ctx );
                    return;
                }

                if ( b )
                {
                    this.actual.onNext( t );
                }
                else
                {
                    this.s.request( 1L );
                    Operators.onDiscard( t, this.ctx );
                }
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
                return false;
            }
            else
            {
                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.ctx, this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }

                    Operators.onDiscard( t, this.ctx );
                    return false;
                }

                if ( b )
                {
                    this.actual.onNext( t );
                    return true;
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    return false;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }

        @Nullable
        public T poll()
        {
            if ( this.sourceMode == 2 )
            {
                long dropped = 0L;

                while ( true )
                {
                    Object v = this.s.poll();

                    try
                    {
                        if ( v == null || this.predicate.test( v ) )
                        {
                            if ( dropped != 0L )
                            {
                                this.request( dropped );
                            }

                            return v;
                        }

                        Operators.onDiscard( v, this.ctx );
                        ++dropped;
                    }
                    catch ( Throwable var6 )
                    {
                        RuntimeException e_ = Operators.onNextPollError( v, var6, this.currentContext() );
                        Operators.onDiscard( v, this.ctx );
                        if ( e_ != null )
                        {
                            throw e_;
                        }
                    }
                }
            }
            else
            {
                while ( true )
                {
                    Object v = this.s.poll();

                    try
                    {
                        if ( v == null || this.predicate.test( v ) )
                        {
                            return v;
                        }

                        Operators.onDiscard( v, this.ctx );
                    }
                    catch ( Throwable var7 )
                    {
                        RuntimeException e_ = Operators.onNextPollError( v, var7, this.currentContext() );
                        Operators.onDiscard( v, this.ctx );
                        if ( e_ != null )
                        {
                            throw e_;
                        }
                    }
                }
            }
        }

        public boolean isEmpty()
        {
            return this.s.isEmpty();
        }

        public void clear()
        {
            this.s.clear();
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 4) != 0 )
            {
                return 0;
            }
            else
            {
                int m = this.s.requestFusion( requestedMode );
                this.sourceMode = m;
                return m;
            }
        }

        public int size()
        {
            return this.s.size();
        }
    }
}
