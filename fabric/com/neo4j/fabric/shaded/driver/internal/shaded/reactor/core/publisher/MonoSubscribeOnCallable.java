package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionException;

final class MonoSubscribeOnCallable<T> extends Mono<T> implements Fuseable, Scannable
{
    final Callable<? extends T> callable;
    final Scheduler scheduler;

    MonoSubscribeOnCallable( Callable<? extends T> callable, Scheduler scheduler )
    {
        this.callable = (Callable) Objects.requireNonNull( callable, "callable" );
        this.scheduler = (Scheduler) Objects.requireNonNull( scheduler, "scheduler" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        FluxSubscribeOnCallable.CallableSubscribeOnSubscription<T> parent =
                new FluxSubscribeOnCallable.CallableSubscribeOnSubscription( actual, this.callable, this.scheduler );
        actual.onSubscribe( parent );

        try
        {
            parent.setMainFuture( this.scheduler.schedule( parent ) );
        }
        catch ( RejectedExecutionException var4 )
        {
            if ( parent.state != 4 )
            {
                actual.onError( Operators.onRejectedExecution( var4, actual.currentContext() ) );
            }
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : null;
    }
}
