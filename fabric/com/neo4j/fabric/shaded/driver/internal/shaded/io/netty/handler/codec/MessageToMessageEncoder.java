package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOutboundHandlerAdapter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCountUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.PromiseCombiner;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.StringUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.TypeParameterMatcher;

import java.util.List;

public abstract class MessageToMessageEncoder<I> extends ChannelOutboundHandlerAdapter
{
    private final TypeParameterMatcher matcher;

    protected MessageToMessageEncoder()
    {
        this.matcher = TypeParameterMatcher.find( this, MessageToMessageEncoder.class, "I" );
    }

    protected MessageToMessageEncoder( Class<? extends I> outboundMessageType )
    {
        this.matcher = TypeParameterMatcher.get( outboundMessageType );
    }

    private static void writeVoidPromise( ChannelHandlerContext ctx, CodecOutputList out )
    {
        ChannelPromise voidPromise = ctx.voidPromise();

        for ( int i = 0; i < out.size(); ++i )
        {
            ctx.write( out.getUnsafe( i ), voidPromise );
        }
    }

    private static void writePromiseCombiner( ChannelHandlerContext ctx, CodecOutputList out, ChannelPromise promise )
    {
        PromiseCombiner combiner = new PromiseCombiner( ctx.executor() );

        for ( int i = 0; i < out.size(); ++i )
        {
            combiner.add( (Future) ctx.write( out.getUnsafe( i ) ) );
        }

        combiner.finish( promise );
    }

    public boolean acceptOutboundMessage( Object msg ) throws Exception
    {
        return this.matcher.match( msg );
    }

    public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise ) throws Exception
    {
        CodecOutputList out = null;
        boolean var13 = false;

        try
        {
            var13 = true;
            if ( this.acceptOutboundMessage( msg ) )
            {
                out = CodecOutputList.newInstance();
                Object cast = msg;

                try
                {
                    this.encode( ctx, cast, out );
                }
                finally
                {
                    ReferenceCountUtil.release( msg );
                }

                if ( out.isEmpty() )
                {
                    out.recycle();
                    out = null;
                    throw new EncoderException( StringUtil.simpleClassName( (Object) this ) + " must produce at least one message." );
                }

                var13 = false;
            }
            else
            {
                ctx.write( msg, promise );
                var13 = false;
            }
        }
        catch ( EncoderException var19 )
        {
            throw var19;
        }
        catch ( Throwable var20 )
        {
            throw new EncoderException( var20 );
        }
        finally
        {
            if ( var13 )
            {
                if ( out != null )
                {
                    int sizeMinusOne = out.size() - 1;
                    if ( sizeMinusOne == 0 )
                    {
                        ctx.write( out.getUnsafe( 0 ), promise );
                    }
                    else if ( sizeMinusOne > 0 )
                    {
                        if ( promise == ctx.voidPromise() )
                        {
                            writeVoidPromise( ctx, out );
                        }
                        else
                        {
                            writePromiseCombiner( ctx, out, promise );
                        }
                    }

                    out.recycle();
                }
            }
        }

        if ( out != null )
        {
            int sizeMinusOne = out.size() - 1;
            if ( sizeMinusOne == 0 )
            {
                ctx.write( out.getUnsafe( 0 ), promise );
            }
            else if ( sizeMinusOne > 0 )
            {
                if ( promise == ctx.voidPromise() )
                {
                    writeVoidPromise( ctx, out );
                }
                else
                {
                    writePromiseCombiner( ctx, out, promise );
                }
            }

            out.recycle();
        }
    }

    protected abstract void encode( ChannelHandlerContext var1, I var2, List<Object> var3 ) throws Exception;
}
