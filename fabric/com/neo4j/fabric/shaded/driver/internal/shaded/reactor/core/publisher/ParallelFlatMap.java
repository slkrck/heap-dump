package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Queue;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;

final class ParallelFlatMap<T, R> extends ParallelFlux<R> implements Scannable
{
    final ParallelFlux<T> source;
    final Function<? super T,? extends Publisher<? extends R>> mapper;
    final boolean delayError;
    final int maxConcurrency;
    final Supplier<? extends Queue<R>> mainQueueSupplier;
    final int prefetch;
    final Supplier<? extends Queue<R>> innerQueueSupplier;

    ParallelFlatMap( ParallelFlux<T> source, Function<? super T,? extends Publisher<? extends R>> mapper, boolean delayError, int maxConcurrency,
            Supplier<? extends Queue<R>> mainQueueSupplier, int prefetch, Supplier<? extends Queue<R>> innerQueueSupplier )
    {
        this.source = source;
        this.mapper = mapper;
        this.delayError = delayError;
        this.maxConcurrency = maxConcurrency;
        this.mainQueueSupplier = mainQueueSupplier;
        this.prefetch = prefetch;
        this.innerQueueSupplier = innerQueueSupplier;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.DELAY_ERROR ? this.delayError : null;
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    public void subscribe( CoreSubscriber<? super R>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<T>[] parents = new CoreSubscriber[n];

            for ( int i = 0; i < n; ++i )
            {
                parents[i] =
                        new FluxFlatMap.FlatMapMain( subscribers[i], this.mapper, this.delayError, this.maxConcurrency, this.mainQueueSupplier, this.prefetch,
                                this.innerQueueSupplier );
            }

            this.source.subscribe( parents );
        }
    }
}
