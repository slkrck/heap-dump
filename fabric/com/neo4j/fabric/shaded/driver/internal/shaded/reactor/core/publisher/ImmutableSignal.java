package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.io.Serializable;
import java.util.Objects;

import org.reactivestreams.Subscription;

final class ImmutableSignal<T> implements Signal<T>, Serializable
{
    /**
     * @deprecated
     */
    @Deprecated
    static final Signal<Void> ON_COMPLETE;
    private static final long serialVersionUID = -2004454746525418508L;

    static
    {
        ON_COMPLETE = new ImmutableSignal( Context.empty(), SignalType.ON_COMPLETE, (Object) null, (Throwable) null, (Subscription) null );
    }

    private final transient Context context;
    private final SignalType type;
    private final Throwable throwable;
    private final T value;
    private final transient Subscription subscription;

    ImmutableSignal( Context context, SignalType type, @Nullable T value, @Nullable Throwable e, @Nullable Subscription subscription )
    {
        this.context = context;
        this.value = value;
        this.subscription = subscription;
        this.throwable = e;
        this.type = type;
    }

    @Nullable
    public Throwable getThrowable()
    {
        return this.throwable;
    }

    @Nullable
    public Subscription getSubscription()
    {
        return this.subscription;
    }

    @Nullable
    public T get()
    {
        return this.value;
    }

    public SignalType getType()
    {
        return this.type;
    }

    public Context getContext()
    {
        return this.context;
    }

    public boolean equals( @Nullable Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && o instanceof Signal )
        {
            Signal<?> signal = (Signal) o;
            if ( this.getType() != signal.getType() )
            {
                return false;
            }
            else if ( this.isOnComplete() )
            {
                return true;
            }
            else if ( this.isOnSubscribe() )
            {
                return Objects.equals( this.getSubscription(), signal.getSubscription() );
            }
            else if ( this.isOnError() )
            {
                return Objects.equals( this.getThrowable(), signal.getThrowable() );
            }
            else
            {
                return this.isOnNext() ? Objects.equals( this.get(), signal.get() ) : false;
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.getType().hashCode();
        if ( this.isOnError() )
        {
            return 31 * result + (this.getThrowable() != null ? this.getThrowable().hashCode() : 0);
        }
        else if ( this.isOnNext() )
        {
            return 31 * result + (this.get() != null ? this.get().hashCode() : 0);
        }
        else
        {
            return this.isOnSubscribe() ? 31 * result + (this.getSubscription() != null ? this.getSubscription().hashCode() : 0) : result;
        }
    }

    public String toString()
    {
        switch ( this.getType() )
        {
        case ON_SUBSCRIBE:
            return String.format( "onSubscribe(%s)", this.getSubscription() );
        case ON_NEXT:
            return String.format( "onNext(%s)", this.get() );
        case ON_ERROR:
            return String.format( "onError(%s)", this.getThrowable() );
        case ON_COMPLETE:
            return "onComplete()";
        default:
            return String.format( "Signal type=%s", this.getType() );
        }
    }
}
