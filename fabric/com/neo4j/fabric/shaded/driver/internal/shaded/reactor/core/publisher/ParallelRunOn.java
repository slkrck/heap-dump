package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Queue;
import java.util.function.Supplier;

final class ParallelRunOn<T> extends ParallelFlux<T> implements Scannable
{
    final ParallelFlux<? extends T> source;
    final Scheduler scheduler;
    final int prefetch;
    final Supplier<Queue<T>> queueSupplier;

    ParallelRunOn( ParallelFlux<? extends T> parent, Scheduler scheduler, int prefetch, Supplier<Queue<T>> queueSupplier )
    {
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.source = parent;
            this.scheduler = scheduler;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
        }
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<T>[] parents = new CoreSubscriber[n];
            boolean conditional = subscribers[0] instanceof Fuseable.ConditionalSubscriber;

            for ( int i = 0; i < n; ++i )
            {
                Scheduler.Worker w = this.scheduler.createWorker();
                if ( conditional )
                {
                    parents[i] = new FluxPublishOn.PublishOnConditionalSubscriber( (Fuseable.ConditionalSubscriber) subscribers[i], this.scheduler, w, true,
                            this.prefetch, this.prefetch, this.queueSupplier );
                }
                else
                {
                    parents[i] =
                            new FluxPublishOn.PublishOnSubscriber( subscribers[i], this.scheduler, w, true, this.prefetch, this.prefetch, this.queueSupplier );
                }
            }

            this.source.subscribe( parents );
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }
}
