package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.group;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.GenericFutureListener;

public interface ChannelGroupFutureListener extends GenericFutureListener<ChannelGroupFuture>
{
}
