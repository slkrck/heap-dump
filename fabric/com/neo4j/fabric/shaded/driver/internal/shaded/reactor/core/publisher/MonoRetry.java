package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class MonoRetry<T> extends InternalMonoOperator<T,T>
{
    final long times;

    MonoRetry( Mono<? extends T> source, long times )
    {
        super( source );
        if ( times < 0L )
        {
            throw new IllegalArgumentException( "times >= 0 required" );
        }
        else
        {
            this.times = times;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRetry.RetrySubscriber<T> parent = new FluxRetry.RetrySubscriber( this.source, actual, this.times );
        actual.onSubscribe( parent );
        if ( !parent.isCancelled() )
        {
            parent.resubscribe();
        }

        return null;
    }
}
