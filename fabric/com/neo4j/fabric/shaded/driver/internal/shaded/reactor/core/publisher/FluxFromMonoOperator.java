package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

abstract class FluxFromMonoOperator<I, O> extends Flux<O> implements Scannable, OptimizableOperator<O,I>
{
    protected final Mono<? extends I> source;
    @Nullable
    final OptimizableOperator<?,I> optimizableOperator;

    protected FluxFromMonoOperator( Mono<? extends I> source )
    {
        this.source = (Mono) Objects.requireNonNull( source );
        this.optimizableOperator = source instanceof OptimizableOperator ? (OptimizableOperator) source : null;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.PARENT ? this.source : null;
        }
    }

    public final void subscribe( CoreSubscriber<? super O> subscriber )
    {
        Object operator = this;

        while ( true )
        {
            subscriber = ((OptimizableOperator) operator).subscribeOrReturn( subscriber );
            if ( subscriber == null )
            {
                return;
            }

            OptimizableOperator newSource = ((OptimizableOperator) operator).nextOptimizableSource();
            if ( newSource == null )
            {
                ((OptimizableOperator) operator).source().subscribe( subscriber );
                return;
            }

            operator = newSource;
        }
    }

    @Nullable
    public abstract CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super O> var1 );

    public final CorePublisher<? extends I> source()
    {
        return this.source;
    }

    public final OptimizableOperator<?,? extends I> nextOptimizableSource()
    {
        return this.optimizableOperator;
    }
}
