package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufHolder;

interface PemEncoded extends ByteBufHolder
{
    boolean isSensitive();

    PemEncoded copy();

    PemEncoded duplicate();

    PemEncoded retainedDuplicate();

    PemEncoded replace( ByteBuf var1 );

    PemEncoded retain();

    PemEncoded retain( int var1 );

    PemEncoded touch();

    PemEncoded touch( Object var1 );
}
