package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.UncheckedBooleanSupplier;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

public interface RecvByteBufAllocator
{
    RecvByteBufAllocator.Handle newHandle();

    public interface ExtendedHandle extends RecvByteBufAllocator.Handle
    {
        boolean continueReading( UncheckedBooleanSupplier var1 );
    }

    /**
     * @deprecated
     */
    @Deprecated
    public interface Handle
    {
        ByteBuf allocate( ByteBufAllocator var1 );

        int guess();

        void reset( ChannelConfig var1 );

        void incMessagesRead( int var1 );

        void lastBytesRead( int var1 );

        int lastBytesRead();

        void attemptedBytesRead( int var1 );

        int attemptedBytesRead();

        boolean continueReading();

        void readComplete();
    }

    public static class DelegatingHandle implements RecvByteBufAllocator.Handle
    {
        private final RecvByteBufAllocator.Handle delegate;

        public DelegatingHandle( RecvByteBufAllocator.Handle delegate )
        {
            this.delegate = (RecvByteBufAllocator.Handle) ObjectUtil.checkNotNull( delegate, "delegate" );
        }

        protected final RecvByteBufAllocator.Handle delegate()
        {
            return this.delegate;
        }

        public ByteBuf allocate( ByteBufAllocator alloc )
        {
            return this.delegate.allocate( alloc );
        }

        public int guess()
        {
            return this.delegate.guess();
        }

        public void reset( ChannelConfig config )
        {
            this.delegate.reset( config );
        }

        public void incMessagesRead( int numMessages )
        {
            this.delegate.incMessagesRead( numMessages );
        }

        public void lastBytesRead( int bytes )
        {
            this.delegate.lastBytesRead( bytes );
        }

        public int lastBytesRead()
        {
            return this.delegate.lastBytesRead();
        }

        public boolean continueReading()
        {
            return this.delegate.continueReading();
        }

        public int attemptedBytesRead()
        {
            return this.delegate.attemptedBytesRead();
        }

        public void attemptedBytesRead( int bytes )
        {
            this.delegate.attemptedBytesRead( bytes );
        }

        public void readComplete()
        {
            this.delegate.readComplete();
        }
    }
}
