package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCounted;

import java.security.cert.X509Certificate;

interface OpenSslKeyMaterial extends ReferenceCounted
{
    X509Certificate[] certificateChain();

    long certificateChainAddress();

    long privateKeyAddress();

    OpenSslKeyMaterial retain();

    OpenSslKeyMaterial retain( int var1 );

    OpenSslKeyMaterial touch();

    OpenSslKeyMaterial touch( Object var1 );

    boolean release();

    boolean release( int var1 );
}
