package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Logger;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Loggers;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;

import sun.misc.Unsafe;

enum UnsafeSupport
{
    static final Logger logger =Loggers.getLogger(UnsafeSupport .class);
    private static final Unsafe UNSAFE;

    static
    {
        String javaSpecVersion = System.getProperty( "java.specification.version" );
        logger.debug( "Starting UnsafeSupport init in Java " + javaSpecVersion );
        ByteBuffer direct = ByteBuffer.allocateDirect( 1 );

        Object maybeUnsafe;
        try
        {
            Field unsafeField = Unsafe.class.getDeclaredField( "theUnsafe" );
            unsafeField.setAccessible( true );
            maybeUnsafe = unsafeField.get( (Object) null );
        }
        catch ( SecurityException | IllegalAccessException | NoSuchFieldException var14 )
        {
            maybeUnsafe = var14;
        }

        Unsafe unsafe;
        if ( maybeUnsafe instanceof Throwable )
        {
            unsafe = null;
            logger.debug( "Unsafe unavailable - Could not get it via Field sun.misc.Unsafe.theUnsafe", (Throwable) maybeUnsafe );
        }
        else
        {
            unsafe = (Unsafe) maybeUnsafe;
            logger.trace( "sun.misc.Unsafe.theUnsafe ok" );
        }

        Unsafe finalUnsafe;
        if ( unsafe != null )
        {
            finalUnsafe = unsafe;

            NoSuchMethodException maybeException;
            try
            {
                finalUnsafe.getClass().getDeclaredMethod( "copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE );
                maybeException = null;
            }
            catch ( SecurityException | NoSuchMethodException var13 )
            {
                maybeException = var13;
            }

            if ( maybeException == null )
            {
                logger.trace( "sun.misc.Unsafe.copyMemory ok" );
            }
            else
            {
                unsafe = null;
                logger.debug( "Unsafe unavailable - failed on sun.misc.Unsafe.copyMemory", (Throwable) maybeException );
            }
        }

        if ( unsafe != null )
        {
            finalUnsafe = unsafe;

            Object maybeAddressField;
            try
            {
                Field field = Buffer.class.getDeclaredField( "address" );
                long offset = finalUnsafe.objectFieldOffset( field );
                long heapAddress = finalUnsafe.getLong( ByteBuffer.allocate( 1 ), offset );
                long directAddress = finalUnsafe.getLong( direct, offset );
                if ( heapAddress != 0L && "1.8".equals( javaSpecVersion ) )
                {
                    maybeAddressField = new IllegalStateException( "A heap buffer must have 0 address in Java 8, got " + heapAddress );
                }
                else if ( heapAddress == 0L && !"1.8".equals( javaSpecVersion ) )
                {
                    maybeAddressField = new IllegalStateException( "A heap buffer must have non-zero address in Java " + javaSpecVersion );
                }
                else if ( directAddress == 0L )
                {
                    maybeAddressField = new IllegalStateException( "A direct buffer must have non-zero address" );
                }
                else
                {
                    maybeAddressField = field;
                }
            }
            catch ( SecurityException | NoSuchFieldException var15 )
            {
                maybeAddressField = var15;
            }

            if ( maybeAddressField instanceof Throwable )
            {
                logger.debug( "Unsafe unavailable - failed on java.nio.Buffer.address", (Throwable) maybeAddressField );
                unsafe = null;
            }
            else
            {
                logger.trace( "java.nio.Buffer.address ok" );
                logger.debug( "Unsafe is available" );
            }
        }

        UNSAFE = unsafe;
    }

    static Unsafe getUnsafe()
    {
        return UNSAFE;
    }

    static boolean hasUnsafe()
    {
        return UNSAFE != null;
    }
    }
