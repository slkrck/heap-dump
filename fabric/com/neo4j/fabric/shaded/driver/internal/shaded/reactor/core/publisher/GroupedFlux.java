package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

public abstract class GroupedFlux<K, V> extends Flux<V>
{
    @Nullable
    public abstract K key();
}
