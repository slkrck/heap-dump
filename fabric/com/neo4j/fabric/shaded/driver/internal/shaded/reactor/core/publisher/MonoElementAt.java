package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

import org.reactivestreams.Subscription;

final class MonoElementAt<T> extends MonoFromFluxOperator<T,T> implements Fuseable
{
    final long index;
    final T defaultValue;

    MonoElementAt( Flux<? extends T> source, long index )
    {
        super( source );
        if ( index < 0L )
        {
            throw new IndexOutOfBoundsException( "index >= required but it was " + index );
        }
        else
        {
            this.index = index;
            this.defaultValue = null;
        }
    }

    MonoElementAt( Flux<? extends T> source, long index, T defaultValue )
    {
        super( source );
        if ( index < 0L )
        {
            throw new IndexOutOfBoundsException( "index >= required but it was " + index );
        }
        else
        {
            this.index = index;
            this.defaultValue = Objects.requireNonNull( defaultValue, "defaultValue" );
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoElementAt.ElementAtSubscriber( actual, this.index, this.defaultValue );
    }

    static final class ElementAtSubscriber<T> extends Operators.MonoSubscriber<T,T>
    {
        final T defaultValue;
        long index;
        Subscription s;
        boolean done;

        ElementAtSubscriber( CoreSubscriber<? super T> actual, long index, T defaultValue )
        {
            super( actual );
            this.index = index;
            this.defaultValue = defaultValue;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
            }
        }

        public void request( long n )
        {
            super.request( n );
            if ( n > 0L )
            {
                this.s.request( Long.MAX_VALUE );
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                long i = this.index;
                if ( i == 0L )
                {
                    this.done = true;
                    this.s.cancel();
                    this.actual.onNext( t );
                    this.actual.onComplete();
                }
                else
                {
                    this.index = i - 1L;
                    Operators.onDiscard( t, this.actual.currentContext() );
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                if ( this.defaultValue != null )
                {
                    this.complete( this.defaultValue );
                }
                else
                {
                    this.actual.onError( Operators.onOperatorError( new IndexOutOfBoundsException(), this.actual.currentContext() ) );
                }
            }
        }
    }
}
