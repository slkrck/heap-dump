package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Subscription;

final class MonoPublishOn<T> extends InternalMonoOperator<T,T>
{
    final Scheduler scheduler;

    MonoPublishOn( Mono<? extends T> source, Scheduler scheduler )
    {
        super( source );
        this.scheduler = scheduler;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoPublishOn.PublishOnSubscriber( actual, this.scheduler );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }

    static final class PublishOnSubscriber<T> implements InnerOperator<T,T>, Runnable
    {
        static final AtomicReferenceFieldUpdater<MonoPublishOn.PublishOnSubscriber,Disposable> FUTURE =
                AtomicReferenceFieldUpdater.newUpdater( MonoPublishOn.PublishOnSubscriber.class, Disposable.class, "future" );
        static final AtomicReferenceFieldUpdater<MonoPublishOn.PublishOnSubscriber,Object> VALUE =
                AtomicReferenceFieldUpdater.newUpdater( MonoPublishOn.PublishOnSubscriber.class, Object.class, "value" );
        final CoreSubscriber<? super T> actual;
        final Scheduler scheduler;
        Subscription s;
        volatile Disposable future;
        volatile T value;
        volatile Throwable error;

        PublishOnSubscriber( CoreSubscriber<? super T> actual, Scheduler scheduler )
        {
            this.actual = actual;
            this.scheduler = scheduler;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.future == OperatorDisposables.DISPOSED;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.scheduler : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.value = t;
            this.trySchedule( this, (Throwable) null, t );
        }

        public void onError( Throwable t )
        {
            this.error = t;
            this.trySchedule( (Subscription) null, t, (Object) null );
        }

        public void onComplete()
        {
            if ( this.value == null )
            {
                this.trySchedule( (Subscription) null, (Throwable) null, (Object) null );
            }
        }

        void trySchedule( @Nullable Subscription subscription, @Nullable Throwable suppressed, @Nullable Object dataSignal )
        {
            if ( this.future == null )
            {
                try
                {
                    this.future = this.scheduler.schedule( this );
                }
                catch ( RejectedExecutionException var5 )
                {
                    this.actual.onError( Operators.onRejectedExecution( var5, subscription, suppressed, dataSignal, this.actual.currentContext() ) );
                }
            }
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            Disposable c = this.future;
            if ( c != OperatorDisposables.DISPOSED )
            {
                c = (Disposable) FUTURE.getAndSet( this, OperatorDisposables.DISPOSED );
                if ( c != null && !OperatorDisposables.isDisposed( c ) )
                {
                    c.dispose();
                }

                this.value = null;
            }

            this.s.cancel();
        }

        public void run()
        {
            if ( !OperatorDisposables.isDisposed( this.future ) )
            {
                T v = VALUE.getAndSet( this, (Object) null );
                if ( v != null )
                {
                    this.actual.onNext( v );
                    this.actual.onComplete();
                }
                else
                {
                    Throwable e = this.error;
                    if ( e != null )
                    {
                        this.actual.onError( e );
                    }
                    else
                    {
                        this.actual.onComplete();
                    }
                }
            }
        }
    }
}
