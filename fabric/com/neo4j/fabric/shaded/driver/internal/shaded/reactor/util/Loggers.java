package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.regex.Matcher;

public abstract class Loggers
{
    public static final String FALLBACK_PROPERTY = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.logging.fallback";
    private static Loggers.LoggerFactory LOGGER_FACTORY;

    static
    {
        resetLoggerFactory();
    }

    Loggers()
    {
    }

    public static final void resetLoggerFactory()
    {
        try
        {
            useSl4jLoggers();
        }
        catch ( Throwable var1 )
        {
            if ( isFallbackToJdk() )
            {
                useJdkLoggers();
            }
            else
            {
                useConsoleLoggers();
            }
        }
    }

    static final boolean isFallbackToJdk()
    {
        return "JDK".equalsIgnoreCase( System.getProperty( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.logging.fallback" ) );
    }

    public static final void useConsoleLoggers()
    {
        String name = Loggers.LoggerFactory.class.getName();
        Loggers.LoggerFactory loggerFactory = new Loggers.ConsoleLoggerFactory( false );
        loggerFactory.getLogger( name ).debug( "Using Console logging" );
        LOGGER_FACTORY = loggerFactory;
    }

    public static final void useVerboseConsoleLoggers()
    {
        String name = Loggers.LoggerFactory.class.getName();
        Loggers.LoggerFactory loggerFactory = new Loggers.ConsoleLoggerFactory( true );
        loggerFactory.getLogger( name ).debug( "Using Verbose Console logging" );
        LOGGER_FACTORY = loggerFactory;
    }

    public static final void useCustomLoggers( Function<String,? extends Logger> loggerFactory )
    {
        String name = Loggers.LoggerFactory.class.getName();
        ((Logger) loggerFactory.apply( name )).debug( "Using custom logging" );
        LOGGER_FACTORY = loggerFactory::apply;
    }

    public static final void useJdkLoggers()
    {
        String name = Loggers.LoggerFactory.class.getName();
        Loggers.LoggerFactory loggerFactory = new Loggers.JdkLoggerFactory();
        loggerFactory.getLogger( name ).debug( "Using JDK logging framework" );
        LOGGER_FACTORY = loggerFactory;
    }

    public static final void useSl4jLoggers()
    {
        String name = Loggers.LoggerFactory.class.getName();
        Loggers.LoggerFactory loggerFactory = new Loggers.Slf4JLoggerFactory();
        loggerFactory.getLogger( name ).debug( "Using Slf4j logging framework" );
        LOGGER_FACTORY = loggerFactory;
    }

    public static Logger getLogger( String name )
    {
        return LOGGER_FACTORY.getLogger( name );
    }

    public static Logger getLogger( Class<?> cls )
    {
        return LOGGER_FACTORY.getLogger( cls.getName() );
    }

    private interface LoggerFactory
    {
        Logger getLogger( String var1 );
    }

    private static final class ConsoleLoggerFactory implements Loggers.LoggerFactory
    {
        private static final HashMap<String,Logger> consoleLoggers = new HashMap();
        final boolean verbose;

        private ConsoleLoggerFactory( boolean verbose )
        {
            this.verbose = verbose;
        }

        public Logger getLogger( String name )
        {
            return (Logger) consoleLoggers.computeIfAbsent( name, ( n ) -> {
                return new Loggers.ConsoleLogger( n, this.verbose );
            } );
        }
    }

    static final class ConsoleLogger implements Logger
    {
        private final String name;
        private final PrintStream err;
        private final PrintStream log;
        private final boolean verbose;

        ConsoleLogger( String name, PrintStream log, PrintStream err, boolean verbose )
        {
            this.name = name;
            this.log = log;
            this.err = err;
            this.verbose = verbose;
        }

        ConsoleLogger( String name, boolean verbose )
        {
            this( name, System.out, System.err, verbose );
        }

        public String getName()
        {
            return this.name;
        }

        @Nullable
        final String format( @Nullable String from, @Nullable Object... arguments )
        {
            if ( from == null )
            {
                return null;
            }
            else
            {
                String computed = from;
                if ( arguments != null && arguments.length != 0 )
                {
                    Object[] var4 = arguments;
                    int var5 = arguments.length;

                    for ( int var6 = 0; var6 < var5; ++var6 )
                    {
                        Object argument = var4[var6];
                        computed = computed.replaceFirst( "\\{\\}", Matcher.quoteReplacement( String.valueOf( argument ) ) );
                    }
                }

                return computed;
            }
        }

        public boolean isTraceEnabled()
        {
            return this.verbose;
        }

        public synchronized void trace( String msg )
        {
            if ( this.verbose )
            {
                this.log.format( "[TRACE] (%s) %s\n", Thread.currentThread().getName(), msg );
            }
        }

        public synchronized void trace( String format, Object... arguments )
        {
            if ( this.verbose )
            {
                this.log.format( "[TRACE] (%s) %s\n", Thread.currentThread().getName(), this.format( format, arguments ) );
            }
        }

        public synchronized void trace( String msg, Throwable t )
        {
            if ( this.verbose )
            {
                this.log.format( "[TRACE] (%s) %s - %s\n", Thread.currentThread().getName(), msg, t );
                t.printStackTrace( this.log );
            }
        }

        public boolean isDebugEnabled()
        {
            return this.verbose;
        }

        public synchronized void debug( String msg )
        {
            if ( this.verbose )
            {
                this.log.format( "[DEBUG] (%s) %s\n", Thread.currentThread().getName(), msg );
            }
        }

        public synchronized void debug( String format, Object... arguments )
        {
            if ( this.verbose )
            {
                this.log.format( "[DEBUG] (%s) %s\n", Thread.currentThread().getName(), this.format( format, arguments ) );
            }
        }

        public synchronized void debug( String msg, Throwable t )
        {
            if ( this.verbose )
            {
                this.log.format( "[DEBUG] (%s) %s - %s\n", Thread.currentThread().getName(), msg, t );
                t.printStackTrace( this.log );
            }
        }

        public boolean isInfoEnabled()
        {
            return true;
        }

        public synchronized void info( String msg )
        {
            this.log.format( "[ INFO] (%s) %s\n", Thread.currentThread().getName(), msg );
        }

        public synchronized void info( String format, Object... arguments )
        {
            this.log.format( "[ INFO] (%s) %s\n", Thread.currentThread().getName(), this.format( format, arguments ) );
        }

        public synchronized void info( String msg, Throwable t )
        {
            this.log.format( "[ INFO] (%s) %s - %s\n", Thread.currentThread().getName(), msg, t );
            t.printStackTrace( this.log );
        }

        public boolean isWarnEnabled()
        {
            return true;
        }

        public synchronized void warn( String msg )
        {
            this.err.format( "[ WARN] (%s) %s\n", Thread.currentThread().getName(), msg );
        }

        public synchronized void warn( String format, Object... arguments )
        {
            this.err.format( "[ WARN] (%s) %s\n", Thread.currentThread().getName(), this.format( format, arguments ) );
        }

        public synchronized void warn( String msg, Throwable t )
        {
            this.err.format( "[ WARN] (%s) %s - %s\n", Thread.currentThread().getName(), msg, t );
            t.printStackTrace( this.err );
        }

        public boolean isErrorEnabled()
        {
            return true;
        }

        public synchronized void error( String msg )
        {
            this.err.format( "[ERROR] (%s) %s\n", Thread.currentThread().getName(), msg );
        }

        public synchronized void error( String format, Object... arguments )
        {
            this.err.format( "[ERROR] (%s) %s\n", Thread.currentThread().getName(), this.format( format, arguments ) );
        }

        public synchronized void error( String msg, Throwable t )
        {
            this.err.format( "[ERROR] (%s) %s - %s\n", Thread.currentThread().getName(), msg, t );
            t.printStackTrace( this.err );
        }
    }

    private static class JdkLoggerFactory implements Loggers.LoggerFactory
    {
        private JdkLoggerFactory()
        {
        }

        public Logger getLogger( String name )
        {
            return new Loggers.JdkLogger( java.util.logging.Logger.getLogger( name ) );
        }
    }

    static final class JdkLogger implements Logger
    {
        private final java.util.logging.Logger logger;

        public JdkLogger( java.util.logging.Logger logger )
        {
            this.logger = logger;
        }

        public String getName()
        {
            return this.logger.getName();
        }

        public boolean isTraceEnabled()
        {
            return this.logger.isLoggable( Level.FINEST );
        }

        public void trace( String msg )
        {
            this.logger.log( Level.FINEST, msg );
        }

        public void trace( String format, Object... arguments )
        {
            this.logger.log( Level.FINEST, this.format( format, arguments ) );
        }

        public void trace( String msg, Throwable t )
        {
            this.logger.log( Level.FINEST, msg, t );
        }

        public boolean isDebugEnabled()
        {
            return this.logger.isLoggable( Level.FINE );
        }

        public void debug( String msg )
        {
            this.logger.log( Level.FINE, msg );
        }

        public void debug( String format, Object... arguments )
        {
            this.logger.log( Level.FINE, this.format( format, arguments ) );
        }

        public void debug( String msg, Throwable t )
        {
            this.logger.log( Level.FINE, msg, t );
        }

        public boolean isInfoEnabled()
        {
            return this.logger.isLoggable( Level.INFO );
        }

        public void info( String msg )
        {
            this.logger.log( Level.INFO, msg );
        }

        public void info( String format, Object... arguments )
        {
            this.logger.log( Level.INFO, this.format( format, arguments ) );
        }

        public void info( String msg, Throwable t )
        {
            this.logger.log( Level.INFO, msg, t );
        }

        public boolean isWarnEnabled()
        {
            return this.logger.isLoggable( Level.WARNING );
        }

        public void warn( String msg )
        {
            this.logger.log( Level.WARNING, msg );
        }

        public void warn( String format, Object... arguments )
        {
            this.logger.log( Level.WARNING, this.format( format, arguments ) );
        }

        public void warn( String msg, Throwable t )
        {
            this.logger.log( Level.WARNING, msg, t );
        }

        public boolean isErrorEnabled()
        {
            return this.logger.isLoggable( Level.SEVERE );
        }

        public void error( String msg )
        {
            this.logger.log( Level.SEVERE, msg );
        }

        public void error( String format, Object... arguments )
        {
            this.logger.log( Level.SEVERE, this.format( format, arguments ) );
        }

        public void error( String msg, Throwable t )
        {
            this.logger.log( Level.SEVERE, msg, t );
        }

        @Nullable
        final String format( @Nullable String from, @Nullable Object... arguments )
        {
            if ( from == null )
            {
                return null;
            }
            else
            {
                String computed = from;
                if ( arguments != null && arguments.length != 0 )
                {
                    Object[] var4 = arguments;
                    int var5 = arguments.length;

                    for ( int var6 = 0; var6 < var5; ++var6 )
                    {
                        Object argument = var4[var6];
                        computed = computed.replaceFirst( "\\{\\}", Matcher.quoteReplacement( String.valueOf( argument ) ) );
                    }
                }

                return computed;
            }
        }
    }

    private static class Slf4JLogger implements Logger
    {
        private final org.slf4j.Logger logger;

        public Slf4JLogger( org.slf4j.Logger logger )
        {
            this.logger = logger;
        }

        public String getName()
        {
            return this.logger.getName();
        }

        public boolean isTraceEnabled()
        {
            return this.logger.isTraceEnabled();
        }

        public void trace( String msg )
        {
            this.logger.trace( msg );
        }

        public void trace( String format, Object... arguments )
        {
            this.logger.trace( format, arguments );
        }

        public void trace( String msg, Throwable t )
        {
            this.logger.trace( msg, t );
        }

        public boolean isDebugEnabled()
        {
            return this.logger.isDebugEnabled();
        }

        public void debug( String msg )
        {
            this.logger.debug( msg );
        }

        public void debug( String format, Object... arguments )
        {
            this.logger.debug( format, arguments );
        }

        public void debug( String msg, Throwable t )
        {
            this.logger.debug( msg, t );
        }

        public boolean isInfoEnabled()
        {
            return this.logger.isInfoEnabled();
        }

        public void info( String msg )
        {
            this.logger.info( msg );
        }

        public void info( String format, Object... arguments )
        {
            this.logger.info( format, arguments );
        }

        public void info( String msg, Throwable t )
        {
            this.logger.info( msg, t );
        }

        public boolean isWarnEnabled()
        {
            return this.logger.isWarnEnabled();
        }

        public void warn( String msg )
        {
            this.logger.warn( msg );
        }

        public void warn( String format, Object... arguments )
        {
            this.logger.warn( format, arguments );
        }

        public void warn( String msg, Throwable t )
        {
            this.logger.warn( msg, t );
        }

        public boolean isErrorEnabled()
        {
            return this.logger.isErrorEnabled();
        }

        public void error( String msg )
        {
            this.logger.error( msg );
        }

        public void error( String format, Object... arguments )
        {
            this.logger.error( format, arguments );
        }

        public void error( String msg, Throwable t )
        {
            this.logger.error( msg, t );
        }
    }

    private static class Slf4JLoggerFactory implements Loggers.LoggerFactory
    {
        private Slf4JLoggerFactory()
        {
        }

        public Logger getLogger( String name )
        {
            return new Loggers.Slf4JLogger( org.slf4j.LoggerFactory.getLogger( name ) );
        }
    }
}
