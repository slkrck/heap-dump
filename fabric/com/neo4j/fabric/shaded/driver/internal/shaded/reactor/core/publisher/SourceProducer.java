package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.reactivestreams.Publisher;

interface SourceProducer<O> extends Scannable, Publisher<O>
{
    @Nullable
    default Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return Scannable.from( (Object) null );
        }
        else
        {
            return key == Scannable.Attr.ACTUAL ? Scannable.from( (Object) null ) : null;
        }
    }

    default String stepName()
    {
        return "source(" + this.getClass().getSimpleName() + ")";
    }
}
