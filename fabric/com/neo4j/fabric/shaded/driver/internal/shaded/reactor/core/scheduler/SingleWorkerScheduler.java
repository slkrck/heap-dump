package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

final class SingleWorkerScheduler implements Scheduler, Executor, Scannable
{
    final Scheduler.Worker main;

    SingleWorkerScheduler( Scheduler actual )
    {
        this.main = actual.createWorker();
    }

    public void dispose()
    {
        this.main.dispose();
    }

    public Disposable schedule( Runnable task )
    {
        return this.main.schedule( task );
    }

    public Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        return this.main.schedule( task, delay, unit );
    }

    public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        return this.main.schedulePeriodically( task, initialDelay, period, unit );
    }

    public void execute( Runnable command )
    {
        this.main.schedule( command );
    }

    public Scheduler.Worker createWorker()
    {
        return new ExecutorScheduler.ExecutorSchedulerWorker( this );
    }

    public boolean isDisposed()
    {
        return this.main.isDisposed();
    }

    public String toString()
    {
        Scannable mainScannable = Scannable.from( this.main );
        return mainScannable.isScanAvailable() ? "singleWorker(" + mainScannable.scanUnsafe( Scannable.Attr.NAME ) + ")"
                                               : "singleWorker(" + this.main.toString() + ")";
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.main;
            }
            else
            {
                return key == Scannable.Attr.NAME ? this.toString() : Scannable.from( this.main ).scanUnsafe( key );
            }
        }
        else
        {
            return this.isDisposed();
        }
    }
}
