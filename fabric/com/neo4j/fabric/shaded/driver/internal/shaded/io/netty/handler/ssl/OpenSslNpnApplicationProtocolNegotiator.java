package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.util.List;

/**
 * @deprecated
 */
@Deprecated
public final class OpenSslNpnApplicationProtocolNegotiator implements OpenSslApplicationProtocolNegotiator
{
    private final List<String> protocols;

    public OpenSslNpnApplicationProtocolNegotiator( Iterable<String> protocols )
    {
        this.protocols = (List) ObjectUtil.checkNotNull( ApplicationProtocolUtil.toList( protocols ), "protocols" );
    }

    public OpenSslNpnApplicationProtocolNegotiator( String... protocols )
    {
        this.protocols = (List) ObjectUtil.checkNotNull( ApplicationProtocolUtil.toList( protocols ), "protocols" );
    }

    public ApplicationProtocolConfig.Protocol protocol()
    {
        return ApplicationProtocolConfig.Protocol.NPN;
    }

    public List<String> protocols()
    {
        return this.protocols;
    }

    public ApplicationProtocolConfig.SelectorFailureBehavior selectorFailureBehavior()
    {
        return ApplicationProtocolConfig.SelectorFailureBehavior.CHOOSE_MY_LAST_PROTOCOL;
    }

    public ApplicationProtocolConfig.SelectedListenerFailureBehavior selectedListenerFailureBehavior()
    {
        return ApplicationProtocolConfig.SelectedListenerFailureBehavior.ACCEPT;
    }
}
