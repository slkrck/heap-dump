package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Consumer;
import java.util.function.LongConsumer;

import org.reactivestreams.Subscription;

final class MonoPeekFuseable<T> extends InternalMonoOperator<T,T> implements Fuseable, SignalPeek<T>
{
    final Consumer<? super Subscription> onSubscribeCall;
    final Consumer<? super T> onNextCall;
    final LongConsumer onRequestCall;
    final Runnable onCancelCall;

    MonoPeekFuseable( Mono<? extends T> source, @Nullable Consumer<? super Subscription> onSubscribeCall, @Nullable Consumer<? super T> onNextCall,
            @Nullable LongConsumer onRequestCall, @Nullable Runnable onCancelCall )
    {
        super( source );
        this.onSubscribeCall = onSubscribeCall;
        this.onNextCall = onNextCall;
        this.onRequestCall = onRequestCall;
        this.onCancelCall = onCancelCall;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return (CoreSubscriber) (actual instanceof Fuseable.ConditionalSubscriber ? new FluxPeekFuseable.PeekFuseableConditionalSubscriber(
                (Fuseable.ConditionalSubscriber) actual, this ) : new FluxPeekFuseable.PeekFuseableSubscriber( actual, this ));
    }

    @Nullable
    public Consumer<? super Subscription> onSubscribeCall()
    {
        return this.onSubscribeCall;
    }

    @Nullable
    public Consumer<? super T> onNextCall()
    {
        return this.onNextCall;
    }

    @Nullable
    public Consumer<? super Throwable> onErrorCall()
    {
        return null;
    }

    @Nullable
    public Runnable onCompleteCall()
    {
        return null;
    }

    @Nullable
    public Runnable onAfterTerminateCall()
    {
        return null;
    }

    @Nullable
    public LongConsumer onRequestCall()
    {
        return this.onRequestCall;
    }

    @Nullable
    public Runnable onCancelCall()
    {
        return this.onCancelCall;
    }
}
