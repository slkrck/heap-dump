package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Objects;
import java.util.function.Consumer;

final class MonoDoOnEachFuseable<T> extends InternalMonoOperator<T,T> implements Fuseable
{
    final Consumer<? super Signal<T>> onSignal;

    MonoDoOnEachFuseable( Mono<? extends T> source, Consumer<? super Signal<T>> onSignal )
    {
        super( source );
        this.onSignal = (Consumer) Objects.requireNonNull( onSignal, "onSignal" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return FluxDoOnEach.createSubscriber( actual, this.onSignal, true, true );
    }
}
