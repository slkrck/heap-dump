package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.util.List;

public class FixedLengthFrameDecoder extends ByteToMessageDecoder
{
    private final int frameLength;

    public FixedLengthFrameDecoder( int frameLength )
    {
        ObjectUtil.checkPositive( frameLength, "frameLength" );
        this.frameLength = frameLength;
    }

    protected final void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out ) throws Exception
    {
        Object decoded = this.decode( ctx, in );
        if ( decoded != null )
        {
            out.add( decoded );
        }
    }

    protected Object decode( ChannelHandlerContext ctx, ByteBuf in ) throws Exception
    {
        return in.readableBytes() < this.frameLength ? null : in.readRetainedSlice( this.frameLength );
    }
}
