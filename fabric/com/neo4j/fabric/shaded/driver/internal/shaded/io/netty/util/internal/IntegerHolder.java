package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

/**
 * @deprecated
 */
@Deprecated
public final class IntegerHolder
{
    public int value;
}
