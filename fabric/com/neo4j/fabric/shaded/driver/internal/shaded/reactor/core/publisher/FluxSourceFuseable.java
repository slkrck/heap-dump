package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

import org.reactivestreams.Publisher;

final class FluxSourceFuseable<I> extends Flux<I> implements Fuseable, SourceProducer<I>, OptimizableOperator<I,I>
{
    final Publisher<? extends I> source;
    @Nullable
    final OptimizableOperator<?,I> optimizableOperator;

    FluxSourceFuseable( Publisher<? extends I> source )
    {
        this.source = (Publisher) Objects.requireNonNull( source );
        this.optimizableOperator = source instanceof OptimizableOperator ? (OptimizableOperator) source : null;
    }

    public void subscribe( CoreSubscriber<? super I> actual )
    {
        this.source.subscribe( actual );
    }

    public CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super I> actual )
    {
        return actual;
    }

    public final CorePublisher<? extends I> source()
    {
        return this;
    }

    public final OptimizableOperator<?,? extends I> nextOptimizableSource()
    {
        return this.optimizableOperator;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.PARENT ? this.source : null;
        }
    }
}
