package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

public interface LongCounter
{
    void add( long var1 );

    void increment();

    void decrement();

    long value();
}
