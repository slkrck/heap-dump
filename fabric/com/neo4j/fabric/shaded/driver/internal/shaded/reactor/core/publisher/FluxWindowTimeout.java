package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxWindowTimeout<T> extends InternalFluxOperator<T,Flux<T>>
{
    final int maxSize;
    final long timespan;
    final Scheduler timer;

    FluxWindowTimeout( Flux<T> source, int maxSize, long timespan, Scheduler timer )
    {
        super( source );
        if ( timespan <= 0L )
        {
            throw new IllegalArgumentException( "Timeout period must be strictly positive" );
        }
        else if ( maxSize <= 0 )
        {
            throw new IllegalArgumentException( "maxSize must be strictly positive" );
        }
        else
        {
            this.timer = (Scheduler) Objects.requireNonNull( timer, "Timer" );
            this.timespan = timespan;
            this.maxSize = maxSize;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Flux<T>> actual )
    {
        return new FluxWindowTimeout.WindowTimeoutSubscriber( actual, this.maxSize, this.timespan, this.timer );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.timer : super.scanUnsafe( key );
    }

    static final class WindowTimeoutSubscriber<T> implements InnerOperator<T,Flux<T>>
    {
        static final AtomicLongFieldUpdater<FluxWindowTimeout.WindowTimeoutSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxWindowTimeout.WindowTimeoutSubscriber.class, "requested" );
        static final AtomicIntegerFieldUpdater<FluxWindowTimeout.WindowTimeoutSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindowTimeout.WindowTimeoutSubscriber.class, "wip" );
        static final AtomicReferenceFieldUpdater<FluxWindowTimeout.WindowTimeoutSubscriber,Disposable> TIMER =
                AtomicReferenceFieldUpdater.newUpdater( FluxWindowTimeout.WindowTimeoutSubscriber.class, Disposable.class, "timer" );
        final CoreSubscriber<? super Flux<T>> actual;
        final long timespan;
        final Scheduler scheduler;
        final int maxSize;
        final Scheduler.Worker worker;
        final Queue<Object> queue;
        Throwable error;
        volatile boolean done;
        volatile boolean cancelled;
        volatile long requested;
        volatile int wip;
        int count;
        long producerIndex;
        Subscription s;
        UnicastProcessor<T> window;
        volatile boolean terminated;
        volatile Disposable timer;

        WindowTimeoutSubscriber( CoreSubscriber<? super Flux<T>> actual, int maxSize, long timespan, Scheduler scheduler )
        {
            this.actual = actual;
            this.queue = (Queue) Queues.unboundedMultiproducer().get();
            this.timespan = timespan;
            this.scheduler = scheduler;
            this.maxSize = maxSize;
            this.worker = scheduler.createWorker();
        }

        public CoreSubscriber<? super Flux<T>> actual()
        {
            return this.actual;
        }

        public Stream<? extends Scannable> inners()
        {
            UnicastProcessor<T> w = this.window;
            return w == null ? Stream.empty() : Stream.of( w );
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.maxSize;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.queue.size();
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.worker : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                Subscriber<? super Flux<T>> a = this.actual;
                a.onSubscribe( this );
                if ( this.cancelled )
                {
                    return;
                }

                UnicastProcessor<T> w = UnicastProcessor.create();
                this.window = w;
                long r = this.requested;
                if ( r == 0L )
                {
                    a.onError( Operators.onOperatorError( s, Exceptions.failWithOverflow(), this.actual.currentContext() ) );
                    return;
                }

                a.onNext( w );
                if ( r != Long.MAX_VALUE )
                {
                    REQUESTED.decrementAndGet( this );
                }

                if ( OperatorDisposables.replace( TIMER, this, this.newPeriod() ) )
                {
                    s.request( Long.MAX_VALUE );
                }
            }
        }

        Disposable newPeriod()
        {
            try
            {
                return this.worker.schedulePeriodically( new FluxWindowTimeout.WindowTimeoutSubscriber.ConsumerIndexHolder( this.producerIndex, this ),
                        this.timespan, this.timespan, TimeUnit.MILLISECONDS );
            }
            catch ( Exception var2 )
            {
                this.actual.onError( Operators.onRejectedExecution( var2, this.s, (Throwable) null, (Object) null, this.actual.currentContext() ) );
                return Disposables.disposed();
            }
        }

        public void onNext( T t )
        {
            if ( !this.terminated )
            {
                if ( WIP.get( this ) == 0 && WIP.compareAndSet( this, 0, 1 ) )
                {
                    UnicastProcessor<T> w = this.window;
                    w.onNext( t );
                    int c = this.count + 1;
                    if ( c >= this.maxSize )
                    {
                        ++this.producerIndex;
                        this.count = 0;
                        w.onComplete();
                        long r = this.requested;
                        if ( r == 0L )
                        {
                            this.window = null;
                            this.actual.onError( Operators.onOperatorError( this.s, Exceptions.failWithOverflow(), t, this.actual.currentContext() ) );
                            this.timer.dispose();
                            this.worker.dispose();
                            return;
                        }

                        w = UnicastProcessor.create();
                        this.window = w;
                        this.actual.onNext( w );
                        if ( r != Long.MAX_VALUE )
                        {
                            REQUESTED.decrementAndGet( this );
                        }

                        Disposable tm = this.timer;
                        tm.dispose();
                        Disposable task = this.newPeriod();
                        if ( !TIMER.compareAndSet( this, tm, task ) )
                        {
                            task.dispose();
                        }
                    }
                    else
                    {
                        this.count = c;
                    }

                    if ( WIP.decrementAndGet( this ) == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    this.queue.offer( t );
                    if ( !this.enter() )
                    {
                        return;
                    }
                }

                this.drainLoop();
            }
        }

        public void onError( Throwable t )
        {
            this.error = t;
            this.done = true;
            if ( this.enter() )
            {
                this.drainLoop();
            }

            this.actual.onError( t );
            this.timer.dispose();
            this.worker.dispose();
        }

        public void onComplete()
        {
            this.done = true;
            if ( this.enter() )
            {
                this.drainLoop();
            }

            this.actual.onComplete();
            this.timer.dispose();
            this.worker.dispose();
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
            }
        }

        public void cancel()
        {
            this.cancelled = true;
        }

        void drainLoop()
        {
            Queue<Object> q = this.queue;
            Subscriber<? super Flux<T>> a = this.actual;
            UnicastProcessor<T> w = this.window;
            int missed = 1;

            while ( !this.terminated )
            {
                boolean d = this.done;
                Object o = q.poll();
                boolean empty = o == null;
                boolean isHolder = o instanceof FluxWindowTimeout.WindowTimeoutSubscriber.ConsumerIndexHolder;
                if ( d && (empty || isHolder) )
                {
                    this.window = null;
                    q.clear();
                    Throwable err = this.error;
                    if ( err != null )
                    {
                        w.onError( err );
                    }
                    else
                    {
                        w.onComplete();
                    }

                    this.timer.dispose();
                    this.worker.dispose();
                    return;
                }

                if ( empty )
                {
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else if ( isHolder )
                {
                    w.onComplete();
                    this.count = 0;
                    w = UnicastProcessor.create();
                    this.window = w;
                    long r = this.requested;
                    if ( r == 0L )
                    {
                        this.window = null;
                        this.queue.clear();
                        a.onError( Operators.onOperatorError( this.s, Exceptions.failWithOverflow(), this.actual.currentContext() ) );
                        this.timer.dispose();
                        this.worker.dispose();
                        return;
                    }

                    a.onNext( w );
                    if ( r != Long.MAX_VALUE )
                    {
                        REQUESTED.decrementAndGet( this );
                    }
                }
                else
                {
                    w.onNext( o );
                    int c = this.count + 1;
                    if ( c >= this.maxSize )
                    {
                        ++this.producerIndex;
                        this.count = 0;
                        w.onComplete();
                        long r = this.requested;
                        if ( r == 0L )
                        {
                            this.window = null;
                            a.onError( Operators.onOperatorError( this.s, Exceptions.failWithOverflow(), o, this.actual.currentContext() ) );
                            this.timer.dispose();
                            this.worker.dispose();
                            return;
                        }

                        w = UnicastProcessor.create();
                        this.window = w;
                        this.actual.onNext( w );
                        if ( r != Long.MAX_VALUE )
                        {
                            REQUESTED.decrementAndGet( this );
                        }

                        Disposable tm = this.timer;
                        tm.dispose();
                        Disposable task = this.newPeriod();
                        if ( !TIMER.compareAndSet( this, tm, task ) )
                        {
                            task.dispose();
                        }
                    }
                    else
                    {
                        this.count = c;
                    }
                }
            }

            this.s.cancel();
            q.clear();
            this.timer.dispose();
            this.worker.dispose();
        }

        boolean enter()
        {
            return WIP.getAndIncrement( this ) == 0;
        }

        static final class ConsumerIndexHolder implements Runnable
        {
            final long index;
            final FluxWindowTimeout.WindowTimeoutSubscriber<?> parent;

            ConsumerIndexHolder( long index, FluxWindowTimeout.WindowTimeoutSubscriber<?> parent )
            {
                this.index = index;
                this.parent = parent;
            }

            public void run()
            {
                FluxWindowTimeout.WindowTimeoutSubscriber<?> p = this.parent;
                if ( !p.cancelled )
                {
                    p.queue.offer( this );
                }
                else
                {
                    p.terminated = true;
                    p.timer.dispose();
                    p.worker.dispose();
                }

                if ( p.enter() )
                {
                    p.drainLoop();
                }
            }
        }
    }
}
