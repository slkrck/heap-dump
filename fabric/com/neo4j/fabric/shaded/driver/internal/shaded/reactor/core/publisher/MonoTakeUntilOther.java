package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;

import org.reactivestreams.Publisher;

final class MonoTakeUntilOther<T, U> extends InternalMonoOperator<T,T>
{
    private final Publisher<U> other;

    MonoTakeUntilOther( Mono<? extends T> source, Publisher<U> other )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxTakeUntilOther.TakeUntilMainSubscriber<T> mainSubscriber = new FluxTakeUntilOther.TakeUntilMainSubscriber( actual );
        FluxTakeUntilOther.TakeUntilOtherSubscriber<U> otherSubscriber = new FluxTakeUntilOther.TakeUntilOtherSubscriber( mainSubscriber );
        this.other.subscribe( otherSubscriber );
        return mainSubscriber;
    }
}
