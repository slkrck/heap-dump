package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxOnBackpressureLatest<T> extends InternalFluxOperator<T,T>
{
    FluxOnBackpressureLatest( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxOnBackpressureLatest.LatestSubscriber( actual );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    static final class LatestSubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicLongFieldUpdater<FluxOnBackpressureLatest.LatestSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxOnBackpressureLatest.LatestSubscriber.class, "requested" );
        static final AtomicIntegerFieldUpdater<FluxOnBackpressureLatest.LatestSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxOnBackpressureLatest.LatestSubscriber.class, "wip" );
        static final AtomicReferenceFieldUpdater<FluxOnBackpressureLatest.LatestSubscriber,Object> VALUE =
                AtomicReferenceFieldUpdater.newUpdater( FluxOnBackpressureLatest.LatestSubscriber.class, Object.class, "value" );
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        volatile long requested;
        volatile int wip;
        Subscription s;
        Throwable error;
        volatile boolean done;
        volatile boolean cancelled;
        volatile T value;

        LatestSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.drain();
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    Object toDiscard = VALUE.getAndSet( this, (Object) null );
                    if ( toDiscard != null )
                    {
                        Operators.onDiscard( toDiscard, this.ctx );
                    }
                }
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            Object toDiscard = VALUE.getAndSet( this, t );
            if ( toDiscard != null )
            {
                Operators.onDiscard( toDiscard, this.ctx );
            }

            this.drain();
        }

        public void onError( Throwable t )
        {
            this.error = t;
            this.done = true;
            this.drain();
        }

        public void onComplete()
        {
            this.done = true;
            this.drain();
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                Subscriber<? super T> a = this.actual;
                int missed = 1;

                do
                {
                    if ( this.checkTerminated( this.done, this.value == null, a ) )
                    {
                        return;
                    }

                    long r = this.requested;

                    long e;
                    for ( e = 0L; r != e; ++e )
                    {
                        boolean d = this.done;
                        T v = VALUE.getAndSet( this, (Object) null );
                        boolean empty = v == null;
                        if ( this.checkTerminated( d, empty, a ) )
                        {
                            return;
                        }

                        if ( empty )
                        {
                            break;
                        }

                        a.onNext( v );
                    }

                    if ( r == e && this.checkTerminated( this.done, this.value == null, a ) )
                    {
                        return;
                    }

                    if ( e != 0L && r != Long.MAX_VALUE )
                    {
                        Operators.produced( REQUESTED, this, e );
                    }

                    missed = WIP.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }

        boolean checkTerminated( boolean d, boolean empty, Subscriber<? super T> a )
        {
            if ( this.cancelled )
            {
                Object toDiscard = VALUE.getAndSet( this, (Object) null );
                if ( toDiscard != null )
                {
                    Operators.onDiscard( toDiscard, this.ctx );
                }

                return true;
            }
            else
            {
                if ( d )
                {
                    Throwable e = this.error;
                    if ( e != null )
                    {
                        Object toDiscard = VALUE.getAndSet( this, (Object) null );
                        if ( toDiscard != null )
                        {
                            Operators.onDiscard( toDiscard, this.ctx );
                        }

                        a.onError( e );
                        return true;
                    }

                    if ( empty )
                    {
                        a.onComplete();
                        return true;
                    }
                }

                return false;
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.value != null ? 1 : 0;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? Integer.MAX_VALUE : InnerOperator.super.scanUnsafe( key );
            }
        }
    }
}
