package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufHolder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

public final class DefaultMessageSizeEstimator implements MessageSizeEstimator
{
    public static final MessageSizeEstimator DEFAULT = new DefaultMessageSizeEstimator( 8 );
    private final MessageSizeEstimator.Handle handle;

    public DefaultMessageSizeEstimator( int unknownSize )
    {
        ObjectUtil.checkPositiveOrZero( unknownSize, "unknownSize" );
        this.handle = new DefaultMessageSizeEstimator.HandleImpl( unknownSize );
    }

    public MessageSizeEstimator.Handle newHandle()
    {
        return this.handle;
    }

    private static final class HandleImpl implements MessageSizeEstimator.Handle
    {
        private final int unknownSize;

        private HandleImpl( int unknownSize )
        {
            this.unknownSize = unknownSize;
        }

        public int size( Object msg )
        {
            if ( msg instanceof ByteBuf )
            {
                return ((ByteBuf) msg).readableBytes();
            }
            else if ( msg instanceof ByteBufHolder )
            {
                return ((ByteBufHolder) msg).content().readableBytes();
            }
            else
            {
                return msg instanceof FileRegion ? 0 : this.unknownSize;
            }
        }
    }
}
