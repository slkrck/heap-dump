package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.io.Serializable;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Consumer;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

abstract class EventLoopProcessor<IN> extends FluxProcessor<IN,IN> implements Runnable
{
    static final int SHUTDOWN = 1;
    static final int FORCED_SHUTDOWN = 2;
    static final AtomicIntegerFieldUpdater<EventLoopProcessor> SUBSCRIBER_COUNT =
            AtomicIntegerFieldUpdater.newUpdater( EventLoopProcessor.class, "subscriberCount" );
    static final AtomicIntegerFieldUpdater<EventLoopProcessor> TERMINATED = AtomicIntegerFieldUpdater.newUpdater( EventLoopProcessor.class, "terminated" );
    final ExecutorService executor;
    final ExecutorService requestTaskExecutor;
    final EventLoopProcessor.EventLoopContext contextClassLoader;
    final String name;
    final boolean autoCancel;
    final RingBuffer<EventLoopProcessor.Slot<IN>> ringBuffer;
    final WaitStrategy readWait = WaitStrategy.liteBlocking();
    Subscription upstreamSubscription;
    volatile boolean cancelled;
    volatile int terminated;
    volatile Throwable error;
    volatile int subscriberCount;

    EventLoopProcessor( int bufferSize, @Nullable ThreadFactory threadFactory, @Nullable ExecutorService executor, ExecutorService requestExecutor,
            boolean autoCancel, boolean multiproducers, Supplier<EventLoopProcessor.Slot<IN>> factory, WaitStrategy strategy )
    {
        if ( !Queues.isPowerOfTwo( bufferSize ) )
        {
            throw new IllegalArgumentException( "bufferSize must be a power of 2 : " + bufferSize );
        }
        else if ( bufferSize < 1 )
        {
            throw new IllegalArgumentException( "bufferSize must be strictly positive, was: " + bufferSize );
        }
        else
        {
            this.autoCancel = autoCancel;
            this.contextClassLoader = new EventLoopProcessor.EventLoopContext( multiproducers );
            this.name = defaultName( threadFactory, this.getClass() );
            this.requestTaskExecutor = (ExecutorService) Objects.requireNonNull( requestExecutor, "requestTaskExecutor" );
            if ( executor == null )
            {
                this.executor = Executors.newCachedThreadPool( threadFactory );
            }
            else
            {
                this.executor = executor;
            }

            if ( multiproducers )
            {
                this.ringBuffer = RingBuffer.createMultiProducer( factory, bufferSize, strategy, this );
            }
            else
            {
                this.ringBuffer = RingBuffer.createSingleProducer( factory, bufferSize, strategy, this );
            }
        }
    }

    static <E> Flux<E> coldSource( RingBuffer<EventLoopProcessor.Slot<E>> ringBuffer, @Nullable Throwable t, @Nullable Throwable error,
            RingBuffer.Sequence start )
    {
        Flux<E> bufferIterable = generate( start::getAsLong, ( seq, sink ) -> {
            long s = seq + 1L;
            if ( s > ringBuffer.getCursor() )
            {
                sink.complete();
            }
            else
            {
                E d = ((EventLoopProcessor.Slot) ringBuffer.get( s )).value;
                if ( d != null )
                {
                    sink.next( d );
                }
            }

            return s;
        } );
        if ( error != null )
        {
            if ( t != null )
            {
                t = Exceptions.addSuppressed( t, error );
                return concat( new Publisher[]{bufferIterable, Flux.error( t )} );
            }
            else
            {
                return concat( new Publisher[]{bufferIterable, Flux.error( error )} );
            }
        }
        else
        {
            return bufferIterable;
        }
    }

    static Runnable createRequestTask( Subscription upstream, EventLoopProcessor<?> p, @Nullable Consumer<Long> postWaitCallback, LongSupplier readCount )
    {
        return new EventLoopProcessor.RequestTask( upstream, p, postWaitCallback, readCount );
    }

    static boolean waitRequestOrTerminalEvent( LongSupplier pendingRequest, RingBuffer.Reader barrier, AtomicBoolean isRunning, LongSupplier nextSequence,
            Runnable waiter )
    {
        while ( true )
        {
            try
            {
                if ( pendingRequest.getAsLong() <= 0L )
                {
                    long waitedSequence = nextSequence.getAsLong() + 1L;
                    waiter.run();
                    barrier.waitFor( waitedSequence, waiter );
                    if ( !isRunning.get() )
                    {
                        WaitStrategy.alert();
                    }

                    LockSupport.parkNanos( 1L );
                    continue;
                }
            }
            catch ( InterruptedException var7 )
            {
                Thread.currentThread().interrupt();
            }
            catch ( Exception var8 )
            {
                if ( isRunning.get() && !WaitStrategy.isAlert( var8 ) )
                {
                    throw var8;
                }

                return false;
            }

            return true;
        }
    }

    static void addCap( RingBuffer.Sequence sequence, long toAdd )
    {
        long u;
        long r;
        do
        {
            r = sequence.getAsLong();
            if ( r == Long.MAX_VALUE )
            {
                return;
            }

            u = Operators.addCap( r, toAdd );
        }
        while ( !sequence.compareAndSet( r, u ) );
    }

    static long getAndSub( RingBuffer.Sequence sequence, long toSub )
    {
        while ( true )
        {
            long r = sequence.getAsLong();
            if ( r != 0L && r != Long.MAX_VALUE )
            {
                long u = Operators.subOrZero( r, toSub );
                if ( !sequence.compareAndSet( r, u ) )
                {
                    continue;
                }

                return r;
            }

            return r;
        }
    }

    protected static String defaultName( @Nullable ThreadFactory threadFactory, Class<? extends EventLoopProcessor> clazz )
    {
        String name = threadFactory instanceof Supplier ? ((Supplier) threadFactory).get().toString() : null;
        return null != name ? name : clazz.getSimpleName();
    }

    protected static ExecutorService defaultRequestTaskExecutor( String name )
    {
        return Executors.newCachedThreadPool( ( r ) -> {
            return new Thread( r, name + "[request-task]" );
        } );
    }

    public abstract long getPending();

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.PARENT ? this.upstreamSubscription : super.scanUnsafe( key );
    }

    public final boolean alive()
    {
        return 0 == this.terminated;
    }

    public final boolean awaitAndShutdown()
    {
        return this.awaitAndShutdown( Duration.ofSeconds( -1L ) );
    }

    /**
     * @deprecated
     */
    @Deprecated
    public final boolean awaitAndShutdown( long timeout, TimeUnit timeUnit )
    {
        try
        {
            this.shutdown();
            return this.executor.awaitTermination( timeout, timeUnit );
        }
        catch ( InterruptedException var5 )
        {
            Thread.currentThread().interrupt();
            return false;
        }
    }

    public final boolean awaitAndShutdown( Duration timeout )
    {
        long nanos = -1L;
        if ( !timeout.isNegative() )
        {
            nanos = timeout.toNanos();
        }

        try
        {
            this.shutdown();
            return this.executor.awaitTermination( nanos, TimeUnit.NANOSECONDS );
        }
        catch ( InterruptedException var5 )
        {
            Thread.currentThread().interrupt();
            return false;
        }
    }

    public Stream<? extends Scannable> inners()
    {
        return Stream.empty();
    }

    public Flux<IN> drain()
    {
        return Flux.empty();
    }

    public final Flux<IN> forceShutdown()
    {
        int t = this.terminated;
        if ( t != 2 && TERMINATED.compareAndSet( this, t, 2 ) )
        {
            this.executor.shutdownNow();
            this.requestTaskExecutor.shutdownNow();
        }

        return this.drain();
    }

    public final long getAvailableCapacity()
    {
        return (long) (this.ringBuffer.bufferSize() - this.ringBuffer.getPending());
    }

    @Nullable
    public final Throwable getError()
    {
        return this.error;
    }

    public final String toString()
    {
        return "/Processors/" + this.name + "/" + this.contextClassLoader.hashCode();
    }

    public final int hashCode()
    {
        return this.contextClassLoader.hashCode();
    }

    public boolean isSerialized()
    {
        return this.contextClassLoader.multiproducer;
    }

    public final boolean isTerminated()
    {
        return this.terminated > 0;
    }

    public final void onComplete()
    {
        if ( TERMINATED.compareAndSet( this, 0, 1 ) )
        {
            this.upstreamSubscription = null;
            this.doComplete();
            this.executor.shutdown();
            this.readWait.signalAllWhenBlocking();
        }
    }

    public final void onError( Throwable t )
    {
        Objects.requireNonNull( t, "onError" );
        if ( TERMINATED.compareAndSet( this, 0, 1 ) )
        {
            this.error = t;
            this.upstreamSubscription = null;
            this.doError( t );
            this.executor.shutdown();
            this.readWait.signalAllWhenBlocking();
        }
        else
        {
            Operators.onErrorDropped( t, Context.empty() );
        }
    }

    public final void onNext( IN o )
    {
        Objects.requireNonNull( o, "onNext" );
        long seqId = this.ringBuffer.next();
        EventLoopProcessor.Slot<IN> signal = (EventLoopProcessor.Slot) this.ringBuffer.get( seqId );
        signal.value = o;
        this.ringBuffer.publish( seqId );
    }

    public final void onSubscribe( Subscription s )
    {
        if ( Operators.validate( this.upstreamSubscription, s ) )
        {
            this.upstreamSubscription = s;

            try
            {
                if ( s != Operators.emptySubscription() )
                {
                    this.requestTask( s );
                }
            }
            catch ( Throwable var3 )
            {
                this.onError( Operators.onOperatorError( s, var3, this.currentContext() ) );
            }
        }
    }

    protected boolean serializeAlways()
    {
        return !this.contextClassLoader.multiproducer;
    }

    public final void shutdown()
    {
        try
        {
            this.onComplete();
            this.executor.shutdown();
            this.requestTaskExecutor.shutdown();
        }
        catch ( Throwable var2 )
        {
            this.onError( Operators.onOperatorError( var2, this.currentContext() ) );
        }
    }

    public final int getBufferSize()
    {
        return this.ringBuffer.bufferSize();
    }

    final void cancel()
    {
        this.cancelled = true;
        if ( TERMINATED.compareAndSet( this, 0, 1 ) )
        {
            this.executor.shutdown();
        }

        this.readWait.signalAllWhenBlocking();
    }

    protected void doComplete()
    {
    }

    protected void requestTask( Subscription s )
    {
    }

    final void decrementSubscribers()
    {
        Subscription subscription = this.upstreamSubscription;
        int subs = SUBSCRIBER_COUNT.decrementAndGet( this );
        if ( subs == 0 && subscription != null && this.autoCancel )
        {
            this.upstreamSubscription = null;
            this.cancel();
        }
    }

    public long downstreamCount()
    {
        return (long) this.subscriberCount;
    }

    abstract void doError( Throwable var1 );

    final boolean incrementSubscribers()
    {
        return SUBSCRIBER_COUNT.getAndIncrement( this ) == 0;
    }

    static final class EventLoopFactory implements ThreadFactory, Supplier<String>
    {
        static final AtomicInteger COUNT = new AtomicInteger();
        final String name;
        final boolean daemon;

        EventLoopFactory( String name, boolean daemon )
        {
            this.name = name;
            this.daemon = daemon;
        }

        public Thread newThread( Runnable r )
        {
            Thread t = new Thread( r, this.name + "-" + COUNT.incrementAndGet() );
            t.setDaemon( this.daemon );
            return t;
        }

        public String get()
        {
            return this.name;
        }
    }

    public static final class Slot<T> implements Serializable
    {
        private static final long serialVersionUID = 5172014386416785095L;
        public T value;
    }

    static final class EventLoopContext extends ClassLoader
    {
        final boolean multiproducer;

        EventLoopContext( boolean multiproducer )
        {
            super( Thread.currentThread().getContextClassLoader() );
            this.multiproducer = multiproducer;
        }
    }

    static final class RequestTask implements Runnable
    {
        final LongSupplier readCount;
        final Subscription upstream;
        final EventLoopProcessor<?> parent;
        final Consumer<Long> postWaitCallback;

        RequestTask( Subscription upstream, EventLoopProcessor<?> p, @Nullable Consumer<Long> postWaitCallback, LongSupplier readCount )
        {
            this.parent = p;
            this.readCount = readCount;
            this.postWaitCallback = postWaitCallback;
            this.upstream = upstream;
        }

        public void run()
        {
            long bufferSize = (long) this.parent.ringBuffer.bufferSize();
            long limit = bufferSize == 1L ? bufferSize : bufferSize - Math.max( bufferSize >> 2, 1L );
            long cursor = -1L;

            try
            {
                this.parent.run();
                this.upstream.request( bufferSize );

                while ( true )
                {
                    long c = cursor + limit;
                    cursor = this.parent.readWait.waitFor( c, this.readCount, this.parent );
                    if ( this.postWaitCallback != null )
                    {
                        this.postWaitCallback.accept( cursor );
                    }

                    this.upstream.request( limit + (cursor - c) );
                }
            }
            catch ( InterruptedException var9 )
            {
                Thread.currentThread().interrupt();
            }
            catch ( Throwable var10 )
            {
                if ( WaitStrategy.isAlert( var10 ) )
                {
                    if ( this.parent.cancelled )
                    {
                        this.upstream.cancel();
                    }

                    return;
                }

                this.parent.onError( Operators.onOperatorError( var10, this.parent.currentContext() ) );
            }
        }
    }
}
