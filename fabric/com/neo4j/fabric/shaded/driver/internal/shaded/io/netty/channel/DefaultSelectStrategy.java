package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.IntSupplier;

final class DefaultSelectStrategy implements SelectStrategy
{
    static final SelectStrategy INSTANCE = new DefaultSelectStrategy();

    private DefaultSelectStrategy()
    {
    }

    public int calculateStrategy( IntSupplier selectSupplier, boolean hasTasks ) throws Exception
    {
        return hasTasks ? selectSupplier.get() : -1;
    }
}
