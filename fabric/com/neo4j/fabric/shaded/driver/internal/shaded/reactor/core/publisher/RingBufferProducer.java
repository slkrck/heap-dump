package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

abstract class RingBufferProducer
{
    static final AtomicReferenceFieldUpdater<RingBufferProducer,RingBuffer.Sequence[]> SEQUENCE_UPDATER =
            AtomicReferenceFieldUpdater.newUpdater( RingBufferProducer.class, RingBuffer.Sequence[].class, "gatingSequences" );
    final Runnable spinObserver;
    final int bufferSize;
    final WaitStrategy waitStrategy;
    final RingBuffer.Sequence cursor = RingBuffer.newSequence( -1L );
    volatile RingBuffer.Sequence[] gatingSequences = new RingBuffer.Sequence[0];

    RingBufferProducer( int bufferSize, WaitStrategy waitStrategy, @Nullable Runnable spinObserver )
    {
        this.spinObserver = spinObserver;
        this.bufferSize = bufferSize;
        this.waitStrategy = waitStrategy;
    }

    final long getCursor()
    {
        return this.cursor.getAsLong();
    }

    final int getBufferSize()
    {
        return this.bufferSize;
    }

    final void addGatingSequence( RingBuffer.Sequence gatingSequence )
    {
        RingBuffer.addSequence( this, SEQUENCE_UPDATER, gatingSequence );
    }

    boolean removeGatingSequence( RingBuffer.Sequence sequence )
    {
        return RingBuffer.removeSequence( this, SEQUENCE_UPDATER, sequence );
    }

    long getMinimumSequence( @Nullable RingBuffer.Sequence excludeSequence )
    {
        return RingBuffer.getMinimumSequence( excludeSequence, this.gatingSequences, this.cursor.getAsLong() );
    }

    RingBuffer.Reader newBarrier()
    {
        return new RingBuffer.Reader( this, this.waitStrategy, this.cursor );
    }

    abstract long getHighestPublishedSequence( long var1, long var3 );

    abstract long getPending();

    abstract long next();

    abstract long next( int var1 );

    abstract void publish( long var1 );

    RingBuffer.Sequence[] getGatingSequences()
    {
        return this.gatingSequences;
    }
}
