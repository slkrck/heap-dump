package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.Consumer;
import java.util.function.Function;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxOnBackpressureBuffer<O> extends InternalFluxOperator<O,O> implements Fuseable
{
    final Consumer<? super O> onOverflow;
    final int bufferSize;
    final boolean unbounded;

    FluxOnBackpressureBuffer( Flux<? extends O> source, int bufferSize, boolean unbounded, @Nullable Consumer<? super O> onOverflow )
    {
        super( source );
        if ( bufferSize < 1 )
        {
            throw new IllegalArgumentException( "Buffer Size must be strictly positive" );
        }
        else
        {
            this.bufferSize = bufferSize;
            this.unbounded = unbounded;
            this.onOverflow = onOverflow;
        }
    }

    public CoreSubscriber<? super O> subscribeOrReturn( CoreSubscriber<? super O> actual )
    {
        return new FluxOnBackpressureBuffer.BackpressureBufferSubscriber( actual, this.bufferSize, this.unbounded, this.onOverflow );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    static final class BackpressureBufferSubscriber<T> implements Fuseable.QueueSubscription<T>, InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxOnBackpressureBuffer.BackpressureBufferSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxOnBackpressureBuffer.BackpressureBufferSubscriber.class, "wip" );
        static final AtomicLongFieldUpdater<FluxOnBackpressureBuffer.BackpressureBufferSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxOnBackpressureBuffer.BackpressureBufferSubscriber.class, "requested" );
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        final Queue<T> queue;
        final int capacityOrSkip;
        final Consumer<? super T> onOverflow;
        Subscription s;
        volatile boolean cancelled;
        volatile boolean enabledFusion;
        volatile boolean done;
        Throwable error;
        volatile int wip;
        volatile long requested;

        BackpressureBufferSubscriber( CoreSubscriber<? super T> actual, int bufferSize, boolean unbounded, @Nullable Consumer<? super T> onOverflow )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.onOverflow = onOverflow;
            Queue q;
            if ( unbounded )
            {
                q = (Queue) Queues.unbounded( bufferSize ).get();
            }
            else
            {
                q = (Queue) Queues.get( bufferSize ).get();
            }

            if ( !unbounded && Queues.capacity( q ) > bufferSize )
            {
                this.capacityOrSkip = bufferSize;
            }
            else
            {
                this.capacityOrSkip = Integer.MAX_VALUE;
            }

            this.queue = q;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key != Scannable.Attr.TERMINATED )
            {
                if ( key == Scannable.Attr.CANCELLED )
                {
                    return this.cancelled;
                }
                else if ( key == Scannable.Attr.BUFFERED )
                {
                    return this.queue.size();
                }
                else if ( key == Scannable.Attr.ERROR )
                {
                    return this.error;
                }
                else if ( key == Scannable.Attr.PREFETCH )
                {
                    return Integer.MAX_VALUE;
                }
                else if ( key == Scannable.Attr.DELAY_ERROR )
                {
                    return true;
                }
                else
                {
                    return key == Scannable.Attr.CAPACITY ? this.capacityOrSkip == Integer.MAX_VALUE ? Queues.capacity( this.queue ) : this.capacityOrSkip
                                                          : InnerOperator.super.scanUnsafe( key );
                }
            }
            else
            {
                return this.done && this.queue.isEmpty();
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
            }
            else if ( (this.capacityOrSkip == Integer.MAX_VALUE || this.queue.size() < this.capacityOrSkip) && this.queue.offer( t ) )
            {
                this.drain();
            }
            else
            {
                Throwable ex = Operators.onOperatorError( this.s, Exceptions.failWithOverflow(), t, this.ctx );
                if ( this.onOverflow != null )
                {
                    try
                    {
                        this.onOverflow.accept( t );
                    }
                    catch ( Throwable var4 )
                    {
                        Exceptions.throwIfFatal( var4 );
                        ex.initCause( var4 );
                    }
                }

                Operators.onDiscard( t, this.ctx );
                this.onError( ex );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.error = t;
                this.done = true;
                this.drain();
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.drain();
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                int missed = 1;

                do
                {
                    Subscriber<? super T> a = this.actual;
                    if ( a != null )
                    {
                        if ( this.enabledFusion )
                        {
                            this.drainFused( a );
                        }
                        else
                        {
                            this.drainRegular( a );
                        }

                        return;
                    }

                    missed = WIP.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }

        void drainRegular( Subscriber<? super T> a )
        {
            int missed = 1;
            Queue q = this.queue;

            do
            {
                long r = this.requested;

                long e;
                for ( e = 0L; r != e; ++e )
                {
                    boolean d = this.done;
                    T t = q.poll();
                    boolean empty = t == null;
                    if ( this.checkTerminated( d, empty, a ) )
                    {
                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    a.onNext( t );
                }

                if ( r == e && this.checkTerminated( this.done, q.isEmpty(), a ) )
                {
                    return;
                }

                if ( e != 0L && r != Long.MAX_VALUE )
                {
                    REQUESTED.addAndGet( this, -e );
                }

                missed = WIP.addAndGet( this, -missed );
            }
            while ( missed != 0 );
        }

        void drainFused( Subscriber<? super T> a )
        {
            int missed = 1;
            Queue q = this.queue;

            while ( !this.cancelled )
            {
                boolean d = this.done;
                a.onNext( (Object) null );
                if ( d )
                {
                    Throwable ex = this.error;
                    if ( ex != null )
                    {
                        a.onError( ex );
                    }
                    else
                    {
                        a.onComplete();
                    }

                    return;
                }

                missed = WIP.addAndGet( this, -missed );
                if ( missed == 0 )
                {
                    return;
                }
            }

            this.s.cancel();
            Operators.onDiscardQueueWithClear( q, this.ctx, (Function) null );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.drain();
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                if ( !this.enabledFusion && WIP.getAndIncrement( this ) == 0 )
                {
                    Operators.onDiscardQueueWithClear( this.queue, this.ctx, (Function) null );
                }
            }
        }

        @Nullable
        public T poll()
        {
            return this.queue.poll();
        }

        public int size()
        {
            return this.queue.size();
        }

        public boolean isEmpty()
        {
            return this.queue.isEmpty();
        }

        public void clear()
        {
            Operators.onDiscardQueueWithClear( this.queue, this.ctx, (Function) null );
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 2) != 0 )
            {
                this.enabledFusion = true;
                return 2;
            }
            else
            {
                return 0;
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        boolean checkTerminated( boolean d, boolean empty, Subscriber<? super T> a )
        {
            if ( this.cancelled )
            {
                this.s.cancel();
                Operators.onDiscardQueueWithClear( this.queue, this.ctx, (Function) null );
                return true;
            }
            else if ( d && empty )
            {
                Throwable e = this.error;
                if ( e != null )
                {
                    a.onError( e );
                }
                else
                {
                    a.onComplete();
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
