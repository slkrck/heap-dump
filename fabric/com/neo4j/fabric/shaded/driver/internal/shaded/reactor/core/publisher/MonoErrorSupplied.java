package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.time.Duration;
import java.util.Objects;
import java.util.function.Supplier;

final class MonoErrorSupplied<T> extends Mono<T> implements Fuseable.ScalarCallable, SourceProducer<T>
{
    final Supplier<? extends Throwable> errorSupplier;

    MonoErrorSupplied( Supplier<? extends Throwable> errorSupplier )
    {
        this.errorSupplier = (Supplier) Objects.requireNonNull( errorSupplier, "errorSupplier" );
    }

    public T block( Duration m )
    {
        Throwable error = (Throwable) Objects.requireNonNull( this.errorSupplier.get(), "the errorSupplier returned null" );
        throw Exceptions.propagate( error );
    }

    public T block()
    {
        Throwable error = (Throwable) Objects.requireNonNull( this.errorSupplier.get(), "the errorSupplier returned null" );
        throw Exceptions.propagate( error );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Throwable error = (Throwable) Objects.requireNonNull( this.errorSupplier.get(), "the errorSupplier returned null" );
        Operators.error( actual, error );
    }

    public Object call() throws Exception
    {
        Throwable error = (Throwable) Objects.requireNonNull( this.errorSupplier.get(), "the errorSupplier returned null" );
        if ( error instanceof Exception )
        {
            throw (Exception) error;
        }
        else
        {
            throw Exceptions.propagate( error );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
