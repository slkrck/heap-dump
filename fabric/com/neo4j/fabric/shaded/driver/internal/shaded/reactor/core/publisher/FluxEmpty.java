package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

final class FluxEmpty extends Flux<Object> implements Fuseable.ScalarCallable<Object>, SourceProducer<Object>
{
    private static final Flux<Object> INSTANCE = new FluxEmpty();

    private FluxEmpty()
    {
    }

    public static <T> Flux<T> instance()
    {
        return INSTANCE;
    }

    public void subscribe( CoreSubscriber<? super Object> actual )
    {
        Operators.complete( actual );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    @Nullable
    public Object call() throws Exception
    {
        return null;
    }
}
