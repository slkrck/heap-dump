package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket;

import java.net.InetSocketAddress;

public interface SocketChannel extends DuplexChannel
{
    ServerSocketChannel parent();

    SocketChannelConfig config();

    InetSocketAddress localAddress();

    InetSocketAddress remoteAddress();
}
