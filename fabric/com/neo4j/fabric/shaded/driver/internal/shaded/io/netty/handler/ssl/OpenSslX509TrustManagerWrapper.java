package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.EmptyArrays;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivilegedAction;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.net.ssl.X509TrustManager;

final class OpenSslX509TrustManagerWrapper
{
    private static final InternalLogger LOGGER = InternalLoggerFactory.getInstance( OpenSslX509TrustManagerWrapper.class );
    private static final OpenSslX509TrustManagerWrapper.TrustManagerWrapper WRAPPER;

    static
    {
        OpenSslX509TrustManagerWrapper.TrustManagerWrapper wrapper = new OpenSslX509TrustManagerWrapper.TrustManagerWrapper()
        {
            public X509TrustManager wrapIfNeeded( X509TrustManager manager )
            {
                return manager;
            }
        };
        Throwable cause = null;
        Throwable unsafeCause = PlatformDependent.getUnsafeUnavailabilityCause();
        if ( unsafeCause == null )
        {
            final SSLContext context;
            try
            {
                context = newSSLContext();
                context.init( (KeyManager[]) null, new TrustManager[]{new X509TrustManager()
                {
                    public void checkClientTrusted( X509Certificate[] x509Certificates, String s ) throws CertificateException
                    {
                        throw new CertificateException();
                    }

                    public void checkServerTrusted( X509Certificate[] x509Certificates, String s ) throws CertificateException
                    {
                        throw new CertificateException();
                    }

                    public X509Certificate[] getAcceptedIssuers()
                    {
                        return EmptyArrays.EMPTY_X509_CERTIFICATES;
                    }
                }}, (SecureRandom) null );
            }
            catch ( Throwable var6 )
            {
                context = null;
                cause = var6;
            }

            if ( cause != null )
            {
                LOGGER.debug( "Unable to access wrapped TrustManager", cause );
            }
            else
            {
                Object maybeWrapper = AccessController.doPrivileged( new PrivilegedAction<Object>()
                {
                    public Object run()
                    {
                        try
                        {
                            Field contextSpiField = SSLContext.class.getDeclaredField( "contextSpi" );
                            long spiOffset = PlatformDependent.objectFieldOffset( contextSpiField );
                            Object spi = PlatformDependent.getObject( context, spiOffset );
                            if ( spi != null )
                            {
                                Class clazz = spi.getClass();

                                do
                                {
                                    try
                                    {
                                        Field trustManagerField = clazz.getDeclaredField( "trustManager" );
                                        long tmOffset = PlatformDependent.objectFieldOffset( trustManagerField );
                                        Object trustManager = PlatformDependent.getObject( spi, tmOffset );
                                        if ( trustManager instanceof X509ExtendedTrustManager )
                                        {
                                            return new OpenSslX509TrustManagerWrapper.UnsafeTrustManagerWrapper( spiOffset, tmOffset );
                                        }
                                    }
                                    catch ( NoSuchFieldException var10 )
                                    {
                                    }

                                    clazz = clazz.getSuperclass();
                                }
                                while ( clazz != null );
                            }

                            throw new NoSuchFieldException();
                        }
                        catch ( NoSuchFieldException var11 )
                        {
                            return var11;
                        }
                        catch ( SecurityException var12 )
                        {
                            return var12;
                        }
                    }
                } );
                if ( maybeWrapper instanceof Throwable )
                {
                    LOGGER.debug( "Unable to access wrapped TrustManager", (Throwable) maybeWrapper );
                }
                else
                {
                    wrapper = (OpenSslX509TrustManagerWrapper.TrustManagerWrapper) maybeWrapper;
                }
            }
        }
        else
        {
            LOGGER.debug( "Unable to access wrapped TrustManager", cause );
        }

        WRAPPER = wrapper;
    }

    private OpenSslX509TrustManagerWrapper()
    {
    }

    static X509TrustManager wrapIfNeeded( X509TrustManager trustManager )
    {
        return WRAPPER.wrapIfNeeded( trustManager );
    }

    private static SSLContext newSSLContext() throws NoSuchAlgorithmException
    {
        return SSLContext.getInstance( "TLS" );
    }

    private interface TrustManagerWrapper
    {
        X509TrustManager wrapIfNeeded( X509TrustManager var1 );
    }

    private static final class UnsafeTrustManagerWrapper implements OpenSslX509TrustManagerWrapper.TrustManagerWrapper
    {
        private final long spiOffset;
        private final long tmOffset;

        UnsafeTrustManagerWrapper( long spiOffset, long tmOffset )
        {
            this.spiOffset = spiOffset;
            this.tmOffset = tmOffset;
        }

        public X509TrustManager wrapIfNeeded( X509TrustManager manager )
        {
            if ( !(manager instanceof X509ExtendedTrustManager) )
            {
                try
                {
                    SSLContext ctx = OpenSslX509TrustManagerWrapper.newSSLContext();
                    ctx.init( (KeyManager[]) null, new TrustManager[]{manager}, (SecureRandom) null );
                    Object spi = PlatformDependent.getObject( ctx, this.spiOffset );
                    if ( spi != null )
                    {
                        Object tm = PlatformDependent.getObject( spi, this.tmOffset );
                        if ( tm instanceof X509ExtendedTrustManager )
                        {
                            return (X509TrustManager) tm;
                        }
                    }
                }
                catch ( NoSuchAlgorithmException var5 )
                {
                    PlatformDependent.throwException( var5 );
                }
                catch ( KeyManagementException var6 )
                {
                    PlatformDependent.throwException( var6 );
                }
            }

            return manager;
        }
    }
}
