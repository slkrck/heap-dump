package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

public interface DecoderResultProvider
{
    DecoderResult decoderResult();

    void setDecoderResult( DecoderResult var1 );
}
