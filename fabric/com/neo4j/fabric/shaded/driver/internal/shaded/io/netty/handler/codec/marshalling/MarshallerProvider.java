package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.marshalling;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import org.jboss.marshalling.Marshaller;

public interface MarshallerProvider
{
    Marshaller getMarshaller( ChannelHandlerContext var1 ) throws Exception;
}
