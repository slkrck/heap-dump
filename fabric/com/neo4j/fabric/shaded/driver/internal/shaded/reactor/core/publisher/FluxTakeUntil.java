package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.Predicate;

import org.reactivestreams.Subscription;

final class FluxTakeUntil<T> extends InternalFluxOperator<T,T>
{
    final Predicate<? super T> predicate;

    FluxTakeUntil( Flux<? extends T> source, Predicate<? super T> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxTakeUntil.TakeUntilPredicateSubscriber( actual, this.predicate );
    }

    static final class TakeUntilPredicateSubscriber<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        final Predicate<? super T> predicate;
        Subscription s;
        boolean done;

        TakeUntilPredicateSubscriber( CoreSubscriber<? super T> actual, Predicate<? super T> predicate )
        {
            this.actual = actual;
            this.predicate = predicate;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.actual.onNext( t );

                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var4 )
                {
                    this.onError( Operators.onOperatorError( this.s, var4, t, this.actual.currentContext() ) );
                    return;
                }

                if ( b )
                {
                    this.s.cancel();
                    this.onComplete();
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
