package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.protobuf;

import com.google.protobuf.nano.MessageNano;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToMessageDecoder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.util.List;

@ChannelHandler.Sharable
public class ProtobufDecoderNano extends MessageToMessageDecoder<ByteBuf>
{
    private final Class<? extends MessageNano> clazz;

    public ProtobufDecoderNano( Class<? extends MessageNano> clazz )
    {
        this.clazz = (Class) ObjectUtil.checkNotNull( clazz, "You must provide a Class" );
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out ) throws Exception
    {
        int length = msg.readableBytes();
        byte[] array;
        int offset;
        if ( msg.hasArray() )
        {
            array = msg.array();
            offset = msg.arrayOffset() + msg.readerIndex();
        }
        else
        {
            array = ByteBufUtil.getBytes( msg, msg.readerIndex(), length, false );
            offset = 0;
        }

        MessageNano prototype = (MessageNano) this.clazz.getConstructor().newInstance();
        out.add( MessageNano.mergeFrom( prototype, array, offset, length ) );
    }
}
