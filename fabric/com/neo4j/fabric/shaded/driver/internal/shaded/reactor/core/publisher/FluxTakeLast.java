package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.ArrayDeque;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.BooleanSupplier;

import org.reactivestreams.Subscription;

final class FluxTakeLast<T> extends InternalFluxOperator<T,T>
{
    final int n;

    FluxTakeLast( Flux<? extends T> source, int n )
    {
        super( source );
        if ( n < 0 )
        {
            throw new IllegalArgumentException( "n >= required but it was " + n );
        }
        else
        {
            this.n = n;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return (CoreSubscriber) (this.n == 0 ? new FluxTakeLast.TakeLastZeroSubscriber( actual ) : new FluxTakeLast.TakeLastManySubscriber( actual, this.n ));
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    static final class TakeLastManySubscriber<T> extends ArrayDeque<T> implements BooleanSupplier, InnerOperator<T,T>
    {
        static final AtomicLongFieldUpdater<FluxTakeLast.TakeLastManySubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxTakeLast.TakeLastManySubscriber.class, "requested" );
        final CoreSubscriber<? super T> actual;
        final int n;
        volatile boolean cancelled;
        Subscription s;
        volatile long requested;

        TakeLastManySubscriber( CoreSubscriber<? super T> actual, int n )
        {
            this.actual = actual;
            this.n = n;
        }

        public boolean getAsBoolean()
        {
            return this.cancelled;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                DrainUtils.postCompleteRequest( n, this.actual, this, REQUESTED, this, this );
            }
        }

        public void cancel()
        {
            this.cancelled = true;
            this.s.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.size() == this.n )
            {
                this.poll();
            }

            this.offer( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            DrainUtils.postComplete( this.actual, this, REQUESTED, this, this );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.size() : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }
    }

    static final class TakeLastZeroSubscriber<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        Subscription s;

        TakeLastZeroSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
