package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Processor;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

public final class MonoProcessor<O> extends Mono<O> implements Processor<O,O>, CoreSubscriber<O>, Disposable, Subscription, Scannable
{
    static final AtomicReferenceFieldUpdater<MonoProcessor,MonoProcessor.NextInner[]> SUBSCRIBERS =
            AtomicReferenceFieldUpdater.newUpdater( MonoProcessor.class, MonoProcessor.NextInner[].class, "subscribers" );
    static final MonoProcessor.NextInner[] EMPTY = new MonoProcessor.NextInner[0];
    static final MonoProcessor.NextInner[] TERMINATED = new MonoProcessor.NextInner[0];
    static final MonoProcessor.NextInner[] EMPTY_WITH_SOURCE = new MonoProcessor.NextInner[0];
    static final AtomicReferenceFieldUpdater<MonoProcessor,Subscription> UPSTREAM =
            AtomicReferenceFieldUpdater.newUpdater( MonoProcessor.class, Subscription.class, "subscription" );
    volatile MonoProcessor.NextInner<O>[] subscribers;
    CorePublisher<? extends O> source;
    Throwable error;
    O value;
    volatile Subscription subscription;

    MonoProcessor( @Nullable CorePublisher<? extends O> source )
    {
        this.source = source;
        SUBSCRIBERS.lazySet( this, source != null ? EMPTY_WITH_SOURCE : EMPTY );
    }

    public static <T> MonoProcessor<T> create()
    {
        return new MonoProcessor( (CorePublisher) null );
    }

    public final void cancel()
    {
        if ( !this.isTerminated() )
        {
            Subscription s = (Subscription) UPSTREAM.getAndSet( this, Operators.cancelledSubscription() );
            if ( s != Operators.cancelledSubscription() )
            {
                this.source = null;
                if ( s != null )
                {
                    s.cancel();
                }
            }
        }
    }

    public void dispose()
    {
        Subscription s = (Subscription) UPSTREAM.getAndSet( this, Operators.cancelledSubscription() );
        if ( s != Operators.cancelledSubscription() )
        {
            this.source = null;
            if ( s != null )
            {
                s.cancel();
            }

            MonoProcessor.NextInner[] a;
            if ( (a = (MonoProcessor.NextInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED )) != TERMINATED )
            {
                Exception e = new CancellationException( "Disposed" );
                this.error = e;
                this.value = null;
                MonoProcessor.NextInner[] var4 = a;
                int var5 = a.length;

                for ( int var6 = 0; var6 < var5; ++var6 )
                {
                    MonoProcessor.NextInner<O> as = var4[var6];
                    as.onError( e );
                }
            }
        }
    }

    @Nullable
    public O block()
    {
        return this.block( (Duration) null );
    }

    @Nullable
    public O block( @Nullable Duration timeout )
    {
        try
        {
            if ( !this.isPending() )
            {
                return this.peek();
            }
            else
            {
                this.connect();
                long delay;
                if ( null == timeout )
                {
                    delay = 0L;
                }
                else
                {
                    delay = System.nanoTime() + timeout.toNanos();
                }

                while ( true )
                {
                    MonoProcessor.NextInner<O>[] inners = this.subscribers;
                    if ( inners == TERMINATED )
                    {
                        if ( this.error != null )
                        {
                            RuntimeException re = Exceptions.propagate( this.error );
                            re = Exceptions.addSuppressed( (RuntimeException) re, new Exception( "Mono#block terminated with an error" ) );
                            throw re;
                        }

                        if ( this.value == null )
                        {
                            return null;
                        }

                        return this.value;
                    }

                    if ( timeout != null && delay < System.nanoTime() )
                    {
                        this.cancel();
                        throw new IllegalStateException( "Timeout on Mono blocking read" );
                    }

                    Thread.sleep( 1L );
                }
            }
        }
        catch ( InterruptedException var6 )
        {
            Thread.currentThread().interrupt();
            throw new IllegalStateException( "Thread Interruption on Mono blocking read" );
        }
    }

    @Nullable
    public final Throwable getError()
    {
        return this.isTerminated() ? this.error : null;
    }

    public boolean isCancelled()
    {
        return this.subscription == Operators.cancelledSubscription() && !this.isTerminated();
    }

    public final boolean isError()
    {
        return this.getError() != null;
    }

    public final boolean isSuccess()
    {
        return this.isTerminated() && this.error == null;
    }

    public final boolean isTerminated()
    {
        return this.subscribers == TERMINATED;
    }

    public boolean isDisposed()
    {
        return this.isTerminated() || this.isCancelled();
    }

    public final void onComplete()
    {
        this.onNext( (Object) null );
    }

    public final void onError( Throwable cause )
    {
        Objects.requireNonNull( cause, "onError cannot be null" );
        if ( UPSTREAM.getAndSet( this, Operators.cancelledSubscription() ) == Operators.cancelledSubscription() )
        {
            Operators.onErrorDroppedMulticast( cause );
        }
        else
        {
            this.error = cause;
            this.value = null;
            this.source = null;
            MonoProcessor.NextInner[] var2 = (MonoProcessor.NextInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
            int var3 = var2.length;

            for ( int var4 = 0; var4 < var3; ++var4 )
            {
                MonoProcessor.NextInner<O> as = var2[var4];
                as.onError( cause );
            }
        }
    }

    public final void onNext( @Nullable O value )
    {
        Subscription s;
        if ( (s = (Subscription) UPSTREAM.getAndSet( this, Operators.cancelledSubscription() )) == Operators.cancelledSubscription() )
        {
            if ( value != null )
            {
                Operators.onNextDroppedMulticast( value );
            }
        }
        else
        {
            this.value = value;
            Publisher<? extends O> parent = this.source;
            this.source = null;
            MonoProcessor.NextInner<O>[] array = (MonoProcessor.NextInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
            MonoProcessor.NextInner[] var5;
            int var6;
            int var7;
            MonoProcessor.NextInner as;
            if ( value == null )
            {
                var5 = array;
                var6 = array.length;

                for ( var7 = 0; var7 < var6; ++var7 )
                {
                    as = var5[var7];
                    as.onComplete();
                }
            }
            else
            {
                if ( s != null && !(parent instanceof Mono) )
                {
                    s.cancel();
                }

                var5 = array;
                var6 = array.length;

                for ( var7 = 0; var7 < var6; ++var7 )
                {
                    as = var5[var7];
                    as.complete( value );
                }
            }
        }
    }

    public final void onSubscribe( Subscription subscription )
    {
        if ( Operators.setOnce( UPSTREAM, this, subscription ) )
        {
            subscription.request( Long.MAX_VALUE );
        }
    }

    public Stream<? extends Scannable> inners()
    {
        return Stream.of( this.subscribers );
    }

    @Nullable
    public O peek()
    {
        if ( !this.isTerminated() )
        {
            return null;
        }
        else if ( this.value != null )
        {
            return this.value;
        }
        else if ( this.error != null )
        {
            RuntimeException re = Exceptions.propagate( this.error );
            re = Exceptions.addSuppressed( (RuntimeException) re, new Exception( "Mono#peek terminated with an error" ) );
            throw re;
        }
        else
        {
            return null;
        }
    }

    public final void request( long n )
    {
        Operators.validate( n );
    }

    public void subscribe( CoreSubscriber<? super O> actual )
    {
        MonoProcessor.NextInner<O> as = new MonoProcessor.NextInner( actual, this );
        actual.onSubscribe( as );
        if ( this.add( as ) )
        {
            if ( as.isCancelled() )
            {
                this.remove( as );
            }
        }
        else
        {
            Throwable ex = this.error;
            if ( ex != null )
            {
                actual.onError( ex );
            }
            else
            {
                O v = this.value;
                if ( v != null )
                {
                    as.complete( v );
                }
                else
                {
                    as.onComplete();
                }
            }
        }
    }

    void connect()
    {
        Publisher<? extends O> parent = this.source;
        if ( parent != null && SUBSCRIBERS.compareAndSet( this, EMPTY_WITH_SOURCE, EMPTY ) )
        {
            parent.subscribe( this );
        }
    }

    public Context currentContext()
    {
        return Operators.multiSubscribersContext( this.subscribers );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        boolean t = this.isTerminated();
        if ( key == Scannable.Attr.TERMINATED )
        {
            return t;
        }
        else if ( key == Scannable.Attr.PARENT )
        {
            return this.subscription;
        }
        else if ( key == Scannable.Attr.ERROR )
        {
            return this.error;
        }
        else if ( key == Scannable.Attr.PREFETCH )
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return key == Scannable.Attr.CANCELLED ? this.isCancelled() : null;
        }
    }

    final boolean isPending()
    {
        return !this.isTerminated();
    }

    public final long downstreamCount()
    {
        return (long) this.subscribers.length;
    }

    public final boolean hasDownstreams()
    {
        return this.downstreamCount() != 0L;
    }

    boolean add( MonoProcessor.NextInner<O> ps )
    {
        MonoProcessor.NextInner[] a;
        MonoProcessor.NextInner[] b;
        do
        {
            a = this.subscribers;
            if ( a == TERMINATED )
            {
                return false;
            }

            int n = a.length;
            b = new MonoProcessor.NextInner[n + 1];
            System.arraycopy( a, 0, b, 0, n );
            b[n] = ps;
        }
        while ( !SUBSCRIBERS.compareAndSet( this, a, b ) );

        Publisher<? extends O> parent = this.source;
        if ( parent != null && a == EMPTY_WITH_SOURCE )
        {
            parent.subscribe( this );
        }

        return true;
    }

    void remove( MonoProcessor.NextInner<O> ps )
    {
        MonoProcessor.NextInner[] a;
        MonoProcessor.NextInner[] b;
        do
        {
            a = this.subscribers;
            int n = a.length;
            if ( n == 0 )
            {
                return;
            }

            int j = -1;

            for ( int i = 0; i < n; ++i )
            {
                if ( a[i] == ps )
                {
                    j = i;
                    break;
                }
            }

            if ( j < 0 )
            {
                return;
            }

            if ( n == 1 )
            {
                b = EMPTY;
            }
            else
            {
                b = new MonoProcessor.NextInner[n - 1];
                System.arraycopy( a, 0, b, 0, j );
                System.arraycopy( a, j + 1, b, j, n - j - 1 );
            }
        }
        while ( !SUBSCRIBERS.compareAndSet( this, a, b ) );
    }

    static final class NextInner<T> extends Operators.MonoSubscriber<T,T>
    {
        final MonoProcessor<T> parent;

        NextInner( CoreSubscriber<? super T> actual, MonoProcessor<T> parent )
        {
            super( actual );
            this.parent = parent;
        }

        public void cancel()
        {
            if ( STATE.getAndSet( this, 4 ) != 4 )
            {
                this.parent.remove( this );
            }
        }

        public void onComplete()
        {
            if ( !this.isCancelled() )
            {
                this.actual.onComplete();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.isCancelled() )
            {
                Operators.onOperatorError( t, this.currentContext() );
            }
            else
            {
                this.actual.onError( t );
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.parent : super.scanUnsafe( key );
        }
    }
}
