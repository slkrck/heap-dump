package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxSkipUntilOther<T, U> extends InternalFluxOperator<T,T>
{
    final Publisher<U> other;

    FluxSkipUntilOther( Flux<? extends T> source, Publisher<U> other )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxSkipUntilOther.SkipUntilMainSubscriber<T> mainSubscriber = new FluxSkipUntilOther.SkipUntilMainSubscriber( actual );
        FluxSkipUntilOther.SkipUntilOtherSubscriber<U> otherSubscriber = new FluxSkipUntilOther.SkipUntilOtherSubscriber( mainSubscriber );
        this.other.subscribe( otherSubscriber );
        return mainSubscriber;
    }

    static final class SkipUntilMainSubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicReferenceFieldUpdater<FluxSkipUntilOther.SkipUntilMainSubscriber,Subscription> MAIN =
                AtomicReferenceFieldUpdater.newUpdater( FluxSkipUntilOther.SkipUntilMainSubscriber.class, Subscription.class, "main" );
        static final AtomicReferenceFieldUpdater<FluxSkipUntilOther.SkipUntilMainSubscriber,Subscription> OTHER =
                AtomicReferenceFieldUpdater.newUpdater( FluxSkipUntilOther.SkipUntilMainSubscriber.class, Subscription.class, "other" );
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        volatile Subscription main;
        volatile Subscription other;
        volatile boolean gate;

        SkipUntilMainSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = Operators.serialize( actual );
            this.ctx = actual.currentContext();
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.main;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.main == Operators.cancelledSubscription() : InnerOperator.super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( Scannable.from( this.other ) );
        }

        void setOther( Subscription s )
        {
            if ( !OTHER.compareAndSet( this, (Object) null, s ) )
            {
                s.cancel();
                if ( this.other != Operators.cancelledSubscription() )
                {
                    Operators.reportSubscriptionSet();
                }
            }
        }

        public void request( long n )
        {
            this.main.request( n );
        }

        public void cancel()
        {
            Operators.terminate( MAIN, this );
            Operators.terminate( OTHER, this );
        }

        public void onSubscribe( Subscription s )
        {
            if ( !MAIN.compareAndSet( this, (Object) null, s ) )
            {
                s.cancel();
                if ( this.main != Operators.cancelledSubscription() )
                {
                    Operators.reportSubscriptionSet();
                }
            }
            else
            {
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.gate )
            {
                this.actual.onNext( t );
            }
            else
            {
                Operators.onDiscard( t, this.ctx );
                this.main.request( 1L );
            }
        }

        public void onError( Throwable t )
        {
            if ( MAIN.compareAndSet( this, (Object) null, Operators.cancelledSubscription() ) )
            {
                Operators.error( this.actual, t );
            }
            else if ( this.main == Operators.cancelledSubscription() )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.cancel();
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            Operators.terminate( OTHER, this );
            this.actual.onComplete();
        }
    }

    static final class SkipUntilOtherSubscriber<U> implements InnerConsumer<U>
    {
        final FluxSkipUntilOther.SkipUntilMainSubscriber<?> main;

        SkipUntilOtherSubscriber( FluxSkipUntilOther.SkipUntilMainSubscriber<?> main )
        {
            this.main = main;
        }

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.main.other == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.main.other;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL ? this.main : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            this.main.setOther( s );
            s.request( Long.MAX_VALUE );
        }

        public void onNext( U t )
        {
            if ( !this.main.gate )
            {
                FluxSkipUntilOther.SkipUntilMainSubscriber<?> m = this.main;
                m.other.cancel();
                m.gate = true;
                m.other = Operators.cancelledSubscription();
            }
        }

        public void onError( Throwable t )
        {
            FluxSkipUntilOther.SkipUntilMainSubscriber<?> m = this.main;
            if ( m.gate )
            {
                Operators.onErrorDropped( t, this.main.currentContext() );
            }
            else
            {
                m.onError( t );
            }
        }

        public void onComplete()
        {
            FluxSkipUntilOther.SkipUntilMainSubscriber<?> m = this.main;
            if ( !m.gate )
            {
                m.gate = true;
                m.other = Operators.cancelledSubscription();
            }
        }
    }
}
