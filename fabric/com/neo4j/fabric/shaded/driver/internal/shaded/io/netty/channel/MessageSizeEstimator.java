package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

public interface MessageSizeEstimator
{
    MessageSizeEstimator.Handle newHandle();

    public interface Handle
    {
        int size( Object var1 );
    }
}
