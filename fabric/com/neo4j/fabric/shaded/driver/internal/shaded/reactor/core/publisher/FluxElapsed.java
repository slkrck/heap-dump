package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuples;

import java.util.concurrent.TimeUnit;

import org.reactivestreams.Subscription;

final class FluxElapsed<T> extends InternalFluxOperator<T,Tuple2<Long,T>> implements Fuseable
{
    final Scheduler scheduler;

    FluxElapsed( Flux<T> source, Scheduler scheduler )
    {
        super( source );
        this.scheduler = scheduler;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Tuple2<Long,T>> actual )
    {
        return new FluxElapsed.ElapsedSubscriber( actual, this.scheduler );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }

    static final class ElapsedSubscriber<T> implements InnerOperator<T,Tuple2<Long,T>>, Fuseable.QueueSubscription<Tuple2<Long,T>>
    {
        final CoreSubscriber<? super Tuple2<Long,T>> actual;
        final Scheduler scheduler;
        Subscription s;
        Fuseable.QueueSubscription<T> qs;
        long lastTime;

        ElapsedSubscriber( CoreSubscriber<? super Tuple2<Long,T>> actual, Scheduler scheduler )
        {
            this.actual = actual;
            this.scheduler = scheduler;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.scheduler : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.lastTime = this.scheduler.now( TimeUnit.MILLISECONDS );
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public CoreSubscriber<? super Tuple2<Long,T>> actual()
        {
            return this.actual;
        }

        public void onNext( T t )
        {
            if ( t == null )
            {
                this.actual.onNext( (Object) null );
            }
            else
            {
                this.actual.onNext( this.snapshot( t ) );
            }
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }

        public int requestFusion( int requestedMode )
        {
            Fuseable.QueueSubscription<T> qs = Operators.as( this.s );
            if ( qs != null )
            {
                this.qs = qs;
                return qs.requestFusion( requestedMode );
            }
            else
            {
                return 0;
            }
        }

        Tuple2<Long,T> snapshot( T data )
        {
            long now = this.scheduler.now( TimeUnit.MILLISECONDS );
            long last = this.lastTime;
            this.lastTime = now;
            long delta = now - last;
            return Tuples.of( delta, data );
        }

        @Nullable
        public Tuple2<Long,T> poll()
        {
            T data = this.qs.poll();
            return data != null ? this.snapshot( data ) : null;
        }

        public int size()
        {
            return this.qs.size();
        }

        public boolean isEmpty()
        {
            return this.qs.isEmpty();
        }

        public void clear()
        {
            this.qs.clear();
        }
    }
}
