package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class FluxDoFirst<T> extends InternalFluxOperator<T,T>
{
    final Runnable onFirst;

    FluxDoFirst( Flux<? extends T> source, Runnable onFirst )
    {
        super( source );
        this.onFirst = onFirst;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        try
        {
            this.onFirst.run();
            return actual;
        }
        catch ( Throwable var3 )
        {
            Operators.error( actual, var3 );
            return null;
        }
    }
}
