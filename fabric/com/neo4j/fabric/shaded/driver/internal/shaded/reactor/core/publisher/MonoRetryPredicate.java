package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.Predicate;

final class MonoRetryPredicate<T> extends InternalMonoOperator<T,T>
{
    final Predicate<? super Throwable> predicate;

    MonoRetryPredicate( Mono<? extends T> source, Predicate<? super Throwable> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRetryPredicate.RetryPredicateSubscriber<T> parent = new FluxRetryPredicate.RetryPredicateSubscriber( this.source, actual, this.predicate );
        actual.onSubscribe( parent );
        if ( !parent.isCancelled() )
        {
            parent.resubscribe();
        }

        return null;
    }
}
