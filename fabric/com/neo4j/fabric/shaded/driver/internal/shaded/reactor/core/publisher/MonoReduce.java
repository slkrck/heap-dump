package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.BiFunction;

import org.reactivestreams.Subscription;

final class MonoReduce<T> extends MonoFromFluxOperator<T,T> implements Fuseable
{
    final BiFunction<T,T,T> aggregator;

    MonoReduce( Flux<? extends T> source, BiFunction<T,T,T> aggregator )
    {
        super( source );
        this.aggregator = (BiFunction) Objects.requireNonNull( aggregator, "aggregator" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoReduce.ReduceSubscriber( actual, this.aggregator );
    }

    static final class ReduceSubscriber<T> extends Operators.MonoSubscriber<T,T>
    {
        final BiFunction<T,T,T> aggregator;
        Subscription s;
        boolean done;

        ReduceSubscriber( CoreSubscriber<? super T> actual, BiFunction<T,T,T> aggregator )
        {
            super( actual );
            this.aggregator = aggregator;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                T r = this.value;
                if ( r == null )
                {
                    this.value = t;
                }
                else
                {
                    try
                    {
                        r = Objects.requireNonNull( this.aggregator.apply( r, t ), "The aggregator returned a null value" );
                    }
                    catch ( Throwable var5 )
                    {
                        this.done = true;
                        Context ctx = this.actual.currentContext();
                        Operators.onDiscard( t, ctx );
                        Operators.onDiscard( this.value, ctx );
                        this.value = null;
                        this.actual.onError( Operators.onOperatorError( this.s, var5, t, this.actual.currentContext() ) );
                        return;
                    }

                    this.value = r;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                Operators.onDiscard( this.value, this.actual.currentContext() );
                this.value = null;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                T r = this.value;
                if ( r != null )
                {
                    this.complete( r );
                }
                else
                {
                    this.actual.onComplete();
                }
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }
    }
}
