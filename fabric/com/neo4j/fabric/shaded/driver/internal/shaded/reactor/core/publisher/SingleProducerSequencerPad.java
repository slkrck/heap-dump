package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

abstract class SingleProducerSequencerPad extends RingBufferProducer
{
    protected long p1;
    protected long p2;
    protected long p3;
    protected long p4;
    protected long p5;
    protected long p6;
    protected long p7;

    SingleProducerSequencerPad( int bufferSize, WaitStrategy waitStrategy, @Nullable Runnable spinObserver )
    {
        super( bufferSize, waitStrategy, spinObserver );
    }
}
