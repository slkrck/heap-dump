package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Objects;
import java.util.function.Predicate;

final class MonoFilter<T> extends InternalMonoOperator<T,T>
{
    final Predicate<? super T> predicate;

    MonoFilter( Mono<? extends T> source, Predicate<? super T> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return (CoreSubscriber) (actual instanceof Fuseable.ConditionalSubscriber ? new FluxFilter.FilterConditionalSubscriber(
                (Fuseable.ConditionalSubscriber) actual, this.predicate ) : new FluxFilter.FilterSubscriber( actual, this.predicate ));
    }
}
