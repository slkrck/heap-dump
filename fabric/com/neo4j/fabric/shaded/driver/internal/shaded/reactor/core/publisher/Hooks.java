package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Logger;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Loggers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import org.reactivestreams.Publisher;

public abstract class Hooks
{
    static final Logger log = Loggers.getLogger( Hooks.class );
    static final String KEY_ON_ERROR_DROPPED = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.onErrorDropped.local";
    static final String KEY_ON_NEXT_DROPPED = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.onNextDropped.local";
    static final String KEY_ON_OPERATOR_ERROR = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.onOperatorError.local";
    static final String KEY_ON_DISCARD = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.onDiscard.local";
    static final String KEY_ON_REJECTED_EXECUTION = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.onRejectedExecution.local";
    private static final LinkedHashMap<String,Function<? super Publisher<Object>,? extends Publisher<Object>>> onEachOperatorHooks = new LinkedHashMap( 1 );
    private static final LinkedHashMap<String,Function<? super Publisher<Object>,? extends Publisher<Object>>> onLastOperatorHooks = new LinkedHashMap( 1 );
    private static final LinkedHashMap<String,BiFunction<? super Throwable,Object,? extends Throwable>> onOperatorErrorHooks = new LinkedHashMap( 1 );
    static Function<Publisher,Publisher> onEachOperatorHook;
    static volatile Function<Publisher,Publisher> onLastOperatorHook;
    static volatile BiFunction<? super Throwable,Object,? extends Throwable> onOperatorErrorHook;
    static volatile Consumer<? super Throwable> onErrorDroppedHook;
    static volatile Consumer<Object> onNextDroppedHook;
    static volatile OnNextFailureStrategy onNextErrorHook;
    static boolean GLOBAL_TRACE =
            Boolean.parseBoolean( System.getProperty( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.trace.operatorStacktrace", "false" ) );

    Hooks()
    {
    }

    public static void onEachOperator( Function<? super Publisher<Object>,? extends Publisher<Object>> onEachOperator )
    {
        onEachOperator( onEachOperator.toString(), onEachOperator );
    }

    public static void onEachOperator( String key, Function<? super Publisher<Object>,? extends Publisher<Object>> onEachOperator )
    {
        Objects.requireNonNull( key, "key" );
        Objects.requireNonNull( onEachOperator, "onEachOperator" );
        log.debug( "Hooking onEachOperator: {}", key );
        synchronized ( log )
        {
            onEachOperatorHooks.put( key, onEachOperator );
            onEachOperatorHook = createOrUpdateOpHook( onEachOperatorHooks.values() );
        }
    }

    public static void resetOnEachOperator( String key )
    {
        Objects.requireNonNull( key, "key" );
        log.debug( "Reset onEachOperator: {}", key );
        synchronized ( log )
        {
            onEachOperatorHooks.remove( key );
            onEachOperatorHook = createOrUpdateOpHook( onEachOperatorHooks.values() );
        }
    }

    public static void resetOnEachOperator()
    {
        log.debug( "Reset to factory defaults : onEachOperator" );
        synchronized ( log )
        {
            onEachOperatorHooks.clear();
            onEachOperatorHook = null;
        }
    }

    public static void onErrorDropped( Consumer<? super Throwable> c )
    {
        Objects.requireNonNull( c, "onErrorDroppedHook" );
        log.debug( "Hooking new default : onErrorDropped" );
        synchronized ( log )
        {
            if ( onErrorDroppedHook != null )
            {
                Consumer<Throwable> _c = onErrorDroppedHook.andThen( c );
                onErrorDroppedHook = _c;
            }
            else
            {
                onErrorDroppedHook = c;
            }
        }
    }

    public static void onLastOperator( Function<? super Publisher<Object>,? extends Publisher<Object>> onLastOperator )
    {
        onLastOperator( onLastOperator.toString(), onLastOperator );
    }

    public static void onLastOperator( String key, Function<? super Publisher<Object>,? extends Publisher<Object>> onLastOperator )
    {
        Objects.requireNonNull( key, "key" );
        Objects.requireNonNull( onLastOperator, "onLastOperator" );
        log.debug( "Hooking onLastOperator: {}", key );
        synchronized ( log )
        {
            onLastOperatorHooks.put( key, onLastOperator );
            onLastOperatorHook = createOrUpdateOpHook( onLastOperatorHooks.values() );
        }
    }

    public static void resetOnLastOperator( String key )
    {
        Objects.requireNonNull( key, "key" );
        log.debug( "Reset onLastOperator: {}", key );
        synchronized ( log )
        {
            onLastOperatorHooks.remove( key );
            onLastOperatorHook = createOrUpdateOpHook( onLastOperatorHooks.values() );
        }
    }

    public static void resetOnLastOperator()
    {
        log.debug( "Reset to factory defaults : onLastOperator" );
        synchronized ( log )
        {
            onLastOperatorHooks.clear();
            onLastOperatorHook = null;
        }
    }

    public static void onNextDropped( Consumer<Object> c )
    {
        Objects.requireNonNull( c, "onNextDroppedHook" );
        log.debug( "Hooking new default : onNextDropped" );
        synchronized ( log )
        {
            if ( onNextDroppedHook != null )
            {
                onNextDroppedHook = onNextDroppedHook.andThen( c );
            }
            else
            {
                onNextDroppedHook = c;
            }
        }
    }

    public static void onNextDroppedFail()
    {
        log.debug( "Enabling failure mode for onNextDropped" );
        synchronized ( log )
        {
            onNextDroppedHook = ( n ) -> {
                throw Exceptions.failWithCancel();
            };
        }
    }

    public static void onOperatorDebug()
    {
        log.debug( "Enabling stacktrace debugging via onOperatorDebug" );
        GLOBAL_TRACE = true;
    }

    public static void resetOnOperatorDebug()
    {
        GLOBAL_TRACE = false;
    }

    public static void onNextError( BiFunction<? super Throwable,Object,? extends Throwable> onNextError )
    {
        Objects.requireNonNull( onNextError, "onNextError" );
        log.debug( "Hooking new default : onNextError" );
        if ( onNextError instanceof OnNextFailureStrategy )
        {
            synchronized ( log )
            {
                onNextErrorHook = (OnNextFailureStrategy) onNextError;
            }
        }
        else
        {
            synchronized ( log )
            {
                onNextErrorHook = new OnNextFailureStrategy.LambdaOnNextErrorStrategy( onNextError );
            }
        }
    }

    public static void onOperatorError( BiFunction<? super Throwable,Object,? extends Throwable> onOperatorError )
    {
        onOperatorError( onOperatorError.toString(), onOperatorError );
    }

    public static void onOperatorError( String key, BiFunction<? super Throwable,Object,? extends Throwable> onOperatorError )
    {
        Objects.requireNonNull( key, "key" );
        Objects.requireNonNull( onOperatorError, "onOperatorError" );
        log.debug( "Hooking onOperatorError: {}", key );
        synchronized ( log )
        {
            onOperatorErrorHooks.put( key, onOperatorError );
            onOperatorErrorHook = createOrUpdateOpErrorHook( onOperatorErrorHooks.values() );
        }
    }

    public static void resetOnOperatorError( String key )
    {
        Objects.requireNonNull( key, "key" );
        log.debug( "Reset onOperatorError: {}", key );
        synchronized ( log )
        {
            onOperatorErrorHooks.remove( key );
            onOperatorErrorHook = createOrUpdateOpErrorHook( onOperatorErrorHooks.values() );
        }
    }

    public static void resetOnOperatorError()
    {
        log.debug( "Reset to factory defaults : onOperatorError" );
        synchronized ( log )
        {
            onOperatorErrorHooks.clear();
            onOperatorErrorHook = null;
        }
    }

    public static void resetOnErrorDropped()
    {
        log.debug( "Reset to factory defaults : onErrorDropped" );
        synchronized ( log )
        {
            onErrorDroppedHook = null;
        }
    }

    public static void resetOnNextDropped()
    {
        log.debug( "Reset to factory defaults : onNextDropped" );
        synchronized ( log )
        {
            onNextDroppedHook = null;
        }
    }

    public static void resetOnNextError()
    {
        log.debug( "Reset to factory defaults : onNextError" );
        synchronized ( log )
        {
            onNextErrorHook = null;
        }
    }

    @Nullable
    static Function<Publisher,Publisher> createOrUpdateOpHook( Collection<Function<? super Publisher<Object>,? extends Publisher<Object>>> hooks )
    {
        Function<Publisher,Publisher> composite = null;
        Iterator var2 = hooks.iterator();

        while ( var2.hasNext() )
        {
            Function<? super Publisher<Object>,? extends Publisher<Object>> function = (Function) var2.next();
            if ( composite != null )
            {
                composite = composite.andThen( function );
            }
            else
            {
                composite = function;
            }
        }

        return composite;
    }

    @Nullable
    static BiFunction<? super Throwable,Object,? extends Throwable> createOrUpdateOpErrorHook(
            Collection<BiFunction<? super Throwable,Object,? extends Throwable>> hooks )
    {
        BiFunction<? super Throwable,Object,? extends Throwable> composite = null;
        Iterator var2 = hooks.iterator();

        while ( var2.hasNext() )
        {
            BiFunction<? super Throwable,Object,? extends Throwable> function = (BiFunction) var2.next();
            if ( composite != null )
            {
                composite = ( e, data ) -> {
                    return (Throwable) function.apply( composite.apply( e, data ), data );
                };
            }
            else
            {
                composite = function;
            }
        }

        return composite;
    }

    static final Map<String,Function<? super Publisher<Object>,? extends Publisher<Object>>> getOnEachOperatorHooks()
    {
        return Collections.unmodifiableMap( onEachOperatorHooks );
    }

    static final Map<String,Function<? super Publisher<Object>,? extends Publisher<Object>>> getOnLastOperatorHooks()
    {
        return Collections.unmodifiableMap( onLastOperatorHooks );
    }

    static final Map<String,BiFunction<? super Throwable,Object,? extends Throwable>> getOnOperatorErrorHooks()
    {
        return Collections.unmodifiableMap( onOperatorErrorHooks );
    }

    /**
     * @deprecated
     */
    @Nullable
    @Deprecated
    public static <T, P extends Publisher<T>> Publisher<T> addReturnInfo( @Nullable P publisher, String method )
    {
        return publisher == null ? null : addAssemblyInfo( publisher, new FluxOnAssembly.MethodReturnSnapshot( method ) );
    }

    /**
     * @deprecated
     */
    @Nullable
    @Deprecated
    public static <T, P extends Publisher<T>> Publisher<T> addCallSiteInfo( @Nullable P publisher, String callSite )
    {
        return publisher == null ? null : addAssemblyInfo( publisher, new FluxOnAssembly.AssemblySnapshot( callSite ) );
    }

    static <T, P extends Publisher<T>> Publisher<T> addAssemblyInfo( P publisher, FluxOnAssembly.AssemblySnapshot stacktrace )
    {
        if ( publisher instanceof Callable )
        {
            return (Publisher) (publisher instanceof Mono ? new MonoCallableOnAssembly( (Mono) publisher, stacktrace )
                                                          : new FluxCallableOnAssembly( (Flux) publisher, stacktrace ));
        }
        else if ( publisher instanceof Mono )
        {
            return new MonoOnAssembly( (Mono) publisher, stacktrace );
        }
        else if ( publisher instanceof ParallelFlux )
        {
            return new ParallelFluxOnAssembly( (ParallelFlux) publisher, stacktrace );
        }
        else
        {
            return (Publisher) (publisher instanceof ConnectableFlux ? new ConnectableFluxOnAssembly( (ConnectableFlux) publisher, stacktrace )
                                                                     : new FluxOnAssembly( (Flux) publisher, stacktrace ));
        }
    }
}
