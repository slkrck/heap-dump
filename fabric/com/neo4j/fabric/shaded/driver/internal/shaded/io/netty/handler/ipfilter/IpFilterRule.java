package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ipfilter;

import java.net.InetSocketAddress;

public interface IpFilterRule
{
    boolean matches( InetSocketAddress var1 );

    IpFilterRuleType ruleType();
}
