package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

public enum ProtocolDetectionState
{
    NEEDS_MORE_DATA,
    INVALID,
    DETECTED;
}
