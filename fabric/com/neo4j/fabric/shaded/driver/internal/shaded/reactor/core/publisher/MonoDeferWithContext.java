package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.Function;

final class MonoDeferWithContext<T> extends Mono<T> implements SourceProducer<T>
{
    final Function<Context,? extends Mono<? extends T>> supplier;

    MonoDeferWithContext( Function<Context,? extends Mono<? extends T>> supplier )
    {
        this.supplier = (Function) Objects.requireNonNull( supplier, "supplier" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Context ctx = actual.currentContext();

        Mono p;
        try
        {
            p = (Mono) Objects.requireNonNull( this.supplier.apply( ctx ), "The Mono returned by the supplier is null" );
        }
        catch ( Throwable var5 )
        {
            Operators.error( actual, Operators.onOperatorError( var5, ctx ) );
            return;
        }

        p.subscribe( actual );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
