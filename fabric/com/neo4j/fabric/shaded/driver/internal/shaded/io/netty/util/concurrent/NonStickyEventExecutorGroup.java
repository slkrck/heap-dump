package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

public final class NonStickyEventExecutorGroup implements EventExecutorGroup
{
    private final EventExecutorGroup group;
    private final int maxTaskExecutePerRun;

    public NonStickyEventExecutorGroup( EventExecutorGroup group )
    {
        this( group, 1024 );
    }

    public NonStickyEventExecutorGroup( EventExecutorGroup group, int maxTaskExecutePerRun )
    {
        this.group = verify( group );
        this.maxTaskExecutePerRun = ObjectUtil.checkPositive( maxTaskExecutePerRun, "maxTaskExecutePerRun" );
    }

    private static EventExecutorGroup verify( EventExecutorGroup group )
    {
        Iterator executors = ((EventExecutorGroup) ObjectUtil.checkNotNull( group, "group" )).iterator();

        EventExecutor executor;
        do
        {
            if ( !executors.hasNext() )
            {
                return group;
            }

            executor = (EventExecutor) executors.next();
        }
        while ( !(executor instanceof OrderedEventExecutor) );

        throw new IllegalArgumentException( "EventExecutorGroup " + group + " contains OrderedEventExecutors: " + executor );
    }

    private NonStickyEventExecutorGroup.NonStickyOrderedEventExecutor newExecutor( EventExecutor executor )
    {
        return new NonStickyEventExecutorGroup.NonStickyOrderedEventExecutor( executor, this.maxTaskExecutePerRun );
    }

    public boolean isShuttingDown()
    {
        return this.group.isShuttingDown();
    }

    public Future<?> shutdownGracefully()
    {
        return this.group.shutdownGracefully();
    }

    public Future<?> shutdownGracefully( long quietPeriod, long timeout, TimeUnit unit )
    {
        return this.group.shutdownGracefully( quietPeriod, timeout, unit );
    }

    public Future<?> terminationFuture()
    {
        return this.group.terminationFuture();
    }

    public void shutdown()
    {
        this.group.shutdown();
    }

    public List<Runnable> shutdownNow()
    {
        return this.group.shutdownNow();
    }

    public EventExecutor next()
    {
        return this.newExecutor( this.group.next() );
    }

    public Iterator<EventExecutor> iterator()
    {
        final Iterator<EventExecutor> itr = this.group.iterator();
        return new Iterator<EventExecutor>()
        {
            public boolean hasNext()
            {
                return itr.hasNext();
            }

            public EventExecutor next()
            {
                return NonStickyEventExecutorGroup.this.newExecutor( (EventExecutor) itr.next() );
            }

            public void remove()
            {
                itr.remove();
            }
        };
    }

    public Future<?> submit( Runnable task )
    {
        return this.group.submit( task );
    }

    public <T> Future<T> submit( Runnable task, T result )
    {
        return this.group.submit( task, result );
    }

    public <T> Future<T> submit( Callable<T> task )
    {
        return this.group.submit( task );
    }

    public ScheduledFuture<?> schedule( Runnable command, long delay, TimeUnit unit )
    {
        return this.group.schedule( command, delay, unit );
    }

    public <V> ScheduledFuture<V> schedule( Callable<V> callable, long delay, TimeUnit unit )
    {
        return this.group.schedule( callable, delay, unit );
    }

    public ScheduledFuture<?> scheduleAtFixedRate( Runnable command, long initialDelay, long period, TimeUnit unit )
    {
        return this.group.scheduleAtFixedRate( command, initialDelay, period, unit );
    }

    public ScheduledFuture<?> scheduleWithFixedDelay( Runnable command, long initialDelay, long delay, TimeUnit unit )
    {
        return this.group.scheduleWithFixedDelay( command, initialDelay, delay, unit );
    }

    public boolean isShutdown()
    {
        return this.group.isShutdown();
    }

    public boolean isTerminated()
    {
        return this.group.isTerminated();
    }

    public boolean awaitTermination( long timeout, TimeUnit unit ) throws InterruptedException
    {
        return this.group.awaitTermination( timeout, unit );
    }

    public <T> List<java.util.concurrent.Future<T>> invokeAll( Collection<? extends Callable<T>> tasks ) throws InterruptedException
    {
        return this.group.invokeAll( tasks );
    }

    public <T> List<java.util.concurrent.Future<T>> invokeAll( Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit )
            throws InterruptedException
    {
        return this.group.invokeAll( tasks, timeout, unit );
    }

    public <T> T invokeAny( Collection<? extends Callable<T>> tasks ) throws InterruptedException, ExecutionException
    {
        return this.group.invokeAny( tasks );
    }

    public <T> T invokeAny( Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit )
            throws InterruptedException, ExecutionException, TimeoutException
    {
        return this.group.invokeAny( tasks, timeout, unit );
    }

    public void execute( Runnable command )
    {
        this.group.execute( command );
    }

    private static final class NonStickyOrderedEventExecutor extends AbstractEventExecutor implements Runnable, OrderedEventExecutor
    {
        private static final int NONE = 0;
        private static final int SUBMITTED = 1;
        private static final int RUNNING = 2;
        private final EventExecutor executor;
        private final Queue<Runnable> tasks = PlatformDependent.newMpscQueue();
        private final AtomicInteger state = new AtomicInteger();
        private final int maxTaskExecutePerRun;

        NonStickyOrderedEventExecutor( EventExecutor executor, int maxTaskExecutePerRun )
        {
            super( executor );
            this.executor = executor;
            this.maxTaskExecutePerRun = maxTaskExecutePerRun;
        }

        public void run()
        {
            if ( this.state.compareAndSet( 1, 2 ) )
            {
                while ( true )
                {
                    int i = 0;

                    try
                    {
                        while ( i < this.maxTaskExecutePerRun )
                        {
                            Runnable task = (Runnable) this.tasks.poll();
                            if ( task == null )
                            {
                                break;
                            }

                            safeExecute( task );
                            ++i;
                        }
                    }
                    finally
                    {
                        if ( i == this.maxTaskExecutePerRun )
                        {
                            try
                            {
                                this.state.set( 1 );
                                this.executor.execute( this );
                                return;
                            }
                            catch ( Throwable var8 )
                            {
                                this.state.set( 2 );
                            }
                        }
                        else
                        {
                            this.state.set( 0 );
                            if ( this.tasks.peek() == null || !this.state.compareAndSet( 0, 2 ) )
                            {
                                return;
                            }
                        }
                    }
                }
            }
        }

        public boolean inEventLoop( Thread thread )
        {
            return false;
        }

        public boolean inEventLoop()
        {
            return false;
        }

        public boolean isShuttingDown()
        {
            return this.executor.isShutdown();
        }

        public Future<?> shutdownGracefully( long quietPeriod, long timeout, TimeUnit unit )
        {
            return this.executor.shutdownGracefully( quietPeriod, timeout, unit );
        }

        public Future<?> terminationFuture()
        {
            return this.executor.terminationFuture();
        }

        public void shutdown()
        {
            this.executor.shutdown();
        }

        public boolean isShutdown()
        {
            return this.executor.isShutdown();
        }

        public boolean isTerminated()
        {
            return this.executor.isTerminated();
        }

        public boolean awaitTermination( long timeout, TimeUnit unit ) throws InterruptedException
        {
            return this.executor.awaitTermination( timeout, unit );
        }

        public void execute( Runnable command )
        {
            if ( !this.tasks.offer( command ) )
            {
                throw new RejectedExecutionException();
            }
            else
            {
                if ( this.state.compareAndSet( 0, 1 ) )
                {
                    try
                    {
                        this.executor.execute( this );
                    }
                    catch ( Throwable var3 )
                    {
                        this.tasks.remove( command );
                        PlatformDependent.throwException( var3 );
                    }
                }
            }
        }
    }
}
