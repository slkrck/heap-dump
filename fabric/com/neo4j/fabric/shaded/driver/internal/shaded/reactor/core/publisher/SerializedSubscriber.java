package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.reactivestreams.Subscription;

final class SerializedSubscriber<T> implements InnerOperator<T,T>
{
    final CoreSubscriber<? super T> actual;
    boolean emitting;
    boolean missed;
    volatile boolean done;
    volatile boolean cancelled;
    SerializedSubscriber.LinkedArrayNode<T> head;
    SerializedSubscriber.LinkedArrayNode<T> tail;
    Throwable error;
    Subscription s;

    SerializedSubscriber( CoreSubscriber<? super T> actual )
    {
        this.actual = actual;
    }

    public void onSubscribe( Subscription s )
    {
        if ( Operators.validate( this.s, s ) )
        {
            this.s = s;
            this.actual.onSubscribe( this );
        }
    }

    public void onNext( T t )
    {
        if ( !this.cancelled && !this.done )
        {
            synchronized ( this )
            {
                if ( this.cancelled || this.done )
                {
                    return;
                }

                if ( this.emitting )
                {
                    this.serAdd( t );
                    this.missed = true;
                    return;
                }

                this.emitting = true;
            }

            this.actual.onNext( t );
            this.serDrainLoop( this.actual );
        }
    }

    public void onError( Throwable t )
    {
        if ( !this.cancelled && !this.done )
        {
            synchronized ( this )
            {
                if ( this.cancelled || this.done )
                {
                    return;
                }

                this.done = true;
                this.error = t;
                if ( this.emitting )
                {
                    this.missed = true;
                    return;
                }
            }

            this.actual.onError( t );
        }
    }

    public void onComplete()
    {
        if ( !this.cancelled && !this.done )
        {
            synchronized ( this )
            {
                if ( this.cancelled || this.done )
                {
                    return;
                }

                this.done = true;
                if ( this.emitting )
                {
                    this.missed = true;
                    return;
                }
            }

            this.actual.onComplete();
        }
    }

    public void request( long n )
    {
        this.s.request( n );
    }

    public void cancel()
    {
        this.cancelled = true;
        this.s.cancel();
    }

    void serAdd( T value )
    {
        SerializedSubscriber.LinkedArrayNode<T> t = this.tail;
        if ( t == null )
        {
            t = new SerializedSubscriber.LinkedArrayNode( value );
            this.head = t;
            this.tail = t;
        }
        else if ( t.count == 16 )
        {
            SerializedSubscriber.LinkedArrayNode<T> n = new SerializedSubscriber.LinkedArrayNode( value );
            t.next = n;
            this.tail = n;
        }
        else
        {
            t.array[t.count++] = value;
        }
    }

    void serDrainLoop( CoreSubscriber<? super T> actual )
    {
        while ( !this.cancelled )
        {
            boolean d;
            Throwable e;
            SerializedSubscriber.LinkedArrayNode n;
            synchronized ( this )
            {
                if ( this.cancelled )
                {
                    return;
                }

                if ( !this.missed )
                {
                    this.emitting = false;
                    return;
                }

                this.missed = false;
                d = this.done;
                e = this.error;
                n = this.head;
                this.head = null;
                this.tail = null;
            }

            while ( n != null )
            {
                T[] arr = n.array;
                int c = n.count;

                for ( int i = 0; i < c; ++i )
                {
                    if ( this.cancelled )
                    {
                        return;
                    }

                    actual.onNext( arr[i] );
                }

                n = n.next;
            }

            if ( this.cancelled )
            {
                return;
            }

            if ( e != null )
            {
                actual.onError( e );
                return;
            }

            if ( d )
            {
                actual.onComplete();
                return;
            }
        }
    }

    public CoreSubscriber<? super T> actual()
    {
        return this.actual;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.s;
        }
        else if ( key == Scannable.Attr.ERROR )
        {
            return this.error;
        }
        else if ( key == Scannable.Attr.BUFFERED )
        {
            return this.producerCapacity();
        }
        else if ( key == Scannable.Attr.CAPACITY )
        {
            return 16;
        }
        else if ( key == Scannable.Attr.CANCELLED )
        {
            return this.cancelled;
        }
        else
        {
            return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
        }
    }

    int producerCapacity()
    {
        SerializedSubscriber.LinkedArrayNode<T> node = this.tail;
        return node != null ? node.count : 0;
    }

    static final class LinkedArrayNode<T>
    {
        static final int DEFAULT_CAPACITY = 16;
        final T[] array = (Object[]) (new Object[16]);
        int count;
        SerializedSubscriber.LinkedArrayNode<T> next;

        LinkedArrayNode( T value )
        {
            this.array[0] = value;
            this.count = 1;
        }
    }
}
