package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Stream;

final class Context0 implements Context
{
    static final Context0 INSTANCE = new Context0();

    public Context put( Object key, Object value )
    {
        Objects.requireNonNull( key, "key" );
        Objects.requireNonNull( value, "value" );
        return new Context1( key, value );
    }

    public Context delete( Object key )
    {
        return this;
    }

    public <T> T get( Object key )
    {
        throw new NoSuchElementException( "Context is empty" );
    }

    public boolean hasKey( Object key )
    {
        return false;
    }

    public int size()
    {
        return 0;
    }

    public String toString()
    {
        return "Context0{}";
    }

    public Stream<Entry<Object,Object>> stream()
    {
        return Stream.empty();
    }
}
