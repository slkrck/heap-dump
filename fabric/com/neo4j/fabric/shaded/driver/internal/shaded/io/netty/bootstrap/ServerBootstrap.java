package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelConfig;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelInboundHandlerAdapter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelInitializer;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOption;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPipeline;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ServerChannel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AttributeKey;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class ServerBootstrap extends AbstractBootstrap<ServerBootstrap,ServerChannel>
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( ServerBootstrap.class );
    private final Map<ChannelOption<?>,Object> childOptions = new ConcurrentHashMap();
    private final Map<AttributeKey<?>,Object> childAttrs = new ConcurrentHashMap();
    private final ServerBootstrapConfig config = new ServerBootstrapConfig( this );
    private volatile EventLoopGroup childGroup;
    private volatile ChannelHandler childHandler;

    public ServerBootstrap()
    {
    }

    private ServerBootstrap( ServerBootstrap bootstrap )
    {
        super( bootstrap );
        this.childGroup = bootstrap.childGroup;
        this.childHandler = bootstrap.childHandler;
        this.childOptions.putAll( bootstrap.childOptions );
        this.childAttrs.putAll( bootstrap.childAttrs );
    }

    public ServerBootstrap group( EventLoopGroup group )
    {
        return this.group( group, group );
    }

    public ServerBootstrap group( EventLoopGroup parentGroup, EventLoopGroup childGroup )
    {
        super.group( parentGroup );
        ObjectUtil.checkNotNull( childGroup, "childGroup" );
        if ( this.childGroup != null )
        {
            throw new IllegalStateException( "childGroup set already" );
        }
        else
        {
            this.childGroup = childGroup;
            return this;
        }
    }

    public <T> ServerBootstrap childOption( ChannelOption<T> childOption, T value )
    {
        ObjectUtil.checkNotNull( childOption, "childOption" );
        if ( value == null )
        {
            this.childOptions.remove( childOption );
        }
        else
        {
            this.childOptions.put( childOption, value );
        }

        return this;
    }

    public <T> ServerBootstrap childAttr( AttributeKey<T> childKey, T value )
    {
        ObjectUtil.checkNotNull( childKey, "childKey" );
        if ( value == null )
        {
            this.childAttrs.remove( childKey );
        }
        else
        {
            this.childAttrs.put( childKey, value );
        }

        return this;
    }

    public ServerBootstrap childHandler( ChannelHandler childHandler )
    {
        this.childHandler = (ChannelHandler) ObjectUtil.checkNotNull( childHandler, "childHandler" );
        return this;
    }

    void init( Channel channel )
    {
        setChannelOptions( channel, (Entry[]) this.options0().entrySet().toArray( newOptionArray( 0 ) ), logger );
        setAttributes( channel, (Entry[]) this.attrs0().entrySet().toArray( newAttrArray( 0 ) ) );
        ChannelPipeline p = channel.pipeline();
        final EventLoopGroup currentChildGroup = this.childGroup;
        final ChannelHandler currentChildHandler = this.childHandler;
        final Entry<ChannelOption<?>,Object>[] currentChildOptions = (Entry[]) this.childOptions.entrySet().toArray( newOptionArray( 0 ) );
        final Entry<AttributeKey<?>,Object>[] currentChildAttrs = (Entry[]) this.childAttrs.entrySet().toArray( newAttrArray( 0 ) );
        p.addLast( new ChannelInitializer<Channel>()
        {
            public void initChannel( final Channel ch )
            {
                final ChannelPipeline pipeline = ch.pipeline();
                ChannelHandler handler = ServerBootstrap.this.config.handler();
                if ( handler != null )
                {
                    pipeline.addLast( handler );
                }

                ch.eventLoop().execute( new Runnable()
                {
                    public void run()
                    {
                        pipeline.addLast( new ServerBootstrap.ServerBootstrapAcceptor( ch, currentChildGroup, currentChildHandler, currentChildOptions,
                                currentChildAttrs ) );
                    }
                } );
            }
        } );
    }

    public ServerBootstrap validate()
    {
        super.validate();
        if ( this.childHandler == null )
        {
            throw new IllegalStateException( "childHandler not set" );
        }
        else
        {
            if ( this.childGroup == null )
            {
                logger.warn( "childGroup is not set. Using parentGroup instead." );
                this.childGroup = this.config.group();
            }

            return this;
        }
    }

    public ServerBootstrap clone()
    {
        return new ServerBootstrap( this );
    }

    /**
     * @deprecated
     */
    @Deprecated
    public EventLoopGroup childGroup()
    {
        return this.childGroup;
    }

    final ChannelHandler childHandler()
    {
        return this.childHandler;
    }

    final Map<ChannelOption<?>,Object> childOptions()
    {
        return copiedMap( this.childOptions );
    }

    final Map<AttributeKey<?>,Object> childAttrs()
    {
        return copiedMap( this.childAttrs );
    }

    public final ServerBootstrapConfig config()
    {
        return this.config;
    }

    private static class ServerBootstrapAcceptor extends ChannelInboundHandlerAdapter
    {
        private final EventLoopGroup childGroup;
        private final ChannelHandler childHandler;
        private final Entry<ChannelOption<?>,Object>[] childOptions;
        private final Entry<AttributeKey<?>,Object>[] childAttrs;
        private final Runnable enableAutoReadTask;

        ServerBootstrapAcceptor( final Channel channel, EventLoopGroup childGroup, ChannelHandler childHandler, Entry<ChannelOption<?>,Object>[] childOptions,
                Entry<AttributeKey<?>,Object>[] childAttrs )
        {
            this.childGroup = childGroup;
            this.childHandler = childHandler;
            this.childOptions = childOptions;
            this.childAttrs = childAttrs;
            this.enableAutoReadTask = new Runnable()
            {
                public void run()
                {
                    channel.config().setAutoRead( true );
                }
            };
        }

        private static void forceClose( Channel child, Throwable t )
        {
            child.unsafe().closeForcibly();
            ServerBootstrap.logger.warn( "Failed to register an accepted channel: {}", child, t );
        }

        public void channelRead( ChannelHandlerContext ctx, Object msg )
        {
            final Channel child = (Channel) msg;
            child.pipeline().addLast( this.childHandler );
            AbstractBootstrap.setChannelOptions( child, this.childOptions, ServerBootstrap.logger );
            AbstractBootstrap.setAttributes( child, this.childAttrs );

            try
            {
                this.childGroup.register( child ).addListener( new ChannelFutureListener()
                {
                    public void operationComplete( ChannelFuture future ) throws Exception
                    {
                        if ( !future.isSuccess() )
                        {
                            ServerBootstrap.ServerBootstrapAcceptor.forceClose( child, future.cause() );
                        }
                    }
                } );
            }
            catch ( Throwable var5 )
            {
                forceClose( child, var5 );
            }
        }

        public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception
        {
            ChannelConfig config = ctx.channel().config();
            if ( config.isAutoRead() )
            {
                config.setAutoRead( false );
                ctx.channel().eventLoop().schedule( this.enableAutoReadTask, 1L, TimeUnit.SECONDS );
            }

            ctx.fireExceptionCaught( cause );
        }
    }
}
