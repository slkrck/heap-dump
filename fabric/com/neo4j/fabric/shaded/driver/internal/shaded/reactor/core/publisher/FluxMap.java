package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Subscription;

final class FluxMap<T, R> extends InternalFluxOperator<T,R>
{
    final Function<? super T,? extends R> mapper;

    FluxMap( Flux<? extends T> source, Function<? super T,? extends R> mapper )
    {
        super( source );
        this.mapper = (Function) Objects.requireNonNull( mapper, "mapper" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<? super R> cs = (Fuseable.ConditionalSubscriber) actual;
            return new FluxMap.MapConditionalSubscriber( cs, this.mapper );
        }
        else
        {
            return new FluxMap.MapSubscriber( actual, this.mapper );
        }
    }

    static final class MapConditionalSubscriber<T, R> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,R>
    {
        final Fuseable.ConditionalSubscriber<? super R> actual;
        final Function<? super T,? extends R> mapper;
        boolean done;
        Subscription s;

        MapConditionalSubscriber( Fuseable.ConditionalSubscriber<? super R> actual, Function<? super T,? extends R> mapper )
        {
            this.actual = actual;
            this.mapper = mapper;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                Object v;
                try
                {
                    v = Objects.requireNonNull( this.mapper.apply( t ), "The mapper returned a null value." );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.actual.currentContext(), this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }
                    else
                    {
                        this.s.request( 1L );
                    }

                    return;
                }

                this.actual.onNext( v );
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
                return true;
            }
            else
            {
                try
                {
                    R v = Objects.requireNonNull( this.mapper.apply( t ), "The mapper returned a null value." );
                    return this.actual.tryOnNext( v );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.actual.currentContext(), this.s );
                    if ( e_ != null )
                    {
                        this.done = true;
                        this.actual.onError( e_ );
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }

    static final class MapSubscriber<T, R> implements InnerOperator<T,R>
    {
        final CoreSubscriber<? super R> actual;
        final Function<? super T,? extends R> mapper;
        boolean done;
        Subscription s;

        MapSubscriber( CoreSubscriber<? super R> actual, Function<? super T,? extends R> mapper )
        {
            this.actual = actual;
            this.mapper = mapper;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                Object v;
                try
                {
                    v = Objects.requireNonNull( this.mapper.apply( t ), "The mapper returned a null value." );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.actual.currentContext(), this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }
                    else
                    {
                        this.s.request( 1L );
                    }

                    return;
                }

                this.actual.onNext( v );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
