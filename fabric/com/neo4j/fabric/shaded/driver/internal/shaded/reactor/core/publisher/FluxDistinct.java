package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

final class FluxDistinct<T, K, C> extends InternalFluxOperator<T,T>
{
    final Function<? super T,? extends K> keyExtractor;
    final Supplier<C> collectionSupplier;
    final BiPredicate<C,K> distinctPredicate;
    final Consumer<C> cleanupCallback;

    FluxDistinct( Flux<? extends T> source, Function<? super T,? extends K> keyExtractor, Supplier<C> collectionSupplier, BiPredicate<C,K> distinctPredicate,
            Consumer<C> cleanupCallback )
    {
        super( source );
        this.keyExtractor = (Function) Objects.requireNonNull( keyExtractor, "keyExtractor" );
        this.collectionSupplier = (Supplier) Objects.requireNonNull( collectionSupplier, "collectionSupplier" );
        this.distinctPredicate = (BiPredicate) Objects.requireNonNull( distinctPredicate, "distinctPredicate" );
        this.cleanupCallback = (Consumer) Objects.requireNonNull( cleanupCallback, "cleanupCallback" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        Object collection;
        try
        {
            collection = Objects.requireNonNull( this.collectionSupplier.get(), "The collectionSupplier returned a null collection" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        return (CoreSubscriber) (actual instanceof Fuseable.ConditionalSubscriber ? new FluxDistinct.DistinctConditionalSubscriber(
                (Fuseable.ConditionalSubscriber) actual, collection, this.keyExtractor, this.distinctPredicate, this.cleanupCallback )
                                                                                  : new FluxDistinct.DistinctSubscriber( actual, collection, this.keyExtractor,
                                                                                          this.distinctPredicate, this.cleanupCallback ));
    }

    static final class DistinctFuseableSubscriber<T, K, C> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        final C collection;
        final Function<? super T,? extends K> keyExtractor;
        final BiPredicate<C,K> distinctPredicate;
        final Consumer<C> cleanupCallback;
        Fuseable.QueueSubscription<T> qs;
        boolean done;
        int sourceMode;

        DistinctFuseableSubscriber( CoreSubscriber<? super T> actual, C collection, Function<? super T,? extends K> keyExtractor, BiPredicate<C,K> predicate,
                Consumer<C> callback )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.collection = collection;
            this.keyExtractor = keyExtractor;
            this.distinctPredicate = predicate;
            this.cleanupCallback = callback;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.qs, s ) )
            {
                this.qs = (Fuseable.QueueSubscription) s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( !this.tryOnNext( t ) )
            {
                this.qs.request( 1L );
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.sourceMode == 2 )
            {
                this.actual.onNext( (Object) null );
                return true;
            }
            else if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
                return true;
            }
            else
            {
                Object k;
                try
                {
                    k = Objects.requireNonNull( this.keyExtractor.apply( t ), "The distinct extractor returned a null value." );
                }
                catch ( Throwable var6 )
                {
                    this.onError( Operators.onOperatorError( this.qs, var6, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return true;
                }

                boolean b;
                try
                {
                    b = this.distinctPredicate.test( this.collection, k );
                }
                catch ( Throwable var5 )
                {
                    this.onError( Operators.onOperatorError( this.qs, var5, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return true;
                }

                if ( b )
                {
                    this.actual.onNext( t );
                    return true;
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    return false;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                this.cleanupCallback.accept( this.collection );
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.cleanupCallback.accept( this.collection );
                this.actual.onComplete();
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.qs.request( n );
        }

        public void cancel()
        {
            this.qs.cancel();
            if ( this.collection != null )
            {
                this.cleanupCallback.accept( this.collection );
            }
        }

        public int requestFusion( int requestedMode )
        {
            int m = this.qs.requestFusion( requestedMode );
            this.sourceMode = m;
            return m;
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.qs;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        @Nullable
        public T poll()
        {
            if ( this.sourceMode == 2 )
            {
                long dropped = 0L;

                while ( true )
                {
                    T v = this.qs.poll();
                    if ( v == null )
                    {
                        return null;
                    }

                    try
                    {
                        K r = Objects.requireNonNull( this.keyExtractor.apply( v ), "The keyExtractor returned a null collection" );
                        if ( this.distinctPredicate.test( this.collection, r ) )
                        {
                            if ( dropped != 0L )
                            {
                                this.request( dropped );
                            }

                            return v;
                        }

                        Operators.onDiscard( v, this.ctx );
                        ++dropped;
                    }
                    catch ( Throwable var5 )
                    {
                        Operators.onDiscard( v, this.ctx );
                        throw var5;
                    }
                }
            }
            else
            {
                while ( true )
                {
                    T v = this.qs.poll();
                    if ( v == null )
                    {
                        return null;
                    }

                    try
                    {
                        K r = Objects.requireNonNull( this.keyExtractor.apply( v ), "The keyExtractor returned a null collection" );
                        if ( this.distinctPredicate.test( this.collection, r ) )
                        {
                            return v;
                        }

                        Operators.onDiscard( v, this.ctx );
                    }
                    catch ( Throwable var6 )
                    {
                        Operators.onDiscard( v, this.ctx );
                        throw var6;
                    }
                }
            }
        }

        public boolean isEmpty()
        {
            return this.qs.isEmpty();
        }

        public void clear()
        {
            this.qs.clear();
            this.cleanupCallback.accept( this.collection );
        }

        public int size()
        {
            return this.qs.size();
        }
    }

    static final class DistinctConditionalSubscriber<T, K, C> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,T>
    {
        final Fuseable.ConditionalSubscriber<? super T> actual;
        final Context ctx;
        final C collection;
        final Function<? super T,? extends K> keyExtractor;
        final BiPredicate<C,K> distinctPredicate;
        final Consumer<C> cleanupCallback;
        Subscription s;
        boolean done;

        DistinctConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, C collection, Function<? super T,? extends K> keyExtractor,
                BiPredicate<C,K> distinctPredicate, Consumer<C> cleanupCallback )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.collection = collection;
            this.keyExtractor = keyExtractor;
            this.distinctPredicate = distinctPredicate;
            this.cleanupCallback = cleanupCallback;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
            }
            else
            {
                Object k;
                try
                {
                    k = Objects.requireNonNull( this.keyExtractor.apply( t ), "The distinct extractor returned a null value." );
                }
                catch ( Throwable var6 )
                {
                    this.onError( Operators.onOperatorError( this.s, var6, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return;
                }

                boolean b;
                try
                {
                    b = this.distinctPredicate.test( this.collection, k );
                }
                catch ( Throwable var5 )
                {
                    this.onError( Operators.onOperatorError( this.s, var5, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return;
                }

                if ( b )
                {
                    this.actual.onNext( t );
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    this.s.request( 1L );
                }
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
                return true;
            }
            else
            {
                Object k;
                try
                {
                    k = Objects.requireNonNull( this.keyExtractor.apply( t ), "The distinct extractor returned a null value." );
                }
                catch ( Throwable var6 )
                {
                    this.onError( Operators.onOperatorError( this.s, var6, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return true;
                }

                boolean b;
                try
                {
                    b = this.distinctPredicate.test( this.collection, k );
                }
                catch ( Throwable var5 )
                {
                    this.onError( Operators.onOperatorError( this.s, var5, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return true;
                }

                if ( b )
                {
                    return this.actual.tryOnNext( t );
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    return false;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                this.cleanupCallback.accept( this.collection );
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.cleanupCallback.accept( this.collection );
                this.actual.onComplete();
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
            if ( this.collection != null )
            {
                this.cleanupCallback.accept( this.collection );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }
    }

    static final class DistinctSubscriber<T, K, C> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        final C collection;
        final Function<? super T,? extends K> keyExtractor;
        final BiPredicate<C,K> distinctPredicate;
        final Consumer<C> cleanupCallback;
        Subscription s;
        boolean done;

        DistinctSubscriber( CoreSubscriber<? super T> actual, C collection, Function<? super T,? extends K> keyExtractor, BiPredicate<C,K> distinctPredicate,
                Consumer<C> cleanupCallback )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.collection = collection;
            this.keyExtractor = keyExtractor;
            this.distinctPredicate = distinctPredicate;
            this.cleanupCallback = cleanupCallback;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( !this.tryOnNext( t ) )
            {
                this.s.request( 1L );
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
                return true;
            }
            else
            {
                Object k;
                try
                {
                    k = Objects.requireNonNull( this.keyExtractor.apply( t ), "The distinct extractor returned a null value." );
                }
                catch ( Throwable var6 )
                {
                    this.onError( Operators.onOperatorError( this.s, var6, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return true;
                }

                boolean b;
                try
                {
                    b = this.distinctPredicate.test( this.collection, k );
                }
                catch ( Throwable var5 )
                {
                    this.onError( Operators.onOperatorError( this.s, var5, t, this.ctx ) );
                    Operators.onDiscard( t, this.ctx );
                    return true;
                }

                if ( b )
                {
                    this.actual.onNext( t );
                    return true;
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    return false;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                this.cleanupCallback.accept( this.collection );
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.cleanupCallback.accept( this.collection );
                this.actual.onComplete();
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
            if ( this.collection != null )
            {
                this.cleanupCallback.accept( this.collection );
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }
    }
}
