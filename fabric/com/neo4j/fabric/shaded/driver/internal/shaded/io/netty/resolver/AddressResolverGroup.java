package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.resolver;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.FutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.io.Closeable;
import java.net.SocketAddress;
import java.util.IdentityHashMap;
import java.util.Map;

public abstract class AddressResolverGroup<T extends SocketAddress> implements Closeable
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( AddressResolverGroup.class );
    private final Map<EventExecutor,AddressResolver<T>> resolvers = new IdentityHashMap();

    protected AddressResolverGroup()
    {
    }

    public AddressResolver<T> getResolver( final EventExecutor executor )
    {
        if ( executor == null )
        {
            throw new NullPointerException( "executor" );
        }
        else if ( executor.isShuttingDown() )
        {
            throw new IllegalStateException( "executor not accepting a task" );
        }
        else
        {
            synchronized ( this.resolvers )
            {
                AddressResolver<T> r = (AddressResolver) this.resolvers.get( executor );
                if ( r == null )
                {
                    final AddressResolver newResolver;
                    try
                    {
                        newResolver = this.newResolver( executor );
                    }
                    catch ( Exception var7 )
                    {
                        throw new IllegalStateException( "failed to create a new resolver", var7 );
                    }

                    this.resolvers.put( executor, newResolver );
                    executor.terminationFuture().addListener( new FutureListener<Object>()
                    {
                        public void operationComplete( Future<Object> future ) throws Exception
                        {
                            synchronized ( AddressResolverGroup.this.resolvers )
                            {
                                AddressResolverGroup.this.resolvers.remove( executor );
                            }

                            newResolver.close();
                        }
                    } );
                    r = newResolver;
                }

                return r;
            }
        }
    }

    protected abstract AddressResolver<T> newResolver( EventExecutor var1 ) throws Exception;

    public void close()
    {
        AddressResolver[] rArray;
        synchronized ( this.resolvers )
        {
            rArray = (AddressResolver[]) ((AddressResolver[]) this.resolvers.values().toArray( new AddressResolver[0] ));
            this.resolvers.clear();
        }

        AddressResolver[] var2 = rArray;
        int var3 = rArray.length;

        for ( int var4 = 0; var4 < var3; ++var4 )
        {
            AddressResolver r = var2[var4];

            try
            {
                r.close();
            }
            catch ( Throwable var7 )
            {
                logger.warn( "Failed to close a resolver:", var7 );
            }
        }
    }
}
