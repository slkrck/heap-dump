package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelInboundHandlerAdapter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.SSL;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ReflectionUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.SystemPropertyUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.lang.reflect.Field;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;

public abstract class SslMasterKeyHandler extends ChannelInboundHandlerAdapter
{
    public static final String SYSTEM_PROP_KEY = "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.ssl.masterKeyHandler";
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( SslMasterKeyHandler.class );
    private static final Class<?> SSL_SESSIONIMPL_CLASS;
    private static final Field SSL_SESSIONIMPL_MASTER_SECRET_FIELD;
    private static final Throwable UNAVAILABILITY_CAUSE;

    static
    {
        Throwable cause = null;
        Class<?> clazz = null;
        Field field = null;

        try
        {
            clazz = Class.forName( "sun.security.ssl.SSLSessionImpl" );
            field = clazz.getDeclaredField( "masterSecret" );
            cause = ReflectionUtil.trySetAccessible( field, true );
        }
        catch ( Throwable var4 )
        {
            cause = var4;
            logger.debug( "sun.security.ssl.SSLSessionImpl is unavailable.", var4 );
        }

        UNAVAILABILITY_CAUSE = cause;
        SSL_SESSIONIMPL_CLASS = clazz;
        SSL_SESSIONIMPL_MASTER_SECRET_FIELD = field;
    }

    protected SslMasterKeyHandler()
    {
    }

    public static void ensureSunSslEngineAvailability()
    {
        if ( UNAVAILABILITY_CAUSE != null )
        {
            throw new IllegalStateException( "Failed to find SSLSessionImpl on classpath", UNAVAILABILITY_CAUSE );
        }
    }

    public static Throwable sunSslEngineUnavailabilityCause()
    {
        return UNAVAILABILITY_CAUSE;
    }

    public static boolean isSunSslEngineAvailable()
    {
        return UNAVAILABILITY_CAUSE == null;
    }

    public static SslMasterKeyHandler newWireSharkSslMasterKeyHandler()
    {
        return new SslMasterKeyHandler.WiresharkSslMasterKeyHandler();
    }

    protected abstract void accept( SecretKey var1, SSLSession var2 );

    public final void userEventTriggered( ChannelHandlerContext ctx, Object evt )
    {
        if ( evt == SslHandshakeCompletionEvent.SUCCESS )
        {
            boolean shouldHandle = SystemPropertyUtil.getBoolean( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.ssl.masterKeyHandler", false );
            if ( shouldHandle )
            {
                SslHandler handler = (SslHandler) ctx.pipeline().get( SslHandler.class );
                SSLEngine engine = handler.engine();
                SSLSession sslSession = engine.getSession();
                if ( isSunSslEngineAvailable() && sslSession.getClass().equals( SSL_SESSIONIMPL_CLASS ) )
                {
                    SecretKey secretKey;
                    try
                    {
                        secretKey = (SecretKey) SSL_SESSIONIMPL_MASTER_SECRET_FIELD.get( sslSession );
                    }
                    catch ( IllegalAccessException var9 )
                    {
                        throw new IllegalArgumentException( "Failed to access the field 'masterSecret' via reflection.", var9 );
                    }

                    this.accept( secretKey, sslSession );
                }
                else if ( OpenSsl.isAvailable() && engine instanceof ReferenceCountedOpenSslEngine )
                {
                    SecretKeySpec secretKey = new SecretKeySpec( SSL.getMasterKey( ((ReferenceCountedOpenSslEngine) engine).sslPointer() ), "AES" );
                    this.accept( secretKey, sslSession );
                }
            }
        }

        ctx.fireUserEventTriggered( evt );
    }

    private static final class WiresharkSslMasterKeyHandler extends SslMasterKeyHandler
    {
        private static final InternalLogger wireshark_logger =
                InternalLoggerFactory.getInstance( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.wireshark" );
        private static final char[] hexCode = "0123456789ABCDEF".toCharArray();

        private WiresharkSslMasterKeyHandler()
        {
        }

        protected void accept( SecretKey masterKey, SSLSession session )
        {
            if ( masterKey.getEncoded().length != 48 )
            {
                throw new IllegalArgumentException( "An invalid length master key was provided." );
            }
            else
            {
                byte[] sessionId = session.getId();
                wireshark_logger.warn( "RSA Session-ID:{} Master-Key:{}", ByteBufUtil.hexDump( sessionId ).toLowerCase(),
                        ByteBufUtil.hexDump( masterKey.getEncoded() ).toLowerCase() );
            }
        }
    }
}
