package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.concurrent.Callable;

final class FluxCallableOnAssembly<T> extends InternalFluxOperator<T,T> implements Fuseable, Callable<T>, AssemblyOp
{
    final FluxOnAssembly.AssemblySnapshot stacktrace;

    FluxCallableOnAssembly( Flux<? extends T> source, FluxOnAssembly.AssemblySnapshot stacktrace )
    {
        super( source );
        this.stacktrace = stacktrace;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return FluxOnAssembly.wrapSubscriber( actual, this.source, this.stacktrace );
    }

    public T call() throws Exception
    {
        return ((Callable) this.source).call();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.ACTUAL_METADATA ? !this.stacktrace.checkpointed : super.scanUnsafe( key );
    }

    public String stepName()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }

    public String toString()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }
}
