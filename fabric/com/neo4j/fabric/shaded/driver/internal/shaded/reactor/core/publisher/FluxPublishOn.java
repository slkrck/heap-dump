package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxPublishOn<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    final Scheduler scheduler;
    final boolean delayError;
    final Supplier<? extends Queue<T>> queueSupplier;
    final int prefetch;
    final int lowTide;

    FluxPublishOn( Flux<? extends T> source, Scheduler scheduler, boolean delayError, int prefetch, int lowTide, Supplier<? extends Queue<T>> queueSupplier )
    {
        super( source );
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.scheduler = (Scheduler) Objects.requireNonNull( scheduler, "scheduler" );
            this.delayError = delayError;
            this.prefetch = prefetch;
            this.lowTide = lowTide;
            this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        Scheduler.Worker worker;
        try
        {
            worker = (Scheduler.Worker) Objects.requireNonNull( this.scheduler.createWorker(), "The scheduler returned a null worker" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<? super T> cs = (Fuseable.ConditionalSubscriber) actual;
            this.source.subscribe(
                    (CoreSubscriber) (new FluxPublishOn.PublishOnConditionalSubscriber( cs, this.scheduler, worker, this.delayError, this.prefetch,
                            this.lowTide, this.queueSupplier )) );
            return null;
        }
        else
        {
            return new FluxPublishOn.PublishOnSubscriber( actual, this.scheduler, worker, this.delayError, this.prefetch, this.lowTide, this.queueSupplier );
        }
    }

    static final class PublishOnConditionalSubscriber<T> implements Fuseable.QueueSubscription<T>, Runnable, InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxPublishOn.PublishOnConditionalSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxPublishOn.PublishOnConditionalSubscriber.class, "wip" );
        static final AtomicLongFieldUpdater<FluxPublishOn.PublishOnConditionalSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxPublishOn.PublishOnConditionalSubscriber.class, "requested" );
        final Fuseable.ConditionalSubscriber<? super T> actual;
        final Scheduler.Worker worker;
        final Scheduler scheduler;
        final boolean delayError;
        final int prefetch;
        final int limit;
        final Supplier<? extends Queue<T>> queueSupplier;
        Subscription s;
        Queue<T> queue;
        volatile boolean cancelled;
        volatile boolean done;
        Throwable error;
        volatile int wip;
        volatile long requested;
        int sourceMode;
        long produced;
        long consumed;
        boolean outputFused;

        PublishOnConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, Scheduler scheduler, Scheduler.Worker worker, boolean delayError,
                int prefetch, int lowTide, Supplier<? extends Queue<T>> queueSupplier )
        {
            this.actual = actual;
            this.worker = worker;
            this.scheduler = scheduler;
            this.delayError = delayError;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
            this.limit = Operators.unboundedOrLimit( prefetch, lowTide );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    Fuseable.QueueSubscription<T> f = (Fuseable.QueueSubscription) s;
                    int m = f.requestFusion( 7 );
                    if ( m == 1 )
                    {
                        this.sourceMode = 1;
                        this.queue = f;
                        this.done = true;
                        this.actual.onSubscribe( this );
                        return;
                    }

                    if ( m == 2 )
                    {
                        this.sourceMode = 2;
                        this.queue = f;
                        this.actual.onSubscribe( this );
                        s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
                        return;
                    }
                }

                this.queue = (Queue) this.queueSupplier.get();
                this.actual.onSubscribe( this );
                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        public void onNext( T t )
        {
            if ( this.sourceMode == 2 )
            {
                this.trySchedule( this, (Throwable) null, (Object) null );
            }
            else if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                if ( !this.queue.offer( t ) )
                {
                    this.error = Operators.onOperatorError( this.s,
                            Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ), t,
                            this.actual.currentContext() );
                    this.done = true;
                }

                this.trySchedule( this, (Throwable) null, t );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.error = t;
                this.done = true;
                this.trySchedule( (Subscription) null, t, (Object) null );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.trySchedule( (Subscription) null, (Throwable) null, (Object) null );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.trySchedule( this, (Throwable) null, (Object) null );
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                this.worker.dispose();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                }
            }
        }

        void trySchedule( @Nullable Subscription subscription, @Nullable Throwable suppressed, @Nullable Object dataSignal )
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                try
                {
                    this.worker.schedule( this );
                }
                catch ( RejectedExecutionException var5 )
                {
                    Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                    this.actual.onError( Operators.onRejectedExecution( var5, subscription, suppressed, dataSignal, this.actual.currentContext() ) );
                }
            }
        }

        void runSync()
        {
            int missed = 1;
            Fuseable.ConditionalSubscriber<? super T> a = this.actual;
            Queue<T> q = this.queue;
            long e = this.produced;

            while ( true )
            {
                long r = this.requested;

                while ( e != r )
                {
                    Object v;
                    try
                    {
                        v = q.poll();
                    }
                    catch ( Throwable var10 )
                    {
                        this.doError( a, Operators.onOperatorError( this.s, var10, this.actual.currentContext() ) );
                        return;
                    }

                    if ( this.cancelled )
                    {
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        return;
                    }

                    if ( v == null )
                    {
                        this.doComplete( a );
                        return;
                    }

                    if ( a.tryOnNext( v ) )
                    {
                        ++e;
                    }
                }

                if ( this.cancelled )
                {
                    Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                    return;
                }

                if ( q.isEmpty() )
                {
                    this.doComplete( a );
                    return;
                }

                int w = this.wip;
                if ( missed == w )
                {
                    this.produced = e;
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    missed = w;
                }
            }
        }

        void runAsync()
        {
            int missed = 1;
            Fuseable.ConditionalSubscriber<? super T> a = this.actual;
            Queue<T> q = this.queue;
            long emitted = this.produced;
            long polled = this.consumed;

            while ( true )
            {
                long r = this.requested;

                while ( emitted != r )
                {
                    boolean d = this.done;

                    Object v;
                    try
                    {
                        v = q.poll();
                    }
                    catch ( Throwable var13 )
                    {
                        Exceptions.throwIfFatal( var13 );
                        this.s.cancel();
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        this.doError( a, Operators.onOperatorError( var13, this.actual.currentContext() ) );
                        return;
                    }

                    boolean empty = v == null;
                    if ( this.checkTerminated( d, empty, a ) )
                    {
                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    if ( a.tryOnNext( v ) )
                    {
                        ++emitted;
                    }

                    ++polled;
                    if ( polled == (long) this.limit )
                    {
                        this.s.request( polled );
                        polled = 0L;
                    }
                }

                if ( emitted == r && this.checkTerminated( this.done, q.isEmpty(), a ) )
                {
                    return;
                }

                int w = this.wip;
                if ( missed == w )
                {
                    this.produced = emitted;
                    this.consumed = polled;
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    missed = w;
                }
            }
        }

        void runBackfused()
        {
            int missed = 1;

            while ( !this.cancelled )
            {
                boolean d = this.done;
                this.actual.onNext( (Object) null );
                if ( d )
                {
                    Throwable e = this.error;
                    if ( e != null )
                    {
                        this.doError( this.actual, e );
                    }
                    else
                    {
                        this.doComplete( this.actual );
                    }

                    return;
                }

                missed = WIP.addAndGet( this, -missed );
                if ( missed == 0 )
                {
                    return;
                }
            }

            Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
        }

        public void run()
        {
            if ( this.outputFused )
            {
                this.runBackfused();
            }
            else if ( this.sourceMode == 1 )
            {
                this.runSync();
            }
            else
            {
                this.runAsync();
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.queue != null ? this.queue.size() : 0;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.DELAY_ERROR )
            {
                return this.delayError;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.worker : InnerOperator.super.scanUnsafe( key );
            }
        }

        void doComplete( Subscriber<?> a )
        {
            a.onComplete();
            this.worker.dispose();
        }

        void doError( Subscriber<?> a, Throwable e )
        {
            try
            {
                a.onError( e );
            }
            finally
            {
                this.worker.dispose();
            }
        }

        boolean checkTerminated( boolean d, boolean empty, Subscriber<?> a )
        {
            if ( this.cancelled )
            {
                Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                return true;
            }
            else
            {
                if ( d )
                {
                    Throwable e;
                    if ( this.delayError )
                    {
                        if ( empty )
                        {
                            e = this.error;
                            if ( e != null )
                            {
                                this.doError( a, e );
                            }
                            else
                            {
                                this.doComplete( a );
                            }

                            return true;
                        }
                    }
                    else
                    {
                        e = this.error;
                        if ( e != null )
                        {
                            Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                            this.doError( a, e );
                            return true;
                        }

                        if ( empty )
                        {
                            this.doComplete( a );
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        public void clear()
        {
            Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
        }

        public boolean isEmpty()
        {
            return this.queue.isEmpty();
        }

        @Nullable
        public T poll()
        {
            T v = this.queue.poll();
            if ( v != null && this.sourceMode != 1 )
            {
                long p = this.consumed + 1L;
                if ( p == (long) this.limit )
                {
                    this.consumed = 0L;
                    this.s.request( p );
                }
                else
                {
                    this.consumed = p;
                }
            }

            return v;
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 2) != 0 )
            {
                this.outputFused = true;
                return 2;
            }
            else
            {
                return 0;
            }
        }

        public int size()
        {
            return this.queue.size();
        }
    }

    static final class PublishOnSubscriber<T> implements Fuseable.QueueSubscription<T>, Runnable, InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxPublishOn.PublishOnSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxPublishOn.PublishOnSubscriber.class, "wip" );
        static final AtomicLongFieldUpdater<FluxPublishOn.PublishOnSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxPublishOn.PublishOnSubscriber.class, "requested" );
        final CoreSubscriber<? super T> actual;
        final Scheduler scheduler;
        final Scheduler.Worker worker;
        final boolean delayError;
        final int prefetch;
        final int limit;
        final Supplier<? extends Queue<T>> queueSupplier;
        Subscription s;
        Queue<T> queue;
        volatile boolean cancelled;
        volatile boolean done;
        Throwable error;
        volatile int wip;
        volatile long requested;
        int sourceMode;
        long produced;
        boolean outputFused;

        PublishOnSubscriber( CoreSubscriber<? super T> actual, Scheduler scheduler, Scheduler.Worker worker, boolean delayError, int prefetch, int lowTide,
                Supplier<? extends Queue<T>> queueSupplier )
        {
            this.actual = actual;
            this.worker = worker;
            this.scheduler = scheduler;
            this.delayError = delayError;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
            this.limit = Operators.unboundedOrLimit( prefetch, lowTide );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    Fuseable.QueueSubscription<T> f = (Fuseable.QueueSubscription) s;
                    int m = f.requestFusion( 7 );
                    if ( m == 1 )
                    {
                        this.sourceMode = 1;
                        this.queue = f;
                        this.done = true;
                        this.actual.onSubscribe( this );
                        return;
                    }

                    if ( m == 2 )
                    {
                        this.sourceMode = 2;
                        this.queue = f;
                        this.actual.onSubscribe( this );
                        s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
                        return;
                    }
                }

                this.queue = (Queue) this.queueSupplier.get();
                this.actual.onSubscribe( this );
                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        public void onNext( T t )
        {
            if ( this.sourceMode == 2 )
            {
                this.trySchedule( this, (Throwable) null, (Object) null );
            }
            else if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                if ( !this.queue.offer( t ) )
                {
                    this.error = Operators.onOperatorError( this.s,
                            Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ), t,
                            this.actual.currentContext() );
                    this.done = true;
                }

                this.trySchedule( this, (Throwable) null, t );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.error = t;
                this.done = true;
                this.trySchedule( (Subscription) null, t, (Object) null );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.trySchedule( (Subscription) null, (Throwable) null, (Object) null );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.trySchedule( this, (Throwable) null, (Object) null );
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                this.worker.dispose();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                }
            }
        }

        void trySchedule( @Nullable Subscription subscription, @Nullable Throwable suppressed, @Nullable Object dataSignal )
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                try
                {
                    this.worker.schedule( this );
                }
                catch ( RejectedExecutionException var5 )
                {
                    Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                    this.actual.onError( Operators.onRejectedExecution( var5, subscription, suppressed, dataSignal, this.actual.currentContext() ) );
                }
            }
        }

        void runSync()
        {
            int missed = 1;
            Subscriber<? super T> a = this.actual;
            Queue<T> q = this.queue;
            long e = this.produced;

            while ( true )
            {
                for ( long r = this.requested; e != r; ++e )
                {
                    Object v;
                    try
                    {
                        v = q.poll();
                    }
                    catch ( Throwable var10 )
                    {
                        this.doError( a, Operators.onOperatorError( this.s, var10, this.actual.currentContext() ) );
                        return;
                    }

                    if ( this.cancelled )
                    {
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        return;
                    }

                    if ( v == null )
                    {
                        this.doComplete( a );
                        return;
                    }

                    a.onNext( v );
                }

                if ( this.cancelled )
                {
                    Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                    return;
                }

                if ( q.isEmpty() )
                {
                    this.doComplete( a );
                    return;
                }

                int w = this.wip;
                if ( missed == w )
                {
                    this.produced = e;
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    missed = w;
                }
            }
        }

        void runAsync()
        {
            int missed = 1;
            Subscriber<? super T> a = this.actual;
            Queue<T> q = this.queue;
            long e = this.produced;

            while ( true )
            {
                long r = this.requested;

                while ( e != r )
                {
                    boolean d = this.done;

                    Object v;
                    try
                    {
                        v = q.poll();
                    }
                    catch ( Throwable var11 )
                    {
                        Exceptions.throwIfFatal( var11 );
                        this.s.cancel();
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        this.doError( a, Operators.onOperatorError( var11, this.actual.currentContext() ) );
                        return;
                    }

                    boolean empty = v == null;
                    if ( this.checkTerminated( d, empty, a ) )
                    {
                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    a.onNext( v );
                    ++e;
                    if ( e == (long) this.limit )
                    {
                        if ( r != Long.MAX_VALUE )
                        {
                            r = REQUESTED.addAndGet( this, -e );
                        }

                        this.s.request( e );
                        e = 0L;
                    }
                }

                if ( e == r && this.checkTerminated( this.done, q.isEmpty(), a ) )
                {
                    return;
                }

                int w = this.wip;
                if ( missed == w )
                {
                    this.produced = e;
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    missed = w;
                }
            }
        }

        void runBackfused()
        {
            int missed = 1;

            while ( !this.cancelled )
            {
                boolean d = this.done;
                this.actual.onNext( (Object) null );
                if ( d )
                {
                    Throwable e = this.error;
                    if ( e != null )
                    {
                        this.doError( this.actual, e );
                    }
                    else
                    {
                        this.doComplete( this.actual );
                    }

                    return;
                }

                missed = WIP.addAndGet( this, -missed );
                if ( missed == 0 )
                {
                    return;
                }
            }

            Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
        }

        void doComplete( Subscriber<?> a )
        {
            a.onComplete();
            this.worker.dispose();
        }

        void doError( Subscriber<?> a, Throwable e )
        {
            try
            {
                a.onError( e );
            }
            finally
            {
                this.worker.dispose();
            }
        }

        public void run()
        {
            if ( this.outputFused )
            {
                this.runBackfused();
            }
            else if ( this.sourceMode == 1 )
            {
                this.runSync();
            }
            else
            {
                this.runAsync();
            }
        }

        boolean checkTerminated( boolean d, boolean empty, Subscriber<?> a )
        {
            if ( this.cancelled )
            {
                Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                return true;
            }
            else
            {
                if ( d )
                {
                    Throwable e;
                    if ( this.delayError )
                    {
                        if ( empty )
                        {
                            e = this.error;
                            if ( e != null )
                            {
                                this.doError( a, e );
                            }
                            else
                            {
                                this.doComplete( a );
                            }

                            return true;
                        }
                    }
                    else
                    {
                        e = this.error;
                        if ( e != null )
                        {
                            Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                            this.doError( a, e );
                            return true;
                        }

                        if ( empty )
                        {
                            this.doComplete( a );
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.queue != null ? this.queue.size() : 0;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.DELAY_ERROR )
            {
                return this.delayError;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.worker : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void clear()
        {
            Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
        }

        public boolean isEmpty()
        {
            return this.queue.isEmpty();
        }

        @Nullable
        public T poll()
        {
            T v = this.queue.poll();
            if ( v != null && this.sourceMode != 1 )
            {
                long p = this.produced + 1L;
                if ( p == (long) this.limit )
                {
                    this.produced = 0L;
                    this.s.request( p );
                }
                else
                {
                    this.produced = p;
                }
            }

            return v;
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 2) != 0 )
            {
                this.outputFused = true;
                return 2;
            }
            else
            {
                return 0;
            }
        }

        public int size()
        {
            return this.queue.size();
        }
    }
}
