package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.KeyManagerFactorySpi;
import javax.net.ssl.ManagerFactoryParameters;

public final class OpenSslCachingX509KeyManagerFactory extends KeyManagerFactory
{
    public OpenSslCachingX509KeyManagerFactory( final KeyManagerFactory factory )
    {
        super( new KeyManagerFactorySpi()
        {
            protected void engineInit( KeyStore keyStore, char[] chars ) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException
            {
                factory.init( keyStore, chars );
            }

            protected void engineInit( ManagerFactoryParameters managerFactoryParameters ) throws InvalidAlgorithmParameterException
            {
                factory.init( managerFactoryParameters );
            }

            protected KeyManager[] engineGetKeyManagers()
            {
                return factory.getKeyManagers();
            }
        }, factory.getProvider(), factory.getAlgorithm() );
    }
}
