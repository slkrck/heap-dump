package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

public interface SynchronousSink<T>
{
    void complete();

    Context currentContext();

    void error( Throwable var1 );

    void next( T var1 );
}
