package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Subscription;

public abstract class BaseSubscriber<T> implements CoreSubscriber<T>, Subscription, Disposable
{
    static AtomicReferenceFieldUpdater<BaseSubscriber,Subscription> S =
            AtomicReferenceFieldUpdater.newUpdater( BaseSubscriber.class, Subscription.class, "subscription" );
    volatile Subscription subscription;

    protected Subscription upstream()
    {
        return this.subscription;
    }

    public boolean isDisposed()
    {
        return this.subscription == Operators.cancelledSubscription();
    }

    public void dispose()
    {
        this.cancel();
    }

    protected void hookOnSubscribe( Subscription subscription )
    {
        subscription.request( Long.MAX_VALUE );
    }

    protected void hookOnNext( T value )
    {
    }

    protected void hookOnComplete()
    {
    }

    protected void hookOnError( Throwable throwable )
    {
        throw Exceptions.errorCallbackNotImplemented( throwable );
    }

    protected void hookOnCancel()
    {
    }

    protected void hookFinally( SignalType type )
    {
    }

    public final void onSubscribe( Subscription s )
    {
        if ( Operators.setOnce( S, this, s ) )
        {
            try
            {
                this.hookOnSubscribe( s );
            }
            catch ( Throwable var3 )
            {
                this.onError( Operators.onOperatorError( s, var3, this.currentContext() ) );
            }
        }
    }

    public final void onNext( T value )
    {
        Objects.requireNonNull( value, "onNext" );

        try
        {
            this.hookOnNext( value );
        }
        catch ( Throwable var3 )
        {
            this.onError( Operators.onOperatorError( this.subscription, var3, value, this.currentContext() ) );
        }
    }

    public final void onError( Throwable t )
    {
        Objects.requireNonNull( t, "onError" );
        if ( S.getAndSet( this, Operators.cancelledSubscription() ) == Operators.cancelledSubscription() )
        {
            Operators.onErrorDropped( t, this.currentContext() );
        }
        else
        {
            try
            {
                this.hookOnError( t );
            }
            catch ( Throwable var6 )
            {
                Throwable e = Exceptions.addSuppressed( var6, t );
                Operators.onErrorDropped( e, this.currentContext() );
            }
            finally
            {
                this.safeHookFinally( SignalType.ON_ERROR );
            }
        }
    }

    public final void onComplete()
    {
        if ( S.getAndSet( this, Operators.cancelledSubscription() ) != Operators.cancelledSubscription() )
        {
            try
            {
                this.hookOnComplete();
            }
            catch ( Throwable var5 )
            {
                this.hookOnError( Operators.onOperatorError( var5, this.currentContext() ) );
            }
            finally
            {
                this.safeHookFinally( SignalType.ON_COMPLETE );
            }
        }
    }

    public final void request( long n )
    {
        if ( Operators.validate( n ) )
        {
            Subscription s = this.subscription;
            if ( s != null )
            {
                s.request( n );
            }
        }
    }

    public final void requestUnbounded()
    {
        this.request( Long.MAX_VALUE );
    }

    public final void cancel()
    {
        if ( Operators.terminate( S, this ) )
        {
            try
            {
                this.hookOnCancel();
            }
            catch ( Throwable var5 )
            {
                this.hookOnError( Operators.onOperatorError( this.subscription, var5, this.currentContext() ) );
            }
            finally
            {
                this.safeHookFinally( SignalType.CANCEL );
            }
        }
    }

    void safeHookFinally( SignalType type )
    {
        try
        {
            this.hookFinally( type );
        }
        catch ( Throwable var3 )
        {
            Operators.onErrorDropped( var3, this.currentContext() );
        }
    }

    public String toString()
    {
        return this.getClass().getSimpleName();
    }
}
