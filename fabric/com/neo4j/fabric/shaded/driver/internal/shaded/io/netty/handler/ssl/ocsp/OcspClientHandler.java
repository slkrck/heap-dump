package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.ocsp;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelInboundHandlerAdapter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.ReferenceCountedOpenSslEngine;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.SslHandshakeCompletionEvent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import javax.net.ssl.SSLHandshakeException;

public abstract class OcspClientHandler extends ChannelInboundHandlerAdapter
{
    private final ReferenceCountedOpenSslEngine engine;

    protected OcspClientHandler( ReferenceCountedOpenSslEngine engine )
    {
        this.engine = (ReferenceCountedOpenSslEngine) ObjectUtil.checkNotNull( engine, "engine" );
    }

    protected abstract boolean verify( ChannelHandlerContext var1, ReferenceCountedOpenSslEngine var2 ) throws Exception;

    public void userEventTriggered( ChannelHandlerContext ctx, Object evt ) throws Exception
    {
        if ( evt instanceof SslHandshakeCompletionEvent )
        {
            ctx.pipeline().remove( (ChannelHandler) this );
            SslHandshakeCompletionEvent event = (SslHandshakeCompletionEvent) evt;
            if ( event.isSuccess() && !this.verify( ctx, this.engine ) )
            {
                throw new SSLHandshakeException( "Bad OCSP response" );
            }
        }

        ctx.fireUserEventTriggered( evt );
    }
}
