package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

final class FluxBufferTimeout<T, C extends Collection<? super T>> extends InternalFluxOperator<T,C>
{
    final int batchSize;
    final Supplier<C> bufferSupplier;
    final Scheduler timer;
    final long timespan;

    FluxBufferTimeout( Flux<T> source, int maxSize, long timespan, Scheduler timer, Supplier<C> bufferSupplier )
    {
        super( source );
        if ( timespan <= 0L )
        {
            throw new IllegalArgumentException( "Timeout period must be strictly positive" );
        }
        else if ( maxSize <= 0 )
        {
            throw new IllegalArgumentException( "maxSize must be strictly positive" );
        }
        else
        {
            this.timer = (Scheduler) Objects.requireNonNull( timer, "Timer" );
            this.timespan = timespan;
            this.batchSize = maxSize;
            this.bufferSupplier = (Supplier) Objects.requireNonNull( bufferSupplier, "bufferSupplier" );
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super C> actual )
    {
        return new FluxBufferTimeout.BufferTimeoutSubscriber( Operators.serialize( actual ), this.batchSize, this.timespan, this.timer.createWorker(),
                this.bufferSupplier );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.timer : super.scanUnsafe( key );
    }

    static final class BufferTimeoutSubscriber<T, C extends Collection<? super T>> implements InnerOperator<T,C>
    {
        static final int NOT_TERMINATED = 0;
        static final int TERMINATED_WITH_SUCCESS = 1;
        static final int TERMINATED_WITH_ERROR = 2;
        static final int TERMINATED_WITH_CANCEL = 3;
        static final AtomicIntegerFieldUpdater<FluxBufferTimeout.BufferTimeoutSubscriber> TERMINATED =
                AtomicIntegerFieldUpdater.newUpdater( FluxBufferTimeout.BufferTimeoutSubscriber.class, "terminated" );
        static final AtomicLongFieldUpdater<FluxBufferTimeout.BufferTimeoutSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxBufferTimeout.BufferTimeoutSubscriber.class, "requested" );
        static final AtomicIntegerFieldUpdater<FluxBufferTimeout.BufferTimeoutSubscriber> INDEX =
                AtomicIntegerFieldUpdater.newUpdater( FluxBufferTimeout.BufferTimeoutSubscriber.class, "index" );
        final CoreSubscriber<? super C> actual;
        final int batchSize;
        final long timespan;
        final Scheduler.Worker timer;
        final Runnable flushTask;
        final Supplier<C> bufferSupplier;
        protected Subscription subscription;
        volatile int terminated = 0;
        volatile long requested;
        volatile int index = 0;
        volatile Disposable timespanRegistration;
        volatile C values;

        BufferTimeoutSubscriber( CoreSubscriber<? super C> actual, int maxSize, long timespan, Scheduler.Worker timer, Supplier<C> bufferSupplier )
        {
            this.actual = actual;
            this.timespan = timespan;
            this.timer = timer;
            this.flushTask = () -> {
                if ( this.terminated == 0 )
                {
                    int index;
                    do
                    {
                        index = this.index;
                        if ( index == 0 )
                        {
                            return;
                        }
                    }
                    while ( !INDEX.compareAndSet( this, index, 0 ) );

                    this.flushCallback( (Object) null );
                }
            };
            this.batchSize = maxSize;
            this.bufferSupplier = bufferSupplier;
        }

        protected void doOnSubscribe()
        {
            this.values = (Collection) this.bufferSupplier.get();
        }

        void nextCallback( T value )
        {
            synchronized ( this )
            {
                C v = this.values;
                if ( v == null )
                {
                    v = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null buffer" );
                    this.values = v;
                }

                v.add( value );
            }
        }

        void flushCallback( @Nullable T ev )
        {
            boolean flush = false;
            Collection v;
            synchronized ( this )
            {
                v = this.values;
                if ( v != null && !v.isEmpty() )
                {
                    this.values = (Collection) this.bufferSupplier.get();
                    flush = true;
                }
            }

            if ( flush )
            {
                long r = this.requested;
                if ( r != 0L )
                {
                    if ( r == Long.MAX_VALUE )
                    {
                        this.actual.onNext( v );
                        return;
                    }

                    do
                    {
                        long next = r - 1L;
                        if ( REQUESTED.compareAndSet( this, r, next ) )
                        {
                            this.actual.onNext( v );
                            return;
                        }

                        r = this.requested;
                    }
                    while ( r > 0L );
                }

                this.cancel();
                this.actual.onError( Exceptions.failWithOverflow( "Could not emit buffer due to lack of requests" ) );
                Operators.onDiscardMultiple( v, this.actual.currentContext() );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.subscription;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.terminated == 3;
            }
            else if ( key != Scannable.Attr.TERMINATED )
            {
                if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
                {
                    return this.requested;
                }
                else if ( key == Scannable.Attr.CAPACITY )
                {
                    return this.batchSize;
                }
                else if ( key == Scannable.Attr.BUFFERED )
                {
                    return this.batchSize - this.index;
                }
                else
                {
                    return key == Scannable.Attr.RUN_ON ? this.timer : InnerOperator.super.scanUnsafe( key );
                }
            }
            else
            {
                return this.terminated == 2 || this.terminated == 1;
            }
        }

        public void onNext( T value )
        {
            int index;
            do
            {
                index = this.index + 1;
            }
            while ( !INDEX.compareAndSet( this, index - 1, index ) );

            if ( index == 1 )
            {
                try
                {
                    this.timespanRegistration = this.timer.schedule( this.flushTask, this.timespan, TimeUnit.MILLISECONDS );
                }
                catch ( RejectedExecutionException var5 )
                {
                    Context ctx = this.actual.currentContext();
                    this.onError( Operators.onRejectedExecution( var5, this.subscription, (Throwable) null, value, ctx ) );
                    Operators.onDiscard( value, ctx );
                    return;
                }
            }

            this.nextCallback( value );
            if ( this.index % this.batchSize == 0 )
            {
                this.index = 0;
                if ( this.timespanRegistration != null )
                {
                    this.timespanRegistration.dispose();
                    this.timespanRegistration = null;
                }

                this.flushCallback( value );
            }
        }

        void checkedComplete()
        {
            try
            {
                this.flushCallback( (Object) null );
            }
            finally
            {
                this.actual.onComplete();
            }
        }

        final boolean isCompleted()
        {
            return this.terminated == 1;
        }

        final boolean isFailed()
        {
            return this.terminated == 2;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                if ( this.terminated != 0 )
                {
                    return;
                }

                if ( this.batchSize != Integer.MAX_VALUE && n != Long.MAX_VALUE )
                {
                    this.requestMore( Operators.multiplyCap( n, (long) this.batchSize ) );
                }
                else
                {
                    this.requestMore( Long.MAX_VALUE );
                }
            }
        }

        final void requestMore( long n )
        {
            Subscription s = this.subscription;
            if ( s != null )
            {
                s.request( n );
            }
        }

        public CoreSubscriber<? super C> actual()
        {
            return this.actual;
        }

        public void onComplete()
        {
            if ( TERMINATED.compareAndSet( this, 0, 1 ) )
            {
                this.timer.dispose();
                this.checkedComplete();
            }
        }

        public void onError( Throwable throwable )
        {
            if ( TERMINATED.compareAndSet( this, 0, 2 ) )
            {
                this.timer.dispose();
                Context ctx = this.actual.currentContext();
                synchronized ( this )
                {
                    C v = this.values;
                    if ( v != null )
                    {
                        Operators.onDiscardMultiple( v, ctx );
                        v.clear();
                        this.values = null;
                    }
                }

                this.actual.onError( throwable );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.subscription, s ) )
            {
                this.subscription = s;
                this.doOnSubscribe();
                this.actual.onSubscribe( this );
            }
        }

        public void cancel()
        {
            if ( TERMINATED.compareAndSet( this, 0, 3 ) )
            {
                this.timer.dispose();
                Subscription s = this.subscription;
                if ( s != null )
                {
                    this.subscription = null;
                    s.cancel();
                }

                C v = this.values;
                if ( v != null )
                {
                    Operators.onDiscardMultiple( v, this.actual.currentContext() );
                    v.clear();
                }
            }
        }
    }
}
