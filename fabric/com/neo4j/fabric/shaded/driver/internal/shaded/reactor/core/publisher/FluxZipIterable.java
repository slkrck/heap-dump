package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.BiFunction;

import org.reactivestreams.Subscription;

final class FluxZipIterable<T, U, R> extends InternalFluxOperator<T,R>
{
    final Iterable<? extends U> other;
    final BiFunction<? super T,? super U,? extends R> zipper;

    FluxZipIterable( Flux<? extends T> source, Iterable<? extends U> other, BiFunction<? super T,? super U,? extends R> zipper )
    {
        super( source );
        this.other = (Iterable) Objects.requireNonNull( other, "other" );
        this.zipper = (BiFunction) Objects.requireNonNull( zipper, "zipper" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        Iterator it;
        try
        {
            it = (Iterator) Objects.requireNonNull( this.other.iterator(), "The other iterable produced a null iterator" );
        }
        catch ( Throwable var6 )
        {
            Operators.error( actual, Operators.onOperatorError( var6, actual.currentContext() ) );
            return null;
        }

        boolean b;
        try
        {
            b = it.hasNext();
        }
        catch ( Throwable var5 )
        {
            Operators.error( actual, Operators.onOperatorError( var5, actual.currentContext() ) );
            return null;
        }

        if ( !b )
        {
            Operators.complete( actual );
            return null;
        }
        else
        {
            return new FluxZipIterable.ZipSubscriber( actual, it, this.zipper );
        }
    }

    static final class ZipSubscriber<T, U, R> implements InnerOperator<T,R>
    {
        final CoreSubscriber<? super R> actual;
        final Iterator<? extends U> it;
        final BiFunction<? super T,? super U,? extends R> zipper;
        Subscription s;
        boolean done;

        ZipSubscriber( CoreSubscriber<? super R> actual, Iterator<? extends U> it, BiFunction<? super T,? super U,? extends R> zipper )
        {
            this.actual = actual;
            this.it = it;
            this.zipper = zipper;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                Object u;
                try
                {
                    u = this.it.next();
                }
                catch ( Throwable var8 )
                {
                    this.done = true;
                    this.actual.onError( Operators.onOperatorError( this.s, var8, t, this.actual.currentContext() ) );
                    return;
                }

                Object r;
                try
                {
                    r = Objects.requireNonNull( this.zipper.apply( t, u ), "The zipper returned a null value" );
                }
                catch ( Throwable var7 )
                {
                    this.done = true;
                    this.actual.onError( Operators.onOperatorError( this.s, var7, t, this.actual.currentContext() ) );
                    return;
                }

                this.actual.onNext( r );

                boolean b;
                try
                {
                    b = this.it.hasNext();
                }
                catch ( Throwable var6 )
                {
                    this.done = true;
                    this.actual.onError( Operators.onOperatorError( this.s, var6, t, this.actual.currentContext() ) );
                    return;
                }

                if ( !b )
                {
                    this.done = true;
                    this.s.cancel();
                    this.actual.onComplete();
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
