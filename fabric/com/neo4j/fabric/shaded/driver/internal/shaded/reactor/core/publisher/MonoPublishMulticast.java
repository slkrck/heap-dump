package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Subscription;

final class MonoPublishMulticast<T, R> extends InternalMonoOperator<T,R> implements Fuseable
{
    final Function<? super Mono<T>,? extends Mono<? extends R>> transform;

    MonoPublishMulticast( Mono<? extends T> source, Function<? super Mono<T>,? extends Mono<? extends R>> transform )
    {
        super( source );
        this.transform = (Function) Objects.requireNonNull( transform, "transform" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        MonoPublishMulticast.MonoPublishMulticaster multicast = new MonoPublishMulticast.MonoPublishMulticaster( actual.currentContext() );

        Mono out;
        try
        {
            out = (Mono) Objects.requireNonNull( this.transform.apply( fromDirect( multicast ) ), "The transform returned a null Mono" );
        }
        catch ( Throwable var5 )
        {
            Operators.error( actual, Operators.onOperatorError( var5, actual.currentContext() ) );
            return null;
        }

        if ( out instanceof Fuseable )
        {
            out.subscribe( (CoreSubscriber) (new FluxPublishMulticast.CancelFuseableMulticaster( actual, multicast )) );
        }
        else
        {
            out.subscribe( (CoreSubscriber) (new FluxPublishMulticast.CancelMulticaster( actual, multicast )) );
        }

        return multicast;
    }

    static final class PublishMulticastInner<T> implements InnerProducer<T>
    {
        static final AtomicIntegerFieldUpdater<MonoPublishMulticast.PublishMulticastInner> CANCELLED =
                AtomicIntegerFieldUpdater.newUpdater( MonoPublishMulticast.PublishMulticastInner.class, "cancelled" );
        final MonoPublishMulticast.MonoPublishMulticaster<T> parent;
        final CoreSubscriber<? super T> actual;
        volatile int cancelled;

        PublishMulticastInner( MonoPublishMulticast.MonoPublishMulticaster<T> parent, CoreSubscriber<? super T> actual )
        {
            this.parent = parent;
            this.actual = actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.cancelled == 1 : InnerProducer.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                this.parent.drain();
            }
        }

        public void cancel()
        {
            if ( CANCELLED.compareAndSet( this, 0, 1 ) )
            {
                this.parent.remove( this );
                this.parent.drain();
            }
        }
    }

    static final class MonoPublishMulticaster<T> extends Mono<T> implements InnerConsumer<T>, FluxPublishMulticast.PublishMulticasterParent
    {
        static final AtomicReferenceFieldUpdater<MonoPublishMulticast.MonoPublishMulticaster,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoPublishMulticast.MonoPublishMulticaster.class, Subscription.class, "s" );
        static final AtomicIntegerFieldUpdater<MonoPublishMulticast.MonoPublishMulticaster> WIP =
                AtomicIntegerFieldUpdater.newUpdater( MonoPublishMulticast.MonoPublishMulticaster.class, "wip" );
        static final AtomicReferenceFieldUpdater<MonoPublishMulticast.MonoPublishMulticaster,MonoPublishMulticast.PublishMulticastInner[]> SUBSCRIBERS =
                AtomicReferenceFieldUpdater.newUpdater( MonoPublishMulticast.MonoPublishMulticaster.class, MonoPublishMulticast.PublishMulticastInner[].class,
                        "subscribers" );
        static final MonoPublishMulticast.PublishMulticastInner[] EMPTY = new MonoPublishMulticast.PublishMulticastInner[0];
        static final MonoPublishMulticast.PublishMulticastInner[] TERMINATED = new MonoPublishMulticast.PublishMulticastInner[0];
        final Context context;
        volatile Subscription s;
        volatile int wip;
        volatile MonoPublishMulticast.PublishMulticastInner<T>[] subscribers;
        volatile boolean done;
        @Nullable
        T value;
        Throwable error;
        volatile boolean connected;

        MonoPublishMulticaster( Context ctx )
        {
            SUBSCRIBERS.lazySet( this, EMPTY );
            this.context = ctx;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return 1;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.value != null ? 1 : 0 : null;
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        public Context currentContext()
        {
            return this.context;
        }

        public void subscribe( CoreSubscriber<? super T> actual )
        {
            MonoPublishMulticast.PublishMulticastInner<T> pcs = new MonoPublishMulticast.PublishMulticastInner( this, actual );
            actual.onSubscribe( pcs );
            if ( this.add( pcs ) )
            {
                if ( pcs.cancelled == 1 )
                {
                    this.remove( pcs );
                    return;
                }

                this.drain();
            }
            else
            {
                Throwable ex = this.error;
                if ( ex != null )
                {
                    actual.onError( ex );
                }
                else
                {
                    actual.onComplete();
                }
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                this.connected = true;
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.context );
            }
            else
            {
                this.value = t;
                this.done = true;
                this.drain();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.context );
            }
            else
            {
                this.error = t;
                this.done = true;
                this.drain();
            }
        }

        public void onComplete()
        {
            this.done = true;
            this.drain();
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                int missed = 1;

                do
                {
                    if ( this.connected )
                    {
                        if ( this.s == Operators.cancelledSubscription() )
                        {
                            this.value = null;
                            return;
                        }

                        T v = this.value;
                        MonoPublishMulticast.PublishMulticastInner<T>[] a = this.subscribers;
                        int n = a.length;
                        if ( n != 0 )
                        {
                            if ( this.s == Operators.cancelledSubscription() )
                            {
                                this.value = null;
                                return;
                            }

                            MonoPublishMulticast.PublishMulticastInner[] castedArray;
                            int i;
                            if ( v == null )
                            {
                                castedArray = (MonoPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                a = castedArray;
                                n = castedArray.length;

                                for ( i = 0; i < n; ++i )
                                {
                                    a[i].actual.onComplete();
                                }

                                return;
                            }

                            castedArray = (MonoPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                            a = castedArray;
                            n = castedArray.length;

                            for ( i = 0; i < n; ++i )
                            {
                                a[i].actual.onNext( v );
                                a[i].actual.onComplete();
                            }

                            this.value = null;
                            return;
                        }
                    }

                    missed = WIP.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }

        boolean add( MonoPublishMulticast.PublishMulticastInner<T> s )
        {
            MonoPublishMulticast.PublishMulticastInner[] a;
            MonoPublishMulticast.PublishMulticastInner[] b;
            do
            {
                a = this.subscribers;
                if ( a == TERMINATED )
                {
                    return false;
                }

                int n = a.length;
                b = new MonoPublishMulticast.PublishMulticastInner[n + 1];
                System.arraycopy( a, 0, b, 0, n );
                b[n] = s;
            }
            while ( !SUBSCRIBERS.compareAndSet( this, a, b ) );

            return true;
        }

        void remove( MonoPublishMulticast.PublishMulticastInner<T> s )
        {
            while ( true )
            {
                MonoPublishMulticast.PublishMulticastInner<T>[] a = this.subscribers;
                if ( a != TERMINATED && a != EMPTY )
                {
                    int n = a.length;
                    int j = -1;

                    for ( int i = 0; i < n; ++i )
                    {
                        if ( a[i] == s )
                        {
                            j = i;
                            break;
                        }
                    }

                    if ( j < 0 )
                    {
                        return;
                    }

                    MonoPublishMulticast.PublishMulticastInner[] b;
                    if ( n == 1 )
                    {
                        b = EMPTY;
                    }
                    else
                    {
                        b = new MonoPublishMulticast.PublishMulticastInner[n - 1];
                        System.arraycopy( a, 0, b, 0, j );
                        System.arraycopy( a, j + 1, b, j, n - j - 1 );
                    }

                    if ( !SUBSCRIBERS.compareAndSet( this, a, b ) )
                    {
                        continue;
                    }

                    return;
                }

                return;
            }
        }

        public void terminate()
        {
            Operators.terminate( S, this );
            if ( WIP.getAndIncrement( this ) == 0 && this.connected )
            {
                this.value = null;
            }
        }
    }
}
