package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

final class BlockingLastSubscriber<T> extends BlockingSingleSubscriber<T>
{
    public void onNext( T t )
    {
        this.value = t;
    }

    public void onError( Throwable t )
    {
        this.value = null;
        this.error = t;
        this.countDown();
    }
}
