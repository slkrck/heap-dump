package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.time.Duration;
import java.util.Objects;

final class MonoError<T> extends Mono<T> implements Fuseable.ScalarCallable, SourceProducer<T>
{
    final Throwable error;

    MonoError( Throwable error )
    {
        this.error = (Throwable) Objects.requireNonNull( error, "error" );
    }

    public T block( Duration m )
    {
        throw Exceptions.propagate( this.error );
    }

    public T block()
    {
        throw Exceptions.propagate( this.error );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Operators.error( actual, this.error );
    }

    public Object call() throws Exception
    {
        if ( this.error instanceof Exception )
        {
            throw (Exception) this.error;
        }
        else
        {
            throw Exceptions.propagate( this.error );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
