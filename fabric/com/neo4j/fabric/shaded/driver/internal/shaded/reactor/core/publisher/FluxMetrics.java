package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Logger;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Loggers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;
import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.Timer.Sample;

import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxMetrics<T> extends InternalFluxOperator<T,T>
{
    static final String REACTOR_DEFAULT_NAME = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor";
    static final String METER_MALFORMED = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.malformed.source";
    static final String METER_SUBSCRIBED = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.subscribed";
    static final String METER_FLOW_DURATION = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.flow.duration";
    static final String METER_ON_NEXT_DELAY = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.onNext.delay";
    static final String METER_REQUESTED = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.requested";
    static final String TAG_KEY_EXCEPTION = "exception";
    static final String TAG_SEQUENCE_NAME = "flow";
    static final Tags DEFAULT_TAGS_FLUX = Tags.of( "type", "Flux" );
    static final Tags DEFAULT_TAGS_MONO = Tags.of( "type", "Mono" );
    static final Tag TAG_ON_ERROR = Tag.of( "status", "error" );
    static final Tags TAG_ON_COMPLETE = Tags.of( new String[]{"status", "completed", "exception", ""} );
    static final Tags TAG_CANCEL = Tags.of( new String[]{"status", "cancelled", "exception", ""} );
    static final Logger log = Loggers.getLogger( FluxMetrics.class );
    static final BiFunction<Tags,Tuple2<String,String>,Tags> TAG_ACCUMULATOR = ( prev, tuple ) -> {
        return prev.and( new Tag[]{Tag.of( (String) tuple.getT1(), (String) tuple.getT2() )} );
    };
    static final BinaryOperator<Tags> TAG_COMBINER = Tags::and;
    final String name;
    final Tags tags;
    final MeterRegistry registryCandidate;

    FluxMetrics( Flux<? extends T> flux )
    {
        this( flux, (MeterRegistry) null );
    }

    FluxMetrics( Flux<? extends T> flux, @Nullable MeterRegistry registry )
    {
        super( flux );
        this.name = resolveName( flux );
        this.tags = resolveTags( flux, DEFAULT_TAGS_FLUX, this.name );
        if ( registry == null )
        {
            this.registryCandidate = Metrics.globalRegistry;
        }
        else
        {
            this.registryCandidate = registry;
        }
    }

    static String resolveName( Publisher<?> source )
    {
        Scannable scannable = Scannable.from( source );
        if ( scannable.isScanAvailable() )
        {
            String nameOrDefault = scannable.name();
            return scannable.stepName().equals( nameOrDefault ) ? "com.neo4j.fabric.shaded.driver.internal.shaded.reactor" : nameOrDefault;
        }
        else
        {
            log.warn(
                    "Attempting to activate metrics but the upstream is not Scannable. You might want to use `name()` (and optionally `tags()`) right before `metrics()`" );
            return "com.neo4j.fabric.shaded.driver.internal.shaded.reactor";
        }
    }

    static Tags resolveTags( Publisher<?> source, Tags tags, String sequenceName )
    {
        Scannable scannable = Scannable.from( source );
        tags = tags.and( new Tag[]{Tag.of( "flow", sequenceName )} );
        return scannable.isScanAvailable() ? (Tags) scannable.tags().reduce( tags, TAG_ACCUMULATOR, TAG_COMBINER ) : tags;
    }

    static void recordCancel( Tags commonTags, MeterRegistry registry, Sample flowDuration )
    {
        Timer timer = Timer.builder( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.flow.duration" ).tags( commonTags.and( TAG_CANCEL ) ).description(
                "Times the duration elapsed between a subscription and the cancellation of the sequence" ).register( registry );
        flowDuration.stop( timer );
    }

    static void recordMalformed( Tags commonTags, MeterRegistry registry )
    {
        registry.counter( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.malformed.source", commonTags ).increment();
    }

    static void recordOnError( Tags commonTags, MeterRegistry registry, Sample flowDuration, Throwable e )
    {
        Timer timer =
                Timer.builder( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.flow.duration" ).tags( commonTags.and( new Tag[]{TAG_ON_ERROR} ) ).tag(
                        "exception", e.getClass().getName() ).description(
                        "Times the duration elapsed between a subscription and the onError termination of the sequence, with the exception name as a tag." ).register(
                        registry );
        flowDuration.stop( timer );
    }

    static void recordOnComplete( Tags commonTags, MeterRegistry registry, Sample flowDuration )
    {
        Timer timer =
                Timer.builder( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.flow.duration" ).tags( commonTags.and( TAG_ON_COMPLETE ) ).description(
                        "Times the duration elapsed between a subscription and the onComplete termination of the sequence" ).register( registry );
        flowDuration.stop( timer );
    }

    static void recordOnSubscribe( Tags commonTags, MeterRegistry registry )
    {
        Counter.builder( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.subscribed" ).tags( commonTags ).baseUnit( "subscribers" ).description(
                "Counts how many Reactor sequences have been subscribed to" ).register( registry ).increment();
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxMetrics.MetricsSubscriber( actual, this.registryCandidate, Clock.SYSTEM, this.name, this.tags );
    }

    static class MetricsSubscriber<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        final Clock clock;
        final Tags commonTags;
        final MeterRegistry registry;
        final DistributionSummary requestedCounter;
        final Timer onNextIntervalTimer;
        Sample subscribeToTerminateSample;
        long lastNextEventNanos = -1L;
        boolean done;
        Subscription s;

        MetricsSubscriber( CoreSubscriber<? super T> actual, MeterRegistry registry, Clock clock, String sequenceName, Tags commonTags )
        {
            this.actual = actual;
            this.clock = clock;
            this.commonTags = commonTags;
            this.registry = registry;
            this.onNextIntervalTimer = Timer.builder( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.onNext.delay" ).tags( commonTags ).description(
                    "Measures delays between onNext signals (or between onSubscribe and first onNext)" ).register( registry );
            if ( !"com.neo4j.fabric.shaded.driver.internal.shaded.reactor".equals( sequenceName ) )
            {
                this.requestedCounter =
                        DistributionSummary.builder( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.requested" ).tags( commonTags ).description(
                                "Counts the amount requested to a named Flux by all subscribers, until at least one requests an unbounded amount" ).baseUnit(
                                "requested amount" ).register( registry );
            }
            else
            {
                this.requestedCounter = null;
            }
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public final void cancel()
        {
            FluxMetrics.recordCancel( this.commonTags, this.registry, this.subscribeToTerminateSample );
            this.s.cancel();
        }

        public final void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                FluxMetrics.recordOnComplete( this.commonTags, this.registry, this.subscribeToTerminateSample );
                this.actual.onComplete();
            }
        }

        public final void onError( Throwable e )
        {
            if ( this.done )
            {
                FluxMetrics.recordMalformed( this.commonTags, this.registry );
                Operators.onErrorDropped( e, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                FluxMetrics.recordOnError( this.commonTags, this.registry, this.subscribeToTerminateSample, e );
                this.actual.onError( e );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                FluxMetrics.recordMalformed( this.commonTags, this.registry );
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                long last = this.lastNextEventNanos;
                this.lastNextEventNanos = this.clock.monotonicTime();
                this.onNextIntervalTimer.record( this.lastNextEventNanos - last, TimeUnit.NANOSECONDS );
                this.actual.onNext( t );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                FluxMetrics.recordOnSubscribe( this.commonTags, this.registry );
                this.subscribeToTerminateSample = Timer.start( this.clock );
                this.lastNextEventNanos = this.clock.monotonicTime();
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public final void request( long l )
        {
            if ( Operators.validate( l ) )
            {
                if ( this.requestedCounter != null )
                {
                    this.requestedCounter.record( (double) l );
                }

                this.s.request( l );
            }
        }
    }
}
