package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.Consumer;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxOnBackpressureBufferStrategy<O> extends InternalFluxOperator<O,O>
{
    final Consumer<? super O> onBufferOverflow;
    final int bufferSize;
    final boolean delayError;
    final BufferOverflowStrategy bufferOverflowStrategy;

    FluxOnBackpressureBufferStrategy( Flux<? extends O> source, int bufferSize, @Nullable Consumer<? super O> onBufferOverflow,
            BufferOverflowStrategy bufferOverflowStrategy )
    {
        super( source );
        this.bufferSize = bufferSize;
        this.onBufferOverflow = onBufferOverflow;
        this.bufferOverflowStrategy = bufferOverflowStrategy;
        this.delayError = onBufferOverflow != null || bufferOverflowStrategy == BufferOverflowStrategy.ERROR;
    }

    public CoreSubscriber<? super O> subscribeOrReturn( CoreSubscriber<? super O> actual )
    {
        return new FluxOnBackpressureBufferStrategy.BackpressureBufferDropOldestSubscriber( actual, this.bufferSize, this.delayError, this.onBufferOverflow,
                this.bufferOverflowStrategy );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    static final class BackpressureBufferDropOldestSubscriber<T> extends ArrayDeque<T> implements InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxOnBackpressureBufferStrategy.BackpressureBufferDropOldestSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxOnBackpressureBufferStrategy.BackpressureBufferDropOldestSubscriber.class, "wip" );
        static final AtomicLongFieldUpdater<FluxOnBackpressureBufferStrategy.BackpressureBufferDropOldestSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxOnBackpressureBufferStrategy.BackpressureBufferDropOldestSubscriber.class, "requested" );
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        final int bufferSize;
        final Consumer<? super T> onOverflow;
        final boolean delayError;
        final BufferOverflowStrategy overflowStrategy;
        Subscription s;
        volatile boolean cancelled;
        volatile boolean done;
        Throwable error;
        volatile int wip;
        volatile long requested;

        BackpressureBufferDropOldestSubscriber( CoreSubscriber<? super T> actual, int bufferSize, boolean delayError, @Nullable Consumer<? super T> onOverflow,
                BufferOverflowStrategy overflowStrategy )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.delayError = delayError;
            this.onOverflow = onOverflow;
            this.overflowStrategy = overflowStrategy;
            this.bufferSize = bufferSize;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key != Scannable.Attr.TERMINATED )
            {
                if ( key == Scannable.Attr.CANCELLED )
                {
                    return this.cancelled;
                }
                else if ( key == Scannable.Attr.BUFFERED )
                {
                    return this.size();
                }
                else if ( key == Scannable.Attr.ERROR )
                {
                    return this.error;
                }
                else if ( key == Scannable.Attr.PREFETCH )
                {
                    return Integer.MAX_VALUE;
                }
                else
                {
                    return key == Scannable.Attr.DELAY_ERROR ? this.delayError : InnerOperator.super.scanUnsafe( key );
                }
            }
            else
            {
                return this.done && this.isEmpty();
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
            }
            else
            {
                boolean callOnOverflow = false;
                boolean callOnError = false;
                T overflowElement = t;
                synchronized ( this )
                {
                    if ( this.size() == this.bufferSize )
                    {
                        callOnOverflow = true;
                        switch ( this.overflowStrategy )
                        {
                        case DROP_OLDEST:
                            overflowElement = this.pollFirst();
                            this.offer( t );
                        case DROP_LATEST:
                            break;
                        case ERROR:
                        default:
                            callOnError = true;
                        }
                    }
                    else
                    {
                        this.offer( t );
                    }
                }

                if ( callOnOverflow )
                {
                    if ( this.onOverflow != null )
                    {
                        label112:
                        {
                            try
                            {
                                this.onOverflow.accept( overflowElement );
                                break label112;
                            }
                            catch ( Throwable var12 )
                            {
                                Throwable ex = Operators.onOperatorError( this.s, var12, overflowElement, this.ctx );
                                this.onError( ex );
                            }
                            finally
                            {
                                Operators.onDiscard( overflowElement, this.ctx );
                            }

                            return;
                        }
                    }
                    else
                    {
                        Operators.onDiscard( overflowElement, this.ctx );
                    }
                }

                if ( callOnError )
                {
                    Throwable ex = Operators.onOperatorError( this.s, Exceptions.failWithOverflow(), overflowElement, this.ctx );
                    this.onError( ex );
                }

                if ( !callOnError && !callOnOverflow )
                {
                    this.drain();
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.error = t;
                this.done = true;
                this.drain();
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.drain();
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                int missed = 1;

                do
                {
                    Subscriber<? super T> a = this.actual;
                    if ( a != null )
                    {
                        this.innerDrain( a );
                        return;
                    }

                    missed = WIP.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }

        void innerDrain( Subscriber<? super T> a )
        {
            int missed = 1;

            do
            {
                long r = this.requested;

                long e;
                boolean empty;
                for ( e = 0L; r != e; ++e )
                {
                    empty = this.done;
                    Object t;
                    synchronized ( this )
                    {
                        t = this.poll();
                    }

                    boolean empty = t == null;
                    if ( this.checkTerminated( empty, empty, a ) )
                    {
                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    a.onNext( t );
                }

                if ( r == e )
                {
                    synchronized ( this )
                    {
                        empty = this.isEmpty();
                    }

                    if ( this.checkTerminated( this.done, empty, a ) )
                    {
                        return;
                    }
                }

                if ( e != 0L && r != Long.MAX_VALUE )
                {
                    Operators.produced( REQUESTED, this, e );
                }

                missed = WIP.addAndGet( this, -missed );
            }
            while ( missed != 0 );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.drain();
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    synchronized ( this )
                    {
                        this.clear();
                    }
                }
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        boolean checkTerminated( boolean d, boolean empty, Subscriber<? super T> a )
        {
            if ( this.cancelled )
            {
                this.s.cancel();
                synchronized ( this )
                {
                    this.clear();
                    return true;
                }
            }
            else
            {
                if ( d )
                {
                    Throwable e;
                    if ( this.delayError )
                    {
                        if ( empty )
                        {
                            e = this.error;
                            if ( e != null )
                            {
                                a.onError( e );
                            }
                            else
                            {
                                a.onComplete();
                            }

                            return true;
                        }
                    }
                    else
                    {
                        e = this.error;
                        if ( e != null )
                        {
                            synchronized ( this )
                            {
                                this.clear();
                            }

                            a.onError( e );
                            return true;
                        }

                        if ( empty )
                        {
                            a.onComplete();
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        public void clear()
        {
            Operators.onDiscardMultiple( (Collection) this, this.ctx );
            super.clear();
        }
    }
}
