package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public interface Signal<T> extends Supplier<T>, Consumer<Subscriber<? super T>>
{
    static <T> Signal<T> complete()
    {
        return ImmutableSignal.ON_COMPLETE;
    }

    static <T> Signal<T> complete( Context context )
    {
        return (Signal) (context.isEmpty() ? ImmutableSignal.ON_COMPLETE
                                           : new ImmutableSignal( context, SignalType.ON_COMPLETE, (Object) null, (Throwable) null, (Subscription) null ));
    }

    static <T> Signal<T> error( Throwable e )
    {
        return error( e, Context.empty() );
    }

    static <T> Signal<T> error( Throwable e, Context context )
    {
        return new ImmutableSignal( context, SignalType.ON_ERROR, (Object) null, e, (Subscription) null );
    }

    static <T> Signal<T> next( T t )
    {
        return next( t, Context.empty() );
    }

    static <T> Signal<T> next( T t, Context context )
    {
        return new ImmutableSignal( context, SignalType.ON_NEXT, t, (Throwable) null, (Subscription) null );
    }

    static <T> Signal<T> subscribe( Subscription subscription )
    {
        return subscribe( subscription, Context.empty() );
    }

    static <T> Signal<T> subscribe( Subscription subscription, Context context )
    {
        return new ImmutableSignal( context, SignalType.ON_SUBSCRIBE, (Object) null, (Throwable) null, subscription );
    }

    static boolean isComplete( Object o )
    {
        return o == ImmutableSignal.ON_COMPLETE || o instanceof Signal && ((Signal) o).getType() == SignalType.ON_COMPLETE;
    }

    static boolean isError( Object o )
    {
        return o instanceof Signal && ((Signal) o).getType() == SignalType.ON_ERROR;
    }

    @Nullable
    Throwable getThrowable();

    @Nullable
    Subscription getSubscription();

    @Nullable
    T get();

    default boolean hasValue()
    {
        return this.isOnNext() && this.get() != null;
    }

    default boolean hasError()
    {
        return this.isOnError() && this.getThrowable() != null;
    }

    SignalType getType();

    Context getContext();

    default boolean isOnError()
    {
        return this.getType() == SignalType.ON_ERROR;
    }

    default boolean isOnComplete()
    {
        return this.getType() == SignalType.ON_COMPLETE;
    }

    default boolean isOnSubscribe()
    {
        return this.getType() == SignalType.ON_SUBSCRIBE;
    }

    default boolean isOnNext()
    {
        return this.getType() == SignalType.ON_NEXT;
    }

    default void accept( Subscriber<? super T> observer )
    {
        if ( this.isOnNext() )
        {
            observer.onNext( this.get() );
        }
        else if ( this.isOnComplete() )
        {
            observer.onComplete();
        }
        else if ( this.isOnError() )
        {
            observer.onError( this.getThrowable() );
        }
        else if ( this.isOnSubscribe() )
        {
            observer.onSubscribe( this.getSubscription() );
        }
    }
}
