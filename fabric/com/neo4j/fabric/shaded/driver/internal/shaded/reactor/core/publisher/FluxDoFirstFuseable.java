package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

final class FluxDoFirstFuseable<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    final Runnable onFirst;

    FluxDoFirstFuseable( Flux<? extends T> fuseableSource, Runnable onFirst )
    {
        super( fuseableSource );
        this.onFirst = onFirst;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        try
        {
            this.onFirst.run();
            return actual;
        }
        catch ( Throwable var3 )
        {
            Operators.error( actual, var3 );
            return null;
        }
    }
}
