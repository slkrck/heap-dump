package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class MonoSourceFlux<I> extends MonoFromFluxOperator<I,I>
{
    MonoSourceFlux( Flux<? extends I> source )
    {
        super( source );
    }

    public CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super I> actual )
    {
        return actual;
    }
}
