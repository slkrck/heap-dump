package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.Callable;

final class FluxCallable<T> extends Flux<T> implements Callable<T>, Fuseable, SourceProducer<T>
{
    final Callable<T> callable;

    FluxCallable( Callable<T> callable )
    {
        this.callable = callable;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Operators.MonoSubscriber<T,T> wrapper = new Operators.MonoSubscriber( actual );
        actual.onSubscribe( wrapper );

        try
        {
            T v = this.callable.call();
            if ( v == null )
            {
                wrapper.onComplete();
            }
            else
            {
                wrapper.complete( v );
            }
        }
        catch ( Throwable var4 )
        {
            actual.onError( Operators.onOperatorError( var4, actual.currentContext() ) );
        }
    }

    @Nullable
    public T call() throws Exception
    {
        return this.callable.call();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
