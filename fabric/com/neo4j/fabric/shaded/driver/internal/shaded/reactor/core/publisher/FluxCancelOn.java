package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

import org.reactivestreams.Subscription;

final class FluxCancelOn<T> extends InternalFluxOperator<T,T>
{
    final Scheduler scheduler;

    public FluxCancelOn( Flux<T> source, Scheduler scheduler )
    {
        super( source );
        this.scheduler = (Scheduler) Objects.requireNonNull( scheduler, "scheduler" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxCancelOn.CancelSubscriber( actual, this.scheduler );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }

    static final class CancelSubscriber<T> implements InnerOperator<T,T>, Runnable
    {
        static final AtomicIntegerFieldUpdater<FluxCancelOn.CancelSubscriber> CANCELLED =
                AtomicIntegerFieldUpdater.newUpdater( FluxCancelOn.CancelSubscriber.class, "cancelled" );
        final CoreSubscriber<? super T> actual;
        final Scheduler scheduler;
        Subscription s;
        volatile int cancelled = 0;

        CancelSubscriber( CoreSubscriber<? super T> actual, Scheduler scheduler )
        {
            this.actual = actual;
            this.scheduler = scheduler;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled == 1;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.scheduler : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void run()
        {
            this.s.cancel();
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            if ( CANCELLED.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.scheduler.schedule( this );
                }
                catch ( RejectedExecutionException var2 )
                {
                    throw Operators.onRejectedExecution( var2, this.actual.currentContext() );
                }
            }
        }
    }
}
