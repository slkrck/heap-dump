package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Subscription;

final class MonoFlatMap<T, R> extends InternalMonoOperator<T,R> implements Fuseable
{
    final Function<? super T,? extends Mono<? extends R>> mapper;

    MonoFlatMap( Mono<? extends T> source, Function<? super T,? extends Mono<? extends R>> mapper )
    {
        super( source );
        this.mapper = (Function) Objects.requireNonNull( mapper, "mapper" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        if ( FluxFlatMap.trySubscribeScalarMap( this.source, actual, this.mapper, true, false ) )
        {
            return null;
        }
        else
        {
            MonoFlatMap.FlatMapMain<T,R> manager = new MonoFlatMap.FlatMapMain( actual, this.mapper );
            actual.onSubscribe( manager );
            return manager;
        }
    }

    static final class FlatMapInner<R> implements InnerConsumer<R>
    {
        static final AtomicReferenceFieldUpdater<MonoFlatMap.FlatMapInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoFlatMap.FlatMapInner.class, Subscription.class, "s" );
        final MonoFlatMap.FlatMapMain<?,R> parent;
        volatile Subscription s;
        boolean done;

        FlatMapInner( MonoFlatMap.FlatMapMain<?,R> parent )
        {
            this.parent = parent;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.s == Operators.cancelledSubscription() : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( R t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.parent.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.complete( t );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.parent.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.secondError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.parent.secondComplete();
            }
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }
    }

    static final class FlatMapMain<T, R> extends Operators.MonoSubscriber<T,R>
    {
        static final AtomicReferenceFieldUpdater<MonoFlatMap.FlatMapMain,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoFlatMap.FlatMapMain.class, Subscription.class, "s" );
        final Function<? super T,? extends Mono<? extends R>> mapper;
        final MonoFlatMap.FlatMapInner<R> second;
        boolean done;
        volatile Subscription s;

        FlatMapMain( CoreSubscriber<? super R> subscriber, Function<? super T,? extends Mono<? extends R>> mapper )
        {
            super( subscriber );
            this.mapper = mapper;
            this.second = new MonoFlatMap.FlatMapInner( this );
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.second );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;

                Mono m;
                try
                {
                    m = (Mono) Objects.requireNonNull( this.mapper.apply( t ), "The mapper returned a null Mono" );
                }
                catch ( Throwable var8 )
                {
                    this.actual.onError( Operators.onOperatorError( this.s, var8, t, this.actual.currentContext() ) );
                    return;
                }

                if ( m instanceof Callable )
                {
                    Callable c = (Callable) m;

                    Object v;
                    try
                    {
                        v = c.call();
                    }
                    catch ( Throwable var6 )
                    {
                        this.actual.onError( Operators.onOperatorError( this.s, var6, t, this.actual.currentContext() ) );
                        return;
                    }

                    if ( v == null )
                    {
                        this.actual.onComplete();
                    }
                    else
                    {
                        this.complete( v );
                    }
                }
                else
                {
                    try
                    {
                        m.subscribe( (CoreSubscriber) this.second );
                    }
                    catch ( Throwable var7 )
                    {
                        this.actual.onError( Operators.onOperatorError( this, var7, t, this.actual.currentContext() ) );
                    }
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        public void cancel()
        {
            super.cancel();
            Operators.terminate( S, this );
            this.second.cancel();
        }

        void secondError( Throwable ex )
        {
            this.actual.onError( ex );
        }

        void secondComplete()
        {
            this.actual.onComplete();
        }
    }
}
