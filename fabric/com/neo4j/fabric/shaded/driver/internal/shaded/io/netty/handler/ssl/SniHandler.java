package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.DecoderException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AsyncMapping;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.DomainNameMapping;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.Mapping;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCountUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Promise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

public class SniHandler extends AbstractSniHandler<SslContext>
{
    private static final SniHandler.Selection EMPTY_SELECTION = new SniHandler.Selection( (SslContext) null, (String) null );
    protected final AsyncMapping<String,SslContext> mapping;
    private volatile SniHandler.Selection selection;

    public SniHandler( Mapping<? super String,? extends SslContext> mapping )
    {
        this( (AsyncMapping) (new SniHandler.AsyncMappingAdapter( mapping )) );
    }

    public SniHandler( DomainNameMapping<? extends SslContext> mapping )
    {
        this( (Mapping) mapping );
    }

    public SniHandler( AsyncMapping<? super String,? extends SslContext> mapping )
    {
        this.selection = EMPTY_SELECTION;
        this.mapping = (AsyncMapping) ObjectUtil.checkNotNull( mapping, "mapping" );
    }

    public String hostname()
    {
        return this.selection.hostname;
    }

    public SslContext sslContext()
    {
        return this.selection.context;
    }

    protected Future<SslContext> lookup( ChannelHandlerContext ctx, String hostname ) throws Exception
    {
        return this.mapping.map( hostname, ctx.executor().newPromise() );
    }

    protected final void onLookupComplete( ChannelHandlerContext ctx, String hostname, Future<SslContext> future ) throws Exception
    {
        if ( !future.isSuccess() )
        {
            Throwable cause = future.cause();
            if ( cause instanceof Error )
            {
                throw (Error) cause;
            }
            else
            {
                throw new DecoderException( "failed to get the SslContext for " + hostname, cause );
            }
        }
        else
        {
            SslContext sslContext = (SslContext) future.getNow();
            this.selection = new SniHandler.Selection( sslContext, hostname );

            try
            {
                this.replaceHandler( ctx, hostname, sslContext );
            }
            catch ( Throwable var6 )
            {
                this.selection = EMPTY_SELECTION;
                PlatformDependent.throwException( var6 );
            }
        }
    }

    protected void replaceHandler( ChannelHandlerContext ctx, String hostname, SslContext sslContext ) throws Exception
    {
        SslHandler sslHandler = null;

        try
        {
            sslHandler = this.newSslHandler( sslContext, ctx.alloc() );
            ctx.pipeline().replace( (ChannelHandler) this, SslHandler.class.getName(), sslHandler );
            sslHandler = null;
        }
        finally
        {
            if ( sslHandler != null )
            {
                ReferenceCountUtil.safeRelease( sslHandler.engine() );
            }
        }
    }

    protected SslHandler newSslHandler( SslContext context, ByteBufAllocator allocator )
    {
        return context.newHandler( allocator );
    }

    private static final class Selection
    {
        final SslContext context;
        final String hostname;

        Selection( SslContext context, String hostname )
        {
            this.context = context;
            this.hostname = hostname;
        }
    }

    private static final class AsyncMappingAdapter implements AsyncMapping<String,SslContext>
    {
        private final Mapping<? super String,? extends SslContext> mapping;

        private AsyncMappingAdapter( Mapping<? super String,? extends SslContext> mapping )
        {
            this.mapping = (Mapping) ObjectUtil.checkNotNull( mapping, "mapping" );
        }

        public Future<SslContext> map( String input, Promise<SslContext> promise )
        {
            SslContext context;
            try
            {
                context = (SslContext) this.mapping.map( input );
            }
            catch ( Throwable var5 )
            {
                return promise.setFailure( var5 );
            }

            return promise.setSuccess( context );
        }
    }
}
