package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util;

public class Metrics
{
    static final boolean isMicrometerAvailable;

    static
    {
        boolean micrometer;
        try
        {
            io.micrometer.core.instrument.Metrics.globalRegistry.getRegistries();
            micrometer = true;
        }
        catch ( Throwable var2 )
        {
            micrometer = false;
        }

        isMicrometerAvailable = micrometer;
    }

    public static final boolean isInstrumentationAvailable()
    {
        return isMicrometerAvailable;
    }
}
