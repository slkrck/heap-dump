package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxBufferWhen<T, OPEN, CLOSE, BUFFER extends Collection<? super T>> extends InternalFluxOperator<T,BUFFER>
{
    final Publisher<OPEN> start;
    final Function<? super OPEN,? extends Publisher<CLOSE>> end;
    final Supplier<BUFFER> bufferSupplier;
    final Supplier<? extends Queue<BUFFER>> queueSupplier;

    FluxBufferWhen( Flux<? extends T> source, Publisher<OPEN> start, Function<? super OPEN,? extends Publisher<CLOSE>> end, Supplier<BUFFER> bufferSupplier,
            Supplier<? extends Queue<BUFFER>> queueSupplier )
    {
        super( source );
        this.start = (Publisher) Objects.requireNonNull( start, "start" );
        this.end = (Function) Objects.requireNonNull( end, "end" );
        this.bufferSupplier = (Supplier) Objects.requireNonNull( bufferSupplier, "bufferSupplier" );
        this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super BUFFER> actual )
    {
        FluxBufferWhen.BufferWhenMainSubscriber<T,OPEN,CLOSE,BUFFER> main =
                new FluxBufferWhen.BufferWhenMainSubscriber( actual, this.bufferSupplier, this.queueSupplier, this.start, this.end );
        actual.onSubscribe( main );
        FluxBufferWhen.BufferWhenOpenSubscriber<OPEN> bos = new FluxBufferWhen.BufferWhenOpenSubscriber( main );
        if ( main.subscribers.add( bos ) )
        {
            this.start.subscribe( bos );
            return main;
        }
        else
        {
            return null;
        }
    }

    static final class BufferWhenCloseSubscriber<T, BUFFER extends Collection<? super T>> implements Disposable, InnerConsumer<Object>
    {
        static final AtomicReferenceFieldUpdater<FluxBufferWhen.BufferWhenCloseSubscriber,Subscription> SUBSCRIPTION =
                AtomicReferenceFieldUpdater.newUpdater( FluxBufferWhen.BufferWhenCloseSubscriber.class, Subscription.class, "subscription" );
        final FluxBufferWhen.BufferWhenMainSubscriber<T,?,?,BUFFER> parent;
        final long index;
        volatile Subscription subscription;

        BufferWhenCloseSubscriber( FluxBufferWhen.BufferWhenMainSubscriber<T,?,?,BUFFER> parent, long index )
        {
            this.parent = parent;
            this.index = index;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( SUBSCRIPTION, this, s ) )
            {
                this.subscription.request( Long.MAX_VALUE );
            }
        }

        public void dispose()
        {
            Operators.terminate( SUBSCRIPTION, this );
        }

        public boolean isDisposed()
        {
            return this.subscription == Operators.cancelledSubscription();
        }

        public void onNext( Object t )
        {
            Subscription s = this.subscription;
            if ( s != Operators.cancelledSubscription() )
            {
                SUBSCRIPTION.lazySet( this, Operators.cancelledSubscription() );
                s.cancel();
                this.parent.close( this, this.index );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.subscription != Operators.cancelledSubscription() )
            {
                SUBSCRIPTION.lazySet( this, Operators.cancelledSubscription() );
                this.parent.boundaryError( this, t );
            }
            else
            {
                Operators.onErrorDropped( t, this.parent.ctx );
            }
        }

        public void onComplete()
        {
            if ( this.subscription != Operators.cancelledSubscription() )
            {
                SUBSCRIPTION.lazySet( this, Operators.cancelledSubscription() );
                this.parent.close( this, this.index );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.subscription;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return Long.MAX_VALUE;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.isDisposed() : null;
            }
        }
    }

    static final class BufferWhenOpenSubscriber<OPEN> implements Disposable, InnerConsumer<OPEN>
    {
        static final AtomicReferenceFieldUpdater<FluxBufferWhen.BufferWhenOpenSubscriber,Subscription> SUBSCRIPTION =
                AtomicReferenceFieldUpdater.newUpdater( FluxBufferWhen.BufferWhenOpenSubscriber.class, Subscription.class, "subscription" );
        final FluxBufferWhen.BufferWhenMainSubscriber<?,OPEN,?,?> parent;
        volatile Subscription subscription;

        BufferWhenOpenSubscriber( FluxBufferWhen.BufferWhenMainSubscriber<?,OPEN,?,?> parent )
        {
            this.parent = parent;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( SUBSCRIPTION, this, s ) )
            {
                this.subscription.request( Long.MAX_VALUE );
            }
        }

        public void dispose()
        {
            Operators.terminate( SUBSCRIPTION, this );
        }

        public boolean isDisposed()
        {
            return this.subscription == Operators.cancelledSubscription();
        }

        public void onNext( OPEN t )
        {
            this.parent.open( t );
        }

        public void onError( Throwable t )
        {
            SUBSCRIPTION.lazySet( this, Operators.cancelledSubscription() );
            this.parent.boundaryError( this, t );
        }

        public void onComplete()
        {
            SUBSCRIPTION.lazySet( this, Operators.cancelledSubscription() );
            this.parent.openComplete( this );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.subscription;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return Long.MAX_VALUE;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.isDisposed() : null;
            }
        }
    }

    static final class BufferWhenMainSubscriber<T, OPEN, CLOSE, BUFFER extends Collection<? super T>> implements InnerOperator<T,BUFFER>
    {
        static final AtomicLongFieldUpdater<FluxBufferWhen.BufferWhenMainSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxBufferWhen.BufferWhenMainSubscriber.class, "requested" );
        static final AtomicReferenceFieldUpdater<FluxBufferWhen.BufferWhenMainSubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxBufferWhen.BufferWhenMainSubscriber.class, Subscription.class, "s" );
        static final AtomicReferenceFieldUpdater<FluxBufferWhen.BufferWhenMainSubscriber,Throwable> ERRORS =
                AtomicReferenceFieldUpdater.newUpdater( FluxBufferWhen.BufferWhenMainSubscriber.class, Throwable.class, "errors" );
        static final AtomicIntegerFieldUpdater<FluxBufferWhen.BufferWhenMainSubscriber> WINDOWS =
                AtomicIntegerFieldUpdater.newUpdater( FluxBufferWhen.BufferWhenMainSubscriber.class, "windows" );
        final CoreSubscriber<? super BUFFER> actual;
        final Context ctx;
        final Publisher<? extends OPEN> bufferOpen;
        final Function<? super OPEN,? extends Publisher<? extends CLOSE>> bufferClose;
        final Supplier<BUFFER> bufferSupplier;
        final Disposable.Composite subscribers;
        final Queue<BUFFER> queue;
        volatile long requested;
        volatile Subscription s;
        volatile Throwable errors;
        volatile int windows;
        volatile boolean done;
        volatile boolean cancelled;
        long index;
        LinkedHashMap<Long,BUFFER> buffers;
        long emitted;

        BufferWhenMainSubscriber( CoreSubscriber<? super BUFFER> actual, Supplier<BUFFER> bufferSupplier, Supplier<? extends Queue<BUFFER>> queueSupplier,
                Publisher<? extends OPEN> bufferOpen, Function<? super OPEN,? extends Publisher<? extends CLOSE>> bufferClose )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.bufferOpen = bufferOpen;
            this.bufferClose = bufferClose;
            this.bufferSupplier = bufferSupplier;
            this.queue = (Queue) queueSupplier.get();
            this.buffers = new LinkedHashMap();
            this.subscribers = Disposables.composite();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public CoreSubscriber<? super BUFFER> actual()
        {
            return this.actual;
        }

        public void onNext( T t )
        {
            synchronized ( this )
            {
                Map<Long,BUFFER> bufs = this.buffers;
                if ( bufs != null )
                {
                    if ( bufs.isEmpty() )
                    {
                        Operators.onDiscard( t, this.ctx );
                    }
                    else
                    {
                        Iterator var4 = bufs.values().iterator();

                        while ( var4.hasNext() )
                        {
                            BUFFER b = (Collection) var4.next();
                            b.add( t );
                        }
                    }
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( Exceptions.addThrowable( ERRORS, this, t ) )
            {
                this.subscribers.dispose();
                LinkedHashMap bufs;
                synchronized ( this )
                {
                    bufs = this.buffers;
                    this.buffers = null;
                }

                this.done = true;
                this.drain();
                if ( bufs != null )
                {
                    Iterator var3 = bufs.values().iterator();

                    while ( var3.hasNext() )
                    {
                        BUFFER b = (Collection) var3.next();
                        Operators.onDiscardMultiple( b, this.ctx );
                    }
                }
            }
            else
            {
                Operators.onErrorDropped( t, this.ctx );
            }
        }

        public void onComplete()
        {
            this.subscribers.dispose();
            synchronized ( this )
            {
                Map<Long,BUFFER> bufs = this.buffers;
                if ( bufs == null )
                {
                    return;
                }

                Iterator var3 = bufs.values().iterator();

                while ( true )
                {
                    if ( !var3.hasNext() )
                    {
                        this.buffers = null;
                        break;
                    }

                    BUFFER b = (Collection) var3.next();
                    this.queue.offer( b );
                }
            }

            this.done = true;
            this.drain();
        }

        public void request( long n )
        {
            Operators.addCap( REQUESTED, this, n );
            this.drain();
        }

        public void cancel()
        {
            if ( Operators.terminate( S, this ) )
            {
                this.cancelled = true;
                this.subscribers.dispose();
                LinkedHashMap bufs;
                synchronized ( this )
                {
                    bufs = this.buffers;
                    this.buffers = null;
                }

                if ( WINDOWS.getAndIncrement( this ) == 0 )
                {
                    Operators.onDiscardQueueWithClear( this.queue, this.ctx, Collection::stream );
                }

                if ( bufs != null && !bufs.isEmpty() )
                {
                    Iterator var2 = bufs.values().iterator();

                    while ( var2.hasNext() )
                    {
                        BUFFER buffer = (Collection) var2.next();
                        Operators.onDiscardMultiple( buffer, this.ctx );
                    }
                }
            }
        }

        void drain()
        {
            if ( WINDOWS.getAndIncrement( this ) == 0 )
            {
                int missed = 1;
                long e = this.emitted;
                Subscriber<? super BUFFER> a = this.actual;
                Queue q = this.queue;

                do
                {
                    long r;
                    for ( r = this.requested; e != r; ++e )
                    {
                        if ( this.cancelled )
                        {
                            Operators.onDiscardQueueWithClear( q, this.ctx, Collection::stream );
                            return;
                        }

                        boolean d = this.done;
                        if ( d && this.errors != null )
                        {
                            Operators.onDiscardQueueWithClear( q, this.ctx, Collection::stream );
                            Throwable ex = Exceptions.terminate( ERRORS, this );
                            a.onError( ex );
                            return;
                        }

                        BUFFER v = (Collection) q.poll();
                        boolean empty = v == null;
                        if ( d && empty )
                        {
                            a.onComplete();
                            return;
                        }

                        if ( empty )
                        {
                            break;
                        }

                        a.onNext( v );
                    }

                    if ( e == r )
                    {
                        if ( this.cancelled )
                        {
                            Operators.onDiscardQueueWithClear( q, this.ctx, Collection::stream );
                            return;
                        }

                        if ( this.done )
                        {
                            if ( this.errors != null )
                            {
                                Operators.onDiscardQueueWithClear( q, this.ctx, Collection::stream );
                                Throwable ex = Exceptions.terminate( ERRORS, this );
                                a.onError( ex );
                                return;
                            }

                            if ( q.isEmpty() )
                            {
                                a.onComplete();
                                return;
                            }
                        }
                    }

                    this.emitted = e;
                    missed = WINDOWS.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }

        void open( OPEN token )
        {
            Publisher p;
            Collection buf;
            try
            {
                buf = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null Collection" );
                p = (Publisher) Objects.requireNonNull( this.bufferClose.apply( token ), "The bufferClose returned a null Publisher" );
            }
            catch ( Throwable var10 )
            {
                Exceptions.throwIfFatal( var10 );
                Operators.terminate( S, this );
                if ( Exceptions.addThrowable( ERRORS, this, var10 ) )
                {
                    this.subscribers.dispose();
                    LinkedHashMap bufs;
                    synchronized ( this )
                    {
                        bufs = this.buffers;
                        this.buffers = null;
                    }

                    this.done = true;
                    this.drain();
                    if ( bufs != null )
                    {
                        Iterator var6 = bufs.values().iterator();

                        while ( var6.hasNext() )
                        {
                            BUFFER buffer = (Collection) var6.next();
                            Operators.onDiscardMultiple( buffer, this.ctx );
                        }
                    }
                }
                else
                {
                    Operators.onErrorDropped( var10, this.ctx );
                }

                return;
            }

            long idx = (long) (this.index++);
            synchronized ( this )
            {
                Map<Long,BUFFER> bufs = this.buffers;
                if ( bufs == null )
                {
                    return;
                }

                bufs.put( idx, buf );
            }

            FluxBufferWhen.BufferWhenCloseSubscriber<T,BUFFER> bc = new FluxBufferWhen.BufferWhenCloseSubscriber( this, idx );
            this.subscribers.add( bc );
            p.subscribe( bc );
        }

        void openComplete( FluxBufferWhen.BufferWhenOpenSubscriber<OPEN> os )
        {
            this.subscribers.remove( os );
            if ( this.subscribers.size() == 0 )
            {
                Operators.terminate( S, this );
                this.done = true;
                this.drain();
            }
        }

        void close( FluxBufferWhen.BufferWhenCloseSubscriber<T,BUFFER> closer, long idx )
        {
            this.subscribers.remove( closer );
            boolean makeDone = false;
            if ( this.subscribers.size() == 0 )
            {
                makeDone = true;
                Operators.terminate( S, this );
            }

            synchronized ( this )
            {
                Map<Long,BUFFER> bufs = this.buffers;
                if ( bufs == null )
                {
                    return;
                }

                this.queue.offer( this.buffers.remove( idx ) );
            }

            if ( makeDone )
            {
                this.done = true;
            }

            this.drain();
        }

        void boundaryError( Disposable boundary, Throwable ex )
        {
            Operators.terminate( S, this );
            this.subscribers.remove( boundary );
            if ( Exceptions.addThrowable( ERRORS, this, ex ) )
            {
                this.subscribers.dispose();
                LinkedHashMap bufs;
                synchronized ( this )
                {
                    bufs = this.buffers;
                    this.buffers = null;
                }

                this.done = true;
                this.drain();
                if ( bufs != null )
                {
                    Iterator var4 = bufs.values().iterator();

                    while ( var4.hasNext() )
                    {
                        BUFFER buffer = (Collection) var4.next();
                        Operators.onDiscardMultiple( buffer, this.ctx );
                    }
                }
            }
            else
            {
                Operators.onErrorDropped( ex, this.ctx );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.actual;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.buffers.values().stream().mapToInt( Collection::size ).sum();
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.ERROR ? this.errors : null;
            }
        }
    }
}
