package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class StrictSubscriber<T> implements Scannable, CoreSubscriber<T>, Subscription
{
    static final AtomicReferenceFieldUpdater<StrictSubscriber,Subscription> S =
            AtomicReferenceFieldUpdater.newUpdater( StrictSubscriber.class, Subscription.class, "s" );
    static final AtomicLongFieldUpdater<StrictSubscriber> REQUESTED = AtomicLongFieldUpdater.newUpdater( StrictSubscriber.class, "requested" );
    static final AtomicIntegerFieldUpdater<StrictSubscriber> WIP = AtomicIntegerFieldUpdater.newUpdater( StrictSubscriber.class, "wip" );
    static final AtomicReferenceFieldUpdater<StrictSubscriber,Throwable> ERROR =
            AtomicReferenceFieldUpdater.newUpdater( StrictSubscriber.class, Throwable.class, "error" );
    final Subscriber<? super T> actual;
    volatile Subscription s;
    volatile long requested;
    volatile int wip;
    volatile Throwable error;
    volatile boolean done;

    StrictSubscriber( Subscriber<? super T> actual )
    {
        this.actual = actual;
    }

    public void onSubscribe( Subscription s )
    {
        if ( Operators.validate( this.s, s ) )
        {
            this.actual.onSubscribe( this );
            if ( Operators.setOnce( S, this, s ) )
            {
                long r = REQUESTED.getAndSet( this, 0L );
                if ( r != 0L )
                {
                    s.request( r );
                }
            }
        }
        else
        {
            this.onError( new IllegalStateException( "§2.12 violated: onSubscribe must be called at most once" ) );
        }
    }

    public void onNext( T t )
    {
        if ( WIP.get( this ) == 0 && WIP.compareAndSet( this, 0, 1 ) )
        {
            this.actual.onNext( t );
            if ( WIP.decrementAndGet( this ) != 0 )
            {
                Throwable ex = Exceptions.terminate( ERROR, this );
                if ( ex != null )
                {
                    this.actual.onError( ex );
                }
                else
                {
                    this.actual.onComplete();
                }
            }
        }
    }

    public void onError( Throwable t )
    {
        this.done = true;
        if ( Exceptions.addThrowable( ERROR, this, t ) )
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                this.actual.onError( Exceptions.terminate( ERROR, this ) );
            }
        }
        else
        {
            Operators.onErrorDropped( t, Context.empty() );
        }
    }

    public void onComplete()
    {
        this.done = true;
        if ( WIP.getAndIncrement( this ) == 0 )
        {
            Throwable ex = Exceptions.terminate( ERROR, this );
            if ( ex != null )
            {
                this.actual.onError( ex );
            }
            else
            {
                this.actual.onComplete();
            }
        }
    }

    public void request( long n )
    {
        if ( n <= 0L )
        {
            this.cancel();
            this.onError( new IllegalArgumentException( "§3.9 violated: positive request amount required but it was " + n ) );
        }
        else
        {
            Subscription a = this.s;
            if ( a != null )
            {
                a.request( n );
            }
            else
            {
                Operators.addCap( REQUESTED, this, n );
                a = this.s;
                if ( a != null )
                {
                    long r = REQUESTED.getAndSet( this, 0L );
                    if ( r != 0L )
                    {
                        a.request( n );
                    }
                }
            }
        }
    }

    public void cancel()
    {
        if ( !this.done )
        {
            Operators.terminate( S, this );
        }
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.s;
        }
        else if ( key == Scannable.Attr.CANCELLED )
        {
            return this.s == Operators.cancelledSubscription();
        }
        else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
        {
            return this.requested;
        }
        else
        {
            return key == Scannable.Attr.ACTUAL ? this.actual : null;
        }
    }
}
