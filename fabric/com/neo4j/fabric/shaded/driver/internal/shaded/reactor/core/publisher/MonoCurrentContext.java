package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

final class MonoCurrentContext extends Mono<Context> implements Fuseable, Scannable
{
    static final MonoCurrentContext INSTANCE = new MonoCurrentContext();

    public void subscribe( CoreSubscriber<? super Context> actual )
    {
        Context ctx = actual.currentContext();
        actual.onSubscribe( Operators.scalarSubscription( actual, ctx ) );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
