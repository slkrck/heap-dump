package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core;

import org.reactivestreams.Publisher;

public interface CorePublisher<T> extends Publisher<T>
{
    void subscribe( CoreSubscriber<? super T> var1 );
}
