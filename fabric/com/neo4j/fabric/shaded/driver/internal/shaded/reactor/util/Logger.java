package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util;

public interface Logger
{
    String getName();

    boolean isTraceEnabled();

    void trace( String var1 );

    void trace( String var1, Object... var2 );

    void trace( String var1, Throwable var2 );

    boolean isDebugEnabled();

    void debug( String var1 );

    void debug( String var1, Object... var2 );

    void debug( String var1, Throwable var2 );

    boolean isInfoEnabled();

    void info( String var1 );

    void info( String var1, Object... var2 );

    void info( String var1, Throwable var2 );

    boolean isWarnEnabled();

    void warn( String var1 );

    void warn( String var1, Object... var2 );

    void warn( String var1, Throwable var2 );

    boolean isErrorEnabled();

    void error( String var1 );

    void error( String var1, Object... var2 );

    void error( String var1, Throwable var2 );
}
