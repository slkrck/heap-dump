package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelInboundHandlerAdapter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPipeline;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

public abstract class ApplicationProtocolNegotiationHandler extends ChannelInboundHandlerAdapter
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( ApplicationProtocolNegotiationHandler.class );
    private final String fallbackProtocol;

    protected ApplicationProtocolNegotiationHandler( String fallbackProtocol )
    {
        this.fallbackProtocol = (String) ObjectUtil.checkNotNull( fallbackProtocol, "fallbackProtocol" );
    }

    public void userEventTriggered( ChannelHandlerContext ctx, Object evt ) throws Exception
    {
        if ( evt instanceof SslHandshakeCompletionEvent )
        {
            label93:
            {
                boolean var10 = false;

                ChannelPipeline pipeline;
                label88:
                {
                    try
                    {
                        var10 = true;
                        SslHandshakeCompletionEvent handshakeEvent = (SslHandshakeCompletionEvent) evt;
                        if ( handshakeEvent.isSuccess() )
                        {
                            SslHandler sslHandler = (SslHandler) ctx.pipeline().get( SslHandler.class );
                            if ( sslHandler == null )
                            {
                                throw new IllegalStateException(
                                        "cannot find a SslHandler in the pipeline (required for application-level protocol negotiation)" );
                            }

                            String protocol = sslHandler.applicationProtocol();
                            this.configurePipeline( ctx, protocol != null ? protocol : this.fallbackProtocol );
                            var10 = false;
                        }
                        else
                        {
                            this.handshakeFailure( ctx, handshakeEvent.cause() );
                            var10 = false;
                        }
                        break label88;
                    }
                    catch ( Throwable var11 )
                    {
                        this.exceptionCaught( ctx, var11 );
                        var10 = false;
                    }
                    finally
                    {
                        if ( var10 )
                        {
                            ChannelPipeline pipeline = ctx.pipeline();
                            if ( pipeline.context( (ChannelHandler) this ) != null )
                            {
                                pipeline.remove( (ChannelHandler) this );
                            }
                        }
                    }

                    pipeline = ctx.pipeline();
                    if ( pipeline.context( (ChannelHandler) this ) != null )
                    {
                        pipeline.remove( (ChannelHandler) this );
                    }
                    break label93;
                }

                pipeline = ctx.pipeline();
                if ( pipeline.context( (ChannelHandler) this ) != null )
                {
                    pipeline.remove( (ChannelHandler) this );
                }
            }
        }

        ctx.fireUserEventTriggered( evt );
    }

    protected abstract void configurePipeline( ChannelHandlerContext var1, String var2 ) throws Exception;

    protected void handshakeFailure( ChannelHandlerContext ctx, Throwable cause ) throws Exception
    {
        logger.warn( "{} TLS handshake failed:", ctx.channel(), cause );
        ctx.close();
    }

    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception
    {
        logger.warn( "{} Failed to select the application-level protocol:", ctx.channel(), cause );
        ctx.fireExceptionCaught( cause );
        ctx.close();
    }
}
