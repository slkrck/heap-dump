package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Loggers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxUsingWhen<T, S> extends Flux<T> implements SourceProducer<T>
{
    final Publisher<S> resourceSupplier;
    final Function<? super S,? extends Publisher<? extends T>> resourceClosure;
    final Function<? super S,? extends Publisher<?>> asyncComplete;
    final BiFunction<? super S,? super Throwable,? extends Publisher<?>> asyncError;
    @Nullable
    final Function<? super S,? extends Publisher<?>> asyncCancel;

    FluxUsingWhen( Publisher<S> resourceSupplier, Function<? super S,? extends Publisher<? extends T>> resourceClosure,
            Function<? super S,? extends Publisher<?>> asyncComplete, BiFunction<? super S,? super Throwable,? extends Publisher<?>> asyncError,
            @Nullable Function<? super S,? extends Publisher<?>> asyncCancel )
    {
        this.resourceSupplier = (Publisher) Objects.requireNonNull( resourceSupplier, "resourceSupplier" );
        this.resourceClosure = (Function) Objects.requireNonNull( resourceClosure, "resourceClosure" );
        this.asyncComplete = (Function) Objects.requireNonNull( asyncComplete, "asyncComplete" );
        this.asyncError = (BiFunction) Objects.requireNonNull( asyncError, "asyncError" );
        this.asyncCancel = asyncCancel;
    }

    private static <RESOURCE, T> Publisher<? extends T> deriveFluxFromResource( RESOURCE resource,
            Function<? super RESOURCE,? extends Publisher<? extends T>> resourceClosure )
    {
        Object p;
        try
        {
            p = (Publisher) Objects.requireNonNull( resourceClosure.apply( resource ), "The resourceClosure function returned a null value" );
        }
        catch ( Throwable var4 )
        {
            p = Flux.error( var4 );
        }

        return (Publisher) p;
    }

    private static <RESOURCE, T> FluxUsingWhen.UsingWhenSubscriber<? super T,RESOURCE> prepareSubscriberForResource( RESOURCE resource,
            CoreSubscriber<? super T> actual, Function<? super RESOURCE,? extends Publisher<?>> asyncComplete,
            BiFunction<? super RESOURCE,? super Throwable,? extends Publisher<?>> asyncError,
            @Nullable Function<? super RESOURCE,? extends Publisher<?>> asyncCancel, @Nullable Operators.DeferredSubscription arbiter )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<? super T> conditionalActual = (Fuseable.ConditionalSubscriber) actual;
            return new FluxUsingWhen.UsingWhenConditionalSubscriber( conditionalActual, resource, asyncComplete, asyncError, asyncCancel, arbiter );
        }
        else
        {
            return new FluxUsingWhen.UsingWhenSubscriber( actual, resource, asyncComplete, asyncError, asyncCancel, arbiter );
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        if ( this.resourceSupplier instanceof Callable )
        {
            try
            {
                Callable<S> resourceCallable = (Callable) this.resourceSupplier;
                S resource = resourceCallable.call();
                if ( resource == null )
                {
                    Operators.complete( actual );
                }
                else
                {
                    Publisher<? extends T> p = deriveFluxFromResource( resource, this.resourceClosure );
                    FluxUsingWhen.UsingWhenSubscriber<? super T,S> subscriber =
                            prepareSubscriberForResource( resource, actual, this.asyncComplete, this.asyncError, this.asyncCancel,
                                    (Operators.DeferredSubscription) null );
                    p.subscribe( subscriber );
                }
            }
            catch ( Throwable var6 )
            {
                Operators.error( actual, var6 );
            }
        }
        else
        {
            this.resourceSupplier.subscribe(
                    new FluxUsingWhen.ResourceSubscriber( actual, this.resourceClosure, this.asyncComplete, this.asyncError, this.asyncCancel,
                            this.resourceSupplier instanceof Mono ) );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    private interface UsingWhenParent<T> extends InnerOperator<T,T>
    {
        void deferredComplete();

        void deferredError( Throwable var1 );
    }

    static final class CancelInner implements InnerConsumer<Object>
    {
        final FluxUsingWhen.UsingWhenParent parent;

        CancelInner( FluxUsingWhen.UsingWhenParent ts )
        {
            this.parent = ts;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            ((Subscription) Objects.requireNonNull( s, "Subscription cannot be null" )).request( Long.MAX_VALUE );
        }

        public void onNext( Object o )
        {
        }

        public void onError( Throwable e )
        {
            Loggers.getLogger( FluxUsingWhen.class ).warn( "Async resource cleanup failed after cancel", e );
        }

        public void onComplete()
        {
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL ? this.parent.actual() : null;
            }
        }
    }

    static final class CommitInner implements InnerConsumer<Object>
    {
        final FluxUsingWhen.UsingWhenParent parent;
        boolean done;

        CommitInner( FluxUsingWhen.UsingWhenParent ts )
        {
            this.parent = ts;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            ((Subscription) Objects.requireNonNull( s, "Subscription cannot be null" )).request( Long.MAX_VALUE );
        }

        public void onNext( Object o )
        {
        }

        public void onError( Throwable e )
        {
            this.done = true;
            Throwable e_ = Operators.onOperatorError( e, this.parent.currentContext() );
            Throwable commitError = new RuntimeException( "Async resource cleanup failed after onComplete", e_ );
            this.parent.deferredError( commitError );
        }

        public void onComplete()
        {
            this.done = true;
            this.parent.deferredComplete();
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent.actual();
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : null;
            }
        }
    }

    static final class RollbackInner implements InnerConsumer<Object>
    {
        final FluxUsingWhen.UsingWhenParent parent;
        final Throwable rollbackCause;
        boolean done;

        RollbackInner( FluxUsingWhen.UsingWhenParent ts, Throwable rollbackCause )
        {
            this.parent = ts;
            this.rollbackCause = rollbackCause;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            ((Subscription) Objects.requireNonNull( s, "Subscription cannot be null" )).request( Long.MAX_VALUE );
        }

        public void onNext( Object o )
        {
        }

        public void onError( Throwable e )
        {
            this.done = true;
            RuntimeException rollbackError = new RuntimeException( "Async resource cleanup failed after onError", e );
            this.parent.deferredError( Exceptions.addSuppressed( rollbackError, this.rollbackCause ) );
        }

        public void onComplete()
        {
            this.done = true;
            this.parent.deferredError( this.rollbackCause );
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent.actual();
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.rollbackCause;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : null;
            }
        }
    }

    static final class UsingWhenConditionalSubscriber<T, S> extends FluxUsingWhen.UsingWhenSubscriber<T,S> implements Fuseable.ConditionalSubscriber<T>
    {
        final Fuseable.ConditionalSubscriber<? super T> actual;

        UsingWhenConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, S resource, Function<? super S,? extends Publisher<?>> asyncComplete,
                BiFunction<? super S,? super Throwable,? extends Publisher<?>> asyncError, @Nullable Function<? super S,? extends Publisher<?>> asyncCancel,
                @Nullable Operators.DeferredSubscription arbiter )
        {
            super( actual, resource, asyncComplete, asyncError, asyncCancel, arbiter );
            this.actual = actual;
        }

        public boolean tryOnNext( T t )
        {
            return this.actual.tryOnNext( t );
        }
    }

    static class UsingWhenSubscriber<T, S> implements FluxUsingWhen.UsingWhenParent<T>
    {
        static final AtomicReferenceFieldUpdater<FluxUsingWhen.UsingWhenSubscriber,Subscription> SUBSCRIPTION =
                AtomicReferenceFieldUpdater.newUpdater( FluxUsingWhen.UsingWhenSubscriber.class, Subscription.class, "s" );
        static final AtomicIntegerFieldUpdater<FluxUsingWhen.UsingWhenSubscriber> CALLBACK_APPLIED =
                AtomicIntegerFieldUpdater.newUpdater( FluxUsingWhen.UsingWhenSubscriber.class, "callbackApplied" );
        final CoreSubscriber<? super T> actual;
        final S resource;
        final Function<? super S,? extends Publisher<?>> asyncComplete;
        final BiFunction<? super S,? super Throwable,? extends Publisher<?>> asyncError;
        @Nullable
        final Function<? super S,? extends Publisher<?>> asyncCancel;
        @Nullable
        final Operators.DeferredSubscription arbiter;
        volatile Subscription s;
        volatile int callbackApplied;
        Throwable error;

        UsingWhenSubscriber( CoreSubscriber<? super T> actual, S resource, Function<? super S,? extends Publisher<?>> asyncComplete,
                BiFunction<? super S,? super Throwable,? extends Publisher<?>> asyncError, @Nullable Function<? super S,? extends Publisher<?>> asyncCancel,
                @Nullable Operators.DeferredSubscription arbiter )
        {
            this.actual = actual;
            this.resource = resource;
            this.asyncComplete = asyncComplete;
            this.asyncError = asyncError;
            this.asyncCancel = asyncCancel;
            this.arbiter = arbiter;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.error != null;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error == Exceptions.TERMINATED ? null : this.error;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : FluxUsingWhen.UsingWhenParent.super.scanUnsafe( key );
            }
        }

        public void request( long l )
        {
            if ( Operators.validate( l ) )
            {
                this.s.request( l );
            }
        }

        public void cancel()
        {
            if ( CALLBACK_APPLIED.compareAndSet( this, 0, 1 ) && Operators.terminate( SUBSCRIPTION, this ) )
            {
                try
                {
                    if ( this.asyncCancel != null )
                    {
                        Flux.from( (Publisher) this.asyncCancel.apply( this.resource ) ).subscribe( (CoreSubscriber) (new FluxUsingWhen.CancelInner( this )) );
                    }
                    else
                    {
                        Flux.from( (Publisher) this.asyncComplete.apply( this.resource ) ).subscribe(
                                (CoreSubscriber) (new FluxUsingWhen.CancelInner( this )) );
                    }
                }
                catch ( Throwable var2 )
                {
                    Loggers.getLogger( FluxUsingWhen.class ).warn( "Error generating async resource cleanup during onCancel", var2 );
                }
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            if ( CALLBACK_APPLIED.compareAndSet( this, 0, 1 ) )
            {
                Publisher p;
                try
                {
                    p = (Publisher) Objects.requireNonNull( this.asyncError.apply( this.resource, t ), "The asyncError returned a null Publisher" );
                }
                catch ( Throwable var5 )
                {
                    Throwable _e = Operators.onOperatorError( var5, this.actual.currentContext() );
                    _e = Exceptions.addSuppressed( _e, t );
                    this.actual.onError( _e );
                    return;
                }

                p.subscribe( new FluxUsingWhen.RollbackInner( this, t ) );
            }
        }

        public void onComplete()
        {
            if ( CALLBACK_APPLIED.compareAndSet( this, 0, 1 ) )
            {
                Publisher p;
                try
                {
                    p = (Publisher) Objects.requireNonNull( this.asyncComplete.apply( this.resource ), "The asyncComplete returned a null Publisher" );
                }
                catch ( Throwable var4 )
                {
                    Throwable _e = Operators.onOperatorError( var4, this.actual.currentContext() );
                    this.deferredError( _e );
                    return;
                }

                p.subscribe( new FluxUsingWhen.CommitInner( this ) );
            }
        }

        public void deferredComplete()
        {
            this.error = Exceptions.TERMINATED;
            this.actual.onComplete();
        }

        public void deferredError( Throwable error )
        {
            this.error = error;
            this.actual.onError( error );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                if ( this.arbiter == null )
                {
                    this.actual.onSubscribe( this );
                }
                else
                {
                    this.arbiter.set( s );
                }
            }
        }
    }

    static class ResourceSubscriber<S, T> extends Operators.DeferredSubscription implements InnerConsumer<S>
    {
        final CoreSubscriber<? super T> actual;
        final Function<? super S,? extends Publisher<? extends T>> resourceClosure;
        final Function<? super S,? extends Publisher<?>> asyncComplete;
        final BiFunction<? super S,? super Throwable,? extends Publisher<?>> asyncError;
        @Nullable
        final Function<? super S,? extends Publisher<?>> asyncCancel;
        final boolean isMonoSource;
        Subscription resourceSubscription;
        boolean resourceProvided;
        FluxUsingWhen.UsingWhenSubscriber<? super T,S> closureSubscriber;

        ResourceSubscriber( CoreSubscriber<? super T> actual, Function<? super S,? extends Publisher<? extends T>> resourceClosure,
                Function<? super S,? extends Publisher<?>> asyncComplete, BiFunction<? super S,? super Throwable,? extends Publisher<?>> asyncError,
                @Nullable Function<? super S,? extends Publisher<?>> asyncCancel, boolean isMonoSource )
        {
            this.actual = (CoreSubscriber) Objects.requireNonNull( actual, "actual" );
            this.resourceClosure = (Function) Objects.requireNonNull( resourceClosure, "resourceClosure" );
            this.asyncComplete = (Function) Objects.requireNonNull( asyncComplete, "asyncComplete" );
            this.asyncError = (BiFunction) Objects.requireNonNull( asyncError, "asyncError" );
            this.asyncCancel = asyncCancel;
            this.isMonoSource = isMonoSource;
        }

        public void onNext( S resource )
        {
            if ( this.resourceProvided )
            {
                Operators.onNextDropped( resource, this.actual.currentContext() );
            }
            else
            {
                this.resourceProvided = true;
                Publisher<? extends T> p = FluxUsingWhen.deriveFluxFromResource( resource, this.resourceClosure );
                this.closureSubscriber =
                        FluxUsingWhen.prepareSubscriberForResource( resource, this.actual, this.asyncComplete, this.asyncError, this.asyncCancel, this );
                p.subscribe( this.closureSubscriber );
                if ( !this.isMonoSource )
                {
                    this.resourceSubscription.cancel();
                }
            }
        }

        public Context currentContext()
        {
            return this.actual.currentContext();
        }

        public void onError( Throwable throwable )
        {
            if ( this.resourceProvided )
            {
                Operators.onErrorDropped( throwable, this.actual.currentContext() );
            }
            else
            {
                this.actual.onError( throwable );
            }
        }

        public void onComplete()
        {
            if ( !this.resourceProvided )
            {
                this.actual.onComplete();
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.resourceSubscription, s ) )
            {
                this.resourceSubscription = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void cancel()
        {
            if ( !this.resourceProvided )
            {
                this.resourceSubscription.cancel();
                super.cancel();
            }
            else
            {
                Operators.terminate( S, this );
                if ( this.closureSubscriber != null )
                {
                    this.closureSubscriber.cancel();
                }
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.resourceSubscription;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.actual;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.resourceProvided : null;
            }
        }
    }
}
