package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

public interface ByteBufAllocatorMetric
{
    long usedHeapMemory();

    long usedDirectMemory();
}
