package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

interface InnerOperator<I, O> extends InnerConsumer<I>, InnerProducer<O>
{
    default Context currentContext()
    {
        return this.actual().currentContext();
    }
}
