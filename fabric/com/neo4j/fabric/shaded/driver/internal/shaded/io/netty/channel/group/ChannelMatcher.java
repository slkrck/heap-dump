package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.group;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;

public interface ChannelMatcher
{
    boolean matches( Channel var1 );
}
