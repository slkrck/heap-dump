package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

public final class NoOpTypeParameterMatcher extends TypeParameterMatcher
{
    public boolean match( Object msg )
    {
        return true;
    }
}
