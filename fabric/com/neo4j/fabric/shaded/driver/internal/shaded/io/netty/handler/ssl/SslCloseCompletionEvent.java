package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

public final class SslCloseCompletionEvent extends SslCompletionEvent
{
    public static final SslCloseCompletionEvent SUCCESS = new SslCloseCompletionEvent();

    private SslCloseCompletionEvent()
    {
    }

    public SslCloseCompletionEvent( Throwable cause )
    {
        super( cause );
    }
}
