package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Logger;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Loggers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

class MonoCacheTime<T> extends InternalMonoOperator<T,T> implements Runnable
{
    static final AtomicReferenceFieldUpdater<MonoCacheTime,Signal> STATE = AtomicReferenceFieldUpdater.newUpdater( MonoCacheTime.class, Signal.class, "state" );
    static final Signal<?> EMPTY;
    private static final Logger LOGGER = Loggers.getLogger( MonoCacheTime.class );

    static
    {
        EMPTY = new ImmutableSignal( Context.empty(), SignalType.ON_NEXT, (Object) null, (Throwable) null, (Subscription) null );
    }

    final Function<? super Signal<T>,Duration> ttlGenerator;
    final Scheduler clock;
    volatile Signal<T> state;

    MonoCacheTime( Mono<? extends T> source, Duration ttl, Scheduler clock )
    {
        super( source );
        this.ttlGenerator = ( ignoredSignal ) -> {
            return ttl;
        };
        this.clock = clock;
        Signal<T> state = EMPTY;
        this.state = state;
    }

    MonoCacheTime( Mono<? extends T> source, Function<? super Signal<T>,Duration> ttlGenerator, Scheduler clock )
    {
        super( source );
        this.ttlGenerator = ttlGenerator;
        this.clock = clock;
        Signal<T> state = EMPTY;
        this.state = state;
    }

    MonoCacheTime( Mono<? extends T> source, Function<? super T,Duration> valueTtlGenerator, Function<Throwable,Duration> errorTtlGenerator,
            Supplier<Duration> emptyTtlGenerator, Scheduler clock )
    {
        super( source );
        this.ttlGenerator = ( sig ) -> {
            if ( sig.isOnNext() )
            {
                return (Duration) valueTtlGenerator.apply( sig.get() );
            }
            else
            {
                return sig.isOnError() ? (Duration) errorTtlGenerator.apply( sig.getThrowable() ) : (Duration) emptyTtlGenerator.get();
            }
        };
        this.clock = clock;
        Signal<T> emptyState = EMPTY;
        this.state = emptyState;
    }

    public void run()
    {
        LOGGER.debug( "expired {}", this.state );
        Signal<T> emptyState = EMPTY;
        this.state = emptyState;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        MonoCacheTime.CacheMonoSubscriber<T> inner = new MonoCacheTime.CacheMonoSubscriber( actual );
        actual.onSubscribe( inner );

        while ( true )
        {
            Signal<T> state = this.state;
            if ( state != EMPTY && !(state instanceof MonoCacheTime.CoordinatorSubscriber) )
            {
                if ( state.isOnNext() )
                {
                    inner.complete( state.get() );
                }
                else if ( state.isOnComplete() )
                {
                    inner.onComplete();
                }
                else
                {
                    inner.onError( state.getThrowable() );
                }
                break;
            }

            boolean subscribe = false;
            MonoCacheTime.CoordinatorSubscriber coordinator;
            if ( state == EMPTY )
            {
                coordinator = new MonoCacheTime.CoordinatorSubscriber( this );
                if ( !STATE.compareAndSet( this, EMPTY, coordinator ) )
                {
                    continue;
                }

                subscribe = true;
            }
            else
            {
                coordinator = (MonoCacheTime.CoordinatorSubscriber) state;
            }

            if ( coordinator.add( inner ) )
            {
                if ( inner.isCancelled() )
                {
                    coordinator.remove( inner );
                }
                else
                {
                    inner.coordinator = coordinator;
                }

                if ( subscribe )
                {
                    this.source.subscribe( (CoreSubscriber) coordinator );
                }
                break;
            }
        }

        return null;
    }

    static final class CacheMonoSubscriber<T> extends Operators.MonoSubscriber<T,T>
    {
        MonoCacheTime.CoordinatorSubscriber<T> coordinator;

        CacheMonoSubscriber( CoreSubscriber<? super T> actual )
        {
            super( actual );
        }

        public void cancel()
        {
            super.cancel();
            MonoCacheTime.CoordinatorSubscriber<T> coordinator = this.coordinator;
            if ( coordinator != null )
            {
                coordinator.remove( this );
            }
        }
    }

    static final class CoordinatorSubscriber<T> implements InnerConsumer<T>, Signal<T>
    {
        static final AtomicReferenceFieldUpdater<MonoCacheTime.CoordinatorSubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoCacheTime.CoordinatorSubscriber.class, Subscription.class, "subscription" );
        static final AtomicReferenceFieldUpdater<MonoCacheTime.CoordinatorSubscriber,Operators.MonoSubscriber[]> SUBSCRIBERS =
                AtomicReferenceFieldUpdater.newUpdater( MonoCacheTime.CoordinatorSubscriber.class, Operators.MonoSubscriber[].class, "subscribers" );
        private static final Operators.MonoSubscriber[] TERMINATED = new Operators.MonoSubscriber[0];
        private static final Operators.MonoSubscriber[] EMPTY = new Operators.MonoSubscriber[0];
        final MonoCacheTime<T> main;
        volatile Subscription subscription;
        volatile Operators.MonoSubscriber<T,T>[] subscribers;

        CoordinatorSubscriber( MonoCacheTime<T> main )
        {
            this.main = main;
            this.subscribers = EMPTY;
        }

        public Throwable getThrowable()
        {
            throw new UnsupportedOperationException( "illegal signal use" );
        }

        public Subscription getSubscription()
        {
            throw new UnsupportedOperationException( "illegal signal use" );
        }

        public T get()
        {
            throw new UnsupportedOperationException( "illegal signal use" );
        }

        public SignalType getType()
        {
            throw new UnsupportedOperationException( "illegal signal use" );
        }

        public Context getContext()
        {
            throw new UnsupportedOperationException( "illegal signal use: getContext" );
        }

        final boolean add( Operators.MonoSubscriber<T,T> toAdd )
        {
            Operators.MonoSubscriber[] a;
            Operators.MonoSubscriber[] b;
            do
            {
                a = this.subscribers;
                if ( a == TERMINATED )
                {
                    return false;
                }

                int n = a.length;
                b = new Operators.MonoSubscriber[n + 1];
                System.arraycopy( a, 0, b, 0, n );
                b[n] = toAdd;
            }
            while ( !SUBSCRIBERS.compareAndSet( this, a, b ) );

            return true;
        }

        final void remove( Operators.MonoSubscriber<T,T> toRemove )
        {
            while ( true )
            {
                Operators.MonoSubscriber<T,T>[] a = this.subscribers;
                if ( a != TERMINATED && a != EMPTY )
                {
                    int n = a.length;
                    int j = -1;

                    for ( int i = 0; i < n; ++i )
                    {
                        if ( a[i] == toRemove )
                        {
                            j = i;
                            break;
                        }
                    }

                    if ( j < 0 )
                    {
                        return;
                    }

                    Operators.MonoSubscriber[] b;
                    if ( n == 1 )
                    {
                        b = EMPTY;
                    }
                    else
                    {
                        b = new Operators.MonoSubscriber[n - 1];
                        System.arraycopy( a, 0, b, 0, j );
                        System.arraycopy( a, j + 1, b, j, n - j - 1 );
                    }

                    if ( !SUBSCRIBERS.compareAndSet( this, a, b ) )
                    {
                        continue;
                    }

                    return;
                }

                return;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.subscription, s ) )
            {
                this.subscription = s;
                s.request( Long.MAX_VALUE );
            }
        }

        private void signalCached( Signal<T> signal )
        {
            Signal<T> signalToPropagate = signal;
            if ( MonoCacheTime.STATE.compareAndSet( this.main, this, signal ) )
            {
                Duration ttl = null;

                try
                {
                    ttl = (Duration) this.main.ttlGenerator.apply( signal );
                }
                catch ( Throwable var7 )
                {
                    signalToPropagate = Signal.error( var7 );
                    MonoCacheTime.STATE.set( this.main, signalToPropagate );
                    if ( signal.isOnError() )
                    {
                        Exceptions.addSuppressed( var7, signal.getThrowable() );
                    }
                }

                if ( ttl != null && ttl.isZero() )
                {
                    this.main.run();
                }
                else if ( ttl != null )
                {
                    this.main.clock.schedule( this.main, ttl.toMillis(), TimeUnit.MILLISECONDS );
                }
                else
                {
                    if ( signal.isOnNext() )
                    {
                        Operators.onNextDropped( signal.get(), this.currentContext() );
                    }

                    this.main.run();
                }
            }

            Operators.MonoSubscriber[] var8 = (Operators.MonoSubscriber[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
            int var4 = var8.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                Operators.MonoSubscriber<T,T> inner = var8[var5];
                if ( signalToPropagate.isOnNext() )
                {
                    inner.complete( signalToPropagate.get() );
                }
                else if ( signalToPropagate.isOnError() )
                {
                    inner.onError( signalToPropagate.getThrowable() );
                }
                else
                {
                    inner.onComplete();
                }
            }
        }

        public void onNext( T t )
        {
            if ( this.main.state != this )
            {
                Operators.onNextDroppedMulticast( t );
            }
            else
            {
                this.signalCached( Signal.next( t ) );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.main.state != this )
            {
                Operators.onErrorDroppedMulticast( t );
            }
            else
            {
                this.signalCached( Signal.error( t ) );
            }
        }

        public void onComplete()
        {
            this.signalCached( Signal.complete() );
        }

        public Context currentContext()
        {
            return Operators.multiSubscribersContext( this.subscribers );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return null;
        }
    }
}
