package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ServerChannel;

import java.net.InetSocketAddress;

public interface ServerSocketChannel extends ServerChannel
{
    ServerSocketChannelConfig config();

    InetSocketAddress localAddress();

    InetSocketAddress remoteAddress();
}
