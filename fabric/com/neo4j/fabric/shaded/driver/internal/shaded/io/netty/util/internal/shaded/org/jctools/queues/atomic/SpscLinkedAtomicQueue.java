package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.shaded.org.jctools.queues.atomic;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.shaded.org.jctools.queues.MessagePassingQueue;

public class SpscLinkedAtomicQueue<E> extends BaseLinkedAtomicQueue<E>
{
    public SpscLinkedAtomicQueue()
    {
        LinkedQueueAtomicNode<E> node = this.newNode();
        this.spProducerNode( node );
        this.spConsumerNode( node );
        node.soNext( (LinkedQueueAtomicNode) null );
    }

    public boolean offer( E e )
    {
        if ( null == e )
        {
            throw new NullPointerException();
        }
        else
        {
            LinkedQueueAtomicNode<E> nextNode = this.newNode( e );
            this.lpProducerNode().soNext( nextNode );
            this.spProducerNode( nextNode );
            return true;
        }
    }

    public E poll()
    {
        return this.relaxedPoll();
    }

    public E peek()
    {
        return this.relaxedPeek();
    }

    public int fill( MessagePassingQueue.Supplier<E> s )
    {
        long result = 0L;

        do
        {
            this.fill( s, 4096 );
            result += 4096L;
        }
        while ( result <= 2147479551L );

        return (int) result;
    }

    public int fill( MessagePassingQueue.Supplier<E> s, int limit )
    {
        if ( limit == 0 )
        {
            return 0;
        }
        else
        {
            LinkedQueueAtomicNode<E> tail = this.newNode( s.get() );

            for ( int i = 1; i < limit; ++i )
            {
                LinkedQueueAtomicNode<E> temp = this.newNode( s.get() );
                tail.soNext( temp );
                tail = temp;
            }

            LinkedQueueAtomicNode<E> oldPNode = this.lpProducerNode();
            oldPNode.soNext( tail );
            this.spProducerNode( tail );
            return limit;
        }
    }

    public void fill( MessagePassingQueue.Supplier<E> s, MessagePassingQueue.WaitStrategy wait, MessagePassingQueue.ExitCondition exit )
    {
        LinkedQueueAtomicNode chaserNode = this.producerNode;

        while ( exit.keepRunning() )
        {
            for ( int i = 0; i < 4096; ++i )
            {
                LinkedQueueAtomicNode<E> nextNode = this.newNode( s.get() );
                chaserNode.soNext( nextNode );
                chaserNode = nextNode;
                this.producerNode = nextNode;
            }
        }
    }
}
