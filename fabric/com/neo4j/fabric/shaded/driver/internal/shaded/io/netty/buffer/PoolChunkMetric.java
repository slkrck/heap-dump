package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

public interface PoolChunkMetric
{
    int usage();

    int chunkSize();

    int freeBytes();
}
