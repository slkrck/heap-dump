package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.stream.Stream;

import org.reactivestreams.Processor;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public abstract class FluxProcessor<IN, OUT> extends Flux<OUT> implements Processor<IN,OUT>, CoreSubscriber<IN>, Scannable, Disposable
{
    public static <T> FluxProcessor<Publisher<? extends T>,T> switchOnNext()
    {
        UnicastProcessor<Publisher<? extends T>> emitter = UnicastProcessor.create();
        FluxProcessor<Publisher<? extends T>,T> p = wrap( emitter, switchOnNext( emitter ) );
        return p;
    }

    public static <IN, OUT> FluxProcessor<IN,OUT> wrap( Subscriber<IN> upstream, Publisher<OUT> downstream )
    {
        return new DelegateProcessor( downstream, upstream );
    }

    public void dispose()
    {
        this.onError( new CancellationException( "Disposed" ) );
    }

    public long downstreamCount()
    {
        return this.inners().count();
    }

    public int getBufferSize()
    {
        return Integer.MAX_VALUE;
    }

    @Nullable
    public Throwable getError()
    {
        return null;
    }

    public boolean hasDownstreams()
    {
        return this.downstreamCount() != 0L;
    }

    public final boolean hasCompleted()
    {
        return this.isTerminated() && this.getError() == null;
    }

    public final boolean hasError()
    {
        return this.isTerminated() && this.getError() != null;
    }

    public Stream<? extends Scannable> inners()
    {
        return Stream.empty();
    }

    public boolean isTerminated()
    {
        return false;
    }

    public boolean isSerialized()
    {
        return false;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.TERMINATED )
        {
            return this.isTerminated();
        }
        else if ( key == Scannable.Attr.ERROR )
        {
            return this.getError();
        }
        else
        {
            return key == Scannable.Attr.CAPACITY ? this.getBufferSize() : null;
        }
    }

    public final FluxProcessor<IN,OUT> serialize()
    {
        return new DelegateProcessor( this, Operators.serialize( this ) );
    }

    public final FluxSink<IN> sink()
    {
        return this.sink( FluxSink.OverflowStrategy.IGNORE );
    }

    public final FluxSink<IN> sink( FluxSink.OverflowStrategy strategy )
    {
        Objects.requireNonNull( strategy, "strategy" );
        if ( this.getBufferSize() == Integer.MAX_VALUE )
        {
            strategy = FluxSink.OverflowStrategy.IGNORE;
        }

        FluxCreate.BaseSink<IN> s = FluxCreate.createSink( this, strategy );
        this.onSubscribe( s );
        if ( !s.isCancelled() && (!this.isSerialized() || this.getBufferSize() != Integer.MAX_VALUE) )
        {
            return (FluxSink) (this.serializeAlways() ? new FluxCreate.SerializedSink( s ) : new FluxCreate.SerializeOnRequestSink( s ));
        }
        else
        {
            return s;
        }
    }

    protected boolean serializeAlways()
    {
        return true;
    }
}
