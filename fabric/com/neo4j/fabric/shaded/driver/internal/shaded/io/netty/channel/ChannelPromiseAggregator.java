package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.PromiseAggregator;

/**
 * @deprecated
 */
@Deprecated
public final class ChannelPromiseAggregator extends PromiseAggregator<Void,ChannelFuture> implements ChannelFutureListener
{
    public ChannelPromiseAggregator( ChannelPromise aggregatePromise )
    {
        super( aggregatePromise );
    }
}
