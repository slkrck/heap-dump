package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

public interface PoolSubpageMetric
{
    int maxNumElements();

    int numAvailable();

    int elementSize();

    int pageSize();
}
