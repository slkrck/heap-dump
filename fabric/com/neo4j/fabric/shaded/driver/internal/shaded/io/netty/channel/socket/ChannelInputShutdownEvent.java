package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket;

public final class ChannelInputShutdownEvent
{
    public static final ChannelInputShutdownEvent INSTANCE = new ChannelInputShutdownEvent();

    private ChannelInputShutdownEvent()
    {
    }
}
