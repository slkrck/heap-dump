package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import java.util.List;

/**
 * @deprecated
 */
public interface ApplicationProtocolNegotiator
{
    List<String> protocols();
}
