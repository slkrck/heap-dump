package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxSample<T, U> extends InternalFluxOperator<T,T>
{
    final Publisher<U> other;

    FluxSample( Flux<? extends T> source, Publisher<U> other )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        CoreSubscriber<T> serial = Operators.serialize( actual );
        FluxSample.SampleMainSubscriber<T> main = new FluxSample.SampleMainSubscriber( serial );
        actual.onSubscribe( main );
        this.other.subscribe( new FluxSample.SampleOther( main ) );
        return main;
    }

    static final class SampleOther<T, U> implements InnerConsumer<U>
    {
        final FluxSample.SampleMainSubscriber<T> main;

        SampleOther( FluxSample.SampleMainSubscriber<T> main )
        {
            this.main = main;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.main.other;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.main;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.main.other == Operators.cancelledSubscription();
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? Integer.MAX_VALUE : null;
            }
        }

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            this.main.setOther( s );
        }

        public void onNext( U t )
        {
            FluxSample.SampleMainSubscriber<T> m = this.main;
            T v = m.getAndNullValue();
            if ( v != null )
            {
                if ( m.requested != 0L )
                {
                    m.actual.onNext( v );
                    if ( m.requested != Long.MAX_VALUE )
                    {
                        m.decrement();
                    }

                    return;
                }

                m.cancel();
                m.actual.onError( Exceptions.failWithOverflow( "Can't signal value due to lack of requests" ) );
                Operators.onDiscard( v, m.ctx );
            }
        }

        public void onError( Throwable t )
        {
            FluxSample.SampleMainSubscriber<T> m = this.main;
            m.cancelMain();
            m.actual.onError( t );
        }

        public void onComplete()
        {
            FluxSample.SampleMainSubscriber<T> m = this.main;
            m.cancelMain();
            m.actual.onComplete();
        }
    }

    static final class SampleMainSubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicReferenceFieldUpdater<FluxSample.SampleMainSubscriber,Object> VALUE =
                AtomicReferenceFieldUpdater.newUpdater( FluxSample.SampleMainSubscriber.class, Object.class, "value" );
        static final AtomicReferenceFieldUpdater<FluxSample.SampleMainSubscriber,Subscription> MAIN =
                AtomicReferenceFieldUpdater.newUpdater( FluxSample.SampleMainSubscriber.class, Subscription.class, "main" );
        static final AtomicReferenceFieldUpdater<FluxSample.SampleMainSubscriber,Subscription> OTHER =
                AtomicReferenceFieldUpdater.newUpdater( FluxSample.SampleMainSubscriber.class, Subscription.class, "other" );
        static final AtomicLongFieldUpdater<FluxSample.SampleMainSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxSample.SampleMainSubscriber.class, "requested" );
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        volatile T value;
        volatile Subscription main;
        volatile Subscription other;
        volatile long requested;

        SampleMainSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( Scannable.from( this.other ) );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.main;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.main == Operators.cancelledSubscription();
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.value != null ? 1 : 0 : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( !MAIN.compareAndSet( this, (Object) null, s ) )
            {
                s.cancel();
                if ( this.main != Operators.cancelledSubscription() )
                {
                    Operators.reportSubscriptionSet();
                }
            }
            else
            {
                s.request( Long.MAX_VALUE );
            }
        }

        void cancelMain()
        {
            Subscription s = this.main;
            if ( s != Operators.cancelledSubscription() )
            {
                s = (Subscription) MAIN.getAndSet( this, Operators.cancelledSubscription() );
                if ( s != null && s != Operators.cancelledSubscription() )
                {
                    s.cancel();
                }
            }
        }

        void cancelOther()
        {
            Subscription s = this.other;
            if ( s != Operators.cancelledSubscription() )
            {
                s = (Subscription) OTHER.getAndSet( this, Operators.cancelledSubscription() );
                if ( s != null && s != Operators.cancelledSubscription() )
                {
                    s.cancel();
                }
            }
        }

        void setOther( Subscription s )
        {
            if ( !OTHER.compareAndSet( this, (Object) null, s ) )
            {
                s.cancel();
                if ( this.other != Operators.cancelledSubscription() )
                {
                    Operators.reportSubscriptionSet();
                }
            }
            else
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
            }
        }

        public void cancel()
        {
            this.cancelMain();
            this.cancelOther();
        }

        public void onNext( T t )
        {
            Object old = VALUE.getAndSet( this, t );
            if ( old != null )
            {
                Operators.onDiscard( old, this.ctx );
            }
        }

        public void onError( Throwable t )
        {
            this.cancelOther();
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.cancelOther();
            T v = this.value;
            if ( v != null )
            {
                this.actual.onNext( this.value );
            }

            this.actual.onComplete();
        }

        @Nullable
        T getAndNullValue()
        {
            return VALUE.getAndSet( this, (Object) null );
        }

        void decrement()
        {
            REQUESTED.decrementAndGet( this );
        }
    }
}
