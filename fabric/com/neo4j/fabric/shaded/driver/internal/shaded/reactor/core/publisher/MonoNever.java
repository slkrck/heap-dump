package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

final class MonoNever extends Mono<Object> implements SourceProducer<Object>
{
    static final Mono<Object> INSTANCE = new MonoNever();

    static <T> Mono<T> instance()
    {
        return INSTANCE;
    }

    public void subscribe( CoreSubscriber<? super Object> actual )
    {
        actual.onSubscribe( Operators.emptySubscription() );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
