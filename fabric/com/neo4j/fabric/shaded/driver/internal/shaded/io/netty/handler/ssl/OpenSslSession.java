package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

interface OpenSslSession extends SSLSession
{
    void handshakeFinished() throws SSLException;

    void tryExpandApplicationBufferSize( int var1 );
}
