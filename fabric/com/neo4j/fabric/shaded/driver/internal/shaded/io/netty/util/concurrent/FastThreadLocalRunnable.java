package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

final class FastThreadLocalRunnable implements Runnable
{
    private final Runnable runnable;

    private FastThreadLocalRunnable( Runnable runnable )
    {
        this.runnable = (Runnable) ObjectUtil.checkNotNull( runnable, "runnable" );
    }

    static Runnable wrap( Runnable runnable )
    {
        return (Runnable) (runnable instanceof FastThreadLocalRunnable ? runnable : new FastThreadLocalRunnable( runnable ));
    }

    public void run()
    {
        try
        {
            this.runnable.run();
        }
        finally
        {
            FastThreadLocal.removeAll();
        }
    }
}
