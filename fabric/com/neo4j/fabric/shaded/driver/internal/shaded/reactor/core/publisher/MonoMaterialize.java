package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class MonoMaterialize<T> extends InternalMonoOperator<T,Signal<T>>
{
    MonoMaterialize( Mono<T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Signal<T>> actual )
    {
        return new FluxMaterialize.MaterializeSubscriber( new MonoNext.NextSubscriber( actual ) );
    }
}
