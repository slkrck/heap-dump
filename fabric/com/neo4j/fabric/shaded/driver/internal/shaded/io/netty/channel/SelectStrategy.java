package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.IntSupplier;

public interface SelectStrategy
{
    int SELECT = -1;
    int CONTINUE = -2;
    int BUSY_WAIT = -3;

    int calculateStrategy( IntSupplier var1, boolean var2 ) throws Exception;
}
