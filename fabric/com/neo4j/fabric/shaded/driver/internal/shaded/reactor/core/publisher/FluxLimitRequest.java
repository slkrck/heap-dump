package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.concurrent.atomic.AtomicLongFieldUpdater;

import org.reactivestreams.Subscription;

final class FluxLimitRequest<T> extends InternalFluxOperator<T,T>
{
    final long cap;

    FluxLimitRequest( Flux<T> flux, long cap )
    {
        super( flux );
        this.cap = cap;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxLimitRequest.FluxLimitRequestSubscriber( actual, this.cap );
    }

    public int getPrefetch()
    {
        return 0;
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.cap : super.scanUnsafe( key );
    }

    static class FluxLimitRequestSubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicLongFieldUpdater<FluxLimitRequest.FluxLimitRequestSubscriber> REQUEST_REMAINING =
                AtomicLongFieldUpdater.newUpdater( FluxLimitRequest.FluxLimitRequestSubscriber.class, "requestRemaining" );
        final CoreSubscriber<? super T> actual;
        Subscription parent;
        long toProduce;
        volatile long requestRemaining;

        FluxLimitRequestSubscriber( CoreSubscriber<? super T> actual, long cap )
        {
            this.actual = actual;
            this.toProduce = cap;
            this.requestRemaining = cap;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void onNext( T t )
        {
            long r = this.toProduce;
            if ( r > 0L )
            {
                this.toProduce = --r;
                this.actual.onNext( t );
                if ( r == 0L )
                {
                    this.parent.cancel();
                    this.actual.onComplete();
                }
            }
        }

        public void onError( Throwable throwable )
        {
            if ( this.toProduce != 0L )
            {
                this.toProduce = 0L;
                this.actual.onError( throwable );
            }
        }

        public void onComplete()
        {
            if ( this.toProduce != 0L )
            {
                this.toProduce = 0L;
                this.actual.onComplete();
            }
        }

        public void onSubscribe( Subscription s )
        {
            this.parent = s;
            this.actual.onSubscribe( this );
        }

        public void request( long l )
        {
            long r;
            long newRequest;
            long u;
            do
            {
                r = this.requestRemaining;
                if ( r <= l )
                {
                    newRequest = r;
                }
                else
                {
                    newRequest = l;
                }

                u = r - newRequest;
            }
            while ( !REQUEST_REMAINING.compareAndSet( this, r, u ) );

            if ( newRequest != 0L )
            {
                this.parent.request( newRequest );
            }
        }

        public void cancel()
        {
            this.parent.cancel();
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.toProduce == 0L : InnerOperator.super.scanUnsafe( key );
            }
        }
    }
}
