package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.reactivestreams.Subscription;

final class FluxDematerialize<T> extends InternalFluxOperator<Signal<T>,T>
{
    FluxDematerialize( Flux<Signal<T>> source )
    {
        super( source );
    }

    public CoreSubscriber<? super Signal<T>> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxDematerialize.DematerializeSubscriber( actual, false );
    }

    static final class DematerializeSubscriber<T> implements InnerOperator<Signal<T>,T>
    {
        final CoreSubscriber<? super T> actual;
        final boolean completeAfterOnNext;
        Subscription s;
        boolean done;
        volatile boolean cancelled;

        DematerializeSubscriber( CoreSubscriber<? super T> subscriber, boolean completeAfterOnNext )
        {
            this.actual = subscriber;
            this.completeAfterOnNext = completeAfterOnNext;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? 0 : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( Signal<T> t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                if ( t.isOnComplete() )
                {
                    this.s.cancel();
                    this.onComplete();
                }
                else if ( t.isOnError() )
                {
                    this.s.cancel();
                    this.onError( t.getThrowable() );
                }
                else if ( t.isOnNext() )
                {
                    this.actual.onNext( t.get() );
                    if ( this.completeAfterOnNext )
                    {
                        this.onComplete();
                    }
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                this.s.request( n );
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }
    }
}
