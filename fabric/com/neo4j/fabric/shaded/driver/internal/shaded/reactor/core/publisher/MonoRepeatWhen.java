package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

final class MonoRepeatWhen<T> extends FluxFromMonoOperator<T,T>
{
    final Function<? super Flux<Long>,? extends Publisher<?>> whenSourceFactory;

    MonoRepeatWhen( Mono<? extends T> source, Function<? super Flux<Long>,? extends Publisher<?>> whenSourceFactory )
    {
        super( source );
        this.whenSourceFactory = (Function) Objects.requireNonNull( whenSourceFactory, "whenSourceFactory" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRepeatWhen.RepeatWhenOtherSubscriber other = new FluxRepeatWhen.RepeatWhenOtherSubscriber();
        Subscriber<Long> signaller = Operators.serialize( other.completionSignal );
        signaller.onSubscribe( Operators.emptySubscription() );
        CoreSubscriber<T> serial = Operators.serialize( actual );
        FluxRepeatWhen.RepeatWhenMainSubscriber<T> main = new FluxRepeatWhen.RepeatWhenMainSubscriber( serial, signaller, this.source );
        other.main = main;
        serial.onSubscribe( main );

        Publisher p;
        try
        {
            p = (Publisher) Objects.requireNonNull( this.whenSourceFactory.apply( other ), "The whenSourceFactory returned a null Publisher" );
        }
        catch ( Throwable var8 )
        {
            actual.onError( Operators.onOperatorError( var8, actual.currentContext() ) );
            return null;
        }

        p.subscribe( other );
        return !main.cancelled ? main : null;
    }
}
