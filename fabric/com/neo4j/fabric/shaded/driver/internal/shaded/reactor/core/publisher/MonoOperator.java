package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

public abstract class MonoOperator<I, O> extends Mono<O> implements Scannable
{
    protected final Mono<? extends I> source;

    protected MonoOperator( Mono<? extends I> source )
    {
        this.source = (Mono) Objects.requireNonNull( source );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return key == Scannable.Attr.PARENT ? this.source : null;
        }
    }
}
