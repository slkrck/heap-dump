package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.BooleanSupplier;

final class MonoRepeatPredicate<T> extends FluxFromMonoOperator<T,T>
{
    final BooleanSupplier predicate;

    MonoRepeatPredicate( Mono<? extends T> source, BooleanSupplier predicate )
    {
        super( source );
        this.predicate = (BooleanSupplier) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRepeatPredicate.RepeatPredicateSubscriber<T> parent = new FluxRepeatPredicate.RepeatPredicateSubscriber( this.source, actual, this.predicate );
        actual.onSubscribe( parent );
        if ( !parent.isCancelled() )
        {
            parent.resubscribe();
        }

        return null;
    }
}
