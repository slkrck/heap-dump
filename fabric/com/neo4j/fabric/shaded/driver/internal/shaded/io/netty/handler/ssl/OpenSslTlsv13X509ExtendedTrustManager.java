package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

import java.net.Socket;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.X509ExtendedTrustManager;

final class OpenSslTlsv13X509ExtendedTrustManager extends X509ExtendedTrustManager
{
    private final X509ExtendedTrustManager tm;

    private OpenSslTlsv13X509ExtendedTrustManager( X509ExtendedTrustManager tm )
    {
        this.tm = tm;
    }

    static X509ExtendedTrustManager wrap( X509ExtendedTrustManager tm )
    {
        return (X509ExtendedTrustManager) (PlatformDependent.javaVersion() < 11 && OpenSsl.isTlsv13Supported() ? new OpenSslTlsv13X509ExtendedTrustManager( tm )
                                                                                                               : tm);
    }

    private static SSLEngine wrapEngine( final SSLEngine engine )
    {
        final SSLSession session = engine.getHandshakeSession();
        return (SSLEngine) (session != null && "TLSv1.3".equals( session.getProtocol() ) ? new JdkSslEngine( engine )
        {
            public String getNegotiatedApplicationProtocol()
            {
                return engine instanceof ApplicationProtocolAccessor ? ((ApplicationProtocolAccessor) engine).getNegotiatedApplicationProtocol()
                                                                     : super.getNegotiatedApplicationProtocol();
            }

            public SSLSession getHandshakeSession()
            {
                if ( PlatformDependent.javaVersion() >= 7 && session instanceof ExtendedOpenSslSession )
                {
                    final ExtendedOpenSslSession extendedOpenSslSession = (ExtendedOpenSslSession) session;
                    return new ExtendedOpenSslSession( extendedOpenSslSession )
                    {
                        public List getRequestedServerNames()
                        {
                            return extendedOpenSslSession.getRequestedServerNames();
                        }

                        public String[] getPeerSupportedSignatureAlgorithms()
                        {
                            return extendedOpenSslSession.getPeerSupportedSignatureAlgorithms();
                        }

                        public String getProtocol()
                        {
                            return "TLSv1.2";
                        }
                    };
                }
                else
                {
                    return new SSLSession()
                    {
                        public byte[] getId()
                        {
                            return session.getId();
                        }

                        public SSLSessionContext getSessionContext()
                        {
                            return session.getSessionContext();
                        }

                        public long getCreationTime()
                        {
                            return session.getCreationTime();
                        }

                        public long getLastAccessedTime()
                        {
                            return session.getLastAccessedTime();
                        }

                        public void invalidate()
                        {
                            session.invalidate();
                        }

                        public boolean isValid()
                        {
                            return session.isValid();
                        }

                        public void putValue( String s, Object o )
                        {
                            session.putValue( s, o );
                        }

                        public Object getValue( String s )
                        {
                            return session.getValue( s );
                        }

                        public void removeValue( String s )
                        {
                            session.removeValue( s );
                        }

                        public String[] getValueNames()
                        {
                            return session.getValueNames();
                        }

                        public Certificate[] getPeerCertificates() throws SSLPeerUnverifiedException
                        {
                            return session.getPeerCertificates();
                        }

                        public Certificate[] getLocalCertificates()
                        {
                            return session.getLocalCertificates();
                        }

                        public javax.security.cert.X509Certificate[] getPeerCertificateChain() throws SSLPeerUnverifiedException
                        {
                            return session.getPeerCertificateChain();
                        }

                        public Principal getPeerPrincipal() throws SSLPeerUnverifiedException
                        {
                            return session.getPeerPrincipal();
                        }

                        public Principal getLocalPrincipal()
                        {
                            return session.getLocalPrincipal();
                        }

                        public String getCipherSuite()
                        {
                            return session.getCipherSuite();
                        }

                        public String getProtocol()
                        {
                            return "TLSv1.2";
                        }

                        public String getPeerHost()
                        {
                            return session.getPeerHost();
                        }

                        public int getPeerPort()
                        {
                            return session.getPeerPort();
                        }

                        public int getPacketBufferSize()
                        {
                            return session.getPacketBufferSize();
                        }

                        public int getApplicationBufferSize()
                        {
                            return session.getApplicationBufferSize();
                        }
                    };
                }
            }
        } : engine);
    }

    public void checkClientTrusted( X509Certificate[] x509Certificates, String s, Socket socket ) throws CertificateException
    {
        this.tm.checkClientTrusted( x509Certificates, s, socket );
    }

    public void checkServerTrusted( X509Certificate[] x509Certificates, String s, Socket socket ) throws CertificateException
    {
        this.tm.checkServerTrusted( x509Certificates, s, socket );
    }

    public void checkClientTrusted( X509Certificate[] x509Certificates, String s, SSLEngine sslEngine ) throws CertificateException
    {
        this.tm.checkClientTrusted( x509Certificates, s, wrapEngine( sslEngine ) );
    }

    public void checkServerTrusted( X509Certificate[] x509Certificates, String s, SSLEngine sslEngine ) throws CertificateException
    {
        this.tm.checkServerTrusted( x509Certificates, s, wrapEngine( sslEngine ) );
    }

    public void checkClientTrusted( X509Certificate[] x509Certificates, String s ) throws CertificateException
    {
        this.tm.checkClientTrusted( x509Certificates, s );
    }

    public void checkServerTrusted( X509Certificate[] x509Certificates, String s ) throws CertificateException
    {
        this.tm.checkServerTrusted( x509Certificates, s );
    }

    public X509Certificate[] getAcceptedIssuers()
    {
        return this.tm.getAcceptedIssuers();
    }
}
