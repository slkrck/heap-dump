package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.Objects;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;

final class FluxDefer<T> extends Flux<T> implements SourceProducer<T>
{
    final Supplier<? extends Publisher<? extends T>> supplier;

    FluxDefer( Supplier<? extends Publisher<? extends T>> supplier )
    {
        this.supplier = (Supplier) Objects.requireNonNull( supplier, "supplier" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Publisher p;
        try
        {
            p = (Publisher) Objects.requireNonNull( this.supplier.get(), "The Publisher returned by the supplier is null" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return;
        }

        from( p ).subscribe( actual );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
