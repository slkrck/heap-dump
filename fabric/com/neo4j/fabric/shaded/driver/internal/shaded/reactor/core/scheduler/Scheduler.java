package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;

import java.util.concurrent.TimeUnit;

public interface Scheduler extends Disposable
{
    Disposable schedule( Runnable var1 );

    default Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        throw Exceptions.failWithRejectedNotTimeCapable();
    }

    default Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        throw Exceptions.failWithRejectedNotTimeCapable();
    }

    default long now( TimeUnit unit )
    {
        return unit.convert( System.currentTimeMillis(), TimeUnit.MILLISECONDS );
    }

    Scheduler.Worker createWorker();

    default void dispose()
    {
    }

    default void start()
    {
    }

    public interface Worker extends Disposable
    {
        Disposable schedule( Runnable var1 );

        default Disposable schedule( Runnable task, long delay, TimeUnit unit )
        {
            throw Exceptions.failWithRejectedNotTimeCapable();
        }

        default Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
        {
            throw Exceptions.failWithRejectedNotTimeCapable();
        }
    }
}
