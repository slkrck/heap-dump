package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

public interface ChannelFactory<T extends Channel> extends com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.ChannelFactory<T>
{
    T newChannel();
}
