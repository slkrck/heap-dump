package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

/**
 * @deprecated
 */
@Deprecated
public interface ResourceLeak
{
    void record();

    void record( Object var1 );

    boolean close();
}
