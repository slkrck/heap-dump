package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxWindowWhen<T, U, V> extends InternalFluxOperator<T,Flux<T>>
{
    final Publisher<U> start;
    final Function<? super U,? extends Publisher<V>> end;
    final Supplier<? extends Queue<T>> processorQueueSupplier;

    FluxWindowWhen( Flux<? extends T> source, Publisher<U> start, Function<? super U,? extends Publisher<V>> end,
            Supplier<? extends Queue<T>> processorQueueSupplier )
    {
        super( source );
        this.start = (Publisher) Objects.requireNonNull( start, "start" );
        this.end = (Function) Objects.requireNonNull( end, "end" );
        this.processorQueueSupplier = (Supplier) Objects.requireNonNull( processorQueueSupplier, "processorQueueSupplier" );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Flux<T>> actual )
    {
        FluxWindowWhen.WindowWhenMainSubscriber<T,U,V> main =
                new FluxWindowWhen.WindowWhenMainSubscriber( actual, this.start, this.end, this.processorQueueSupplier );
        actual.onSubscribe( main );
        if ( main.cancelled )
        {
            return null;
        }
        else
        {
            FluxWindowWhen.WindowWhenOpenSubscriber<T,U> os = new FluxWindowWhen.WindowWhenOpenSubscriber( main );
            if ( FluxWindowWhen.WindowWhenMainSubscriber.BOUNDARY.compareAndSet( main, (Object) null, os ) )
            {
                FluxWindowWhen.WindowWhenMainSubscriber.OPEN_WINDOW_COUNT.incrementAndGet( main );
                this.start.subscribe( os );
                return main;
            }
            else
            {
                return null;
            }
        }
    }

    static final class WindowWhenCloseSubscriber<T, V> implements Disposable, Subscriber<V>
    {
        static final AtomicReferenceFieldUpdater<FluxWindowWhen.WindowWhenCloseSubscriber,Subscription> SUBSCRIPTION =
                AtomicReferenceFieldUpdater.newUpdater( FluxWindowWhen.WindowWhenCloseSubscriber.class, Subscription.class, "subscription" );
        final FluxWindowWhen.WindowWhenMainSubscriber<T,?,V> parent;
        final UnicastProcessor<T> w;
        volatile Subscription subscription;
        boolean done;

        WindowWhenCloseSubscriber( FluxWindowWhen.WindowWhenMainSubscriber<T,?,V> parent, UnicastProcessor<T> w )
        {
            this.parent = parent;
            this.w = w;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( SUBSCRIPTION, this, s ) )
            {
                this.subscription.request( Long.MAX_VALUE );
            }
        }

        public void dispose()
        {
            Operators.terminate( SUBSCRIPTION, this );
        }

        public boolean isDisposed()
        {
            return this.subscription == Operators.cancelledSubscription();
        }

        public void onNext( V t )
        {
            if ( !this.done )
            {
                this.done = true;
                this.dispose();
                this.parent.close( this );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.parent.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.error( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.parent.close( this );
            }
        }
    }

    static final class WindowWhenOpenSubscriber<T, U> implements Disposable, Subscriber<U>
    {
        static final AtomicReferenceFieldUpdater<FluxWindowWhen.WindowWhenOpenSubscriber,Subscription> SUBSCRIPTION =
                AtomicReferenceFieldUpdater.newUpdater( FluxWindowWhen.WindowWhenOpenSubscriber.class, Subscription.class, "subscription" );
        final FluxWindowWhen.WindowWhenMainSubscriber<T,U,?> parent;
        volatile Subscription subscription;
        boolean done;

        WindowWhenOpenSubscriber( FluxWindowWhen.WindowWhenMainSubscriber<T,U,?> parent )
        {
            this.parent = parent;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( SUBSCRIPTION, this, s ) )
            {
                this.subscription.request( Long.MAX_VALUE );
            }
        }

        public void dispose()
        {
            Operators.terminate( SUBSCRIPTION, this );
        }

        public boolean isDisposed()
        {
            return this.subscription == Operators.cancelledSubscription();
        }

        public void onNext( U t )
        {
            if ( !this.done )
            {
                this.parent.open( t );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.parent.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.error( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.parent.onComplete();
            }
        }
    }

    static final class WindowOperation<T, U>
    {
        final UnicastProcessor<T> w;
        final U open;

        WindowOperation( @Nullable UnicastProcessor<T> w, @Nullable U open )
        {
            this.w = w;
            this.open = open;
        }
    }

    static final class WindowWhenMainSubscriber<T, U, V> extends QueueDrainSubscriber<T,Object,Flux<T>>
    {
        static final AtomicReferenceFieldUpdater<FluxWindowWhen.WindowWhenMainSubscriber,Disposable> BOUNDARY =
                AtomicReferenceFieldUpdater.newUpdater( FluxWindowWhen.WindowWhenMainSubscriber.class, Disposable.class, "boundary" );
        static final AtomicLongFieldUpdater<FluxWindowWhen.WindowWhenMainSubscriber> OPEN_WINDOW_COUNT =
                AtomicLongFieldUpdater.newUpdater( FluxWindowWhen.WindowWhenMainSubscriber.class, "openWindowCount" );
        final Publisher<U> open;
        final Function<? super U,? extends Publisher<V>> close;
        final Supplier<? extends Queue<T>> processorQueueSupplier;
        final Disposable.Composite resources;
        final List<UnicastProcessor<T>> windows;
        Subscription s;
        volatile Disposable boundary;
        volatile long openWindowCount;

        WindowWhenMainSubscriber( CoreSubscriber<? super Flux<T>> actual, Publisher<U> open, Function<? super U,? extends Publisher<V>> close,
                Supplier<? extends Queue<T>> processorQueueSupplier )
        {
            super( actual, (Queue) Queues.unboundedMultiproducer().get() );
            this.open = open;
            this.close = close;
            this.processorQueueSupplier = processorQueueSupplier;
            this.resources = Disposables.composite();
            this.windows = new ArrayList();
            OPEN_WINDOW_COUNT.lazySet( this, 1L );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                if ( this.fastEnter() )
                {
                    Iterator var2 = this.windows.iterator();

                    while ( var2.hasNext() )
                    {
                        UnicastProcessor<T> w = (UnicastProcessor) var2.next();
                        w.onNext( t );
                    }

                    if ( this.leave( -1 ) == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    this.queue.offer( t );
                    if ( !this.enter() )
                    {
                        return;
                    }
                }

                this.drainLoop();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.error = t;
                this.done = true;
                if ( this.enter() )
                {
                    this.drainLoop();
                }

                if ( OPEN_WINDOW_COUNT.decrementAndGet( this ) == 0L )
                {
                    this.resources.dispose();
                }
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                if ( this.enter() )
                {
                    this.drainLoop();
                }

                if ( OPEN_WINDOW_COUNT.decrementAndGet( this ) == 0L )
                {
                    this.resources.dispose();
                }
            }
        }

        void error( Throwable t )
        {
            this.s.cancel();
            this.resources.dispose();
            OperatorDisposables.dispose( BOUNDARY, this );
            this.actual.onError( t );
        }

        public void request( long n )
        {
            this.requested( n );
        }

        public void cancel()
        {
            this.cancelled = true;
        }

        void dispose()
        {
            this.resources.dispose();
            OperatorDisposables.dispose( BOUNDARY, this );
        }

        void drainLoop()
        {
            Queue<Object> q = this.queue;
            Subscriber<? super Flux<T>> a = this.actual;
            List<UnicastProcessor<T>> ws = this.windows;
            int missed = 1;

            do
            {
                while ( true )
                {
                    boolean d = this.done;
                    Object o = q.poll();
                    boolean empty = o == null;
                    if ( d && empty )
                    {
                        this.dispose();
                        Throwable e = this.error;
                        Iterator var17;
                        UnicastProcessor w;
                        if ( e != null )
                        {
                            this.actual.onError( e );
                            var17 = ws.iterator();

                            while ( var17.hasNext() )
                            {
                                w = (UnicastProcessor) var17.next();
                                w.onError( e );
                            }
                        }
                        else
                        {
                            this.actual.onComplete();
                            var17 = ws.iterator();

                            while ( var17.hasNext() )
                            {
                                w = (UnicastProcessor) var17.next();
                                w.onComplete();
                            }
                        }

                        ws.clear();
                        return;
                    }

                    if ( empty )
                    {
                        missed = this.leave( -missed );
                        break;
                    }

                    UnicastProcessor w;
                    if ( o instanceof FluxWindowWhen.WindowOperation )
                    {
                        FluxWindowWhen.WindowOperation<T,U> wo = (FluxWindowWhen.WindowOperation) o;
                        w = wo.w;
                        if ( w != null )
                        {
                            if ( ws.remove( wo.w ) )
                            {
                                wo.w.onComplete();
                                if ( OPEN_WINDOW_COUNT.decrementAndGet( this ) == 0L )
                                {
                                    this.dispose();
                                    return;
                                }
                            }
                        }
                        else if ( !this.cancelled )
                        {
                            w = UnicastProcessor.create( (Queue) this.processorQueueSupplier.get() );
                            long r = this.requested();
                            if ( r != 0L )
                            {
                                ws.add( w );
                                a.onNext( w );
                                if ( r != Long.MAX_VALUE )
                                {
                                    this.produced( 1L );
                                }

                                Publisher p;
                                try
                                {
                                    p = (Publisher) Objects.requireNonNull( this.close.apply( wo.open ), "The publisher supplied is null" );
                                }
                                catch ( Throwable var14 )
                                {
                                    this.cancelled = true;
                                    a.onError( var14 );
                                    continue;
                                }

                                FluxWindowWhen.WindowWhenCloseSubscriber<T,V> cl = new FluxWindowWhen.WindowWhenCloseSubscriber( this, w );
                                if ( this.resources.add( cl ) )
                                {
                                    OPEN_WINDOW_COUNT.getAndIncrement( this );
                                    p.subscribe( cl );
                                }
                            }
                            else
                            {
                                this.cancelled = true;
                                a.onError( Exceptions.failWithOverflow( "Could not deliver new window due to lack of requests" ) );
                            }
                        }
                    }
                    else
                    {
                        Iterator var8 = ws.iterator();

                        while ( var8.hasNext() )
                        {
                            w = (UnicastProcessor) var8.next();
                            w.onNext( o );
                        }
                    }
                }
            }
            while ( missed != 0 );
        }

        void open( U b )
        {
            this.queue.offer( new FluxWindowWhen.WindowOperation( (UnicastProcessor) null, b ) );
            if ( this.enter() )
            {
                this.drainLoop();
            }
        }

        void close( FluxWindowWhen.WindowWhenCloseSubscriber<T,V> w )
        {
            this.resources.remove( w );
            this.queue.offer( new FluxWindowWhen.WindowOperation( w.w, (Object) null ) );
            if ( this.enter() )
            {
                this.drainLoop();
            }
        }
    }
}
