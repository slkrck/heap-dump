package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.pool;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoop;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;

public interface ChannelHealthChecker
{
    ChannelHealthChecker ACTIVE = new ChannelHealthChecker()
    {
        public Future<Boolean> isHealthy( Channel channel )
        {
            EventLoop loop = channel.eventLoop();
            return channel.isActive() ? loop.newSucceededFuture( Boolean.TRUE ) : loop.newSucceededFuture( Boolean.FALSE );
        }
    };

    Future<Boolean> isHealthy( Channel var1 );
}
