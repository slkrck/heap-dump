package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class MonoSubscribeOn<T> extends InternalMonoOperator<T,T>
{
    final Scheduler scheduler;

    MonoSubscribeOn( Mono<? extends T> source, Scheduler scheduler )
    {
        super( source );
        this.scheduler = scheduler;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        Scheduler.Worker worker = this.scheduler.createWorker();
        MonoSubscribeOn.SubscribeOnSubscriber<T> parent = new MonoSubscribeOn.SubscribeOnSubscriber( this.source, actual, worker );
        actual.onSubscribe( parent );

        try
        {
            worker.schedule( parent );
        }
        catch ( RejectedExecutionException var5 )
        {
            if ( parent.s != Operators.cancelledSubscription() )
            {
                actual.onError( Operators.onRejectedExecution( var5, parent, (Throwable) null, (Object) null, actual.currentContext() ) );
            }
        }

        return null;
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }

    static final class SubscribeOnSubscriber<T> implements InnerOperator<T,T>, Runnable
    {
        static final AtomicReferenceFieldUpdater<MonoSubscribeOn.SubscribeOnSubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoSubscribeOn.SubscribeOnSubscriber.class, Subscription.class, "s" );
        static final AtomicLongFieldUpdater<MonoSubscribeOn.SubscribeOnSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( MonoSubscribeOn.SubscribeOnSubscriber.class, "requested" );
        static final AtomicReferenceFieldUpdater<MonoSubscribeOn.SubscribeOnSubscriber,Thread> THREAD =
                AtomicReferenceFieldUpdater.newUpdater( MonoSubscribeOn.SubscribeOnSubscriber.class, Thread.class, "thread" );
        final CoreSubscriber<? super T> actual;
        final Publisher<? extends T> parent;
        final Scheduler.Worker worker;
        volatile Subscription s;
        volatile long requested;
        volatile Thread thread;

        SubscribeOnSubscriber( Publisher<? extends T> parent, CoreSubscriber<? super T> actual, Scheduler.Worker worker )
        {
            this.actual = actual;
            this.parent = parent;
            this.worker = worker;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.worker : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void run()
        {
            THREAD.lazySet( this, Thread.currentThread() );
            this.parent.subscribe( this );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                long r = REQUESTED.getAndSet( this, 0L );
                if ( r != 0L )
                {
                    this.trySchedule( r, s );
                }
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            try
            {
                this.actual.onError( t );
            }
            finally
            {
                this.worker.dispose();
                THREAD.lazySet( this, (Object) null );
            }
        }

        public void onComplete()
        {
            this.actual.onComplete();
            this.worker.dispose();
            THREAD.lazySet( this, (Object) null );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Subscription a = this.s;
                if ( a != null )
                {
                    this.trySchedule( n, a );
                }
                else
                {
                    Operators.addCap( REQUESTED, this, n );
                    a = this.s;
                    if ( a != null )
                    {
                        long r = REQUESTED.getAndSet( this, 0L );
                        if ( r != 0L )
                        {
                            this.trySchedule( n, a );
                        }
                    }
                }
            }
        }

        void trySchedule( long n, Subscription s )
        {
            if ( Thread.currentThread() == THREAD.get( this ) )
            {
                s.request( n );
            }
            else
            {
                try
                {
                    this.worker.schedule( () -> {
                        s.request( n );
                    } );
                }
                catch ( RejectedExecutionException var5 )
                {
                    if ( !this.worker.isDisposed() )
                    {
                        this.actual.onError( Operators.onRejectedExecution( var5, this, (Throwable) null, (Object) null, this.actual.currentContext() ) );
                    }
                }
            }
        }

        public void cancel()
        {
            if ( Operators.terminate( S, this ) )
            {
                this.worker.dispose();
            }
        }
    }
}
