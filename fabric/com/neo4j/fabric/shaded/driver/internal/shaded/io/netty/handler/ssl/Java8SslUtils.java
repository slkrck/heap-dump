package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SNIHostName;
import javax.net.ssl.SNIMatcher;
import javax.net.ssl.SNIServerName;
import javax.net.ssl.SSLParameters;

final class Java8SslUtils
{
    private Java8SslUtils()
    {
    }

    static List<String> getSniHostNames( SSLParameters sslParameters )
    {
        List<SNIServerName> names = sslParameters.getServerNames();
        if ( names != null && !names.isEmpty() )
        {
            List<String> strings = new ArrayList( names.size() );
            Iterator var3 = names.iterator();

            while ( var3.hasNext() )
            {
                SNIServerName serverName = (SNIServerName) var3.next();
                if ( !(serverName instanceof SNIHostName) )
                {
                    throw new IllegalArgumentException( "Only " + SNIHostName.class.getName() + " instances are supported, but found: " + serverName );
                }

                strings.add( ((SNIHostName) serverName).getAsciiName() );
            }

            return strings;
        }
        else
        {
            return Collections.emptyList();
        }
    }

    static void setSniHostNames( SSLParameters sslParameters, List<String> names )
    {
        sslParameters.setServerNames( getSniHostNames( names ) );
    }

    static List getSniHostNames( List<String> names )
    {
        if ( names != null && !names.isEmpty() )
        {
            List<SNIServerName> sniServerNames = new ArrayList( names.size() );
            Iterator var2 = names.iterator();

            while ( var2.hasNext() )
            {
                String name = (String) var2.next();
                sniServerNames.add( new SNIHostName( name ) );
            }

            return sniServerNames;
        }
        else
        {
            return Collections.emptyList();
        }
    }

    static List getSniHostName( byte[] hostname )
    {
        return hostname != null && hostname.length != 0 ? Collections.singletonList( new SNIHostName( hostname ) ) : Collections.emptyList();
    }

    static boolean getUseCipherSuitesOrder( SSLParameters sslParameters )
    {
        return sslParameters.getUseCipherSuitesOrder();
    }

    static void setUseCipherSuitesOrder( SSLParameters sslParameters, boolean useOrder )
    {
        sslParameters.setUseCipherSuitesOrder( useOrder );
    }

    static void setSNIMatchers( SSLParameters sslParameters, Collection<?> matchers )
    {
        sslParameters.setSNIMatchers( matchers );
    }

    static boolean checkSniHostnameMatch( Collection<?> matchers, byte[] hostname )
    {
        if ( matchers != null && !matchers.isEmpty() )
        {
            SNIHostName name = new SNIHostName( hostname );
            Iterator matcherIt = matchers.iterator();

            SNIMatcher matcher;
            do
            {
                if ( !matcherIt.hasNext() )
                {
                    return false;
                }

                matcher = (SNIMatcher) matcherIt.next();
            }
            while ( matcher.getType() != 0 || !matcher.matches( name ) );

            return true;
        }
        else
        {
            return true;
        }
    }
}
