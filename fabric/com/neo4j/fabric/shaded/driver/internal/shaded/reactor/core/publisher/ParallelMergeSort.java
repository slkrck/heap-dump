package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class ParallelMergeSort<T> extends Flux<T> implements Scannable
{
    final ParallelFlux<List<T>> source;
    final Comparator<? super T> comparator;

    ParallelMergeSort( ParallelFlux<List<T>> source, Comparator<? super T> comparator )
    {
        this.source = source;
        this.comparator = comparator;
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        ParallelMergeSort.MergeSortMain<T> parent = new ParallelMergeSort.MergeSortMain( actual, this.source.parallelism(), this.comparator );
        actual.onSubscribe( parent );
        this.source.subscribe( (CoreSubscriber[]) parent.subscribers );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    static final class MergeSortInner<T> implements InnerConsumer<List<T>>
    {
        static final AtomicReferenceFieldUpdater<ParallelMergeSort.MergeSortInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( ParallelMergeSort.MergeSortInner.class, Subscription.class, "s" );
        final ParallelMergeSort.MergeSortMain<T> parent;
        final int index;
        volatile Subscription s;

        MergeSortInner( ParallelMergeSort.MergeSortMain<T> parent, int index )
        {
            this.parent = parent;
            this.index = index;
        }

        public Context currentContext()
        {
            return this.parent.actual.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? Integer.MAX_VALUE : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( List<T> t )
        {
            this.parent.innerNext( t, this.index );
        }

        public void onError( Throwable t )
        {
            this.parent.innerError( t );
        }

        public void onComplete()
        {
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }
    }

    static final class MergeSortMain<T> implements InnerProducer<T>
    {
        static final AtomicIntegerFieldUpdater<ParallelMergeSort.MergeSortMain> WIP =
                AtomicIntegerFieldUpdater.newUpdater( ParallelMergeSort.MergeSortMain.class, "wip" );
        static final AtomicLongFieldUpdater<ParallelMergeSort.MergeSortMain> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( ParallelMergeSort.MergeSortMain.class, "requested" );
        static final AtomicIntegerFieldUpdater<ParallelMergeSort.MergeSortMain> REMAINING =
                AtomicIntegerFieldUpdater.newUpdater( ParallelMergeSort.MergeSortMain.class, "remaining" );
        static final AtomicReferenceFieldUpdater<ParallelMergeSort.MergeSortMain,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( ParallelMergeSort.MergeSortMain.class, Throwable.class, "error" );
        final ParallelMergeSort.MergeSortInner<T>[] subscribers;
        final List<T>[] lists;
        final int[] indexes;
        final Comparator<? super T> comparator;
        final CoreSubscriber<? super T> actual;
        volatile int wip;
        volatile long requested;
        volatile boolean cancelled;
        volatile int remaining;
        volatile Throwable error;

        MergeSortMain( CoreSubscriber<? super T> actual, int n, Comparator<? super T> comparator )
        {
            this.comparator = comparator;
            this.actual = actual;
            ParallelMergeSort.MergeSortInner<T>[] s = new ParallelMergeSort.MergeSortInner[n];

            for ( int i = 0; i < n; ++i )
            {
                s[i] = new ParallelMergeSort.MergeSortInner( this, i );
            }

            this.subscribers = s;
            this.lists = new List[n];
            this.indexes = new int[n];
            REMAINING.lazySet( this, n );
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.subscribers.length - this.remaining : InnerProducer.super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                if ( this.remaining == 0 )
                {
                    this.drain();
                }
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.cancelAll();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    Arrays.fill( this.lists, (Object) null );
                }
            }
        }

        void cancelAll()
        {
            ParallelMergeSort.MergeSortInner[] var1 = this.subscribers;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                ParallelMergeSort.MergeSortInner<T> s = var1[var3];
                s.cancel();
            }
        }

        void innerNext( List<T> value, int index )
        {
            this.lists[index] = value;
            if ( REMAINING.decrementAndGet( this ) == 0 )
            {
                this.drain();
            }
        }

        void innerError( Throwable ex )
        {
            if ( ERROR.compareAndSet( this, (Object) null, ex ) )
            {
                this.cancelAll();
                this.drain();
            }
            else if ( this.error != ex )
            {
                Operators.onErrorDropped( ex, this.actual.currentContext() );
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                int missed = 1;
                Subscriber<? super T> a = this.actual;
                List<T>[] lists = this.lists;
                int[] indexes = this.indexes;
                int n = indexes.length;

                while ( true )
                {
                    long r = this.requested;

                    long e;
                    Throwable ex;
                    int i;
                    for ( e = 0L; e != r; ++e )
                    {
                        if ( this.cancelled )
                        {
                            Arrays.fill( lists, (Object) null );
                            return;
                        }

                        ex = this.error;
                        if ( ex != null )
                        {
                            this.cancelAll();
                            Arrays.fill( lists, (Object) null );
                            a.onError( ex );
                            return;
                        }

                        T min = null;
                        i = -1;

                        for ( int i = 0; i < n; ++i )
                        {
                            List<T> list = lists[i];
                            int index = indexes[i];
                            if ( list.size() != index )
                            {
                                if ( min == null )
                                {
                                    min = list.get( index );
                                    i = i;
                                }
                                else
                                {
                                    T b = list.get( index );
                                    if ( this.comparator.compare( min, b ) > 0 )
                                    {
                                        min = b;
                                        i = i;
                                    }
                                }
                            }
                        }

                        if ( min == null )
                        {
                            Arrays.fill( lists, (Object) null );
                            a.onComplete();
                            return;
                        }

                        a.onNext( min );
                        int var10002 = indexes[i]++;
                    }

                    if ( e == r )
                    {
                        if ( this.cancelled )
                        {
                            Arrays.fill( lists, (Object) null );
                            return;
                        }

                        ex = this.error;
                        if ( ex != null )
                        {
                            this.cancelAll();
                            Arrays.fill( lists, (Object) null );
                            a.onError( ex );
                            return;
                        }

                        boolean empty = true;

                        for ( i = 0; i < n; ++i )
                        {
                            if ( indexes[i] != lists[i].size() )
                            {
                                empty = false;
                                break;
                            }
                        }

                        if ( empty )
                        {
                            Arrays.fill( lists, (Object) null );
                            a.onComplete();
                            return;
                        }
                    }

                    if ( e != 0L && r != Long.MAX_VALUE )
                    {
                        REQUESTED.addAndGet( this, -e );
                    }

                    int w = this.wip;
                    if ( w == missed )
                    {
                        missed = WIP.addAndGet( this, -missed );
                        if ( missed == 0 )
                        {
                            return;
                        }
                    }
                    else
                    {
                        missed = w;
                    }
                }
            }
        }
    }
}
