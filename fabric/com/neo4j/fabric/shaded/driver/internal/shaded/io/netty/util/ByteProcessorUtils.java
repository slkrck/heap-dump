package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

final class ByteProcessorUtils
{
    static final byte SPACE = 32;
    static final byte HTAB = 9;
    static final byte CARRIAGE_RETURN = 13;
    static final byte LINE_FEED = 10;

    private ByteProcessorUtils()
    {
    }
}
