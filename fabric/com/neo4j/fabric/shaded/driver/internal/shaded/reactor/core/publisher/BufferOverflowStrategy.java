package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

public enum BufferOverflowStrategy
{
    ERROR,
    DROP_LATEST,
    DROP_OLDEST;
}
