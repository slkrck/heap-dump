package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.CompleteFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.GenericFutureListener;

abstract class CompleteChannelFuture extends CompleteFuture<Void> implements ChannelFuture
{
    private final Channel channel;

    protected CompleteChannelFuture( Channel channel, EventExecutor executor )
    {
        super( executor );
        if ( channel == null )
        {
            throw new NullPointerException( "channel" );
        }
        else
        {
            this.channel = channel;
        }
    }

    protected EventExecutor executor()
    {
        EventExecutor e = super.executor();
        return (EventExecutor) (e == null ? this.channel().eventLoop() : e);
    }

    public ChannelFuture addListener( GenericFutureListener<? extends Future<? super Void>> listener )
    {
        super.addListener( listener );
        return this;
    }

    public ChannelFuture addListeners( GenericFutureListener<? extends Future<? super Void>>... listeners )
    {
        super.addListeners( listeners );
        return this;
    }

    public ChannelFuture removeListener( GenericFutureListener<? extends Future<? super Void>> listener )
    {
        super.removeListener( listener );
        return this;
    }

    public ChannelFuture removeListeners( GenericFutureListener<? extends Future<? super Void>>... listeners )
    {
        super.removeListeners( listeners );
        return this;
    }

    public ChannelFuture syncUninterruptibly()
    {
        return this;
    }

    public ChannelFuture sync() throws InterruptedException
    {
        return this;
    }

    public ChannelFuture await() throws InterruptedException
    {
        return this;
    }

    public ChannelFuture awaitUninterruptibly()
    {
        return this;
    }

    public Channel channel()
    {
        return this.channel;
    }

    public Void getNow()
    {
        return null;
    }

    public boolean isVoid()
    {
        return false;
    }
}
