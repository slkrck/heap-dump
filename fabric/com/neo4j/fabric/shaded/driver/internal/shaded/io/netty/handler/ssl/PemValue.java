package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AbstractReferenceCounted;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.IllegalReferenceCountException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

class PemValue extends AbstractReferenceCounted implements PemEncoded
{
    private final ByteBuf content;
    private final boolean sensitive;

    PemValue( ByteBuf content, boolean sensitive )
    {
        this.content = (ByteBuf) ObjectUtil.checkNotNull( content, "content" );
        this.sensitive = sensitive;
    }

    public boolean isSensitive()
    {
        return this.sensitive;
    }

    public ByteBuf content()
    {
        int count = this.refCnt();
        if ( count <= 0 )
        {
            throw new IllegalReferenceCountException( count );
        }
        else
        {
            return this.content;
        }
    }

    public PemValue copy()
    {
        return this.replace( this.content.copy() );
    }

    public PemValue duplicate()
    {
        return this.replace( this.content.duplicate() );
    }

    public PemValue retainedDuplicate()
    {
        return this.replace( this.content.retainedDuplicate() );
    }

    public PemValue replace( ByteBuf content )
    {
        return new PemValue( content, this.sensitive );
    }

    public PemValue touch()
    {
        return (PemValue) super.touch();
    }

    public PemValue touch( Object hint )
    {
        this.content.touch( hint );
        return this;
    }

    public PemValue retain()
    {
        return (PemValue) super.retain();
    }

    public PemValue retain( int increment )
    {
        return (PemValue) super.retain( increment );
    }

    protected void deallocate()
    {
        if ( this.sensitive )
        {
            SslUtils.zeroout( this.content );
        }

        this.content.release();
    }
}
