package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Subscription;

final class ParallelGroup<T> extends Flux<GroupedFlux<Integer,T>> implements Scannable, Fuseable
{
    final ParallelFlux<? extends T> source;

    ParallelGroup( ParallelFlux<? extends T> source )
    {
        this.source = source;
    }

    public void subscribe( CoreSubscriber<? super GroupedFlux<Integer,T>> actual )
    {
        int n = this.source.parallelism();
        ParallelGroup.ParallelInnerGroup<T>[] groups = new ParallelGroup.ParallelInnerGroup[n];

        for ( int i = 0; i < n; ++i )
        {
            groups[i] = new ParallelGroup.ParallelInnerGroup( i );
        }

        FluxArray.subscribe( actual, groups );
        this.source.subscribe( (CoreSubscriber[]) groups );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    static final class ParallelInnerGroup<T> extends GroupedFlux<Integer,T> implements InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<ParallelGroup.ParallelInnerGroup> ONCE =
                AtomicIntegerFieldUpdater.newUpdater( ParallelGroup.ParallelInnerGroup.class, "once" );
        static final AtomicReferenceFieldUpdater<ParallelGroup.ParallelInnerGroup,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( ParallelGroup.ParallelInnerGroup.class, Subscription.class, "s" );
        static final AtomicLongFieldUpdater<ParallelGroup.ParallelInnerGroup> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( ParallelGroup.ParallelInnerGroup.class, "requested" );
        final int key;
        volatile int once;
        volatile Subscription s;
        volatile long requested;
        CoreSubscriber<? super T> actual;

        ParallelInnerGroup( int key )
        {
            this.key = key;
        }

        public Integer key()
        {
            return this.key;
        }

        public void subscribe( CoreSubscriber<? super T> actual )
        {
            if ( ONCE.compareAndSet( this, 0, 1 ) )
            {
                this.actual = actual;
                actual.onSubscribe( this );
            }
            else
            {
                Operators.error( actual, new IllegalStateException( "This ParallelGroup can be subscribed to at most once." ) );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.s == Operators.cancelledSubscription() : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                long r = REQUESTED.getAndSet( this, 0L );
                if ( r != 0L )
                {
                    s.request( r );
                }
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Subscription a = this.s;
                if ( a == null )
                {
                    Operators.addCap( REQUESTED, this, n );
                    a = this.s;
                    if ( a != null )
                    {
                        long r = REQUESTED.getAndSet( this, 0L );
                        if ( r != 0L )
                        {
                            a.request( n );
                        }
                    }
                }
                else
                {
                    a.request( n );
                }
            }
        }

        public void cancel()
        {
            Operators.terminate( S, this );
        }
    }
}
