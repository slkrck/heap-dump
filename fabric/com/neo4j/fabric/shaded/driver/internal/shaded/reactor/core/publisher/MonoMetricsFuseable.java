package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import org.reactivestreams.Subscription;

final class MonoMetricsFuseable<T> extends InternalMonoOperator<T,T> implements Fuseable
{
    final String name;
    final Tags tags;
    final MeterRegistry registryCandidate;

    MonoMetricsFuseable( Mono<? extends T> mono )
    {
        this( mono, (MeterRegistry) null );
    }

    MonoMetricsFuseable( Mono<? extends T> mono, @Nullable MeterRegistry registryCandidate )
    {
        super( mono );
        this.name = FluxMetrics.resolveName( mono );
        this.tags = FluxMetrics.resolveTags( mono, FluxMetrics.DEFAULT_TAGS_MONO, this.name );
        if ( registryCandidate == null )
        {
            this.registryCandidate = Metrics.globalRegistry;
        }
        else
        {
            this.registryCandidate = registryCandidate;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoMetricsFuseable.MetricsFuseableSubscriber( actual, this.registryCandidate, Clock.SYSTEM, this.tags );
    }

    static final class MetricsFuseableSubscriber<T> extends MonoMetrics.MetricsSubscriber<T> implements Fuseable, Fuseable.QueueSubscription<T>
    {
        int mode;
        @Nullable
        Fuseable.QueueSubscription<T> qs;

        MetricsFuseableSubscriber( CoreSubscriber<? super T> actual, MeterRegistry registry, Clock clock, Tags sequenceTags )
        {
            super( actual, registry, clock, sequenceTags );
        }

        public void clear()
        {
            if ( this.qs != null )
            {
                this.qs.clear();
            }
        }

        public boolean isEmpty()
        {
            return this.qs == null || this.qs.isEmpty();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                FluxMetrics.recordOnSubscribe( this.commonTags, this.registry );
                this.subscribeToTerminateSample = Timer.start( this.clock );
                this.qs = Operators.as( s );
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        @Nullable
        public T poll()
        {
            if ( this.qs == null )
            {
                return null;
            }
            else
            {
                try
                {
                    T v = this.qs.poll();
                    if ( !this.done && (v != null || this.mode == 1) )
                    {
                        FluxMetrics.recordOnComplete( this.commonTags, this.registry, this.subscribeToTerminateSample );
                    }

                    this.done = true;
                    return v;
                }
                catch ( Throwable var2 )
                {
                    FluxMetrics.recordOnError( this.commonTags, this.registry, this.subscribeToTerminateSample, var2 );
                    throw var2;
                }
            }
        }

        public int requestFusion( int mode )
        {
            if ( this.qs != null )
            {
                this.mode = this.qs.requestFusion( mode );
                return this.mode;
            }
            else
            {
                return 0;
            }
        }

        public int size()
        {
            return this.qs == null ? 0 : this.qs.size();
        }
    }
}
