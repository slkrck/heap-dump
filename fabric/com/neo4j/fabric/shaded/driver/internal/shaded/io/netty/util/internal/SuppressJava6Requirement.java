package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention( RetentionPolicy.CLASS )
@Target( {ElementType.METHOD, ElementType.CONSTRUCTOR} )
public @interface SuppressJava6Requirement
{
    String reason();
}
