package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.Predicate;

import org.reactivestreams.Subscription;

final class MonoAll<T> extends MonoFromFluxOperator<T,Boolean> implements Fuseable
{
    final Predicate<? super T> predicate;

    MonoAll( Flux<? extends T> source, Predicate<? super T> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Boolean> actual )
    {
        return new MonoAll.AllSubscriber( actual, this.predicate );
    }

    static final class AllSubscriber<T> extends Operators.MonoSubscriber<T,Boolean>
    {
        final Predicate<? super T> predicate;
        Subscription s;
        boolean done;

        AllSubscriber( CoreSubscriber<? super Boolean> actual, Predicate<? super T> predicate )
        {
            super( actual );
            this.predicate = predicate;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
            }
        }

        public void cancel()
        {
            this.s.cancel();
            super.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( !this.done )
            {
                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var4 )
                {
                    this.done = true;
                    this.actual.onError( Operators.onOperatorError( this.s, var4, t, this.actual.currentContext() ) );
                    return;
                }

                if ( !b )
                {
                    this.done = true;
                    this.s.cancel();
                    this.complete( false );
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.complete( true );
            }
        }
    }
}
