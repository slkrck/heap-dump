package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Stream;

final class Context1 implements Context, Entry<Object,Object>
{
    final Object key;
    final Object value;

    Context1( Object key, Object value )
    {
        this.key = Objects.requireNonNull( key, "key" );
        this.value = Objects.requireNonNull( value, "value" );
    }

    public Context put( Object key, Object value )
    {
        Objects.requireNonNull( key, "key" );
        Objects.requireNonNull( value, "value" );
        return (Context) (this.key.equals( key ) ? new Context1( key, value ) : new Context2( this.key, this.value, key, value ));
    }

    public Context delete( Object key )
    {
        Objects.requireNonNull( key, "key" );
        return (Context) (this.key.equals( key ) ? Context.empty() : this);
    }

    public boolean hasKey( Object key )
    {
        return this.key.equals( key );
    }

    public <T> T get( Object key )
    {
        if ( this.hasKey( key ) )
        {
            return this.value;
        }
        else
        {
            throw new NoSuchElementException( "Context does not contain key: " + key );
        }
    }

    public Stream<Entry<Object,Object>> stream()
    {
        return Stream.of( this );
    }

    public Object getKey()
    {
        return this.key;
    }

    public Object getValue()
    {
        return this.value;
    }

    public Object setValue( Object value )
    {
        throw new UnsupportedOperationException( "Does not support in-place update" );
    }

    public int size()
    {
        return 1;
    }

    public String toString()
    {
        return "Context1{" + this.key + '=' + this.value + '}';
    }
}
