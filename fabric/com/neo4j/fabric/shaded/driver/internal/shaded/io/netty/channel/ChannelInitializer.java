package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@ChannelHandler.Sharable
public abstract class ChannelInitializer<C extends Channel> extends ChannelInboundHandlerAdapter
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( ChannelInitializer.class );
    private final Set<ChannelHandlerContext> initMap = Collections.newSetFromMap( new ConcurrentHashMap() );

    protected abstract void initChannel( C var1 ) throws Exception;

    public final void channelRegistered( ChannelHandlerContext ctx ) throws Exception
    {
        if ( this.initChannel( ctx ) )
        {
            ctx.pipeline().fireChannelRegistered();
            this.removeState( ctx );
        }
        else
        {
            ctx.fireChannelRegistered();
        }
    }

    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception
    {
        if ( logger.isWarnEnabled() )
        {
            logger.warn( "Failed to initialize a channel. Closing: " + ctx.channel(), cause );
        }

        ctx.close();
    }

    public void handlerAdded( ChannelHandlerContext ctx ) throws Exception
    {
        if ( ctx.channel().isRegistered() && this.initChannel( ctx ) )
        {
            this.removeState( ctx );
        }
    }

    public void handlerRemoved( ChannelHandlerContext ctx ) throws Exception
    {
        this.initMap.remove( ctx );
    }

    private boolean initChannel( ChannelHandlerContext ctx ) throws Exception
    {
        if ( this.initMap.add( ctx ) )
        {
            boolean var7 = false;

            ChannelPipeline pipeline;
            label77:
            {
                try
                {
                    var7 = true;
                    this.initChannel( ctx.channel() );
                    var7 = false;
                    break label77;
                }
                catch ( Throwable var8 )
                {
                    this.exceptionCaught( ctx, var8 );
                    var7 = false;
                }
                finally
                {
                    if ( var7 )
                    {
                        ChannelPipeline pipeline = ctx.pipeline();
                        if ( pipeline.context( (ChannelHandler) this ) != null )
                        {
                            pipeline.remove( (ChannelHandler) this );
                        }
                    }
                }

                pipeline = ctx.pipeline();
                if ( pipeline.context( (ChannelHandler) this ) != null )
                {
                    pipeline.remove( (ChannelHandler) this );
                }

                return true;
            }

            pipeline = ctx.pipeline();
            if ( pipeline.context( (ChannelHandler) this ) != null )
            {
                pipeline.remove( (ChannelHandler) this );
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    private void removeState( final ChannelHandlerContext ctx )
    {
        if ( ctx.isRemoved() )
        {
            this.initMap.remove( ctx );
        }
        else
        {
            ctx.executor().execute( new Runnable()
            {
                public void run()
                {
                    ChannelInitializer.this.initMap.remove( ctx );
                }
            } );
        }
    }
}
