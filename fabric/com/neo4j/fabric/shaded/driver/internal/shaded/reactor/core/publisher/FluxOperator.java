package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

public abstract class FluxOperator<I, O> extends Flux<O> implements Scannable
{
    protected final Flux<? extends I> source;

    protected FluxOperator( Flux<? extends I> source )
    {
        this.source = (Flux) Objects.requireNonNull( source );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.PARENT ? this.source : null;
        }
    }
}
