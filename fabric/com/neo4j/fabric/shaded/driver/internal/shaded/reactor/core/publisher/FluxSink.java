package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.function.LongConsumer;

public interface FluxSink<T>
{
    void complete();

    Context currentContext();

    void error( Throwable var1 );

    FluxSink<T> next( T var1 );

    long requestedFromDownstream();

    boolean isCancelled();

    FluxSink<T> onRequest( LongConsumer var1 );

    FluxSink<T> onCancel( Disposable var1 );

    FluxSink<T> onDispose( Disposable var1 );

    public static enum OverflowStrategy
    {
        IGNORE,
        ERROR,
        DROP,
        LATEST,
        BUFFER;
    }
}
