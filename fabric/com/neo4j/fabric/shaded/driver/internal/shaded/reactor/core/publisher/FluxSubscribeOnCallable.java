package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

final class FluxSubscribeOnCallable<T> extends Flux<T> implements Fuseable, Scannable
{
    final Callable<? extends T> callable;
    final Scheduler scheduler;

    FluxSubscribeOnCallable( Callable<? extends T> callable, Scheduler scheduler )
    {
        this.callable = (Callable) Objects.requireNonNull( callable, "callable" );
        this.scheduler = (Scheduler) Objects.requireNonNull( scheduler, "scheduler" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        FluxSubscribeOnCallable.CallableSubscribeOnSubscription<T> parent =
                new FluxSubscribeOnCallable.CallableSubscribeOnSubscription( actual, this.callable, this.scheduler );
        actual.onSubscribe( parent );

        try
        {
            Disposable f = this.scheduler.schedule( parent );
            parent.setMainFuture( f );
        }
        catch ( RejectedExecutionException var4 )
        {
            if ( parent.state != 4 )
            {
                actual.onError( Operators.onRejectedExecution( var4, actual.currentContext() ) );
            }
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : null;
    }

    static final class CallableSubscribeOnSubscription<T> implements Fuseable.QueueSubscription<T>, InnerProducer<T>, Runnable
    {
        static final AtomicIntegerFieldUpdater<FluxSubscribeOnCallable.CallableSubscribeOnSubscription> STATE =
                AtomicIntegerFieldUpdater.newUpdater( FluxSubscribeOnCallable.CallableSubscribeOnSubscription.class, "state" );
        static final int NO_REQUEST_HAS_VALUE = 1;
        static final int HAS_REQUEST_NO_VALUE = 2;
        static final int HAS_REQUEST_HAS_VALUE = 3;
        static final int HAS_CANCELLED = 4;
        static final int NO_VALUE = 1;
        static final int HAS_VALUE = 2;
        static final int COMPLETE = 3;
        static final AtomicReferenceFieldUpdater<FluxSubscribeOnCallable.CallableSubscribeOnSubscription,Disposable> MAIN_FUTURE =
                AtomicReferenceFieldUpdater.newUpdater( FluxSubscribeOnCallable.CallableSubscribeOnSubscription.class, Disposable.class, "mainFuture" );
        static final AtomicReferenceFieldUpdater<FluxSubscribeOnCallable.CallableSubscribeOnSubscription,Disposable> REQUEST_FUTURE =
                AtomicReferenceFieldUpdater.newUpdater( FluxSubscribeOnCallable.CallableSubscribeOnSubscription.class, Disposable.class, "requestFuture" );
        final CoreSubscriber<? super T> actual;
        final Callable<? extends T> callable;
        final Scheduler scheduler;
        volatile int state;
        T value;
        int fusionState;
        volatile Disposable mainFuture;
        volatile Disposable requestFuture;

        CallableSubscribeOnSubscription( CoreSubscriber<? super T> actual, Callable<? extends T> callable, Scheduler scheduler )
        {
            this.actual = actual;
            this.callable = callable;
            this.scheduler = scheduler;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.state == 4;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.value != null ? 1 : 0;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.scheduler : InnerProducer.super.scanUnsafe( key );
            }
        }

        public void cancel()
        {
            this.state = 4;
            this.fusionState = 3;
            Disposable a = this.mainFuture;
            if ( a != OperatorDisposables.DISPOSED )
            {
                a = (Disposable) MAIN_FUTURE.getAndSet( this, OperatorDisposables.DISPOSED );
                if ( a != null && a != OperatorDisposables.DISPOSED )
                {
                    a.dispose();
                }
            }

            a = this.requestFuture;
            if ( a != OperatorDisposables.DISPOSED )
            {
                a = (Disposable) REQUEST_FUTURE.getAndSet( this, OperatorDisposables.DISPOSED );
                if ( a != null && a != OperatorDisposables.DISPOSED )
                {
                    a.dispose();
                }
            }
        }

        public void clear()
        {
            this.value = null;
            this.fusionState = 3;
        }

        public boolean isEmpty()
        {
            return this.fusionState == 3;
        }

        @Nullable
        public T poll()
        {
            if ( this.fusionState == 2 )
            {
                this.fusionState = 3;
                return this.value;
            }
            else
            {
                return null;
            }
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 2) != 0 && (requestedMode & 4) == 0 )
            {
                this.fusionState = 1;
                return 2;
            }
            else
            {
                return 0;
            }
        }

        public int size()
        {
            return this.isEmpty() ? 0 : 1;
        }

        void setMainFuture( Disposable c )
        {
            Disposable a;
            do
            {
                a = this.mainFuture;
                if ( a == OperatorDisposables.DISPOSED )
                {
                    c.dispose();
                    return;
                }
            }
            while ( !MAIN_FUTURE.compareAndSet( this, a, c ) );
        }

        void setRequestFuture( Disposable c )
        {
            Disposable a;
            do
            {
                a = this.requestFuture;
                if ( a == OperatorDisposables.DISPOSED )
                {
                    c.dispose();
                    return;
                }
            }
            while ( !REQUEST_FUTURE.compareAndSet( this, a, c ) );
        }

        public void run()
        {
            Object v;
            try
            {
                v = this.callable.call();
            }
            catch ( Throwable var3 )
            {
                this.actual.onError( Operators.onOperatorError( this, var3, this.actual.currentContext() ) );
                return;
            }

            if ( v == null )
            {
                this.fusionState = 3;
                this.actual.onComplete();
            }
            else
            {
                int s;
                do
                {
                    s = this.state;
                    if ( s == 4 || s == 3 || s == 1 )
                    {
                        return;
                    }

                    if ( s == 2 )
                    {
                        if ( this.fusionState == 1 )
                        {
                            this.value = v;
                            this.fusionState = 2;
                        }

                        this.actual.onNext( v );
                        if ( this.state != 4 )
                        {
                            this.actual.onComplete();
                        }

                        return;
                    }

                    this.value = v;
                }
                while ( !STATE.compareAndSet( this, s, 1 ) );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                int s;
                do
                {
                    s = this.state;
                    if ( s == 4 || s == 2 || s == 3 )
                    {
                        return;
                    }

                    if ( s == 1 )
                    {
                        if ( STATE.compareAndSet( this, s, 3 ) )
                        {
                            try
                            {
                                Disposable f = this.scheduler.schedule( this::emitValue );
                                this.setRequestFuture( f );
                            }
                            catch ( RejectedExecutionException var5 )
                            {
                                this.actual.onError( Operators.onRejectedExecution( var5, this.actual.currentContext() ) );
                            }
                        }

                        return;
                    }
                }
                while ( !STATE.compareAndSet( this, s, 2 ) );
            }
        }

        void emitValue()
        {
            if ( this.fusionState == 1 )
            {
                this.fusionState = 2;
            }

            T v = this.value;
            this.clear();
            if ( v != null )
            {
                this.actual.onNext( v );
            }

            if ( this.state != 4 )
            {
                this.actual.onComplete();
            }
        }
    }
}
