package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Supplier;

@FunctionalInterface
public interface Disposable
{
    void dispose();

    default boolean isDisposed()
    {
        return false;
    }

    public interface Composite extends Disposable
    {
        boolean add( Disposable var1 );

        default boolean addAll( Collection<? extends Disposable> ds )
        {
            boolean abort = this.isDisposed();
            Iterator var3 = ds.iterator();

            while ( var3.hasNext() )
            {
                Disposable d = (Disposable) var3.next();
                if ( abort )
                {
                    d.dispose();
                }
                else
                {
                    abort = !this.add( d );
                }
            }

            return !abort;
        }

        void dispose();

        boolean isDisposed();

        boolean remove( Disposable var1 );

        int size();
    }

    public interface Swap extends Disposable, Supplier<Disposable>
    {
        boolean update( @Nullable Disposable var1 );

        boolean replace( @Nullable Disposable var1 );
    }
}
