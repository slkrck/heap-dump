package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Delayed;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public final class UnorderedThreadPoolEventExecutor extends ScheduledThreadPoolExecutor implements EventExecutor
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( UnorderedThreadPoolEventExecutor.class );
    private final Promise<?> terminationFuture;
    private final Set<EventExecutor> executorSet;

    public UnorderedThreadPoolEventExecutor( int corePoolSize )
    {
        this( corePoolSize, (ThreadFactory) (new DefaultThreadFactory( UnorderedThreadPoolEventExecutor.class )) );
    }

    public UnorderedThreadPoolEventExecutor( int corePoolSize, ThreadFactory threadFactory )
    {
        super( corePoolSize, threadFactory );
        this.terminationFuture = GlobalEventExecutor.INSTANCE.newPromise();
        this.executorSet = Collections.singleton( this );
    }

    public UnorderedThreadPoolEventExecutor( int corePoolSize, java.util.concurrent.RejectedExecutionHandler handler )
    {
        this( corePoolSize, new DefaultThreadFactory( UnorderedThreadPoolEventExecutor.class ), handler );
    }

    public UnorderedThreadPoolEventExecutor( int corePoolSize, ThreadFactory threadFactory, java.util.concurrent.RejectedExecutionHandler handler )
    {
        super( corePoolSize, threadFactory, handler );
        this.terminationFuture = GlobalEventExecutor.INSTANCE.newPromise();
        this.executorSet = Collections.singleton( this );
    }

    public EventExecutor next()
    {
        return this;
    }

    public EventExecutorGroup parent()
    {
        return this;
    }

    public boolean inEventLoop()
    {
        return false;
    }

    public boolean inEventLoop( Thread thread )
    {
        return false;
    }

    public <V> Promise<V> newPromise()
    {
        return new DefaultPromise( this );
    }

    public <V> ProgressivePromise<V> newProgressivePromise()
    {
        return new DefaultProgressivePromise( this );
    }

    public <V> Future<V> newSucceededFuture( V result )
    {
        return new SucceededFuture( this, result );
    }

    public <V> Future<V> newFailedFuture( Throwable cause )
    {
        return new FailedFuture( this, cause );
    }

    public boolean isShuttingDown()
    {
        return this.isShutdown();
    }

    public List<Runnable> shutdownNow()
    {
        List<Runnable> tasks = super.shutdownNow();
        this.terminationFuture.trySuccess( (Object) null );
        return tasks;
    }

    public void shutdown()
    {
        super.shutdown();
        this.terminationFuture.trySuccess( (Object) null );
    }

    public Future<?> shutdownGracefully()
    {
        return this.shutdownGracefully( 2L, 15L, TimeUnit.SECONDS );
    }

    public Future<?> shutdownGracefully( long quietPeriod, long timeout, TimeUnit unit )
    {
        this.shutdown();
        return this.terminationFuture();
    }

    public Future<?> terminationFuture()
    {
        return this.terminationFuture;
    }

    public Iterator<EventExecutor> iterator()
    {
        return this.executorSet.iterator();
    }

    protected <V> RunnableScheduledFuture<V> decorateTask( Runnable runnable, RunnableScheduledFuture<V> task )
    {
        return (RunnableScheduledFuture) (runnable instanceof UnorderedThreadPoolEventExecutor.NonNotifyRunnable ? task
                                                                                                                 : new UnorderedThreadPoolEventExecutor.RunnableScheduledFutureTask(
                                                                                                                         this, runnable, task ));
    }

    protected <V> RunnableScheduledFuture<V> decorateTask( Callable<V> callable, RunnableScheduledFuture<V> task )
    {
        return new UnorderedThreadPoolEventExecutor.RunnableScheduledFutureTask( this, callable, task );
    }

    public ScheduledFuture<?> schedule( Runnable command, long delay, TimeUnit unit )
    {
        return (ScheduledFuture) super.schedule( command, delay, unit );
    }

    public <V> ScheduledFuture<V> schedule( Callable<V> callable, long delay, TimeUnit unit )
    {
        return (ScheduledFuture) super.schedule( callable, delay, unit );
    }

    public ScheduledFuture<?> scheduleAtFixedRate( Runnable command, long initialDelay, long period, TimeUnit unit )
    {
        return (ScheduledFuture) super.scheduleAtFixedRate( command, initialDelay, period, unit );
    }

    public ScheduledFuture<?> scheduleWithFixedDelay( Runnable command, long initialDelay, long delay, TimeUnit unit )
    {
        return (ScheduledFuture) super.scheduleWithFixedDelay( command, initialDelay, delay, unit );
    }

    public Future<?> submit( Runnable task )
    {
        return (Future) super.submit( task );
    }

    public <T> Future<T> submit( Runnable task, T result )
    {
        return (Future) super.submit( task, result );
    }

    public <T> Future<T> submit( Callable<T> task )
    {
        return (Future) super.submit( task );
    }

    public void execute( Runnable command )
    {
        super.schedule( new UnorderedThreadPoolEventExecutor.NonNotifyRunnable( command ), 0L, TimeUnit.NANOSECONDS );
    }

    private static final class NonNotifyRunnable implements Runnable
    {
        private final Runnable task;

        NonNotifyRunnable( Runnable task )
        {
            this.task = task;
        }

        public void run()
        {
            this.task.run();
        }
    }

    private static final class RunnableScheduledFutureTask<V> extends PromiseTask<V> implements RunnableScheduledFuture<V>, ScheduledFuture<V>
    {
        private final RunnableScheduledFuture<V> future;

        RunnableScheduledFutureTask( EventExecutor executor, Runnable runnable, RunnableScheduledFuture<V> future )
        {
            super( executor, runnable, (Object) null );
            this.future = future;
        }

        RunnableScheduledFutureTask( EventExecutor executor, Callable<V> callable, RunnableScheduledFuture<V> future )
        {
            super( executor, callable );
            this.future = future;
        }

        public void run()
        {
            if ( !this.isPeriodic() )
            {
                super.run();
            }
            else if ( !this.isDone() )
            {
                try
                {
                    this.task.call();
                }
                catch ( Throwable var2 )
                {
                    if ( !this.tryFailureInternal( var2 ) )
                    {
                        UnorderedThreadPoolEventExecutor.logger.warn( "Failure during execution of task", var2 );
                    }
                }
            }
        }

        public boolean isPeriodic()
        {
            return this.future.isPeriodic();
        }

        public long getDelay( TimeUnit unit )
        {
            return this.future.getDelay( unit );
        }

        public int compareTo( Delayed o )
        {
            return this.future.compareTo( o );
        }
    }
}
