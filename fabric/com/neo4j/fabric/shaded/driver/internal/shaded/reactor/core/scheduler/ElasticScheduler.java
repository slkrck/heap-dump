package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import java.util.stream.Stream;

final class ElasticScheduler implements Scheduler, Supplier<ScheduledExecutorService>, Scannable
{
    static final AtomicLong COUNTER = new AtomicLong();
    static final ThreadFactory EVICTOR_FACTORY = ( r ) -> {
        Thread t = new Thread( r, "elastic-evictor-" + COUNTER.incrementAndGet() );
        t.setDaemon( true );
        return t;
    };
    static final ElasticScheduler.CachedService SHUTDOWN = new ElasticScheduler.CachedService( (ElasticScheduler) null );
    static final int DEFAULT_TTL_SECONDS = 60;
    final ThreadFactory factory;
    final int ttlSeconds;
    final Deque<ElasticScheduler.ScheduledExecutorServiceExpiry> cache;
    final Queue<ElasticScheduler.CachedService> all;
    final ScheduledExecutorService evictor;
    volatile boolean shutdown;

    ElasticScheduler( ThreadFactory factory, int ttlSeconds )
    {
        if ( ttlSeconds < 0 )
        {
            throw new IllegalArgumentException( "ttlSeconds must be positive, was: " + ttlSeconds );
        }
        else
        {
            this.ttlSeconds = ttlSeconds;
            this.factory = factory;
            this.cache = new ConcurrentLinkedDeque();
            this.all = new ConcurrentLinkedQueue();
            this.evictor = Executors.newScheduledThreadPool( 1, EVICTOR_FACTORY );
            this.evictor.scheduleAtFixedRate( this::eviction, (long) ttlSeconds, (long) ttlSeconds, TimeUnit.SECONDS );
        }
    }

    public ScheduledExecutorService get()
    {
        ScheduledThreadPoolExecutor poolExecutor = new ScheduledThreadPoolExecutor( 1, this.factory );
        poolExecutor.setMaximumPoolSize( 1 );
        poolExecutor.setRemoveOnCancelPolicy( true );
        return poolExecutor;
    }

    public void start()
    {
        throw new UnsupportedOperationException( "Restarting not supported yet" );
    }

    public boolean isDisposed()
    {
        return this.shutdown;
    }

    public void dispose()
    {
        if ( !this.shutdown )
        {
            this.shutdown = true;
            this.evictor.shutdownNow();
            this.cache.clear();

            ElasticScheduler.CachedService cached;
            while ( (cached = (ElasticScheduler.CachedService) this.all.poll()) != null )
            {
                cached.exec.shutdownNow();
            }
        }
    }

    ElasticScheduler.CachedService pick()
    {
        if ( this.shutdown )
        {
            return SHUTDOWN;
        }
        else
        {
            ElasticScheduler.ScheduledExecutorServiceExpiry e = (ElasticScheduler.ScheduledExecutorServiceExpiry) this.cache.pollLast();
            if ( e != null )
            {
                return e.cached;
            }
            else
            {
                ElasticScheduler.CachedService result = new ElasticScheduler.CachedService( this );
                this.all.offer( result );
                if ( this.shutdown )
                {
                    this.all.remove( result );
                    return SHUTDOWN;
                }
                else
                {
                    return result;
                }
            }
        }
    }

    public Disposable schedule( Runnable task )
    {
        ElasticScheduler.CachedService cached = this.pick();
        return Schedulers.directSchedule( cached.exec, task, cached, 0L, TimeUnit.MILLISECONDS );
    }

    public Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        ElasticScheduler.CachedService cached = this.pick();
        return Schedulers.directSchedule( cached.exec, task, cached, delay, unit );
    }

    public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        ElasticScheduler.CachedService cached = this.pick();
        return Disposables.composite( Schedulers.directSchedulePeriodically( cached.exec, task, initialDelay, period, unit ), cached );
    }

    public String toString()
    {
        StringBuilder ts = (new StringBuilder( "elastic" )).append( '(' );
        if ( this.factory instanceof ReactorThreadFactory )
        {
            ts.append( '"' ).append( ((ReactorThreadFactory) this.factory).get() ).append( '"' );
        }

        ts.append( ')' );
        return ts.toString();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            if ( key == Scannable.Attr.CAPACITY )
            {
                return Integer.MAX_VALUE;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.cache.size();
            }
            else
            {
                return key == Scannable.Attr.NAME ? this.toString() : null;
            }
        }
        else
        {
            return this.isDisposed();
        }
    }

    public Stream<? extends Scannable> inners()
    {
        return this.cache.stream().map( ( cached ) -> {
            return cached.cached;
        } );
    }

    public Scheduler.Worker createWorker()
    {
        return new ElasticScheduler.ElasticWorker( this.pick() );
    }

    void eviction()
    {
        long now = System.currentTimeMillis();
        List<ElasticScheduler.ScheduledExecutorServiceExpiry> list = new ArrayList( this.cache );
        Iterator var4 = list.iterator();

        while ( var4.hasNext() )
        {
            ElasticScheduler.ScheduledExecutorServiceExpiry e = (ElasticScheduler.ScheduledExecutorServiceExpiry) var4.next();
            if ( e.expireMillis < now && this.cache.remove( e ) )
            {
                e.cached.exec.shutdownNow();
                this.all.remove( e.cached );
            }
        }
    }

    static final class ElasticWorker extends AtomicBoolean implements Scheduler.Worker, Scannable
    {
        final ElasticScheduler.CachedService cached;
        final Disposable.Composite tasks;

        ElasticWorker( ElasticScheduler.CachedService cached )
        {
            this.cached = cached;
            this.tasks = Disposables.composite();
        }

        public Disposable schedule( Runnable task )
        {
            return Schedulers.workerSchedule( this.cached.exec, this.tasks, task, 0L, TimeUnit.MILLISECONDS );
        }

        public Disposable schedule( Runnable task, long delay, TimeUnit unit )
        {
            return Schedulers.workerSchedule( this.cached.exec, this.tasks, task, delay, unit );
        }

        public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
        {
            return Schedulers.workerSchedulePeriodically( this.cached.exec, this.tasks, task, initialDelay, period, unit );
        }

        public void dispose()
        {
            if ( this.compareAndSet( false, true ) )
            {
                this.tasks.dispose();
                this.cached.dispose();
            }
        }

        public boolean isDisposed()
        {
            return this.tasks.isDisposed();
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                if ( key == Scannable.Attr.NAME )
                {
                    return this.cached.scanUnsafe( key ) + ".worker";
                }
                else
                {
                    return key == Scannable.Attr.PARENT ? this.cached.parent : this.cached.scanUnsafe( key );
                }
            }
            else
            {
                return this.isDisposed();
            }
        }
    }

    static final class ScheduledExecutorServiceExpiry
    {
        final ElasticScheduler.CachedService cached;
        final long expireMillis;

        ScheduledExecutorServiceExpiry( ElasticScheduler.CachedService cached, long expireMillis )
        {
            this.cached = cached;
            this.expireMillis = expireMillis;
        }
    }

    static final class CachedService implements Disposable, Scannable
    {
        final ElasticScheduler parent;
        final ScheduledExecutorService exec;

        CachedService( @Nullable ElasticScheduler parent )
        {
            this.parent = parent;
            if ( parent != null )
            {
                this.exec = Schedulers.decorateExecutorService( parent, parent.get() );
            }
            else
            {
                this.exec = Executors.newSingleThreadScheduledExecutor();
                this.exec.shutdownNow();
            }
        }

        public void dispose()
        {
            if ( this.exec != null && this != ElasticScheduler.SHUTDOWN && !this.parent.shutdown )
            {
                ElasticScheduler.ScheduledExecutorServiceExpiry e =
                        new ElasticScheduler.ScheduledExecutorServiceExpiry( this, System.currentTimeMillis() + (long) this.parent.ttlSeconds * 1000L );
                this.parent.cache.offerLast( e );
                if ( this.parent.shutdown && this.parent.cache.remove( e ) )
                {
                    this.exec.shutdownNow();
                }
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.NAME )
            {
                return this.parent.scanUnsafe( key );
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                if ( key == Scannable.Attr.CAPACITY )
                {
                    Integer capacity = (Integer) Schedulers.scanExecutor( this.exec, key );
                    if ( capacity == null || capacity == -1 )
                    {
                        return 1;
                    }
                }

                return Schedulers.scanExecutor( this.exec, key );
            }
            else
            {
                return this.isDisposed();
            }
        }
    }
}
