package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

public interface ByteBufAllocatorMetricProvider
{
    ByteBufAllocatorMetric metric();
}
