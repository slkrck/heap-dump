package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.UncheckedBooleanSupplier;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

public class DefaultMaxBytesRecvByteBufAllocator implements MaxBytesRecvByteBufAllocator
{
    private volatile int maxBytesPerRead;
    private volatile int maxBytesPerIndividualRead;

    public DefaultMaxBytesRecvByteBufAllocator()
    {
        this( 65536, 65536 );
    }

    public DefaultMaxBytesRecvByteBufAllocator( int maxBytesPerRead, int maxBytesPerIndividualRead )
    {
        checkMaxBytesPerReadPair( maxBytesPerRead, maxBytesPerIndividualRead );
        this.maxBytesPerRead = maxBytesPerRead;
        this.maxBytesPerIndividualRead = maxBytesPerIndividualRead;
    }

    private static void checkMaxBytesPerReadPair( int maxBytesPerRead, int maxBytesPerIndividualRead )
    {
        ObjectUtil.checkPositive( maxBytesPerRead, "maxBytesPerRead" );
        ObjectUtil.checkPositive( maxBytesPerIndividualRead, "maxBytesPerIndividualRead" );
        if ( maxBytesPerRead < maxBytesPerIndividualRead )
        {
            throw new IllegalArgumentException(
                    "maxBytesPerRead cannot be less than maxBytesPerIndividualRead (" + maxBytesPerIndividualRead + "): " + maxBytesPerRead );
        }
    }

    public RecvByteBufAllocator.Handle newHandle()
    {
        return new DefaultMaxBytesRecvByteBufAllocator.HandleImpl();
    }

    public int maxBytesPerRead()
    {
        return this.maxBytesPerRead;
    }

    public DefaultMaxBytesRecvByteBufAllocator maxBytesPerRead( int maxBytesPerRead )
    {
        ObjectUtil.checkPositive( maxBytesPerRead, "maxBytesPerRead" );
        synchronized ( this )
        {
            int maxBytesPerIndividualRead = this.maxBytesPerIndividualRead();
            if ( maxBytesPerRead < maxBytesPerIndividualRead )
            {
                throw new IllegalArgumentException(
                        "maxBytesPerRead cannot be less than maxBytesPerIndividualRead (" + maxBytesPerIndividualRead + "): " + maxBytesPerRead );
            }
            else
            {
                this.maxBytesPerRead = maxBytesPerRead;
                return this;
            }
        }
    }

    public int maxBytesPerIndividualRead()
    {
        return this.maxBytesPerIndividualRead;
    }

    public DefaultMaxBytesRecvByteBufAllocator maxBytesPerIndividualRead( int maxBytesPerIndividualRead )
    {
        ObjectUtil.checkPositive( maxBytesPerIndividualRead, "maxBytesPerIndividualRead" );
        synchronized ( this )
        {
            int maxBytesPerRead = this.maxBytesPerRead();
            if ( maxBytesPerIndividualRead > maxBytesPerRead )
            {
                throw new IllegalArgumentException(
                        "maxBytesPerIndividualRead cannot be greater than maxBytesPerRead (" + maxBytesPerRead + "): " + maxBytesPerIndividualRead );
            }
            else
            {
                this.maxBytesPerIndividualRead = maxBytesPerIndividualRead;
                return this;
            }
        }
    }

    public synchronized Entry<Integer,Integer> maxBytesPerReadPair()
    {
        return new SimpleEntry( this.maxBytesPerRead, this.maxBytesPerIndividualRead );
    }

    public DefaultMaxBytesRecvByteBufAllocator maxBytesPerReadPair( int maxBytesPerRead, int maxBytesPerIndividualRead )
    {
        checkMaxBytesPerReadPair( maxBytesPerRead, maxBytesPerIndividualRead );
        synchronized ( this )
        {
            this.maxBytesPerRead = maxBytesPerRead;
            this.maxBytesPerIndividualRead = maxBytesPerIndividualRead;
            return this;
        }
    }

    private final class HandleImpl implements RecvByteBufAllocator.ExtendedHandle
    {
        private final UncheckedBooleanSupplier defaultMaybeMoreSupplier;
        private int individualReadMax;
        private int bytesToRead;
        private int lastBytesRead;
        private int attemptBytesRead;

        private HandleImpl()
        {
            this.defaultMaybeMoreSupplier = new UncheckedBooleanSupplier()
            {
                public boolean get()
                {
                    return HandleImpl.this.attemptBytesRead == HandleImpl.this.lastBytesRead;
                }
            };
        }

        public ByteBuf allocate( ByteBufAllocator alloc )
        {
            return alloc.ioBuffer( this.guess() );
        }

        public int guess()
        {
            return Math.min( this.individualReadMax, this.bytesToRead );
        }

        public void reset( ChannelConfig config )
        {
            this.bytesToRead = DefaultMaxBytesRecvByteBufAllocator.this.maxBytesPerRead();
            this.individualReadMax = DefaultMaxBytesRecvByteBufAllocator.this.maxBytesPerIndividualRead();
        }

        public void incMessagesRead( int amt )
        {
        }

        public void lastBytesRead( int bytes )
        {
            this.lastBytesRead = bytes;
            this.bytesToRead -= bytes;
        }

        public int lastBytesRead()
        {
            return this.lastBytesRead;
        }

        public boolean continueReading()
        {
            return this.continueReading( this.defaultMaybeMoreSupplier );
        }

        public boolean continueReading( UncheckedBooleanSupplier maybeMoreDataSupplier )
        {
            return this.bytesToRead > 0 && maybeMoreDataSupplier.get();
        }

        public void readComplete()
        {
        }

        public void attemptedBytesRead( int bytes )
        {
            this.attemptBytesRead = bytes;
        }

        public int attemptedBytesRead()
        {
            return this.attemptBytesRead;
        }
    }
}
