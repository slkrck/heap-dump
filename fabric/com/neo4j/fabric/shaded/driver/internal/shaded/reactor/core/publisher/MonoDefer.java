package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.Objects;
import java.util.function.Supplier;

final class MonoDefer<T> extends Mono<T> implements SourceProducer<T>
{
    final Supplier<? extends Mono<? extends T>> supplier;

    MonoDefer( Supplier<? extends Mono<? extends T>> supplier )
    {
        this.supplier = (Supplier) Objects.requireNonNull( supplier, "supplier" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Mono p;
        try
        {
            p = (Mono) Objects.requireNonNull( this.supplier.get(), "The Mono returned by the supplier is null" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return;
        }

        p.subscribe( actual );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
