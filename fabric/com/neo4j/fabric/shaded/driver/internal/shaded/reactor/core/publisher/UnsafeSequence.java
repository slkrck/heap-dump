package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import java.util.function.LongSupplier;

import sun.misc.Unsafe;

final class UnsafeSequence extends RhsPadding implements RingBuffer.Sequence, LongSupplier
{
    private static final Unsafe UNSAFE = (Unsafe) RingBuffer.getUnsafe();
    private static final long VALUE_OFFSET;

    static
    {
        try
        {
            VALUE_OFFSET = UNSAFE.objectFieldOffset( Value.class.getDeclaredField( "value" ) );
        }
        catch ( Exception var1 )
        {
            throw new RuntimeException( var1 );
        }
    }

    UnsafeSequence( long initialValue )
    {
        UNSAFE.putOrderedLong( this, VALUE_OFFSET, initialValue );
    }

    public long getAsLong()
    {
        return this.value;
    }

    public void set( long value )
    {
        UNSAFE.putOrderedLong( this, VALUE_OFFSET, value );
    }

    public boolean compareAndSet( long expectedValue, long newValue )
    {
        return UNSAFE.compareAndSwapLong( this, VALUE_OFFSET, expectedValue, newValue );
    }
}
