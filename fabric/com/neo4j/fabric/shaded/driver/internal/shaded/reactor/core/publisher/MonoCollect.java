package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

final class MonoCollect<T, R> extends MonoFromFluxOperator<T,R> implements Fuseable
{
    final Supplier<R> supplier;
    final BiConsumer<? super R,? super T> action;

    MonoCollect( Flux<? extends T> source, Supplier<R> supplier, BiConsumer<? super R,? super T> action )
    {
        super( source );
        this.supplier = (Supplier) Objects.requireNonNull( supplier, "supplier" );
        this.action = (BiConsumer) Objects.requireNonNull( action );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        Object container;
        try
        {
            container = Objects.requireNonNull( this.supplier.get(), "The supplier returned a null container" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        return new MonoCollect.CollectSubscriber( actual, this.action, container );
    }

    static final class CollectSubscriber<T, R> extends Operators.MonoSubscriber<T,R>
    {
        final BiConsumer<? super R,? super T> action;
        Subscription s;
        boolean done;

        CollectSubscriber( CoreSubscriber<? super R> actual, BiConsumer<? super R,? super T> action, R container )
        {
            super( actual );
            this.action = action;
            this.value = container;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                try
                {
                    this.action.accept( this.value, t );
                }
                catch ( Throwable var3 )
                {
                    this.onError( Operators.onOperatorError( this, var3, t, this.actual.currentContext() ) );
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                R v = this.value;
                Operators.onDiscard( v, this.currentContext() );
                this.value = null;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.complete( this.value );
            }
        }

        public void setValue( R value )
        {
        }
    }
}
