package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;

import java.util.concurrent.TimeUnit;

import org.reactivestreams.Subscription;

final class FluxMetricsFuseable<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    final String name;
    final Tags tags;
    final MeterRegistry registryCandidate;

    FluxMetricsFuseable( Flux<? extends T> flux )
    {
        this( flux, (MeterRegistry) null );
    }

    FluxMetricsFuseable( Flux<? extends T> flux, @Nullable MeterRegistry candidate )
    {
        super( flux );
        this.name = FluxMetrics.resolveName( flux );
        this.tags = FluxMetrics.resolveTags( flux, FluxMetrics.DEFAULT_TAGS_FLUX, this.name );
        if ( candidate == null )
        {
            this.registryCandidate = Metrics.globalRegistry;
        }
        else
        {
            this.registryCandidate = candidate;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxMetricsFuseable.MetricsFuseableSubscriber( actual, this.registryCandidate, Clock.SYSTEM, this.name, this.tags );
    }

    static final class MetricsFuseableSubscriber<T> extends FluxMetrics.MetricsSubscriber<T> implements Fuseable, Fuseable.QueueSubscription<T>
    {
        int mode;
        @Nullable
        Fuseable.QueueSubscription<T> qs;

        MetricsFuseableSubscriber( CoreSubscriber<? super T> actual, MeterRegistry registry, Clock clock, String sequenceName, Tags sequenceTags )
        {
            super( actual, registry, clock, sequenceName, sequenceTags );
        }

        public void clear()
        {
            if ( this.qs != null )
            {
                this.qs.clear();
            }
        }

        public boolean isEmpty()
        {
            return this.qs == null || this.qs.isEmpty();
        }

        public void onNext( T t )
        {
            if ( this.mode == 2 )
            {
                this.actual.onNext( (Object) null );
            }
            else if ( this.done )
            {
                FluxMetrics.recordMalformed( this.commonTags, this.registry );
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                long last = this.lastNextEventNanos;
                this.lastNextEventNanos = this.clock.monotonicTime();
                this.onNextIntervalTimer.record( this.lastNextEventNanos - last, TimeUnit.NANOSECONDS );
                this.actual.onNext( t );
            }
        }

        @Nullable
        public T poll()
        {
            if ( this.qs == null )
            {
                return null;
            }
            else
            {
                try
                {
                    T v = this.qs.poll();
                    if ( v == null && this.mode == 1 )
                    {
                        FluxMetrics.recordOnComplete( this.commonTags, this.registry, this.subscribeToTerminateSample );
                    }

                    if ( v != null )
                    {
                        long last = this.lastNextEventNanos;
                        this.lastNextEventNanos = this.clock.monotonicTime();
                        this.onNextIntervalTimer.record( this.lastNextEventNanos - last, TimeUnit.NANOSECONDS );
                    }

                    return v;
                }
                catch ( Throwable var4 )
                {
                    FluxMetrics.recordOnError( this.commonTags, this.registry, this.subscribeToTerminateSample, var4 );
                    throw var4;
                }
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                FluxMetrics.recordOnSubscribe( this.commonTags, this.registry );
                this.subscribeToTerminateSample = Timer.start( this.clock );
                this.lastNextEventNanos = this.clock.monotonicTime();
                this.qs = Operators.as( s );
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public int requestFusion( int mode )
        {
            if ( this.qs != null )
            {
                this.mode = this.qs.requestFusion( mode );
                return this.mode;
            }
            else
            {
                return 0;
            }
        }

        public int size()
        {
            return this.qs == null ? 0 : this.qs.size();
        }
    }
}
