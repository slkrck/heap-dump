package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.time.Duration;
import java.util.Objects;

final class MonoJust<T> extends Mono<T> implements Fuseable.ScalarCallable<T>, Fuseable, SourceProducer<T>
{
    final T value;

    MonoJust( T value )
    {
        this.value = Objects.requireNonNull( value, "value" );
    }

    public T call() throws Exception
    {
        return this.value;
    }

    public T block( Duration m )
    {
        return this.value;
    }

    public T block()
    {
        return this.value;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        actual.onSubscribe( Operators.scalarSubscription( actual, this.value ) );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.BUFFERED ? 1 : null;
    }
}
