package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Collections;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.stream.Stream;

public interface Context
{
    static Context empty()
    {
        return Context0.INSTANCE;
    }

    static Context of( Object key, Object value )
    {
        return new Context1( key, value );
    }

    static Context of( Object key1, Object value1, Object key2, Object value2 )
    {
        return new Context2( key1, value1, key2, value2 );
    }

    static Context of( Object key1, Object value1, Object key2, Object value2, Object key3, Object value3 )
    {
        return new Context3( key1, value1, key2, value2, key3, value3 );
    }

    static Context of( Object key1, Object value1, Object key2, Object value2, Object key3, Object value3, Object key4, Object value4 )
    {
        return new Context4( key1, value1, key2, value2, key3, value3, key4, value4 );
    }

    static Context of( Object key1, Object value1, Object key2, Object value2, Object key3, Object value3, Object key4, Object value4, Object key5,
            Object value5 )
    {
        return new Context5( key1, value1, key2, value2, key3, value3, key4, value4, key5, value5 );
    }

    static Context of( Map<?,?> map )
    {
        int size = ((Map) Objects.requireNonNull( map, "map" )).size();
        if ( size == 0 )
        {
            return empty();
        }
        else
        {
            if ( size <= 5 )
            {
                Entry[] entries = (Entry[]) map.entrySet().toArray( new Entry[size] );
                switch ( size )
                {
                case 1:
                    return new Context1( entries[0].getKey(), entries[0].getValue() );
                case 2:
                    return new Context2( entries[0].getKey(), entries[0].getValue(), entries[1].getKey(), entries[1].getValue() );
                case 3:
                    return new Context3( entries[0].getKey(), entries[0].getValue(), entries[1].getKey(), entries[1].getValue(), entries[2].getKey(),
                            entries[2].getValue() );
                case 4:
                    return new Context4( entries[0].getKey(), entries[0].getValue(), entries[1].getKey(), entries[1].getValue(), entries[2].getKey(),
                            entries[2].getValue(), entries[3].getKey(), entries[3].getValue() );
                case 5:
                    return new Context5( entries[0].getKey(), entries[0].getValue(), entries[1].getKey(), entries[1].getValue(), entries[2].getKey(),
                            entries[2].getValue(), entries[3].getKey(), entries[3].getValue(), entries[4].getKey(), entries[4].getValue() );
                }
            }

            return new ContextN( Collections.emptyMap(), map );
        }
    }

    <T> T get( Object var1 );

    default <T> T get( Class<T> key )
    {
        T v = this.get( (Object) key );
        if ( key.isInstance( v ) )
        {
            return v;
        }
        else
        {
            throw new NoSuchElementException( "Context does not contain a value of type " + key.getName() );
        }
    }

    @Nullable
    default <T> T getOrDefault( Object key, @Nullable T defaultValue )
    {
        return !this.hasKey( key ) ? defaultValue : this.get( key );
    }

    default <T> Optional<T> getOrEmpty( Object key )
    {
        return this.hasKey( key ) ? Optional.of( this.get( key ) ) : Optional.empty();
    }

    boolean hasKey( Object var1 );

    default boolean isEmpty()
    {
        return this == Context0.INSTANCE || this instanceof Context0;
    }

    Context put( Object var1, Object var2 );

    default Context putNonNull( Object key, @Nullable Object valueOrNull )
    {
        return valueOrNull != null ? this.put( key, valueOrNull ) : this;
    }

    Context delete( Object var1 );

    int size();

    Stream<Entry<Object,Object>> stream();

    default Context putAll( Context other )
    {
        return other.isEmpty() ? this : (Context) other.stream().reduce( this, ( c, e ) -> {
            return c.put( e.getKey(), e.getValue() );
        }, ( c1, c2 ) -> {
            throw new UnsupportedOperationException( "Context.putAll should not use a parallelized stream" );
        } );
    }
}
