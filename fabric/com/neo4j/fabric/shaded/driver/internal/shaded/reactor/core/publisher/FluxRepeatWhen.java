package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxRepeatWhen<T> extends InternalFluxOperator<T,T>
{
    final Function<? super Flux<Long>,? extends Publisher<?>> whenSourceFactory;

    FluxRepeatWhen( Flux<? extends T> source, Function<? super Flux<Long>,? extends Publisher<?>> whenSourceFactory )
    {
        super( source );
        this.whenSourceFactory = (Function) Objects.requireNonNull( whenSourceFactory, "whenSourceFactory" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRepeatWhen.RepeatWhenOtherSubscriber other = new FluxRepeatWhen.RepeatWhenOtherSubscriber();
        Subscriber<Long> signaller = Operators.serialize( other.completionSignal );
        signaller.onSubscribe( Operators.emptySubscription() );
        CoreSubscriber<T> serial = Operators.serialize( actual );
        FluxRepeatWhen.RepeatWhenMainSubscriber<T> main = new FluxRepeatWhen.RepeatWhenMainSubscriber( serial, signaller, this.source );
        other.main = main;
        serial.onSubscribe( main );

        Publisher p;
        try
        {
            p = (Publisher) Objects.requireNonNull( this.whenSourceFactory.apply( other ), "The whenSourceFactory returned a null Publisher" );
        }
        catch ( Throwable var8 )
        {
            actual.onError( Operators.onOperatorError( var8, actual.currentContext() ) );
            return null;
        }

        p.subscribe( other );
        return !main.cancelled ? main : null;
    }

    static final class RepeatWhenOtherSubscriber extends Flux<Long> implements InnerConsumer<Object>, OptimizableOperator<Long,Long>
    {
        final DirectProcessor<Long> completionSignal = new DirectProcessor();
        FluxRepeatWhen.RepeatWhenMainSubscriber<?> main;

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.main.otherArbiter;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL ? this.main : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            this.main.setWhen( s );
        }

        public void onNext( Object t )
        {
            this.main.resubscribe( t );
        }

        public void onError( Throwable t )
        {
            this.main.whenError( t );
        }

        public void onComplete()
        {
            this.main.whenComplete();
        }

        public void subscribe( CoreSubscriber<? super Long> actual )
        {
            this.completionSignal.subscribe( actual );
        }

        public CoreSubscriber<? super Long> subscribeOrReturn( CoreSubscriber<? super Long> actual )
        {
            return actual;
        }

        public DirectProcessor<Long> source()
        {
            return this.completionSignal;
        }

        public OptimizableOperator<?,? extends Long> nextOptimizableSource()
        {
            return null;
        }
    }

    static final class RepeatWhenMainSubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxRepeatWhen.RepeatWhenMainSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxRepeatWhen.RepeatWhenMainSubscriber.class, "wip" );
        final Operators.DeferredSubscription otherArbiter;
        final Subscriber<Long> signaller;
        final CorePublisher<? extends T> source;
        volatile int wip;
        Context context;
        long produced;

        RepeatWhenMainSubscriber( CoreSubscriber<? super T> actual, Subscriber<Long> signaller, CorePublisher<? extends T> source )
        {
            super( actual );
            this.signaller = signaller;
            this.source = source;
            this.otherArbiter = new Operators.DeferredSubscription();
            this.context = actual.currentContext();
        }

        public Context currentContext()
        {
            return this.context;
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( Scannable.from( this.signaller ), this.otherArbiter );
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.otherArbiter.cancel();
                super.cancel();
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
            ++this.produced;
        }

        public void onError( Throwable t )
        {
            this.otherArbiter.cancel();
            this.actual.onError( t );
        }

        public void onComplete()
        {
            long p = this.produced;
            if ( p != 0L )
            {
                this.produced = 0L;
                this.produced( p );
            }

            this.otherArbiter.request( 1L );
            this.signaller.onNext( p );
        }

        void setWhen( Subscription w )
        {
            this.otherArbiter.set( w );
        }

        void resubscribe( Object trigger )
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                do
                {
                    if ( this.cancelled )
                    {
                        return;
                    }

                    if ( trigger instanceof Context )
                    {
                        this.context = (Context) trigger;
                    }

                    this.source.subscribe( this );
                }
                while ( WIP.decrementAndGet( this ) != 0 );
            }
        }

        void whenError( Throwable e )
        {
            super.cancel();
            this.actual.onError( e );
        }

        void whenComplete()
        {
            super.cancel();
            this.actual.onComplete();
        }
    }
}
