package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.LongSupplier;
import java.util.function.Supplier;

abstract class RingBuffer<E> implements LongSupplier
{
    static final long INITIAL_CURSOR_VALUE = -1L;
    private static final boolean HAS_UNSAFE = hasUnsafe0();

    static <T> void addSequence( T holder, AtomicReferenceFieldUpdater<T,RingBuffer.Sequence[]> updater, RingBuffer.Sequence sequence )
    {
        RingBuffer.Sequence[] updatedSequences;
        RingBuffer.Sequence[] currentSequences;
        do
        {
            currentSequences = (RingBuffer.Sequence[]) updater.get( holder );
            updatedSequences = (RingBuffer.Sequence[]) Arrays.copyOf( currentSequences, currentSequences.length + 1 );
            updatedSequences[currentSequences.length] = sequence;
        }
        while ( !updater.compareAndSet( holder, currentSequences, updatedSequences ) );
    }

    private static <T> int countMatching( T[] values, T toMatch )
    {
        int numToRemove = 0;
        Object[] var3 = values;
        int var4 = values.length;

        for ( int var5 = 0; var5 < var4; ++var5 )
        {
            T value = var3[var5];
            if ( value == toMatch )
            {
                ++numToRemove;
            }
        }

        return numToRemove;
    }

    static <T> boolean removeSequence( T holder, AtomicReferenceFieldUpdater<T,RingBuffer.Sequence[]> sequenceUpdater, RingBuffer.Sequence sequence )
    {
        while ( true )
        {
            RingBuffer.Sequence[] oldSequences = (RingBuffer.Sequence[]) sequenceUpdater.get( holder );
            int numToRemove = countMatching( oldSequences, sequence );
            if ( 0 != numToRemove )
            {
                int oldSize = oldSequences.length;
                RingBuffer.Sequence[] newSequences = new RingBuffer.Sequence[oldSize - numToRemove];
                int i = 0;

                for ( int var8 = 0; i < oldSize; ++i )
                {
                    RingBuffer.Sequence testSequence = oldSequences[i];
                    if ( sequence != testSequence )
                    {
                        newSequences[var8++] = testSequence;
                    }
                }

                if ( !sequenceUpdater.compareAndSet( holder, oldSequences, newSequences ) )
                {
                    continue;
                }
            }

            return numToRemove != 0;
        }
    }

    static <E> RingBuffer<E> createMultiProducer( Supplier<E> factory, int bufferSize, WaitStrategy waitStrategy, Runnable spinObserver )
    {
        if ( hasUnsafe() )
        {
            MultiProducerRingBuffer sequencer = new MultiProducerRingBuffer( bufferSize, waitStrategy, spinObserver );
            return new UnsafeRingBuffer( factory, sequencer );
        }
        else
        {
            throw new IllegalStateException( "This JVM does not support sun.misc.Unsafe" );
        }
    }

    static <E> RingBuffer<E> createSingleProducer( Supplier<E> factory, int bufferSize, WaitStrategy waitStrategy )
    {
        return createSingleProducer( factory, bufferSize, waitStrategy, (Runnable) null );
    }

    static <E> RingBuffer<E> createSingleProducer( Supplier<E> factory, int bufferSize, WaitStrategy waitStrategy, @Nullable Runnable spinObserver )
    {
        SingleProducerSequencer sequencer = new SingleProducerSequencer( bufferSize, waitStrategy, spinObserver );
        return (RingBuffer) (hasUnsafe() && Queues.isPowerOfTwo( bufferSize ) ? new UnsafeRingBuffer( factory, sequencer )
                                                                              : new NotFunRingBuffer( factory, sequencer ));
    }

    static long getMinimumSequence( RingBuffer.Sequence[] sequences, long minimum )
    {
        int i = 0;

        for ( int n = sequences.length; i < n; ++i )
        {
            long value = sequences[i].getAsLong();
            minimum = Math.min( minimum, value );
        }

        return minimum;
    }

    static long getMinimumSequence( @Nullable RingBuffer.Sequence excludeSequence, RingBuffer.Sequence[] sequences, long minimum )
    {
        int i = 0;

        for ( int n = sequences.length; i < n; ++i )
        {
            if ( excludeSequence == null || sequences[i] != excludeSequence )
            {
                long value = sequences[i].getAsLong();
                minimum = Math.min( minimum, value );
            }
        }

        return minimum;
    }

    static <T> T getUnsafe()
    {
        return UnsafeSupport.getUnsafe();
    }

    static int log2( int i )
    {
        int r;
        for ( r = 0; (i >>= 1) != 0; ++r )
        {
        }

        return r;
    }

    static RingBuffer.Sequence newSequence( long init )
    {
        return (RingBuffer.Sequence) (hasUnsafe() ? new UnsafeSequence( init ) : new AtomicSequence( init ));
    }

    static boolean hasUnsafe()
    {
        return HAS_UNSAFE;
    }

    static boolean hasUnsafe0()
    {
        return UnsafeSupport.hasUnsafe();
    }

    abstract void addGatingSequence( RingBuffer.Sequence var1 );

    abstract int bufferSize();

    abstract E get( long var1 );

    public long getAsLong()
    {
        return this.getCursor();
    }

    abstract long getCursor();

    abstract long getMinimumGatingSequence();

    abstract long getMinimumGatingSequence( RingBuffer.Sequence var1 );

    abstract int getPending();

    RingBuffer.Sequence[] getSequenceReceivers()
    {
        return this.getSequencer().getGatingSequences();
    }

    abstract RingBuffer.Reader newReader();

    abstract long next();

    abstract long next( int var1 );

    abstract void publish( long var1 );

    abstract boolean removeGatingSequence( RingBuffer.Sequence var1 );

    abstract RingBufferProducer getSequencer();

    interface Sequence extends LongSupplier
    {
        long INITIAL_VALUE = -1L;

        void set( long var1 );

        boolean compareAndSet( long var1, long var3 );
    }

    static final class Reader
    {
        private final WaitStrategy waitStrategy;
        private final RingBuffer.Sequence cursorSequence;
        private final RingBufferProducer sequenceProducer;
        private volatile boolean alerted = false;

        Reader( RingBufferProducer sequenceProducer, WaitStrategy waitStrategy, RingBuffer.Sequence cursorSequence )
        {
            this.sequenceProducer = sequenceProducer;
            this.waitStrategy = waitStrategy;
            this.cursorSequence = cursorSequence;
        }

        long waitFor( long sequence, Runnable consumer ) throws InterruptedException
        {
            if ( this.alerted )
            {
                WaitStrategy.alert();
            }

            long availableSequence = this.waitStrategy.waitFor( sequence, this.cursorSequence, consumer );
            return availableSequence < sequence ? availableSequence : this.sequenceProducer.getHighestPublishedSequence( sequence, availableSequence );
        }

        boolean isAlerted()
        {
            return this.alerted;
        }

        void alert()
        {
            this.alerted = true;
            this.waitStrategy.signalAllWhenBlocking();
        }

        void signal()
        {
            this.waitStrategy.signalAllWhenBlocking();
        }

        void clearAlert()
        {
            this.alerted = false;
        }
    }
}
