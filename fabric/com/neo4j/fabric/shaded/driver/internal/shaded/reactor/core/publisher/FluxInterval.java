package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

import org.reactivestreams.Subscription;

final class FluxInterval extends Flux<Long> implements SourceProducer<Long>
{
    final Scheduler timedScheduler;
    final long initialDelay;
    final long period;
    final TimeUnit unit;

    FluxInterval( long initialDelay, long period, TimeUnit unit, Scheduler timedScheduler )
    {
        if ( period < 0L )
        {
            throw new IllegalArgumentException( "period >= 0 required but it was " + period );
        }
        else
        {
            this.initialDelay = initialDelay;
            this.period = period;
            this.unit = (TimeUnit) Objects.requireNonNull( unit, "unit" );
            this.timedScheduler = (Scheduler) Objects.requireNonNull( timedScheduler, "timedScheduler" );
        }
    }

    public void subscribe( CoreSubscriber<? super Long> actual )
    {
        Scheduler.Worker w = this.timedScheduler.createWorker();
        FluxInterval.IntervalRunnable r = new FluxInterval.IntervalRunnable( actual, w );
        actual.onSubscribe( r );

        try
        {
            w.schedulePeriodically( r, this.initialDelay, this.period, this.unit );
        }
        catch ( RejectedExecutionException var5 )
        {
            if ( !r.cancelled )
            {
                actual.onError( Operators.onRejectedExecution( var5, r, (Throwable) null, (Object) null, actual.currentContext() ) );
            }
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.timedScheduler : null;
    }

    static final class IntervalRunnable implements Runnable, Subscription, InnerProducer<Long>
    {
        static final AtomicLongFieldUpdater<FluxInterval.IntervalRunnable> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxInterval.IntervalRunnable.class, "requested" );
        final CoreSubscriber<? super Long> actual;
        final Scheduler.Worker worker;
        volatile long requested;
        long count;
        volatile boolean cancelled;

        IntervalRunnable( CoreSubscriber<? super Long> actual, Scheduler.Worker worker )
        {
            this.actual = actual;
            this.worker = worker;
        }

        public CoreSubscriber<? super Long> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.worker : InnerProducer.super.scanUnsafe( key );
            }
        }

        public void run()
        {
            if ( !this.cancelled )
            {
                if ( this.requested != 0L )
                {
                    this.actual.onNext( Long.valueOf( (long) (this.count++) ) );
                    if ( this.requested != Long.MAX_VALUE )
                    {
                        REQUESTED.decrementAndGet( this );
                    }
                }
                else
                {
                    this.cancel();
                    this.actual.onError( Exceptions.failWithOverflow( "Could not emit tick " + this.count +
                            " due to lack of requests (interval doesn't support small downstream requests that replenish slower than the ticks)" ) );
                }
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.worker.dispose();
            }
        }
    }
}
