package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;

final class MonoElapsed<T> extends InternalMonoOperator<T,Tuple2<Long,T>> implements Fuseable
{
    final Scheduler scheduler;

    MonoElapsed( Mono<T> source, Scheduler scheduler )
    {
        super( source );
        this.scheduler = scheduler;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Tuple2<Long,T>> actual )
    {
        return new FluxElapsed.ElapsedSubscriber( actual, this.scheduler );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }
}
