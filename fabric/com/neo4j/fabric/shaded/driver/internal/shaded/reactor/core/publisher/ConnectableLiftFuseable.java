package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import org.reactivestreams.Publisher;

final class ConnectableLiftFuseable<I, O> extends InternalConnectableFluxOperator<I,O> implements Scannable, Fuseable
{
    final BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter;

    ConnectableLiftFuseable( ConnectableFlux<I> p, BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter )
    {
        super( (ConnectableFlux) Objects.requireNonNull( p, "source" ) );
        this.lifter = lifter;
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    public void connect( Consumer<? super Disposable> cancelSupport )
    {
        this.source.connect();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.source.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.PARENT ? this.source : null;
        }
    }

    public final CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super O> actual )
    {
        CoreSubscriber<? super I> input = (CoreSubscriber) this.lifter.apply( this.source, actual );
        Objects.requireNonNull( input, "Lifted subscriber MUST NOT be null" );
        if ( actual instanceof Fuseable.QueueSubscription && !(input instanceof Fuseable.QueueSubscription) )
        {
            input = new FluxHide.SuppressFuseableSubscriber( (CoreSubscriber) input );
        }

        return (CoreSubscriber) input;
    }
}
