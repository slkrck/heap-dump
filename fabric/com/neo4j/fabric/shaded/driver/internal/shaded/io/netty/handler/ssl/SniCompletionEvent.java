package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

public final class SniCompletionEvent extends SslCompletionEvent
{
    private final String hostname;

    SniCompletionEvent( String hostname )
    {
        this.hostname = hostname;
    }

    SniCompletionEvent( String hostname, Throwable cause )
    {
        super( cause );
        this.hostname = hostname;
    }

    SniCompletionEvent( Throwable cause )
    {
        this( (String) null, cause );
    }

    public String hostname()
    {
        return this.hostname;
    }

    public String toString()
    {
        Throwable cause = this.cause();
        return cause == null ? this.getClass().getSimpleName() + "(SUCCESS='" + this.hostname + "'\")" : this.getClass().getSimpleName() + '(' + cause + ')';
    }
}
