package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

public interface HashingStrategy<T>
{
    HashingStrategy JAVA_HASHER = new HashingStrategy()
    {
        public int hashCode( Object obj )
        {
            return obj != null ? obj.hashCode() : 0;
        }

        public boolean equals( Object a, Object b )
        {
            return a == b || a != null && a.equals( b );
        }
    };

    int hashCode( T var1 );

    boolean equals( T var1, T var2 );
}
