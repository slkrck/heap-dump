package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakTracker;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.nio.ByteOrder;

class SimpleLeakAwareByteBuf extends WrappedByteBuf
{
    final ResourceLeakTracker<ByteBuf> leak;
    private final ByteBuf trackedByteBuf;

    SimpleLeakAwareByteBuf( ByteBuf wrapped, ByteBuf trackedByteBuf, ResourceLeakTracker<ByteBuf> leak )
    {
        super( wrapped );
        this.trackedByteBuf = (ByteBuf) ObjectUtil.checkNotNull( trackedByteBuf, "trackedByteBuf" );
        this.leak = (ResourceLeakTracker) ObjectUtil.checkNotNull( leak, "leak" );
    }

    SimpleLeakAwareByteBuf( ByteBuf wrapped, ResourceLeakTracker<ByteBuf> leak )
    {
        this( wrapped, wrapped, leak );
    }

    private static ByteBuf unwrapSwapped( ByteBuf buf )
    {
        if ( !(buf instanceof SwappedByteBuf) )
        {
            return buf;
        }
        else
        {
            do
            {
                buf = buf.unwrap();
            }
            while ( buf instanceof SwappedByteBuf );

            return buf;
        }
    }

    public ByteBuf slice()
    {
        return this.newSharedLeakAwareByteBuf( super.slice() );
    }

    public ByteBuf retainedSlice()
    {
        return this.unwrappedDerived( super.retainedSlice() );
    }

    public ByteBuf retainedSlice( int index, int length )
    {
        return this.unwrappedDerived( super.retainedSlice( index, length ) );
    }

    public ByteBuf retainedDuplicate()
    {
        return this.unwrappedDerived( super.retainedDuplicate() );
    }

    public ByteBuf readRetainedSlice( int length )
    {
        return this.unwrappedDerived( super.readRetainedSlice( length ) );
    }

    public ByteBuf slice( int index, int length )
    {
        return this.newSharedLeakAwareByteBuf( super.slice( index, length ) );
    }

    public ByteBuf duplicate()
    {
        return this.newSharedLeakAwareByteBuf( super.duplicate() );
    }

    public ByteBuf readSlice( int length )
    {
        return this.newSharedLeakAwareByteBuf( super.readSlice( length ) );
    }

    public ByteBuf asReadOnly()
    {
        return this.newSharedLeakAwareByteBuf( super.asReadOnly() );
    }

    public ByteBuf touch()
    {
        return this;
    }

    public ByteBuf touch( Object hint )
    {
        return this;
    }

    public boolean release()
    {
        if ( super.release() )
        {
            this.closeLeak();
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean release( int decrement )
    {
        if ( super.release( decrement ) )
        {
            this.closeLeak();
            return true;
        }
        else
        {
            return false;
        }
    }

    private void closeLeak()
    {
        boolean closed = this.leak.close( this.trackedByteBuf );

        assert closed;
    }

    public ByteBuf order( ByteOrder endianness )
    {
        return this.order() == endianness ? this : this.newSharedLeakAwareByteBuf( super.order( endianness ) );
    }

    private ByteBuf unwrappedDerived( ByteBuf derived )
    {
        ByteBuf unwrappedDerived = unwrapSwapped( derived );
        if ( unwrappedDerived instanceof AbstractPooledDerivedByteBuf )
        {
            ((AbstractPooledDerivedByteBuf) unwrappedDerived).parent( this );
            ResourceLeakTracker<ByteBuf> newLeak = AbstractByteBuf.leakDetector.track( derived );
            return (ByteBuf) (newLeak == null ? derived : this.newLeakAwareByteBuf( derived, newLeak ));
        }
        else
        {
            return this.newSharedLeakAwareByteBuf( derived );
        }
    }

    private SimpleLeakAwareByteBuf newSharedLeakAwareByteBuf( ByteBuf wrapped )
    {
        return this.newLeakAwareByteBuf( wrapped, this.trackedByteBuf, this.leak );
    }

    private SimpleLeakAwareByteBuf newLeakAwareByteBuf( ByteBuf wrapped, ResourceLeakTracker<ByteBuf> leakTracker )
    {
        return this.newLeakAwareByteBuf( wrapped, wrapped, leakTracker );
    }

    protected SimpleLeakAwareByteBuf newLeakAwareByteBuf( ByteBuf buf, ByteBuf trackedByteBuf, ResourceLeakTracker<ByteBuf> leakTracker )
    {
        return new SimpleLeakAwareByteBuf( buf, trackedByteBuf, leakTracker );
    }
}
