package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

import java.nio.ByteBuffer;

final class WrappedUnpooledUnsafeDirectByteBuf extends UnpooledUnsafeDirectByteBuf
{
    WrappedUnpooledUnsafeDirectByteBuf( ByteBufAllocator alloc, long memoryAddress, int size, boolean doFree )
    {
        super( alloc, PlatformDependent.directBuffer( memoryAddress, size ), size, doFree );
    }

    protected void freeDirect( ByteBuffer buffer )
    {
        PlatformDependent.freeMemory( this.memoryAddress );
    }
}
