package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import java.io.Serializable;

public interface ChannelId extends Serializable, Comparable<ChannelId>
{
    String asShortText();

    String asLongText();
}
