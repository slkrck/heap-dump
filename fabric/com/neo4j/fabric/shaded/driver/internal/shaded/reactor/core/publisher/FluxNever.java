package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import org.reactivestreams.Publisher;

final class FluxNever extends Flux<Object> implements SourceProducer<Object>
{
    static final Publisher<Object> INSTANCE = new FluxNever();

    static <T> Flux<T> instance()
    {
        return (Flux) INSTANCE;
    }

    public void subscribe( CoreSubscriber<? super Object> actual )
    {
        actual.onSubscribe( Operators.emptySubscription() );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
