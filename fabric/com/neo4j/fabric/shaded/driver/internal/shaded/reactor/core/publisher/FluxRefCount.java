package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Consumer;

import org.reactivestreams.Subscription;

final class FluxRefCount<T> extends Flux<T> implements Scannable, Fuseable
{
    final ConnectableFlux<? extends T> source;
    final int n;
    @Nullable
    FluxRefCount.RefCountMonitor<T> connection;

    FluxRefCount( ConnectableFlux<? extends T> source, int n )
    {
        if ( n <= 0 )
        {
            throw new IllegalArgumentException( "n > 0 required but it was " + n );
        }
        else
        {
            this.source = (ConnectableFlux) Objects.requireNonNull( source, "source" );
            this.n = n;
        }
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        boolean connect = false;
        FluxRefCount.RefCountMonitor conn;
        synchronized ( this )
        {
            conn = this.connection;
            if ( conn == null || conn.terminated )
            {
                conn = new FluxRefCount.RefCountMonitor( this );
                this.connection = conn;
            }

            long c = conn.subscribers++;
            if ( !conn.connected && c + 1L == (long) this.n )
            {
                connect = true;
                conn.connected = true;
            }
        }

        this.source.subscribe( new FluxRefCount.RefCountInner( actual, conn ) );
        if ( connect )
        {
            this.source.connect( conn );
        }
    }

    void cancel( FluxRefCount.RefCountMonitor rc )
    {
        Disposable dispose = null;
        synchronized ( this )
        {
            if ( rc.terminated )
            {
                return;
            }

            long c = rc.subscribers - 1L;
            rc.subscribers = c;
            if ( c != 0L || !rc.connected )
            {
                return;
            }

            if ( rc == this.connection )
            {
                dispose = (Disposable) FluxRefCount.RefCountMonitor.DISCONNECT.getAndSet( rc, Disposables.disposed() );
                this.connection = null;
            }
        }

        if ( dispose != null )
        {
            dispose.dispose();
        }
    }

    void terminated( FluxRefCount.RefCountMonitor rc )
    {
        synchronized ( this )
        {
            if ( !rc.terminated )
            {
                rc.terminated = true;
                this.connection = null;
            }
        }
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.PARENT ? this.source : null;
        }
    }

    static final class RefCountInner<T> implements Fuseable.QueueSubscription<T>, InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxRefCount.RefCountInner> PARENT_DONE =
                AtomicIntegerFieldUpdater.newUpdater( FluxRefCount.RefCountInner.class, "parentDone" );
        final CoreSubscriber<? super T> actual;
        final FluxRefCount.RefCountMonitor<T> connection;
        Subscription s;
        Fuseable.QueueSubscription<T> qs;
        volatile int parentDone;

        RefCountInner( CoreSubscriber<? super T> actual, FluxRefCount.RefCountMonitor<T> connection )
        {
            this.actual = actual;
            this.connection = connection;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.parentDone == 1;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.parentDone == 2 : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            if ( PARENT_DONE.compareAndSet( this, 0, 1 ) )
            {
                this.connection.upstreamFinished();
                this.actual.onError( t );
            }
            else
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
        }

        public void onComplete()
        {
            if ( PARENT_DONE.compareAndSet( this, 0, 1 ) )
            {
                this.connection.upstreamFinished();
                this.actual.onComplete();
            }
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
            if ( PARENT_DONE.compareAndSet( this, 0, 2 ) )
            {
                this.connection.innerCancelled();
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public int requestFusion( int requestedMode )
        {
            if ( this.s instanceof Fuseable.QueueSubscription )
            {
                this.qs = (Fuseable.QueueSubscription) this.s;
                return this.qs.requestFusion( requestedMode );
            }
            else
            {
                return 0;
            }
        }

        @Nullable
        public T poll()
        {
            return this.qs.poll();
        }

        public int size()
        {
            return this.qs.size();
        }

        public boolean isEmpty()
        {
            return this.qs.isEmpty();
        }

        public void clear()
        {
            this.qs.clear();
        }
    }

    static final class RefCountMonitor<T> implements Consumer<Disposable>
    {
        static final AtomicReferenceFieldUpdater<FluxRefCount.RefCountMonitor,Disposable> DISCONNECT =
                AtomicReferenceFieldUpdater.newUpdater( FluxRefCount.RefCountMonitor.class, Disposable.class, "disconnect" );
        final FluxRefCount<? extends T> parent;
        long subscribers;
        boolean terminated;
        boolean connected;
        volatile Disposable disconnect;

        RefCountMonitor( FluxRefCount<? extends T> parent )
        {
            this.parent = parent;
        }

        public void accept( Disposable r )
        {
            OperatorDisposables.replace( DISCONNECT, this, r );
        }

        void innerCancelled()
        {
            this.parent.cancel( this );
        }

        void upstreamFinished()
        {
            this.parent.terminated( this );
        }
    }
}
