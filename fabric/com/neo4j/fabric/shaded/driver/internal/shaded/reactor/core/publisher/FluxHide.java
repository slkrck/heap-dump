package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.reactivestreams.Subscription;

final class FluxHide<T> extends InternalFluxOperator<T,T>
{
    FluxHide( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxHide.HideSubscriber( actual );
    }

    static final class SuppressFuseableSubscriber<T> implements InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        final CoreSubscriber<? super T> actual;
        Subscription s;

        SuppressFuseableSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }

        public int requestFusion( int requestedMode )
        {
            return 0;
        }

        @Nullable
        public T poll()
        {
            return null;
        }

        public boolean isEmpty()
        {
            return false;
        }

        public void clear()
        {
        }

        public int size()
        {
            return 0;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }
    }

    static final class HideSubscriber<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        Subscription s;

        HideSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            this.s = s;
            this.actual.onSubscribe( this );
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }
    }
}
