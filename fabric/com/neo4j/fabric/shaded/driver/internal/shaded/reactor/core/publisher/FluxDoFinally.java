package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.function.Consumer;

import org.reactivestreams.Subscription;

final class FluxDoFinally<T> extends InternalFluxOperator<T,T>
{
    final Consumer<SignalType> onFinally;

    FluxDoFinally( Flux<? extends T> source, Consumer<SignalType> onFinally )
    {
        super( source );
        this.onFinally = onFinally;
    }

    static <T> CoreSubscriber<T> createSubscriber( CoreSubscriber<? super T> s, Consumer<SignalType> onFinally, boolean fuseable )
    {
        if ( fuseable )
        {
            return (CoreSubscriber) (s instanceof Fuseable.ConditionalSubscriber ? new FluxDoFinally.DoFinallyFuseableConditionalSubscriber(
                    (Fuseable.ConditionalSubscriber) s, onFinally ) : new FluxDoFinally.DoFinallyFuseableSubscriber( s, onFinally ));
        }
        else
        {
            return (CoreSubscriber) (s instanceof Fuseable.ConditionalSubscriber ? new FluxDoFinally.DoFinallyConditionalSubscriber(
                    (Fuseable.ConditionalSubscriber) s, onFinally ) : new FluxDoFinally.DoFinallySubscriber( s, onFinally ));
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return createSubscriber( actual, this.onFinally, false );
    }

    static final class DoFinallyFuseableConditionalSubscriber<T> extends FluxDoFinally.DoFinallyFuseableSubscriber<T>
            implements Fuseable.ConditionalSubscriber<T>
    {
        DoFinallyFuseableConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, Consumer<SignalType> onFinally )
        {
            super( actual, onFinally );
        }

        public boolean tryOnNext( T t )
        {
            return ((Fuseable.ConditionalSubscriber) this.actual).tryOnNext( t );
        }
    }

    static final class DoFinallyConditionalSubscriber<T> extends FluxDoFinally.DoFinallySubscriber<T> implements Fuseable.ConditionalSubscriber<T>
    {
        DoFinallyConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, Consumer<SignalType> onFinally )
        {
            super( actual, onFinally );
        }

        public boolean tryOnNext( T t )
        {
            return ((Fuseable.ConditionalSubscriber) this.actual).tryOnNext( t );
        }
    }

    static class DoFinallyFuseableSubscriber<T> extends FluxDoFinally.DoFinallySubscriber<T> implements Fuseable, Fuseable.QueueSubscription<T>
    {
        DoFinallyFuseableSubscriber( CoreSubscriber<? super T> actual, Consumer<SignalType> onFinally )
        {
            super( actual, onFinally );
        }

        public int requestFusion( int mode )
        {
            Fuseable.QueueSubscription<T> qs = this.qs;
            if ( qs != null && (mode & 4) == 0 )
            {
                int m = qs.requestFusion( mode );
                if ( m != 0 )
                {
                    this.syncFused = m == 1;
                }

                return m;
            }
            else
            {
                return 0;
            }
        }

        public void clear()
        {
            if ( this.qs != null )
            {
                this.qs.clear();
            }
        }

        public boolean isEmpty()
        {
            return this.qs == null || this.qs.isEmpty();
        }

        @Nullable
        public T poll()
        {
            if ( this.qs == null )
            {
                return null;
            }
            else
            {
                T v = this.qs.poll();
                if ( v == null && this.syncFused )
                {
                    this.runFinally( SignalType.ON_COMPLETE );
                }

                return v;
            }
        }

        public int size()
        {
            return this.qs == null ? 0 : this.qs.size();
        }
    }

    static class DoFinallySubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxDoFinally.DoFinallySubscriber> ONCE =
                AtomicIntegerFieldUpdater.newUpdater( FluxDoFinally.DoFinallySubscriber.class, "once" );
        final CoreSubscriber<? super T> actual;
        final Consumer<SignalType> onFinally;
        volatile int once;
        Fuseable.QueueSubscription<T> qs;
        Subscription s;
        boolean syncFused;

        DoFinallySubscriber( CoreSubscriber<? super T> actual, Consumer<SignalType> onFinally )
        {
            this.actual = actual;
            this.onFinally = onFinally;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED ? InnerOperator.super.scanUnsafe( key ) : this.once == 1;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    this.qs = (Fuseable.QueueSubscription) s;
                }

                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            try
            {
                this.actual.onError( t );
            }
            finally
            {
                this.runFinally( SignalType.ON_ERROR );
            }
        }

        public void onComplete()
        {
            this.actual.onComplete();
            this.runFinally( SignalType.ON_COMPLETE );
        }

        public void cancel()
        {
            this.s.cancel();
            this.runFinally( SignalType.CANCEL );
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        void runFinally( SignalType signalType )
        {
            if ( ONCE.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.onFinally.accept( signalType );
                }
                catch ( Throwable var3 )
                {
                    Exceptions.throwIfFatal( var3 );
                    Operators.onErrorDropped( var3, this.actual.currentContext() );
                }
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }
    }
}
