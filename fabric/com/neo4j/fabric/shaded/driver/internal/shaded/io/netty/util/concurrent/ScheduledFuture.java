package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

public interface ScheduledFuture<V> extends Future<V>, java.util.concurrent.ScheduledFuture<V>
{
}
