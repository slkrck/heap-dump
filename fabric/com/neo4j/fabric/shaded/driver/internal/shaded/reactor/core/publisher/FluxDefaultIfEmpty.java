package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

import org.reactivestreams.Subscription;

final class FluxDefaultIfEmpty<T> extends InternalFluxOperator<T,T>
{
    final T value;

    FluxDefaultIfEmpty( Flux<? extends T> source, T value )
    {
        super( source );
        this.value = Objects.requireNonNull( value, "value" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxDefaultIfEmpty.DefaultIfEmptySubscriber( actual, this.value );
    }

    static final class DefaultIfEmptySubscriber<T> extends Operators.MonoSubscriber<T,T>
    {
        Subscription s;
        boolean hasValue;

        DefaultIfEmptySubscriber( CoreSubscriber<? super T> actual, T value )
        {
            super( actual );
            this.value = value;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
        }

        public void request( long n )
        {
            super.request( n );
            this.s.request( n );
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( !this.hasValue )
            {
                this.hasValue = true;
            }

            this.actual.onNext( t );
        }

        public void onComplete()
        {
            if ( this.hasValue )
            {
                this.actual.onComplete();
            }
            else
            {
                this.complete( this.value );
            }
        }

        public void setValue( T value )
        {
        }

        public int requestFusion( int requestedMode )
        {
            return 0;
        }
    }
}
