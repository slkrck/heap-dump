package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import java.util.stream.Stream;

final class BoundedElasticScheduler implements Scheduler, Supplier<ScheduledExecutorService>, Scannable
{
    static final AtomicLong COUNTER = new AtomicLong();
    static final ThreadFactory EVICTOR_FACTORY = ( r ) -> {
        Thread t = new Thread( r, "elasticBounded-evictor-" + COUNTER.incrementAndGet() );
        t.setDaemon( true );
        return t;
    };
    static final BoundedElasticScheduler.CachedService SHUTDOWN = new BoundedElasticScheduler.CachedService( (BoundedElasticScheduler) null );
    static final int DEFAULT_TTL_SECONDS = 60;
    static final AtomicIntegerFieldUpdater<BoundedElasticScheduler> REMAINING_THREADS =
            AtomicIntegerFieldUpdater.newUpdater( BoundedElasticScheduler.class, "remainingThreads" );
    static final AtomicIntegerFieldUpdater<BoundedElasticScheduler> REMAINING_DEFERRED_TASKS =
            AtomicIntegerFieldUpdater.newUpdater( BoundedElasticScheduler.class, "remainingDeferredTasks" );
    final ThreadFactory factory;
    final int ttlSeconds;
    final int threadCap;
    final int deferredTaskCap;
    final Deque<BoundedElasticScheduler.CachedServiceExpiry> idleServicesWithExpiry;
    final Queue<BoundedElasticScheduler.DeferredFacade> deferredFacades;
    final Queue<BoundedElasticScheduler.CachedService> allServices;
    final ScheduledExecutorService evictor;
    volatile boolean shutdown;
    volatile int remainingThreads;
    volatile int remainingDeferredTasks;

    BoundedElasticScheduler( int threadCap, int deferredTaskCap, ThreadFactory factory, int ttlSeconds )
    {
        if ( ttlSeconds < 0 )
        {
            throw new IllegalArgumentException( "ttlSeconds must be positive, was: " + ttlSeconds );
        }
        else
        {
            this.ttlSeconds = ttlSeconds;
            if ( threadCap < 1 )
            {
                throw new IllegalArgumentException( "threadCap must be strictly positive, was: " + threadCap );
            }
            else if ( deferredTaskCap < 1 )
            {
                throw new IllegalArgumentException( "deferredTaskCap must be strictly positive, was: " + deferredTaskCap );
            }
            else
            {
                this.threadCap = threadCap;
                this.remainingThreads = threadCap;
                this.deferredTaskCap = deferredTaskCap;
                this.remainingDeferredTasks = deferredTaskCap;
                this.factory = factory;
                this.idleServicesWithExpiry = new ConcurrentLinkedDeque();
                this.deferredFacades = new ConcurrentLinkedQueue();
                this.allServices = new ConcurrentLinkedQueue();
                this.evictor = Executors.newScheduledThreadPool( 1, EVICTOR_FACTORY );
                this.evictor.scheduleAtFixedRate( () -> {
                    this.eviction( System::currentTimeMillis );
                }, (long) ttlSeconds, (long) ttlSeconds, TimeUnit.SECONDS );
            }
        }
    }

    public ScheduledExecutorService get()
    {
        ScheduledThreadPoolExecutor poolExecutor = new ScheduledThreadPoolExecutor( 1, this.factory );
        poolExecutor.setMaximumPoolSize( 1 );
        poolExecutor.setRemoveOnCancelPolicy( true );
        return poolExecutor;
    }

    public void start()
    {
        throw new UnsupportedOperationException( "Restarting not supported yet" );
    }

    public boolean isDisposed()
    {
        return this.shutdown;
    }

    public void dispose()
    {
        if ( !this.shutdown )
        {
            this.shutdown = true;
            this.evictor.shutdownNow();
            this.idleServicesWithExpiry.clear();

            BoundedElasticScheduler.CachedService cached;
            while ( (cached = (BoundedElasticScheduler.CachedService) this.allServices.poll()) != null )
            {
                cached.exec.shutdownNow();
            }
        }
    }

    @Nullable
    BoundedElasticScheduler.CachedService tryPick()
    {
        if ( this.shutdown )
        {
            return SHUTDOWN;
        }
        else
        {
            BoundedElasticScheduler.CachedServiceExpiry e = (BoundedElasticScheduler.CachedServiceExpiry) this.idleServicesWithExpiry.pollLast();
            if ( e != null )
            {
                return e.cached;
            }
            else if ( REMAINING_THREADS.decrementAndGet( this ) < 0 )
            {
                REMAINING_THREADS.incrementAndGet( this );
                return this.shutdown ? SHUTDOWN : null;
            }
            else
            {
                BoundedElasticScheduler.CachedService result = new BoundedElasticScheduler.CachedService( this );
                this.allServices.offer( result );
                if ( this.shutdown )
                {
                    this.allServices.remove( result );
                    return SHUTDOWN;
                }
                else
                {
                    return result;
                }
            }
        }
    }

    public Scheduler.Worker createWorker()
    {
        if ( this.shutdown )
        {
            return new BoundedElasticScheduler.ActiveWorker( SHUTDOWN );
        }
        else
        {
            BoundedElasticScheduler.CachedServiceExpiry e = (BoundedElasticScheduler.CachedServiceExpiry) this.idleServicesWithExpiry.pollLast();
            if ( e != null )
            {
                return new BoundedElasticScheduler.ActiveWorker( e.cached );
            }
            else if ( REMAINING_THREADS.decrementAndGet( this ) < 0 )
            {
                REMAINING_THREADS.incrementAndGet( this );
                if ( this.shutdown )
                {
                    return new BoundedElasticScheduler.ActiveWorker( SHUTDOWN );
                }
                else
                {
                    BoundedElasticScheduler.DeferredWorker deferredWorker = new BoundedElasticScheduler.DeferredWorker( this );
                    this.deferredFacades.offer( deferredWorker );
                    return deferredWorker;
                }
            }
            else
            {
                BoundedElasticScheduler.CachedService availableService = new BoundedElasticScheduler.CachedService( this );
                this.allServices.offer( availableService );
                if ( this.shutdown )
                {
                    this.allServices.remove( availableService );
                    return new BoundedElasticScheduler.ActiveWorker( SHUTDOWN );
                }
                else
                {
                    return new BoundedElasticScheduler.ActiveWorker( availableService );
                }
            }
        }
    }

    public Disposable schedule( Runnable task )
    {
        BoundedElasticScheduler.CachedService cached = this.tryPick();
        if ( cached != null )
        {
            return Schedulers.directSchedule( cached.exec, task, cached, 0L, TimeUnit.MILLISECONDS );
        }
        else if ( this.deferredTaskCap == Integer.MAX_VALUE )
        {
            BoundedElasticScheduler.DeferredDirect deferredDirect = new BoundedElasticScheduler.DeferredDirect( task, 0L, 0L, TimeUnit.MILLISECONDS, this );
            this.deferredFacades.offer( deferredDirect );
            return deferredDirect;
        }
        else
        {
            int remTasks;
            do
            {
                remTasks = REMAINING_DEFERRED_TASKS.get( this );
                if ( remTasks <= 0 )
                {
                    throw Exceptions.failWithRejected( "hard cap on deferred tasks reached for " + this.toString() );
                }
            }
            while ( !REMAINING_DEFERRED_TASKS.compareAndSet( this, remTasks, remTasks - 1 ) );

            BoundedElasticScheduler.DeferredDirect deferredDirect = new BoundedElasticScheduler.DeferredDirect( task, 0L, 0L, TimeUnit.MILLISECONDS, this );
            this.deferredFacades.offer( deferredDirect );
            return deferredDirect;
        }
    }

    public Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        BoundedElasticScheduler.CachedService cached = this.tryPick();
        if ( cached != null )
        {
            return Schedulers.directSchedule( cached.exec, task, cached, delay, unit );
        }
        else if ( this.deferredTaskCap == Integer.MAX_VALUE )
        {
            BoundedElasticScheduler.DeferredDirect deferredDirect = new BoundedElasticScheduler.DeferredDirect( task, delay, 0L, TimeUnit.MILLISECONDS, this );
            this.deferredFacades.offer( deferredDirect );
            return deferredDirect;
        }
        else
        {
            int remTasks;
            do
            {
                remTasks = REMAINING_DEFERRED_TASKS.get( this );
                if ( remTasks <= 0 )
                {
                    throw Exceptions.failWithRejected( "hard cap on deferred tasks reached for " + this.toString() );
                }
            }
            while ( !REMAINING_DEFERRED_TASKS.compareAndSet( this, remTasks, remTasks - 1 ) );

            BoundedElasticScheduler.DeferredDirect deferredDirect = new BoundedElasticScheduler.DeferredDirect( task, delay, 0L, TimeUnit.MILLISECONDS, this );
            this.deferredFacades.offer( deferredDirect );
            return deferredDirect;
        }
    }

    public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        BoundedElasticScheduler.CachedService cached = this.tryPick();
        if ( cached != null )
        {
            return Disposables.composite( Schedulers.directSchedulePeriodically( cached.exec, task, initialDelay, period, unit ), cached );
        }
        else if ( this.deferredTaskCap == Integer.MAX_VALUE )
        {
            BoundedElasticScheduler.DeferredDirect deferredDirect =
                    new BoundedElasticScheduler.DeferredDirect( task, initialDelay, period, TimeUnit.MILLISECONDS, this );
            this.deferredFacades.offer( deferredDirect );
            return deferredDirect;
        }
        else
        {
            int remTasks;
            do
            {
                remTasks = REMAINING_DEFERRED_TASKS.get( this );
                if ( remTasks <= 0 )
                {
                    throw Exceptions.failWithRejected( "hard cap on deferred tasks reached for " + this.toString() );
                }
            }
            while ( !REMAINING_DEFERRED_TASKS.compareAndSet( this, remTasks, remTasks - 1 ) );

            BoundedElasticScheduler.DeferredDirect deferredDirect =
                    new BoundedElasticScheduler.DeferredDirect( task, initialDelay, period, TimeUnit.MILLISECONDS, this );
            this.deferredFacades.offer( deferredDirect );
            return deferredDirect;
        }
    }

    public String toString()
    {
        StringBuilder ts = (new StringBuilder( "boundedElastic" )).append( '(' );
        if ( this.factory instanceof ReactorThreadFactory )
        {
            ts.append( '"' ).append( ((ReactorThreadFactory) this.factory).get() ).append( "\"," );
        }

        ts.append( "maxThreads=" ).append( this.threadCap ).append( ",maxTaskQueued=" ).append(
                this.deferredTaskCap == Integer.MAX_VALUE ? "unbounded" : this.deferredTaskCap ).append( ",ttl=" ).append( this.ttlSeconds ).append( "s)" );
        return ts.toString();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            if ( key == Scannable.Attr.CAPACITY )
            {
                return this.threadCap;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.idleServicesWithExpiry.size();
            }
            else
            {
                return key == Scannable.Attr.NAME ? this.toString() : null;
            }
        }
        else
        {
            return this.isDisposed();
        }
    }

    public Stream<? extends Scannable> inners()
    {
        return this.idleServicesWithExpiry.stream().map( ( cached ) -> {
            return cached.cached;
        } );
    }

    void eviction( LongSupplier nowSupplier )
    {
        long now = nowSupplier.getAsLong();
        List<BoundedElasticScheduler.CachedServiceExpiry> list = new ArrayList( this.idleServicesWithExpiry );
        Iterator var5 = list.iterator();

        while ( var5.hasNext() )
        {
            BoundedElasticScheduler.CachedServiceExpiry e = (BoundedElasticScheduler.CachedServiceExpiry) var5.next();
            if ( e.expireMillis < now && this.idleServicesWithExpiry.remove( e ) )
            {
                e.cached.exec.shutdownNow();
                this.allServices.remove( e.cached );
                REMAINING_THREADS.incrementAndGet( this );
            }
        }
    }

    @FunctionalInterface
    interface DeferredFacade
    {
        void setService( BoundedElasticScheduler.CachedService var1 );
    }

    static final class DeferredDirect extends AtomicReference<BoundedElasticScheduler.CachedService>
            implements Scannable, Disposable, BoundedElasticScheduler.DeferredFacade
    {
        static final AtomicReferenceFieldUpdater<BoundedElasticScheduler.DeferredDirect,Disposable> ACTIVE_TASK =
                AtomicReferenceFieldUpdater.newUpdater( BoundedElasticScheduler.DeferredDirect.class, Disposable.class, "activeTask" );
        static final AtomicIntegerFieldUpdater<BoundedElasticScheduler.DeferredDirect> DISPOSED =
                AtomicIntegerFieldUpdater.newUpdater( BoundedElasticScheduler.DeferredDirect.class, "disposed" );
        final Runnable task;
        final long delay;
        final long period;
        final TimeUnit timeUnit;
        final BoundedElasticScheduler parent;
        volatile Disposable activeTask;
        volatile int disposed;

        DeferredDirect( Runnable task, long delay, long period, TimeUnit unit, BoundedElasticScheduler parent )
        {
            this.task = task;
            this.delay = delay;
            this.period = period;
            this.timeUnit = unit;
            this.parent = parent;
        }

        public void setService( BoundedElasticScheduler.CachedService service )
        {
            if ( DISPOSED.get( this ) == 1 )
            {
                service.dispose();
            }
            else
            {
                if ( this.compareAndSet( (Object) null, service ) )
                {
                    if ( this.parent.deferredTaskCap != Integer.MAX_VALUE )
                    {
                        BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.incrementAndGet( this.parent );
                    }

                    if ( this.period == 0L && this.delay == 0L )
                    {
                        ACTIVE_TASK.set( this, Schedulers.directSchedule( service.exec, this.task, this, 0L, TimeUnit.SECONDS ) );
                    }
                    else if ( this.period != 0L )
                    {
                        ACTIVE_TASK.set( this, Schedulers.directSchedulePeriodically( service.exec, this.task, this.delay, this.period, this.timeUnit ) );
                    }
                    else
                    {
                        ACTIVE_TASK.set( this, Schedulers.directSchedule( service.exec, this.task, this, this.delay, this.timeUnit ) );
                    }
                }
                else
                {
                    service.dispose();
                }
            }
        }

        public void dispose()
        {
            if ( DISPOSED.compareAndSet( this, 0, 1 ) )
            {
                if ( this.parent.deferredFacades.remove( this ) && this.parent.deferredTaskCap != Integer.MAX_VALUE )
                {
                    BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.incrementAndGet( this.parent );
                }

                Disposable at = (Disposable) ACTIVE_TASK.getAndSet( this, (Object) null );
                if ( at != null )
                {
                    at.dispose();
                }

                BoundedElasticScheduler.CachedService c = (BoundedElasticScheduler.CachedService) this.getAndSet( (Object) null );
                if ( c != null )
                {
                    c.dispose();
                }
            }
        }

        public boolean isDisposed()
        {
            return DISPOSED.get( this ) == 1;
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                if ( key == Scannable.Attr.NAME )
                {
                    return this.parent.toString() + ".deferredDirect";
                }
                else if ( key == Scannable.Attr.CAPACITY )
                {
                    return 1;
                }
                else if ( key == Scannable.Attr.PARENT )
                {
                    return this.parent;
                }
                else
                {
                    BoundedElasticScheduler.CachedService d = (BoundedElasticScheduler.CachedService) this.get();
                    return key == Scannable.Attr.BUFFERED ? d == null ? 1 : 0 : null;
                }
            }
            else
            {
                return this.isDisposed();
            }
        }
    }

    static final class DeferredWorker extends ConcurrentLinkedQueue<BoundedElasticScheduler.DeferredWorkerTask>
            implements Scheduler.Worker, Scannable, BoundedElasticScheduler.DeferredFacade
    {
        static final AtomicReferenceFieldUpdater<BoundedElasticScheduler.DeferredWorker,BoundedElasticScheduler.ActiveWorker> DELEGATE =
                AtomicReferenceFieldUpdater.newUpdater( BoundedElasticScheduler.DeferredWorker.class, BoundedElasticScheduler.ActiveWorker.class, "delegate" );
        static final AtomicIntegerFieldUpdater<BoundedElasticScheduler.DeferredWorker> DISPOSED =
                AtomicIntegerFieldUpdater.newUpdater( BoundedElasticScheduler.DeferredWorker.class, "disposed" );
        final BoundedElasticScheduler parent;
        final String workerName;
        volatile BoundedElasticScheduler.ActiveWorker delegate;
        volatile int disposed;

        DeferredWorker( BoundedElasticScheduler parent )
        {
            this.parent = parent;
            this.workerName = parent.toString() + ".deferredWorker";
        }

        public void setService( BoundedElasticScheduler.CachedService service )
        {
            if ( DISPOSED.get( this ) == 1 )
            {
                service.dispose();
            }
            else
            {
                BoundedElasticScheduler.ActiveWorker delegate = new BoundedElasticScheduler.ActiveWorker( service );
                BoundedElasticScheduler.DeferredWorkerTask pendingTask;
                if ( DELEGATE.compareAndSet( this, (Object) null, delegate ) )
                {
                    while ( (pendingTask = (BoundedElasticScheduler.DeferredWorkerTask) this.poll()) != null )
                    {
                        pendingTask.activate( delegate );
                    }
                }
                else
                {
                    service.dispose();
                }
            }
        }

        public Disposable schedule( Runnable task )
        {
            if ( DISPOSED.get( this ) == 1 )
            {
                throw Exceptions.failWithRejected( "Worker has been disposed" );
            }
            else
            {
                BoundedElasticScheduler.ActiveWorker aw = (BoundedElasticScheduler.ActiveWorker) DELEGATE.get( this );
                if ( aw != null )
                {
                    return aw.schedule( task );
                }
                else if ( this.parent.deferredTaskCap == Integer.MAX_VALUE )
                {
                    BoundedElasticScheduler.DeferredWorkerTask pendingTask =
                            new BoundedElasticScheduler.DeferredWorkerTask( this, task, 0L, 0L, TimeUnit.MILLISECONDS );
                    this.offer( pendingTask );
                    return pendingTask;
                }
                else
                {
                    int remTasks;
                    do
                    {
                        remTasks = BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.get( this.parent );
                        if ( remTasks <= 0 )
                        {
                            throw Exceptions.failWithRejected( "hard cap on deferred tasks reached for " + this.toString() );
                        }
                    }
                    while ( !BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.compareAndSet( this.parent, remTasks, remTasks - 1 ) );

                    BoundedElasticScheduler.DeferredWorkerTask pendingTask =
                            new BoundedElasticScheduler.DeferredWorkerTask( this, task, 0L, 0L, TimeUnit.MILLISECONDS );
                    this.offer( pendingTask );
                    return pendingTask;
                }
            }
        }

        public Disposable schedule( Runnable task, long delay, TimeUnit unit )
        {
            if ( DISPOSED.get( this ) == 1 )
            {
                throw Exceptions.failWithRejected( "Worker has been disposed" );
            }
            else
            {
                BoundedElasticScheduler.ActiveWorker aw = (BoundedElasticScheduler.ActiveWorker) DELEGATE.get( this );
                if ( aw != null )
                {
                    return aw.schedule( task, delay, unit );
                }
                else if ( this.parent.deferredTaskCap == Integer.MAX_VALUE )
                {
                    BoundedElasticScheduler.DeferredWorkerTask pendingTask = new BoundedElasticScheduler.DeferredWorkerTask( this, task, delay, 0L, unit );
                    this.offer( pendingTask );
                    return pendingTask;
                }
                else
                {
                    int remTasks;
                    do
                    {
                        remTasks = BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.get( this.parent );
                        if ( remTasks <= 0 )
                        {
                            throw Exceptions.failWithRejected( "hard cap on deferred tasks reached for " + this.toString() );
                        }
                    }
                    while ( !BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.compareAndSet( this.parent, remTasks, remTasks - 1 ) );

                    BoundedElasticScheduler.DeferredWorkerTask pendingTask = new BoundedElasticScheduler.DeferredWorkerTask( this, task, delay, 0L, unit );
                    this.offer( pendingTask );
                    return pendingTask;
                }
            }
        }

        public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
        {
            if ( DISPOSED.get( this ) == 1 )
            {
                throw Exceptions.failWithRejected( "Worker has been disposed" );
            }
            else
            {
                BoundedElasticScheduler.ActiveWorker aw = (BoundedElasticScheduler.ActiveWorker) DELEGATE.get( this );
                if ( aw != null )
                {
                    return aw.schedulePeriodically( task, initialDelay, period, unit );
                }
                else if ( this.parent.deferredTaskCap == Integer.MAX_VALUE )
                {
                    BoundedElasticScheduler.DeferredWorkerTask pendingTask =
                            new BoundedElasticScheduler.DeferredWorkerTask( this, task, initialDelay, period, unit );
                    this.offer( pendingTask );
                    return pendingTask;
                }
                else
                {
                    int remTasks;
                    do
                    {
                        remTasks = BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.get( this.parent );
                        if ( remTasks <= 0 )
                        {
                            throw Exceptions.failWithRejected( "hard cap on deferred tasks reached for " + this.toString() );
                        }
                    }
                    while ( !BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.compareAndSet( this.parent, remTasks, remTasks - 1 ) );

                    BoundedElasticScheduler.DeferredWorkerTask pendingTask =
                            new BoundedElasticScheduler.DeferredWorkerTask( this, task, initialDelay, period, unit );
                    this.offer( pendingTask );
                    return pendingTask;
                }
            }
        }

        public void dispose()
        {
            if ( DISPOSED.compareAndSet( this, 0, 1 ) )
            {
                this.parent.deferredFacades.remove( this );

                BoundedElasticScheduler.DeferredWorkerTask pendingTask;
                while ( (pendingTask = (BoundedElasticScheduler.DeferredWorkerTask) this.poll()) != null )
                {
                    pendingTask.disposeInner();
                }

                BoundedElasticScheduler.ActiveWorker aw = (BoundedElasticScheduler.ActiveWorker) DELEGATE.getAndSet( this, (Object) null );
                if ( aw != null )
                {
                    aw.dispose();
                }
            }
        }

        public boolean isDisposed()
        {
            return DISPOSED.get( this ) == 1;
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                if ( key == Scannable.Attr.NAME )
                {
                    return this.workerName;
                }
                else if ( key == Scannable.Attr.CAPACITY )
                {
                    return BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.get( this.parent );
                }
                else if ( key == Scannable.Attr.BUFFERED )
                {
                    return this.size();
                }
                else
                {
                    return key == Scannable.Attr.PARENT ? this.parent : null;
                }
            }
            else
            {
                return this.isDisposed();
            }
        }
    }

    static final class DeferredWorkerTask implements Disposable
    {
        static final AtomicReferenceFieldUpdater<BoundedElasticScheduler.DeferredWorkerTask,Disposable> ACTIVATED =
                AtomicReferenceFieldUpdater.newUpdater( BoundedElasticScheduler.DeferredWorkerTask.class, Disposable.class, "activated" );
        final BoundedElasticScheduler.DeferredWorker parent;
        final Runnable task;
        final long delay;
        final long period;
        final TimeUnit timeUnit;
        volatile Disposable activated;

        DeferredWorkerTask( BoundedElasticScheduler.DeferredWorker parent, Runnable task, long delay, long period, TimeUnit unit )
        {
            this.parent = parent;
            this.task = task;
            this.delay = delay;
            this.period = period;
            this.timeUnit = unit;
        }

        void activate( BoundedElasticScheduler.ActiveWorker delegate )
        {
            if ( this.parent.parent.deferredTaskCap != Integer.MAX_VALUE )
            {
                BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.incrementAndGet( this.parent.parent );
            }

            if ( this.period == 0L && this.delay == 0L )
            {
                this.activated = delegate.schedule( this.task );
            }
            else if ( this.period != 0L )
            {
                this.activated = delegate.schedulePeriodically( this.task, this.delay, this.period, this.timeUnit );
            }
            else
            {
                this.activated = delegate.schedule( this.task, this.delay, this.timeUnit );
            }
        }

        public void dispose()
        {
            this.parent.remove( this );
            this.disposeInner();
        }

        void disposeInner()
        {
            if ( this.parent.parent.deferredTaskCap != Integer.MAX_VALUE )
            {
                BoundedElasticScheduler.REMAINING_DEFERRED_TASKS.incrementAndGet( this.parent.parent );
            }

            if ( this.activated != null )
            {
                this.activated.dispose();
            }
        }
    }

    static final class ActiveWorker extends AtomicBoolean implements Scheduler.Worker, Scannable
    {
        final BoundedElasticScheduler.CachedService cached;
        final Disposable.Composite tasks;

        ActiveWorker( BoundedElasticScheduler.CachedService cached )
        {
            this.cached = cached;
            this.tasks = Disposables.composite();
        }

        public Disposable schedule( Runnable task )
        {
            return Schedulers.workerSchedule( this.cached.exec, this.tasks, task, 0L, TimeUnit.MILLISECONDS );
        }

        public Disposable schedule( Runnable task, long delay, TimeUnit unit )
        {
            return Schedulers.workerSchedule( this.cached.exec, this.tasks, task, delay, unit );
        }

        public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
        {
            return Schedulers.workerSchedulePeriodically( this.cached.exec, this.tasks, task, initialDelay, period, unit );
        }

        public void dispose()
        {
            if ( this.compareAndSet( false, true ) )
            {
                this.tasks.dispose();
                this.cached.dispose();
            }
        }

        public boolean isDisposed()
        {
            return this.tasks.isDisposed();
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                if ( key == Scannable.Attr.NAME )
                {
                    return this.cached.scanUnsafe( key ) + ".worker";
                }
                else
                {
                    return key == Scannable.Attr.PARENT ? this.cached.parent : this.cached.scanUnsafe( key );
                }
            }
            else
            {
                return this.isDisposed();
            }
        }
    }

    static final class CachedServiceExpiry
    {
        final BoundedElasticScheduler.CachedService cached;
        final long expireMillis;

        CachedServiceExpiry( BoundedElasticScheduler.CachedService cached, long expireMillis )
        {
            this.cached = cached;
            this.expireMillis = expireMillis;
        }
    }

    static final class CachedService implements Disposable, Scannable
    {
        final BoundedElasticScheduler parent;
        final ScheduledExecutorService exec;

        CachedService( @Nullable BoundedElasticScheduler parent )
        {
            this.parent = parent;
            if ( parent != null )
            {
                this.exec = Schedulers.decorateExecutorService( parent, parent.get() );
            }
            else
            {
                this.exec = Executors.newSingleThreadScheduledExecutor();
                this.exec.shutdownNow();
            }
        }

        public void dispose()
        {
            if ( this.exec != null && this != BoundedElasticScheduler.SHUTDOWN && !this.parent.shutdown )
            {
                BoundedElasticScheduler.DeferredFacade deferredFacade = (BoundedElasticScheduler.DeferredFacade) this.parent.deferredFacades.poll();
                if ( deferredFacade != null )
                {
                    deferredFacade.setService( this );
                }
                else
                {
                    BoundedElasticScheduler.CachedServiceExpiry e =
                            new BoundedElasticScheduler.CachedServiceExpiry( this, System.currentTimeMillis() + (long) this.parent.ttlSeconds * 1000L );
                    this.parent.idleServicesWithExpiry.offerLast( e );
                    if ( this.parent.shutdown && this.parent.idleServicesWithExpiry.remove( e ) )
                    {
                        this.exec.shutdownNow();
                    }
                }
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.NAME )
            {
                return this.parent.scanUnsafe( key );
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                if ( key == Scannable.Attr.CAPACITY )
                {
                    Integer capacity = (Integer) Schedulers.scanExecutor( this.exec, key );
                    if ( capacity == null || capacity == -1 )
                    {
                        return 1;
                    }
                }

                return Schedulers.scanExecutor( this.exec, key );
            }
            else
            {
                return this.isDisposed();
            }
        }
    }
}
