package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.NoSuchElementException;
import java.util.Objects;

import org.reactivestreams.Subscription;

final class MonoTakeLastOne<T> extends MonoFromFluxOperator<T,T> implements Fuseable
{
    final T defaultValue;

    MonoTakeLastOne( Flux<? extends T> source )
    {
        super( source );
        this.defaultValue = null;
    }

    MonoTakeLastOne( Flux<? extends T> source, T defaultValue )
    {
        super( source );
        this.defaultValue = Objects.requireNonNull( defaultValue, "defaultValue" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoTakeLastOne.TakeLastOneSubscriber( actual, this.defaultValue, true );
    }

    static final class TakeLastOneSubscriber<T> extends Operators.MonoSubscriber<T,T>
    {
        final boolean mustEmit;
        final T defaultValue;
        Subscription s;

        TakeLastOneSubscriber( CoreSubscriber<? super T> actual, @Nullable T defaultValue, boolean mustEmit )
        {
            super( actual );
            this.defaultValue = defaultValue;
            this.mustEmit = mustEmit;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
        }

        public void onNext( T t )
        {
            T old = this.value;
            this.value = t;
            Operators.onDiscard( old, this.actual.currentContext() );
        }

        public void onComplete()
        {
            T v = this.value;
            if ( v == null )
            {
                if ( this.mustEmit )
                {
                    if ( this.defaultValue != null )
                    {
                        this.complete( this.defaultValue );
                    }
                    else
                    {
                        this.actual.onError( Operators.onOperatorError( new NoSuchElementException( "Flux#last() didn't observe any onNext signal" ),
                                this.actual.currentContext() ) );
                    }
                }
                else
                {
                    this.actual.onComplete();
                }
            }
            else
            {
                this.complete( v );
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }

        public void setValue( T value )
        {
        }
    }
}
