package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

final class FluxBuffer<T, C extends Collection<? super T>> extends InternalFluxOperator<T,C>
{
    final int size;
    final int skip;
    final Supplier<C> bufferSupplier;

    FluxBuffer( Flux<? extends T> source, int size, Supplier<C> bufferSupplier )
    {
        this( source, size, size, bufferSupplier );
    }

    FluxBuffer( Flux<? extends T> source, int size, int skip, Supplier<C> bufferSupplier )
    {
        super( source );
        if ( size <= 0 )
        {
            throw new IllegalArgumentException( "size > 0 required but it was " + size );
        }
        else if ( skip <= 0 )
        {
            throw new IllegalArgumentException( "skip > 0 required but it was " + size );
        }
        else
        {
            this.size = size;
            this.skip = skip;
            this.bufferSupplier = (Supplier) Objects.requireNonNull( bufferSupplier, "bufferSupplier" );
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super C> actual )
    {
        if ( this.size == this.skip )
        {
            return new FluxBuffer.BufferExactSubscriber( actual, this.size, this.bufferSupplier );
        }
        else
        {
            return (CoreSubscriber) (this.skip > this.size ? new FluxBuffer.BufferSkipSubscriber( actual, this.size, this.skip, this.bufferSupplier )
                                                           : new FluxBuffer.BufferOverlappingSubscriber( actual, this.size, this.skip, this.bufferSupplier ));
        }
    }

    static final class BufferOverlappingSubscriber<T, C extends Collection<? super T>> extends ArrayDeque<C> implements BooleanSupplier, InnerOperator<T,C>
    {
        static final AtomicIntegerFieldUpdater<FluxBuffer.BufferOverlappingSubscriber> ONCE =
                AtomicIntegerFieldUpdater.newUpdater( FluxBuffer.BufferOverlappingSubscriber.class, "once" );
        static final AtomicLongFieldUpdater<FluxBuffer.BufferOverlappingSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxBuffer.BufferOverlappingSubscriber.class, "requested" );
        final CoreSubscriber<? super C> actual;
        final Supplier<C> bufferSupplier;
        final int size;
        final int skip;
        Subscription s;
        boolean done;
        long index;
        volatile boolean cancelled;
        long produced;
        volatile int once;
        volatile long requested;

        BufferOverlappingSubscriber( CoreSubscriber<? super C> actual, int size, int skip, Supplier<C> bufferSupplier )
        {
            this.actual = actual;
            this.size = size;
            this.skip = skip;
            this.bufferSupplier = bufferSupplier;
        }

        public boolean getAsBoolean()
        {
            return this.cancelled;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( !DrainUtils.postCompleteRequest( n, this.actual, this, REQUESTED, this, this ) )
                {
                    long r;
                    if ( this.once == 0 && ONCE.compareAndSet( this, 0, 1 ) )
                    {
                        r = Operators.multiplyCap( (long) this.skip, n - 1L );
                        long r = Operators.addCap( (long) this.size, r );
                        this.s.request( r );
                    }
                    else
                    {
                        r = Operators.multiplyCap( (long) this.skip, n );
                        this.s.request( r );
                    }
                }
            }
        }

        public void cancel()
        {
            this.cancelled = true;
            this.s.cancel();
            this.clear();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                long i = this.index;
                Collection b;
                if ( i % (long) this.skip == 0L )
                {
                    try
                    {
                        b = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null buffer" );
                    }
                    catch ( Throwable var7 )
                    {
                        Context ctx = this.actual.currentContext();
                        this.onError( Operators.onOperatorError( this.s, var7, t, ctx ) );
                        Operators.onDiscard( t, ctx );
                        return;
                    }

                    this.offer( b );
                }

                b = (Collection) this.peek();
                if ( b != null && b.size() + 1 == this.size )
                {
                    this.poll();
                    b.add( t );
                    this.actual.onNext( b );
                    ++this.produced;
                }

                Iterator var5 = this.iterator();

                while ( var5.hasNext() )
                {
                    C b0 = (Collection) var5.next();
                    b0.add( t );
                }

                this.index = i + 1L;
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.clear();
                this.actual.onError( t );
            }
        }

        public void clear()
        {
            Context ctx = this.actual.currentContext();
            Iterator var2 = this.iterator();

            while ( var2.hasNext() )
            {
                C b = (Collection) var2.next();
                Operators.onDiscardMultiple( b, ctx );
            }

            super.clear();
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                long p = this.produced;
                if ( p != 0L )
                {
                    Operators.produced( REQUESTED, this, p );
                }

                DrainUtils.postComplete( this.actual, this, REQUESTED, this, this );
            }
        }

        public CoreSubscriber<? super C> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.size() * this.size;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.stream().mapToInt( Collection::size ).sum();
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.requested : InnerOperator.super.scanUnsafe( key );
            }
        }
    }

    static final class BufferSkipSubscriber<T, C extends Collection<? super T>> implements InnerOperator<T,C>
    {
        static final AtomicIntegerFieldUpdater<FluxBuffer.BufferSkipSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxBuffer.BufferSkipSubscriber.class, "wip" );
        final CoreSubscriber<? super C> actual;
        final Context ctx;
        final Supplier<C> bufferSupplier;
        final int size;
        final int skip;
        C buffer;
        Subscription s;
        boolean done;
        long index;
        volatile int wip;

        BufferSkipSubscriber( CoreSubscriber<? super C> actual, int size, int skip, Supplier<C> bufferSupplier )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.size = size;
            this.skip = skip;
            this.bufferSupplier = bufferSupplier;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( this.wip == 0 && WIP.compareAndSet( this, 0, 1 ) )
                {
                    long u = Operators.multiplyCap( n, (long) this.size );
                    long v = Operators.multiplyCap( (long) (this.skip - this.size), n - 1L );
                    this.s.request( Operators.addCap( u, v ) );
                }
                else
                {
                    this.s.request( Operators.multiplyCap( (long) this.skip, n ) );
                }
            }
        }

        public void cancel()
        {
            this.s.cancel();
            Operators.onDiscardMultiple( this.buffer, this.ctx );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
            }
            else
            {
                C b = this.buffer;
                long i = this.index;
                if ( i % (long) this.skip == 0L )
                {
                    try
                    {
                        b = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null buffer" );
                    }
                    catch ( Throwable var6 )
                    {
                        this.onError( Operators.onOperatorError( this.s, var6, t, this.ctx ) );
                        Operators.onDiscard( t, this.ctx );
                        return;
                    }

                    this.buffer = b;
                }

                if ( b != null )
                {
                    b.add( t );
                    if ( b.size() == this.size )
                    {
                        this.buffer = null;
                        this.actual.onNext( b );
                    }
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                }

                this.index = i + 1L;
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                C b = this.buffer;
                this.buffer = null;
                this.actual.onError( t );
                Operators.onDiscardMultiple( b, this.ctx );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                C b = this.buffer;
                this.buffer = null;
                if ( b != null )
                {
                    this.actual.onNext( b );
                }

                this.actual.onComplete();
            }
        }

        public CoreSubscriber<? super C> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.size;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                C b = this.buffer;
                return b != null ? b.size() : 0;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? this.size : InnerOperator.super.scanUnsafe( key );
            }
        }
    }

    static final class BufferExactSubscriber<T, C extends Collection<? super T>> implements InnerOperator<T,C>
    {
        final CoreSubscriber<? super C> actual;
        final Supplier<C> bufferSupplier;
        final int size;
        C buffer;
        Subscription s;
        boolean done;

        BufferExactSubscriber( CoreSubscriber<? super C> actual, int size, Supplier<C> bufferSupplier )
        {
            this.actual = actual;
            this.size = size;
            this.bufferSupplier = bufferSupplier;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                this.s.request( Operators.multiplyCap( n, (long) this.size ) );
            }
        }

        public void cancel()
        {
            this.s.cancel();
            Operators.onDiscardMultiple( this.buffer, this.actual.currentContext() );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                C b = this.buffer;
                if ( b == null )
                {
                    try
                    {
                        b = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null buffer" );
                    }
                    catch ( Throwable var5 )
                    {
                        Context ctx = this.actual.currentContext();
                        this.onError( Operators.onOperatorError( this.s, var5, t, ctx ) );
                        Operators.onDiscard( t, ctx );
                        return;
                    }

                    this.buffer = b;
                }

                b.add( t );
                if ( b.size() == this.size )
                {
                    this.buffer = null;
                    this.actual.onNext( b );
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
                Operators.onDiscardMultiple( this.buffer, this.actual.currentContext() );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                C b = this.buffer;
                if ( b != null && !b.isEmpty() )
                {
                    this.actual.onNext( b );
                }

                this.actual.onComplete();
            }
        }

        public CoreSubscriber<? super C> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                C b = this.buffer;
                return b != null ? b.size() : 0;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.size;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? this.size : InnerOperator.super.scanUnsafe( key );
            }
        }
    }
}
