package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxFlattenIterable<T, R> extends InternalFluxOperator<T,R> implements Fuseable
{
    final Function<? super T,? extends Iterable<? extends R>> mapper;
    final int prefetch;
    final Supplier<Queue<T>> queueSupplier;

    FluxFlattenIterable( Flux<? extends T> source, Function<? super T,? extends Iterable<? extends R>> mapper, int prefetch, Supplier<Queue<T>> queueSupplier )
    {
        super( source );
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.mapper = (Function) Objects.requireNonNull( mapper, "mapper" );
            this.prefetch = prefetch;
            this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        if ( this.source instanceof Callable )
        {
            Object v;
            try
            {
                v = ((Callable) this.source).call();
            }
            catch ( Throwable var7 )
            {
                Operators.error( actual, Operators.onOperatorError( var7, actual.currentContext() ) );
                return null;
            }

            if ( v == null )
            {
                Operators.complete( actual );
                return null;
            }
            else
            {
                Iterator it;
                try
                {
                    Iterable<? extends R> iter = (Iterable) this.mapper.apply( v );
                    it = iter.iterator();
                }
                catch ( Throwable var6 )
                {
                    Context ctx = actual.currentContext();
                    Operators.error( actual, Operators.onOperatorError( var6, ctx ) );
                    Operators.onDiscard( v, ctx );
                    return null;
                }

                FluxIterable.subscribe( actual, it );
                return null;
            }
        }
        else
        {
            return new FluxFlattenIterable.FlattenIterableSubscriber( actual, this.mapper, this.prefetch, this.queueSupplier );
        }
    }

    static final class FlattenIterableSubscriber<T, R> implements InnerOperator<T,R>, Fuseable.QueueSubscription<R>
    {
        static final AtomicIntegerFieldUpdater<FluxFlattenIterable.FlattenIterableSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxFlattenIterable.FlattenIterableSubscriber.class, "wip" );
        static final AtomicLongFieldUpdater<FluxFlattenIterable.FlattenIterableSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxFlattenIterable.FlattenIterableSubscriber.class, "requested" );
        static final AtomicReferenceFieldUpdater<FluxFlattenIterable.FlattenIterableSubscriber,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( FluxFlattenIterable.FlattenIterableSubscriber.class, Throwable.class, "error" );
        final CoreSubscriber<? super R> actual;
        final Function<? super T,? extends Iterable<? extends R>> mapper;
        final int prefetch;
        final int limit;
        final Supplier<Queue<T>> queueSupplier;
        volatile int wip;
        volatile long requested;
        Subscription s;
        Queue<T> queue;
        volatile boolean done;
        volatile boolean cancelled;
        volatile Throwable error;
        Iterator<? extends R> current;
        int consumed;
        int fusionMode;

        FlattenIterableSubscriber( CoreSubscriber<? super R> actual, Function<? super T,? extends Iterable<? extends R>> mapper, int prefetch,
                Supplier<Queue<T>> queueSupplier )
        {
            this.actual = actual;
            this.mapper = mapper;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
            this.limit = Operators.unboundedOrLimit( prefetch );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.queue != null ? this.queue.size() : 0 : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    Fuseable.QueueSubscription<T> qs = (Fuseable.QueueSubscription) s;
                    int m = qs.requestFusion( 3 );
                    if ( m == 1 )
                    {
                        this.fusionMode = m;
                        this.queue = qs;
                        this.done = true;
                        this.actual.onSubscribe( this );
                        return;
                    }

                    if ( m == 2 )
                    {
                        this.fusionMode = m;
                        this.queue = qs;
                        this.actual.onSubscribe( this );
                        s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
                        return;
                    }
                }

                this.queue = (Queue) this.queueSupplier.get();
                this.actual.onSubscribe( this );
                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        public void onNext( T t )
        {
            if ( this.fusionMode != 2 && !this.queue.offer( t ) )
            {
                Context ctx = this.actual.currentContext();
                this.onError(
                        Operators.onOperatorError( this.s, Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ),
                                ctx ) );
                Operators.onDiscard( t, ctx );
            }
            else
            {
                this.drain();
            }
        }

        public void onError( Throwable t )
        {
            if ( Exceptions.addThrowable( ERROR, this, t ) )
            {
                this.done = true;
                this.drain();
            }
            else
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
        }

        public void onComplete()
        {
            this.done = true;
            this.drain();
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.drain();
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                }
            }
        }

        void drainAsync()
        {
            Subscriber<? super R> a = this.actual;
            Queue<T> q = this.queue;
            int missed = 1;
            Iterator it = this.current;

            label143:
            while ( true )
            {
                boolean b;
                if ( it == null )
                {
                    if ( this.cancelled )
                    {
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        return;
                    }

                    Throwable ex = this.error;
                    if ( ex != null )
                    {
                        ex = Exceptions.terminate( ERROR, this );
                        this.current = null;
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        a.onError( ex );
                        return;
                    }

                    boolean d = this.done;

                    Object t;
                    try
                    {
                        t = q.poll();
                    }
                    catch ( Throwable var13 )
                    {
                        this.current = null;
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        a.onError( var13 );
                        return;
                    }

                    boolean empty = t == null;
                    if ( d && empty )
                    {
                        a.onComplete();
                        return;
                    }

                    if ( !empty )
                    {
                        try
                        {
                            Iterable<? extends R> iterable = (Iterable) this.mapper.apply( t );
                            it = iterable.iterator();
                            b = it.hasNext();
                        }
                        catch ( Throwable var14 )
                        {
                            it = null;
                            Context ctx = this.actual.currentContext();
                            this.onError( Operators.onOperatorError( this.s, var14, t, ctx ) );
                            Operators.onDiscard( t, ctx );
                            continue;
                        }

                        if ( !b )
                        {
                            it = null;
                            int c = this.consumed + 1;
                            if ( c == this.limit )
                            {
                                this.consumed = 0;
                                this.s.request( (long) c );
                            }
                            else
                            {
                                this.consumed = c;
                            }
                            continue;
                        }
                    }
                }

                if ( it != null )
                {
                    long r = this.requested;
                    long e = 0L;

                    while ( true )
                    {
                        boolean b;
                        Throwable ex;
                        if ( e != r )
                        {
                            if ( this.cancelled )
                            {
                                this.current = null;
                                Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                                return;
                            }

                            ex = this.error;
                            if ( ex != null )
                            {
                                ex = Exceptions.terminate( ERROR, this );
                                this.current = null;
                                Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                                a.onError( ex );
                                return;
                            }

                            Object v;
                            try
                            {
                                v = Objects.requireNonNull( it.next(), "iterator returned null" );
                            }
                            catch ( Throwable var16 )
                            {
                                this.onError( Operators.onOperatorError( this.s, var16, this.actual.currentContext() ) );
                                continue;
                            }

                            a.onNext( v );
                            if ( this.cancelled )
                            {
                                this.current = null;
                                Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                                return;
                            }

                            ++e;

                            try
                            {
                                b = it.hasNext();
                            }
                            catch ( Throwable var15 )
                            {
                                this.onError( Operators.onOperatorError( this.s, var15, this.actual.currentContext() ) );
                                continue;
                            }

                            if ( b )
                            {
                                continue;
                            }

                            int c = this.consumed + 1;
                            if ( c == this.limit )
                            {
                                this.consumed = 0;
                                this.s.request( (long) c );
                            }
                            else
                            {
                                this.consumed = c;
                            }

                            it = null;
                            this.current = null;
                        }

                        if ( e == r )
                        {
                            if ( this.cancelled )
                            {
                                this.current = null;
                                Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                                return;
                            }

                            ex = this.error;
                            if ( ex != null )
                            {
                                ex = Exceptions.terminate( ERROR, this );
                                this.current = null;
                                Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                                a.onError( ex );
                                return;
                            }

                            b = this.done;
                            b = q.isEmpty() && it == null;
                            if ( b && b )
                            {
                                this.current = null;
                                a.onComplete();
                                return;
                            }
                        }

                        if ( e != 0L && r != Long.MAX_VALUE )
                        {
                            REQUESTED.addAndGet( this, -e );
                        }

                        if ( it == null )
                        {
                            continue label143;
                        }
                        break;
                    }
                }

                this.current = it;
                missed = WIP.addAndGet( this, -missed );
                if ( missed == 0 )
                {
                    return;
                }
            }
        }

        void drainSync()
        {
            Subscriber<? super R> a = this.actual;
            int missed = 1;
            Iterator it = this.current;

            label120:
            while ( true )
            {
                boolean b;
                if ( it == null )
                {
                    if ( this.cancelled )
                    {
                        Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                        return;
                    }

                    boolean d = this.done;
                    Queue q = this.queue;

                    Object t;
                    try
                    {
                        t = q.poll();
                    }
                    catch ( Throwable var15 )
                    {
                        this.current = null;
                        Operators.onDiscardQueueWithClear( q, this.actual.currentContext(), (Function) null );
                        a.onError( var15 );
                        return;
                    }

                    boolean empty = t == null;
                    if ( d && empty )
                    {
                        a.onComplete();
                        return;
                    }

                    if ( !empty )
                    {
                        try
                        {
                            Iterable<? extends R> iterable = (Iterable) this.mapper.apply( t );
                            it = iterable.iterator();
                            b = it.hasNext();
                        }
                        catch ( Throwable var14 )
                        {
                            this.current = null;
                            Context ctx = this.actual.currentContext();
                            a.onError( Operators.onOperatorError( this.s, var14, t, ctx ) );
                            Operators.onDiscard( t, ctx );
                            return;
                        }

                        if ( !b )
                        {
                            it = null;
                            continue;
                        }
                    }
                }

                if ( it != null )
                {
                    long r = this.requested;
                    long e = 0L;

                    while ( true )
                    {
                        if ( e != r )
                        {
                            if ( this.cancelled )
                            {
                                this.current = null;
                                Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                                return;
                            }

                            Object v;
                            try
                            {
                                v = Objects.requireNonNull( it.next(), "iterator returned null" );
                            }
                            catch ( Throwable var12 )
                            {
                                this.current = null;
                                a.onError( Operators.onOperatorError( this.s, var12, this.actual.currentContext() ) );
                                return;
                            }

                            a.onNext( v );
                            if ( this.cancelled )
                            {
                                this.current = null;
                                Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                                return;
                            }

                            ++e;

                            try
                            {
                                b = it.hasNext();
                            }
                            catch ( Throwable var13 )
                            {
                                this.current = null;
                                a.onError( Operators.onOperatorError( this.s, var13, this.actual.currentContext() ) );
                                return;
                            }

                            if ( b )
                            {
                                continue;
                            }

                            it = null;
                            this.current = null;
                        }

                        if ( e == r )
                        {
                            if ( this.cancelled )
                            {
                                this.current = null;
                                Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
                                return;
                            }

                            boolean d = this.done;
                            b = this.queue.isEmpty() && it == null;
                            if ( d && b )
                            {
                                this.current = null;
                                a.onComplete();
                                return;
                            }
                        }

                        if ( e != 0L && r != Long.MAX_VALUE )
                        {
                            REQUESTED.addAndGet( this, -e );
                        }

                        if ( it == null )
                        {
                            continue label120;
                        }
                        break;
                    }
                }

                this.current = it;
                missed = WIP.addAndGet( this, -missed );
                if ( missed == 0 )
                {
                    return;
                }
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                if ( this.fusionMode == 1 )
                {
                    this.drainSync();
                }
                else
                {
                    this.drainAsync();
                }
            }
        }

        public void clear()
        {
            this.current = null;
            Operators.onDiscardQueueWithClear( this.queue, this.actual.currentContext(), (Function) null );
        }

        public boolean isEmpty()
        {
            Iterator<? extends R> it = this.current;
            if ( it != null )
            {
                return !it.hasNext();
            }
            else
            {
                return this.queue.isEmpty();
            }
        }

        @Nullable
        public R poll()
        {
            Iterator it = this.current;

            while ( true )
            {
                Object r;
                if ( it == null )
                {
                    r = this.queue.poll();
                    if ( r == null )
                    {
                        return null;
                    }

                    try
                    {
                        it = ((Iterable) this.mapper.apply( r )).iterator();
                    }
                    catch ( Throwable var4 )
                    {
                        Operators.onDiscard( r, this.actual.currentContext() );
                        throw var4;
                    }

                    if ( !it.hasNext() )
                    {
                        continue;
                    }

                    this.current = it;
                }
                else if ( !it.hasNext() )
                {
                    it = null;
                    continue;
                }

                r = Objects.requireNonNull( it.next(), "iterator returned null" );
                if ( !it.hasNext() )
                {
                    this.current = null;
                }

                return r;
            }
        }

        public int requestFusion( int requestedMode )
        {
            return (requestedMode & 1) != 0 && this.fusionMode == 1 ? 1 : 0;
        }

        public int size()
        {
            return this.queue.size();
        }
    }
}
