package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Logger;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Loggers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

/**
 * @deprecated
 */
@Deprecated
public final class WorkQueueProcessor<E> extends EventLoopProcessor<E>
{
    static final Supplier FACTORY = EventLoopProcessor.Slot::new;
    static final AtomicIntegerFieldUpdater<WorkQueueProcessor> REPLAYING = AtomicIntegerFieldUpdater.newUpdater( WorkQueueProcessor.class, "replaying" );
    static final Logger log = Loggers.getLogger( WorkQueueProcessor.class );
    final RingBuffer.Sequence workSequence = RingBuffer.newSequence( -1L );
    final Queue<Object> claimedDisposed = new ConcurrentLinkedQueue();
    final WaitStrategy writeWait;
    volatile int replaying;

    WorkQueueProcessor( @Nullable ThreadFactory threadFactory, @Nullable ExecutorService executor, ExecutorService requestTaskExecutor, int bufferSize,
            WaitStrategy waitStrategy, boolean share, boolean autoCancel )
    {
        super( bufferSize, threadFactory, executor, requestTaskExecutor, autoCancel, share, FACTORY, waitStrategy );
        this.writeWait = waitStrategy;
        this.ringBuffer.addGatingSequence( this.workSequence );
    }

    public static final <T> WorkQueueProcessor.Builder<T> builder()
    {
        return new WorkQueueProcessor.Builder();
    }

    public static <E> WorkQueueProcessor<E> create()
    {
        return builder().build();
    }

    public static <E> WorkQueueProcessor<E> create( String name, int bufferSize )
    {
        return builder().name( name ).bufferSize( bufferSize ).build();
    }

    public static <E> WorkQueueProcessor<E> share( String name, int bufferSize )
    {
        return builder().share( true ).name( name ).bufferSize( bufferSize ).build();
    }

    static int bestEffortMaxSubscribers( ExecutorService executor )
    {
        int maxSubscribers = Integer.MIN_VALUE;
        if ( executor instanceof ThreadPoolExecutor )
        {
            maxSubscribers = ((ThreadPoolExecutor) executor).getMaximumPoolSize();
        }
        else if ( executor instanceof ForkJoinPool )
        {
            maxSubscribers = ((ForkJoinPool) executor).getParallelism();
        }

        return maxSubscribers;
    }

    public void subscribe( CoreSubscriber<? super E> actual )
    {
        Objects.requireNonNull( actual, "subscribe" );
        if ( !this.alive() )
        {
            TopicProcessor.coldSource( this.ringBuffer, (Throwable) null, this.error, this.workSequence ).subscribe( actual );
        }
        else
        {
            WorkQueueProcessor.WorkQueueInner signalProcessor = new WorkQueueProcessor.WorkQueueInner( actual, this );

            try
            {
                this.incrementSubscribers();
                signalProcessor.sequence.set( this.workSequence.getAsLong() );
                this.ringBuffer.addGatingSequence( signalProcessor.sequence );
                int maxSubscribers = bestEffortMaxSubscribers( this.executor );
                if ( maxSubscribers > Integer.MIN_VALUE && this.subscriberCount > maxSubscribers )
                {
                    throw new IllegalStateException( "The executor service could not accommodate another subscriber, detected limit " + maxSubscribers );
                }

                this.executor.execute( signalProcessor );
            }
            catch ( Throwable var4 )
            {
                this.decrementSubscribers();
                this.ringBuffer.removeGatingSequence( signalProcessor.sequence );
                if ( RejectedExecutionException.class.isAssignableFrom( var4.getClass() ) )
                {
                    TopicProcessor.coldSource( this.ringBuffer, var4, this.error, this.workSequence ).subscribe( actual );
                }
                else
                {
                    Operators.error( actual, var4 );
                }
            }
        }
    }

    public Flux<E> drain()
    {
        return TopicProcessor.coldSource( this.ringBuffer, (Throwable) null, this.error, this.workSequence );
    }

    protected void doError( Throwable t )
    {
        this.writeWait.signalAllWhenBlocking();
    }

    protected void doComplete()
    {
        this.writeWait.signalAllWhenBlocking();
    }

    protected void requestTask( Subscription s )
    {
        ExecutorService var10000 = this.requestTaskExecutor;
        RingBuffer var10004 = this.ringBuffer;
        var10004.getClass();
        var10000.execute( createRequestTask( s, this, (Consumer) null, var10004::getMinimumGatingSequence ) );
    }

    public long getPending()
    {
        return (long) (this.getBufferSize() - this.ringBuffer.getPending() + this.claimedDisposed.size());
    }

    public void run()
    {
        if ( !this.alive() )
        {
            WaitStrategy.alert();
        }
    }

    static final class WorkQueueInner<T> implements Runnable, Subscription, Scannable
    {
        final AtomicBoolean running = new AtomicBoolean( true );
        final RingBuffer.Sequence sequence = RingBuffer.newSequence( -1L );
        final RingBuffer.Sequence pendingRequest = RingBuffer.newSequence( 0L );
        final RingBuffer.Reader barrier;
        final WorkQueueProcessor<T> processor;
        final CoreSubscriber<? super T> subscriber;
        final Runnable waiter = new Runnable()
        {
            public void run()
            {
                if ( WorkQueueInner.this.barrier.isAlerted() || !WorkQueueInner.this.isRunning() ||
                        WorkQueueInner.this.replay( WorkQueueInner.this.pendingRequest.getAsLong() == Long.MAX_VALUE ) )
                {
                    WaitStrategy.alert();
                }
            }
        };

        WorkQueueInner( CoreSubscriber<? super T> subscriber, WorkQueueProcessor<T> processor )
        {
            this.processor = processor;
            this.subscriber = subscriber;
            this.barrier = processor.ringBuffer.newReader();
        }

        void halt()
        {
            this.running.set( false );
            this.barrier.alert();
        }

        boolean isRunning()
        {
            return this.running.get() && (this.processor.terminated == 0 ||
                    this.processor.terminated != 2 && this.processor.error == null && this.processor.ringBuffer.getAsLong() > this.sequence.getAsLong());
        }

        public void run()
        {
            boolean processedSequence = true;

            try
            {
                Thread.currentThread().setContextClassLoader( this.processor.contextClassLoader );
                this.subscriber.onSubscribe( this );
                long cachedAvailableSequence = Long.MIN_VALUE;
                long nextSequence = this.sequence.getAsLong();
                EventLoopProcessor.Slot<T> event = null;
                boolean unbounded = this.pendingRequest.getAsLong() == Long.MAX_VALUE;
                if ( !EventLoopProcessor.waitRequestOrTerminalEvent( this.pendingRequest, this.barrier, this.running, this.sequence, this.waiter ) &&
                        this.replay( unbounded ) )
                {
                    if ( !this.running.get() )
                    {
                        return;
                    }

                    if ( this.processor.terminated == 1 )
                    {
                        if ( this.processor.ringBuffer.getAsLong() == -1L )
                        {
                            if ( this.processor.error != null )
                            {
                                this.subscriber.onError( this.processor.error );
                                return;
                            }

                            this.subscriber.onComplete();
                            return;
                        }
                    }
                    else if ( this.processor.terminated == 2 )
                    {
                        return;
                    }
                }

                while ( true )
                {
                    try
                    {
                        if ( processedSequence )
                        {
                            if ( !this.running.get() )
                            {
                                return;
                            }

                            processedSequence = false;

                            do
                            {
                                for ( nextSequence = this.processor.workSequence.getAsLong() + 1L; !unbounded && this.pendingRequest.getAsLong() == 0L;
                                        LockSupport.parkNanos( 1L ) )
                                {
                                    if ( !this.isRunning() )
                                    {
                                        WaitStrategy.alert();
                                    }
                                }

                                this.sequence.set( nextSequence - 1L );
                            }
                            while ( !this.processor.workSequence.compareAndSet( nextSequence - 1L, nextSequence ) );
                        }

                        if ( cachedAvailableSequence >= nextSequence )
                        {
                            event = (EventLoopProcessor.Slot) this.processor.ringBuffer.get( nextSequence );

                            try
                            {
                                this.readNextEvent( unbounded );
                            }
                            catch ( Exception var13 )
                            {
                                if ( !this.running.get() || !WaitStrategy.isAlert( var13 ) )
                                {
                                    throw var13;
                                }

                                this.barrier.clearAlert();
                            }

                            processedSequence = true;
                            this.subscriber.onNext( event.value );
                        }
                        else
                        {
                            this.processor.readWait.signalAllWhenBlocking();
                            cachedAvailableSequence = this.barrier.waitFor( nextSequence, this.waiter );
                        }
                    }
                    catch ( RuntimeException | InterruptedException var14 )
                    {
                        if ( var14 instanceof InterruptedException )
                        {
                            Thread.currentThread().interrupt();
                        }

                        if ( Exceptions.isCancel( var14 ) )
                        {
                            this.reschedule( event );
                            return;
                        }

                        if ( !WaitStrategy.isAlert( var14 ) )
                        {
                            throw Exceptions.propagate( var14 );
                        }

                        this.barrier.clearAlert();
                        if ( !this.running.get() )
                        {
                            return;
                        }

                        if ( this.processor.terminated == 1 )
                        {
                            if ( this.processor.error != null )
                            {
                                processedSequence = true;
                                this.subscriber.onError( this.processor.error );
                                return;
                            }

                            if ( this.processor.ringBuffer.getPending() == 0 )
                            {
                                processedSequence = true;
                                this.subscriber.onComplete();
                                return;
                            }
                        }
                        else if ( this.processor.terminated == 2 )
                        {
                            return;
                        }
                    }
                }
            }
            finally
            {
                this.processor.decrementSubscribers();
                this.running.set( false );
                if ( !processedSequence )
                {
                    this.processor.claimedDisposed.add( this.sequence );
                }
                else
                {
                    this.processor.ringBuffer.removeGatingSequence( this.sequence );
                }

                this.processor.writeWait.signalAllWhenBlocking();
            }
        }

        boolean replay( boolean unbounded )
        {
            if ( WorkQueueProcessor.REPLAYING.compareAndSet( this.processor, 0, 1 ) )
            {
                try
                {
                    boolean var3;
                    try
                    {
                        RingBuffer.Sequence s = null;

                        while ( this.running.get() )
                        {
                            Object v = this.processor.claimedDisposed.peek();
                            if ( v == null )
                            {
                                this.processor.readWait.signalAllWhenBlocking();
                                boolean var15 = !this.processor.alive() && this.processor.ringBuffer.getPending() == 0;
                                return var15;
                            }

                            if ( v instanceof RingBuffer.Sequence )
                            {
                                s = (RingBuffer.Sequence) v;
                                long cursor = s.getAsLong() + 1L;
                                if ( cursor > this.processor.ringBuffer.getAsLong() )
                                {
                                    this.processor.readWait.signalAllWhenBlocking();
                                    boolean var6 = !this.processor.alive() && this.processor.ringBuffer.getPending() == 0;
                                    return var6;
                                }

                                this.barrier.waitFor( cursor, this.waiter );
                                v = ((EventLoopProcessor.Slot) this.processor.ringBuffer.get( cursor )).value;
                                if ( v == null )
                                {
                                    this.processor.ringBuffer.removeGatingSequence( s );
                                    this.processor.claimedDisposed.poll();
                                    s = null;
                                    continue;
                                }
                            }

                            this.readNextEvent( unbounded );
                            this.subscriber.onNext( v );
                            this.processor.claimedDisposed.poll();
                            if ( s != null )
                            {
                                this.processor.ringBuffer.removeGatingSequence( s );
                                s = null;
                            }
                        }

                        this.processor.readWait.signalAllWhenBlocking();
                        var3 = true;
                        return var3;
                    }
                    catch ( RuntimeException var11 )
                    {
                        if ( this.running.get() && !Exceptions.isCancel( var11 ) )
                        {
                            throw var11;
                        }
                        else
                        {
                            this.running.set( false );
                            var3 = true;
                            return var3;
                        }
                    }
                    catch ( InterruptedException var12 )
                    {
                        this.running.set( false );
                        Thread.currentThread().interrupt();
                        var3 = true;
                        return var3;
                    }
                }
                finally
                {
                    WorkQueueProcessor.REPLAYING.compareAndSet( this.processor, 1, 0 );
                }
            }
            else
            {
                return !this.processor.alive() && this.processor.ringBuffer.getPending() == 0;
            }
        }

        boolean reschedule( @Nullable EventLoopProcessor.Slot<T> event )
        {
            if ( event != null && event.value != null )
            {
                this.processor.claimedDisposed.add( event.value );
                this.barrier.alert();
                this.processor.readWait.signalAllWhenBlocking();
                return true;
            }
            else
            {
                return false;
            }
        }

        void readNextEvent( boolean unbounded )
        {
            for ( ; !unbounded && EventLoopProcessor.getAndSub( this.pendingRequest, 1L ) == 0L; LockSupport.parkNanos( 1L ) )
            {
                if ( !this.isRunning() )
                {
                    WaitStrategy.alert();
                }
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.processor;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.subscriber;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.processor.isTerminated();
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return !this.running.get();
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.pendingRequest.getAsLong() : null;
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && this.running.get() )
            {
                EventLoopProcessor.addCap( this.pendingRequest, n );
            }
        }

        public void cancel()
        {
            this.halt();
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static final class Builder<T>
    {
        String name;
        ExecutorService executor;
        ExecutorService requestTaskExecutor;
        int bufferSize;
        WaitStrategy waitStrategy;
        boolean share;
        boolean autoCancel;

        Builder()
        {
            this.bufferSize = Queues.SMALL_BUFFER_SIZE;
            this.autoCancel = true;
            this.share = false;
        }

        public WorkQueueProcessor.Builder<T> name( @Nullable String name )
        {
            if ( this.executor != null )
            {
                throw new IllegalArgumentException( "Executor service is configured, name will not be used." );
            }
            else
            {
                this.name = name;
                return this;
            }
        }

        public WorkQueueProcessor.Builder<T> bufferSize( int bufferSize )
        {
            if ( !Queues.isPowerOfTwo( bufferSize ) )
            {
                throw new IllegalArgumentException( "bufferSize must be a power of 2 : " + bufferSize );
            }
            else if ( bufferSize < 1 )
            {
                throw new IllegalArgumentException( "bufferSize must be strictly positive, was: " + bufferSize );
            }
            else
            {
                this.bufferSize = bufferSize;
                return this;
            }
        }

        public WorkQueueProcessor.Builder<T> waitStrategy( @Nullable WaitStrategy waitStrategy )
        {
            this.waitStrategy = waitStrategy;
            return this;
        }

        public WorkQueueProcessor.Builder<T> autoCancel( boolean autoCancel )
        {
            this.autoCancel = autoCancel;
            return this;
        }

        public WorkQueueProcessor.Builder<T> executor( @Nullable ExecutorService executor )
        {
            this.executor = executor;
            return this;
        }

        public WorkQueueProcessor.Builder<T> requestTaskExecutor( @Nullable ExecutorService requestTaskExecutor )
        {
            this.requestTaskExecutor = requestTaskExecutor;
            return this;
        }

        public WorkQueueProcessor.Builder<T> share( boolean share )
        {
            this.share = share;
            return this;
        }

        public WorkQueueProcessor<T> build()
        {
            String name = this.name != null ? this.name : WorkQueueProcessor.class.getSimpleName();
            WaitStrategy waitStrategy = this.waitStrategy != null ? this.waitStrategy : WaitStrategy.liteBlocking();
            ThreadFactory threadFactory = this.executor != null ? null : new EventLoopProcessor.EventLoopFactory( name, this.autoCancel );
            ExecutorService requestTaskExecutor = this.requestTaskExecutor != null ? this.requestTaskExecutor : EventLoopProcessor.defaultRequestTaskExecutor(
                    EventLoopProcessor.defaultName( threadFactory, WorkQueueProcessor.class ) );
            return new WorkQueueProcessor( threadFactory, this.executor, requestTaskExecutor, this.bufferSize, waitStrategy, this.share, this.autoCancel );
        }
    }
}
