package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import org.reactivestreams.Subscription;

final class ParallelThen extends Mono<Void> implements Scannable, Fuseable
{
    final ParallelFlux<?> source;

    ParallelThen( ParallelFlux<?> source )
    {
        this.source = source;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.PARENT ? this.source : null;
    }

    public void subscribe( CoreSubscriber<? super Void> actual )
    {
        ParallelThen.ThenMain parent = new ParallelThen.ThenMain( actual, this.source.parallelism() );
        actual.onSubscribe( parent );
        this.source.subscribe( (CoreSubscriber[]) parent.subscribers );
    }

    static final class ThenInner implements InnerConsumer<Object>
    {
        static final AtomicReferenceFieldUpdater<ParallelThen.ThenInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( ParallelThen.ThenInner.class, Subscription.class, "s" );
        final ParallelThen.ThenMain parent;
        volatile Subscription s;

        ThenInner( ParallelThen.ThenMain parent )
        {
            this.parent = parent;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? Integer.MAX_VALUE : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( Object t )
        {
            Operators.onDiscard( t, this.parent.currentContext() );
        }

        public void onError( Throwable t )
        {
            this.parent.innerError( t );
        }

        public void onComplete()
        {
            this.parent.innerComplete();
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }
    }

    static final class ThenMain extends Operators.MonoSubscriber<Object,Void>
    {
        static final AtomicIntegerFieldUpdater<ParallelThen.ThenMain> REMAINING =
                AtomicIntegerFieldUpdater.newUpdater( ParallelThen.ThenMain.class, "remaining" );
        static final AtomicReferenceFieldUpdater<ParallelThen.ThenMain,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( ParallelThen.ThenMain.class, Throwable.class, "error" );
        final ParallelThen.ThenInner[] subscribers;
        volatile int remaining;
        volatile Throwable error;

        ThenMain( CoreSubscriber<? super Void> subscriber, int n )
        {
            super( subscriber );
            ParallelThen.ThenInner[] a = new ParallelThen.ThenInner[n];

            for ( int i = 0; i < n; ++i )
            {
                a[i] = new ParallelThen.ThenInner( this );
            }

            this.subscribers = a;
            REMAINING.lazySet( this, n );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? REMAINING.get( this ) == 0 : super.scanUnsafe( key );
            }
        }

        public void cancel()
        {
            ParallelThen.ThenInner[] var1 = this.subscribers;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                ParallelThen.ThenInner inner = var1[var3];
                inner.cancel();
            }

            super.cancel();
        }

        void innerError( Throwable ex )
        {
            if ( ERROR.compareAndSet( this, (Object) null, ex ) )
            {
                this.cancel();
                this.actual.onError( ex );
            }
            else if ( this.error != ex )
            {
                Operators.onErrorDropped( ex, this.actual.currentContext() );
            }
        }

        void innerComplete()
        {
            if ( REMAINING.decrementAndGet( this ) == 0 )
            {
                this.actual.onComplete();
            }
        }
    }
}
