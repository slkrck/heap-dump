package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.Timer.Sample;
import org.reactivestreams.Subscription;

final class MonoMetrics<T> extends InternalMonoOperator<T,T>
{
    final String name;
    final Tags tags;
    final MeterRegistry meterRegistry;

    MonoMetrics( Mono<? extends T> mono )
    {
        this( mono, (MeterRegistry) null );
    }

    MonoMetrics( Mono<? extends T> mono, @Nullable MeterRegistry meterRegistry )
    {
        super( mono );
        this.name = FluxMetrics.resolveName( mono );
        this.tags = FluxMetrics.resolveTags( mono, FluxMetrics.DEFAULT_TAGS_MONO, this.name );
        if ( meterRegistry == null )
        {
            this.meterRegistry = Metrics.globalRegistry;
        }
        else
        {
            this.meterRegistry = meterRegistry;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoMetrics.MetricsSubscriber( actual, this.meterRegistry, Clock.SYSTEM, this.tags );
    }

    static class MetricsSubscriber<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        final Clock clock;
        final Tags commonTags;
        final MeterRegistry registry;
        Sample subscribeToTerminateSample;
        boolean done;
        Subscription s;

        MetricsSubscriber( CoreSubscriber<? super T> actual, MeterRegistry registry, Clock clock, Tags commonTags )
        {
            this.actual = actual;
            this.clock = clock;
            this.commonTags = commonTags;
            this.registry = registry;
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public final void cancel()
        {
            FluxMetrics.recordCancel( this.commonTags, this.registry, this.subscribeToTerminateSample );
            this.s.cancel();
        }

        public final void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                FluxMetrics.recordOnComplete( this.commonTags, this.registry, this.subscribeToTerminateSample );
                this.actual.onComplete();
            }
        }

        public final void onError( Throwable e )
        {
            if ( this.done )
            {
                FluxMetrics.recordMalformed( this.commonTags, this.registry );
                Operators.onErrorDropped( e, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                FluxMetrics.recordOnError( this.commonTags, this.registry, this.subscribeToTerminateSample, e );
                this.actual.onError( e );
            }
        }

        public final void onNext( T t )
        {
            if ( this.done )
            {
                FluxMetrics.recordMalformed( this.commonTags, this.registry );
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                FluxMetrics.recordOnComplete( this.commonTags, this.registry, this.subscribeToTerminateSample );
                this.actual.onNext( t );
                this.actual.onComplete();
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                FluxMetrics.recordOnSubscribe( this.commonTags, this.registry );
                this.subscribeToTerminateSample = Timer.start( this.clock );
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public final void request( long l )
        {
            if ( Operators.validate( l ) )
            {
                this.s.request( l );
            }
        }
    }
}
