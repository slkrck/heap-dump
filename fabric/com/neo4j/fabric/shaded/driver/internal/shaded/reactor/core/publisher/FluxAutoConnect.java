package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.function.Consumer;

final class FluxAutoConnect<T> extends Flux<T> implements Scannable
{
    static final AtomicIntegerFieldUpdater<FluxAutoConnect> REMAINING = AtomicIntegerFieldUpdater.newUpdater( FluxAutoConnect.class, "remaining" );
    final ConnectableFlux<? extends T> source;
    final Consumer<? super Disposable> cancelSupport;
    volatile int remaining;

    FluxAutoConnect( ConnectableFlux<? extends T> source, int n, Consumer<? super Disposable> cancelSupport )
    {
        if ( n <= 0 )
        {
            throw new IllegalArgumentException( "n > required but it was " + n );
        }
        else
        {
            this.source = (ConnectableFlux) Objects.requireNonNull( source, "source" );
            this.cancelSupport = (Consumer) Objects.requireNonNull( cancelSupport, "cancelSupport" );
            REMAINING.lazySet( this, n );
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        this.source.subscribe( actual );
        if ( this.remaining > 0 && REMAINING.decrementAndGet( this ) == 0 )
        {
            this.source.connect( this.cancelSupport );
        }
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.CAPACITY ? this.remaining : null;
        }
    }
}
