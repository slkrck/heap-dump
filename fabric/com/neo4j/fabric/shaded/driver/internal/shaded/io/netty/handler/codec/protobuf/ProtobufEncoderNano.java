package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.protobuf;

import com.google.protobuf.nano.CodedOutputByteBufferNano;
import com.google.protobuf.nano.MessageNano;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

@ChannelHandler.Sharable
public class ProtobufEncoderNano extends MessageToMessageEncoder<MessageNano>
{
    protected void encode( ChannelHandlerContext ctx, MessageNano msg, List<Object> out ) throws Exception
    {
        int size = msg.getSerializedSize();
        ByteBuf buffer = ctx.alloc().heapBuffer( size, size );
        byte[] array = buffer.array();
        CodedOutputByteBufferNano cobbn = CodedOutputByteBufferNano.newInstance( array, buffer.arrayOffset(), buffer.capacity() );
        msg.writeTo( cobbn );
        buffer.writerIndex( size );
        out.add( buffer );
    }
}
