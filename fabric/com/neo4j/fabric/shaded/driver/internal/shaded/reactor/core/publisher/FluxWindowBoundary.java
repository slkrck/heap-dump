package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxWindowBoundary<T, U> extends InternalFluxOperator<T,Flux<T>>
{
    final Publisher<U> other;
    final Supplier<? extends Queue<T>> processorQueueSupplier;

    FluxWindowBoundary( Flux<? extends T> source, Publisher<U> other, Supplier<? extends Queue<T>> processorQueueSupplier )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
        this.processorQueueSupplier = (Supplier) Objects.requireNonNull( processorQueueSupplier, "processorQueueSupplier" );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Flux<T>> actual )
    {
        FluxWindowBoundary.WindowBoundaryMain<T,U> main =
                new FluxWindowBoundary.WindowBoundaryMain( actual, this.processorQueueSupplier, (Queue) this.processorQueueSupplier.get() );
        actual.onSubscribe( main );
        if ( main.emit( main.window ) )
        {
            this.other.subscribe( main.boundary );
            return main;
        }
        else
        {
            return null;
        }
    }

    static final class WindowBoundaryOther<U> extends Operators.DeferredSubscription implements InnerConsumer<U>
    {
        final FluxWindowBoundary.WindowBoundaryMain<?,U> main;

        WindowBoundaryOther( FluxWindowBoundary.WindowBoundaryMain<?,U> main )
        {
            this.main = main;
        }

        public void onSubscribe( Subscription s )
        {
            if ( this.set( s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.ACTUAL ? this.main : super.scanUnsafe( key );
        }

        public void onNext( U t )
        {
            this.main.boundaryNext();
        }

        public void onError( Throwable t )
        {
            this.main.boundaryError( t );
        }

        public void onComplete()
        {
            this.main.boundaryComplete();
        }
    }

    static final class WindowBoundaryMain<T, U> implements InnerOperator<T,Flux<T>>, Disposable
    {
        static final AtomicReferenceFieldUpdater<FluxWindowBoundary.WindowBoundaryMain,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxWindowBoundary.WindowBoundaryMain.class, Subscription.class, "s" );
        static final AtomicLongFieldUpdater<FluxWindowBoundary.WindowBoundaryMain> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxWindowBoundary.WindowBoundaryMain.class, "requested" );
        static final AtomicReferenceFieldUpdater<FluxWindowBoundary.WindowBoundaryMain,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( FluxWindowBoundary.WindowBoundaryMain.class, Throwable.class, "error" );
        static final AtomicIntegerFieldUpdater<FluxWindowBoundary.WindowBoundaryMain> CANCELLED =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindowBoundary.WindowBoundaryMain.class, "cancelled" );
        static final AtomicIntegerFieldUpdater<FluxWindowBoundary.WindowBoundaryMain> WINDOW_COUNT =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindowBoundary.WindowBoundaryMain.class, "windowCount" );
        static final AtomicIntegerFieldUpdater<FluxWindowBoundary.WindowBoundaryMain> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindowBoundary.WindowBoundaryMain.class, "wip" );
        static final Object BOUNDARY_MARKER = new Object();
        static final Object DONE = new Object();
        final Supplier<? extends Queue<T>> processorQueueSupplier;
        final FluxWindowBoundary.WindowBoundaryOther<U> boundary;
        final Queue<Object> queue;
        final CoreSubscriber<? super Flux<T>> actual;
        UnicastProcessor<T> window;
        volatile Subscription s;
        volatile long requested;
        volatile Throwable error;
        volatile int cancelled;
        volatile int windowCount;
        volatile int wip;
        boolean done;

        WindowBoundaryMain( CoreSubscriber<? super Flux<T>> actual, Supplier<? extends Queue<T>> processorQueueSupplier, Queue<T> processorQueue )
        {
            this.actual = actual;
            this.processorQueueSupplier = processorQueueSupplier;
            this.window = new UnicastProcessor( processorQueue, this );
            WINDOW_COUNT.lazySet( this, 2 );
            this.boundary = new FluxWindowBoundary.WindowBoundaryOther( this );
            this.queue = (Queue) Queues.unboundedMultiproducer().get();
        }

        public final CoreSubscriber<? super Flux<T>> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled == 1;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.queue.size() : InnerOperator.super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.boundary, this.window );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                synchronized ( this )
                {
                    this.queue.offer( t );
                }

                this.drain();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.boundary.cancel();
                if ( Exceptions.addThrowable( ERROR, this, t ) )
                {
                    this.drain();
                }
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.boundary.cancel();
                synchronized ( this )
                {
                    this.queue.offer( DONE );
                }

                this.drain();
            }
        }

        public void dispose()
        {
            if ( WINDOW_COUNT.decrementAndGet( this ) == 0 )
            {
                this.cancelMain();
                this.boundary.cancel();
            }
        }

        public boolean isDisposed()
        {
            return this.cancelled == 1 || this.done;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
            }
        }

        void cancelMain()
        {
            Operators.terminate( S, this );
        }

        public void cancel()
        {
            if ( CANCELLED.compareAndSet( this, 0, 1 ) )
            {
                this.dispose();
            }
        }

        void boundaryNext()
        {
            synchronized ( this )
            {
                this.queue.offer( BOUNDARY_MARKER );
            }

            if ( this.cancelled != 0 )
            {
                this.boundary.cancel();
            }

            this.drain();
        }

        void boundaryError( Throwable e )
        {
            this.cancelMain();
            if ( Exceptions.addThrowable( ERROR, this, e ) )
            {
                this.drain();
            }
            else
            {
                Operators.onErrorDropped( e, this.actual.currentContext() );
            }
        }

        void boundaryComplete()
        {
            this.cancelMain();
            synchronized ( this )
            {
                this.queue.offer( DONE );
            }

            this.drain();
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                Subscriber<? super Flux<T>> a = this.actual;
                Queue<Object> q = this.queue;
                UnicastProcessor<T> w = this.window;
                int missed = 1;

                while ( this.error == null )
                {
                    Object o = q.poll();
                    if ( o == null )
                    {
                        missed = WIP.addAndGet( this, -missed );
                        if ( missed == 0 )
                        {
                            return;
                        }
                    }
                    else
                    {
                        if ( o == DONE )
                        {
                            q.clear();
                            w.onComplete();
                            a.onComplete();
                            return;
                        }

                        if ( o != BOUNDARY_MARKER )
                        {
                            w.onNext( o );
                        }

                        if ( o == BOUNDARY_MARKER )
                        {
                            w.onComplete();
                            if ( this.cancelled == 0 )
                            {
                                if ( this.requested == 0L )
                                {
                                    q.clear();
                                    this.cancelMain();
                                    this.boundary.cancel();
                                    a.onError( Exceptions.failWithOverflow( "Could not create new window due to lack of requests" ) );
                                    return;
                                }

                                Queue<T> pq = (Queue) this.processorQueueSupplier.get();
                                WINDOW_COUNT.getAndIncrement( this );
                                w = new UnicastProcessor( pq, this );
                                this.window = w;
                                a.onNext( w );
                                if ( this.requested != Long.MAX_VALUE )
                                {
                                    REQUESTED.decrementAndGet( this );
                                }
                            }
                        }
                    }
                }

                q.clear();
                Throwable e = Exceptions.terminate( ERROR, this );
                if ( e != Exceptions.TERMINATED )
                {
                    w.onError( e );
                    a.onError( e );
                }
            }
        }

        boolean emit( UnicastProcessor<T> w )
        {
            long r = this.requested;
            if ( r != 0L )
            {
                this.actual.onNext( w );
                if ( r != Long.MAX_VALUE )
                {
                    REQUESTED.decrementAndGet( this );
                }

                return true;
            }
            else
            {
                this.cancel();
                this.actual.onError( Exceptions.failWithOverflow( "Could not emit buffer due to lack of requests" ) );
                return false;
            }
        }
    }
}
