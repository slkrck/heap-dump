package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

import org.reactivestreams.Publisher;

final class MonoIgnorePublisher<T> extends Mono<T> implements Scannable, OptimizableOperator<T,T>
{
    final Publisher<? extends T> source;
    @Nullable
    final OptimizableOperator<?,T> optimizableOperator;

    MonoIgnorePublisher( Publisher<? extends T> source )
    {
        this.source = (Publisher) Objects.requireNonNull( source, "publisher" );
        this.optimizableOperator = source instanceof OptimizableOperator ? (OptimizableOperator) source : null;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        this.source.subscribe( this.subscribeOrReturn( actual ) );
    }

    public final CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoIgnoreElements.IgnoreElementsSubscriber( actual );
    }

    public final CorePublisher<? extends T> source()
    {
        return this;
    }

    public final OptimizableOperator<?,? extends T> nextOptimizableSource()
    {
        return this.optimizableOperator;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.PARENT ? this.source : null;
    }
}
