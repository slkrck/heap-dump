package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.CertificateCallback;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.SSLContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.SniHostNameMatcher;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.CharsetUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.net.ssl.X509TrustManager;

public final class ReferenceCountedOpenSslServerContext extends ReferenceCountedOpenSslContext
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( ReferenceCountedOpenSslServerContext.class );
    private static final byte[] ID = new byte[]{110, 101, 116, 116, 121};
    private final OpenSslServerSessionContext sessionContext;

    ReferenceCountedOpenSslServerContext( X509Certificate[] trustCertCollection, TrustManagerFactory trustManagerFactory, X509Certificate[] keyCertChain,
            PrivateKey key, String keyPassword, KeyManagerFactory keyManagerFactory, Iterable<String> ciphers, CipherSuiteFilter cipherFilter,
            ApplicationProtocolConfig apn, long sessionCacheSize, long sessionTimeout, ClientAuth clientAuth, String[] protocols, boolean startTls,
            boolean enableOcsp, String keyStore ) throws SSLException
    {
        this( trustCertCollection, trustManagerFactory, keyCertChain, key, keyPassword, keyManagerFactory, ciphers, cipherFilter, toNegotiator( apn ),
                sessionCacheSize, sessionTimeout, clientAuth, protocols, startTls, enableOcsp, keyStore );
    }

    ReferenceCountedOpenSslServerContext( X509Certificate[] trustCertCollection, TrustManagerFactory trustManagerFactory, X509Certificate[] keyCertChain,
            PrivateKey key, String keyPassword, KeyManagerFactory keyManagerFactory, Iterable<String> ciphers, CipherSuiteFilter cipherFilter,
            OpenSslApplicationProtocolNegotiator apn, long sessionCacheSize, long sessionTimeout, ClientAuth clientAuth, String[] protocols, boolean startTls,
            boolean enableOcsp, String keyStore ) throws SSLException
    {
        super( ciphers, cipherFilter, (OpenSslApplicationProtocolNegotiator) apn, sessionCacheSize, sessionTimeout, 1, keyCertChain, clientAuth, protocols,
                startTls, enableOcsp, true );
        boolean success = false;

        try
        {
            this.sessionContext = newSessionContext( this, this.ctx, this.engineMap, trustCertCollection, trustManagerFactory, keyCertChain, key, keyPassword,
                    keyManagerFactory, keyStore );
            success = true;
        }
        finally
        {
            if ( !success )
            {
                this.release();
            }
        }
    }

    static OpenSslServerSessionContext newSessionContext( ReferenceCountedOpenSslContext thiz, long ctx, OpenSslEngineMap engineMap,
            X509Certificate[] trustCertCollection, TrustManagerFactory trustManagerFactory, X509Certificate[] keyCertChain, PrivateKey key, String keyPassword,
            KeyManagerFactory keyManagerFactory, String keyStore ) throws SSLException
    {
        OpenSslKeyMaterialProvider keyMaterialProvider = null;

        OpenSslServerSessionContext var36;
        try
        {
            try
            {
                SSLContext.setVerify( ctx, 0, 10 );
                if ( !OpenSsl.useKeyManagerFactory() )
                {
                    if ( keyManagerFactory != null )
                    {
                        throw new IllegalArgumentException( "KeyManagerFactory not supported" );
                    }

                    ObjectUtil.checkNotNull( keyCertChain, "keyCertChain" );
                    setKeyMaterial( ctx, keyCertChain, key, keyPassword );
                }
                else
                {
                    if ( keyManagerFactory == null )
                    {
                        char[] keyPasswordChars = keyStorePassword( keyPassword );
                        KeyStore ks = buildKeyStore( keyCertChain, key, keyPasswordChars, keyStore );
                        if ( ks.aliases().hasMoreElements() )
                        {
                            keyManagerFactory = new OpenSslX509KeyManagerFactory();
                        }
                        else
                        {
                            keyManagerFactory =
                                    new OpenSslCachingX509KeyManagerFactory( KeyManagerFactory.getInstance( KeyManagerFactory.getDefaultAlgorithm() ) );
                        }

                        ((KeyManagerFactory) keyManagerFactory).init( ks, keyPasswordChars );
                    }

                    keyMaterialProvider = providerFor( (KeyManagerFactory) keyManagerFactory, keyPassword );
                    SSLContext.setCertificateCallback( ctx, new ReferenceCountedOpenSslServerContext.OpenSslServerCertificateCallback( engineMap,
                            new OpenSslKeyMaterialManager( keyMaterialProvider ) ) );
                }
            }
            catch ( Exception var29 )
            {
                throw new SSLException( "failed to set certificate and key", var29 );
            }

            try
            {
                if ( trustCertCollection != null )
                {
                    trustManagerFactory = buildTrustManagerFactory( trustCertCollection, trustManagerFactory, keyStore );
                }
                else if ( trustManagerFactory == null )
                {
                    trustManagerFactory = TrustManagerFactory.getInstance( TrustManagerFactory.getDefaultAlgorithm() );
                    trustManagerFactory.init( (KeyStore) null );
                }

                X509TrustManager manager = chooseTrustManager( trustManagerFactory.getTrustManagers() );
                if ( useExtendedTrustManager( manager ) )
                {
                    SSLContext.setCertVerifyCallback( ctx,
                            new ReferenceCountedOpenSslServerContext.ExtendedTrustManagerVerifyCallback( engineMap, (X509ExtendedTrustManager) manager ) );
                }
                else
                {
                    SSLContext.setCertVerifyCallback( ctx, new ReferenceCountedOpenSslServerContext.TrustManagerVerifyCallback( engineMap, manager ) );
                }

                X509Certificate[] issuers = manager.getAcceptedIssuers();
                if ( issuers != null && issuers.length > 0 )
                {
                    long bio = 0L;

                    try
                    {
                        bio = toBIO( ByteBufAllocator.DEFAULT, issuers );
                        if ( !SSLContext.setCACertificateBio( ctx, bio ) )
                        {
                            throw new SSLException( "unable to setup accepted issuers for trustmanager " + manager );
                        }
                    }
                    finally
                    {
                        freeBio( bio );
                    }
                }

                if ( PlatformDependent.javaVersion() >= 8 )
                {
                    SSLContext.setSniHostnameMatcher( ctx, new ReferenceCountedOpenSslServerContext.OpenSslSniHostnameMatcher( engineMap ) );
                }
            }
            catch ( SSLException var30 )
            {
                throw var30;
            }
            catch ( Exception var31 )
            {
                throw new SSLException( "unable to setup trustmanager", var31 );
            }

            OpenSslServerSessionContext sessionContext = new OpenSslServerSessionContext( thiz, keyMaterialProvider );
            sessionContext.setSessionIdContext( ID );
            keyMaterialProvider = null;
            var36 = sessionContext;
        }
        finally
        {
            if ( keyMaterialProvider != null )
            {
                keyMaterialProvider.destroy();
            }
        }

        return var36;
    }

    public OpenSslServerSessionContext sessionContext()
    {
        return this.sessionContext;
    }

    private static final class OpenSslSniHostnameMatcher implements SniHostNameMatcher
    {
        private final OpenSslEngineMap engineMap;

        OpenSslSniHostnameMatcher( OpenSslEngineMap engineMap )
        {
            this.engineMap = engineMap;
        }

        public boolean match( long ssl, String hostname )
        {
            ReferenceCountedOpenSslEngine engine = this.engineMap.get( ssl );
            if ( engine != null )
            {
                return engine.checkSniHostnameMatch( hostname.getBytes( CharsetUtil.UTF_8 ) );
            }
            else
            {
                ReferenceCountedOpenSslServerContext.logger.warn( "No ReferenceCountedOpenSslEngine found for SSL pointer: {}", (Object) ssl );
                return false;
            }
        }
    }

    private static final class ExtendedTrustManagerVerifyCallback extends ReferenceCountedOpenSslContext.AbstractCertificateVerifier
    {
        private final X509ExtendedTrustManager manager;

        ExtendedTrustManagerVerifyCallback( OpenSslEngineMap engineMap, X509ExtendedTrustManager manager )
        {
            super( engineMap );
            this.manager = OpenSslTlsv13X509ExtendedTrustManager.wrap( manager );
        }

        void verify( ReferenceCountedOpenSslEngine engine, X509Certificate[] peerCerts, String auth ) throws Exception
        {
            this.manager.checkClientTrusted( peerCerts, auth, engine );
        }
    }

    private static final class TrustManagerVerifyCallback extends ReferenceCountedOpenSslContext.AbstractCertificateVerifier
    {
        private final X509TrustManager manager;

        TrustManagerVerifyCallback( OpenSslEngineMap engineMap, X509TrustManager manager )
        {
            super( engineMap );
            this.manager = manager;
        }

        void verify( ReferenceCountedOpenSslEngine engine, X509Certificate[] peerCerts, String auth ) throws Exception
        {
            this.manager.checkClientTrusted( peerCerts, auth );
        }
    }

    private static final class OpenSslServerCertificateCallback implements CertificateCallback
    {
        private final OpenSslEngineMap engineMap;
        private final OpenSslKeyMaterialManager keyManagerHolder;

        OpenSslServerCertificateCallback( OpenSslEngineMap engineMap, OpenSslKeyMaterialManager keyManagerHolder )
        {
            this.engineMap = engineMap;
            this.keyManagerHolder = keyManagerHolder;
        }

        public void handle( long ssl, byte[] keyTypeBytes, byte[][] asn1DerEncodedPrincipals ) throws Exception
        {
            ReferenceCountedOpenSslEngine engine = this.engineMap.get( ssl );

            try
            {
                this.keyManagerHolder.setKeyMaterialServerSide( engine );
            }
            catch ( Throwable var7 )
            {
                ReferenceCountedOpenSslServerContext.logger.debug( "Failed to set the server-side key material", var7 );
                engine.initHandshakeException( var7 );
            }
        }
    }
}
