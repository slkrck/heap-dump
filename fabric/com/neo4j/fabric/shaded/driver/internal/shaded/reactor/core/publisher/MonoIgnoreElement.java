package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class MonoIgnoreElement<T> extends InternalMonoOperator<T,T>
{
    MonoIgnoreElement( Mono<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoIgnoreElements.IgnoreElementsSubscriber( actual );
    }
}
