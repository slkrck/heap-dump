package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class ContextN extends HashMap<Object,Object> implements Context, Function<Entry<Object,Object>,Entry<Object,Object>>, BiConsumer<Object,Object>
{
    ContextN( Object key1, Object value1, Object key2, Object value2, Object key3, Object value3, Object key4, Object value4, Object key5, Object value5,
            Object key6, Object value6 )
    {
        super( 6, 1.0F );
        this.accept( key1, value1 );
        this.accept( key2, value2 );
        this.accept( key3, value3 );
        this.accept( key4, value4 );
        this.accept( key5, value5 );
        this.accept( key6, value6 );
    }

    ContextN( Map<Object,Object> map, Object key, Object value )
    {
        super( ((Map) Objects.requireNonNull( map, "map" )).size() + 1, 1.0F );
        map.forEach( this );
        this.accept( key, value );
    }

    ContextN( Map<Object,Object> sourceMap, Map<?,?> other )
    {
        super( ((Map) Objects.requireNonNull( sourceMap, "sourceMap" )).size() + ((Map) Objects.requireNonNull( other, "other" )).size(), 1.0F );
        sourceMap.forEach( this );
        other.forEach( this );
    }

    public void accept( Object key, Object value )
    {
        super.put( Objects.requireNonNull( key, "key" ), Objects.requireNonNull( value, "value" ) );
    }

    public Context put( Object key, Object value )
    {
        Objects.requireNonNull( key, "key" );
        Objects.requireNonNull( key, "value" );
        return new ContextN( this, key, value );
    }

    public Context delete( Object key )
    {
        Objects.requireNonNull( key, "key" );
        if ( !this.hasKey( key ) )
        {
            return this;
        }
        else
        {
            int s = this.size() - 1;
            if ( s == 5 )
            {
                Entry<Object,Object>[] arr = new Entry[s];
                int idx = 0;
                Iterator var5 = this.entrySet().iterator();

                while ( var5.hasNext() )
                {
                    Entry<Object,Object> entry = (Entry) var5.next();
                    if ( !entry.getKey().equals( key ) )
                    {
                        arr[idx] = entry;
                        ++idx;
                    }
                }

                return new Context5( arr[0].getKey(), arr[0].getValue(), arr[1].getKey(), arr[1].getValue(), arr[2].getKey(), arr[2].getValue(),
                        arr[3].getKey(), arr[3].getValue(), arr[4].getKey(), arr[4].getValue() );
            }
            else
            {
                ContextN newInstance = new ContextN( this, Collections.emptyMap() );
                newInstance.remove( key );
                return newInstance;
            }
        }
    }

    public boolean hasKey( Object key )
    {
        return super.containsKey( key );
    }

    public Object get( Object key )
    {
        Object o = super.get( key );
        if ( o != null )
        {
            return o;
        }
        else
        {
            throw new NoSuchElementException( "Context does not contain key: " + key );
        }
    }

    @Nullable
    public Object getOrDefault( Object key, @Nullable Object defaultValue )
    {
        Object o = super.get( key );
        return o != null ? o : defaultValue;
    }

    public Stream<Entry<Object,Object>> stream()
    {
        return this.entrySet().stream().map( this );
    }

    public Entry<Object,Object> apply( Entry<Object,Object> o )
    {
        return new Context1( o.getKey(), o.getValue() );
    }

    public Context putAll( Context other )
    {
        if ( other.isEmpty() )
        {
            return this;
        }
        else if ( other instanceof ContextN )
        {
            return new ContextN( this, (ContextN) other );
        }
        else
        {
            Map<?,?> mapOther = (Map) other.stream().collect( Collectors.toMap( Entry::getKey, Entry::getValue ) );
            return new ContextN( this, mapOther );
        }
    }

    public String toString()
    {
        return "ContextN" + super.toString();
    }
}
