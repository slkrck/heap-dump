package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

import java.util.concurrent.locks.LockSupport;

import sun.misc.Unsafe;

final class MultiProducerRingBuffer extends RingBufferProducer
{
    private static final Unsafe UNSAFE = (Unsafe) RingBuffer.getUnsafe();
    private static final long BASE;
    private static final long SCALE;

    static
    {
        BASE = (long) UNSAFE.arrayBaseOffset( int[].class );
        SCALE = (long) UNSAFE.arrayIndexScale( int[].class );
    }

    private final RingBuffer.Sequence gatingSequenceCache = new UnsafeSequence( -1L );
    private final int[] availableBuffer;
    private final int indexMask;
    private final int indexShift;

    MultiProducerRingBuffer( int bufferSize, WaitStrategy waitStrategy, Runnable spinObserver )
    {
        super( bufferSize, waitStrategy, spinObserver );
        this.availableBuffer = new int[bufferSize];
        this.indexMask = bufferSize - 1;
        this.indexShift = RingBuffer.log2( bufferSize );
        this.initialiseAvailableBuffer();
    }

    long next()
    {
        return this.next( 1 );
    }

    long next( int n )
    {
        while ( true )
        {
            long current = this.cursor.getAsLong();
            long next = current + (long) n;
            long wrapPoint = next - (long) this.bufferSize;
            long cachedGatingSequence = this.gatingSequenceCache.getAsLong();
            if ( wrapPoint <= cachedGatingSequence && cachedGatingSequence <= current )
            {
                if ( this.cursor.compareAndSet( current, next ) )
                {
                    return next;
                }
            }
            else
            {
                long gatingSequence = RingBuffer.getMinimumSequence( this.gatingSequences, current );
                if ( wrapPoint > gatingSequence )
                {
                    if ( this.spinObserver != null )
                    {
                        this.spinObserver.run();
                    }

                    LockSupport.parkNanos( 1L );
                }
                else
                {
                    this.gatingSequenceCache.set( gatingSequence );
                }
            }
        }
    }

    long getPending()
    {
        long consumed = RingBuffer.getMinimumSequence( this.gatingSequences, this.cursor.getAsLong() );
        long produced = this.cursor.getAsLong();
        return produced - consumed;
    }

    private void initialiseAvailableBuffer()
    {
        for ( int i = this.availableBuffer.length - 1; i != 0; --i )
        {
            this.setAvailableBufferValue( i, -1 );
        }

        this.setAvailableBufferValue( 0, -1 );
    }

    void publish( long sequence )
    {
        this.setAvailable( sequence );
        this.waitStrategy.signalAllWhenBlocking();
    }

    private void setAvailable( long sequence )
    {
        this.setAvailableBufferValue( this.calculateIndex( sequence ), this.calculateAvailabilityFlag( sequence ) );
    }

    private void setAvailableBufferValue( int index, int flag )
    {
        long bufferAddress = (long) index * SCALE + BASE;
        UNSAFE.putOrderedInt( this.availableBuffer, bufferAddress, flag );
    }

    boolean isAvailable( long sequence )
    {
        int index = this.calculateIndex( sequence );
        int flag = this.calculateAvailabilityFlag( sequence );
        long bufferAddress = (long) index * SCALE + BASE;
        return UNSAFE.getIntVolatile( this.availableBuffer, bufferAddress ) == flag;
    }

    long getHighestPublishedSequence( long lowerBound, long availableSequence )
    {
        for ( long sequence = lowerBound; sequence <= availableSequence; ++sequence )
        {
            if ( !this.isAvailable( sequence ) )
            {
                return sequence - 1L;
            }
        }

        return availableSequence;
    }

    private int calculateAvailabilityFlag( long sequence )
    {
        return (int) (sequence >>> this.indexShift);
    }

    private int calculateIndex( long sequence )
    {
        return (int) sequence & this.indexMask;
    }
}
