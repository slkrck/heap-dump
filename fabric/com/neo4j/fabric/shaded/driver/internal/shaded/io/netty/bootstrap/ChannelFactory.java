package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;

/**
 * @deprecated
 */
@Deprecated
public interface ChannelFactory<T extends Channel>
{
    T newChannel();
}
