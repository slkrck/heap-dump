package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;

final class MonoDefaultIfEmpty<T> extends InternalMonoOperator<T,T>
{
    final T defaultValue;

    MonoDefaultIfEmpty( Mono<? extends T> source, T defaultValue )
    {
        super( source );
        this.defaultValue = Objects.requireNonNull( defaultValue, "defaultValue" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxDefaultIfEmpty.DefaultIfEmptySubscriber( actual, this.defaultValue );
    }
}
