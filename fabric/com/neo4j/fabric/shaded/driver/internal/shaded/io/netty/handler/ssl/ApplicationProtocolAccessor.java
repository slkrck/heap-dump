package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

interface ApplicationProtocolAccessor
{
    String getNegotiatedApplicationProtocol();
}
