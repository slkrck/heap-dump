package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

abstract class SingleProducerSequencerFields extends SingleProducerSequencerPad
{
    protected long nextValue = -1L;
    protected long cachedValue = -1L;

    SingleProducerSequencerFields( int bufferSize, WaitStrategy waitStrategy, @Nullable Runnable spinObserver )
    {
        super( bufferSize, waitStrategy, spinObserver );
    }
}
