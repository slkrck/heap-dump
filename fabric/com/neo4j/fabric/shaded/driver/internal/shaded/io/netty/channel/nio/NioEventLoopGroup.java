package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.nio;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.DefaultSelectStrategyFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoop;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopTaskQueueFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.MultithreadEventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.SelectStrategyFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutorChooserFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.RejectedExecutionHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.RejectedExecutionHandlers;

import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;

public class NioEventLoopGroup extends MultithreadEventLoopGroup
{
    public NioEventLoopGroup()
    {
        this( 0 );
    }

    public NioEventLoopGroup( int nThreads )
    {
        this( nThreads, (Executor) null );
    }

    public NioEventLoopGroup( int nThreads, ThreadFactory threadFactory )
    {
        this( nThreads, threadFactory, SelectorProvider.provider() );
    }

    public NioEventLoopGroup( int nThreads, Executor executor )
    {
        this( nThreads, executor, SelectorProvider.provider() );
    }

    public NioEventLoopGroup( int nThreads, ThreadFactory threadFactory, SelectorProvider selectorProvider )
    {
        this( nThreads, threadFactory, selectorProvider, DefaultSelectStrategyFactory.INSTANCE );
    }

    public NioEventLoopGroup( int nThreads, ThreadFactory threadFactory, SelectorProvider selectorProvider, SelectStrategyFactory selectStrategyFactory )
    {
        super( nThreads, threadFactory, selectorProvider, selectStrategyFactory, RejectedExecutionHandlers.reject() );
    }

    public NioEventLoopGroup( int nThreads, Executor executor, SelectorProvider selectorProvider )
    {
        this( nThreads, executor, selectorProvider, DefaultSelectStrategyFactory.INSTANCE );
    }

    public NioEventLoopGroup( int nThreads, Executor executor, SelectorProvider selectorProvider, SelectStrategyFactory selectStrategyFactory )
    {
        super( nThreads, executor, selectorProvider, selectStrategyFactory, RejectedExecutionHandlers.reject() );
    }

    public NioEventLoopGroup( int nThreads, Executor executor, EventExecutorChooserFactory chooserFactory, SelectorProvider selectorProvider,
            SelectStrategyFactory selectStrategyFactory )
    {
        super( nThreads, executor, chooserFactory, selectorProvider, selectStrategyFactory, RejectedExecutionHandlers.reject() );
    }

    public NioEventLoopGroup( int nThreads, Executor executor, EventExecutorChooserFactory chooserFactory, SelectorProvider selectorProvider,
            SelectStrategyFactory selectStrategyFactory, RejectedExecutionHandler rejectedExecutionHandler )
    {
        super( nThreads, executor, chooserFactory, selectorProvider, selectStrategyFactory, rejectedExecutionHandler );
    }

    public NioEventLoopGroup( int nThreads, Executor executor, EventExecutorChooserFactory chooserFactory, SelectorProvider selectorProvider,
            SelectStrategyFactory selectStrategyFactory, RejectedExecutionHandler rejectedExecutionHandler, EventLoopTaskQueueFactory taskQueueFactory )
    {
        super( nThreads, executor, chooserFactory, selectorProvider, selectStrategyFactory, rejectedExecutionHandler, taskQueueFactory );
    }

    public void setIoRatio( int ioRatio )
    {
        Iterator var2 = this.iterator();

        while ( var2.hasNext() )
        {
            EventExecutor e = (EventExecutor) var2.next();
            ((NioEventLoop) e).setIoRatio( ioRatio );
        }
    }

    public void rebuildSelectors()
    {
        Iterator var1 = this.iterator();

        while ( var1.hasNext() )
        {
            EventExecutor e = (EventExecutor) var1.next();
            ((NioEventLoop) e).rebuildSelector();
        }
    }

    protected EventLoop newChild( Executor executor, Object... args ) throws Exception
    {
        EventLoopTaskQueueFactory queueFactory = args.length == 4 ? (EventLoopTaskQueueFactory) args[3] : null;
        return new NioEventLoop( this, executor, (SelectorProvider) args[0], ((SelectStrategyFactory) args[1]).newSelectStrategy(),
                (RejectedExecutionHandler) args[2], queueFactory );
    }
}
