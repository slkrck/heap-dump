package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Schedulers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.reactivestreams.Subscription;

abstract class BlockingSingleSubscriber<T> extends CountDownLatch implements InnerConsumer<T>, Disposable
{
    T value;
    Throwable error;
    Subscription s;
    volatile boolean cancelled;

    BlockingSingleSubscriber()
    {
        super( 1 );
    }

    public final void onSubscribe( Subscription s )
    {
        this.s = s;
        if ( !this.cancelled )
        {
            s.request( Long.MAX_VALUE );
        }
    }

    public final void onComplete()
    {
        this.countDown();
    }

    public final void dispose()
    {
        this.cancelled = true;
        Subscription s = this.s;
        if ( s != null )
        {
            this.s = null;
            s.cancel();
        }
    }

    @Nullable
    final T blockingGet()
    {
        if ( Schedulers.isInNonBlockingThread() )
        {
            throw new IllegalStateException(
                    "block()/blockFirst()/blockLast() are blocking, which is not supported in thread " + Thread.currentThread().getName() );
        }
        else
        {
            if ( this.getCount() != 0L )
            {
                try
                {
                    this.await();
                }
                catch ( InterruptedException var3 )
                {
                    this.dispose();
                    throw Exceptions.propagate( var3 );
                }
            }

            Throwable e = this.error;
            if ( e != null )
            {
                RuntimeException re = Exceptions.propagate( e );
                re.addSuppressed( new Exception( "#block terminated with an error" ) );
                throw re;
            }
            else
            {
                return this.value;
            }
        }
    }

    @Nullable
    final T blockingGet( long timeout, TimeUnit unit )
    {
        if ( Schedulers.isInNonBlockingThread() )
        {
            throw new IllegalStateException(
                    "block()/blockFirst()/blockLast() are blocking, which is not supported in thread " + Thread.currentThread().getName() );
        }
        else
        {
            RuntimeException re;
            if ( this.getCount() != 0L )
            {
                try
                {
                    if ( !this.await( timeout, unit ) )
                    {
                        this.dispose();
                        throw new IllegalStateException( "Timeout on blocking read for " + timeout + " " + unit );
                    }
                }
                catch ( InterruptedException var6 )
                {
                    this.dispose();
                    re = Exceptions.propagate( var6 );
                    re.addSuppressed( new Exception( "#block has been interrupted" ) );
                    throw re;
                }
            }

            Throwable e = this.error;
            if ( e != null )
            {
                re = Exceptions.propagate( e );
                re.addSuppressed( new Exception( "#block terminated with an error" ) );
                throw re;
            }
            else
            {
                return this.value;
            }
        }
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.TERMINATED )
        {
            return this.getCount() == 0L;
        }
        else if ( key == Scannable.Attr.PARENT )
        {
            return this.s;
        }
        else if ( key == Scannable.Attr.CANCELLED )
        {
            return this.cancelled;
        }
        else if ( key == Scannable.Attr.ERROR )
        {
            return this.error;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? Integer.MAX_VALUE : null;
        }
    }

    public boolean isDisposed()
    {
        return this.cancelled || this.getCount() == 0L;
    }
}
