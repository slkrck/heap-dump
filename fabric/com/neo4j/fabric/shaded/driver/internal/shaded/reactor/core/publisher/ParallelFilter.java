package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Predicate;

final class ParallelFilter<T> extends ParallelFlux<T> implements Scannable
{
    final ParallelFlux<T> source;
    final Predicate<? super T> predicate;

    ParallelFilter( ParallelFlux<T> source, Predicate<? super T> predicate )
    {
        this.source = source;
        this.predicate = predicate;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<? super T>[] parents = new CoreSubscriber[n];
            boolean conditional = subscribers[0] instanceof Fuseable.ConditionalSubscriber;

            for ( int i = 0; i < n; ++i )
            {
                if ( conditional )
                {
                    parents[i] = new FluxFilter.FilterConditionalSubscriber( (Fuseable.ConditionalSubscriber) subscribers[i], this.predicate );
                }
                else
                {
                    parents[i] = new FluxFilter.FilterSubscriber( subscribers[i], this.predicate );
                }
            }

            this.source.subscribe( parents );
        }
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }
}
