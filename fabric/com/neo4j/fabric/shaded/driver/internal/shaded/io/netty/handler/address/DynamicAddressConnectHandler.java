package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.address;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOutboundHandlerAdapter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;

import java.net.SocketAddress;

public abstract class DynamicAddressConnectHandler extends ChannelOutboundHandlerAdapter
{
    public final void connect( ChannelHandlerContext ctx, SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise )
    {
        SocketAddress remote;
        SocketAddress local;
        try
        {
            remote = this.remoteAddress( remoteAddress, localAddress );
            local = this.localAddress( remoteAddress, localAddress );
        }
        catch ( Exception var8 )
        {
            promise.setFailure( var8 );
            return;
        }

        ctx.connect( remote, local, promise ).addListener( new ChannelFutureListener()
        {
            public void operationComplete( ChannelFuture future )
            {
                if ( future.isSuccess() )
                {
                    future.channel().pipeline().remove( (ChannelHandler) DynamicAddressConnectHandler.this );
                }
            }
        } );
    }

    protected SocketAddress localAddress( SocketAddress remoteAddress, SocketAddress localAddress ) throws Exception
    {
        return localAddress;
    }

    protected SocketAddress remoteAddress( SocketAddress remoteAddress, SocketAddress localAddress ) throws Exception
    {
        return remoteAddress;
    }
}
