package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.jvm.ExecutorServiceMetrics;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.core.instrument.internal.TimedExecutorService;
import io.micrometer.core.instrument.search.Search;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

final class SchedulerMetricDecorator implements BiFunction<Scheduler,ScheduledExecutorService,ScheduledExecutorService>, Disposable
{
    static final String TAG_SCHEDULER_ID = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.scheduler.id";
    static final String METRICS_DECORATOR_KEY = "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.metrics.decorator";
    final WeakHashMap<Scheduler,String> seenSchedulers = new WeakHashMap();
    final Map<String,AtomicInteger> schedulerDifferentiator = new HashMap();
    final WeakHashMap<Scheduler,AtomicInteger> executorDifferentiator = new WeakHashMap();

    public synchronized ScheduledExecutorService apply( Scheduler scheduler, ScheduledExecutorService service )
    {
        String schedulerName = (String) Scannable.from( scheduler ).scanOrDefault( Scannable.Attr.NAME, scheduler.getClass().getName() );
        String schedulerId = (String) this.seenSchedulers.computeIfAbsent( scheduler, ( s ) -> {
            int schedulerDifferentiator = ((AtomicInteger) this.schedulerDifferentiator.computeIfAbsent( schedulerName, ( k ) -> {
                return new AtomicInteger( 0 );
            } )).getAndIncrement();
            return schedulerDifferentiator == 0 ? schedulerName : schedulerName + "#" + schedulerDifferentiator;
        } );
        String executorId = schedulerId + "-" + ((AtomicInteger) this.executorDifferentiator.computeIfAbsent( scheduler, ( key ) -> {
            return new AtomicInteger( 0 );
        } )).getAndIncrement();
        Tags tags = Tags.of( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.scheduler.id", schedulerId );
        ExecutorServiceMetrics.monitor( Metrics.globalRegistry, service, executorId, tags );
        return new SchedulerMetricDecorator.TimedScheduledExecutorService( Metrics.globalRegistry, service, executorId, tags );
    }

    public void dispose()
    {
        Collection var10000 =
                Search.in( Metrics.globalRegistry ).tagKeys( new String[]{"com.neo4j.fabric.shaded.driver.internal.shaded.reactor.scheduler.id"} ).meters();
        CompositeMeterRegistry var10001 = Metrics.globalRegistry;
        var10000.forEach( var10001::remove );
        this.seenSchedulers.clear();
        this.schedulerDifferentiator.clear();
        this.executorDifferentiator.clear();
    }

    static final class TimedScheduledExecutorService extends TimedExecutorService implements ScheduledExecutorService
    {
        final ScheduledExecutorService delegate;

        public TimedScheduledExecutorService( MeterRegistry registry, ScheduledExecutorService delegate, String executorServiceName, Iterable<Tag> tags )
        {
            super( registry, delegate, executorServiceName, tags );
            this.delegate = delegate;
        }

        public ScheduledFuture<?> schedule( Runnable command, long delay, TimeUnit unit )
        {
            return this.delegate.schedule( command, delay, unit );
        }

        public <V> ScheduledFuture<V> schedule( Callable<V> callable, long delay, TimeUnit unit )
        {
            return this.delegate.schedule( callable, delay, unit );
        }

        public ScheduledFuture<?> scheduleAtFixedRate( Runnable command, long initialDelay, long period, TimeUnit unit )
        {
            return this.delegate.scheduleAtFixedRate( command, initialDelay, period, unit );
        }

        public ScheduledFuture<?> scheduleWithFixedDelay( Runnable command, long initialDelay, long delay, TimeUnit unit )
        {
            return this.delegate.scheduleWithFixedDelay( command, initialDelay, delay, unit );
        }
    }
}
