package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.Recycler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCountUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Promise;

public final class PendingWrite
{
    private static final Recycler<PendingWrite> RECYCLER = new Recycler<PendingWrite>()
    {
        protected PendingWrite newObject( Recycler.Handle<PendingWrite> handle )
        {
            return new PendingWrite( handle );
        }
    };
    private final Recycler.Handle<PendingWrite> handle;
    private Object msg;
    private Promise<Void> promise;

    private PendingWrite( Recycler.Handle<PendingWrite> handle )
    {
        this.handle = handle;
    }

    public static PendingWrite newInstance( Object msg, Promise<Void> promise )
    {
        PendingWrite pending = (PendingWrite) RECYCLER.get();
        pending.msg = msg;
        pending.promise = promise;
        return pending;
    }

    public boolean recycle()
    {
        this.msg = null;
        this.promise = null;
        this.handle.recycle( this );
        return true;
    }

    public boolean failAndRecycle( Throwable cause )
    {
        ReferenceCountUtil.release( this.msg );
        if ( this.promise != null )
        {
            this.promise.setFailure( cause );
        }

        return this.recycle();
    }

    public boolean successAndRecycle()
    {
        if ( this.promise != null )
        {
            this.promise.setSuccess( (Object) null );
        }

        return this.recycle();
    }

    public Object msg()
    {
        return this.msg;
    }

    public Promise<Void> promise()
    {
        return this.promise;
    }

    public Promise<Void> recycleAndGet()
    {
        Promise<Void> promise = this.promise;
        this.recycle();
        return promise;
    }
}
