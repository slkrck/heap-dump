package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

abstract class InternalMonoOperator<I, O> extends MonoOperator<I,O> implements Scannable, OptimizableOperator<O,I>
{
    @Nullable
    final OptimizableOperator<?,I> optimizableOperator;

    protected InternalMonoOperator( Mono<? extends I> source )
    {
        super( source );
        this.optimizableOperator = source instanceof OptimizableOperator ? (OptimizableOperator) source : null;
    }

    public final void subscribe( CoreSubscriber<? super O> subscriber )
    {
        Object operator = this;

        while ( true )
        {
            subscriber = ((OptimizableOperator) operator).subscribeOrReturn( subscriber );
            if ( subscriber == null )
            {
                return;
            }

            OptimizableOperator newSource = ((OptimizableOperator) operator).nextOptimizableSource();
            if ( newSource == null )
            {
                ((OptimizableOperator) operator).source().subscribe( subscriber );
                return;
            }

            operator = newSource;
        }
    }

    @Nullable
    public abstract CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super O> var1 );

    public final CorePublisher<? extends I> source()
    {
        return this.source;
    }

    public final OptimizableOperator<?,? extends I> nextOptimizableSource()
    {
        return this.optimizableOperator;
    }
}
