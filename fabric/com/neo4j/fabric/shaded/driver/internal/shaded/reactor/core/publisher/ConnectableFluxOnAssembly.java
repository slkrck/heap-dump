package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Consumer;

final class ConnectableFluxOnAssembly<T> extends InternalConnectableFluxOperator<T,T> implements Fuseable, AssemblyOp, Scannable
{
    final FluxOnAssembly.AssemblySnapshot stacktrace;

    ConnectableFluxOnAssembly( ConnectableFlux<T> source, FluxOnAssembly.AssemblySnapshot stacktrace )
    {
        super( source );
        this.stacktrace = stacktrace;
    }

    public final CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return FluxOnAssembly.wrapSubscriber( actual, this.source, this.stacktrace );
    }

    public void connect( Consumer<? super Disposable> cancelSupport )
    {
        this.source.connect( cancelSupport );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.ACTUAL_METADATA ? !this.stacktrace.checkpointed : null;
        }
    }

    public String stepName()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }

    public String toString()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }
}
