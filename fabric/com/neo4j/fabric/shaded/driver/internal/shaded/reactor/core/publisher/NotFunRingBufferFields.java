package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import java.util.function.Supplier;

abstract class NotFunRingBufferFields<E> extends RingBuffer<E>
{
    final int bufferSize;
    final RingBufferProducer sequenceProducer;
    private final long indexMask;
    private final Object[] entries;

    NotFunRingBufferFields( Supplier<E> eventFactory, RingBufferProducer sequenceProducer )
    {
        this.sequenceProducer = sequenceProducer;
        this.bufferSize = sequenceProducer.getBufferSize();
        this.indexMask = (long) (this.bufferSize - 1);
        this.entries = new Object[sequenceProducer.getBufferSize()];
        this.fill( eventFactory );
    }

    private void fill( Supplier<E> eventFactory )
    {
        for ( int i = 0; i < this.bufferSize; ++i )
        {
            this.entries[i] = eventFactory.get();
        }
    }

    final E elementAt( long sequence )
    {
        return this.entries[(int) (sequence & this.indexMask)];
    }
}
