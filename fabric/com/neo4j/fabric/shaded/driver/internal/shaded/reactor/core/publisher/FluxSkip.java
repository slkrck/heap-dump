package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;
import org.reactivestreams.Subscription;

final class FluxSkip<T> extends InternalFluxOperator<T,T>
{
    final long n;

    FluxSkip( Flux<? extends T> source, long n )
    {
        super( source );
        if ( n < 0L )
        {
            throw new IllegalArgumentException( "n >= 0 required but it was " + n );
        }
        else
        {
            this.n = n;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxSkip.SkipSubscriber( actual, this.n );
    }

    static final class SkipSubscriber<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        long remaining;
        Subscription s;

        SkipSubscriber( CoreSubscriber<? super T> actual, long n )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.remaining = n;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                long n = this.remaining;
                this.actual.onSubscribe( this );
                s.request( n );
            }
        }

        public void onNext( T t )
        {
            long r = this.remaining;
            if ( r == 0L )
            {
                this.actual.onNext( t );
            }
            else
            {
                Operators.onDiscard( t, this.ctx );
                this.remaining = r - 1L;
            }
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
