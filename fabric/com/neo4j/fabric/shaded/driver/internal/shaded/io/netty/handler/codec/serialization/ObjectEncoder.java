package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.serialization;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufOutputStream;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToByteEncoder;

import java.io.Serializable;

@ChannelHandler.Sharable
public class ObjectEncoder extends MessageToByteEncoder<Serializable>
{
    private static final byte[] LENGTH_PLACEHOLDER = new byte[4];

    protected void encode( ChannelHandlerContext ctx, Serializable msg, ByteBuf out ) throws Exception
    {
        int startIdx = out.writerIndex();
        ByteBufOutputStream bout = new ByteBufOutputStream( out );
        CompactObjectOutputStream oout = null;

        try
        {
            bout.write( LENGTH_PLACEHOLDER );
            oout = new CompactObjectOutputStream( bout );
            oout.writeObject( msg );
            oout.flush();
        }
        finally
        {
            if ( oout != null )
            {
                oout.close();
            }
            else
            {
                bout.close();
            }
        }

        int endIdx = out.writerIndex();
        out.setInt( startIdx, endIdx - startIdx - 4 );
    }
}
