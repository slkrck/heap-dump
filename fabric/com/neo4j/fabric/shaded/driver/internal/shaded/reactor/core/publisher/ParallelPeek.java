package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Consumer;
import java.util.function.LongConsumer;

import org.reactivestreams.Subscription;

final class ParallelPeek<T> extends ParallelFlux<T> implements SignalPeek<T>
{
    final ParallelFlux<T> source;
    final Consumer<? super T> onNext;
    final Consumer<? super T> onAfterNext;
    final Consumer<? super Throwable> onError;
    final Runnable onComplete;
    final Runnable onAfterTerminated;
    final Consumer<? super Subscription> onSubscribe;
    final LongConsumer onRequest;
    final Runnable onCancel;

    ParallelPeek( ParallelFlux<T> source, @Nullable Consumer<? super T> onNext, @Nullable Consumer<? super T> onAfterNext,
            @Nullable Consumer<? super Throwable> onError, @Nullable Runnable onComplete, @Nullable Runnable onAfterTerminated,
            @Nullable Consumer<? super Subscription> onSubscribe, @Nullable LongConsumer onRequest, @Nullable Runnable onCancel )
    {
        this.source = source;
        this.onNext = onNext;
        this.onAfterNext = onAfterNext;
        this.onError = onError;
        this.onComplete = onComplete;
        this.onAfterTerminated = onAfterTerminated;
        this.onSubscribe = onSubscribe;
        this.onRequest = onRequest;
        this.onCancel = onCancel;
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<? super T>[] parents = new CoreSubscriber[n];
            boolean conditional = subscribers[0] instanceof Fuseable.ConditionalSubscriber;

            for ( int i = 0; i < n; ++i )
            {
                if ( conditional )
                {
                    parents[i] = new FluxPeekFuseable.PeekConditionalSubscriber( (Fuseable.ConditionalSubscriber) subscribers[i], this );
                }
                else
                {
                    parents[i] = new FluxPeek.PeekSubscriber( subscribers[i], this );
                }
            }

            this.source.subscribe( parents );
        }
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    @Nullable
    public Consumer<? super Subscription> onSubscribeCall()
    {
        return this.onSubscribe;
    }

    @Nullable
    public Consumer<? super T> onNextCall()
    {
        return this.onNext;
    }

    @Nullable
    public Consumer<? super Throwable> onErrorCall()
    {
        return this.onError;
    }

    @Nullable
    public Runnable onCompleteCall()
    {
        return this.onComplete;
    }

    public Runnable onAfterTerminateCall()
    {
        return this.onAfterTerminated;
    }

    @Nullable
    public LongConsumer onRequestCall()
    {
        return this.onRequest;
    }

    @Nullable
    public Runnable onCancelCall()
    {
        return this.onCancel;
    }

    @Nullable
    public Consumer<? super T> onAfterNextCall()
    {
        return this.onAfterNext;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }
}
