package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.GenericFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Promise;

public interface ChannelPromise extends ChannelFuture, Promise<Void>
{
    Channel channel();

    ChannelPromise setSuccess( Void var1 );

    ChannelPromise setSuccess();

    boolean trySuccess();

    ChannelPromise setFailure( Throwable var1 );

    ChannelPromise addListener( GenericFutureListener<? extends Future<? super Void>> var1 );

    ChannelPromise addListeners( GenericFutureListener<? extends Future<? super Void>>... var1 );

    ChannelPromise removeListener( GenericFutureListener<? extends Future<? super Void>> var1 );

    ChannelPromise removeListeners( GenericFutureListener<? extends Future<? super Void>>... var1 );

    ChannelPromise sync() throws InterruptedException;

    ChannelPromise syncUninterruptibly();

    ChannelPromise await() throws InterruptedException;

    ChannelPromise awaitUninterruptibly();

    ChannelPromise unvoid();
}
