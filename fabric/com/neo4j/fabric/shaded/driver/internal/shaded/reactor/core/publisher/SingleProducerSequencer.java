package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

import java.util.concurrent.locks.LockSupport;

final class SingleProducerSequencer extends SingleProducerSequencerFields
{
    protected long p1;
    protected long p2;
    protected long p3;
    protected long p4;
    protected long p5;
    protected long p6;
    protected long p7;

    SingleProducerSequencer( int bufferSize, WaitStrategy waitStrategy, @Nullable Runnable spinObserver )
    {
        super( bufferSize, waitStrategy, spinObserver );
    }

    long next()
    {
        return this.next( 1 );
    }

    long next( int n )
    {
        long nextValue = this.nextValue;
        long nextSequence = nextValue + (long) n;
        long wrapPoint = nextSequence - (long) this.bufferSize;
        long cachedGatingSequence = this.cachedValue;
        if ( wrapPoint > cachedGatingSequence || cachedGatingSequence > nextValue )
        {
            while ( true )
            {
                long minSequence;
                if ( wrapPoint <= (minSequence = RingBuffer.getMinimumSequence( this.gatingSequences, nextValue )) )
                {
                    this.cachedValue = minSequence;
                    break;
                }

                if ( this.spinObserver != null )
                {
                    this.spinObserver.run();
                }

                LockSupport.parkNanos( 1L );
            }
        }

        this.nextValue = nextSequence;
        return nextSequence;
    }

    public long getPending()
    {
        long nextValue = this.nextValue;
        long consumed = RingBuffer.getMinimumSequence( this.gatingSequences, nextValue );
        return nextValue - consumed;
    }

    void publish( long sequence )
    {
        this.cursor.set( sequence );
        this.waitStrategy.signalAllWhenBlocking();
    }

    long getHighestPublishedSequence( long lowerBound, long availableSequence )
    {
        return availableSequence;
    }
}
