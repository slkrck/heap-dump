package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.oio;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.AbstractChannel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoop;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ThreadPerChannelEventLoop;

import java.net.SocketAddress;

/**
 * @deprecated
 */
@Deprecated
public abstract class AbstractOioChannel extends AbstractChannel
{
    protected static final int SO_TIMEOUT = 1000;
    private final Runnable readTask = new Runnable()
    {
        public void run()
        {
            AbstractOioChannel.this.doRead();
        }
    };
    boolean readPending;
    private final Runnable clearReadPendingRunnable = new Runnable()
    {
        public void run()
        {
            AbstractOioChannel.this.readPending = false;
        }
    };

    protected AbstractOioChannel( Channel parent )
    {
        super( parent );
    }

    protected AbstractChannel.AbstractUnsafe newUnsafe()
    {
        return new AbstractOioChannel.DefaultOioUnsafe();
    }

    protected boolean isCompatible( EventLoop loop )
    {
        return loop instanceof ThreadPerChannelEventLoop;
    }

    protected abstract void doConnect( SocketAddress var1, SocketAddress var2 ) throws Exception;

    protected void doBeginRead() throws Exception
    {
        if ( !this.readPending )
        {
            this.readPending = true;
            this.eventLoop().execute( this.readTask );
        }
    }

    protected abstract void doRead();

    /**
     * @deprecated
     */
    @Deprecated
    protected boolean isReadPending()
    {
        return this.readPending;
    }

    /**
     * @deprecated
     */
    @Deprecated
    protected void setReadPending( final boolean readPending )
    {
        if ( this.isRegistered() )
        {
            EventLoop eventLoop = this.eventLoop();
            if ( eventLoop.inEventLoop() )
            {
                this.readPending = readPending;
            }
            else
            {
                eventLoop.execute( new Runnable()
                {
                    public void run()
                    {
                        AbstractOioChannel.this.readPending = readPending;
                    }
                } );
            }
        }
        else
        {
            this.readPending = readPending;
        }
    }

    protected final void clearReadPending()
    {
        if ( this.isRegistered() )
        {
            EventLoop eventLoop = this.eventLoop();
            if ( eventLoop.inEventLoop() )
            {
                this.readPending = false;
            }
            else
            {
                eventLoop.execute( this.clearReadPendingRunnable );
            }
        }
        else
        {
            this.readPending = false;
        }
    }

    private final class DefaultOioUnsafe extends AbstractChannel.AbstractUnsafe
    {
        private DefaultOioUnsafe()
        {
            super();
        }

        public void connect( SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise )
        {
            if ( promise.setUncancellable() && this.ensureOpen( promise ) )
            {
                try
                {
                    boolean wasActive = AbstractOioChannel.this.isActive();
                    AbstractOioChannel.this.doConnect( remoteAddress, localAddress );
                    boolean active = AbstractOioChannel.this.isActive();
                    this.safeSetSuccess( promise );
                    if ( !wasActive && active )
                    {
                        AbstractOioChannel.this.pipeline().fireChannelActive();
                    }
                }
                catch ( Throwable var6 )
                {
                    this.safeSetFailure( promise, this.annotateConnectException( var6, remoteAddress ) );
                    this.closeIfClosed();
                }
            }
        }
    }
}
