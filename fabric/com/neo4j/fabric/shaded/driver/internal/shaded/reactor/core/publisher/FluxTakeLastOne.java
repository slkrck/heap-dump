package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

final class FluxTakeLastOne<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    FluxTakeLastOne( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoTakeLastOne.TakeLastOneSubscriber( actual, (Object) null, false );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }
}
