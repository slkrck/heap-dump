package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.base64.Base64;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.base64.Base64Dialect;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.NetUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.EmptyArrays;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLHandshakeException;

final class SslUtils
{
    static final Set<String> TLSV13_CIPHERS = Collections.unmodifiableSet( new LinkedHashSet(
            Arrays.asList( "TLS_AES_256_GCM_SHA384", "TLS_CHACHA20_POLY1305_SHA256", "TLS_AES_128_GCM_SHA256", "TLS_AES_128_CCM_8_SHA256",
                    "TLS_AES_128_CCM_SHA256" ) ) );
    static final String PROTOCOL_SSL_V2_HELLO = "SSLv2Hello";
    static final String PROTOCOL_SSL_V2 = "SSLv2";
    static final String PROTOCOL_SSL_V3 = "SSLv3";
    static final String PROTOCOL_TLS_V1 = "TLSv1";
    static final String PROTOCOL_TLS_V1_1 = "TLSv1.1";
    static final String PROTOCOL_TLS_V1_2 = "TLSv1.2";
    static final String PROTOCOL_TLS_V1_3 = "TLSv1.3";
    static final String INVALID_CIPHER = "SSL_NULL_WITH_NULL_NULL";
    static final int SSL_CONTENT_TYPE_CHANGE_CIPHER_SPEC = 20;
    static final int SSL_CONTENT_TYPE_ALERT = 21;
    static final int SSL_CONTENT_TYPE_HANDSHAKE = 22;
    static final int SSL_CONTENT_TYPE_APPLICATION_DATA = 23;
    static final int SSL_CONTENT_TYPE_EXTENSION_HEARTBEAT = 24;
    static final int SSL_RECORD_HEADER_LENGTH = 5;
    static final int NOT_ENOUGH_DATA = -1;
    static final int NOT_ENCRYPTED = -2;
    static final String[] DEFAULT_CIPHER_SUITES;
    static final String[] DEFAULT_TLSV13_CIPHER_SUITES;
    static final String[] TLSV13_CIPHER_SUITES = new String[]{"TLS_AES_128_GCM_SHA256", "TLS_AES_256_GCM_SHA384"};

    static
    {
        if ( PlatformDependent.javaVersion() >= 11 )
        {
            DEFAULT_TLSV13_CIPHER_SUITES = TLSV13_CIPHER_SUITES;
        }
        else
        {
            DEFAULT_TLSV13_CIPHER_SUITES = EmptyArrays.EMPTY_STRINGS;
        }

        List<String> defaultCiphers = new ArrayList();
        defaultCiphers.add( "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384" );
        defaultCiphers.add( "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256" );
        defaultCiphers.add( "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256" );
        defaultCiphers.add( "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384" );
        defaultCiphers.add( "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA" );
        defaultCiphers.add( "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA" );
        defaultCiphers.add( "TLS_RSA_WITH_AES_128_GCM_SHA256" );
        defaultCiphers.add( "TLS_RSA_WITH_AES_128_CBC_SHA" );
        defaultCiphers.add( "TLS_RSA_WITH_AES_256_CBC_SHA" );
        Collections.addAll( defaultCiphers, DEFAULT_TLSV13_CIPHER_SUITES );
        DEFAULT_CIPHER_SUITES = (String[]) defaultCiphers.toArray( new String[0] );
    }

    private SslUtils()
    {
    }

    static void addIfSupported( Set<String> supported, List<String> enabled, String... names )
    {
        String[] var3 = names;
        int var4 = names.length;

        for ( int var5 = 0; var5 < var4; ++var5 )
        {
            String n = var3[var5];
            if ( supported.contains( n ) )
            {
                enabled.add( n );
            }
        }
    }

    static void useFallbackCiphersIfDefaultIsEmpty( List<String> defaultCiphers, Iterable<String> fallbackCiphers )
    {
        if ( defaultCiphers.isEmpty() )
        {
            Iterator var2 = fallbackCiphers.iterator();

            while ( var2.hasNext() )
            {
                String cipher = (String) var2.next();
                if ( !cipher.startsWith( "SSL_" ) && !cipher.contains( "_RC4_" ) )
                {
                    defaultCiphers.add( cipher );
                }
            }
        }
    }

    static void useFallbackCiphersIfDefaultIsEmpty( List<String> defaultCiphers, String... fallbackCiphers )
    {
        useFallbackCiphersIfDefaultIsEmpty( defaultCiphers, (Iterable) Arrays.asList( fallbackCiphers ) );
    }

    static SSLHandshakeException toSSLHandshakeException( Throwable e )
    {
        return e instanceof SSLHandshakeException ? (SSLHandshakeException) e
                                                  : (SSLHandshakeException) (new SSLHandshakeException( e.getMessage() )).initCause( e );
    }

    static int getEncryptedPacketLength( ByteBuf buffer, int offset )
    {
        int packetLength = 0;
        boolean tls;
        switch ( buffer.getUnsignedByte( offset ) )
        {
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
            tls = true;
            break;
        default:
            tls = false;
        }

        if ( tls )
        {
            int majorVersion = buffer.getUnsignedByte( offset + 1 );
            if ( majorVersion == 3 )
            {
                packetLength = unsignedShortBE( buffer, offset + 3 ) + 5;
                if ( packetLength <= 5 )
                {
                    tls = false;
                }
            }
            else
            {
                tls = false;
            }
        }

        if ( !tls )
        {
            int headerLength = (buffer.getUnsignedByte( offset ) & 128) != 0 ? 2 : 3;
            int majorVersion = buffer.getUnsignedByte( offset + headerLength + 1 );
            if ( majorVersion != 2 && majorVersion != 3 )
            {
                return -2;
            }

            packetLength = headerLength == 2 ? (shortBE( buffer, offset ) & 32767) + 2 : (shortBE( buffer, offset ) & 16383) + 3;
            if ( packetLength <= headerLength )
            {
                return -1;
            }
        }

        return packetLength;
    }

    private static int unsignedShortBE( ByteBuf buffer, int offset )
    {
        return buffer.order() == ByteOrder.BIG_ENDIAN ? buffer.getUnsignedShort( offset ) : buffer.getUnsignedShortLE( offset );
    }

    private static short shortBE( ByteBuf buffer, int offset )
    {
        return buffer.order() == ByteOrder.BIG_ENDIAN ? buffer.getShort( offset ) : buffer.getShortLE( offset );
    }

    private static short unsignedByte( byte b )
    {
        return (short) (b & 255);
    }

    private static int unsignedShortBE( ByteBuffer buffer, int offset )
    {
        return shortBE( buffer, offset ) & '\uffff';
    }

    private static short shortBE( ByteBuffer buffer, int offset )
    {
        return buffer.order() == ByteOrder.BIG_ENDIAN ? buffer.getShort( offset ) : ByteBufUtil.swapShort( buffer.getShort( offset ) );
    }

    static int getEncryptedPacketLength( ByteBuffer[] buffers, int offset )
    {
        ByteBuffer buffer = buffers[offset];
        if ( buffer.remaining() >= 5 )
        {
            return getEncryptedPacketLength( buffer );
        }
        else
        {
            ByteBuffer tmp = ByteBuffer.allocate( 5 );

            do
            {
                buffer = buffers[offset++].duplicate();
                if ( buffer.remaining() > tmp.remaining() )
                {
                    buffer.limit( buffer.position() + tmp.remaining() );
                }

                tmp.put( buffer );
            }
            while ( tmp.hasRemaining() );

            tmp.flip();
            return getEncryptedPacketLength( tmp );
        }
    }

    private static int getEncryptedPacketLength( ByteBuffer buffer )
    {
        int packetLength = 0;
        int pos = buffer.position();
        boolean tls;
        switch ( unsignedByte( buffer.get( pos ) ) )
        {
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
            tls = true;
            break;
        default:
            tls = false;
        }

        if ( tls )
        {
            int majorVersion = unsignedByte( buffer.get( pos + 1 ) );
            if ( majorVersion == 3 )
            {
                packetLength = unsignedShortBE( buffer, pos + 3 ) + 5;
                if ( packetLength <= 5 )
                {
                    tls = false;
                }
            }
            else
            {
                tls = false;
            }
        }

        if ( !tls )
        {
            int headerLength = (unsignedByte( buffer.get( pos ) ) & 128) != 0 ? 2 : 3;
            int majorVersion = unsignedByte( buffer.get( pos + headerLength + 1 ) );
            if ( majorVersion != 2 && majorVersion != 3 )
            {
                return -2;
            }

            packetLength = headerLength == 2 ? (shortBE( buffer, pos ) & 32767) + 2 : (shortBE( buffer, pos ) & 16383) + 3;
            if ( packetLength <= headerLength )
            {
                return -1;
            }
        }

        return packetLength;
    }

    static void handleHandshakeFailure( ChannelHandlerContext ctx, Throwable cause, boolean notify )
    {
        ctx.flush();
        if ( notify )
        {
            ctx.fireUserEventTriggered( new SslHandshakeCompletionEvent( cause ) );
        }

        ctx.close();
    }

    static void zeroout( ByteBuf buffer )
    {
        if ( !buffer.isReadOnly() )
        {
            buffer.setZero( 0, buffer.capacity() );
        }
    }

    static void zerooutAndRelease( ByteBuf buffer )
    {
        zeroout( buffer );
        buffer.release();
    }

    static ByteBuf toBase64( ByteBufAllocator allocator, ByteBuf src )
    {
        ByteBuf dst = Base64.encode( src, src.readerIndex(), src.readableBytes(), true, Base64Dialect.STANDARD, allocator );
        src.readerIndex( src.writerIndex() );
        return dst;
    }

    static boolean isValidHostNameForSNI( String hostname )
    {
        return hostname != null && hostname.indexOf( 46 ) > 0 && !hostname.endsWith( "." ) && !NetUtil.isValidIpV4Address( hostname ) &&
                !NetUtil.isValidIpV6Address( hostname );
    }

    static boolean isTLSv13Cipher( String cipher )
    {
        return TLSV13_CIPHERS.contains( cipher );
    }
}
