package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ipfilter;

public enum IpFilterRuleType
{
    ACCEPT,
    REJECT;
}
