package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

final class ImmediateScheduler implements Scheduler, Scannable
{
    static final Disposable FINISHED = Disposables.disposed();
    private static final ImmediateScheduler INSTANCE = new ImmediateScheduler();

    private ImmediateScheduler()
    {
    }

    public static Scheduler instance()
    {
        return INSTANCE;
    }

    public Disposable schedule( Runnable task )
    {
        task.run();
        return FINISHED;
    }

    public void dispose()
    {
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            return key == Scannable.Attr.NAME ? "immediate" : null;
        }
        else
        {
            return this.isDisposed();
        }
    }

    public Scheduler.Worker createWorker()
    {
        return new ImmediateScheduler.ImmediateSchedulerWorker();
    }

    static final class ImmediateSchedulerWorker implements Scheduler.Worker, Scannable
    {
        volatile boolean shutdown;

        public Disposable schedule( Runnable task )
        {
            if ( this.shutdown )
            {
                throw Exceptions.failWithRejected();
            }
            else
            {
                task.run();
                return ImmediateScheduler.FINISHED;
            }
        }

        public void dispose()
        {
            this.shutdown = true;
        }

        public boolean isDisposed()
        {
            return this.shutdown;
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                return key == Scannable.Attr.NAME ? "immediate.worker" : null;
            }
            else
            {
                return this.shutdown;
            }
        }
    }
}
