package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

/**
 * @deprecated
 */
@Deprecated
public interface OpenSslApplicationProtocolNegotiator extends ApplicationProtocolNegotiator
{
    ApplicationProtocolConfig.Protocol protocol();

    ApplicationProtocolConfig.SelectorFailureBehavior selectorFailureBehavior();

    ApplicationProtocolConfig.SelectedListenerFailureBehavior selectedListenerFailureBehavior();
}
