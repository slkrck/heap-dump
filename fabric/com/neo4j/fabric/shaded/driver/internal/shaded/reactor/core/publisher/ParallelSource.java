package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class ParallelSource<T> extends ParallelFlux<T> implements Scannable
{
    final Publisher<? extends T> source;
    final int parallelism;
    final int prefetch;
    final Supplier<Queue<T>> queueSupplier;

    ParallelSource( Publisher<? extends T> source, int parallelism, int prefetch, Supplier<Queue<T>> queueSupplier )
    {
        if ( parallelism <= 0 )
        {
            throw new IllegalArgumentException( "parallelism > 0 required but it was " + parallelism );
        }
        else if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.source = source;
            this.parallelism = parallelism;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public int parallelism()
    {
        return this.parallelism;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            this.source.subscribe( new ParallelSource.ParallelSourceMain( subscribers, this.prefetch, this.queueSupplier ) );
        }
    }

    static final class ParallelSourceMain<T> implements InnerConsumer<T>
    {
        static final AtomicIntegerFieldUpdater<ParallelSource.ParallelSourceMain> WIP =
                AtomicIntegerFieldUpdater.newUpdater( ParallelSource.ParallelSourceMain.class, "wip" );
        static final AtomicIntegerFieldUpdater<ParallelSource.ParallelSourceMain> SUBSCRIBER_COUNT =
                AtomicIntegerFieldUpdater.newUpdater( ParallelSource.ParallelSourceMain.class, "subscriberCount" );
        final CoreSubscriber<? super T>[] subscribers;
        final AtomicLongArray requests;
        final long[] emissions;
        final int prefetch;
        final int limit;
        final Supplier<Queue<T>> queueSupplier;
        Subscription s;
        Queue<T> queue;
        Throwable error;
        volatile boolean done;
        int index;
        volatile boolean cancelled;
        volatile int wip;
        volatile int subscriberCount;
        int produced;
        int sourceMode;

        ParallelSourceMain( CoreSubscriber<? super T>[] subscribers, int prefetch, Supplier<Queue<T>> queueSupplier )
        {
            this.subscribers = subscribers;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
            this.limit = Operators.unboundedOrLimit( prefetch );
            this.requests = new AtomicLongArray( subscribers.length );
            this.emissions = new long[subscribers.length];
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.queue != null ? this.queue.size() : 0 : null;
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers ).map( Scannable::from );
        }

        public Context currentContext()
        {
            return this.subscribers[0].currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    Fuseable.QueueSubscription<T> qs = (Fuseable.QueueSubscription) s;
                    int m = qs.requestFusion( 7 );
                    if ( m == 1 )
                    {
                        this.sourceMode = m;
                        this.queue = qs;
                        this.done = true;
                        this.setupSubscribers();
                        this.drain();
                        return;
                    }

                    if ( m == 2 )
                    {
                        this.sourceMode = m;
                        this.queue = qs;
                        this.setupSubscribers();
                        s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
                        return;
                    }
                }

                this.queue = (Queue) this.queueSupplier.get();
                this.setupSubscribers();
                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        void setupSubscribers()
        {
            int m = this.subscribers.length;

            for ( int i = 0; i < m; ++i )
            {
                if ( this.cancelled )
                {
                    return;
                }

                SUBSCRIBER_COUNT.lazySet( this, i + 1 );
                this.subscribers[i].onSubscribe( new ParallelSource.ParallelSourceMain.ParallelSourceInner( this, i, m ) );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.currentContext() );
            }
            else if ( this.sourceMode == 0 && !this.queue.offer( t ) )
            {
                this.onError(
                        Operators.onOperatorError( this.s, Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ),
                                t, this.currentContext() ) );
            }
            else
            {
                this.drain();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.currentContext() );
            }
            else
            {
                this.error = t;
                this.done = true;
                this.drain();
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.drain();
            }
        }

        void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    this.queue.clear();
                }
            }
        }

        void drainAsync()
        {
            int missed = 1;
            Queue<T> q = this.queue;
            CoreSubscriber<? super T>[] a = this.subscribers;
            AtomicLongArray r = this.requests;
            long[] e = this.emissions;
            int n = e.length;
            int idx = this.index;
            int consumed = this.produced;

            while ( true )
            {
                int notReady = 0;

                do
                {
                    if ( this.cancelled )
                    {
                        q.clear();
                        return;
                    }

                    boolean d = this.done;
                    int var13;
                    CoreSubscriber s;
                    CoreSubscriber[] var25;
                    int var26;
                    if ( d )
                    {
                        Throwable ex = this.error;
                        if ( ex != null )
                        {
                            q.clear();
                            var25 = a;
                            var13 = a.length;

                            for ( var26 = 0; var26 < var13; ++var26 )
                            {
                                s = var25[var26];
                                s.onError( ex );
                            }

                            return;
                        }
                    }

                    boolean empty = q.isEmpty();
                    if ( d && empty )
                    {
                        var25 = a;
                        var13 = a.length;

                        for ( var26 = 0; var26 < var13; ++var26 )
                        {
                            s = var25[var26];
                            s.onComplete();
                        }

                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    long ridx = r.get( idx );
                    long eidx = e[idx];
                    if ( ridx != eidx )
                    {
                        Object v;
                        try
                        {
                            v = q.poll();
                        }
                        catch ( Throwable var22 )
                        {
                            Throwable ex = Operators.onOperatorError( this.s, var22, a[idx].currentContext() );
                            CoreSubscriber[] var18 = a;
                            int var19 = a.length;

                            for ( int var20 = 0; var20 < var19; ++var20 )
                            {
                                Subscriber<? super T> s = var18[var20];
                                s.onError( ex );
                            }

                            return;
                        }

                        if ( v == null )
                        {
                            break;
                        }

                        a[idx].onNext( v );
                        e[idx] = eidx + 1L;
                        ++consumed;
                        int c = consumed;
                        if ( consumed == this.limit )
                        {
                            consumed = 0;
                            this.s.request( (long) c );
                        }

                        notReady = 0;
                    }
                    else
                    {
                        ++notReady;
                    }

                    ++idx;
                    if ( idx == n )
                    {
                        idx = 0;
                    }
                }
                while ( notReady != n );

                int w = this.wip;
                if ( w == missed )
                {
                    this.index = idx;
                    this.produced = consumed;
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    missed = w;
                }
            }
        }

        void drainSync()
        {
            int missed = 1;
            Queue<T> q = this.queue;
            CoreSubscriber<? super T>[] a = this.subscribers;
            AtomicLongArray r = this.requests;
            long[] e = this.emissions;
            int n = e.length;
            int idx = this.index;

            while ( true )
            {
                int notReady = 0;

                do
                {
                    if ( this.cancelled )
                    {
                        q.clear();
                        return;
                    }

                    if ( q.isEmpty() )
                    {
                        CoreSubscriber[] var21 = a;
                        int var10 = a.length;

                        for ( int var22 = 0; var22 < var10; ++var22 )
                        {
                            Subscriber<? super T> s = var21[var22];
                            s.onComplete();
                        }

                        return;
                    }

                    long ridx = r.get( idx );
                    long eidx = e[idx];
                    if ( ridx != eidx )
                    {
                        Object v;
                        int var16;
                        try
                        {
                            v = q.poll();
                        }
                        catch ( Throwable var19 )
                        {
                            Throwable ex = Operators.onOperatorError( this.s, var19, a[idx].currentContext() );
                            CoreSubscriber[] var15 = a;
                            var16 = a.length;

                            for ( int var17 = 0; var17 < var16; ++var17 )
                            {
                                Subscriber<? super T> s = var15[var17];
                                s.onError( ex );
                            }

                            return;
                        }

                        if ( v == null )
                        {
                            CoreSubscriber[] var23 = a;
                            int var24 = a.length;

                            for ( var16 = 0; var16 < var24; ++var16 )
                            {
                                Subscriber<? super T> s = var23[var16];
                                s.onComplete();
                            }

                            return;
                        }

                        a[idx].onNext( v );
                        e[idx] = eidx + 1L;
                        notReady = 0;
                    }
                    else
                    {
                        ++notReady;
                    }

                    ++idx;
                    if ( idx == n )
                    {
                        idx = 0;
                    }
                }
                while ( notReady != n );

                int w = this.wip;
                if ( w == missed )
                {
                    this.index = idx;
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    missed = w;
                }
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                if ( this.sourceMode == 1 )
                {
                    this.drainSync();
                }
                else
                {
                    this.drainAsync();
                }
            }
        }

        static final class ParallelSourceInner<T> implements InnerProducer<T>
        {
            final ParallelSource.ParallelSourceMain<T> parent;
            final int index;
            final int length;

            ParallelSourceInner( ParallelSource.ParallelSourceMain<T> parent, int index, int length )
            {
                this.index = index;
                this.length = length;
                this.parent = parent;
            }

            public CoreSubscriber<? super T> actual()
            {
                return this.parent.subscribers[this.index];
            }

            @Nullable
            public Object scanUnsafe( Scannable.Attr key )
            {
                return key == Scannable.Attr.PARENT ? this.parent : InnerProducer.super.scanUnsafe( key );
            }

            public void request( long n )
            {
                if ( Operators.validate( n ) )
                {
                    AtomicLongArray ra = this.parent.requests;

                    long r;
                    long u;
                    do
                    {
                        r = ra.get( this.index );
                        if ( r == Long.MAX_VALUE )
                        {
                            return;
                        }

                        u = Operators.addCap( r, n );
                    }
                    while ( !ra.compareAndSet( this.index, r, u ) );

                    if ( this.parent.subscriberCount == this.length )
                    {
                        this.parent.drain();
                    }
                }
            }

            public void cancel()
            {
                this.parent.cancel();
            }
        }
    }
}
