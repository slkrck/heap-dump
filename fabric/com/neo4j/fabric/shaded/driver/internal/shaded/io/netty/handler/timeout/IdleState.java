package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.timeout;

public enum IdleState
{
    READER_IDLE,
    WRITER_IDLE,
    ALL_IDLE;
}
