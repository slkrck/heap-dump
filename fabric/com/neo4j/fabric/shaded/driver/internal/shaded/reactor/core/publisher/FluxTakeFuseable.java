package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

final class FluxTakeFuseable<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    final long n;

    FluxTakeFuseable( Flux<? extends T> source, long n )
    {
        super( source );
        if ( n < 0L )
        {
            throw new IllegalArgumentException( "n >= 0 required but it was " + n );
        }
        else
        {
            this.n = n;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxTake.TakeFuseableSubscriber( actual, this.n );
    }
}
