package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

public interface SelectStrategyFactory
{
    SelectStrategy newSelectStrategy();
}
