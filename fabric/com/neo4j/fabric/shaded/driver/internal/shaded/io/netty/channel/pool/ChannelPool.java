package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.pool;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Promise;

import java.io.Closeable;

public interface ChannelPool extends Closeable
{
    Future<Channel> acquire();

    Future<Channel> acquire( Promise<Channel> var1 );

    Future<Void> release( Channel var1 );

    Future<Void> release( Channel var1, Promise<Void> var2 );

    void close();
}
