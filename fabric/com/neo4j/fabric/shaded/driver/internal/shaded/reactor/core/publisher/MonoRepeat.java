package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class MonoRepeat<T> extends FluxFromMonoOperator<T,T>
{
    final long times;

    MonoRepeat( Mono<? extends T> source, long times )
    {
        super( source );
        if ( times <= 0L )
        {
            throw new IllegalArgumentException( "times > 0 required" );
        }
        else
        {
            this.times = times;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRepeat.RepeatSubscriber<T> parent = new FluxRepeat.RepeatSubscriber( this.source, actual, this.times + 1L );
        actual.onSubscribe( parent );
        if ( !parent.isCancelled() )
        {
            parent.onComplete();
        }

        return null;
    }
}
