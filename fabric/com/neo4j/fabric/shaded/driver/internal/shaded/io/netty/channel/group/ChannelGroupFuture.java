package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.group;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.GenericFutureListener;

import java.util.Iterator;

public interface ChannelGroupFuture extends Future<Void>, Iterable<ChannelFuture>
{
    ChannelGroup group();

    ChannelFuture find( Channel var1 );

    boolean isSuccess();

    ChannelGroupException cause();

    boolean isPartialSuccess();

    boolean isPartialFailure();

    ChannelGroupFuture addListener( GenericFutureListener<? extends Future<? super Void>> var1 );

    ChannelGroupFuture addListeners( GenericFutureListener<? extends Future<? super Void>>... var1 );

    ChannelGroupFuture removeListener( GenericFutureListener<? extends Future<? super Void>> var1 );

    ChannelGroupFuture removeListeners( GenericFutureListener<? extends Future<? super Void>>... var1 );

    ChannelGroupFuture await() throws InterruptedException;

    ChannelGroupFuture awaitUninterruptibly();

    ChannelGroupFuture syncUninterruptibly();

    ChannelGroupFuture sync() throws InterruptedException;

    Iterator<ChannelFuture> iterator();
}
