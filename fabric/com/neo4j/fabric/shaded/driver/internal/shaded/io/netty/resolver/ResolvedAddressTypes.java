package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.resolver;

public enum ResolvedAddressTypes
{
    IPV4_ONLY,
    IPV6_ONLY,
    IPV4_PREFERRED,
    IPV6_PREFERRED;
}
