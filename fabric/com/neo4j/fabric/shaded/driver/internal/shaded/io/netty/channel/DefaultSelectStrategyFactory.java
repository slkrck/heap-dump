package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

public final class DefaultSelectStrategyFactory implements SelectStrategyFactory
{
    public static final SelectStrategyFactory INSTANCE = new DefaultSelectStrategyFactory();

    private DefaultSelectStrategyFactory()
    {
    }

    public SelectStrategy newSelectStrategy()
    {
        return DefaultSelectStrategy.INSTANCE;
    }
}
