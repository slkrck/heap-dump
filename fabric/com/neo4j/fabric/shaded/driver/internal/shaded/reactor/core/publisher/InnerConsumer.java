package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

interface InnerConsumer<I> extends CoreSubscriber<I>, Scannable
{
    default String stepName()
    {
        String name = this.getClass().getSimpleName();
        if ( name.contains( "@" ) && name.contains( "$" ) )
        {
            name = name.substring( 0, name.indexOf( 36 ) ).substring( name.lastIndexOf( 46 ) + 1 );
        }

        String stripped = OPERATOR_NAME_UNRELATED_WORDS_PATTERN.matcher( name ).replaceAll( "" );
        return !stripped.isEmpty() ? stripped.substring( 0, 1 ).toLowerCase() + stripped.substring( 1 ) : stripped;
    }
}
