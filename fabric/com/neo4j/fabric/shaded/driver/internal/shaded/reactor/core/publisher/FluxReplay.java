package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxReplay<T> extends ConnectableFlux<T> implements Scannable, Fuseable, OptimizableOperator<T,T>
{
    static final AtomicReferenceFieldUpdater<FluxReplay,FluxReplay.ReplaySubscriber> CONNECTION =
            AtomicReferenceFieldUpdater.newUpdater( FluxReplay.class, FluxReplay.ReplaySubscriber.class, "connection" );
    final CorePublisher<T> source;
    final int history;
    final long ttl;
    final Scheduler scheduler;
    @Nullable
    final OptimizableOperator<?,T> optimizableOperator;
    volatile FluxReplay.ReplaySubscriber<T> connection;

    FluxReplay( CorePublisher<T> source, int history, long ttl, @Nullable Scheduler scheduler )
    {
        this.source = (CorePublisher) Objects.requireNonNull( source, "source" );
        this.optimizableOperator = source instanceof OptimizableOperator ? (OptimizableOperator) source : null;
        this.history = history;
        if ( history < 0 )
        {
            throw new IllegalArgumentException( "History cannot be negative : " + history );
        }
        else if ( scheduler != null && ttl < 0L )
        {
            throw new IllegalArgumentException( "TTL cannot be negative : " + ttl );
        }
        else
        {
            this.ttl = ttl;
            this.scheduler = scheduler;
        }
    }

    public int getPrefetch()
    {
        return this.history;
    }

    FluxReplay.ReplaySubscriber<T> newState()
    {
        if ( this.scheduler != null )
        {
            return new FluxReplay.ReplaySubscriber( new FluxReplay.SizeAndTimeBoundReplayBuffer( this.history, this.ttl, this.scheduler ), this );
        }
        else
        {
            return this.history != Integer.MAX_VALUE ? new FluxReplay.ReplaySubscriber( new FluxReplay.SizeBoundReplayBuffer( this.history ), this )
                                                     : new FluxReplay.ReplaySubscriber( new FluxReplay.UnboundedReplayBuffer( Queues.SMALL_BUFFER_SIZE ),
                                                             this );
        }
    }

    public void connect( Consumer<? super Disposable> cancelSupport )
    {
        while ( true )
        {
            FluxReplay.ReplaySubscriber<T> s = this.connection;
            if ( s == null )
            {
                FluxReplay.ReplaySubscriber<T> u = this.newState();
                if ( !CONNECTION.compareAndSet( this, (Object) null, u ) )
                {
                    continue;
                }

                s = u;
            }

            boolean doConnect = s.tryConnect();
            cancelSupport.accept( s );
            if ( doConnect )
            {
                this.source.subscribe( s );
            }

            return;
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        CoreSubscriber nextSubscriber = this.subscribeOrReturn( actual );
        if ( nextSubscriber != null )
        {
            this.source.subscribe( nextSubscriber );
        }
    }

    public final CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        while ( true )
        {
            FluxReplay.ReplaySubscriber<T> c = this.connection;
            boolean expired = this.scheduler != null && c != null && c.buffer.isExpired();
            if ( c == null || expired )
            {
                FluxReplay.ReplaySubscriber<T> u = this.newState();
                if ( !CONNECTION.compareAndSet( this, c, u ) )
                {
                    continue;
                }

                c = u;
            }

            FluxReplay.ReplayInner<T> inner = new FluxReplay.ReplayInner( actual );
            actual.onSubscribe( inner );
            c.add( inner );
            if ( inner.isCancelled() )
            {
                c.remove( inner );
                return null;
            }

            inner.parent = c;
            c.buffer.replay( inner );
            if ( expired )
            {
                return c;
            }

            return null;
        }
    }

    public final CorePublisher<? extends T> source()
    {
        return this.source;
    }

    public final OptimizableOperator<?,? extends T> nextOptimizableSource()
    {
        return this.optimizableOperator;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.RUN_ON ? this.scheduler : null;
        }
    }

    interface ReplayBuffer<T>
    {
        void add( T var1 );

        void onError( Throwable var1 );

        @Nullable
        Throwable getError();

        void onComplete();

        void replay( FluxReplay.ReplaySubscription<T> var1 );

        boolean isDone();

        @Nullable
        T poll( FluxReplay.ReplaySubscription<T> var1 );

        void clear( FluxReplay.ReplaySubscription<T> var1 );

        boolean isEmpty( FluxReplay.ReplaySubscription<T> var1 );

        int size( FluxReplay.ReplaySubscription<T> var1 );

        int size();

        int capacity();

        boolean isExpired();
    }

    interface ReplaySubscription<T> extends Fuseable.QueueSubscription<T>, InnerProducer<T>
    {
        CoreSubscriber<? super T> actual();

        boolean enter();

        int leave( int var1 );

        void produced( long var1 );

        void node( @Nullable Object var1 );

        @Nullable
        Object node();

        int tailIndex();

        void tailIndex( int var1 );

        int index();

        void index( int var1 );

        int fusionMode();

        boolean isCancelled();

        long requested();
    }

    static final class ReplayInner<T> implements FluxReplay.ReplaySubscription<T>
    {
        static final AtomicIntegerFieldUpdater<FluxReplay.ReplayInner> WIP = AtomicIntegerFieldUpdater.newUpdater( FluxReplay.ReplayInner.class, "wip" );
        static final AtomicLongFieldUpdater<FluxReplay.ReplayInner> REQUESTED = AtomicLongFieldUpdater.newUpdater( FluxReplay.ReplayInner.class, "requested" );
        final CoreSubscriber<? super T> actual;
        FluxReplay.ReplaySubscriber<T> parent;
        int index;
        int tailIndex;
        Object node;
        int fusionMode;
        volatile int wip;
        volatile long requested;

        ReplayInner( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( this.fusionMode() == 0 )
                {
                    Operators.addCapCancellable( REQUESTED, this, n );
                }

                FluxReplay.ReplaySubscriber<T> p = this.parent;
                if ( p != null )
                {
                    p.buffer.replay( this );
                }
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else if ( key != Scannable.Attr.TERMINATED )
            {
                if ( key == Scannable.Attr.BUFFERED )
                {
                    return this.size();
                }
                else if ( key == Scannable.Attr.CANCELLED )
                {
                    return this.isCancelled();
                }
                else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
                {
                    return Math.max( 0L, this.requested );
                }
                else
                {
                    return key == Scannable.Attr.RUN_ON ? this.parent.parent.scheduler : FluxReplay.ReplaySubscription.super.scanUnsafe( key );
                }
            }
            else
            {
                return this.parent != null && this.parent.isTerminated();
            }
        }

        public void cancel()
        {
            if ( REQUESTED.getAndSet( this, Long.MIN_VALUE ) != Long.MIN_VALUE )
            {
                FluxReplay.ReplaySubscriber<T> p = this.parent;
                if ( p != null )
                {
                    p.remove( this );
                }

                if ( this.enter() )
                {
                    this.node = null;
                }
            }
        }

        public long requested()
        {
            return this.requested;
        }

        public boolean isCancelled()
        {
            return this.requested == Long.MIN_VALUE;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public int requestFusion( int requestedMode )
        {
            if ( (requestedMode & 2) != 0 )
            {
                this.fusionMode = 2;
                return 2;
            }
            else
            {
                return 0;
            }
        }

        @Nullable
        public T poll()
        {
            FluxReplay.ReplaySubscriber<T> p = this.parent;
            return p != null ? p.buffer.poll( this ) : null;
        }

        public void clear()
        {
            FluxReplay.ReplaySubscriber<T> p = this.parent;
            if ( p != null )
            {
                p.buffer.clear( this );
            }
        }

        public boolean isEmpty()
        {
            FluxReplay.ReplaySubscriber<T> p = this.parent;
            return p == null || p.buffer.isEmpty( this );
        }

        public int size()
        {
            FluxReplay.ReplaySubscriber<T> p = this.parent;
            return p != null ? p.buffer.size( this ) : 0;
        }

        public void node( @Nullable Object node )
        {
            this.node = node;
        }

        public int fusionMode()
        {
            return this.fusionMode;
        }

        @Nullable
        public Object node()
        {
            return this.node;
        }

        public int index()
        {
            return this.index;
        }

        public void index( int index )
        {
            this.index = index;
        }

        public int tailIndex()
        {
            return this.tailIndex;
        }

        public void tailIndex( int tailIndex )
        {
            this.tailIndex = tailIndex;
        }

        public boolean enter()
        {
            return WIP.getAndIncrement( this ) == 0;
        }

        public int leave( int missed )
        {
            return WIP.addAndGet( this, -missed );
        }

        public void produced( long n )
        {
            REQUESTED.addAndGet( this, -n );
        }
    }

    static final class ReplaySubscriber<T> implements InnerConsumer<T>, Disposable
    {
        static final AtomicReferenceFieldUpdater<FluxReplay.ReplaySubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxReplay.ReplaySubscriber.class, Subscription.class, "s" );
        static final AtomicIntegerFieldUpdater<FluxReplay.ReplaySubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxReplay.ReplaySubscriber.class, "wip" );
        static final AtomicIntegerFieldUpdater<FluxReplay.ReplaySubscriber> CONNECTED =
                AtomicIntegerFieldUpdater.newUpdater( FluxReplay.ReplaySubscriber.class, "connected" );
        static final FluxReplay.ReplaySubscription[] EMPTY = new FluxReplay.ReplaySubscription[0];
        static final FluxReplay.ReplaySubscription[] TERMINATED = new FluxReplay.ReplaySubscription[0];
        final FluxReplay<T> parent;
        final FluxReplay.ReplayBuffer<T> buffer;
        volatile Subscription s;
        volatile FluxReplay.ReplaySubscription<T>[] subscribers;
        volatile int wip;
        volatile int connected;
        volatile boolean cancelled;

        ReplaySubscriber( FluxReplay.ReplayBuffer<T> buffer, FluxReplay<T> parent )
        {
            this.buffer = buffer;
            this.parent = parent;
            this.subscribers = EMPTY;
        }

        public void onSubscribe( Subscription s )
        {
            if ( this.buffer.isDone() )
            {
                s.cancel();
            }
            else if ( Operators.setOnce( S, this, s ) )
            {
                long max = (long) this.parent.history;
                FluxReplay.ReplaySubscription[] var4 = this.subscribers;
                int var5 = var4.length;

                for ( int var6 = 0; var6 < var5; ++var6 )
                {
                    FluxReplay.ReplaySubscription<T> subscriber = var4[var6];
                    max = Math.max( subscriber.fusionMode() != 0 ? Long.MAX_VALUE : subscriber.requested(), max );
                    if ( max == Long.MAX_VALUE )
                    {
                        break;
                    }
                }

                s.request( max );
            }
        }

        public void onNext( T t )
        {
            FluxReplay.ReplayBuffer<T> b = this.buffer;
            if ( b.isDone() )
            {
                Operators.onNextDropped( t, this.currentContext() );
            }
            else
            {
                b.add( t );
                FluxReplay.ReplaySubscription[] var3 = this.subscribers;
                int var4 = var3.length;

                for ( int var5 = 0; var5 < var4; ++var5 )
                {
                    FluxReplay.ReplaySubscription<T> rs = var3[var5];
                    b.replay( rs );
                }
            }
        }

        public void onError( Throwable t )
        {
            FluxReplay.ReplayBuffer<T> b = this.buffer;
            if ( b.isDone() )
            {
                Operators.onErrorDropped( t, this.currentContext() );
            }
            else
            {
                b.onError( t );
                FluxReplay.ReplaySubscription[] var3 = this.terminate();
                int var4 = var3.length;

                for ( int var5 = 0; var5 < var4; ++var5 )
                {
                    FluxReplay.ReplaySubscription<T> rs = var3[var5];
                    b.replay( rs );
                }
            }
        }

        public void onComplete()
        {
            FluxReplay.ReplayBuffer<T> b = this.buffer;
            if ( !b.isDone() )
            {
                b.onComplete();
                FluxReplay.ReplaySubscription[] var2 = this.terminate();
                int var3 = var2.length;

                for ( int var4 = 0; var4 < var3; ++var4 )
                {
                    FluxReplay.ReplaySubscription<T> rs = var2[var4];
                    b.replay( rs );
                }
            }
        }

        public void dispose()
        {
            if ( !this.cancelled )
            {
                if ( Operators.terminate( S, this ) )
                {
                    this.cancelled = true;
                    FluxReplay.CONNECTION.lazySet( this.parent, (Object) null );
                    CancellationException ex = new CancellationException( "Disconnected" );
                    this.buffer.onError( ex );
                    FluxReplay.ReplaySubscription[] var2 = this.terminate();
                    int var3 = var2.length;

                    for ( int var4 = 0; var4 < var3; ++var4 )
                    {
                        FluxReplay.ReplaySubscription<T> inner = var2[var4];
                        this.buffer.replay( inner );
                    }
                }
            }
        }

        boolean add( FluxReplay.ReplayInner<T> inner )
        {
            if ( this.subscribers == TERMINATED )
            {
                return false;
            }
            else
            {
                synchronized ( this )
                {
                    FluxReplay.ReplaySubscription<T>[] a = this.subscribers;
                    if ( a == TERMINATED )
                    {
                        return false;
                    }
                    else
                    {
                        int n = a.length;
                        FluxReplay.ReplayInner<T>[] b = new FluxReplay.ReplayInner[n + 1];
                        System.arraycopy( a, 0, b, 0, n );
                        b[n] = inner;
                        this.subscribers = b;
                        return true;
                    }
                }
            }
        }

        void remove( FluxReplay.ReplaySubscription<T> inner )
        {
            FluxReplay.ReplaySubscription<T>[] a = this.subscribers;
            if ( a != TERMINATED && a != EMPTY )
            {
                synchronized ( this )
                {
                    a = this.subscribers;
                    if ( a != TERMINATED && a != EMPTY )
                    {
                        int j = -1;
                        int n = a.length;

                        for ( int i = 0; i < n; ++i )
                        {
                            if ( a[i] == inner )
                            {
                                j = i;
                                break;
                            }
                        }

                        if ( j >= 0 )
                        {
                            Object b;
                            if ( n == 1 )
                            {
                                b = EMPTY;
                            }
                            else
                            {
                                b = new FluxReplay.ReplayInner[n - 1];
                                System.arraycopy( a, 0, b, 0, j );
                                System.arraycopy( a, j + 1, b, j, n - j - 1 );
                            }

                            this.subscribers = (FluxReplay.ReplaySubscription[]) b;
                        }
                    }
                }
            }
        }

        FluxReplay.ReplaySubscription<T>[] terminate()
        {
            FluxReplay.ReplaySubscription<T>[] a = this.subscribers;
            if ( a == TERMINATED )
            {
                return a;
            }
            else
            {
                synchronized ( this )
                {
                    a = this.subscribers;
                    if ( a != TERMINATED )
                    {
                        this.subscribers = TERMINATED;
                    }

                    return a;
                }
            }
        }

        boolean isTerminated()
        {
            return this.subscribers == TERMINATED;
        }

        boolean tryConnect()
        {
            return this.connected == 0 && CONNECTED.compareAndSet( this, 0, 1 );
        }

        public Context currentContext()
        {
            return Operators.multiSubscribersContext( this.subscribers );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.buffer.capacity();
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.buffer.getError();
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.buffer.size();
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.isTerminated();
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.cancelled : null;
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        public boolean isDisposed()
        {
            return this.cancelled;
        }
    }

    static final class SizeBoundReplayBuffer<T> implements FluxReplay.ReplayBuffer<T>
    {
        final int limit;
        volatile FluxReplay.SizeBoundReplayBuffer.Node<T> head;
        FluxReplay.SizeBoundReplayBuffer.Node<T> tail;
        int size;
        volatile boolean done;
        Throwable error;

        SizeBoundReplayBuffer( int limit )
        {
            if ( limit < 0 )
            {
                throw new IllegalArgumentException( "Limit cannot be negative" );
            }
            else
            {
                this.limit = limit;
                FluxReplay.SizeBoundReplayBuffer.Node<T> n = new FluxReplay.SizeBoundReplayBuffer.Node( (Object) null );
                this.tail = n;
                this.head = n;
            }
        }

        public boolean isExpired()
        {
            return false;
        }

        public int capacity()
        {
            return this.limit;
        }

        public void add( T value )
        {
            FluxReplay.SizeBoundReplayBuffer.Node<T> n = new FluxReplay.SizeBoundReplayBuffer.Node( value );
            this.tail.set( n );
            this.tail = n;
            int s = this.size;
            if ( s == this.limit )
            {
                this.head = (FluxReplay.SizeBoundReplayBuffer.Node) this.head.get();
            }
            else
            {
                this.size = s + 1;
            }
        }

        public void onError( Throwable ex )
        {
            this.error = ex;
            this.done = true;
        }

        public void onComplete()
        {
            this.done = true;
        }

        void replayNormal( FluxReplay.ReplaySubscription<T> rs )
        {
            Subscriber<? super T> a = rs.actual();
            int missed = 1;

            do
            {
                long r = rs.requested();
                long e = 0L;
                FluxReplay.SizeBoundReplayBuffer.Node<T> node = (FluxReplay.SizeBoundReplayBuffer.Node) rs.node();
                if ( node == null )
                {
                    node = this.head;
                }

                boolean d;
                while ( e != r )
                {
                    if ( rs.isCancelled() )
                    {
                        rs.node( (Object) null );
                        return;
                    }

                    d = this.done;
                    FluxReplay.SizeBoundReplayBuffer.Node<T> next = (FluxReplay.SizeBoundReplayBuffer.Node) node.get();
                    boolean empty = next == null;
                    if ( d && empty )
                    {
                        rs.node( (Object) null );
                        Throwable ex = this.error;
                        if ( ex != null )
                        {
                            a.onError( ex );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    a.onNext( next.value );
                    ++e;
                    node = next;
                }

                if ( e == r )
                {
                    if ( rs.isCancelled() )
                    {
                        rs.node( (Object) null );
                        return;
                    }

                    d = this.done;
                    boolean empty = node.get() == null;
                    if ( d && empty )
                    {
                        rs.node( (Object) null );
                        Throwable ex = this.error;
                        if ( ex != null )
                        {
                            a.onError( ex );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return;
                    }
                }

                if ( e != 0L && r != Long.MAX_VALUE )
                {
                    rs.produced( e );
                }

                rs.node( node );
                missed = rs.leave( missed );
            }
            while ( missed != 0 );
        }

        void replayFused( FluxReplay.ReplaySubscription<T> rs )
        {
            int missed = 1;
            CoreSubscriber a = rs.actual();

            while ( !rs.isCancelled() )
            {
                boolean d = this.done;
                a.onNext( (Object) null );
                if ( d )
                {
                    Throwable ex = this.error;
                    if ( ex != null )
                    {
                        a.onError( ex );
                    }
                    else
                    {
                        a.onComplete();
                    }

                    return;
                }

                missed = rs.leave( missed );
                if ( missed == 0 )
                {
                    return;
                }
            }

            rs.node( (Object) null );
        }

        public void replay( FluxReplay.ReplaySubscription<T> rs )
        {
            if ( rs.enter() )
            {
                if ( rs.fusionMode() == 0 )
                {
                    this.replayNormal( rs );
                }
                else
                {
                    this.replayFused( rs );
                }
            }
        }

        @Nullable
        public Throwable getError()
        {
            return this.error;
        }

        public boolean isDone()
        {
            return this.done;
        }

        @Nullable
        public T poll( FluxReplay.ReplaySubscription<T> rs )
        {
            FluxReplay.SizeBoundReplayBuffer.Node<T> node = (FluxReplay.SizeBoundReplayBuffer.Node) rs.node();
            if ( node == null )
            {
                node = this.head;
                rs.node( node );
            }

            FluxReplay.SizeBoundReplayBuffer.Node<T> next = (FluxReplay.SizeBoundReplayBuffer.Node) node.get();
            if ( next == null )
            {
                return null;
            }
            else
            {
                rs.node( next );
                return next.value;
            }
        }

        public void clear( FluxReplay.ReplaySubscription<T> rs )
        {
            rs.node( (Object) null );
        }

        public boolean isEmpty( FluxReplay.ReplaySubscription<T> rs )
        {
            FluxReplay.SizeBoundReplayBuffer.Node<T> node = (FluxReplay.SizeBoundReplayBuffer.Node) rs.node();
            if ( node == null )
            {
                node = this.head;
                rs.node( node );
            }

            return node.get() == null;
        }

        public int size( FluxReplay.ReplaySubscription<T> rs )
        {
            FluxReplay.SizeBoundReplayBuffer.Node<T> node = (FluxReplay.SizeBoundReplayBuffer.Node) rs.node();
            if ( node == null )
            {
                node = this.head;
            }

            int count;
            FluxReplay.SizeBoundReplayBuffer.Node next;
            for ( count = 0; (next = (FluxReplay.SizeBoundReplayBuffer.Node) node.get()) != null && count != Integer.MAX_VALUE; node = next )
            {
                ++count;
            }

            return count;
        }

        public int size()
        {
            FluxReplay.SizeBoundReplayBuffer.Node<T> node = this.head;

            int count;
            FluxReplay.SizeBoundReplayBuffer.Node next;
            for ( count = 0; (next = (FluxReplay.SizeBoundReplayBuffer.Node) node.get()) != null && count != Integer.MAX_VALUE; node = next )
            {
                ++count;
            }

            return count;
        }

        static final class Node<T> extends AtomicReference<FluxReplay.SizeBoundReplayBuffer.Node<T>>
        {
            private static final long serialVersionUID = 3713592843205853725L;
            final T value;

            Node( @Nullable T value )
            {
                this.value = value;
            }
        }
    }

    static final class UnboundedReplayBuffer<T> implements FluxReplay.ReplayBuffer<T>
    {
        final int batchSize;
        final Object[] head;
        volatile int size;
        Object[] tail;
        int tailIndex;
        volatile boolean done;
        Throwable error;

        UnboundedReplayBuffer( int batchSize )
        {
            this.batchSize = batchSize;
            Object[] n = new Object[batchSize + 1];
            this.tail = n;
            this.head = n;
        }

        public boolean isExpired()
        {
            return false;
        }

        @Nullable
        public Throwable getError()
        {
            return this.error;
        }

        public int capacity()
        {
            return Integer.MAX_VALUE;
        }

        public void add( T value )
        {
            int i = this.tailIndex;
            Object[] a = this.tail;
            if ( i == a.length - 1 )
            {
                Object[] b = new Object[a.length];
                b[0] = value;
                this.tailIndex = 1;
                a[i] = b;
                this.tail = b;
            }
            else
            {
                a[i] = value;
                this.tailIndex = i + 1;
            }

            ++this.size;
        }

        public void onError( Throwable ex )
        {
            this.error = ex;
            this.done = true;
        }

        public void onComplete()
        {
            this.done = true;
        }

        void replayNormal( FluxReplay.ReplaySubscription<T> rs )
        {
            int missed = 1;
            Subscriber<? super T> a = rs.actual();
            int n = this.batchSize;

            do
            {
                long r = rs.requested();
                long e = 0L;
                Object[] node = (Object[]) ((Object[]) rs.node());
                if ( node == null )
                {
                    node = this.head;
                }

                int tailIndex = rs.tailIndex();

                int index;
                boolean d;
                boolean empty;
                Throwable ex;
                for ( index = rs.index(); e != r; ++index )
                {
                    if ( rs.isCancelled() )
                    {
                        rs.node( (Object) null );
                        return;
                    }

                    d = this.done;
                    empty = index == this.size;
                    if ( d && empty )
                    {
                        rs.node( (Object) null );
                        ex = this.error;
                        if ( ex != null )
                        {
                            a.onError( ex );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    if ( tailIndex == n )
                    {
                        node = (Object[]) ((Object[]) node[tailIndex]);
                        tailIndex = 0;
                    }

                    T v = node[tailIndex];
                    a.onNext( v );
                    ++e;
                    ++tailIndex;
                }

                if ( e == r )
                {
                    if ( rs.isCancelled() )
                    {
                        rs.node( (Object) null );
                        return;
                    }

                    d = this.done;
                    empty = index == this.size;
                    if ( d && empty )
                    {
                        rs.node( (Object) null );
                        ex = this.error;
                        if ( ex != null )
                        {
                            a.onError( ex );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return;
                    }
                }

                if ( e != 0L && r != Long.MAX_VALUE )
                {
                    rs.produced( e );
                }

                rs.index( index );
                rs.tailIndex( tailIndex );
                rs.node( node );
                missed = rs.leave( missed );
            }
            while ( missed != 0 );
        }

        void replayFused( FluxReplay.ReplaySubscription<T> rs )
        {
            int missed = 1;
            CoreSubscriber a = rs.actual();

            while ( !rs.isCancelled() )
            {
                boolean d = this.done;
                a.onNext( (Object) null );
                if ( d )
                {
                    Throwable ex = this.error;
                    if ( ex != null )
                    {
                        a.onError( ex );
                    }
                    else
                    {
                        a.onComplete();
                    }

                    return;
                }

                missed = rs.leave( missed );
                if ( missed == 0 )
                {
                    return;
                }
            }

            rs.node( (Object) null );
        }

        public void replay( FluxReplay.ReplaySubscription<T> rs )
        {
            if ( rs.enter() )
            {
                if ( rs.fusionMode() == 0 )
                {
                    this.replayNormal( rs );
                }
                else
                {
                    this.replayFused( rs );
                }
            }
        }

        public boolean isDone()
        {
            return this.done;
        }

        @Nullable
        public T poll( FluxReplay.ReplaySubscription<T> rs )
        {
            int index = rs.index();
            if ( index == this.size )
            {
                return null;
            }
            else
            {
                Object[] node = (Object[]) ((Object[]) rs.node());
                if ( node == null )
                {
                    node = this.head;
                    rs.node( node );
                }

                int tailIndex = rs.tailIndex();
                if ( tailIndex == this.batchSize )
                {
                    node = (Object[]) ((Object[]) node[tailIndex]);
                    tailIndex = 0;
                    rs.node( node );
                }

                T v = node[tailIndex];
                rs.index( index + 1 );
                rs.tailIndex( tailIndex + 1 );
                return v;
            }
        }

        public void clear( FluxReplay.ReplaySubscription<T> rs )
        {
            rs.node( (Object) null );
        }

        public boolean isEmpty( FluxReplay.ReplaySubscription<T> rs )
        {
            return rs.index() == this.size;
        }

        public int size( FluxReplay.ReplaySubscription<T> rs )
        {
            return this.size - rs.index();
        }

        public int size()
        {
            return this.size;
        }
    }

    static final class SizeAndTimeBoundReplayBuffer<T> implements FluxReplay.ReplayBuffer<T>
    {
        static final long NOT_DONE = Long.MIN_VALUE;
        final int limit;
        final long maxAge;
        final Scheduler scheduler;
        int size;
        volatile FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> head;
        FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> tail;
        Throwable error;
        volatile long done = Long.MIN_VALUE;

        SizeAndTimeBoundReplayBuffer( int limit, long maxAge, Scheduler scheduler )
        {
            this.limit = limit;
            this.maxAge = maxAge;
            this.scheduler = scheduler;
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> h = new FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode( (Object) null, 0L );
            this.tail = h;
            this.head = h;
        }

        public boolean isExpired()
        {
            long done = this.done;
            return done != Long.MIN_VALUE && this.scheduler.now( TimeUnit.MILLISECONDS ) - this.maxAge > done;
        }

        void replayNormal( FluxReplay.ReplaySubscription<T> rs )
        {
            int missed = 1;
            CoreSubscriber a = rs.actual();

            do
            {
                FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> node = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) rs.node();
                long r;
                if ( node == null )
                {
                    node = this.head;
                    if ( this.done == Long.MIN_VALUE )
                    {
                        r = this.scheduler.now( TimeUnit.MILLISECONDS ) - this.maxAge;

                        for ( FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode next = node; next != null;
                                next = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) next.get() )
                        {
                            long ts = next.time;
                            if ( ts > r )
                            {
                                break;
                            }

                            node = next;
                        }
                    }
                }

                r = rs.requested();

                boolean d;
                FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode next;
                long e;
                for ( e = 0L; e != r; node = next )
                {
                    if ( rs.isCancelled() )
                    {
                        rs.node( (Object) null );
                        return;
                    }

                    d = this.done != Long.MIN_VALUE;
                    next = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) node.get();
                    boolean empty = next == null;
                    if ( d && empty )
                    {
                        rs.node( (Object) null );
                        Throwable ex = this.error;
                        if ( ex != null )
                        {
                            a.onError( ex );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }

                    a.onNext( next.value );
                    ++e;
                }

                if ( e == r )
                {
                    if ( rs.isCancelled() )
                    {
                        rs.node( (Object) null );
                        return;
                    }

                    d = this.done != Long.MIN_VALUE;
                    boolean empty = node.get() == null;
                    if ( d && empty )
                    {
                        rs.node( (Object) null );
                        Throwable ex = this.error;
                        if ( ex != null )
                        {
                            a.onError( ex );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return;
                    }
                }

                if ( e != 0L && r != Long.MAX_VALUE )
                {
                    rs.produced( e );
                }

                rs.node( node );
                missed = rs.leave( missed );
            }
            while ( missed != 0 );
        }

        void replayFused( FluxReplay.ReplaySubscription<T> rs )
        {
            int missed = 1;
            CoreSubscriber a = rs.actual();

            do
            {
                if ( rs.isCancelled() )
                {
                    rs.node( (Object) null );
                    return;
                }

                boolean d = this.done != Long.MIN_VALUE;
                a.onNext( (Object) null );
                if ( d )
                {
                    Throwable ex = this.error;
                    if ( ex != null )
                    {
                        a.onError( ex );
                    }
                    else
                    {
                        a.onComplete();
                    }

                    return;
                }

                missed = rs.leave( missed );
            }
            while ( missed != 0 );
        }

        public void onError( Throwable ex )
        {
            this.done = this.scheduler.now( TimeUnit.MILLISECONDS );
            this.error = ex;
        }

        @Nullable
        public Throwable getError()
        {
            return this.error;
        }

        public void onComplete()
        {
            this.done = this.scheduler.now( TimeUnit.MILLISECONDS );
        }

        public boolean isDone()
        {
            return this.done != Long.MIN_VALUE;
        }

        FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> latestHead( FluxReplay.ReplaySubscription<T> rs )
        {
            long now = this.scheduler.now( TimeUnit.MILLISECONDS ) - this.maxAge;
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> h = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) rs.node();
            if ( h == null )
            {
                h = this.head;
            }

            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode n;
            while ( (n = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) h.get()) != null && n.time <= now )
            {
                h = n;
            }

            return h;
        }

        @Nullable
        public T poll( FluxReplay.ReplaySubscription<T> rs )
        {
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> node = this.latestHead( rs );

            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode next;
            for ( long now = this.scheduler.now( TimeUnit.MILLISECONDS ) - this.maxAge;
                    (next = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) node.get()) != null; node = next )
            {
                if ( next.time > now )
                {
                    node = next;
                    break;
                }
            }

            if ( next == null )
            {
                return null;
            }
            else
            {
                rs.node( next );
                return node.value;
            }
        }

        public void clear( FluxReplay.ReplaySubscription<T> rs )
        {
            rs.node( (Object) null );
        }

        public boolean isEmpty( FluxReplay.ReplaySubscription<T> rs )
        {
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> node = this.latestHead( rs );
            return node.get() == null;
        }

        public int size( FluxReplay.ReplaySubscription<T> rs )
        {
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> node = this.latestHead( rs );

            int count;
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode next;
            for ( count = 0; (next = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) node.get()) != null && count != Integer.MAX_VALUE; node = next )
            {
                ++count;
            }

            return count;
        }

        public int size()
        {
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> node = this.head;

            int count;
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode next;
            for ( count = 0; (next = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) node.get()) != null && count != Integer.MAX_VALUE; node = next )
            {
                ++count;
            }

            return count;
        }

        public int capacity()
        {
            return this.limit;
        }

        public void add( T value )
        {
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> n =
                    new FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode( value, this.scheduler.now( TimeUnit.MILLISECONDS ) );
            this.tail.set( n );
            this.tail = n;
            int s = this.size;
            if ( s == this.limit )
            {
                this.head = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) this.head.get();
            }
            else
            {
                this.size = s + 1;
            }

            long limit = this.scheduler.now( TimeUnit.MILLISECONDS ) - this.maxAge;
            FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> h = this.head;
            int removed = 0;

            while ( true )
            {
                FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T> next = (FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode) h.get();
                if ( next == null )
                {
                    break;
                }

                if ( next.time > limit )
                {
                    if ( removed != 0 )
                    {
                        this.size -= removed;
                        this.head = h;
                    }
                    break;
                }

                h = next;
                ++removed;
            }
        }

        public void replay( FluxReplay.ReplaySubscription<T> rs )
        {
            if ( rs.enter() )
            {
                if ( rs.fusionMode() == 0 )
                {
                    this.replayNormal( rs );
                }
                else
                {
                    this.replayFused( rs );
                }
            }
        }

        static final class TimedNode<T> extends AtomicReference<FluxReplay.SizeAndTimeBoundReplayBuffer.TimedNode<T>>
        {
            final T value;
            final long time;

            TimedNode( @Nullable T value, long time )
            {
                this.value = value;
                this.time = time;
            }
        }
    }
}
