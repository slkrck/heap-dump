package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Supplier;

final class SingleScheduler implements Scheduler, Supplier<ScheduledExecutorService>, Scannable
{
    static final AtomicLong COUNTER = new AtomicLong();
    static final AtomicReferenceFieldUpdater<SingleScheduler,ScheduledExecutorService> EXECUTORS =
            AtomicReferenceFieldUpdater.newUpdater( SingleScheduler.class, ScheduledExecutorService.class, "executor" );
    static final ScheduledExecutorService TERMINATED = Executors.newSingleThreadScheduledExecutor();

    static
    {
        TERMINATED.shutdownNow();
    }

    final ThreadFactory factory;
    volatile ScheduledExecutorService executor;

    SingleScheduler( ThreadFactory factory )
    {
        this.factory = factory;
        this.init();
    }

    public ScheduledExecutorService get()
    {
        ScheduledThreadPoolExecutor e = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool( 1, this.factory );
        e.setRemoveOnCancelPolicy( true );
        e.setMaximumPoolSize( 1 );
        return e;
    }

    private void init()
    {
        EXECUTORS.lazySet( this, Schedulers.decorateExecutorService( this, this.get() ) );
    }

    public boolean isDisposed()
    {
        return this.executor == TERMINATED;
    }

    public void start()
    {
        ScheduledExecutorService b = null;

        ScheduledExecutorService a;
        do
        {
            a = this.executor;
            if ( a != TERMINATED )
            {
                if ( b != null )
                {
                    b.shutdownNow();
                }

                return;
            }

            if ( b == null )
            {
                b = Schedulers.decorateExecutorService( this, this.get() );
            }
        }
        while ( !EXECUTORS.compareAndSet( this, a, b ) );
    }

    public void dispose()
    {
        ScheduledExecutorService a = this.executor;
        if ( a != TERMINATED )
        {
            a = (ScheduledExecutorService) EXECUTORS.getAndSet( this, TERMINATED );
            if ( a != TERMINATED )
            {
                a.shutdownNow();
            }
        }
    }

    public Disposable schedule( Runnable task )
    {
        return Schedulers.directSchedule( this.executor, task, (Disposable) null, 0L, TimeUnit.MILLISECONDS );
    }

    public Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        return Schedulers.directSchedule( this.executor, task, (Disposable) null, delay, unit );
    }

    public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        return Schedulers.directSchedulePeriodically( this.executor, task, initialDelay, period, unit );
    }

    public String toString()
    {
        StringBuilder ts = (new StringBuilder( "single" )).append( '(' );
        if ( this.factory instanceof ReactorThreadFactory )
        {
            ts.append( '"' ).append( ((ReactorThreadFactory) this.factory).get() ).append( '"' );
        }

        return ts.append( ')' ).toString();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            if ( key == Scannable.Attr.NAME )
            {
                return this.toString();
            }
            else
            {
                return key != Scannable.Attr.CAPACITY && key != Scannable.Attr.BUFFERED ? Schedulers.scanExecutor( this.executor, key ) : 1;
            }
        }
        else
        {
            return this.isDisposed();
        }
    }

    public Scheduler.Worker createWorker()
    {
        return new ExecutorServiceWorker( this.executor );
    }
}
