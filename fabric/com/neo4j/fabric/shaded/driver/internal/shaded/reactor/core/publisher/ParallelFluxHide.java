package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

final class ParallelFluxHide<T> extends ParallelFlux<T> implements Scannable
{
    final ParallelFlux<T> source;

    ParallelFluxHide( ParallelFlux<T> source )
    {
        this.source = source;
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<? super T>[] parents = new CoreSubscriber[n];

            for ( int i = 0; i < n; ++i )
            {
                parents[i] = new FluxHide.HideSubscriber( subscribers[i] );
            }

            this.source.subscribe( parents );
        }
    }
}
