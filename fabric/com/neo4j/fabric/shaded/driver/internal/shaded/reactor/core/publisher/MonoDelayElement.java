package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import org.reactivestreams.Subscription;

final class MonoDelayElement<T> extends InternalMonoOperator<T,T>
{
    final Scheduler timedScheduler;
    final long delay;
    final TimeUnit unit;

    MonoDelayElement( Mono<? extends T> source, long delay, TimeUnit unit, Scheduler timedScheduler )
    {
        super( source );
        this.delay = delay;
        this.unit = (TimeUnit) Objects.requireNonNull( unit, "unit" );
        this.timedScheduler = (Scheduler) Objects.requireNonNull( timedScheduler, "timedScheduler" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoDelayElement.DelayElementSubscriber( actual, this.timedScheduler, this.delay, this.unit );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.timedScheduler : super.scanUnsafe( key );
    }

    static final class DelayElementSubscriber<T> extends Operators.MonoSubscriber<T,T>
    {
        final long delay;
        final Scheduler scheduler;
        final TimeUnit unit;
        Subscription s;
        volatile Disposable task;
        boolean done;

        DelayElementSubscriber( CoreSubscriber<? super T> actual, Scheduler scheduler, long delay, TimeUnit unit )
        {
            super( actual );
            this.scheduler = scheduler;
            this.delay = delay;
            this.unit = unit;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
            }
        }

        public void cancel()
        {
            super.cancel();
            if ( this.task != null )
            {
                this.task.dispose();
            }

            if ( this.s != Operators.cancelledSubscription() )
            {
                this.s.cancel();
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;

                try
                {
                    this.task = this.scheduler.schedule( () -> {
                        this.complete( t );
                    }, this.delay, this.unit );
                }
                catch ( RejectedExecutionException var3 )
                {
                    throw Operators.onRejectedExecution( var3, this, (Throwable) null, t, this.actual.currentContext() );
                }
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }
    }
}
