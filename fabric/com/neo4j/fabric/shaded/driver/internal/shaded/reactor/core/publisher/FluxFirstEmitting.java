package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxFirstEmitting<T> extends Flux<T> implements SourceProducer<T>
{
    final Publisher<? extends T>[] array;
    final Iterable<? extends Publisher<? extends T>> iterable;

    @SafeVarargs
    FluxFirstEmitting( Publisher<? extends T>... array )
    {
        this.array = (Publisher[]) Objects.requireNonNull( array, "array" );
        this.iterable = null;
    }

    FluxFirstEmitting( Iterable<? extends Publisher<? extends T>> iterable )
    {
        this.array = null;
        this.iterable = (Iterable) Objects.requireNonNull( iterable );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Publisher<? extends T>[] a = this.array;
        int n;
        if ( a == null )
        {
            n = 0;
            a = new Publisher[8];

            Iterator it;
            try
            {
                it = (Iterator) Objects.requireNonNull( this.iterable.iterator(), "The iterator returned is null" );
            }
            catch ( Throwable var10 )
            {
                Operators.error( actual, Operators.onOperatorError( var10, actual.currentContext() ) );
                return;
            }

            while ( true )
            {
                boolean b;
                try
                {
                    b = it.hasNext();
                }
                catch ( Throwable var8 )
                {
                    Operators.error( actual, Operators.onOperatorError( var8, actual.currentContext() ) );
                    return;
                }

                if ( !b )
                {
                    break;
                }

                Publisher p;
                try
                {
                    p = (Publisher) Objects.requireNonNull( it.next(), "The Publisher returned by the iterator is null" );
                }
                catch ( Throwable var9 )
                {
                    Operators.error( actual, Operators.onOperatorError( var9, actual.currentContext() ) );
                    return;
                }

                if ( n == a.length )
                {
                    Publisher<? extends T>[] c = new Publisher[n + (n >> 2)];
                    System.arraycopy( a, 0, c, 0, n );
                    a = c;
                }

                a[n++] = p;
            }
        }
        else
        {
            n = a.length;
        }

        if ( n == 0 )
        {
            Operators.complete( actual );
        }
        else if ( n == 1 )
        {
            Publisher<? extends T> p = a[0];
            if ( p == null )
            {
                Operators.error( actual, new NullPointerException( "The single source Publisher is null" ) );
            }
            else
            {
                p.subscribe( actual );
            }
        }
        else
        {
            FluxFirstEmitting.RaceCoordinator<T> coordinator = new FluxFirstEmitting.RaceCoordinator( n );
            coordinator.subscribe( a, n, actual );
        }
    }

    @Nullable
    FluxFirstEmitting<T> ambAdditionalSource( Publisher<? extends T> source )
    {
        if ( this.array != null )
        {
            int n = this.array.length;
            Publisher<? extends T>[] newArray = new Publisher[n + 1];
            System.arraycopy( this.array, 0, newArray, 0, n );
            newArray[n] = source;
            return new FluxFirstEmitting( newArray );
        }
        else
        {
            return null;
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class FirstEmittingSubscriber<T> extends Operators.DeferredSubscription implements InnerOperator<T,T>
    {
        final FluxFirstEmitting.RaceCoordinator<T> parent;
        final CoreSubscriber<? super T> actual;
        final int index;
        boolean won;

        FirstEmittingSubscriber( CoreSubscriber<? super T> actual, FluxFirstEmitting.RaceCoordinator<T> parent, int index )
        {
            this.actual = actual;
            this.parent = parent;
            this.index = index;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.parent.cancelled : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            this.set( s );
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void onNext( T t )
        {
            if ( this.won )
            {
                this.actual.onNext( t );
            }
            else if ( this.parent.tryWin( this.index ) )
            {
                this.won = true;
                this.actual.onNext( t );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.won )
            {
                this.actual.onError( t );
            }
            else if ( this.parent.tryWin( this.index ) )
            {
                this.won = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( this.won )
            {
                this.actual.onComplete();
            }
            else if ( this.parent.tryWin( this.index ) )
            {
                this.won = true;
                this.actual.onComplete();
            }
        }
    }

    static final class RaceCoordinator<T> implements Subscription, Scannable
    {
        static final AtomicIntegerFieldUpdater<FluxFirstEmitting.RaceCoordinator> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxFirstEmitting.RaceCoordinator.class, "wip" );
        final FluxFirstEmitting.FirstEmittingSubscriber<T>[] subscribers;
        volatile boolean cancelled;
        volatile int wip;

        RaceCoordinator( int n )
        {
            this.subscribers = new FluxFirstEmitting.FirstEmittingSubscriber[n];
            this.wip = Integer.MIN_VALUE;
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.CANCELLED ? this.cancelled : null;
        }

        void subscribe( Publisher<? extends T>[] sources, int n, CoreSubscriber<? super T> actual )
        {
            FluxFirstEmitting.FirstEmittingSubscriber<T>[] a = this.subscribers;

            int i;
            for ( i = 0; i < n; ++i )
            {
                a[i] = new FluxFirstEmitting.FirstEmittingSubscriber( actual, this, i );
            }

            actual.onSubscribe( this );

            for ( i = 0; i < n; ++i )
            {
                if ( this.cancelled || this.wip != Integer.MIN_VALUE )
                {
                    return;
                }

                Publisher<? extends T> p = sources[i];
                if ( p == null )
                {
                    if ( WIP.compareAndSet( this, Integer.MIN_VALUE, -1 ) )
                    {
                        actual.onError( new NullPointerException( "The " + i + " th Publisher source is null" ) );
                    }

                    return;
                }

                p.subscribe( a[i] );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                int w = this.wip;
                if ( w >= 0 )
                {
                    this.subscribers[w].request( n );
                }
                else
                {
                    FluxFirstEmitting.FirstEmittingSubscriber[] var4 = this.subscribers;
                    int var5 = var4.length;

                    for ( int var6 = 0; var6 < var5; ++var6 )
                    {
                        FluxFirstEmitting.FirstEmittingSubscriber<T> s = var4[var6];
                        s.request( n );
                    }
                }
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                int w = this.wip;
                if ( w >= 0 )
                {
                    this.subscribers[w].cancel();
                }
                else
                {
                    FluxFirstEmitting.FirstEmittingSubscriber[] var2 = this.subscribers;
                    int var3 = var2.length;

                    for ( int var4 = 0; var4 < var3; ++var4 )
                    {
                        FluxFirstEmitting.FirstEmittingSubscriber<T> s = var2[var4];
                        s.cancel();
                    }
                }
            }
        }

        boolean tryWin( int index )
        {
            if ( this.wip == Integer.MIN_VALUE && WIP.compareAndSet( this, Integer.MIN_VALUE, index ) )
            {
                FluxFirstEmitting.FirstEmittingSubscriber<T>[] a = this.subscribers;
                int n = a.length;

                for ( int i = 0; i < n; ++i )
                {
                    if ( i != index )
                    {
                        a[i].cancel();
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
