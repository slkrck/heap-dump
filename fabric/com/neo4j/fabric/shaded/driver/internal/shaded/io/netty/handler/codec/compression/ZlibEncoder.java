package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToByteEncoder;

public abstract class ZlibEncoder extends MessageToByteEncoder<ByteBuf>
{
    protected ZlibEncoder()
    {
        super( false );
    }

    public abstract boolean isClosed();

    public abstract ChannelFuture close();

    public abstract ChannelFuture close( ChannelPromise var1 );
}
