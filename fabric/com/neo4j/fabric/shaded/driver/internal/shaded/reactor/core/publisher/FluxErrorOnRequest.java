package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

final class FluxErrorOnRequest<T> extends Flux<T> implements SourceProducer<T>
{
    final Throwable error;

    FluxErrorOnRequest( Throwable error )
    {
        this.error = (Throwable) Objects.requireNonNull( error );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        actual.onSubscribe( new FluxErrorOnRequest.ErrorSubscription( actual, this.error ) );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class ErrorSubscription implements InnerProducer
    {
        static final AtomicIntegerFieldUpdater<FluxErrorOnRequest.ErrorSubscription> ONCE =
                AtomicIntegerFieldUpdater.newUpdater( FluxErrorOnRequest.ErrorSubscription.class, "once" );
        final CoreSubscriber<?> actual;
        final Throwable error;
        volatile int once;

        ErrorSubscription( CoreSubscriber<?> actual, Throwable error )
        {
            this.actual = actual;
            this.error = error;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && ONCE.compareAndSet( this, 0, 1 ) )
            {
                this.actual.onError( this.error );
            }
        }

        public void cancel()
        {
            this.once = 1;
        }

        public CoreSubscriber actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key != Scannable.Attr.CANCELLED && key != Scannable.Attr.TERMINATED ? InnerProducer.super.scanUnsafe( key ) : this.once == 1;
            }
        }
    }
}
