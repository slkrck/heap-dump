package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.reactivestreams.Subscription;

final class MonoCollectList<T> extends MonoFromFluxOperator<T,List<T>> implements Fuseable
{
    MonoCollectList( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super List<T>> actual )
    {
        return new MonoCollectList.MonoCollectListSubscriber( actual );
    }

    static final class MonoCollectListSubscriber<T> extends Operators.MonoSubscriber<T,List<T>>
    {
        Subscription s;
        List<T> list = new ArrayList();
        boolean done;

        MonoCollectListSubscriber( CoreSubscriber<? super List<T>> actual )
        {
            super( actual );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                synchronized ( this )
                {
                    List<T> l = this.list;
                    if ( l != null )
                    {
                        l.add( t );
                        return;
                    }
                }

                Operators.onDiscard( t, this.actual.currentContext() );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                List l;
                synchronized ( this )
                {
                    l = this.list;
                    this.list = null;
                }

                Operators.onDiscardMultiple( (Collection) l, this.actual.currentContext() );
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                List l;
                synchronized ( this )
                {
                    l = this.list;
                    this.list = null;
                }

                if ( l != null )
                {
                    this.complete( l );
                }
            }
        }

        protected void discard( List<T> v )
        {
            Operators.onDiscardMultiple( (Collection) v, this.actual.currentContext() );
        }

        public void cancel()
        {
            List l;
            synchronized ( this )
            {
                int state = STATE.getAndSet( this, 4 );
                if ( state <= 2 )
                {
                    l = this.list;
                    this.value = null;
                    this.list = null;
                }
                else
                {
                    l = null;
                }
            }

            if ( l != null )
            {
                this.s.cancel();
                Operators.onDiscardMultiple( (Collection) l, this.actual.currentContext() );
            }
        }
    }
}
