package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.group;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufHolder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelId;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ServerChannel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCountUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.StringUtil;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public class DefaultChannelGroup extends AbstractSet<Channel> implements ChannelGroup
{
    private static final AtomicInteger nextId = new AtomicInteger();
    private final String name;
    private final EventExecutor executor;
    private final ConcurrentMap<ChannelId,Channel> serverChannels;
    private final ConcurrentMap<ChannelId,Channel> nonServerChannels;
    private final ChannelFutureListener remover;
    private final VoidChannelGroupFuture voidFuture;
    private final boolean stayClosed;
    private volatile boolean closed;

    public DefaultChannelGroup( EventExecutor executor )
    {
        this( executor, false );
    }

    public DefaultChannelGroup( String name, EventExecutor executor )
    {
        this( name, executor, false );
    }

    public DefaultChannelGroup( EventExecutor executor, boolean stayClosed )
    {
        this( "group-0x" + Integer.toHexString( nextId.incrementAndGet() ), executor, stayClosed );
    }

    public DefaultChannelGroup( String name, EventExecutor executor, boolean stayClosed )
    {
        this.serverChannels = PlatformDependent.newConcurrentHashMap();
        this.nonServerChannels = PlatformDependent.newConcurrentHashMap();
        this.remover = new ChannelFutureListener()
        {
            public void operationComplete( ChannelFuture future ) throws Exception
            {
                DefaultChannelGroup.this.remove( future.channel() );
            }
        };
        this.voidFuture = new VoidChannelGroupFuture( this );
        if ( name == null )
        {
            throw new NullPointerException( "name" );
        }
        else
        {
            this.name = name;
            this.executor = executor;
            this.stayClosed = stayClosed;
        }
    }

    private static Object safeDuplicate( Object message )
    {
        if ( message instanceof ByteBuf )
        {
            return ((ByteBuf) message).retainedDuplicate();
        }
        else
        {
            return message instanceof ByteBufHolder ? ((ByteBufHolder) message).retainedDuplicate() : ReferenceCountUtil.retain( message );
        }
    }

    public String name()
    {
        return this.name;
    }

    public Channel find( ChannelId id )
    {
        Channel c = (Channel) this.nonServerChannels.get( id );
        return c != null ? c : (Channel) this.serverChannels.get( id );
    }

    public boolean isEmpty()
    {
        return this.nonServerChannels.isEmpty() && this.serverChannels.isEmpty();
    }

    public int size()
    {
        return this.nonServerChannels.size() + this.serverChannels.size();
    }

    public boolean contains( Object o )
    {
        if ( o instanceof ServerChannel )
        {
            return this.serverChannels.containsValue( o );
        }
        else
        {
            return o instanceof Channel ? this.nonServerChannels.containsValue( o ) : false;
        }
    }

    public boolean add( Channel channel )
    {
        ConcurrentMap<ChannelId,Channel> map = channel instanceof ServerChannel ? this.serverChannels : this.nonServerChannels;
        boolean added = map.putIfAbsent( channel.id(), channel ) == null;
        if ( added )
        {
            channel.closeFuture().addListener( this.remover );
        }

        if ( this.stayClosed && this.closed )
        {
            channel.close();
        }

        return added;
    }

    public boolean remove( Object o )
    {
        Channel c = null;
        if ( o instanceof ChannelId )
        {
            c = (Channel) this.nonServerChannels.remove( o );
            if ( c == null )
            {
                c = (Channel) this.serverChannels.remove( o );
            }
        }
        else if ( o instanceof Channel )
        {
            c = (Channel) o;
            if ( c instanceof ServerChannel )
            {
                c = (Channel) this.serverChannels.remove( c.id() );
            }
            else
            {
                c = (Channel) this.nonServerChannels.remove( c.id() );
            }
        }

        if ( c == null )
        {
            return false;
        }
        else
        {
            c.closeFuture().removeListener( this.remover );
            return true;
        }
    }

    public void clear()
    {
        this.nonServerChannels.clear();
        this.serverChannels.clear();
    }

    public Iterator<Channel> iterator()
    {
        return new CombinedIterator( this.serverChannels.values().iterator(), this.nonServerChannels.values().iterator() );
    }

    public Object[] toArray()
    {
        Collection<Channel> channels = new ArrayList( this.size() );
        channels.addAll( this.serverChannels.values() );
        channels.addAll( this.nonServerChannels.values() );
        return channels.toArray();
    }

    public <T> T[] toArray( T[] a )
    {
        Collection<Channel> channels = new ArrayList( this.size() );
        channels.addAll( this.serverChannels.values() );
        channels.addAll( this.nonServerChannels.values() );
        return channels.toArray( a );
    }

    public ChannelGroupFuture close()
    {
        return this.close( ChannelMatchers.all() );
    }

    public ChannelGroupFuture disconnect()
    {
        return this.disconnect( ChannelMatchers.all() );
    }

    public ChannelGroupFuture deregister()
    {
        return this.deregister( ChannelMatchers.all() );
    }

    public ChannelGroupFuture write( Object message )
    {
        return this.write( message, ChannelMatchers.all() );
    }

    public ChannelGroupFuture write( Object message, ChannelMatcher matcher )
    {
        return this.write( message, matcher, false );
    }

    public ChannelGroupFuture write( Object message, ChannelMatcher matcher, boolean voidPromise )
    {
        if ( message == null )
        {
            throw new NullPointerException( "message" );
        }
        else if ( matcher == null )
        {
            throw new NullPointerException( "matcher" );
        }
        else
        {
            Object future;
            if ( voidPromise )
            {
                Iterator var5 = this.nonServerChannels.values().iterator();

                while ( var5.hasNext() )
                {
                    Channel c = (Channel) var5.next();
                    if ( matcher.matches( c ) )
                    {
                        c.write( safeDuplicate( message ), c.voidPromise() );
                    }
                }

                future = this.voidFuture;
            }
            else
            {
                Map<Channel,ChannelFuture> futures = new LinkedHashMap( this.size() );
                Iterator var9 = this.nonServerChannels.values().iterator();

                while ( var9.hasNext() )
                {
                    Channel c = (Channel) var9.next();
                    if ( matcher.matches( c ) )
                    {
                        futures.put( c, c.write( safeDuplicate( message ) ) );
                    }
                }

                future = new DefaultChannelGroupFuture( this, futures, this.executor );
            }

            ReferenceCountUtil.release( message );
            return (ChannelGroupFuture) future;
        }
    }

    public ChannelGroup flush()
    {
        return this.flush( ChannelMatchers.all() );
    }

    public ChannelGroupFuture flushAndWrite( Object message )
    {
        return this.writeAndFlush( message );
    }

    public ChannelGroupFuture writeAndFlush( Object message )
    {
        return this.writeAndFlush( message, ChannelMatchers.all() );
    }

    public ChannelGroupFuture disconnect( ChannelMatcher matcher )
    {
        if ( matcher == null )
        {
            throw new NullPointerException( "matcher" );
        }
        else
        {
            Map<Channel,ChannelFuture> futures = new LinkedHashMap( this.size() );
            Iterator var3 = this.serverChannels.values().iterator();

            Channel c;
            while ( var3.hasNext() )
            {
                c = (Channel) var3.next();
                if ( matcher.matches( c ) )
                {
                    futures.put( c, c.disconnect() );
                }
            }

            var3 = this.nonServerChannels.values().iterator();

            while ( var3.hasNext() )
            {
                c = (Channel) var3.next();
                if ( matcher.matches( c ) )
                {
                    futures.put( c, c.disconnect() );
                }
            }

            return new DefaultChannelGroupFuture( this, futures, this.executor );
        }
    }

    public ChannelGroupFuture close( ChannelMatcher matcher )
    {
        if ( matcher == null )
        {
            throw new NullPointerException( "matcher" );
        }
        else
        {
            Map<Channel,ChannelFuture> futures = new LinkedHashMap( this.size() );
            if ( this.stayClosed )
            {
                this.closed = true;
            }

            Iterator var3 = this.serverChannels.values().iterator();

            Channel c;
            while ( var3.hasNext() )
            {
                c = (Channel) var3.next();
                if ( matcher.matches( c ) )
                {
                    futures.put( c, c.close() );
                }
            }

            var3 = this.nonServerChannels.values().iterator();

            while ( var3.hasNext() )
            {
                c = (Channel) var3.next();
                if ( matcher.matches( c ) )
                {
                    futures.put( c, c.close() );
                }
            }

            return new DefaultChannelGroupFuture( this, futures, this.executor );
        }
    }

    public ChannelGroupFuture deregister( ChannelMatcher matcher )
    {
        if ( matcher == null )
        {
            throw new NullPointerException( "matcher" );
        }
        else
        {
            Map<Channel,ChannelFuture> futures = new LinkedHashMap( this.size() );
            Iterator var3 = this.serverChannels.values().iterator();

            Channel c;
            while ( var3.hasNext() )
            {
                c = (Channel) var3.next();
                if ( matcher.matches( c ) )
                {
                    futures.put( c, c.deregister() );
                }
            }

            var3 = this.nonServerChannels.values().iterator();

            while ( var3.hasNext() )
            {
                c = (Channel) var3.next();
                if ( matcher.matches( c ) )
                {
                    futures.put( c, c.deregister() );
                }
            }

            return new DefaultChannelGroupFuture( this, futures, this.executor );
        }
    }

    public ChannelGroup flush( ChannelMatcher matcher )
    {
        Iterator var2 = this.nonServerChannels.values().iterator();

        while ( var2.hasNext() )
        {
            Channel c = (Channel) var2.next();
            if ( matcher.matches( c ) )
            {
                c.flush();
            }
        }

        return this;
    }

    public ChannelGroupFuture flushAndWrite( Object message, ChannelMatcher matcher )
    {
        return this.writeAndFlush( message, matcher );
    }

    public ChannelGroupFuture writeAndFlush( Object message, ChannelMatcher matcher )
    {
        return this.writeAndFlush( message, matcher, false );
    }

    public ChannelGroupFuture writeAndFlush( Object message, ChannelMatcher matcher, boolean voidPromise )
    {
        if ( message == null )
        {
            throw new NullPointerException( "message" );
        }
        else
        {
            Object future;
            if ( voidPromise )
            {
                Iterator var5 = this.nonServerChannels.values().iterator();

                while ( var5.hasNext() )
                {
                    Channel c = (Channel) var5.next();
                    if ( matcher.matches( c ) )
                    {
                        c.writeAndFlush( safeDuplicate( message ), c.voidPromise() );
                    }
                }

                future = this.voidFuture;
            }
            else
            {
                Map<Channel,ChannelFuture> futures = new LinkedHashMap( this.size() );
                Iterator var9 = this.nonServerChannels.values().iterator();

                while ( var9.hasNext() )
                {
                    Channel c = (Channel) var9.next();
                    if ( matcher.matches( c ) )
                    {
                        futures.put( c, c.writeAndFlush( safeDuplicate( message ) ) );
                    }
                }

                future = new DefaultChannelGroupFuture( this, futures, this.executor );
            }

            ReferenceCountUtil.release( message );
            return (ChannelGroupFuture) future;
        }
    }

    public ChannelGroupFuture newCloseFuture()
    {
        return this.newCloseFuture( ChannelMatchers.all() );
    }

    public ChannelGroupFuture newCloseFuture( ChannelMatcher matcher )
    {
        Map<Channel,ChannelFuture> futures = new LinkedHashMap( this.size() );
        Iterator var3 = this.serverChannels.values().iterator();

        Channel c;
        while ( var3.hasNext() )
        {
            c = (Channel) var3.next();
            if ( matcher.matches( c ) )
            {
                futures.put( c, c.closeFuture() );
            }
        }

        var3 = this.nonServerChannels.values().iterator();

        while ( var3.hasNext() )
        {
            c = (Channel) var3.next();
            if ( matcher.matches( c ) )
            {
                futures.put( c, c.closeFuture() );
            }
        }

        return new DefaultChannelGroupFuture( this, futures, this.executor );
    }

    public int hashCode()
    {
        return System.identityHashCode( this );
    }

    public boolean equals( Object o )
    {
        return this == o;
    }

    public int compareTo( ChannelGroup o )
    {
        int v = this.name().compareTo( o.name() );
        return v != 0 ? v : System.identityHashCode( this ) - System.identityHashCode( o );
    }

    public String toString()
    {
        return StringUtil.simpleClassName( (Object) this ) + "(name: " + this.name() + ", size: " + this.size() + ')';
    }
}
