package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.serialization;

public interface ClassResolver
{
    Class<?> resolve( String var1 ) throws ClassNotFoundException;
}
