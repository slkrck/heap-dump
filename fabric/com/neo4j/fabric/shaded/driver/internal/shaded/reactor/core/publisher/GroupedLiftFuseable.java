package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.BiFunction;

import org.reactivestreams.Publisher;

final class GroupedLiftFuseable<K, I, O> extends GroupedFlux<K,O> implements Scannable, Fuseable
{
    final BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter;
    final GroupedFlux<K,I> source;

    GroupedLiftFuseable( GroupedFlux<K,I> p, BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter )
    {
        this.source = (GroupedFlux) Objects.requireNonNull( p, "source" );
        this.lifter = lifter;
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    public K key()
    {
        return this.source.key();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public void subscribe( CoreSubscriber<? super O> actual )
    {
        CoreSubscriber<? super I> input = (CoreSubscriber) this.lifter.apply( this.source, actual );
        Objects.requireNonNull( input, "Lifted subscriber MUST NOT be null" );
        if ( actual instanceof Fuseable.QueueSubscription && !(input instanceof Fuseable.QueueSubscription) )
        {
            input = new FluxHide.SuppressFuseableSubscriber( (CoreSubscriber) input );
        }

        this.source.subscribe( (CoreSubscriber) input );
    }
}
