package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

import java.util.EventListener;

public interface GenericFutureListener<F extends Future<?>> extends EventListener
{
    void operationComplete( F var1 ) throws Exception;
}
