package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.marshalling;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToByteEncoder;
import org.jboss.marshalling.Marshaller;

@ChannelHandler.Sharable
public class CompatibleMarshallingEncoder extends MessageToByteEncoder<Object>
{
    private final MarshallerProvider provider;

    public CompatibleMarshallingEncoder( MarshallerProvider provider )
    {
        this.provider = provider;
    }

    protected void encode( ChannelHandlerContext ctx, Object msg, ByteBuf out ) throws Exception
    {
        Marshaller marshaller = this.provider.getMarshaller( ctx );
        marshaller.start( new ChannelBufferByteOutput( out ) );
        marshaller.writeObject( msg );
        marshaller.finish();
        marshaller.close();
    }
}
