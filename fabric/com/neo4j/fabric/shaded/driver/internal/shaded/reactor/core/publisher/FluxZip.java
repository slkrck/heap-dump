package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxZip<T, R> extends Flux<R> implements SourceProducer<R>
{
    final Publisher<? extends T>[] sources;
    final Iterable<? extends Publisher<? extends T>> sourcesIterable;
    final Function<? super Object[],? extends R> zipper;
    final Supplier<? extends Queue<T>> queueSupplier;
    final int prefetch;

    <U> FluxZip( Publisher<? extends T> p1, Publisher<? extends U> p2, BiFunction<? super T,? super U,? extends R> zipper2,
            Supplier<? extends Queue<T>> queueSupplier, int prefetch )
    {
        this( (Publisher[]) (new Publisher[]{(Publisher) Objects.requireNonNull( p1, "p1" ), (Publisher) Objects.requireNonNull( p2, "p2" )}),
                new FluxZip.PairwiseZipper( new BiFunction[]{(BiFunction) Objects.requireNonNull( zipper2, "zipper2" )} ), queueSupplier, prefetch );
    }

    FluxZip( Publisher<? extends T>[] sources, Function<? super Object[],? extends R> zipper, Supplier<? extends Queue<T>> queueSupplier, int prefetch )
    {
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.sources = (Publisher[]) Objects.requireNonNull( sources, "sources" );
            if ( sources.length == 0 )
            {
                throw new IllegalArgumentException( "at least one source is required" );
            }
            else
            {
                this.sourcesIterable = null;
                this.zipper = (Function) Objects.requireNonNull( zipper, "zipper" );
                this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
                this.prefetch = prefetch;
            }
        }
    }

    FluxZip( Iterable<? extends Publisher<? extends T>> sourcesIterable, Function<? super Object[],? extends R> zipper,
            Supplier<? extends Queue<T>> queueSupplier, int prefetch )
    {
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.sources = null;
            this.sourcesIterable = (Iterable) Objects.requireNonNull( sourcesIterable, "sourcesIterable" );
            this.zipper = (Function) Objects.requireNonNull( zipper, "zipper" );
            this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
            this.prefetch = prefetch;
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    @Nullable
    FluxZip<T,R> zipAdditionalSource( Publisher source, BiFunction zipper )
    {
        Publisher[] oldSources = this.sources;
        if ( oldSources != null && this.zipper instanceof FluxZip.PairwiseZipper )
        {
            int oldLen = oldSources.length;
            Publisher<? extends T>[] newSources = new Publisher[oldLen + 1];
            System.arraycopy( oldSources, 0, newSources, 0, oldLen );
            newSources[oldLen] = source;
            Function<Object[],R> z = ((FluxZip.PairwiseZipper) this.zipper).then( zipper );
            return new FluxZip( newSources, z, this.queueSupplier, this.prefetch );
        }
        else
        {
            return null;
        }
    }

    public void subscribe( CoreSubscriber<? super R> actual )
    {
        Publisher<? extends T>[] srcs = this.sources;
        if ( srcs != null )
        {
            this.handleArrayMode( actual, srcs );
        }
        else
        {
            this.handleIterableMode( actual, this.sourcesIterable );
        }
    }

    void handleIterableMode( CoreSubscriber<? super R> s, Iterable<? extends Publisher<? extends T>> sourcesIterable )
    {
        Object[] scalars = new Object[8];
        Publisher<? extends T>[] srcs = new Publisher[8];
        int n = 0;
        int sc = 0;

        for ( Iterator var7 = sourcesIterable.iterator(); var7.hasNext(); ++n )
        {
            Publisher<? extends T> p = (Publisher) var7.next();
            if ( p == null )
            {
                Operators.error( s,
                        Operators.onOperatorError( new NullPointerException( "The sourcesIterable returned a null Publisher" ), s.currentContext() ) );
                return;
            }

            if ( p instanceof Callable )
            {
                Callable callable = (Callable) p;

                Object v;
                try
                {
                    v = callable.call();
                }
                catch ( Throwable var13 )
                {
                    Operators.error( s, Operators.onOperatorError( var13, s.currentContext() ) );
                    return;
                }

                if ( v == null )
                {
                    Operators.complete( s );
                    return;
                }

                if ( n == scalars.length )
                {
                    Object[] b = new Object[n + (n >> 1)];
                    System.arraycopy( scalars, 0, b, 0, n );
                    Publisher<T>[] c = new Publisher[b.length];
                    System.arraycopy( srcs, 0, c, 0, n );
                    scalars = b;
                    srcs = c;
                }

                scalars[n] = v;
                ++sc;
            }
            else
            {
                if ( n == srcs.length )
                {
                    Object[] b = new Object[n + (n >> 1)];
                    System.arraycopy( scalars, 0, b, 0, n );
                    Publisher<T>[] c = new Publisher[b.length];
                    System.arraycopy( srcs, 0, c, 0, n );
                    scalars = b;
                    srcs = c;
                }

                srcs[n] = p;
            }
        }

        if ( n == 0 )
        {
            Operators.complete( s );
        }
        else
        {
            if ( n < scalars.length )
            {
                scalars = Arrays.copyOfRange( scalars, 0, n, scalars.getClass() );
            }

            this.handleBoth( s, srcs, scalars, n, sc );
        }
    }

    void handleArrayMode( CoreSubscriber<? super R> s, Publisher<? extends T>[] srcs )
    {
        Object[] scalars = null;
        int n = srcs.length;
        int sc = 0;

        for ( int j = 0; j < n; ++j )
        {
            Publisher<? extends T> p = srcs[j];
            if ( p == null )
            {
                Operators.error( s, new NullPointerException( "The sources contained a null Publisher" ) );
                return;
            }

            if ( p instanceof Callable )
            {
                Object v;
                try
                {
                    v = ((Callable) p).call();
                }
                catch ( Throwable var10 )
                {
                    Operators.error( s, Operators.onOperatorError( var10, s.currentContext() ) );
                    return;
                }

                if ( v == null )
                {
                    Operators.complete( s );
                    return;
                }

                if ( scalars == null )
                {
                    scalars = new Object[n];
                }

                scalars[j] = v;
                ++sc;
            }
        }

        this.handleBoth( s, srcs, scalars, n, sc );
    }

    void handleBoth( CoreSubscriber<? super R> s, Publisher<? extends T>[] srcs, @Nullable Object[] scalars, int n, int sc )
    {
        if ( sc != 0 && scalars != null )
        {
            if ( n != sc )
            {
                FluxZip.ZipSingleCoordinator<T,R> coordinator = new FluxZip.ZipSingleCoordinator( s, scalars, n, this.zipper );
                s.onSubscribe( coordinator );
                coordinator.subscribe( n, sc, srcs );
            }
            else
            {
                Operators.MonoSubscriber<R,R> sds = new Operators.MonoSubscriber( s );
                s.onSubscribe( sds );

                Object r;
                try
                {
                    r = Objects.requireNonNull( this.zipper.apply( scalars ), "The zipper returned a null value" );
                }
                catch ( Throwable var9 )
                {
                    s.onError( Operators.onOperatorError( var9, s.currentContext() ) );
                    return;
                }

                sds.complete( r );
            }
        }
        else
        {
            FluxZip.ZipCoordinator<T,R> coordinator = new FluxZip.ZipCoordinator( s, this.zipper, n, this.queueSupplier, this.prefetch );
            s.onSubscribe( coordinator );
            coordinator.subscribe( srcs, n );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.PREFETCH ? this.prefetch : null;
    }

    static final class PairwiseZipper<R> implements Function<Object[],R>
    {
        final BiFunction[] zippers;

        PairwiseZipper( BiFunction[] zippers )
        {
            this.zippers = zippers;
        }

        public R apply( Object[] args )
        {
            Object o = this.zippers[0].apply( args[0], args[1] );

            for ( int i = 1; i < this.zippers.length; ++i )
            {
                o = this.zippers[i].apply( o, args[i + 1] );
            }

            return o;
        }

        public FluxZip.PairwiseZipper then( BiFunction zipper )
        {
            BiFunction[] zippers = this.zippers;
            int n = zippers.length;
            BiFunction[] newZippers = new BiFunction[n + 1];
            System.arraycopy( zippers, 0, newZippers, 0, n );
            newZippers[n] = zipper;
            return new FluxZip.PairwiseZipper( newZippers );
        }
    }

    static final class ZipInner<T> implements InnerConsumer<T>
    {
        static final AtomicReferenceFieldUpdater<FluxZip.ZipInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxZip.ZipInner.class, Subscription.class, "s" );
        final FluxZip.ZipCoordinator<T,?> parent;
        final int prefetch;
        final int limit;
        final int index;
        final Supplier<? extends Queue<T>> queueSupplier;
        volatile Queue<T> queue;
        volatile Subscription s;
        long produced;
        volatile boolean done;
        int sourceMode;

        ZipInner( FluxZip.ZipCoordinator<T,?> parent, int prefetch, int index, Supplier<? extends Queue<T>> queueSupplier )
        {
            this.parent = parent;
            this.prefetch = prefetch;
            this.index = index;
            this.queueSupplier = queueSupplier;
            this.limit = Operators.unboundedOrLimit( prefetch );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    Fuseable.QueueSubscription<T> f = (Fuseable.QueueSubscription) s;
                    int m = f.requestFusion( 7 );
                    if ( m == 1 )
                    {
                        this.sourceMode = 1;
                        this.queue = f;
                        this.done = true;
                        this.parent.drain();
                        return;
                    }

                    if ( m == 2 )
                    {
                        this.sourceMode = 2;
                        this.queue = f;
                    }
                    else
                    {
                        this.queue = (Queue) this.queueSupplier.get();
                    }
                }
                else
                {
                    this.queue = (Queue) this.queueSupplier.get();
                }

                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        public void onNext( T t )
        {
            if ( this.sourceMode != 2 && !this.queue.offer( t ) )
            {
                this.onError(
                        Operators.onOperatorError( this.s, Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ),
                                this.currentContext() ) );
            }
            else
            {
                this.parent.drain();
            }
        }

        public Context currentContext()
        {
            return this.parent.actual.currentContext();
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.error( t, this.index );
            }
        }

        public void onComplete()
        {
            this.done = true;
            this.parent.drain();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.queue != null ? this.queue.size() : 0;
            }
            else if ( key != Scannable.Attr.TERMINATED )
            {
                return key == Scannable.Attr.PREFETCH ? this.prefetch : null;
            }
            else
            {
                return this.done && (this.queue == null || this.queue.isEmpty());
            }
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }

        void request( long n )
        {
            if ( this.sourceMode != 1 )
            {
                long p = this.produced + n;
                if ( p >= (long) this.limit )
                {
                    this.produced = 0L;
                    this.s.request( p );
                }
                else
                {
                    this.produced = p;
                }
            }
        }
    }

    static final class ZipCoordinator<T, R> implements InnerProducer<R>
    {
        static final AtomicIntegerFieldUpdater<FluxZip.ZipCoordinator> WIP = AtomicIntegerFieldUpdater.newUpdater( FluxZip.ZipCoordinator.class, "wip" );
        static final AtomicLongFieldUpdater<FluxZip.ZipCoordinator> REQUESTED = AtomicLongFieldUpdater.newUpdater( FluxZip.ZipCoordinator.class, "requested" );
        static final AtomicReferenceFieldUpdater<FluxZip.ZipCoordinator,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( FluxZip.ZipCoordinator.class, Throwable.class, "error" );
        final CoreSubscriber<? super R> actual;
        final FluxZip.ZipInner<T>[] subscribers;
        final Function<? super Object[],? extends R> zipper;
        final Object[] current;
        volatile int wip;
        volatile long requested;
        volatile Throwable error;
        volatile boolean cancelled;

        ZipCoordinator( CoreSubscriber<? super R> actual, Function<? super Object[],? extends R> zipper, int n, Supplier<? extends Queue<T>> queueSupplier,
                int prefetch )
        {
            this.actual = actual;
            this.zipper = zipper;
            FluxZip.ZipInner<T>[] a = new FluxZip.ZipInner[n];

            for ( int i = 0; i < n; ++i )
            {
                a[i] = new FluxZip.ZipInner( this, prefetch, i, queueSupplier );
            }

            this.current = new Object[n];
            this.subscribers = a;
        }

        void subscribe( Publisher<? extends T>[] sources, int n )
        {
            FluxZip.ZipInner<T>[] a = this.subscribers;

            for ( int i = 0; i < n; ++i )
            {
                if ( this.cancelled || this.error != null )
                {
                    return;
                }

                sources[i].subscribe( a[i] );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.drain();
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.cancelAll();
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.cancelled : InnerProducer.super.scanUnsafe( key );
            }
        }

        void error( Throwable e, int index )
        {
            if ( Exceptions.addThrowable( ERROR, this, e ) )
            {
                this.drain();
            }
            else
            {
                Operators.onErrorDropped( e, this.actual.currentContext() );
            }
        }

        void cancelAll()
        {
            FluxZip.ZipInner[] var1 = this.subscribers;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                FluxZip.ZipInner<T> s = var1[var3];
                s.cancel();
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                CoreSubscriber<? super R> a = this.actual;
                FluxZip.ZipInner<T>[] qs = this.subscribers;
                int n = qs.length;
                Object[] values = this.current;
                int missed = 1;

                do
                {
                    long r = this.requested;
                    long e = 0L;

                    Throwable ex;
                    Throwable ex;
                    while ( r != e )
                    {
                        if ( this.cancelled )
                        {
                            return;
                        }

                        if ( this.error != null )
                        {
                            this.cancelAll();
                            ex = Exceptions.terminate( ERROR, this );
                            a.onError( ex );
                            return;
                        }

                        boolean empty = false;

                        for ( int j = 0; j < n; ++j )
                        {
                            FluxZip.ZipInner<T> inner = qs[j];
                            if ( values[j] == null )
                            {
                                try
                                {
                                    boolean d = inner.done;
                                    Queue<T> q = inner.queue;
                                    T v = q != null ? q.poll() : null;
                                    boolean sourceEmpty = v == null;
                                    if ( d && sourceEmpty )
                                    {
                                        this.cancelAll();
                                        a.onComplete();
                                        return;
                                    }

                                    if ( !sourceEmpty )
                                    {
                                        values[j] = v;
                                    }
                                    else
                                    {
                                        empty = true;
                                    }
                                }
                                catch ( Throwable var18 )
                                {
                                    Throwable ex = Operators.onOperatorError( var18, this.actual.currentContext() );
                                    this.cancelAll();
                                    Exceptions.addThrowable( ERROR, this, ex );
                                    ex = Exceptions.terminate( ERROR, this );
                                    a.onError( ex );
                                    return;
                                }
                            }
                        }

                        if ( empty )
                        {
                            break;
                        }

                        Object v;
                        try
                        {
                            v = Objects.requireNonNull( this.zipper.apply( values.clone() ), "The zipper returned a null value" );
                        }
                        catch ( Throwable var17 )
                        {
                            ex = Operators.onOperatorError( (Subscription) null, var17, values.clone(), this.actual.currentContext() );
                            this.cancelAll();
                            Exceptions.addThrowable( ERROR, this, ex );
                            ex = Exceptions.terminate( ERROR, this );
                            a.onError( ex );
                            return;
                        }

                        a.onNext( v );
                        ++e;
                        Arrays.fill( values, (Object) null );
                    }

                    int j;
                    FluxZip.ZipInner inner;
                    if ( r == e )
                    {
                        if ( this.cancelled )
                        {
                            return;
                        }

                        if ( this.error != null )
                        {
                            this.cancelAll();
                            ex = Exceptions.terminate( ERROR, this );
                            a.onError( ex );
                            return;
                        }

                        for ( j = 0; j < n; ++j )
                        {
                            inner = qs[j];
                            if ( values[j] == null )
                            {
                                try
                                {
                                    boolean d = inner.done;
                                    Queue<T> q = inner.queue;
                                    T v = q != null ? q.poll() : null;
                                    boolean empty = v == null;
                                    if ( d && empty )
                                    {
                                        this.cancelAll();
                                        a.onComplete();
                                        return;
                                    }

                                    if ( !empty )
                                    {
                                        values[j] = v;
                                    }
                                }
                                catch ( Throwable var19 )
                                {
                                    ex = Operators.onOperatorError( (Subscription) null, var19, values, this.actual.currentContext() );
                                    this.cancelAll();
                                    Exceptions.addThrowable( ERROR, this, ex );
                                    ex = Exceptions.terminate( ERROR, this );
                                    a.onError( ex );
                                    return;
                                }
                            }
                        }
                    }

                    if ( e != 0L )
                    {
                        for ( j = 0; j < n; ++j )
                        {
                            inner = qs[j];
                            inner.request( e );
                        }

                        if ( r != Long.MAX_VALUE )
                        {
                            REQUESTED.addAndGet( this, -e );
                        }
                    }

                    missed = WIP.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }
    }

    static final class ZipSingleSubscriber<T> implements InnerConsumer<T>, Disposable
    {
        static final AtomicReferenceFieldUpdater<FluxZip.ZipSingleSubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxZip.ZipSingleSubscriber.class, Subscription.class, "s" );
        final FluxZip.ZipSingleCoordinator<T,?> parent;
        final int index;
        volatile Subscription s;
        boolean done;

        ZipSingleSubscriber( FluxZip.ZipSingleCoordinator<T,?> parent, int index )
        {
            this.parent = parent;
            this.index = index;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.parent.scalars[this.index] == null ? 0 : 1 : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                this.s = s;
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.parent.currentContext() );
            }
            else
            {
                this.done = true;
                Operators.terminate( S, this );
                this.parent.next( t, this.index );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.parent.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.error( t, this.index );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.parent.complete( this.index );
            }
        }

        public void dispose()
        {
            Operators.terminate( S, this );
        }
    }

    static final class ZipSingleCoordinator<T, R> extends Operators.MonoSubscriber<R,R>
    {
        static final AtomicIntegerFieldUpdater<FluxZip.ZipSingleCoordinator> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxZip.ZipSingleCoordinator.class, "wip" );
        final Function<? super Object[],? extends R> zipper;
        final Object[] scalars;
        final FluxZip.ZipSingleSubscriber<T>[] subscribers;
        volatile int wip;

        ZipSingleCoordinator( CoreSubscriber<? super R> subscriber, Object[] scalars, int n, Function<? super Object[],? extends R> zipper )
        {
            super( subscriber );
            this.zipper = zipper;
            this.scalars = scalars;
            FluxZip.ZipSingleSubscriber<T>[] a = new FluxZip.ZipSingleSubscriber[n];

            for ( int i = 0; i < n; ++i )
            {
                if ( scalars[i] == null )
                {
                    a[i] = new FluxZip.ZipSingleSubscriber( this, i );
                }
            }

            this.subscribers = a;
        }

        void subscribe( int n, int sc, Publisher<? extends T>[] sources )
        {
            WIP.lazySet( this, n - sc );
            FluxZip.ZipSingleSubscriber<T>[] a = this.subscribers;

            for ( int i = 0; i < n && this.wip > 0 && !this.isCancelled(); ++i )
            {
                FluxZip.ZipSingleSubscriber<T> s = a[i];
                if ( s != null )
                {
                    sources[i].subscribe( s );
                }
            }
        }

        void next( T value, int index )
        {
            Object[] a = this.scalars;
            a[index] = value;
            if ( WIP.decrementAndGet( this ) == 0 )
            {
                Object r;
                try
                {
                    r = Objects.requireNonNull( this.zipper.apply( a ), "The zipper returned a null value" );
                }
                catch ( Throwable var6 )
                {
                    this.actual.onError( Operators.onOperatorError( this, var6, value, this.actual.currentContext() ) );
                    return;
                }

                this.complete( r );
            }
        }

        void error( Throwable e, int index )
        {
            if ( WIP.getAndSet( this, 0 ) > 0 )
            {
                this.cancelAll();
                this.actual.onError( e );
            }
            else
            {
                Operators.onErrorDropped( e, this.actual.currentContext() );
            }
        }

        void complete( int index )
        {
            if ( WIP.getAndSet( this, 0 ) > 0 )
            {
                this.cancelAll();
                this.actual.onComplete();
            }
        }

        public void cancel()
        {
            super.cancel();
            this.cancelAll();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.wip == 0;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.wip > 0 ? this.scalars.length : 0 : super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        void cancelAll()
        {
            FluxZip.ZipSingleSubscriber[] var1 = this.subscribers;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                FluxZip.ZipSingleSubscriber<T> s = var1[var3];
                if ( s != null )
                {
                    s.dispose();
                }
            }
        }
    }
}
