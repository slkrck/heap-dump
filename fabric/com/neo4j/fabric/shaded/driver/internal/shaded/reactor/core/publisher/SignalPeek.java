package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.function.Consumer;
import java.util.function.LongConsumer;

import org.reactivestreams.Subscription;

interface SignalPeek<T> extends Scannable
{
    @Nullable
    Consumer<? super Subscription> onSubscribeCall();

    @Nullable
    Consumer<? super T> onNextCall();

    @Nullable
    Consumer<? super Throwable> onErrorCall();

    @Nullable
    Runnable onCompleteCall();

    @Nullable
    Runnable onAfterTerminateCall();

    @Nullable
    LongConsumer onRequestCall();

    @Nullable
    Runnable onCancelCall();

    @Nullable
    default Consumer<? super T> onAfterNextCall()
    {
        return null;
    }

    @Nullable
    default Consumer<? super Context> onCurrentContextCall()
    {
        return null;
    }
}
