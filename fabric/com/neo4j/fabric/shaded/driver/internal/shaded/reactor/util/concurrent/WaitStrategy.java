package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.LongSupplier;

/**
 * @deprecated
 */
@Deprecated
public abstract class WaitStrategy
{
    public static WaitStrategy blocking()
    {
        return new WaitStrategy.Blocking();
    }

    public static WaitStrategy busySpin()
    {
        return WaitStrategy.BusySpin.INSTANCE;
    }

    public static boolean isAlert( Throwable t )
    {
        return t == WaitStrategy.AlertException.INSTANCE;
    }

    public static WaitStrategy liteBlocking()
    {
        return new WaitStrategy.LiteBlocking();
    }

    public static WaitStrategy parking()
    {
        return WaitStrategy.Parking.INSTANCE;
    }

    public static WaitStrategy parking( int retries )
    {
        return new WaitStrategy.Parking( retries );
    }

    public static WaitStrategy phasedOff( long spinTimeout, long yieldTimeout, TimeUnit units, WaitStrategy delegate )
    {
        return new WaitStrategy.PhasedOff( spinTimeout, yieldTimeout, units, delegate );
    }

    public static WaitStrategy phasedOffLiteLock( long spinTimeout, long yieldTimeout, TimeUnit units )
    {
        return phasedOff( spinTimeout, yieldTimeout, units, liteBlocking() );
    }

    public static WaitStrategy phasedOffLock( long spinTimeout, long yieldTimeout, TimeUnit units )
    {
        return phasedOff( spinTimeout, yieldTimeout, units, blocking() );
    }

    public static WaitStrategy phasedOffSleep( long spinTimeout, long yieldTimeout, TimeUnit units )
    {
        return phasedOff( spinTimeout, yieldTimeout, units, parking( 0 ) );
    }

    public static WaitStrategy sleeping()
    {
        return WaitStrategy.Sleeping.INSTANCE;
    }

    public static WaitStrategy yielding()
    {
        return WaitStrategy.Yielding.INSTANCE;
    }

    public static void alert()
    {
        throw WaitStrategy.AlertException.INSTANCE;
    }

    public void signalAllWhenBlocking()
    {
    }

    public abstract long waitFor( long var1, LongSupplier var3, Runnable var4 ) throws InterruptedException;

    static final class Yielding extends WaitStrategy
    {
        static final WaitStrategy.Yielding INSTANCE = new WaitStrategy.Yielding();
        private static final int SPIN_TRIES = 100;

        public long waitFor( long sequence, LongSupplier cursor, Runnable barrier ) throws InterruptedException
        {
            long availableSequence;
            for ( int counter = 100; (availableSequence = cursor.getAsLong()) < sequence; counter = this.applyWaitMethod( barrier, counter ) )
            {
            }

            return availableSequence;
        }

        private int applyWaitMethod( Runnable barrier, int counter ) throws WaitStrategy.AlertException
        {
            barrier.run();
            if ( 0 == counter )
            {
                Thread.yield();
            }
            else
            {
                --counter;
            }

            return counter;
        }
    }

    static final class Parking extends WaitStrategy
    {
        static final WaitStrategy.Parking INSTANCE = new WaitStrategy.Parking();
        private static final int DEFAULT_RETRIES = 200;
        private final int retries;

        Parking()
        {
            this( 200 );
        }

        Parking( int retries )
        {
            this.retries = retries;
        }

        public long waitFor( long sequence, LongSupplier cursor, Runnable barrier ) throws InterruptedException
        {
            long availableSequence;
            for ( int counter = this.retries; (availableSequence = cursor.getAsLong()) < sequence; counter = this.applyWaitMethod( barrier, counter ) )
            {
            }

            return availableSequence;
        }

        private int applyWaitMethod( Runnable barrier, int counter ) throws WaitStrategy.AlertException
        {
            barrier.run();
            if ( counter > 100 )
            {
                --counter;
            }
            else if ( counter > 0 )
            {
                --counter;
                Thread.yield();
            }
            else
            {
                LockSupport.parkNanos( 1L );
            }

            return counter;
        }
    }

    static final class PhasedOff extends WaitStrategy
    {
        private static final int SPIN_TRIES = 10000;
        private final long spinTimeoutNanos;
        private final long yieldTimeoutNanos;
        private final WaitStrategy fallbackStrategy;

        PhasedOff( long spinTimeout, long yieldTimeout, TimeUnit units, WaitStrategy fallbackStrategy )
        {
            this.spinTimeoutNanos = units.toNanos( spinTimeout );
            this.yieldTimeoutNanos = this.spinTimeoutNanos + units.toNanos( yieldTimeout );
            this.fallbackStrategy = fallbackStrategy;
        }

        public void signalAllWhenBlocking()
        {
            this.fallbackStrategy.signalAllWhenBlocking();
        }

        public long waitFor( long sequence, LongSupplier cursor, Runnable barrier ) throws InterruptedException
        {
            long startTime = 0L;

            long availableSequence;
            for ( int counter = 10000; (availableSequence = cursor.getAsLong()) < sequence; barrier.run() )
            {
                --counter;
                if ( 0 == counter )
                {
                    if ( 0L == startTime )
                    {
                        startTime = System.nanoTime();
                    }
                    else
                    {
                        long timeDelta = System.nanoTime() - startTime;
                        if ( timeDelta > this.yieldTimeoutNanos )
                        {
                            return this.fallbackStrategy.waitFor( sequence, cursor, barrier );
                        }

                        if ( timeDelta > this.spinTimeoutNanos )
                        {
                            Thread.yield();
                        }
                    }

                    counter = 10000;
                }
            }

            return availableSequence;
        }
    }

    static final class LiteBlocking extends WaitStrategy
    {
        private final Lock lock = new ReentrantLock();
        private final Condition processorNotifyCondition;
        private final AtomicBoolean signalNeeded;

        LiteBlocking()
        {
            this.processorNotifyCondition = this.lock.newCondition();
            this.signalNeeded = new AtomicBoolean( false );
        }

        public void signalAllWhenBlocking()
        {
            if ( this.signalNeeded.getAndSet( false ) )
            {
                this.lock.lock();

                try
                {
                    this.processorNotifyCondition.signalAll();
                }
                finally
                {
                    this.lock.unlock();
                }
            }
        }

        public long waitFor( long sequence, LongSupplier cursorSequence, Runnable barrier ) throws InterruptedException
        {
            if ( cursorSequence.getAsLong() < sequence )
            {
                this.lock.lock();

                try
                {
                    do
                    {
                        this.signalNeeded.getAndSet( true );
                        if ( cursorSequence.getAsLong() >= sequence )
                        {
                            break;
                        }

                        barrier.run();
                        this.processorNotifyCondition.await();
                    }
                    while ( cursorSequence.getAsLong() < sequence );
                }
                finally
                {
                    this.lock.unlock();
                }
            }

            long availableSequence;
            while ( (availableSequence = cursorSequence.getAsLong()) < sequence )
            {
                barrier.run();
            }

            return availableSequence;
        }
    }

    static final class Sleeping extends WaitStrategy
    {
        static final WaitStrategy.Sleeping INSTANCE = new WaitStrategy.Sleeping();

        public long waitFor( long sequence, LongSupplier cursor, Runnable barrier ) throws InterruptedException
        {
            long availableSequence;
            while ( (availableSequence = cursor.getAsLong()) < sequence )
            {
                barrier.run();
                Thread.sleep( 1L );
            }

            return availableSequence;
        }
    }

    static final class BusySpin extends WaitStrategy
    {
        static final WaitStrategy.BusySpin INSTANCE = new WaitStrategy.BusySpin();

        public long waitFor( long sequence, LongSupplier cursor, Runnable barrier ) throws InterruptedException
        {
            long availableSequence;
            while ( (availableSequence = cursor.getAsLong()) < sequence )
            {
                barrier.run();
            }

            return availableSequence;
        }
    }

    static final class Blocking extends WaitStrategy
    {
        private final Lock lock = new ReentrantLock();
        private final Condition processorNotifyCondition;

        Blocking()
        {
            this.processorNotifyCondition = this.lock.newCondition();
        }

        public void signalAllWhenBlocking()
        {
            this.lock.lock();

            try
            {
                this.processorNotifyCondition.signalAll();
            }
            finally
            {
                this.lock.unlock();
            }
        }

        public long waitFor( long sequence, LongSupplier cursorSequence, Runnable barrier ) throws InterruptedException
        {
            if ( cursorSequence.getAsLong() < sequence )
            {
                this.lock.lock();

                try
                {
                    while ( cursorSequence.getAsLong() < sequence )
                    {
                        barrier.run();
                        this.processorNotifyCondition.await();
                    }
                }
                finally
                {
                    this.lock.unlock();
                }
            }

            long availableSequence;
            while ( (availableSequence = cursorSequence.getAsLong()) < sequence )
            {
                barrier.run();
            }

            return availableSequence;
        }
    }

    static final class AlertException extends RuntimeException
    {
        public static final WaitStrategy.AlertException INSTANCE = new WaitStrategy.AlertException();

        private AlertException()
        {
        }

        public Throwable fillInStackTrace()
        {
            return this;
        }
    }
}
