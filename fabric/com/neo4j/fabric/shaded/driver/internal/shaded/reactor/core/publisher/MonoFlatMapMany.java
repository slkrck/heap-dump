package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class MonoFlatMapMany<T, R> extends FluxFromMonoOperator<T,R>
{
    final Function<? super T,? extends Publisher<? extends R>> mapper;

    MonoFlatMapMany( Mono<? extends T> source, Function<? super T,? extends Publisher<? extends R>> mapper )
    {
        super( source );
        this.mapper = mapper;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        return FluxFlatMap.trySubscribeScalarMap( this.source, actual, this.mapper, false, false ) ? null
                                                                                                   : new MonoFlatMapMany.FlatMapManyMain( actual, this.mapper );
    }

    static final class FlatMapManyInner<R> implements InnerConsumer<R>
    {
        final MonoFlatMapMany.FlatMapManyMain<?,R> parent;
        final CoreSubscriber<? super R> actual;

        FlatMapManyInner( MonoFlatMapMany.FlatMapManyMain<?,R> parent, CoreSubscriber<? super R> actual )
        {
            this.parent = parent;
            this.actual = actual;
        }

        public Context currentContext()
        {
            return this.actual.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent.inner;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.parent.requested : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            this.parent.onSubscribeInner( s );
        }

        public void onNext( R t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }
    }

    static final class FlatMapManyMain<T, R> implements InnerOperator<T,R>
    {
        static final AtomicReferenceFieldUpdater<MonoFlatMapMany.FlatMapManyMain,Subscription> INNER =
                AtomicReferenceFieldUpdater.newUpdater( MonoFlatMapMany.FlatMapManyMain.class, Subscription.class, "inner" );
        static final AtomicLongFieldUpdater<MonoFlatMapMany.FlatMapManyMain> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( MonoFlatMapMany.FlatMapManyMain.class, "requested" );
        final CoreSubscriber<? super R> actual;
        final Function<? super T,? extends Publisher<? extends R>> mapper;
        Subscription main;
        volatile Subscription inner;
        volatile long requested;
        boolean hasValue;

        FlatMapManyMain( CoreSubscriber<? super R> actual, Function<? super T,? extends Publisher<? extends R>> mapper )
        {
            this.actual = actual;
            this.mapper = mapper;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.main : InnerOperator.super.scanUnsafe( key );
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( Scannable.from( this.inner ) );
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            Subscription a = this.inner;
            if ( a != null )
            {
                a.request( n );
            }
            else if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                a = this.inner;
                if ( a != null )
                {
                    n = REQUESTED.getAndSet( this, 0L );
                    if ( n != 0L )
                    {
                        a.request( n );
                    }
                }
            }
        }

        public void cancel()
        {
            this.main.cancel();
            Operators.terminate( INNER, this );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.main, s ) )
            {
                this.main = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        void onSubscribeInner( Subscription s )
        {
            if ( Operators.setOnce( INNER, this, s ) )
            {
                long r = REQUESTED.getAndSet( this, 0L );
                if ( r != 0L )
                {
                    s.request( r );
                }
            }
        }

        public void onNext( T t )
        {
            this.hasValue = true;

            Publisher p;
            try
            {
                p = (Publisher) Objects.requireNonNull( this.mapper.apply( t ), "The mapper returned a null Publisher." );
            }
            catch ( Throwable var6 )
            {
                this.actual.onError( Operators.onOperatorError( this, var6, t, this.actual.currentContext() ) );
                return;
            }

            if ( p instanceof Callable )
            {
                Object v;
                try
                {
                    v = ((Callable) p).call();
                }
                catch ( Throwable var5 )
                {
                    this.actual.onError( Operators.onOperatorError( this, var5, t, this.actual.currentContext() ) );
                    return;
                }

                if ( v == null )
                {
                    this.actual.onComplete();
                }
                else
                {
                    this.onSubscribeInner( Operators.scalarSubscription( this.actual, v ) );
                }
            }
            else
            {
                p.subscribe( new MonoFlatMapMany.FlatMapManyInner( this, this.actual ) );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.hasValue )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.hasValue )
            {
                this.actual.onComplete();
            }
        }
    }
}
