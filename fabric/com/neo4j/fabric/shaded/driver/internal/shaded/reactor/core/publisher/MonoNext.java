package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

import org.reactivestreams.Subscription;

final class MonoNext<T> extends MonoFromFluxOperator<T,T>
{
    MonoNext( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoNext.NextSubscriber( actual );
    }

    static final class NextSubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicIntegerFieldUpdater<MonoNext.NextSubscriber> WIP = AtomicIntegerFieldUpdater.newUpdater( MonoNext.NextSubscriber.class, "wip" );
        final CoreSubscriber<? super T> actual;
        Subscription s;
        boolean done;
        volatile int wip;

        NextSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.s.cancel();
                this.actual.onNext( t );
                this.onComplete();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        public void request( long n )
        {
            if ( WIP.compareAndSet( this, 0, 1 ) )
            {
                this.s.request( Long.MAX_VALUE );
            }
        }

        public void cancel()
        {
            this.s.cancel();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }
    }
}
