package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

public enum ClientAuth
{
    NONE,
    OPTIONAL,
    REQUIRE;
}
