package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.bytes;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

public class ByteArrayDecoder extends MessageToMessageDecoder<ByteBuf>
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out ) throws Exception
    {
        out.add( ByteBufUtil.getBytes( msg ) );
    }
}
