package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

public interface Constant<T extends Constant<T>> extends Comparable<T>
{
    int id();

    String name();
}
