package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

final class FluxStream<T> extends Flux<T> implements Fuseable, SourceProducer<T>
{
    final Supplier<? extends Stream<? extends T>> streamSupplier;

    FluxStream( Supplier<? extends Stream<? extends T>> streamSupplier )
    {
        this.streamSupplier = (Supplier) Objects.requireNonNull( streamSupplier, "streamSupplier" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Stream stream;
        try
        {
            stream = (Stream) Objects.requireNonNull( this.streamSupplier.get(), "The stream supplier returned a null Stream" );
        }
        catch ( Throwable var6 )
        {
            Operators.error( actual, Operators.onOperatorError( var6, actual.currentContext() ) );
            return;
        }

        Iterator it;
        try
        {
            it = (Iterator) Objects.requireNonNull( stream.iterator(), "The stream returned a null Iterator" );
        }
        catch ( Throwable var5 )
        {
            Operators.error( actual, Operators.onOperatorError( var5, actual.currentContext() ) );
            return;
        }

        FluxIterable.subscribe( actual, it, stream::close );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
