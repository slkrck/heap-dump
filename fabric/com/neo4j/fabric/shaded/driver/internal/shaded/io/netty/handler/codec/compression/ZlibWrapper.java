package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.compression;

public enum ZlibWrapper
{
    ZLIB,
    GZIP,
    NONE,
    ZLIB_OR_NONE;
}
