package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

final class ParallelLog<T> extends ParallelFlux<T> implements Scannable
{
    final ParallelFlux<T> source;
    final SignalPeek<T> log;

    ParallelLog( ParallelFlux<T> source, SignalPeek<T> log )
    {
        this.source = source;
        this.log = log;
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<? super T>[] parents = new CoreSubscriber[n];
            boolean conditional = subscribers[0] instanceof Fuseable.ConditionalSubscriber;

            for ( int i = 0; i < n; ++i )
            {
                if ( conditional )
                {
                    parents[i] = new FluxPeekFuseable.PeekConditionalSubscriber( (Fuseable.ConditionalSubscriber) subscribers[i], this.log );
                }
                else
                {
                    parents[i] = new FluxPeek.PeekSubscriber( subscribers[i], this.log );
                }
            }

            this.source.subscribe( parents );
        }
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }
}
