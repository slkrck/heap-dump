package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.local;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.DefaultEventLoopGroup;

import java.util.concurrent.ThreadFactory;

/**
 * @deprecated
 */
@Deprecated
public class LocalEventLoopGroup extends DefaultEventLoopGroup
{
    public LocalEventLoopGroup()
    {
    }

    public LocalEventLoopGroup( int nThreads )
    {
        super( nThreads );
    }

    public LocalEventLoopGroup( int nThreads, ThreadFactory threadFactory )
    {
        super( nThreads, threadFactory );
    }
}
