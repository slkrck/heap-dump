package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.stream.Stream;

import org.reactivestreams.Subscription;

public final class EmitterProcessor<T> extends FluxProcessor<T,T>
{
    static final FluxPublish.PubSubInner[] EMPTY = new FluxPublish.PublishInner[0];
    static final AtomicReferenceFieldUpdater<EmitterProcessor,Subscription> S =
            AtomicReferenceFieldUpdater.newUpdater( EmitterProcessor.class, Subscription.class, "s" );
    static final AtomicReferenceFieldUpdater<EmitterProcessor,FluxPublish.PubSubInner[]> SUBSCRIBERS =
            AtomicReferenceFieldUpdater.newUpdater( EmitterProcessor.class, FluxPublish.PubSubInner[].class, "subscribers" );
    static final AtomicIntegerFieldUpdater<EmitterProcessor> WIP = AtomicIntegerFieldUpdater.newUpdater( EmitterProcessor.class, "wip" );
    static final AtomicReferenceFieldUpdater<EmitterProcessor,Throwable> ERROR =
            AtomicReferenceFieldUpdater.newUpdater( EmitterProcessor.class, Throwable.class, "error" );
    final int prefetch;
    final boolean autoCancel;
    volatile Subscription s;
    volatile FluxPublish.PubSubInner<T>[] subscribers;
    volatile int wip;
    volatile Queue<T> queue;
    int sourceMode;
    volatile boolean done;
    volatile Throwable error;

    EmitterProcessor( boolean autoCancel, int prefetch )
    {
        if ( prefetch < 1 )
        {
            throw new IllegalArgumentException( "bufferSize must be strictly positive, was: " + prefetch );
        }
        else
        {
            this.autoCancel = autoCancel;
            this.prefetch = prefetch;
            SUBSCRIBERS.lazySet( this, EMPTY );
        }
    }

    public static <E> EmitterProcessor<E> create()
    {
        return create( Queues.SMALL_BUFFER_SIZE, true );
    }

    public static <E> EmitterProcessor<E> create( boolean autoCancel )
    {
        return create( Queues.SMALL_BUFFER_SIZE, autoCancel );
    }

    public static <E> EmitterProcessor<E> create( int bufferSize )
    {
        return create( bufferSize, true );
    }

    public static <E> EmitterProcessor<E> create( int bufferSize, boolean autoCancel )
    {
        return new EmitterProcessor( autoCancel, bufferSize );
    }

    public Stream<? extends Scannable> inners()
    {
        return Stream.of( this.subscribers );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Objects.requireNonNull( actual, "subscribe" );
        EmitterProcessor.EmitterInner<T> inner = new EmitterProcessor.EmitterInner( actual, this );
        actual.onSubscribe( inner );
        if ( !inner.isCancelled() )
        {
            if ( this.add( inner ) )
            {
                if ( inner.isCancelled() )
                {
                    this.remove( inner );
                }

                this.drain();
            }
            else
            {
                Throwable e = this.error;
                if ( e != null )
                {
                    inner.actual.onError( e );
                }
                else
                {
                    inner.actual.onComplete();
                }
            }
        }
    }

    public int getPending()
    {
        Queue<T> q = this.queue;
        return q != null ? q.size() : 0;
    }

    public void onSubscribe( Subscription s )
    {
        if ( Operators.setOnce( S, this, s ) )
        {
            if ( s instanceof Fuseable.QueueSubscription )
            {
                Fuseable.QueueSubscription<T> f = (Fuseable.QueueSubscription) s;
                int m = f.requestFusion( 3 );
                if ( m == 1 )
                {
                    this.sourceMode = m;
                    this.queue = f;
                    this.drain();
                    return;
                }

                if ( m == 2 )
                {
                    this.sourceMode = m;
                    this.queue = f;
                    s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
                    return;
                }
            }

            this.queue = (Queue) Queues.get( this.prefetch ).get();
            s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
        }
    }

    public void onNext( T t )
    {
        if ( this.done )
        {
            Operators.onNextDropped( t, this.currentContext() );
        }
        else if ( this.sourceMode == 2 )
        {
            this.drain();
        }
        else
        {
            Objects.requireNonNull( t, "onNext" );
            Queue<T> q = this.queue;
            if ( q == null )
            {
                if ( Operators.setOnce( S, this, Operators.emptySubscription() ) )
                {
                    q = (Queue) Queues.get( this.prefetch ).get();
                    this.queue = q;
                }
                else
                {
                    do
                    {
                        if ( this.isDisposed() )
                        {
                            return;
                        }

                        q = this.queue;
                    }
                    while ( q == null );
                }
            }

            while ( !q.offer( t ) )
            {
                LockSupport.parkNanos( 10L );
            }

            this.drain();
        }
    }

    public void onError( Throwable t )
    {
        Objects.requireNonNull( t, "onError" );
        if ( this.done )
        {
            Operators.onErrorDropped( t, this.currentContext() );
        }
        else
        {
            if ( Exceptions.addThrowable( ERROR, this, t ) )
            {
                this.done = true;
                this.drain();
            }
            else
            {
                Operators.onErrorDroppedMulticast( t );
            }
        }
    }

    public void onComplete()
    {
        if ( !this.done )
        {
            this.done = true;
            this.drain();
        }
    }

    @Nullable
    public Throwable getError()
    {
        return this.error;
    }

    public boolean isCancelled()
    {
        return Operators.cancelledSubscription() == this.s;
    }

    public final int getBufferSize()
    {
        return this.prefetch;
    }

    public boolean isTerminated()
    {
        return this.done && this.getPending() == 0;
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.s;
        }
        else if ( key == Scannable.Attr.BUFFERED )
        {
            return this.getPending();
        }
        else if ( key == Scannable.Attr.CANCELLED )
        {
            return this.isCancelled();
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : super.scanUnsafe( key );
        }
    }

    final void drain()
    {
        if ( WIP.getAndIncrement( this ) == 0 )
        {
            int missed = 1;

            while ( true )
            {
                boolean d = this.done;
                Queue<T> q = this.queue;
                boolean empty = q == null || q.isEmpty();
                if ( this.checkTerminated( d, empty ) )
                {
                    return;
                }

                FluxPublish.PubSubInner<T>[] a = this.subscribers;
                if ( a != EMPTY && !empty )
                {
                    long maxRequested = Long.MAX_VALUE;
                    int len = a.length;
                    int cancel = 0;
                    FluxPublish.PubSubInner[] var10 = a;
                    int var11 = a.length;

                    for ( int var12 = 0; var12 < var11; ++var12 )
                    {
                        FluxPublish.PubSubInner<T> inner = var10[var12];
                        long r = inner.requested;
                        if ( r >= 0L )
                        {
                            maxRequested = Math.min( maxRequested, r );
                        }
                        else
                        {
                            ++cancel;
                        }
                    }

                    if ( len == cancel )
                    {
                        Object v;
                        try
                        {
                            v = q.poll();
                        }
                        catch ( Throwable var17 )
                        {
                            Exceptions.addThrowable( ERROR, this, Operators.onOperatorError( this.s, var17, this.currentContext() ) );
                            d = true;
                            v = null;
                        }

                        if ( this.checkTerminated( d, v == null ) )
                        {
                            return;
                        }

                        if ( this.sourceMode != 1 )
                        {
                            this.s.request( 1L );
                        }
                        continue;
                    }

                    int e;
                    for ( e = 0; (long) e < maxRequested && cancel != Integer.MIN_VALUE; ++e )
                    {
                        d = this.done;

                        Object v;
                        try
                        {
                            v = q.poll();
                        }
                        catch ( Throwable var16 )
                        {
                            Exceptions.addThrowable( ERROR, this, Operators.onOperatorError( this.s, var16, this.currentContext() ) );
                            d = true;
                            v = null;
                        }

                        empty = v == null;
                        if ( this.checkTerminated( d, empty ) )
                        {
                            return;
                        }

                        if ( empty )
                        {
                            if ( this.sourceMode == 1 )
                            {
                                this.done = true;
                                this.checkTerminated( true, true );
                            }
                            break;
                        }

                        FluxPublish.PubSubInner[] var21 = a;
                        int var22 = a.length;

                        for ( int var23 = 0; var23 < var22; ++var23 )
                        {
                            FluxPublish.PubSubInner<T> inner = var21[var23];
                            inner.actual.onNext( v );
                            if ( Operators.producedCancellable( FluxPublish.PublishInner.REQUESTED, inner, 1L ) == Long.MIN_VALUE )
                            {
                                cancel = Integer.MIN_VALUE;
                            }
                        }
                    }

                    if ( e != 0 && this.sourceMode != 1 )
                    {
                        this.s.request( (long) e );
                    }

                    if ( maxRequested != 0L && !empty )
                    {
                        continue;
                    }
                }
                else if ( this.sourceMode == 1 )
                {
                    this.done = true;
                    if ( this.checkTerminated( true, empty ) )
                    {
                        break;
                    }
                }

                missed = WIP.addAndGet( this, -missed );
                if ( missed == 0 )
                {
                    break;
                }
            }
        }
    }

    FluxPublish.PubSubInner<T>[] terminate()
    {
        return (FluxPublish.PubSubInner[]) SUBSCRIBERS.getAndSet( this, FluxPublish.PublishSubscriber.TERMINATED );
    }

    boolean checkTerminated( boolean d, boolean empty )
    {
        if ( this.s == Operators.cancelledSubscription() )
        {
            if ( this.autoCancel )
            {
                this.terminate();
                Queue<T> q = this.queue;
                if ( q != null )
                {
                    q.clear();
                }
            }

            return true;
        }
        else
        {
            if ( d )
            {
                Throwable e = this.error;
                int var6;
                if ( e != null && e != Exceptions.TERMINATED )
                {
                    Queue<T> q = this.queue;
                    if ( q != null )
                    {
                        q.clear();
                    }

                    FluxPublish.PubSubInner[] var11 = this.terminate();
                    var6 = var11.length;

                    for ( int var12 = 0; var12 < var6; ++var12 )
                    {
                        FluxPublish.PubSubInner<T> inner = var11[var12];
                        inner.actual.onError( e );
                    }

                    return true;
                }

                if ( empty )
                {
                    FluxPublish.PubSubInner[] var4 = this.terminate();
                    int var5 = var4.length;

                    for ( var6 = 0; var6 < var5; ++var6 )
                    {
                        FluxPublish.PubSubInner<T> inner = var4[var6];
                        inner.actual.onComplete();
                    }

                    return true;
                }
            }

            return false;
        }
    }

    final boolean add( EmitterProcessor.EmitterInner<T> inner )
    {
        FluxPublish.PubSubInner[] a;
        FluxPublish.PubSubInner[] b;
        do
        {
            a = this.subscribers;
            if ( a == FluxPublish.PublishSubscriber.TERMINATED )
            {
                return false;
            }

            int n = a.length;
            b = new FluxPublish.PubSubInner[n + 1];
            System.arraycopy( a, 0, b, 0, n );
            b[n] = inner;
        }
        while ( !SUBSCRIBERS.compareAndSet( this, a, b ) );

        return true;
    }

    final void remove( FluxPublish.PubSubInner<T> inner )
    {
        FluxPublish.PubSubInner<T>[] a = this.subscribers;
        if ( a != FluxPublish.PublishSubscriber.TERMINATED && a != EMPTY )
        {
            int n = a.length;
            int j = -1;

            for ( int i = 0; i < n; ++i )
            {
                if ( a[i] == inner )
                {
                    j = i;
                    break;
                }
            }

            if ( j >= 0 )
            {
                FluxPublish.PubSubInner[] b;
                if ( n == 1 )
                {
                    b = EMPTY;
                }
                else
                {
                    b = new FluxPublish.PubSubInner[n - 1];
                    System.arraycopy( a, 0, b, 0, j );
                    System.arraycopy( a, j + 1, b, j, n - j - 1 );
                }

                if ( SUBSCRIBERS.compareAndSet( this, a, b ) && this.autoCancel && b == EMPTY && Operators.terminate( S, this ) )
                {
                    if ( WIP.getAndIncrement( this ) != 0 )
                    {
                        return;
                    }

                    this.terminate();
                    Queue<T> q = this.queue;
                    if ( q != null )
                    {
                        q.clear();
                    }
                }
            }
        }
    }

    public long downstreamCount()
    {
        return (long) this.subscribers.length;
    }

    static final class EmitterInner<T> extends FluxPublish.PubSubInner<T>
    {
        final EmitterProcessor<T> parent;

        EmitterInner( CoreSubscriber<? super T> actual, EmitterProcessor<T> parent )
        {
            super( actual );
            this.parent = parent;
        }

        void drainParent()
        {
            this.parent.drain();
        }

        void removeAndDrainParent()
        {
            this.parent.remove( this );
            this.parent.drain();
        }
    }
}
