package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.BiFunction;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxSwitchOnFirst<T, R> extends InternalFluxOperator<T,R>
{
    static final int STATE_INIT = 0;
    static final int STATE_SUBSCRIBED_ONCE = 1;
    final BiFunction<Signal<? extends T>,Flux<T>,Publisher<? extends R>> transformer;

    FluxSwitchOnFirst( Flux<? extends T> source, BiFunction<Signal<? extends T>,Flux<T>,Publisher<? extends R>> transformer )
    {
        super( source );
        this.transformer = (BiFunction) Objects.requireNonNull( transformer, "transformer" );
    }

    public int getPrefetch()
    {
        return 1;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            this.source.subscribe(
                    (CoreSubscriber) (new FluxSwitchOnFirst.SwitchOnFirstConditionalInner( (Fuseable.ConditionalSubscriber) actual, this.transformer )) );
            return null;
        }
        else
        {
            return new FluxSwitchOnFirst.SwitchOnFirstInner( actual, this.transformer );
        }
    }

    static final class SwitchOnFirstConditionalInnerSubscriber<T> implements InnerConsumer<T>, Fuseable.ConditionalSubscriber<T>
    {
        final FluxSwitchOnFirst.AbstractSwitchOnFirstInner<?,? super T> parent;
        final Fuseable.ConditionalSubscriber<? super T> inner;

        SwitchOnFirstConditionalInnerSubscriber( FluxSwitchOnFirst.AbstractSwitchOnFirstInner<?,? super T> parent,
                Fuseable.ConditionalSubscriber<? super T> inner )
        {
            this.parent = parent;
            this.inner = inner;
        }

        public Context currentContext()
        {
            return this.inner.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            this.inner.onSubscribe( s );
        }

        public void onNext( T t )
        {
            this.inner.onNext( t );
        }

        public boolean tryOnNext( T t )
        {
            return this.inner.tryOnNext( t );
        }

        public void onError( Throwable throwable )
        {
            if ( !this.parent.done )
            {
                this.parent.cancel();
            }

            this.inner.onError( throwable );
        }

        public void onComplete()
        {
            if ( !this.parent.done )
            {
                this.parent.cancel();
            }

            this.inner.onComplete();
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL ? this.inner : null;
            }
        }
    }

    static final class SwitchOnFirstInnerSubscriber<T> implements InnerConsumer<T>
    {
        final FluxSwitchOnFirst.AbstractSwitchOnFirstInner<?,T> parent;
        final CoreSubscriber<? super T> inner;

        SwitchOnFirstInnerSubscriber( FluxSwitchOnFirst.AbstractSwitchOnFirstInner<?,T> parent, CoreSubscriber<? super T> inner )
        {
            this.parent = parent;
            this.inner = inner;
        }

        public Context currentContext()
        {
            return this.inner.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            this.inner.onSubscribe( s );
        }

        public void onNext( T t )
        {
            this.inner.onNext( t );
        }

        public void onError( Throwable throwable )
        {
            if ( !this.parent.done )
            {
                this.parent.cancel();
            }

            this.inner.onError( throwable );
        }

        public void onComplete()
        {
            if ( !this.parent.done )
            {
                this.parent.cancel();
            }

            this.inner.onComplete();
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL ? this.inner : null;
            }
        }
    }

    static final class SwitchOnFirstConditionalInner<T, R> extends FluxSwitchOnFirst.AbstractSwitchOnFirstInner<T,R>
            implements Fuseable.ConditionalSubscriber<T>
    {
        SwitchOnFirstConditionalInner( Fuseable.ConditionalSubscriber<? super R> outer,
                BiFunction<Signal<? extends T>,Flux<T>,Publisher<? extends R>> transformer )
        {
            super( outer, transformer );
        }

        public void subscribe( CoreSubscriber<? super T> actual )
        {
            if ( this.state == 0 && STATE.compareAndSet( this, 0, 1 ) )
            {
                if ( this.first == null && this.done )
                {
                    if ( this.throwable != null )
                    {
                        Operators.error( actual, this.throwable );
                    }
                    else
                    {
                        Operators.complete( actual );
                    }

                    return;
                }

                INNER.lazySet( this, Operators.toConditionalSubscriber( actual ) );
                actual.onSubscribe( this );
            }
            else
            {
                Operators.error( actual, new IllegalStateException( "FluxSwitchOnFirst allows only one Subscriber" ) );
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.currentContext() );
                return false;
            }
            else
            {
                Fuseable.ConditionalSubscriber<? super T> i = (Fuseable.ConditionalSubscriber) this.inner;
                if ( i == null )
                {
                    CoreSubscriber o = this.outer;

                    Publisher result;
                    try
                    {
                        result = (Publisher) Objects.requireNonNull( this.transformer.apply( Signal.next( t, o.currentContext() ), this ),
                                "The transformer returned a null value" );
                    }
                    catch ( Throwable var6 )
                    {
                        this.done = true;
                        Operators.error( o, Operators.onOperatorError( this.s, var6, t, o.currentContext() ) );
                        return false;
                    }

                    this.first = t;
                    result.subscribe( o );
                    return true;
                }
                else
                {
                    return i.tryOnNext( t );
                }
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( this.first != null && this.drainRegular() && n != Long.MAX_VALUE )
                {
                    if ( --n > 0L )
                    {
                        this.s.request( n );
                        return;
                    }

                    return;
                }

                this.s.request( n );
            }
        }

        void drain()
        {
            this.drainRegular();
        }

        boolean drainRegular()
        {
            if ( WIP.getAndIncrement( this ) != 0 )
            {
                return false;
            }
            else
            {
                T f = this.first;
                int m = 1;
                boolean sent = false;
                Fuseable.ConditionalSubscriber a = (Fuseable.ConditionalSubscriber) this.inner;

                do
                {
                    if ( f != null )
                    {
                        this.first = null;
                        if ( this.cancelled )
                        {
                            Operators.onDiscard( f, a.currentContext() );
                            return false;
                        }

                        sent = a.tryOnNext( f );
                        f = null;
                    }

                    if ( this.cancelled )
                    {
                        return false;
                    }

                    if ( this.done )
                    {
                        Throwable t = this.throwable;
                        if ( t != null )
                        {
                            a.onError( t );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return sent;
                    }

                    m = WIP.addAndGet( this, -m );
                }
                while ( m != 0 );

                return sent;
            }
        }
    }

    static final class SwitchOnFirstInner<T, R> extends FluxSwitchOnFirst.AbstractSwitchOnFirstInner<T,R>
    {
        SwitchOnFirstInner( CoreSubscriber<? super R> outer, BiFunction<Signal<? extends T>,Flux<T>,Publisher<? extends R>> transformer )
        {
            super( outer, transformer );
        }

        public void subscribe( CoreSubscriber<? super T> actual )
        {
            if ( this.state == 0 && STATE.compareAndSet( this, 0, 1 ) )
            {
                if ( this.first == null && this.done )
                {
                    if ( this.throwable != null )
                    {
                        Operators.error( actual, this.throwable );
                    }
                    else
                    {
                        Operators.complete( actual );
                    }

                    return;
                }

                INNER.lazySet( this, actual );
                actual.onSubscribe( this );
            }
            else
            {
                Operators.error( actual, new IllegalStateException( "FluxSwitchOnFirst allows only one Subscriber" ) );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( this.first != null )
                {
                    this.drain();
                    if ( n != Long.MAX_VALUE )
                    {
                        if ( --n > 0L )
                        {
                            this.s.request( n );
                            return;
                        }

                        return;
                    }
                }

                this.s.request( n );
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                T f = this.first;
                int m = 1;
                CoreSubscriber a = this.inner;

                do
                {
                    if ( f != null )
                    {
                        this.first = null;
                        if ( this.cancelled )
                        {
                            Operators.onDiscard( f, a.currentContext() );
                            return;
                        }

                        a.onNext( f );
                        f = null;
                    }

                    if ( this.cancelled )
                    {
                        return;
                    }

                    if ( this.done )
                    {
                        Throwable t = this.throwable;
                        if ( t != null )
                        {
                            a.onError( t );
                        }
                        else
                        {
                            a.onComplete();
                        }

                        return;
                    }

                    m = WIP.addAndGet( this, -m );
                }
                while ( m != 0 );
            }
        }
    }

    abstract static class AbstractSwitchOnFirstInner<T, R> extends Flux<T> implements InnerOperator<T,R>
    {
        static final AtomicReferenceFieldUpdater<FluxSwitchOnFirst.AbstractSwitchOnFirstInner,CoreSubscriber> INNER =
                AtomicReferenceFieldUpdater.newUpdater( FluxSwitchOnFirst.AbstractSwitchOnFirstInner.class, CoreSubscriber.class, "inner" );
        static final AtomicIntegerFieldUpdater<FluxSwitchOnFirst.AbstractSwitchOnFirstInner> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxSwitchOnFirst.AbstractSwitchOnFirstInner.class, "wip" );
        static final AtomicIntegerFieldUpdater<FluxSwitchOnFirst.AbstractSwitchOnFirstInner> STATE =
                AtomicIntegerFieldUpdater.newUpdater( FluxSwitchOnFirst.AbstractSwitchOnFirstInner.class, "state" );
        final CoreSubscriber<? super R> outer;
        final BiFunction<Signal<? extends T>,Flux<T>,Publisher<? extends R>> transformer;
        Subscription s;
        Throwable throwable;
        T first;
        boolean done;
        volatile boolean cancelled;
        volatile CoreSubscriber<? super T> inner;
        volatile int wip;
        volatile int state;

        AbstractSwitchOnFirstInner( CoreSubscriber<? super R> outer, BiFunction<Signal<? extends T>,Flux<T>,Publisher<? extends R>> transformer )
        {
            this.outer =
                    (CoreSubscriber) (outer instanceof Fuseable.ConditionalSubscriber ? new FluxSwitchOnFirst.SwitchOnFirstConditionalInnerSubscriber( this,
                            (Fuseable.ConditionalSubscriber) outer ) : new FluxSwitchOnFirst.SwitchOnFirstInnerSubscriber( this, outer ));
            this.transformer = transformer;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else
            {
                return key != Scannable.Attr.TERMINATED ? InnerOperator.super.scanUnsafe( key ) : this.done || this.cancelled;
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.outer;
        }

        public Context currentContext()
        {
            CoreSubscriber<? super T> actual = this.inner;
            return actual != null ? actual.currentContext() : this.outer.currentContext();
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.s.cancel();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    INNER.lazySet( this, (Object) null );
                    T f = this.first;
                    if ( f != null )
                    {
                        this.first = null;
                        Operators.onDiscard( f, this.currentContext() );
                    }
                }
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                s.request( 1L );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.currentContext() );
            }
            else
            {
                CoreSubscriber<? super T> i = this.inner;
                if ( i == null )
                {
                    CoreSubscriber o = this.outer;

                    Publisher result;
                    try
                    {
                        result = (Publisher) Objects.requireNonNull( this.transformer.apply( Signal.next( t, o.currentContext() ), this ),
                                "The transformer returned a null value" );
                    }
                    catch ( Throwable var6 )
                    {
                        this.done = true;
                        Operators.error( o, Operators.onOperatorError( this.s, var6, t, o.currentContext() ) );
                        return;
                    }

                    this.first = t;
                    result.subscribe( o );
                }
                else
                {
                    i.onNext( t );
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.currentContext() );
            }
            else
            {
                this.throwable = t;
                this.done = true;
                CoreSubscriber<? super T> i = this.inner;
                T f = this.first;
                if ( f == null && i == null && !this.cancelled )
                {
                    CoreSubscriber o = this.outer;

                    Publisher result;
                    try
                    {
                        result = (Publisher) Objects.requireNonNull( this.transformer.apply( Signal.error( t, o.currentContext() ), this ),
                                "The transformer returned a null value" );
                    }
                    catch ( Throwable var7 )
                    {
                        this.done = true;
                        Operators.error( o, Operators.onOperatorError( this.s, var7, t, o.currentContext() ) );
                        return;
                    }

                    result.subscribe( o );
                }
                else
                {
                    if ( f == null )
                    {
                        this.drain();
                    }
                }
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                CoreSubscriber<? super T> i = this.inner;
                T f = this.first;
                if ( f == null && i == null && !this.cancelled )
                {
                    CoreSubscriber o = this.outer;

                    Publisher result;
                    try
                    {
                        result = (Publisher) Objects.requireNonNull( this.transformer.apply( Signal.complete( o.currentContext() ), this ),
                                "The transformer returned a null value" );
                    }
                    catch ( Throwable var6 )
                    {
                        this.done = true;
                        Operators.error( o, Operators.onOperatorError( this.s, var6, (Object) null, o.currentContext() ) );
                        return;
                    }

                    result.subscribe( o );
                }
                else
                {
                    if ( f == null )
                    {
                        this.drain();
                    }
                }
            }
        }

        abstract void drain();
    }
}
