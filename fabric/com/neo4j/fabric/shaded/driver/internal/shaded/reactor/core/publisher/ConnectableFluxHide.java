package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Consumer;

final class ConnectableFluxHide<T> extends InternalConnectableFluxOperator<T,T> implements Scannable
{
    ConnectableFluxHide( ConnectableFlux<T> source )
    {
        super( source );
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public final CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return actual;
    }

    public void connect( Consumer<? super Disposable> cancelSupport )
    {
        this.source.connect( cancelSupport );
    }
}
