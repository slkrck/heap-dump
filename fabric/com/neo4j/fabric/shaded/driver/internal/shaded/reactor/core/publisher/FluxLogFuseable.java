package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

final class FluxLogFuseable<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    final SignalPeek<T> log;

    FluxLogFuseable( Flux<? extends T> source, SignalPeek<T> log )
    {
        super( source );
        this.log = log;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return (CoreSubscriber) (actual instanceof Fuseable.ConditionalSubscriber ? new FluxPeekFuseable.PeekFuseableConditionalSubscriber(
                (Fuseable.ConditionalSubscriber) actual, this.log ) : new FluxPeekFuseable.PeekFuseableSubscriber( actual, this.log ));
    }
}
