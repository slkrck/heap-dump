package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.Queue;
import java.util.function.Function;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;

final class ParallelConcatMap<T, R> extends ParallelFlux<R> implements Scannable
{
    final ParallelFlux<T> source;
    final Function<? super T,? extends Publisher<? extends R>> mapper;
    final Supplier<? extends Queue<T>> queueSupplier;
    final int prefetch;
    final FluxConcatMap.ErrorMode errorMode;

    ParallelConcatMap( ParallelFlux<T> source, Function<? super T,? extends Publisher<? extends R>> mapper, Supplier<? extends Queue<T>> queueSupplier,
            int prefetch, FluxConcatMap.ErrorMode errorMode )
    {
        this.source = source;
        this.mapper = (Function) Objects.requireNonNull( mapper, "mapper" );
        this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
        this.prefetch = prefetch;
        this.errorMode = (FluxConcatMap.ErrorMode) Objects.requireNonNull( errorMode, "errorMode" );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.DELAY_ERROR ? this.errorMode != FluxConcatMap.ErrorMode.IMMEDIATE : null;
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    public void subscribe( CoreSubscriber<? super R>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<T>[] parents = new CoreSubscriber[n];

            for ( int i = 0; i < n; ++i )
            {
                parents[i] = FluxConcatMap.subscriber( subscribers[i], this.mapper, this.queueSupplier, this.prefetch, this.errorMode );
            }

            this.source.subscribe( parents );
        }
    }
}
