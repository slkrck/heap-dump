package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import java.util.Queue;

public interface EventLoopTaskQueueFactory
{
    Queue<Runnable> newTaskQueue( int var1 );
}
