package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

final class SchedulerTask implements Runnable, Disposable, Callable<Void>
{
    static final Future<Void> FINISHED = new FutureTask( () -> {
        return null;
    } );
    static final Future<Void> CANCELLED = new FutureTask( () -> {
        return null;
    } );
    static final Disposable TAKEN = Disposables.disposed();
    static final AtomicReferenceFieldUpdater<SchedulerTask,Future> FUTURE =
            AtomicReferenceFieldUpdater.newUpdater( SchedulerTask.class, Future.class, "future" );
    static final AtomicReferenceFieldUpdater<SchedulerTask,Disposable> PARENT =
            AtomicReferenceFieldUpdater.newUpdater( SchedulerTask.class, Disposable.class, "parent" );
    final Runnable task;
    volatile Future<?> future;
    volatile Disposable parent;
    Thread thread;

    SchedulerTask( Runnable task, @Nullable Disposable parent )
    {
        this.task = task;
        PARENT.lazySet( this, parent );
    }

    @Nullable
    public Void call()
    {
        this.thread = Thread.currentThread();
        Disposable d = null;

        while ( true )
        {
            boolean var7 = false;

            try
            {
                var7 = true;
                d = this.parent;
                if ( d == TAKEN || d == null || PARENT.compareAndSet( this, d, TAKEN ) )
                {
                    try
                    {
                        this.task.run();
                        var7 = false;
                    }
                    catch ( Throwable var8 )
                    {
                        Schedulers.handleError( var8 );
                        var7 = false;
                    }
                    break;
                }
            }
            finally
            {
                if ( var7 )
                {
                    this.thread = null;

                    Future f;
                    do
                    {
                        f = this.future;
                    }
                    while ( f != CANCELLED && !FUTURE.compareAndSet( this, f, FINISHED ) );

                    if ( d != null )
                    {
                        d.dispose();
                    }
                }
            }
        }

        this.thread = null;

        Future f;
        do
        {
            f = this.future;
        }
        while ( f != CANCELLED && !FUTURE.compareAndSet( this, f, FINISHED ) );

        if ( d != null )
        {
            d.dispose();
        }

        return null;
    }

    public void run()
    {
        this.call();
    }

    void setFuture( Future<?> f )
    {
        Future o;
        do
        {
            o = this.future;
            if ( o == FINISHED )
            {
                return;
            }

            if ( o == CANCELLED )
            {
                f.cancel( this.thread != Thread.currentThread() );
                return;
            }
        }
        while ( !FUTURE.compareAndSet( this, o, f ) );
    }

    public boolean isDisposed()
    {
        Future<?> a = this.future;
        return FINISHED == a || CANCELLED == a;
    }

    public void dispose()
    {
        while ( true )
        {
            Future f = this.future;
            if ( f != FINISHED && f != CANCELLED )
            {
                if ( !FUTURE.compareAndSet( this, f, CANCELLED ) )
                {
                    continue;
                }

                if ( f != null )
                {
                    f.cancel( this.thread != Thread.currentThread() );
                }
            }

            while ( true )
            {
                Disposable d = this.parent;
                if ( d == TAKEN || d == null )
                {
                    break;
                }

                if ( PARENT.compareAndSet( this, d, TAKEN ) )
                {
                    d.dispose();
                    break;
                }
            }

            return;
        }
    }
}
