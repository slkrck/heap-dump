package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Supplier;
import java.util.stream.Stream;

final class ParallelScheduler implements Scheduler, Supplier<ScheduledExecutorService>, Scannable
{
    static final AtomicLong COUNTER = new AtomicLong();
    static final AtomicReferenceFieldUpdater<ParallelScheduler,ScheduledExecutorService[]> EXECUTORS =
            AtomicReferenceFieldUpdater.newUpdater( ParallelScheduler.class, ScheduledExecutorService[].class, "executors" );
    static final ScheduledExecutorService[] SHUTDOWN = new ScheduledExecutorService[0];
    static final ScheduledExecutorService TERMINATED = Executors.newSingleThreadScheduledExecutor();

    static
    {
        TERMINATED.shutdownNow();
    }

    final int n;
    final ThreadFactory factory;
    volatile ScheduledExecutorService[] executors;
    int roundRobin;

    ParallelScheduler( int n, ThreadFactory factory )
    {
        if ( n <= 0 )
        {
            throw new IllegalArgumentException( "n > 0 required but it was " + n );
        }
        else
        {
            this.n = n;
            this.factory = factory;
            this.init( n );
        }
    }

    public ScheduledExecutorService get()
    {
        ScheduledThreadPoolExecutor poolExecutor = new ScheduledThreadPoolExecutor( 1, this.factory );
        poolExecutor.setMaximumPoolSize( 1 );
        poolExecutor.setRemoveOnCancelPolicy( true );
        return poolExecutor;
    }

    void init( int n )
    {
        ScheduledExecutorService[] a = new ScheduledExecutorService[n];

        for ( int i = 0; i < n; ++i )
        {
            a[i] = Schedulers.decorateExecutorService( this, this.get() );
        }

        EXECUTORS.lazySet( this, a );
    }

    public boolean isDisposed()
    {
        return this.executors == SHUTDOWN;
    }

    public void start()
    {
        ScheduledExecutorService[] b = null;

        ScheduledExecutorService[] a;
        do
        {
            a = this.executors;
            if ( a != SHUTDOWN )
            {
                if ( b != null )
                {
                    ScheduledExecutorService[] var7 = b;
                    int var4 = b.length;

                    for ( int var5 = 0; var5 < var4; ++var5 )
                    {
                        ScheduledExecutorService exec = var7[var5];
                        exec.shutdownNow();
                    }
                }

                return;
            }

            if ( b == null )
            {
                b = new ScheduledExecutorService[this.n];

                for ( int i = 0; i < this.n; ++i )
                {
                    b[i] = Schedulers.decorateExecutorService( this, this.get() );
                }
            }
        }
        while ( !EXECUTORS.compareAndSet( this, a, b ) );
    }

    public void dispose()
    {
        ScheduledExecutorService[] a = this.executors;
        if ( a != SHUTDOWN )
        {
            a = (ScheduledExecutorService[]) EXECUTORS.getAndSet( this, SHUTDOWN );
            if ( a != SHUTDOWN )
            {
                ScheduledExecutorService[] var2 = a;
                int var3 = a.length;

                for ( int var4 = 0; var4 < var3; ++var4 )
                {
                    ScheduledExecutorService exec = var2[var4];
                    exec.shutdownNow();
                }
            }
        }
    }

    ScheduledExecutorService pick()
    {
        ScheduledExecutorService[] a = this.executors;
        if ( a != SHUTDOWN )
        {
            int idx = this.roundRobin;
            if ( idx == this.n )
            {
                idx = 0;
                this.roundRobin = 1;
            }
            else
            {
                this.roundRobin = idx + 1;
            }

            return a[idx];
        }
        else
        {
            return TERMINATED;
        }
    }

    public Disposable schedule( Runnable task )
    {
        return Schedulers.directSchedule( this.pick(), task, (Disposable) null, 0L, TimeUnit.MILLISECONDS );
    }

    public Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        return Schedulers.directSchedule( this.pick(), task, (Disposable) null, delay, unit );
    }

    public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        return Schedulers.directSchedulePeriodically( this.pick(), task, initialDelay, period, unit );
    }

    public String toString()
    {
        StringBuilder ts = (new StringBuilder( "parallel" )).append( '(' ).append( this.n );
        if ( this.factory instanceof ReactorThreadFactory )
        {
            ts.append( ",\"" ).append( ((ReactorThreadFactory) this.factory).get() ).append( '"' );
        }

        ts.append( ')' );
        return ts.toString();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            if ( key != Scannable.Attr.CAPACITY && key != Scannable.Attr.BUFFERED )
            {
                return key == Scannable.Attr.NAME ? this.toString() : null;
            }
            else
            {
                return this.n;
            }
        }
        else
        {
            return this.isDisposed();
        }
    }

    public Stream<? extends Scannable> inners()
    {
        return Stream.of( this.executors ).map( ( exec ) -> {
            return ( key ) -> {
                return Schedulers.scanExecutor( exec, key );
            };
        } );
    }

    public Scheduler.Worker createWorker()
    {
        return new ExecutorServiceWorker( this.pick() );
    }
}
