package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Publisher;

final class MonoRetryWhen<T> extends InternalMonoOperator<T,T>
{
    final Function<? super Flux<Throwable>,? extends Publisher<?>> whenSourceFactory;

    MonoRetryWhen( Mono<? extends T> source, Function<? super Flux<Throwable>,? extends Publisher<?>> whenSourceFactory )
    {
        super( source );
        this.whenSourceFactory = (Function) Objects.requireNonNull( whenSourceFactory, "whenSourceFactory" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRetryWhen.subscribe( actual, this.whenSourceFactory, this.source );
        return null;
    }
}
