package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Supplier;

final class MonoFlattenIterable<T, R> extends FluxFromMonoOperator<T,R> implements Fuseable
{
    final Function<? super T,? extends Iterable<? extends R>> mapper;
    final int prefetch;
    final Supplier<Queue<T>> queueSupplier;

    MonoFlattenIterable( Mono<? extends T> source, Function<? super T,? extends Iterable<? extends R>> mapper, int prefetch, Supplier<Queue<T>> queueSupplier )
    {
        super( source );
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.mapper = (Function) Objects.requireNonNull( mapper, "mapper" );
            this.prefetch = prefetch;
            this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        if ( this.source instanceof Callable )
        {
            Object v;
            try
            {
                v = ((Callable) this.source).call();
            }
            catch ( Throwable var6 )
            {
                Operators.error( actual, Operators.onOperatorError( var6, actual.currentContext() ) );
                return null;
            }

            if ( v == null )
            {
                Operators.complete( actual );
                return null;
            }
            else
            {
                Iterator it;
                try
                {
                    Iterable<? extends R> iter = (Iterable) this.mapper.apply( v );
                    it = iter.iterator();
                }
                catch ( Throwable var5 )
                {
                    Operators.error( actual, Operators.onOperatorError( var5, actual.currentContext() ) );
                    return null;
                }

                FluxIterable.subscribe( actual, it );
                return null;
            }
        }
        else
        {
            return new FluxFlattenIterable.FlattenIterableSubscriber( actual, this.mapper, this.prefetch, this.queueSupplier );
        }
    }
}
