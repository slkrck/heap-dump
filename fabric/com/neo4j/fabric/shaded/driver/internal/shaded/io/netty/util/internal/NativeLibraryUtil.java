package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

final class NativeLibraryUtil
{
    private NativeLibraryUtil()
    {
    }

    public static void loadLibrary( String libName, boolean absolute )
    {
        if ( absolute )
        {
            System.load( libName );
        }
        else
        {
            System.loadLibrary( libName );
        }
    }
}
