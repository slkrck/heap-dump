package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Function;

final class ParallelMap<T, R> extends ParallelFlux<R> implements Scannable
{
    final ParallelFlux<T> source;
    final Function<? super T,? extends R> mapper;

    ParallelMap( ParallelFlux<T> source, Function<? super T,? extends R> mapper )
    {
        this.source = source;
        this.mapper = mapper;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public void subscribe( CoreSubscriber<? super R>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<? super T>[] parents = new CoreSubscriber[n];
            boolean conditional = subscribers[0] instanceof Fuseable.ConditionalSubscriber;

            for ( int i = 0; i < n; ++i )
            {
                if ( conditional )
                {
                    parents[i] = new FluxMap.MapConditionalSubscriber( (Fuseable.ConditionalSubscriber) subscribers[i], this.mapper );
                }
                else
                {
                    parents[i] = new FluxMap.MapSubscriber( subscribers[i], this.mapper );
                }
            }

            this.source.subscribe( parents );
        }
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }
}
