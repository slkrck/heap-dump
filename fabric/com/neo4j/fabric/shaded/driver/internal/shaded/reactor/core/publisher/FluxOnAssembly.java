package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple4;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuples;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxOnAssembly<T> extends InternalFluxOperator<T,T> implements Fuseable, AssemblyOp
{
    final FluxOnAssembly.AssemblySnapshot snapshotStack;

    FluxOnAssembly( Flux<? extends T> source, FluxOnAssembly.AssemblySnapshot snapshotStack )
    {
        super( source );
        this.snapshotStack = snapshotStack;
    }

    static void fillStacktraceHeader( StringBuilder sb, Class<?> sourceClass, @Nullable String description )
    {
        sb.append( "\nAssembly trace from producer [" ).append( sourceClass.getName() ).append( "]" );
        if ( description != null )
        {
            sb.append( ", described as [" ).append( description ).append( "]" );
        }

        sb.append( " :\n" );
    }

    static <T> CoreSubscriber<? super T> wrapSubscriber( CoreSubscriber<? super T> actual, Flux<? extends T> source,
            @Nullable FluxOnAssembly.AssemblySnapshot snapshotStack )
    {
        if ( snapshotStack != null )
        {
            if ( actual instanceof Fuseable.ConditionalSubscriber )
            {
                Fuseable.ConditionalSubscriber<? super T> cs = (Fuseable.ConditionalSubscriber) actual;
                return new FluxOnAssembly.OnAssemblyConditionalSubscriber( cs, snapshotStack, source );
            }
            else
            {
                return new FluxOnAssembly.OnAssemblySubscriber( actual, snapshotStack, source );
            }
        }
        else
        {
            return actual;
        }
    }

    static int getParentOrThis( Scannable parent )
    {
        return (Integer) parent.parents().filter( ( s ) -> {
            return !(s instanceof AssemblyOp);
        } ).findFirst().map( Object::hashCode ).orElse( parent.hashCode() );
    }

    public String stepName()
    {
        return this.snapshotStack.operatorAssemblyInformation();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.ACTUAL_METADATA ? !this.snapshotStack.checkpointed : super.scanUnsafe( key );
    }

    public String toString()
    {
        return this.snapshotStack.operatorAssemblyInformation();
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return wrapSubscriber( actual, this.source, this.snapshotStack );
    }

    static final class OnAssemblyConditionalSubscriber<T> extends FluxOnAssembly.OnAssemblySubscriber<T> implements Fuseable.ConditionalSubscriber<T>
    {
        final Fuseable.ConditionalSubscriber<? super T> actualCS;

        OnAssemblyConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, FluxOnAssembly.AssemblySnapshot stacktrace, Publisher<?> parent )
        {
            super( actual, stacktrace, parent );
            this.actualCS = actual;
        }

        public boolean tryOnNext( T t )
        {
            return this.actualCS.tryOnNext( t );
        }
    }

    static class OnAssemblySubscriber<T> implements InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        final FluxOnAssembly.AssemblySnapshot snapshotStack;
        final Publisher<?> parent;
        final CoreSubscriber<? super T> actual;
        Fuseable.QueueSubscription<T> qs;
        Subscription s;
        int fusionMode;

        OnAssemblySubscriber( CoreSubscriber<? super T> actual, FluxOnAssembly.AssemblySnapshot snapshotStack, Publisher<?> parent )
        {
            this.actual = actual;
            this.snapshotStack = snapshotStack;
            this.parent = parent;
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL_METADATA ? !this.snapshotStack.checkpointed : InnerOperator.super.scanUnsafe( key );
            }
        }

        public String toString()
        {
            return this.snapshotStack.operatorAssemblyInformation();
        }

        public String stepName()
        {
            return this.toString();
        }

        public final void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public final void onError( Throwable t )
        {
            this.actual.onError( this.fail( t ) );
        }

        public final void onComplete()
        {
            this.actual.onComplete();
        }

        public final int requestFusion( int requestedMode )
        {
            Fuseable.QueueSubscription<T> qs = this.qs;
            if ( qs != null )
            {
                int m = qs.requestFusion( requestedMode );
                if ( m != 0 )
                {
                    this.fusionMode = m;
                }

                return m;
            }
            else
            {
                return 0;
            }
        }

        final Throwable fail( Throwable t )
        {
            boolean lightCheckpoint = this.snapshotStack.isLight();
            FluxOnAssembly.OnAssemblyException onAssemblyException = null;
            Throwable[] var4 = t.getSuppressed();
            int var5 = var4.length;

            int i;
            for ( i = 0; i < var5; ++i )
            {
                Throwable e = var4[i];
                if ( e instanceof FluxOnAssembly.OnAssemblyException )
                {
                    onAssemblyException = (FluxOnAssembly.OnAssemblyException) e;
                    break;
                }
            }

            if ( onAssemblyException == null )
            {
                if ( lightCheckpoint )
                {
                    onAssemblyException = new FluxOnAssembly.OnAssemblyException( "" );
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    FluxOnAssembly.fillStacktraceHeader( sb, this.parent.getClass(), this.snapshotStack.getDescription() );
                    sb.append( this.snapshotStack.toAssemblyInformation().replaceFirst( "\\n$", "" ) );
                    String description = sb.toString();
                    onAssemblyException = new FluxOnAssembly.OnAssemblyException( description );
                }

                t = Exceptions.addSuppressed( (Throwable) t, onAssemblyException );
                StackTraceElement[] stackTrace = t.getStackTrace();
                if ( stackTrace.length > 0 )
                {
                    StackTraceElement[] newStackTrace = new StackTraceElement[stackTrace.length];
                    i = 0;
                    StackTraceElement[] var16 = stackTrace;
                    int var8 = stackTrace.length;

                    for ( int var9 = 0; var9 < var8; ++var9 )
                    {
                        StackTraceElement stackTraceElement = var16[var9];
                        String className = stackTraceElement.getClassName();
                        if ( !className.startsWith( "com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher." ) ||
                                !className.contains( "OnAssembly" ) )
                        {
                            newStackTrace[i] = stackTraceElement;
                            ++i;
                        }
                    }

                    newStackTrace = (StackTraceElement[]) Arrays.copyOf( newStackTrace, i );
                    onAssemblyException.setStackTrace( newStackTrace );
                    t.setStackTrace( new StackTraceElement[]{stackTrace[0]} );
                }
            }

            onAssemblyException.add( this.parent, this.snapshotStack );
            return t;
        }

        public final boolean isEmpty()
        {
            try
            {
                return this.qs.isEmpty();
            }
            catch ( Throwable var2 )
            {
                Exceptions.throwIfFatal( var2 );
                throw Exceptions.propagate( this.fail( var2 ) );
            }
        }

        public final void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.qs = Operators.as( s );
                this.actual.onSubscribe( this );
            }
        }

        public final int size()
        {
            return this.qs.size();
        }

        public final void clear()
        {
            this.qs.clear();
        }

        public final void request( long n )
        {
            this.s.request( n );
        }

        public final void cancel()
        {
            this.s.cancel();
        }

        @Nullable
        public final T poll()
        {
            try
            {
                return this.qs.poll();
            }
            catch ( Throwable var2 )
            {
                Exceptions.throwIfFatal( var2 );
                throw Exceptions.propagate( this.fail( var2 ) );
            }
        }
    }

    static final class OnAssemblyException extends RuntimeException
    {
        private static final long serialVersionUID = 5278398300974016773L;
        final List<Tuple4<Integer,String,String,Integer>> chainOrder = new LinkedList();

        OnAssemblyException( String message )
        {
            super( message );
        }

        public Throwable fillInStackTrace()
        {
            return this;
        }

        void add( Publisher<?> parent, FluxOnAssembly.AssemblySnapshot snapshot )
        {
            if ( snapshot.isLight() )
            {
                this.add( parent, snapshot.lightPrefix(), snapshot.getDescription() );
            }
            else
            {
                String assemblyInformation = snapshot.toAssemblyInformation();
                String[] parts = Traces.extractOperatorAssemblyInformationParts( assemblyInformation );
                if ( parts.length > 0 )
                {
                    String prefix = parts.length > 1 ? parts[0] : "";
                    String line = parts[parts.length - 1];
                    this.add( parent, prefix, line );
                }
            }
        }

        private void add( Publisher<?> parent, String prefix, String line )
        {
            int key = FluxOnAssembly.getParentOrThis( Scannable.from( parent ) );
            synchronized ( this.chainOrder )
            {
                int i = 0;
                int n = this.chainOrder.size();

                for ( int j = n - 1; j >= 0; --j )
                {
                    Tuple4<Integer,String,String,Integer> tmp = (Tuple4) this.chainOrder.get( j );
                    if ( (Integer) tmp.getT1() == key )
                    {
                        i = (Integer) tmp.getT4();
                        break;
                    }
                }

                while ( true )
                {
                    Tuple4<Integer,String,String,Integer> t = Tuples.of( parent.hashCode(), prefix, line, i );
                    if ( !this.chainOrder.contains( t ) )
                    {
                        this.chainOrder.add( t );
                        return;
                    }

                    ++i;
                }
            }
        }

        public String getMessage()
        {
            synchronized ( this.chainOrder )
            {
                if ( this.chainOrder.isEmpty() )
                {
                    return super.getMessage();
                }
                else
                {
                    int maxWidth = 0;
                    Iterator var3 = this.chainOrder.iterator();

                    while ( var3.hasNext() )
                    {
                        Tuple4<Integer,String,String,Integer> t = (Tuple4) var3.next();
                        int length = ((String) t.getT2()).length();
                        if ( length > maxWidth )
                        {
                            maxWidth = length;
                        }
                    }

                    StringBuilder sb = (new StringBuilder( super.getMessage() )).append( "\nError has been observed at the following site(s):\n" );
                    Iterator var13 = this.chainOrder.iterator();

                    while ( var13.hasNext() )
                    {
                        Tuple4<Integer,String,String,Integer> t = (Tuple4) var13.next();
                        Integer indent = (Integer) t.getT4();
                        String operator = (String) t.getT2();
                        String message = (String) t.getT3();
                        sb.append( "\t|_" );

                        int i;
                        for ( i = 0; i < indent; ++i )
                        {
                            sb.append( "____" );
                        }

                        for ( i = operator.length(); i < maxWidth + 1; ++i )
                        {
                            sb.append( ' ' );
                        }

                        sb.append( operator );
                        sb.append( " ⇢ " );
                        sb.append( message );
                        sb.append( "\n" );
                    }

                    sb.append( "Stack trace:" );
                    return sb.toString();
                }
            }
        }
    }

    static final class MethodReturnSnapshot extends FluxOnAssembly.AssemblySnapshot
    {
        MethodReturnSnapshot( String method )
        {
            super( true, method, (Supplier) null, null );
            this.cached = method;
        }

        public boolean isLight()
        {
            return true;
        }

        String operatorAssemblyInformation()
        {
            return this.cached;
        }
    }

    static final class AssemblyLightSnapshot extends FluxOnAssembly.AssemblySnapshot
    {
        AssemblyLightSnapshot( @Nullable String description )
        {
            super( true, description, (Supplier) null, null );
            this.cached = "checkpoint(\"" + description + "\")";
        }

        public boolean isLight()
        {
            return true;
        }

        public String lightPrefix()
        {
            return "checkpoint";
        }

        String operatorAssemblyInformation()
        {
            return this.cached;
        }
    }

    static class AssemblySnapshot
    {
        final boolean checkpointed;
        @Nullable
        final String description;
        final Supplier<String> assemblyInformationSupplier;
        String cached;

        AssemblySnapshot( @Nullable String description, Supplier<String> assemblyInformationSupplier )
        {
            this( description != null, description, assemblyInformationSupplier );
        }

        AssemblySnapshot( String assemblyInformation )
        {
            this.checkpointed = false;
            this.description = null;
            this.assemblyInformationSupplier = null;
            this.cached = assemblyInformation;
        }

        private AssemblySnapshot( boolean checkpointed, @Nullable String description, Supplier<String> assemblyInformationSupplier )
        {
            this.checkpointed = checkpointed;
            this.description = description;
            this.assemblyInformationSupplier = assemblyInformationSupplier;
        }

        @Nullable
        public String getDescription()
        {
            return this.description;
        }

        public boolean isLight()
        {
            return false;
        }

        public String lightPrefix()
        {
            return "";
        }

        String toAssemblyInformation()
        {
            if ( this.cached == null )
            {
                this.cached = (String) this.assemblyInformationSupplier.get();
            }

            return this.cached;
        }

        String operatorAssemblyInformation()
        {
            return Traces.extractOperatorAssemblyInformation( this.toAssemblyInformation() );
        }
    }
}
