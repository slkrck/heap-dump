package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.shaded.org.jctools.queues;

public interface QueueProgressIndicators
{
    long currentProducerIndex();

    long currentConsumerIndex();
}
