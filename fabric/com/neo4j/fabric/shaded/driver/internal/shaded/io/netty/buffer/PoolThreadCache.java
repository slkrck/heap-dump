package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.Recycler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.MathUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.nio.ByteBuffer;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

final class PoolThreadCache
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( PoolThreadCache.class );
    final PoolArena<byte[]> heapArena;
    final PoolArena<ByteBuffer> directArena;
    private final PoolThreadCache.MemoryRegionCache<byte[]>[] tinySubPageHeapCaches;
    private final PoolThreadCache.MemoryRegionCache<byte[]>[] smallSubPageHeapCaches;
    private final PoolThreadCache.MemoryRegionCache<ByteBuffer>[] tinySubPageDirectCaches;
    private final PoolThreadCache.MemoryRegionCache<ByteBuffer>[] smallSubPageDirectCaches;
    private final PoolThreadCache.MemoryRegionCache<byte[]>[] normalHeapCaches;
    private final PoolThreadCache.MemoryRegionCache<ByteBuffer>[] normalDirectCaches;
    private final int numShiftsNormalDirect;
    private final int numShiftsNormalHeap;
    private final int freeSweepAllocationThreshold;
    private final AtomicBoolean freed = new AtomicBoolean();
    private int allocations;

    PoolThreadCache( PoolArena<byte[]> heapArena, PoolArena<ByteBuffer> directArena, int tinyCacheSize, int smallCacheSize, int normalCacheSize,
            int maxCachedBufferCapacity, int freeSweepAllocationThreshold )
    {
        ObjectUtil.checkPositiveOrZero( maxCachedBufferCapacity, "maxCachedBufferCapacity" );
        this.freeSweepAllocationThreshold = freeSweepAllocationThreshold;
        this.heapArena = heapArena;
        this.directArena = directArena;
        if ( directArena != null )
        {
            this.tinySubPageDirectCaches = createSubPageCaches( tinyCacheSize, 32, PoolArena.SizeClass.Tiny );
            this.smallSubPageDirectCaches = createSubPageCaches( smallCacheSize, directArena.numSmallSubpagePools, PoolArena.SizeClass.Small );
            this.numShiftsNormalDirect = log2( directArena.pageSize );
            this.normalDirectCaches = createNormalCaches( normalCacheSize, maxCachedBufferCapacity, directArena );
            directArena.numThreadCaches.getAndIncrement();
        }
        else
        {
            this.tinySubPageDirectCaches = null;
            this.smallSubPageDirectCaches = null;
            this.normalDirectCaches = null;
            this.numShiftsNormalDirect = -1;
        }

        if ( heapArena != null )
        {
            this.tinySubPageHeapCaches = createSubPageCaches( tinyCacheSize, 32, PoolArena.SizeClass.Tiny );
            this.smallSubPageHeapCaches = createSubPageCaches( smallCacheSize, heapArena.numSmallSubpagePools, PoolArena.SizeClass.Small );
            this.numShiftsNormalHeap = log2( heapArena.pageSize );
            this.normalHeapCaches = createNormalCaches( normalCacheSize, maxCachedBufferCapacity, heapArena );
            heapArena.numThreadCaches.getAndIncrement();
        }
        else
        {
            this.tinySubPageHeapCaches = null;
            this.smallSubPageHeapCaches = null;
            this.normalHeapCaches = null;
            this.numShiftsNormalHeap = -1;
        }

        if ( (this.tinySubPageDirectCaches != null || this.smallSubPageDirectCaches != null || this.normalDirectCaches != null ||
                this.tinySubPageHeapCaches != null || this.smallSubPageHeapCaches != null || this.normalHeapCaches != null) &&
                freeSweepAllocationThreshold < 1 )
        {
            throw new IllegalArgumentException( "freeSweepAllocationThreshold: " + freeSweepAllocationThreshold + " (expected: > 0)" );
        }
    }

    private static <T> PoolThreadCache.MemoryRegionCache<T>[] createSubPageCaches( int cacheSize, int numCaches, PoolArena.SizeClass sizeClass )
    {
        if ( cacheSize > 0 && numCaches > 0 )
        {
            PoolThreadCache.MemoryRegionCache<T>[] cache = new PoolThreadCache.MemoryRegionCache[numCaches];

            for ( int i = 0; i < cache.length; ++i )
            {
                cache[i] = new PoolThreadCache.SubPageMemoryRegionCache( cacheSize, sizeClass );
            }

            return cache;
        }
        else
        {
            return null;
        }
    }

    private static <T> PoolThreadCache.MemoryRegionCache<T>[] createNormalCaches( int cacheSize, int maxCachedBufferCapacity, PoolArena<T> area )
    {
        if ( cacheSize > 0 && maxCachedBufferCapacity > 0 )
        {
            int max = Math.min( area.chunkSize, maxCachedBufferCapacity );
            int arraySize = Math.max( 1, log2( max / area.pageSize ) + 1 );
            PoolThreadCache.MemoryRegionCache<T>[] cache = new PoolThreadCache.MemoryRegionCache[arraySize];

            for ( int i = 0; i < cache.length; ++i )
            {
                cache[i] = new PoolThreadCache.NormalMemoryRegionCache( cacheSize );
            }

            return cache;
        }
        else
        {
            return null;
        }
    }

    private static int log2( int val )
    {
        int res;
        for ( res = 0; val > 1; ++res )
        {
            val >>= 1;
        }

        return res;
    }

    private static int free( PoolThreadCache.MemoryRegionCache<?>[] caches, boolean finalizer )
    {
        if ( caches == null )
        {
            return 0;
        }
        else
        {
            int numFreed = 0;
            PoolThreadCache.MemoryRegionCache[] var3 = caches;
            int var4 = caches.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                PoolThreadCache.MemoryRegionCache<?> c = var3[var5];
                numFreed += free( c, finalizer );
            }

            return numFreed;
        }
    }

    private static int free( PoolThreadCache.MemoryRegionCache<?> cache, boolean finalizer )
    {
        return cache == null ? 0 : cache.free( finalizer );
    }

    private static void trim( PoolThreadCache.MemoryRegionCache<?>[] caches )
    {
        if ( caches != null )
        {
            PoolThreadCache.MemoryRegionCache[] var1 = caches;
            int var2 = caches.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                PoolThreadCache.MemoryRegionCache<?> c = var1[var3];
                trim( c );
            }
        }
    }

    private static void trim( PoolThreadCache.MemoryRegionCache<?> cache )
    {
        if ( cache != null )
        {
            cache.trim();
        }
    }

    private static <T> PoolThreadCache.MemoryRegionCache<T> cache( PoolThreadCache.MemoryRegionCache<T>[] cache, int idx )
    {
        return cache != null && idx <= cache.length - 1 ? cache[idx] : null;
    }

    boolean allocateTiny( PoolArena<?> area, PooledByteBuf<?> buf, int reqCapacity, int normCapacity )
    {
        return this.allocate( this.cacheForTiny( area, normCapacity ), buf, reqCapacity );
    }

    boolean allocateSmall( PoolArena<?> area, PooledByteBuf<?> buf, int reqCapacity, int normCapacity )
    {
        return this.allocate( this.cacheForSmall( area, normCapacity ), buf, reqCapacity );
    }

    boolean allocateNormal( PoolArena<?> area, PooledByteBuf<?> buf, int reqCapacity, int normCapacity )
    {
        return this.allocate( this.cacheForNormal( area, normCapacity ), buf, reqCapacity );
    }

    private boolean allocate( PoolThreadCache.MemoryRegionCache<?> cache, PooledByteBuf buf, int reqCapacity )
    {
        if ( cache == null )
        {
            return false;
        }
        else
        {
            boolean allocated = cache.allocate( buf, reqCapacity );
            if ( ++this.allocations >= this.freeSweepAllocationThreshold )
            {
                this.allocations = 0;
                this.trim();
            }

            return allocated;
        }
    }

    boolean add( PoolArena<?> area, PoolChunk chunk, ByteBuffer nioBuffer, long handle, int normCapacity, PoolArena.SizeClass sizeClass )
    {
        PoolThreadCache.MemoryRegionCache<?> cache = this.cache( area, normCapacity, sizeClass );
        return cache == null ? false : cache.add( chunk, nioBuffer, handle );
    }

    private PoolThreadCache.MemoryRegionCache<?> cache( PoolArena<?> area, int normCapacity, PoolArena.SizeClass sizeClass )
    {
        switch ( sizeClass )
        {
        case Normal:
            return this.cacheForNormal( area, normCapacity );
        case Small:
            return this.cacheForSmall( area, normCapacity );
        case Tiny:
            return this.cacheForTiny( area, normCapacity );
        default:
            throw new Error();
        }
    }

    protected void finalize() throws Throwable
    {
        try
        {
            super.finalize();
        }
        finally
        {
            this.free( true );
        }
    }

    void free( boolean finalizer )
    {
        if ( this.freed.compareAndSet( false, true ) )
        {
            int numFreed = free( this.tinySubPageDirectCaches, finalizer ) + free( this.smallSubPageDirectCaches, finalizer ) +
                    free( this.normalDirectCaches, finalizer ) + free( this.tinySubPageHeapCaches, finalizer ) +
                    free( this.smallSubPageHeapCaches, finalizer ) + free( this.normalHeapCaches, finalizer );
            if ( numFreed > 0 && logger.isDebugEnabled() )
            {
                logger.debug( "Freed {} thread-local buffer(s) from thread: {}", numFreed, Thread.currentThread().getName() );
            }

            if ( this.directArena != null )
            {
                this.directArena.numThreadCaches.getAndDecrement();
            }

            if ( this.heapArena != null )
            {
                this.heapArena.numThreadCaches.getAndDecrement();
            }
        }
    }

    void trim()
    {
        trim( this.tinySubPageDirectCaches );
        trim( this.smallSubPageDirectCaches );
        trim( this.normalDirectCaches );
        trim( this.tinySubPageHeapCaches );
        trim( this.smallSubPageHeapCaches );
        trim( this.normalHeapCaches );
    }

    private PoolThreadCache.MemoryRegionCache<?> cacheForTiny( PoolArena<?> area, int normCapacity )
    {
        int idx = PoolArena.tinyIdx( normCapacity );
        return area.isDirect() ? cache( this.tinySubPageDirectCaches, idx ) : cache( this.tinySubPageHeapCaches, idx );
    }

    private PoolThreadCache.MemoryRegionCache<?> cacheForSmall( PoolArena<?> area, int normCapacity )
    {
        int idx = PoolArena.smallIdx( normCapacity );
        return area.isDirect() ? cache( this.smallSubPageDirectCaches, idx ) : cache( this.smallSubPageHeapCaches, idx );
    }

    private PoolThreadCache.MemoryRegionCache<?> cacheForNormal( PoolArena<?> area, int normCapacity )
    {
        int idx;
        if ( area.isDirect() )
        {
            idx = log2( normCapacity >> this.numShiftsNormalDirect );
            return cache( this.normalDirectCaches, idx );
        }
        else
        {
            idx = log2( normCapacity >> this.numShiftsNormalHeap );
            return cache( this.normalHeapCaches, idx );
        }
    }

    private abstract static class MemoryRegionCache<T>
    {
        private static final Recycler<PoolThreadCache.MemoryRegionCache.Entry> RECYCLER = new Recycler<PoolThreadCache.MemoryRegionCache.Entry>()
        {
            protected PoolThreadCache.MemoryRegionCache.Entry newObject( Recycler.Handle<PoolThreadCache.MemoryRegionCache.Entry> handle )
            {
                return new PoolThreadCache.MemoryRegionCache.Entry( handle );
            }
        };
        private final int size;
        private final Queue<PoolThreadCache.MemoryRegionCache.Entry<T>> queue;
        private final PoolArena.SizeClass sizeClass;
        private int allocations;

        MemoryRegionCache( int size, PoolArena.SizeClass sizeClass )
        {
            this.size = MathUtil.safeFindNextPositivePowerOfTwo( size );
            this.queue = PlatformDependent.newFixedMpscQueue( this.size );
            this.sizeClass = sizeClass;
        }

        private static PoolThreadCache.MemoryRegionCache.Entry newEntry( PoolChunk<?> chunk, ByteBuffer nioBuffer, long handle )
        {
            PoolThreadCache.MemoryRegionCache.Entry entry = (PoolThreadCache.MemoryRegionCache.Entry) RECYCLER.get();
            entry.chunk = chunk;
            entry.nioBuffer = nioBuffer;
            entry.handle = handle;
            return entry;
        }

        protected abstract void initBuf( PoolChunk<T> var1, ByteBuffer var2, long var3, PooledByteBuf<T> var5, int var6 );

        public final boolean add( PoolChunk<T> chunk, ByteBuffer nioBuffer, long handle )
        {
            PoolThreadCache.MemoryRegionCache.Entry<T> entry = newEntry( chunk, nioBuffer, handle );
            boolean queued = this.queue.offer( entry );
            if ( !queued )
            {
                entry.recycle();
            }

            return queued;
        }

        public final boolean allocate( PooledByteBuf<T> buf, int reqCapacity )
        {
            PoolThreadCache.MemoryRegionCache.Entry<T> entry = (PoolThreadCache.MemoryRegionCache.Entry) this.queue.poll();
            if ( entry == null )
            {
                return false;
            }
            else
            {
                this.initBuf( entry.chunk, entry.nioBuffer, entry.handle, buf, reqCapacity );
                entry.recycle();
                ++this.allocations;
                return true;
            }
        }

        public final int free( boolean finalizer )
        {
            return this.free( Integer.MAX_VALUE, finalizer );
        }

        private int free( int max, boolean finalizer )
        {
            int numFreed;
            for ( numFreed = 0; numFreed < max; ++numFreed )
            {
                PoolThreadCache.MemoryRegionCache.Entry<T> entry = (PoolThreadCache.MemoryRegionCache.Entry) this.queue.poll();
                if ( entry == null )
                {
                    return numFreed;
                }

                this.freeEntry( entry, finalizer );
            }

            return numFreed;
        }

        public final void trim()
        {
            int free = this.size - this.allocations;
            this.allocations = 0;
            if ( free > 0 )
            {
                this.free( free, false );
            }
        }

        private void freeEntry( PoolThreadCache.MemoryRegionCache.Entry entry, boolean finalizer )
        {
            PoolChunk chunk = entry.chunk;
            long handle = entry.handle;
            ByteBuffer nioBuffer = entry.nioBuffer;
            if ( !finalizer )
            {
                entry.recycle();
            }

            chunk.arena.freeChunk( chunk, handle, this.sizeClass, nioBuffer, finalizer );
        }

        static final class Entry<T>
        {
            final Recycler.Handle<PoolThreadCache.MemoryRegionCache.Entry<?>> recyclerHandle;
            PoolChunk<T> chunk;
            ByteBuffer nioBuffer;
            long handle = -1L;

            Entry( Recycler.Handle<PoolThreadCache.MemoryRegionCache.Entry<?>> recyclerHandle )
            {
                this.recyclerHandle = recyclerHandle;
            }

            void recycle()
            {
                this.chunk = null;
                this.nioBuffer = null;
                this.handle = -1L;
                this.recyclerHandle.recycle( this );
            }
        }
    }

    private static final class NormalMemoryRegionCache<T> extends PoolThreadCache.MemoryRegionCache<T>
    {
        NormalMemoryRegionCache( int size )
        {
            super( size, PoolArena.SizeClass.Normal );
        }

        protected void initBuf( PoolChunk<T> chunk, ByteBuffer nioBuffer, long handle, PooledByteBuf<T> buf, int reqCapacity )
        {
            chunk.initBuf( buf, nioBuffer, handle, reqCapacity );
        }
    }

    private static final class SubPageMemoryRegionCache<T> extends PoolThreadCache.MemoryRegionCache<T>
    {
        SubPageMemoryRegionCache( int size, PoolArena.SizeClass sizeClass )
        {
            super( size, sizeClass );
        }

        protected void initBuf( PoolChunk<T> chunk, ByteBuffer nioBuffer, long handle, PooledByteBuf<T> buf, int reqCapacity )
        {
            chunk.initBufWithSubpage( buf, nioBuffer, handle, reqCapacity );
        }
    }
}
