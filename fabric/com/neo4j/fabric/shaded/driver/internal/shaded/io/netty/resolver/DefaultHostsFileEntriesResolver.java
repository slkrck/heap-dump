package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.resolver;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.CharsetUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;

public final class DefaultHostsFileEntriesResolver implements HostsFileEntriesResolver
{
    private final Map<String,Inet4Address> inet4Entries;
    private final Map<String,Inet6Address> inet6Entries;

    public DefaultHostsFileEntriesResolver()
    {
        this( parseEntries() );
    }

    DefaultHostsFileEntriesResolver( HostsFileEntries entries )
    {
        this.inet4Entries = entries.inet4Entries();
        this.inet6Entries = entries.inet6Entries();
    }

    private static HostsFileEntries parseEntries()
    {
        return PlatformDependent.isWindows() ? HostsFileParser.parseSilently( Charset.defaultCharset(), CharsetUtil.UTF_16, CharsetUtil.UTF_8 )
                                             : HostsFileParser.parseSilently();
    }

    public InetAddress address( String inetHost, ResolvedAddressTypes resolvedAddressTypes )
    {
        String normalized = this.normalize( inetHost );
        switch ( resolvedAddressTypes )
        {
        case IPV4_ONLY:
            return (InetAddress) this.inet4Entries.get( normalized );
        case IPV6_ONLY:
            return (InetAddress) this.inet6Entries.get( normalized );
        case IPV4_PREFERRED:
            Inet4Address inet4Address = (Inet4Address) this.inet4Entries.get( normalized );
            return (InetAddress) (inet4Address != null ? inet4Address : (InetAddress) this.inet6Entries.get( normalized ));
        case IPV6_PREFERRED:
            Inet6Address inet6Address = (Inet6Address) this.inet6Entries.get( normalized );
            return (InetAddress) (inet6Address != null ? inet6Address : (InetAddress) this.inet4Entries.get( normalized ));
        default:
            throw new IllegalArgumentException( "Unknown ResolvedAddressTypes " + resolvedAddressTypes );
        }
    }

    String normalize( String inetHost )
    {
        return inetHost.toLowerCase( Locale.ENGLISH );
    }
}
