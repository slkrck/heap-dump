package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxBufferBoundary<T, U, C extends Collection<? super T>> extends InternalFluxOperator<T,C>
{
    final Publisher<U> other;
    final Supplier<C> bufferSupplier;

    FluxBufferBoundary( Flux<? extends T> source, Publisher<U> other, Supplier<C> bufferSupplier )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
        this.bufferSupplier = (Supplier) Objects.requireNonNull( bufferSupplier, "bufferSupplier" );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super C> actual )
    {
        Collection buffer;
        try
        {
            buffer = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null buffer" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        FluxBufferBoundary.BufferBoundaryMain<T,U,C> parent =
                new FluxBufferBoundary.BufferBoundaryMain( this.source instanceof FluxInterval ? actual : Operators.serialize( actual ), buffer,
                        this.bufferSupplier );
        actual.onSubscribe( parent );
        this.other.subscribe( parent.other );
        return parent;
    }

    static final class BufferBoundaryOther<U> extends Operators.DeferredSubscription implements InnerConsumer<U>
    {
        final FluxBufferBoundary.BufferBoundaryMain<?,U,?> main;

        BufferBoundaryOther( FluxBufferBoundary.BufferBoundaryMain<?,U,?> main )
        {
            this.main = main;
        }

        public void onSubscribe( Subscription s )
        {
            if ( this.set( s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.ACTUAL ? this.main : super.scanUnsafe( key );
        }

        public void onNext( U t )
        {
            this.main.otherNext();
        }

        public void onError( Throwable t )
        {
            this.main.otherError( t );
        }

        public void onComplete()
        {
            this.main.otherComplete();
        }
    }

    static final class BufferBoundaryMain<T, U, C extends Collection<? super T>> implements InnerOperator<T,C>
    {
        static final AtomicReferenceFieldUpdater<FluxBufferBoundary.BufferBoundaryMain,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxBufferBoundary.BufferBoundaryMain.class, Subscription.class, "s" );
        static final AtomicLongFieldUpdater<FluxBufferBoundary.BufferBoundaryMain> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxBufferBoundary.BufferBoundaryMain.class, "requested" );
        final Supplier<C> bufferSupplier;
        final CoreSubscriber<? super C> actual;
        final Context ctx;
        final FluxBufferBoundary.BufferBoundaryOther<U> other;
        C buffer;
        volatile Subscription s;
        volatile long requested;

        BufferBoundaryMain( CoreSubscriber<? super C> actual, C buffer, Supplier<C> bufferSupplier )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.buffer = buffer;
            this.bufferSupplier = bufferSupplier;
            this.other = new FluxBufferBoundary.BufferBoundaryOther( this );
        }

        public CoreSubscriber<? super C> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                C buffer = this.buffer;
                return buffer != null ? buffer.size() : 0;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.requested : InnerOperator.super.scanUnsafe( key );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
            }
        }

        public void cancel()
        {
            Operators.terminate( S, this );
            Operators.onDiscardMultiple( this.buffer, this.ctx );
            this.other.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            synchronized ( this )
            {
                C b = this.buffer;
                if ( b != null )
                {
                    b.add( t );
                    return;
                }
            }

            Operators.onNextDropped( t, this.ctx );
        }

        public void onError( Throwable t )
        {
            if ( Operators.terminate( S, this ) )
            {
                Collection b;
                synchronized ( this )
                {
                    b = this.buffer;
                    this.buffer = null;
                }

                this.other.cancel();
                this.actual.onError( t );
                Operators.onDiscardMultiple( b, this.ctx );
            }
            else
            {
                Operators.onErrorDropped( t, this.ctx );
            }
        }

        public void onComplete()
        {
            if ( Operators.terminate( S, this ) )
            {
                Collection b;
                synchronized ( this )
                {
                    b = this.buffer;
                    this.buffer = null;
                }

                this.other.cancel();
                if ( !b.isEmpty() )
                {
                    if ( this.emit( b ) )
                    {
                        this.actual.onComplete();
                    }
                }
                else
                {
                    this.actual.onComplete();
                }
            }
        }

        void otherComplete()
        {
            Subscription s = (Subscription) S.getAndSet( this, Operators.cancelledSubscription() );
            if ( s != Operators.cancelledSubscription() )
            {
                Collection b;
                synchronized ( this )
                {
                    b = this.buffer;
                    this.buffer = null;
                }

                if ( s != null )
                {
                    s.cancel();
                }

                if ( b != null && !b.isEmpty() )
                {
                    if ( this.emit( b ) )
                    {
                        this.actual.onComplete();
                    }
                }
                else
                {
                    this.actual.onComplete();
                }
            }
        }

        void otherError( Throwable t )
        {
            Subscription s = (Subscription) S.getAndSet( this, Operators.cancelledSubscription() );
            if ( s != Operators.cancelledSubscription() )
            {
                Collection b;
                synchronized ( this )
                {
                    b = this.buffer;
                    this.buffer = null;
                }

                if ( s != null )
                {
                    s.cancel();
                }

                this.actual.onError( t );
                Operators.onDiscardMultiple( b, this.ctx );
            }
            else
            {
                Operators.onErrorDropped( t, this.ctx );
            }
        }

        void otherNext()
        {
            Collection c;
            try
            {
                c = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null buffer" );
            }
            catch ( Throwable var6 )
            {
                this.otherError( Operators.onOperatorError( this.other, var6, this.ctx ) );
                return;
            }

            Collection b;
            synchronized ( this )
            {
                b = this.buffer;
                this.buffer = c;
            }

            if ( b != null && !b.isEmpty() )
            {
                this.emit( b );
            }
        }

        boolean emit( C b )
        {
            long r = this.requested;
            if ( r != 0L )
            {
                this.actual.onNext( b );
                if ( r != Long.MAX_VALUE )
                {
                    REQUESTED.decrementAndGet( this );
                }

                return true;
            }
            else
            {
                this.actual.onError( Operators.onOperatorError( this, Exceptions.failWithOverflow(), b, this.ctx ) );
                Operators.onDiscardMultiple( b, this.ctx );
                return false;
            }
        }
    }
}
