package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.WaitStrategy;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

/**
 * @deprecated
 */
@Deprecated
public final class TopicProcessor<E> extends EventLoopProcessor<E>
{
    final RingBuffer.Reader barrier;
    final RingBuffer.Sequence minimum = RingBuffer.newSequence( -1L );

    TopicProcessor( @Nullable ThreadFactory threadFactory, @Nullable ExecutorService executor, ExecutorService requestTaskExecutor, int bufferSize,
            WaitStrategy waitStrategy, boolean shared, boolean autoCancel, @Nullable Supplier<E> signalSupplier )
    {
        super( bufferSize, threadFactory, executor, requestTaskExecutor, autoCancel, shared, () -> {
            EventLoopProcessor.Slot<E> signal = new EventLoopProcessor.Slot();
            if ( signalSupplier != null )
            {
                signal.value = signalSupplier.get();
            }

            return signal;
        }, waitStrategy );
        this.barrier = this.ringBuffer.newReader();
    }

    public static <E> TopicProcessor.Builder<E> builder()
    {
        return new TopicProcessor.Builder();
    }

    public static <E> TopicProcessor<E> create()
    {
        return builder().build();
    }

    public static <E> TopicProcessor<E> create( String name, int bufferSize )
    {
        return builder().name( name ).bufferSize( bufferSize ).build();
    }

    public static <E> TopicProcessor<E> share( String name, int bufferSize )
    {
        return builder().share( true ).name( name ).bufferSize( bufferSize ).build();
    }

    public void subscribe( CoreSubscriber<? super E> actual )
    {
        Objects.requireNonNull( actual, "subscribe" );
        if ( !this.alive() )
        {
            coldSource( this.ringBuffer, (Throwable) null, this.error, this.minimum ).subscribe( actual );
        }
        else
        {
            RingBuffer.Sequence pendingRequest = RingBuffer.newSequence( 0L );
            TopicProcessor.TopicInner<E> signalProcessor = new TopicProcessor.TopicInner( this, pendingRequest, actual );
            if ( this.incrementSubscribers() )
            {
                signalProcessor.sequence.set( this.minimum.getAsLong() );
                this.ringBuffer.addGatingSequence( signalProcessor.sequence );
            }
            else
            {
                signalProcessor.sequence.set( this.ringBuffer.getCursor() );
                this.ringBuffer.addGatingSequence( signalProcessor.sequence );
            }

            try
            {
                this.executor.execute( signalProcessor );
            }
            catch ( Throwable var5 )
            {
                this.ringBuffer.removeGatingSequence( signalProcessor.sequence );
                this.decrementSubscribers();
                if ( !this.alive() && RejectedExecutionException.class.isAssignableFrom( var5.getClass() ) )
                {
                    coldSource( this.ringBuffer, var5, this.error, this.minimum ).subscribe( actual );
                }
                else
                {
                    Operators.error( actual, var5 );
                }
            }
        }
    }

    public Flux<E> drain()
    {
        return coldSource( this.ringBuffer, (Throwable) null, this.error, this.minimum );
    }

    protected void doError( Throwable t )
    {
        this.barrier.signal();
    }

    protected void doComplete()
    {
        this.barrier.signal();
    }

    public long getPending()
    {
        return (long) this.ringBuffer.getPending();
    }

    protected void requestTask( Subscription s )
    {
        this.minimum.set( this.ringBuffer.getCursor() );
        this.ringBuffer.addGatingSequence( this.minimum );
        this.requestTaskExecutor.execute( createRequestTask( s, this, this.minimum::set, () -> {
            return SUBSCRIBER_COUNT.get( this ) == 0 ? this.minimum.getAsLong() : this.ringBuffer.getMinimumGatingSequence( this.minimum );
        } ) );
    }

    public void run()
    {
        if ( !this.alive() && SUBSCRIBER_COUNT.get( this ) == 0 )
        {
            WaitStrategy.alert();
        }
    }

    static final class TopicInner<T> implements Runnable, Subscription, Scannable
    {
        final AtomicBoolean running = new AtomicBoolean( true );
        final RingBuffer.Sequence sequence = RingBuffer.newSequence( -1L );
        final TopicProcessor<T> processor;
        final RingBuffer.Sequence pendingRequest;
        final CoreSubscriber<? super T> subscriber;
        final Runnable waiter = new Runnable()
        {
            public void run()
            {
                if ( !TopicInner.this.running.get() || TopicInner.this.processor.isTerminated() )
                {
                    WaitStrategy.alert();
                }
            }
        };

        TopicInner( TopicProcessor<T> processor, RingBuffer.Sequence pendingRequest, CoreSubscriber<? super T> subscriber )
        {
            this.processor = processor;
            this.pendingRequest = pendingRequest;
            this.subscriber = subscriber;
        }

        void halt()
        {
            this.running.set( false );
            this.processor.barrier.alert();
        }

        public void run()
        {
            try
            {
                Thread.currentThread().setContextClassLoader( this.processor.contextClassLoader );
                this.subscriber.onSubscribe( this );
                if ( !EventLoopProcessor.waitRequestOrTerminalEvent( this.pendingRequest, this.processor.barrier, this.running, this.sequence, this.waiter ) )
                {
                    if ( !this.running.get() )
                    {
                        return;
                    }

                    if ( this.processor.terminated == 1 )
                    {
                        if ( this.processor.ringBuffer.getAsLong() == -1L )
                        {
                            if ( this.processor.error != null )
                            {
                                this.subscriber.onError( this.processor.error );
                                return;
                            }

                            this.subscriber.onComplete();
                            return;
                        }
                    }
                    else if ( this.processor.terminated == 2 )
                    {
                        return;
                    }
                }

                long nextSequence = this.sequence.getAsLong() + 1L;
                boolean unbounded = this.pendingRequest.getAsLong() == Long.MAX_VALUE;

                while ( true )
                {
                    try
                    {
                        long availableSequence;
                        for ( availableSequence = this.processor.barrier.waitFor( nextSequence, this.waiter ); nextSequence <= availableSequence;
                                ++nextSequence )
                        {
                            EventLoopProcessor.Slot event;
                            for ( event = (EventLoopProcessor.Slot) this.processor.ringBuffer.get( nextSequence );
                                    !unbounded && EventLoopProcessor.getAndSub( this.pendingRequest, 1L ) == 0L; LockSupport.parkNanos( 1L ) )
                            {
                                if ( !this.running.get() || this.processor.isTerminated() )
                                {
                                    WaitStrategy.alert();
                                }
                            }

                            this.subscriber.onNext( event.value );
                        }

                        this.sequence.set( availableSequence );
                        if ( Operators.emptySubscription() != this.processor.upstreamSubscription )
                        {
                            this.processor.readWait.signalAllWhenBlocking();
                        }
                    }
                    catch ( Throwable var10 )
                    {
                        if ( !WaitStrategy.isAlert( var10 ) && !Exceptions.isCancel( var10 ) )
                        {
                            throw Exceptions.propagate( var10 );
                        }

                        if ( !this.running.get() )
                        {
                            return;
                        }

                        if ( this.processor.terminated == 1 )
                        {
                            if ( this.processor.error != null )
                            {
                                this.subscriber.onError( this.processor.error );
                                return;
                            }

                            if ( nextSequence > this.processor.ringBuffer.getAsLong() )
                            {
                                this.subscriber.onComplete();
                                return;
                            }

                            LockSupport.parkNanos( 1L );
                        }
                        else if ( this.processor.terminated == 2 )
                        {
                            return;
                        }

                        this.processor.barrier.clearAlert();
                    }
                }
            }
            finally
            {
                this.processor.ringBuffer.removeGatingSequence( this.sequence );
                this.processor.decrementSubscribers();
                this.running.set( false );
                this.processor.readWait.signalAllWhenBlocking();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.processor;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.subscriber;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.processor.isTerminated();
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return !this.running.get();
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.pendingRequest.getAsLong();
            }
            else if ( key == Scannable.Attr.LARGE_BUFFERED )
            {
                return this.processor.ringBuffer.getCursor() - this.sequence.getAsLong();
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                long realBuffered = this.processor.ringBuffer.getCursor() - this.sequence.getAsLong();
                return realBuffered <= 2147483647L ? (int) realBuffered : Integer.MIN_VALUE;
            }
            else
            {
                return null;
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && this.running.get() )
            {
                EventLoopProcessor.addCap( this.pendingRequest, n );
            }
        }

        public void cancel()
        {
            this.halt();
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static final class Builder<T>
    {
        String name;
        ExecutorService executor;
        ExecutorService requestTaskExecutor;
        int bufferSize;
        WaitStrategy waitStrategy;
        boolean share;
        boolean autoCancel;
        Supplier<T> signalSupplier;

        Builder()
        {
            this.bufferSize = Queues.SMALL_BUFFER_SIZE;
            this.autoCancel = true;
            this.share = false;
        }

        public TopicProcessor.Builder<T> name( @Nullable String name )
        {
            if ( this.executor != null )
            {
                throw new IllegalArgumentException( "Executor service is configured, name will not be used." );
            }
            else
            {
                this.name = name;
                return this;
            }
        }

        public TopicProcessor.Builder<T> bufferSize( int bufferSize )
        {
            if ( !Queues.isPowerOfTwo( bufferSize ) )
            {
                throw new IllegalArgumentException( "bufferSize must be a power of 2 : " + bufferSize );
            }
            else if ( bufferSize < 1 )
            {
                throw new IllegalArgumentException( "bufferSize must be strictly positive, was: " + bufferSize );
            }
            else
            {
                this.bufferSize = bufferSize;
                return this;
            }
        }

        public TopicProcessor.Builder<T> waitStrategy( @Nullable WaitStrategy waitStrategy )
        {
            this.waitStrategy = waitStrategy;
            return this;
        }

        public TopicProcessor.Builder<T> autoCancel( boolean autoCancel )
        {
            this.autoCancel = autoCancel;
            return this;
        }

        public TopicProcessor.Builder<T> executor( @Nullable ExecutorService executor )
        {
            this.executor = executor;
            return this;
        }

        public TopicProcessor.Builder<T> requestTaskExecutor( @Nullable ExecutorService requestTaskExecutor )
        {
            this.requestTaskExecutor = requestTaskExecutor;
            return this;
        }

        public TopicProcessor.Builder<T> share( boolean share )
        {
            this.share = share;
            return this;
        }

        public TopicProcessor.Builder<T> signalSupplier( @Nullable Supplier<T> signalSupplier )
        {
            this.signalSupplier = signalSupplier;
            return this;
        }

        public TopicProcessor<T> build()
        {
            this.name = this.name != null ? this.name : TopicProcessor.class.getSimpleName();
            this.waitStrategy = this.waitStrategy != null ? this.waitStrategy : WaitStrategy.phasedOffLiteLock( 200L, 100L, TimeUnit.MILLISECONDS );
            ThreadFactory threadFactory = this.executor != null ? null : new EventLoopProcessor.EventLoopFactory( this.name, this.autoCancel );
            ExecutorService requestTaskExecutor = this.requestTaskExecutor != null ? this.requestTaskExecutor : EventLoopProcessor.defaultRequestTaskExecutor(
                    EventLoopProcessor.defaultName( threadFactory, TopicProcessor.class ) );
            return new TopicProcessor( threadFactory, this.executor, requestTaskExecutor, this.bufferSize, this.waitStrategy, this.share, this.autoCancel,
                    this.signalSupplier );
        }
    }
}
