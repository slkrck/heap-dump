package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import java.util.function.Function;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;

@Metadata( mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3 )
final class MonoWhenFunctionsKt$sam$java_util_function_Function$0 implements Function
{
    MonoWhenFunctionsKt$sam$java_util_function_Function$0( Function1 var1 )
    {
        this.function = var1;
    }
}
