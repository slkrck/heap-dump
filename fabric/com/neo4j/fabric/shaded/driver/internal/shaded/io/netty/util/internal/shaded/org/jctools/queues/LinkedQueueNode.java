package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.shaded.org.jctools.queues;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.shaded.org.jctools.util.UnsafeAccess;

final class LinkedQueueNode<E>
{
    private static final long NEXT_OFFSET;

    static
    {
        try
        {
            NEXT_OFFSET = UnsafeAccess.UNSAFE.objectFieldOffset( LinkedQueueNode.class.getDeclaredField( "next" ) );
        }
        catch ( NoSuchFieldException var1 )
        {
            throw new RuntimeException( var1 );
        }
    }

    private E value;
    private volatile LinkedQueueNode<E> next;

    LinkedQueueNode()
    {
        this( (Object) null );
    }

    LinkedQueueNode( E val )
    {
        this.spValue( val );
    }

    public E getAndNullValue()
    {
        E temp = this.lpValue();
        this.spValue( (Object) null );
        return temp;
    }

    public E lpValue()
    {
        return this.value;
    }

    public void spValue( E newValue )
    {
        this.value = newValue;
    }

    public void soNext( LinkedQueueNode<E> n )
    {
        UnsafeAccess.UNSAFE.putOrderedObject( this, NEXT_OFFSET, n );
    }

    public LinkedQueueNode<E> lvNext()
    {
        return this.next;
    }
}
