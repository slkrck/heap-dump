package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Consumer;

import org.reactivestreams.Subscription;

final class LambdaMonoSubscriber<T> implements InnerConsumer<T>, Disposable
{
    static final AtomicReferenceFieldUpdater<LambdaMonoSubscriber,Subscription> S =
            AtomicReferenceFieldUpdater.newUpdater( LambdaMonoSubscriber.class, Subscription.class, "subscription" );
    final Consumer<? super T> consumer;
    final Consumer<? super Throwable> errorConsumer;
    final Runnable completeConsumer;
    final Consumer<? super Subscription> subscriptionConsumer;
    final Context initialContext;
    volatile Subscription subscription;

    LambdaMonoSubscriber( @Nullable Consumer<? super T> consumer, @Nullable Consumer<? super Throwable> errorConsumer, @Nullable Runnable completeConsumer,
            @Nullable Consumer<? super Subscription> subscriptionConsumer, @Nullable Context initialContext )
    {
        this.consumer = consumer;
        this.errorConsumer = errorConsumer;
        this.completeConsumer = completeConsumer;
        this.subscriptionConsumer = subscriptionConsumer;
        this.initialContext = initialContext == null ? Context.empty() : initialContext;
    }

    LambdaMonoSubscriber( @Nullable Consumer<? super T> consumer, @Nullable Consumer<? super Throwable> errorConsumer, @Nullable Runnable completeConsumer,
            @Nullable Consumer<? super Subscription> subscriptionConsumer )
    {
        this( consumer, errorConsumer, completeConsumer, subscriptionConsumer, (Context) null );
    }

    public Context currentContext()
    {
        return this.initialContext;
    }

    public final void onSubscribe( Subscription s )
    {
        if ( Operators.validate( this.subscription, s ) )
        {
            this.subscription = s;
            if ( this.subscriptionConsumer != null )
            {
                try
                {
                    this.subscriptionConsumer.accept( s );
                }
                catch ( Throwable var3 )
                {
                    Exceptions.throwIfFatal( var3 );
                    s.cancel();
                    this.onError( var3 );
                }
            }
            else
            {
                s.request( Long.MAX_VALUE );
            }
        }
    }

    public final void onComplete()
    {
        Subscription s = (Subscription) S.getAndSet( this, Operators.cancelledSubscription() );
        if ( s != Operators.cancelledSubscription() )
        {
            if ( this.completeConsumer != null )
            {
                try
                {
                    this.completeConsumer.run();
                }
                catch ( Throwable var3 )
                {
                    Operators.onErrorDropped( var3, this.initialContext );
                }
            }
        }
    }

    public final void onError( Throwable t )
    {
        Subscription s = (Subscription) S.getAndSet( this, Operators.cancelledSubscription() );
        if ( s == Operators.cancelledSubscription() )
        {
            Operators.onErrorDropped( t, this.initialContext );
        }
        else
        {
            this.doError( t );
        }
    }

    void doError( Throwable t )
    {
        if ( this.errorConsumer != null )
        {
            this.errorConsumer.accept( t );
        }
        else
        {
            throw Exceptions.errorCallbackNotImplemented( t );
        }
    }

    public final void onNext( T x )
    {
        Subscription s = (Subscription) S.getAndSet( this, Operators.cancelledSubscription() );
        if ( s == Operators.cancelledSubscription() )
        {
            Operators.onNextDropped( x, this.initialContext );
        }
        else
        {
            if ( this.consumer != null )
            {
                try
                {
                    this.consumer.accept( x );
                }
                catch ( Throwable var5 )
                {
                    Operators.onErrorDropped( var5, this.initialContext );
                    return;
                }
            }

            if ( this.completeConsumer != null )
            {
                try
                {
                    this.completeConsumer.run();
                }
                catch ( Throwable var4 )
                {
                    Operators.onErrorDropped( var4, this.initialContext );
                }
            }
        }
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.subscription;
        }
        else if ( key == Scannable.Attr.PREFETCH )
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED ? null : this.isDisposed();
        }
    }

    public boolean isDisposed()
    {
        return this.subscription == Operators.cancelledSubscription();
    }

    public void dispose()
    {
        Subscription s = (Subscription) S.getAndSet( this, Operators.cancelledSubscription() );
        if ( s != null && s != Operators.cancelledSubscription() )
        {
            s.cancel();
        }
    }
}
