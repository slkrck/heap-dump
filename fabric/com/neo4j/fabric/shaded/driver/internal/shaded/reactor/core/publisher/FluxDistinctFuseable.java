package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

final class FluxDistinctFuseable<T, K, C> extends InternalFluxOperator<T,T> implements Fuseable
{
    final Function<? super T,? extends K> keyExtractor;
    final Supplier<C> collectionSupplier;
    final BiPredicate<C,K> distinctPredicate;
    final Consumer<C> cleanupCallback;

    FluxDistinctFuseable( Flux<? extends T> source, Function<? super T,? extends K> keyExtractor, Supplier<C> collectionSupplier,
            BiPredicate<C,K> distinctPredicate, Consumer<C> cleanupCallback )
    {
        super( source );
        this.keyExtractor = (Function) Objects.requireNonNull( keyExtractor, "keyExtractor" );
        this.collectionSupplier = (Supplier) Objects.requireNonNull( collectionSupplier, "collectionSupplier" );
        this.distinctPredicate = (BiPredicate) Objects.requireNonNull( distinctPredicate, "distinctPredicate" );
        this.cleanupCallback = (Consumer) Objects.requireNonNull( cleanupCallback, "cleanupCallback" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        Object collection;
        try
        {
            collection = Objects.requireNonNull( this.collectionSupplier.get(), "The collectionSupplier returned a null collection" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        return new FluxDistinct.DistinctFuseableSubscriber( actual, collection, this.keyExtractor, this.distinctPredicate, this.cleanupCallback );
    }
}
