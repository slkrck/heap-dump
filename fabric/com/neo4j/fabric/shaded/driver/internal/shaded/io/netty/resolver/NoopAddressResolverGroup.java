package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.resolver;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;

import java.net.SocketAddress;

public final class NoopAddressResolverGroup extends AddressResolverGroup<SocketAddress>
{
    public static final NoopAddressResolverGroup INSTANCE = new NoopAddressResolverGroup();

    private NoopAddressResolverGroup()
    {
    }

    protected AddressResolver<SocketAddress> newResolver( EventExecutor executor ) throws Exception
    {
        return new NoopAddressResolver( executor );
    }
}
