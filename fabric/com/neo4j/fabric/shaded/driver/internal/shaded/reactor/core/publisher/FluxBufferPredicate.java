package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.BiPredicate;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

final class FluxBufferPredicate<T, C extends Collection<? super T>> extends InternalFluxOperator<T,C>
{
    final Predicate<? super T> predicate;
    final Supplier<C> bufferSupplier;
    final FluxBufferPredicate.Mode mode;

    FluxBufferPredicate( Flux<? extends T> source, Predicate<? super T> predicate, Supplier<C> bufferSupplier, FluxBufferPredicate.Mode mode )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
        this.bufferSupplier = (Supplier) Objects.requireNonNull( bufferSupplier, "bufferSupplier" );
        this.mode = mode;
    }

    public int getPrefetch()
    {
        return 1;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super C> actual )
    {
        Collection initialBuffer;
        try
        {
            initialBuffer = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null initial buffer" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        FluxBufferPredicate.BufferPredicateSubscriber<T,C> parent =
                new FluxBufferPredicate.BufferPredicateSubscriber( actual, initialBuffer, this.bufferSupplier, this.predicate, this.mode );
        return parent;
    }

    public static enum Mode
    {
        UNTIL,
        UNTIL_CUT_BEFORE,
        WHILE;
    }

    static class ChangedPredicate<T, K> implements Predicate<T>, Disposable
    {
        private Function<? super T,? extends K> keySelector;
        private BiPredicate<? super K,? super K> keyComparator;
        private K lastKey;

        ChangedPredicate( Function<? super T,? extends K> keySelector, BiPredicate<? super K,? super K> keyComparator )
        {
            this.keySelector = keySelector;
            this.keyComparator = keyComparator;
        }

        public void dispose()
        {
            this.lastKey = null;
        }

        public boolean test( T t )
        {
            K k = this.keySelector.apply( t );
            if ( null == this.lastKey )
            {
                this.lastKey = k;
                return false;
            }
            else
            {
                boolean match = this.keyComparator.test( this.lastKey, k );
                this.lastKey = k;
                return !match;
            }
        }
    }

    static final class BufferPredicateSubscriber<T, C extends Collection<? super T>> extends AbstractQueue<C>
            implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,C>, BooleanSupplier
    {
        static final AtomicLongFieldUpdater<FluxBufferPredicate.BufferPredicateSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxBufferPredicate.BufferPredicateSubscriber.class, "requested" );
        static final AtomicReferenceFieldUpdater<FluxBufferPredicate.BufferPredicateSubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxBufferPredicate.BufferPredicateSubscriber.class, Subscription.class, "s" );
        final CoreSubscriber<? super C> actual;
        final Supplier<C> bufferSupplier;
        final FluxBufferPredicate.Mode mode;
        final Predicate<? super T> predicate;
        C buffer;
        boolean done;
        volatile boolean fastpath;
        volatile long requested;
        volatile Subscription s;

        BufferPredicateSubscriber( CoreSubscriber<? super C> actual, C initialBuffer, Supplier<C> bufferSupplier, Predicate<? super T> predicate,
                FluxBufferPredicate.Mode mode )
        {
            this.actual = actual;
            this.buffer = initialBuffer;
            this.bufferSupplier = bufferSupplier;
            this.predicate = predicate;
            this.mode = mode;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( n == Long.MAX_VALUE )
                {
                    this.fastpath = true;
                    this.requested = Long.MAX_VALUE;
                    this.s.request( Long.MAX_VALUE );
                }
                else if ( !DrainUtils.postCompleteRequest( n, this.actual, this, REQUESTED, this, this ) )
                {
                    this.s.request( 1L );
                }
            }
        }

        public void cancel()
        {
            this.cleanup();
            Operators.terminate( S, this );
            Operators.onDiscardMultiple( this.buffer, this.actual.currentContext() );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( !this.tryOnNext( t ) )
            {
                this.s.request( 1L );
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
                return true;
            }
            else
            {
                Collection b = this.buffer;

                boolean match;
                try
                {
                    match = this.predicate.test( t );
                }
                catch ( Throwable var6 )
                {
                    Context ctx = this.actual.currentContext();
                    this.onError( Operators.onOperatorError( this.s, var6, t, ctx ) );
                    Operators.onDiscardMultiple( this.buffer, ctx );
                    Operators.onDiscard( t, ctx );
                    return true;
                }

                boolean requestMore;
                if ( this.mode == FluxBufferPredicate.Mode.UNTIL && match )
                {
                    b.add( t );
                    requestMore = this.onNextNewBuffer();
                }
                else if ( this.mode == FluxBufferPredicate.Mode.UNTIL_CUT_BEFORE && match )
                {
                    requestMore = this.onNextNewBuffer();
                    b = this.buffer;
                    b.add( t );
                }
                else
                {
                    if ( this.mode != FluxBufferPredicate.Mode.WHILE || match )
                    {
                        b.add( t );
                        return this.fastpath || this.requested == 0L;
                    }

                    requestMore = this.onNextNewBuffer();
                }

                return !requestMore;
            }
        }

        @Nullable
        C triggerNewBuffer()
        {
            C b = this.buffer;
            if ( b.isEmpty() )
            {
                return null;
            }
            else
            {
                Collection c;
                try
                {
                    c = (Collection) Objects.requireNonNull( this.bufferSupplier.get(), "The bufferSupplier returned a null buffer" );
                }
                catch ( Throwable var4 )
                {
                    this.onError( Operators.onOperatorError( this.s, var4, this.actual.currentContext() ) );
                    return null;
                }

                this.buffer = c;
                return b;
            }
        }

        boolean onNextNewBuffer()
        {
            C b = this.triggerNewBuffer();
            return b != null ? this.emit( b ) : true;
        }

        public CoreSubscriber<? super C> actual()
        {
            return this.actual;
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.cleanup();
                Operators.onDiscardMultiple( this.buffer, this.actual.currentContext() );
                this.buffer = null;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.cleanup();
                DrainUtils.postComplete( this.actual, this, REQUESTED, this, this );
            }
        }

        boolean emit( C b )
        {
            if ( this.fastpath )
            {
                this.actual.onNext( b );
                return false;
            }
            else
            {
                long r = REQUESTED.getAndDecrement( this );
                if ( r > 0L )
                {
                    this.actual.onNext( b );
                    return this.requested > 0L;
                }
                else
                {
                    this.cancel();
                    this.actual.onError( Exceptions.failWithOverflow( "Could not emit buffer due to lack of requests" ) );
                    return false;
                }
            }
        }

        void cleanup()
        {
            if ( this.predicate instanceof Disposable )
            {
                ((Disposable) this.predicate).dispose();
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.getAsBoolean();
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                C b = this.buffer;
                return b != null ? b.size() : 0;
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.requested : InnerOperator.super.scanUnsafe( key );
            }
        }

        public boolean getAsBoolean()
        {
            return this.s == Operators.cancelledSubscription();
        }

        public Iterator<C> iterator()
        {
            return this.isEmpty() ? Collections.emptyIterator() : Collections.singleton( this.buffer ).iterator();
        }

        public boolean offer( C objects )
        {
            throw new IllegalArgumentException();
        }

        @Nullable
        public C poll()
        {
            C b = this.buffer;
            if ( b != null && !b.isEmpty() )
            {
                this.buffer = null;
                return b;
            }
            else
            {
                return null;
            }
        }

        @Nullable
        public C peek()
        {
            return this.buffer;
        }

        public int size()
        {
            return this.buffer != null && !this.buffer.isEmpty() ? 1 : 0;
        }

        public String toString()
        {
            return "FluxBufferPredicate";
        }
    }
}
