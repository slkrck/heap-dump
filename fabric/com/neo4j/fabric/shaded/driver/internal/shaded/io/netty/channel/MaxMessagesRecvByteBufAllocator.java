package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

public interface MaxMessagesRecvByteBufAllocator extends RecvByteBufAllocator
{
    int maxMessagesPerRead();

    MaxMessagesRecvByteBufAllocator maxMessagesPerRead( int var1 );
}
