package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Objects;
import java.util.function.BiConsumer;

final class MonoHandleFuseable<T, R> extends InternalMonoOperator<T,R> implements Fuseable
{
    final BiConsumer<? super T,SynchronousSink<R>> handler;

    MonoHandleFuseable( Mono<? extends T> source, BiConsumer<? super T,SynchronousSink<R>> handler )
    {
        super( source );
        this.handler = (BiConsumer) Objects.requireNonNull( handler, "handler" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        return new FluxHandleFuseable.HandleFuseableSubscriber( actual, this.handler );
    }
}
