package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Publisher;

final class MonoTimeout<T, U, V> extends InternalMonoOperator<T,T>
{
    static final Function NEVER = ( e ) -> {
        return Flux.never();
    };
    final Publisher<U> firstTimeout;
    final Publisher<? extends T> other;
    final String timeoutDescription;

    MonoTimeout( Mono<? extends T> source, Publisher<U> firstTimeout, String timeoutDescription )
    {
        super( source );
        this.firstTimeout = (Publisher) Objects.requireNonNull( firstTimeout, "firstTimeout" );
        this.other = null;
        this.timeoutDescription = timeoutDescription;
    }

    MonoTimeout( Mono<? extends T> source, Publisher<U> firstTimeout, Publisher<? extends T> other )
    {
        super( source );
        this.firstTimeout = (Publisher) Objects.requireNonNull( firstTimeout, "firstTimeout" );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
        this.timeoutDescription = null;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        CoreSubscriber<T> serial = Operators.serialize( actual );
        FluxTimeout.TimeoutMainSubscriber<T,V> main = new FluxTimeout.TimeoutMainSubscriber( serial, NEVER, this.other,
                FluxTimeout.addNameToTimeoutDescription( this.source, this.timeoutDescription ) );
        serial.onSubscribe( main );
        FluxTimeout.TimeoutTimeoutSubscriber ts = new FluxTimeout.TimeoutTimeoutSubscriber( main, 0L );
        main.setTimeout( ts );
        this.firstTimeout.subscribe( ts );
        return main;
    }
}
