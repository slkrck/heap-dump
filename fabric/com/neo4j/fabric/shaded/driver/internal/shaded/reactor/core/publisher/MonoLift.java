package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.BiFunction;

import org.reactivestreams.Publisher;

final class MonoLift<I, O> extends InternalMonoOperator<I,O>
{
    final BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter;

    MonoLift( Publisher<I> p, BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter )
    {
        super( Mono.from( p ) );
        this.lifter = lifter;
    }

    public CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super O> actual )
    {
        CoreSubscriber<? super I> input = (CoreSubscriber) this.lifter.apply( this.source, actual );
        Objects.requireNonNull( input, "Lifted subscriber MUST NOT be null" );
        return input;
    }
}
