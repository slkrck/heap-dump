package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

final class MonoDelay extends Mono<Long> implements Scannable, SourceProducer<Long>
{
    final Scheduler timedScheduler;
    final long delay;
    final TimeUnit unit;

    MonoDelay( long delay, TimeUnit unit, Scheduler timedScheduler )
    {
        this.delay = delay;
        this.unit = (TimeUnit) Objects.requireNonNull( unit, "unit" );
        this.timedScheduler = (Scheduler) Objects.requireNonNull( timedScheduler, "timedScheduler" );
    }

    public void subscribe( CoreSubscriber<? super Long> actual )
    {
        MonoDelay.MonoDelayRunnable r = new MonoDelay.MonoDelayRunnable( actual );
        actual.onSubscribe( r );

        try
        {
            r.setCancel( this.timedScheduler.schedule( r, this.delay, this.unit ) );
        }
        catch ( RejectedExecutionException var4 )
        {
            if ( r.cancel != OperatorDisposables.DISPOSED )
            {
                actual.onError( Operators.onRejectedExecution( var4, r, (Throwable) null, (Object) null, actual.currentContext() ) );
            }
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.timedScheduler : null;
    }

    static final class MonoDelayRunnable implements Runnable, InnerProducer<Long>
    {
        static final AtomicReferenceFieldUpdater<MonoDelay.MonoDelayRunnable,Disposable> CANCEL =
                AtomicReferenceFieldUpdater.newUpdater( MonoDelay.MonoDelayRunnable.class, Disposable.class, "cancel" );
        static final Disposable FINISHED = Disposables.disposed();
        final CoreSubscriber<? super Long> actual;
        volatile Disposable cancel;
        volatile boolean requested;

        MonoDelayRunnable( CoreSubscriber<? super Long> actual )
        {
            this.actual = actual;
        }

        public void setCancel( Disposable cancel )
        {
            if ( !CANCEL.compareAndSet( this, (Object) null, cancel ) )
            {
                cancel.dispose();
            }
        }

        public CoreSubscriber<? super Long> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.cancel == FINISHED;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.cancel == OperatorDisposables.DISPOSED : InnerProducer.super.scanUnsafe( key );
            }
        }

        public void run()
        {
            if ( this.requested )
            {
                try
                {
                    if ( CANCEL.getAndSet( this, FINISHED ) != OperatorDisposables.DISPOSED )
                    {
                        this.actual.onNext( 0L );
                        this.actual.onComplete();
                    }
                }
                catch ( Throwable var2 )
                {
                    this.actual.onError( Operators.onOperatorError( var2, this.actual.currentContext() ) );
                }
            }
            else
            {
                this.actual.onError( Exceptions.failWithOverflow( "Could not emit value due to lack of requests" ) );
            }
        }

        public void cancel()
        {
            Disposable c = this.cancel;
            if ( c != OperatorDisposables.DISPOSED && c != FINISHED )
            {
                c = (Disposable) CANCEL.getAndSet( this, OperatorDisposables.DISPOSED );
                if ( c != null && c != OperatorDisposables.DISPOSED && c != FINISHED )
                {
                    c.dispose();
                }
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                this.requested = true;
            }
        }
    }
}
