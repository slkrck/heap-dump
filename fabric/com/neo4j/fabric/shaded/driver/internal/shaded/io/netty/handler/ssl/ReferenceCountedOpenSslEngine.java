package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.Buffer;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.SSL;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AbstractReferenceCounted;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.CharsetUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCounted;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakDetector;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakDetectorFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakTracker;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.EmptyArrays;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ReadOnlyBufferException;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.locks.Lock;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionBindingEvent;
import javax.net.ssl.SSLSessionBindingListener;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLEngineResult.HandshakeStatus;
import javax.net.ssl.SSLEngineResult.Status;
import javax.security.cert.X509Certificate;

public class ReferenceCountedOpenSslEngine extends SSLEngine implements ReferenceCounted, ApplicationProtocolAccessor
{
    static final int MAX_PLAINTEXT_LENGTH;
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( ReferenceCountedOpenSslEngine.class );
    private static final ResourceLeakDetector<ReferenceCountedOpenSslEngine> leakDetector =
            ResourceLeakDetectorFactory.instance().newResourceLeakDetector( ReferenceCountedOpenSslEngine.class );
    private static final int OPENSSL_OP_NO_PROTOCOL_INDEX_SSLV2 = 0;
    private static final int OPENSSL_OP_NO_PROTOCOL_INDEX_SSLV3 = 1;
    private static final int OPENSSL_OP_NO_PROTOCOL_INDEX_TLSv1 = 2;
    private static final int OPENSSL_OP_NO_PROTOCOL_INDEX_TLSv1_1 = 3;
    private static final int OPENSSL_OP_NO_PROTOCOL_INDEX_TLSv1_2 = 4;
    private static final int OPENSSL_OP_NO_PROTOCOL_INDEX_TLSv1_3 = 5;
    private static final int[] OPENSSL_OP_NO_PROTOCOLS;
    private static final int MAX_RECORD_SIZE;
    private static final AtomicIntegerFieldUpdater<ReferenceCountedOpenSslEngine> DESTROYED_UPDATER;
    private static final SSLEngineResult NEED_UNWRAP_OK;
    private static final SSLEngineResult NEED_UNWRAP_CLOSED;
    private static final SSLEngineResult NEED_WRAP_OK;
    private static final SSLEngineResult NEED_WRAP_CLOSED;
    private static final SSLEngineResult CLOSED_NOT_HANDSHAKING;

    static
    {
        OPENSSL_OP_NO_PROTOCOLS =
                new int[]{SSL.SSL_OP_NO_SSLv2, SSL.SSL_OP_NO_SSLv3, SSL.SSL_OP_NO_TLSv1, SSL.SSL_OP_NO_TLSv1_1, SSL.SSL_OP_NO_TLSv1_2, SSL.SSL_OP_NO_TLSv1_3};
        MAX_PLAINTEXT_LENGTH = SSL.SSL_MAX_PLAINTEXT_LENGTH;
        MAX_RECORD_SIZE = SSL.SSL_MAX_RECORD_LENGTH;
        DESTROYED_UPDATER = AtomicIntegerFieldUpdater.newUpdater( ReferenceCountedOpenSslEngine.class, "destroyed" );
        NEED_UNWRAP_OK = new SSLEngineResult( Status.OK, HandshakeStatus.NEED_UNWRAP, 0, 0 );
        NEED_UNWRAP_CLOSED = new SSLEngineResult( Status.CLOSED, HandshakeStatus.NEED_UNWRAP, 0, 0 );
        NEED_WRAP_OK = new SSLEngineResult( Status.OK, HandshakeStatus.NEED_WRAP, 0, 0 );
        NEED_WRAP_CLOSED = new SSLEngineResult( Status.CLOSED, HandshakeStatus.NEED_WRAP, 0, 0 );
        CLOSED_NOT_HANDSHAKING = new SSLEngineResult( Status.CLOSED, HandshakeStatus.NOT_HANDSHAKING, 0, 0 );
    }

    final boolean jdkCompatibilityMode;
    final ByteBufAllocator alloc;
    private final ResourceLeakTracker<ReferenceCountedOpenSslEngine> leak;
    private final AbstractReferenceCounted refCnt;
    private final boolean clientMode;
    private final OpenSslEngineMap engineMap;
    private final OpenSslApplicationProtocolNegotiator apn;
    private final OpenSslSession session;
    private final ByteBuffer[] singleSrcBuffer;
    private final ByteBuffer[] singleDstBuffer;
    private final boolean enableOcsp;
    private long ssl;
    private long networkBIO;
    private ReferenceCountedOpenSslEngine.HandshakeState handshakeState;
    private boolean receivedShutdown;
    private volatile int destroyed;
    private volatile String applicationProtocol;
    private volatile boolean needTask;
    private volatile ClientAuth clientAuth;
    private volatile Certificate[] localCertificateChain;
    private volatile long lastAccessed;
    private String endPointIdentificationAlgorithm;
    private Object algorithmConstraints;
    private List<String> sniHostNames;
    private volatile Collection<?> matchers;
    private boolean isInboundDone;
    private boolean outboundClosed;
    private int maxWrapOverhead;
    private int maxWrapBufferSize;
    private Throwable handshakeException;

    ReferenceCountedOpenSslEngine( ReferenceCountedOpenSslContext context, ByteBufAllocator alloc, String peerHost, int peerPort, boolean jdkCompatibilityMode,
            boolean leakDetection )
    {
        super( peerHost, peerPort );
        this.handshakeState = ReferenceCountedOpenSslEngine.HandshakeState.NOT_STARTED;
        this.refCnt = new AbstractReferenceCounted()
        {
            public ReferenceCounted touch( Object hint )
            {
                if ( ReferenceCountedOpenSslEngine.this.leak != null )
                {
                    ReferenceCountedOpenSslEngine.this.leak.record( hint );
                }

                return ReferenceCountedOpenSslEngine.this;
            }

            protected void deallocate()
            {
                ReferenceCountedOpenSslEngine.this.shutdown();
                if ( ReferenceCountedOpenSslEngine.this.leak != null )
                {
                    boolean closed = ReferenceCountedOpenSslEngine.this.leak.close( ReferenceCountedOpenSslEngine.this );

                    assert closed;
                }
            }
        };
        this.clientAuth = ClientAuth.NONE;
        this.lastAccessed = -1L;
        this.singleSrcBuffer = new ByteBuffer[1];
        this.singleDstBuffer = new ByteBuffer[1];
        OpenSsl.ensureAvailability();
        this.alloc = (ByteBufAllocator) ObjectUtil.checkNotNull( alloc, "alloc" );
        this.apn = (OpenSslApplicationProtocolNegotiator) context.applicationProtocolNegotiator();
        this.clientMode = context.isClient();
        if ( PlatformDependent.javaVersion() >= 7 )
        {
            this.session = new ExtendedOpenSslSession( new ReferenceCountedOpenSslEngine.DefaultOpenSslSession( context.sessionContext() ) )
            {
                private String[] peerSupportedSignatureAlgorithms;
                private List requestedServerNames;

                public List getRequestedServerNames()
                {
                    if ( ReferenceCountedOpenSslEngine.this.clientMode )
                    {
                        return Java8SslUtils.getSniHostNames( ReferenceCountedOpenSslEngine.this.sniHostNames );
                    }
                    else
                    {
                        synchronized ( ReferenceCountedOpenSslEngine.this )
                        {
                            if ( this.requestedServerNames == null )
                            {
                                if ( ReferenceCountedOpenSslEngine.this.isDestroyed() )
                                {
                                    this.requestedServerNames = Collections.emptyList();
                                }
                                else
                                {
                                    String name = SSL.getSniHostname( ReferenceCountedOpenSslEngine.this.ssl );
                                    if ( name == null )
                                    {
                                        this.requestedServerNames = Collections.emptyList();
                                    }
                                    else
                                    {
                                        this.requestedServerNames = Java8SslUtils.getSniHostName(
                                                SSL.getSniHostname( ReferenceCountedOpenSslEngine.this.ssl ).getBytes( CharsetUtil.UTF_8 ) );
                                    }
                                }
                            }

                            return this.requestedServerNames;
                        }
                    }
                }

                public String[] getPeerSupportedSignatureAlgorithms()
                {
                    synchronized ( ReferenceCountedOpenSslEngine.this )
                    {
                        if ( this.peerSupportedSignatureAlgorithms == null )
                        {
                            if ( ReferenceCountedOpenSslEngine.this.isDestroyed() )
                            {
                                this.peerSupportedSignatureAlgorithms = EmptyArrays.EMPTY_STRINGS;
                            }
                            else
                            {
                                String[] algs = SSL.getSigAlgs( ReferenceCountedOpenSslEngine.this.ssl );
                                if ( algs == null )
                                {
                                    this.peerSupportedSignatureAlgorithms = EmptyArrays.EMPTY_STRINGS;
                                }
                                else
                                {
                                    Set<String> algorithmList = new LinkedHashSet( algs.length );
                                    String[] var4 = algs;
                                    int var5 = algs.length;

                                    for ( int var6 = 0; var6 < var5; ++var6 )
                                    {
                                        String alg = var4[var6];
                                        String converted = SignatureAlgorithmConverter.toJavaName( alg );
                                        if ( converted != null )
                                        {
                                            algorithmList.add( converted );
                                        }
                                    }

                                    this.peerSupportedSignatureAlgorithms = (String[]) algorithmList.toArray( new String[0] );
                                }
                            }
                        }

                        return (String[]) this.peerSupportedSignatureAlgorithms.clone();
                    }
                }

                public List<byte[]> getStatusResponses()
                {
                    byte[] ocspResponse = null;
                    if ( ReferenceCountedOpenSslEngine.this.enableOcsp && ReferenceCountedOpenSslEngine.this.clientMode )
                    {
                        synchronized ( ReferenceCountedOpenSslEngine.this )
                        {
                            if ( !ReferenceCountedOpenSslEngine.this.isDestroyed() )
                            {
                                ocspResponse = SSL.getOcspResponse( ReferenceCountedOpenSslEngine.this.ssl );
                            }
                        }
                    }

                    return ocspResponse == null ? Collections.emptyList() : Collections.singletonList( ocspResponse );
                }
            };
        }
        else
        {
            this.session = new ReferenceCountedOpenSslEngine.DefaultOpenSslSession( context.sessionContext() );
        }

        this.engineMap = context.engineMap;
        this.enableOcsp = context.enableOcsp;
        this.localCertificateChain = context.keyCertChain;
        this.jdkCompatibilityMode = jdkCompatibilityMode;
        Lock readerLock = context.ctxLock.readLock();
        readerLock.lock();

        long finalSsl;
        try
        {
            finalSsl = SSL.newSSL( context.ctx, !context.isClient() );
        }
        finally
        {
            readerLock.unlock();
        }

        synchronized ( this )
        {
            this.ssl = finalSsl;

            try
            {
                this.networkBIO = SSL.bioNewByteBuffer( this.ssl, context.getBioNonApplicationBufferSize() );
                this.setClientAuth( this.clientMode ? ClientAuth.NONE : context.clientAuth );
                if ( context.protocols != null )
                {
                    this.setEnabledProtocols( context.protocols );
                }

                if ( this.clientMode && SslUtils.isValidHostNameForSNI( peerHost ) )
                {
                    SSL.setTlsExtHostName( this.ssl, peerHost );
                    this.sniHostNames = Collections.singletonList( peerHost );
                }

                if ( this.enableOcsp )
                {
                    SSL.enableOcsp( this.ssl );
                }

                if ( !jdkCompatibilityMode )
                {
                    SSL.setMode( this.ssl, SSL.getMode( this.ssl ) | SSL.SSL_MODE_ENABLE_PARTIAL_WRITE );
                }

                this.calculateMaxWrapOverhead();
            }
            catch ( Throwable var16 )
            {
                this.shutdown();
                PlatformDependent.throwException( var16 );
            }
        }

        this.leak = leakDetection ? leakDetector.track( this ) : null;
    }

    private static boolean isProtocolEnabled( int opts, int disableMask, String protocolString )
    {
        return (opts & disableMask) == 0 && OpenSsl.SUPPORTED_PROTOCOLS_SET.contains( protocolString );
    }

    private static HandshakeStatus pendingStatus( int pendingStatus )
    {
        return pendingStatus > 0 ? HandshakeStatus.NEED_WRAP : HandshakeStatus.NEED_UNWRAP;
    }

    private static boolean isEmpty( Object[] arr )
    {
        return arr == null || arr.length == 0;
    }

    private static boolean isEmpty( byte[] cert )
    {
        return cert == null || cert.length == 0;
    }

    private static String toJavaCipherSuitePrefix( String protocolVersion )
    {
        char c;
        if ( protocolVersion != null && !protocolVersion.isEmpty() )
        {
            c = protocolVersion.charAt( 0 );
        }
        else
        {
            c = 0;
        }

        switch ( c )
        {
        case 'S':
            return "SSL";
        case 'T':
            return "TLS";
        default:
            return "UNKNOWN";
        }
    }

    private static boolean isEndPointVerificationEnabled( String endPointIdentificationAlgorithm )
    {
        return endPointIdentificationAlgorithm != null && !endPointIdentificationAlgorithm.isEmpty();
    }

    private static long bufferAddress( ByteBuffer b )
    {
        assert b.isDirect();

        return PlatformDependent.hasUnsafe() ? PlatformDependent.directBufferAddress( b ) : Buffer.address( b );
    }

    final void setKeyMaterial( OpenSslKeyMaterial keyMaterial ) throws Exception
    {
        SSL.setKeyMaterial( this.ssl, keyMaterial.certificateChainAddress(), keyMaterial.privateKeyAddress() );
        this.localCertificateChain = keyMaterial.certificateChain();
    }

    public byte[] getOcspResponse()
    {
        if ( !this.enableOcsp )
        {
            throw new IllegalStateException( "OCSP stapling is not enabled" );
        }
        else if ( !this.clientMode )
        {
            throw new IllegalStateException( "Not a client SSLEngine" );
        }
        else
        {
            synchronized ( this )
            {
                return SSL.getOcspResponse( this.ssl );
            }
        }
    }

    public void setOcspResponse( byte[] response )
    {
        if ( !this.enableOcsp )
        {
            throw new IllegalStateException( "OCSP stapling is not enabled" );
        }
        else if ( this.clientMode )
        {
            throw new IllegalStateException( "Not a server SSLEngine" );
        }
        else
        {
            synchronized ( this )
            {
                SSL.setOcspResponse( this.ssl, response );
            }
        }
    }

    public final int refCnt()
    {
        return this.refCnt.refCnt();
    }

    public final ReferenceCounted retain()
    {
        this.refCnt.retain();
        return this;
    }

    public final ReferenceCounted retain( int increment )
    {
        this.refCnt.retain( increment );
        return this;
    }

    public final ReferenceCounted touch()
    {
        this.refCnt.touch();
        return this;
    }

    public final ReferenceCounted touch( Object hint )
    {
        this.refCnt.touch( hint );
        return this;
    }

    public final boolean release()
    {
        return this.refCnt.release();
    }

    public final boolean release( int decrement )
    {
        return this.refCnt.release( decrement );
    }

    public final synchronized SSLSession getHandshakeSession()
    {
        switch ( this.handshakeState )
        {
        case NOT_STARTED:
        case FINISHED:
            return null;
        default:
            return this.session;
        }
    }

    public final synchronized long sslPointer()
    {
        return this.ssl;
    }

    public final synchronized void shutdown()
    {
        if ( DESTROYED_UPDATER.compareAndSet( this, 0, 1 ) )
        {
            this.engineMap.remove( this.ssl );
            SSL.freeSSL( this.ssl );
            this.ssl = this.networkBIO = 0L;
            this.isInboundDone = this.outboundClosed = true;
        }

        SSL.clearError();
    }

    private int writePlaintextData( ByteBuffer src, int len )
    {
        int pos = src.position();
        int limit = src.limit();
        int sslWrote;
        if ( src.isDirect() )
        {
            sslWrote = SSL.writeToSSL( this.ssl, bufferAddress( src ) + (long) pos, len );
            if ( sslWrote > 0 )
            {
                src.position( pos + sslWrote );
            }
        }
        else
        {
            ByteBuf buf = this.alloc.directBuffer( len );

            try
            {
                src.limit( pos + len );
                buf.setBytes( 0, (ByteBuffer) src );
                src.limit( limit );
                sslWrote = SSL.writeToSSL( this.ssl, OpenSsl.memoryAddress( buf ), len );
                if ( sslWrote > 0 )
                {
                    src.position( pos + sslWrote );
                }
                else
                {
                    src.position( pos );
                }
            }
            finally
            {
                buf.release();
            }
        }

        return sslWrote;
    }

    private ByteBuf writeEncryptedData( ByteBuffer src, int len )
    {
        int pos = src.position();
        if ( src.isDirect() )
        {
            SSL.bioSetByteBuffer( this.networkBIO, bufferAddress( src ) + (long) pos, len, false );
        }
        else
        {
            ByteBuf buf = this.alloc.directBuffer( len );

            try
            {
                int limit = src.limit();
                src.limit( pos + len );
                buf.writeBytes( src );
                src.position( pos );
                src.limit( limit );
                SSL.bioSetByteBuffer( this.networkBIO, OpenSsl.memoryAddress( buf ), len, false );
                return buf;
            }
            catch ( Throwable var6 )
            {
                buf.release();
                PlatformDependent.throwException( var6 );
            }
        }

        return null;
    }

    private int readPlaintextData( ByteBuffer dst )
    {
        int pos = dst.position();
        int sslRead;
        if ( dst.isDirect() )
        {
            sslRead = SSL.readFromSSL( this.ssl, bufferAddress( dst ) + (long) pos, dst.limit() - pos );
            if ( sslRead > 0 )
            {
                dst.position( pos + sslRead );
            }
        }
        else
        {
            int limit = dst.limit();
            int len = Math.min( this.maxEncryptedPacketLength0(), limit - pos );
            ByteBuf buf = this.alloc.directBuffer( len );

            try
            {
                sslRead = SSL.readFromSSL( this.ssl, OpenSsl.memoryAddress( buf ), len );
                if ( sslRead > 0 )
                {
                    dst.limit( pos + sslRead );
                    buf.getBytes( buf.readerIndex(), dst );
                    dst.limit( limit );
                }
            }
            finally
            {
                buf.release();
            }
        }

        return sslRead;
    }

    final synchronized int maxWrapOverhead()
    {
        return this.maxWrapOverhead;
    }

    final synchronized int maxEncryptedPacketLength()
    {
        return this.maxEncryptedPacketLength0();
    }

    final int maxEncryptedPacketLength0()
    {
        return this.maxWrapOverhead + MAX_PLAINTEXT_LENGTH;
    }

    final int calculateMaxLengthForWrap( int plaintextLength, int numComponents )
    {
        return (int) Math.min( (long) this.maxWrapBufferSize, (long) plaintextLength + (long) this.maxWrapOverhead * (long) numComponents );
    }

    final synchronized int sslPending()
    {
        return this.sslPending0();
    }

    private void calculateMaxWrapOverhead()
    {
        this.maxWrapOverhead = SSL.getMaxWrapOverhead( this.ssl );
        this.maxWrapBufferSize = this.jdkCompatibilityMode ? this.maxEncryptedPacketLength0() : this.maxEncryptedPacketLength0() << 4;
    }

    private int sslPending0()
    {
        return this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.FINISHED ? 0 : SSL.sslPending( this.ssl );
    }

    private boolean isBytesAvailableEnoughForWrap( int bytesAvailable, int plaintextLength, int numComponents )
    {
        return (long) bytesAvailable - (long) this.maxWrapOverhead * (long) numComponents >= (long) plaintextLength;
    }

    public final SSLEngineResult wrap( ByteBuffer[] srcs, int offset, int length, ByteBuffer dst ) throws SSLException
    {
        if ( srcs == null )
        {
            throw new IllegalArgumentException( "srcs is null" );
        }
        else if ( dst == null )
        {
            throw new IllegalArgumentException( "dst is null" );
        }
        else if ( offset < srcs.length && offset + length <= srcs.length )
        {
            if ( dst.isReadOnly() )
            {
                throw new ReadOnlyBufferException();
            }
            else
            {
                synchronized ( this )
                {
                    if ( this.isOutboundDone() )
                    {
                        return !this.isInboundDone() && !this.isDestroyed() ? NEED_UNWRAP_CLOSED : CLOSED_NOT_HANDSHAKING;
                    }
                    else
                    {
                        int bytesProduced = 0;
                        ByteBuf bioReadCopyBuf = null;

                        try
                        {
                            if ( dst.isDirect() )
                            {
                                SSL.bioSetByteBuffer( this.networkBIO, bufferAddress( dst ) + (long) dst.position(), dst.remaining(), true );
                            }
                            else
                            {
                                bioReadCopyBuf = this.alloc.directBuffer( dst.remaining() );
                                SSL.bioSetByteBuffer( this.networkBIO, OpenSsl.memoryAddress( bioReadCopyBuf ), bioReadCopyBuf.writableBytes(), true );
                            }

                            int bioLengthBefore = SSL.bioLengthByteBuffer( this.networkBIO );
                            if ( this.outboundClosed )
                            {
                                SSLEngineResult var25;
                                if ( !this.isBytesAvailableEnoughForWrap( dst.remaining(), 2, 1 ) )
                                {
                                    var25 = new SSLEngineResult( Status.BUFFER_OVERFLOW, this.getHandshakeStatus(), 0, 0 );
                                    return var25;
                                }
                                else
                                {
                                    bytesProduced = SSL.bioFlushByteBuffer( this.networkBIO );
                                    if ( bytesProduced <= 0 )
                                    {
                                        var25 = this.newResultMayFinishHandshake( HandshakeStatus.NOT_HANDSHAKING, 0, 0 );
                                        return var25;
                                    }
                                    else if ( !this.doSSLShutdown() )
                                    {
                                        var25 = this.newResultMayFinishHandshake( HandshakeStatus.NOT_HANDSHAKING, 0, bytesProduced );
                                        return var25;
                                    }
                                    else
                                    {
                                        bytesProduced = bioLengthBefore - SSL.bioLengthByteBuffer( this.networkBIO );
                                        var25 = this.newResultMayFinishHandshake( HandshakeStatus.NEED_WRAP, 0, bytesProduced );
                                        return var25;
                                    }
                                }
                            }
                            else
                            {
                                HandshakeStatus status = HandshakeStatus.NOT_HANDSHAKING;
                                if ( this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.FINISHED )
                                {
                                    if ( this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.STARTED_EXPLICITLY )
                                    {
                                        this.handshakeState = ReferenceCountedOpenSslEngine.HandshakeState.STARTED_IMPLICITLY;
                                    }

                                    bytesProduced = SSL.bioFlushByteBuffer( this.networkBIO );
                                    SSLEngineResult var24;
                                    if ( this.handshakeException != null )
                                    {
                                        var24 = this.newResult( HandshakeStatus.NEED_UNWRAP, 0, bytesProduced );
                                        return var24;
                                    }

                                    status = this.handshake();
                                    bytesProduced = bioLengthBefore - SSL.bioLengthByteBuffer( this.networkBIO );
                                    if ( status == HandshakeStatus.NEED_TASK )
                                    {
                                        var24 = this.newResult( status, 0, bytesProduced );
                                        return var24;
                                    }

                                    if ( bytesProduced > 0 )
                                    {
                                        var24 = this.newResult( this.mayFinishHandshake(
                                                status != HandshakeStatus.FINISHED ? (bytesProduced == bioLengthBefore ? HandshakeStatus.NEED_WRAP
                                                                                                                       : this.getHandshakeStatus(
                                                                                                                               SSL.bioLengthNonApplication(
                                                                                                                                       this.networkBIO ) ))
                                                                                   : HandshakeStatus.FINISHED ), 0, bytesProduced );
                                        return var24;
                                    }

                                    if ( status == HandshakeStatus.NEED_UNWRAP )
                                    {
                                        var24 = this.isOutboundDone() ? NEED_UNWRAP_CLOSED : NEED_UNWRAP_OK;
                                        return var24;
                                    }

                                    if ( this.outboundClosed )
                                    {
                                        bytesProduced = SSL.bioFlushByteBuffer( this.networkBIO );
                                        var24 = this.newResultMayFinishHandshake( status, 0, bytesProduced );
                                        return var24;
                                    }
                                }

                                int endOffset = offset + length;
                                int bytesConsumed;
                                SSLEngineResult var27;
                                if ( this.jdkCompatibilityMode )
                                {
                                    bytesConsumed = 0;

                                    for ( int i = offset; i < endOffset; ++i )
                                    {
                                        ByteBuffer src = srcs[i];
                                        if ( src == null )
                                        {
                                            throw new IllegalArgumentException( "srcs[" + i + "] is null" );
                                        }

                                        if ( bytesConsumed != MAX_PLAINTEXT_LENGTH )
                                        {
                                            bytesConsumed += src.remaining();
                                            if ( bytesConsumed > MAX_PLAINTEXT_LENGTH || bytesConsumed < 0 )
                                            {
                                                bytesConsumed = MAX_PLAINTEXT_LENGTH;
                                            }
                                        }
                                    }

                                    if ( !this.isBytesAvailableEnoughForWrap( dst.remaining(), bytesConsumed, 1 ) )
                                    {
                                        var27 = new SSLEngineResult( Status.BUFFER_OVERFLOW, this.getHandshakeStatus(), 0, 0 );
                                        return var27;
                                    }
                                }

                                bytesConsumed = 0;

                                for ( bytesProduced = SSL.bioFlushByteBuffer( this.networkBIO ); offset < endOffset; ++offset )
                                {
                                    ByteBuffer src = srcs[offset];
                                    int remaining = src.remaining();
                                    if ( remaining != 0 )
                                    {
                                        int bytesWritten;
                                        int sslError;
                                        SSLEngineResult var16;
                                        if ( this.jdkCompatibilityMode )
                                        {
                                            bytesWritten = this.writePlaintextData( src, Math.min( remaining, MAX_PLAINTEXT_LENGTH - bytesConsumed ) );
                                        }
                                        else
                                        {
                                            sslError = dst.remaining() - bytesProduced - this.maxWrapOverhead;
                                            if ( sslError <= 0 )
                                            {
                                                var16 = new SSLEngineResult( Status.BUFFER_OVERFLOW, this.getHandshakeStatus(), bytesConsumed, bytesProduced );
                                                return var16;
                                            }

                                            bytesWritten = this.writePlaintextData( src, Math.min( remaining, sslError ) );
                                        }

                                        if ( bytesWritten <= 0 )
                                        {
                                            sslError = SSL.getError( this.ssl, bytesWritten );
                                            if ( sslError == SSL.SSL_ERROR_ZERO_RETURN )
                                            {
                                                if ( !this.receivedShutdown )
                                                {
                                                    this.closeAll();
                                                    bytesProduced += bioLengthBefore - SSL.bioLengthByteBuffer( this.networkBIO );
                                                    HandshakeStatus hs = this.mayFinishHandshake(
                                                            status != HandshakeStatus.FINISHED ? (bytesProduced == dst.remaining() ? HandshakeStatus.NEED_WRAP
                                                                                                                                   : this.getHandshakeStatus(
                                                                                                                                           SSL.bioLengthNonApplication(
                                                                                                                                                   this.networkBIO ) ))
                                                                                               : HandshakeStatus.FINISHED );
                                                    SSLEngineResult var17 = this.newResult( hs, bytesConsumed, bytesProduced );
                                                    return var17;
                                                }

                                                var16 = this.newResult( HandshakeStatus.NOT_HANDSHAKING, bytesConsumed, bytesProduced );
                                                return var16;
                                            }

                                            if ( sslError == SSL.SSL_ERROR_WANT_READ )
                                            {
                                                var16 = this.newResult( HandshakeStatus.NEED_UNWRAP, bytesConsumed, bytesProduced );
                                                return var16;
                                            }

                                            if ( sslError == SSL.SSL_ERROR_WANT_WRITE )
                                            {
                                                var16 = this.newResult( Status.BUFFER_OVERFLOW, status, bytesConsumed, bytesProduced );
                                                return var16;
                                            }

                                            if ( sslError != SSL.SSL_ERROR_WANT_X509_LOOKUP && sslError != SSL.SSL_ERROR_WANT_CERTIFICATE_VERIFY &&
                                                    sslError != SSL.SSL_ERROR_WANT_PRIVATE_KEY_OPERATION )
                                            {
                                                throw this.shutdownWithError( "SSL_write", sslError );
                                            }

                                            var16 = this.newResult( HandshakeStatus.NEED_TASK, bytesConsumed, bytesProduced );
                                            return var16;
                                        }

                                        bytesConsumed += bytesWritten;
                                        sslError = SSL.bioLengthByteBuffer( this.networkBIO );
                                        bytesProduced += bioLengthBefore - sslError;
                                        bioLengthBefore = sslError;
                                        if ( this.jdkCompatibilityMode || bytesProduced == dst.remaining() )
                                        {
                                            var16 = this.newResultMayFinishHandshake( status, bytesConsumed, bytesProduced );
                                            return var16;
                                        }
                                    }
                                }

                                var27 = this.newResultMayFinishHandshake( status, bytesConsumed, bytesProduced );
                                return var27;
                            }
                        }
                        finally
                        {
                            SSL.bioClearByteBuffer( this.networkBIO );
                            if ( bioReadCopyBuf == null )
                            {
                                dst.position( dst.position() + bytesProduced );
                            }
                            else
                            {
                                assert bioReadCopyBuf.readableBytes() <= dst.remaining() :
                                        "The destination buffer " + dst + " didn't have enough remaining space to hold the encrypted content in " +
                                                bioReadCopyBuf;

                                dst.put( bioReadCopyBuf.internalNioBuffer( bioReadCopyBuf.readerIndex(), bytesProduced ) );
                                bioReadCopyBuf.release();
                            }
                        }
                    }
                }
            }
        }
        else
        {
            throw new IndexOutOfBoundsException(
                    "offset: " + offset + ", length: " + length + " (expected: offset <= offset + length <= srcs.length (" + srcs.length + "))" );
        }
    }

    private SSLEngineResult newResult( HandshakeStatus hs, int bytesConsumed, int bytesProduced )
    {
        return this.newResult( Status.OK, hs, bytesConsumed, bytesProduced );
    }

    private SSLEngineResult newResult( Status status, HandshakeStatus hs, int bytesConsumed, int bytesProduced )
    {
        if ( this.isOutboundDone() )
        {
            if ( this.isInboundDone() )
            {
                hs = HandshakeStatus.NOT_HANDSHAKING;
                this.shutdown();
            }

            return new SSLEngineResult( Status.CLOSED, hs, bytesConsumed, bytesProduced );
        }
        else
        {
            if ( hs == HandshakeStatus.NEED_TASK )
            {
                this.needTask = true;
            }

            return new SSLEngineResult( status, hs, bytesConsumed, bytesProduced );
        }
    }

    private SSLEngineResult newResultMayFinishHandshake( HandshakeStatus hs, int bytesConsumed, int bytesProduced ) throws SSLException
    {
        return this.newResult( this.mayFinishHandshake( hs != HandshakeStatus.FINISHED ? this.getHandshakeStatus() : HandshakeStatus.FINISHED ), bytesConsumed,
                bytesProduced );
    }

    private SSLEngineResult newResultMayFinishHandshake( Status status, HandshakeStatus hs, int bytesConsumed, int bytesProduced ) throws SSLException
    {
        return this.newResult( status, this.mayFinishHandshake( hs != HandshakeStatus.FINISHED ? this.getHandshakeStatus() : HandshakeStatus.FINISHED ),
                bytesConsumed, bytesProduced );
    }

    private SSLException shutdownWithError( String operations, int sslError )
    {
        return this.shutdownWithError( operations, sslError, SSL.getLastErrorNumber() );
    }

    private SSLException shutdownWithError( String operation, int sslError, int error )
    {
        String errorString = SSL.getErrorString( (long) error );
        if ( logger.isDebugEnabled() )
        {
            logger.debug( "{} failed with {}: OpenSSL error: {} {}", operation, sslError, error, errorString );
        }

        this.shutdown();
        if ( this.handshakeState == ReferenceCountedOpenSslEngine.HandshakeState.FINISHED )
        {
            return new SSLException( errorString );
        }
        else
        {
            SSLHandshakeException exception = new SSLHandshakeException( errorString );
            if ( this.handshakeException != null )
            {
                exception.initCause( this.handshakeException );
                this.handshakeException = null;
            }

            return exception;
        }
    }

    public final SSLEngineResult unwrap( ByteBuffer[] srcs, int srcsOffset, int srcsLength, ByteBuffer[] dsts, int dstsOffset, int dstsLength )
            throws SSLException
    {
        if ( srcs == null )
        {
            throw new NullPointerException( "srcs" );
        }
        else if ( srcsOffset < srcs.length && srcsOffset + srcsLength <= srcs.length )
        {
            if ( dsts == null )
            {
                throw new IllegalArgumentException( "dsts is null" );
            }
            else if ( dstsOffset < dsts.length && dstsOffset + dstsLength <= dsts.length )
            {
                long capacity = 0L;
                int dstsEndOffset = dstsOffset + dstsLength;

                int srcsEndOffset;
                for ( srcsEndOffset = dstsOffset; srcsEndOffset < dstsEndOffset; ++srcsEndOffset )
                {
                    ByteBuffer dst = dsts[srcsEndOffset];
                    if ( dst == null )
                    {
                        throw new IllegalArgumentException( "dsts[" + srcsEndOffset + "] is null" );
                    }

                    if ( dst.isReadOnly() )
                    {
                        throw new ReadOnlyBufferException();
                    }

                    capacity += (long) dst.remaining();
                }

                srcsEndOffset = srcsOffset + srcsLength;
                long len = 0L;

                for ( int i = srcsOffset; i < srcsEndOffset; ++i )
                {
                    ByteBuffer src = srcs[i];
                    if ( src == null )
                    {
                        throw new IllegalArgumentException( "srcs[" + i + "] is null" );
                    }

                    len += (long) src.remaining();
                }

                synchronized ( this )
                {
                    if ( this.isInboundDone() )
                    {
                        return !this.isOutboundDone() && !this.isDestroyed() ? NEED_WRAP_CLOSED : CLOSED_NOT_HANDSHAKING;
                    }
                    else
                    {
                        HandshakeStatus status = HandshakeStatus.NOT_HANDSHAKING;
                        if ( this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.FINISHED )
                        {
                            if ( this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.STARTED_EXPLICITLY )
                            {
                                this.handshakeState = ReferenceCountedOpenSslEngine.HandshakeState.STARTED_IMPLICITLY;
                            }

                            status = this.handshake();
                            if ( status == HandshakeStatus.NEED_TASK )
                            {
                                return this.newResult( status, 0, 0 );
                            }

                            if ( status == HandshakeStatus.NEED_WRAP )
                            {
                                return NEED_WRAP_OK;
                            }

                            if ( this.isInboundDone )
                            {
                                return NEED_WRAP_CLOSED;
                            }
                        }

                        int sslPending = this.sslPending0();
                        int packetLength;
                        int bytesProduced;
                        if ( this.jdkCompatibilityMode )
                        {
                            if ( len < 5L )
                            {
                                return this.newResultMayFinishHandshake( Status.BUFFER_UNDERFLOW, status, 0, 0 );
                            }

                            packetLength = SslUtils.getEncryptedPacketLength( srcs, srcsOffset );
                            if ( packetLength == -2 )
                            {
                                throw new NotSslRecordException( "not an SSL/TLS record" );
                            }

                            bytesProduced = packetLength - 5;
                            if ( (long) bytesProduced > capacity )
                            {
                                if ( bytesProduced > MAX_RECORD_SIZE )
                                {
                                    throw new SSLException( "Illegal packet length: " + bytesProduced + " > " + this.session.getApplicationBufferSize() );
                                }

                                this.session.tryExpandApplicationBufferSize( bytesProduced );
                                return this.newResultMayFinishHandshake( Status.BUFFER_OVERFLOW, status, 0, 0 );
                            }

                            if ( len < (long) packetLength )
                            {
                                return this.newResultMayFinishHandshake( Status.BUFFER_UNDERFLOW, status, 0, 0 );
                            }
                        }
                        else
                        {
                            if ( len == 0L && sslPending <= 0 )
                            {
                                return this.newResultMayFinishHandshake( Status.BUFFER_UNDERFLOW, status, 0, 0 );
                            }

                            if ( capacity == 0L )
                            {
                                return this.newResultMayFinishHandshake( Status.BUFFER_OVERFLOW, status, 0, 0 );
                            }

                            packetLength = (int) Math.min( 2147483647L, len );
                        }

                        assert srcsOffset < srcsEndOffset;

                        assert capacity > 0L;

                        bytesProduced = 0;
                        int bytesConsumed = 0;

                        try
                        {
                            label906:
                            while ( true )
                            {
                                ByteBuffer src = srcs[srcsOffset];
                                int remaining = src.remaining();
                                ByteBuf bioWriteCopyBuf;
                                int pendingEncryptedBytes;
                                if ( remaining == 0 )
                                {
                                    if ( sslPending <= 0 )
                                    {
                                        ++srcsOffset;
                                        if ( srcsOffset < srcsEndOffset )
                                        {
                                            continue;
                                        }
                                        break;
                                    }

                                    bioWriteCopyBuf = null;
                                    pendingEncryptedBytes = SSL.bioLengthByteBuffer( this.networkBIO );
                                }
                                else
                                {
                                    pendingEncryptedBytes = Math.min( packetLength, remaining );
                                    bioWriteCopyBuf = this.writeEncryptedData( src, pendingEncryptedBytes );
                                }

                                try
                                {
                                    while ( true )
                                    {
                                        while ( true )
                                        {
                                            ByteBuffer dst = dsts[dstsOffset];
                                            if ( !dst.hasRemaining() )
                                            {
                                                ++dstsOffset;
                                                if ( dstsOffset >= dstsEndOffset )
                                                {
                                                    break label906;
                                                }
                                            }
                                            else
                                            {
                                                int bytesRead = this.readPlaintextData( dst );
                                                int localBytesConsumed = pendingEncryptedBytes - SSL.bioLengthByteBuffer( this.networkBIO );
                                                bytesConsumed += localBytesConsumed;
                                                packetLength -= localBytesConsumed;
                                                pendingEncryptedBytes -= localBytesConsumed;
                                                src.position( src.position() + localBytesConsumed );
                                                SSLEngineResult sslError;
                                                if ( bytesRead <= 0 )
                                                {
                                                    sslError = SSL.getError( this.ssl, bytesRead );
                                                    if ( sslError != SSL.SSL_ERROR_WANT_READ && sslError != SSL.SSL_ERROR_WANT_WRITE )
                                                    {
                                                        SSLEngineResult var27;
                                                        if ( sslError == SSL.SSL_ERROR_ZERO_RETURN )
                                                        {
                                                            if ( !this.receivedShutdown )
                                                            {
                                                                this.closeAll();
                                                            }

                                                            var27 = this.newResultMayFinishHandshake( this.isInboundDone() ? Status.CLOSED : Status.OK, status,
                                                                    bytesConsumed, bytesProduced );
                                                            return var27;
                                                        }

                                                        if ( sslError != SSL.SSL_ERROR_WANT_X509_LOOKUP && sslError != SSL.SSL_ERROR_WANT_CERTIFICATE_VERIFY &&
                                                                sslError != SSL.SSL_ERROR_WANT_PRIVATE_KEY_OPERATION )
                                                        {
                                                            var27 = this.sslReadErrorResult( (int) sslError, SSL.getLastErrorNumber(), bytesConsumed,
                                                                    bytesProduced );
                                                            return var27;
                                                        }

                                                        var27 = this.newResult( this.isInboundDone() ? Status.CLOSED : Status.OK, HandshakeStatus.NEED_TASK,
                                                                bytesConsumed, bytesProduced );
                                                        return var27;
                                                    }

                                                    ++srcsOffset;
                                                    if ( srcsOffset < srcsEndOffset )
                                                    {
                                                        continue label906;
                                                    }
                                                    break label906;
                                                }

                                                bytesProduced += bytesRead;
                                                if ( !dst.hasRemaining() )
                                                {
                                                    sslPending = this.sslPending0();
                                                    ++dstsOffset;
                                                    if ( dstsOffset >= dstsEndOffset )
                                                    {
                                                        sslError =
                                                                sslPending > 0 ? this.newResult( Status.BUFFER_OVERFLOW, status, bytesConsumed, bytesProduced )
                                                                               : this.newResultMayFinishHandshake(
                                                                                       this.isInboundDone() ? Status.CLOSED : Status.OK, status, bytesConsumed,
                                                                                       bytesProduced );
                                                        return sslError;
                                                    }
                                                }
                                                else if ( packetLength == 0 || this.jdkCompatibilityMode )
                                                {
                                                    break label906;
                                                }
                                            }
                                        }
                                    }
                                }
                                finally
                                {
                                    if ( bioWriteCopyBuf != null )
                                    {
                                        bioWriteCopyBuf.release();
                                    }
                                }
                            }
                        }
                        finally
                        {
                            SSL.bioClearByteBuffer( this.networkBIO );
                            this.rejectRemoteInitiatedRenegotiation();
                        }

                        if ( !this.receivedShutdown && (SSL.getShutdown( this.ssl ) & SSL.SSL_RECEIVED_SHUTDOWN) == SSL.SSL_RECEIVED_SHUTDOWN )
                        {
                            this.closeAll();
                        }

                        return this.newResultMayFinishHandshake( this.isInboundDone() ? Status.CLOSED : Status.OK, status, bytesConsumed, bytesProduced );
                    }
                }
            }
            else
            {
                throw new IndexOutOfBoundsException(
                        "offset: " + dstsOffset + ", length: " + dstsLength + " (expected: offset <= offset + length <= dsts.length (" + dsts.length + "))" );
            }
        }
        else
        {
            throw new IndexOutOfBoundsException(
                    "offset: " + srcsOffset + ", length: " + srcsLength + " (expected: offset <= offset + length <= srcs.length (" + srcs.length + "))" );
        }
    }

    private SSLEngineResult sslReadErrorResult( int error, int stackError, int bytesConsumed, int bytesProduced ) throws SSLException
    {
        if ( SSL.bioLengthNonApplication( this.networkBIO ) > 0 )
        {
            if ( this.handshakeException == null && this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.FINISHED )
            {
                this.handshakeException = new SSLHandshakeException( SSL.getErrorString( (long) stackError ) );
            }

            SSL.clearError();
            return new SSLEngineResult( Status.OK, HandshakeStatus.NEED_WRAP, bytesConsumed, bytesProduced );
        }
        else
        {
            throw this.shutdownWithError( "SSL_read", error, stackError );
        }
    }

    private void closeAll() throws SSLException
    {
        this.receivedShutdown = true;
        this.closeOutbound();
        this.closeInbound();
    }

    private void rejectRemoteInitiatedRenegotiation() throws SSLHandshakeException
    {
        if ( !this.isDestroyed() && SSL.getHandshakeCount( this.ssl ) > 1 && !"TLSv1.3".equals( this.session.getProtocol() ) &&
                this.handshakeState == ReferenceCountedOpenSslEngine.HandshakeState.FINISHED )
        {
            this.shutdown();
            throw new SSLHandshakeException( "remote-initiated renegotiation not allowed" );
        }
    }

    public final SSLEngineResult unwrap( ByteBuffer[] srcs, ByteBuffer[] dsts ) throws SSLException
    {
        return this.unwrap( srcs, 0, srcs.length, dsts, 0, dsts.length );
    }

    private ByteBuffer[] singleSrcBuffer( ByteBuffer src )
    {
        this.singleSrcBuffer[0] = src;
        return this.singleSrcBuffer;
    }

    private void resetSingleSrcBuffer()
    {
        this.singleSrcBuffer[0] = null;
    }

    private ByteBuffer[] singleDstBuffer( ByteBuffer src )
    {
        this.singleDstBuffer[0] = src;
        return this.singleDstBuffer;
    }

    private void resetSingleDstBuffer()
    {
        this.singleDstBuffer[0] = null;
    }

    public final synchronized SSLEngineResult unwrap( ByteBuffer src, ByteBuffer[] dsts, int offset, int length ) throws SSLException
    {
        SSLEngineResult var5;
        try
        {
            var5 = this.unwrap( this.singleSrcBuffer( src ), 0, 1, dsts, offset, length );
        }
        finally
        {
            this.resetSingleSrcBuffer();
        }

        return var5;
    }

    public final synchronized SSLEngineResult wrap( ByteBuffer src, ByteBuffer dst ) throws SSLException
    {
        SSLEngineResult var3;
        try
        {
            var3 = this.wrap( this.singleSrcBuffer( src ), dst );
        }
        finally
        {
            this.resetSingleSrcBuffer();
        }

        return var3;
    }

    public final synchronized SSLEngineResult unwrap( ByteBuffer src, ByteBuffer dst ) throws SSLException
    {
        SSLEngineResult var3;
        try
        {
            var3 = this.unwrap( this.singleSrcBuffer( src ), this.singleDstBuffer( dst ) );
        }
        finally
        {
            this.resetSingleSrcBuffer();
            this.resetSingleDstBuffer();
        }

        return var3;
    }

    public final synchronized SSLEngineResult unwrap( ByteBuffer src, ByteBuffer[] dsts ) throws SSLException
    {
        SSLEngineResult var3;
        try
        {
            var3 = this.unwrap( this.singleSrcBuffer( src ), dsts );
        }
        finally
        {
            this.resetSingleSrcBuffer();
        }

        return var3;
    }

    public final synchronized Runnable getDelegatedTask()
    {
        if ( this.isDestroyed() )
        {
            return null;
        }
        else
        {
            final Runnable task = SSL.getTask( this.ssl );
            return task == null ? null : new Runnable()
            {
                public void run()
                {
                    if ( !ReferenceCountedOpenSslEngine.this.isDestroyed() )
                    {
                        try
                        {
                            task.run();
                        }
                        finally
                        {
                            ReferenceCountedOpenSslEngine.this.needTask = false;
                        }
                    }
                }
            };
        }
    }

    public final synchronized void closeInbound() throws SSLException
    {
        if ( !this.isInboundDone )
        {
            this.isInboundDone = true;
            if ( this.isOutboundDone() )
            {
                this.shutdown();
            }

            if ( this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.NOT_STARTED && !this.receivedShutdown )
            {
                throw new SSLException( "Inbound closed before receiving peer's close_notify: possible truncation attack?" );
            }
        }
    }

    public final synchronized boolean isInboundDone()
    {
        return this.isInboundDone;
    }

    public final synchronized void closeOutbound()
    {
        if ( !this.outboundClosed )
        {
            this.outboundClosed = true;
            if ( this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.NOT_STARTED && !this.isDestroyed() )
            {
                int mode = SSL.getShutdown( this.ssl );
                if ( (mode & SSL.SSL_SENT_SHUTDOWN) != SSL.SSL_SENT_SHUTDOWN )
                {
                    this.doSSLShutdown();
                }
            }
            else
            {
                this.shutdown();
            }
        }
    }

    private boolean doSSLShutdown()
    {
        if ( SSL.isInInit( this.ssl ) != 0 )
        {
            return false;
        }
        else
        {
            int err = SSL.shutdownSSL( this.ssl );
            if ( err < 0 )
            {
                int sslErr = SSL.getError( this.ssl, err );
                if ( sslErr == SSL.SSL_ERROR_SYSCALL || sslErr == SSL.SSL_ERROR_SSL )
                {
                    if ( logger.isDebugEnabled() )
                    {
                        int error = SSL.getLastErrorNumber();
                        logger.debug( "SSL_shutdown failed: OpenSSL error: {} {}", error, SSL.getErrorString( (long) error ) );
                    }

                    this.shutdown();
                    return false;
                }

                SSL.clearError();
            }

            return true;
        }
    }

    public final synchronized boolean isOutboundDone()
    {
        return this.outboundClosed && (this.networkBIO == 0L || SSL.bioLengthNonApplication( this.networkBIO ) == 0);
    }

    public final String[] getSupportedCipherSuites()
    {
        return (String[]) OpenSsl.AVAILABLE_CIPHER_SUITES.toArray( new String[0] );
    }

    public final String[] getEnabledCipherSuites()
    {
        String[] enabled;
        synchronized ( this )
        {
            if ( this.isDestroyed() )
            {
                return EmptyArrays.EMPTY_STRINGS;
            }

            enabled = SSL.getCiphers( this.ssl );
        }

        if ( enabled == null )
        {
            return EmptyArrays.EMPTY_STRINGS;
        }
        else
        {
            List<String> enabledList = new ArrayList();
            synchronized ( this )
            {
                for ( int i = 0; i < enabled.length; ++i )
                {
                    String mapped = this.toJavaCipherSuite( enabled[i] );
                    String cipher = mapped == null ? enabled[i] : mapped;
                    if ( OpenSsl.isTlsv13Supported() || !SslUtils.isTLSv13Cipher( cipher ) )
                    {
                        enabledList.add( cipher );
                    }
                }

                return (String[]) enabledList.toArray( new String[0] );
            }
        }
    }

    public final void setEnabledCipherSuites( String[] cipherSuites )
    {
        ObjectUtil.checkNotNull( cipherSuites, "cipherSuites" );
        StringBuilder buf = new StringBuilder();
        StringBuilder bufTLSv13 = new StringBuilder();
        CipherSuiteConverter.convertToCipherStrings( Arrays.asList( cipherSuites ), buf, bufTLSv13, OpenSsl.isBoringSSL() );
        String cipherSuiteSpec = buf.toString();
        String cipherSuiteSpecTLSv13 = bufTLSv13.toString();
        if ( !OpenSsl.isTlsv13Supported() && !cipherSuiteSpecTLSv13.isEmpty() )
        {
            throw new IllegalArgumentException( "TLSv1.3 is not supported by this java version." );
        }
        else
        {
            synchronized ( this )
            {
                if ( this.isDestroyed() )
                {
                    throw new IllegalStateException( "failed to enable cipher suites: " + cipherSuiteSpec );
                }
                else
                {
                    try
                    {
                        SSL.setCipherSuites( this.ssl, cipherSuiteSpec, false );
                        if ( OpenSsl.isTlsv13Supported() )
                        {
                            SSL.setCipherSuites( this.ssl, cipherSuiteSpecTLSv13, true );
                        }
                    }
                    catch ( Exception var9 )
                    {
                        throw new IllegalStateException( "failed to enable cipher suites: " + cipherSuiteSpec, var9 );
                    }
                }
            }
        }
    }

    public final String[] getSupportedProtocols()
    {
        return (String[]) OpenSsl.SUPPORTED_PROTOCOLS_SET.toArray( new String[0] );
    }

    public final String[] getEnabledProtocols()
    {
        List<String> enabled = new ArrayList( 6 );
        enabled.add( "SSLv2Hello" );
        int opts;
        synchronized ( this )
        {
            if ( this.isDestroyed() )
            {
                return (String[]) enabled.toArray( new String[0] );
            }

            opts = SSL.getOptions( this.ssl );
        }

        if ( isProtocolEnabled( opts, SSL.SSL_OP_NO_TLSv1, "TLSv1" ) )
        {
            enabled.add( "TLSv1" );
        }

        if ( isProtocolEnabled( opts, SSL.SSL_OP_NO_TLSv1_1, "TLSv1.1" ) )
        {
            enabled.add( "TLSv1.1" );
        }

        if ( isProtocolEnabled( opts, SSL.SSL_OP_NO_TLSv1_2, "TLSv1.2" ) )
        {
            enabled.add( "TLSv1.2" );
        }

        if ( isProtocolEnabled( opts, SSL.SSL_OP_NO_TLSv1_3, "TLSv1.3" ) )
        {
            enabled.add( "TLSv1.3" );
        }

        if ( isProtocolEnabled( opts, SSL.SSL_OP_NO_SSLv2, "SSLv2" ) )
        {
            enabled.add( "SSLv2" );
        }

        if ( isProtocolEnabled( opts, SSL.SSL_OP_NO_SSLv3, "SSLv3" ) )
        {
            enabled.add( "SSLv3" );
        }

        return (String[]) enabled.toArray( new String[0] );
    }

    public final void setEnabledProtocols( String[] protocols )
    {
        if ( protocols == null )
        {
            throw new IllegalArgumentException();
        }
        else
        {
            int minProtocolIndex = OPENSSL_OP_NO_PROTOCOLS.length;
            int maxProtocolIndex = 0;
            String[] var4 = protocols;
            int opts = protocols.length;

            int i;
            for ( i = 0; i < opts; ++i )
            {
                String p = var4[i];
                if ( !OpenSsl.SUPPORTED_PROTOCOLS_SET.contains( p ) )
                {
                    throw new IllegalArgumentException( "Protocol " + p + " is not supported." );
                }

                if ( p.equals( "SSLv2" ) )
                {
                    if ( minProtocolIndex > 0 )
                    {
                        minProtocolIndex = 0;
                    }

                    if ( maxProtocolIndex < 0 )
                    {
                        maxProtocolIndex = 0;
                    }
                }
                else if ( p.equals( "SSLv3" ) )
                {
                    if ( minProtocolIndex > 1 )
                    {
                        minProtocolIndex = 1;
                    }

                    if ( maxProtocolIndex < 1 )
                    {
                        maxProtocolIndex = 1;
                    }
                }
                else if ( p.equals( "TLSv1" ) )
                {
                    if ( minProtocolIndex > 2 )
                    {
                        minProtocolIndex = 2;
                    }

                    if ( maxProtocolIndex < 2 )
                    {
                        maxProtocolIndex = 2;
                    }
                }
                else if ( p.equals( "TLSv1.1" ) )
                {
                    if ( minProtocolIndex > 3 )
                    {
                        minProtocolIndex = 3;
                    }

                    if ( maxProtocolIndex < 3 )
                    {
                        maxProtocolIndex = 3;
                    }
                }
                else if ( p.equals( "TLSv1.2" ) )
                {
                    if ( minProtocolIndex > 4 )
                    {
                        minProtocolIndex = 4;
                    }

                    if ( maxProtocolIndex < 4 )
                    {
                        maxProtocolIndex = 4;
                    }
                }
                else if ( p.equals( "TLSv1.3" ) )
                {
                    if ( minProtocolIndex > 5 )
                    {
                        minProtocolIndex = 5;
                    }

                    if ( maxProtocolIndex < 5 )
                    {
                        maxProtocolIndex = 5;
                    }
                }
            }

            synchronized ( this )
            {
                if ( this.isDestroyed() )
                {
                    throw new IllegalStateException( "failed to enable protocols: " + Arrays.asList( protocols ) );
                }
                else
                {
                    SSL.clearOptions( this.ssl,
                            SSL.SSL_OP_NO_SSLv2 | SSL.SSL_OP_NO_SSLv3 | SSL.SSL_OP_NO_TLSv1 | SSL.SSL_OP_NO_TLSv1_1 | SSL.SSL_OP_NO_TLSv1_2 |
                                    SSL.SSL_OP_NO_TLSv1_3 );
                    opts = 0;

                    for ( i = 0; i < minProtocolIndex; ++i )
                    {
                        opts |= OPENSSL_OP_NO_PROTOCOLS[i];
                    }

                    assert maxProtocolIndex != Integer.MAX_VALUE;

                    for ( i = maxProtocolIndex + 1; i < OPENSSL_OP_NO_PROTOCOLS.length; ++i )
                    {
                        opts |= OPENSSL_OP_NO_PROTOCOLS[i];
                    }

                    SSL.setOptions( this.ssl, opts );
                }
            }
        }
    }

    public final SSLSession getSession()
    {
        return this.session;
    }

    public final synchronized void beginHandshake() throws SSLException
    {
        switch ( this.handshakeState )
        {
        case NOT_STARTED:
            this.handshakeState = ReferenceCountedOpenSslEngine.HandshakeState.STARTED_EXPLICITLY;
            if ( this.handshake() == HandshakeStatus.NEED_TASK )
            {
                this.needTask = true;
            }

            this.calculateMaxWrapOverhead();
            break;
        case FINISHED:
            throw new SSLException( "renegotiation unsupported" );
        case STARTED_IMPLICITLY:
            this.checkEngineClosed();
            this.handshakeState = ReferenceCountedOpenSslEngine.HandshakeState.STARTED_EXPLICITLY;
            this.calculateMaxWrapOverhead();
        case STARTED_EXPLICITLY:
            break;
        default:
            throw new Error();
        }
    }

    private void checkEngineClosed() throws SSLException
    {
        if ( this.isDestroyed() )
        {
            throw new SSLException( "engine closed" );
        }
    }

    private HandshakeStatus handshakeException() throws SSLException
    {
        if ( SSL.bioLengthNonApplication( this.networkBIO ) > 0 )
        {
            return HandshakeStatus.NEED_WRAP;
        }
        else
        {
            Throwable exception = this.handshakeException;

            assert exception != null;

            this.handshakeException = null;
            this.shutdown();
            if ( exception instanceof SSLHandshakeException )
            {
                throw (SSLHandshakeException) exception;
            }
            else
            {
                SSLHandshakeException e = new SSLHandshakeException( "General OpenSslEngine problem" );
                e.initCause( exception );
                throw e;
            }
        }
    }

    final void initHandshakeException( Throwable cause )
    {
        assert this.handshakeException == null;

        this.handshakeException = cause;
    }

    private HandshakeStatus handshake() throws SSLException
    {
        if ( this.needTask )
        {
            return HandshakeStatus.NEED_TASK;
        }
        else if ( this.handshakeState == ReferenceCountedOpenSslEngine.HandshakeState.FINISHED )
        {
            return HandshakeStatus.FINISHED;
        }
        else
        {
            this.checkEngineClosed();
            if ( this.handshakeException != null )
            {
                return this.handshakeException();
            }
            else
            {
                this.engineMap.add( this );
                if ( this.lastAccessed == -1L )
                {
                    this.lastAccessed = System.currentTimeMillis();
                }

                int code = SSL.doHandshake( this.ssl );
                if ( code <= 0 )
                {
                    int sslError = SSL.getError( this.ssl, code );
                    if ( sslError != SSL.SSL_ERROR_WANT_READ && sslError != SSL.SSL_ERROR_WANT_WRITE )
                    {
                        if ( sslError != SSL.SSL_ERROR_WANT_X509_LOOKUP && sslError != SSL.SSL_ERROR_WANT_CERTIFICATE_VERIFY &&
                                sslError != SSL.SSL_ERROR_WANT_PRIVATE_KEY_OPERATION )
                        {
                            if ( this.handshakeException != null )
                            {
                                return this.handshakeException();
                            }
                            else
                            {
                                throw this.shutdownWithError( "SSL_do_handshake", sslError );
                            }
                        }
                        else
                        {
                            return HandshakeStatus.NEED_TASK;
                        }
                    }
                    else
                    {
                        return pendingStatus( SSL.bioLengthNonApplication( this.networkBIO ) );
                    }
                }
                else if ( SSL.bioLengthNonApplication( this.networkBIO ) > 0 )
                {
                    return HandshakeStatus.NEED_WRAP;
                }
                else
                {
                    this.session.handshakeFinished();
                    return HandshakeStatus.FINISHED;
                }
            }
        }
    }

    private HandshakeStatus mayFinishHandshake( HandshakeStatus status ) throws SSLException
    {
        return status == HandshakeStatus.NOT_HANDSHAKING && this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.FINISHED ? this.handshake()
                                                                                                                                         : status;
    }

    public final synchronized HandshakeStatus getHandshakeStatus()
    {
        if ( this.needPendingStatus() )
        {
            return this.needTask ? HandshakeStatus.NEED_TASK : pendingStatus( SSL.bioLengthNonApplication( this.networkBIO ) );
        }
        else
        {
            return HandshakeStatus.NOT_HANDSHAKING;
        }
    }

    private HandshakeStatus getHandshakeStatus( int pending )
    {
        if ( this.needPendingStatus() )
        {
            return this.needTask ? HandshakeStatus.NEED_TASK : pendingStatus( pending );
        }
        else
        {
            return HandshakeStatus.NOT_HANDSHAKING;
        }
    }

    private boolean needPendingStatus()
    {
        return this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.NOT_STARTED && !this.isDestroyed() &&
                (this.handshakeState != ReferenceCountedOpenSslEngine.HandshakeState.FINISHED || this.isInboundDone() || this.isOutboundDone());
    }

    private String toJavaCipherSuite( String openSslCipherSuite )
    {
        if ( openSslCipherSuite == null )
        {
            return null;
        }
        else
        {
            String version = SSL.getVersion( this.ssl );
            String prefix = toJavaCipherSuitePrefix( version );
            return CipherSuiteConverter.toJava( openSslCipherSuite, prefix );
        }
    }

    public final boolean getUseClientMode()
    {
        return this.clientMode;
    }

    public final void setUseClientMode( boolean clientMode )
    {
        if ( clientMode != this.clientMode )
        {
            throw new UnsupportedOperationException();
        }
    }

    public final boolean getNeedClientAuth()
    {
        return this.clientAuth == ClientAuth.REQUIRE;
    }

    public final void setNeedClientAuth( boolean b )
    {
        this.setClientAuth( b ? ClientAuth.REQUIRE : ClientAuth.NONE );
    }

    public final boolean getWantClientAuth()
    {
        return this.clientAuth == ClientAuth.OPTIONAL;
    }

    public final void setWantClientAuth( boolean b )
    {
        this.setClientAuth( b ? ClientAuth.OPTIONAL : ClientAuth.NONE );
    }

    public final synchronized void setVerify( int verifyMode, int depth )
    {
        SSL.setVerify( this.ssl, verifyMode, depth );
    }

    private void setClientAuth( ClientAuth mode )
    {
        if ( !this.clientMode )
        {
            synchronized ( this )
            {
                if ( this.clientAuth != mode )
                {
                    switch ( mode )
                    {
                    case NONE:
                        SSL.setVerify( this.ssl, 0, 10 );
                        break;
                    case REQUIRE:
                        SSL.setVerify( this.ssl, 2, 10 );
                        break;
                    case OPTIONAL:
                        SSL.setVerify( this.ssl, 1, 10 );
                        break;
                    default:
                        throw new Error( mode.toString() );
                    }

                    this.clientAuth = mode;
                }
            }
        }
    }

    public final boolean getEnableSessionCreation()
    {
        return false;
    }

    public final void setEnableSessionCreation( boolean b )
    {
        if ( b )
        {
            throw new UnsupportedOperationException();
        }
    }

    public final synchronized SSLParameters getSSLParameters()
    {
        SSLParameters sslParameters = super.getSSLParameters();
        int version = PlatformDependent.javaVersion();
        if ( version >= 7 )
        {
            sslParameters.setEndpointIdentificationAlgorithm( this.endPointIdentificationAlgorithm );
            Java7SslParametersUtils.setAlgorithmConstraints( sslParameters, this.algorithmConstraints );
            if ( version >= 8 )
            {
                if ( this.sniHostNames != null )
                {
                    Java8SslUtils.setSniHostNames( sslParameters, this.sniHostNames );
                }

                if ( !this.isDestroyed() )
                {
                    Java8SslUtils.setUseCipherSuitesOrder( sslParameters, (SSL.getOptions( this.ssl ) & SSL.SSL_OP_CIPHER_SERVER_PREFERENCE) != 0 );
                }

                Java8SslUtils.setSNIMatchers( sslParameters, this.matchers );
            }
        }

        return sslParameters;
    }

    public final synchronized void setSSLParameters( SSLParameters sslParameters )
    {
        int version = PlatformDependent.javaVersion();
        if ( version >= 7 )
        {
            if ( sslParameters.getAlgorithmConstraints() != null )
            {
                throw new IllegalArgumentException( "AlgorithmConstraints are not supported." );
            }

            if ( version >= 8 )
            {
                if ( !this.isDestroyed() )
                {
                    if ( this.clientMode )
                    {
                        List<String> sniHostNames = Java8SslUtils.getSniHostNames( sslParameters );
                        Iterator var4 = sniHostNames.iterator();

                        while ( var4.hasNext() )
                        {
                            String name = (String) var4.next();
                            SSL.setTlsExtHostName( this.ssl, name );
                        }

                        this.sniHostNames = sniHostNames;
                    }

                    if ( Java8SslUtils.getUseCipherSuitesOrder( sslParameters ) )
                    {
                        SSL.setOptions( this.ssl, SSL.SSL_OP_CIPHER_SERVER_PREFERENCE );
                    }
                    else
                    {
                        SSL.clearOptions( this.ssl, SSL.SSL_OP_CIPHER_SERVER_PREFERENCE );
                    }
                }

                this.matchers = sslParameters.getSNIMatchers();
            }

            String endPointIdentificationAlgorithm = sslParameters.getEndpointIdentificationAlgorithm();
            boolean endPointVerificationEnabled = isEndPointVerificationEnabled( endPointIdentificationAlgorithm );
            if ( this.clientMode && endPointVerificationEnabled )
            {
                SSL.setVerify( this.ssl, 2, -1 );
            }

            this.endPointIdentificationAlgorithm = endPointIdentificationAlgorithm;
            this.algorithmConstraints = sslParameters.getAlgorithmConstraints();
        }

        super.setSSLParameters( sslParameters );
    }

    private boolean isDestroyed()
    {
        return this.destroyed != 0;
    }

    final boolean checkSniHostnameMatch( byte[] hostname )
    {
        return Java8SslUtils.checkSniHostnameMatch( this.matchers, hostname );
    }

    public String getNegotiatedApplicationProtocol()
    {
        return this.applicationProtocol;
    }

    private static enum HandshakeState
    {
        NOT_STARTED,
        STARTED_IMPLICITLY,
        STARTED_EXPLICITLY,
        FINISHED;
    }

    private final class DefaultOpenSslSession implements OpenSslSession
    {
        private final OpenSslSessionContext sessionContext;
        private X509Certificate[] x509PeerCerts;
        private Certificate[] peerCerts;
        private String protocol;
        private String cipher;
        private byte[] id;
        private long creationTime;
        private volatile int applicationBufferSize;
        private Map<String,Object> values;

        DefaultOpenSslSession( OpenSslSessionContext sessionContext )
        {
            this.applicationBufferSize = ReferenceCountedOpenSslEngine.MAX_PLAINTEXT_LENGTH;
            this.sessionContext = sessionContext;
        }

        private SSLSessionBindingEvent newSSLSessionBindingEvent( String name )
        {
            return new SSLSessionBindingEvent( ReferenceCountedOpenSslEngine.this.session, name );
        }

        public byte[] getId()
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                return this.id == null ? EmptyArrays.EMPTY_BYTES : (byte[]) this.id.clone();
            }
        }

        public SSLSessionContext getSessionContext()
        {
            return this.sessionContext;
        }

        public long getCreationTime()
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                if ( this.creationTime == 0L && !ReferenceCountedOpenSslEngine.this.isDestroyed() )
                {
                    this.creationTime = SSL.getTime( ReferenceCountedOpenSslEngine.this.ssl ) * 1000L;
                }
            }

            return this.creationTime;
        }

        public long getLastAccessedTime()
        {
            long lastAccessed = ReferenceCountedOpenSslEngine.this.lastAccessed;
            return lastAccessed == -1L ? this.getCreationTime() : lastAccessed;
        }

        public void invalidate()
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                if ( !ReferenceCountedOpenSslEngine.this.isDestroyed() )
                {
                    SSL.setTimeout( ReferenceCountedOpenSslEngine.this.ssl, 0L );
                }
            }
        }

        public boolean isValid()
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                if ( !ReferenceCountedOpenSslEngine.this.isDestroyed() )
                {
                    return System.currentTimeMillis() - SSL.getTimeout( ReferenceCountedOpenSslEngine.this.ssl ) * 1000L <
                            SSL.getTime( ReferenceCountedOpenSslEngine.this.ssl ) * 1000L;
                }
                else
                {
                    return false;
                }
            }
        }

        public void putValue( String name, Object value )
        {
            if ( name == null )
            {
                throw new NullPointerException( "name" );
            }
            else if ( value == null )
            {
                throw new NullPointerException( "value" );
            }
            else
            {
                Object old;
                synchronized ( this )
                {
                    Map<String,Object> values = this.values;
                    if ( values == null )
                    {
                        values = this.values = new HashMap( 2 );
                    }

                    old = values.put( name, value );
                }

                if ( value instanceof SSLSessionBindingListener )
                {
                    ((SSLSessionBindingListener) value).valueBound( this.newSSLSessionBindingEvent( name ) );
                }

                this.notifyUnbound( old, name );
            }
        }

        public Object getValue( String name )
        {
            if ( name == null )
            {
                throw new NullPointerException( "name" );
            }
            else
            {
                synchronized ( this )
                {
                    return this.values == null ? null : this.values.get( name );
                }
            }
        }

        public void removeValue( String name )
        {
            if ( name == null )
            {
                throw new NullPointerException( "name" );
            }
            else
            {
                Object old;
                synchronized ( this )
                {
                    Map<String,Object> values = this.values;
                    if ( values == null )
                    {
                        return;
                    }

                    old = values.remove( name );
                }

                this.notifyUnbound( old, name );
            }
        }

        public String[] getValueNames()
        {
            synchronized ( this )
            {
                Map<String,Object> values = this.values;
                return values != null && !values.isEmpty() ? (String[]) values.keySet().toArray( new String[0] ) : EmptyArrays.EMPTY_STRINGS;
            }
        }

        private void notifyUnbound( Object value, String name )
        {
            if ( value instanceof SSLSessionBindingListener )
            {
                ((SSLSessionBindingListener) value).valueUnbound( this.newSSLSessionBindingEvent( name ) );
            }
        }

        public void handshakeFinished() throws SSLException
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                if ( !ReferenceCountedOpenSslEngine.this.isDestroyed() )
                {
                    this.id = SSL.getSessionId( ReferenceCountedOpenSslEngine.this.ssl );
                    this.cipher = ReferenceCountedOpenSslEngine.this.toJavaCipherSuite( SSL.getCipherForSSL( ReferenceCountedOpenSslEngine.this.ssl ) );
                    this.protocol = SSL.getVersion( ReferenceCountedOpenSslEngine.this.ssl );
                    this.initPeerCerts();
                    this.selectApplicationProtocol();
                    ReferenceCountedOpenSslEngine.this.calculateMaxWrapOverhead();
                    ReferenceCountedOpenSslEngine.this.handshakeState = ReferenceCountedOpenSslEngine.HandshakeState.FINISHED;
                }
                else
                {
                    throw new SSLException( "Already closed" );
                }
            }
        }

        private void initPeerCerts()
        {
            byte[][] chain = SSL.getPeerCertChain( ReferenceCountedOpenSslEngine.this.ssl );
            if ( ReferenceCountedOpenSslEngine.this.clientMode )
            {
                if ( ReferenceCountedOpenSslEngine.isEmpty( (Object[]) chain ) )
                {
                    this.peerCerts = EmptyArrays.EMPTY_CERTIFICATES;
                    this.x509PeerCerts = EmptyArrays.EMPTY_JAVAX_X509_CERTIFICATES;
                }
                else
                {
                    this.peerCerts = new Certificate[chain.length];
                    this.x509PeerCerts = new X509Certificate[chain.length];
                    this.initCerts( chain, 0 );
                }
            }
            else
            {
                byte[] clientCert = SSL.getPeerCertificate( ReferenceCountedOpenSslEngine.this.ssl );
                if ( ReferenceCountedOpenSslEngine.isEmpty( clientCert ) )
                {
                    this.peerCerts = EmptyArrays.EMPTY_CERTIFICATES;
                    this.x509PeerCerts = EmptyArrays.EMPTY_JAVAX_X509_CERTIFICATES;
                }
                else if ( ReferenceCountedOpenSslEngine.isEmpty( (Object[]) chain ) )
                {
                    this.peerCerts = new Certificate[]{new OpenSslX509Certificate( clientCert )};
                    this.x509PeerCerts = new X509Certificate[]{new OpenSslJavaxX509Certificate( clientCert )};
                }
                else
                {
                    this.peerCerts = new Certificate[chain.length + 1];
                    this.x509PeerCerts = new X509Certificate[chain.length + 1];
                    this.peerCerts[0] = new OpenSslX509Certificate( clientCert );
                    this.x509PeerCerts[0] = new OpenSslJavaxX509Certificate( clientCert );
                    this.initCerts( chain, 1 );
                }
            }
        }

        private void initCerts( byte[][] chain, int startPos )
        {
            for ( int i = 0; i < chain.length; ++i )
            {
                int certPos = startPos + i;
                this.peerCerts[certPos] = new OpenSslX509Certificate( chain[i] );
                this.x509PeerCerts[certPos] = new OpenSslJavaxX509Certificate( chain[i] );
            }
        }

        private void selectApplicationProtocol() throws SSLException
        {
            ApplicationProtocolConfig.SelectedListenerFailureBehavior behavior = ReferenceCountedOpenSslEngine.this.apn.selectedListenerFailureBehavior();
            List<String> protocols = ReferenceCountedOpenSslEngine.this.apn.protocols();
            String applicationProtocol;
            switch ( ReferenceCountedOpenSslEngine.this.apn.protocol() )
            {
            case NONE:
                break;
            case ALPN:
                applicationProtocol = SSL.getAlpnSelected( ReferenceCountedOpenSslEngine.this.ssl );
                if ( applicationProtocol != null )
                {
                    ReferenceCountedOpenSslEngine.this.applicationProtocol = this.selectApplicationProtocol( protocols, behavior, applicationProtocol );
                }
                break;
            case NPN:
                applicationProtocol = SSL.getNextProtoNegotiated( ReferenceCountedOpenSslEngine.this.ssl );
                if ( applicationProtocol != null )
                {
                    ReferenceCountedOpenSslEngine.this.applicationProtocol = this.selectApplicationProtocol( protocols, behavior, applicationProtocol );
                }
                break;
            case NPN_AND_ALPN:
                applicationProtocol = SSL.getAlpnSelected( ReferenceCountedOpenSslEngine.this.ssl );
                if ( applicationProtocol == null )
                {
                    applicationProtocol = SSL.getNextProtoNegotiated( ReferenceCountedOpenSslEngine.this.ssl );
                }

                if ( applicationProtocol != null )
                {
                    ReferenceCountedOpenSslEngine.this.applicationProtocol = this.selectApplicationProtocol( protocols, behavior, applicationProtocol );
                }
                break;
            default:
                throw new Error();
            }
        }

        private String selectApplicationProtocol( List<String> protocols, ApplicationProtocolConfig.SelectedListenerFailureBehavior behavior,
                String applicationProtocol ) throws SSLException
        {
            if ( behavior == ApplicationProtocolConfig.SelectedListenerFailureBehavior.ACCEPT )
            {
                return applicationProtocol;
            }
            else
            {
                int size = protocols.size();

                assert size > 0;

                if ( protocols.contains( applicationProtocol ) )
                {
                    return applicationProtocol;
                }
                else if ( behavior == ApplicationProtocolConfig.SelectedListenerFailureBehavior.CHOOSE_MY_LAST_PROTOCOL )
                {
                    return (String) protocols.get( size - 1 );
                }
                else
                {
                    throw new SSLException( "unknown protocol " + applicationProtocol );
                }
            }
        }

        public Certificate[] getPeerCertificates() throws SSLPeerUnverifiedException
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                if ( ReferenceCountedOpenSslEngine.isEmpty( (Object[]) this.peerCerts ) )
                {
                    throw new SSLPeerUnverifiedException( "peer not verified" );
                }
                else
                {
                    return (Certificate[]) this.peerCerts.clone();
                }
            }
        }

        public Certificate[] getLocalCertificates()
        {
            Certificate[] localCerts = ReferenceCountedOpenSslEngine.this.localCertificateChain;
            return localCerts == null ? null : (Certificate[]) localCerts.clone();
        }

        public X509Certificate[] getPeerCertificateChain() throws SSLPeerUnverifiedException
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                if ( ReferenceCountedOpenSslEngine.isEmpty( (Object[]) this.x509PeerCerts ) )
                {
                    throw new SSLPeerUnverifiedException( "peer not verified" );
                }
                else
                {
                    return (X509Certificate[]) this.x509PeerCerts.clone();
                }
            }
        }

        public Principal getPeerPrincipal() throws SSLPeerUnverifiedException
        {
            Certificate[] peer = this.getPeerCertificates();
            return ((java.security.cert.X509Certificate) peer[0]).getSubjectX500Principal();
        }

        public Principal getLocalPrincipal()
        {
            Certificate[] local = ReferenceCountedOpenSslEngine.this.localCertificateChain;
            return local != null && local.length != 0 ? ((java.security.cert.X509Certificate) local[0]).getIssuerX500Principal() : null;
        }

        public String getCipherSuite()
        {
            synchronized ( ReferenceCountedOpenSslEngine.this )
            {
                return this.cipher == null ? "SSL_NULL_WITH_NULL_NULL" : this.cipher;
            }
        }

        public String getProtocol()
        {
            String protocol = this.protocol;
            if ( protocol == null )
            {
                synchronized ( ReferenceCountedOpenSslEngine.this )
                {
                    if ( !ReferenceCountedOpenSslEngine.this.isDestroyed() )
                    {
                        protocol = SSL.getVersion( ReferenceCountedOpenSslEngine.this.ssl );
                    }
                    else
                    {
                        protocol = "";
                    }
                }
            }

            return protocol;
        }

        public String getPeerHost()
        {
            return ReferenceCountedOpenSslEngine.this.getPeerHost();
        }

        public int getPeerPort()
        {
            return ReferenceCountedOpenSslEngine.this.getPeerPort();
        }

        public int getPacketBufferSize()
        {
            return ReferenceCountedOpenSslEngine.this.maxEncryptedPacketLength();
        }

        public int getApplicationBufferSize()
        {
            return this.applicationBufferSize;
        }

        public void tryExpandApplicationBufferSize( int packetLengthDataOnly )
        {
            if ( packetLengthDataOnly > ReferenceCountedOpenSslEngine.MAX_PLAINTEXT_LENGTH &&
                    this.applicationBufferSize != ReferenceCountedOpenSslEngine.MAX_RECORD_SIZE )
            {
                this.applicationBufferSize = ReferenceCountedOpenSslEngine.MAX_RECORD_SIZE;
            }
        }
    }
}
