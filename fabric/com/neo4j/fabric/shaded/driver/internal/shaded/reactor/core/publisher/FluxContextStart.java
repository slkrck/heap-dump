package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Subscription;

final class FluxContextStart<T> extends InternalFluxOperator<T,T> implements Fuseable
{
    final Function<Context,Context> doOnContext;

    FluxContextStart( Flux<? extends T> source, Function<Context,Context> doOnContext )
    {
        super( source );
        this.doOnContext = (Function) Objects.requireNonNull( doOnContext, "doOnContext" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        Context c;
        try
        {
            c = (Context) this.doOnContext.apply( actual.currentContext() );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        return new FluxContextStart.ContextStartSubscriber( actual, c );
    }

    static final class ContextStartSubscriber<T> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        final CoreSubscriber<? super T> actual;
        final Fuseable.ConditionalSubscriber<? super T> actualConditional;
        final Context context;
        Fuseable.QueueSubscription<T> qs;
        Subscription s;

        ContextStartSubscriber( CoreSubscriber<? super T> actual, Context context )
        {
            this.actual = actual;
            this.context = context;
            if ( actual instanceof Fuseable.ConditionalSubscriber )
            {
                this.actualConditional = (Fuseable.ConditionalSubscriber) actual;
            }
            else
            {
                this.actualConditional = null;
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }

        public Context currentContext()
        {
            return this.context;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    this.qs = (Fuseable.QueueSubscription) s;
                }

                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public boolean tryOnNext( T t )
        {
            if ( this.actualConditional != null )
            {
                return this.actualConditional.tryOnNext( t );
            }
            else
            {
                this.actual.onNext( t );
                return true;
            }
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }

        public int requestFusion( int requestedMode )
        {
            return this.qs == null ? 0 : this.qs.requestFusion( requestedMode );
        }

        @Nullable
        public T poll()
        {
            return this.qs != null ? this.qs.poll() : null;
        }

        public boolean isEmpty()
        {
            return this.qs == null || this.qs.isEmpty();
        }

        public void clear()
        {
            if ( this.qs != null )
            {
                this.qs.clear();
            }
        }

        public int size()
        {
            return this.qs != null ? this.qs.size() : 0;
        }
    }
}
