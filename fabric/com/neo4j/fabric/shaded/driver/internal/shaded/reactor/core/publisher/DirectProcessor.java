package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Subscription;

public final class DirectProcessor<T> extends FluxProcessor<T,T>
{
    private static final DirectProcessor.DirectInner[] EMPTY = new DirectProcessor.DirectInner[0];
    private static final DirectProcessor.DirectInner[] TERMINATED = new DirectProcessor.DirectInner[0];
    private static final AtomicReferenceFieldUpdater<DirectProcessor,DirectProcessor.DirectInner[]> SUBSCRIBERS =
            AtomicReferenceFieldUpdater.newUpdater( DirectProcessor.class, DirectProcessor.DirectInner[].class, "subscribers" );
    Throwable error;
    private volatile DirectProcessor.DirectInner<T>[] subscribers;

    DirectProcessor()
    {
        this.subscribers = EMPTY;
    }

    public static <E> DirectProcessor<E> create()
    {
        return new DirectProcessor();
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public void onSubscribe( Subscription s )
    {
        Objects.requireNonNull( s, "s" );
        if ( this.subscribers != TERMINATED )
        {
            s.request( Long.MAX_VALUE );
        }
        else
        {
            s.cancel();
        }
    }

    public void onNext( T t )
    {
        Objects.requireNonNull( t, "t" );
        DirectProcessor.DirectInner<T>[] inners = this.subscribers;
        if ( inners == TERMINATED )
        {
            Operators.onNextDropped( t, this.currentContext() );
        }
        else
        {
            DirectProcessor.DirectInner[] var3 = inners;
            int var4 = inners.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                DirectProcessor.DirectInner<T> s = var3[var5];
                s.onNext( t );
            }
        }
    }

    public void onError( Throwable t )
    {
        Objects.requireNonNull( t, "t" );
        DirectProcessor.DirectInner<T>[] inners = this.subscribers;
        if ( inners == TERMINATED )
        {
            Operators.onErrorDropped( t, this.currentContext() );
        }
        else
        {
            this.error = t;
            DirectProcessor.DirectInner[] var3 = (DirectProcessor.DirectInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
            int var4 = var3.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                DirectProcessor.DirectInner<?> s = var3[var5];
                s.onError( t );
            }
        }
    }

    public void onComplete()
    {
        DirectProcessor.DirectInner[] var1 = (DirectProcessor.DirectInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
        int var2 = var1.length;

        for ( int var3 = 0; var3 < var2; ++var3 )
        {
            DirectProcessor.DirectInner<?> s = var1[var3];
            s.onComplete();
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Objects.requireNonNull( actual, "subscribe" );
        DirectProcessor.DirectInner<T> p = new DirectProcessor.DirectInner( actual, this );
        actual.onSubscribe( p );
        if ( this.add( p ) )
        {
            if ( p.cancelled )
            {
                this.remove( p );
            }
        }
        else
        {
            Throwable e = this.error;
            if ( e != null )
            {
                actual.onError( e );
            }
            else
            {
                actual.onComplete();
            }
        }
    }

    public Stream<? extends Scannable> inners()
    {
        return Stream.of( this.subscribers );
    }

    public boolean isTerminated()
    {
        return TERMINATED == this.subscribers;
    }

    public long downstreamCount()
    {
        return (long) this.subscribers.length;
    }

    boolean add( DirectProcessor.DirectInner<T> s )
    {
        DirectProcessor.DirectInner<T>[] a = this.subscribers;
        if ( a == TERMINATED )
        {
            return false;
        }
        else
        {
            synchronized ( this )
            {
                a = this.subscribers;
                if ( a == TERMINATED )
                {
                    return false;
                }
                else
                {
                    int len = a.length;
                    DirectProcessor.DirectInner<T>[] b = new DirectProcessor.DirectInner[len + 1];
                    System.arraycopy( a, 0, b, 0, len );
                    b[len] = s;
                    this.subscribers = b;
                    return true;
                }
            }
        }
    }

    void remove( DirectProcessor.DirectInner<T> s )
    {
        DirectProcessor.DirectInner<T>[] a = this.subscribers;
        if ( a != TERMINATED && a != EMPTY )
        {
            synchronized ( this )
            {
                a = this.subscribers;
                if ( a != TERMINATED && a != EMPTY )
                {
                    int len = a.length;
                    int j = -1;

                    for ( int i = 0; i < len; ++i )
                    {
                        if ( a[i] == s )
                        {
                            j = i;
                            break;
                        }
                    }

                    if ( j >= 0 )
                    {
                        if ( len == 1 )
                        {
                            this.subscribers = EMPTY;
                        }
                        else
                        {
                            DirectProcessor.DirectInner<T>[] b = new DirectProcessor.DirectInner[len - 1];
                            System.arraycopy( a, 0, b, 0, j );
                            System.arraycopy( a, j + 1, b, j, len - j - 1 );
                            this.subscribers = b;
                        }
                    }
                }
            }
        }
    }

    public boolean hasDownstreams()
    {
        DirectProcessor.DirectInner<T>[] s = this.subscribers;
        return s != EMPTY && s != TERMINATED;
    }

    @Nullable
    public Throwable getError()
    {
        return this.subscribers == TERMINATED ? this.error : null;
    }

    static final class DirectInner<T> implements InnerProducer<T>
    {
        static final AtomicLongFieldUpdater<DirectProcessor.DirectInner> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( DirectProcessor.DirectInner.class, "requested" );
        final CoreSubscriber<? super T> actual;
        final DirectProcessor<T> parent;
        volatile boolean cancelled;
        volatile long requested;

        DirectInner( CoreSubscriber<? super T> actual, DirectProcessor<T> parent )
        {
            this.actual = actual;
            this.parent = parent;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.parent.remove( this );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.cancelled : InnerProducer.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        void onNext( T value )
        {
            if ( this.requested != 0L )
            {
                this.actual.onNext( value );
                if ( this.requested != Long.MAX_VALUE )
                {
                    REQUESTED.decrementAndGet( this );
                }
            }
            else
            {
                this.parent.remove( this );
                this.actual.onError( Exceptions.failWithOverflow( "Can't deliver value due to lack of requests" ) );
            }
        }

        void onError( Throwable e )
        {
            this.actual.onError( e );
        }

        void onComplete()
        {
            this.actual.onComplete();
        }
    }
}
