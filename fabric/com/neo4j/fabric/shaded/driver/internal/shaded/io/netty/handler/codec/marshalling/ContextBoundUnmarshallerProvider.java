package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.marshalling;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.Attribute;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AttributeKey;
import org.jboss.marshalling.MarshallerFactory;
import org.jboss.marshalling.MarshallingConfiguration;
import org.jboss.marshalling.Unmarshaller;

public class ContextBoundUnmarshallerProvider extends DefaultUnmarshallerProvider
{
    private static final AttributeKey<Unmarshaller> UNMARSHALLER = AttributeKey.valueOf( ContextBoundUnmarshallerProvider.class, "UNMARSHALLER" );

    public ContextBoundUnmarshallerProvider( MarshallerFactory factory, MarshallingConfiguration config )
    {
        super( factory, config );
    }

    public Unmarshaller getUnmarshaller( ChannelHandlerContext ctx ) throws Exception
    {
        Attribute<Unmarshaller> attr = ctx.channel().attr( UNMARSHALLER );
        Unmarshaller unmarshaller = (Unmarshaller) attr.get();
        if ( unmarshaller == null )
        {
            unmarshaller = super.getUnmarshaller( ctx );
            attr.set( unmarshaller );
        }

        return unmarshaller;
    }
}
