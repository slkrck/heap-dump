package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ipfilter;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;

@ChannelHandler.Sharable
public class RuleBasedIpFilter extends AbstractRemoteAddressFilter<InetSocketAddress>
{
    private final IpFilterRule[] rules;

    public RuleBasedIpFilter( IpFilterRule... rules )
    {
        if ( rules == null )
        {
            throw new NullPointerException( "rules" );
        }
        else
        {
            this.rules = rules;
        }
    }

    protected boolean accept( ChannelHandlerContext ctx, InetSocketAddress remoteAddress ) throws Exception
    {
        IpFilterRule[] var3 = this.rules;
        int var4 = var3.length;

        for ( int var5 = 0; var5 < var4; ++var5 )
        {
            IpFilterRule rule = var3[var5];
            if ( rule == null )
            {
                break;
            }

            if ( rule.matches( remoteAddress ) )
            {
                return rule.ruleType() == IpFilterRuleType.ACCEPT;
            }
        }

        return true;
    }
}
