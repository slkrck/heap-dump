package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.DefaultPriorityQueue;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PriorityQueue;

import java.util.Comparator;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.Delayed;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public abstract class AbstractScheduledEventExecutor extends AbstractEventExecutor
{
    private static final Comparator<ScheduledFutureTask<?>> SCHEDULED_FUTURE_TASK_COMPARATOR = new Comparator<ScheduledFutureTask<?>>()
    {
        public int compare( ScheduledFutureTask<?> o1, ScheduledFutureTask<?> o2 )
        {
            return o1.compareTo( (Delayed) o2 );
        }
    };
    PriorityQueue<ScheduledFutureTask<?>> scheduledTaskQueue;

    protected AbstractScheduledEventExecutor()
    {
    }

    protected AbstractScheduledEventExecutor( EventExecutorGroup parent )
    {
        super( parent );
    }

    protected static long nanoTime()
    {
        return ScheduledFutureTask.nanoTime();
    }

    protected static long deadlineToDelayNanos( long deadlineNanos )
    {
        return ScheduledFutureTask.deadlineToDelayNanos( deadlineNanos );
    }

    protected static long initialNanoTime()
    {
        return ScheduledFutureTask.initialNanoTime();
    }

    private static boolean isNullOrEmpty( Queue<ScheduledFutureTask<?>> queue )
    {
        return queue == null || queue.isEmpty();
    }

    PriorityQueue<ScheduledFutureTask<?>> scheduledTaskQueue()
    {
        if ( this.scheduledTaskQueue == null )
        {
            this.scheduledTaskQueue = new DefaultPriorityQueue( SCHEDULED_FUTURE_TASK_COMPARATOR, 11 );
        }

        return this.scheduledTaskQueue;
    }

    protected void cancelScheduledTasks()
    {
        assert this.inEventLoop();

        PriorityQueue<ScheduledFutureTask<?>> scheduledTaskQueue = this.scheduledTaskQueue;
        if ( !isNullOrEmpty( scheduledTaskQueue ) )
        {
            ScheduledFutureTask<?>[] scheduledTasks = (ScheduledFutureTask[]) scheduledTaskQueue.toArray( new ScheduledFutureTask[0] );
            ScheduledFutureTask[] var3 = scheduledTasks;
            int var4 = scheduledTasks.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                ScheduledFutureTask<?> task = var3[var5];
                task.cancelWithoutRemove( false );
            }

            scheduledTaskQueue.clearIgnoringIndexes();
        }
    }

    protected final Runnable pollScheduledTask()
    {
        return this.pollScheduledTask( nanoTime() );
    }

    protected final Runnable pollScheduledTask( long nanoTime )
    {
        assert this.inEventLoop();

        Queue<ScheduledFutureTask<?>> scheduledTaskQueue = this.scheduledTaskQueue;
        ScheduledFutureTask<?> scheduledTask = scheduledTaskQueue == null ? null : (ScheduledFutureTask) scheduledTaskQueue.peek();
        if ( scheduledTask != null && scheduledTask.deadlineNanos() - nanoTime <= 0L )
        {
            scheduledTaskQueue.remove();
            return scheduledTask;
        }
        else
        {
            return null;
        }
    }

    protected final long nextScheduledTaskNano()
    {
        ScheduledFutureTask<?> scheduledTask = this.peekScheduledTask();
        return scheduledTask != null ? Math.max( 0L, scheduledTask.deadlineNanos() - nanoTime() ) : -1L;
    }

    protected final long nextScheduledTaskDeadlineNanos()
    {
        ScheduledFutureTask<?> scheduledTask = this.peekScheduledTask();
        return scheduledTask != null ? scheduledTask.deadlineNanos() : -1L;
    }

    final ScheduledFutureTask<?> peekScheduledTask()
    {
        Queue<ScheduledFutureTask<?>> scheduledTaskQueue = this.scheduledTaskQueue;
        return scheduledTaskQueue != null ? (ScheduledFutureTask) scheduledTaskQueue.peek() : null;
    }

    protected final boolean hasScheduledTasks()
    {
        ScheduledFutureTask<?> scheduledTask = this.peekScheduledTask();
        return scheduledTask != null && scheduledTask.deadlineNanos() <= nanoTime();
    }

    public ScheduledFuture<?> schedule( Runnable command, long delay, TimeUnit unit )
    {
        ObjectUtil.checkNotNull( command, "command" );
        ObjectUtil.checkNotNull( unit, "unit" );
        if ( delay < 0L )
        {
            delay = 0L;
        }

        this.validateScheduled0( delay, unit );
        return this.schedule( new ScheduledFutureTask( this, command, (Object) null, ScheduledFutureTask.deadlineNanos( unit.toNanos( delay ) ) ) );
    }

    public <V> ScheduledFuture<V> schedule( Callable<V> callable, long delay, TimeUnit unit )
    {
        ObjectUtil.checkNotNull( callable, "callable" );
        ObjectUtil.checkNotNull( unit, "unit" );
        if ( delay < 0L )
        {
            delay = 0L;
        }

        this.validateScheduled0( delay, unit );
        return this.schedule( new ScheduledFutureTask( this, callable, ScheduledFutureTask.deadlineNanos( unit.toNanos( delay ) ) ) );
    }

    public ScheduledFuture<?> scheduleAtFixedRate( Runnable command, long initialDelay, long period, TimeUnit unit )
    {
        ObjectUtil.checkNotNull( command, "command" );
        ObjectUtil.checkNotNull( unit, "unit" );
        if ( initialDelay < 0L )
        {
            throw new IllegalArgumentException( String.format( "initialDelay: %d (expected: >= 0)", initialDelay ) );
        }
        else if ( period <= 0L )
        {
            throw new IllegalArgumentException( String.format( "period: %d (expected: > 0)", period ) );
        }
        else
        {
            this.validateScheduled0( initialDelay, unit );
            this.validateScheduled0( period, unit );
            return this.schedule( new ScheduledFutureTask( this, Executors.callable( command, (Object) null ),
                    ScheduledFutureTask.deadlineNanos( unit.toNanos( initialDelay ) ), unit.toNanos( period ) ) );
        }
    }

    public ScheduledFuture<?> scheduleWithFixedDelay( Runnable command, long initialDelay, long delay, TimeUnit unit )
    {
        ObjectUtil.checkNotNull( command, "command" );
        ObjectUtil.checkNotNull( unit, "unit" );
        if ( initialDelay < 0L )
        {
            throw new IllegalArgumentException( String.format( "initialDelay: %d (expected: >= 0)", initialDelay ) );
        }
        else if ( delay <= 0L )
        {
            throw new IllegalArgumentException( String.format( "delay: %d (expected: > 0)", delay ) );
        }
        else
        {
            this.validateScheduled0( initialDelay, unit );
            this.validateScheduled0( delay, unit );
            return this.schedule( new ScheduledFutureTask( this, Executors.callable( command, (Object) null ),
                    ScheduledFutureTask.deadlineNanos( unit.toNanos( initialDelay ) ), -unit.toNanos( delay ) ) );
        }
    }

    private void validateScheduled0( long amount, TimeUnit unit )
    {
        this.validateScheduled( amount, unit );
    }

    /**
     * @deprecated
     */
    @Deprecated
    protected void validateScheduled( long amount, TimeUnit unit )
    {
    }

    private <V> ScheduledFuture<V> schedule( final ScheduledFutureTask<V> task )
    {
        if ( this.inEventLoop() )
        {
            this.scheduledTaskQueue().add( task );
        }
        else
        {
            this.executeScheduledRunnable( new Runnable()
            {
                public void run()
                {
                    AbstractScheduledEventExecutor.this.scheduledTaskQueue().add( task );
                }
            }, true, task.deadlineNanos() );
        }

        return task;
    }

    final void removeScheduled( final ScheduledFutureTask<?> task )
    {
        if ( this.inEventLoop() )
        {
            this.scheduledTaskQueue().removeTyped( task );
        }
        else
        {
            this.executeScheduledRunnable( new Runnable()
            {
                public void run()
                {
                    AbstractScheduledEventExecutor.this.scheduledTaskQueue().removeTyped( task );
                }
            }, false, task.deadlineNanos() );
        }
    }

    void executeScheduledRunnable( Runnable runnable, boolean isAddition, long deadlineNanos )
    {
        this.execute( runnable );
    }
}
