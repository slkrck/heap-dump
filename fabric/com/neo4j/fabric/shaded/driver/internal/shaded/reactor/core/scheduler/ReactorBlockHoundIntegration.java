package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.blockhound.BlockHound.Builder;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.blockhound.integration.BlockHoundIntegration;

import java.util.concurrent.ScheduledThreadPoolExecutor;

public final class ReactorBlockHoundIntegration implements BlockHoundIntegration
{
    public void applyTo( Builder builder )
    {
        builder.nonBlockingThreadPredicate( ( current ) -> {
            NonBlocking.class.getClass();
            return current.or( NonBlocking.class::isInstance );
        } );
        builder.allowBlockingCallsInside( "java.util.concurrent.ScheduledThreadPoolExecutor$DelayedWorkQueue", "offer" );
        builder.allowBlockingCallsInside( ScheduledThreadPoolExecutor.class.getName() + "$DelayedWorkQueue", "take" );
        Schedulers.onScheduleHook( "BlockHound", ReactorBlockHoundIntegration.Wrapper::new );
        builder.disallowBlockingCallsInside( ReactorBlockHoundIntegration.Wrapper.class.getName(), "run" );
    }

    static final class Wrapper implements Runnable
    {
        final Runnable delegate;

        Wrapper( Runnable delegate )
        {
            this.delegate = delegate;
        }

        public void run()
        {
            this.delegate.run();
        }
    }
}
