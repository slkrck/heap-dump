package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

public interface RejectedExecutionHandler
{
    void rejected( Runnable var1, SingleThreadEventExecutor var2 );
}
