package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxPublishMulticast<T, R> extends InternalFluxOperator<T,R> implements Fuseable
{
    final Function<? super Flux<T>,? extends Publisher<? extends R>> transform;
    final Supplier<? extends Queue<T>> queueSupplier;
    final int prefetch;

    FluxPublishMulticast( Flux<? extends T> source, Function<? super Flux<T>,? extends Publisher<? extends R>> transform, int prefetch,
            Supplier<? extends Queue<T>> queueSupplier )
    {
        super( source );
        if ( prefetch < 1 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.prefetch = prefetch;
            this.transform = (Function) Objects.requireNonNull( transform, "transform" );
            this.queueSupplier = (Supplier) Objects.requireNonNull( queueSupplier, "queueSupplier" );
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        FluxPublishMulticast.FluxPublishMulticaster multicast =
                new FluxPublishMulticast.FluxPublishMulticaster( this.prefetch, this.queueSupplier, actual.currentContext() );

        Publisher out;
        try
        {
            out = (Publisher) Objects.requireNonNull( this.transform.apply( multicast ), "The transform returned a null Publisher" );
        }
        catch ( Throwable var5 )
        {
            Operators.error( actual, Operators.onOperatorError( var5, actual.currentContext() ) );
            return null;
        }

        if ( out instanceof Fuseable )
        {
            out.subscribe( new FluxPublishMulticast.CancelFuseableMulticaster( actual, multicast ) );
        }
        else
        {
            out.subscribe( new FluxPublishMulticast.CancelMulticaster( actual, multicast ) );
        }

        return multicast;
    }

    interface PublishMulticasterParent
    {
        void terminate();
    }

    static final class CancelFuseableMulticaster<T> implements InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        final CoreSubscriber<? super T> actual;
        final FluxPublishMulticast.PublishMulticasterParent parent;
        Fuseable.QueueSubscription<T> s;

        CancelFuseableMulticaster( CoreSubscriber<? super T> actual, FluxPublishMulticast.PublishMulticasterParent parent )
        {
            this.actual = actual;
            this.parent = parent;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
            this.parent.terminate();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = Operators.as( s );
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
            this.parent.terminate();
        }

        public void onComplete()
        {
            this.actual.onComplete();
            this.parent.terminate();
        }

        public int requestFusion( int requestedMode )
        {
            return this.s.requestFusion( requestedMode );
        }

        @Nullable
        public T poll()
        {
            return this.s.poll();
        }

        public boolean isEmpty()
        {
            return this.s.isEmpty();
        }

        public int size()
        {
            return this.s.size();
        }

        public void clear()
        {
            this.s.clear();
        }
    }

    static final class CancelMulticaster<T> implements InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        final CoreSubscriber<? super T> actual;
        final FluxPublishMulticast.PublishMulticasterParent parent;
        Subscription s;

        CancelMulticaster( CoreSubscriber<? super T> actual, FluxPublishMulticast.PublishMulticasterParent parent )
        {
            this.actual = actual;
            this.parent = parent;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
            this.parent.terminate();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
            this.parent.terminate();
        }

        public void onComplete()
        {
            this.actual.onComplete();
            this.parent.terminate();
        }

        public int requestFusion( int requestedMode )
        {
            return 0;
        }

        public void clear()
        {
        }

        public boolean isEmpty()
        {
            return false;
        }

        public int size()
        {
            return 0;
        }

        @Nullable
        public T poll()
        {
            return null;
        }
    }

    static final class PublishMulticastInner<T> implements InnerProducer<T>
    {
        static final AtomicLongFieldUpdater<FluxPublishMulticast.PublishMulticastInner> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxPublishMulticast.PublishMulticastInner.class, "requested" );
        final FluxPublishMulticast.FluxPublishMulticaster<T> parent;
        final CoreSubscriber<? super T> actual;
        volatile long requested;

        PublishMulticastInner( FluxPublishMulticast.FluxPublishMulticaster<T> parent, CoreSubscriber<? super T> actual )
        {
            this.parent = parent;
            this.actual = actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return Math.max( 0L, this.requested );
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? Long.MIN_VALUE == this.requested : InnerProducer.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCapCancellable( REQUESTED, this, n );
                this.parent.drain();
            }
        }

        public void cancel()
        {
            if ( REQUESTED.getAndSet( this, Long.MIN_VALUE ) != Long.MIN_VALUE )
            {
                this.parent.remove( this );
                this.parent.drain();
            }
        }

        void produced( long n )
        {
            Operators.producedCancellable( REQUESTED, this, n );
        }
    }

    static final class FluxPublishMulticaster<T> extends Flux<T> implements InnerConsumer<T>, FluxPublishMulticast.PublishMulticasterParent
    {
        static final AtomicReferenceFieldUpdater<FluxPublishMulticast.FluxPublishMulticaster,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxPublishMulticast.FluxPublishMulticaster.class, Subscription.class, "s" );
        static final AtomicIntegerFieldUpdater<FluxPublishMulticast.FluxPublishMulticaster> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxPublishMulticast.FluxPublishMulticaster.class, "wip" );
        static final AtomicReferenceFieldUpdater<FluxPublishMulticast.FluxPublishMulticaster,FluxPublishMulticast.PublishMulticastInner[]> SUBSCRIBERS =
                AtomicReferenceFieldUpdater.newUpdater( FluxPublishMulticast.FluxPublishMulticaster.class, FluxPublishMulticast.PublishMulticastInner[].class,
                        "subscribers" );
        static final FluxPublishMulticast.PublishMulticastInner[] EMPTY = new FluxPublishMulticast.PublishMulticastInner[0];
        static final FluxPublishMulticast.PublishMulticastInner[] TERMINATED = new FluxPublishMulticast.PublishMulticastInner[0];
        final int limit;
        final int prefetch;
        final Supplier<? extends Queue<T>> queueSupplier;
        final Context context;
        Queue<T> queue;
        volatile Subscription s;
        volatile int wip;
        volatile FluxPublishMulticast.PublishMulticastInner<T>[] subscribers;
        volatile boolean done;
        volatile boolean connected;
        Throwable error;
        int produced;
        int sourceMode;

        FluxPublishMulticaster( int prefetch, Supplier<? extends Queue<T>> queueSupplier, Context ctx )
        {
            this.prefetch = prefetch;
            this.limit = Operators.unboundedOrLimit( prefetch );
            this.queueSupplier = queueSupplier;
            SUBSCRIBERS.lazySet( this, EMPTY );
            this.context = ctx;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.queue != null ? this.queue.size() : 0 : null;
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        public Context currentContext()
        {
            return this.context;
        }

        public void subscribe( CoreSubscriber<? super T> actual )
        {
            FluxPublishMulticast.PublishMulticastInner<T> pcs = new FluxPublishMulticast.PublishMulticastInner( this, actual );
            actual.onSubscribe( pcs );
            if ( this.add( pcs ) )
            {
                if ( pcs.requested == Long.MIN_VALUE )
                {
                    this.remove( pcs );
                    return;
                }

                this.drain();
            }
            else
            {
                Throwable ex = this.error;
                if ( ex != null )
                {
                    actual.onError( ex );
                }
                else
                {
                    actual.onComplete();
                }
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                if ( s instanceof Fuseable.QueueSubscription )
                {
                    Fuseable.QueueSubscription<T> qs = (Fuseable.QueueSubscription) s;
                    int m = qs.requestFusion( 3 );
                    if ( m == 1 )
                    {
                        this.sourceMode = m;
                        this.queue = qs;
                        this.done = true;
                        this.connected = true;
                        this.drain();
                        return;
                    }

                    if ( m == 2 )
                    {
                        this.sourceMode = m;
                        this.queue = qs;
                        this.connected = true;
                        s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
                        return;
                    }
                }

                this.queue = (Queue) this.queueSupplier.get();
                this.connected = true;
                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.context );
            }
            else if ( this.sourceMode != 2 && !this.queue.offer( t ) )
            {
                this.onError(
                        Operators.onOperatorError( this.s, Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ),
                                t, this.context ) );
            }
            else
            {
                this.drain();
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.context );
            }
            else
            {
                this.error = t;
                this.done = true;
                this.drain();
            }
        }

        public void onComplete()
        {
            this.done = true;
            this.drain();
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                if ( this.sourceMode == 1 )
                {
                    this.drainSync();
                }
                else
                {
                    this.drainAsync();
                }
            }
        }

        void drainSync()
        {
            int missed = 1;

            do
            {
                if ( this.connected )
                {
                    if ( this.s == Operators.cancelledSubscription() )
                    {
                        this.queue.clear();
                        return;
                    }

                    Queue<T> queue = this.queue;
                    FluxPublishMulticast.PublishMulticastInner<T>[] a = this.subscribers;
                    int n = a.length;
                    if ( n != 0 )
                    {
                        long r = Long.MAX_VALUE;

                        for ( int i = 0; i < n; ++i )
                        {
                            long u = a[i].requested;
                            if ( u != Long.MIN_VALUE )
                            {
                                r = Math.min( r, u );
                            }
                        }

                        long e = 0L;

                        label101:
                        while ( true )
                        {
                            if ( e == r )
                            {
                                if ( this.s == Operators.cancelledSubscription() )
                                {
                                    queue.clear();
                                    return;
                                }

                                int i;
                                if ( queue.isEmpty() )
                                {
                                    a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                    n = a.length;

                                    for ( i = 0; i < n; ++i )
                                    {
                                        a[i].actual.onComplete();
                                    }

                                    return;
                                }

                                if ( e == 0L )
                                {
                                    break;
                                }

                                i = 0;

                                while ( true )
                                {
                                    if ( i >= n )
                                    {
                                        break label101;
                                    }

                                    a[i].produced( e );
                                    ++i;
                                }
                            }

                            if ( this.s == Operators.cancelledSubscription() )
                            {
                                queue.clear();
                                return;
                            }

                            Object v;
                            try
                            {
                                v = queue.poll();
                            }
                            catch ( Throwable var14 )
                            {
                                Throwable ex = var14;
                                this.error = Operators.onOperatorError( this.s, var14, this.context );
                                queue.clear();
                                a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                n = a.length;

                                for ( int i = 0; i < n; ++i )
                                {
                                    a[i].actual.onError( ex );
                                }

                                return;
                            }

                            int i;
                            if ( v == null )
                            {
                                a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                n = a.length;

                                for ( i = 0; i < n; ++i )
                                {
                                    a[i].actual.onComplete();
                                }

                                return;
                            }

                            for ( i = 0; i < n; ++i )
                            {
                                a[i].actual.onNext( v );
                            }

                            ++e;
                        }
                    }
                }

                missed = WIP.addAndGet( this, -missed );
            }
            while ( missed != 0 );
        }

        void drainAsync()
        {
            int missed = 1;
            int p = this.produced;

            do
            {
                if ( this.connected )
                {
                    if ( this.s == Operators.cancelledSubscription() )
                    {
                        this.queue.clear();
                        return;
                    }

                    Queue<T> queue = this.queue;
                    FluxPublishMulticast.PublishMulticastInner<T>[] a = this.subscribers;
                    int n = a.length;
                    if ( n != 0 )
                    {
                        long r = Long.MAX_VALUE;

                        for ( int i = 0; i < n; ++i )
                        {
                            long u = a[i].requested;
                            if ( u != Long.MIN_VALUE )
                            {
                                r = Math.min( r, u );
                            }
                        }

                        long e = 0L;

                        boolean d;
                        while ( e != r )
                        {
                            if ( this.s == Operators.cancelledSubscription() )
                            {
                                queue.clear();
                                return;
                            }

                            d = this.done;

                            Object v;
                            int i;
                            try
                            {
                                v = queue.poll();
                            }
                            catch ( Throwable var17 )
                            {
                                Throwable ex = var17;
                                queue.clear();
                                this.error = Operators.onOperatorError( this.s, var17, this.context );
                                a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                n = a.length;

                                for ( i = 0; i < n; ++i )
                                {
                                    a[i].actual.onError( ex );
                                }

                                return;
                            }

                            boolean empty = v == null;
                            if ( d )
                            {
                                Throwable ex = this.error;
                                int i;
                                if ( ex != null )
                                {
                                    queue.clear();
                                    a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                    n = a.length;

                                    for ( i = 0; i < n; ++i )
                                    {
                                        a[i].actual.onError( ex );
                                    }

                                    return;
                                }

                                if ( empty )
                                {
                                    a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                    n = a.length;

                                    for ( i = 0; i < n; ++i )
                                    {
                                        a[i].actual.onComplete();
                                    }

                                    return;
                                }
                            }

                            if ( empty )
                            {
                                break;
                            }

                            for ( i = 0; i < n; ++i )
                            {
                                a[i].actual.onNext( v );
                            }

                            ++e;
                            ++p;
                            if ( p == this.limit )
                            {
                                this.s.request( (long) p );
                                p = 0;
                            }
                        }

                        if ( e == r )
                        {
                            if ( this.s == Operators.cancelledSubscription() )
                            {
                                queue.clear();
                                return;
                            }

                            d = this.done;
                            if ( d )
                            {
                                Throwable ex = this.error;
                                int i;
                                if ( ex != null )
                                {
                                    queue.clear();
                                    a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                    n = a.length;

                                    for ( i = 0; i < n; ++i )
                                    {
                                        a[i].actual.onError( ex );
                                    }

                                    return;
                                }

                                if ( queue.isEmpty() )
                                {
                                    a = (FluxPublishMulticast.PublishMulticastInner[]) SUBSCRIBERS.getAndSet( this, TERMINATED );
                                    n = a.length;

                                    for ( i = 0; i < n; ++i )
                                    {
                                        a[i].actual.onComplete();
                                    }

                                    return;
                                }
                            }
                        }

                        if ( e != 0L )
                        {
                            for ( int i = 0; i < n; ++i )
                            {
                                a[i].produced( e );
                            }
                        }
                    }
                }

                this.produced = p;
                missed = WIP.addAndGet( this, -missed );
            }
            while ( missed != 0 );
        }

        boolean add( FluxPublishMulticast.PublishMulticastInner<T> s )
        {
            FluxPublishMulticast.PublishMulticastInner[] a;
            FluxPublishMulticast.PublishMulticastInner[] b;
            do
            {
                a = this.subscribers;
                if ( a == TERMINATED )
                {
                    return false;
                }

                int n = a.length;
                b = new FluxPublishMulticast.PublishMulticastInner[n + 1];
                System.arraycopy( a, 0, b, 0, n );
                b[n] = s;
            }
            while ( !SUBSCRIBERS.compareAndSet( this, a, b ) );

            return true;
        }

        void remove( FluxPublishMulticast.PublishMulticastInner<T> s )
        {
            while ( true )
            {
                FluxPublishMulticast.PublishMulticastInner<T>[] a = this.subscribers;
                if ( a != TERMINATED && a != EMPTY )
                {
                    int n = a.length;
                    int j = -1;

                    for ( int i = 0; i < n; ++i )
                    {
                        if ( a[i] == s )
                        {
                            j = i;
                            break;
                        }
                    }

                    if ( j < 0 )
                    {
                        return;
                    }

                    FluxPublishMulticast.PublishMulticastInner[] b;
                    if ( n == 1 )
                    {
                        b = EMPTY;
                    }
                    else
                    {
                        b = new FluxPublishMulticast.PublishMulticastInner[n - 1];
                        System.arraycopy( a, 0, b, 0, j );
                        System.arraycopy( a, j + 1, b, j, n - j - 1 );
                    }

                    if ( !SUBSCRIBERS.compareAndSet( this, a, b ) )
                    {
                        continue;
                    }

                    return;
                }

                return;
            }
        }

        public void terminate()
        {
            Operators.terminate( S, this );
            if ( WIP.getAndIncrement( this ) == 0 && this.connected )
            {
                this.queue.clear();
            }
        }
    }
}
