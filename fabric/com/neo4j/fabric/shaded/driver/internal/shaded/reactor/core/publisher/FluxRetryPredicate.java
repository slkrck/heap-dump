package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.function.Predicate;

final class FluxRetryPredicate<T> extends InternalFluxOperator<T,T>
{
    final Predicate<? super Throwable> predicate;

    FluxRetryPredicate( Flux<? extends T> source, Predicate<? super Throwable> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxRetryPredicate.RetryPredicateSubscriber<T> parent = new FluxRetryPredicate.RetryPredicateSubscriber( this.source, actual, this.predicate );
        actual.onSubscribe( parent );
        if ( !parent.isCancelled() )
        {
            parent.resubscribe();
        }

        return null;
    }

    static final class RetryPredicateSubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxRetryPredicate.RetryPredicateSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxRetryPredicate.RetryPredicateSubscriber.class, "wip" );
        final CorePublisher<? extends T> source;
        final Predicate<? super Throwable> predicate;
        volatile int wip;
        long produced;

        RetryPredicateSubscriber( CorePublisher<? extends T> source, CoreSubscriber<? super T> actual, Predicate<? super Throwable> predicate )
        {
            super( actual );
            this.source = source;
            this.predicate = predicate;
        }

        public void onNext( T t )
        {
            ++this.produced;
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            boolean b;
            try
            {
                b = this.predicate.test( t );
            }
            catch ( Throwable var5 )
            {
                Throwable _t = Operators.onOperatorError( var5, this.actual.currentContext() );
                _t = Exceptions.addSuppressed( _t, t );
                this.actual.onError( _t );
                return;
            }

            if ( b )
            {
                this.resubscribe();
            }
            else
            {
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        void resubscribe()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                do
                {
                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    long c = this.produced;
                    if ( c != 0L )
                    {
                        this.produced = 0L;
                        this.produced( c );
                    }

                    this.source.subscribe( this );
                }
                while ( WIP.decrementAndGet( this ) != 0 );
            }
        }
    }
}
