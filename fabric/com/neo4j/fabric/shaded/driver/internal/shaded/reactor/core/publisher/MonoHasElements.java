package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.reactivestreams.Subscription;

final class MonoHasElements<T> extends MonoFromFluxOperator<T,Boolean> implements Fuseable
{
    MonoHasElements( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Boolean> actual )
    {
        return new MonoHasElements.HasElementsSubscriber( actual );
    }

    static final class HasElementsSubscriber<T> extends Operators.MonoSubscriber<T,Boolean>
    {
        Subscription s;

        HasElementsSubscriber( CoreSubscriber<? super Boolean> actual )
        {
            super( actual );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            this.s.cancel();
            this.complete( true );
        }

        public void onComplete()
        {
            this.complete( false );
        }
    }
}
