package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.reactivestreams.Processor;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxWindow<T> extends InternalFluxOperator<T,Flux<T>>
{
    final int size;
    final int skip;
    final Supplier<? extends Queue<T>> processorQueueSupplier;
    final Supplier<? extends Queue<UnicastProcessor<T>>> overflowQueueSupplier;

    FluxWindow( Flux<? extends T> source, int size, Supplier<? extends Queue<T>> processorQueueSupplier )
    {
        super( source );
        if ( size <= 0 )
        {
            throw new IllegalArgumentException( "size > 0 required but it was " + size );
        }
        else
        {
            this.size = size;
            this.skip = size;
            this.processorQueueSupplier = (Supplier) Objects.requireNonNull( processorQueueSupplier, "processorQueueSupplier" );
            this.overflowQueueSupplier = null;
        }
    }

    FluxWindow( Flux<? extends T> source, int size, int skip, Supplier<? extends Queue<T>> processorQueueSupplier,
            Supplier<? extends Queue<UnicastProcessor<T>>> overflowQueueSupplier )
    {
        super( source );
        if ( size <= 0 )
        {
            throw new IllegalArgumentException( "size > 0 required but it was " + size );
        }
        else if ( skip <= 0 )
        {
            throw new IllegalArgumentException( "skip > 0 required but it was " + skip );
        }
        else
        {
            this.size = size;
            this.skip = skip;
            this.processorQueueSupplier = (Supplier) Objects.requireNonNull( processorQueueSupplier, "processorQueueSupplier" );
            this.overflowQueueSupplier = (Supplier) Objects.requireNonNull( overflowQueueSupplier, "overflowQueueSupplier" );
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super Flux<T>> actual )
    {
        if ( this.skip == this.size )
        {
            return new FluxWindow.WindowExactSubscriber( actual, this.size, this.processorQueueSupplier );
        }
        else
        {
            return (CoreSubscriber) (this.skip > this.size ? new FluxWindow.WindowSkipSubscriber( actual, this.size, this.skip, this.processorQueueSupplier )
                                                           : new FluxWindow.WindowOverlapSubscriber( actual, this.size, this.skip, this.processorQueueSupplier,
                                                                   (Queue) this.overflowQueueSupplier.get() ));
        }
    }

    static final class WindowOverlapSubscriber<T> extends ArrayDeque<UnicastProcessor<T>> implements Disposable, InnerOperator<T,Flux<T>>
    {
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowOverlapSubscriber> CANCELLED =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowOverlapSubscriber.class, "cancelled" );
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowOverlapSubscriber> WINDOW_COUNT =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowOverlapSubscriber.class, "windowCount" );
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowOverlapSubscriber> FIRST_REQUEST =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowOverlapSubscriber.class, "firstRequest" );
        static final AtomicLongFieldUpdater<FluxWindow.WindowOverlapSubscriber> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxWindow.WindowOverlapSubscriber.class, "requested" );
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowOverlapSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowOverlapSubscriber.class, "wip" );
        final CoreSubscriber<? super Flux<T>> actual;
        final Supplier<? extends Queue<T>> processorQueueSupplier;
        final Queue<UnicastProcessor<T>> queue;
        final int size;
        final int skip;
        volatile int cancelled;
        volatile int windowCount;
        volatile int firstRequest;
        volatile long requested;
        volatile int wip;
        int index;
        int produced;
        Subscription s;
        volatile boolean done;
        Throwable error;

        WindowOverlapSubscriber( CoreSubscriber<? super Flux<T>> actual, int size, int skip, Supplier<? extends Queue<T>> processorQueueSupplier,
                Queue<UnicastProcessor<T>> overflowQueue )
        {
            this.actual = actual;
            this.size = size;
            this.skip = skip;
            this.processorQueueSupplier = processorQueueSupplier;
            WINDOW_COUNT.lazySet( this, 1 );
            this.queue = overflowQueue;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                int i = this.index;
                if ( i == 0 && this.cancelled == 0 )
                {
                    WINDOW_COUNT.getAndIncrement( this );
                    UnicastProcessor<T> w = new UnicastProcessor( (Queue) this.processorQueueSupplier.get(), this );
                    this.offer( w );
                    this.queue.offer( w );
                    this.drain();
                }

                ++i;
                Iterator var5 = this.iterator();

                Processor w;
                while ( var5.hasNext() )
                {
                    w = (Processor) var5.next();
                    w.onNext( t );
                }

                int p = this.produced + 1;
                if ( p == this.size )
                {
                    this.produced = p - this.skip;
                    w = (Processor) this.poll();
                    if ( w != null )
                    {
                        w.onComplete();
                    }
                }
                else
                {
                    this.produced = p;
                }

                if ( i == this.skip )
                {
                    this.index = 0;
                }
                else
                {
                    this.index = i;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                Iterator var2 = this.iterator();

                while ( var2.hasNext() )
                {
                    Processor<T,T> w = (Processor) var2.next();
                    w.onError( t );
                }

                this.clear();
                this.error = t;
                this.drain();
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                Iterator var1 = this.iterator();

                while ( var1.hasNext() )
                {
                    Processor<T,T> w = (Processor) var1.next();
                    w.onComplete();
                }

                this.clear();
                this.drain();
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                Subscriber<? super Flux<T>> a = this.actual;
                Queue<UnicastProcessor<T>> q = this.queue;
                int missed = 1;

                do
                {
                    long r = this.requested;

                    long e;
                    for ( e = 0L; e != r; ++e )
                    {
                        boolean d = this.done;
                        UnicastProcessor<T> t = (UnicastProcessor) q.poll();
                        boolean empty = t == null;
                        if ( this.checkTerminated( d, empty, a, q ) )
                        {
                            return;
                        }

                        if ( empty )
                        {
                            break;
                        }

                        a.onNext( t );
                    }

                    if ( e == r && this.checkTerminated( this.done, q.isEmpty(), a, q ) )
                    {
                        return;
                    }

                    if ( e != 0L && r != Long.MAX_VALUE )
                    {
                        REQUESTED.addAndGet( this, -e );
                    }

                    missed = WIP.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }

        boolean checkTerminated( boolean d, boolean empty, Subscriber<?> a, Queue<?> q )
        {
            if ( this.cancelled == 1 )
            {
                q.clear();
                return true;
            }
            else
            {
                if ( d )
                {
                    Throwable e = this.error;
                    if ( e != null )
                    {
                        q.clear();
                        a.onError( e );
                        return true;
                    }

                    if ( empty )
                    {
                        a.onComplete();
                        return true;
                    }
                }

                return false;
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                long u;
                if ( this.firstRequest == 0 && FIRST_REQUEST.compareAndSet( this, 0, 1 ) )
                {
                    u = Operators.multiplyCap( (long) this.skip, n - 1L );
                    long v = Operators.addCap( (long) this.size, u );
                    this.s.request( v );
                }
                else
                {
                    u = Operators.multiplyCap( (long) this.skip, n );
                    this.s.request( u );
                }

                this.drain();
            }
        }

        public void cancel()
        {
            if ( CANCELLED.compareAndSet( this, 0, 1 ) )
            {
                this.dispose();
            }
        }

        public void dispose()
        {
            if ( WINDOW_COUNT.decrementAndGet( this ) == 0 )
            {
                this.s.cancel();
            }
        }

        public CoreSubscriber<? super Flux<T>> actual()
        {
            return this.actual;
        }

        public boolean isDisposed()
        {
            return this.cancelled == 1 || this.done;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled == 1;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.size;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.LARGE_BUFFERED )
            {
                return (long) this.queue.size() + (long) this.size();
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                long realBuffered = (long) this.queue.size() + (long) this.size();
                return realBuffered < 2147483647L ? (int) realBuffered : Integer.MIN_VALUE;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.requested : InnerOperator.super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.toArray() ).map( Scannable::from );
        }
    }

    static final class WindowSkipSubscriber<T> implements Disposable, InnerOperator<T,Flux<T>>
    {
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowSkipSubscriber> CANCELLED =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowSkipSubscriber.class, "cancelled" );
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowSkipSubscriber> WINDOW_COUNT =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowSkipSubscriber.class, "windowCount" );
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowSkipSubscriber> FIRST_REQUEST =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowSkipSubscriber.class, "firstRequest" );
        final CoreSubscriber<? super Flux<T>> actual;
        final Context ctx;
        final Supplier<? extends Queue<T>> processorQueueSupplier;
        final int size;
        final int skip;
        volatile int cancelled;
        volatile int windowCount;
        volatile int firstRequest;
        int index;
        Subscription s;
        UnicastProcessor<T> window;
        boolean done;

        WindowSkipSubscriber( CoreSubscriber<? super Flux<T>> actual, int size, int skip, Supplier<? extends Queue<T>> processorQueueSupplier )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.size = size;
            this.skip = skip;
            this.processorQueueSupplier = processorQueueSupplier;
            WINDOW_COUNT.lazySet( this, 1 );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
            }
            else
            {
                int i = this.index;
                UnicastProcessor<T> w = this.window;
                if ( i == 0 )
                {
                    WINDOW_COUNT.getAndIncrement( this );
                    w = new UnicastProcessor( (Queue) this.processorQueueSupplier.get(), this );
                    this.window = w;
                    this.actual.onNext( w );
                }

                ++i;
                if ( w != null )
                {
                    w.onNext( t );
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                }

                if ( i == this.size )
                {
                    this.window = null;
                    if ( w != null )
                    {
                        w.onComplete();
                    }
                }

                if ( i == this.skip )
                {
                    this.index = 0;
                }
                else
                {
                    this.index = i;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                Processor<T,T> w = this.window;
                if ( w != null )
                {
                    this.window = null;
                    w.onError( t );
                }

                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                Processor<T,T> w = this.window;
                if ( w != null )
                {
                    this.window = null;
                    w.onComplete();
                }

                this.actual.onComplete();
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                long u;
                if ( this.firstRequest == 0 && FIRST_REQUEST.compareAndSet( this, 0, 1 ) )
                {
                    u = Operators.multiplyCap( (long) this.size, n );
                    long v = Operators.multiplyCap( (long) (this.skip - this.size), n - 1L );
                    long w = Operators.addCap( u, v );
                    this.s.request( w );
                }
                else
                {
                    u = Operators.multiplyCap( (long) this.skip, n );
                    this.s.request( u );
                }
            }
        }

        public void cancel()
        {
            if ( CANCELLED.compareAndSet( this, 0, 1 ) )
            {
                this.dispose();
            }
        }

        public boolean isDisposed()
        {
            return this.cancelled == 1 || this.done;
        }

        public void dispose()
        {
            if ( WINDOW_COUNT.decrementAndGet( this ) == 0 )
            {
                this.s.cancel();
            }
        }

        public CoreSubscriber<? super Flux<T>> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled == 1;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.size;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.window );
        }
    }

    static final class WindowExactSubscriber<T> implements Disposable, InnerOperator<T,Flux<T>>
    {
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowExactSubscriber> CANCELLED =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowExactSubscriber.class, "cancelled" );
        static final AtomicIntegerFieldUpdater<FluxWindow.WindowExactSubscriber> WINDOW_COUNT =
                AtomicIntegerFieldUpdater.newUpdater( FluxWindow.WindowExactSubscriber.class, "windowCount" );
        final CoreSubscriber<? super Flux<T>> actual;
        final Supplier<? extends Queue<T>> processorQueueSupplier;
        final int size;
        volatile int cancelled;
        volatile int windowCount;
        int index;
        Subscription s;
        UnicastProcessor<T> window;
        boolean done;

        WindowExactSubscriber( CoreSubscriber<? super Flux<T>> actual, int size, Supplier<? extends Queue<T>> processorQueueSupplier )
        {
            this.actual = actual;
            this.size = size;
            this.processorQueueSupplier = processorQueueSupplier;
            WINDOW_COUNT.lazySet( this, 1 );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                int i = this.index;
                UnicastProcessor<T> w = this.window;
                if ( this.cancelled == 0 && i == 0 )
                {
                    WINDOW_COUNT.getAndIncrement( this );
                    w = new UnicastProcessor( (Queue) this.processorQueueSupplier.get(), this );
                    this.window = w;
                    this.actual.onNext( w );
                }

                ++i;
                w.onNext( t );
                if ( i == this.size )
                {
                    this.index = 0;
                    this.window = null;
                    w.onComplete();
                }
                else
                {
                    this.index = i;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                Processor<T,T> w = this.window;
                if ( w != null )
                {
                    this.window = null;
                    w.onError( t );
                }

                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                Processor<T,T> w = this.window;
                if ( w != null )
                {
                    this.window = null;
                    w.onComplete();
                }

                this.actual.onComplete();
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                long u = Operators.multiplyCap( (long) this.size, n );
                this.s.request( u );
            }
        }

        public void cancel()
        {
            if ( CANCELLED.compareAndSet( this, 0, 1 ) )
            {
                this.dispose();
            }
        }

        public void dispose()
        {
            if ( WINDOW_COUNT.decrementAndGet( this ) == 0 )
            {
                this.s.cancel();
            }
        }

        public CoreSubscriber<? super Flux<T>> actual()
        {
            return this.actual;
        }

        public boolean isDisposed()
        {
            return this.cancelled == 1 || this.done;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled == 1;
            }
            else if ( key == Scannable.Attr.CAPACITY )
            {
                return this.size;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.window );
        }
    }
}
