package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

final class MonoSupplier<T> extends Mono<T> implements Callable<T>, Fuseable, SourceProducer<T>
{
    final Supplier<? extends T> supplier;

    MonoSupplier( Supplier<? extends T> callable )
    {
        this.supplier = (Supplier) Objects.requireNonNull( callable, "callable" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Operators.MonoSubscriber<T,T> sds = new Operators.MonoSubscriber( actual );
        actual.onSubscribe( sds );
        if ( !sds.isCancelled() )
        {
            try
            {
                T t = this.supplier.get();
                if ( t == null )
                {
                    sds.onComplete();
                }
                else
                {
                    sds.complete( t );
                }
            }
            catch ( Throwable var4 )
            {
                actual.onError( Operators.onOperatorError( var4, actual.currentContext() ) );
            }
        }
    }

    @Nullable
    public T block( Duration m )
    {
        return this.supplier.get();
    }

    @Nullable
    public T block()
    {
        return this.block( Duration.ZERO );
    }

    @Nullable
    public T call() throws Exception
    {
        return this.supplier.get();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
