package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import org.reactivestreams.Publisher;

final class ParallelArraySource<T> extends ParallelFlux<T> implements SourceProducer<T>
{
    final Publisher<T>[] sources;

    ParallelArraySource( Publisher<T>[] sources )
    {
        if ( sources != null && sources.length != 0 )
        {
            this.sources = sources;
        }
        else
        {
            throw new IllegalArgumentException( "Zero publishers not supported" );
        }
    }

    public int parallelism()
    {
        return this.sources.length;
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;

            for ( int i = 0; i < n; ++i )
            {
                Flux.from( this.sources[i] ).subscribe( subscribers[i] );
            }
        }
    }
}
