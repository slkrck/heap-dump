package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Logger;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.Loggers;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

final class MonoCompletionStage<T> extends Mono<T> implements Fuseable, Scannable
{
    static final Logger LOGGER = Loggers.getLogger( MonoCompletionStage.class );
    final CompletionStage<? extends T> future;

    MonoCompletionStage( CompletionStage<? extends T> future )
    {
        this.future = (CompletionStage) Objects.requireNonNull( future, "future" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Operators.MonoSubscriber<T,T> sds = new Operators.MonoSubscriber( actual );
        actual.onSubscribe( sds );
        if ( !sds.isCancelled() )
        {
            this.future.whenComplete( ( v, e ) -> {
                if ( !sds.isCancelled() )
                {
                    try
                    {
                        if ( e instanceof CompletionException )
                        {
                            actual.onError( e.getCause() );
                        }
                        else if ( e != null )
                        {
                            actual.onError( e );
                        }
                        else if ( v != null )
                        {
                            sds.complete( v );
                        }
                        else
                        {
                            actual.onComplete();
                        }
                    }
                    catch ( Throwable var5 )
                    {
                        Operators.onErrorDropped( var5, actual.currentContext() );
                        throw Exceptions.bubble( var5 );
                    }
                }
                else
                {
                    Context ctx = sds.currentContext();
                    if ( e != null && !(e instanceof CancellationException) )
                    {
                        Operators.onErrorDropped( e, ctx );
                        Operators.onDiscard( v, ctx );
                    }
                    else
                    {
                        Operators.onDiscard( v, ctx );
                    }
                }
            } );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
