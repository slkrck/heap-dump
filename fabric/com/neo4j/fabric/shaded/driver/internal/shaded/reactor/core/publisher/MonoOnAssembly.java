package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

final class MonoOnAssembly<T> extends InternalMonoOperator<T,T> implements Fuseable, AssemblyOp
{
    final FluxOnAssembly.AssemblySnapshot stacktrace;

    MonoOnAssembly( Mono<? extends T> source, FluxOnAssembly.AssemblySnapshot stacktrace )
    {
        super( source );
        this.stacktrace = stacktrace;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<? super T> cs = (Fuseable.ConditionalSubscriber) actual;
            return new FluxOnAssembly.OnAssemblyConditionalSubscriber( cs, this.stacktrace, this.source );
        }
        else
        {
            return new FluxOnAssembly.OnAssemblySubscriber( actual, this.stacktrace, this.source );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.ACTUAL_METADATA ? !this.stacktrace.checkpointed : super.scanUnsafe( key );
    }

    public String stepName()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }

    public String toString()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }
}
