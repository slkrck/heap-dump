package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

final class ParallelFluxOnAssembly<T> extends ParallelFlux<T> implements Fuseable, AssemblyOp, Scannable
{
    final ParallelFlux<T> source;
    final FluxOnAssembly.AssemblySnapshot stacktrace;

    ParallelFluxOnAssembly( ParallelFlux<T> source, FluxOnAssembly.AssemblySnapshot stacktrace )
    {
        this.source = source;
        this.stacktrace = stacktrace;
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<? super T>[] parents = new CoreSubscriber[n];

            for ( int i = 0; i < n; ++i )
            {
                CoreSubscriber<? super T> s = subscribers[i];
                Object s;
                if ( s instanceof Fuseable.ConditionalSubscriber )
                {
                    Fuseable.ConditionalSubscriber<? super T> cs = (Fuseable.ConditionalSubscriber) s;
                    s = new FluxOnAssembly.OnAssemblyConditionalSubscriber( cs, this.stacktrace, this.source );
                }
                else
                {
                    s = new FluxOnAssembly.OnAssemblySubscriber( s, this.stacktrace, this.source );
                }

                parents[i] = (CoreSubscriber) s;
            }

            this.source.subscribe( parents );
        }
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else if ( key == Scannable.Attr.PREFETCH )
        {
            return this.getPrefetch();
        }
        else
        {
            return key == Scannable.Attr.ACTUAL_METADATA ? !this.stacktrace.checkpointed : null;
        }
    }

    public String stepName()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }

    public String toString()
    {
        return this.stacktrace.operatorAssemblyInformation();
    }
}
