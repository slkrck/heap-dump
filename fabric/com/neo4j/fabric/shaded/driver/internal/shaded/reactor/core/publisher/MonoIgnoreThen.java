package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class MonoIgnoreThen<T> extends Mono<T> implements Fuseable, Scannable
{
    final Publisher<?>[] ignore;
    final Mono<T> last;

    MonoIgnoreThen( Publisher<?>[] ignore, Mono<T> last )
    {
        this.ignore = (Publisher[]) Objects.requireNonNull( ignore, "ignore" );
        this.last = (Mono) Objects.requireNonNull( last, "last" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        MonoIgnoreThen.ThenIgnoreMain<T> manager = new MonoIgnoreThen.ThenIgnoreMain( actual, this.ignore, this.last );
        actual.onSubscribe( manager );
        manager.drain();
    }

    <U> MonoIgnoreThen<U> shift( Mono<U> newLast )
    {
        Objects.requireNonNull( newLast, "newLast" );
        Publisher<?>[] a = this.ignore;
        int n = a.length;
        Publisher<?>[] b = new Publisher[n + 1];
        System.arraycopy( a, 0, b, 0, n );
        b[n] = this.last;
        return new MonoIgnoreThen( b, newLast );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class ThenAcceptInner<T> implements InnerConsumer<T>
    {
        static final AtomicReferenceFieldUpdater<MonoIgnoreThen.ThenAcceptInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoIgnoreThen.ThenAcceptInner.class, Subscription.class, "s" );
        final MonoIgnoreThen.ThenIgnoreMain<T> parent;
        volatile Subscription s;
        boolean done;

        ThenAcceptInner( MonoIgnoreThen.ThenIgnoreMain<T> parent )
        {
            this.parent = parent;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.s == Operators.cancelledSubscription() : null;
            }
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.parent.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.complete( t );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.parent.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.parent.onComplete();
            }
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }
    }

    static final class ThenIgnoreInner implements InnerConsumer<Object>
    {
        static final AtomicReferenceFieldUpdater<MonoIgnoreThen.ThenIgnoreInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoIgnoreThen.ThenIgnoreInner.class, Subscription.class, "s" );
        final MonoIgnoreThen.ThenIgnoreMain<?> parent;
        volatile Subscription s;

        ThenIgnoreInner( MonoIgnoreThen.ThenIgnoreMain<?> parent )
        {
            this.parent = parent;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.s == Operators.cancelledSubscription() : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.replace( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        public void onNext( Object t )
        {
            Operators.onDiscard( t, this.parent.currentContext() );
        }

        public void onError( Throwable t )
        {
            this.parent.onError( t );
        }

        public void onComplete()
        {
            this.parent.ignoreDone();
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }

        void clear()
        {
            S.lazySet( this, (Object) null );
        }
    }

    static final class ThenIgnoreMain<T> extends Operators.MonoSubscriber<T,T>
    {
        static final AtomicIntegerFieldUpdater<MonoIgnoreThen.ThenIgnoreMain> WIP =
                AtomicIntegerFieldUpdater.newUpdater( MonoIgnoreThen.ThenIgnoreMain.class, "wip" );
        final MonoIgnoreThen.ThenIgnoreInner ignore;
        final MonoIgnoreThen.ThenAcceptInner<T> accept;
        final Publisher<?>[] ignoreMonos;
        final Mono<T> lastMono;
        int index;
        volatile boolean active;
        volatile int wip;

        ThenIgnoreMain( CoreSubscriber<? super T> subscriber, Publisher<?>[] ignoreMonos, Mono<T> lastMono )
        {
            super( subscriber );
            this.ignoreMonos = ignoreMonos;
            this.lastMono = lastMono;
            this.ignore = new MonoIgnoreThen.ThenIgnoreInner( this );
            this.accept = new MonoIgnoreThen.ThenAcceptInner( this );
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.ignore, this.accept );
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                while ( true )
                {
                    while ( !this.isCancelled() )
                    {
                        if ( !this.active )
                        {
                            Publisher<?>[] a = this.ignoreMonos;
                            int i = this.index;
                            if ( i == a.length )
                            {
                                this.ignore.clear();
                                Mono<T> m = this.lastMono;
                                if ( m instanceof Callable )
                                {
                                    Object v;
                                    try
                                    {
                                        v = ((Callable) m).call();
                                    }
                                    catch ( Throwable var6 )
                                    {
                                        this.actual.onError( Operators.onOperatorError( var6, this.actual.currentContext() ) );
                                        return;
                                    }

                                    if ( v == null )
                                    {
                                        this.actual.onComplete();
                                    }
                                    else
                                    {
                                        this.complete( v );
                                    }

                                    return;
                                }

                                this.active = true;
                                m.subscribe( (CoreSubscriber) this.accept );
                            }
                            else
                            {
                                Publisher<?> m = a[i];
                                this.index = i + 1;
                                if ( m instanceof Callable )
                                {
                                    try
                                    {
                                        ((Callable) m).call();
                                        continue;
                                    }
                                    catch ( Throwable var7 )
                                    {
                                        this.actual.onError( Operators.onOperatorError( var7, this.actual.currentContext() ) );
                                        return;
                                    }
                                }

                                this.active = true;
                                m.subscribe( this.ignore );
                            }
                        }

                        if ( WIP.decrementAndGet( this ) == 0 )
                        {
                            return;
                        }
                    }

                    return;
                }
            }
        }

        public void cancel()
        {
            super.cancel();
            this.ignore.cancel();
            this.accept.cancel();
        }

        void ignoreDone()
        {
            this.active = false;
            this.drain();
        }
    }
}
