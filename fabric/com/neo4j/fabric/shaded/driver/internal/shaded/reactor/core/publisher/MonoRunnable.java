package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

import org.reactivestreams.Subscription;

final class MonoRunnable<T> extends Mono<T> implements Callable<Void>, SourceProducer<T>
{
    final Runnable run;

    MonoRunnable( Runnable run )
    {
        this.run = (Runnable) Objects.requireNonNull( run, "run" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        MonoRunnable.MonoRunnableEagerSubscription s = new MonoRunnable.MonoRunnableEagerSubscription();
        actual.onSubscribe( s );
        if ( !s.isCancelled() )
        {
            try
            {
                this.run.run();
                actual.onComplete();
            }
            catch ( Throwable var4 )
            {
                actual.onError( Operators.onOperatorError( var4, actual.currentContext() ) );
            }
        }
    }

    @Nullable
    public T block( Duration m )
    {
        this.run.run();
        return null;
    }

    @Nullable
    public T block()
    {
        this.run.run();
        return null;
    }

    @Nullable
    public Void call() throws Exception
    {
        this.run.run();
        return null;
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class MonoRunnableEagerSubscription extends AtomicBoolean implements Subscription
    {
        public void request( long n )
        {
        }

        public void cancel()
        {
            this.set( true );
        }

        public boolean isCancelled()
        {
            return this.get();
        }
    }
}
