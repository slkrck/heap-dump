package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.PromiseNotifier;

public final class ChannelPromiseNotifier extends PromiseNotifier<Void,ChannelFuture> implements ChannelFutureListener
{
    public ChannelPromiseNotifier( ChannelPromise... promises )
    {
        super( promises );
    }

    public ChannelPromiseNotifier( boolean logNotifyFailure, ChannelPromise... promises )
    {
        super( logNotifyFailure, promises );
    }
}
