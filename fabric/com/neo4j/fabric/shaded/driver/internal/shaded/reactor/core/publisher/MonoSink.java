package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.function.LongConsumer;

public interface MonoSink<T>
{
    Context currentContext();

    void success();

    void success( @Nullable T var1 );

    void error( Throwable var1 );

    MonoSink<T> onRequest( LongConsumer var1 );

    MonoSink<T> onCancel( Disposable var1 );

    MonoSink<T> onDispose( Disposable var1 );
}
