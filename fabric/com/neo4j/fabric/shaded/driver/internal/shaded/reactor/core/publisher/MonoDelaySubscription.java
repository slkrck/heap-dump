package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.Consumer;

import org.reactivestreams.Publisher;

final class MonoDelaySubscription<T, U> extends InternalMonoOperator<T,T> implements Consumer<FluxDelaySubscription.DelaySubscriptionOtherSubscriber<T,U>>
{
    final Publisher<U> other;

    MonoDelaySubscription( Mono<? extends T> source, Publisher<U> other )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        this.other.subscribe( new FluxDelaySubscription.DelaySubscriptionOtherSubscriber( actual, this ) );
        return null;
    }

    public void accept( FluxDelaySubscription.DelaySubscriptionOtherSubscriber<T,U> s )
    {
        this.source.subscribe( (CoreSubscriber) (new FluxDelaySubscription.DelaySubscriptionMainSubscriber( s.actual, s )) );
    }
}
