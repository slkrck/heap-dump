package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class DelegateProcessor<IN, OUT> extends FluxProcessor<IN,OUT>
{
    final Publisher<OUT> downstream;
    final Subscriber<IN> upstream;

    DelegateProcessor( Publisher<OUT> downstream, Subscriber<IN> upstream )
    {
        this.downstream = (Publisher) Objects.requireNonNull( downstream, "Downstream must not be null" );
        this.upstream = (Subscriber) Objects.requireNonNull( upstream, "Upstream must not be null" );
    }

    public Context currentContext()
    {
        return this.upstream instanceof CoreSubscriber ? ((CoreSubscriber) this.upstream).currentContext() : Context.empty();
    }

    public void onComplete()
    {
        this.upstream.onComplete();
    }

    public void onError( Throwable t )
    {
        this.upstream.onError( t );
    }

    public void onNext( IN in )
    {
        this.upstream.onNext( in );
    }

    public void onSubscribe( Subscription s )
    {
        this.upstream.onSubscribe( s );
    }

    public void subscribe( CoreSubscriber<? super OUT> actual )
    {
        Objects.requireNonNull( actual, "subscribe" );
        this.downstream.subscribe( actual );
    }

    public boolean isSerialized()
    {
        return this.upstream instanceof SerializedSubscriber || this.upstream instanceof FluxProcessor && ((FluxProcessor) this.upstream).isSerialized();
    }

    public Stream<? extends Scannable> inners()
    {
        return Scannable.from( this.upstream ).inners();
    }

    public int getBufferSize()
    {
        return (Integer) Scannable.from( this.upstream ).scanOrDefault( Scannable.Attr.CAPACITY, super.getBufferSize() );
    }

    @Nullable
    public Throwable getError()
    {
        return (Throwable) Scannable.from( this.upstream ).scanOrDefault( Scannable.Attr.ERROR, super.getError() );
    }

    public boolean isTerminated()
    {
        return (Boolean) Scannable.from( this.upstream ).scanOrDefault( Scannable.Attr.TERMINATED, super.isTerminated() );
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.PARENT ? this.downstream : Scannable.from( this.upstream ).scanUnsafe( key );
    }
}
