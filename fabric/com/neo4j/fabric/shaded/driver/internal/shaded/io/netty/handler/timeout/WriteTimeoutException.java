package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.timeout;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

public final class WriteTimeoutException extends TimeoutException
{
    public static final WriteTimeoutException INSTANCE = PlatformDependent.javaVersion() >= 7 ? new WriteTimeoutException( true ) : new WriteTimeoutException();
    private static final long serialVersionUID = -144786655770296065L;

    private WriteTimeoutException()
    {
    }

    private WriteTimeoutException( boolean shared )
    {
        super( shared );
    }
}
