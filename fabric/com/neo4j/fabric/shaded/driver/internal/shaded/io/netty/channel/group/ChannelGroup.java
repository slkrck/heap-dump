package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.group;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelId;

import java.util.Set;

public interface ChannelGroup extends Set<Channel>, Comparable<ChannelGroup>
{
    String name();

    Channel find( ChannelId var1 );

    ChannelGroupFuture write( Object var1 );

    ChannelGroupFuture write( Object var1, ChannelMatcher var2 );

    ChannelGroupFuture write( Object var1, ChannelMatcher var2, boolean var3 );

    ChannelGroup flush();

    ChannelGroup flush( ChannelMatcher var1 );

    ChannelGroupFuture writeAndFlush( Object var1 );

    /**
     * @deprecated
     */
    @Deprecated
    ChannelGroupFuture flushAndWrite( Object var1 );

    ChannelGroupFuture writeAndFlush( Object var1, ChannelMatcher var2 );

    ChannelGroupFuture writeAndFlush( Object var1, ChannelMatcher var2, boolean var3 );

    /**
     * @deprecated
     */
    @Deprecated
    ChannelGroupFuture flushAndWrite( Object var1, ChannelMatcher var2 );

    ChannelGroupFuture disconnect();

    ChannelGroupFuture disconnect( ChannelMatcher var1 );

    ChannelGroupFuture close();

    ChannelGroupFuture close( ChannelMatcher var1 );

    /**
     * @deprecated
     */
    @Deprecated
    ChannelGroupFuture deregister();

    /**
     * @deprecated
     */
    @Deprecated
    ChannelGroupFuture deregister( ChannelMatcher var1 );

    ChannelGroupFuture newCloseFuture();

    ChannelGroupFuture newCloseFuture( ChannelMatcher var1 );
}
