package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxOnErrorResume<T> extends InternalFluxOperator<T,T>
{
    final Function<? super Throwable,? extends Publisher<? extends T>> nextFactory;

    FluxOnErrorResume( Flux<? extends T> source, Function<? super Throwable,? extends Publisher<? extends T>> nextFactory )
    {
        super( source );
        this.nextFactory = (Function) Objects.requireNonNull( nextFactory, "nextFactory" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxOnErrorResume.ResumeSubscriber( actual, this.nextFactory );
    }

    static final class ResumeSubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        final Function<? super Throwable,? extends Publisher<? extends T>> nextFactory;
        boolean second;

        ResumeSubscriber( CoreSubscriber<? super T> actual, Function<? super Throwable,? extends Publisher<? extends T>> nextFactory )
        {
            super( actual );
            this.nextFactory = nextFactory;
        }

        public void onSubscribe( Subscription s )
        {
            if ( !this.second )
            {
                this.actual.onSubscribe( this );
            }

            this.set( s );
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
            if ( !this.second )
            {
                this.producedOne();
            }
        }

        public void onError( Throwable t )
        {
            if ( !this.second )
            {
                this.second = true;

                Publisher p;
                try
                {
                    p = (Publisher) Objects.requireNonNull( this.nextFactory.apply( t ), "The nextFactory returned a null Publisher" );
                }
                catch ( Throwable var5 )
                {
                    Throwable _e = Operators.onOperatorError( var5, this.actual.currentContext() );
                    _e = Exceptions.addSuppressed( _e, t );
                    this.actual.onError( _e );
                    return;
                }

                p.subscribe( this );
            }
            else
            {
                this.actual.onError( t );
            }
        }
    }
}
