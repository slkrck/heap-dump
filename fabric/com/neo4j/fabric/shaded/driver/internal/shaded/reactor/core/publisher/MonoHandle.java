package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.BiConsumer;

final class MonoHandle<T, R> extends InternalMonoOperator<T,R>
{
    final BiConsumer<? super T,SynchronousSink<R>> handler;

    MonoHandle( Mono<? extends T> source, BiConsumer<? super T,SynchronousSink<R>> handler )
    {
        super( source );
        this.handler = (BiConsumer) Objects.requireNonNull( handler, "handler" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        return new FluxHandle.HandleSubscriber( actual, this.handler );
    }
}
