package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.logging;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogLevel;

public enum LogLevel
{
    TRACE( InternalLogLevel.TRACE ),
    DEBUG( InternalLogLevel.DEBUG ),
    INFO( InternalLogLevel.INFO ),
    WARN( InternalLogLevel.WARN ),
    ERROR( InternalLogLevel.ERROR );

    private final InternalLogLevel internalLevel;

    private LogLevel( InternalLogLevel internalLevel )
    {
        this.internalLevel = internalLevel;
    }

    public InternalLogLevel toInternalLevel()
    {
        return this.internalLevel;
    }
}
