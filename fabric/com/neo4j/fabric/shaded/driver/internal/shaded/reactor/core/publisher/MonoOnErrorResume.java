package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Publisher;

final class MonoOnErrorResume<T> extends InternalMonoOperator<T,T>
{
    final Function<? super Throwable,? extends Publisher<? extends T>> nextFactory;

    MonoOnErrorResume( Mono<? extends T> source, Function<? super Throwable,? extends Mono<? extends T>> nextFactory )
    {
        super( source );
        this.nextFactory = (Function) Objects.requireNonNull( nextFactory, "nextFactory" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxOnErrorResume.ResumeSubscriber( actual, this.nextFactory );
    }
}
