package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;

final class SucceededChannelFuture extends CompleteChannelFuture
{
    SucceededChannelFuture( Channel channel, EventExecutor executor )
    {
        super( channel, executor );
    }

    public Throwable cause()
    {
        return null;
    }

    public boolean isSuccess()
    {
        return true;
    }
}
