package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.BiConsumer;

import org.reactivestreams.Subscription;

final class FluxHandle<T, R> extends InternalFluxOperator<T,R>
{
    final BiConsumer<? super T,SynchronousSink<R>> handler;

    FluxHandle( Flux<? extends T> source, BiConsumer<? super T,SynchronousSink<R>> handler )
    {
        super( source );
        this.handler = (BiConsumer) Objects.requireNonNull( handler, "handler" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<? super R> cs = (Fuseable.ConditionalSubscriber) actual;
            return new FluxHandle.HandleConditionalSubscriber( cs, this.handler );
        }
        else
        {
            return new FluxHandle.HandleSubscriber( actual, this.handler );
        }
    }

    static final class HandleConditionalSubscriber<T, R> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,R>, SynchronousSink<R>
    {
        final Fuseable.ConditionalSubscriber<? super R> actual;
        final BiConsumer<? super T,SynchronousSink<R>> handler;
        boolean done;
        boolean stop;
        Throwable error;
        R data;
        Subscription s;

        HandleConditionalSubscriber( Fuseable.ConditionalSubscriber<? super R> actual, BiConsumer<? super T,SynchronousSink<R>> handler )
        {
            this.actual = actual;
            this.handler = handler;
        }

        public Context currentContext()
        {
            return this.actual.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                Throwable e_;
                try
                {
                    this.handler.accept( t, this );
                }
                catch ( Throwable var4 )
                {
                    e_ = Operators.onNextError( t, var4, this.actual.currentContext(), this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }
                    else
                    {
                        this.error = null;
                        this.s.request( 1L );
                    }

                    return;
                }

                R v = this.data;
                this.data = null;
                if ( v != null )
                {
                    this.actual.onNext( v );
                }

                if ( this.stop )
                {
                    this.done = true;
                    if ( this.error != null )
                    {
                        e_ = Operators.onNextError( t, this.error, this.actual.currentContext(), this.s );
                        if ( e_ != null )
                        {
                            this.actual.onError( e_ );
                        }
                        else
                        {
                            this.reset();
                            this.s.request( 1L );
                        }
                    }
                    else
                    {
                        this.s.cancel();
                        this.actual.onComplete();
                    }
                }
                else if ( v == null )
                {
                    this.s.request( 1L );
                }
            }
        }

        private void reset()
        {
            this.done = false;
            this.stop = false;
            this.error = null;
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
                return false;
            }
            else
            {
                try
                {
                    this.handler.accept( t, this );
                }
                catch ( Throwable var5 )
                {
                    Throwable e_ = Operators.onNextError( t, var5, this.actual.currentContext(), this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                        return true;
                    }

                    this.reset();
                    return false;
                }

                R v = this.data;
                boolean emit = false;
                this.data = null;
                if ( v != null )
                {
                    emit = this.actual.tryOnNext( v );
                }

                if ( this.stop )
                {
                    this.done = true;
                    if ( this.error != null )
                    {
                        Throwable e_ = Operators.onNextError( t, this.error, this.actual.currentContext(), this.s );
                        if ( e_ == null )
                        {
                            this.reset();
                            return false;
                        }

                        this.actual.onError( e_ );
                    }
                    else
                    {
                        this.s.cancel();
                        this.actual.onComplete();
                    }

                    return true;
                }
                else
                {
                    return emit;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void complete()
        {
            if ( this.stop )
            {
                throw new IllegalStateException( "Cannot complete after a complete or error" );
            }
            else
            {
                this.stop = true;
            }
        }

        public void error( Throwable e )
        {
            if ( this.stop )
            {
                throw new IllegalStateException( "Cannot error after a complete or error" );
            }
            else
            {
                this.error = (Throwable) Objects.requireNonNull( e, "error" );
                this.stop = true;
            }
        }

        public void next( R o )
        {
            if ( this.data != null )
            {
                throw new IllegalStateException( "Cannot emit more than one data" );
            }
            else if ( this.stop )
            {
                throw new IllegalStateException( "Cannot emit after a complete or error" );
            }
            else
            {
                this.data = Objects.requireNonNull( o, "data" );
            }
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.ERROR ? this.error : InnerOperator.super.scanUnsafe( key );
            }
        }
    }

    static final class HandleSubscriber<T, R> implements InnerOperator<T,R>, Fuseable.ConditionalSubscriber<T>, SynchronousSink<R>
    {
        final CoreSubscriber<? super R> actual;
        final BiConsumer<? super T,SynchronousSink<R>> handler;
        boolean done;
        boolean stop;
        Throwable error;
        R data;
        Subscription s;

        HandleSubscriber( CoreSubscriber<? super R> actual, BiConsumer<? super T,SynchronousSink<R>> handler )
        {
            this.actual = actual;
            this.handler = handler;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public Context currentContext()
        {
            return this.actual.currentContext();
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                Throwable e_;
                try
                {
                    this.handler.accept( t, this );
                }
                catch ( Throwable var4 )
                {
                    e_ = Operators.onNextError( t, var4, this.actual.currentContext(), this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                    }
                    else
                    {
                        this.reset();
                        this.s.request( 1L );
                    }

                    return;
                }

                R v = this.data;
                this.data = null;
                if ( v != null )
                {
                    this.actual.onNext( v );
                }

                if ( this.stop )
                {
                    if ( this.error != null )
                    {
                        e_ = Operators.onNextError( t, this.error, this.actual.currentContext(), this.s );
                        if ( e_ != null )
                        {
                            this.onError( e_ );
                        }
                        else
                        {
                            this.reset();
                            this.s.request( 1L );
                        }
                    }
                    else
                    {
                        this.s.cancel();
                        this.onComplete();
                    }
                }
                else if ( v == null )
                {
                    this.s.request( 1L );
                }
            }
        }

        private void reset()
        {
            this.done = false;
            this.stop = false;
            this.error = null;
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
                return false;
            }
            else
            {
                Throwable e_;
                try
                {
                    this.handler.accept( t, this );
                }
                catch ( Throwable var4 )
                {
                    e_ = Operators.onNextError( t, var4, this.actual.currentContext(), this.s );
                    if ( e_ != null )
                    {
                        this.onError( e_ );
                        return true;
                    }

                    this.reset();
                    return false;
                }

                R v = this.data;
                this.data = null;
                if ( v != null )
                {
                    this.actual.onNext( v );
                }

                if ( this.stop )
                {
                    if ( this.error != null )
                    {
                        e_ = Operators.onNextError( t, this.error, this.actual.currentContext(), this.s );
                        if ( e_ == null )
                        {
                            this.reset();
                            return false;
                        }

                        this.onError( e_ );
                    }
                    else
                    {
                        this.s.cancel();
                        this.onComplete();
                    }

                    return true;
                }
                else
                {
                    return v != null;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        public void complete()
        {
            if ( this.stop )
            {
                throw new IllegalStateException( "Cannot complete after a complete or error" );
            }
            else
            {
                this.stop = true;
            }
        }

        public void error( Throwable e )
        {
            if ( this.stop )
            {
                throw new IllegalStateException( "Cannot error after a complete or error" );
            }
            else
            {
                this.error = (Throwable) Objects.requireNonNull( e, "error" );
                this.stop = true;
            }
        }

        public void next( R o )
        {
            if ( this.data != null )
            {
                throw new IllegalStateException( "Cannot emit more than one data" );
            }
            else if ( this.stop )
            {
                throw new IllegalStateException( "Cannot emit after a complete or error" );
            }
            else
            {
                this.data = Objects.requireNonNull( o, "data" );
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.ERROR ? this.error : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
