package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

import org.jetbrains.annotations.NotNull;

final class DelegateServiceScheduler implements Scheduler, Scannable
{
    final String executorName;
    final ScheduledExecutorService executor;

    DelegateServiceScheduler( String executorName, ExecutorService executorService )
    {
        this.executorName = executorName;
        ScheduledExecutorService exec = convert( executorService );
        this.executor = Schedulers.decorateExecutorService( this, exec );
    }

    static ScheduledExecutorService convert( ExecutorService executor )
    {
        return (ScheduledExecutorService) (executor instanceof ScheduledExecutorService ? (ScheduledExecutorService) executor
                                                                                        : new DelegateServiceScheduler.UnsupportedScheduledExecutorService(
                                                                                                executor ));
    }

    public Scheduler.Worker createWorker()
    {
        return new ExecutorServiceWorker( this.executor );
    }

    public Disposable schedule( Runnable task )
    {
        return Schedulers.directSchedule( this.executor, task, (Disposable) null, 0L, TimeUnit.MILLISECONDS );
    }

    public Disposable schedule( Runnable task, long delay, TimeUnit unit )
    {
        return Schedulers.directSchedule( this.executor, task, (Disposable) null, delay, unit );
    }

    public Disposable schedulePeriodically( Runnable task, long initialDelay, long period, TimeUnit unit )
    {
        return Schedulers.directSchedulePeriodically( this.executor, task, initialDelay, period, unit );
    }

    public boolean isDisposed()
    {
        return this.executor.isShutdown();
    }

    public void dispose()
    {
        this.executor.shutdownNow();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
        {
            return key == Scannable.Attr.NAME ? this.toString() : Schedulers.scanExecutor( this.executor, key );
        }
        else
        {
            return this.isDisposed();
        }
    }

    public String toString()
    {
        return "fromExecutorService(" + this.executorName + ')';
    }

    static final class UnsupportedScheduledExecutorService implements ScheduledExecutorService, Supplier<ExecutorService>
    {
        final ExecutorService exec;

        UnsupportedScheduledExecutorService( ExecutorService exec )
        {
            this.exec = exec;
        }

        public ExecutorService get()
        {
            return this.exec;
        }

        public void shutdown()
        {
            this.exec.shutdown();
        }

        @NotNull
        public List<Runnable> shutdownNow()
        {
            return this.exec.shutdownNow();
        }

        public boolean isShutdown()
        {
            return this.exec.isShutdown();
        }

        public boolean isTerminated()
        {
            return this.exec.isTerminated();
        }

        public boolean awaitTermination( long timeout, @NotNull TimeUnit unit ) throws InterruptedException
        {
            return this.exec.awaitTermination( timeout, unit );
        }

        @NotNull
        public <T> Future<T> submit( @NotNull Callable<T> task )
        {
            return this.exec.submit( task );
        }

        @NotNull
        public <T> Future<T> submit( @NotNull Runnable task, T result )
        {
            return this.exec.submit( task, result );
        }

        @NotNull
        public Future<?> submit( @NotNull Runnable task )
        {
            return this.exec.submit( task );
        }

        @NotNull
        public <T> List<Future<T>> invokeAll( @NotNull Collection<? extends Callable<T>> tasks ) throws InterruptedException
        {
            return this.exec.invokeAll( tasks );
        }

        @NotNull
        public <T> List<Future<T>> invokeAll( @NotNull Collection<? extends Callable<T>> tasks, long timeout, @NotNull TimeUnit unit )
                throws InterruptedException
        {
            return this.exec.invokeAll( tasks, timeout, unit );
        }

        @NotNull
        public <T> T invokeAny( @NotNull Collection<? extends Callable<T>> tasks ) throws InterruptedException, ExecutionException
        {
            return this.exec.invokeAny( tasks );
        }

        public <T> T invokeAny( @NotNull Collection<? extends Callable<T>> tasks, long timeout, @NotNull TimeUnit unit )
                throws InterruptedException, ExecutionException, TimeoutException
        {
            return this.exec.invokeAny( tasks, timeout, unit );
        }

        public void execute( @NotNull Runnable command )
        {
            this.exec.execute( command );
        }

        @NotNull
        public ScheduledFuture<?> schedule( @NotNull Runnable command, long delay, @NotNull TimeUnit unit )
        {
            throw Exceptions.failWithRejectedNotTimeCapable();
        }

        @NotNull
        public <V> ScheduledFuture<V> schedule( @NotNull Callable<V> callable, long delay, @NotNull TimeUnit unit )
        {
            throw Exceptions.failWithRejectedNotTimeCapable();
        }

        @NotNull
        public ScheduledFuture<?> scheduleAtFixedRate( @NotNull Runnable command, long initialDelay, long period, @NotNull TimeUnit unit )
        {
            throw Exceptions.failWithRejectedNotTimeCapable();
        }

        @NotNull
        public ScheduledFuture<?> scheduleWithFixedDelay( @NotNull Runnable command, long initialDelay, long delay, @NotNull TimeUnit unit )
        {
            throw Exceptions.failWithRejectedNotTimeCapable();
        }

        public String toString()
        {
            return this.exec.toString();
        }
    }
}
