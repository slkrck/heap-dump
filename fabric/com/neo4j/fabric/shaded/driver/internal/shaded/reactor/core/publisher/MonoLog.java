package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

final class MonoLog<T> extends InternalMonoOperator<T,T>
{
    final SignalPeek<T> log;

    MonoLog( Mono<? extends T> source, SignalPeek<T> log )
    {
        super( source );
        this.log = log;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            Fuseable.ConditionalSubscriber<T> s2 = (Fuseable.ConditionalSubscriber) actual;
            return new FluxPeekFuseable.PeekConditionalSubscriber( s2, this.log );
        }
        else
        {
            return new FluxPeek.PeekSubscriber( actual, this.log );
        }
    }
}
