package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuples;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

final class MonoName<T> extends InternalMonoOperator<T,T>
{
    final String name;
    final Set<Tuple2<String,String>> tags;

    MonoName( Mono<? extends T> source, @Nullable String name, @Nullable Set<Tuple2<String,String>> tags )
    {
        super( source );
        this.name = name;
        this.tags = tags;
    }

    static <T> Mono<T> createOrAppend( Mono<T> source, String name )
    {
        Objects.requireNonNull( name, "name" );
        if ( source instanceof MonoName )
        {
            MonoName<T> s = (MonoName) source;
            return new MonoName( s.source, name, s.tags );
        }
        else if ( source instanceof MonoNameFuseable )
        {
            MonoNameFuseable<T> s = (MonoNameFuseable) source;
            return new MonoNameFuseable( s.source, name, s.tags );
        }
        else
        {
            return (Mono) (source instanceof Fuseable ? new MonoNameFuseable( source, name, (Set) null ) : new MonoName( source, name, (Set) null ));
        }
    }

    static <T> Mono<T> createOrAppend( Mono<T> source, String tagName, String tagValue )
    {
        Objects.requireNonNull( tagName, "tagName" );
        Objects.requireNonNull( tagValue, "tagValue" );
        Set<Tuple2<String,String>> tags = Collections.singleton( Tuples.of( tagName, tagValue ) );
        if ( source instanceof MonoName )
        {
            MonoName<T> s = (MonoName) source;
            if ( s.tags != null )
            {
                tags = new HashSet( (Collection) tags );
                ((Set) tags).addAll( s.tags );
            }

            return new MonoName( s.source, s.name, (Set) tags );
        }
        else if ( source instanceof MonoNameFuseable )
        {
            MonoNameFuseable<T> s = (MonoNameFuseable) source;
            if ( s.tags != null )
            {
                tags = new HashSet( (Collection) tags );
                ((Set) tags).addAll( s.tags );
            }

            return new MonoNameFuseable( s.source, s.name, (Set) tags );
        }
        else
        {
            return (Mono) (source instanceof Fuseable ? new MonoNameFuseable( source, (String) null, (Set) tags )
                                                      : new MonoName( source, (String) null, (Set) tags ));
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return actual;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.NAME )
        {
            return this.name;
        }
        else
        {
            return key == Scannable.Attr.TAGS && this.tags != null ? this.tags.stream() : super.scanUnsafe( key );
        }
    }
}
