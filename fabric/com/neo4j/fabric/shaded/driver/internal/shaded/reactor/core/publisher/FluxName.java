package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuples;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

final class FluxName<T> extends InternalFluxOperator<T,T>
{
    final String name;
    final Set<Tuple2<String,String>> tags;

    FluxName( Flux<? extends T> source, @Nullable String name, @Nullable Set<Tuple2<String,String>> tags )
    {
        super( source );
        this.name = name;
        this.tags = tags;
    }

    static <T> Flux<T> createOrAppend( Flux<T> source, String name )
    {
        Objects.requireNonNull( name, "name" );
        if ( source instanceof FluxName )
        {
            FluxName<T> s = (FluxName) source;
            return new FluxName( s.source, name, s.tags );
        }
        else if ( source instanceof FluxNameFuseable )
        {
            FluxNameFuseable<T> s = (FluxNameFuseable) source;
            return new FluxNameFuseable( s.source, name, s.tags );
        }
        else
        {
            return (Flux) (source instanceof Fuseable ? new FluxNameFuseable( source, name, (Set) null ) : new FluxName( source, name, (Set) null ));
        }
    }

    static <T> Flux<T> createOrAppend( Flux<T> source, String tagName, String tagValue )
    {
        Objects.requireNonNull( tagName, "tagName" );
        Objects.requireNonNull( tagValue, "tagValue" );
        Set<Tuple2<String,String>> tags = Collections.singleton( Tuples.of( tagName, tagValue ) );
        if ( source instanceof FluxName )
        {
            FluxName<T> s = (FluxName) source;
            if ( s.tags != null )
            {
                tags = new HashSet( (Collection) tags );
                ((Set) tags).addAll( s.tags );
            }

            return new FluxName( s.source, s.name, (Set) tags );
        }
        else if ( source instanceof FluxNameFuseable )
        {
            FluxNameFuseable<T> s = (FluxNameFuseable) source;
            if ( s.tags != null )
            {
                tags = new HashSet( (Collection) tags );
                ((Set) tags).addAll( s.tags );
            }

            return new FluxNameFuseable( s.source, s.name, (Set) tags );
        }
        else
        {
            return (Flux) (source instanceof Fuseable ? new FluxNameFuseable( source, (String) null, (Set) tags )
                                                      : new FluxName( source, (String) null, (Set) tags ));
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return actual;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.NAME )
        {
            return this.name;
        }
        else
        {
            return key == Scannable.Attr.TAGS && this.tags != null ? this.tags.stream() : super.scanUnsafe( key );
        }
    }
}
