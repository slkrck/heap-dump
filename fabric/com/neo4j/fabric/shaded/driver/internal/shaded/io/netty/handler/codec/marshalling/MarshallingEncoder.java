package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.marshalling;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToByteEncoder;
import org.jboss.marshalling.Marshaller;

@ChannelHandler.Sharable
public class MarshallingEncoder extends MessageToByteEncoder<Object>
{
    private static final byte[] LENGTH_PLACEHOLDER = new byte[4];
    private final MarshallerProvider provider;

    public MarshallingEncoder( MarshallerProvider provider )
    {
        this.provider = provider;
    }

    protected void encode( ChannelHandlerContext ctx, Object msg, ByteBuf out ) throws Exception
    {
        Marshaller marshaller = this.provider.getMarshaller( ctx );
        int lengthPos = out.writerIndex();
        out.writeBytes( LENGTH_PLACEHOLDER );
        ChannelBufferByteOutput output = new ChannelBufferByteOutput( out );
        marshaller.start( output );
        marshaller.writeObject( msg );
        marshaller.finish();
        marshaller.close();
        out.setInt( lengthPos, out.writerIndex() - lengthPos - 4 );
    }
}
