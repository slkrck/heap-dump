package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Consumer;
import java.util.function.LongConsumer;

final class MonoCreate<T> extends Mono<T> implements SourceProducer<T>
{
    static final Disposable TERMINATED;
    static final Disposable CANCELLED;

    static
    {
        TERMINATED = OperatorDisposables.DISPOSED;
        CANCELLED = Disposables.disposed();
    }

    final Consumer<MonoSink<T>> callback;

    MonoCreate( Consumer<MonoSink<T>> callback )
    {
        this.callback = callback;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        MonoCreate.DefaultMonoSink<T> emitter = new MonoCreate.DefaultMonoSink( actual );
        actual.onSubscribe( emitter );

        try
        {
            this.callback.accept( emitter );
        }
        catch ( Throwable var4 )
        {
            emitter.error( Operators.onOperatorError( var4, actual.currentContext() ) );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class DefaultMonoSink<T> extends AtomicBoolean implements MonoSink<T>, InnerProducer<T>
    {
        static final AtomicReferenceFieldUpdater<MonoCreate.DefaultMonoSink,Disposable> DISPOSABLE =
                AtomicReferenceFieldUpdater.newUpdater( MonoCreate.DefaultMonoSink.class, Disposable.class, "disposable" );
        static final AtomicIntegerFieldUpdater<MonoCreate.DefaultMonoSink> STATE =
                AtomicIntegerFieldUpdater.newUpdater( MonoCreate.DefaultMonoSink.class, "state" );
        static final AtomicReferenceFieldUpdater<MonoCreate.DefaultMonoSink,LongConsumer> REQUEST_CONSUMER =
                AtomicReferenceFieldUpdater.newUpdater( MonoCreate.DefaultMonoSink.class, LongConsumer.class, "requestConsumer" );
        static final int NO_REQUEST_HAS_VALUE = 1;
        static final int HAS_REQUEST_NO_VALUE = 2;
        static final int HAS_REQUEST_HAS_VALUE = 3;
        final CoreSubscriber<? super T> actual;
        volatile Disposable disposable;
        volatile int state;
        volatile LongConsumer requestConsumer;
        T value;

        DefaultMonoSink( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        public Context currentContext()
        {
            return this.actual.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED )
            {
                return key == Scannable.Attr.CANCELLED ? this.disposable == MonoCreate.CANCELLED : InnerProducer.super.scanUnsafe( key );
            }
            else
            {
                return this.state == 3 || this.state == 1 || this.disposable == MonoCreate.TERMINATED;
            }
        }

        public void success()
        {
            if ( !this.isDisposed() )
            {
                if ( STATE.getAndSet( this, 3 ) != 3 )
                {
                    try
                    {
                        this.actual.onComplete();
                    }
                    finally
                    {
                        this.disposeResource( false );
                    }
                }
            }
        }

        public void success( @Nullable T value )
        {
            if ( value == null )
            {
                this.success();
            }
            else if ( this.isDisposed() )
            {
                Operators.onNextDropped( value, this.actual.currentContext() );
            }
            else
            {
                int s;
                do
                {
                    s = this.state;
                    if ( s == 3 || s == 1 )
                    {
                        Operators.onNextDropped( value, this.actual.currentContext() );
                        return;
                    }

                    if ( s == 2 )
                    {
                        if ( STATE.compareAndSet( this, s, 3 ) )
                        {
                            try
                            {
                                this.actual.onNext( value );
                                this.actual.onComplete();
                            }
                            catch ( Throwable var7 )
                            {
                                this.actual.onError( var7 );
                            }
                            finally
                            {
                                this.disposeResource( false );
                            }
                        }

                        return;
                    }

                    this.value = value;
                }
                while ( !STATE.compareAndSet( this, s, 1 ) );
            }
        }

        public void error( Throwable e )
        {
            if ( this.isDisposed() )
            {
                Operators.onOperatorError( e, this.actual.currentContext() );
            }
            else
            {
                if ( STATE.getAndSet( this, 3 ) != 3 )
                {
                    try
                    {
                        this.actual.onError( e );
                    }
                    finally
                    {
                        this.disposeResource( false );
                    }
                }
                else
                {
                    Operators.onOperatorError( e, this.actual.currentContext() );
                }
            }
        }

        public MonoSink<T> onRequest( LongConsumer consumer )
        {
            Objects.requireNonNull( consumer, "onRequest" );
            if ( !REQUEST_CONSUMER.compareAndSet( this, (Object) null, consumer ) )
            {
                throw new IllegalStateException( "A consumer has already been assigned to consume requests" );
            }
            else
            {
                int s = this.state;
                if ( s == 2 || s == 3 )
                {
                    consumer.accept( Long.MAX_VALUE );
                }

                return this;
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public MonoSink<T> onCancel( Disposable d )
        {
            Objects.requireNonNull( d, "onCancel" );
            FluxCreate.SinkDisposable sd = new FluxCreate.SinkDisposable( (Disposable) null, d );
            if ( !DISPOSABLE.compareAndSet( this, (Object) null, sd ) )
            {
                Disposable c = this.disposable;
                if ( c == MonoCreate.CANCELLED )
                {
                    d.dispose();
                }
                else if ( c instanceof FluxCreate.SinkDisposable )
                {
                    FluxCreate.SinkDisposable current = (FluxCreate.SinkDisposable) c;
                    if ( current.onCancel == null )
                    {
                        current.onCancel = d;
                    }
                    else
                    {
                        d.dispose();
                    }
                }
            }

            return this;
        }

        public MonoSink<T> onDispose( Disposable d )
        {
            Objects.requireNonNull( d, "onDispose" );
            FluxCreate.SinkDisposable sd = new FluxCreate.SinkDisposable( d, (Disposable) null );
            if ( !DISPOSABLE.compareAndSet( this, (Object) null, sd ) )
            {
                Disposable c = this.disposable;
                if ( this.isDisposed() )
                {
                    d.dispose();
                }
                else if ( c instanceof FluxCreate.SinkDisposable )
                {
                    FluxCreate.SinkDisposable current = (FluxCreate.SinkDisposable) c;
                    if ( current.disposable == null )
                    {
                        current.disposable = d;
                    }
                    else
                    {
                        d.dispose();
                    }
                }
            }

            return this;
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                LongConsumer consumer = this.requestConsumer;
                if ( consumer != null )
                {
                    consumer.accept( n );
                }

                int s;
                do
                {
                    s = this.state;
                    if ( s == 2 || s == 3 )
                    {
                        return;
                    }

                    if ( s == 1 )
                    {
                        if ( STATE.compareAndSet( this, s, 3 ) )
                        {
                            try
                            {
                                this.actual.onNext( this.value );
                                this.actual.onComplete();
                            }
                            finally
                            {
                                this.disposeResource( false );
                            }
                        }

                        return;
                    }
                }
                while ( !STATE.compareAndSet( this, s, 2 ) );
            }
        }

        public void cancel()
        {
            if ( STATE.getAndSet( this, 3 ) != 3 )
            {
                T old = this.value;
                this.value = null;
                Operators.onDiscard( old, this.actual.currentContext() );
                this.disposeResource( true );
            }
        }

        void disposeResource( boolean isCancel )
        {
            Disposable target = isCancel ? MonoCreate.CANCELLED : MonoCreate.TERMINATED;
            Disposable d = this.disposable;
            if ( d != MonoCreate.TERMINATED && d != MonoCreate.CANCELLED )
            {
                d = (Disposable) DISPOSABLE.getAndSet( this, target );
                if ( d != null && d != MonoCreate.TERMINATED && d != MonoCreate.CANCELLED )
                {
                    if ( isCancel && d instanceof FluxCreate.SinkDisposable )
                    {
                        ((FluxCreate.SinkDisposable) d).cancel();
                    }

                    d.dispose();
                }
            }
        }

        public String toString()
        {
            return "MonoSink";
        }

        boolean isDisposed()
        {
            Disposable d = this.disposable;
            return d == MonoCreate.CANCELLED || d == MonoCreate.TERMINATED;
        }
    }
}
