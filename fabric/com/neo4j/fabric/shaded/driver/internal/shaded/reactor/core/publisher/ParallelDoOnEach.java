package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.LongConsumer;

import org.reactivestreams.Subscription;

final class ParallelDoOnEach<T> extends ParallelFlux<T> implements Scannable
{
    final ParallelFlux<T> source;
    final BiConsumer<Context,? super T> onNext;
    final BiConsumer<Context,? super Throwable> onError;
    final Consumer<Context> onComplete;

    ParallelDoOnEach( ParallelFlux<T> source, @Nullable BiConsumer<Context,? super T> onNext, @Nullable BiConsumer<Context,? super Throwable> onError,
            @Nullable Consumer<Context> onComplete )
    {
        this.source = source;
        this.onNext = onNext;
        this.onError = onError;
        this.onComplete = onComplete;
    }

    public void subscribe( CoreSubscriber<? super T>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<? super T>[] parents = new CoreSubscriber[n];
            boolean conditional = subscribers[0] instanceof Fuseable.ConditionalSubscriber;

            for ( int i = 0; i < n; ++i )
            {
                CoreSubscriber<? super T> subscriber = subscribers[i];
                SignalPeek<T> signalPeek = new ParallelDoOnEach.DoOnEachSignalPeek( subscriber.currentContext() );
                if ( conditional )
                {
                    parents[i] = new FluxPeekFuseable.PeekConditionalSubscriber( (Fuseable.ConditionalSubscriber) subscriber, signalPeek );
                }
                else
                {
                    parents[i] = new FluxPeek.PeekSubscriber( subscriber, signalPeek );
                }
            }

            this.source.subscribe( parents );
        }
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    private class DoOnEachSignalPeek implements SignalPeek<T>
    {
        Consumer<? super T> onNextCall;
        Consumer<? super Throwable> onErrorCall;
        Runnable onCompleteCall;

        public DoOnEachSignalPeek( Context ctx )
        {
            this.onNextCall = ParallelDoOnEach.this.onNext != null ? ( v ) -> {
                ParallelDoOnEach.this.onNext.accept( ctx, v );
            } : null;
            this.onErrorCall = ParallelDoOnEach.this.onError != null ? ( e ) -> {
                ParallelDoOnEach.this.onError.accept( ctx, e );
            } : null;
            this.onCompleteCall = ParallelDoOnEach.this.onComplete != null ? () -> {
                ParallelDoOnEach.this.onComplete.accept( ctx );
            } : null;
        }

        public Consumer<? super Subscription> onSubscribeCall()
        {
            return null;
        }

        public Consumer<? super T> onNextCall()
        {
            return this.onNextCall;
        }

        public Consumer<? super Throwable> onErrorCall()
        {
            return this.onErrorCall;
        }

        public Runnable onCompleteCall()
        {
            return this.onCompleteCall;
        }

        public Runnable onAfterTerminateCall()
        {
            return null;
        }

        public LongConsumer onRequestCall()
        {
            return null;
        }

        public Runnable onCancelCall()
        {
            return null;
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            return null;
        }
    }
}
