package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

import org.reactivestreams.Publisher;

final class FluxConcatIterable<T> extends Flux<T> implements SourceProducer<T>
{
    final Iterable<? extends Publisher<? extends T>> iterable;

    FluxConcatIterable( Iterable<? extends Publisher<? extends T>> iterable )
    {
        this.iterable = (Iterable) Objects.requireNonNull( iterable, "iterable" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Iterator it;
        try
        {
            it = (Iterator) Objects.requireNonNull( this.iterable.iterator(), "The Iterator returned is null" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return;
        }

        FluxConcatIterable.ConcatIterableSubscriber<T> parent = new FluxConcatIterable.ConcatIterableSubscriber( actual, it );
        actual.onSubscribe( parent );
        if ( !parent.isCancelled() )
        {
            parent.onComplete();
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class ConcatIterableSubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxConcatIterable.ConcatIterableSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxConcatIterable.ConcatIterableSubscriber.class, "wip" );
        final Iterator<? extends Publisher<? extends T>> it;
        volatile int wip;
        long produced;

        ConcatIterableSubscriber( CoreSubscriber<? super T> actual, Iterator<? extends Publisher<? extends T>> it )
        {
            super( actual );
            this.it = it;
        }

        public void onNext( T t )
        {
            ++this.produced;
            this.actual.onNext( t );
        }

        public void onComplete()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                Iterator a = this.it;

                do
                {
                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    boolean b;
                    try
                    {
                        b = a.hasNext();
                    }
                    catch ( Throwable var7 )
                    {
                        this.onError( Operators.onOperatorError( this, var7, this.actual.currentContext() ) );
                        return;
                    }

                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    if ( !b )
                    {
                        this.actual.onComplete();
                        return;
                    }

                    Publisher p;
                    try
                    {
                        p = (Publisher) Objects.requireNonNull( this.it.next(), "The Publisher returned by the iterator is null" );
                    }
                    catch ( Throwable var6 )
                    {
                        this.actual.onError( Operators.onOperatorError( this, var6, this.actual.currentContext() ) );
                        return;
                    }

                    if ( this.isCancelled() )
                    {
                        return;
                    }

                    long c = this.produced;
                    if ( c != 0L )
                    {
                        this.produced = 0L;
                        this.produced( c );
                    }

                    p.subscribe( this );
                    if ( this.isCancelled() )
                    {
                        return;
                    }
                }
                while ( WIP.decrementAndGet( this ) != 0 );
            }
        }
    }
}
