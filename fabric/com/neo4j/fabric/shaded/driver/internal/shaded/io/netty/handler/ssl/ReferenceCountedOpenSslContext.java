package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.CertificateVerifier;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.SSL;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.SSLContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.internal.tcnative.SSLPrivateKeyMethod;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AbstractReferenceCounted;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCounted;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakDetector;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakDetectorFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakTracker;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.StringUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.SystemPropertyUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.security.AccessController;
import java.security.PrivateKey;
import java.security.PrivilegedAction;
import java.security.SignatureException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateRevokedException;
import java.security.cert.X509Certificate;
import java.security.cert.CertPathValidatorException.BasicReason;
import java.security.cert.CertPathValidatorException.Reason;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;

public abstract class ReferenceCountedOpenSslContext extends SslContext implements ReferenceCounted
{
    protected static final int VERIFY_DEPTH = 10;
    static final boolean USE_TASKS =
            SystemPropertyUtil.getBoolean( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.openssl.useTasks", false );
    static final OpenSslApplicationProtocolNegotiator NONE_PROTOCOL_NEGOTIATOR = new OpenSslApplicationProtocolNegotiator()
    {
        public ApplicationProtocolConfig.Protocol protocol()
        {
            return ApplicationProtocolConfig.Protocol.NONE;
        }

        public List<String> protocols()
        {
            return Collections.emptyList();
        }

        public ApplicationProtocolConfig.SelectorFailureBehavior selectorFailureBehavior()
        {
            return ApplicationProtocolConfig.SelectorFailureBehavior.CHOOSE_MY_LAST_PROTOCOL;
        }

        public ApplicationProtocolConfig.SelectedListenerFailureBehavior selectedListenerFailureBehavior()
        {
            return ApplicationProtocolConfig.SelectedListenerFailureBehavior.ACCEPT;
        }
    };
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( ReferenceCountedOpenSslContext.class );
    private static final int DEFAULT_BIO_NON_APPLICATION_BUFFER_SIZE = (Integer) AccessController.doPrivileged( new PrivilegedAction<Integer>()
    {
        public Integer run()
        {
            return Math.max( 1,
                    SystemPropertyUtil.getInt( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.openssl.bioNonApplicationBufferSize",
                            2048 ) );
        }
    } );
    private static final Integer DH_KEY_LENGTH;
    private static final ResourceLeakDetector<ReferenceCountedOpenSslContext> leakDetector =
            ResourceLeakDetectorFactory.instance().newResourceLeakDetector( ReferenceCountedOpenSslContext.class );

    static
    {
        Integer dhLen = null;

        try
        {
            String dhKeySize = (String) AccessController.doPrivileged( new PrivilegedAction<String>()
            {
                public String run()
                {
                    return SystemPropertyUtil.get( "jdk.tls.ephemeralDHKeySize" );
                }
            } );
            if ( dhKeySize != null )
            {
                try
                {
                    dhLen = Integer.valueOf( dhKeySize );
                }
                catch ( NumberFormatException var3 )
                {
                    logger.debug( "ReferenceCountedOpenSslContext supports -Djdk.tls.ephemeralDHKeySize={int}, but got: " + dhKeySize );
                }
            }
        }
        catch ( Throwable var4 )
        {
        }

        DH_KEY_LENGTH = dhLen;
    }

    final Certificate[] keyCertChain;
    final ClientAuth clientAuth;
    final String[] protocols;
    final boolean enableOcsp;
    final OpenSslEngineMap engineMap;
    final ReadWriteLock ctxLock;
    private final List<String> unmodifiableCiphers;
    private final long sessionCacheSize;
    private final long sessionTimeout;
    private final OpenSslApplicationProtocolNegotiator apn;
    private final int mode;
    private final ResourceLeakTracker<ReferenceCountedOpenSslContext> leak;
    private final AbstractReferenceCounted refCnt;
    protected long ctx;
    private volatile int bioNonApplicationBufferSize;

    ReferenceCountedOpenSslContext( Iterable<String> ciphers, CipherSuiteFilter cipherFilter, ApplicationProtocolConfig apnCfg, long sessionCacheSize,
            long sessionTimeout, int mode, Certificate[] keyCertChain, ClientAuth clientAuth, String[] protocols, boolean startTls, boolean enableOcsp,
            boolean leakDetection ) throws SSLException
    {
        this( ciphers, cipherFilter, toNegotiator( apnCfg ), sessionCacheSize, sessionTimeout, mode, keyCertChain, clientAuth, protocols, startTls, enableOcsp,
                leakDetection );
    }

    ReferenceCountedOpenSslContext( Iterable<String> ciphers, CipherSuiteFilter cipherFilter, OpenSslApplicationProtocolNegotiator apn, long sessionCacheSize,
            long sessionTimeout, int mode, Certificate[] keyCertChain, ClientAuth clientAuth, String[] protocols, boolean startTls, boolean enableOcsp,
            boolean leakDetection ) throws SSLException
    {
        super( startTls );
        this.refCnt = new AbstractReferenceCounted()
        {
            public ReferenceCounted touch( Object hint )
            {
                if ( ReferenceCountedOpenSslContext.this.leak != null )
                {
                    ReferenceCountedOpenSslContext.this.leak.record( hint );
                }

                return ReferenceCountedOpenSslContext.this;
            }

            protected void deallocate()
            {
                ReferenceCountedOpenSslContext.this.destroy();
                if ( ReferenceCountedOpenSslContext.this.leak != null )
                {
                    boolean closed = ReferenceCountedOpenSslContext.this.leak.close( ReferenceCountedOpenSslContext.this );

                    assert closed;
                }
            }
        };
        this.engineMap = new ReferenceCountedOpenSslContext.DefaultOpenSslEngineMap();
        this.ctxLock = new ReentrantReadWriteLock();
        this.bioNonApplicationBufferSize = DEFAULT_BIO_NON_APPLICATION_BUFFER_SIZE;
        OpenSsl.ensureAvailability();
        if ( enableOcsp && !OpenSsl.isOcspSupported() )
        {
            throw new IllegalStateException( "OCSP is not supported." );
        }
        else if ( mode != 1 && mode != 0 )
        {
            throw new IllegalArgumentException( "mode most be either SSL.SSL_MODE_SERVER or SSL.SSL_MODE_CLIENT" );
        }
        else
        {
            this.leak = leakDetection ? leakDetector.track( this ) : null;
            this.mode = mode;
            this.clientAuth = this.isServer() ? (ClientAuth) ObjectUtil.checkNotNull( clientAuth, "clientAuth" ) : ClientAuth.NONE;
            this.protocols = protocols;
            this.enableOcsp = enableOcsp;
            this.keyCertChain = keyCertChain == null ? null : (Certificate[]) keyCertChain.clone();
            this.unmodifiableCiphers = Arrays.asList(
                    ((CipherSuiteFilter) ObjectUtil.checkNotNull( cipherFilter, "cipherFilter" )).filterCipherSuites( ciphers, OpenSsl.DEFAULT_CIPHERS,
                            OpenSsl.availableJavaCipherSuites() ) );
            this.apn = (OpenSslApplicationProtocolNegotiator) ObjectUtil.checkNotNull( apn, "apn" );
            boolean success = false;

            try
            {
                try
                {
                    int protocolOpts = 30;
                    if ( OpenSsl.isTlsv13Supported() )
                    {
                        protocolOpts |= 32;
                    }

                    this.ctx = SSLContext.make( protocolOpts, mode );
                }
                catch ( Exception var30 )
                {
                    throw new SSLException( "failed to create an SSL_CTX", var30 );
                }

                boolean tlsv13Supported = OpenSsl.isTlsv13Supported();
                StringBuilder cipherBuilder = new StringBuilder();
                StringBuilder cipherTLSv13Builder = new StringBuilder();

                try
                {
                    if ( this.unmodifiableCiphers.isEmpty() )
                    {
                        SSLContext.setCipherSuite( this.ctx, "", false );
                        if ( tlsv13Supported )
                        {
                            SSLContext.setCipherSuite( this.ctx, "", true );
                        }
                    }
                    else
                    {
                        CipherSuiteConverter.convertToCipherStrings( this.unmodifiableCiphers, cipherBuilder, cipherTLSv13Builder, OpenSsl.isBoringSSL() );
                        SSLContext.setCipherSuite( this.ctx, cipherBuilder.toString(), false );
                        if ( tlsv13Supported )
                        {
                            SSLContext.setCipherSuite( this.ctx, cipherTLSv13Builder.toString(), true );
                        }
                    }
                }
                catch ( SSLException var28 )
                {
                    throw var28;
                }
                catch ( Exception var29 )
                {
                    throw new SSLException( "failed to set cipher suite: " + this.unmodifiableCiphers, var29 );
                }

                int options = SSLContext.getOptions( this.ctx ) | SSL.SSL_OP_NO_SSLv2 | SSL.SSL_OP_NO_SSLv3 | SSL.SSL_OP_NO_TLSv1_3 |
                        SSL.SSL_OP_CIPHER_SERVER_PREFERENCE | SSL.SSL_OP_NO_COMPRESSION | SSL.SSL_OP_NO_TICKET;
                if ( cipherBuilder.length() == 0 )
                {
                    options |= SSL.SSL_OP_NO_SSLv2 | SSL.SSL_OP_NO_SSLv3 | SSL.SSL_OP_NO_TLSv1 | SSL.SSL_OP_NO_TLSv1_1 | SSL.SSL_OP_NO_TLSv1_2;
                }

                SSLContext.setOptions( this.ctx, options );
                SSLContext.setMode( this.ctx, SSLContext.getMode( this.ctx ) | SSL.SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER );
                if ( DH_KEY_LENGTH != null )
                {
                    SSLContext.setTmpDHLength( this.ctx, DH_KEY_LENGTH );
                }

                List<String> nextProtoList = apn.protocols();
                if ( !nextProtoList.isEmpty() )
                {
                    String[] appProtocols = (String[]) nextProtoList.toArray( new String[0] );
                    int selectorBehavior = opensslSelectorFailureBehavior( apn.selectorFailureBehavior() );
                    switch ( apn.protocol() )
                    {
                    case NPN:
                        SSLContext.setNpnProtos( this.ctx, appProtocols, selectorBehavior );
                        break;
                    case ALPN:
                        SSLContext.setAlpnProtos( this.ctx, appProtocols, selectorBehavior );
                        break;
                    case NPN_AND_ALPN:
                        SSLContext.setNpnProtos( this.ctx, appProtocols, selectorBehavior );
                        SSLContext.setAlpnProtos( this.ctx, appProtocols, selectorBehavior );
                        break;
                    default:
                        throw new Error();
                    }
                }

                if ( sessionCacheSize <= 0L )
                {
                    sessionCacheSize = SSLContext.setSessionCacheSize( this.ctx, 20480L );
                }

                this.sessionCacheSize = sessionCacheSize;
                SSLContext.setSessionCacheSize( this.ctx, sessionCacheSize );
                if ( sessionTimeout <= 0L )
                {
                    sessionTimeout = SSLContext.setSessionCacheTimeout( this.ctx, 300L );
                }

                this.sessionTimeout = sessionTimeout;
                SSLContext.setSessionCacheTimeout( this.ctx, sessionTimeout );
                if ( enableOcsp )
                {
                    SSLContext.enableOcsp( this.ctx, this.isClient() );
                }

                SSLContext.setUseTasks( this.ctx, USE_TASKS );
                success = true;
            }
            finally
            {
                if ( !success )
                {
                    this.release();
                }
            }
        }
    }

    private static int opensslSelectorFailureBehavior( ApplicationProtocolConfig.SelectorFailureBehavior behavior )
    {
        switch ( behavior )
        {
        case NO_ADVERTISE:
            return 0;
        case CHOOSE_MY_LAST_PROTOCOL:
            return 1;
        default:
            throw new Error();
        }
    }

    protected static X509Certificate[] certificates( byte[][] chain )
    {
        X509Certificate[] peerCerts = new X509Certificate[chain.length];

        for ( int i = 0; i < peerCerts.length; ++i )
        {
            peerCerts[i] = new OpenSslX509Certificate( chain[i] );
        }

        return peerCerts;
    }

    protected static X509TrustManager chooseTrustManager( TrustManager[] managers )
    {
        TrustManager[] var1 = managers;
        int var2 = managers.length;

        for ( int var3 = 0; var3 < var2; ++var3 )
        {
            TrustManager m = var1[var3];
            if ( m instanceof X509TrustManager )
            {
                if ( PlatformDependent.javaVersion() >= 7 )
                {
                    return OpenSslX509TrustManagerWrapper.wrapIfNeeded( (X509TrustManager) m );
                }

                return (X509TrustManager) m;
            }
        }

        throw new IllegalStateException( "no X509TrustManager found" );
    }

    protected static X509KeyManager chooseX509KeyManager( KeyManager[] kms )
    {
        KeyManager[] var1 = kms;
        int var2 = kms.length;

        for ( int var3 = 0; var3 < var2; ++var3 )
        {
            KeyManager km = var1[var3];
            if ( km instanceof X509KeyManager )
            {
                return (X509KeyManager) km;
            }
        }

        throw new IllegalStateException( "no X509KeyManager found" );
    }

    static OpenSslApplicationProtocolNegotiator toNegotiator( ApplicationProtocolConfig config )
    {
        if ( config == null )
        {
            return NONE_PROTOCOL_NEGOTIATOR;
        }
        else
        {
            switch ( config.protocol() )
            {
            case NPN:
            case ALPN:
            case NPN_AND_ALPN:
                switch ( config.selectedListenerFailureBehavior() )
                {
                case CHOOSE_MY_LAST_PROTOCOL:
                case ACCEPT:
                    switch ( config.selectorFailureBehavior() )
                    {
                    case NO_ADVERTISE:
                    case CHOOSE_MY_LAST_PROTOCOL:
                        return new OpenSslDefaultApplicationProtocolNegotiator( config );
                    default:
                        throw new UnsupportedOperationException( "OpenSSL provider does not support " + config.selectorFailureBehavior() + " behavior" );
                    }
                default:
                    throw new UnsupportedOperationException( "OpenSSL provider does not support " + config.selectedListenerFailureBehavior() + " behavior" );
                }
            case NONE:
                return NONE_PROTOCOL_NEGOTIATOR;
            default:
                throw new Error();
            }
        }
    }

    static boolean useExtendedTrustManager( X509TrustManager trustManager )
    {
        return PlatformDependent.javaVersion() >= 7 && trustManager instanceof X509ExtendedTrustManager;
    }

    static void setKeyMaterial( long ctx, X509Certificate[] keyCertChain, PrivateKey key, String keyPassword ) throws SSLException
    {
        long keyBio = 0L;
        long keyCertChainBio = 0L;
        long keyCertChainBio2 = 0L;
        PemEncoded encoded = null;

        try
        {
            encoded = PemX509Certificate.toPEM( ByteBufAllocator.DEFAULT, true, keyCertChain );
            keyCertChainBio = toBIO( ByteBufAllocator.DEFAULT, encoded.retain() );
            keyCertChainBio2 = toBIO( ByteBufAllocator.DEFAULT, encoded.retain() );
            if ( key != null )
            {
                keyBio = toBIO( ByteBufAllocator.DEFAULT, key );
            }

            SSLContext.setCertificateBio( ctx, keyCertChainBio, keyBio, keyPassword == null ? "" : keyPassword );
            SSLContext.setCertificateChainBio( ctx, keyCertChainBio2, true );
        }
        catch ( SSLException var17 )
        {
            throw var17;
        }
        catch ( Exception var18 )
        {
            throw new SSLException( "failed to set certificate and key", var18 );
        }
        finally
        {
            freeBio( keyBio );
            freeBio( keyCertChainBio );
            freeBio( keyCertChainBio2 );
            if ( encoded != null )
            {
                encoded.release();
            }
        }
    }

    static void freeBio( long bio )
    {
        if ( bio != 0L )
        {
            SSL.freeBIO( bio );
        }
    }

    static long toBIO( ByteBufAllocator allocator, PrivateKey key ) throws Exception
    {
        if ( key == null )
        {
            return 0L;
        }
        else
        {
            PemEncoded pem = PemPrivateKey.toPEM( allocator, true, key );

            long var3;
            try
            {
                var3 = toBIO( allocator, pem.retain() );
            }
            finally
            {
                pem.release();
            }

            return var3;
        }
    }

    static long toBIO( ByteBufAllocator allocator, X509Certificate... certChain ) throws Exception
    {
        if ( certChain == null )
        {
            return 0L;
        }
        else if ( certChain.length == 0 )
        {
            throw new IllegalArgumentException( "certChain can't be empty" );
        }
        else
        {
            PemEncoded pem = PemX509Certificate.toPEM( allocator, true, certChain );

            long var3;
            try
            {
                var3 = toBIO( allocator, pem.retain() );
            }
            finally
            {
                pem.release();
            }

            return var3;
        }
    }

    static long toBIO( ByteBufAllocator allocator, PemEncoded pem ) throws Exception
    {
        long var4;
        try
        {
            ByteBuf content = pem.content();
            if ( content.isDirect() )
            {
                long var29 = newBIO( content.retainedSlice() );
                return var29;
            }

            ByteBuf buffer = allocator.directBuffer( content.readableBytes() );

            try
            {
                buffer.writeBytes( content, content.readerIndex(), content.readableBytes() );
                var4 = newBIO( buffer.retainedSlice() );
            }
            finally
            {
                try
                {
                    if ( pem.isSensitive() )
                    {
                        SslUtils.zeroout( buffer );
                    }
                }
                finally
                {
                    buffer.release();
                }
            }
        }
        finally
        {
            pem.release();
        }

        return var4;
    }

    private static long newBIO( ByteBuf buffer ) throws Exception
    {
        long var4;
        try
        {
            long bio = SSL.newMemBIO();
            int readable = buffer.readableBytes();
            if ( SSL.bioWrite( bio, OpenSsl.memoryAddress( buffer ) + (long) buffer.readerIndex(), readable ) != readable )
            {
                SSL.freeBIO( bio );
                throw new IllegalStateException( "Could not write data to memory BIO" );
            }

            var4 = bio;
        }
        finally
        {
            buffer.release();
        }

        return var4;
    }

    static OpenSslKeyMaterialProvider providerFor( KeyManagerFactory factory, String password )
    {
        if ( factory instanceof OpenSslX509KeyManagerFactory )
        {
            return ((OpenSslX509KeyManagerFactory) factory).newProvider();
        }
        else
        {
            X509KeyManager keyManager = chooseX509KeyManager( factory.getKeyManagers() );
            return (OpenSslKeyMaterialProvider) (factory instanceof OpenSslCachingX509KeyManagerFactory ? new OpenSslCachingKeyMaterialProvider( keyManager,
                    password ) : new OpenSslKeyMaterialProvider( keyManager, password ));
        }
    }

    public final List<String> cipherSuites()
    {
        return this.unmodifiableCiphers;
    }

    public final long sessionCacheSize()
    {
        return this.sessionCacheSize;
    }

    public final long sessionTimeout()
    {
        return this.sessionTimeout;
    }

    public ApplicationProtocolNegotiator applicationProtocolNegotiator()
    {
        return this.apn;
    }

    public final boolean isClient()
    {
        return this.mode == 0;
    }

    public final SSLEngine newEngine( ByteBufAllocator alloc, String peerHost, int peerPort )
    {
        return this.newEngine0( alloc, peerHost, peerPort, true );
    }

    protected final SslHandler newHandler( ByteBufAllocator alloc, boolean startTls )
    {
        return new SslHandler( this.newEngine0( alloc, (String) null, -1, false ), startTls );
    }

    protected final SslHandler newHandler( ByteBufAllocator alloc, String peerHost, int peerPort, boolean startTls )
    {
        return new SslHandler( this.newEngine0( alloc, peerHost, peerPort, false ), startTls );
    }

    protected SslHandler newHandler( ByteBufAllocator alloc, boolean startTls, Executor executor )
    {
        return new SslHandler( this.newEngine0( alloc, (String) null, -1, false ), startTls, executor );
    }

    protected SslHandler newHandler( ByteBufAllocator alloc, String peerHost, int peerPort, boolean startTls, Executor executor )
    {
        return new SslHandler( this.newEngine0( alloc, peerHost, peerPort, false ), executor );
    }

    SSLEngine newEngine0( ByteBufAllocator alloc, String peerHost, int peerPort, boolean jdkCompatibilityMode )
    {
        return new ReferenceCountedOpenSslEngine( this, alloc, peerHost, peerPort, jdkCompatibilityMode, true );
    }

    public final SSLEngine newEngine( ByteBufAllocator alloc )
    {
        return this.newEngine( alloc, (String) null, -1 );
    }

    /**
     * @deprecated
     */
    @Deprecated
    public final long context()
    {
        return this.sslCtxPointer();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public final OpenSslSessionStats stats()
    {
        return this.sessionContext().stats();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public boolean getRejectRemoteInitiatedRenegotiation()
    {
        return true;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setRejectRemoteInitiatedRenegotiation( boolean rejectRemoteInitiatedRenegotiation )
    {
        if ( !rejectRemoteInitiatedRenegotiation )
        {
            throw new UnsupportedOperationException( "Renegotiation is not supported" );
        }
    }

    public int getBioNonApplicationBufferSize()
    {
        return this.bioNonApplicationBufferSize;
    }

    public void setBioNonApplicationBufferSize( int bioNonApplicationBufferSize )
    {
        this.bioNonApplicationBufferSize = ObjectUtil.checkPositiveOrZero( bioNonApplicationBufferSize, "bioNonApplicationBufferSize" );
    }

    /**
     * @deprecated
     */
    @Deprecated
    public final void setTicketKeys( byte[] keys )
    {
        this.sessionContext().setTicketKeys( keys );
    }

    public abstract OpenSslSessionContext sessionContext();

    /**
     * @deprecated
     */
    @Deprecated
    public final long sslCtxPointer()
    {
        Lock readerLock = this.ctxLock.readLock();
        readerLock.lock();

        long var2;
        try
        {
            var2 = SSLContext.getSslCtx( this.ctx );
        }
        finally
        {
            readerLock.unlock();
        }

        return var2;
    }

    public final void setPrivateKeyMethod( OpenSslPrivateKeyMethod method )
    {
        ObjectUtil.checkNotNull( method, "method" );
        Lock writerLock = this.ctxLock.writeLock();
        writerLock.lock();

        try
        {
            SSLContext.setPrivateKeyMethod( this.ctx, new ReferenceCountedOpenSslContext.PrivateKeyMethod( this.engineMap, method ) );
        }
        finally
        {
            writerLock.unlock();
        }
    }

    private void destroy()
    {
        Lock writerLock = this.ctxLock.writeLock();
        writerLock.lock();

        try
        {
            if ( this.ctx != 0L )
            {
                if ( this.enableOcsp )
                {
                    SSLContext.disableOcsp( this.ctx );
                }

                SSLContext.free( this.ctx );
                this.ctx = 0L;
                OpenSslSessionContext context = this.sessionContext();
                if ( context != null )
                {
                    context.destroy();
                }
            }
        }
        finally
        {
            writerLock.unlock();
        }
    }

    public final int refCnt()
    {
        return this.refCnt.refCnt();
    }

    public final ReferenceCounted retain()
    {
        this.refCnt.retain();
        return this;
    }

    public final ReferenceCounted retain( int increment )
    {
        this.refCnt.retain( increment );
        return this;
    }

    public final ReferenceCounted touch()
    {
        this.refCnt.touch();
        return this;
    }

    public final ReferenceCounted touch( Object hint )
    {
        this.refCnt.touch( hint );
        return this;
    }

    public final boolean release()
    {
        return this.refCnt.release();
    }

    public final boolean release( int decrement )
    {
        return this.refCnt.release( decrement );
    }

    private static final class PrivateKeyMethod implements SSLPrivateKeyMethod
    {
        private final OpenSslEngineMap engineMap;
        private final OpenSslPrivateKeyMethod keyMethod;

        PrivateKeyMethod( OpenSslEngineMap engineMap, OpenSslPrivateKeyMethod keyMethod )
        {
            this.engineMap = engineMap;
            this.keyMethod = keyMethod;
        }

        private static byte[] verifyResult( byte[] result ) throws SignatureException
        {
            if ( result == null )
            {
                throw new SignatureException();
            }
            else
            {
                return result;
            }
        }

        private ReferenceCountedOpenSslEngine retrieveEngine( long ssl ) throws SSLException
        {
            ReferenceCountedOpenSslEngine engine = this.engineMap.get( ssl );
            if ( engine == null )
            {
                throw new SSLException( "Could not find a " + StringUtil.simpleClassName( ReferenceCountedOpenSslEngine.class ) + " for sslPointer " + ssl );
            }
            else
            {
                return engine;
            }
        }

        public byte[] sign( long ssl, int signatureAlgorithm, byte[] digest ) throws Exception
        {
            ReferenceCountedOpenSslEngine engine = this.retrieveEngine( ssl );

            try
            {
                return verifyResult( this.keyMethod.sign( engine, signatureAlgorithm, digest ) );
            }
            catch ( Exception var7 )
            {
                engine.initHandshakeException( var7 );
                throw var7;
            }
        }

        public byte[] decrypt( long ssl, byte[] input ) throws Exception
        {
            ReferenceCountedOpenSslEngine engine = this.retrieveEngine( ssl );

            try
            {
                return verifyResult( this.keyMethod.decrypt( engine, input ) );
            }
            catch ( Exception var6 )
            {
                engine.initHandshakeException( var6 );
                throw var6;
            }
        }
    }

    private static final class DefaultOpenSslEngineMap implements OpenSslEngineMap
    {
        private final Map<Long,ReferenceCountedOpenSslEngine> engines;

        private DefaultOpenSslEngineMap()
        {
            this.engines = PlatformDependent.newConcurrentHashMap();
        }

        public ReferenceCountedOpenSslEngine remove( long ssl )
        {
            return (ReferenceCountedOpenSslEngine) this.engines.remove( ssl );
        }

        public void add( ReferenceCountedOpenSslEngine engine )
        {
            this.engines.put( engine.sslPointer(), engine );
        }

        public ReferenceCountedOpenSslEngine get( long ssl )
        {
            return (ReferenceCountedOpenSslEngine) this.engines.get( ssl );
        }
    }

    abstract static class AbstractCertificateVerifier extends CertificateVerifier
    {
        private final OpenSslEngineMap engineMap;

        AbstractCertificateVerifier( OpenSslEngineMap engineMap )
        {
            this.engineMap = engineMap;
        }

        public final int verify( long ssl, byte[][] chain, String auth )
        {
            X509Certificate[] peerCerts = ReferenceCountedOpenSslContext.certificates( chain );
            ReferenceCountedOpenSslEngine engine = this.engineMap.get( ssl );

            try
            {
                this.verify( engine, peerCerts, auth );
                return CertificateVerifier.X509_V_OK;
            }
            catch ( Throwable var11 )
            {
                ReferenceCountedOpenSslContext.logger.debug( "verification of certificate failed", var11 );
                engine.initHandshakeException( var11 );
                if ( var11 instanceof OpenSslCertificateException )
                {
                    return ((OpenSslCertificateException) var11).errorCode();
                }
                else if ( var11 instanceof CertificateExpiredException )
                {
                    return CertificateVerifier.X509_V_ERR_CERT_HAS_EXPIRED;
                }
                else if ( var11 instanceof CertificateNotYetValidException )
                {
                    return CertificateVerifier.X509_V_ERR_CERT_NOT_YET_VALID;
                }
                else
                {
                    if ( PlatformDependent.javaVersion() >= 7 )
                    {
                        if ( var11 instanceof CertificateRevokedException )
                        {
                            return CertificateVerifier.X509_V_ERR_CERT_REVOKED;
                        }

                        for ( Throwable wrapped = var11.getCause(); wrapped != null; wrapped = wrapped.getCause() )
                        {
                            if ( wrapped instanceof CertPathValidatorException )
                            {
                                CertPathValidatorException ex = (CertPathValidatorException) wrapped;
                                Reason reason = ex.getReason();
                                if ( reason == BasicReason.EXPIRED )
                                {
                                    return CertificateVerifier.X509_V_ERR_CERT_HAS_EXPIRED;
                                }

                                if ( reason == BasicReason.NOT_YET_VALID )
                                {
                                    return CertificateVerifier.X509_V_ERR_CERT_NOT_YET_VALID;
                                }

                                if ( reason == BasicReason.REVOKED )
                                {
                                    return CertificateVerifier.X509_V_ERR_CERT_REVOKED;
                                }
                            }
                        }
                    }

                    return CertificateVerifier.X509_V_ERR_UNSPECIFIED;
                }
            }
        }

        abstract void verify( ReferenceCountedOpenSslEngine var1, X509Certificate[] var2, String var3 ) throws Exception;
    }
}
