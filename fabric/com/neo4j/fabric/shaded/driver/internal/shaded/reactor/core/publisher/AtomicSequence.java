package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.function.LongSupplier;

final class AtomicSequence extends RhsPadding implements LongSupplier, RingBuffer.Sequence
{
    private static final AtomicLongFieldUpdater<Value> UPDATER = AtomicLongFieldUpdater.newUpdater( Value.class, "value" );

    AtomicSequence( long initialValue )
    {
        UPDATER.lazySet( this, initialValue );
    }

    public long getAsLong()
    {
        return this.value;
    }

    public void set( long value )
    {
        UPDATER.set( this, value );
    }

    public boolean compareAndSet( long expectedValue, long newValue )
    {
        return UPDATER.compareAndSet( this, expectedValue, newValue );
    }
}
