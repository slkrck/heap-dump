package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;

final class FluxJust<T> extends Flux<T> implements Fuseable.ScalarCallable<T>, Fuseable, SourceProducer<T>
{
    final T value;

    FluxJust( T value )
    {
        this.value = Objects.requireNonNull( value, "value" );
    }

    public T call() throws Exception
    {
        return this.value;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        actual.onSubscribe( new FluxJust.WeakScalarSubscription( this.value, actual ) );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.BUFFERED ? 1 : null;
    }

    static final class WeakScalarSubscription<T> implements Fuseable.QueueSubscription<T>, InnerProducer<T>
    {
        final T value;
        final CoreSubscriber<? super T> actual;
        boolean terminado;

        WeakScalarSubscription( @Nullable T value, CoreSubscriber<? super T> actual )
        {
            this.value = value;
            this.actual = actual;
        }

        public void request( long elements )
        {
            if ( !this.terminado )
            {
                this.terminado = true;
                if ( this.value != null )
                {
                    this.actual.onNext( this.value );
                }

                this.actual.onComplete();
            }
        }

        public void cancel()
        {
            this.terminado = true;
        }

        public int requestFusion( int requestedMode )
        {
            return (requestedMode & 1) != 0 ? 1 : 0;
        }

        @Nullable
        public T poll()
        {
            if ( !this.terminado )
            {
                this.terminado = true;
                return this.value;
            }
            else
            {
                return null;
            }
        }

        public boolean isEmpty()
        {
            return this.terminado;
        }

        public int size()
        {
            return this.isEmpty() ? 0 : 1;
        }

        public void clear()
        {
            this.terminado = true;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED ? InnerProducer.super.scanUnsafe( key ) : this.terminado;
        }
    }
}
