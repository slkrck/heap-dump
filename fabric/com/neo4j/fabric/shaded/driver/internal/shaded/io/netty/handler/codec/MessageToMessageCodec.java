package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelDuplexHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.TypeParameterMatcher;

import java.util.List;

public abstract class MessageToMessageCodec<INBOUND_IN, OUTBOUND_IN> extends ChannelDuplexHandler
{
    private final TypeParameterMatcher inboundMsgMatcher;
    private final MessageToMessageDecoder<Object> decoder = new MessageToMessageDecoder<Object>()
    {
        public boolean acceptInboundMessage( Object msg ) throws Exception
        {
            return MessageToMessageCodec.this.acceptInboundMessage( msg );
        }

        protected void decode( ChannelHandlerContext ctx, Object msg, List<Object> out ) throws Exception
        {
            MessageToMessageCodec.this.decode( ctx, msg, out );
        }
    };
    private final TypeParameterMatcher outboundMsgMatcher;
    private final MessageToMessageEncoder<Object> encoder = new MessageToMessageEncoder<Object>()
    {
        public boolean acceptOutboundMessage( Object msg ) throws Exception
        {
            return MessageToMessageCodec.this.acceptOutboundMessage( msg );
        }

        protected void encode( ChannelHandlerContext ctx, Object msg, List<Object> out ) throws Exception
        {
            MessageToMessageCodec.this.encode( ctx, msg, out );
        }
    };

    protected MessageToMessageCodec()
    {
        this.inboundMsgMatcher = TypeParameterMatcher.find( this, MessageToMessageCodec.class, "INBOUND_IN" );
        this.outboundMsgMatcher = TypeParameterMatcher.find( this, MessageToMessageCodec.class, "OUTBOUND_IN" );
    }

    protected MessageToMessageCodec( Class<? extends INBOUND_IN> inboundMessageType, Class<? extends OUTBOUND_IN> outboundMessageType )
    {
        this.inboundMsgMatcher = TypeParameterMatcher.get( inboundMessageType );
        this.outboundMsgMatcher = TypeParameterMatcher.get( outboundMessageType );
    }

    public void channelRead( ChannelHandlerContext ctx, Object msg ) throws Exception
    {
        this.decoder.channelRead( ctx, msg );
    }

    public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise ) throws Exception
    {
        this.encoder.write( ctx, msg, promise );
    }

    public boolean acceptInboundMessage( Object msg ) throws Exception
    {
        return this.inboundMsgMatcher.match( msg );
    }

    public boolean acceptOutboundMessage( Object msg ) throws Exception
    {
        return this.outboundMsgMatcher.match( msg );
    }

    protected abstract void encode( ChannelHandlerContext var1, OUTBOUND_IN var2, List<Object> var3 ) throws Exception;

    protected abstract void decode( ChannelHandlerContext var1, INBOUND_IN var2, List<Object> var3 ) throws Exception;
}
