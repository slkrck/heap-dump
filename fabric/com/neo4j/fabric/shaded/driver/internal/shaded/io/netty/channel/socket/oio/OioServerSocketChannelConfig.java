package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket.oio;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.MessageSizeEstimator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.RecvByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.WriteBufferWaterMark;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket.ServerSocketChannelConfig;

/**
 * @deprecated
 */
@Deprecated
public interface OioServerSocketChannelConfig extends ServerSocketChannelConfig
{
    int getSoTimeout();

    OioServerSocketChannelConfig setSoTimeout( int var1 );

    OioServerSocketChannelConfig setBacklog( int var1 );

    OioServerSocketChannelConfig setReuseAddress( boolean var1 );

    OioServerSocketChannelConfig setReceiveBufferSize( int var1 );

    OioServerSocketChannelConfig setPerformancePreferences( int var1, int var2, int var3 );

    OioServerSocketChannelConfig setConnectTimeoutMillis( int var1 );

    /**
     * @deprecated
     */
    @Deprecated
    OioServerSocketChannelConfig setMaxMessagesPerRead( int var1 );

    OioServerSocketChannelConfig setWriteSpinCount( int var1 );

    OioServerSocketChannelConfig setAllocator( ByteBufAllocator var1 );

    OioServerSocketChannelConfig setRecvByteBufAllocator( RecvByteBufAllocator var1 );

    OioServerSocketChannelConfig setAutoRead( boolean var1 );

    OioServerSocketChannelConfig setAutoClose( boolean var1 );

    OioServerSocketChannelConfig setWriteBufferHighWaterMark( int var1 );

    OioServerSocketChannelConfig setWriteBufferLowWaterMark( int var1 );

    OioServerSocketChannelConfig setWriteBufferWaterMark( WriteBufferWaterMark var1 );

    OioServerSocketChannelConfig setMessageSizeEstimator( MessageSizeEstimator var1 );
}
