package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;

import java.util.Collection;

final class EmptyCompositeDisposable implements Disposable.Composite
{
    public boolean add( Disposable d )
    {
        return false;
    }

    public boolean addAll( Collection<? extends Disposable> ds )
    {
        return false;
    }

    public boolean remove( Disposable d )
    {
        return false;
    }

    public int size()
    {
        return 0;
    }

    public void dispose()
    {
    }

    public boolean isDisposed()
    {
        return false;
    }
}
