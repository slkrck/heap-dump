package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

final class InstantPeriodicWorkerTask implements Disposable, Callable<Void>
{
    static final Disposable.Composite DISPOSED = new EmptyCompositeDisposable();
    static final Future<Void> CANCELLED = new FutureTask( () -> {
        return null;
    } );
    static final AtomicReferenceFieldUpdater<InstantPeriodicWorkerTask,Future> REST =
            AtomicReferenceFieldUpdater.newUpdater( InstantPeriodicWorkerTask.class, Future.class, "rest" );
    static final AtomicReferenceFieldUpdater<InstantPeriodicWorkerTask,Future> FIRST =
            AtomicReferenceFieldUpdater.newUpdater( InstantPeriodicWorkerTask.class, Future.class, "first" );
    static final AtomicReferenceFieldUpdater<InstantPeriodicWorkerTask,Disposable.Composite> PARENT =
            AtomicReferenceFieldUpdater.newUpdater( InstantPeriodicWorkerTask.class, Disposable.Composite.class, "parent" );
    final Runnable task;
    final ExecutorService executor;
    volatile Future<?> rest;
    volatile Future<?> first;
    volatile Disposable.Composite parent;
    Thread thread;

    InstantPeriodicWorkerTask( Runnable task, ExecutorService executor )
    {
        this.task = task;
        this.executor = executor;
    }

    InstantPeriodicWorkerTask( Runnable task, ExecutorService executor, Disposable.Composite parent )
    {
        this.task = task;
        this.executor = executor;
        PARENT.lazySet( this, parent );
    }

    @Nullable
    public Void call()
    {
        this.thread = Thread.currentThread();

        try
        {
            this.task.run();
            this.setRest( this.executor.submit( this ) );
        }
        catch ( Throwable var5 )
        {
            Schedulers.handleError( var5 );
        }
        finally
        {
            this.thread = null;
        }

        return null;
    }

    void setRest( Future<?> f )
    {
        Future o;
        do
        {
            o = this.rest;
            if ( o == CANCELLED )
            {
                f.cancel( this.thread != Thread.currentThread() );
                return;
            }
        }
        while ( !REST.compareAndSet( this, o, f ) );
    }

    void setFirst( Future<?> f )
    {
        Future o;
        do
        {
            o = this.first;
            if ( o == CANCELLED )
            {
                f.cancel( this.thread != Thread.currentThread() );
                return;
            }
        }
        while ( !FIRST.compareAndSet( this, o, f ) );
    }

    public boolean isDisposed()
    {
        return this.rest == CANCELLED;
    }

    public void dispose()
    {
        while ( true )
        {
            Future f = this.first;
            if ( f != CANCELLED )
            {
                if ( !FIRST.compareAndSet( this, f, CANCELLED ) )
                {
                    continue;
                }

                if ( f != null )
                {
                    f.cancel( this.thread != Thread.currentThread() );
                }
            }

            while ( true )
            {
                f = this.rest;
                if ( f == CANCELLED )
                {
                    break;
                }

                if ( REST.compareAndSet( this, f, CANCELLED ) )
                {
                    if ( f != null )
                    {
                        f.cancel( this.thread != Thread.currentThread() );
                    }
                    break;
                }
            }

            Disposable.Composite o;
            do
            {
                o = this.parent;
                if ( o == DISPOSED || o == null )
                {
                    return;
                }
            }
            while ( !PARENT.compareAndSet( this, o, DISPOSED ) );

            o.remove( this );
            return;
        }
    }
}
