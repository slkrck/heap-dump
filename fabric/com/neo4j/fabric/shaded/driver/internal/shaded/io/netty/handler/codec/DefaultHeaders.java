package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.HashingStrategy;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.MathUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Map.Entry;

public class DefaultHeaders<K, V, T extends Headers<K,V,T>> implements Headers<K,V,T>
{
    static final int HASH_CODE_SEED = -1028477387;
    protected final DefaultHeaders.HeaderEntry<K,V> head;
    private final DefaultHeaders.HeaderEntry<K,V>[] entries;
    private final byte hashMask;
    private final ValueConverter<V> valueConverter;
    private final DefaultHeaders.NameValidator<K> nameValidator;
    private final HashingStrategy<K> hashingStrategy;
    int size;

    public DefaultHeaders( ValueConverter<V> valueConverter )
    {
        this( HashingStrategy.JAVA_HASHER, valueConverter );
    }

    public DefaultHeaders( ValueConverter<V> valueConverter, DefaultHeaders.NameValidator<K> nameValidator )
    {
        this( HashingStrategy.JAVA_HASHER, valueConverter, nameValidator );
    }

    public DefaultHeaders( HashingStrategy<K> nameHashingStrategy, ValueConverter<V> valueConverter )
    {
        this( nameHashingStrategy, valueConverter, DefaultHeaders.NameValidator.NOT_NULL );
    }

    public DefaultHeaders( HashingStrategy<K> nameHashingStrategy, ValueConverter<V> valueConverter, DefaultHeaders.NameValidator<K> nameValidator )
    {
        this( nameHashingStrategy, valueConverter, nameValidator, 16 );
    }

    public DefaultHeaders( HashingStrategy<K> nameHashingStrategy, ValueConverter<V> valueConverter, DefaultHeaders.NameValidator<K> nameValidator,
            int arraySizeHint )
    {
        this.valueConverter = (ValueConverter) ObjectUtil.checkNotNull( valueConverter, "valueConverter" );
        this.nameValidator = (DefaultHeaders.NameValidator) ObjectUtil.checkNotNull( nameValidator, "nameValidator" );
        this.hashingStrategy = (HashingStrategy) ObjectUtil.checkNotNull( nameHashingStrategy, "nameHashingStrategy" );
        this.entries = new DefaultHeaders.HeaderEntry[MathUtil.findNextPositivePowerOfTwo( Math.max( 2, Math.min( arraySizeHint, 128 ) ) )];
        this.hashMask = (byte) (this.entries.length - 1);
        this.head = new DefaultHeaders.HeaderEntry();
    }

    public V get( K name )
    {
        ObjectUtil.checkNotNull( name, "name" );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        DefaultHeaders.HeaderEntry<K,V> e = this.entries[i];

        Object value;
        for ( value = null; e != null; e = e.next )
        {
            if ( e.hash == h && this.hashingStrategy.equals( name, e.key ) )
            {
                value = e.value;
            }
        }

        return value;
    }

    public V get( K name, V defaultValue )
    {
        V value = this.get( name );
        return value == null ? defaultValue : value;
    }

    public V getAndRemove( K name )
    {
        int h = this.hashingStrategy.hashCode( name );
        return this.remove0( h, this.index( h ), ObjectUtil.checkNotNull( name, "name" ) );
    }

    public V getAndRemove( K name, V defaultValue )
    {
        V value = this.getAndRemove( name );
        return value == null ? defaultValue : value;
    }

    public List<V> getAll( K name )
    {
        ObjectUtil.checkNotNull( name, "name" );
        LinkedList<V> values = new LinkedList();
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );

        for ( DefaultHeaders.HeaderEntry e = this.entries[i]; e != null; e = e.next )
        {
            if ( e.hash == h && this.hashingStrategy.equals( name, e.key ) )
            {
                values.addFirst( e.getValue() );
            }
        }

        return values;
    }

    public Iterator<V> valueIterator( K name )
    {
        return new DefaultHeaders.ValueIterator( name );
    }

    public List<V> getAllAndRemove( K name )
    {
        List<V> all = this.getAll( name );
        this.remove( name );
        return all;
    }

    public boolean contains( K name )
    {
        return this.get( name ) != null;
    }

    public boolean containsObject( K name, Object value )
    {
        return this.contains( name, this.valueConverter.convertObject( ObjectUtil.checkNotNull( value, "value" ) ) );
    }

    public boolean containsBoolean( K name, boolean value )
    {
        return this.contains( name, this.valueConverter.convertBoolean( value ) );
    }

    public boolean containsByte( K name, byte value )
    {
        return this.contains( name, this.valueConverter.convertByte( value ) );
    }

    public boolean containsChar( K name, char value )
    {
        return this.contains( name, this.valueConverter.convertChar( value ) );
    }

    public boolean containsShort( K name, short value )
    {
        return this.contains( name, this.valueConverter.convertShort( value ) );
    }

    public boolean containsInt( K name, int value )
    {
        return this.contains( name, this.valueConverter.convertInt( value ) );
    }

    public boolean containsLong( K name, long value )
    {
        return this.contains( name, this.valueConverter.convertLong( value ) );
    }

    public boolean containsFloat( K name, float value )
    {
        return this.contains( name, this.valueConverter.convertFloat( value ) );
    }

    public boolean containsDouble( K name, double value )
    {
        return this.contains( name, this.valueConverter.convertDouble( value ) );
    }

    public boolean containsTimeMillis( K name, long value )
    {
        return this.contains( name, this.valueConverter.convertTimeMillis( value ) );
    }

    public boolean contains( K name, V value )
    {
        return this.contains( name, value, HashingStrategy.JAVA_HASHER );
    }

    public final boolean contains( K name, V value, HashingStrategy<? super V> valueHashingStrategy )
    {
        ObjectUtil.checkNotNull( name, "name" );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );

        for ( DefaultHeaders.HeaderEntry e = this.entries[i]; e != null; e = e.next )
        {
            if ( e.hash == h && this.hashingStrategy.equals( name, e.key ) && valueHashingStrategy.equals( value, e.value ) )
            {
                return true;
            }
        }

        return false;
    }

    public int size()
    {
        return this.size;
    }

    public boolean isEmpty()
    {
        return this.head == this.head.after;
    }

    public Set<K> names()
    {
        if ( this.isEmpty() )
        {
            return Collections.emptySet();
        }
        else
        {
            Set<K> names = new LinkedHashSet( this.size() );

            for ( DefaultHeaders.HeaderEntry e = this.head.after; e != this.head; e = e.after )
            {
                names.add( e.getKey() );
            }

            return names;
        }
    }

    public T add( K name, V value )
    {
        this.nameValidator.validateName( name );
        ObjectUtil.checkNotNull( value, "value" );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        this.add0( h, i, name, value );
        return this.thisT();
    }

    public T add( K name, Iterable<? extends V> values )
    {
        this.nameValidator.validateName( name );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        Iterator var5 = values.iterator();

        while ( var5.hasNext() )
        {
            V v = var5.next();
            this.add0( h, i, name, v );
        }

        return this.thisT();
    }

    public T add( K name, V... values )
    {
        this.nameValidator.validateName( name );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        Object[] var5 = values;
        int var6 = values.length;

        for ( int var7 = 0; var7 < var6; ++var7 )
        {
            V v = var5[var7];
            this.add0( h, i, name, v );
        }

        return this.thisT();
    }

    public T addObject( K name, Object value )
    {
        return this.add( name, this.valueConverter.convertObject( ObjectUtil.checkNotNull( value, "value" ) ) );
    }

    public T addObject( K name, Iterable<?> values )
    {
        Iterator var3 = values.iterator();

        while ( var3.hasNext() )
        {
            Object value = var3.next();
            this.addObject( name, value );
        }

        return this.thisT();
    }

    public T addObject( K name, Object... values )
    {
        Object[] var3 = values;
        int var4 = values.length;

        for ( int var5 = 0; var5 < var4; ++var5 )
        {
            Object value = var3[var5];
            this.addObject( name, value );
        }

        return this.thisT();
    }

    public T addInt( K name, int value )
    {
        return this.add( name, this.valueConverter.convertInt( value ) );
    }

    public T addLong( K name, long value )
    {
        return this.add( name, this.valueConverter.convertLong( value ) );
    }

    public T addDouble( K name, double value )
    {
        return this.add( name, this.valueConverter.convertDouble( value ) );
    }

    public T addTimeMillis( K name, long value )
    {
        return this.add( name, this.valueConverter.convertTimeMillis( value ) );
    }

    public T addChar( K name, char value )
    {
        return this.add( name, this.valueConverter.convertChar( value ) );
    }

    public T addBoolean( K name, boolean value )
    {
        return this.add( name, this.valueConverter.convertBoolean( value ) );
    }

    public T addFloat( K name, float value )
    {
        return this.add( name, this.valueConverter.convertFloat( value ) );
    }

    public T addByte( K name, byte value )
    {
        return this.add( name, this.valueConverter.convertByte( value ) );
    }

    public T addShort( K name, short value )
    {
        return this.add( name, this.valueConverter.convertShort( value ) );
    }

    public T add( Headers<? extends K,? extends V,?> headers )
    {
        if ( headers == this )
        {
            throw new IllegalArgumentException( "can't add to itself." );
        }
        else
        {
            this.addImpl( headers );
            return this.thisT();
        }
    }

    protected void addImpl( Headers<? extends K,? extends V,?> headers )
    {
        if ( headers instanceof DefaultHeaders )
        {
            DefaultHeaders<? extends K,? extends V,T> defaultHeaders = (DefaultHeaders) headers;
            DefaultHeaders.HeaderEntry<? extends K,? extends V> e = defaultHeaders.head.after;
            if ( defaultHeaders.hashingStrategy == this.hashingStrategy && defaultHeaders.nameValidator == this.nameValidator )
            {
                while ( e != defaultHeaders.head )
                {
                    this.add0( e.hash, this.index( e.hash ), e.key, e.value );
                    e = e.after;
                }
            }
            else
            {
                while ( e != defaultHeaders.head )
                {
                    this.add( e.key, e.value );
                    e = e.after;
                }
            }
        }
        else
        {
            Iterator var4 = headers.iterator();

            while ( var4.hasNext() )
            {
                Entry<? extends K,? extends V> header = (Entry) var4.next();
                this.add( header.getKey(), header.getValue() );
            }
        }
    }

    public T set( K name, V value )
    {
        this.nameValidator.validateName( name );
        ObjectUtil.checkNotNull( value, "value" );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        this.remove0( h, i, name );
        this.add0( h, i, name, value );
        return this.thisT();
    }

    public T set( K name, Iterable<? extends V> values )
    {
        this.nameValidator.validateName( name );
        ObjectUtil.checkNotNull( values, "values" );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        this.remove0( h, i, name );
        Iterator var5 = values.iterator();

        while ( var5.hasNext() )
        {
            V v = var5.next();
            if ( v == null )
            {
                break;
            }

            this.add0( h, i, name, v );
        }

        return this.thisT();
    }

    public T set( K name, V... values )
    {
        this.nameValidator.validateName( name );
        ObjectUtil.checkNotNull( values, "values" );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        this.remove0( h, i, name );
        Object[] var5 = values;
        int var6 = values.length;

        for ( int var7 = 0; var7 < var6; ++var7 )
        {
            V v = var5[var7];
            if ( v == null )
            {
                break;
            }

            this.add0( h, i, name, v );
        }

        return this.thisT();
    }

    public T setObject( K name, Object value )
    {
        ObjectUtil.checkNotNull( value, "value" );
        V convertedValue = ObjectUtil.checkNotNull( this.valueConverter.convertObject( value ), "convertedValue" );
        return this.set( name, convertedValue );
    }

    public T setObject( K name, Iterable<?> values )
    {
        this.nameValidator.validateName( name );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        this.remove0( h, i, name );
        Iterator var5 = values.iterator();

        while ( var5.hasNext() )
        {
            Object v = var5.next();
            if ( v == null )
            {
                break;
            }

            this.add0( h, i, name, this.valueConverter.convertObject( v ) );
        }

        return this.thisT();
    }

    public T setObject( K name, Object... values )
    {
        this.nameValidator.validateName( name );
        int h = this.hashingStrategy.hashCode( name );
        int i = this.index( h );
        this.remove0( h, i, name );
        Object[] var5 = values;
        int var6 = values.length;

        for ( int var7 = 0; var7 < var6; ++var7 )
        {
            Object v = var5[var7];
            if ( v == null )
            {
                break;
            }

            this.add0( h, i, name, this.valueConverter.convertObject( v ) );
        }

        return this.thisT();
    }

    public T setInt( K name, int value )
    {
        return this.set( name, this.valueConverter.convertInt( value ) );
    }

    public T setLong( K name, long value )
    {
        return this.set( name, this.valueConverter.convertLong( value ) );
    }

    public T setDouble( K name, double value )
    {
        return this.set( name, this.valueConverter.convertDouble( value ) );
    }

    public T setTimeMillis( K name, long value )
    {
        return this.set( name, this.valueConverter.convertTimeMillis( value ) );
    }

    public T setFloat( K name, float value )
    {
        return this.set( name, this.valueConverter.convertFloat( value ) );
    }

    public T setChar( K name, char value )
    {
        return this.set( name, this.valueConverter.convertChar( value ) );
    }

    public T setBoolean( K name, boolean value )
    {
        return this.set( name, this.valueConverter.convertBoolean( value ) );
    }

    public T setByte( K name, byte value )
    {
        return this.set( name, this.valueConverter.convertByte( value ) );
    }

    public T setShort( K name, short value )
    {
        return this.set( name, this.valueConverter.convertShort( value ) );
    }

    public T set( Headers<? extends K,? extends V,?> headers )
    {
        if ( headers != this )
        {
            this.clear();
            this.addImpl( headers );
        }

        return this.thisT();
    }

    public T setAll( Headers<? extends K,? extends V,?> headers )
    {
        if ( headers != this )
        {
            Iterator var2 = headers.names().iterator();

            while ( var2.hasNext() )
            {
                K key = var2.next();
                this.remove( key );
            }

            this.addImpl( headers );
        }

        return this.thisT();
    }

    public boolean remove( K name )
    {
        return this.getAndRemove( name ) != null;
    }

    public T clear()
    {
        Arrays.fill( this.entries, (Object) null );
        this.head.before = this.head.after = this.head;
        this.size = 0;
        return this.thisT();
    }

    public Iterator<Entry<K,V>> iterator()
    {
        return new DefaultHeaders.HeaderIterator();
    }

    public Boolean getBoolean( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToBoolean( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public boolean getBoolean( K name, boolean defaultValue )
    {
        Boolean v = this.getBoolean( name );
        return v != null ? v : defaultValue;
    }

    public Byte getByte( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToByte( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public byte getByte( K name, byte defaultValue )
    {
        Byte v = this.getByte( name );
        return v != null ? v : defaultValue;
    }

    public Character getChar( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToChar( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public char getChar( K name, char defaultValue )
    {
        Character v = this.getChar( name );
        return v != null ? v : defaultValue;
    }

    public Short getShort( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToShort( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public short getShort( K name, short defaultValue )
    {
        Short v = this.getShort( name );
        return v != null ? v : defaultValue;
    }

    public Integer getInt( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToInt( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public int getInt( K name, int defaultValue )
    {
        Integer v = this.getInt( name );
        return v != null ? v : defaultValue;
    }

    public Long getLong( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToLong( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public long getLong( K name, long defaultValue )
    {
        Long v = this.getLong( name );
        return v != null ? v : defaultValue;
    }

    public Float getFloat( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToFloat( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public float getFloat( K name, float defaultValue )
    {
        Float v = this.getFloat( name );
        return v != null ? v : defaultValue;
    }

    public Double getDouble( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToDouble( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public double getDouble( K name, double defaultValue )
    {
        Double v = this.getDouble( name );
        return v != null ? v : defaultValue;
    }

    public Long getTimeMillis( K name )
    {
        Object v = this.get( name );

        try
        {
            return v != null ? this.valueConverter.convertToTimeMillis( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public long getTimeMillis( K name, long defaultValue )
    {
        Long v = this.getTimeMillis( name );
        return v != null ? v : defaultValue;
    }

    public Boolean getBooleanAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToBoolean( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public boolean getBooleanAndRemove( K name, boolean defaultValue )
    {
        Boolean v = this.getBooleanAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Byte getByteAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToByte( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public byte getByteAndRemove( K name, byte defaultValue )
    {
        Byte v = this.getByteAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Character getCharAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToChar( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public char getCharAndRemove( K name, char defaultValue )
    {
        Character v = this.getCharAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Short getShortAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToShort( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public short getShortAndRemove( K name, short defaultValue )
    {
        Short v = this.getShortAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Integer getIntAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToInt( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public int getIntAndRemove( K name, int defaultValue )
    {
        Integer v = this.getIntAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Long getLongAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToLong( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public long getLongAndRemove( K name, long defaultValue )
    {
        Long v = this.getLongAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Float getFloatAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToFloat( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public float getFloatAndRemove( K name, float defaultValue )
    {
        Float v = this.getFloatAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Double getDoubleAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToDouble( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public double getDoubleAndRemove( K name, double defaultValue )
    {
        Double v = this.getDoubleAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public Long getTimeMillisAndRemove( K name )
    {
        Object v = this.getAndRemove( name );

        try
        {
            return v != null ? this.valueConverter.convertToTimeMillis( v ) : null;
        }
        catch ( RuntimeException var4 )
        {
            return null;
        }
    }

    public long getTimeMillisAndRemove( K name, long defaultValue )
    {
        Long v = this.getTimeMillisAndRemove( name );
        return v != null ? v : defaultValue;
    }

    public boolean equals( Object o )
    {
        return !(o instanceof Headers) ? false : this.equals( (Headers) o, HashingStrategy.JAVA_HASHER );
    }

    public int hashCode()
    {
        return this.hashCode( HashingStrategy.JAVA_HASHER );
    }

    public final boolean equals( Headers<K,V,?> h2, HashingStrategy<V> valueHashingStrategy )
    {
        if ( h2.size() != this.size() )
        {
            return false;
        }
        else if ( this == h2 )
        {
            return true;
        }
        else
        {
            Iterator var3 = this.names().iterator();

            while ( var3.hasNext() )
            {
                K name = var3.next();
                List<V> otherValues = h2.getAll( name );
                List<V> values = this.getAll( name );
                if ( otherValues.size() != values.size() )
                {
                    return false;
                }

                for ( int i = 0; i < otherValues.size(); ++i )
                {
                    if ( !valueHashingStrategy.equals( otherValues.get( i ), values.get( i ) ) )
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }

    public final int hashCode( HashingStrategy<V> valueHashingStrategy )
    {
        int result = -1028477387;
        Iterator var3 = this.names().iterator();

        while ( var3.hasNext() )
        {
            K name = var3.next();
            result = 31 * result + this.hashingStrategy.hashCode( name );
            List<V> values = this.getAll( name );

            for ( int i = 0; i < values.size(); ++i )
            {
                result = 31 * result + valueHashingStrategy.hashCode( values.get( i ) );
            }
        }

        return result;
    }

    public String toString()
    {
        return HeadersUtils.toString( this.getClass(), this.iterator(), this.size() );
    }

    protected DefaultHeaders.HeaderEntry<K,V> newHeaderEntry( int h, K name, V value, DefaultHeaders.HeaderEntry<K,V> next )
    {
        return new DefaultHeaders.HeaderEntry( h, name, value, next, this.head );
    }

    protected ValueConverter<V> valueConverter()
    {
        return this.valueConverter;
    }

    private int index( int hash )
    {
        return hash & this.hashMask;
    }

    private void add0( int h, int i, K name, V value )
    {
        this.entries[i] = this.newHeaderEntry( h, name, value, this.entries[i] );
        ++this.size;
    }

    private V remove0( int h, int i, K name )
    {
        DefaultHeaders.HeaderEntry<K,V> e = this.entries[i];
        if ( e == null )
        {
            return null;
        }
        else
        {
            V value = null;

            for ( DefaultHeaders.HeaderEntry next = e.next; next != null; next = e.next )
            {
                if ( next.hash == h && this.hashingStrategy.equals( name, next.key ) )
                {
                    value = next.value;
                    e.next = next.next;
                    next.remove();
                    --this.size;
                }
                else
                {
                    e = next;
                }
            }

            e = this.entries[i];
            if ( e.hash == h && this.hashingStrategy.equals( name, e.key ) )
            {
                if ( value == null )
                {
                    value = e.value;
                }

                this.entries[i] = e.next;
                e.remove();
                --this.size;
            }

            return value;
        }
    }

    private DefaultHeaders.HeaderEntry<K,V> remove0( DefaultHeaders.HeaderEntry<K,V> entry, DefaultHeaders.HeaderEntry<K,V> previous )
    {
        int i = this.index( entry.hash );
        DefaultHeaders.HeaderEntry<K,V> e = this.entries[i];
        if ( e == entry )
        {
            this.entries[i] = entry.next;
            previous = this.entries[i];
        }
        else
        {
            previous.next = entry.next;
        }

        entry.remove();
        --this.size;
        return previous;
    }

    private T thisT()
    {
        return this;
    }

    public DefaultHeaders<K,V,T> copy()
    {
        DefaultHeaders<K,V,T> copy = new DefaultHeaders( this.hashingStrategy, this.valueConverter, this.nameValidator, this.entries.length );
        copy.addImpl( this );
        return copy;
    }

    public interface NameValidator<K>
    {
        DefaultHeaders.NameValidator NOT_NULL = new DefaultHeaders.NameValidator()
        {
            public void validateName( Object name )
            {
                ObjectUtil.checkNotNull( name, "name" );
            }
        };

        void validateName( K var1 );
    }

    protected static class HeaderEntry<K, V> implements Entry<K,V>
    {
        protected final int hash;
        protected final K key;
        protected V value;
        protected DefaultHeaders.HeaderEntry<K,V> next;
        protected DefaultHeaders.HeaderEntry<K,V> before;
        protected DefaultHeaders.HeaderEntry<K,V> after;

        protected HeaderEntry( int hash, K key )
        {
            this.hash = hash;
            this.key = key;
        }

        HeaderEntry( int hash, K key, V value, DefaultHeaders.HeaderEntry<K,V> next, DefaultHeaders.HeaderEntry<K,V> head )
        {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
            this.after = head;
            this.before = head.before;
            this.pointNeighborsToThis();
        }

        HeaderEntry()
        {
            this.hash = -1;
            this.key = null;
            this.before = this.after = this;
        }

        protected final void pointNeighborsToThis()
        {
            this.before.after = this;
            this.after.before = this;
        }

        public final DefaultHeaders.HeaderEntry<K,V> before()
        {
            return this.before;
        }

        public final DefaultHeaders.HeaderEntry<K,V> after()
        {
            return this.after;
        }

        protected void remove()
        {
            this.before.after = this.after;
            this.after.before = this.before;
        }

        public final K getKey()
        {
            return this.key;
        }

        public final V getValue()
        {
            return this.value;
        }

        public final V setValue( V value )
        {
            ObjectUtil.checkNotNull( value, "value" );
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        public final String toString()
        {
            return this.key.toString() + '=' + this.value.toString();
        }

        public boolean equals( Object o )
        {
            if ( !(o instanceof Entry) )
            {
                return false;
            }
            else
            {
                boolean var10000;
                label38:
                {
                    label27:
                    {
                        Entry<?,?> other = (Entry) o;
                        if ( this.getKey() == null )
                        {
                            if ( other.getKey() != null )
                            {
                                break label27;
                            }
                        }
                        else if ( !this.getKey().equals( other.getKey() ) )
                        {
                            break label27;
                        }

                        if ( this.getValue() == null )
                        {
                            if ( other.getValue() == null )
                            {
                                break label38;
                            }
                        }
                        else if ( this.getValue().equals( other.getValue() ) )
                        {
                            break label38;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }

                var10000 = true;
                return var10000;
            }
        }

        public int hashCode()
        {
            return (this.key == null ? 0 : this.key.hashCode()) ^ (this.value == null ? 0 : this.value.hashCode());
        }
    }

    private final class ValueIterator implements Iterator<V>
    {
        private final K name;
        private final int hash;
        private DefaultHeaders.HeaderEntry<K,V> removalPrevious;
        private DefaultHeaders.HeaderEntry<K,V> previous;
        private DefaultHeaders.HeaderEntry<K,V> next;

        ValueIterator( K name )
        {
            this.name = ObjectUtil.checkNotNull( name, "name" );
            this.hash = DefaultHeaders.this.hashingStrategy.hashCode( name );
            this.calculateNext( DefaultHeaders.this.entries[DefaultHeaders.this.index( this.hash )] );
        }

        public boolean hasNext()
        {
            return this.next != null;
        }

        public V next()
        {
            if ( !this.hasNext() )
            {
                throw new NoSuchElementException();
            }
            else
            {
                if ( this.previous != null )
                {
                    this.removalPrevious = this.previous;
                }

                this.previous = this.next;
                this.calculateNext( this.next.next );
                return this.previous.value;
            }
        }

        public void remove()
        {
            if ( this.previous == null )
            {
                throw new IllegalStateException();
            }
            else
            {
                this.removalPrevious = DefaultHeaders.this.remove0( this.previous, this.removalPrevious );
                this.previous = null;
            }
        }

        private void calculateNext( DefaultHeaders.HeaderEntry<K,V> entry )
        {
            while ( entry != null )
            {
                if ( entry.hash == this.hash && DefaultHeaders.this.hashingStrategy.equals( this.name, entry.key ) )
                {
                    this.next = entry;
                    return;
                }

                entry = entry.next;
            }

            this.next = null;
        }
    }

    private final class HeaderIterator implements Iterator<Entry<K,V>>
    {
        private DefaultHeaders.HeaderEntry<K,V> current;

        private HeaderIterator()
        {
            this.current = DefaultHeaders.this.head;
        }

        public boolean hasNext()
        {
            return this.current.after != DefaultHeaders.this.head;
        }

        public Entry<K,V> next()
        {
            this.current = this.current.after;
            if ( this.current == DefaultHeaders.this.head )
            {
                throw new NoSuchElementException();
            }
            else
            {
                return this.current;
            }
        }

        public void remove()
        {
            throw new UnsupportedOperationException( "read only" );
        }
    }
}
