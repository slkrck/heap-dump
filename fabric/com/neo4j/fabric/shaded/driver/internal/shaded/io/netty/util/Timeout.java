package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

public interface Timeout
{
    Timer timer();

    TimerTask task();

    boolean isExpired();

    boolean isCancelled();

    boolean cancel();
}
