package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

final class WorkerTask implements Runnable, Disposable, Callable<Void>
{
    static final Disposable.Composite DISPOSED = new EmptyCompositeDisposable();
    static final Disposable.Composite DONE = new EmptyCompositeDisposable();
    static final Future<Void> FINISHED = new FutureTask( () -> {
        return null;
    } );
    static final Future<Void> SYNC_CANCELLED = new FutureTask( () -> {
        return null;
    } );
    static final Future<Void> ASYNC_CANCELLED = new FutureTask( () -> {
        return null;
    } );
    static final AtomicReferenceFieldUpdater<WorkerTask,Future> FUTURE = AtomicReferenceFieldUpdater.newUpdater( WorkerTask.class, Future.class, "future" );
    static final AtomicReferenceFieldUpdater<WorkerTask,Disposable.Composite> PARENT =
            AtomicReferenceFieldUpdater.newUpdater( WorkerTask.class, Disposable.Composite.class, "parent" );
    static final AtomicReferenceFieldUpdater<WorkerTask,Thread> THREAD = AtomicReferenceFieldUpdater.newUpdater( WorkerTask.class, Thread.class, "thread" );
    final Runnable task;
    volatile Future<?> future;
    volatile Disposable.Composite parent;
    volatile Thread thread;

    WorkerTask( Runnable task, Disposable.Composite parent )
    {
        this.task = task;
        PARENT.lazySet( this, parent );
    }

    @Nullable
    public Void call()
    {
        THREAD.lazySet( this, Thread.currentThread() );
        boolean var8 = false;

        try
        {
            var8 = true;
            this.task.run();
            var8 = false;
        }
        catch ( Throwable var9 )
        {
            Schedulers.handleError( var9 );
            var8 = false;
        }
        finally
        {
            if ( var8 )
            {
                THREAD.lazySet( this, (Object) null );
                Disposable.Composite o = this.parent;
                if ( o != DISPOSED && PARENT.compareAndSet( this, o, DONE ) && o != null )
                {
                    o.remove( this );
                }

                Future f;
                do
                {
                    f = this.future;
                }
                while ( f != SYNC_CANCELLED && f != ASYNC_CANCELLED && !FUTURE.compareAndSet( this, f, FINISHED ) );
            }
        }

        THREAD.lazySet( this, (Object) null );
        Disposable.Composite o = this.parent;
        if ( o != DISPOSED && PARENT.compareAndSet( this, o, DONE ) && o != null )
        {
            o.remove( this );
        }

        Future f;
        do
        {
            f = this.future;
        }
        while ( f != SYNC_CANCELLED && f != ASYNC_CANCELLED && !FUTURE.compareAndSet( this, f, FINISHED ) );

        return null;
    }

    public void run()
    {
        this.call();
    }

    void setFuture( Future<?> f )
    {
        Future o;
        do
        {
            o = this.future;
            if ( o == FINISHED )
            {
                return;
            }

            if ( o == SYNC_CANCELLED )
            {
                f.cancel( false );
                return;
            }

            if ( o == ASYNC_CANCELLED )
            {
                f.cancel( true );
                return;
            }
        }
        while ( !FUTURE.compareAndSet( this, o, f ) );
    }

    public boolean isDisposed()
    {
        Disposable.Composite o = (Disposable.Composite) PARENT.get( this );
        return o == DISPOSED || o == DONE;
    }

    public void dispose()
    {
        while ( true )
        {
            Future f = this.future;
            if ( f != FINISHED && f != SYNC_CANCELLED && f != ASYNC_CANCELLED )
            {
                boolean async = this.thread != Thread.currentThread();
                if ( !FUTURE.compareAndSet( this, f, async ? ASYNC_CANCELLED : SYNC_CANCELLED ) )
                {
                    continue;
                }

                if ( f != null )
                {
                    f.cancel( async );
                }
            }

            Disposable.Composite o;
            do
            {
                o = this.parent;
                if ( o == DONE || o == DISPOSED || o == null )
                {
                    return;
                }
            }
            while ( !PARENT.compareAndSet( this, o, DISPOSED ) );

            o.remove( this );
            return;
        }
    }
}
