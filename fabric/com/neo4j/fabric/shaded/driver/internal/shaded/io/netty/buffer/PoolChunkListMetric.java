package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

public interface PoolChunkListMetric extends Iterable<PoolChunkMetric>
{
    int minUsage();

    int maxUsage();
}
