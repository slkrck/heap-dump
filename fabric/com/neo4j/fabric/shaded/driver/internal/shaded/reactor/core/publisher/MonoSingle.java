package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.NoSuchElementException;
import java.util.Objects;

import org.reactivestreams.Subscription;

final class MonoSingle<T> extends MonoFromFluxOperator<T,T> implements Fuseable
{
    final T defaultValue;
    final boolean completeOnEmpty;

    MonoSingle( Flux<? extends T> source )
    {
        super( source );
        this.defaultValue = null;
        this.completeOnEmpty = false;
    }

    MonoSingle( Flux<? extends T> source, @Nullable T defaultValue, boolean completeOnEmpty )
    {
        super( source );
        this.defaultValue = completeOnEmpty ? defaultValue : Objects.requireNonNull( defaultValue, "defaultValue" );
        this.completeOnEmpty = completeOnEmpty;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoSingle.SingleSubscriber( actual, this.defaultValue, this.completeOnEmpty );
    }

    static final class SingleSubscriber<T> extends Operators.MonoSubscriber<T,T>
    {
        @Nullable
        final T defaultValue;
        final boolean completeOnEmpty;
        Subscription s;
        int count;
        boolean done;

        SingleSubscriber( CoreSubscriber<? super T> actual, @Nullable T defaultValue, boolean completeOnEmpty )
        {
            super( actual );
            this.defaultValue = defaultValue;
            this.completeOnEmpty = completeOnEmpty;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
            }
        }

        public void request( long n )
        {
            super.request( n );
            if ( n > 0L )
            {
                this.s.request( Long.MAX_VALUE );
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }

        public void setValue( T value )
        {
            this.value = value;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.value = t;
                if ( ++this.count > 1 )
                {
                    this.cancel();
                    this.onError( new IndexOutOfBoundsException( "Source emitted more than one item" ) );
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                int c = this.count;
                if ( c == 0 )
                {
                    if ( this.completeOnEmpty )
                    {
                        this.actual.onComplete();
                        return;
                    }

                    T t = this.defaultValue;
                    if ( t != null )
                    {
                        this.complete( t );
                    }
                    else
                    {
                        this.actual.onError(
                                Operators.onOperatorError( this, new NoSuchElementException( "Source was empty" ), this.actual.currentContext() ) );
                    }
                }
                else if ( c == 1 )
                {
                    this.complete( this.value );
                }
            }
        }
    }
}
