package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;

import org.reactivestreams.Publisher;

final class FluxSwitchIfEmpty<T> extends InternalFluxOperator<T,T>
{
    final Publisher<? extends T> other;

    FluxSwitchIfEmpty( Flux<? extends T> source, Publisher<? extends T> other )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxSwitchIfEmpty.SwitchIfEmptySubscriber<T> parent = new FluxSwitchIfEmpty.SwitchIfEmptySubscriber( actual, this.other );
        actual.onSubscribe( parent );
        return parent;
    }

    static final class SwitchIfEmptySubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        final Publisher<? extends T> other;
        boolean once;

        SwitchIfEmptySubscriber( CoreSubscriber<? super T> actual, Publisher<? extends T> other )
        {
            super( actual );
            this.other = other;
        }

        public void onNext( T t )
        {
            if ( !this.once )
            {
                this.once = true;
            }

            this.actual.onNext( t );
        }

        public void onComplete()
        {
            if ( !this.once )
            {
                this.once = true;
                this.other.subscribe( this );
            }
            else
            {
                this.actual.onComplete();
            }
        }
    }
}
