package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCountUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ResourceLeakDetector;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutorGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.FastThreadLocal;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.StringUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.WeakHashMap;
import java.util.Map.Entry;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public class DefaultChannelPipeline implements ChannelPipeline
{
    static final InternalLogger logger = InternalLoggerFactory.getInstance( DefaultChannelPipeline.class );
    private static final String HEAD_NAME = generateName0( DefaultChannelPipeline.HeadContext.class );
    private static final String TAIL_NAME = generateName0( DefaultChannelPipeline.TailContext.class );
    private static final FastThreadLocal<Map<Class<?>,String>> nameCaches = new FastThreadLocal<Map<Class<?>,String>>()
    {
        protected Map<Class<?>,String> initialValue()
        {
            return new WeakHashMap();
        }
    };
    private static final AtomicReferenceFieldUpdater<DefaultChannelPipeline,MessageSizeEstimator.Handle> ESTIMATOR =
            AtomicReferenceFieldUpdater.newUpdater( DefaultChannelPipeline.class, MessageSizeEstimator.Handle.class, "estimatorHandle" );
    final AbstractChannelHandlerContext head;
    final AbstractChannelHandlerContext tail;
    private final Channel channel;
    private final ChannelFuture succeededFuture;
    private final VoidChannelPromise voidPromise;
    private final boolean touch = ResourceLeakDetector.isEnabled();
    private Map<EventExecutorGroup,EventExecutor> childExecutors;
    private volatile MessageSizeEstimator.Handle estimatorHandle;
    private boolean firstRegistration = true;
    private DefaultChannelPipeline.PendingHandlerCallback pendingHandlerCallbackHead;
    private boolean registered;

    protected DefaultChannelPipeline( Channel channel )
    {
        this.channel = (Channel) ObjectUtil.checkNotNull( channel, "channel" );
        this.succeededFuture = new SucceededChannelFuture( channel, (EventExecutor) null );
        this.voidPromise = new VoidChannelPromise( channel, true );
        this.tail = new DefaultChannelPipeline.TailContext( this );
        this.head = new DefaultChannelPipeline.HeadContext( this );
        this.head.next = this.tail;
        this.tail.prev = this.head;
    }

    private static void addBefore0( AbstractChannelHandlerContext ctx, AbstractChannelHandlerContext newCtx )
    {
        newCtx.prev = ctx.prev;
        newCtx.next = ctx;
        ctx.prev.next = newCtx;
        ctx.prev = newCtx;
    }

    private static void addAfter0( AbstractChannelHandlerContext ctx, AbstractChannelHandlerContext newCtx )
    {
        newCtx.prev = ctx;
        newCtx.next = ctx.next;
        ctx.next.prev = newCtx;
        ctx.next = newCtx;
    }

    private static String generateName0( Class<?> handlerType )
    {
        return StringUtil.simpleClassName( handlerType ) + "#0";
    }

    private static void replace0( AbstractChannelHandlerContext oldCtx, AbstractChannelHandlerContext newCtx )
    {
        AbstractChannelHandlerContext prev = oldCtx.prev;
        AbstractChannelHandlerContext next = oldCtx.next;
        newCtx.prev = prev;
        newCtx.next = next;
        prev.next = newCtx;
        next.prev = newCtx;
        oldCtx.prev = newCtx;
        oldCtx.next = newCtx;
    }

    private static void checkMultiplicity( ChannelHandler handler )
    {
        if ( handler instanceof ChannelHandlerAdapter )
        {
            ChannelHandlerAdapter h = (ChannelHandlerAdapter) handler;
            if ( !h.isSharable() && h.added )
            {
                throw new ChannelPipelineException( h.getClass().getName() + " is not a @Sharable handler, so can't be added or removed multiple times." );
            }

            h.added = true;
        }
    }

    final MessageSizeEstimator.Handle estimatorHandle()
    {
        MessageSizeEstimator.Handle handle = this.estimatorHandle;
        if ( handle == null )
        {
            handle = this.channel.config().getMessageSizeEstimator().newHandle();
            if ( !ESTIMATOR.compareAndSet( this, (Object) null, handle ) )
            {
                handle = this.estimatorHandle;
            }
        }

        return handle;
    }

    final Object touch( Object msg, AbstractChannelHandlerContext next )
    {
        return this.touch ? ReferenceCountUtil.touch( msg, next ) : msg;
    }

    private AbstractChannelHandlerContext newContext( EventExecutorGroup group, String name, ChannelHandler handler )
    {
        return new DefaultChannelHandlerContext( this, this.childExecutor( group ), name, handler );
    }

    private EventExecutor childExecutor( EventExecutorGroup group )
    {
        if ( group == null )
        {
            return null;
        }
        else
        {
            Boolean pinEventExecutor = (Boolean) this.channel.config().getOption( ChannelOption.SINGLE_EVENTEXECUTOR_PER_GROUP );
            if ( pinEventExecutor != null && !pinEventExecutor )
            {
                return group.next();
            }
            else
            {
                Map<EventExecutorGroup,EventExecutor> childExecutors = this.childExecutors;
                if ( childExecutors == null )
                {
                    childExecutors = this.childExecutors = new IdentityHashMap( 4 );
                }

                EventExecutor childExecutor = (EventExecutor) childExecutors.get( group );
                if ( childExecutor == null )
                {
                    childExecutor = group.next();
                    childExecutors.put( group, childExecutor );
                }

                return childExecutor;
            }
        }
    }

    public final Channel channel()
    {
        return this.channel;
    }

    public final ChannelPipeline addFirst( String name, ChannelHandler handler )
    {
        return this.addFirst( (EventExecutorGroup) null, name, handler );
    }

    public final ChannelPipeline addFirst( EventExecutorGroup group, String name, ChannelHandler handler )
    {
        AbstractChannelHandlerContext newCtx;
        synchronized ( this )
        {
            checkMultiplicity( handler );
            name = this.filterName( name, handler );
            newCtx = this.newContext( group, name, handler );
            this.addFirst0( newCtx );
            if ( !this.registered )
            {
                newCtx.setAddPending();
                this.callHandlerCallbackLater( newCtx, true );
                return this;
            }

            EventExecutor executor = newCtx.executor();
            if ( !executor.inEventLoop() )
            {
                this.callHandlerAddedInEventLoop( newCtx, executor );
                return this;
            }
        }

        this.callHandlerAdded0( newCtx );
        return this;
    }

    private void addFirst0( AbstractChannelHandlerContext newCtx )
    {
        AbstractChannelHandlerContext nextCtx = this.head.next;
        newCtx.prev = this.head;
        newCtx.next = nextCtx;
        this.head.next = newCtx;
        nextCtx.prev = newCtx;
    }

    public final ChannelPipeline addLast( String name, ChannelHandler handler )
    {
        return this.addLast( (EventExecutorGroup) null, name, handler );
    }

    public final ChannelPipeline addLast( EventExecutorGroup group, String name, ChannelHandler handler )
    {
        AbstractChannelHandlerContext newCtx;
        synchronized ( this )
        {
            checkMultiplicity( handler );
            newCtx = this.newContext( group, this.filterName( name, handler ), handler );
            this.addLast0( newCtx );
            if ( !this.registered )
            {
                newCtx.setAddPending();
                this.callHandlerCallbackLater( newCtx, true );
                return this;
            }

            EventExecutor executor = newCtx.executor();
            if ( !executor.inEventLoop() )
            {
                this.callHandlerAddedInEventLoop( newCtx, executor );
                return this;
            }
        }

        this.callHandlerAdded0( newCtx );
        return this;
    }

    private void addLast0( AbstractChannelHandlerContext newCtx )
    {
        AbstractChannelHandlerContext prev = this.tail.prev;
        newCtx.prev = prev;
        newCtx.next = this.tail;
        prev.next = newCtx;
        this.tail.prev = newCtx;
    }

    public final ChannelPipeline addBefore( String baseName, String name, ChannelHandler handler )
    {
        return this.addBefore( (EventExecutorGroup) null, baseName, name, handler );
    }

    public final ChannelPipeline addBefore( EventExecutorGroup group, String baseName, String name, ChannelHandler handler )
    {
        AbstractChannelHandlerContext newCtx;
        synchronized ( this )
        {
            checkMultiplicity( handler );
            name = this.filterName( name, handler );
            AbstractChannelHandlerContext ctx = this.getContextOrDie( baseName );
            newCtx = this.newContext( group, name, handler );
            addBefore0( ctx, newCtx );
            if ( !this.registered )
            {
                newCtx.setAddPending();
                this.callHandlerCallbackLater( newCtx, true );
                return this;
            }

            EventExecutor executor = newCtx.executor();
            if ( !executor.inEventLoop() )
            {
                this.callHandlerAddedInEventLoop( newCtx, executor );
                return this;
            }
        }

        this.callHandlerAdded0( newCtx );
        return this;
    }

    private String filterName( String name, ChannelHandler handler )
    {
        if ( name == null )
        {
            return this.generateName( handler );
        }
        else
        {
            this.checkDuplicateName( name );
            return name;
        }
    }

    public final ChannelPipeline addAfter( String baseName, String name, ChannelHandler handler )
    {
        return this.addAfter( (EventExecutorGroup) null, baseName, name, handler );
    }

    public final ChannelPipeline addAfter( EventExecutorGroup group, String baseName, String name, ChannelHandler handler )
    {
        AbstractChannelHandlerContext newCtx;
        synchronized ( this )
        {
            checkMultiplicity( handler );
            name = this.filterName( name, handler );
            AbstractChannelHandlerContext ctx = this.getContextOrDie( baseName );
            newCtx = this.newContext( group, name, handler );
            addAfter0( ctx, newCtx );
            if ( !this.registered )
            {
                newCtx.setAddPending();
                this.callHandlerCallbackLater( newCtx, true );
                return this;
            }

            EventExecutor executor = newCtx.executor();
            if ( !executor.inEventLoop() )
            {
                this.callHandlerAddedInEventLoop( newCtx, executor );
                return this;
            }
        }

        this.callHandlerAdded0( newCtx );
        return this;
    }

    public final ChannelPipeline addFirst( ChannelHandler handler )
    {
        return this.addFirst( (String) null, (ChannelHandler) handler );
    }

    public final ChannelPipeline addFirst( ChannelHandler... handlers )
    {
        return this.addFirst( (EventExecutorGroup) null, (ChannelHandler[]) handlers );
    }

    public final ChannelPipeline addFirst( EventExecutorGroup executor, ChannelHandler... handlers )
    {
        if ( handlers == null )
        {
            throw new NullPointerException( "handlers" );
        }
        else if ( handlers.length != 0 && handlers[0] != null )
        {
            int size;
            for ( size = 1; size < handlers.length && handlers[size] != null; ++size )
            {
            }

            for ( int i = size - 1; i >= 0; --i )
            {
                ChannelHandler h = handlers[i];
                this.addFirst( executor, (String) null, h );
            }

            return this;
        }
        else
        {
            return this;
        }
    }

    public final ChannelPipeline addLast( ChannelHandler handler )
    {
        return this.addLast( (String) null, (ChannelHandler) handler );
    }

    public final ChannelPipeline addLast( ChannelHandler... handlers )
    {
        return this.addLast( (EventExecutorGroup) null, (ChannelHandler[]) handlers );
    }

    public final ChannelPipeline addLast( EventExecutorGroup executor, ChannelHandler... handlers )
    {
        if ( handlers == null )
        {
            throw new NullPointerException( "handlers" );
        }
        else
        {
            ChannelHandler[] var3 = handlers;
            int var4 = handlers.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                ChannelHandler h = var3[var5];
                if ( h == null )
                {
                    break;
                }

                this.addLast( executor, (String) null, h );
            }

            return this;
        }
    }

    private String generateName( ChannelHandler handler )
    {
        Map<Class<?>,String> cache = (Map) nameCaches.get();
        Class<?> handlerType = handler.getClass();
        String name = (String) cache.get( handlerType );
        if ( name == null )
        {
            name = generateName0( handlerType );
            cache.put( handlerType, name );
        }

        if ( this.context0( name ) != null )
        {
            String baseName = name.substring( 0, name.length() - 1 );
            int i = 1;

            while ( true )
            {
                String newName = baseName + i;
                if ( this.context0( newName ) == null )
                {
                    name = newName;
                    break;
                }

                ++i;
            }
        }

        return name;
    }

    public final ChannelPipeline remove( ChannelHandler handler )
    {
        this.remove( this.getContextOrDie( handler ) );
        return this;
    }

    public final ChannelHandler remove( String name )
    {
        return this.remove( this.getContextOrDie( name ) ).handler();
    }

    public final <T extends ChannelHandler> T remove( Class<T> handlerType )
    {
        return this.remove( this.getContextOrDie( handlerType ) ).handler();
    }

    public final <T extends ChannelHandler> T removeIfExists( String name )
    {
        return this.removeIfExists( this.context( name ) );
    }

    public final <T extends ChannelHandler> T removeIfExists( Class<T> handlerType )
    {
        return this.removeIfExists( this.context( handlerType ) );
    }

    public final <T extends ChannelHandler> T removeIfExists( ChannelHandler handler )
    {
        return this.removeIfExists( this.context( handler ) );
    }

    private <T extends ChannelHandler> T removeIfExists( ChannelHandlerContext ctx )
    {
        return ctx == null ? null : this.remove( (AbstractChannelHandlerContext) ctx ).handler();
    }

    private AbstractChannelHandlerContext remove( final AbstractChannelHandlerContext ctx )
    {
        assert ctx != this.head && ctx != this.tail;

        synchronized ( this )
        {
            this.atomicRemoveFromHandlerList( ctx );
            if ( !this.registered )
            {
                this.callHandlerCallbackLater( ctx, false );
                return ctx;
            }

            EventExecutor executor = ctx.executor();
            if ( !executor.inEventLoop() )
            {
                executor.execute( new Runnable()
                {
                    public void run()
                    {
                        DefaultChannelPipeline.this.callHandlerRemoved0( ctx );
                    }
                } );
                return ctx;
            }
        }

        this.callHandlerRemoved0( ctx );
        return ctx;
    }

    private synchronized void atomicRemoveFromHandlerList( AbstractChannelHandlerContext ctx )
    {
        AbstractChannelHandlerContext prev = ctx.prev;
        AbstractChannelHandlerContext next = ctx.next;
        prev.next = next;
        next.prev = prev;
    }

    public final ChannelHandler removeFirst()
    {
        if ( this.head.next == this.tail )
        {
            throw new NoSuchElementException();
        }
        else
        {
            return this.remove( this.head.next ).handler();
        }
    }

    public final ChannelHandler removeLast()
    {
        if ( this.head.next == this.tail )
        {
            throw new NoSuchElementException();
        }
        else
        {
            return this.remove( this.tail.prev ).handler();
        }
    }

    public final ChannelPipeline replace( ChannelHandler oldHandler, String newName, ChannelHandler newHandler )
    {
        this.replace( this.getContextOrDie( oldHandler ), newName, newHandler );
        return this;
    }

    public final ChannelHandler replace( String oldName, String newName, ChannelHandler newHandler )
    {
        return this.replace( this.getContextOrDie( oldName ), newName, newHandler );
    }

    public final <T extends ChannelHandler> T replace( Class<T> oldHandlerType, String newName, ChannelHandler newHandler )
    {
        return this.replace( this.getContextOrDie( oldHandlerType ), newName, newHandler );
    }

    private ChannelHandler replace( final AbstractChannelHandlerContext ctx, String newName, ChannelHandler newHandler )
    {
        assert ctx != this.head && ctx != this.tail;

        final AbstractChannelHandlerContext newCtx;
        synchronized ( this )
        {
            checkMultiplicity( newHandler );
            if ( newName == null )
            {
                newName = this.generateName( newHandler );
            }
            else
            {
                boolean sameName = ctx.name().equals( newName );
                if ( !sameName )
                {
                    this.checkDuplicateName( newName );
                }
            }

            newCtx = this.newContext( ctx.executor, newName, newHandler );
            replace0( ctx, newCtx );
            if ( !this.registered )
            {
                this.callHandlerCallbackLater( newCtx, true );
                this.callHandlerCallbackLater( ctx, false );
                return ctx.handler();
            }

            EventExecutor executor = ctx.executor();
            if ( !executor.inEventLoop() )
            {
                executor.execute( new Runnable()
                {
                    public void run()
                    {
                        DefaultChannelPipeline.this.callHandlerAdded0( newCtx );
                        DefaultChannelPipeline.this.callHandlerRemoved0( ctx );
                    }
                } );
                return ctx.handler();
            }
        }

        this.callHandlerAdded0( newCtx );
        this.callHandlerRemoved0( ctx );
        return ctx.handler();
    }

    private void callHandlerAdded0( AbstractChannelHandlerContext ctx )
    {
        try
        {
            ctx.callHandlerAdded();
        }
        catch ( Throwable var6 )
        {
            boolean removed = false;

            try
            {
                this.atomicRemoveFromHandlerList( ctx );
                ctx.callHandlerRemoved();
                removed = true;
            }
            catch ( Throwable var5 )
            {
                if ( logger.isWarnEnabled() )
                {
                    logger.warn( "Failed to remove a handler: " + ctx.name(), var5 );
                }
            }

            if ( removed )
            {
                this.fireExceptionCaught(
                        new ChannelPipelineException( ctx.handler().getClass().getName() + ".handlerAdded() has thrown an exception; removed.", var6 ) );
            }
            else
            {
                this.fireExceptionCaught(
                        new ChannelPipelineException( ctx.handler().getClass().getName() + ".handlerAdded() has thrown an exception; also failed to remove.",
                                var6 ) );
            }
        }
    }

    private void callHandlerRemoved0( AbstractChannelHandlerContext ctx )
    {
        try
        {
            ctx.callHandlerRemoved();
        }
        catch ( Throwable var3 )
        {
            this.fireExceptionCaught( new ChannelPipelineException( ctx.handler().getClass().getName() + ".handlerRemoved() has thrown an exception.", var3 ) );
        }
    }

    final void invokeHandlerAddedIfNeeded()
    {
        assert this.channel.eventLoop().inEventLoop();

        if ( this.firstRegistration )
        {
            this.firstRegistration = false;
            this.callHandlerAddedForAllHandlers();
        }
    }

    public final ChannelHandler first()
    {
        ChannelHandlerContext first = this.firstContext();
        return first == null ? null : first.handler();
    }

    public final ChannelHandlerContext firstContext()
    {
        AbstractChannelHandlerContext first = this.head.next;
        return first == this.tail ? null : this.head.next;
    }

    public final ChannelHandler last()
    {
        AbstractChannelHandlerContext last = this.tail.prev;
        return last == this.head ? null : last.handler();
    }

    public final ChannelHandlerContext lastContext()
    {
        AbstractChannelHandlerContext last = this.tail.prev;
        return last == this.head ? null : last;
    }

    public final ChannelHandler get( String name )
    {
        ChannelHandlerContext ctx = this.context( name );
        return ctx == null ? null : ctx.handler();
    }

    public final <T extends ChannelHandler> T get( Class<T> handlerType )
    {
        ChannelHandlerContext ctx = this.context( handlerType );
        return ctx == null ? null : ctx.handler();
    }

    public final ChannelHandlerContext context( String name )
    {
        if ( name == null )
        {
            throw new NullPointerException( "name" );
        }
        else
        {
            return this.context0( name );
        }
    }

    public final ChannelHandlerContext context( ChannelHandler handler )
    {
        if ( handler == null )
        {
            throw new NullPointerException( "handler" );
        }
        else
        {
            for ( AbstractChannelHandlerContext ctx = this.head.next; ctx != null; ctx = ctx.next )
            {
                if ( ctx.handler() == handler )
                {
                    return ctx;
                }
            }

            return null;
        }
    }

    public final ChannelHandlerContext context( Class<? extends ChannelHandler> handlerType )
    {
        if ( handlerType == null )
        {
            throw new NullPointerException( "handlerType" );
        }
        else
        {
            for ( AbstractChannelHandlerContext ctx = this.head.next; ctx != null; ctx = ctx.next )
            {
                if ( handlerType.isAssignableFrom( ctx.handler().getClass() ) )
                {
                    return ctx;
                }
            }

            return null;
        }
    }

    public final List<String> names()
    {
        List<String> list = new ArrayList();

        for ( AbstractChannelHandlerContext ctx = this.head.next; ctx != null; ctx = ctx.next )
        {
            list.add( ctx.name() );
        }

        return list;
    }

    public final Map<String,ChannelHandler> toMap()
    {
        Map<String,ChannelHandler> map = new LinkedHashMap();

        for ( AbstractChannelHandlerContext ctx = this.head.next; ctx != this.tail; ctx = ctx.next )
        {
            map.put( ctx.name(), ctx.handler() );
        }

        return map;
    }

    public final Iterator<Entry<String,ChannelHandler>> iterator()
    {
        return this.toMap().entrySet().iterator();
    }

    public final String toString()
    {
        StringBuilder buf = (new StringBuilder()).append( StringUtil.simpleClassName( (Object) this ) ).append( '{' );
        AbstractChannelHandlerContext ctx = this.head.next;

        while ( ctx != this.tail )
        {
            buf.append( '(' ).append( ctx.name() ).append( " = " ).append( ctx.handler().getClass().getName() ).append( ')' );
            ctx = ctx.next;
            if ( ctx == this.tail )
            {
                break;
            }

            buf.append( ", " );
        }

        buf.append( '}' );
        return buf.toString();
    }

    public final ChannelPipeline fireChannelRegistered()
    {
        AbstractChannelHandlerContext.invokeChannelRegistered( this.head );
        return this;
    }

    public final ChannelPipeline fireChannelUnregistered()
    {
        AbstractChannelHandlerContext.invokeChannelUnregistered( this.head );
        return this;
    }

    private synchronized void destroy()
    {
        this.destroyUp( this.head.next, false );
    }

    private void destroyUp( final AbstractChannelHandlerContext ctx, boolean inEventLoop )
    {
        Thread currentThread = Thread.currentThread();
        AbstractChannelHandlerContext tail = this.tail;

        while ( true )
        {
            if ( ctx == tail )
            {
                this.destroyDown( currentThread, tail.prev, inEventLoop );
                break;
            }

            EventExecutor executor = ctx.executor();
            if ( !inEventLoop && !executor.inEventLoop( currentThread ) )
            {
                executor.execute( new Runnable()
                {
                    public void run()
                    {
                        DefaultChannelPipeline.this.destroyUp( ctx, true );
                    }
                } );
                break;
            }

            ctx = ctx.next;
            inEventLoop = false;
        }
    }

    private void destroyDown( Thread currentThread, final AbstractChannelHandlerContext ctx, boolean inEventLoop )
    {
        for ( AbstractChannelHandlerContext head = this.head; ctx != head; inEventLoop = false )
        {
            EventExecutor executor = ctx.executor();
            if ( !inEventLoop && !executor.inEventLoop( currentThread ) )
            {
                executor.execute( new Runnable()
                {
                    public void run()
                    {
                        DefaultChannelPipeline.this.destroyDown( Thread.currentThread(), ctx, true );
                    }
                } );
                break;
            }

            this.atomicRemoveFromHandlerList( ctx );
            this.callHandlerRemoved0( ctx );
            ctx = ctx.prev;
        }
    }

    public final ChannelPipeline fireChannelActive()
    {
        AbstractChannelHandlerContext.invokeChannelActive( this.head );
        return this;
    }

    public final ChannelPipeline fireChannelInactive()
    {
        AbstractChannelHandlerContext.invokeChannelInactive( this.head );
        return this;
    }

    public final ChannelPipeline fireExceptionCaught( Throwable cause )
    {
        AbstractChannelHandlerContext.invokeExceptionCaught( this.head, cause );
        return this;
    }

    public final ChannelPipeline fireUserEventTriggered( Object event )
    {
        AbstractChannelHandlerContext.invokeUserEventTriggered( this.head, event );
        return this;
    }

    public final ChannelPipeline fireChannelRead( Object msg )
    {
        AbstractChannelHandlerContext.invokeChannelRead( this.head, msg );
        return this;
    }

    public final ChannelPipeline fireChannelReadComplete()
    {
        AbstractChannelHandlerContext.invokeChannelReadComplete( this.head );
        return this;
    }

    public final ChannelPipeline fireChannelWritabilityChanged()
    {
        AbstractChannelHandlerContext.invokeChannelWritabilityChanged( this.head );
        return this;
    }

    public final ChannelFuture bind( SocketAddress localAddress )
    {
        return this.tail.bind( localAddress );
    }

    public final ChannelFuture connect( SocketAddress remoteAddress )
    {
        return this.tail.connect( remoteAddress );
    }

    public final ChannelFuture connect( SocketAddress remoteAddress, SocketAddress localAddress )
    {
        return this.tail.connect( remoteAddress, localAddress );
    }

    public final ChannelFuture disconnect()
    {
        return this.tail.disconnect();
    }

    public final ChannelFuture close()
    {
        return this.tail.close();
    }

    public final ChannelFuture deregister()
    {
        return this.tail.deregister();
    }

    public final ChannelPipeline flush()
    {
        this.tail.flush();
        return this;
    }

    public final ChannelFuture bind( SocketAddress localAddress, ChannelPromise promise )
    {
        return this.tail.bind( localAddress, promise );
    }

    public final ChannelFuture connect( SocketAddress remoteAddress, ChannelPromise promise )
    {
        return this.tail.connect( remoteAddress, promise );
    }

    public final ChannelFuture connect( SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise )
    {
        return this.tail.connect( remoteAddress, localAddress, promise );
    }

    public final ChannelFuture disconnect( ChannelPromise promise )
    {
        return this.tail.disconnect( promise );
    }

    public final ChannelFuture close( ChannelPromise promise )
    {
        return this.tail.close( promise );
    }

    public final ChannelFuture deregister( ChannelPromise promise )
    {
        return this.tail.deregister( promise );
    }

    public final ChannelPipeline read()
    {
        this.tail.read();
        return this;
    }

    public final ChannelFuture write( Object msg )
    {
        return this.tail.write( msg );
    }

    public final ChannelFuture write( Object msg, ChannelPromise promise )
    {
        return this.tail.write( msg, promise );
    }

    public final ChannelFuture writeAndFlush( Object msg, ChannelPromise promise )
    {
        return this.tail.writeAndFlush( msg, promise );
    }

    public final ChannelFuture writeAndFlush( Object msg )
    {
        return this.tail.writeAndFlush( msg );
    }

    public final ChannelPromise newPromise()
    {
        return new DefaultChannelPromise( this.channel );
    }

    public final ChannelProgressivePromise newProgressivePromise()
    {
        return new DefaultChannelProgressivePromise( this.channel );
    }

    public final ChannelFuture newSucceededFuture()
    {
        return this.succeededFuture;
    }

    public final ChannelFuture newFailedFuture( Throwable cause )
    {
        return new FailedChannelFuture( this.channel, (EventExecutor) null, cause );
    }

    public final ChannelPromise voidPromise()
    {
        return this.voidPromise;
    }

    private void checkDuplicateName( String name )
    {
        if ( this.context0( name ) != null )
        {
            throw new IllegalArgumentException( "Duplicate handler name: " + name );
        }
    }

    private AbstractChannelHandlerContext context0( String name )
    {
        for ( AbstractChannelHandlerContext context = this.head.next; context != this.tail; context = context.next )
        {
            if ( context.name().equals( name ) )
            {
                return context;
            }
        }

        return null;
    }

    private AbstractChannelHandlerContext getContextOrDie( String name )
    {
        AbstractChannelHandlerContext ctx = (AbstractChannelHandlerContext) this.context( name );
        if ( ctx == null )
        {
            throw new NoSuchElementException( name );
        }
        else
        {
            return ctx;
        }
    }

    private AbstractChannelHandlerContext getContextOrDie( ChannelHandler handler )
    {
        AbstractChannelHandlerContext ctx = (AbstractChannelHandlerContext) this.context( handler );
        if ( ctx == null )
        {
            throw new NoSuchElementException( handler.getClass().getName() );
        }
        else
        {
            return ctx;
        }
    }

    private AbstractChannelHandlerContext getContextOrDie( Class<? extends ChannelHandler> handlerType )
    {
        AbstractChannelHandlerContext ctx = (AbstractChannelHandlerContext) this.context( handlerType );
        if ( ctx == null )
        {
            throw new NoSuchElementException( handlerType.getName() );
        }
        else
        {
            return ctx;
        }
    }

    private void callHandlerAddedForAllHandlers()
    {
        DefaultChannelPipeline.PendingHandlerCallback pendingHandlerCallbackHead;
        synchronized ( this )
        {
            assert !this.registered;

            this.registered = true;
            pendingHandlerCallbackHead = this.pendingHandlerCallbackHead;
            this.pendingHandlerCallbackHead = null;
        }

        for ( DefaultChannelPipeline.PendingHandlerCallback task = pendingHandlerCallbackHead; task != null; task = task.next )
        {
            task.execute();
        }
    }

    private void callHandlerCallbackLater( AbstractChannelHandlerContext ctx, boolean added )
    {
        assert !this.registered;

        DefaultChannelPipeline.PendingHandlerCallback task =
                added ? new DefaultChannelPipeline.PendingHandlerAddedTask( ctx ) : new DefaultChannelPipeline.PendingHandlerRemovedTask( ctx );
        DefaultChannelPipeline.PendingHandlerCallback pending = this.pendingHandlerCallbackHead;
        if ( pending == null )
        {
            this.pendingHandlerCallbackHead = (DefaultChannelPipeline.PendingHandlerCallback) task;
        }
        else
        {
            while ( pending.next != null )
            {
                pending = pending.next;
            }

            pending.next = (DefaultChannelPipeline.PendingHandlerCallback) task;
        }
    }

    private void callHandlerAddedInEventLoop( final AbstractChannelHandlerContext newCtx, EventExecutor executor )
    {
        newCtx.setAddPending();
        executor.execute( new Runnable()
        {
            public void run()
            {
                DefaultChannelPipeline.this.callHandlerAdded0( newCtx );
            }
        } );
    }

    protected void onUnhandledInboundException( Throwable cause )
    {
        try
        {
            logger.warn(
                    "An exceptionCaught() event was fired, and it reached at the tail of the pipeline. It usually means the last handler in the pipeline did not handle the exception.",
                    cause );
        }
        finally
        {
            ReferenceCountUtil.release( cause );
        }
    }

    protected void onUnhandledInboundChannelActive()
    {
    }

    protected void onUnhandledInboundChannelInactive()
    {
    }

    protected void onUnhandledInboundMessage( Object msg )
    {
        try
        {
            logger.debug( "Discarded inbound message {} that reached at the tail of the pipeline. Please check your pipeline configuration.", msg );
        }
        finally
        {
            ReferenceCountUtil.release( msg );
        }
    }

    protected void onUnhandledInboundMessage( ChannelHandlerContext ctx, Object msg )
    {
        this.onUnhandledInboundMessage( msg );
        if ( logger.isDebugEnabled() )
        {
            logger.debug( "Discarded message pipeline : {}. Channel : {}.", ctx.pipeline().names(), ctx.channel() );
        }
    }

    protected void onUnhandledInboundChannelReadComplete()
    {
    }

    protected void onUnhandledInboundUserEventTriggered( Object evt )
    {
        ReferenceCountUtil.release( evt );
    }

    protected void onUnhandledChannelWritabilityChanged()
    {
    }

    protected void incrementPendingOutboundBytes( long size )
    {
        ChannelOutboundBuffer buffer = this.channel.unsafe().outboundBuffer();
        if ( buffer != null )
        {
            buffer.incrementPendingOutboundBytes( size );
        }
    }

    protected void decrementPendingOutboundBytes( long size )
    {
        ChannelOutboundBuffer buffer = this.channel.unsafe().outboundBuffer();
        if ( buffer != null )
        {
            buffer.decrementPendingOutboundBytes( size );
        }
    }

    private abstract static class PendingHandlerCallback implements Runnable
    {
        final AbstractChannelHandlerContext ctx;
        DefaultChannelPipeline.PendingHandlerCallback next;

        PendingHandlerCallback( AbstractChannelHandlerContext ctx )
        {
            this.ctx = ctx;
        }

        abstract void execute();
    }

    private final class PendingHandlerRemovedTask extends DefaultChannelPipeline.PendingHandlerCallback
    {
        PendingHandlerRemovedTask( AbstractChannelHandlerContext ctx )
        {
            super( ctx );
        }

        public void run()
        {
            DefaultChannelPipeline.this.callHandlerRemoved0( this.ctx );
        }

        void execute()
        {
            EventExecutor executor = this.ctx.executor();
            if ( executor.inEventLoop() )
            {
                DefaultChannelPipeline.this.callHandlerRemoved0( this.ctx );
            }
            else
            {
                try
                {
                    executor.execute( this );
                }
                catch ( RejectedExecutionException var3 )
                {
                    if ( DefaultChannelPipeline.logger.isWarnEnabled() )
                    {
                        DefaultChannelPipeline.logger.warn( "Can't invoke handlerRemoved() as the EventExecutor {} rejected it, removing handler {}.", executor,
                                this.ctx.name(), var3 );
                    }

                    this.ctx.setRemoved();
                }
            }
        }
    }

    private final class PendingHandlerAddedTask extends DefaultChannelPipeline.PendingHandlerCallback
    {
        PendingHandlerAddedTask( AbstractChannelHandlerContext ctx )
        {
            super( ctx );
        }

        public void run()
        {
            DefaultChannelPipeline.this.callHandlerAdded0( this.ctx );
        }

        void execute()
        {
            EventExecutor executor = this.ctx.executor();
            if ( executor.inEventLoop() )
            {
                DefaultChannelPipeline.this.callHandlerAdded0( this.ctx );
            }
            else
            {
                try
                {
                    executor.execute( this );
                }
                catch ( RejectedExecutionException var3 )
                {
                    if ( DefaultChannelPipeline.logger.isWarnEnabled() )
                    {
                        DefaultChannelPipeline.logger.warn( "Can't invoke handlerAdded() as the EventExecutor {} rejected it, removing handler {}.", executor,
                                this.ctx.name(), var3 );
                    }

                    DefaultChannelPipeline.this.atomicRemoveFromHandlerList( this.ctx );
                    this.ctx.setRemoved();
                }
            }
        }
    }

    final class HeadContext extends AbstractChannelHandlerContext implements ChannelOutboundHandler, ChannelInboundHandler
    {
        private final Channel.Unsafe unsafe;

        HeadContext( DefaultChannelPipeline pipeline )
        {
            super( pipeline, (EventExecutor) null, DefaultChannelPipeline.HEAD_NAME, DefaultChannelPipeline.HeadContext.class );
            this.unsafe = pipeline.channel().unsafe();
            this.setAddComplete();
        }

        public ChannelHandler handler()
        {
            return this;
        }

        public void handlerAdded( ChannelHandlerContext ctx )
        {
        }

        public void handlerRemoved( ChannelHandlerContext ctx )
        {
        }

        public void bind( ChannelHandlerContext ctx, SocketAddress localAddress, ChannelPromise promise )
        {
            this.unsafe.bind( localAddress, promise );
        }

        public void connect( ChannelHandlerContext ctx, SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise )
        {
            this.unsafe.connect( remoteAddress, localAddress, promise );
        }

        public void disconnect( ChannelHandlerContext ctx, ChannelPromise promise )
        {
            this.unsafe.disconnect( promise );
        }

        public void close( ChannelHandlerContext ctx, ChannelPromise promise )
        {
            this.unsafe.close( promise );
        }

        public void deregister( ChannelHandlerContext ctx, ChannelPromise promise )
        {
            this.unsafe.deregister( promise );
        }

        public void read( ChannelHandlerContext ctx )
        {
            this.unsafe.beginRead();
        }

        public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise )
        {
            this.unsafe.write( msg, promise );
        }

        public void flush( ChannelHandlerContext ctx )
        {
            this.unsafe.flush();
        }

        public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause )
        {
            ctx.fireExceptionCaught( cause );
        }

        public void channelRegistered( ChannelHandlerContext ctx )
        {
            DefaultChannelPipeline.this.invokeHandlerAddedIfNeeded();
            ctx.fireChannelRegistered();
        }

        public void channelUnregistered( ChannelHandlerContext ctx )
        {
            ctx.fireChannelUnregistered();
            if ( !DefaultChannelPipeline.this.channel.isOpen() )
            {
                DefaultChannelPipeline.this.destroy();
            }
        }

        public void channelActive( ChannelHandlerContext ctx )
        {
            ctx.fireChannelActive();
            this.readIfIsAutoRead();
        }

        public void channelInactive( ChannelHandlerContext ctx )
        {
            ctx.fireChannelInactive();
        }

        public void channelRead( ChannelHandlerContext ctx, Object msg )
        {
            ctx.fireChannelRead( msg );
        }

        public void channelReadComplete( ChannelHandlerContext ctx )
        {
            ctx.fireChannelReadComplete();
            this.readIfIsAutoRead();
        }

        private void readIfIsAutoRead()
        {
            if ( DefaultChannelPipeline.this.channel.config().isAutoRead() )
            {
                DefaultChannelPipeline.this.channel.read();
            }
        }

        public void userEventTriggered( ChannelHandlerContext ctx, Object evt )
        {
            ctx.fireUserEventTriggered( evt );
        }

        public void channelWritabilityChanged( ChannelHandlerContext ctx )
        {
            ctx.fireChannelWritabilityChanged();
        }
    }

    final class TailContext extends AbstractChannelHandlerContext implements ChannelInboundHandler
    {
        TailContext( DefaultChannelPipeline pipeline )
        {
            super( pipeline, (EventExecutor) null, DefaultChannelPipeline.TAIL_NAME, DefaultChannelPipeline.TailContext.class );
            this.setAddComplete();
        }

        public ChannelHandler handler()
        {
            return this;
        }

        public void channelRegistered( ChannelHandlerContext ctx )
        {
        }

        public void channelUnregistered( ChannelHandlerContext ctx )
        {
        }

        public void channelActive( ChannelHandlerContext ctx )
        {
            DefaultChannelPipeline.this.onUnhandledInboundChannelActive();
        }

        public void channelInactive( ChannelHandlerContext ctx )
        {
            DefaultChannelPipeline.this.onUnhandledInboundChannelInactive();
        }

        public void channelWritabilityChanged( ChannelHandlerContext ctx )
        {
            DefaultChannelPipeline.this.onUnhandledChannelWritabilityChanged();
        }

        public void handlerAdded( ChannelHandlerContext ctx )
        {
        }

        public void handlerRemoved( ChannelHandlerContext ctx )
        {
        }

        public void userEventTriggered( ChannelHandlerContext ctx, Object evt )
        {
            DefaultChannelPipeline.this.onUnhandledInboundUserEventTriggered( evt );
        }

        public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause )
        {
            DefaultChannelPipeline.this.onUnhandledInboundException( cause );
        }

        public void channelRead( ChannelHandlerContext ctx, Object msg )
        {
            DefaultChannelPipeline.this.onUnhandledInboundMessage( ctx, msg );
        }

        public void channelReadComplete( ChannelHandlerContext ctx )
        {
            DefaultChannelPipeline.this.onUnhandledInboundChannelReadComplete();
        }
    }
}
