package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

final class BlockingFirstSubscriber<T> extends BlockingSingleSubscriber<T>
{
    public void onNext( T t )
    {
        if ( this.value == null )
        {
            this.value = t;
            this.dispose();
            this.countDown();
        }
    }

    public void onError( Throwable t )
    {
        if ( this.value == null )
        {
            this.error = t;
        }

        this.countDown();
    }
}
