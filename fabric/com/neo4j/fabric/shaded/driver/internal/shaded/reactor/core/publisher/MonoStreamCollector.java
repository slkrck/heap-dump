package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collector;

import org.reactivestreams.Subscription;

final class MonoStreamCollector<T, A, R> extends MonoFromFluxOperator<T,R> implements Fuseable
{
    final Collector<? super T,A,? extends R> collector;

    MonoStreamCollector( Flux<? extends T> source, Collector<? super T,A,? extends R> collector )
    {
        super( source );
        this.collector = (Collector) Objects.requireNonNull( collector, "collector" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        Object container;
        BiConsumer accumulator;
        Function finisher;
        try
        {
            container = this.collector.supplier().get();
            accumulator = this.collector.accumulator();
            finisher = this.collector.finisher();
        }
        catch ( Throwable var6 )
        {
            Operators.error( actual, Operators.onOperatorError( var6, actual.currentContext() ) );
            return null;
        }

        return new MonoStreamCollector.StreamCollectorSubscriber( actual, container, accumulator, finisher );
    }

    static final class StreamCollectorSubscriber<T, A, R> extends Operators.MonoSubscriber<T,R>
    {
        final BiConsumer<? super A,? super T> accumulator;
        final Function<? super A,? extends R> finisher;
        A container;
        Subscription s;
        boolean done;

        StreamCollectorSubscriber( CoreSubscriber<? super R> actual, A container, BiConsumer<? super A,? super T> accumulator,
                Function<? super A,? extends R> finisher )
        {
            super( actual );
            this.container = container;
            this.accumulator = accumulator;
            this.finisher = finisher;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                try
                {
                    this.accumulator.accept( this.container, t );
                }
                catch ( Throwable var3 )
                {
                    this.onError( Operators.onOperatorError( this.s, var3, t, this.actual.currentContext() ) );
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.container = null;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                A a = this.container;
                this.container = null;

                Object r;
                try
                {
                    r = this.finisher.apply( a );
                }
                catch ( Throwable var4 )
                {
                    this.actual.onError( Operators.onOperatorError( var4, this.actual.currentContext() ) );
                    return;
                }

                this.complete( r );
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }
    }
}
