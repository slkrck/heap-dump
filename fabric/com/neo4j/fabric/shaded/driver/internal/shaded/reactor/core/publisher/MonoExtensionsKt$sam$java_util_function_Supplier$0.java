package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import java.util.function.Supplier;

import kotlin.Metadata;
import kotlin.jvm.functions.Function0;

@Metadata( mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3 )
final class MonoExtensionsKt$sam$java_util_function_Supplier$0 implements Supplier
{
    MonoExtensionsKt$sam$java_util_function_Supplier$0( Function0 var1 )
    {
        this.function = var1;
    }
}
