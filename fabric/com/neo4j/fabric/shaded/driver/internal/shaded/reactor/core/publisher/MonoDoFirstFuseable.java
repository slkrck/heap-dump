package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

final class MonoDoFirstFuseable<T> extends InternalMonoOperator<T,T> implements Fuseable
{
    final Runnable onFirst;

    MonoDoFirstFuseable( Mono<? extends T> source, Runnable onFirst )
    {
        super( source );
        this.onFirst = onFirst;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        try
        {
            this.onFirst.run();
            return actual;
        }
        catch ( Throwable var3 )
        {
            Operators.error( actual, var3 );
            return null;
        }
    }
}
