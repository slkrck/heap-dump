package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging;

public enum InternalLogLevel
{
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR;
}
