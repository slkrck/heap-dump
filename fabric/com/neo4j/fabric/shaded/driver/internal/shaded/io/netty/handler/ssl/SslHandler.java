package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.CompositeByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.Unpooled;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.AbstractCoalescingBufferQueue;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOutboundHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromiseNotifier;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.ByteToMessageDecoder;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.DecoderException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.UnsupportedMessageTypeException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCountUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.ReferenceCounted;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.DefaultPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.FutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.ImmediateExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Promise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.PromiseNotifier;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLEngineResult.HandshakeStatus;
import javax.net.ssl.SSLEngineResult.Status;

public class SslHandler extends ByteToMessageDecoder implements ChannelOutboundHandler
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( SslHandler.class );
    private static final Pattern IGNORABLE_CLASS_IN_STACK = Pattern.compile( "^.*(?:Socket|Datagram|Sctp|Udt)Channel.*$" );
    private static final Pattern IGNORABLE_ERROR_MESSAGE = Pattern.compile( "^.*(?:connection.*(?:reset|closed|abort|broken)|broken.*pipe).*$", 2 );
    private static final int MAX_PLAINTEXT_LENGTH = 16384;
    private final SSLEngine engine;
    private final SslHandler.SslEngineType engineType;
    private final Executor delegatedTaskExecutor;
    private final boolean jdkCompatibilityMode;
    private final ByteBuffer[] singleBuffer;
    private final boolean startTls;
    private final SslHandler.LazyChannelPromise sslClosePromise;
    volatile int wrapDataSize;
    private volatile ChannelHandlerContext ctx;
    private boolean sentFirstMessage;
    private boolean flushedBeforeHandshake;
    private boolean readDuringHandshake;
    private boolean handshakeStarted;
    private SslHandler.SslHandlerCoalescingBufferQueue pendingUnencryptedWrites;
    private Promise<Channel> handshakePromise;
    private boolean needsFlush;
    private boolean outboundClosed;
    private boolean closeNotify;
    private boolean processTask;
    private int packetLength;
    private boolean firedChannelRead;
    private volatile long handshakeTimeoutMillis;
    private volatile long closeNotifyFlushTimeoutMillis;
    private volatile long closeNotifyReadTimeoutMillis;

    public SslHandler( SSLEngine engine )
    {
        this( engine, false );
    }

    public SslHandler( SSLEngine engine, boolean startTls )
    {
        this( engine, startTls, ImmediateExecutor.INSTANCE );
    }

    public SslHandler( SSLEngine engine, Executor delegatedTaskExecutor )
    {
        this( engine, false, delegatedTaskExecutor );
    }

    public SslHandler( SSLEngine engine, boolean startTls, Executor delegatedTaskExecutor )
    {
        this.singleBuffer = new ByteBuffer[1];
        this.handshakePromise = new SslHandler.LazyChannelPromise();
        this.sslClosePromise = new SslHandler.LazyChannelPromise();
        this.handshakeTimeoutMillis = 10000L;
        this.closeNotifyFlushTimeoutMillis = 3000L;
        this.wrapDataSize = 16384;
        if ( engine == null )
        {
            throw new NullPointerException( "engine" );
        }
        else if ( delegatedTaskExecutor == null )
        {
            throw new NullPointerException( "delegatedTaskExecutor" );
        }
        else
        {
            this.engine = engine;
            this.engineType = SslHandler.SslEngineType.forEngine( engine );
            this.delegatedTaskExecutor = delegatedTaskExecutor;
            this.startTls = startTls;
            this.jdkCompatibilityMode = this.engineType.jdkCompatibilityMode( engine );
            this.setCumulator( this.engineType.cumulator );
        }
    }

    private static IllegalStateException newPendingWritesNullException()
    {
        return new IllegalStateException( "pendingUnencryptedWrites is null, handlerRemoved0 called?" );
    }

    public static boolean isEncrypted( ByteBuf buffer )
    {
        if ( buffer.readableBytes() < 5 )
        {
            throw new IllegalArgumentException( "buffer must have at least 5 readable bytes" );
        }
        else
        {
            return SslUtils.getEncryptedPacketLength( buffer, buffer.readerIndex() ) != -2;
        }
    }

    private static ByteBuffer toByteBuffer( ByteBuf out, int index, int len )
    {
        return out.nioBufferCount() == 1 ? out.internalNioBuffer( index, len ) : out.nioBuffer( index, len );
    }

    private static boolean inEventLoop( Executor executor )
    {
        return executor instanceof EventExecutor && ((EventExecutor) executor).inEventLoop();
    }

    private static void runAllDelegatedTasks( SSLEngine engine )
    {
        while ( true )
        {
            Runnable task = engine.getDelegatedTask();
            if ( task == null )
            {
                return;
            }

            task.run();
        }
    }

    private static void addCloseListener( ChannelFuture future, ChannelPromise promise )
    {
        future.addListener( new ChannelPromiseNotifier( false, new ChannelPromise[]{promise} ) );
    }

    private static boolean attemptCopyToCumulation( ByteBuf cumulation, ByteBuf next, int wrapDataSize )
    {
        int inReadableBytes = next.readableBytes();
        int cumulationCapacity = cumulation.capacity();
        if ( wrapDataSize - cumulation.readableBytes() < inReadableBytes || (!cumulation.isWritable( inReadableBytes ) || cumulationCapacity < wrapDataSize) &&
                (cumulationCapacity >= wrapDataSize || !ByteBufUtil.ensureWritableSuccess( cumulation.ensureWritable( inReadableBytes, false ) )) )
        {
            return false;
        }
        else
        {
            cumulation.writeBytes( next );
            next.release();
            return true;
        }
    }

    public long getHandshakeTimeoutMillis()
    {
        return this.handshakeTimeoutMillis;
    }

    public void setHandshakeTimeoutMillis( long handshakeTimeoutMillis )
    {
        if ( handshakeTimeoutMillis < 0L )
        {
            throw new IllegalArgumentException( "handshakeTimeoutMillis: " + handshakeTimeoutMillis + " (expected: >= 0)" );
        }
        else
        {
            this.handshakeTimeoutMillis = handshakeTimeoutMillis;
        }
    }

    public void setHandshakeTimeout( long handshakeTimeout, TimeUnit unit )
    {
        if ( unit == null )
        {
            throw new NullPointerException( "unit" );
        }
        else
        {
            this.setHandshakeTimeoutMillis( unit.toMillis( handshakeTimeout ) );
        }
    }

    public final void setWrapDataSize( int wrapDataSize )
    {
        this.wrapDataSize = wrapDataSize;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public long getCloseNotifyTimeoutMillis()
    {
        return this.getCloseNotifyFlushTimeoutMillis();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setCloseNotifyTimeoutMillis( long closeNotifyFlushTimeoutMillis )
    {
        this.setCloseNotifyFlushTimeoutMillis( closeNotifyFlushTimeoutMillis );
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setCloseNotifyTimeout( long closeNotifyTimeout, TimeUnit unit )
    {
        this.setCloseNotifyFlushTimeout( closeNotifyTimeout, unit );
    }

    public final long getCloseNotifyFlushTimeoutMillis()
    {
        return this.closeNotifyFlushTimeoutMillis;
    }

    public final void setCloseNotifyFlushTimeoutMillis( long closeNotifyFlushTimeoutMillis )
    {
        if ( closeNotifyFlushTimeoutMillis < 0L )
        {
            throw new IllegalArgumentException( "closeNotifyFlushTimeoutMillis: " + closeNotifyFlushTimeoutMillis + " (expected: >= 0)" );
        }
        else
        {
            this.closeNotifyFlushTimeoutMillis = closeNotifyFlushTimeoutMillis;
        }
    }

    public final void setCloseNotifyFlushTimeout( long closeNotifyFlushTimeout, TimeUnit unit )
    {
        this.setCloseNotifyFlushTimeoutMillis( unit.toMillis( closeNotifyFlushTimeout ) );
    }

    public final long getCloseNotifyReadTimeoutMillis()
    {
        return this.closeNotifyReadTimeoutMillis;
    }

    public final void setCloseNotifyReadTimeoutMillis( long closeNotifyReadTimeoutMillis )
    {
        if ( closeNotifyReadTimeoutMillis < 0L )
        {
            throw new IllegalArgumentException( "closeNotifyReadTimeoutMillis: " + closeNotifyReadTimeoutMillis + " (expected: >= 0)" );
        }
        else
        {
            this.closeNotifyReadTimeoutMillis = closeNotifyReadTimeoutMillis;
        }
    }

    public final void setCloseNotifyReadTimeout( long closeNotifyReadTimeout, TimeUnit unit )
    {
        this.setCloseNotifyReadTimeoutMillis( unit.toMillis( closeNotifyReadTimeout ) );
    }

    public SSLEngine engine()
    {
        return this.engine;
    }

    public String applicationProtocol()
    {
        SSLEngine engine = this.engine();
        return !(engine instanceof ApplicationProtocolAccessor) ? null : ((ApplicationProtocolAccessor) engine).getNegotiatedApplicationProtocol();
    }

    public Future<Channel> handshakeFuture()
    {
        return this.handshakePromise;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public ChannelFuture close()
    {
        return this.closeOutbound();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public ChannelFuture close( ChannelPromise promise )
    {
        return this.closeOutbound( promise );
    }

    public ChannelFuture closeOutbound()
    {
        return this.closeOutbound( this.ctx.newPromise() );
    }

    public ChannelFuture closeOutbound( final ChannelPromise promise )
    {
        ChannelHandlerContext ctx = this.ctx;
        if ( ctx.executor().inEventLoop() )
        {
            this.closeOutbound0( promise );
        }
        else
        {
            ctx.executor().execute( new Runnable()
            {
                public void run()
                {
                    SslHandler.this.closeOutbound0( promise );
                }
            } );
        }

        return promise;
    }

    private void closeOutbound0( ChannelPromise promise )
    {
        this.outboundClosed = true;
        this.engine.closeOutbound();

        try
        {
            this.flush( this.ctx, promise );
        }
        catch ( Exception var3 )
        {
            if ( !promise.tryFailure( var3 ) )
            {
                logger.warn( "{} flush() raised a masked exception.", this.ctx.channel(), var3 );
            }
        }
    }

    public Future<Channel> sslCloseFuture()
    {
        return this.sslClosePromise;
    }

    public void handlerRemoved0( ChannelHandlerContext ctx ) throws Exception
    {
        if ( !this.pendingUnencryptedWrites.isEmpty() )
        {
            this.pendingUnencryptedWrites.releaseAndFailAll( ctx, new ChannelException( "Pending write on removal of SslHandler" ) );
        }

        this.pendingUnencryptedWrites = null;
        if ( this.engine instanceof ReferenceCounted )
        {
            ((ReferenceCounted) this.engine).release();
        }
    }

    public void bind( ChannelHandlerContext ctx, SocketAddress localAddress, ChannelPromise promise ) throws Exception
    {
        ctx.bind( localAddress, promise );
    }

    public void connect( ChannelHandlerContext ctx, SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise ) throws Exception
    {
        ctx.connect( remoteAddress, localAddress, promise );
    }

    public void deregister( ChannelHandlerContext ctx, ChannelPromise promise ) throws Exception
    {
        ctx.deregister( promise );
    }

    public void disconnect( ChannelHandlerContext ctx, ChannelPromise promise ) throws Exception
    {
        this.closeOutboundAndChannel( ctx, promise, true );
    }

    public void close( ChannelHandlerContext ctx, ChannelPromise promise ) throws Exception
    {
        this.closeOutboundAndChannel( ctx, promise, false );
    }

    public void read( ChannelHandlerContext ctx ) throws Exception
    {
        if ( !this.handshakePromise.isDone() )
        {
            this.readDuringHandshake = true;
        }

        ctx.read();
    }

    public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise ) throws Exception
    {
        if ( !(msg instanceof ByteBuf) )
        {
            UnsupportedMessageTypeException exception = new UnsupportedMessageTypeException( msg, new Class[]{ByteBuf.class} );
            ReferenceCountUtil.safeRelease( msg );
            promise.setFailure( exception );
        }
        else if ( this.pendingUnencryptedWrites == null )
        {
            ReferenceCountUtil.safeRelease( msg );
            promise.setFailure( newPendingWritesNullException() );
        }
        else
        {
            this.pendingUnencryptedWrites.add( (ByteBuf) msg, promise );
        }
    }

    public void flush( ChannelHandlerContext ctx ) throws Exception
    {
        if ( this.startTls && !this.sentFirstMessage )
        {
            this.sentFirstMessage = true;
            this.pendingUnencryptedWrites.writeAndRemoveAll( ctx );
            this.forceFlush( ctx );
            this.startHandshakeProcessing();
        }
        else if ( !this.processTask )
        {
            try
            {
                this.wrapAndFlush( ctx );
            }
            catch ( Throwable var3 )
            {
                this.setHandshakeFailure( ctx, var3 );
                PlatformDependent.throwException( var3 );
            }
        }
    }

    private void wrapAndFlush( ChannelHandlerContext ctx ) throws SSLException
    {
        if ( this.pendingUnencryptedWrites.isEmpty() )
        {
            this.pendingUnencryptedWrites.add( Unpooled.EMPTY_BUFFER, ctx.newPromise() );
        }

        if ( !this.handshakePromise.isDone() )
        {
            this.flushedBeforeHandshake = true;
        }

        try
        {
            this.wrap( ctx, false );
        }
        finally
        {
            this.forceFlush( ctx );
        }
    }

    private void wrap( ChannelHandlerContext ctx, boolean inUnwrap ) throws SSLException
    {
        ByteBuf out = null;
        ChannelPromise promise = null;
        ByteBufAllocator alloc = ctx.alloc();
        boolean needUnwrap = false;
        ByteBuf buf = null;

        try
        {
            int wrapDataSize = this.wrapDataSize;

            while ( !ctx.isRemoved() )
            {
                promise = ctx.newPromise();
                buf = wrapDataSize > 0 ? this.pendingUnencryptedWrites.remove( alloc, wrapDataSize, promise )
                                       : this.pendingUnencryptedWrites.removeFirst( promise );
                if ( buf == null )
                {
                    break;
                }

                if ( out == null )
                {
                    out = this.allocateOutNetBuf( ctx, buf.readableBytes(), buf.nioBufferCount() );
                }

                SSLEngineResult result = this.wrap( alloc, this.engine, buf, out );
                if ( result.getStatus() == Status.CLOSED )
                {
                    buf.release();
                    buf = null;
                    SSLException exception = new SSLException( "SSLEngine closed already" );
                    promise.tryFailure( exception );
                    promise = null;
                    this.pendingUnencryptedWrites.releaseAndFailAll( ctx, exception );
                    return;
                }

                if ( buf.isReadable() )
                {
                    this.pendingUnencryptedWrites.addFirst( buf, promise );
                    promise = null;
                }
                else
                {
                    buf.release();
                }

                buf = null;
                switch ( result.getHandshakeStatus() )
                {
                case NEED_TASK:
                    if ( !this.runDelegatedTasks( inUnwrap ) )
                    {
                        return;
                    }
                    break;
                case FINISHED:
                    this.setHandshakeSuccess();
                case NOT_HANDSHAKING:
                    this.setHandshakeSuccessIfStillHandshaking();
                case NEED_WRAP:
                    this.finishWrap( ctx, out, promise, inUnwrap, false );
                    promise = null;
                    out = null;
                    break;
                case NEED_UNWRAP:
                    needUnwrap = true;
                    return;
                default:
                    throw new IllegalStateException( "Unknown handshake status: " + result.getHandshakeStatus() );
                }
            }
        }
        finally
        {
            if ( buf != null )
            {
                buf.release();
            }

            this.finishWrap( ctx, out, promise, inUnwrap, needUnwrap );
        }
    }

    private void finishWrap( ChannelHandlerContext ctx, ByteBuf out, ChannelPromise promise, boolean inUnwrap, boolean needUnwrap )
    {
        if ( out == null )
        {
            out = Unpooled.EMPTY_BUFFER;
        }
        else if ( !out.isReadable() )
        {
            out.release();
            out = Unpooled.EMPTY_BUFFER;
        }

        if ( promise != null )
        {
            ctx.write( out, promise );
        }
        else
        {
            ctx.write( out );
        }

        if ( inUnwrap )
        {
            this.needsFlush = true;
        }

        if ( needUnwrap )
        {
            this.readIfNeeded( ctx );
        }
    }

    private boolean wrapNonAppData( final ChannelHandlerContext ctx, boolean inUnwrap ) throws SSLException
    {
        ByteBuf out = null;
        ByteBufAllocator alloc = ctx.alloc();

        try
        {
            SSLEngineResult result;
            HandshakeStatus status;
            do
            {
                if ( ctx.isRemoved() )
                {
                    return false;
                }

                if ( out == null )
                {
                    out = this.allocateOutNetBuf( ctx, 2048, 1 );
                }

                result = this.wrap( alloc, this.engine, Unpooled.EMPTY_BUFFER, out );
                if ( result.bytesProduced() > 0 )
                {
                    ctx.write( out ).addListener( new ChannelFutureListener()
                    {
                        public void operationComplete( ChannelFuture future )
                        {
                            Throwable cause = future.cause();
                            if ( cause != null )
                            {
                                SslHandler.this.setHandshakeFailureTransportFailure( ctx, cause );
                            }
                        }
                    } );
                    if ( inUnwrap )
                    {
                        this.needsFlush = true;
                    }

                    out = null;
                }

                status = result.getHandshakeStatus();
                boolean var7;
                switch ( status )
                {
                case NEED_TASK:
                    if ( !this.runDelegatedTasks( inUnwrap ) )
                    {
                        return false;
                    }
                    break;
                case FINISHED:
                    this.setHandshakeSuccess();
                    var7 = false;
                    return var7;
                case NOT_HANDSHAKING:
                    this.setHandshakeSuccessIfStillHandshaking();
                    if ( !inUnwrap )
                    {
                        this.unwrapNonAppData( ctx );
                    }

                    var7 = true;
                    return var7;
                case NEED_WRAP:
                    break;
                case NEED_UNWRAP:
                    if ( inUnwrap )
                    {
                        var7 = false;
                        return var7;
                    }

                    this.unwrapNonAppData( ctx );
                    break;
                default:
                    throw new IllegalStateException( "Unknown handshake status: " + result.getHandshakeStatus() );
                }
            }
            while ( (result.bytesProduced() != 0 || status == HandshakeStatus.NEED_TASK) &&
                    (result.bytesConsumed() != 0 || result.getHandshakeStatus() != HandshakeStatus.NOT_HANDSHAKING) );

            return false;
        }
        finally
        {
            if ( out != null )
            {
                out.release();
            }
        }
    }

    private SSLEngineResult wrap( ByteBufAllocator alloc, SSLEngine engine, ByteBuf in, ByteBuf out ) throws SSLException
    {
        ByteBuf newDirectIn = null;

        try
        {
            int readerIndex = in.readerIndex();
            int readableBytes = in.readableBytes();
            ByteBuffer[] in0;
            if ( !in.isDirect() && this.engineType.wantsDirectBuffer )
            {
                newDirectIn = alloc.directBuffer( readableBytes );
                newDirectIn.writeBytes( in, readerIndex, readableBytes );
                in0 = this.singleBuffer;
                in0[0] = newDirectIn.internalNioBuffer( newDirectIn.readerIndex(), readableBytes );
            }
            else if ( !(in instanceof CompositeByteBuf) && in.nioBufferCount() == 1 )
            {
                in0 = this.singleBuffer;
                in0[0] = in.internalNioBuffer( readerIndex, readableBytes );
            }
            else
            {
                in0 = in.nioBuffers();
            }

            while ( true )
            {
                ByteBuffer out0 = out.nioBuffer( out.writerIndex(), out.writableBytes() );
                SSLEngineResult result = engine.wrap( in0, out0 );
                in.skipBytes( result.bytesConsumed() );
                out.writerIndex( out.writerIndex() + result.bytesProduced() );
                switch ( result.getStatus() )
                {
                case BUFFER_OVERFLOW:
                    out.ensureWritable( engine.getSession().getPacketBufferSize() );
                    break;
                default:
                    SSLEngineResult var11 = result;
                    return var11;
                }
            }
        }
        finally
        {
            this.singleBuffer[0] = null;
            if ( newDirectIn != null )
            {
                newDirectIn.release();
            }
        }
    }

    public void channelInactive( ChannelHandlerContext ctx ) throws Exception
    {
        ClosedChannelException exception = new ClosedChannelException();
        this.setHandshakeFailure( ctx, exception, !this.outboundClosed, this.handshakeStarted, false );
        this.notifyClosePromise( exception );
        super.channelInactive( ctx );
    }

    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception
    {
        if ( this.ignoreException( cause ) )
        {
            if ( logger.isDebugEnabled() )
            {
                logger.debug(
                        "{} Swallowing a harmless 'connection reset by peer / broken pipe' error that occurred while writing close_notify in response to the peer's close_notify",
                        ctx.channel(), cause );
            }

            if ( ctx.channel().isActive() )
            {
                ctx.close();
            }
        }
        else
        {
            ctx.fireExceptionCaught( cause );
        }
    }

    private boolean ignoreException( Throwable t )
    {
        if ( !(t instanceof SSLException) && t instanceof IOException && this.sslClosePromise.isDone() )
        {
            String message = t.getMessage();
            if ( message != null && IGNORABLE_ERROR_MESSAGE.matcher( message ).matches() )
            {
                return true;
            }

            StackTraceElement[] elements = t.getStackTrace();
            StackTraceElement[] var4 = elements;
            int var5 = elements.length;

            for ( int var6 = 0; var6 < var5; ++var6 )
            {
                StackTraceElement element = var4[var6];
                String classname = element.getClassName();
                String methodname = element.getMethodName();
                if ( !classname.startsWith( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty." ) && "read".equals( methodname ) )
                {
                    if ( IGNORABLE_CLASS_IN_STACK.matcher( classname ).matches() )
                    {
                        return true;
                    }

                    try
                    {
                        Class<?> clazz = PlatformDependent.getClassLoader( this.getClass() ).loadClass( classname );
                        if ( SocketChannel.class.isAssignableFrom( clazz ) || DatagramChannel.class.isAssignableFrom( clazz ) )
                        {
                            return true;
                        }

                        if ( PlatformDependent.javaVersion() >= 7 && "com.sun.nio.sctp.SctpChannel".equals( clazz.getSuperclass().getName() ) )
                        {
                            return true;
                        }
                    }
                    catch ( Throwable var11 )
                    {
                        logger.debug( "Unexpected exception while loading class {} classname {}", this.getClass(), classname, var11 );
                    }
                }
            }
        }

        return false;
    }

    private void decodeJdkCompatible( ChannelHandlerContext ctx, ByteBuf in ) throws NotSslRecordException
    {
        int packetLength = this.packetLength;
        int readableBytes;
        if ( packetLength > 0 )
        {
            if ( in.readableBytes() < packetLength )
            {
                return;
            }
        }
        else
        {
            readableBytes = in.readableBytes();
            if ( readableBytes < 5 )
            {
                return;
            }

            packetLength = SslUtils.getEncryptedPacketLength( in, in.readerIndex() );
            if ( packetLength == -2 )
            {
                NotSslRecordException e = new NotSslRecordException( "not an SSL/TLS record: " + ByteBufUtil.hexDump( in ) );
                in.skipBytes( in.readableBytes() );
                this.setHandshakeFailure( ctx, e );
                throw e;
            }

            assert packetLength > 0;

            if ( packetLength > readableBytes )
            {
                this.packetLength = packetLength;
                return;
            }
        }

        this.packetLength = 0;

        try
        {
            readableBytes = this.unwrap( ctx, in, in.readerIndex(), packetLength );

            assert readableBytes == packetLength || this.engine.isInboundDone() :
                    "we feed the SSLEngine a packets worth of data: " + packetLength + " but it only consumed: " + readableBytes;

            in.skipBytes( readableBytes );
        }
        catch ( Throwable var6 )
        {
            this.handleUnwrapThrowable( ctx, var6 );
        }
    }

    private void decodeNonJdkCompatible( ChannelHandlerContext ctx, ByteBuf in )
    {
        try
        {
            in.skipBytes( this.unwrap( ctx, in, in.readerIndex(), in.readableBytes() ) );
        }
        catch ( Throwable var4 )
        {
            this.handleUnwrapThrowable( ctx, var4 );
        }
    }

    private void handleUnwrapThrowable( ChannelHandlerContext ctx, Throwable cause )
    {
        try
        {
            if ( this.handshakePromise.tryFailure( cause ) )
            {
                ctx.fireUserEventTriggered( new SslHandshakeCompletionEvent( cause ) );
            }

            this.wrapAndFlush( ctx );
        }
        catch ( SSLException var7 )
        {
            logger.debug( "SSLException during trying to call SSLEngine.wrap(...) because of an previous SSLException, ignoring...", (Throwable) var7 );
        }
        finally
        {
            this.setHandshakeFailure( ctx, cause, true, false, true );
        }

        PlatformDependent.throwException( cause );
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out ) throws SSLException
    {
        if ( !this.processTask )
        {
            if ( this.jdkCompatibilityMode )
            {
                this.decodeJdkCompatible( ctx, in );
            }
            else
            {
                this.decodeNonJdkCompatible( ctx, in );
            }
        }
    }

    public void channelReadComplete( ChannelHandlerContext ctx ) throws Exception
    {
        this.channelReadComplete0( ctx );
    }

    private void channelReadComplete0( ChannelHandlerContext ctx )
    {
        this.discardSomeReadBytes();
        this.flushIfNeeded( ctx );
        this.readIfNeeded( ctx );
        this.firedChannelRead = false;
        ctx.fireChannelReadComplete();
    }

    private void readIfNeeded( ChannelHandlerContext ctx )
    {
        if ( !ctx.channel().config().isAutoRead() && (!this.firedChannelRead || !this.handshakePromise.isDone()) )
        {
            ctx.read();
        }
    }

    private void flushIfNeeded( ChannelHandlerContext ctx )
    {
        if ( this.needsFlush )
        {
            this.forceFlush( ctx );
        }
    }

    private void unwrapNonAppData( ChannelHandlerContext ctx ) throws SSLException
    {
        this.unwrap( ctx, Unpooled.EMPTY_BUFFER, 0, 0 );
    }

    private int unwrap( ChannelHandlerContext ctx, ByteBuf packet, int offset, int length ) throws SSLException
    {
        boolean wrapLater = false;
        boolean notifyClosure = false;
        int overflowReadableBytes = -1;
        ByteBuf decodeOut = this.allocate( ctx, length );

        try
        {
            label199:
            while ( !ctx.isRemoved() )
            {
                SSLEngineResult result = this.engineType.unwrap( this, packet, offset, length, decodeOut );
                Status status = result.getStatus();
                HandshakeStatus handshakeStatus = result.getHandshakeStatus();
                int produced = result.bytesProduced();
                int consumed = result.bytesConsumed();
                offset += consumed;
                length -= consumed;
                switch ( status )
                {
                case BUFFER_OVERFLOW:
                    int readableBytes = decodeOut.readableBytes();
                    int previousOverflowReadableBytes = overflowReadableBytes;
                    overflowReadableBytes = readableBytes;
                    int bufferSize = this.engine.getSession().getApplicationBufferSize() - readableBytes;
                    if ( readableBytes > 0 )
                    {
                        this.firedChannelRead = true;
                        ctx.fireChannelRead( decodeOut );
                        decodeOut = null;
                        if ( bufferSize <= 0 )
                        {
                            bufferSize = this.engine.getSession().getApplicationBufferSize();
                        }
                    }
                    else
                    {
                        decodeOut.release();
                        decodeOut = null;
                    }

                    if ( readableBytes == 0 && previousOverflowReadableBytes == 0 )
                    {
                        throw new IllegalStateException(
                                "Two consecutive overflows but no content was consumed. " + SSLSession.class.getSimpleName() + " getApplicationBufferSize: " +
                                        this.engine.getSession().getApplicationBufferSize() + " maybe too small." );
                    }

                    decodeOut = this.allocate( ctx, this.engineType.calculatePendingData( this, bufferSize ) );
                    continue;
                case CLOSED:
                    notifyClosure = true;
                    overflowReadableBytes = -1;
                    break;
                default:
                    overflowReadableBytes = -1;
                }

                switch ( handshakeStatus )
                {
                case NEED_TASK:
                    if ( !this.runDelegatedTasks( true ) )
                    {
                        wrapLater = false;
                        break label199;
                    }
                    break;
                case FINISHED:
                    this.setHandshakeSuccess();
                    wrapLater = true;
                    break;
                case NOT_HANDSHAKING:
                    if ( this.setHandshakeSuccessIfStillHandshaking() )
                    {
                        wrapLater = true;
                        continue;
                    }

                    if ( length == 0 )
                    {
                        break label199;
                    }
                    break;
                case NEED_WRAP:
                    if ( this.wrapNonAppData( ctx, true ) && length == 0 )
                    {
                        break label199;
                    }
                case NEED_UNWRAP:
                    break;
                default:
                    throw new IllegalStateException( "unknown handshake status: " + handshakeStatus );
                }

                if ( status == Status.BUFFER_UNDERFLOW || handshakeStatus != HandshakeStatus.NEED_TASK && consumed == 0 && produced == 0 )
                {
                    if ( handshakeStatus == HandshakeStatus.NEED_UNWRAP )
                    {
                        this.readIfNeeded( ctx );
                    }
                    break;
                }
            }

            if ( this.flushedBeforeHandshake && this.handshakePromise.isDone() )
            {
                this.flushedBeforeHandshake = false;
                wrapLater = true;
            }

            if ( wrapLater )
            {
                this.wrap( ctx, true );
            }

            if ( notifyClosure )
            {
                this.notifyClosePromise( (Throwable) null );
            }
        }
        finally
        {
            if ( decodeOut != null )
            {
                if ( decodeOut.isReadable() )
                {
                    this.firedChannelRead = true;
                    ctx.fireChannelRead( decodeOut );
                }
                else
                {
                    decodeOut.release();
                }
            }
        }

        return length - length;
    }

    private boolean runDelegatedTasks( boolean inUnwrap )
    {
        if ( this.delegatedTaskExecutor != ImmediateExecutor.INSTANCE && !inEventLoop( this.delegatedTaskExecutor ) )
        {
            this.executeDelegatedTasks( inUnwrap );
            return false;
        }
        else
        {
            runAllDelegatedTasks( this.engine );
            return true;
        }
    }

    private void executeDelegatedTasks( boolean inUnwrap )
    {
        this.processTask = true;

        try
        {
            this.delegatedTaskExecutor.execute( new SslHandler.SslTasksRunner( inUnwrap ) );
        }
        catch ( RejectedExecutionException var3 )
        {
            this.processTask = false;
            throw var3;
        }
    }

    private boolean setHandshakeSuccessIfStillHandshaking()
    {
        if ( !this.handshakePromise.isDone() )
        {
            this.setHandshakeSuccess();
            return true;
        }
        else
        {
            return false;
        }
    }

    private void setHandshakeSuccess()
    {
        this.handshakePromise.trySuccess( this.ctx.channel() );
        if ( logger.isDebugEnabled() )
        {
            logger.debug( "{} HANDSHAKEN: {}", this.ctx.channel(), this.engine.getSession().getCipherSuite() );
        }

        this.ctx.fireUserEventTriggered( SslHandshakeCompletionEvent.SUCCESS );
        if ( this.readDuringHandshake && !this.ctx.channel().config().isAutoRead() )
        {
            this.readDuringHandshake = false;
            this.ctx.read();
        }
    }

    private void setHandshakeFailure( ChannelHandlerContext ctx, Throwable cause )
    {
        this.setHandshakeFailure( ctx, cause, true, true, false );
    }

    private void setHandshakeFailure( ChannelHandlerContext ctx, Throwable cause, boolean closeInbound, boolean notify, boolean alwaysFlushAndClose )
    {
        try
        {
            this.outboundClosed = true;
            this.engine.closeOutbound();
            if ( closeInbound )
            {
                try
                {
                    this.engine.closeInbound();
                }
                catch ( SSLException var11 )
                {
                    if ( logger.isDebugEnabled() )
                    {
                        String msg = var11.getMessage();
                        if ( msg == null ||
                                !msg.contains( "possible truncation attack" ) && !msg.contains( "closing inbound before receiving peer's close_notify" ) )
                        {
                            logger.debug( "{} SSLEngine.closeInbound() raised an exception.", ctx.channel(), var11 );
                        }
                    }
                }
            }

            if ( this.handshakePromise.tryFailure( cause ) || alwaysFlushAndClose )
            {
                SslUtils.handleHandshakeFailure( ctx, cause, notify );
            }
        }
        finally
        {
            this.releaseAndFailAll( ctx, cause );
        }
    }

    private void setHandshakeFailureTransportFailure( ChannelHandlerContext ctx, Throwable cause )
    {
        try
        {
            SSLException transportFailure = new SSLException( "failure when writing TLS control frames", cause );
            this.releaseAndFailAll( ctx, transportFailure );
            if ( this.handshakePromise.tryFailure( transportFailure ) )
            {
                ctx.fireUserEventTriggered( new SslHandshakeCompletionEvent( transportFailure ) );
            }
        }
        finally
        {
            ctx.close();
        }
    }

    private void releaseAndFailAll( ChannelHandlerContext ctx, Throwable cause )
    {
        if ( this.pendingUnencryptedWrites != null )
        {
            this.pendingUnencryptedWrites.releaseAndFailAll( ctx, cause );
        }
    }

    private void notifyClosePromise( Throwable cause )
    {
        if ( cause == null )
        {
            if ( this.sslClosePromise.trySuccess( this.ctx.channel() ) )
            {
                this.ctx.fireUserEventTriggered( SslCloseCompletionEvent.SUCCESS );
            }
        }
        else if ( this.sslClosePromise.tryFailure( cause ) )
        {
            this.ctx.fireUserEventTriggered( new SslCloseCompletionEvent( cause ) );
        }
    }

    private void closeOutboundAndChannel( ChannelHandlerContext ctx, final ChannelPromise promise, boolean disconnect ) throws Exception
    {
        this.outboundClosed = true;
        this.engine.closeOutbound();
        if ( !ctx.channel().isActive() )
        {
            if ( disconnect )
            {
                ctx.disconnect( promise );
            }
            else
            {
                ctx.close( promise );
            }
        }
        else
        {
            ChannelPromise closeNotifyPromise = ctx.newPromise();

            try
            {
                this.flush( ctx, closeNotifyPromise );
            }
            finally
            {
                if ( !this.closeNotify )
                {
                    this.closeNotify = true;
                    this.safeClose( ctx, closeNotifyPromise,
                            ctx.newPromise().addListener( new ChannelPromiseNotifier( false, new ChannelPromise[]{promise} ) ) );
                }
                else
                {
                    this.sslClosePromise.addListener( new FutureListener<Channel>()
                    {
                        public void operationComplete( Future<Channel> future )
                        {
                            promise.setSuccess();
                        }
                    } );
                }
            }
        }
    }

    private void flush( ChannelHandlerContext ctx, ChannelPromise promise ) throws Exception
    {
        if ( this.pendingUnencryptedWrites != null )
        {
            this.pendingUnencryptedWrites.add( Unpooled.EMPTY_BUFFER, promise );
        }
        else
        {
            promise.setFailure( newPendingWritesNullException() );
        }

        this.flush( ctx );
    }

    public void handlerAdded( ChannelHandlerContext ctx ) throws Exception
    {
        this.ctx = ctx;
        this.pendingUnencryptedWrites = new SslHandler.SslHandlerCoalescingBufferQueue( ctx.channel(), 16 );
        if ( ctx.channel().isActive() )
        {
            this.startHandshakeProcessing();
        }
    }

    private void startHandshakeProcessing()
    {
        if ( !this.handshakeStarted )
        {
            this.handshakeStarted = true;
            if ( this.engine.getUseClientMode() )
            {
                this.handshake();
            }

            this.applyHandshakeTimeout();
        }
    }

    public Future<Channel> renegotiate()
    {
        ChannelHandlerContext ctx = this.ctx;
        if ( ctx == null )
        {
            throw new IllegalStateException();
        }
        else
        {
            return this.renegotiate( ctx.executor().newPromise() );
        }
    }

    public Future<Channel> renegotiate( final Promise<Channel> promise )
    {
        if ( promise == null )
        {
            throw new NullPointerException( "promise" );
        }
        else
        {
            ChannelHandlerContext ctx = this.ctx;
            if ( ctx == null )
            {
                throw new IllegalStateException();
            }
            else
            {
                EventExecutor executor = ctx.executor();
                if ( !executor.inEventLoop() )
                {
                    executor.execute( new Runnable()
                    {
                        public void run()
                        {
                            SslHandler.this.renegotiateOnEventLoop( promise );
                        }
                    } );
                    return promise;
                }
                else
                {
                    this.renegotiateOnEventLoop( promise );
                    return promise;
                }
            }
        }
    }

    private void renegotiateOnEventLoop( Promise<Channel> newHandshakePromise )
    {
        Promise<Channel> oldHandshakePromise = this.handshakePromise;
        if ( !oldHandshakePromise.isDone() )
        {
            oldHandshakePromise.addListener( new PromiseNotifier( new Promise[]{newHandshakePromise} ) );
        }
        else
        {
            this.handshakePromise = newHandshakePromise;
            this.handshake();
            this.applyHandshakeTimeout();
        }
    }

    private void handshake()
    {
        if ( this.engine.getHandshakeStatus() == HandshakeStatus.NOT_HANDSHAKING )
        {
            if ( !this.handshakePromise.isDone() )
            {
                ChannelHandlerContext ctx = this.ctx;

                try
                {
                    this.engine.beginHandshake();
                    this.wrapNonAppData( ctx, false );
                }
                catch ( Throwable var6 )
                {
                    this.setHandshakeFailure( ctx, var6 );
                }
                finally
                {
                    this.forceFlush( ctx );
                }
            }
        }
    }

    private void applyHandshakeTimeout()
    {
        final Promise<Channel> localHandshakePromise = this.handshakePromise;
        long handshakeTimeoutMillis = this.handshakeTimeoutMillis;
        if ( handshakeTimeoutMillis > 0L && !localHandshakePromise.isDone() )
        {
            final ScheduledFuture<?> timeoutFuture = this.ctx.executor().schedule( new Runnable()
            {
                public void run()
                {
                    if ( !localHandshakePromise.isDone() )
                    {
                        SSLException exception = new SSLException( "handshake timed out" );

                        try
                        {
                            if ( localHandshakePromise.tryFailure( exception ) )
                            {
                                SslUtils.handleHandshakeFailure( SslHandler.this.ctx, exception, true );
                            }
                        }
                        finally
                        {
                            SslHandler.this.releaseAndFailAll( SslHandler.this.ctx, exception );
                        }
                    }
                }
            }, handshakeTimeoutMillis, TimeUnit.MILLISECONDS );
            localHandshakePromise.addListener( new FutureListener<Channel>()
            {
                public void operationComplete( Future<Channel> f ) throws Exception
                {
                    timeoutFuture.cancel( false );
                }
            } );
        }
    }

    private void forceFlush( ChannelHandlerContext ctx )
    {
        this.needsFlush = false;
        ctx.flush();
    }

    public void channelActive( ChannelHandlerContext ctx ) throws Exception
    {
        if ( !this.startTls )
        {
            this.startHandshakeProcessing();
        }

        ctx.fireChannelActive();
    }

    private void safeClose( final ChannelHandlerContext ctx, final ChannelFuture flushFuture, final ChannelPromise promise )
    {
        if ( !ctx.channel().isActive() )
        {
            ctx.close( promise );
        }
        else
        {
            final com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.ScheduledFuture timeoutFuture;
            if ( !flushFuture.isDone() )
            {
                long closeNotifyTimeout = this.closeNotifyFlushTimeoutMillis;
                if ( closeNotifyTimeout > 0L )
                {
                    timeoutFuture = ctx.executor().schedule( new Runnable()
                    {
                        public void run()
                        {
                            if ( !flushFuture.isDone() )
                            {
                                SslHandler.logger.warn( "{} Last write attempt timed out; force-closing the connection.", (Object) ctx.channel() );
                                SslHandler.addCloseListener( ctx.close( ctx.newPromise() ), promise );
                            }
                        }
                    }, closeNotifyTimeout, TimeUnit.MILLISECONDS );
                }
                else
                {
                    timeoutFuture = null;
                }
            }
            else
            {
                timeoutFuture = null;
            }

            flushFuture.addListener( new ChannelFutureListener()
            {
                public void operationComplete( ChannelFuture f ) throws Exception
                {
                    if ( timeoutFuture != null )
                    {
                        timeoutFuture.cancel( false );
                    }

                    final long closeNotifyReadTimeout = SslHandler.this.closeNotifyReadTimeoutMillis;
                    if ( closeNotifyReadTimeout <= 0L )
                    {
                        SslHandler.addCloseListener( ctx.close( ctx.newPromise() ), promise );
                    }
                    else
                    {
                        final com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.ScheduledFuture closeNotifyReadTimeoutFuture;
                        if ( !SslHandler.this.sslClosePromise.isDone() )
                        {
                            closeNotifyReadTimeoutFuture = ctx.executor().schedule( new Runnable()
                            {
                                public void run()
                                {
                                    if ( !SslHandler.this.sslClosePromise.isDone() )
                                    {
                                        SslHandler.logger.debug( "{} did not receive close_notify in {}ms; force-closing the connection.", ctx.channel(),
                                                closeNotifyReadTimeout );
                                        SslHandler.addCloseListener( ctx.close( ctx.newPromise() ), promise );
                                    }
                                }
                            }, closeNotifyReadTimeout, TimeUnit.MILLISECONDS );
                        }
                        else
                        {
                            closeNotifyReadTimeoutFuture = null;
                        }

                        SslHandler.this.sslClosePromise.addListener( new FutureListener<Channel>()
                        {
                            public void operationComplete( Future<Channel> future ) throws Exception
                            {
                                if ( closeNotifyReadTimeoutFuture != null )
                                {
                                    closeNotifyReadTimeoutFuture.cancel( false );
                                }

                                SslHandler.addCloseListener( ctx.close( ctx.newPromise() ), promise );
                            }
                        } );
                    }
                }
            } );
        }
    }

    private ByteBuf allocate( ChannelHandlerContext ctx, int capacity )
    {
        ByteBufAllocator alloc = ctx.alloc();
        return this.engineType.wantsDirectBuffer ? alloc.directBuffer( capacity ) : alloc.buffer( capacity );
    }

    private ByteBuf allocateOutNetBuf( ChannelHandlerContext ctx, int pendingBytes, int numComponents )
    {
        return this.allocate( ctx, this.engineType.calculateWrapBufferCapacity( this, pendingBytes, numComponents ) );
    }

    private static enum SslEngineType
    {
        TCNATIVE( true, ByteToMessageDecoder.COMPOSITE_CUMULATOR )
                {
                    SSLEngineResult unwrap( SslHandler handler, ByteBuf in, int readerIndex, int len, ByteBuf out ) throws SSLException
                    {
                        int nioBufferCount = in.nioBufferCount();
                        int writerIndex = out.writerIndex();
                        SSLEngineResult result;
                        if ( nioBufferCount > 1 )
                        {
                            ReferenceCountedOpenSslEngine opensslEngine = (ReferenceCountedOpenSslEngine) handler.engine;

                            try
                            {
                                handler.singleBuffer[0] = SslHandler.toByteBuffer( out, writerIndex, out.writableBytes() );
                                result = opensslEngine.unwrap( in.nioBuffers( readerIndex, len ), handler.singleBuffer );
                            }
                            finally
                            {
                                handler.singleBuffer[0] = null;
                            }
                        }
                        else
                        {
                            result = handler.engine.unwrap( SslHandler.toByteBuffer( in, readerIndex, len ),
                                    SslHandler.toByteBuffer( out, writerIndex, out.writableBytes() ) );
                        }

                        out.writerIndex( writerIndex + result.bytesProduced() );
                        return result;
                    }

                    int getPacketBufferSize( SslHandler handler )
                    {
                        return ((ReferenceCountedOpenSslEngine) handler.engine).maxEncryptedPacketLength0();
                    }

                    int calculateWrapBufferCapacity( SslHandler handler, int pendingBytes, int numComponents )
                    {
                        return ((ReferenceCountedOpenSslEngine) handler.engine).calculateMaxLengthForWrap( pendingBytes, numComponents );
                    }

                    int calculatePendingData( SslHandler handler, int guess )
                    {
                        int sslPending = ((ReferenceCountedOpenSslEngine) handler.engine).sslPending();
                        return sslPending > 0 ? sslPending : guess;
                    }

                    boolean jdkCompatibilityMode( SSLEngine engine )
                    {
                        return ((ReferenceCountedOpenSslEngine) engine).jdkCompatibilityMode;
                    }
                },
        CONSCRYPT( true, ByteToMessageDecoder.COMPOSITE_CUMULATOR )
                {
                    SSLEngineResult unwrap( SslHandler handler, ByteBuf in, int readerIndex, int len, ByteBuf out ) throws SSLException
                    {
                        int nioBufferCount = in.nioBufferCount();
                        int writerIndex = out.writerIndex();
                        SSLEngineResult result;
                        if ( nioBufferCount > 1 )
                        {
                            try
                            {
                                handler.singleBuffer[0] = SslHandler.toByteBuffer( out, writerIndex, out.writableBytes() );
                                result = ((ConscryptAlpnSslEngine) handler.engine).unwrap( in.nioBuffers( readerIndex, len ), handler.singleBuffer );
                            }
                            finally
                            {
                                handler.singleBuffer[0] = null;
                            }
                        }
                        else
                        {
                            result = handler.engine.unwrap( SslHandler.toByteBuffer( in, readerIndex, len ),
                                    SslHandler.toByteBuffer( out, writerIndex, out.writableBytes() ) );
                        }

                        out.writerIndex( writerIndex + result.bytesProduced() );
                        return result;
                    }

                    int calculateWrapBufferCapacity( SslHandler handler, int pendingBytes, int numComponents )
                    {
                        return ((ConscryptAlpnSslEngine) handler.engine).calculateOutNetBufSize( pendingBytes, numComponents );
                    }

                    int calculatePendingData( SslHandler handler, int guess )
                    {
                        return guess;
                    }

                    boolean jdkCompatibilityMode( SSLEngine engine )
                    {
                        return true;
                    }
                },
        JDK( false, ByteToMessageDecoder.MERGE_CUMULATOR )
                {
                    SSLEngineResult unwrap( SslHandler handler, ByteBuf in, int readerIndex, int len, ByteBuf out ) throws SSLException
                    {
                        int writerIndex = out.writerIndex();
                        ByteBuffer inNioBuffer = SslHandler.toByteBuffer( in, readerIndex, len );
                        int position = inNioBuffer.position();
                        SSLEngineResult result = handler.engine.unwrap( inNioBuffer, SslHandler.toByteBuffer( out, writerIndex, out.writableBytes() ) );
                        out.writerIndex( writerIndex + result.bytesProduced() );
                        if ( result.bytesConsumed() == 0 )
                        {
                            int consumed = inNioBuffer.position() - position;
                            if ( consumed != result.bytesConsumed() )
                            {
                                return new SSLEngineResult( result.getStatus(), result.getHandshakeStatus(), consumed, result.bytesProduced() );
                            }
                        }

                        return result;
                    }

                    int calculateWrapBufferCapacity( SslHandler handler, int pendingBytes, int numComponents )
                    {
                        return handler.engine.getSession().getPacketBufferSize();
                    }

                    int calculatePendingData( SslHandler handler, int guess )
                    {
                        return guess;
                    }

                    boolean jdkCompatibilityMode( SSLEngine engine )
                    {
                        return true;
                    }
                };

        final boolean wantsDirectBuffer;
        final ByteToMessageDecoder.Cumulator cumulator;

        private SslEngineType( boolean wantsDirectBuffer, ByteToMessageDecoder.Cumulator cumulator )
        {
            this.wantsDirectBuffer = wantsDirectBuffer;
            this.cumulator = cumulator;
        }

        static SslHandler.SslEngineType forEngine( SSLEngine engine )
        {
            return engine instanceof ReferenceCountedOpenSslEngine ? TCNATIVE : (engine instanceof ConscryptAlpnSslEngine ? CONSCRYPT : JDK);
        }

        int getPacketBufferSize( SslHandler handler )
        {
            return handler.engine.getSession().getPacketBufferSize();
        }

        abstract SSLEngineResult unwrap( SslHandler var1, ByteBuf var2, int var3, int var4, ByteBuf var5 ) throws SSLException;

        abstract int calculateWrapBufferCapacity( SslHandler var1, int var2, int var3 );

        abstract int calculatePendingData( SslHandler var1, int var2 );

        abstract boolean jdkCompatibilityMode( SSLEngine var1 );
    }

    private final class LazyChannelPromise extends DefaultPromise<Channel>
    {
        private LazyChannelPromise()
        {
        }

        protected EventExecutor executor()
        {
            if ( SslHandler.this.ctx == null )
            {
                throw new IllegalStateException();
            }
            else
            {
                return SslHandler.this.ctx.executor();
            }
        }

        protected void checkDeadLock()
        {
            if ( SslHandler.this.ctx != null )
            {
                super.checkDeadLock();
            }
        }
    }

    private final class SslHandlerCoalescingBufferQueue extends AbstractCoalescingBufferQueue
    {
        SslHandlerCoalescingBufferQueue( Channel channel, int initSize )
        {
            super( channel, initSize );
        }

        protected ByteBuf compose( ByteBufAllocator alloc, ByteBuf cumulation, ByteBuf next )
        {
            int wrapDataSize = SslHandler.this.wrapDataSize;
            if ( !(cumulation instanceof CompositeByteBuf) )
            {
                return SslHandler.attemptCopyToCumulation( cumulation, next, wrapDataSize ) ? cumulation : this.copyAndCompose( alloc, cumulation, next );
            }
            else
            {
                CompositeByteBuf composite = (CompositeByteBuf) cumulation;
                int numComponents = composite.numComponents();
                if ( numComponents == 0 || !SslHandler.attemptCopyToCumulation( composite.internalComponent( numComponents - 1 ), next, wrapDataSize ) )
                {
                    composite.addComponent( true, next );
                }

                return composite;
            }
        }

        protected ByteBuf composeFirst( ByteBufAllocator allocator, ByteBuf first )
        {
            if ( first instanceof CompositeByteBuf )
            {
                CompositeByteBuf composite = (CompositeByteBuf) first;
                first = allocator.directBuffer( composite.readableBytes() );

                try
                {
                    first.writeBytes( (ByteBuf) composite );
                }
                catch ( Throwable var5 )
                {
                    first.release();
                    PlatformDependent.throwException( var5 );
                }

                composite.release();
            }

            return first;
        }

        protected ByteBuf removeEmptyValue()
        {
            return null;
        }
    }

    private final class SslTasksRunner implements Runnable
    {
        private final boolean inUnwrap;

        SslTasksRunner( boolean inUnwrap )
        {
            this.inUnwrap = inUnwrap;
        }

        private void taskError( Throwable e )
        {
            if ( this.inUnwrap )
            {
                try
                {
                    SslHandler.this.handleUnwrapThrowable( SslHandler.this.ctx, e );
                }
                catch ( Throwable var3 )
                {
                    this.safeExceptionCaught( var3 );
                }
            }
            else
            {
                SslHandler.this.setHandshakeFailure( SslHandler.this.ctx, e );
                SslHandler.this.forceFlush( SslHandler.this.ctx );
            }
        }

        private void safeExceptionCaught( Throwable cause )
        {
            try
            {
                SslHandler.this.exceptionCaught( SslHandler.this.ctx, this.wrapIfNeeded( cause ) );
            }
            catch ( Throwable var3 )
            {
                SslHandler.this.ctx.fireExceptionCaught( var3 );
            }
        }

        private Throwable wrapIfNeeded( Throwable cause )
        {
            if ( !this.inUnwrap )
            {
                return cause;
            }
            else
            {
                return (Throwable) (cause instanceof DecoderException ? cause : new DecoderException( cause ));
            }
        }

        private void tryDecodeAgain()
        {
            try
            {
                SslHandler.this.channelRead( SslHandler.this.ctx, Unpooled.EMPTY_BUFFER );
            }
            catch ( Throwable var5 )
            {
                this.safeExceptionCaught( var5 );
            }
            finally
            {
                SslHandler.this.channelReadComplete0( SslHandler.this.ctx );
            }
        }

        private void resumeOnEventExecutor()
        {
            assert SslHandler.this.ctx.executor().inEventLoop();

            SslHandler.this.processTask = false;

            try
            {
                HandshakeStatus status = SslHandler.this.engine.getHandshakeStatus();
                switch ( status )
                {
                case NEED_TASK:
                    SslHandler.this.executeDelegatedTasks( this.inUnwrap );
                    break;
                case FINISHED:
                    SslHandler.this.setHandshakeSuccess();
                case NOT_HANDSHAKING:
                    SslHandler.this.setHandshakeSuccessIfStillHandshaking();

                    try
                    {
                        SslHandler.this.wrap( SslHandler.this.ctx, this.inUnwrap );
                    }
                    catch ( Throwable var3 )
                    {
                        this.taskError( var3 );
                        return;
                    }

                    if ( this.inUnwrap )
                    {
                        SslHandler.this.unwrapNonAppData( SslHandler.this.ctx );
                    }

                    SslHandler.this.forceFlush( SslHandler.this.ctx );
                    this.tryDecodeAgain();
                    break;
                case NEED_WRAP:
                    try
                    {
                        if ( !SslHandler.this.wrapNonAppData( SslHandler.this.ctx, false ) && this.inUnwrap )
                        {
                            SslHandler.this.unwrapNonAppData( SslHandler.this.ctx );
                        }

                        SslHandler.this.forceFlush( SslHandler.this.ctx );
                    }
                    catch ( Throwable var4 )
                    {
                        this.taskError( var4 );
                        return;
                    }

                    this.tryDecodeAgain();
                    break;
                case NEED_UNWRAP:
                    SslHandler.this.unwrapNonAppData( SslHandler.this.ctx );
                    this.tryDecodeAgain();
                    break;
                default:
                    throw new AssertionError();
                }
            }
            catch ( Throwable var5 )
            {
                this.safeExceptionCaught( var5 );
            }
        }

        public void run()
        {
            try
            {
                SslHandler.runAllDelegatedTasks( SslHandler.this.engine );

                assert SslHandler.this.engine.getHandshakeStatus() != HandshakeStatus.NEED_TASK;

                SslHandler.this.ctx.executor().execute( new Runnable()
                {
                    public void run()
                    {
                        SslTasksRunner.this.resumeOnEventExecutor();
                    }
                } );
            }
            catch ( Throwable var2 )
            {
                this.handleException( var2 );
            }
        }

        private void handleException( final Throwable cause )
        {
            if ( SslHandler.this.ctx.executor().inEventLoop() )
            {
                SslHandler.this.processTask = false;
                this.safeExceptionCaught( cause );
            }
            else
            {
                try
                {
                    SslHandler.this.ctx.executor().execute( new Runnable()
                    {
                        public void run()
                        {
                            SslHandler.this.processTask = false;
                            SslTasksRunner.this.safeExceptionCaught( cause );
                        }
                    } );
                }
                catch ( RejectedExecutionException var3 )
                {
                    SslHandler.this.processTask = false;
                    SslHandler.this.ctx.fireExceptionCaught( cause );
                }
            }
        }
    }
}
