package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

public interface Mapping<IN, OUT>
{
    OUT map( IN var1 );
}
