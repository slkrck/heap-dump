package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxMergeOrdered<T> extends Flux<T> implements SourceProducer<T>
{
    final int prefetch;
    final Supplier<Queue<T>> queueSupplier;
    final Comparator<? super T> valueComparator;
    final Publisher<? extends T>[] sources;

    @SafeVarargs
    FluxMergeOrdered( int prefetch, Supplier<Queue<T>> queueSupplier, Comparator<? super T> valueComparator, Publisher<? extends T>... sources )
    {
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.sources = (Publisher[]) Objects.requireNonNull( sources, "sources must be non-null" );
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
            this.valueComparator = valueComparator;
        }
    }

    FluxMergeOrdered<T> mergeAdditionalSource( Publisher<? extends T> source, Comparator<? super T> otherComparator )
    {
        int n = this.sources.length;
        Publisher<? extends T>[] newArray = new Publisher[n + 1];
        System.arraycopy( this.sources, 0, newArray, 0, n );
        newArray[n] = source;
        if ( !this.valueComparator.equals( otherComparator ) )
        {
            Comparator<T> currentComparator = this.valueComparator;
            Comparator<T> newComparator = currentComparator.thenComparing( otherComparator );
            return new FluxMergeOrdered( this.prefetch, this.queueSupplier, newComparator, newArray );
        }
        else
        {
            return new FluxMergeOrdered( this.prefetch, this.queueSupplier, this.valueComparator, newArray );
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.sources.length > 0 ? this.sources[0] : null;
        }
        else if ( key == Scannable.Attr.PREFETCH )
        {
            return this.prefetch;
        }
        else
        {
            return key == Scannable.Attr.DELAY_ERROR ? true : null;
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        FluxMergeOrdered.MergeOrderedMainProducer<T> main =
                new FluxMergeOrdered.MergeOrderedMainProducer( actual, this.valueComparator, this.prefetch, this.sources.length );
        actual.onSubscribe( main );
        main.subscribe( this.sources );
    }

    static final class MergeOrderedInnerSubscriber<T> implements InnerOperator<T,T>
    {
        final FluxMergeOrdered.MergeOrderedMainProducer<T> parent;
        final int prefetch;
        final int limit;
        final Queue<T> queue;
        int consumed;
        volatile boolean done;
        volatile Subscription s;
        AtomicReferenceFieldUpdater<FluxMergeOrdered.MergeOrderedInnerSubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( FluxMergeOrdered.MergeOrderedInnerSubscriber.class, Subscription.class, "s" );

        MergeOrderedInnerSubscriber( FluxMergeOrdered.MergeOrderedMainProducer<T> parent, int prefetch )
        {
            this.parent = parent;
            this.prefetch = prefetch;
            this.limit = prefetch - (prefetch >> 2);
            this.queue = (Queue) Queues.small().get();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( this.S, this, s ) )
            {
                s.request( (long) this.prefetch );
            }
        }

        public void onNext( T item )
        {
            this.queue.offer( item );
            this.parent.drain();
        }

        public void onError( Throwable throwable )
        {
            this.parent.onInnerError( this, throwable );
        }

        public void onComplete()
        {
            this.done = true;
            this.parent.drain();
        }

        public void request( long n )
        {
            int c = this.consumed + 1;
            if ( c == this.limit )
            {
                this.consumed = 0;
                Subscription sub = this.s;
                if ( sub != this )
                {
                    sub.request( (long) c );
                }
            }
            else
            {
                this.consumed = c;
            }
        }

        public void cancel()
        {
            Subscription sub = (Subscription) this.S.getAndSet( this, this );
            if ( sub != null && sub != this )
            {
                sub.cancel();
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.parent.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.queue.size() : null;
            }
        }
    }

    static final class MergeOrderedMainProducer<T> implements InnerProducer<T>
    {
        static final Object DONE = new Object();
        static final AtomicReferenceFieldUpdater<FluxMergeOrdered.MergeOrderedMainProducer,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( FluxMergeOrdered.MergeOrderedMainProducer.class, Throwable.class, "error" );
        static final AtomicIntegerFieldUpdater<FluxMergeOrdered.MergeOrderedMainProducer> CANCELLED =
                AtomicIntegerFieldUpdater.newUpdater( FluxMergeOrdered.MergeOrderedMainProducer.class, "cancelled" );
        static final AtomicLongFieldUpdater<FluxMergeOrdered.MergeOrderedMainProducer> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxMergeOrdered.MergeOrderedMainProducer.class, "requested" );
        static final AtomicLongFieldUpdater<FluxMergeOrdered.MergeOrderedMainProducer> EMITTED =
                AtomicLongFieldUpdater.newUpdater( FluxMergeOrdered.MergeOrderedMainProducer.class, "emitted" );
        static final AtomicIntegerFieldUpdater<FluxMergeOrdered.MergeOrderedMainProducer> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxMergeOrdered.MergeOrderedMainProducer.class, "wip" );
        final CoreSubscriber<? super T> actual;
        final FluxMergeOrdered.MergeOrderedInnerSubscriber<T>[] subscribers;
        final Comparator<? super T> comparator;
        final Object[] values;
        volatile Throwable error;
        volatile int cancelled;
        volatile long requested;
        volatile long emitted;
        volatile int wip;

        MergeOrderedMainProducer( CoreSubscriber<? super T> actual, Comparator<? super T> comparator, int prefetch, int n )
        {
            this.actual = actual;
            this.comparator = comparator;
            FluxMergeOrdered.MergeOrderedInnerSubscriber<T>[] mergeOrderedInnerSub = new FluxMergeOrdered.MergeOrderedInnerSubscriber[n];
            this.subscribers = mergeOrderedInnerSub;

            for ( int i = 0; i < n; ++i )
            {
                this.subscribers[i] = new FluxMergeOrdered.MergeOrderedInnerSubscriber( this, prefetch );
            }

            this.values = new Object[n];
        }

        void subscribe( Publisher<? extends T>[] sources )
        {
            if ( sources.length != this.subscribers.length )
            {
                throw new IllegalArgumentException( "must subscribe with " + this.subscribers.length + " sources" );
            }
            else
            {
                for ( int i = 0; i < sources.length; ++i )
                {
                    ((Publisher) Objects.requireNonNull( sources[i], "subscribed with a null source: sources[" + i + "]" )).subscribe( this.subscribers[i] );
                }
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            Operators.addCap( REQUESTED, this, n );
            this.drain();
        }

        public void cancel()
        {
            if ( CANCELLED.compareAndSet( this, 0, 1 ) )
            {
                FluxMergeOrdered.MergeOrderedInnerSubscriber[] var1 = this.subscribers;
                int var2 = var1.length;

                int var3;
                FluxMergeOrdered.MergeOrderedInnerSubscriber subscriber;
                for ( var3 = 0; var3 < var2; ++var3 )
                {
                    subscriber = var1[var3];
                    subscriber.cancel();
                }

                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    Arrays.fill( this.values, (Object) null );
                    var1 = this.subscribers;
                    var2 = var1.length;

                    for ( var3 = 0; var3 < var2; ++var3 )
                    {
                        subscriber = var1[var3];
                        subscriber.queue.clear();
                    }
                }
            }
        }

        void onInnerError( FluxMergeOrdered.MergeOrderedInnerSubscriber<T> inner, Throwable ex )
        {
            Exceptions.addThrowable( ERROR, this, ex );
            inner.done = true;
            this.drain();
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                int missed = 1;
                CoreSubscriber<? super T> actual = this.actual;
                Comparator<? super T> comparator = this.comparator;
                FluxMergeOrdered.MergeOrderedInnerSubscriber<T>[] subscribers = this.subscribers;
                int n = subscribers.length;
                Object[] values = this.values;
                long e = this.emitted;

                label105:
                do
                {
                    long r = this.requested;

                    int nonEmpty;
                    int i;
                    while ( this.cancelled == 0 )
                    {
                        int done = 0;
                        nonEmpty = 0;

                        for ( i = 0; i < n; ++i )
                        {
                            Object o = values[i];
                            if ( o == DONE )
                            {
                                ++done;
                                ++nonEmpty;
                            }
                            else if ( o == null )
                            {
                                boolean innerDone = subscribers[i].done;
                                o = subscribers[i].queue.poll();
                                if ( o != null )
                                {
                                    values[i] = o;
                                    ++nonEmpty;
                                }
                                else if ( innerDone )
                                {
                                    values[i] = DONE;
                                    ++done;
                                    ++nonEmpty;
                                }
                            }
                            else
                            {
                                ++nonEmpty;
                            }
                        }

                        if ( done == n )
                        {
                            Throwable ex = this.error;
                            if ( ex == null )
                            {
                                actual.onComplete();
                            }
                            else
                            {
                                actual.onError( ex );
                            }

                            return;
                        }

                        if ( nonEmpty != n || e >= r )
                        {
                            this.emitted = e;
                            missed = WIP.addAndGet( this, -missed );
                            continue label105;
                        }

                        T min = null;
                        int minIndex = -1;
                        int i = 0;
                        Object[] var16 = values;
                        int var17 = values.length;

                        for ( int var18 = 0; var18 < var17; ++var18 )
                        {
                            Object o = var16[var18];
                            if ( o != DONE )
                            {
                                boolean smaller;
                                try
                                {
                                    smaller = min == null || comparator.compare( min, o ) > 0;
                                }
                                catch ( Throwable var22 )
                                {
                                    Exceptions.addThrowable( ERROR, this, var22 );
                                    this.cancel();
                                    actual.onError( Exceptions.terminate( ERROR, this ) );
                                    return;
                                }

                                if ( smaller )
                                {
                                    min = o;
                                    minIndex = i;
                                }
                            }

                            ++i;
                        }

                        values[minIndex] = null;
                        actual.onNext( min );
                        ++e;
                        subscribers[minIndex].request( 1L );
                    }

                    Arrays.fill( values, (Object) null );
                    FluxMergeOrdered.MergeOrderedInnerSubscriber[] var23 = subscribers;
                    nonEmpty = subscribers.length;

                    for ( i = 0; i < nonEmpty; ++i )
                    {
                        FluxMergeOrdered.MergeOrderedInnerSubscriber<T> inner = var23[i];
                        inner.queue.clear();
                    }

                    return;
                }
                while ( missed != 0 );
            }
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ACTUAL )
            {
                return this.actual;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled > 0;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.DELAY_ERROR )
            {
                return true;
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.requested - this.emitted : null;
            }
        }
    }
}
