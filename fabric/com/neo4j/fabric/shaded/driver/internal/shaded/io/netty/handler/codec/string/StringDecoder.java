package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.string;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToMessageDecoder;

import java.nio.charset.Charset;
import java.util.List;

@ChannelHandler.Sharable
public class StringDecoder extends MessageToMessageDecoder<ByteBuf>
{
    private final Charset charset;

    public StringDecoder()
    {
        this( Charset.defaultCharset() );
    }

    public StringDecoder( Charset charset )
    {
        if ( charset == null )
        {
            throw new NullPointerException( "charset" );
        }
        else
        {
            this.charset = charset;
        }
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out ) throws Exception
    {
        out.add( msg.toString( this.charset ) );
    }
}
