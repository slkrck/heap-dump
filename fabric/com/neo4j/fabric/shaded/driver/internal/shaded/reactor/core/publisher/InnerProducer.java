package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.reactivestreams.Subscription;

interface InnerProducer<O> extends Scannable, Subscription
{
    CoreSubscriber<? super O> actual();

    @Nullable
    default Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.ACTUAL ? this.actual() : null;
    }
}
