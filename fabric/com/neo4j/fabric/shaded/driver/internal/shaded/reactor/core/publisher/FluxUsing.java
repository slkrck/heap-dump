package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.function.Consumer;
import java.util.function.Function;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxUsing<T, S> extends Flux<T> implements Fuseable, SourceProducer<T>
{
    final Callable<S> resourceSupplier;
    final Function<? super S,? extends Publisher<? extends T>> sourceFactory;
    final Consumer<? super S> resourceCleanup;
    final boolean eager;

    FluxUsing( Callable<S> resourceSupplier, Function<? super S,? extends Publisher<? extends T>> sourceFactory, Consumer<? super S> resourceCleanup,
            boolean eager )
    {
        this.resourceSupplier = (Callable) Objects.requireNonNull( resourceSupplier, "resourceSupplier" );
        this.sourceFactory = (Function) Objects.requireNonNull( sourceFactory, "sourceFactory" );
        this.resourceCleanup = (Consumer) Objects.requireNonNull( resourceCleanup, "resourceCleanup" );
        this.eager = eager;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Object resource;
        try
        {
            resource = this.resourceSupplier.call();
        }
        catch ( Throwable var9 )
        {
            Operators.error( actual, Operators.onOperatorError( var9, actual.currentContext() ) );
            return;
        }

        Publisher p;
        try
        {
            p = (Publisher) Objects.requireNonNull( this.sourceFactory.apply( resource ), "The sourceFactory returned a null value" );
        }
        catch ( Throwable var8 )
        {
            Throwable _e = Operators.onOperatorError( var8, actual.currentContext() );

            try
            {
                this.resourceCleanup.accept( resource );
            }
            catch ( Throwable var7 )
            {
                _e = Exceptions.addSuppressed( var7, _e );
            }

            Operators.error( actual, _e );
            return;
        }

        if ( p instanceof Fuseable )
        {
            from( p ).subscribe( (CoreSubscriber) (new FluxUsing.UsingFuseableSubscriber( actual, this.resourceCleanup, resource, this.eager )) );
        }
        else if ( actual instanceof Fuseable.ConditionalSubscriber )
        {
            from( p ).subscribe(
                    (CoreSubscriber) (new FluxUsing.UsingConditionalSubscriber( (Fuseable.ConditionalSubscriber) actual, this.resourceCleanup, resource,
                            this.eager )) );
        }
        else
        {
            from( p ).subscribe( (CoreSubscriber) (new FluxUsing.UsingSubscriber( actual, this.resourceCleanup, resource, this.eager )) );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class UsingConditionalSubscriber<T, S> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        static final AtomicIntegerFieldUpdater<FluxUsing.UsingConditionalSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxUsing.UsingConditionalSubscriber.class, "wip" );
        final Fuseable.ConditionalSubscriber<? super T> actual;
        final Consumer<? super S> resourceCleanup;
        final S resource;
        final boolean eager;
        Subscription s;
        volatile int wip;

        UsingConditionalSubscriber( Fuseable.ConditionalSubscriber<? super T> actual, Consumer<? super S> resourceCleanup, S resource, boolean eager )
        {
            this.actual = actual;
            this.resourceCleanup = resourceCleanup;
            this.resource = resource;
            this.eager = eager;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
            }
            else
            {
                return this.wip == 1;
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            if ( WIP.compareAndSet( this, 0, 1 ) )
            {
                this.s.cancel();
                this.cleanup();
            }
        }

        void cleanup()
        {
            try
            {
                this.resourceCleanup.accept( this.resource );
            }
            catch ( Throwable var2 )
            {
                Operators.onErrorDropped( var2, this.actual.currentContext() );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public boolean tryOnNext( T t )
        {
            return this.actual.tryOnNext( t );
        }

        public void onError( Throwable t )
        {
            if ( this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.resourceCleanup.accept( this.resource );
                }
                catch ( Throwable var4 )
                {
                    Throwable _e = Operators.onOperatorError( var4, this.actual.currentContext() );
                    t = Exceptions.addSuppressed( _e, t );
                }
            }

            this.actual.onError( t );
            if ( !this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                this.cleanup();
            }
        }

        public void onComplete()
        {
            if ( this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.resourceCleanup.accept( this.resource );
                }
                catch ( Throwable var2 )
                {
                    this.actual.onError( Operators.onOperatorError( var2, this.actual.currentContext() ) );
                    return;
                }
            }

            this.actual.onComplete();
            if ( !this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                this.cleanup();
            }
        }

        public int requestFusion( int requestedMode )
        {
            return 0;
        }

        public void clear()
        {
        }

        public boolean isEmpty()
        {
            return true;
        }

        @Nullable
        public T poll()
        {
            return null;
        }

        public int size()
        {
            return 0;
        }
    }

    static final class UsingFuseableSubscriber<T, S> implements InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        static final AtomicIntegerFieldUpdater<FluxUsing.UsingFuseableSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxUsing.UsingFuseableSubscriber.class, "wip" );
        final CoreSubscriber<? super T> actual;
        final Consumer<? super S> resourceCleanup;
        final S resource;
        final boolean eager;
        Fuseable.QueueSubscription<T> s;
        volatile int wip;
        int mode;

        UsingFuseableSubscriber( CoreSubscriber<? super T> actual, Consumer<? super S> resourceCleanup, S resource, boolean eager )
        {
            this.actual = actual;
            this.resourceCleanup = resourceCleanup;
            this.resource = resource;
            this.eager = eager;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
            }
            else
            {
                return this.wip == 1;
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            if ( WIP.compareAndSet( this, 0, 1 ) )
            {
                this.s.cancel();
                this.cleanup();
            }
        }

        void cleanup()
        {
            try
            {
                this.resourceCleanup.accept( this.resource );
            }
            catch ( Throwable var2 )
            {
                Operators.onErrorDropped( var2, this.actual.currentContext() );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = (Fuseable.QueueSubscription) s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            if ( this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.resourceCleanup.accept( this.resource );
                }
                catch ( Throwable var4 )
                {
                    Throwable _e = Operators.onOperatorError( var4, this.actual.currentContext() );
                    t = Exceptions.addSuppressed( _e, t );
                }
            }

            this.actual.onError( t );
            if ( !this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                this.cleanup();
            }
        }

        public void onComplete()
        {
            if ( this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.resourceCleanup.accept( this.resource );
                }
                catch ( Throwable var2 )
                {
                    this.actual.onError( Operators.onOperatorError( var2, this.actual.currentContext() ) );
                    return;
                }
            }

            this.actual.onComplete();
            if ( !this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                this.cleanup();
            }
        }

        public void clear()
        {
            this.s.clear();
        }

        public boolean isEmpty()
        {
            return this.s.isEmpty();
        }

        @Nullable
        public T poll()
        {
            T v = this.s.poll();
            if ( v == null && this.mode == 1 && WIP.compareAndSet( this, 0, 1 ) )
            {
                this.resourceCleanup.accept( this.resource );
            }

            return v;
        }

        public int requestFusion( int requestedMode )
        {
            int m = this.s.requestFusion( requestedMode );
            this.mode = m;
            return m;
        }

        public int size()
        {
            return this.s.size();
        }
    }

    static final class UsingSubscriber<T, S> implements InnerOperator<T,T>, Fuseable.QueueSubscription<T>
    {
        static final AtomicIntegerFieldUpdater<FluxUsing.UsingSubscriber> WIP = AtomicIntegerFieldUpdater.newUpdater( FluxUsing.UsingSubscriber.class, "wip" );
        final CoreSubscriber<? super T> actual;
        final Consumer<? super S> resourceCleanup;
        final S resource;
        final boolean eager;
        Subscription s;
        volatile int wip;

        UsingSubscriber( CoreSubscriber<? super T> actual, Consumer<? super S> resourceCleanup, S resource, boolean eager )
        {
            this.actual = actual;
            this.resourceCleanup = resourceCleanup;
            this.resource = resource;
            this.eager = eager;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key != Scannable.Attr.TERMINATED && key != Scannable.Attr.CANCELLED )
            {
                return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
            }
            else
            {
                return this.wip == 1;
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            if ( WIP.compareAndSet( this, 0, 1 ) )
            {
                this.s.cancel();
                this.cleanup();
            }
        }

        void cleanup()
        {
            try
            {
                this.resourceCleanup.accept( this.resource );
            }
            catch ( Throwable var2 )
            {
                Operators.onErrorDropped( var2, this.actual.currentContext() );
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            if ( this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.resourceCleanup.accept( this.resource );
                }
                catch ( Throwable var4 )
                {
                    Throwable _e = Operators.onOperatorError( var4, this.actual.currentContext() );
                    t = Exceptions.addSuppressed( _e, t );
                }
            }

            this.actual.onError( t );
            if ( !this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                this.cleanup();
            }
        }

        public void onComplete()
        {
            if ( this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                try
                {
                    this.resourceCleanup.accept( this.resource );
                }
                catch ( Throwable var2 )
                {
                    this.actual.onError( Operators.onOperatorError( var2, this.actual.currentContext() ) );
                    return;
                }
            }

            this.actual.onComplete();
            if ( !this.eager && WIP.compareAndSet( this, 0, 1 ) )
            {
                this.cleanup();
            }
        }

        public int requestFusion( int requestedMode )
        {
            return 0;
        }

        public void clear()
        {
        }

        public boolean isEmpty()
        {
            return true;
        }

        @Nullable
        public T poll()
        {
            return null;
        }

        public int size()
        {
            return 0;
        }
    }
}
