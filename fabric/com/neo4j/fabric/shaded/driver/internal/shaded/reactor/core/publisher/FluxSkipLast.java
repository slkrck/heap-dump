package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.ArrayDeque;
import java.util.function.Function;

import org.reactivestreams.Subscription;

final class FluxSkipLast<T> extends InternalFluxOperator<T,T>
{
    final int n;

    FluxSkipLast( Flux<? extends T> source, int n )
    {
        super( source );
        if ( n < 0 )
        {
            throw new IllegalArgumentException( "n >= 0 required but it was " + n );
        }
        else
        {
            this.n = n;
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxSkipLast.SkipLastSubscriber( actual, this.n );
    }

    static final class SkipLastSubscriber<T> extends ArrayDeque<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        final int n;
        Subscription s;

        SkipLastSubscriber( CoreSubscriber<? super T> actual, int n )
        {
            this.actual = actual;
            this.n = n;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( (long) this.n );
            }
        }

        public void onNext( T t )
        {
            if ( this.size() == this.n )
            {
                this.actual.onNext( this.pollFirst() );
            }

            this.offerLast( t );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
            Operators.onDiscardQueueWithClear( this, this.actual.currentContext(), (Function) null );
        }

        public void onComplete()
        {
            this.actual.onComplete();
            Operators.onDiscardQueueWithClear( this, this.actual.currentContext(), (Function) null );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.n;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.size() : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
            Operators.onDiscardQueueWithClear( this, this.actual.currentContext(), (Function) null );
        }
    }
}
