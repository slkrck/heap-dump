package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.function.Consumer;

final class MonoDoFinally<T> extends InternalMonoOperator<T,T>
{
    final Consumer<SignalType> onFinally;

    MonoDoFinally( Mono<? extends T> source, Consumer<SignalType> onFinally )
    {
        super( source );
        this.onFinally = onFinally;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return FluxDoFinally.createSubscriber( actual, this.onFinally, false );
    }
}
