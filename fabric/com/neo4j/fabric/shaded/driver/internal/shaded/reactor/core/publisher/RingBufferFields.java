package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import java.util.function.Supplier;

import sun.misc.Unsafe;

abstract class RingBufferFields<E> extends RingBufferPad<E>
{
    private static final int BUFFER_PAD;
    private static final long REF_ARRAY_BASE;
    private static final int REF_ELEMENT_SHIFT;
    private static final Unsafe UNSAFE = (Unsafe) RingBuffer.getUnsafe();

    static
    {
        int scale = UNSAFE.arrayIndexScale( Object[].class );
        if ( 4 == scale )
        {
            REF_ELEMENT_SHIFT = 2;
        }
        else
        {
            if ( 8 != scale )
            {
                throw new IllegalStateException( "Unknown pointer size" );
            }

            REF_ELEMENT_SHIFT = 3;
        }

        BUFFER_PAD = 128 / scale;
        REF_ARRAY_BASE = (long) (UNSAFE.arrayBaseOffset( Object[].class ) + (BUFFER_PAD << REF_ELEMENT_SHIFT));
    }

    protected final int bufferSize;
    protected final RingBufferProducer sequenceProducer;
    private final long indexMask;
    private final Object[] entries;

    RingBufferFields( Supplier<E> eventFactory, RingBufferProducer sequenceProducer )
    {
        this.sequenceProducer = sequenceProducer;
        this.bufferSize = sequenceProducer.getBufferSize();
        this.indexMask = (long) (this.bufferSize - 1);
        this.entries = new Object[sequenceProducer.getBufferSize() + 2 * BUFFER_PAD];
        this.fill( eventFactory );
    }

    private void fill( Supplier<E> eventFactory )
    {
        for ( int i = 0; i < this.bufferSize; ++i )
        {
            this.entries[BUFFER_PAD + i] = eventFactory.get();
        }
    }

    final E elementAt( long sequence )
    {
        return UNSAFE.getObject( this.entries, REF_ARRAY_BASE + ((sequence & this.indexMask) << REF_ELEMENT_SHIFT) );
    }
}
