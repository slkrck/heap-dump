package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

public class FixedRecvByteBufAllocator extends DefaultMaxMessagesRecvByteBufAllocator
{
    private final int bufferSize;

    public FixedRecvByteBufAllocator( int bufferSize )
    {
        ObjectUtil.checkPositive( bufferSize, "bufferSize" );
        this.bufferSize = bufferSize;
    }

    public RecvByteBufAllocator.Handle newHandle()
    {
        return new FixedRecvByteBufAllocator.HandleImpl( this.bufferSize );
    }

    public FixedRecvByteBufAllocator respectMaybeMoreData( boolean respectMaybeMoreData )
    {
        super.respectMaybeMoreData( respectMaybeMoreData );
        return this;
    }

    private final class HandleImpl extends DefaultMaxMessagesRecvByteBufAllocator.MaxMessageHandle
    {
        private final int bufferSize;

        HandleImpl( int bufferSize )
        {
            super();
            this.bufferSize = bufferSize;
        }

        public int guess()
        {
            return this.bufferSize;
        }
    }
}
