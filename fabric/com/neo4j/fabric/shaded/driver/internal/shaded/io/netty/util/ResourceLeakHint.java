package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

public interface ResourceLeakHint
{
    String toHintString();
}
