package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;

import java.util.Objects;

final class FluxError<T> extends Flux<T> implements Fuseable.ScalarCallable, SourceProducer<T>
{
    final Throwable error;

    FluxError( Throwable error )
    {
        this.error = (Throwable) Objects.requireNonNull( error );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Operators.error( actual, this.error );
    }

    public Object call() throws Exception
    {
        if ( this.error instanceof Exception )
        {
            throw (Exception) this.error;
        }
        else
        {
            throw Exceptions.propagate( this.error );
        }
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
