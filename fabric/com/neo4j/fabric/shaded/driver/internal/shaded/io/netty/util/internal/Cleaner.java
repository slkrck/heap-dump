package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

import java.nio.ByteBuffer;

interface Cleaner
{
    void freeDirectBuffer( ByteBuffer var1 );
}
