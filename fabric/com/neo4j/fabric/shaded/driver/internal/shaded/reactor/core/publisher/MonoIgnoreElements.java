package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.reactivestreams.Subscription;

final class MonoIgnoreElements<T> extends MonoFromFluxOperator<T,T>
{
    MonoIgnoreElements( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoIgnoreElements.IgnoreElementsSubscriber( actual );
    }

    static final class IgnoreElementsSubscriber<T> implements InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        Subscription s;

        IgnoreElementsSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.PARENT ? this.s : InnerOperator.super.scanUnsafe( key );
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            Operators.onDiscard( t, this.actual.currentContext() );
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            this.actual.onComplete();
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
