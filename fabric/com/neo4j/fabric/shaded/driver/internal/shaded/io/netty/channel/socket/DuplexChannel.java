package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;

public interface DuplexChannel extends Channel
{
    boolean isInputShutdown();

    ChannelFuture shutdownInput();

    ChannelFuture shutdownInput( ChannelPromise var1 );

    boolean isOutputShutdown();

    ChannelFuture shutdownOutput();

    ChannelFuture shutdownOutput( ChannelPromise var1 );

    boolean isShutdown();

    ChannelFuture shutdown();

    ChannelFuture shutdown( ChannelPromise var1 );
}
