package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.CharsetUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public final class NativeLibraryLoader
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( NativeLibraryLoader.class );
    private static final String NATIVE_RESOURCE_HOME = "META-INF/native/";
    private static final File WORKDIR;
    private static final boolean DELETE_NATIVE_LIB_AFTER_LOADING;
    private static final boolean TRY_TO_PATCH_SHADED_ID;
    private static final byte[] UNIQUE_ID_BYTES;

    static
    {
        UNIQUE_ID_BYTES = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".getBytes( CharsetUtil.US_ASCII );
        String workdir = SystemPropertyUtil.get( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.native.workdir" );
        if ( workdir != null )
        {
            File f = new File( workdir );
            f.mkdirs();

            try
            {
                f = f.getAbsoluteFile();
            }
            catch ( Exception var3 )
            {
            }

            WORKDIR = f;
            logger.debug( "-Dio.netty.native.workdir: " + WORKDIR );
        }
        else
        {
            WORKDIR = PlatformDependent.tmpdir();
            logger.debug( "-Dio.netty.native.workdir: " + WORKDIR + " (io.netty.tmpdir)" );
        }

        DELETE_NATIVE_LIB_AFTER_LOADING =
                SystemPropertyUtil.getBoolean( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.native.deleteLibAfterLoading", true );
        logger.debug( "-Dio.netty.native.deleteLibAfterLoading: {}", (Object) DELETE_NATIVE_LIB_AFTER_LOADING );
        TRY_TO_PATCH_SHADED_ID = SystemPropertyUtil.getBoolean( "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.native.tryPatchShadedId", true );
        logger.debug( "-Dio.netty.native.tryPatchShadedId: {}", (Object) TRY_TO_PATCH_SHADED_ID );
    }

    private NativeLibraryLoader()
    {
    }

    public static void loadFirstAvailable( ClassLoader loader, String... names )
    {
        List<Throwable> suppressed = new ArrayList();
        String[] var3 = names;
        int var4 = names.length;
        int var5 = 0;

        while ( var5 < var4 )
        {
            String name = var3[var5];

            try
            {
                load( name, loader );
                return;
            }
            catch ( Throwable var8 )
            {
                suppressed.add( var8 );
                logger.debug( "Unable to load the library '{}', trying next name...", name, var8 );
                ++var5;
            }
        }

        IllegalArgumentException iae = new IllegalArgumentException( "Failed to load any of the given libraries: " + Arrays.toString( names ) );
        ThrowableUtil.addSuppressedAndClear( iae, suppressed );
        throw iae;
    }

    private static String calculatePackagePrefix()
    {
        String maybeShaded = NativeLibraryLoader.class.getName();
        String expected = "io!netty!util!internal!NativeLibraryLoader".replace( '!', '.' );
        if ( !maybeShaded.endsWith( expected ) )
        {
            throw new UnsatisfiedLinkError(
                    String.format( "Could not find prefix added to %s to get %s. When shading, only adding a package prefix is supported", expected,
                            maybeShaded ) );
        }
        else
        {
            return maybeShaded.substring( 0, maybeShaded.length() - expected.length() );
        }
    }

    public static void load( String originalName, ClassLoader loader )
    {
        String packagePrefix = calculatePackagePrefix().replace( '.', '_' );
        String name = packagePrefix + originalName;
        ArrayList suppressed = new ArrayList();

        try
        {
            loadLibrary( loader, name, false );
        }
        catch ( Throwable var26 )
        {
            suppressed.add( var26 );
            logger.debug( "{} cannot be loaded from java.library.path, now trying export to -Dio.netty.native.workdir: {}", name, WORKDIR, var26 );
            String libname = System.mapLibraryName( name );
            String path = "META-INF/native/" + libname;
            InputStream in = null;
            OutputStream out = null;
            File tmpFile = null;
            URL url;
            if ( loader == null )
            {
                url = ClassLoader.getSystemResource( path );
            }
            else
            {
                url = loader.getResource( path );
            }

            try
            {
                if ( url == null )
                {
                    if ( !PlatformDependent.isOsx() )
                    {
                        FileNotFoundException fnf = new FileNotFoundException( path );
                        ThrowableUtil.addSuppressedAndClear( fnf, suppressed );
                        throw fnf;
                    }

                    String fileName = path.endsWith( ".jnilib" ) ? "META-INF/native/lib" + name + ".dynlib" : "META-INF/native/lib" + name + ".jnilib";
                    if ( loader == null )
                    {
                        url = ClassLoader.getSystemResource( fileName );
                    }
                    else
                    {
                        url = loader.getResource( fileName );
                    }

                    if ( url == null )
                    {
                        FileNotFoundException fnf = new FileNotFoundException( fileName );
                        ThrowableUtil.addSuppressedAndClear( fnf, suppressed );
                        throw fnf;
                    }
                }

                int index = libname.lastIndexOf( 46 );
                String prefix = libname.substring( 0, index );
                String suffix = libname.substring( index );
                tmpFile = File.createTempFile( prefix, suffix, WORKDIR );
                in = url.openStream();
                out = new FileOutputStream( tmpFile );
                if ( shouldShadedLibraryIdBePatched( packagePrefix ) )
                {
                    patchShadedLibraryId( in, out, originalName, name );
                }
                else
                {
                    byte[] buffer = new byte[8192];

                    int length;
                    while ( (length = in.read( buffer )) > 0 )
                    {
                        out.write( buffer, 0, length );
                    }
                }

                out.flush();
                closeQuietly( out );
                out = null;
                loadLibrary( loader, tmpFile.getPath(), true );
            }
            catch ( UnsatisfiedLinkError var23 )
            {
                try
                {
                    if ( tmpFile != null && tmpFile.isFile() && tmpFile.canRead() && !NativeLibraryLoader.NoexecVolumeDetector.canExecuteExecutable( tmpFile ) )
                    {
                        logger.info(
                                "{} exists but cannot be executed even when execute permissions set; check volume for \"noexec\" flag; use -D{}=[path] to set native working directory separately.",
                                tmpFile.getPath(), "com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.native.workdir" );
                    }
                }
                catch ( Throwable var22 )
                {
                    suppressed.add( var22 );
                    logger.debug( "Error checking if {} is on a file store mounted with noexec", tmpFile, var22 );
                }

                ThrowableUtil.addSuppressedAndClear( var23, suppressed );
                throw var23;
            }
            catch ( Exception var24 )
            {
                UnsatisfiedLinkError ule = new UnsatisfiedLinkError( "could not load a native library: " + name );
                ule.initCause( var24 );
                ThrowableUtil.addSuppressedAndClear( ule, suppressed );
                throw ule;
            }
            finally
            {
                closeQuietly( in );
                closeQuietly( out );
                if ( tmpFile != null && (!DELETE_NATIVE_LIB_AFTER_LOADING || !tmpFile.delete()) )
                {
                    tmpFile.deleteOnExit();
                }
            }
        }
    }

    static boolean patchShadedLibraryId( InputStream in, OutputStream out, String originalName, String name ) throws IOException
    {
        byte[] buffer = new byte[8192];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream( in.available() );

        int length;
        while ( (length = in.read( buffer )) > 0 )
        {
            byteArrayOutputStream.write( buffer, 0, length );
        }

        byteArrayOutputStream.flush();
        byte[] bytes = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        boolean patched;
        if ( !patchShadedLibraryId( bytes, originalName, name ) )
        {
            String os = PlatformDependent.normalizedOs();
            String arch = PlatformDependent.normalizedArch();
            String osArch = "_" + os + "_" + arch;
            if ( originalName.endsWith( osArch ) )
            {
                patched = patchShadedLibraryId( bytes, originalName.substring( 0, originalName.length() - osArch.length() ), name );
            }
            else
            {
                patched = false;
            }
        }
        else
        {
            patched = true;
        }

        out.write( bytes, 0, bytes.length );
        return patched;
    }

    private static boolean shouldShadedLibraryIdBePatched( String packagePrefix )
    {
        return TRY_TO_PATCH_SHADED_ID && PlatformDependent.isOsx() && !packagePrefix.isEmpty();
    }

    private static boolean patchShadedLibraryId( byte[] bytes, String originalName, String name )
    {
        byte[] nameBytes = originalName.getBytes( CharsetUtil.UTF_8 );
        int idIdx = -1;

        int i;
        label46:
        for ( i = 0; i < bytes.length && bytes.length - i >= nameBytes.length; ++i )
        {
            int idx = i;
            int j = 0;

            while ( j < nameBytes.length && bytes[idx++] == nameBytes[j++] )
            {
                if ( j == nameBytes.length )
                {
                    idIdx = i;
                    break label46;
                }
            }
        }

        if ( idIdx == -1 )
        {
            logger.debug( "Was not able to find the ID of the shaded native library {}, can't adjust it.", (Object) name );
            return false;
        }
        else
        {
            for ( i = 0; i < nameBytes.length; ++i )
            {
                bytes[idIdx + i] = UNIQUE_ID_BYTES[PlatformDependent.threadLocalRandom().nextInt( UNIQUE_ID_BYTES.length )];
            }

            if ( logger.isDebugEnabled() )
            {
                logger.debug( "Found the ID of the shaded native library {}. Replacing ID part {} with {}", name, originalName,
                        new String( bytes, idIdx, nameBytes.length, CharsetUtil.UTF_8 ) );
            }

            return true;
        }
    }

    private static void loadLibrary( ClassLoader loader, String name, boolean absolute )
    {
        Object suppressed = null;

        try
        {
            try
            {
                Class<?> newHelper = tryToLoadClass( loader, NativeLibraryUtil.class );
                loadLibraryByHelper( newHelper, name, absolute );
                logger.debug( "Successfully loaded the library {}", (Object) name );
                return;
            }
            catch ( UnsatisfiedLinkError var5 )
            {
                logger.debug( "Unable to load the library '{}', trying other loading mechanism.", name, var5 );
            }
            catch ( Exception var6 )
            {
                logger.debug( "Unable to load the library '{}', trying other loading mechanism.", name, var6 );
            }

            NativeLibraryUtil.loadLibrary( name, absolute );
            logger.debug( "Successfully loaded the library {}", (Object) name );
        }
        catch ( UnsatisfiedLinkError var7 )
        {
            if ( suppressed != null )
            {
                ThrowableUtil.addSuppressed( var7, (Throwable) suppressed );
            }

            throw var7;
        }
    }

    private static void loadLibraryByHelper( final Class<?> helper, final String name, final boolean absolute ) throws UnsatisfiedLinkError
    {
        Object ret = AccessController.doPrivileged( new PrivilegedAction<Object>()
        {
            public Object run()
            {
                try
                {
                    Method method = helper.getMethod( "loadLibrary", String.class, Boolean.TYPE );
                    method.setAccessible( true );
                    return method.invoke( (Object) null, name, absolute );
                }
                catch ( Exception var2 )
                {
                    return var2;
                }
            }
        } );
        if ( ret instanceof Throwable )
        {
            Throwable t = (Throwable) ret;

            assert !(t instanceof UnsatisfiedLinkError) : t + " should be a wrapper throwable";

            Throwable cause = t.getCause();
            if ( cause instanceof UnsatisfiedLinkError )
            {
                throw (UnsatisfiedLinkError) cause;
            }
            else
            {
                UnsatisfiedLinkError ule = new UnsatisfiedLinkError( t.getMessage() );
                ule.initCause( t );
                throw ule;
            }
        }
    }

    private static Class<?> tryToLoadClass( final ClassLoader loader, final Class<?> helper ) throws ClassNotFoundException
    {
        try
        {
            return Class.forName( helper.getName(), false, loader );
        }
        catch ( ClassNotFoundException var7 )
        {
            if ( loader == null )
            {
                throw var7;
            }
            else
            {
                try
                {
                    final byte[] classBinary = classToByteArray( helper );
                    return (Class) AccessController.doPrivileged( new PrivilegedAction<Class<?>>()
                    {
                        public Class<?> run()
                        {
                            try
                            {
                                Method defineClass =
                                        ClassLoader.class.getDeclaredMethod( "defineClass", String.class, byte[].class, Integer.TYPE, Integer.TYPE );
                                defineClass.setAccessible( true );
                                return (Class) defineClass.invoke( loader, helper.getName(), classBinary, 0, classBinary.length );
                            }
                            catch ( Exception var2 )
                            {
                                throw new IllegalStateException( "Define class failed!", var2 );
                            }
                        }
                    } );
                }
                catch ( ClassNotFoundException var4 )
                {
                    ThrowableUtil.addSuppressed( var4, (Throwable) var7 );
                    throw var4;
                }
                catch ( RuntimeException var5 )
                {
                    ThrowableUtil.addSuppressed( var5, (Throwable) var7 );
                    throw var5;
                }
                catch ( Error var6 )
                {
                    ThrowableUtil.addSuppressed( var6, (Throwable) var7 );
                    throw var6;
                }
            }
        }
    }

    private static byte[] classToByteArray( Class<?> clazz ) throws ClassNotFoundException
    {
        String fileName = clazz.getName();
        int lastDot = fileName.lastIndexOf( 46 );
        if ( lastDot > 0 )
        {
            fileName = fileName.substring( lastDot + 1 );
        }

        URL classUrl = clazz.getResource( fileName + ".class" );
        if ( classUrl == null )
        {
            throw new ClassNotFoundException( clazz.getName() );
        }
        else
        {
            byte[] buf = new byte[1024];
            ByteArrayOutputStream out = new ByteArrayOutputStream( 4096 );
            InputStream in = null;

            try
            {
                in = classUrl.openStream();

                int r;
                while ( (r = in.read( buf )) != -1 )
                {
                    out.write( buf, 0, r );
                }

                byte[] var13 = out.toByteArray();
                return var13;
            }
            catch ( IOException var11 )
            {
                throw new ClassNotFoundException( clazz.getName(), var11 );
            }
            finally
            {
                closeQuietly( in );
                closeQuietly( out );
            }
        }
    }

    private static void closeQuietly( Closeable c )
    {
        if ( c != null )
        {
            try
            {
                c.close();
            }
            catch ( IOException var2 )
            {
            }
        }
    }

    private static final class NoexecVolumeDetector
    {
        private static boolean canExecuteExecutable( File file ) throws IOException
        {
            if ( PlatformDependent.javaVersion() < 7 )
            {
                return true;
            }
            else if ( file.canExecute() )
            {
                return true;
            }
            else
            {
                Set<PosixFilePermission> existingFilePermissions = Files.getPosixFilePermissions( file.toPath() );
                Set<PosixFilePermission> executePermissions =
                        EnumSet.of( PosixFilePermission.OWNER_EXECUTE, PosixFilePermission.GROUP_EXECUTE, PosixFilePermission.OTHERS_EXECUTE );
                if ( existingFilePermissions.containsAll( executePermissions ) )
                {
                    return false;
                }
                else
                {
                    Set<PosixFilePermission> newPermissions = EnumSet.copyOf( existingFilePermissions );
                    newPermissions.addAll( executePermissions );
                    Files.setPosixFilePermissions( file.toPath(), newPermissions );
                    return file.canExecute();
                }
            }
        }
    }
}
