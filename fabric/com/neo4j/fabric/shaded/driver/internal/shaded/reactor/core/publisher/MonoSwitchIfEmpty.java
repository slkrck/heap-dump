package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

import java.util.Objects;

final class MonoSwitchIfEmpty<T> extends InternalMonoOperator<T,T>
{
    final Mono<? extends T> other;

    MonoSwitchIfEmpty( Mono<? extends T> source, Mono<? extends T> other )
    {
        super( source );
        this.other = (Mono) Objects.requireNonNull( other, "other" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxSwitchIfEmpty.SwitchIfEmptySubscriber<T> parent = new FluxSwitchIfEmpty.SwitchIfEmptySubscriber( actual, this.other );
        actual.onSubscribe( parent );
        return parent;
    }
}
