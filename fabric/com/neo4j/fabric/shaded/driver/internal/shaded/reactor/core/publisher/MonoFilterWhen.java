package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

class MonoFilterWhen<T> extends InternalMonoOperator<T,T>
{
    final Function<? super T,? extends Publisher<Boolean>> asyncPredicate;

    MonoFilterWhen( Mono<T> source, Function<? super T,? extends Publisher<Boolean>> asyncPredicate )
    {
        super( source );
        this.asyncPredicate = asyncPredicate;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new MonoFilterWhen.MonoFilterWhenMain( actual, this.asyncPredicate );
    }

    static final class FilterWhenInner implements InnerConsumer<Boolean>
    {
        static final AtomicReferenceFieldUpdater<MonoFilterWhen.FilterWhenInner,Subscription> SUB =
                AtomicReferenceFieldUpdater.newUpdater( MonoFilterWhen.FilterWhenInner.class, Subscription.class, "sub" );
        final MonoFilterWhen.MonoFilterWhenMain<?> main;
        final boolean cancelOnNext;
        boolean done;
        volatile Subscription sub;

        FilterWhenInner( MonoFilterWhen.MonoFilterWhenMain<?> main, boolean cancelOnNext )
        {
            this.main = main;
            this.cancelOnNext = cancelOnNext;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( SUB, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( Boolean t )
        {
            if ( !this.done )
            {
                if ( this.cancelOnNext )
                {
                    this.sub.cancel();
                }

                this.done = true;
                this.main.innerResult( t );
            }
        }

        public void onError( Throwable t )
        {
            if ( !this.done )
            {
                this.done = true;
                this.main.innerError( t );
            }
            else
            {
                Operators.onErrorDropped( t, this.main.currentContext() );
            }
        }

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.main.innerResult( (Boolean) null );
            }
        }

        void cancel()
        {
            Operators.terminate( SUB, this );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.sub;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.main;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.sub == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return Integer.MAX_VALUE;
            }
            else
            {
                return key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM ? this.done ? 0L : 1L : null;
            }
        }
    }

    static final class MonoFilterWhenMain<T> extends Operators.MonoSubscriber<T,T>
    {
        static final AtomicReferenceFieldUpdater<MonoFilterWhen.MonoFilterWhenMain,MonoFilterWhen.FilterWhenInner> ASYNC_FILTER =
                AtomicReferenceFieldUpdater.newUpdater( MonoFilterWhen.MonoFilterWhenMain.class, MonoFilterWhen.FilterWhenInner.class, "asyncFilter" );
        static final MonoFilterWhen.FilterWhenInner INNER_CANCELLED = new MonoFilterWhen.FilterWhenInner( (MonoFilterWhen.MonoFilterWhenMain) null, false );
        final Function<? super T,? extends Publisher<Boolean>> asyncPredicate;
        boolean sourceValued;
        Subscription upstream;
        volatile MonoFilterWhen.FilterWhenInner asyncFilter;

        MonoFilterWhenMain( CoreSubscriber<? super T> actual, Function<? super T,? extends Publisher<Boolean>> asyncPredicate )
        {
            super( actual );
            this.asyncPredicate = asyncPredicate;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.upstream, s ) )
            {
                this.upstream = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            this.sourceValued = true;
            this.setValue( t );

            Publisher p;
            try
            {
                p = (Publisher) Objects.requireNonNull( this.asyncPredicate.apply( t ), "The asyncPredicate returned a null value" );
            }
            catch ( Throwable var6 )
            {
                Exceptions.throwIfFatal( var6 );
                super.onError( var6 );
                Operators.onDiscard( t, this.actual.currentContext() );
                return;
            }

            if ( p instanceof Callable )
            {
                Boolean u;
                try
                {
                    u = (Boolean) ((Callable) p).call();
                }
                catch ( Throwable var5 )
                {
                    Exceptions.throwIfFatal( var5 );
                    super.onError( var5 );
                    Operators.onDiscard( t, this.actual.currentContext() );
                    return;
                }

                if ( u != null && u )
                {
                    this.complete( t );
                }
                else
                {
                    this.actual.onComplete();
                    Operators.onDiscard( t, this.actual.currentContext() );
                }
            }
            else
            {
                MonoFilterWhen.FilterWhenInner inner = new MonoFilterWhen.FilterWhenInner( this, !(p instanceof Mono) );
                if ( ASYNC_FILTER.compareAndSet( this, (Object) null, inner ) )
                {
                    p.subscribe( inner );
                }
            }
        }

        public void onComplete()
        {
            if ( !this.sourceValued )
            {
                super.onComplete();
            }
        }

        public void cancel()
        {
            if ( super.state != 4 )
            {
                super.cancel();
                this.upstream.cancel();
                this.cancelInner();
            }
        }

        void cancelInner()
        {
            MonoFilterWhen.FilterWhenInner a = this.asyncFilter;
            if ( a != INNER_CANCELLED )
            {
                a = (MonoFilterWhen.FilterWhenInner) ASYNC_FILTER.getAndSet( this, INNER_CANCELLED );
                if ( a != null && a != INNER_CANCELLED )
                {
                    a.cancel();
                }
            }
        }

        void innerResult( @Nullable Boolean item )
        {
            if ( item != null && item )
            {
                this.complete( this.value );
            }
            else
            {
                super.onComplete();
                Operators.onDiscard( this.value, this.actual.currentContext() );
            }
        }

        void innerError( Throwable ex )
        {
            super.onError( ex );
            Operators.onDiscard( this.value, this.actual.currentContext() );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.upstream;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.asyncFilter != null ? this.asyncFilter.scanUnsafe( Scannable.Attr.TERMINATED ) : super.scanUnsafe( Scannable.Attr.TERMINATED );
            }
            else
            {
                return super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            MonoFilterWhen.FilterWhenInner c = this.asyncFilter;
            return c == null ? Stream.empty() : Stream.of( c );
        }
    }
}
