package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Supplier;

final class UnsafeRingBuffer<E> extends RingBufferFields<E>
{
    protected long p1;
    protected long p2;
    protected long p3;
    protected long p4;
    protected long p5;
    protected long p6;
    protected long p7;

    UnsafeRingBuffer( Supplier<E> eventFactory, RingBufferProducer sequenceProducer )
    {
        super( eventFactory, sequenceProducer );
    }

    E get( long sequence )
    {
        return this.elementAt( sequence );
    }

    long next()
    {
        return this.sequenceProducer.next();
    }

    long next( int n )
    {
        return this.sequenceProducer.next( n );
    }

    void addGatingSequence( RingBuffer.Sequence gatingSequence )
    {
        this.sequenceProducer.addGatingSequence( gatingSequence );
    }

    long getMinimumGatingSequence()
    {
        return this.getMinimumGatingSequence( (RingBuffer.Sequence) null );
    }

    long getMinimumGatingSequence( @Nullable RingBuffer.Sequence sequence )
    {
        return this.sequenceProducer.getMinimumSequence( sequence );
    }

    boolean removeGatingSequence( RingBuffer.Sequence sequence )
    {
        return this.sequenceProducer.removeGatingSequence( sequence );
    }

    RingBuffer.Reader newReader()
    {
        return this.sequenceProducer.newBarrier();
    }

    long getCursor()
    {
        return this.sequenceProducer.getCursor();
    }

    int bufferSize()
    {
        return this.bufferSize;
    }

    void publish( long sequence )
    {
        this.sequenceProducer.publish( sequence );
    }

    int getPending()
    {
        return (int) this.sequenceProducer.getPending();
    }

    RingBufferProducer getSequencer()
    {
        return this.sequenceProducer;
    }
}
