package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class FluxTakeUntilOther<T, U> extends InternalFluxOperator<T,T>
{
    final Publisher<U> other;

    FluxTakeUntilOther( Flux<? extends T> source, Publisher<U> other )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        FluxTakeUntilOther.TakeUntilMainSubscriber<T> mainSubscriber = new FluxTakeUntilOther.TakeUntilMainSubscriber( actual );
        FluxTakeUntilOther.TakeUntilOtherSubscriber<U> otherSubscriber = new FluxTakeUntilOther.TakeUntilOtherSubscriber( mainSubscriber );
        this.other.subscribe( otherSubscriber );
        return mainSubscriber;
    }

    static final class TakeUntilMainSubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicReferenceFieldUpdater<FluxTakeUntilOther.TakeUntilMainSubscriber,Subscription> MAIN =
                AtomicReferenceFieldUpdater.newUpdater( FluxTakeUntilOther.TakeUntilMainSubscriber.class, Subscription.class, "main" );
        static final AtomicReferenceFieldUpdater<FluxTakeUntilOther.TakeUntilMainSubscriber,Subscription> OTHER =
                AtomicReferenceFieldUpdater.newUpdater( FluxTakeUntilOther.TakeUntilMainSubscriber.class, Subscription.class, "other" );
        final CoreSubscriber<? super T> actual;
        volatile Subscription main;
        volatile Subscription other;

        TakeUntilMainSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = Operators.serialize( actual );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.main;
            }
            else
            {
                return key == Scannable.Attr.CANCELLED ? this.main == Operators.cancelledSubscription() : InnerOperator.super.scanUnsafe( key );
            }
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( Scannable.from( this.other ) );
        }

        void setOther( Subscription s )
        {
            if ( !OTHER.compareAndSet( this, (Object) null, s ) )
            {
                s.cancel();
                if ( this.other != Operators.cancelledSubscription() )
                {
                    Operators.reportSubscriptionSet();
                }
            }
        }

        public void request( long n )
        {
            this.main.request( n );
        }

        void cancelMain()
        {
            Subscription s = this.main;
            if ( s != Operators.cancelledSubscription() )
            {
                s = (Subscription) MAIN.getAndSet( this, Operators.cancelledSubscription() );
                if ( s != null && s != Operators.cancelledSubscription() )
                {
                    s.cancel();
                }
            }
        }

        void cancelOther()
        {
            Subscription s = this.other;
            if ( s != Operators.cancelledSubscription() )
            {
                s = (Subscription) OTHER.getAndSet( this, Operators.cancelledSubscription() );
                if ( s != null && s != Operators.cancelledSubscription() )
                {
                    s.cancel();
                }
            }
        }

        public void cancel()
        {
            this.cancelMain();
            this.cancelOther();
        }

        public void onSubscribe( Subscription s )
        {
            if ( !MAIN.compareAndSet( this, (Object) null, s ) )
            {
                s.cancel();
                if ( this.main != Operators.cancelledSubscription() )
                {
                    Operators.reportSubscriptionSet();
                }
            }
            else
            {
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            if ( this.main == null && MAIN.compareAndSet( this, (Object) null, Operators.cancelledSubscription() ) )
            {
                Operators.error( this.actual, t );
            }
            else
            {
                this.cancel();
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( this.main == null && MAIN.compareAndSet( this, (Object) null, Operators.cancelledSubscription() ) )
            {
                this.cancelOther();
                Operators.complete( this.actual );
            }
            else
            {
                this.cancel();
                this.actual.onComplete();
            }
        }
    }

    static final class TakeUntilOtherSubscriber<U> implements InnerConsumer<U>
    {
        final FluxTakeUntilOther.TakeUntilMainSubscriber<?> main;
        boolean once;

        TakeUntilOtherSubscriber( FluxTakeUntilOther.TakeUntilMainSubscriber<?> main )
        {
            this.main = main;
        }

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.main.other == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.main.other;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL ? this.main : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            this.main.setOther( s );
            s.request( Long.MAX_VALUE );
        }

        public void onNext( U t )
        {
            this.onComplete();
        }

        public void onError( Throwable t )
        {
            if ( !this.once )
            {
                this.once = true;
                this.main.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.once )
            {
                this.once = true;
                this.main.onComplete();
            }
        }
    }
}
