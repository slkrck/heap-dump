package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.resolver;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;

import java.net.InetSocketAddress;

public final class DefaultAddressResolverGroup extends AddressResolverGroup<InetSocketAddress>
{
    public static final DefaultAddressResolverGroup INSTANCE = new DefaultAddressResolverGroup();

    private DefaultAddressResolverGroup()
    {
    }

    protected AddressResolver<InetSocketAddress> newResolver( EventExecutor executor ) throws Exception
    {
        return (new DefaultNameResolver( executor )).asAddressResolver();
    }
}
