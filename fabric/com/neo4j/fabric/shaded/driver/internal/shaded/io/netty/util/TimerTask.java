package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util;

public interface TimerTask
{
    void run( Timeout var1 ) throws Exception;
}
