package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;

final class MonoCancelOn<T> extends InternalMonoOperator<T,T>
{
    final Scheduler scheduler;

    MonoCancelOn( Mono<T> source, Scheduler scheduler )
    {
        super( source );
        this.scheduler = scheduler;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxCancelOn.CancelSubscriber( actual, this.scheduler );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }
}
