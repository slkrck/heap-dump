package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.Signal;

public class DecoderResult
{
    public static final DecoderResult UNFINISHED;
    public static final DecoderResult SUCCESS;
    protected static final Signal SIGNAL_UNFINISHED = Signal.valueOf( DecoderResult.class, "UNFINISHED" );
    protected static final Signal SIGNAL_SUCCESS = Signal.valueOf( DecoderResult.class, "SUCCESS" );

    static
    {
        UNFINISHED = new DecoderResult( SIGNAL_UNFINISHED );
        SUCCESS = new DecoderResult( SIGNAL_SUCCESS );
    }

    private final Throwable cause;

    protected DecoderResult( Throwable cause )
    {
        if ( cause == null )
        {
            throw new NullPointerException( "cause" );
        }
        else
        {
            this.cause = cause;
        }
    }

    public static DecoderResult failure( Throwable cause )
    {
        if ( cause == null )
        {
            throw new NullPointerException( "cause" );
        }
        else
        {
            return new DecoderResult( cause );
        }
    }

    public boolean isFinished()
    {
        return this.cause != SIGNAL_UNFINISHED;
    }

    public boolean isSuccess()
    {
        return this.cause == SIGNAL_SUCCESS;
    }

    public boolean isFailure()
    {
        return this.cause != SIGNAL_SUCCESS && this.cause != SIGNAL_UNFINISHED;
    }

    public Throwable cause()
    {
        return this.isFailure() ? this.cause : null;
    }

    public String toString()
    {
        if ( this.isFinished() )
        {
            if ( this.isSuccess() )
            {
                return "success";
            }
            else
            {
                String cause = this.cause().toString();
                return (new StringBuilder( cause.length() + 17 )).append( "failure(" ).append( cause ).append( ')' ).toString();
            }
        }
        else
        {
            return "unfinished";
        }
    }
}
