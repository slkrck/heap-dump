package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.Function;

final class MonoSubscriberContext<T> extends InternalMonoOperator<T,T> implements Fuseable
{
    final Function<Context,Context> doOnContext;

    MonoSubscriberContext( Mono<? extends T> source, Function<Context,Context> doOnContext )
    {
        super( source );
        this.doOnContext = (Function) Objects.requireNonNull( doOnContext, "doOnContext" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        Context c;
        try
        {
            c = (Context) this.doOnContext.apply( actual.currentContext() );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        return new FluxContextStart.ContextStartSubscriber( actual, c );
    }
}
