package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.BiFunction;

import org.reactivestreams.Subscription;

final class ParallelMergeReduce<T> extends Mono<T> implements Scannable, Fuseable
{
    final ParallelFlux<? extends T> source;
    final BiFunction<T,T,T> reducer;

    ParallelMergeReduce( ParallelFlux<? extends T> source, BiFunction<T,T,T> reducer )
    {
        this.source = source;
        this.reducer = reducer;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.PARENT ? this.source : null;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        ParallelMergeReduce.MergeReduceMain<T> parent = new ParallelMergeReduce.MergeReduceMain( actual, this.source.parallelism(), this.reducer );
        actual.onSubscribe( parent );
        this.source.subscribe( (CoreSubscriber[]) parent.subscribers );
    }

    static final class SlotPair<T>
    {
        static final AtomicIntegerFieldUpdater<ParallelMergeReduce.SlotPair> ACQ =
                AtomicIntegerFieldUpdater.newUpdater( ParallelMergeReduce.SlotPair.class, "acquireIndex" );
        static final AtomicIntegerFieldUpdater<ParallelMergeReduce.SlotPair> REL =
                AtomicIntegerFieldUpdater.newUpdater( ParallelMergeReduce.SlotPair.class, "releaseIndex" );
        T first;
        T second;
        volatile int acquireIndex;
        volatile int releaseIndex;

        int tryAcquireSlot()
        {
            int acquired;
            do
            {
                acquired = this.acquireIndex;
                if ( acquired >= 2 )
                {
                    return -1;
                }
            }
            while ( !ACQ.compareAndSet( this, acquired, acquired + 1 ) );

            return acquired;
        }

        boolean releaseSlot()
        {
            return REL.incrementAndGet( this ) == 2;
        }
    }

    static final class MergeReduceInner<T> implements InnerConsumer<T>
    {
        static final AtomicReferenceFieldUpdater<ParallelMergeReduce.MergeReduceInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( ParallelMergeReduce.MergeReduceInner.class, Subscription.class, "s" );
        final ParallelMergeReduce.MergeReduceMain<T> parent;
        final BiFunction<T,T,T> reducer;
        volatile Subscription s;
        T value;
        boolean done;

        MergeReduceInner( ParallelMergeReduce.MergeReduceMain<T> parent, BiFunction<T,T,T> reducer )
        {
            this.parent = parent;
            this.reducer = reducer;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.value != null ? 1 : 0;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? Integer.MAX_VALUE : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.currentContext() );
            }
            else
            {
                T v = this.value;
                if ( v == null )
                {
                    this.value = t;
                }
                else
                {
                    try
                    {
                        v = Objects.requireNonNull( this.reducer.apply( v, t ), "The reducer returned a null value" );
                    }
                    catch ( Throwable var4 )
                    {
                        this.onError( Operators.onOperatorError( this.s, var4, t, this.currentContext() ) );
                        return;
                    }

                    this.value = v;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.parent.currentContext() );
            }
            else
            {
                this.done = true;
                this.parent.innerError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.parent.innerComplete( this.value );
            }
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }
    }

    static final class MergeReduceMain<T> extends Operators.MonoSubscriber<T,T>
    {
        static final AtomicReferenceFieldUpdater<ParallelMergeReduce.MergeReduceMain,ParallelMergeReduce.SlotPair> CURRENT =
                AtomicReferenceFieldUpdater.newUpdater( ParallelMergeReduce.MergeReduceMain.class, ParallelMergeReduce.SlotPair.class, "current" );
        static final AtomicIntegerFieldUpdater<ParallelMergeReduce.MergeReduceMain> REMAINING =
                AtomicIntegerFieldUpdater.newUpdater( ParallelMergeReduce.MergeReduceMain.class, "remaining" );
        static final AtomicReferenceFieldUpdater<ParallelMergeReduce.MergeReduceMain,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( ParallelMergeReduce.MergeReduceMain.class, Throwable.class, "error" );
        final ParallelMergeReduce.MergeReduceInner<T>[] subscribers;
        final BiFunction<T,T,T> reducer;
        volatile ParallelMergeReduce.SlotPair<T> current;
        volatile int remaining;
        volatile Throwable error;

        MergeReduceMain( CoreSubscriber<? super T> subscriber, int n, BiFunction<T,T,T> reducer )
        {
            super( subscriber );
            ParallelMergeReduce.MergeReduceInner<T>[] a = new ParallelMergeReduce.MergeReduceInner[n];

            for ( int i = 0; i < n; ++i )
            {
                a[i] = new ParallelMergeReduce.MergeReduceInner( this, reducer );
            }

            this.subscribers = a;
            this.reducer = reducer;
            REMAINING.lazySet( this, n );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? REMAINING.get( this ) == 0 : super.scanUnsafe( key );
            }
        }

        @Nullable
        ParallelMergeReduce.SlotPair<T> addValue( T value )
        {
            while ( true )
            {
                ParallelMergeReduce.SlotPair<T> curr = this.current;
                if ( curr == null )
                {
                    curr = new ParallelMergeReduce.SlotPair();
                    if ( !CURRENT.compareAndSet( this, (Object) null, curr ) )
                    {
                        continue;
                    }
                }

                int c = curr.tryAcquireSlot();
                if ( c >= 0 )
                {
                    if ( c == 0 )
                    {
                        curr.first = value;
                    }
                    else
                    {
                        curr.second = value;
                    }

                    if ( curr.releaseSlot() )
                    {
                        CURRENT.compareAndSet( this, curr, (Object) null );
                        return curr;
                    }

                    return null;
                }

                CURRENT.compareAndSet( this, curr, (Object) null );
            }
        }

        public void cancel()
        {
            ParallelMergeReduce.MergeReduceInner[] var1 = this.subscribers;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                ParallelMergeReduce.MergeReduceInner<T> inner = var1[var3];
                inner.cancel();
            }

            super.cancel();
        }

        void innerError( Throwable ex )
        {
            if ( ERROR.compareAndSet( this, (Object) null, ex ) )
            {
                this.cancel();
                this.actual.onError( ex );
            }
            else if ( this.error != ex )
            {
                Operators.onErrorDropped( ex, this.actual.currentContext() );
            }
        }

        void innerComplete( @Nullable T value )
        {
            ParallelMergeReduce.SlotPair sp;
            if ( value != null )
            {
                while ( true )
                {
                    sp = this.addValue( value );
                    if ( sp == null )
                    {
                        break;
                    }

                    try
                    {
                        value = Objects.requireNonNull( this.reducer.apply( sp.first, sp.second ), "The reducer returned a null value" );
                    }
                    catch ( Throwable var4 )
                    {
                        this.innerError( Operators.onOperatorError( this, var4, this.actual.currentContext() ) );
                        return;
                    }
                }
            }

            if ( REMAINING.decrementAndGet( this ) == 0 )
            {
                sp = this.current;
                CURRENT.lazySet( this, (Object) null );
                if ( sp != null )
                {
                    this.complete( sp.first );
                }
                else
                {
                    this.actual.onComplete();
                }
            }
        }
    }
}
