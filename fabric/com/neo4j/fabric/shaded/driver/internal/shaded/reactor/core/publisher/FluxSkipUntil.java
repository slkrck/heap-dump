package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.Predicate;

import org.reactivestreams.Subscription;

final class FluxSkipUntil<T> extends InternalFluxOperator<T,T>
{
    final Predicate<? super T> predicate;

    FluxSkipUntil( Flux<? extends T> source, Predicate<? super T> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxSkipUntil.SkipUntilSubscriber( actual, this.predicate );
    }

    static final class SkipUntilSubscriber<T> implements Fuseable.ConditionalSubscriber<T>, InnerOperator<T,T>
    {
        final CoreSubscriber<? super T> actual;
        final Context ctx;
        final Predicate<? super T> predicate;
        Subscription s;
        boolean done;
        boolean doneSkipping;

        SkipUntilSubscriber( CoreSubscriber<? super T> actual, Predicate<? super T> predicate )
        {
            this.actual = actual;
            this.ctx = actual.currentContext();
            this.predicate = predicate;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
            }
            else if ( this.doneSkipping )
            {
                this.actual.onNext( t );
            }
            else
            {
                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var4 )
                {
                    this.onError( Operators.onOperatorError( this.s, var4, t, this.ctx ) );
                    return;
                }

                if ( b )
                {
                    this.doneSkipping = true;
                    this.actual.onNext( t );
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    this.s.request( 1L );
                }
            }
        }

        public boolean tryOnNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.ctx );
                return true;
            }
            else if ( this.doneSkipping )
            {
                this.actual.onNext( t );
                return true;
            }
            else
            {
                boolean b;
                try
                {
                    b = this.predicate.test( t );
                }
                catch ( Throwable var4 )
                {
                    this.onError( Operators.onOperatorError( this.s, var4, t, this.ctx ) );
                    return true;
                }

                if ( b )
                {
                    this.doneSkipping = true;
                    this.actual.onNext( t );
                    return false;
                }
                else
                {
                    Operators.onDiscard( t, this.ctx );
                    return true;
                }
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.ctx );
            }
            else
            {
                this.done = true;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.actual.onComplete();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : InnerOperator.super.scanUnsafe( key );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
        }
    }
}
