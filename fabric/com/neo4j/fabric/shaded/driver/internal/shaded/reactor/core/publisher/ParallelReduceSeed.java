package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class ParallelReduceSeed<T, R> extends ParallelFlux<R> implements Scannable, Fuseable
{
    final ParallelFlux<? extends T> source;
    final Supplier<R> initialSupplier;
    final BiFunction<R,? super T,R> reducer;

    ParallelReduceSeed( ParallelFlux<? extends T> source, Supplier<R> initialSupplier, BiFunction<R,? super T,R> reducer )
    {
        this.source = source;
        this.initialSupplier = initialSupplier;
        this.reducer = reducer;
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public void subscribe( CoreSubscriber<? super R>[] subscribers )
    {
        if ( this.validate( subscribers ) )
        {
            int n = subscribers.length;
            CoreSubscriber<T>[] parents = new CoreSubscriber[n];

            for ( int i = 0; i < n; ++i )
            {
                Object initialValue;
                try
                {
                    initialValue = Objects.requireNonNull( this.initialSupplier.get(), "The initialSupplier returned a null value" );
                }
                catch ( Throwable var7 )
                {
                    this.reportError( subscribers, Operators.onOperatorError( var7, subscribers[i].currentContext() ) );
                    return;
                }

                parents[i] = new ParallelReduceSeed.ParallelReduceSeedSubscriber( subscribers[i], initialValue, this.reducer );
            }

            this.source.subscribe( parents );
        }
    }

    void reportError( Subscriber<?>[] subscribers, Throwable ex )
    {
        Subscriber[] var3 = subscribers;
        int var4 = subscribers.length;

        for ( int var5 = 0; var5 < var4; ++var5 )
        {
            Subscriber<?> s = var3[var5];
            Operators.error( s, ex );
        }
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    static final class ParallelReduceSeedSubscriber<T, R> extends Operators.MonoSubscriber<T,R>
    {
        final BiFunction<R,? super T,R> reducer;
        R accumulator;
        Subscription s;
        boolean done;

        ParallelReduceSeedSubscriber( CoreSubscriber<? super R> subscriber, R initialValue, BiFunction<R,? super T,R> reducer )
        {
            super( subscriber );
            this.accumulator = initialValue;
            this.reducer = reducer;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            if ( this.done )
            {
                Operators.onNextDropped( t, this.actual.currentContext() );
            }
            else
            {
                Object v;
                try
                {
                    v = Objects.requireNonNull( this.reducer.apply( this.accumulator, t ), "The reducer returned a null value" );
                }
                catch ( Throwable var4 )
                {
                    this.onError( Operators.onOperatorError( this, var4, t, this.actual.currentContext() ) );
                    return;
                }

                this.accumulator = v;
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                this.accumulator = null;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                R a = this.accumulator;
                this.accumulator = null;
                this.complete( a );
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }
    }
}
