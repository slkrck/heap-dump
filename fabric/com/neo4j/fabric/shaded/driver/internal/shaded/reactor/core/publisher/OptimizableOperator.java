package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

interface OptimizableOperator<IN, OUT> extends CorePublisher<IN>
{
    @Nullable
    CoreSubscriber<? super OUT> subscribeOrReturn( CoreSubscriber<? super IN> var1 );

    CorePublisher<? extends OUT> source();

    @Nullable
    OptimizableOperator<?,? extends OUT> nextOptimizableSource();
}
