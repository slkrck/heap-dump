package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.GenericProgressiveFutureListener;

public interface ChannelProgressiveFutureListener extends GenericProgressiveFutureListener<ChannelProgressiveFuture>
{
}
