package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.function.Consumer;

final class MonoDoFinallyFuseable<T> extends InternalMonoOperator<T,T> implements Fuseable
{
    final Consumer<SignalType> onFinally;

    MonoDoFinallyFuseable( Mono<? extends T> source, Consumer<SignalType> onFinally )
    {
        super( source );
        this.onFinally = onFinally;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return FluxDoFinally.createSubscriber( actual, this.onFinally, true );
    }
}
