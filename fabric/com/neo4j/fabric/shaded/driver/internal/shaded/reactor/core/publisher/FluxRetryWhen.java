package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxRetryWhen<T> extends InternalFluxOperator<T,T>
{
    static final Duration MAX_BACKOFF = Duration.ofMillis( Long.MAX_VALUE );
    final Function<? super Flux<Throwable>,? extends Publisher<?>> whenSourceFactory;

    FluxRetryWhen( Flux<? extends T> source, Function<? super Flux<Throwable>,? extends Publisher<?>> whenSourceFactory )
    {
        super( source );
        this.whenSourceFactory = (Function) Objects.requireNonNull( whenSourceFactory, "whenSourceFactory" );
    }

    static <T> void subscribe( CoreSubscriber<? super T> s, Function<? super Flux<Throwable>,? extends Publisher<?>> whenSourceFactory,
            CorePublisher<? extends T> source )
    {
        FluxRetryWhen.RetryWhenOtherSubscriber other = new FluxRetryWhen.RetryWhenOtherSubscriber();
        Subscriber<Throwable> signaller = Operators.serialize( other.completionSignal );
        signaller.onSubscribe( Operators.emptySubscription() );
        CoreSubscriber<T> serial = Operators.serialize( s );
        FluxRetryWhen.RetryWhenMainSubscriber<T> main = new FluxRetryWhen.RetryWhenMainSubscriber( serial, signaller, source );
        other.main = main;
        serial.onSubscribe( main );

        Publisher p;
        try
        {
            p = (Publisher) Objects.requireNonNull( whenSourceFactory.apply( other ), "The whenSourceFactory returned a null Publisher" );
        }
        catch ( Throwable var9 )
        {
            s.onError( Operators.onOperatorError( var9, s.currentContext() ) );
            return;
        }

        p.subscribe( other );
        if ( !main.cancelled )
        {
            source.subscribe( main );
        }
    }

    static Function<Flux<Throwable>,Publisher<Long>> randomExponentialBackoffFunction( long numRetries, Duration firstBackoff, Duration maxBackoff,
            double jitterFactor, Scheduler backoffScheduler )
    {
        if ( jitterFactor >= 0.0D && jitterFactor <= 1.0D )
        {
            Objects.requireNonNull( firstBackoff, "firstBackoff is required" );
            Objects.requireNonNull( maxBackoff, "maxBackoff is required" );
            Objects.requireNonNull( backoffScheduler, "backoffScheduler is required" );
            return ( t ) -> {
                return t.index().flatMap( ( t2 ) -> {
                    long iteration = (Long) t2.getT1();
                    if ( iteration >= numRetries )
                    {
                        return Mono.error(
                                (Throwable) (new IllegalStateException( "Retries exhausted: " + iteration + "/" + numRetries, (Throwable) t2.getT2() )) );
                    }
                    else
                    {
                        Duration nextBackoff;
                        try
                        {
                            nextBackoff = firstBackoff.multipliedBy( (long) Math.pow( 2.0D, (double) iteration ) );
                            if ( nextBackoff.compareTo( maxBackoff ) > 0 )
                            {
                                nextBackoff = maxBackoff;
                            }
                        }
                        catch ( ArithmeticException var22 )
                        {
                            nextBackoff = maxBackoff;
                        }

                        if ( nextBackoff.isZero() )
                        {
                            return Mono.just( iteration );
                        }
                        else
                        {
                            ThreadLocalRandom random = ThreadLocalRandom.current();

                            long jitterOffset;
                            try
                            {
                                jitterOffset = nextBackoff.multipliedBy( (long) (100.0D * jitterFactor) ).dividedBy( 100L ).toMillis();
                            }
                            catch ( ArithmeticException var21 )
                            {
                                jitterOffset = Math.round( 9.223372036854776E18D * jitterFactor );
                            }

                            long lowBound = Math.max( firstBackoff.minus( nextBackoff ).toMillis(), -jitterOffset );
                            long highBound = Math.min( maxBackoff.minus( nextBackoff ).toMillis(), jitterOffset );
                            long jitter;
                            if ( highBound == lowBound )
                            {
                                if ( highBound == 0L )
                                {
                                    jitter = 0L;
                                }
                                else
                                {
                                    jitter = random.nextLong( highBound );
                                }
                            }
                            else
                            {
                                jitter = random.nextLong( lowBound, highBound );
                            }

                            Duration effectiveBackoff = nextBackoff.plusMillis( jitter );
                            return Mono.delay( effectiveBackoff, backoffScheduler );
                        }
                    }
                } );
            };
        }
        else
        {
            throw new IllegalArgumentException( "jitterFactor must be between 0 and 1 (default 0.5)" );
        }
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        subscribe( actual, this.whenSourceFactory, this.source );
        return null;
    }

    static final class RetryWhenOtherSubscriber extends Flux<Throwable> implements InnerConsumer<Object>, OptimizableOperator<Throwable,Throwable>
    {
        final DirectProcessor<Throwable> completionSignal = new DirectProcessor();
        FluxRetryWhen.RetryWhenMainSubscriber<?> main;

        public Context currentContext()
        {
            return this.main.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.main.otherArbiter;
            }
            else
            {
                return key == Scannable.Attr.ACTUAL ? this.main : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            this.main.setWhen( s );
        }

        public void onNext( Object t )
        {
            this.main.resubscribe( t );
        }

        public void onError( Throwable t )
        {
            this.main.whenError( t );
        }

        public void onComplete()
        {
            this.main.whenComplete();
        }

        public void subscribe( CoreSubscriber<? super Throwable> actual )
        {
            this.completionSignal.subscribe( actual );
        }

        public CoreSubscriber<? super Throwable> subscribeOrReturn( CoreSubscriber<? super Throwable> actual )
        {
            return actual;
        }

        public DirectProcessor<Throwable> source()
        {
            return this.completionSignal;
        }

        public OptimizableOperator<?,? extends Throwable> nextOptimizableSource()
        {
            return null;
        }
    }

    static final class RetryWhenMainSubscriber<T> extends Operators.MultiSubscriptionSubscriber<T,T>
    {
        static final AtomicIntegerFieldUpdater<FluxRetryWhen.RetryWhenMainSubscriber> WIP =
                AtomicIntegerFieldUpdater.newUpdater( FluxRetryWhen.RetryWhenMainSubscriber.class, "wip" );
        final Operators.DeferredSubscription otherArbiter;
        final Subscriber<Throwable> signaller;
        final CorePublisher<? extends T> source;
        Context context;
        volatile int wip;
        long produced;

        RetryWhenMainSubscriber( CoreSubscriber<? super T> actual, Subscriber<Throwable> signaller, CorePublisher<? extends T> source )
        {
            super( actual );
            this.signaller = signaller;
            this.source = source;
            this.otherArbiter = new Operators.DeferredSubscription();
            this.context = actual.currentContext();
        }

        public Context currentContext()
        {
            return this.context;
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( Scannable.from( this.signaller ), this.otherArbiter );
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.otherArbiter.cancel();
                super.cancel();
            }
        }

        public void setWhen( Subscription w )
        {
            this.otherArbiter.set( w );
        }

        public void onNext( T t )
        {
            this.actual.onNext( t );
            ++this.produced;
        }

        public void onError( Throwable t )
        {
            long p = this.produced;
            if ( p != 0L )
            {
                this.produced = 0L;
                this.produced( p );
            }

            this.otherArbiter.request( 1L );
            this.signaller.onNext( t );
        }

        public void onComplete()
        {
            this.otherArbiter.cancel();
            this.actual.onComplete();
        }

        void resubscribe( Object trigger )
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                do
                {
                    if ( this.cancelled )
                    {
                        return;
                    }

                    if ( trigger instanceof Context )
                    {
                        this.context = (Context) trigger;
                    }

                    this.source.subscribe( this );
                }
                while ( WIP.decrementAndGet( this ) != 0 );
            }
        }

        void whenError( Throwable e )
        {
            super.cancel();
            this.actual.onError( e );
        }

        void whenComplete()
        {
            super.cancel();
            this.actual.onComplete();
        }
    }
}
