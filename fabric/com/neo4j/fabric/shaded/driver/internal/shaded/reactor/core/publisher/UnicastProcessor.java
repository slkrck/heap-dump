package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Consumer;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class UnicastProcessor<T> extends FluxProcessor<T,T> implements Fuseable.QueueSubscription<T>, Fuseable, InnerOperator<T,T>
{
    static final AtomicReferenceFieldUpdater<UnicastProcessor,Disposable> ON_TERMINATE =
            AtomicReferenceFieldUpdater.newUpdater( UnicastProcessor.class, Disposable.class, "onTerminate" );
    static final AtomicIntegerFieldUpdater<UnicastProcessor> ONCE = AtomicIntegerFieldUpdater.newUpdater( UnicastProcessor.class, "once" );
    static final AtomicIntegerFieldUpdater<UnicastProcessor> WIP = AtomicIntegerFieldUpdater.newUpdater( UnicastProcessor.class, "wip" );
    static final AtomicLongFieldUpdater<UnicastProcessor> REQUESTED = AtomicLongFieldUpdater.newUpdater( UnicastProcessor.class, "requested" );
    final Queue<T> queue;
    final Consumer<? super T> onOverflow;
    volatile Disposable onTerminate;
    volatile boolean done;
    Throwable error;
    volatile CoreSubscriber<? super T> actual;
    volatile boolean cancelled;
    volatile int once;
    volatile int wip;
    volatile long requested;
    boolean outputFused;

    public UnicastProcessor( Queue<T> queue )
    {
        this.queue = (Queue) Objects.requireNonNull( queue, "queue" );
        this.onTerminate = null;
        this.onOverflow = null;
    }

    public UnicastProcessor( Queue<T> queue, Disposable onTerminate )
    {
        this.queue = (Queue) Objects.requireNonNull( queue, "queue" );
        this.onTerminate = (Disposable) Objects.requireNonNull( onTerminate, "onTerminate" );
        this.onOverflow = null;
    }

    public UnicastProcessor( Queue<T> queue, Consumer<? super T> onOverflow, Disposable onTerminate )
    {
        this.queue = (Queue) Objects.requireNonNull( queue, "queue" );
        this.onOverflow = (Consumer) Objects.requireNonNull( onOverflow, "onOverflow" );
        this.onTerminate = (Disposable) Objects.requireNonNull( onTerminate, "onTerminate" );
    }

    public static <E> UnicastProcessor<E> create()
    {
        return new UnicastProcessor( (Queue) Queues.unbounded().get() );
    }

    public static <E> UnicastProcessor<E> create( Queue<E> queue )
    {
        return new UnicastProcessor( queue );
    }

    public static <E> UnicastProcessor<E> create( Queue<E> queue, Disposable endcallback )
    {
        return new UnicastProcessor( queue, endcallback );
    }

    public static <E> UnicastProcessor<E> create( Queue<E> queue, Consumer<? super E> onOverflow, Disposable endcallback )
    {
        return new UnicastProcessor( queue, onOverflow, endcallback );
    }

    public int getBufferSize()
    {
        return Queues.capacity( this.queue );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return Scannable.Attr.BUFFERED == key ? this.queue.size() : super.scanUnsafe( key );
    }

    void doTerminate()
    {
        Disposable r = this.onTerminate;
        if ( r != null && ON_TERMINATE.compareAndSet( this, r, (Object) null ) )
        {
            r.dispose();
        }
    }

    void drainRegular( Subscriber<? super T> a )
    {
        int missed = 1;
        Queue q = this.queue;

        do
        {
            long r = this.requested;

            long e;
            for ( e = 0L; r != e; ++e )
            {
                boolean d = this.done;
                T t = q.poll();
                boolean empty = t == null;
                if ( this.checkTerminated( d, empty, a, q ) )
                {
                    return;
                }

                if ( empty )
                {
                    break;
                }

                a.onNext( t );
            }

            if ( r == e && this.checkTerminated( this.done, q.isEmpty(), a, q ) )
            {
                return;
            }

            if ( e != 0L && r != Long.MAX_VALUE )
            {
                REQUESTED.addAndGet( this, -e );
            }

            missed = WIP.addAndGet( this, -missed );
        }
        while ( missed != 0 );
    }

    void drainFused( Subscriber<? super T> a )
    {
        int missed = 1;
        Queue q = this.queue;

        while ( !this.cancelled )
        {
            boolean d = this.done;
            a.onNext( (Object) null );
            if ( d )
            {
                this.actual = null;
                Throwable ex = this.error;
                if ( ex != null )
                {
                    a.onError( ex );
                }
                else
                {
                    a.onComplete();
                }

                return;
            }

            missed = WIP.addAndGet( this, -missed );
            if ( missed == 0 )
            {
                return;
            }
        }

        q.clear();
        this.actual = null;
    }

    void drain()
    {
        if ( WIP.getAndIncrement( this ) == 0 )
        {
            int missed = 1;

            do
            {
                Subscriber<? super T> a = this.actual;
                if ( a != null )
                {
                    if ( this.outputFused )
                    {
                        this.drainFused( a );
                    }
                    else
                    {
                        this.drainRegular( a );
                    }

                    return;
                }

                missed = WIP.addAndGet( this, -missed );
            }
            while ( missed != 0 );
        }
    }

    boolean checkTerminated( boolean d, boolean empty, Subscriber<? super T> a, Queue<T> q )
    {
        if ( this.cancelled )
        {
            q.clear();
            this.actual = null;
            return true;
        }
        else if ( d && empty )
        {
            Throwable e = this.error;
            this.actual = null;
            if ( e != null )
            {
                a.onError( e );
            }
            else
            {
                a.onComplete();
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public void onSubscribe( Subscription s )
    {
        if ( !this.done && !this.cancelled )
        {
            s.request( Long.MAX_VALUE );
        }
        else
        {
            s.cancel();
        }
    }

    public int getPrefetch()
    {
        return Integer.MAX_VALUE;
    }

    public Context currentContext()
    {
        CoreSubscriber<? super T> actual = this.actual;
        return actual != null ? actual.currentContext() : Context.empty();
    }

    public void onNext( T t )
    {
        if ( !this.done && !this.cancelled )
        {
            if ( !this.queue.offer( t ) )
            {
                Throwable ex = Operators.onOperatorError( (Subscription) null, Exceptions.failWithOverflow(), t, this.currentContext() );
                if ( this.onOverflow != null )
                {
                    try
                    {
                        this.onOverflow.accept( t );
                    }
                    catch ( Throwable var4 )
                    {
                        Exceptions.throwIfFatal( var4 );
                        ex.initCause( var4 );
                    }
                }

                this.onError( Operators.onOperatorError( (Subscription) null, ex, t, this.currentContext() ) );
            }
            else
            {
                this.drain();
            }
        }
        else
        {
            Operators.onNextDropped( t, this.currentContext() );
        }
    }

    public void onError( Throwable t )
    {
        if ( !this.done && !this.cancelled )
        {
            this.error = t;
            this.done = true;
            this.doTerminate();
            this.drain();
        }
        else
        {
            Operators.onErrorDropped( t, this.currentContext() );
        }
    }

    public void onComplete()
    {
        if ( !this.done && !this.cancelled )
        {
            this.done = true;
            this.doTerminate();
            this.drain();
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Objects.requireNonNull( actual, "subscribe" );
        if ( this.once == 0 && ONCE.compareAndSet( this, 0, 1 ) )
        {
            actual.onSubscribe( this );
            this.actual = actual;
            if ( this.cancelled )
            {
                this.actual = null;
            }
            else
            {
                this.drain();
            }
        }
        else
        {
            Operators.error( actual, new IllegalStateException( "UnicastProcessor allows only a single Subscriber" ) );
        }
    }

    public void request( long n )
    {
        if ( Operators.validate( n ) )
        {
            Operators.addCap( REQUESTED, this, n );
            this.drain();
        }
    }

    public void cancel()
    {
        if ( !this.cancelled )
        {
            this.cancelled = true;
            this.doTerminate();
            if ( !this.outputFused && WIP.getAndIncrement( this ) == 0 )
            {
                this.queue.clear();
                this.actual = null;
            }
        }
    }

    @Nullable
    public T poll()
    {
        return this.queue.poll();
    }

    public int size()
    {
        return this.queue.size();
    }

    public boolean isEmpty()
    {
        return this.queue.isEmpty();
    }

    public void clear()
    {
        this.queue.clear();
    }

    public int requestFusion( int requestedMode )
    {
        if ( (requestedMode & 2) != 0 )
        {
            this.outputFused = true;
            return 2;
        }
        else
        {
            return 0;
        }
    }

    public boolean isDisposed()
    {
        return this.cancelled || this.done;
    }

    public boolean isTerminated()
    {
        return this.done;
    }

    @Nullable
    public Throwable getError()
    {
        return this.error;
    }

    public CoreSubscriber<? super T> actual()
    {
        return this.actual;
    }

    public long downstreamCount()
    {
        return this.hasDownstreams() ? 1L : 0L;
    }

    public boolean hasDownstreams()
    {
        return this.actual != null;
    }
}
