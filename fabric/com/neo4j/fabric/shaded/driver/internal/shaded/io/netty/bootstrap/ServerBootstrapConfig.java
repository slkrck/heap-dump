package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOption;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ServerChannel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AttributeKey;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.StringUtil;

import java.util.Map;

public final class ServerBootstrapConfig extends AbstractBootstrapConfig<ServerBootstrap,ServerChannel>
{
    ServerBootstrapConfig( ServerBootstrap bootstrap )
    {
        super( bootstrap );
    }

    public EventLoopGroup childGroup()
    {
        return ((ServerBootstrap) this.bootstrap).childGroup();
    }

    public ChannelHandler childHandler()
    {
        return ((ServerBootstrap) this.bootstrap).childHandler();
    }

    public Map<ChannelOption<?>,Object> childOptions()
    {
        return ((ServerBootstrap) this.bootstrap).childOptions();
    }

    public Map<AttributeKey<?>,Object> childAttrs()
    {
        return ((ServerBootstrap) this.bootstrap).childAttrs();
    }

    public String toString()
    {
        StringBuilder buf = new StringBuilder( super.toString() );
        buf.setLength( buf.length() - 1 );
        buf.append( ", " );
        EventLoopGroup childGroup = this.childGroup();
        if ( childGroup != null )
        {
            buf.append( "childGroup: " );
            buf.append( StringUtil.simpleClassName( (Object) childGroup ) );
            buf.append( ", " );
        }

        Map<ChannelOption<?>,Object> childOptions = this.childOptions();
        if ( !childOptions.isEmpty() )
        {
            buf.append( "childOptions: " );
            buf.append( childOptions );
            buf.append( ", " );
        }

        Map<AttributeKey<?>,Object> childAttrs = this.childAttrs();
        if ( !childAttrs.isEmpty() )
        {
            buf.append( "childAttrs: " );
            buf.append( childAttrs );
            buf.append( ", " );
        }

        ChannelHandler childHandler = this.childHandler();
        if ( childHandler != null )
        {
            buf.append( "childHandler: " );
            buf.append( childHandler );
            buf.append( ", " );
        }

        if ( buf.charAt( buf.length() - 1 ) == '(' )
        {
            buf.append( ')' );
        }
        else
        {
            buf.setCharAt( buf.length() - 2, ')' );
            buf.setLength( buf.length() - 1 );
        }

        return buf.toString();
    }
}
