package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.function.Tuple2;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

import org.reactivestreams.Subscriber;

final class FluxIterable<T> extends Flux<T> implements Fuseable, SourceProducer<T>
{
    final Iterable<? extends T> iterable;
    private final Runnable onClose;

    FluxIterable( Iterable<? extends T> iterable, Runnable onClose )
    {
        this.iterable = (Iterable) Objects.requireNonNull( iterable, "iterable" );
        this.onClose = onClose;
    }

    FluxIterable( Iterable<? extends T> iterable )
    {
        this( iterable, (Runnable) null );
    }

    static <T> void subscribe( CoreSubscriber<? super T> s, Iterator<? extends T> it )
    {
        subscribe( s, it, (Runnable) null );
    }

    static <T> void subscribe( CoreSubscriber<? super T> s, Iterator<? extends T> it, @Nullable Runnable onClose )
    {
        if ( it == null )
        {
            Operators.error( s, new NullPointerException( "The iterator is null" ) );
        }
        else
        {
            boolean b;
            try
            {
                b = it.hasNext();
            }
            catch ( Throwable var8 )
            {
                Operators.error( s, Operators.onOperatorError( var8, s.currentContext() ) );
                if ( onClose != null )
                {
                    try
                    {
                        onClose.run();
                    }
                    catch ( Throwable var6 )
                    {
                        Operators.onErrorDropped( var6, s.currentContext() );
                    }
                }

                return;
            }

            if ( !b )
            {
                Operators.complete( s );
                if ( onClose != null )
                {
                    try
                    {
                        onClose.run();
                    }
                    catch ( Throwable var7 )
                    {
                        Operators.onErrorDropped( var7, s.currentContext() );
                    }
                }
            }
            else
            {
                if ( s instanceof Fuseable.ConditionalSubscriber )
                {
                    s.onSubscribe( new FluxIterable.IterableSubscriptionConditional( (Fuseable.ConditionalSubscriber) s, it, onClose ) );
                }
                else
                {
                    s.onSubscribe( new FluxIterable.IterableSubscription( s, it, onClose ) );
                }
            }
        }
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Iterator it;
        try
        {
            it = this.iterable.iterator();
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return;
        }

        subscribe( actual, it, this.onClose );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.BUFFERED )
        {
            if ( this.iterable instanceof Collection )
            {
                return ((Collection) this.iterable).size();
            }

            if ( this.iterable instanceof Tuple2 )
            {
                return ((Tuple2) this.iterable).size();
            }
        }

        return null;
    }

    static final class IterableSubscriptionConditional<T> implements InnerProducer<T>, Fuseable.SynchronousSubscription<T>
    {
        static final AtomicLongFieldUpdater<FluxIterable.IterableSubscriptionConditional> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxIterable.IterableSubscriptionConditional.class, "requested" );
        static final int STATE_HAS_NEXT_NO_VALUE = 0;
        static final int STATE_HAS_NEXT_HAS_VALUE = 1;
        static final int STATE_NO_NEXT = 2;
        static final int STATE_CALL_HAS_NEXT = 3;
        final Fuseable.ConditionalSubscriber<? super T> actual;
        final Iterator<? extends T> iterator;
        final Runnable onClose;
        volatile boolean cancelled;
        volatile long requested;
        int state;
        T current;

        IterableSubscriptionConditional( Fuseable.ConditionalSubscriber<? super T> actual, Iterator<? extends T> iterator, @Nullable Runnable onClose )
        {
            this.actual = actual;
            this.iterator = iterator;
            this.onClose = onClose;
        }

        IterableSubscriptionConditional( Fuseable.ConditionalSubscriber<? super T> actual, Iterator<? extends T> iterator )
        {
            this( actual, iterator, (Runnable) null );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && Operators.addCap( REQUESTED, this, n ) == 0L )
            {
                if ( n == Long.MAX_VALUE )
                {
                    this.fastPath();
                }
                else
                {
                    this.slowPath( n );
                }
            }
        }

        private void onCloseWithDropError()
        {
            if ( this.onClose != null )
            {
                try
                {
                    this.onClose.run();
                }
                catch ( Throwable var2 )
                {
                    Operators.onErrorDropped( var2, this.actual.currentContext() );
                }
            }
        }

        void slowPath( long n )
        {
            Iterator<? extends T> a = this.iterator;
            Fuseable.ConditionalSubscriber<? super T> s = this.actual;
            long e = 0L;

            while ( true )
            {
                while ( e == n )
                {
                    n = this.requested;
                    if ( n == e )
                    {
                        n = REQUESTED.addAndGet( this, -e );
                        if ( n == 0L )
                        {
                            return;
                        }

                        e = 0L;
                    }
                }

                Object t;
                try
                {
                    t = Objects.requireNonNull( a.next(), "The iterator returned a null value" );
                }
                catch ( Throwable var12 )
                {
                    s.onError( var12 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                boolean consumed = s.tryOnNext( t );
                if ( this.cancelled )
                {
                    return;
                }

                boolean b;
                try
                {
                    b = a.hasNext();
                }
                catch ( Throwable var11 )
                {
                    s.onError( var11 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                if ( !b )
                {
                    s.onComplete();
                    this.onCloseWithDropError();
                    return;
                }

                if ( consumed )
                {
                    ++e;
                }
            }
        }

        void fastPath()
        {
            Iterator<? extends T> a = this.iterator;
            Fuseable.ConditionalSubscriber s = this.actual;

            while ( !this.cancelled )
            {
                Object t;
                try
                {
                    t = Objects.requireNonNull( a.next(), "The iterator returned a null value" );
                }
                catch ( Exception var6 )
                {
                    s.onError( var6 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                s.tryOnNext( t );
                if ( this.cancelled )
                {
                    return;
                }

                boolean b;
                try
                {
                    b = a.hasNext();
                }
                catch ( Exception var7 )
                {
                    s.onError( var7 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                if ( !b )
                {
                    s.onComplete();
                    this.onCloseWithDropError();
                    return;
                }
            }
        }

        public void cancel()
        {
            this.onCloseWithDropError();
            this.cancelled = true;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.state == 2 : InnerProducer.super.scanUnsafe( key );
            }
        }

        public void clear()
        {
            this.state = 2;
        }

        public boolean isEmpty()
        {
            int s = this.state;
            if ( s == 2 )
            {
                return true;
            }
            else if ( s != 1 && s != 0 )
            {
                if ( this.iterator.hasNext() )
                {
                    this.state = 0;
                    return false;
                }
                else
                {
                    this.state = 2;
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        @Nullable
        public T poll()
        {
            if ( !this.isEmpty() )
            {
                Object c;
                if ( this.state == 0 )
                {
                    c = this.iterator.next();
                }
                else
                {
                    c = this.current;
                    this.current = null;
                }

                this.state = 3;
                return c;
            }
            else
            {
                this.onCloseWithDropError();
                return null;
            }
        }

        public int size()
        {
            return this.state == 2 ? 0 : 1;
        }
    }

    static final class IterableSubscription<T> implements InnerProducer<T>, Fuseable.SynchronousSubscription<T>
    {
        static final AtomicLongFieldUpdater<FluxIterable.IterableSubscription> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxIterable.IterableSubscription.class, "requested" );
        static final int STATE_HAS_NEXT_NO_VALUE = 0;
        static final int STATE_HAS_NEXT_HAS_VALUE = 1;
        static final int STATE_NO_NEXT = 2;
        static final int STATE_CALL_HAS_NEXT = 3;
        final CoreSubscriber<? super T> actual;
        final Iterator<? extends T> iterator;
        final Runnable onClose;
        volatile boolean cancelled;
        volatile long requested;
        int state;
        T current;

        IterableSubscription( CoreSubscriber<? super T> actual, Iterator<? extends T> iterator, @Nullable Runnable onClose )
        {
            this.actual = actual;
            this.iterator = iterator;
            this.onClose = onClose;
        }

        IterableSubscription( CoreSubscriber<? super T> actual, Iterator<? extends T> iterator )
        {
            this( actual, iterator, (Runnable) null );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) && Operators.addCap( REQUESTED, this, n ) == 0L )
            {
                if ( n == Long.MAX_VALUE )
                {
                    this.fastPath();
                }
                else
                {
                    this.slowPath( n );
                }
            }
        }

        private void onCloseWithDropError()
        {
            if ( this.onClose != null )
            {
                try
                {
                    this.onClose.run();
                }
                catch ( Throwable var2 )
                {
                    Operators.onErrorDropped( var2, this.actual.currentContext() );
                }
            }
        }

        void slowPath( long n )
        {
            Iterator<? extends T> a = this.iterator;
            Subscriber<? super T> s = this.actual;
            long e = 0L;

            while ( true )
            {
                while ( e == n )
                {
                    n = this.requested;
                    if ( n == e )
                    {
                        n = REQUESTED.addAndGet( this, -e );
                        if ( n == 0L )
                        {
                            return;
                        }

                        e = 0L;
                    }
                }

                Object t;
                try
                {
                    t = Objects.requireNonNull( a.next(), "The iterator returned a null value" );
                }
                catch ( Throwable var11 )
                {
                    s.onError( var11 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                s.onNext( t );
                if ( this.cancelled )
                {
                    return;
                }

                boolean b;
                try
                {
                    b = a.hasNext();
                }
                catch ( Throwable var10 )
                {
                    s.onError( var10 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                if ( !b )
                {
                    s.onComplete();
                    this.onCloseWithDropError();
                    return;
                }

                ++e;
            }
        }

        void fastPath()
        {
            Iterator<? extends T> a = this.iterator;
            CoreSubscriber s = this.actual;

            while ( !this.cancelled )
            {
                Object t;
                try
                {
                    t = Objects.requireNonNull( a.next(), "The iterator returned a null value" );
                }
                catch ( Exception var6 )
                {
                    s.onError( var6 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                s.onNext( t );
                if ( this.cancelled )
                {
                    return;
                }

                boolean b;
                try
                {
                    b = a.hasNext();
                }
                catch ( Exception var7 )
                {
                    s.onError( var7 );
                    this.onCloseWithDropError();
                    return;
                }

                if ( this.cancelled )
                {
                    return;
                }

                if ( !b )
                {
                    s.onComplete();
                    this.onCloseWithDropError();
                    return;
                }
            }
        }

        public void cancel()
        {
            this.onCloseWithDropError();
            this.cancelled = true;
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.state == 2 : InnerProducer.super.scanUnsafe( key );
            }
        }

        public void clear()
        {
            this.state = 2;
        }

        public boolean isEmpty()
        {
            int s = this.state;
            if ( s == 2 )
            {
                return true;
            }
            else if ( s != 1 && s != 0 )
            {
                if ( this.iterator.hasNext() )
                {
                    this.state = 0;
                    return false;
                }
                else
                {
                    this.state = 2;
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        @Nullable
        public T poll()
        {
            if ( !this.isEmpty() )
            {
                Object c;
                if ( this.state == 0 )
                {
                    c = this.iterator.next();
                }
                else
                {
                    c = this.current;
                    this.current = null;
                }

                this.state = 3;
                if ( c == null )
                {
                    this.onCloseWithDropError();
                    throw new NullPointerException( "iterator returned a null value" );
                }
                else
                {
                    return c;
                }
            }
            else
            {
                this.onCloseWithDropError();
                return null;
            }
        }

        public int size()
        {
            return this.state == 2 ? 0 : 1;
        }
    }
}
