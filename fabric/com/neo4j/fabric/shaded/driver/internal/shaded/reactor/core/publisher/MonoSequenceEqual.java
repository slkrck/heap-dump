package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class MonoSequenceEqual<T> extends Mono<Boolean> implements SourceProducer<Boolean>
{
    final Publisher<? extends T> first;
    final Publisher<? extends T> second;
    final BiPredicate<? super T,? super T> comparer;
    final int prefetch;

    MonoSequenceEqual( Publisher<? extends T> first, Publisher<? extends T> second, BiPredicate<? super T,? super T> comparer, int prefetch )
    {
        this.first = (Publisher) Objects.requireNonNull( first, "first" );
        this.second = (Publisher) Objects.requireNonNull( second, "second" );
        this.comparer = (BiPredicate) Objects.requireNonNull( comparer, "comparer" );
        if ( prefetch < 1 )
        {
            throw new IllegalArgumentException( "Buffer size must be strictly positive: " + prefetch );
        }
        else
        {
            this.prefetch = prefetch;
        }
    }

    public void subscribe( CoreSubscriber<? super Boolean> actual )
    {
        MonoSequenceEqual.EqualCoordinator<T> ec = new MonoSequenceEqual.EqualCoordinator( actual, this.prefetch, this.first, this.second, this.comparer );
        actual.onSubscribe( ec );
        ec.subscribe();
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.PREFETCH ? this.prefetch : null;
    }

    static final class EqualSubscriber<T> implements InnerConsumer<T>
    {
        static final AtomicReferenceFieldUpdater<MonoSequenceEqual.EqualSubscriber,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoSequenceEqual.EqualSubscriber.class, Subscription.class, "subscription" );
        final MonoSequenceEqual.EqualCoordinator<T> parent;
        final Queue<T> queue;
        final int prefetch;
        volatile boolean done;
        Throwable error;
        Subscription cachedSubscription;
        volatile Subscription subscription;

        EqualSubscriber( MonoSequenceEqual.EqualCoordinator<T> parent, int prefetch )
        {
            this.parent = parent;
            this.prefetch = prefetch;
            this.queue = (Queue) Queues.get( prefetch ).get();
        }

        public Context currentContext()
        {
            return this.parent.actual.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.subscription == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.subscription;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else
            {
                return key == Scannable.Attr.BUFFERED ? this.queue.size() : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                this.cachedSubscription = s;
                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        public void onNext( T t )
        {
            if ( !this.queue.offer( t ) )
            {
                this.onError( Operators.onOperatorError( this.cachedSubscription,
                        Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ), t, this.currentContext() ) );
            }
            else
            {
                this.parent.drain();
            }
        }

        public void onError( Throwable t )
        {
            this.error = t;
            this.done = true;
            this.parent.drain();
        }

        public void onComplete()
        {
            this.done = true;
            this.parent.drain();
        }
    }

    static final class EqualCoordinator<T> implements InnerProducer<Boolean>
    {
        static final AtomicIntegerFieldUpdater<MonoSequenceEqual.EqualCoordinator> ONCE =
                AtomicIntegerFieldUpdater.newUpdater( MonoSequenceEqual.EqualCoordinator.class, "once" );
        static final AtomicIntegerFieldUpdater<MonoSequenceEqual.EqualCoordinator> WIP =
                AtomicIntegerFieldUpdater.newUpdater( MonoSequenceEqual.EqualCoordinator.class, "wip" );
        final CoreSubscriber<? super Boolean> actual;
        final BiPredicate<? super T,? super T> comparer;
        final Publisher<? extends T> first;
        final Publisher<? extends T> second;
        final MonoSequenceEqual.EqualSubscriber<T> firstSubscriber;
        final MonoSequenceEqual.EqualSubscriber<T> secondSubscriber;
        volatile boolean cancelled;
        volatile int once;
        T v1;
        T v2;
        volatile int wip;

        EqualCoordinator( CoreSubscriber<? super Boolean> actual, int prefetch, Publisher<? extends T> first, Publisher<? extends T> second,
                BiPredicate<? super T,? super T> comparer )
        {
            this.actual = actual;
            this.first = first;
            this.second = second;
            this.comparer = comparer;
            this.firstSubscriber = new MonoSequenceEqual.EqualSubscriber( this, prefetch );
            this.secondSubscriber = new MonoSequenceEqual.EqualSubscriber( this, prefetch );
        }

        public CoreSubscriber<? super Boolean> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.CANCELLED ? this.cancelled : InnerProducer.super.scanUnsafe( key );
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.firstSubscriber, this.secondSubscriber );
        }

        void subscribe()
        {
            if ( ONCE.compareAndSet( this, 0, 1 ) )
            {
                this.first.subscribe( this.firstSubscriber );
                this.second.subscribe( this.secondSubscriber );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                if ( ONCE.compareAndSet( this, 0, 1 ) )
                {
                    this.first.subscribe( this.firstSubscriber );
                    this.second.subscribe( this.secondSubscriber );
                }
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.cancelInner( this.firstSubscriber );
                this.cancelInner( this.secondSubscriber );
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    this.firstSubscriber.queue.clear();
                    this.secondSubscriber.queue.clear();
                }
            }
        }

        void cancel( MonoSequenceEqual.EqualSubscriber<T> s1, Queue<T> q1, MonoSequenceEqual.EqualSubscriber<T> s2, Queue<T> q2 )
        {
            this.cancelled = true;
            this.cancelInner( s1 );
            q1.clear();
            this.cancelInner( s2 );
            q2.clear();
        }

        void cancelInner( MonoSequenceEqual.EqualSubscriber<T> innerSubscriber )
        {
            Subscription s = innerSubscriber.subscription;
            if ( s != Operators.cancelledSubscription() )
            {
                s = (Subscription) MonoSequenceEqual.EqualSubscriber.S.getAndSet( innerSubscriber, Operators.cancelledSubscription() );
                if ( s != null && s != Operators.cancelledSubscription() )
                {
                    s.cancel();
                }
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                int missed = 1;
                MonoSequenceEqual.EqualSubscriber<T> s1 = this.firstSubscriber;
                Queue<T> q1 = s1.queue;
                MonoSequenceEqual.EqualSubscriber<T> s2 = this.secondSubscriber;
                Queue q2 = s2.queue;

                do
                {
                    long r = 0L;

                    boolean e2;
                    boolean e1;
                    do
                    {
                        if ( this.cancelled )
                        {
                            q1.clear();
                            q2.clear();
                            return;
                        }

                        boolean d1 = s1.done;
                        if ( d1 )
                        {
                            Throwable e = s1.error;
                            if ( e != null )
                            {
                                this.cancel( s1, q1, s2, q2 );
                                this.actual.onError( e );
                                return;
                            }
                        }

                        boolean d2 = s2.done;
                        if ( d2 )
                        {
                            Throwable e = s2.error;
                            if ( e != null )
                            {
                                this.cancel( s1, q1, s2, q2 );
                                this.actual.onError( e );
                                return;
                            }
                        }

                        if ( this.v1 == null )
                        {
                            this.v1 = q1.poll();
                        }

                        e1 = this.v1 == null;
                        if ( this.v2 == null )
                        {
                            this.v2 = q2.poll();
                        }

                        e2 = this.v2 == null;
                        if ( d1 && d2 && e1 && e2 )
                        {
                            this.actual.onNext( true );
                            this.actual.onComplete();
                            return;
                        }

                        if ( d1 && d2 && e1 != e2 )
                        {
                            this.cancel( s1, q1, s2, q2 );
                            this.actual.onNext( false );
                            this.actual.onComplete();
                            return;
                        }

                        if ( !e1 && !e2 )
                        {
                            boolean c;
                            try
                            {
                                c = this.comparer.test( this.v1, this.v2 );
                            }
                            catch ( Throwable var14 )
                            {
                                Exceptions.throwIfFatal( var14 );
                                this.cancel( s1, q1, s2, q2 );
                                this.actual.onError( Operators.onOperatorError( var14, this.actual.currentContext() ) );
                                return;
                            }

                            if ( !c )
                            {
                                this.cancel( s1, q1, s2, q2 );
                                this.actual.onNext( false );
                                this.actual.onComplete();
                                return;
                            }

                            ++r;
                            this.v1 = null;
                            this.v2 = null;
                        }
                    }
                    while ( !e1 && !e2 );

                    if ( r != 0L )
                    {
                        s1.cachedSubscription.request( r );
                        s2.cachedSubscription.request( r );
                    }

                    missed = WIP.addAndGet( this, -missed );
                }
                while ( missed != 0 );
            }
        }
    }
}
