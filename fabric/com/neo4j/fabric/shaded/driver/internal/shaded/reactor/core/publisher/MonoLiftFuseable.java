package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Objects;
import java.util.function.BiFunction;

import org.reactivestreams.Publisher;

final class MonoLiftFuseable<I, O> extends InternalMonoOperator<I,O> implements Fuseable
{
    final BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter;

    MonoLiftFuseable( Publisher<I> p, BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter )
    {
        super( Mono.from( p ) );
        this.lifter = lifter;
    }

    public CoreSubscriber<? super I> subscribeOrReturn( CoreSubscriber<? super O> actual )
    {
        CoreSubscriber<? super I> input = (CoreSubscriber) this.lifter.apply( this.source, actual );
        Objects.requireNonNull( input, "Lifted subscriber MUST NOT be null" );
        if ( actual instanceof Fuseable.QueueSubscription && !(input instanceof Fuseable.QueueSubscription) )
        {
            input = new FluxHide.SuppressFuseableSubscriber( (CoreSubscriber) input );
        }

        return (CoreSubscriber) input;
    }
}
