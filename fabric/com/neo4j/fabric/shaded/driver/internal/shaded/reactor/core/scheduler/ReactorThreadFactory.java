package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.jetbrains.annotations.NotNull;

class ReactorThreadFactory implements ThreadFactory, Supplier<String>, UncaughtExceptionHandler
{
    private final String name;
    private final AtomicLong counterReference;
    private final boolean daemon;
    private final boolean rejectBlocking;
    @Nullable
    private final BiConsumer<Thread,Throwable> uncaughtExceptionHandler;

    ReactorThreadFactory( String name, AtomicLong counterReference, boolean daemon, boolean rejectBlocking,
            @Nullable BiConsumer<Thread,Throwable> uncaughtExceptionHandler )
    {
        this.name = name;
        this.counterReference = counterReference;
        this.daemon = daemon;
        this.rejectBlocking = rejectBlocking;
        this.uncaughtExceptionHandler = uncaughtExceptionHandler;
    }

    public final Thread newThread( @NotNull Runnable runnable )
    {
        String newThreadName = this.name + "-" + this.counterReference.incrementAndGet();
        Thread t = this.rejectBlocking ? new ReactorThreadFactory.NonBlockingThread( runnable, newThreadName ) : new Thread( runnable, newThreadName );
        if ( this.daemon )
        {
            ((Thread) t).setDaemon( true );
        }

        if ( this.uncaughtExceptionHandler != null )
        {
            ((Thread) t).setUncaughtExceptionHandler( this );
        }

        return (Thread) t;
    }

    public void uncaughtException( Thread t, Throwable e )
    {
        if ( this.uncaughtExceptionHandler != null )
        {
            this.uncaughtExceptionHandler.accept( t, e );
        }
    }

    public final String get()
    {
        return this.name;
    }

    static final class NonBlockingThread extends Thread implements NonBlocking
    {
        public NonBlockingThread( Runnable target, String name )
        {
            super( target, name );
        }
    }
}
