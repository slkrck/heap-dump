package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.timeout;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelException;

public class TimeoutException extends ChannelException
{
    private static final long serialVersionUID = 4673641882869672533L;

    TimeoutException()
    {
    }

    TimeoutException( boolean shared )
    {
        super( (String) null, (Throwable) null, shared );
    }

    public Throwable fillInStackTrace()
    {
        return this;
    }
}
