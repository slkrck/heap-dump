package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.timeout;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

public final class ReadTimeoutException extends TimeoutException
{
    public static final ReadTimeoutException INSTANCE = PlatformDependent.javaVersion() >= 7 ? new ReadTimeoutException( true ) : new ReadTimeoutException();
    private static final long serialVersionUID = 169287984113283421L;

    ReadTimeoutException()
    {
    }

    private ReadTimeoutException( boolean shared )
    {
        super( shared );
    }
}
