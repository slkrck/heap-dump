package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;

import java.nio.ByteBuffer;

class UnpooledUnsafeNoCleanerDirectByteBuf extends UnpooledUnsafeDirectByteBuf
{
    UnpooledUnsafeNoCleanerDirectByteBuf( ByteBufAllocator alloc, int initialCapacity, int maxCapacity )
    {
        super( alloc, initialCapacity, maxCapacity );
    }

    protected ByteBuffer allocateDirect( int initialCapacity )
    {
        return PlatformDependent.allocateDirectNoCleaner( initialCapacity );
    }

    ByteBuffer reallocateDirect( ByteBuffer oldBuffer, int initialCapacity )
    {
        return PlatformDependent.reallocateDirectNoCleaner( oldBuffer, initialCapacity );
    }

    protected void freeDirect( ByteBuffer buffer )
    {
        PlatformDependent.freeDirectNoCleaner( buffer );
    }

    public ByteBuf capacity( int newCapacity )
    {
        this.checkNewCapacity( newCapacity );
        int oldCapacity = this.capacity();
        if ( newCapacity == oldCapacity )
        {
            return this;
        }
        else
        {
            this.trimIndicesToCapacity( newCapacity );
            this.setByteBuffer( this.reallocateDirect( this.buffer, newCapacity ), false );
            return this;
        }
    }
}
