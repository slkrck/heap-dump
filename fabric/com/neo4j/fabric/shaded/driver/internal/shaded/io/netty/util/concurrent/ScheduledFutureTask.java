package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.DefaultPriorityQueue;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PriorityQueueNode;

import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

final class ScheduledFutureTask<V> extends PromiseTask<V> implements ScheduledFuture<V>, PriorityQueueNode
{
    private static final AtomicLong nextTaskId = new AtomicLong();
    private static final long START_TIME = System.nanoTime();
    private final long id;
    private final long periodNanos;
    private long deadlineNanos;
    private int queueIndex;

    ScheduledFutureTask( AbstractScheduledEventExecutor executor, Runnable runnable, V result, long nanoTime )
    {
        this( executor, toCallable( runnable, result ), nanoTime );
    }

    ScheduledFutureTask( AbstractScheduledEventExecutor executor, Callable<V> callable, long nanoTime, long period )
    {
        super( executor, callable );
        this.id = nextTaskId.getAndIncrement();
        this.queueIndex = -1;
        if ( period == 0L )
        {
            throw new IllegalArgumentException( "period: 0 (expected: != 0)" );
        }
        else
        {
            this.deadlineNanos = nanoTime;
            this.periodNanos = period;
        }
    }

    ScheduledFutureTask( AbstractScheduledEventExecutor executor, Callable<V> callable, long nanoTime )
    {
        super( executor, callable );
        this.id = nextTaskId.getAndIncrement();
        this.queueIndex = -1;
        this.deadlineNanos = nanoTime;
        this.periodNanos = 0L;
    }

    static long nanoTime()
    {
        return System.nanoTime() - START_TIME;
    }

    static long deadlineNanos( long delay )
    {
        long deadlineNanos = nanoTime() + delay;
        return deadlineNanos < 0L ? Long.MAX_VALUE : deadlineNanos;
    }

    static long initialNanoTime()
    {
        return START_TIME;
    }

    static long deadlineToDelayNanos( long deadlineNanos )
    {
        return Math.max( 0L, deadlineNanos - nanoTime() );
    }

    protected EventExecutor executor()
    {
        return super.executor();
    }

    public long deadlineNanos()
    {
        return this.deadlineNanos;
    }

    public long delayNanos()
    {
        return deadlineToDelayNanos( this.deadlineNanos() );
    }

    public long delayNanos( long currentTimeNanos )
    {
        return Math.max( 0L, this.deadlineNanos() - (currentTimeNanos - START_TIME) );
    }

    public long getDelay( TimeUnit unit )
    {
        return unit.convert( this.delayNanos(), TimeUnit.NANOSECONDS );
    }

    public int compareTo( Delayed o )
    {
        if ( this == o )
        {
            return 0;
        }
        else
        {
            ScheduledFutureTask<?> that = (ScheduledFutureTask) o;
            long d = this.deadlineNanos() - that.deadlineNanos();
            if ( d < 0L )
            {
                return -1;
            }
            else if ( d > 0L )
            {
                return 1;
            }
            else if ( this.id < that.id )
            {
                return -1;
            }
            else
            {
                assert this.id != that.id;

                return 1;
            }
        }
    }

    public void run()
    {
        assert this.executor().inEventLoop();

        try
        {
            if ( this.periodNanos == 0L )
            {
                if ( this.setUncancellableInternal() )
                {
                    V result = this.task.call();
                    this.setSuccessInternal( result );
                }
            }
            else if ( !this.isCancelled() )
            {
                this.task.call();
                if ( !this.executor().isShutdown() )
                {
                    if ( this.periodNanos > 0L )
                    {
                        this.deadlineNanos += this.periodNanos;
                    }
                    else
                    {
                        this.deadlineNanos = nanoTime() - this.periodNanos;
                    }

                    if ( !this.isCancelled() )
                    {
                        Queue<ScheduledFutureTask<?>> scheduledTaskQueue = ((AbstractScheduledEventExecutor) this.executor()).scheduledTaskQueue;

                        assert scheduledTaskQueue != null;

                        scheduledTaskQueue.add( this );
                    }
                }
            }
        }
        catch ( Throwable var2 )
        {
            this.setFailureInternal( var2 );
        }
    }

    public boolean cancel( boolean mayInterruptIfRunning )
    {
        boolean canceled = super.cancel( mayInterruptIfRunning );
        if ( canceled )
        {
            ((AbstractScheduledEventExecutor) this.executor()).removeScheduled( this );
        }

        return canceled;
    }

    boolean cancelWithoutRemove( boolean mayInterruptIfRunning )
    {
        return super.cancel( mayInterruptIfRunning );
    }

    protected StringBuilder toStringBuilder()
    {
        StringBuilder buf = super.toStringBuilder();
        buf.setCharAt( buf.length() - 1, ',' );
        return buf.append( " id: " ).append( this.id ).append( ", deadline: " ).append( this.deadlineNanos ).append( ", period: " ).append(
                this.periodNanos ).append( ')' );
    }

    public int priorityQueueIndex( DefaultPriorityQueue<?> queue )
    {
        return this.queueIndex;
    }

    public void priorityQueueIndex( DefaultPriorityQueue<?> queue, int i )
    {
        this.queueIndex = i;
    }
}
