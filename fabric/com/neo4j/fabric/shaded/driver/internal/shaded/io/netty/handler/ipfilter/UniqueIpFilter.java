package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ipfilter;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ConcurrentSet;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Set;

@ChannelHandler.Sharable
public class UniqueIpFilter extends AbstractRemoteAddressFilter<InetSocketAddress>
{
    private final Set<InetAddress> connected = new ConcurrentSet();

    protected boolean accept( ChannelHandlerContext ctx, InetSocketAddress remoteAddress ) throws Exception
    {
        final InetAddress remoteIp = remoteAddress.getAddress();
        if ( !this.connected.add( remoteIp ) )
        {
            return false;
        }
        else
        {
            ctx.channel().closeFuture().addListener( new ChannelFutureListener()
            {
                public void operationComplete( ChannelFuture future ) throws Exception
                {
                    UniqueIpFilter.this.connected.remove( remoteIp );
                }
            } );
            return true;
        }
    }
}
