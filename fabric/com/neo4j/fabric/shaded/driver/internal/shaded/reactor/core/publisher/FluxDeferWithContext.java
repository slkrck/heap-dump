package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.function.Function;

import org.reactivestreams.Publisher;

final class FluxDeferWithContext<T> extends Flux<T> implements SourceProducer<T>
{
    final Function<Context,? extends Publisher<? extends T>> supplier;

    FluxDeferWithContext( Function<Context,? extends Publisher<? extends T>> supplier )
    {
        this.supplier = (Function) Objects.requireNonNull( supplier, "supplier" );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        Context ctx = actual.currentContext();

        Publisher p;
        try
        {
            p = (Publisher) Objects.requireNonNull( this.supplier.apply( ctx ), "The Publisher returned by the supplier is null" );
        }
        catch ( Throwable var5 )
        {
            Operators.error( actual, Operators.onOperatorError( var5, ctx ) );
            return;
        }

        from( p ).subscribe( actual );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }
}
