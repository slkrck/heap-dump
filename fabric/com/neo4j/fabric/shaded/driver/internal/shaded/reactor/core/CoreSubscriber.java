package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public interface CoreSubscriber<T> extends Subscriber<T>
{
    default Context currentContext()
    {
        return Context.empty();
    }

    void onSubscribe( Subscription var1 );
}
