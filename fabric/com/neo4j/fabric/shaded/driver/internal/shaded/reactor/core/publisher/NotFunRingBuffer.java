package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.function.Supplier;

final class NotFunRingBuffer<E> extends NotFunRingBufferFields<E>
{
    NotFunRingBuffer( Supplier<E> eventFactory, RingBufferProducer sequenceProducer )
    {
        super( eventFactory, sequenceProducer );
    }

    E get( long sequence )
    {
        return this.elementAt( sequence );
    }

    long next()
    {
        return this.next( 1 );
    }

    long next( int n )
    {
        return this.sequenceProducer.next( n );
    }

    void addGatingSequence( RingBuffer.Sequence gatingSequence )
    {
        this.sequenceProducer.addGatingSequence( gatingSequence );
    }

    long getMinimumGatingSequence()
    {
        return this.getMinimumGatingSequence( (RingBuffer.Sequence) null );
    }

    long getMinimumGatingSequence( @Nullable RingBuffer.Sequence sequence )
    {
        return this.sequenceProducer.getMinimumSequence( sequence );
    }

    boolean removeGatingSequence( RingBuffer.Sequence sequence )
    {
        return this.sequenceProducer.removeGatingSequence( sequence );
    }

    RingBuffer.Reader newReader()
    {
        return this.sequenceProducer.newBarrier();
    }

    long getCursor()
    {
        return this.sequenceProducer.getCursor();
    }

    int bufferSize()
    {
        return this.bufferSize;
    }

    void publish( long sequence )
    {
        this.sequenceProducer.publish( sequence );
    }

    int getPending()
    {
        return (int) this.sequenceProducer.getPending();
    }

    RingBufferProducer getSequencer()
    {
        return this.sequenceProducer;
    }
}
