package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import org.reactivestreams.Subscription;

final class MonoReduceSeed<T, R> extends MonoFromFluxOperator<T,R> implements Fuseable
{
    final Supplier<R> initialSupplier;
    final BiFunction<R,? super T,R> accumulator;

    MonoReduceSeed( Flux<? extends T> source, Supplier<R> initialSupplier, BiFunction<R,? super T,R> accumulator )
    {
        super( source );
        this.initialSupplier = (Supplier) Objects.requireNonNull( initialSupplier, "initialSupplier" );
        this.accumulator = (BiFunction) Objects.requireNonNull( accumulator, "accumulator" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        Object initialValue;
        try
        {
            initialValue = Objects.requireNonNull( this.initialSupplier.get(), "The initial value supplied is null" );
        }
        catch ( Throwable var4 )
        {
            Operators.error( actual, Operators.onOperatorError( var4, actual.currentContext() ) );
            return null;
        }

        return new MonoReduceSeed.ReduceSeedSubscriber( actual, this.accumulator, initialValue );
    }

    static final class ReduceSeedSubscriber<T, R> extends Operators.MonoSubscriber<T,R>
    {
        final BiFunction<R,? super T,R> accumulator;
        Subscription s;
        boolean done;

        ReduceSeedSubscriber( CoreSubscriber<? super R> actual, BiFunction<R,? super T,R> accumulator, R value )
        {
            super( actual );
            this.accumulator = accumulator;
            this.value = value;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key == Scannable.Attr.PARENT ? this.s : super.scanUnsafe( key );
            }
        }

        public void cancel()
        {
            super.cancel();
            this.s.cancel();
        }

        public void setValue( R value )
        {
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
                s.request( Long.MAX_VALUE );
            }
        }

        public void onNext( T t )
        {
            R v = this.value;
            if ( v != null )
            {
                Object accumulated;
                try
                {
                    accumulated = Objects.requireNonNull( this.accumulator.apply( v, t ), "The accumulator returned a null value" );
                }
                catch ( Throwable var5 )
                {
                    this.onError( Operators.onOperatorError( this, var5, t, this.actual.currentContext() ) );
                    return;
                }

                this.value = accumulated;
            }
            else
            {
                Operators.onDiscard( t, this.actual.currentContext() );
            }
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.actual.currentContext() );
            }
            else
            {
                this.done = true;
                Operators.onDiscard( this.value, this.actual.currentContext() );
                this.value = null;
                this.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.complete( this.value );
            }
        }
    }
}
