package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.function.BiFunction;

import org.reactivestreams.Publisher;

final class ParallelLift<I, O> extends ParallelFlux<O> implements Scannable
{
    final BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter;
    final ParallelFlux<I> source;

    ParallelLift( ParallelFlux<I> p, BiFunction<Publisher,? super CoreSubscriber<? super O>,? extends CoreSubscriber<? super I>> lifter )
    {
        this.source = (ParallelFlux) Objects.requireNonNull( p, "source" );
        this.lifter = lifter;
    }

    public int getPrefetch()
    {
        return this.source.getPrefetch();
    }

    public int parallelism()
    {
        return this.source.parallelism();
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    protected void subscribe( CoreSubscriber<? super O>[] s )
    {
        CoreSubscriber<? super I>[] subscribers = new CoreSubscriber[this.parallelism()];

        for ( int i = 0; i < subscribers.length; ++i )
        {
            subscribers[i] = (CoreSubscriber) Objects.requireNonNull( this.lifter.apply( this.source, s[i] ), "Lifted subscriber MUST NOT be null" );
        }

        this.source.subscribe( subscribers );
    }
}
