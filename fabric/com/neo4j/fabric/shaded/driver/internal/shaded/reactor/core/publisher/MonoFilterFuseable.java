package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Fuseable;

import java.util.Objects;
import java.util.function.Predicate;

final class MonoFilterFuseable<T> extends InternalMonoOperator<T,T> implements Fuseable
{
    final Predicate<? super T> predicate;

    MonoFilterFuseable( Mono<? extends T> source, Predicate<? super T> predicate )
    {
        super( source );
        this.predicate = (Predicate) Objects.requireNonNull( predicate, "predicate" );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return (CoreSubscriber) (actual instanceof Fuseable.ConditionalSubscriber ? new FluxFilterFuseable.FilterFuseableConditionalSubscriber(
                (Fuseable.ConditionalSubscriber) actual, this.predicate ) : new FluxFilterFuseable.FilterFuseableSubscriber( actual, this.predicate ));
    }
}
