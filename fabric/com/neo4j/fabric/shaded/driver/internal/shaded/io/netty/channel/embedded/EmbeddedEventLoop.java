package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.embedded;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.DefaultChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoop;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.AbstractScheduledEventExecutor;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.ObjectUtil;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

final class EmbeddedEventLoop extends AbstractScheduledEventExecutor implements EventLoop
{
    private final Queue<Runnable> tasks = new ArrayDeque( 2 );

    public EventLoopGroup parent()
    {
        return (EventLoopGroup) super.parent();
    }

    public EventLoop next()
    {
        return (EventLoop) super.next();
    }

    public void execute( Runnable command )
    {
        if ( command == null )
        {
            throw new NullPointerException( "command" );
        }
        else
        {
            this.tasks.add( command );
        }
    }

    void runTasks()
    {
        while ( true )
        {
            Runnable task = (Runnable) this.tasks.poll();
            if ( task == null )
            {
                return;
            }

            task.run();
        }
    }

    long runScheduledTasks()
    {
        long time = AbstractScheduledEventExecutor.nanoTime();

        while ( true )
        {
            Runnable task = this.pollScheduledTask( time );
            if ( task == null )
            {
                return this.nextScheduledTaskNano();
            }

            task.run();
        }
    }

    long nextScheduledTask()
    {
        return this.nextScheduledTaskNano();
    }

    protected void cancelScheduledTasks()
    {
        super.cancelScheduledTasks();
    }

    public Future<?> shutdownGracefully( long quietPeriod, long timeout, TimeUnit unit )
    {
        throw new UnsupportedOperationException();
    }

    public Future<?> terminationFuture()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void shutdown()
    {
        throw new UnsupportedOperationException();
    }

    public boolean isShuttingDown()
    {
        return false;
    }

    public boolean isShutdown()
    {
        return false;
    }

    public boolean isTerminated()
    {
        return false;
    }

    public boolean awaitTermination( long timeout, TimeUnit unit )
    {
        return false;
    }

    public ChannelFuture register( Channel channel )
    {
        return this.register( (ChannelPromise) (new DefaultChannelPromise( channel, this )) );
    }

    public ChannelFuture register( ChannelPromise promise )
    {
        ObjectUtil.checkNotNull( promise, "promise" );
        promise.channel().unsafe().register( this, promise );
        return promise;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public ChannelFuture register( Channel channel, ChannelPromise promise )
    {
        channel.unsafe().register( this, promise );
        return promise;
    }

    public boolean inEventLoop()
    {
        return true;
    }

    public boolean inEventLoop( Thread thread )
    {
        return true;
    }
}
