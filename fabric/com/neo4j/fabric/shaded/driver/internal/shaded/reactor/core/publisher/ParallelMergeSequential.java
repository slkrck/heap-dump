package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.reactivestreams.Subscription;

final class ParallelMergeSequential<T> extends Flux<T> implements Scannable
{
    final ParallelFlux<? extends T> source;
    final int prefetch;
    final Supplier<Queue<T>> queueSupplier;

    ParallelMergeSequential( ParallelFlux<? extends T> source, int prefetch, Supplier<Queue<T>> queueSupplier )
    {
        if ( prefetch <= 0 )
        {
            throw new IllegalArgumentException( "prefetch > 0 required but it was " + prefetch );
        }
        else
        {
            this.source = source;
            this.prefetch = prefetch;
            this.queueSupplier = queueSupplier;
        }
    }

    @Nullable
    public Object scanUnsafe( Scannable.Attr key )
    {
        if ( key == Scannable.Attr.PARENT )
        {
            return this.source;
        }
        else
        {
            return key == Scannable.Attr.PREFETCH ? this.getPrefetch() : null;
        }
    }

    public int getPrefetch()
    {
        return this.prefetch;
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        ParallelMergeSequential.MergeSequentialMain<T> parent =
                new ParallelMergeSequential.MergeSequentialMain( actual, this.source.parallelism(), this.prefetch, this.queueSupplier );
        actual.onSubscribe( parent );
        this.source.subscribe( (CoreSubscriber[]) parent.subscribers );
    }

    static final class MergeSequentialInner<T> implements InnerConsumer<T>
    {
        static final AtomicReferenceFieldUpdater<ParallelMergeSequential.MergeSequentialInner,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( ParallelMergeSequential.MergeSequentialInner.class, Subscription.class, "s" );
        final ParallelMergeSequential.MergeSequentialMain<T> parent;
        final int prefetch;
        final int limit;
        long produced;
        volatile Subscription s;
        volatile Queue<T> queue;
        volatile boolean done;

        MergeSequentialInner( ParallelMergeSequential.MergeSequentialMain<T> parent, int prefetch )
        {
            this.parent = parent;
            this.prefetch = prefetch;
            this.limit = Operators.unboundedOrLimit( prefetch );
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.PREFETCH )
            {
                return this.prefetch;
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.queue != null ? this.queue.size() : 0;
            }
            else
            {
                return key == Scannable.Attr.TERMINATED ? this.done : null;
            }
        }

        public Context currentContext()
        {
            return this.parent.actual.currentContext();
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Operators.unboundedOrPrefetch( this.prefetch ) );
            }
        }

        public void onNext( T t )
        {
            this.parent.onNext( this, t );
        }

        public void onError( Throwable t )
        {
            this.parent.onError( t );
        }

        public void onComplete()
        {
            this.parent.onComplete();
        }

        void requestOne()
        {
            long p = this.produced + 1L;
            if ( p == (long) this.limit )
            {
                this.produced = 0L;
                this.s.request( p );
            }
            else
            {
                this.produced = p;
            }
        }

        public void cancel()
        {
            Operators.terminate( S, this );
        }

        Queue<T> getQueue( Supplier<Queue<T>> queueSupplier )
        {
            Queue<T> q = this.queue;
            if ( q == null )
            {
                q = (Queue) queueSupplier.get();
                this.queue = q;
            }

            return q;
        }
    }

    static final class MergeSequentialMain<T> implements InnerProducer<T>
    {
        static final AtomicReferenceFieldUpdater<ParallelMergeSequential.MergeSequentialMain,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( ParallelMergeSequential.MergeSequentialMain.class, Throwable.class, "error" );
        static final AtomicIntegerFieldUpdater<ParallelMergeSequential.MergeSequentialMain> WIP =
                AtomicIntegerFieldUpdater.newUpdater( ParallelMergeSequential.MergeSequentialMain.class, "wip" );
        static final AtomicLongFieldUpdater<ParallelMergeSequential.MergeSequentialMain> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( ParallelMergeSequential.MergeSequentialMain.class, "requested" );
        static final AtomicIntegerFieldUpdater<ParallelMergeSequential.MergeSequentialMain> DONE =
                AtomicIntegerFieldUpdater.newUpdater( ParallelMergeSequential.MergeSequentialMain.class, "done" );
        final ParallelMergeSequential.MergeSequentialInner<T>[] subscribers;
        final Supplier<Queue<T>> queueSupplier;
        final CoreSubscriber<? super T> actual;
        volatile int wip;
        volatile long requested;
        volatile boolean cancelled;
        volatile int done;
        volatile Throwable error;

        MergeSequentialMain( CoreSubscriber<? super T> actual, int n, int prefetch, Supplier<Queue<T>> queueSupplier )
        {
            this.actual = actual;
            this.queueSupplier = queueSupplier;
            ParallelMergeSequential.MergeSequentialInner<T>[] a = new ParallelMergeSequential.MergeSequentialInner[n];

            for ( int i = 0; i < n; ++i )
            {
                a[i] = new ParallelMergeSequential.MergeSequentialInner( this, prefetch );
            }

            this.subscribers = a;
            DONE.lazySet( this, n );
        }

        public final CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancelled;
            }
            else if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done == 0;
            }
            else
            {
                return key == Scannable.Attr.ERROR ? this.error : InnerProducer.super.scanUnsafe( key );
            }
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.subscribers );
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
                this.drain();
            }
        }

        public void cancel()
        {
            if ( !this.cancelled )
            {
                this.cancelled = true;
                this.cancelAll();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    this.cleanup();
                }
            }
        }

        void cancelAll()
        {
            ParallelMergeSequential.MergeSequentialInner[] var1 = this.subscribers;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                ParallelMergeSequential.MergeSequentialInner<T> s = var1[var3];
                s.cancel();
            }
        }

        void cleanup()
        {
            ParallelMergeSequential.MergeSequentialInner[] var1 = this.subscribers;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                ParallelMergeSequential.MergeSequentialInner<T> s = var1[var3];
                s.queue = null;
            }
        }

        void onNext( ParallelMergeSequential.MergeSequentialInner<T> inner, T value )
        {
            Queue q;
            if ( this.wip == 0 && WIP.compareAndSet( this, 0, 1 ) )
            {
                if ( this.requested != 0L )
                {
                    this.actual.onNext( value );
                    if ( this.requested != Long.MAX_VALUE )
                    {
                        REQUESTED.decrementAndGet( this );
                    }

                    inner.requestOne();
                }
                else
                {
                    q = inner.getQueue( this.queueSupplier );
                    if ( !q.offer( value ) )
                    {
                        this.onError( Operators.onOperatorError( this,
                                Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ), value,
                                this.actual.currentContext() ) );
                        return;
                    }
                }

                if ( WIP.decrementAndGet( this ) == 0 )
                {
                    return;
                }
            }
            else
            {
                q = inner.getQueue( this.queueSupplier );
                if ( !q.offer( value ) )
                {
                    this.onError( Operators.onOperatorError( this,
                            Exceptions.failWithOverflow( "Queue is full: Reactive Streams source doesn't respect backpressure" ), value,
                            this.actual.currentContext() ) );
                    return;
                }

                if ( WIP.getAndIncrement( this ) != 0 )
                {
                    return;
                }
            }

            this.drainLoop();
        }

        void onError( Throwable ex )
        {
            if ( ERROR.compareAndSet( this, (Object) null, ex ) )
            {
                this.cancelAll();
                this.drain();
            }
            else if ( this.error != ex )
            {
                Operators.onErrorDropped( ex, this.actual.currentContext() );
            }
        }

        void onComplete()
        {
            if ( DONE.decrementAndGet( this ) >= 0 )
            {
                this.drain();
            }
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                this.drainLoop();
            }
        }

        void drainLoop()
        {
            int missed = 1;
            ParallelMergeSequential.MergeSequentialInner<T>[] s = this.subscribers;
            int n = s.length;
            CoreSubscriber a = this.actual;

            while ( true )
            {
                long r = this.requested;
                long e = 0L;

                Throwable ex;
                boolean d;
                boolean empty;
                int i;
                ParallelMergeSequential.MergeSequentialInner inner;
                Queue q;
                label89:
                while ( e != r )
                {
                    if ( this.cancelled )
                    {
                        this.cleanup();
                        return;
                    }

                    ex = this.error;
                    if ( ex != null )
                    {
                        this.cleanup();
                        a.onError( ex );
                        return;
                    }

                    d = this.done == 0;
                    empty = true;

                    for ( i = 0; i < n; ++i )
                    {
                        inner = s[i];
                        q = inner.queue;
                        if ( q != null )
                        {
                            T v = q.poll();
                            if ( v != null )
                            {
                                empty = false;
                                a.onNext( v );
                                inner.requestOne();
                                if ( ++e == r )
                                {
                                    break label89;
                                }
                            }
                        }
                    }

                    if ( d && empty )
                    {
                        a.onComplete();
                        return;
                    }

                    if ( empty )
                    {
                        break;
                    }
                }

                if ( e == r )
                {
                    if ( this.cancelled )
                    {
                        this.cleanup();
                        return;
                    }

                    ex = this.error;
                    if ( ex != null )
                    {
                        this.cleanup();
                        a.onError( ex );
                        return;
                    }

                    d = this.done == 0;
                    empty = true;

                    for ( i = 0; i < n; ++i )
                    {
                        inner = s[i];
                        q = inner.queue;
                        if ( q != null && !q.isEmpty() )
                        {
                            empty = false;
                            break;
                        }
                    }

                    if ( d && empty )
                    {
                        a.onComplete();
                        return;
                    }
                }

                if ( e != 0L && r != Long.MAX_VALUE )
                {
                    REQUESTED.addAndGet( this, -e );
                }

                int w = this.wip;
                if ( w == missed )
                {
                    missed = WIP.addAndGet( this, -missed );
                    if ( missed == 0 )
                    {
                        return;
                    }
                }
                else
                {
                    missed = w;
                }
            }
        }
    }
}
