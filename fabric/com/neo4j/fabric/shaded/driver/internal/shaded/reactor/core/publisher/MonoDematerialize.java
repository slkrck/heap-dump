package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;

final class MonoDematerialize<T> extends InternalMonoOperator<Signal<T>,T>
{
    MonoDematerialize( Mono<Signal<T>> source )
    {
        super( source );
    }

    public CoreSubscriber<? super Signal<T>> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxDematerialize.DematerializeSubscriber( actual, true );
    }
}
