package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

final class FluxDetach<T> extends InternalFluxOperator<T,T>
{
    FluxDetach( Flux<? extends T> source )
    {
        super( source );
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        return new FluxDetach.DetachSubscriber( actual );
    }

    static final class DetachSubscriber<T> implements InnerOperator<T,T>
    {
        CoreSubscriber<? super T> actual;
        Subscription s;

        DetachSubscriber( CoreSubscriber<? super T> actual )
        {
            this.actual = actual;
        }

        public Context currentContext()
        {
            return this.actual == null ? Context.empty() : this.actual.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.actual == null;
            }
            else
            {
                return key != Scannable.Attr.CANCELLED ? InnerOperator.super.scanUnsafe( key ) : this.actual == null && this.s == null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            Subscriber<? super T> a = this.actual;
            if ( a != null )
            {
                a.onNext( t );
            }
        }

        public void onError( Throwable t )
        {
            Subscriber<? super T> a = this.actual;
            if ( a != null )
            {
                this.actual = null;
                this.s = null;
                a.onError( t );
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void onComplete()
        {
            Subscriber<? super T> a = this.actual;
            if ( a != null )
            {
                this.actual = null;
                this.s = null;
                a.onComplete();
            }
        }

        public void request( long n )
        {
            Subscription a = this.s;
            if ( a != null )
            {
                a.request( n );
            }
        }

        public void cancel()
        {
            Subscription a = this.s;
            if ( a != null )
            {
                this.actual = null;
                this.s = null;
                a.cancel();
            }
        }
    }
}
