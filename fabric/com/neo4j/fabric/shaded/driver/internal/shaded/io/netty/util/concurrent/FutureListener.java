package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent;

public interface FutureListener<V> extends GenericFutureListener<Future<V>>
{
}
