package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Disposables;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.concurrent.Queues;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

final class FluxJoin<TLeft, TRight, TLeftEnd, TRightEnd, R> extends InternalFluxOperator<TLeft,R>
{
    final Publisher<? extends TRight> other;
    final Function<? super TLeft,? extends Publisher<TLeftEnd>> leftEnd;
    final Function<? super TRight,? extends Publisher<TRightEnd>> rightEnd;
    final BiFunction<? super TLeft,? super TRight,? extends R> resultSelector;

    FluxJoin( Flux<TLeft> source, Publisher<? extends TRight> other, Function<? super TLeft,? extends Publisher<TLeftEnd>> leftEnd,
            Function<? super TRight,? extends Publisher<TRightEnd>> rightEnd, BiFunction<? super TLeft,? super TRight,? extends R> resultSelector )
    {
        super( source );
        this.other = (Publisher) Objects.requireNonNull( other, "other" );
        this.leftEnd = (Function) Objects.requireNonNull( leftEnd, "leftEnd" );
        this.rightEnd = (Function) Objects.requireNonNull( rightEnd, "rightEnd" );
        this.resultSelector = (BiFunction) Objects.requireNonNull( resultSelector, "resultSelector" );
    }

    public CoreSubscriber<? super TLeft> subscribeOrReturn( CoreSubscriber<? super R> actual )
    {
        FluxJoin.JoinSubscription<TLeft,TRight,TLeftEnd,TRightEnd,R> parent =
                new FluxJoin.JoinSubscription( actual, this.leftEnd, this.rightEnd, this.resultSelector );
        actual.onSubscribe( parent );
        FluxGroupJoin.LeftRightSubscriber left = new FluxGroupJoin.LeftRightSubscriber( parent, true );
        parent.cancellations.add( left );
        FluxGroupJoin.LeftRightSubscriber right = new FluxGroupJoin.LeftRightSubscriber( parent, false );
        parent.cancellations.add( right );
        this.source.subscribe( (CoreSubscriber) left );
        this.other.subscribe( right );
        return null;
    }

    static final class JoinSubscription<TLeft, TRight, TLeftEnd, TRightEnd, R> implements FluxGroupJoin.JoinSupport<R>
    {
        static final AtomicIntegerFieldUpdater<FluxJoin.JoinSubscription> WIP = AtomicIntegerFieldUpdater.newUpdater( FluxJoin.JoinSubscription.class, "wip" );
        static final AtomicIntegerFieldUpdater<FluxJoin.JoinSubscription> ACTIVE =
                AtomicIntegerFieldUpdater.newUpdater( FluxJoin.JoinSubscription.class, "active" );
        static final AtomicLongFieldUpdater<FluxJoin.JoinSubscription> REQUESTED =
                AtomicLongFieldUpdater.newUpdater( FluxJoin.JoinSubscription.class, "requested" );
        static final AtomicReferenceFieldUpdater<FluxJoin.JoinSubscription,Throwable> ERROR =
                AtomicReferenceFieldUpdater.newUpdater( FluxJoin.JoinSubscription.class, Throwable.class, "error" );
        static final Integer LEFT_VALUE = 1;
        static final Integer RIGHT_VALUE = 2;
        static final Integer LEFT_CLOSE = 3;
        static final Integer RIGHT_CLOSE = 4;
        final Queue<Object> queue;
        final BiPredicate<Object,Object> queueBiOffer;
        final Disposable.Composite cancellations;
        final Map<Integer,TLeft> lefts;
        final Map<Integer,TRight> rights;
        final Function<? super TLeft,? extends Publisher<TLeftEnd>> leftEnd;
        final Function<? super TRight,? extends Publisher<TRightEnd>> rightEnd;
        final BiFunction<? super TLeft,? super TRight,? extends R> resultSelector;
        final CoreSubscriber<? super R> actual;
        volatile int wip;
        volatile int active;
        volatile long requested;
        volatile Throwable error;
        int leftIndex;
        int rightIndex;

        JoinSubscription( CoreSubscriber<? super R> actual, Function<? super TLeft,? extends Publisher<TLeftEnd>> leftEnd,
                Function<? super TRight,? extends Publisher<TRightEnd>> rightEnd, BiFunction<? super TLeft,? super TRight,? extends R> resultSelector )
        {
            this.actual = actual;
            this.cancellations = Disposables.composite();
            this.queue = (Queue) Queues.unboundedMultiproducer().get();
            this.queueBiOffer = (BiPredicate) this.queue;
            this.lefts = new LinkedHashMap();
            this.rights = new LinkedHashMap();
            this.leftEnd = leftEnd;
            this.rightEnd = rightEnd;
            this.resultSelector = resultSelector;
            ACTIVE.lazySet( this, 2 );
        }

        public final CoreSubscriber<? super R> actual()
        {
            return this.actual;
        }

        public Stream<? extends Scannable> inners()
        {
            return Scannable.from( this.cancellations ).inners();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.REQUESTED_FROM_DOWNSTREAM )
            {
                return this.requested;
            }
            else if ( key == Scannable.Attr.CANCELLED )
            {
                return this.cancellations.isDisposed();
            }
            else if ( key == Scannable.Attr.BUFFERED )
            {
                return this.queue.size() / 2;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.active == 0;
            }
            else
            {
                return key == Scannable.Attr.ERROR ? this.error : FluxGroupJoin.JoinSupport.super.scanUnsafe( key );
            }
        }

        public void request( long n )
        {
            if ( Operators.validate( n ) )
            {
                Operators.addCap( REQUESTED, this, n );
            }
        }

        public void cancel()
        {
            if ( !this.cancellations.isDisposed() )
            {
                this.cancellations.dispose();
                if ( WIP.getAndIncrement( this ) == 0 )
                {
                    this.queue.clear();
                }
            }
        }

        void errorAll( Subscriber<?> a )
        {
            Throwable ex = Exceptions.terminate( ERROR, this );
            this.lefts.clear();
            this.rights.clear();
            a.onError( ex );
        }

        void drain()
        {
            if ( WIP.getAndIncrement( this ) == 0 )
            {
                int missed = 1;
                Queue<Object> q = this.queue;
                CoreSubscriber a = this.actual;

                while ( !this.cancellations.isDisposed() )
                {
                    Throwable ex = this.error;
                    if ( ex != null )
                    {
                        q.clear();
                        this.cancellations.dispose();
                        this.errorAll( a );
                        return;
                    }

                    boolean d = this.active == 0;
                    Integer mode = (Integer) q.poll();
                    boolean empty = mode == null;
                    if ( d && empty )
                    {
                        this.lefts.clear();
                        this.rights.clear();
                        this.cancellations.dispose();
                        a.onComplete();
                        return;
                    }

                    if ( empty )
                    {
                        missed = WIP.addAndGet( this, -missed );
                        if ( missed == 0 )
                        {
                            return;
                        }
                    }
                    else
                    {
                        Object val = q.poll();
                        Object right;
                        int idx;
                        Publisher p;
                        FluxGroupJoin.LeftRightEndSubscriber end;
                        long r;
                        long e;
                        Iterator var17;
                        Object left;
                        Object w;
                        if ( mode == LEFT_VALUE )
                        {
                            right = val;
                            idx = this.leftIndex++;
                            this.lefts.put( idx, val );

                            try
                            {
                                p = (Publisher) Objects.requireNonNull( this.leftEnd.apply( right ), "The leftEnd returned a null Publisher" );
                            }
                            catch ( Throwable var24 )
                            {
                                Exceptions.addThrowable( ERROR, this, Operators.onOperatorError( this, var24, val, this.actual.currentContext() ) );
                                this.errorAll( a );
                                return;
                            }

                            end = new FluxGroupJoin.LeftRightEndSubscriber( this, true, idx );
                            this.cancellations.add( end );
                            p.subscribe( end );
                            ex = this.error;
                            if ( ex != null )
                            {
                                q.clear();
                                this.cancellations.dispose();
                                this.errorAll( a );
                                return;
                            }

                            r = this.requested;
                            e = 0L;

                            for ( var17 = this.rights.values().iterator(); var17.hasNext(); ++e )
                            {
                                left = var17.next();

                                try
                                {
                                    w = Objects.requireNonNull( this.resultSelector.apply( right, left ), "The resultSelector returned a null value" );
                                }
                                catch ( Throwable var23 )
                                {
                                    Exceptions.addThrowable( ERROR, this, Operators.onOperatorError( this, var23, left, this.actual.currentContext() ) );
                                    this.errorAll( a );
                                    return;
                                }

                                if ( e == r )
                                {
                                    Exceptions.addThrowable( ERROR, this, Exceptions.failWithOverflow( "Could not emit value due to lack of requests" ) );
                                    q.clear();
                                    this.cancellations.dispose();
                                    this.errorAll( a );
                                    return;
                                }

                                a.onNext( w );
                            }

                            if ( e != 0L )
                            {
                                Operators.produced( REQUESTED, this, e );
                            }
                        }
                        else if ( mode != RIGHT_VALUE )
                        {
                            FluxGroupJoin.LeftRightEndSubscriber end;
                            if ( mode == LEFT_CLOSE )
                            {
                                end = (FluxGroupJoin.LeftRightEndSubscriber) val;
                                this.lefts.remove( end.index );
                                this.cancellations.remove( end );
                            }
                            else if ( mode == RIGHT_CLOSE )
                            {
                                end = (FluxGroupJoin.LeftRightEndSubscriber) val;
                                this.rights.remove( end.index );
                                this.cancellations.remove( end );
                            }
                        }
                        else
                        {
                            right = val;
                            idx = this.rightIndex++;
                            this.rights.put( idx, val );

                            try
                            {
                                p = (Publisher) Objects.requireNonNull( this.rightEnd.apply( right ), "The rightEnd returned a null Publisher" );
                            }
                            catch ( Throwable var22 )
                            {
                                Exceptions.addThrowable( ERROR, this, Operators.onOperatorError( this, var22, val, this.actual.currentContext() ) );
                                this.errorAll( a );
                                return;
                            }

                            end = new FluxGroupJoin.LeftRightEndSubscriber( this, false, idx );
                            this.cancellations.add( end );
                            p.subscribe( end );
                            ex = this.error;
                            if ( ex != null )
                            {
                                q.clear();
                                this.cancellations.dispose();
                                this.errorAll( a );
                                return;
                            }

                            r = this.requested;
                            e = 0L;

                            for ( var17 = this.lefts.values().iterator(); var17.hasNext(); ++e )
                            {
                                left = var17.next();

                                try
                                {
                                    w = Objects.requireNonNull( this.resultSelector.apply( left, right ), "The resultSelector returned a null value" );
                                }
                                catch ( Throwable var21 )
                                {
                                    Exceptions.addThrowable( ERROR, this, Operators.onOperatorError( this, var21, left, this.actual.currentContext() ) );
                                    this.errorAll( a );
                                    return;
                                }

                                if ( e == r )
                                {
                                    Exceptions.addThrowable( ERROR, this, Exceptions.failWithOverflow( "Could not emit value due to lack of requests" ) );
                                    q.clear();
                                    this.cancellations.dispose();
                                    this.errorAll( a );
                                    return;
                                }

                                a.onNext( w );
                            }

                            if ( e != 0L )
                            {
                                Operators.produced( REQUESTED, this, e );
                            }
                        }
                    }
                }

                q.clear();
            }
        }

        public void innerError( Throwable ex )
        {
            if ( Exceptions.addThrowable( ERROR, this, ex ) )
            {
                ACTIVE.decrementAndGet( this );
                this.drain();
            }
            else
            {
                Operators.onErrorDropped( ex, this.actual.currentContext() );
            }
        }

        public void innerComplete( FluxGroupJoin.LeftRightSubscriber sender )
        {
            this.cancellations.remove( sender );
            ACTIVE.decrementAndGet( this );
            this.drain();
        }

        public void innerValue( boolean isLeft, Object o )
        {
            this.queueBiOffer.test( isLeft ? LEFT_VALUE : RIGHT_VALUE, o );
            this.drain();
        }

        public void innerClose( boolean isLeft, FluxGroupJoin.LeftRightEndSubscriber index )
        {
            this.queueBiOffer.test( isLeft ? LEFT_CLOSE : RIGHT_CLOSE, index );
            this.drain();
        }

        public void innerCloseError( Throwable ex )
        {
            if ( Exceptions.addThrowable( ERROR, this, ex ) )
            {
                this.drain();
            }
            else
            {
                Operators.onErrorDropped( ex, this.actual.currentContext() );
            }
        }
    }
}
