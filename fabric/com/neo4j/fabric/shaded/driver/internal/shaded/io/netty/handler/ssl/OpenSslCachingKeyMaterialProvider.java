package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.net.ssl.X509KeyManager;

final class OpenSslCachingKeyMaterialProvider extends OpenSslKeyMaterialProvider
{
    private final ConcurrentMap<String,OpenSslKeyMaterial> cache = new ConcurrentHashMap();

    OpenSslCachingKeyMaterialProvider( X509KeyManager keyManager, String password )
    {
        super( keyManager, password );
    }

    OpenSslKeyMaterial chooseKeyMaterial( ByteBufAllocator allocator, String alias ) throws Exception
    {
        OpenSslKeyMaterial material = (OpenSslKeyMaterial) this.cache.get( alias );
        if ( material == null )
        {
            material = super.chooseKeyMaterial( allocator, alias );
            if ( material == null )
            {
                return null;
            }

            OpenSslKeyMaterial old = (OpenSslKeyMaterial) this.cache.putIfAbsent( alias, material );
            if ( old != null )
            {
                material.release();
                material = old;
            }
        }

        return material.retain();
    }

    void destroy()
    {
        do
        {
            Iterator iterator = this.cache.values().iterator();

            while ( iterator.hasNext() )
            {
                ((OpenSslKeyMaterial) iterator.next()).release();
                iterator.remove();
            }
        }
        while ( !this.cache.isEmpty() );
    }
}
