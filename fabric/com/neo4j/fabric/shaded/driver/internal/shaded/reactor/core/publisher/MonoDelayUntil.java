package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CorePublisher;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Exceptions;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.annotation.Nullable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.util.context.Context;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

final class MonoDelayUntil<T> extends Mono<T> implements Scannable, OptimizableOperator<T,T>
{
    final Mono<T> source;
    @Nullable
    final OptimizableOperator<?,T> optimizableOperator;
    Function<? super T,? extends Publisher<?>>[] otherGenerators;

    MonoDelayUntil( Mono<T> monoSource, Function<? super T,? extends Publisher<?>> triggerGenerator )
    {
        this.source = (Mono) Objects.requireNonNull( monoSource, "monoSource" );
        this.otherGenerators = new Function[]{(Function) Objects.requireNonNull( triggerGenerator, "triggerGenerator" )};
        this.optimizableOperator = this.source instanceof OptimizableOperator ? (OptimizableOperator) this.source : null;
    }

    MonoDelayUntil( Mono<T> monoSource, Function<? super T,? extends Publisher<?>>[] triggerGenerators )
    {
        this.source = (Mono) Objects.requireNonNull( monoSource, "monoSource" );
        this.otherGenerators = triggerGenerators;
        this.optimizableOperator = this.source instanceof OptimizableOperator ? (OptimizableOperator) this.source : null;
    }

    MonoDelayUntil<T> copyWithNewTriggerGenerator( boolean delayError, Function<? super T,? extends Publisher<?>> triggerGenerator )
    {
        Objects.requireNonNull( triggerGenerator, "triggerGenerator" );
        Function<? super T,? extends Publisher<?>>[] oldTriggers = this.otherGenerators;
        Function<? super T,? extends Publisher<?>>[] newTriggers = new Function[oldTriggers.length + 1];
        System.arraycopy( oldTriggers, 0, newTriggers, 0, oldTriggers.length );
        newTriggers[oldTriggers.length] = triggerGenerator;
        return new MonoDelayUntil( this.source, newTriggers );
    }

    public void subscribe( CoreSubscriber<? super T> actual )
    {
        this.source.subscribe( this.subscribeOrReturn( actual ) );
    }

    public final CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        MonoDelayUntil.DelayUntilCoordinator<T> parent = new MonoDelayUntil.DelayUntilCoordinator( actual, this.otherGenerators );
        actual.onSubscribe( parent );
        return parent;
    }

    public final CorePublisher<? extends T> source()
    {
        return this.source;
    }

    public final OptimizableOperator<?,? extends T> nextOptimizableSource()
    {
        return this.optimizableOperator;
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return null;
    }

    static final class DelayUntilTrigger<T> implements InnerConsumer<T>
    {
        static final AtomicReferenceFieldUpdater<MonoDelayUntil.DelayUntilTrigger,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoDelayUntil.DelayUntilTrigger.class, Subscription.class, "s" );
        final MonoDelayUntil.DelayUntilCoordinator<?> parent;
        volatile Subscription s;
        boolean done;
        volatile Throwable error;

        DelayUntilTrigger( MonoDelayUntil.DelayUntilCoordinator<?> parent )
        {
            this.parent = parent;
        }

        public Context currentContext()
        {
            return this.parent.currentContext();
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.CANCELLED )
            {
                return this.s == Operators.cancelledSubscription();
            }
            else if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.ACTUAL )
            {
                return this.parent;
            }
            else if ( key == Scannable.Attr.ERROR )
            {
                return this.error;
            }
            else
            {
                return key == Scannable.Attr.PREFETCH ? Integer.MAX_VALUE : null;
            }
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
            else
            {
                s.cancel();
            }
        }

        public void onNext( Object t )
        {
        }

        public void onError( Throwable t )
        {
            this.error = t;
            if ( MonoDelayUntil.DelayUntilCoordinator.DONE.getAndSet( this.parent, this.parent.n ) != this.parent.n )
            {
                this.parent.cancel();
                this.parent.actual.onError( t );
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                this.parent.signal();
            }
        }

        void cancel()
        {
            Operators.terminate( S, this );
        }
    }

    static final class DelayUntilCoordinator<T> extends Operators.MonoSubscriber<T,T>
    {
        static final MonoDelayUntil.DelayUntilTrigger[] NO_TRIGGER = new MonoDelayUntil.DelayUntilTrigger[0];
        static final AtomicIntegerFieldUpdater<MonoDelayUntil.DelayUntilCoordinator> DONE =
                AtomicIntegerFieldUpdater.newUpdater( MonoDelayUntil.DelayUntilCoordinator.class, "done" );
        static final AtomicReferenceFieldUpdater<MonoDelayUntil.DelayUntilCoordinator,Subscription> S =
                AtomicReferenceFieldUpdater.newUpdater( MonoDelayUntil.DelayUntilCoordinator.class, Subscription.class, "s" );
        final int n;
        final Function<? super T,? extends Publisher<?>>[] otherGenerators;
        volatile int done;
        volatile Subscription s;
        MonoDelayUntil.DelayUntilTrigger[] triggerSubscribers;

        DelayUntilCoordinator( CoreSubscriber<? super T> subscriber, Function<? super T,? extends Publisher<?>>[] otherGenerators )
        {
            super( subscriber );
            this.otherGenerators = otherGenerators;
            this.n = otherGenerators.length;
            this.triggerSubscribers = NO_TRIGGER;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.setOnce( S, this, s ) )
            {
                s.request( Long.MAX_VALUE );
            }
            else
            {
                s.cancel();
            }
        }

        public void onNext( T t )
        {
            if ( this.value == null )
            {
                this.setValue( t );
                this.subscribeNextTrigger( t, this.done );
            }
        }

        public void onError( Throwable t )
        {
            this.actual.onError( t );
        }

        public void onComplete()
        {
            if ( this.value == null && this.state < 3 )
            {
                this.actual.onComplete();
            }
        }

        @Nullable
        public Object scanUnsafe( Scannable.Attr key )
        {
            return key == Scannable.Attr.TERMINATED ? this.done == this.n : super.scanUnsafe( key );
        }

        public Stream<? extends Scannable> inners()
        {
            return Stream.of( this.triggerSubscribers );
        }

        void subscribeNextTrigger( T value, int triggerIndex )
        {
            if ( this.triggerSubscribers == NO_TRIGGER )
            {
                this.triggerSubscribers = new MonoDelayUntil.DelayUntilTrigger[this.otherGenerators.length];
            }

            Function generator = this.otherGenerators[triggerIndex];

            Publisher p;
            try
            {
                p = (Publisher) generator.apply( value );
            }
            catch ( Throwable var6 )
            {
                this.onError( var6 );
                return;
            }

            MonoDelayUntil.DelayUntilTrigger triggerSubscriber = new MonoDelayUntil.DelayUntilTrigger( this );
            this.triggerSubscribers[triggerIndex] = triggerSubscriber;
            p.subscribe( triggerSubscriber );
        }

        void signal()
        {
            int nextIndex = DONE.incrementAndGet( this );
            if ( nextIndex != this.n )
            {
                this.subscribeNextTrigger( this.value, nextIndex );
            }
            else
            {
                Throwable error = null;
                Throwable compositeError = null;

                for ( int i = 0; i < this.n; ++i )
                {
                    MonoDelayUntil.DelayUntilTrigger mt = this.triggerSubscribers[i];
                    Throwable e = mt.error;
                    if ( e != null )
                    {
                        if ( compositeError != null )
                        {
                            compositeError.addSuppressed( e );
                        }
                        else if ( error != null )
                        {
                            compositeError = Exceptions.multiple( error, e );
                        }
                        else
                        {
                            error = e;
                        }
                    }
                }

                if ( compositeError != null )
                {
                    this.actual.onError( compositeError );
                }
                else if ( error != null )
                {
                    this.actual.onError( error );
                }
                else
                {
                    this.complete( this.value );
                }
            }
        }

        public void cancel()
        {
            if ( !this.isCancelled() )
            {
                super.cancel();
                Operators.terminate( S, this );

                for ( int i = 0; i < this.triggerSubscribers.length; ++i )
                {
                    MonoDelayUntil.DelayUntilTrigger ts = this.triggerSubscribers[i];
                    if ( ts != null )
                    {
                        ts.cancel();
                    }
                }
            }
        }
    }
}
