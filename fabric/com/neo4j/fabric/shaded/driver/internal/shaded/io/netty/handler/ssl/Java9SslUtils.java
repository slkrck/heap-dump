package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.EmptyArrays;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.PlatformDependent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.security.SecureRandom;
import java.util.List;
import java.util.function.BiFunction;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManager;

final class Java9SslUtils
{
    private static final InternalLogger logger = InternalLoggerFactory.getInstance( Java9SslUtils.class );
    private static final Method SET_APPLICATION_PROTOCOLS;
    private static final Method GET_APPLICATION_PROTOCOL;
    private static final Method GET_HANDSHAKE_APPLICATION_PROTOCOL;
    private static final Method SET_HANDSHAKE_APPLICATION_PROTOCOL_SELECTOR;
    private static final Method GET_HANDSHAKE_APPLICATION_PROTOCOL_SELECTOR;

    static
    {
        Method getHandshakeApplicationProtocol = null;
        Method getApplicationProtocol = null;
        Method setApplicationProtocols = null;
        Method setHandshakeApplicationProtocolSelector = null;
        Method getHandshakeApplicationProtocolSelector = null;

        try
        {
            SSLContext context = SSLContext.getInstance( "TLS" );
            context.init( (KeyManager[]) null, (TrustManager[]) null, (SecureRandom) null );
            SSLEngine engine = context.createSSLEngine();
            getHandshakeApplicationProtocol = (Method) AccessController.doPrivileged( new PrivilegedExceptionAction<Method>()
            {
                public Method run() throws Exception
                {
                    return SSLEngine.class.getMethod( "getHandshakeApplicationProtocol" );
                }
            } );
            getHandshakeApplicationProtocol.invoke( engine );
            getApplicationProtocol = (Method) AccessController.doPrivileged( new PrivilegedExceptionAction<Method>()
            {
                public Method run() throws Exception
                {
                    return SSLEngine.class.getMethod( "getApplicationProtocol" );
                }
            } );
            getApplicationProtocol.invoke( engine );
            setApplicationProtocols = (Method) AccessController.doPrivileged( new PrivilegedExceptionAction<Method>()
            {
                public Method run() throws Exception
                {
                    return SSLParameters.class.getMethod( "setApplicationProtocols", String[].class );
                }
            } );
            setApplicationProtocols.invoke( engine.getSSLParameters(), EmptyArrays.EMPTY_STRINGS );
            setHandshakeApplicationProtocolSelector = (Method) AccessController.doPrivileged( new PrivilegedExceptionAction<Method>()
            {
                public Method run() throws Exception
                {
                    return SSLEngine.class.getMethod( "setHandshakeApplicationProtocolSelector", BiFunction.class );
                }
            } );
            setHandshakeApplicationProtocolSelector.invoke( engine, new BiFunction<SSLEngine,List<String>,String>()
            {
                public String apply( SSLEngine sslEngine, List<String> strings )
                {
                    return null;
                }
            } );
            getHandshakeApplicationProtocolSelector = (Method) AccessController.doPrivileged( new PrivilegedExceptionAction<Method>()
            {
                public Method run() throws Exception
                {
                    return SSLEngine.class.getMethod( "getHandshakeApplicationProtocolSelector" );
                }
            } );
            getHandshakeApplicationProtocolSelector.invoke( engine );
        }
        catch ( Throwable var7 )
        {
            logger.error( "Unable to initialize Java9SslUtils, but the detected javaVersion was: {}", PlatformDependent.javaVersion(), var7 );
            getHandshakeApplicationProtocol = null;
            getApplicationProtocol = null;
            setApplicationProtocols = null;
            setHandshakeApplicationProtocolSelector = null;
            getHandshakeApplicationProtocolSelector = null;
        }

        GET_HANDSHAKE_APPLICATION_PROTOCOL = getHandshakeApplicationProtocol;
        GET_APPLICATION_PROTOCOL = getApplicationProtocol;
        SET_APPLICATION_PROTOCOLS = setApplicationProtocols;
        SET_HANDSHAKE_APPLICATION_PROTOCOL_SELECTOR = setHandshakeApplicationProtocolSelector;
        GET_HANDSHAKE_APPLICATION_PROTOCOL_SELECTOR = getHandshakeApplicationProtocolSelector;
    }

    private Java9SslUtils()
    {
    }

    static boolean supportsAlpn()
    {
        return GET_APPLICATION_PROTOCOL != null;
    }

    static String getApplicationProtocol( SSLEngine sslEngine )
    {
        try
        {
            return (String) GET_APPLICATION_PROTOCOL.invoke( sslEngine );
        }
        catch ( UnsupportedOperationException var2 )
        {
            throw var2;
        }
        catch ( Exception var3 )
        {
            throw new IllegalStateException( var3 );
        }
    }

    static String getHandshakeApplicationProtocol( SSLEngine sslEngine )
    {
        try
        {
            return (String) GET_HANDSHAKE_APPLICATION_PROTOCOL.invoke( sslEngine );
        }
        catch ( UnsupportedOperationException var2 )
        {
            throw var2;
        }
        catch ( Exception var3 )
        {
            throw new IllegalStateException( var3 );
        }
    }

    static void setApplicationProtocols( SSLEngine engine, List<String> supportedProtocols )
    {
        SSLParameters parameters = engine.getSSLParameters();
        String[] protocolArray = (String[]) supportedProtocols.toArray( EmptyArrays.EMPTY_STRINGS );

        try
        {
            SET_APPLICATION_PROTOCOLS.invoke( parameters, protocolArray );
        }
        catch ( UnsupportedOperationException var5 )
        {
            throw var5;
        }
        catch ( Exception var6 )
        {
            throw new IllegalStateException( var6 );
        }

        engine.setSSLParameters( parameters );
    }

    static void setHandshakeApplicationProtocolSelector( SSLEngine engine, BiFunction<SSLEngine,List<String>,String> selector )
    {
        try
        {
            SET_HANDSHAKE_APPLICATION_PROTOCOL_SELECTOR.invoke( engine, selector );
        }
        catch ( UnsupportedOperationException var3 )
        {
            throw var3;
        }
        catch ( Exception var4 )
        {
            throw new IllegalStateException( var4 );
        }
    }

    static BiFunction<SSLEngine,List<String>,String> getHandshakeApplicationProtocolSelector( SSLEngine engine )
    {
        try
        {
            return (BiFunction) GET_HANDSHAKE_APPLICATION_PROTOCOL_SELECTOR.invoke( engine );
        }
        catch ( UnsupportedOperationException var2 )
        {
            throw var2;
        }
        catch ( Exception var3 )
        {
            throw new IllegalStateException( var3 );
        }
    }
}
