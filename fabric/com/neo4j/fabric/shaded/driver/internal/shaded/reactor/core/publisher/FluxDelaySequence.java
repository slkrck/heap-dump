package com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.publisher;

import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.CoreSubscriber;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.Scannable;
import com.neo4j.fabric.shaded.driver.internal.shaded.reactor.core.scheduler.Scheduler;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

import org.reactivestreams.Subscription;

final class FluxDelaySequence<T> extends InternalFluxOperator<T,T>
{
    final Duration delay;
    final Scheduler scheduler;

    FluxDelaySequence( Flux<T> source, Duration delay, Scheduler scheduler )
    {
        super( source );
        this.delay = delay;
        this.scheduler = scheduler;
    }

    public CoreSubscriber<? super T> subscribeOrReturn( CoreSubscriber<? super T> actual )
    {
        Scheduler.Worker w = this.scheduler.createWorker();
        return new FluxDelaySequence.DelaySubscriber( actual, this.delay, w );
    }

    public Object scanUnsafe( Scannable.Attr key )
    {
        return key == Scannable.Attr.RUN_ON ? this.scheduler : super.scanUnsafe( key );
    }

    static final class DelaySubscriber<T> implements InnerOperator<T,T>
    {
        static final AtomicLongFieldUpdater<FluxDelaySequence.DelaySubscriber> DELAYED =
                AtomicLongFieldUpdater.newUpdater( FluxDelaySequence.DelaySubscriber.class, "delayed" );
        final CoreSubscriber<? super T> actual;
        final long delay;
        final TimeUnit timeUnit;
        final Scheduler.Worker w;
        Subscription s;
        volatile boolean done;
        volatile long delayed;

        DelaySubscriber( CoreSubscriber<? super T> actual, Duration delay, Scheduler.Worker w )
        {
            this.actual = new SerializedSubscriber( actual );
            this.w = w;
            if ( delay.compareTo( Duration.ofMinutes( 1L ) ) < 0 )
            {
                this.delay = delay.toNanos();
                this.timeUnit = TimeUnit.NANOSECONDS;
            }
            else
            {
                this.delay = delay.toMillis();
                this.timeUnit = TimeUnit.MILLISECONDS;
            }
        }

        public CoreSubscriber<? super T> actual()
        {
            return this.actual;
        }

        public void onSubscribe( Subscription s )
        {
            if ( Operators.validate( this.s, s ) )
            {
                this.s = s;
                this.actual.onSubscribe( this );
            }
        }

        public void onNext( T t )
        {
            if ( !this.done && this.delayed >= 0L )
            {
                DELAYED.incrementAndGet( this );
                this.w.schedule( () -> {
                    this.delayedNext( t );
                }, this.delay, this.timeUnit );
            }
            else
            {
                Operators.onNextDropped( t, this.currentContext() );
            }
        }

        private void delayedNext( T t )
        {
            DELAYED.decrementAndGet( this );
            this.actual.onNext( t );
        }

        public void onError( Throwable t )
        {
            if ( this.done )
            {
                Operators.onErrorDropped( t, this.currentContext() );
            }
            else
            {
                this.done = true;
                if ( DELAYED.compareAndSet( this, 0L, -1L ) )
                {
                    this.actual.onError( t );
                }
                else
                {
                    this.w.schedule( new FluxDelaySequence.DelaySubscriber.OnError( t ), this.delay, this.timeUnit );
                }
            }
        }

        public void onComplete()
        {
            if ( !this.done )
            {
                this.done = true;
                if ( DELAYED.compareAndSet( this, 0L, -1L ) )
                {
                    this.actual.onComplete();
                }
                else
                {
                    this.w.schedule( new FluxDelaySequence.DelaySubscriber.OnComplete(), this.delay, this.timeUnit );
                }
            }
        }

        public void request( long n )
        {
            this.s.request( n );
        }

        public void cancel()
        {
            this.s.cancel();
            this.w.dispose();
        }

        public Object scanUnsafe( Scannable.Attr key )
        {
            if ( key == Scannable.Attr.PARENT )
            {
                return this.s;
            }
            else if ( key == Scannable.Attr.RUN_ON )
            {
                return this.w;
            }
            else if ( key == Scannable.Attr.TERMINATED )
            {
                return this.done;
            }
            else
            {
                return key != Scannable.Attr.CANCELLED ? InnerOperator.super.scanUnsafe( key ) : this.w.isDisposed() && !this.done;
            }
        }

        final class OnComplete implements Runnable
        {
            public void run()
            {
                try
                {
                    DelaySubscriber.this.actual.onComplete();
                }
                finally
                {
                    DelaySubscriber.this.w.dispose();
                }
            }
        }

        final class OnError implements Runnable
        {
            private final Throwable t;

            OnError( Throwable t )
            {
                this.t = t;
            }

            public void run()
            {
                try
                {
                    DelaySubscriber.this.actual.onError( this.t );
                }
                finally
                {
                    DelaySubscriber.this.w.dispose();
                }
            }
        }
    }
}
