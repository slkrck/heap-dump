package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket.oio;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOption;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.MessageSizeEstimator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.PreferHeapByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.RecvByteBufAllocator;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.WriteBufferWaterMark;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket.DatagramChannel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket.DefaultDatagramChannelConfig;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Map;

final class DefaultOioDatagramChannelConfig extends DefaultDatagramChannelConfig implements OioDatagramChannelConfig
{
    DefaultOioDatagramChannelConfig( DatagramChannel channel, DatagramSocket javaSocket )
    {
        super( channel, javaSocket );
        this.setAllocator( new PreferHeapByteBufAllocator( this.getAllocator() ) );
    }

    public Map<ChannelOption<?>,Object> getOptions()
    {
        return this.getOptions( super.getOptions(), new ChannelOption[]{ChannelOption.SO_TIMEOUT} );
    }

    public <T> T getOption( ChannelOption<T> option )
    {
        return option == ChannelOption.SO_TIMEOUT ? this.getSoTimeout() : super.getOption( option );
    }

    public <T> boolean setOption( ChannelOption<T> option, T value )
    {
        this.validate( option, value );
        if ( option == ChannelOption.SO_TIMEOUT )
        {
            this.setSoTimeout( (Integer) value );
            return true;
        }
        else
        {
            return super.setOption( option, value );
        }
    }

    public OioDatagramChannelConfig setSoTimeout( int timeout )
    {
        try
        {
            this.javaSocket().setSoTimeout( timeout );
            return this;
        }
        catch ( IOException var3 )
        {
            throw new ChannelException( var3 );
        }
    }

    public int getSoTimeout()
    {
        try
        {
            return this.javaSocket().getSoTimeout();
        }
        catch ( IOException var2 )
        {
            throw new ChannelException( var2 );
        }
    }

    public OioDatagramChannelConfig setBroadcast( boolean broadcast )
    {
        super.setBroadcast( broadcast );
        return this;
    }

    public OioDatagramChannelConfig setInterface( InetAddress interfaceAddress )
    {
        super.setInterface( interfaceAddress );
        return this;
    }

    public OioDatagramChannelConfig setLoopbackModeDisabled( boolean loopbackModeDisabled )
    {
        super.setLoopbackModeDisabled( loopbackModeDisabled );
        return this;
    }

    public OioDatagramChannelConfig setNetworkInterface( NetworkInterface networkInterface )
    {
        super.setNetworkInterface( networkInterface );
        return this;
    }

    public OioDatagramChannelConfig setReuseAddress( boolean reuseAddress )
    {
        super.setReuseAddress( reuseAddress );
        return this;
    }

    public OioDatagramChannelConfig setReceiveBufferSize( int receiveBufferSize )
    {
        super.setReceiveBufferSize( receiveBufferSize );
        return this;
    }

    public OioDatagramChannelConfig setSendBufferSize( int sendBufferSize )
    {
        super.setSendBufferSize( sendBufferSize );
        return this;
    }

    public OioDatagramChannelConfig setTimeToLive( int ttl )
    {
        super.setTimeToLive( ttl );
        return this;
    }

    public OioDatagramChannelConfig setTrafficClass( int trafficClass )
    {
        super.setTrafficClass( trafficClass );
        return this;
    }

    public OioDatagramChannelConfig setWriteSpinCount( int writeSpinCount )
    {
        super.setWriteSpinCount( writeSpinCount );
        return this;
    }

    public OioDatagramChannelConfig setConnectTimeoutMillis( int connectTimeoutMillis )
    {
        super.setConnectTimeoutMillis( connectTimeoutMillis );
        return this;
    }

    public OioDatagramChannelConfig setMaxMessagesPerRead( int maxMessagesPerRead )
    {
        super.setMaxMessagesPerRead( maxMessagesPerRead );
        return this;
    }

    public OioDatagramChannelConfig setAllocator( ByteBufAllocator allocator )
    {
        super.setAllocator( allocator );
        return this;
    }

    public OioDatagramChannelConfig setRecvByteBufAllocator( RecvByteBufAllocator allocator )
    {
        super.setRecvByteBufAllocator( allocator );
        return this;
    }

    public OioDatagramChannelConfig setAutoRead( boolean autoRead )
    {
        super.setAutoRead( autoRead );
        return this;
    }

    public OioDatagramChannelConfig setAutoClose( boolean autoClose )
    {
        super.setAutoClose( autoClose );
        return this;
    }

    public OioDatagramChannelConfig setWriteBufferHighWaterMark( int writeBufferHighWaterMark )
    {
        super.setWriteBufferHighWaterMark( writeBufferHighWaterMark );
        return this;
    }

    public OioDatagramChannelConfig setWriteBufferLowWaterMark( int writeBufferLowWaterMark )
    {
        super.setWriteBufferLowWaterMark( writeBufferLowWaterMark );
        return this;
    }

    public OioDatagramChannelConfig setWriteBufferWaterMark( WriteBufferWaterMark writeBufferWaterMark )
    {
        super.setWriteBufferWaterMark( writeBufferWaterMark );
        return this;
    }

    public OioDatagramChannelConfig setMessageSizeEstimator( MessageSizeEstimator estimator )
    {
        super.setMessageSizeEstimator( estimator );
        return this;
    }
}
