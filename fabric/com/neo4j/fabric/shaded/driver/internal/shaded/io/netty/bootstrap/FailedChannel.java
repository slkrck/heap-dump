package com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.AbstractChannel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelConfig;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelMetadata;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOutboundBuffer;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.DefaultChannelConfig;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoop;

import java.net.SocketAddress;

final class FailedChannel extends AbstractChannel
{
    private static final ChannelMetadata METADATA = new ChannelMetadata( false );
    private final ChannelConfig config = new DefaultChannelConfig( this );

    FailedChannel()
    {
        super( (Channel) null );
    }

    protected AbstractChannel.AbstractUnsafe newUnsafe()
    {
        return new FailedChannel.FailedChannelUnsafe();
    }

    protected boolean isCompatible( EventLoop loop )
    {
        return false;
    }

    protected SocketAddress localAddress0()
    {
        return null;
    }

    protected SocketAddress remoteAddress0()
    {
        return null;
    }

    protected void doBind( SocketAddress localAddress )
    {
        throw new UnsupportedOperationException();
    }

    protected void doDisconnect()
    {
        throw new UnsupportedOperationException();
    }

    protected void doClose()
    {
        throw new UnsupportedOperationException();
    }

    protected void doBeginRead()
    {
        throw new UnsupportedOperationException();
    }

    protected void doWrite( ChannelOutboundBuffer in )
    {
        throw new UnsupportedOperationException();
    }

    public ChannelConfig config()
    {
        return this.config;
    }

    public boolean isOpen()
    {
        return false;
    }

    public boolean isActive()
    {
        return false;
    }

    public ChannelMetadata metadata()
    {
        return METADATA;
    }

    private final class FailedChannelUnsafe extends AbstractChannel.AbstractUnsafe
    {
        private FailedChannelUnsafe()
        {
            super();
        }

        public void connect( SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise )
        {
            promise.setFailure( new UnsupportedOperationException() );
        }
    }
}
