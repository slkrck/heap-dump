package com.neo4j.fabric.shaded.driver.internal.spi;

import com.neo4j.fabric.shaded.driver.Value;

import java.util.Map;

public interface ResponseHandler
{
    void onSuccess( Map<String,Value> var1 );

    void onFailure( Throwable var1 );

    void onRecord( Value[] var1 );

    default boolean canManageAutoRead()
    {
        return false;
    }

    default void disableAutoReadManagement()
    {
    }
}
