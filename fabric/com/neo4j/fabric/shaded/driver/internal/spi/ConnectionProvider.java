package com.neo4j.fabric.shaded.driver.internal.spi;

import com.neo4j.fabric.shaded.driver.internal.async.ConnectionContext;

import java.util.concurrent.CompletionStage;

public interface ConnectionProvider
{
    CompletionStage<Connection> acquireConnection( ConnectionContext var1 );

    CompletionStage<Void> verifyConnectivity();

    CompletionStage<Void> close();

    CompletionStage<Boolean> supportsMultiDb();
}
