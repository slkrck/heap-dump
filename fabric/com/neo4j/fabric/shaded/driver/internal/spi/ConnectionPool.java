package com.neo4j.fabric.shaded.driver.internal.spi;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;

import java.util.Set;
import java.util.concurrent.CompletionStage;

public interface ConnectionPool
{
    CompletionStage<Connection> acquire( BoltServerAddress var1 );

    void retainAll( Set<BoltServerAddress> var1 );

    int inUseConnections( BoltServerAddress var1 );

    int idleConnections( BoltServerAddress var1 );

    CompletionStage<Void> close();

    boolean isOpen( BoltServerAddress var1 );
}
