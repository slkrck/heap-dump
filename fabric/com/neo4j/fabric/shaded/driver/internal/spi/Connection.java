package com.neo4j.fabric.shaded.driver.internal.spi;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.concurrent.CompletionStage;

public interface Connection
{
    boolean isOpen();

    void enableAutoRead();

    void disableAutoRead();

    void write( Message var1, ResponseHandler var2 );

    void write( Message var1, ResponseHandler var2, Message var3, ResponseHandler var4 );

    void writeAndFlush( Message var1, ResponseHandler var2 );

    void writeAndFlush( Message var1, ResponseHandler var2, Message var3, ResponseHandler var4 );

    CompletionStage<Void> reset();

    CompletionStage<Void> release();

    void terminateAndRelease( String var1 );

    BoltServerAddress serverAddress();

    ServerVersion serverVersion();

    BoltProtocol protocol();

    default AccessMode mode()
    {
        throw new UnsupportedOperationException( String.format( "%s does not support access mode.", this.getClass() ) );
    }

    default DatabaseName databaseName()
    {
        throw new UnsupportedOperationException( String.format( "%s does not support database name.", this.getClass() ) );
    }

    void flush();
}
