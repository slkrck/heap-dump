package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.util.Extract;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;
import com.neo4j.fabric.shaded.driver.internal.value.MapValue;
import com.neo4j.fabric.shaded.driver.types.Entity;

import java.util.Map;
import java.util.function.Function;

public abstract class InternalEntity implements Entity, AsValue
{
    private final long id;
    private final Map<String,Value> properties;

    public InternalEntity( long id, Map<String,Value> properties )
    {
        this.id = id;
        this.properties = properties;
    }

    public long id()
    {
        return this.id;
    }

    public int size()
    {
        return this.properties.size();
    }

    public Map<String,Object> asMap()
    {
        return this.asMap( Values.ofObject() );
    }

    public <T> Map<String,T> asMap( Function<Value,T> mapFunction )
    {
        return Extract.map( this.properties, mapFunction );
    }

    public Value asValue()
    {
        return new MapValue( this.properties );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalEntity that = (InternalEntity) o;
            return this.id == that.id;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return (int) (this.id ^ this.id >>> 32);
    }

    public String toString()
    {
        return "Entity{id=" + this.id + ", properties=" + this.properties + '}';
    }

    public boolean containsKey( String key )
    {
        return this.properties.containsKey( key );
    }

    public Iterable<String> keys()
    {
        return this.properties.keySet();
    }

    public Value get( String key )
    {
        Value value = (Value) this.properties.get( key );
        return value == null ? Values.NULL : value;
    }

    public Iterable<Value> values()
    {
        return this.properties.values();
    }

    public <T> Iterable<T> values( Function<Value,T> mapFunction )
    {
        return Iterables.map( this.properties.values(), mapFunction );
    }
}
