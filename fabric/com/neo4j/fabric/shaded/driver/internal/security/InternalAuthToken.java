package com.neo4j.fabric.shaded.driver.internal.security;

import com.neo4j.fabric.shaded.driver.AuthToken;
import com.neo4j.fabric.shaded.driver.Value;

import java.util.Map;

public class InternalAuthToken implements AuthToken
{
    public static final String SCHEME_KEY = "scheme";
    public static final String PRINCIPAL_KEY = "principal";
    public static final String CREDENTIALS_KEY = "credentials";
    public static final String REALM_KEY = "realm";
    public static final String PARAMETERS_KEY = "parameters";
    private final Map<String,Value> content;

    public InternalAuthToken( Map<String,Value> content )
    {
        this.content = content;
    }

    public Map<String,Value> toMap()
    {
        return this.content;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalAuthToken that = (InternalAuthToken) o;
            return this.content != null ? this.content.equals( that.content ) : that.content == null;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.content != null ? this.content.hashCode() : 0;
    }
}
