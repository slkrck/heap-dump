package com.neo4j.fabric.shaded.driver.internal.security;

import javax.net.ssl.SSLContext;

public interface SecurityPlan
{
    boolean requiresEncryption();

    SSLContext sslContext();

    boolean requiresHostnameVerification();
}
