package com.neo4j.fabric.shaded.driver.internal.security;

import com.neo4j.fabric.shaded.driver.internal.util.CertificateTool;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SecurityPlanImpl implements SecurityPlan
{
    private final boolean requiresEncryption;
    private final SSLContext sslContext;
    private final boolean requiresHostnameVerification;

    private SecurityPlanImpl( boolean requiresEncryption, SSLContext sslContext, boolean requiresHostnameVerification )
    {
        this.requiresEncryption = requiresEncryption;
        this.sslContext = sslContext;
        this.requiresHostnameVerification = requiresHostnameVerification;
    }

    public static SecurityPlan forAllCertificates( boolean requiresHostnameVerification ) throws GeneralSecurityException
    {
        SSLContext sslContext = SSLContext.getInstance( "TLS" );
        sslContext.init( new KeyManager[0], new TrustManager[]{new SecurityPlanImpl.TrustAllTrustManager()}, (SecureRandom) null );
        return new SecurityPlanImpl( true, sslContext, requiresHostnameVerification );
    }

    public static SecurityPlan forCustomCASignedCertificates( File certFile, boolean requiresHostnameVerification ) throws GeneralSecurityException, IOException
    {
        KeyStore trustedKeyStore = KeyStore.getInstance( "JKS" );
        trustedKeyStore.load( (InputStream) null, (char[]) null );
        CertificateTool.loadX509Cert( certFile, trustedKeyStore );
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance( "SunX509" );
        trustManagerFactory.init( trustedKeyStore );
        SSLContext sslContext = SSLContext.getInstance( "TLS" );
        sslContext.init( new KeyManager[0], trustManagerFactory.getTrustManagers(), (SecureRandom) null );
        return new SecurityPlanImpl( true, sslContext, requiresHostnameVerification );
    }

    public static SecurityPlan forSystemCASignedCertificates( boolean requiresHostnameVerification ) throws NoSuchAlgorithmException
    {
        return new SecurityPlanImpl( true, SSLContext.getDefault(), requiresHostnameVerification );
    }

    public static SecurityPlan insecure()
    {
        return new SecurityPlanImpl( false, (SSLContext) null, false );
    }

    public boolean requiresEncryption()
    {
        return this.requiresEncryption;
    }

    public SSLContext sslContext()
    {
        return this.sslContext;
    }

    public boolean requiresHostnameVerification()
    {
        return this.requiresHostnameVerification;
    }

    private static class TrustAllTrustManager implements X509TrustManager
    {
        private TrustAllTrustManager()
        {
        }

        public void checkClientTrusted( X509Certificate[] chain, String authType ) throws CertificateException
        {
            throw new CertificateException( "All client connections to this client are forbidden." );
        }

        public void checkServerTrusted( X509Certificate[] chain, String authType ) throws CertificateException
        {
        }

        public X509Certificate[] getAcceptedIssuers()
        {
            return new X509Certificate[0];
        }
    }
}
