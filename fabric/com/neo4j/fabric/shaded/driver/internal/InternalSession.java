package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Result;
import com.neo4j.fabric.shaded.driver.Session;
import com.neo4j.fabric.shaded.driver.Transaction;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.TransactionWork;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.internal.async.NetworkSession;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.Collections;
import java.util.Map;

public class InternalSession extends AbstractQueryRunner implements Session
{
    private final NetworkSession session;

    public InternalSession( NetworkSession session )
    {
        this.session = session;
    }

    public Result run( Query query )
    {
        return this.run( query, TransactionConfig.empty() );
    }

    public Result run( String query, TransactionConfig config )
    {
        return this.run( query, Collections.emptyMap(), config );
    }

    public Result run( String query, Map<String,Object> parameters, TransactionConfig config )
    {
        return this.run( new Query( query, parameters ), config );
    }

    public Result run( Query query, TransactionConfig config )
    {
        ResultCursor cursor = (ResultCursor) Futures.blockingGet( this.session.runAsync( query, config, false ), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while running query in session" );
        } );
        Connection connection = (Connection) Futures.getNow( this.session.connectionAsync() );
        return new InternalResult( connection, cursor );
    }

    public boolean isOpen()
    {
        return this.session.isOpen();
    }

    public void close()
    {
        Futures.blockingGet( this.session.closeAsync(), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while closing the session" );
        } );
    }

    public Transaction beginTransaction()
    {
        return this.beginTransaction( TransactionConfig.empty() );
    }

    public Transaction beginTransaction( TransactionConfig config )
    {
        UnmanagedTransaction tx = (UnmanagedTransaction) Futures.blockingGet( this.session.beginTransactionAsync( config ), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while starting a transaction" );
        } );
        return new InternalTransaction( tx );
    }

    public <T> T readTransaction( TransactionWork<T> work )
    {
        return this.readTransaction( work, TransactionConfig.empty() );
    }

    public <T> T readTransaction( TransactionWork<T> work, TransactionConfig config )
    {
        return this.transaction( AccessMode.READ, work, config );
    }

    public <T> T writeTransaction( TransactionWork<T> work )
    {
        return this.writeTransaction( work, TransactionConfig.empty() );
    }

    public <T> T writeTransaction( TransactionWork<T> work, TransactionConfig config )
    {
        return this.transaction( AccessMode.WRITE, work, config );
    }

    public Bookmark lastBookmark()
    {
        return this.session.lastBookmark();
    }

    public void reset()
    {
        Futures.blockingGet( this.session.resetAsync(), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while resetting the session" );
        } );
    }

    private <T> T transaction( AccessMode mode, TransactionWork<T> work, TransactionConfig config )
    {
        return this.session.retryLogic().retry( () -> {
            Transaction tx = this.beginTransaction( mode, config );
            Throwable var5 = null;

            Object var7;
            try
            {
                T result = work.execute( tx );
                if ( tx.isOpen() )
                {
                    tx.commit();
                }

                var7 = result;
            }
            catch ( Throwable var16 )
            {
                var5 = var16;
                throw var16;
            }
            finally
            {
                if ( tx != null )
                {
                    if ( var5 != null )
                    {
                        try
                        {
                            tx.close();
                        }
                        catch ( Throwable var15 )
                        {
                            var5.addSuppressed( var15 );
                        }
                    }
                    else
                    {
                        tx.close();
                    }
                }
            }

            return var7;
        } );
    }

    private Transaction beginTransaction( AccessMode mode, TransactionConfig config )
    {
        UnmanagedTransaction tx = (UnmanagedTransaction) Futures.blockingGet( this.session.beginTransactionAsync( mode, config ), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while starting a transaction" );
        } );
        return new InternalTransaction( tx );
    }

    private void terminateConnectionOnThreadInterrupt( String reason )
    {
        Connection connection = null;

        try
        {
            connection = (Connection) Futures.getNow( this.session.connectionAsync() );
        }
        catch ( Throwable var4 )
        {
        }

        if ( connection != null )
        {
            connection.terminateAndRelease( reason );
        }
    }
}
