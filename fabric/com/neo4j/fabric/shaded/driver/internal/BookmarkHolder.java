package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Bookmark;

public interface BookmarkHolder
{
    BookmarkHolder NO_OP = new BookmarkHolder()
    {
        public Bookmark getBookmark()
        {
            return InternalBookmark.empty();
        }

        public void setBookmark( Bookmark bookmark )
        {
        }
    };

    Bookmark getBookmark();

    void setBookmark( Bookmark var1 );
}
