package com.neo4j.fabric.shaded.driver.internal;

public interface RoutingErrorHandler
{
    void onConnectionFailure( BoltServerAddress var1 );

    void onWriteFailure( BoltServerAddress var1 );
}
