package com.neo4j.fabric.shaded.driver.internal.messaging.v2;

import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.v1.MessageWriterV1;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackOutput;

public class MessageWriterV2 extends MessageWriterV1
{
    public MessageWriterV2( PackOutput output )
    {
        super( (ValuePacker) (new ValuePackerV2( output )) );
    }
}
