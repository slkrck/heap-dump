package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;

import java.time.Duration;
import java.util.Map;
import java.util.Objects;

public class BeginMessage extends MessageWithMetadata
{
    public static final byte SIGNATURE = 17;

    public BeginMessage( Bookmark bookmark, TransactionConfig config, DatabaseName databaseName, AccessMode mode )
    {
        this( bookmark, config.timeout(), config.metadata(), mode, databaseName );
    }

    public BeginMessage( Bookmark bookmark, Duration txTimeout, Map<String,Value> txMetadata, AccessMode mode, DatabaseName databaseName )
    {
        super( TransactionMetadataBuilder.buildMetadata( txTimeout, txMetadata, databaseName, mode, bookmark ) );
    }

    public byte signature()
    {
        return 17;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            BeginMessage that = (BeginMessage) o;
            return Objects.equals( this.metadata(), that.metadata() );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.metadata()} );
    }

    public String toString()
    {
        return "BEGIN " + this.metadata();
    }
}
