package com.neo4j.fabric.shaded.driver.internal.messaging.v1;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.cursor.AsyncResultCursorOnlyFactory;
import com.neo4j.fabric.shaded.driver.internal.cursor.ResultCursorFactory;
import com.neo4j.fabric.shaded.driver.internal.handlers.BeginTxResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.CommitTxResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.InitResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.NoOpResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullAllResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullHandlers;
import com.neo4j.fabric.shaded.driver.internal.handlers.RollbackTxResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.InitMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.MultiDatabaseUtil;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.PullAllMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.RunMessage;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class BoltProtocolV1 implements BoltProtocol
{
    public static final int VERSION = 1;
    public static final BoltProtocol INSTANCE = new BoltProtocolV1();
    public static final MetadataExtractor METADATA_EXTRACTOR = new MetadataExtractor( "result_available_after", "result_consumed_after" );
    private static final String BEGIN_QUERY = "BEGIN";
    private static final Message BEGIN_MESSAGE = new RunMessage( "BEGIN" );
    private static final Message COMMIT_MESSAGE = new RunMessage( "COMMIT" );
    private static final Message ROLLBACK_MESSAGE = new RunMessage( "ROLLBACK" );

    private static ResultCursorFactory buildResultCursorFactory( Connection connection, Query query, UnmanagedTransaction tx, boolean waitForRunResponse )
    {
        String queryText = query.text();
        Map<String,Value> params = query.parameters().asMap( Values.ofValue() );
        RunMessage runMessage = new RunMessage( queryText, params );
        RunResponseHandler runHandler = new RunResponseHandler( METADATA_EXTRACTOR );
        PullAllResponseHandler pullAllHandler = PullHandlers.newBoltV1PullAllHandler( query, runHandler, connection, tx );
        return new AsyncResultCursorOnlyFactory( connection, runMessage, runHandler, pullAllHandler, waitForRunResponse );
    }

    private static ClientException txConfigNotSupported()
    {
        return new ClientException(
                "Driver is connected to the database that does not support transaction configuration. Please upgrade to neo4j 3.5.0 or later in order to use this functionality" );
    }

    public MessageFormat createMessageFormat()
    {
        return new MessageFormatV1();
    }

    public void initializeChannel( String userAgent, Map<String,Value> authToken, ChannelPromise channelInitializedPromise )
    {
        Channel channel = channelInitializedPromise.channel();
        InitMessage message = new InitMessage( userAgent, authToken );
        InitResponseHandler handler = new InitResponseHandler( channelInitializedPromise );
        ChannelAttributes.messageDispatcher( channel ).enqueue( handler );
        channel.writeAndFlush( message, channel.voidPromise() );
    }

    public void prepareToCloseChannel( Channel channel )
    {
    }

    public CompletionStage<Void> beginTransaction( Connection connection, Bookmark bookmark, TransactionConfig config )
    {
        try
        {
            this.verifyBeforeTransaction( config, connection.databaseName() );
        }
        catch ( Exception var5 )
        {
            return Futures.failedFuture( var5 );
        }

        if ( bookmark.isEmpty() )
        {
            connection.write( BEGIN_MESSAGE, NoOpResponseHandler.INSTANCE, PullAllMessage.PULL_ALL, NoOpResponseHandler.INSTANCE );
            return Futures.completedWithNull();
        }
        else
        {
            CompletableFuture<Void> beginTxFuture = new CompletableFuture();
            connection.writeAndFlush( new RunMessage( "BEGIN", BoltProtocolV1.SingleBookmarkHelper.asBeginTransactionParameters( bookmark ) ),
                    NoOpResponseHandler.INSTANCE, PullAllMessage.PULL_ALL, new BeginTxResponseHandler( beginTxFuture ) );
            return beginTxFuture;
        }
    }

    public CompletionStage<Bookmark> commitTransaction( Connection connection )
    {
        CompletableFuture<Bookmark> commitFuture = new CompletableFuture();
        ResponseHandler pullAllHandler = new CommitTxResponseHandler( commitFuture );
        connection.writeAndFlush( COMMIT_MESSAGE, NoOpResponseHandler.INSTANCE, PullAllMessage.PULL_ALL, pullAllHandler );
        return commitFuture;
    }

    public CompletionStage<Void> rollbackTransaction( Connection connection )
    {
        CompletableFuture<Void> rollbackFuture = new CompletableFuture();
        ResponseHandler pullAllHandler = new RollbackTxResponseHandler( rollbackFuture );
        connection.writeAndFlush( ROLLBACK_MESSAGE, NoOpResponseHandler.INSTANCE, PullAllMessage.PULL_ALL, pullAllHandler );
        return rollbackFuture;
    }

    public ResultCursorFactory runInAutoCommitTransaction( Connection connection, Query query, BookmarkHolder bookmarkHolder, TransactionConfig config,
            boolean waitForRunResponse, long ignored )
    {
        this.verifyBeforeTransaction( config, connection.databaseName() );
        return buildResultCursorFactory( connection, query, (UnmanagedTransaction) null, waitForRunResponse );
    }

    public ResultCursorFactory runInUnmanagedTransaction( Connection connection, Query query, UnmanagedTransaction tx, boolean waitForRunResponse,
            long ignored )
    {
        return buildResultCursorFactory( connection, query, tx, waitForRunResponse );
    }

    public int version()
    {
        return 1;
    }

    private void verifyBeforeTransaction( TransactionConfig config, DatabaseName databaseName )
    {
        if ( config != null && !config.isEmpty() )
        {
            throw txConfigNotSupported();
        }
        else
        {
            MultiDatabaseUtil.assertEmptyDatabaseName( databaseName, this.version() );
        }
    }

    static class SingleBookmarkHelper
    {
        private static final String BOOKMARK_PREFIX = "neo4j:bookmark:v1:tx";
        private static final long UNKNOWN_BOOKMARK_VALUE = -1L;

        static Map<String,Value> asBeginTransactionParameters( Bookmark bookmark )
        {
            if ( bookmark.isEmpty() )
            {
                return Collections.emptyMap();
            }
            else
            {
                Map<String,Value> parameters = Iterables.newHashMapWithSize( 1 );
                parameters.put( "bookmark", Values.value( maxBookmark( bookmark.values() ) ) );
                parameters.put( "bookmarks", Values.value( (Object) bookmark.values() ) );
                return parameters;
            }
        }

        private static String maxBookmark( Iterable<String> bookmarks )
        {
            if ( bookmarks == null )
            {
                return null;
            }
            else
            {
                Iterator<String> iterator = bookmarks.iterator();
                if ( !iterator.hasNext() )
                {
                    return null;
                }
                else
                {
                    String maxBookmark = (String) iterator.next();
                    long maxValue = bookmarkValue( maxBookmark );

                    while ( iterator.hasNext() )
                    {
                        String bookmark = (String) iterator.next();
                        long value = bookmarkValue( bookmark );
                        if ( value > maxValue )
                        {
                            maxBookmark = bookmark;
                            maxValue = value;
                        }
                    }

                    return maxBookmark;
                }
            }
        }

        private static long bookmarkValue( String value )
        {
            if ( value != null && value.startsWith( "neo4j:bookmark:v1:tx" ) )
            {
                try
                {
                    return Long.parseLong( value.substring( "neo4j:bookmark:v1:tx".length() ) );
                }
                catch ( NumberFormatException var2 )
                {
                    return -1L;
                }
            }
            else
            {
                return -1L;
            }
        }
    }
}
