package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.DatabaseNameUtil;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;

public class TransactionMetadataBuilder
{
    private static final String BOOKMARKS_METADATA_KEY = "bookmarks";
    private static final String DATABASE_NAME_KEY = "db";
    private static final String TX_TIMEOUT_METADATA_KEY = "tx_timeout";
    private static final String TX_METADATA_METADATA_KEY = "tx_metadata";
    private static final String MODE_KEY = "mode";
    private static final String MODE_READ_VALUE = "r";

    public static Map<String,Value> buildMetadata( Duration txTimeout, Map<String,Value> txMetadata, AccessMode mode, Bookmark bookmark )
    {
        return buildMetadata( txTimeout, txMetadata, DatabaseNameUtil.defaultDatabase(), mode, bookmark );
    }

    public static Map<String,Value> buildMetadata( Duration txTimeout, Map<String,Value> txMetadata, DatabaseName databaseName, AccessMode mode,
            Bookmark bookmark )
    {
        boolean bookmarksPresent = bookmark != null && !bookmark.isEmpty();
        boolean txTimeoutPresent = txTimeout != null;
        boolean txMetadataPresent = txMetadata != null && !txMetadata.isEmpty();
        boolean accessModePresent = mode == AccessMode.READ;
        boolean databaseNamePresent = databaseName.databaseName().isPresent();
        if ( !bookmarksPresent && !txTimeoutPresent && !txMetadataPresent && !accessModePresent && !databaseNamePresent )
        {
            return Collections.emptyMap();
        }
        else
        {
            Map<String,Value> result = Iterables.newHashMapWithSize( 5 );
            if ( bookmarksPresent )
            {
                result.put( "bookmarks", Values.value( (Object) bookmark.values() ) );
            }

            if ( txTimeoutPresent )
            {
                result.put( "tx_timeout", Values.value( txTimeout.toMillis() ) );
            }

            if ( txMetadataPresent )
            {
                result.put( "tx_metadata", Values.value( (Object) txMetadata ) );
            }

            if ( accessModePresent )
            {
                result.put( "mode", Values.value( "r" ) );
            }

            databaseName.databaseName().ifPresent( ( name ) -> {
                Value var10000 = (Value) result.put( "db", Values.value( name ) );
            } );
            return result;
        }
    }
}
