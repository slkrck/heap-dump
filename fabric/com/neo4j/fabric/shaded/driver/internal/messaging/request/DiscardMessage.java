package com.neo4j.fabric.shaded.driver.internal.messaging.request;

public class DiscardMessage extends AbstractStreamingMessage
{
    public static final byte SIGNATURE = 47;

    public DiscardMessage( long n, long id )
    {
        super( n, id );
    }

    public static DiscardMessage newDiscardAllMessage( long id )
    {
        return new DiscardMessage( -1L, id );
    }

    protected String name()
    {
        return "DISCARD";
    }

    public byte signature()
    {
        return 47;
    }
}
