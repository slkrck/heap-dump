package com.neo4j.fabric.shaded.driver.internal.messaging.v2;

import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.messaging.v1.BoltProtocolV1;

public class BoltProtocolV2 extends BoltProtocolV1
{
    public static final int VERSION = 2;
    public static final BoltProtocol INSTANCE = new BoltProtocolV2();

    public MessageFormat createMessageFormat()
    {
        return new MessageFormatV2();
    }

    public int version()
    {
        return 2;
    }
}
