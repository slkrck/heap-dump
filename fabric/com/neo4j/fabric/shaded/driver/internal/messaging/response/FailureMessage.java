package com.neo4j.fabric.shaded.driver.internal.messaging.response;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class FailureMessage implements Message
{
    public static final byte SIGNATURE = 127;
    private final String code;
    private final String message;

    public FailureMessage( String code, String message )
    {
        this.code = code;
        this.message = message;
    }

    public String code()
    {
        return this.code;
    }

    public String message()
    {
        return this.message;
    }

    public byte signature()
    {
        return 127;
    }

    public String toString()
    {
        return String.format( "FAILURE %s \"%s\"", this.code, this.message );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            boolean var10000;
            label44:
            {
                label30:
                {
                    FailureMessage that = (FailureMessage) o;
                    if ( this.code != null )
                    {
                        if ( !this.code.equals( that.code ) )
                        {
                            break label30;
                        }
                    }
                    else if ( that.code != null )
                    {
                        break label30;
                    }

                    if ( this.message != null )
                    {
                        if ( this.message.equals( that.message ) )
                        {
                            break label44;
                        }
                    }
                    else if ( that.message == null )
                    {
                        break label44;
                    }
                }

                var10000 = false;
                return var10000;
            }

            var10000 = true;
            return var10000;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.code != null ? this.code.hashCode() : 0;
        result = 31 * result + (this.message != null ? this.message.hashCode() : 0);
        return result;
    }
}
