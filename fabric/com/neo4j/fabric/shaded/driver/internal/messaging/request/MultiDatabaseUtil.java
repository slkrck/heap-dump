package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

public final class MultiDatabaseUtil
{
    public static void assertEmptyDatabaseName( DatabaseName databaseName, int boltVersion )
    {
        if ( databaseName.databaseName().isPresent() )
        {
            throw new ClientException(
                    String.format( "Database name parameter for selecting database is not supported in Bolt Protocol Version %s. Database name: '%s'",
                            boltVersion, databaseName.description() ) );
        }
    }

    public static boolean supportsMultiDatabase( Connection connection )
    {
        return connection.serverVersion().greaterThanOrEqual( ServerVersion.v4_0_0 ) && connection.protocol().version() >= 4;
    }
}
