package com.neo4j.fabric.shaded.driver.internal.messaging;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

public abstract class AbstractMessageWriter implements MessageFormat.Writer
{
    private final ValuePacker packer;
    private final Map<Byte,MessageEncoder> encodersByMessageSignature;

    protected AbstractMessageWriter( ValuePacker packer, Map<Byte,MessageEncoder> encodersByMessageSignature )
    {
        this.packer = (ValuePacker) Objects.requireNonNull( packer );
        this.encodersByMessageSignature = (Map) Objects.requireNonNull( encodersByMessageSignature );
    }

    public final void write( Message msg ) throws IOException
    {
        byte signature = msg.signature();
        MessageEncoder encoder = (MessageEncoder) this.encodersByMessageSignature.get( signature );
        if ( encoder == null )
        {
            throw new IOException( "No encoder found for message " + msg + " with signature " + signature );
        }
        else
        {
            encoder.encode( msg, this.packer );
        }
    }
}
