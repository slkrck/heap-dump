package com.neo4j.fabric.shaded.driver.internal.messaging.encode;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.HelloMessage;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;

import java.io.IOException;

public class HelloMessageEncoder implements MessageEncoder
{
    public void encode( Message message, ValuePacker packer ) throws IOException
    {
        Preconditions.checkArgument( message, HelloMessage.class );
        HelloMessage helloMessage = (HelloMessage) message;
        packer.packStructHeader( 1, helloMessage.signature() );
        packer.pack( helloMessage.metadata() );
    }
}
