package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

import java.util.Map;

abstract class MessageWithMetadata implements Message
{
    private final Map<String,Value> metadata;

    public MessageWithMetadata( Map<String,Value> metadata )
    {
        this.metadata = metadata;
    }

    public Map<String,Value> metadata()
    {
        return this.metadata;
    }
}
