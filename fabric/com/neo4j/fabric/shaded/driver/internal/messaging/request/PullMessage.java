package com.neo4j.fabric.shaded.driver.internal.messaging.request;

public class PullMessage extends AbstractStreamingMessage
{
    public static final byte SIGNATURE = 63;
    public static final PullMessage PULL_ALL = new PullMessage( -1L, -1L );

    public PullMessage( long n, long id )
    {
        super( n, id );
    }

    protected String name()
    {
        return "PULL";
    }

    public byte signature()
    {
        return 63;
    }
}
