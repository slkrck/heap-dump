package com.neo4j.fabric.shaded.driver.internal.messaging.response;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

import java.util.Arrays;

public class RecordMessage implements Message
{
    public static final byte SIGNATURE = 113;
    private final Value[] fields;

    public RecordMessage( Value[] fields )
    {
        this.fields = fields;
    }

    public Value[] fields()
    {
        return this.fields;
    }

    public byte signature()
    {
        return 113;
    }

    public String toString()
    {
        return "RECORD " + Arrays.toString( this.fields );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            RecordMessage that = (RecordMessage) o;
            return Arrays.equals( this.fields, that.fields );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Arrays.hashCode( this.fields );
    }
}
