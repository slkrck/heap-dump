package com.neo4j.fabric.shaded.driver.internal.messaging.v4;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.cursor.ResultCursorFactory;
import com.neo4j.fabric.shaded.driver.internal.cursor.ResultCursorFactoryImpl;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullAllResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullHandlers;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.PullResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.RunWithMetadataMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.v3.BoltProtocolV3;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;

public class BoltProtocolV4 extends BoltProtocolV3
{
    public static final int VERSION = 4;
    public static final BoltProtocol INSTANCE = new BoltProtocolV4();

    public MessageFormat createMessageFormat()
    {
        return new MessageFormatV4();
    }

    protected ResultCursorFactory buildResultCursorFactory( Connection connection, Query query, BookmarkHolder bookmarkHolder, UnmanagedTransaction tx,
            RunWithMetadataMessage runMessage, boolean waitForRunResponse, long fetchSize )
    {
        RunResponseHandler runHandler = new RunResponseHandler( METADATA_EXTRACTOR );
        PullAllResponseHandler pullAllHandler = PullHandlers.newBoltV4AutoPullHandler( query, runHandler, connection, bookmarkHolder, tx, fetchSize );
        PullResponseHandler pullHandler = PullHandlers.newBoltV4BasicPullHandler( query, runHandler, connection, bookmarkHolder, tx );
        return new ResultCursorFactoryImpl( connection, runMessage, runHandler, pullHandler, pullAllHandler, waitForRunResponse );
    }

    protected void verifyDatabaseNameBeforeTransaction( DatabaseName databaseName )
    {
    }

    public int version()
    {
        return 4;
    }
}
