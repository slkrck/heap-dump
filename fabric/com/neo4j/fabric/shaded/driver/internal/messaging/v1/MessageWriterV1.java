package com.neo4j.fabric.shaded.driver.internal.messaging.v1;

import com.neo4j.fabric.shaded.driver.internal.messaging.AbstractMessageWriter;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.DiscardAllMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.InitMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.PullAllMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.ResetMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.RunMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackOutput;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;

import java.util.Map;

public class MessageWriterV1 extends AbstractMessageWriter
{
    public MessageWriterV1( PackOutput output )
    {
        this( (ValuePacker) (new ValuePackerV1( output )) );
    }

    protected MessageWriterV1( ValuePacker packer )
    {
        super( packer, buildEncoders() );
    }

    private static Map<Byte,MessageEncoder> buildEncoders()
    {
        Map<Byte,MessageEncoder> result = Iterables.newHashMapWithSize( 6 );
        result.put( (byte) 47, new DiscardAllMessageEncoder() );
        result.put( (byte) 1, new InitMessageEncoder() );
        result.put( (byte) 63, new PullAllMessageEncoder() );
        result.put( (byte) 15, new ResetMessageEncoder() );
        result.put( (byte) 16, new RunMessageEncoder() );
        return result;
    }
}
