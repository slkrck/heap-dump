package com.neo4j.fabric.shaded.driver.internal.messaging;

import com.neo4j.fabric.shaded.driver.Value;

import java.io.IOException;
import java.util.Map;

public interface ValuePacker
{
    void packStructHeader( int var1, byte var2 ) throws IOException;

    void pack( String var1 ) throws IOException;

    void pack( Value var1 ) throws IOException;

    void pack( Map<String,Value> var1 ) throws IOException;
}
