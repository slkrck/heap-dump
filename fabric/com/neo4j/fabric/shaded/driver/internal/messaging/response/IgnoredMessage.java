package com.neo4j.fabric.shaded.driver.internal.messaging.response;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class IgnoredMessage implements Message
{
    public static final byte SIGNATURE = 126;
    public static final IgnoredMessage IGNORED = new IgnoredMessage();

    private IgnoredMessage()
    {
    }

    public byte signature()
    {
        return 126;
    }

    public String toString()
    {
        return "IGNORED {}";
    }

    public boolean equals( Object obj )
    {
        return obj != null && obj.getClass() == this.getClass();
    }

    public int hashCode()
    {
        return 1;
    }
}
