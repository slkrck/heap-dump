package com.neo4j.fabric.shaded.driver.internal.messaging.v3;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.cursor.AsyncResultCursorOnlyFactory;
import com.neo4j.fabric.shaded.driver.internal.cursor.ResultCursorFactory;
import com.neo4j.fabric.shaded.driver.internal.handlers.BeginTxResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.CommitTxResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.HelloResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.NoOpResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullAllResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullHandlers;
import com.neo4j.fabric.shaded.driver.internal.handlers.RollbackTxResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.BeginMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.CommitMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.GoodbyeMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.HelloMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.MultiDatabaseUtil;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.RollbackMessage;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.RunWithMetadataMessage;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.internal.util.MetadataExtractor;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class BoltProtocolV3 implements BoltProtocol
{
    public static final int VERSION = 3;
    public static final BoltProtocol INSTANCE = new BoltProtocolV3();
    public static final MetadataExtractor METADATA_EXTRACTOR = new MetadataExtractor( "t_first", "t_last" );

    public MessageFormat createMessageFormat()
    {
        return new MessageFormatV3();
    }

    public void initializeChannel( String userAgent, Map<String,Value> authToken, ChannelPromise channelInitializedPromise )
    {
        Channel channel = channelInitializedPromise.channel();
        HelloMessage message = new HelloMessage( userAgent, authToken );
        HelloResponseHandler handler = new HelloResponseHandler( channelInitializedPromise );
        ChannelAttributes.messageDispatcher( channel ).enqueue( handler );
        channel.writeAndFlush( message, channel.voidPromise() );
    }

    public void prepareToCloseChannel( Channel channel )
    {
        GoodbyeMessage message = GoodbyeMessage.GOODBYE;
        ChannelAttributes.messageDispatcher( channel ).enqueue( NoOpResponseHandler.INSTANCE );
        channel.writeAndFlush( message, channel.voidPromise() );
    }

    public CompletionStage<Void> beginTransaction( Connection connection, Bookmark bookmark, TransactionConfig config )
    {
        try
        {
            this.verifyDatabaseNameBeforeTransaction( connection.databaseName() );
        }
        catch ( Exception var6 )
        {
            return Futures.failedFuture( var6 );
        }

        BeginMessage beginMessage = new BeginMessage( bookmark, config, connection.databaseName(), connection.mode() );
        if ( bookmark.isEmpty() )
        {
            connection.write( beginMessage, NoOpResponseHandler.INSTANCE );
            return Futures.completedWithNull();
        }
        else
        {
            CompletableFuture<Void> beginTxFuture = new CompletableFuture();
            connection.writeAndFlush( beginMessage, new BeginTxResponseHandler( beginTxFuture ) );
            return beginTxFuture;
        }
    }

    public CompletionStage<Bookmark> commitTransaction( Connection connection )
    {
        CompletableFuture<Bookmark> commitFuture = new CompletableFuture();
        connection.writeAndFlush( CommitMessage.COMMIT, new CommitTxResponseHandler( commitFuture ) );
        return commitFuture;
    }

    public CompletionStage<Void> rollbackTransaction( Connection connection )
    {
        CompletableFuture<Void> rollbackFuture = new CompletableFuture();
        connection.writeAndFlush( RollbackMessage.ROLLBACK, new RollbackTxResponseHandler( rollbackFuture ) );
        return rollbackFuture;
    }

    public ResultCursorFactory runInAutoCommitTransaction( Connection connection, Query query, BookmarkHolder bookmarkHolder, TransactionConfig config,
            boolean waitForRunResponse, long fetchSize )
    {
        this.verifyDatabaseNameBeforeTransaction( connection.databaseName() );
        RunWithMetadataMessage runMessage =
                RunWithMetadataMessage.autoCommitTxRunMessage( query, config, connection.databaseName(), connection.mode(), bookmarkHolder.getBookmark() );
        return this.buildResultCursorFactory( connection, query, bookmarkHolder, (UnmanagedTransaction) null, runMessage, waitForRunResponse, fetchSize );
    }

    public ResultCursorFactory runInUnmanagedTransaction( Connection connection, Query query, UnmanagedTransaction tx, boolean waitForRunResponse,
            long fetchSize )
    {
        RunWithMetadataMessage runMessage = RunWithMetadataMessage.unmanagedTxRunMessage( query );
        return this.buildResultCursorFactory( connection, query, BookmarkHolder.NO_OP, tx, runMessage, waitForRunResponse, fetchSize );
    }

    protected ResultCursorFactory buildResultCursorFactory( Connection connection, Query query, BookmarkHolder bookmarkHolder, UnmanagedTransaction tx,
            RunWithMetadataMessage runMessage, boolean waitForRunResponse, long ignored )
    {
        RunResponseHandler runHandler = new RunResponseHandler( METADATA_EXTRACTOR );
        PullAllResponseHandler pullHandler = PullHandlers.newBoltV3PullAllHandler( query, runHandler, connection, bookmarkHolder, tx );
        return new AsyncResultCursorOnlyFactory( connection, runMessage, runHandler, pullHandler, waitForRunResponse );
    }

    protected void verifyDatabaseNameBeforeTransaction( DatabaseName databaseName )
    {
        MultiDatabaseUtil.assertEmptyDatabaseName( databaseName, this.version() );
    }

    public int version()
    {
        return 3;
    }
}
