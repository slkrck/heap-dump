package com.neo4j.fabric.shaded.driver.internal.messaging.encode;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.BeginMessage;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;

import java.io.IOException;

public class BeginMessageEncoder implements MessageEncoder
{
    public void encode( Message message, ValuePacker packer ) throws IOException
    {
        Preconditions.checkArgument( message, BeginMessage.class );
        BeginMessage beginMessage = (BeginMessage) message;
        packer.packStructHeader( 1, beginMessage.signature() );
        packer.pack( beginMessage.metadata() );
    }
}
