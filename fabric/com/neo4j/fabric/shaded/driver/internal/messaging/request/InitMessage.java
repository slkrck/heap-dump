package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

import java.util.Map;

public class InitMessage implements Message
{
    public static final byte SIGNATURE = 1;
    private final String userAgent;
    private Map<String,Value> authToken;

    public InitMessage( String userAgent, Map<String,Value> authToken )
    {
        this.userAgent = userAgent;
        this.authToken = authToken;
    }

    public String userAgent()
    {
        return this.userAgent;
    }

    public Map<String,Value> authToken()
    {
        return this.authToken;
    }

    public byte signature()
    {
        return 1;
    }

    public String toString()
    {
        return String.format( "INIT \"%s\" {...}", this.userAgent );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            boolean var10000;
            label35:
            {
                InitMessage that = (InitMessage) o;
                if ( this.userAgent != null )
                {
                    if ( this.userAgent.equals( that.userAgent ) )
                    {
                        break label35;
                    }
                }
                else if ( that.userAgent == null )
                {
                    break label35;
                }

                var10000 = false;
                return var10000;
            }

            var10000 = true;
            return var10000;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.userAgent != null ? this.userAgent.hashCode() : 0;
    }
}
