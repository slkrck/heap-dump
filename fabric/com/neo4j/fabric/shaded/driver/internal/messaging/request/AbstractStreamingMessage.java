package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public abstract class AbstractStreamingMessage implements Message
{
    public static final long STREAM_LIMIT_UNLIMITED = -1L;
    private final Map<String,Value> metadata = new HashMap();

    AbstractStreamingMessage( long n, long id )
    {
        this.metadata.put( "n", Values.value( n ) );
        if ( id != -1L )
        {
            this.metadata.put( "qid", Values.value( id ) );
        }
    }

    public Map<String,Value> metadata()
    {
        return this.metadata;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            AbstractStreamingMessage that = (AbstractStreamingMessage) o;
            return Objects.equals( this.metadata, that.metadata );
        }
        else
        {
            return false;
        }
    }

    protected abstract String name();

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.metadata} );
    }

    public String toString()
    {
        return String.format( "%s %s", this.name(), this.metadata );
    }
}
