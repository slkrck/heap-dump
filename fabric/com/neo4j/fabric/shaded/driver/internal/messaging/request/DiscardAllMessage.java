package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class DiscardAllMessage implements Message
{
    public static final byte SIGNATURE = 47;
    public static final DiscardAllMessage DISCARD_ALL = new DiscardAllMessage();

    private DiscardAllMessage()
    {
    }

    public byte signature()
    {
        return 47;
    }

    public String toString()
    {
        return "DISCARD_ALL";
    }
}
