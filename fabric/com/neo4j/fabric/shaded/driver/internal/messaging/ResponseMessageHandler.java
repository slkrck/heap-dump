package com.neo4j.fabric.shaded.driver.internal.messaging;

import com.neo4j.fabric.shaded.driver.Value;

import java.io.IOException;
import java.util.Map;

public interface ResponseMessageHandler
{
    void handleSuccessMessage( Map<String,Value> var1 ) throws IOException;

    void handleRecordMessage( Value[] var1 ) throws IOException;

    void handleFailureMessage( String var1, String var2 ) throws IOException;

    void handleIgnoredMessage() throws IOException;
}
