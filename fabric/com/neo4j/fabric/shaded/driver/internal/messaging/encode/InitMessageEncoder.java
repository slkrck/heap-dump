package com.neo4j.fabric.shaded.driver.internal.messaging.encode;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.InitMessage;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;

import java.io.IOException;

public class InitMessageEncoder implements MessageEncoder
{
    public void encode( Message message, ValuePacker packer ) throws IOException
    {
        Preconditions.checkArgument( message, InitMessage.class );
        InitMessage initMessage = (InitMessage) message;
        packer.packStructHeader( 2, initMessage.signature() );
        packer.pack( initMessage.userAgent() );
        packer.pack( initMessage.authToken() );
    }
}
