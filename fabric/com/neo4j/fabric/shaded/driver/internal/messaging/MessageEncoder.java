package com.neo4j.fabric.shaded.driver.internal.messaging;

import java.io.IOException;

public interface MessageEncoder
{
    void encode( Message var1, ValuePacker var2 ) throws IOException;
}
