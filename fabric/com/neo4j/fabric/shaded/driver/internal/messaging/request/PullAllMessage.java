package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class PullAllMessage implements Message
{
    public static final byte SIGNATURE = 63;
    public static final PullAllMessage PULL_ALL = new PullAllMessage();

    private PullAllMessage()
    {
    }

    public byte signature()
    {
        return 63;
    }

    public String toString()
    {
        return "PULL_ALL";
    }
}
