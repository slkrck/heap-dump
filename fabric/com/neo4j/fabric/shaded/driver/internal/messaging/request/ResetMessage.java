package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class ResetMessage implements Message
{
    public static final byte SIGNATURE = 15;
    public static final ResetMessage RESET = new ResetMessage();

    private ResetMessage()
    {
    }

    public byte signature()
    {
        return 15;
    }

    public String toString()
    {
        return "RESET";
    }
}
