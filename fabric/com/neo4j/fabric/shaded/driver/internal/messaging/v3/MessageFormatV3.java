package com.neo4j.fabric.shaded.driver.internal.messaging.v3;

import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.messaging.v2.MessageReaderV2;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackInput;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackOutput;

public class MessageFormatV3 implements MessageFormat
{
    public MessageFormat.Writer newWriter( PackOutput output )
    {
        return new MessageWriterV3( output );
    }

    public MessageFormat.Reader newReader( PackInput input )
    {
        return new MessageReaderV2( input );
    }
}
