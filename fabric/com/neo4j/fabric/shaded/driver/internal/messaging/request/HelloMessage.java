package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HelloMessage extends MessageWithMetadata
{
    public static final byte SIGNATURE = 1;
    private static final String USER_AGENT_METADATA_KEY = "user_agent";

    public HelloMessage( String userAgent, Map<String,Value> authToken )
    {
        super( buildMetadata( userAgent, authToken ) );
    }

    private static Map<String,Value> buildMetadata( String userAgent, Map<String,Value> authToken )
    {
        Map<String,Value> result = new HashMap( authToken );
        result.put( "user_agent", Values.value( userAgent ) );
        return result;
    }

    public byte signature()
    {
        return 1;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            HelloMessage that = (HelloMessage) o;
            return Objects.equals( this.metadata(), that.metadata() );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.metadata()} );
    }

    public String toString()
    {
        Map<String,Value> metadataCopy = new HashMap( this.metadata() );
        metadataCopy.replace( "credentials", Values.value( "******" ) );
        return "HELLO " + metadataCopy;
    }
}
