package com.neo4j.fabric.shaded.driver.internal.messaging.encode;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.GoodbyeMessage;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;

import java.io.IOException;

public class GoodbyeMessageEncoder implements MessageEncoder
{
    public void encode( Message message, ValuePacker packer ) throws IOException
    {
        Preconditions.checkArgument( message, GoodbyeMessage.class );
        packer.packStructHeader( 0, (byte) 2 );
    }
}
