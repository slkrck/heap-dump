package com.neo4j.fabric.shaded.driver.internal.messaging.v2;

import com.neo4j.fabric.shaded.driver.internal.messaging.ValueUnpacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.v1.MessageReaderV1;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackInput;

public class MessageReaderV2 extends MessageReaderV1
{
    public MessageReaderV2( PackInput input )
    {
        super( (ValueUnpacker) (new ValueUnpackerV2( input )) );
    }
}
