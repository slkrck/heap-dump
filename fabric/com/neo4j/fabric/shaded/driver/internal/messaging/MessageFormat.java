package com.neo4j.fabric.shaded.driver.internal.messaging;

import com.neo4j.fabric.shaded.driver.internal.packstream.PackInput;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackOutput;

import java.io.IOException;

public interface MessageFormat
{
    MessageFormat.Writer newWriter( PackOutput var1 );

    MessageFormat.Reader newReader( PackInput var1 );

    public interface Reader
    {
        void read( ResponseMessageHandler var1 ) throws IOException;
    }

    public interface Writer
    {
        void write( Message var1 ) throws IOException;
    }
}
