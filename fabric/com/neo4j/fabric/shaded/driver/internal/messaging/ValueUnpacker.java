package com.neo4j.fabric.shaded.driver.internal.messaging;

import com.neo4j.fabric.shaded.driver.Value;

import java.io.IOException;
import java.util.Map;

public interface ValueUnpacker
{
    long unpackStructHeader() throws IOException;

    int unpackStructSignature() throws IOException;

    Map<String,Value> unpackMap() throws IOException;

    Value[] unpackArray() throws IOException;
}
