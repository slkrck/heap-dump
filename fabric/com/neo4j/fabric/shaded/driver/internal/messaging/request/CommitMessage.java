package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class CommitMessage implements Message
{
    public static final byte SIGNATURE = 18;
    public static final Message COMMIT = new CommitMessage();

    private CommitMessage()
    {
    }

    public byte signature()
    {
        return 18;
    }

    public String toString()
    {
        return "COMMIT";
    }
}
