package com.neo4j.fabric.shaded.driver.internal.messaging.encode;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.DiscardMessage;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;

import java.io.IOException;

public class DiscardMessageEncoder implements MessageEncoder
{
    public void encode( Message message, ValuePacker packer ) throws IOException
    {
        Preconditions.checkArgument( message, DiscardMessage.class );
        packer.packStructHeader( 1, (byte) 47 );
        packer.pack( ((DiscardMessage) message).metadata() );
    }
}
