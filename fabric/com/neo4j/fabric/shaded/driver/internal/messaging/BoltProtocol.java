package com.neo4j.fabric.shaded.driver.internal.messaging;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.cursor.ResultCursorFactory;
import com.neo4j.fabric.shaded.driver.internal.messaging.v1.BoltProtocolV1;
import com.neo4j.fabric.shaded.driver.internal.messaging.v2.BoltProtocolV2;
import com.neo4j.fabric.shaded.driver.internal.messaging.v3.BoltProtocolV3;
import com.neo4j.fabric.shaded.driver.internal.messaging.v4.BoltProtocolV4;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;

import java.util.Map;
import java.util.concurrent.CompletionStage;

public interface BoltProtocol
{
    static BoltProtocol forChannel( Channel channel )
    {
        return forVersion( ChannelAttributes.protocolVersion( channel ) );
    }

    static BoltProtocol forVersion( int version )
    {
        switch ( version )
        {
        case 1:
            return BoltProtocolV1.INSTANCE;
        case 2:
            return BoltProtocolV2.INSTANCE;
        case 3:
            return BoltProtocolV3.INSTANCE;
        case 4:
            return BoltProtocolV4.INSTANCE;
        default:
            throw new ClientException( "Unknown protocol version: " + version );
        }
    }

    MessageFormat createMessageFormat();

    void initializeChannel( String var1, Map<String,Value> var2, ChannelPromise var3 );

    void prepareToCloseChannel( Channel var1 );

    CompletionStage<Void> beginTransaction( Connection var1, Bookmark var2, TransactionConfig var3 );

    CompletionStage<Bookmark> commitTransaction( Connection var1 );

    CompletionStage<Void> rollbackTransaction( Connection var1 );

    ResultCursorFactory runInAutoCommitTransaction( Connection var1, Query var2, BookmarkHolder var3, TransactionConfig var4, boolean var5, long var6 );

    ResultCursorFactory runInUnmanagedTransaction( Connection var1, Query var2, UnmanagedTransaction var3, boolean var4, long var5 );

    int version();
}
