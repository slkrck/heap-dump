package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class RunWithMetadataMessage extends MessageWithMetadata
{
    public static final byte SIGNATURE = 16;
    private final String query;
    private final Map<String,Value> parameters;

    private RunWithMetadataMessage( String query, Map<String,Value> parameters, Map<String,Value> metadata )
    {
        super( metadata );
        this.query = query;
        this.parameters = parameters;
    }

    public static RunWithMetadataMessage autoCommitTxRunMessage( Query query, TransactionConfig config, DatabaseName databaseName, AccessMode mode,
            Bookmark bookmark )
    {
        return autoCommitTxRunMessage( query, config.timeout(), config.metadata(), databaseName, mode, bookmark );
    }

    public static RunWithMetadataMessage autoCommitTxRunMessage( Query query, Duration txTimeout, Map<String,Value> txMetadata, DatabaseName databaseName,
            AccessMode mode, Bookmark bookmark )
    {
        Map<String,Value> metadata = TransactionMetadataBuilder.buildMetadata( txTimeout, txMetadata, databaseName, mode, bookmark );
        return new RunWithMetadataMessage( query.text(), query.parameters().asMap( Values.ofValue() ), metadata );
    }

    public static RunWithMetadataMessage unmanagedTxRunMessage( Query query )
    {
        return new RunWithMetadataMessage( query.text(), query.parameters().asMap( Values.ofValue() ), Collections.emptyMap() );
    }

    public String query()
    {
        return this.query;
    }

    public Map<String,Value> parameters()
    {
        return this.parameters;
    }

    public byte signature()
    {
        return 16;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            RunWithMetadataMessage that = (RunWithMetadataMessage) o;
            return Objects.equals( this.query, that.query ) && Objects.equals( this.parameters, that.parameters ) &&
                    Objects.equals( this.metadata(), that.metadata() );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.query, this.parameters, this.metadata()} );
    }

    public String toString()
    {
        return "RUN \"" + this.query + "\" " + this.parameters + " " + this.metadata();
    }
}
