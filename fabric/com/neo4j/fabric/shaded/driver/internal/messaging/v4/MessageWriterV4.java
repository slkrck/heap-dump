package com.neo4j.fabric.shaded.driver.internal.messaging.v4;

import com.neo4j.fabric.shaded.driver.internal.messaging.AbstractMessageWriter;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.BeginMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.CommitMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.DiscardMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.GoodbyeMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.HelloMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.PullMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.ResetMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.RollbackMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.encode.RunWithMetadataMessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.v2.ValuePackerV2;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackOutput;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;

import java.util.Map;

public class MessageWriterV4 extends AbstractMessageWriter
{
    public MessageWriterV4( PackOutput output )
    {
        super( new ValuePackerV2( output ), buildEncoders() );
    }

    private static Map<Byte,MessageEncoder> buildEncoders()
    {
        Map<Byte,MessageEncoder> result = Iterables.newHashMapWithSize( 9 );
        result.put( (byte) 1, new HelloMessageEncoder() );
        result.put( (byte) 2, new GoodbyeMessageEncoder() );
        result.put( (byte) 16, new RunWithMetadataMessageEncoder() );
        result.put( (byte) 47, new DiscardMessageEncoder() );
        result.put( (byte) 63, new PullMessageEncoder() );
        result.put( (byte) 17, new BeginMessageEncoder() );
        result.put( (byte) 18, new CommitMessageEncoder() );
        result.put( (byte) 19, new RollbackMessageEncoder() );
        result.put( (byte) 15, new ResetMessageEncoder() );
        return result;
    }
}
