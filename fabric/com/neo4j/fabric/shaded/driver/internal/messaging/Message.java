package com.neo4j.fabric.shaded.driver.internal.messaging;

public interface Message
{
    byte signature();
}
