package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class RollbackMessage implements Message
{
    public static final byte SIGNATURE = 19;
    public static final Message ROLLBACK = new RollbackMessage();

    private RollbackMessage()
    {
    }

    public byte signature()
    {
        return 19;
    }

    public String toString()
    {
        return "ROLLBACK";
    }
}
