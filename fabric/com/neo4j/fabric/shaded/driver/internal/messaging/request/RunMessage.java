package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

import java.util.Collections;
import java.util.Map;

public class RunMessage implements Message
{
    public static final byte SIGNATURE = 16;
    private final String query;
    private final Map<String,Value> parameters;

    public RunMessage( String query )
    {
        this( query, Collections.emptyMap() );
    }

    public RunMessage( String query, Map<String,Value> parameters )
    {
        this.query = query;
        this.parameters = parameters;
    }

    public String query()
    {
        return this.query;
    }

    public Map<String,Value> parameters()
    {
        return this.parameters;
    }

    public byte signature()
    {
        return 16;
    }

    public String toString()
    {
        return String.format( "RUN \"%s\" %s", this.query, this.parameters );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            boolean var10000;
            label44:
            {
                label30:
                {
                    RunMessage that = (RunMessage) o;
                    if ( this.parameters != null )
                    {
                        if ( !this.parameters.equals( that.parameters ) )
                        {
                            break label30;
                        }
                    }
                    else if ( that.parameters != null )
                    {
                        break label30;
                    }

                    if ( this.query != null )
                    {
                        if ( this.query.equals( that.query ) )
                        {
                            break label44;
                        }
                    }
                    else if ( that.query == null )
                    {
                        break label44;
                    }
                }

                var10000 = false;
                return var10000;
            }

            var10000 = true;
            return var10000;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.query != null ? this.query.hashCode() : 0;
        result = 31 * result + (this.parameters != null ? this.parameters.hashCode() : 0);
        return result;
    }
}
