package com.neo4j.fabric.shaded.driver.internal.messaging.encode;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageEncoder;
import com.neo4j.fabric.shaded.driver.internal.messaging.ValuePacker;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.RunWithMetadataMessage;
import com.neo4j.fabric.shaded.driver.internal.util.Preconditions;

import java.io.IOException;

public class RunWithMetadataMessageEncoder implements MessageEncoder
{
    public void encode( Message message, ValuePacker packer ) throws IOException
    {
        Preconditions.checkArgument( message, RunWithMetadataMessage.class );
        RunWithMetadataMessage runMessage = (RunWithMetadataMessage) message;
        packer.packStructHeader( 3, runMessage.signature() );
        packer.pack( runMessage.query() );
        packer.pack( runMessage.parameters() );
        packer.pack( runMessage.metadata() );
    }
}
