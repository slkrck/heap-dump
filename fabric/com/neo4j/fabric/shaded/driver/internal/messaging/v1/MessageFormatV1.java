package com.neo4j.fabric.shaded.driver.internal.messaging.v1;

import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackInput;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackOutput;

public class MessageFormatV1 implements MessageFormat
{
    public static final byte NODE = 78;
    public static final byte RELATIONSHIP = 82;
    public static final byte UNBOUND_RELATIONSHIP = 114;
    public static final byte PATH = 80;
    public static final int NODE_FIELDS = 3;

    public MessageFormat.Writer newWriter( PackOutput output )
    {
        return new MessageWriterV1( output );
    }

    public MessageFormat.Reader newReader( PackInput input )
    {
        return new MessageReaderV1( input );
    }
}
