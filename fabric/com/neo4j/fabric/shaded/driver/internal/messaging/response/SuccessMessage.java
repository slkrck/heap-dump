package com.neo4j.fabric.shaded.driver.internal.messaging.response;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

import java.util.Map;

public class SuccessMessage implements Message
{
    public static final byte SIGNATURE = 112;
    private final Map<String,Value> metadata;

    public SuccessMessage( Map<String,Value> metadata )
    {
        this.metadata = metadata;
    }

    public Map<String,Value> metadata()
    {
        return this.metadata;
    }

    public byte signature()
    {
        return 112;
    }

    public String toString()
    {
        return String.format( "SUCCESS %s", this.metadata );
    }

    public boolean equals( Object obj )
    {
        return obj != null && obj.getClass() == this.getClass();
    }

    public int hashCode()
    {
        return 1;
    }
}
