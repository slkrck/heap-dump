package com.neo4j.fabric.shaded.driver.internal.messaging.request;

import com.neo4j.fabric.shaded.driver.internal.messaging.Message;

public class GoodbyeMessage implements Message
{
    public static final byte SIGNATURE = 2;
    public static final GoodbyeMessage GOODBYE = new GoodbyeMessage();

    private GoodbyeMessage()
    {
    }

    public byte signature()
    {
        return 2;
    }

    public String toString()
    {
        return "GOODBYE";
    }
}
