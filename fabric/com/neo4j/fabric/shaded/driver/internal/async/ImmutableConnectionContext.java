package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.DatabaseNameUtil;
import com.neo4j.fabric.shaded.driver.internal.InternalBookmark;

public class ImmutableConnectionContext implements ConnectionContext
{
    private static final ConnectionContext SINGLE_DB_CONTEXT;
    private static final ConnectionContext MULTI_DB_CONTEXT;

    static
    {
        SINGLE_DB_CONTEXT = new ImmutableConnectionContext( DatabaseNameUtil.defaultDatabase(), InternalBookmark.empty(), AccessMode.READ );
        MULTI_DB_CONTEXT = new ImmutableConnectionContext( DatabaseNameUtil.systemDatabase(), InternalBookmark.empty(), AccessMode.READ );
    }

    private final DatabaseName databaseName;
    private final AccessMode mode;
    private final Bookmark rediscoveryBookmark;

    public ImmutableConnectionContext( DatabaseName databaseName, Bookmark bookmark, AccessMode mode )
    {
        this.databaseName = databaseName;
        this.rediscoveryBookmark = bookmark;
        this.mode = mode;
    }

    public static ConnectionContext simple( boolean supportsMultiDb )
    {
        return supportsMultiDb ? MULTI_DB_CONTEXT : SINGLE_DB_CONTEXT;
    }

    public DatabaseName databaseName()
    {
        return this.databaseName;
    }

    public AccessMode mode()
    {
        return this.mode;
    }

    public Bookmark rediscoveryBookmark()
    {
        return this.rediscoveryBookmark;
    }
}
