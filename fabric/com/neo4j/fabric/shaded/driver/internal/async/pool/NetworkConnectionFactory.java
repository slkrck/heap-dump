package com.neo4j.fabric.shaded.driver.internal.async.pool;

import com.neo4j.fabric.shaded.driver.internal.async.NetworkConnection;
import com.neo4j.fabric.shaded.driver.internal.metrics.MetricsListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

public class NetworkConnectionFactory implements ConnectionFactory
{
    private final Clock clock;
    private final MetricsListener metricsListener;

    public NetworkConnectionFactory( Clock clock, MetricsListener metricsListener )
    {
        this.clock = clock;
        this.metricsListener = metricsListener;
    }

    public Connection createConnection( Channel channel, ExtendedChannelPool pool )
    {
        return new NetworkConnection( channel, pool, this.clock, this.metricsListener );
    }
}
