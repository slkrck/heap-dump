package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.concurrent.CompletionStage;

public class DirectConnection implements Connection
{
    private final Connection delegate;
    private final AccessMode mode;
    private final DatabaseName databaseName;

    public DirectConnection( Connection delegate, DatabaseName databaseName, AccessMode mode )
    {
        this.delegate = delegate;
        this.mode = mode;
        this.databaseName = databaseName;
    }

    public Connection connection()
    {
        return this.delegate;
    }

    public boolean isOpen()
    {
        return this.delegate.isOpen();
    }

    public void enableAutoRead()
    {
        this.delegate.enableAutoRead();
    }

    public void disableAutoRead()
    {
        this.delegate.disableAutoRead();
    }

    public void write( Message message, ResponseHandler handler )
    {
        this.delegate.write( message, handler );
    }

    public void write( Message message1, ResponseHandler handler1, Message message2, ResponseHandler handler2 )
    {
        this.delegate.write( message1, handler1, message2, handler2 );
    }

    public void writeAndFlush( Message message, ResponseHandler handler )
    {
        this.delegate.writeAndFlush( message, handler );
    }

    public void writeAndFlush( Message message1, ResponseHandler handler1, Message message2, ResponseHandler handler2 )
    {
        this.delegate.writeAndFlush( message1, handler1, message2, handler2 );
    }

    public CompletionStage<Void> reset()
    {
        return this.delegate.reset();
    }

    public CompletionStage<Void> release()
    {
        return this.delegate.release();
    }

    public void terminateAndRelease( String reason )
    {
        this.delegate.terminateAndRelease( reason );
    }

    public BoltServerAddress serverAddress()
    {
        return this.delegate.serverAddress();
    }

    public ServerVersion serverVersion()
    {
        return this.delegate.serverVersion();
    }

    public BoltProtocol protocol()
    {
        return this.delegate.protocol();
    }

    public AccessMode mode()
    {
        return this.mode;
    }

    public DatabaseName databaseName()
    {
        return this.databaseName;
    }

    public void flush()
    {
        this.delegate.flush();
    }
}
