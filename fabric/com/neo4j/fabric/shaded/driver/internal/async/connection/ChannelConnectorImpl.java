package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.AuthToken;
import com.neo4j.fabric.shaded.driver.AuthTokens;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.ConnectionSettings;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.ConnectTimeoutHandler;
import com.neo4j.fabric.shaded.driver.internal.security.InternalAuthToken;
import com.neo4j.fabric.shaded.driver.internal.security.SecurityPlan;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.Bootstrap;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOption;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPipeline;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

import java.util.Map;
import java.util.Objects;

public class ChannelConnectorImpl implements ChannelConnector
{
    private final String userAgent;
    private final Map<String,Value> authToken;
    private final SecurityPlan securityPlan;
    private final ChannelPipelineBuilder pipelineBuilder;
    private final int connectTimeoutMillis;
    private final Logging logging;
    private final Clock clock;

    public ChannelConnectorImpl( ConnectionSettings connectionSettings, SecurityPlan securityPlan, Logging logging, Clock clock )
    {
        this( connectionSettings, securityPlan, new ChannelPipelineBuilderImpl(), logging, clock );
    }

    public ChannelConnectorImpl( ConnectionSettings connectionSettings, SecurityPlan securityPlan, ChannelPipelineBuilder pipelineBuilder, Logging logging,
            Clock clock )
    {
        this.userAgent = connectionSettings.userAgent();
        this.authToken = tokenAsMap( connectionSettings.authToken() );
        this.connectTimeoutMillis = connectionSettings.connectTimeoutMillis();
        this.securityPlan = (SecurityPlan) Objects.requireNonNull( securityPlan );
        this.pipelineBuilder = pipelineBuilder;
        this.logging = (Logging) Objects.requireNonNull( logging );
        this.clock = (Clock) Objects.requireNonNull( clock );
    }

    private static Map<String,Value> tokenAsMap( AuthToken token )
    {
        if ( token instanceof InternalAuthToken )
        {
            return ((InternalAuthToken) token).toMap();
        }
        else
        {
            throw new ClientException(
                    "Unknown authentication token, `" + token + "`. Please use one of the supported tokens from `" + AuthTokens.class.getSimpleName() + "`." );
        }
    }

    public ChannelFuture connect( BoltServerAddress address, Bootstrap bootstrap )
    {
        bootstrap.option( ChannelOption.CONNECT_TIMEOUT_MILLIS, this.connectTimeoutMillis );
        bootstrap.handler( new NettyChannelInitializer( address, this.securityPlan, this.connectTimeoutMillis, this.clock, this.logging ) );
        ChannelFuture channelConnected = bootstrap.connect( address.toSocketAddress() );
        Channel channel = channelConnected.channel();
        ChannelPromise handshakeCompleted = channel.newPromise();
        ChannelPromise connectionInitialized = channel.newPromise();
        this.installChannelConnectedListeners( address, channelConnected, handshakeCompleted );
        this.installHandshakeCompletedListeners( handshakeCompleted, connectionInitialized );
        return connectionInitialized;
    }

    private void installChannelConnectedListeners( BoltServerAddress address, ChannelFuture channelConnected, ChannelPromise handshakeCompleted )
    {
        ChannelPipeline pipeline = channelConnected.channel().pipeline();
        channelConnected.addListener( ( future ) -> {
            pipeline.addFirst( new ConnectTimeoutHandler( (long) this.connectTimeoutMillis ) );
        } );
        channelConnected.addListener( new ChannelConnectedListener( address, this.pipelineBuilder, handshakeCompleted, this.logging ) );
    }

    private void installHandshakeCompletedListeners( ChannelPromise handshakeCompleted, ChannelPromise connectionInitialized )
    {
        ChannelPipeline pipeline = handshakeCompleted.channel().pipeline();
        handshakeCompleted.addListener( ( future ) -> {
            ConnectTimeoutHandler var10000 = (ConnectTimeoutHandler) pipeline.remove( ConnectTimeoutHandler.class );
        } );
        handshakeCompleted.addListener( new HandshakeCompletedListener( this.userAgent, this.authToken, connectionInitialized ) );
    }
}
