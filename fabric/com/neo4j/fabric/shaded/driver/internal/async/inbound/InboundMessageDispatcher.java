package com.neo4j.fabric.shaded.driver.internal.async.inbound;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.handlers.ResetResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.logging.ChannelActivityLogger;
import com.neo4j.fabric.shaded.driver.internal.messaging.ResponseMessageHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.ResetMessage;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.ErrorUtil;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

public class InboundMessageDispatcher implements ResponseMessageHandler
{
    private final Channel channel;
    private final Queue<ResponseHandler> handlers = new LinkedList();
    private final Logger log;
    private Throwable currentError;
    private boolean fatalErrorOccurred;
    private ResponseHandler autoReadManagingHandler;

    public InboundMessageDispatcher( Channel channel, Logging logging )
    {
        this.channel = (Channel) Objects.requireNonNull( channel );
        this.log = new ChannelActivityLogger( channel, logging, this.getClass() );
    }

    public void enqueue( ResponseHandler handler )
    {
        if ( this.fatalErrorOccurred )
        {
            handler.onFailure( this.currentError );
        }
        else
        {
            this.handlers.add( handler );
            this.updateAutoReadManagingHandlerIfNeeded( handler );
        }
    }

    public int queuedHandlersCount()
    {
        return this.handlers.size();
    }

    public void handleSuccessMessage( Map<String,Value> meta )
    {
        this.log.debug( "S: SUCCESS %s", meta );
        ResponseHandler handler = this.removeHandler();
        handler.onSuccess( meta );
    }

    public void handleRecordMessage( Value[] fields )
    {
        if ( this.log.isDebugEnabled() )
        {
            this.log.debug( "S: RECORD %s", Arrays.toString( fields ) );
        }

        ResponseHandler handler = (ResponseHandler) this.handlers.peek();
        if ( handler == null )
        {
            throw new IllegalStateException( "No handler exists to handle RECORD message with fields: " + Arrays.toString( fields ) );
        }
        else
        {
            handler.onRecord( fields );
        }
    }

    public void handleFailureMessage( String code, String message )
    {
        this.log.debug( "S: FAILURE %s \"%s\"", code, message );
        this.currentError = ErrorUtil.newNeo4jError( code, message );
        if ( ErrorUtil.isFatal( this.currentError ) )
        {
            this.channel.pipeline().fireExceptionCaught( this.currentError );
        }
        else
        {
            this.enqueue( new ResetResponseHandler( this ) );
            this.channel.writeAndFlush( ResetMessage.RESET, this.channel.voidPromise() );
            ResponseHandler handler = this.removeHandler();
            handler.onFailure( this.currentError );
        }
    }

    public void handleIgnoredMessage()
    {
        this.log.debug( "S: IGNORED" );
        ResponseHandler handler = this.removeHandler();
        Object error;
        if ( this.currentError != null )
        {
            error = this.currentError;
        }
        else
        {
            this.log.warn( "Received IGNORED message for handler %s but error is missing and RESET is not in progress. Current handlers %s", handler,
                    this.handlers );
            error = new ClientException( "Database ignored the request" );
        }

        handler.onFailure( (Throwable) error );
    }

    public void handleChannelError( Throwable error )
    {
        if ( this.currentError != null )
        {
            ErrorUtil.addSuppressed( this.currentError, error );
        }
        else
        {
            this.currentError = error;
        }

        this.fatalErrorOccurred = true;

        while ( !this.handlers.isEmpty() )
        {
            ResponseHandler handler = this.removeHandler();
            handler.onFailure( this.currentError );
        }
    }

    public void clearCurrentError()
    {
        this.currentError = null;
    }

    public Throwable currentError()
    {
        return this.currentError;
    }

    public boolean fatalErrorOccurred()
    {
        return this.fatalErrorOccurred;
    }

    ResponseHandler autoReadManagingHandler()
    {
        return this.autoReadManagingHandler;
    }

    private ResponseHandler removeHandler()
    {
        ResponseHandler handler = (ResponseHandler) this.handlers.remove();
        if ( handler == this.autoReadManagingHandler )
        {
            this.updateAutoReadManagingHandler( (ResponseHandler) null );
        }

        return handler;
    }

    private void updateAutoReadManagingHandlerIfNeeded( ResponseHandler handler )
    {
        if ( handler.canManageAutoRead() )
        {
            this.updateAutoReadManagingHandler( handler );
        }
    }

    private void updateAutoReadManagingHandler( ResponseHandler newHandler )
    {
        if ( this.autoReadManagingHandler != null )
        {
            this.autoReadManagingHandler.disableAutoReadManagement();
            this.channel.config().setAutoRead( true );
        }

        this.autoReadManagingHandler = newHandler;
    }
}
