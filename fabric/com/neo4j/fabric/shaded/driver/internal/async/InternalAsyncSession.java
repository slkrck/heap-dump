package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.async.AsyncSession;
import com.neo4j.fabric.shaded.driver.async.AsyncTransaction;
import com.neo4j.fabric.shaded.driver.async.AsyncTransactionWork;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class InternalAsyncSession extends AsyncAbstractQueryRunner implements AsyncSession
{
    private final NetworkSession session;

    public InternalAsyncSession( NetworkSession session )
    {
        this.session = session;
    }

    public CompletionStage<ResultCursor> runAsync( Query query )
    {
        return this.runAsync( query, TransactionConfig.empty() );
    }

    public CompletionStage<ResultCursor> runAsync( String query, TransactionConfig config )
    {
        return this.runAsync( query, Collections.emptyMap(), config );
    }

    public CompletionStage<ResultCursor> runAsync( String query, Map<String,Object> parameters, TransactionConfig config )
    {
        return this.runAsync( new Query( query, parameters ), config );
    }

    public CompletionStage<ResultCursor> runAsync( Query query, TransactionConfig config )
    {
        return this.session.runAsync( query, config, true );
    }

    public CompletionStage<Void> closeAsync()
    {
        return this.session.closeAsync();
    }

    public CompletionStage<AsyncTransaction> beginTransactionAsync()
    {
        return this.beginTransactionAsync( TransactionConfig.empty() );
    }

    public CompletionStage<AsyncTransaction> beginTransactionAsync( TransactionConfig config )
    {
        return this.session.beginTransactionAsync( config ).thenApply( InternalAsyncTransaction::new );
    }

    public <T> CompletionStage<T> readTransactionAsync( AsyncTransactionWork<CompletionStage<T>> work )
    {
        return this.readTransactionAsync( work, TransactionConfig.empty() );
    }

    public <T> CompletionStage<T> readTransactionAsync( AsyncTransactionWork<CompletionStage<T>> work, TransactionConfig config )
    {
        return this.transactionAsync( AccessMode.READ, work, config );
    }

    public <T> CompletionStage<T> writeTransactionAsync( AsyncTransactionWork<CompletionStage<T>> work )
    {
        return this.writeTransactionAsync( work, TransactionConfig.empty() );
    }

    public <T> CompletionStage<T> writeTransactionAsync( AsyncTransactionWork<CompletionStage<T>> work, TransactionConfig config )
    {
        return this.transactionAsync( AccessMode.WRITE, work, config );
    }

    public Bookmark lastBookmark()
    {
        return this.session.lastBookmark();
    }

    private <T> CompletionStage<T> transactionAsync( AccessMode mode, AsyncTransactionWork<CompletionStage<T>> work, TransactionConfig config )
    {
        return this.session.retryLogic().retryAsync( () -> {
            CompletableFuture<T> resultFuture = new CompletableFuture();
            CompletionStage<UnmanagedTransaction> txFuture = this.session.beginTransactionAsync( mode, config );
            txFuture.whenComplete( ( tx, completionError ) -> {
                Throwable error = Futures.completionExceptionCause( completionError );
                if ( error != null )
                {
                    resultFuture.completeExceptionally( error );
                }
                else
                {
                    this.executeWork( resultFuture, tx, work );
                }
            } );
            return resultFuture;
        } );
    }

    private <T> void executeWork( CompletableFuture<T> resultFuture, UnmanagedTransaction tx, AsyncTransactionWork<CompletionStage<T>> work )
    {
        CompletionStage<T> workFuture = this.safeExecuteWork( tx, work );
        workFuture.whenComplete( ( result, completionError ) -> {
            Throwable error = Futures.completionExceptionCause( completionError );
            if ( error != null )
            {
                this.rollbackTxAfterFailedTransactionWork( tx, resultFuture, error );
            }
            else
            {
                this.closeTxAfterSucceededTransactionWork( tx, resultFuture, result );
            }
        } );
    }

    private <T> CompletionStage<T> safeExecuteWork( UnmanagedTransaction tx, AsyncTransactionWork<CompletionStage<T>> work )
    {
        try
        {
            CompletionStage<T> result = (CompletionStage) work.execute( new InternalAsyncTransaction( tx ) );
            return (CompletionStage) (result == null ? Futures.completedWithNull() : result);
        }
        catch ( Throwable var4 )
        {
            return Futures.failedFuture( var4 );
        }
    }

    private <T> void rollbackTxAfterFailedTransactionWork( UnmanagedTransaction tx, CompletableFuture<T> resultFuture, Throwable error )
    {
        if ( tx.isOpen() )
        {
            tx.rollbackAsync().whenComplete( ( ignore, rollbackError ) -> {
                if ( rollbackError != null )
                {
                    error.addSuppressed( rollbackError );
                }

                resultFuture.completeExceptionally( error );
            } );
        }
        else
        {
            resultFuture.completeExceptionally( error );
        }
    }

    private <T> void closeTxAfterSucceededTransactionWork( UnmanagedTransaction tx, CompletableFuture<T> resultFuture, T result )
    {
        if ( tx.isOpen() )
        {
            tx.commitAsync().whenComplete( ( ignore, completionError ) -> {
                Throwable commitError = Futures.completionExceptionCause( completionError );
                if ( commitError != null )
                {
                    resultFuture.completeExceptionally( commitError );
                }
                else
                {
                    resultFuture.complete( result );
                }
            } );
        }
        else
        {
            resultFuture.complete( result );
        }
    }
}
