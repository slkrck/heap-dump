package com.neo4j.fabric.shaded.driver.internal.async.pool;

import java.util.concurrent.TimeUnit;

public class PoolSettings
{
    public static final int NOT_CONFIGURED = -1;
    public static final int DEFAULT_MAX_CONNECTION_POOL_SIZE = 100;
    public static final long DEFAULT_IDLE_TIME_BEFORE_CONNECTION_TEST = -1L;
    public static final long DEFAULT_MAX_CONNECTION_LIFETIME;
    public static final long DEFAULT_CONNECTION_ACQUISITION_TIMEOUT;

    static
    {
        DEFAULT_MAX_CONNECTION_LIFETIME = TimeUnit.HOURS.toMillis( 1L );
        DEFAULT_CONNECTION_ACQUISITION_TIMEOUT = TimeUnit.SECONDS.toMillis( 60L );
    }

    private final int maxConnectionPoolSize;
    private final long connectionAcquisitionTimeout;
    private final long maxConnectionLifetime;
    private final long idleTimeBeforeConnectionTest;

    public PoolSettings( int maxConnectionPoolSize, long connectionAcquisitionTimeout, long maxConnectionLifetime, long idleTimeBeforeConnectionTest )
    {
        this.maxConnectionPoolSize = maxConnectionPoolSize;
        this.connectionAcquisitionTimeout = connectionAcquisitionTimeout;
        this.maxConnectionLifetime = maxConnectionLifetime;
        this.idleTimeBeforeConnectionTest = idleTimeBeforeConnectionTest;
    }

    public long idleTimeBeforeConnectionTest()
    {
        return this.idleTimeBeforeConnectionTest;
    }

    public boolean idleTimeBeforeConnectionTestEnabled()
    {
        return this.idleTimeBeforeConnectionTest >= 0L;
    }

    public long maxConnectionLifetime()
    {
        return this.maxConnectionLifetime;
    }

    public boolean maxConnectionLifetimeEnabled()
    {
        return this.maxConnectionLifetime > 0L;
    }

    public int maxConnectionPoolSize()
    {
        return this.maxConnectionPoolSize;
    }

    public long connectionAcquisitionTimeout()
    {
        return this.connectionAcquisitionTimeout;
    }
}
