package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.internal.FailableCursor;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ResultCursorsHolder
{
    private final List<CompletionStage<? extends FailableCursor>> cursorStages = new ArrayList();

    private static Throwable findFirstFailure( CompletableFuture<Throwable>[] completedFailureFutures )
    {
        CompletableFuture[] var1 = completedFailureFutures;
        int var2 = completedFailureFutures.length;

        for ( int var3 = 0; var3 < var2; ++var3 )
        {
            CompletableFuture<Throwable> failureFuture = var1[var3];
            Throwable failure = (Throwable) failureFuture.getNow( (Object) null );
            if ( failure != null )
            {
                return failure;
            }
        }

        return null;
    }

    private static CompletionStage<Throwable> retrieveFailure( CompletionStage<? extends FailableCursor> cursorStage )
    {
        return cursorStage.exceptionally( ( cursor ) -> {
            return null;
        } ).thenCompose( ( cursor ) -> {
            return (CompletionStage) (cursor == null ? Futures.completedWithNull() : cursor.discardAllFailureAsync());
        } );
    }

    public void add( CompletionStage<? extends FailableCursor> cursorStage )
    {
        Objects.requireNonNull( cursorStage );
        this.cursorStages.add( cursorStage );
    }

    CompletionStage<Throwable> retrieveNotConsumedError()
    {
        CompletableFuture<Throwable>[] failures = this.retrieveAllFailures();
        return CompletableFuture.allOf( failures ).thenApply( ( ignore ) -> {
            return findFirstFailure( failures );
        } );
    }

    private CompletableFuture<Throwable>[] retrieveAllFailures()
    {
        return (CompletableFuture[]) this.cursorStages.stream().map( ResultCursorsHolder::retrieveFailure ).map( CompletionStage::toCompletableFuture ).toArray(
                ( x$0 ) -> {
                    return new CompletableFuture[x$0];
                } );
    }
}
