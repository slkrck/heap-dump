package com.neo4j.fabric.shaded.driver.internal.async.inbound;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.logging.ChannelActivityLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.LengthFieldBasedFrameDecoder;

public class ChunkDecoder extends LengthFieldBasedFrameDecoder
{
    private static final int MAX_FRAME_BODY_LENGTH = 65535;
    private static final int LENGTH_FIELD_OFFSET = 0;
    private static final int LENGTH_FIELD_LENGTH = 2;
    private static final int LENGTH_ADJUSTMENT = 0;
    private static final int INITIAL_BYTES_TO_STRIP = 2;
    private static final int MAX_FRAME_LENGTH = 65537;
    private final Logging logging;
    private Logger log;

    public ChunkDecoder( Logging logging )
    {
        super( 65537, 0, 2, 0, 2 );
        this.logging = logging;
    }

    public void handlerAdded( ChannelHandlerContext ctx )
    {
        this.log = new ChannelActivityLogger( ctx.channel(), this.logging, this.getClass() );
    }

    protected void handlerRemoved0( ChannelHandlerContext ctx )
    {
        this.log = null;
    }

    protected ByteBuf extractFrame( ChannelHandlerContext ctx, ByteBuf buffer, int index, int length )
    {
        if ( this.log.isTraceEnabled() )
        {
            int originalReaderIndex = buffer.readerIndex();
            int readerIndexWithChunkHeader = originalReaderIndex - 2;
            int lengthWithChunkHeader = 2 + length;
            String hexDump = ByteBufUtil.hexDump( buffer, readerIndexWithChunkHeader, lengthWithChunkHeader );
            this.log.trace( "S: %s", hexDump );
        }

        return super.extractFrame( ctx, buffer, index, length );
    }
}
