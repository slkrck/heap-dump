package com.neo4j.fabric.shaded.driver.internal.async.pool;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;

public interface ConnectionFactory
{
    Connection createConnection( Channel var1, ExtendedChannelPool var2 );
}
