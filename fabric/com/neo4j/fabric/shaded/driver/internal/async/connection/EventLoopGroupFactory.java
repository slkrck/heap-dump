package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.nio.NioEventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.socket.nio.NioSocketChannel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.DefaultThreadFactory;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.FastThreadLocalThread;

import java.util.concurrent.ThreadFactory;

public final class EventLoopGroupFactory
{
    private static final String THREAD_NAME_PREFIX = "Neo4jDriverIO";
    private static final int THREAD_PRIORITY = 10;

    private EventLoopGroupFactory()
    {
    }

    public static Class<? extends Channel> channelClass()
    {
        return NioSocketChannel.class;
    }

    public static EventLoopGroup newEventLoopGroup( int threadCount )
    {
        return new EventLoopGroupFactory.DriverEventLoopGroup( threadCount );
    }

    public static void assertNotInEventLoopThread() throws IllegalStateException
    {
        if ( isEventLoopThread( Thread.currentThread() ) )
        {
            throw new IllegalStateException(
                    "Blocking operation can't be executed in IO thread because it might result in a deadlock. Please do not use blocking API when chaining futures returned by async API methods." );
        }
    }

    public static boolean isEventLoopThread( Thread thread )
    {
        return thread instanceof EventLoopGroupFactory.DriverThread;
    }

    private static class DriverThread extends FastThreadLocalThread
    {
        DriverThread( ThreadGroup group, Runnable target, String name )
        {
            super( group, target, name );
        }
    }

    private static class DriverThreadFactory extends DefaultThreadFactory
    {
        DriverThreadFactory()
        {
            super( (String) "Neo4jDriverIO", 10 );
        }

        protected Thread newThread( Runnable r, String name )
        {
            return new EventLoopGroupFactory.DriverThread( this.threadGroup, r, name );
        }
    }

    private static class DriverEventLoopGroup extends NioEventLoopGroup
    {
        DriverEventLoopGroup()
        {
        }

        DriverEventLoopGroup( int nThreads )
        {
            super( nThreads );
        }

        protected ThreadFactory newDefaultThreadFactory()
        {
            return new EventLoopGroupFactory.DriverThreadFactory();
        }
    }
}
