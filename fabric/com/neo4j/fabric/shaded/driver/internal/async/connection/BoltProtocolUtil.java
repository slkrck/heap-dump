package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.Unpooled;

public final class BoltProtocolUtil
{
    public static final int HTTP = 1213486160;
    public static final int BOLT_MAGIC_PREAMBLE = 1616949271;
    public static final int NO_PROTOCOL_VERSION = 0;
    public static final int CHUNK_HEADER_SIZE_BYTES = 2;
    public static final int DEFAULT_MAX_OUTBOUND_CHUNK_SIZE_BYTES = 16383;
    private static final ByteBuf HANDSHAKE_BUF = Unpooled.unreleasableBuffer( Unpooled.copyInt( 1616949271, 4, 3, 2, 1 ) ).asReadOnly();
    private static final String HANDSHAKE_STRING = createHandshakeString();

    private BoltProtocolUtil()
    {
    }

    public static ByteBuf handshakeBuf()
    {
        return HANDSHAKE_BUF.duplicate();
    }

    public static String handshakeString()
    {
        return HANDSHAKE_STRING;
    }

    public static void writeMessageBoundary( ByteBuf buf )
    {
        buf.writeShort( 0 );
    }

    public static void writeEmptyChunkHeader( ByteBuf buf )
    {
        buf.writeShort( 0 );
    }

    public static void writeChunkHeader( ByteBuf buf, int chunkStartIndex, int headerValue )
    {
        buf.setShort( chunkStartIndex, headerValue );
    }

    private static String createHandshakeString()
    {
        ByteBuf buf = handshakeBuf();
        return String.format( "[0x%s, %s, %s, %s, %s]", Integer.toHexString( buf.readInt() ), buf.readInt(), buf.readInt(), buf.readInt(), buf.readInt() );
    }
}
