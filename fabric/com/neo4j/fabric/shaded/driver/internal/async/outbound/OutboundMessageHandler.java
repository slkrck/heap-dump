package com.neo4j.fabric.shaded.driver.internal.async.outbound;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.async.connection.BoltProtocolUtil;
import com.neo4j.fabric.shaded.driver.internal.logging.ChannelActivityLogger;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.EncoderException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

public class OutboundMessageHandler extends MessageToMessageEncoder<Message>
{
    public static final String NAME = OutboundMessageHandler.class.getSimpleName();
    private final ChunkAwareByteBufOutput output = new ChunkAwareByteBufOutput();
    private final MessageFormat.Writer writer;
    private final Logging logging;
    private Logger log;

    public OutboundMessageHandler( MessageFormat messageFormat, Logging logging )
    {
        this.writer = messageFormat.newWriter( this.output );
        this.logging = logging;
    }

    public void handlerAdded( ChannelHandlerContext ctx )
    {
        this.log = new ChannelActivityLogger( ctx.channel(), this.logging, this.getClass() );
    }

    public void handlerRemoved( ChannelHandlerContext ctx )
    {
        this.log = null;
    }

    protected void encode( ChannelHandlerContext ctx, Message msg, List<Object> out )
    {
        this.log.debug( "C: %s", msg );
        ByteBuf messageBuf = ctx.alloc().ioBuffer();
        this.output.start( messageBuf );

        try
        {
            this.writer.write( msg );
            this.output.stop();
        }
        catch ( Throwable var6 )
        {
            this.output.stop();
            messageBuf.release();
            throw new EncoderException( "Failed to write outbound message: " + msg, var6 );
        }

        if ( this.log.isTraceEnabled() )
        {
            this.log.trace( "C: %s", ByteBufUtil.hexDump( messageBuf ) );
        }

        BoltProtocolUtil.writeMessageBoundary( messageBuf );
        out.add( messageBuf );
    }
}
