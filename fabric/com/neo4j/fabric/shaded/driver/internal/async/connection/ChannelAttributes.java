package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.InboundMessageDispatcher;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.AttributeKey;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

public final class ChannelAttributes
{
    private static final AttributeKey<String> CONNECTION_ID = AttributeKey.newInstance( "connectionId" );
    private static final AttributeKey<String> POOL_ID = AttributeKey.newInstance( "poolId" );
    private static final AttributeKey<Integer> PROTOCOL_VERSION = AttributeKey.newInstance( "protocolVersion" );
    private static final AttributeKey<BoltServerAddress> ADDRESS = AttributeKey.newInstance( "serverAddress" );
    private static final AttributeKey<ServerVersion> SERVER_VERSION = AttributeKey.newInstance( "serverVersion" );
    private static final AttributeKey<Long> CREATION_TIMESTAMP = AttributeKey.newInstance( "creationTimestamp" );
    private static final AttributeKey<Long> LAST_USED_TIMESTAMP = AttributeKey.newInstance( "lastUsedTimestamp" );
    private static final AttributeKey<InboundMessageDispatcher> MESSAGE_DISPATCHER = AttributeKey.newInstance( "messageDispatcher" );
    private static final AttributeKey<String> TERMINATION_REASON = AttributeKey.newInstance( "terminationReason" );

    private ChannelAttributes()
    {
    }

    public static String connectionId( Channel channel )
    {
        return (String) get( channel, CONNECTION_ID );
    }

    public static void setConnectionId( Channel channel, String id )
    {
        setOnce( channel, CONNECTION_ID, id );
    }

    public static String poolId( Channel channel )
    {
        return (String) get( channel, POOL_ID );
    }

    public static void setPoolId( Channel channel, String id )
    {
        setOnce( channel, POOL_ID, id );
    }

    public static int protocolVersion( Channel channel )
    {
        return (Integer) get( channel, PROTOCOL_VERSION );
    }

    public static void setProtocolVersion( Channel channel, int version )
    {
        setOnce( channel, PROTOCOL_VERSION, version );
    }

    public static BoltServerAddress serverAddress( Channel channel )
    {
        return (BoltServerAddress) get( channel, ADDRESS );
    }

    public static void setServerAddress( Channel channel, BoltServerAddress address )
    {
        setOnce( channel, ADDRESS, address );
    }

    public static ServerVersion serverVersion( Channel channel )
    {
        return (ServerVersion) get( channel, SERVER_VERSION );
    }

    public static void setServerVersion( Channel channel, ServerVersion version )
    {
        setOnce( channel, SERVER_VERSION, version );
    }

    public static long creationTimestamp( Channel channel )
    {
        return (Long) get( channel, CREATION_TIMESTAMP );
    }

    public static void setCreationTimestamp( Channel channel, long creationTimestamp )
    {
        setOnce( channel, CREATION_TIMESTAMP, creationTimestamp );
    }

    public static Long lastUsedTimestamp( Channel channel )
    {
        return (Long) get( channel, LAST_USED_TIMESTAMP );
    }

    public static void setLastUsedTimestamp( Channel channel, long lastUsedTimestamp )
    {
        set( channel, LAST_USED_TIMESTAMP, lastUsedTimestamp );
    }

    public static InboundMessageDispatcher messageDispatcher( Channel channel )
    {
        return (InboundMessageDispatcher) get( channel, MESSAGE_DISPATCHER );
    }

    public static void setMessageDispatcher( Channel channel, InboundMessageDispatcher messageDispatcher )
    {
        setOnce( channel, MESSAGE_DISPATCHER, messageDispatcher );
    }

    public static String terminationReason( Channel channel )
    {
        return (String) get( channel, TERMINATION_REASON );
    }

    public static void setTerminationReason( Channel channel, String reason )
    {
        setOnce( channel, TERMINATION_REASON, reason );
    }

    private static <T> T get( Channel channel, AttributeKey<T> key )
    {
        return channel.attr( key ).get();
    }

    private static <T> void set( Channel channel, AttributeKey<T> key, T value )
    {
        channel.attr( key ).set( value );
    }

    private static <T> void setOnce( Channel channel, AttributeKey<T> key, T value )
    {
        T existingValue = channel.attr( key ).setIfAbsent( value );
        if ( existingValue != null )
        {
            throw new IllegalStateException( "Unable to set " + key.name() + " because it is already set to " + existingValue );
        }
    }
}
