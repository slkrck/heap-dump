package com.neo4j.fabric.shaded.driver.internal.async.pool;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.handlers.PingResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.ResetMessage;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.pool.ChannelHealthChecker;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Future;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.Promise;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

public class NettyChannelHealthChecker implements ChannelHealthChecker
{
    private final PoolSettings poolSettings;
    private final Clock clock;
    private final Logger log;

    public NettyChannelHealthChecker( PoolSettings poolSettings, Clock clock, Logging logging )
    {
        this.poolSettings = poolSettings;
        this.clock = clock;
        this.log = logging.getLog( this.getClass().getSimpleName() );
    }

    public Future<Boolean> isHealthy( Channel channel )
    {
        if ( this.isTooOld( channel ) )
        {
            return channel.eventLoop().newSucceededFuture( Boolean.FALSE );
        }
        else
        {
            return this.hasBeenIdleForTooLong( channel ) ? this.ping( channel ) : ACTIVE.isHealthy( channel );
        }
    }

    private boolean isTooOld( Channel channel )
    {
        if ( this.poolSettings.maxConnectionLifetimeEnabled() )
        {
            long creationTimestampMillis = ChannelAttributes.creationTimestamp( channel );
            long currentTimestampMillis = this.clock.millis();
            long ageMillis = currentTimestampMillis - creationTimestampMillis;
            long maxAgeMillis = this.poolSettings.maxConnectionLifetime();
            boolean tooOld = ageMillis > maxAgeMillis;
            if ( tooOld )
            {
                this.log.trace( "Failed acquire channel %s from the pool because it is too old: %s > %s", channel, ageMillis, maxAgeMillis );
            }

            return tooOld;
        }
        else
        {
            return false;
        }
    }

    private boolean hasBeenIdleForTooLong( Channel channel )
    {
        if ( this.poolSettings.idleTimeBeforeConnectionTestEnabled() )
        {
            Long lastUsedTimestamp = ChannelAttributes.lastUsedTimestamp( channel );
            if ( lastUsedTimestamp != null )
            {
                long idleTime = this.clock.millis() - lastUsedTimestamp;
                boolean idleTooLong = idleTime > this.poolSettings.idleTimeBeforeConnectionTest();
                if ( idleTooLong )
                {
                    this.log.trace( "Channel %s has been idle for %s and needs a ping", channel, idleTime );
                }

                return idleTooLong;
            }
        }

        return false;
    }

    private Future<Boolean> ping( Channel channel )
    {
        Promise<Boolean> result = channel.eventLoop().newPromise();
        ChannelAttributes.messageDispatcher( channel ).enqueue( new PingResponseHandler( result, channel, this.log ) );
        channel.writeAndFlush( ResetMessage.RESET, channel.voidPromise() );
        return result;
    }
}
