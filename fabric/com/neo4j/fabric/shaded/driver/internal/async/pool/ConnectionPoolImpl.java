package com.neo4j.fabric.shaded.driver.internal.async.pool;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelConnector;
import com.neo4j.fabric.shaded.driver.internal.metrics.ListenerEvent;
import com.neo4j.fabric.shaded.driver.internal.metrics.MetricsListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.Bootstrap;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConnectionPoolImpl implements ConnectionPool
{
    private final ChannelConnector connector;
    private final Bootstrap bootstrap;
    private final NettyChannelTracker nettyChannelTracker;
    private final NettyChannelHealthChecker channelHealthChecker;
    private final PoolSettings settings;
    private final Logger log;
    private final MetricsListener metricsListener;
    private final boolean ownsEventLoopGroup;
    private final ConcurrentMap<BoltServerAddress,ExtendedChannelPool> pools;
    private final AtomicBoolean closed;
    private final CompletableFuture<Void> closeFuture;
    private final ConnectionFactory connectionFactory;

    public ConnectionPoolImpl( ChannelConnector connector, Bootstrap bootstrap, PoolSettings settings, MetricsListener metricsListener, Logging logging,
            Clock clock, boolean ownsEventLoopGroup )
    {
        this( connector, bootstrap, new NettyChannelTracker( metricsListener, bootstrap.config().group().next(), logging ), settings, metricsListener, logging,
                clock, ownsEventLoopGroup, new NetworkConnectionFactory( clock, metricsListener ) );
    }

    public ConnectionPoolImpl( ChannelConnector connector, Bootstrap bootstrap, NettyChannelTracker nettyChannelTracker, PoolSettings settings,
            MetricsListener metricsListener, Logging logging, Clock clock, boolean ownsEventLoopGroup, ConnectionFactory connectionFactory )
    {
        this.pools = new ConcurrentHashMap();
        this.closed = new AtomicBoolean();
        this.closeFuture = new CompletableFuture();
        this.connector = connector;
        this.bootstrap = bootstrap;
        this.nettyChannelTracker = nettyChannelTracker;
        this.channelHealthChecker = new NettyChannelHealthChecker( settings, clock, logging );
        this.settings = settings;
        this.metricsListener = metricsListener;
        this.log = logging.getLog( ConnectionPool.class.getSimpleName() );
        this.ownsEventLoopGroup = ownsEventLoopGroup;
        this.connectionFactory = connectionFactory;
    }

    public CompletionStage<Connection> acquire( BoltServerAddress address )
    {
        this.log.trace( "Acquiring a connection from pool towards %s", address );
        this.assertNotClosed();
        ExtendedChannelPool pool = this.getOrCreatePool( address );
        ListenerEvent acquireEvent = this.metricsListener.createListenerEvent();
        this.metricsListener.beforeAcquiringOrCreating( pool.id(), acquireEvent );
        CompletionStage<Channel> channelFuture = pool.acquire();
        return channelFuture.handle( ( channel, error ) -> {
            Connection var7;
            try
            {
                this.processAcquisitionError( pool, address, error );
                this.assertNotClosed( address, channel, pool );
                Connection connection = this.connectionFactory.createConnection( channel, pool );
                this.metricsListener.afterAcquiredOrCreated( pool.id(), acquireEvent );
                var7 = connection;
            }
            finally
            {
                this.metricsListener.afterAcquiringOrCreating( pool.id() );
            }

            return var7;
        } );
    }

    public void retainAll( Set<BoltServerAddress> addressesToRetain )
    {
        Iterator var2 = this.pools.keySet().iterator();

        while ( var2.hasNext() )
        {
            BoltServerAddress address = (BoltServerAddress) var2.next();
            if ( !addressesToRetain.contains( address ) )
            {
                int activeChannels = this.nettyChannelTracker.inUseChannelCount( address );
                if ( activeChannels == 0 )
                {
                    ExtendedChannelPool pool = (ExtendedChannelPool) this.pools.remove( address );
                    if ( pool != null )
                    {
                        this.log.info( "Closing connection pool towards %s, it has no active connections and is not in the routing table registry.", address );
                        this.closePoolInBackground( address, pool );
                    }
                }
            }
        }
    }

    public int inUseConnections( BoltServerAddress address )
    {
        return this.nettyChannelTracker.inUseChannelCount( address );
    }

    public int idleConnections( BoltServerAddress address )
    {
        return this.nettyChannelTracker.idleChannelCount( address );
    }

    public CompletionStage<Void> close()
    {
        if ( this.closed.compareAndSet( false, true ) )
        {
            this.nettyChannelTracker.prepareToCloseChannels();
            CompletableFuture<Void> allPoolClosedFuture = this.closeAllPools();
            allPoolClosedFuture.whenComplete( ( ignored, pollCloseError ) -> {
                this.pools.clear();
                if ( !this.ownsEventLoopGroup )
                {
                    Futures.completeWithNullIfNoError( this.closeFuture, pollCloseError );
                }
                else
                {
                    this.shutdownEventLoopGroup( pollCloseError );
                }
            } );
        }

        return this.closeFuture;
    }

    public boolean isOpen( BoltServerAddress address )
    {
        return this.pools.containsKey( address );
    }

    public String toString()
    {
        return "ConnectionPoolImpl{pools=" + this.pools + '}';
    }

    private void processAcquisitionError( ExtendedChannelPool pool, BoltServerAddress serverAddress, Throwable error )
    {
        Throwable cause = Futures.completionExceptionCause( error );
        if ( cause != null )
        {
            if ( cause instanceof TimeoutException )
            {
                this.metricsListener.afterTimedOutToAcquireOrCreate( pool.id() );
                throw new ClientException(
                        "Unable to acquire connection from the pool within configured maximum time of " + this.settings.connectionAcquisitionTimeout() + "ms" );
            }
            else if ( pool.isClosed() )
            {
                throw new ServiceUnavailableException( String.format( "Connection pool for server %s is closed while acquiring a connection.", serverAddress ),
                        cause );
            }
            else
            {
                throw new CompletionException( cause );
            }
        }
    }

    private void assertNotClosed()
    {
        if ( this.closed.get() )
        {
            throw new IllegalStateException( "Pool closed" );
        }
    }

    private void assertNotClosed( BoltServerAddress address, Channel channel, ExtendedChannelPool pool )
    {
        if ( this.closed.get() )
        {
            pool.release( channel );
            this.closePoolInBackground( address, pool );
            this.pools.remove( address );
            this.assertNotClosed();
        }
    }

    ExtendedChannelPool getPool( BoltServerAddress address )
    {
        return (ExtendedChannelPool) this.pools.get( address );
    }

    ExtendedChannelPool newPool( BoltServerAddress address )
    {
        return new NettyChannelPool( address, this.connector, this.bootstrap, this.nettyChannelTracker, this.channelHealthChecker,
                this.settings.connectionAcquisitionTimeout(), this.settings.maxConnectionPoolSize() );
    }

    private ExtendedChannelPool getOrCreatePool( BoltServerAddress address )
    {
        return (ExtendedChannelPool) this.pools.computeIfAbsent( address, ( ignored ) -> {
            ExtendedChannelPool pool = this.newPool( address );
            this.metricsListener.putPoolMetrics( pool.id(), address, this );
            return pool;
        } );
    }

    private CompletionStage<Void> closePool( ExtendedChannelPool pool )
    {
        return pool.close().whenComplete( ( ignored, error ) -> {
            this.metricsListener.removePoolMetrics( pool.id() );
        } );
    }

    private void closePoolInBackground( BoltServerAddress address, ExtendedChannelPool pool )
    {
        this.closePool( pool ).whenComplete( ( ignored, error ) -> {
            if ( error != null )
            {
                this.log.warn( String.format( "An error occurred while closing connection pool towards %s.", address ), error );
            }
        } );
    }

    private EventLoopGroup eventLoopGroup()
    {
        return this.bootstrap.config().group();
    }

    private void shutdownEventLoopGroup( Throwable pollCloseError )
    {
        this.eventLoopGroup().shutdownGracefully( 200L, 15000L, TimeUnit.MILLISECONDS );
        Futures.asCompletionStage( this.eventLoopGroup().terminationFuture() ).whenComplete( ( ignore, eventLoopGroupTerminationError ) -> {
            CompletionException combinedErrors = Futures.combineErrors( pollCloseError, eventLoopGroupTerminationError );
            Futures.completeWithNullIfNoError( this.closeFuture, combinedErrors );
        } );
    }

    private CompletableFuture<Void> closeAllPools()
    {
        return CompletableFuture.allOf( (CompletableFuture[]) this.pools.entrySet().stream().map( ( entry ) -> {
            BoltServerAddress address = (BoltServerAddress) entry.getKey();
            ExtendedChannelPool pool = (ExtendedChannelPool) entry.getValue();
            this.log.info( "Closing connection pool towards %s", address );
            return this.closePool( pool ).toCompletableFuture();
        } ).toArray( ( x$0 ) -> {
            return new CompletableFuture[x$0];
        } ) );
    }
}
