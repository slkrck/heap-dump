package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.InboundMessageDispatcher;
import com.neo4j.fabric.shaded.driver.internal.security.SecurityPlan;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelInitializer;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.ssl.SslHandler;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;

public class NettyChannelInitializer extends ChannelInitializer<Channel>
{
    private final BoltServerAddress address;
    private final SecurityPlan securityPlan;
    private final int connectTimeoutMillis;
    private final Clock clock;
    private final Logging logging;

    public NettyChannelInitializer( BoltServerAddress address, SecurityPlan securityPlan, int connectTimeoutMillis, Clock clock, Logging logging )
    {
        this.address = address;
        this.securityPlan = securityPlan;
        this.connectTimeoutMillis = connectTimeoutMillis;
        this.clock = clock;
        this.logging = logging;
    }

    protected void initChannel( Channel channel )
    {
        if ( this.securityPlan.requiresEncryption() )
        {
            SslHandler sslHandler = this.createSslHandler();
            channel.pipeline().addFirst( sslHandler );
        }

        this.updateChannelAttributes( channel );
    }

    private SslHandler createSslHandler()
    {
        SSLEngine sslEngine = this.createSslEngine();
        SslHandler sslHandler = new SslHandler( sslEngine );
        sslHandler.setHandshakeTimeoutMillis( (long) this.connectTimeoutMillis );
        return sslHandler;
    }

    private SSLEngine createSslEngine()
    {
        SSLContext sslContext = this.securityPlan.sslContext();
        SSLEngine sslEngine = sslContext.createSSLEngine( this.address.host(), this.address.port() );
        sslEngine.setUseClientMode( true );
        if ( this.securityPlan.requiresHostnameVerification() )
        {
            SSLParameters sslParameters = sslEngine.getSSLParameters();
            sslParameters.setEndpointIdentificationAlgorithm( "HTTPS" );
            sslEngine.setSSLParameters( sslParameters );
        }

        return sslEngine;
    }

    private void updateChannelAttributes( Channel channel )
    {
        ChannelAttributes.setServerAddress( channel, this.address );
        ChannelAttributes.setCreationTimestamp( channel, this.clock.millis() );
        ChannelAttributes.setMessageDispatcher( channel, new InboundMessageDispatcher( channel, this.logging ) );
    }
}
