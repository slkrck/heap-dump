package com.neo4j.fabric.shaded.driver.internal.async.outbound;

import com.neo4j.fabric.shaded.driver.internal.async.connection.BoltProtocolUtil;
import com.neo4j.fabric.shaded.driver.internal.packstream.PackOutput;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;

import java.util.Objects;

public class ChunkAwareByteBufOutput implements PackOutput
{
    private final int maxChunkSize;
    private ByteBuf buf;
    private int currentChunkStartIndex;
    private int currentChunkSize;

    public ChunkAwareByteBufOutput()
    {
        this( 16383 );
    }

    ChunkAwareByteBufOutput( int maxChunkSize )
    {
        this.maxChunkSize = verifyMaxChunkSize( maxChunkSize );
    }

    private static int verifyMaxChunkSize( int maxChunkSize )
    {
        if ( maxChunkSize <= 0 )
        {
            throw new IllegalArgumentException( "Max chunk size should be > 0, given: " + maxChunkSize );
        }
        else
        {
            return maxChunkSize;
        }
    }

    public void start( ByteBuf newBuf )
    {
        this.assertNotStarted();
        this.buf = (ByteBuf) Objects.requireNonNull( newBuf );
        this.startNewChunk( 0 );
    }

    public void stop()
    {
        this.writeChunkSizeHeader();
        this.buf = null;
        this.currentChunkStartIndex = 0;
        this.currentChunkSize = 0;
    }

    public PackOutput writeByte( byte value )
    {
        this.ensureCanFitInCurrentChunk( 1 );
        this.buf.writeByte( value );
        ++this.currentChunkSize;
        return this;
    }

    public PackOutput writeBytes( byte[] data )
    {
        int offset = 0;

        int amountToWrite;
        for ( int length = data.length; offset < length; offset += amountToWrite )
        {
            this.ensureCanFitInCurrentChunk( 1 );
            amountToWrite = Math.min( this.availableBytesInCurrentChunk(), length - offset );
            this.buf.writeBytes( data, offset, amountToWrite );
            this.currentChunkSize += amountToWrite;
        }

        return this;
    }

    public PackOutput writeShort( short value )
    {
        this.ensureCanFitInCurrentChunk( 2 );
        this.buf.writeShort( value );
        this.currentChunkSize += 2;
        return this;
    }

    public PackOutput writeInt( int value )
    {
        this.ensureCanFitInCurrentChunk( 4 );
        this.buf.writeInt( value );
        this.currentChunkSize += 4;
        return this;
    }

    public PackOutput writeLong( long value )
    {
        this.ensureCanFitInCurrentChunk( 8 );
        this.buf.writeLong( value );
        this.currentChunkSize += 8;
        return this;
    }

    public PackOutput writeDouble( double value )
    {
        this.ensureCanFitInCurrentChunk( 8 );
        this.buf.writeDouble( value );
        this.currentChunkSize += 8;
        return this;
    }

    private void ensureCanFitInCurrentChunk( int numberOfBytes )
    {
        int targetChunkSize = this.currentChunkSize + numberOfBytes;
        if ( targetChunkSize > this.maxChunkSize )
        {
            this.writeChunkSizeHeader();
            this.startNewChunk( this.buf.writerIndex() );
        }
    }

    private void startNewChunk( int index )
    {
        this.currentChunkStartIndex = index;
        BoltProtocolUtil.writeEmptyChunkHeader( this.buf );
        this.currentChunkSize = 2;
    }

    private void writeChunkSizeHeader()
    {
        int chunkBodySize = this.currentChunkSize - 2;
        BoltProtocolUtil.writeChunkHeader( this.buf, this.currentChunkStartIndex, chunkBodySize );
    }

    private int availableBytesInCurrentChunk()
    {
        return this.maxChunkSize - this.currentChunkSize;
    }

    private void assertNotStarted()
    {
        if ( this.buf != null )
        {
            throw new IllegalStateException( "Already started" );
        }
    }
}
