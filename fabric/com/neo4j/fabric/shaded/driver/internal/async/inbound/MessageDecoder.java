package com.neo4j.fabric.shaded.driver.internal.async.inbound;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class MessageDecoder extends ByteToMessageDecoder
{
    private static final ByteToMessageDecoder.Cumulator DEFAULT_CUMULATOR = determineDefaultCumulator();
    private boolean readMessageBoundary;

    public MessageDecoder()
    {
        this.setCumulator( DEFAULT_CUMULATOR );
    }

    private static ByteToMessageDecoder.Cumulator determineDefaultCumulator()
    {
        String value = System.getProperty( "messageDecoderCumulator", "" );
        return "merge".equals( value ) ? MERGE_CUMULATOR : COMPOSITE_CUMULATOR;
    }

    public void channelRead( ChannelHandlerContext ctx, Object msg ) throws Exception
    {
        if ( msg instanceof ByteBuf )
        {
            this.readMessageBoundary = ((ByteBuf) msg).readableBytes() == 0;
        }

        super.channelRead( ctx, msg );
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out )
    {
        if ( this.readMessageBoundary )
        {
            ByteBuf messageBuf = in.retainedDuplicate();
            in.readerIndex( in.readableBytes() );
            out.add( messageBuf );
            this.readMessageBoundary = false;
        }
    }
}
