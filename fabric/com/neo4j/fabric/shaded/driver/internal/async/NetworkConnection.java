package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.InboundMessageDispatcher;
import com.neo4j.fabric.shaded.driver.internal.async.pool.ExtendedChannelPool;
import com.neo4j.fabric.shaded.driver.internal.handlers.ChannelReleasingResetResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.ResetResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.ResetMessage;
import com.neo4j.fabric.shaded.driver.internal.metrics.ListenerEvent;
import com.neo4j.fabric.shaded.driver.internal.metrics.MetricsListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoop;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicReference;

public class NetworkConnection implements Connection
{
    private final Channel channel;
    private final InboundMessageDispatcher messageDispatcher;
    private final BoltServerAddress serverAddress;
    private final ServerVersion serverVersion;
    private final BoltProtocol protocol;
    private final ExtendedChannelPool channelPool;
    private final CompletableFuture<Void> releaseFuture;
    private final Clock clock;
    private final AtomicReference<NetworkConnection.Status> status;
    private final MetricsListener metricsListener;
    private final ListenerEvent inUseEvent;

    public NetworkConnection( Channel channel, ExtendedChannelPool channelPool, Clock clock, MetricsListener metricsListener )
    {
        this.status = new AtomicReference( NetworkConnection.Status.OPEN );
        this.channel = channel;
        this.messageDispatcher = ChannelAttributes.messageDispatcher( channel );
        this.serverAddress = ChannelAttributes.serverAddress( channel );
        this.serverVersion = ChannelAttributes.serverVersion( channel );
        this.protocol = BoltProtocol.forChannel( channel );
        this.channelPool = channelPool;
        this.releaseFuture = new CompletableFuture();
        this.clock = clock;
        this.metricsListener = metricsListener;
        this.inUseEvent = metricsListener.createListenerEvent();
        metricsListener.afterConnectionCreated( ChannelAttributes.poolId( this.channel ), this.inUseEvent );
    }

    public boolean isOpen()
    {
        return this.status.get() == NetworkConnection.Status.OPEN;
    }

    public void enableAutoRead()
    {
        if ( this.isOpen() )
        {
            this.setAutoRead( true );
        }
    }

    public void disableAutoRead()
    {
        if ( this.isOpen() )
        {
            this.setAutoRead( false );
        }
    }

    public void flush()
    {
        if ( this.verifyOpen( (ResponseHandler) null, (ResponseHandler) null ) )
        {
            this.flushInEventLoop();
        }
    }

    public void write( Message message, ResponseHandler handler )
    {
        if ( this.verifyOpen( handler, (ResponseHandler) null ) )
        {
            this.writeMessageInEventLoop( message, handler, false );
        }
    }

    public void write( Message message1, ResponseHandler handler1, Message message2, ResponseHandler handler2 )
    {
        if ( this.verifyOpen( handler1, handler2 ) )
        {
            this.writeMessagesInEventLoop( message1, handler1, message2, handler2, false );
        }
    }

    public void writeAndFlush( Message message, ResponseHandler handler )
    {
        if ( this.verifyOpen( handler, (ResponseHandler) null ) )
        {
            this.writeMessageInEventLoop( message, handler, true );
        }
    }

    public void writeAndFlush( Message message1, ResponseHandler handler1, Message message2, ResponseHandler handler2 )
    {
        if ( this.verifyOpen( handler1, handler2 ) )
        {
            this.writeMessagesInEventLoop( message1, handler1, message2, handler2, true );
        }
    }

    public CompletionStage<Void> reset()
    {
        CompletableFuture<Void> result = new CompletableFuture();
        ResetResponseHandler handler = new ResetResponseHandler( this.messageDispatcher, result );
        this.writeResetMessageIfNeeded( handler, true );
        return result;
    }

    public CompletionStage<Void> release()
    {
        if ( this.status.compareAndSet( NetworkConnection.Status.OPEN, NetworkConnection.Status.RELEASED ) )
        {
            ChannelReleasingResetResponseHandler handler =
                    new ChannelReleasingResetResponseHandler( this.channel, this.channelPool, this.messageDispatcher, this.clock, this.releaseFuture );
            this.writeResetMessageIfNeeded( handler, false );
            this.metricsListener.afterConnectionReleased( ChannelAttributes.poolId( this.channel ), this.inUseEvent );
        }

        return this.releaseFuture;
    }

    public void terminateAndRelease( String reason )
    {
        if ( this.status.compareAndSet( NetworkConnection.Status.OPEN, NetworkConnection.Status.TERMINATED ) )
        {
            ChannelAttributes.setTerminationReason( this.channel, reason );
            this.channel.close();
            this.channelPool.release( this.channel );
            this.releaseFuture.complete( (Object) null );
            this.metricsListener.afterConnectionReleased( ChannelAttributes.poolId( this.channel ), this.inUseEvent );
        }
    }

    public BoltServerAddress serverAddress()
    {
        return this.serverAddress;
    }

    public ServerVersion serverVersion()
    {
        return this.serverVersion;
    }

    public BoltProtocol protocol()
    {
        return this.protocol;
    }

    private void writeResetMessageIfNeeded( ResponseHandler resetHandler, boolean isSessionReset )
    {
        this.channel.eventLoop().execute( () -> {
            if ( isSessionReset && !this.isOpen() )
            {
                resetHandler.onSuccess( Collections.emptyMap() );
            }
            else
            {
                this.setAutoRead( true );
                this.messageDispatcher.enqueue( resetHandler );
                this.channel.writeAndFlush( ResetMessage.RESET, this.channel.voidPromise() );
            }
        } );
    }

    private void flushInEventLoop()
    {
        EventLoop var10000 = this.channel.eventLoop();
        Channel var10001 = this.channel;
        var10000.execute( var10001::flush );
    }

    private void writeMessageInEventLoop( Message message, ResponseHandler handler, boolean flush )
    {
        this.channel.eventLoop().execute( () -> {
            this.messageDispatcher.enqueue( handler );
            if ( flush )
            {
                this.channel.writeAndFlush( message, this.channel.voidPromise() );
            }
            else
            {
                this.channel.write( message, this.channel.voidPromise() );
            }
        } );
    }

    private void writeMessagesInEventLoop( Message message1, ResponseHandler handler1, Message message2, ResponseHandler handler2, boolean flush )
    {
        this.channel.eventLoop().execute( () -> {
            this.messageDispatcher.enqueue( handler1 );
            this.messageDispatcher.enqueue( handler2 );
            this.channel.write( message1, this.channel.voidPromise() );
            if ( flush )
            {
                this.channel.writeAndFlush( message2, this.channel.voidPromise() );
            }
            else
            {
                this.channel.write( message2, this.channel.voidPromise() );
            }
        } );
    }

    private void setAutoRead( boolean value )
    {
        this.channel.config().setAutoRead( value );
    }

    private boolean verifyOpen( ResponseHandler handler1, ResponseHandler handler2 )
    {
        NetworkConnection.Status connectionStatus = (NetworkConnection.Status) this.status.get();
        switch ( connectionStatus )
        {
        case OPEN:
            return true;
        case RELEASED:
            Exception error = new IllegalStateException( "Connection has been released to the pool and can't be used" );
            if ( handler1 != null )
            {
                handler1.onFailure( error );
            }

            if ( handler2 != null )
            {
                handler2.onFailure( error );
            }

            return false;
        case TERMINATED:
            Exception terminatedError = new IllegalStateException( "Connection has been terminated and can't be used" );
            if ( handler1 != null )
            {
                handler1.onFailure( terminatedError );
            }

            if ( handler2 != null )
            {
                handler2.onFailure( terminatedError );
            }

            return false;
        default:
            throw new IllegalStateException( "Unknown status: " + connectionStatus );
        }
    }

    private static enum Status
    {
        OPEN,
        RELEASED,
        TERMINATED;
    }
}
