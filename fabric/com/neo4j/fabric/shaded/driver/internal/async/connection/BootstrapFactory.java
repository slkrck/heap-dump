package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.Bootstrap;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelOption;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.EventLoopGroup;

public final class BootstrapFactory
{
    private BootstrapFactory()
    {
    }

    public static Bootstrap newBootstrap( int threadCount )
    {
        return newBootstrap( EventLoopGroupFactory.newEventLoopGroup( threadCount ) );
    }

    public static Bootstrap newBootstrap( EventLoopGroup eventLoopGroup )
    {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group( eventLoopGroup );
        bootstrap.channel( EventLoopGroupFactory.channelClass() );
        bootstrap.option( ChannelOption.SO_KEEPALIVE, true );
        bootstrap.option( ChannelOption.SO_REUSEADDR, true );
        return bootstrap;
    }
}
