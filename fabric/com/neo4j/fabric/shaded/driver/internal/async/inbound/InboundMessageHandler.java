package com.neo4j.fabric.shaded.driver.internal.async.inbound;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.logging.ChannelActivityLogger;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBufUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.SimpleChannelInboundHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.DecoderException;

import java.util.Objects;

public class InboundMessageHandler extends SimpleChannelInboundHandler<ByteBuf>
{
    private final ByteBufInput input = new ByteBufInput();
    private final MessageFormat.Reader reader;
    private final Logging logging;
    private InboundMessageDispatcher messageDispatcher;
    private Logger log;

    public InboundMessageHandler( MessageFormat messageFormat, Logging logging )
    {
        this.reader = messageFormat.newReader( this.input );
        this.logging = logging;
    }

    public void handlerAdded( ChannelHandlerContext ctx )
    {
        this.messageDispatcher = (InboundMessageDispatcher) Objects.requireNonNull( ChannelAttributes.messageDispatcher( ctx.channel() ) );
        this.log = new ChannelActivityLogger( ctx.channel(), this.logging, this.getClass() );
    }

    public void handlerRemoved( ChannelHandlerContext ctx )
    {
        this.messageDispatcher = null;
        this.log = null;
    }

    protected void channelRead0( ChannelHandlerContext ctx, ByteBuf msg )
    {
        if ( this.messageDispatcher.fatalErrorOccurred() )
        {
            this.log.warn( "Message ignored because of the previous fatal error. Channel will be closed. Message:\n%s", ByteBufUtil.hexDump( msg ) );
        }
        else
        {
            if ( this.log.isTraceEnabled() )
            {
                this.log.trace( "S: %s", ByteBufUtil.hexDump( msg ) );
            }

            this.input.start( msg );

            try
            {
                this.reader.read( this.messageDispatcher );
            }
            catch ( Throwable var7 )
            {
                throw new DecoderException( "Failed to read inbound message:\n" + ByteBufUtil.hexDump( msg ) + "\n", var7 );
            }
            finally
            {
                this.input.stop();
            }
        }
    }
}
