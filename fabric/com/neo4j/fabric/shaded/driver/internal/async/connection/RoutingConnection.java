package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.RoutingErrorHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.RoutingResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.concurrent.CompletionStage;

public class RoutingConnection implements Connection
{
    private final Connection delegate;
    private final AccessMode accessMode;
    private final RoutingErrorHandler errorHandler;
    private final DatabaseName databaseName;

    public RoutingConnection( Connection delegate, DatabaseName databaseName, AccessMode accessMode, RoutingErrorHandler errorHandler )
    {
        this.delegate = delegate;
        this.databaseName = databaseName;
        this.accessMode = accessMode;
        this.errorHandler = errorHandler;
    }

    public void enableAutoRead()
    {
        this.delegate.enableAutoRead();
    }

    public void disableAutoRead()
    {
        this.delegate.disableAutoRead();
    }

    public void write( Message message, ResponseHandler handler )
    {
        this.delegate.write( message, this.newRoutingResponseHandler( handler ) );
    }

    public void write( Message message1, ResponseHandler handler1, Message message2, ResponseHandler handler2 )
    {
        this.delegate.write( message1, this.newRoutingResponseHandler( handler1 ), message2, this.newRoutingResponseHandler( handler2 ) );
    }

    public void writeAndFlush( Message message, ResponseHandler handler )
    {
        this.delegate.writeAndFlush( message, this.newRoutingResponseHandler( handler ) );
    }

    public void writeAndFlush( Message message1, ResponseHandler handler1, Message message2, ResponseHandler handler2 )
    {
        this.delegate.writeAndFlush( message1, this.newRoutingResponseHandler( handler1 ), message2, this.newRoutingResponseHandler( handler2 ) );
    }

    public CompletionStage<Void> reset()
    {
        return this.delegate.reset();
    }

    public boolean isOpen()
    {
        return this.delegate.isOpen();
    }

    public CompletionStage<Void> release()
    {
        return this.delegate.release();
    }

    public void terminateAndRelease( String reason )
    {
        this.delegate.terminateAndRelease( reason );
    }

    public BoltServerAddress serverAddress()
    {
        return this.delegate.serverAddress();
    }

    public ServerVersion serverVersion()
    {
        return this.delegate.serverVersion();
    }

    public BoltProtocol protocol()
    {
        return this.delegate.protocol();
    }

    public void flush()
    {
        this.delegate.flush();
    }

    public AccessMode mode()
    {
        return this.accessMode;
    }

    public DatabaseName databaseName()
    {
        return this.databaseName;
    }

    private RoutingResponseHandler newRoutingResponseHandler( ResponseHandler handler )
    {
        return new RoutingResponseHandler( handler, this.serverAddress(), this.accessMode, this.errorHandler );
    }
}
