package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.TransactionNestingException;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.FailableCursor;
import com.neo4j.fabric.shaded.driver.internal.cursor.AsyncResultCursor;
import com.neo4j.fabric.shaded.driver.internal.cursor.ResultCursorFactory;
import com.neo4j.fabric.shaded.driver.internal.cursor.RxResultCursor;
import com.neo4j.fabric.shaded.driver.internal.logging.PrefixedLogger;
import com.neo4j.fabric.shaded.driver.internal.retry.RetryLogic;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionProvider;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicBoolean;

public class NetworkSession
{
    private static final String LOG_NAME = "Session";
    protected final Logger logger;
    private final ConnectionProvider connectionProvider;
    private final NetworkSession.NetworkSessionConnectionContext connectionContext;
    private final AccessMode mode;
    private final RetryLogic retryLogic;
    private final BookmarkHolder bookmarkHolder;
    private final long fetchSize;
    private final AtomicBoolean open = new AtomicBoolean( true );
    private volatile CompletionStage<UnmanagedTransaction> transactionStage = Futures.completedWithNull();
    private volatile CompletionStage<Connection> connectionStage = Futures.completedWithNull();
    private volatile CompletionStage<? extends FailableCursor> resultCursorStage = Futures.completedWithNull();

    public NetworkSession( ConnectionProvider connectionProvider, RetryLogic retryLogic, DatabaseName databaseName, AccessMode mode,
            BookmarkHolder bookmarkHolder, long fetchSize, Logging logging )
    {
        this.connectionProvider = connectionProvider;
        this.mode = mode;
        this.retryLogic = retryLogic;
        this.logger = new PrefixedLogger( "[" + this.hashCode() + "]", logging.getLog( "Session" ) );
        this.bookmarkHolder = bookmarkHolder;
        this.connectionContext = new NetworkSession.NetworkSessionConnectionContext( databaseName, bookmarkHolder.getBookmark() );
        this.fetchSize = fetchSize;
    }

    public CompletionStage<ResultCursor> runAsync( Query query, TransactionConfig config, boolean waitForRunResponse )
    {
        CompletionStage<AsyncResultCursor> newResultCursorStage =
                this.buildResultCursorFactory( query, config, waitForRunResponse ).thenCompose( ResultCursorFactory::asyncResult );
        this.resultCursorStage = newResultCursorStage.exceptionally( ( error ) -> {
            return null;
        } );
        return newResultCursorStage.thenApply( ( cursor ) -> {
            return cursor;
        } );
    }

    public CompletionStage<RxResultCursor> runRx( Query query, TransactionConfig config )
    {
        CompletionStage<RxResultCursor> newResultCursorStage =
                this.buildResultCursorFactory( query, config, true ).thenCompose( ResultCursorFactory::rxResult );
        this.resultCursorStage = newResultCursorStage.exceptionally( ( error ) -> {
            return null;
        } );
        return newResultCursorStage;
    }

    public CompletionStage<UnmanagedTransaction> beginTransactionAsync( TransactionConfig config )
    {
        return this.beginTransactionAsync( this.mode, config );
    }

    public CompletionStage<UnmanagedTransaction> beginTransactionAsync( AccessMode mode, TransactionConfig config )
    {
        this.ensureSessionIsOpen();
        CompletionStage<UnmanagedTransaction> newTransactionStage = this.ensureNoOpenTxBeforeStartingTx().thenCompose( ( ignore ) -> {
            return this.acquireConnection( mode );
        } ).thenCompose( ( connection ) -> {
            UnmanagedTransaction tx = new UnmanagedTransaction( connection, this.bookmarkHolder, this.fetchSize );
            return tx.beginAsync( this.bookmarkHolder.getBookmark(), config );
        } );
        CompletionStage<UnmanagedTransaction> currentTransactionStage = this.transactionStage;
        this.transactionStage = newTransactionStage.exceptionally( ( error ) -> {
            return null;
        } ).thenCompose( ( tx ) -> {
            return (CompletionStage) (tx == null ? currentTransactionStage : CompletableFuture.completedFuture( tx ));
        } );
        return newTransactionStage;
    }

    public CompletionStage<Void> resetAsync()
    {
        return this.existingTransactionOrNull().thenAccept( ( tx ) -> {
            if ( tx != null )
            {
                tx.markTerminated();
            }
        } ).thenCompose( ( ignore ) -> {
            return this.connectionStage;
        } ).thenCompose( ( connection ) -> {
            return (CompletionStage) (connection != null ? connection.reset() : Futures.completedWithNull());
        } );
    }

    public RetryLogic retryLogic()
    {
        return this.retryLogic;
    }

    public Bookmark lastBookmark()
    {
        return this.bookmarkHolder.getBookmark();
    }

    public CompletionStage<Void> releaseConnectionAsync()
    {
        return this.connectionStage.thenCompose( ( connection ) -> {
            return (CompletionStage) (connection != null ? connection.release() : Futures.completedWithNull());
        } );
    }

    public CompletionStage<Connection> connectionAsync()
    {
        return this.connectionStage;
    }

    public boolean isOpen()
    {
        return this.open.get();
    }

    public CompletionStage<Void> closeAsync()
    {
        return (CompletionStage) (this.open.compareAndSet( true, false ) ? this.resultCursorStage.thenCompose( ( cursor ) -> {
            return (CompletionStage) (cursor != null ? cursor.discardAllFailureAsync() : Futures.completedWithNull());
        } ).thenCompose( ( cursorError ) -> {
            return this.closeTransactionAndReleaseConnection().thenApply( ( txCloseError ) -> {
                CompletionException combinedError = Futures.combineErrors( cursorError, txCloseError );
                if ( combinedError != null )
                {
                    throw combinedError;
                }
                else
                {
                    return null;
                }
            } );
        } ) : Futures.completedWithNull());
    }

    protected CompletionStage<Boolean> currentConnectionIsOpen()
    {
        return this.connectionStage.handle( ( connection, error ) -> {
            return error == null && connection != null && connection.isOpen();
        } );
    }

    private CompletionStage<ResultCursorFactory> buildResultCursorFactory( Query query, TransactionConfig config, boolean waitForRunResponse )
    {
        this.ensureSessionIsOpen();
        return this.ensureNoOpenTxBeforeRunningQuery().thenCompose( ( ignore ) -> {
            return this.acquireConnection( this.mode );
        } ).thenCompose( ( connection ) -> {
            try
            {
                ResultCursorFactory factory =
                        connection.protocol().runInAutoCommitTransaction( connection, query, this.bookmarkHolder, config, waitForRunResponse, this.fetchSize );
                return CompletableFuture.completedFuture( factory );
            }
            catch ( Throwable var6 )
            {
                return Futures.failedFuture( var6 );
            }
        } );
    }

    private CompletionStage<Connection> acquireConnection( AccessMode mode )
    {
        CompletionStage<Connection> currentConnectionStage = this.connectionStage;
        CompletionStage<Connection> newConnectionStage = this.resultCursorStage.thenCompose( ( cursor ) -> {
            return (CompletionStage) (cursor == null ? Futures.completedWithNull() : cursor.pullAllFailureAsync());
        } ).thenCompose( ( error ) -> {
            if ( error == null )
            {
                return currentConnectionStage.exceptionally( ( ignore ) -> {
                    return null;
                } );
            }
            else
            {
                throw new CompletionException( error );
            }
        } ).thenCompose( ( existingConnection ) -> {
            if ( existingConnection != null && existingConnection.isOpen() )
            {
                throw new IllegalStateException( "Existing open connection detected" );
            }
            else
            {
                return this.connectionProvider.acquireConnection( this.connectionContext.contextWithMode( mode ) );
            }
        } );
        this.connectionStage = newConnectionStage.exceptionally( ( error ) -> {
            return null;
        } );
        return newConnectionStage;
    }

    private CompletionStage<Throwable> closeTransactionAndReleaseConnection()
    {
        return this.existingTransactionOrNull().thenCompose( ( tx ) -> {
            return (CompletionStage) (tx != null ? tx.closeAsync().thenApply( ( ignore ) -> {
                return (Throwable) null;
            } ).exceptionally( ( error ) -> {
                return error;
            } ) : Futures.completedWithNull());
        } ).thenCompose( ( txCloseError ) -> {
            return this.releaseConnectionAsync().thenApply( ( ignore ) -> {
                return txCloseError;
            } );
        } );
    }

    private CompletionStage<Void> ensureNoOpenTxBeforeRunningQuery()
    {
        return this.ensureNoOpenTx(
                "Queries cannot be run directly on a session with an open transaction; either run from within the transaction or use a different session." );
    }

    private CompletionStage<Void> ensureNoOpenTxBeforeStartingTx()
    {
        return this.ensureNoOpenTx(
                "You cannot begin a transaction on a session with an open transaction; either run from within the transaction or use a different session." );
    }

    private CompletionStage<Void> ensureNoOpenTx( String errorMessage )
    {
        return this.existingTransactionOrNull().thenAccept( ( tx ) -> {
            if ( tx != null )
            {
                throw new TransactionNestingException( errorMessage );
            }
        } );
    }

    private CompletionStage<UnmanagedTransaction> existingTransactionOrNull()
    {
        return this.transactionStage.exceptionally( ( error ) -> {
            return null;
        } ).thenApply( ( tx ) -> {
            return tx != null && tx.isOpen() ? tx : null;
        } );
    }

    private void ensureSessionIsOpen()
    {
        if ( !this.open.get() )
        {
            throw new ClientException( "No more interaction with this session are allowed as the current session is already closed. " );
        }
    }

    private static class NetworkSessionConnectionContext implements ConnectionContext
    {
        private final DatabaseName databaseName;
        private final Bookmark rediscoveryBookmark;
        private AccessMode mode;

        private NetworkSessionConnectionContext( DatabaseName databaseName, Bookmark bookmark )
        {
            this.databaseName = databaseName;
            this.rediscoveryBookmark = bookmark;
        }

        private ConnectionContext contextWithMode( AccessMode mode )
        {
            this.mode = mode;
            return this;
        }

        public DatabaseName databaseName()
        {
            return this.databaseName;
        }

        public AccessMode mode()
        {
            return this.mode;
        }

        public Bookmark rediscoveryBookmark()
        {
            return this.rediscoveryBookmark;
        }
    }
}
