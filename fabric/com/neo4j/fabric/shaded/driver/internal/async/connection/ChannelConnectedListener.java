package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.logging.ChannelActivityLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPipeline;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;

public class ChannelConnectedListener implements ChannelFutureListener
{
    private final BoltServerAddress address;
    private final ChannelPipelineBuilder pipelineBuilder;
    private final ChannelPromise handshakeCompletedPromise;
    private final Logging logging;

    public ChannelConnectedListener( BoltServerAddress address, ChannelPipelineBuilder pipelineBuilder, ChannelPromise handshakeCompletedPromise,
            Logging logging )
    {
        this.address = address;
        this.pipelineBuilder = pipelineBuilder;
        this.handshakeCompletedPromise = handshakeCompletedPromise;
        this.logging = logging;
    }

    private static Throwable databaseUnavailableError( BoltServerAddress address, Throwable cause )
    {
        return new ServiceUnavailableException(
                String.format( "Unable to connect to %s, ensure the database is running and that there is a working network connection to it.", address ),
                cause );
    }

    public void operationComplete( ChannelFuture future )
    {
        Channel channel = future.channel();
        Logger log = new ChannelActivityLogger( channel, this.logging, this.getClass() );
        if ( future.isSuccess() )
        {
            log.trace( "Channel %s connected, initiating bolt handshake", channel );
            ChannelPipeline pipeline = channel.pipeline();
            pipeline.addLast( new HandshakeHandler( this.pipelineBuilder, this.handshakeCompletedPromise, this.logging ) );
            log.debug( "C: [Bolt Handshake] %s", BoltProtocolUtil.handshakeString() );
            channel.writeAndFlush( BoltProtocolUtil.handshakeBuf(), channel.voidPromise() );
        }
        else
        {
            this.handshakeCompletedPromise.setFailure( databaseUnavailableError( this.address, future.cause() ) );
        }
    }
}
