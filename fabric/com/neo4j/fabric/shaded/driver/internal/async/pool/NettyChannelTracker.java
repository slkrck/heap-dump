package com.neo4j.fabric.shaded.driver.internal.async.pool;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.metrics.ListenerEvent;
import com.neo4j.fabric.shaded.driver.internal.metrics.MetricsListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.group.ChannelGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.group.DefaultChannelGroup;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.pool.ChannelPoolHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutor;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class NettyChannelTracker implements ChannelPoolHandler
{
    private final Map<BoltServerAddress,AtomicInteger> addressToInUseChannelCount;
    private final Map<BoltServerAddress,AtomicInteger> addressToIdleChannelCount;
    private final Logger log;
    private final MetricsListener metricsListener;
    private final ChannelFutureListener closeListener;
    private final ChannelGroup allChannels;

    public NettyChannelTracker( MetricsListener metricsListener, EventExecutor eventExecutor, Logging logging )
    {
        this( metricsListener, (ChannelGroup) (new DefaultChannelGroup( "all-connections", eventExecutor )), logging );
    }

    public NettyChannelTracker( MetricsListener metricsListener, ChannelGroup channels, Logging logging )
    {
        this.addressToInUseChannelCount = new ConcurrentHashMap();
        this.addressToIdleChannelCount = new ConcurrentHashMap();
        this.closeListener = ( future ) -> {
            this.channelClosed( future.channel() );
        };
        this.metricsListener = metricsListener;
        this.log = logging.getLog( this.getClass().getSimpleName() );
        this.allChannels = channels;
    }

    public void channelReleased( Channel channel )
    {
        this.log.debug( "Channel [0x%s] released back to the pool", channel.id() );
        this.decrementInUse( channel );
        this.incrementIdle( channel );
        channel.closeFuture().addListener( this.closeListener );
    }

    public void channelAcquired( Channel channel )
    {
        this.log.debug( "Channel [0x%s] acquired from the pool. Local address: %s, remote address: %s", channel.id(), channel.localAddress(),
                channel.remoteAddress() );
        this.incrementInUse( channel );
        this.decrementIdle( channel );
        channel.closeFuture().removeListener( this.closeListener );
    }

    public void channelCreated( Channel channel )
    {
        throw new IllegalStateException( "Untraceable channel created." );
    }

    public void channelCreated( Channel channel, ListenerEvent creatingEvent )
    {
        this.log.debug( "Channel [0x%s] created. Local address: %s, remote address: %s", channel.id(), channel.localAddress(), channel.remoteAddress() );
        this.incrementIdle( channel );
        this.metricsListener.afterCreated( ChannelAttributes.poolId( channel ), creatingEvent );
        this.allChannels.add( channel );
    }

    public ListenerEvent channelCreating( String poolId )
    {
        ListenerEvent creatingEvent = this.metricsListener.createListenerEvent();
        this.metricsListener.beforeCreating( poolId, creatingEvent );
        return creatingEvent;
    }

    public void channelFailedToCreate( String poolId )
    {
        this.metricsListener.afterFailedToCreate( poolId );
    }

    public void channelClosed( Channel channel )
    {
        this.decrementIdle( channel );
        this.metricsListener.afterClosed( ChannelAttributes.poolId( channel ) );
    }

    public int inUseChannelCount( BoltServerAddress address )
    {
        AtomicInteger count = (AtomicInteger) this.addressToInUseChannelCount.get( address );
        return count == null ? 0 : count.get();
    }

    public int idleChannelCount( BoltServerAddress address )
    {
        AtomicInteger count = (AtomicInteger) this.addressToIdleChannelCount.get( address );
        return count == null ? 0 : count.get();
    }

    public void prepareToCloseChannels()
    {
        Iterator var1 = this.allChannels.iterator();

        while ( var1.hasNext() )
        {
            Channel channel = (Channel) var1.next();
            BoltProtocol protocol = BoltProtocol.forChannel( channel );

            try
            {
                protocol.prepareToCloseChannel( channel );
            }
            catch ( Throwable var5 )
            {
                this.log.debug(
                        "Failed to prepare to close Channel %s due to error %s. It is safe to ignore this error as the channel will be closed despite if it is successfully prepared to close or not.",
                        channel, var5.getMessage() );
            }
        }
    }

    private void incrementInUse( Channel channel )
    {
        this.increment( channel, this.addressToInUseChannelCount );
    }

    private void decrementInUse( Channel channel )
    {
        this.decrement( channel, this.addressToInUseChannelCount );
    }

    private void incrementIdle( Channel channel )
    {
        this.increment( channel, this.addressToIdleChannelCount );
    }

    private void decrementIdle( Channel channel )
    {
        this.decrement( channel, this.addressToIdleChannelCount );
    }

    private void increment( Channel channel, Map<BoltServerAddress,AtomicInteger> countMap )
    {
        BoltServerAddress address = ChannelAttributes.serverAddress( channel );
        AtomicInteger count = (AtomicInteger) countMap.computeIfAbsent( address, ( k ) -> {
            return new AtomicInteger();
        } );
        count.incrementAndGet();
    }

    private void decrement( Channel channel, Map<BoltServerAddress,AtomicInteger> countMap )
    {
        BoltServerAddress address = ChannelAttributes.serverAddress( channel );
        AtomicInteger count = (AtomicInteger) countMap.get( address );
        if ( count == null )
        {
            throw new IllegalStateException( "No count exist for address '" + address + "'" );
        }
        else
        {
            count.decrementAndGet();
        }
    }
}
