package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.SecurityException;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.internal.logging.ChannelActivityLogger;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.DecoderException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.ReplayingDecoder;
import com.neo4j.fabric.shaded.driver.internal.util.ErrorUtil;

import java.util.List;
import javax.net.ssl.SSLHandshakeException;

public class HandshakeHandler extends ReplayingDecoder<Void>
{
    private final ChannelPipelineBuilder pipelineBuilder;
    private final ChannelPromise handshakeCompletedPromise;
    private final Logging logging;
    private boolean failed;
    private Logger log;

    public HandshakeHandler( ChannelPipelineBuilder pipelineBuilder, ChannelPromise handshakeCompletedPromise, Logging logging )
    {
        this.pipelineBuilder = pipelineBuilder;
        this.handshakeCompletedPromise = handshakeCompletedPromise;
        this.logging = logging;
    }

    private static Throwable protocolNoSupportedByServerError()
    {
        return new ClientException(
                "The server does not support any of the protocol versions supported by this driver. Ensure that you are using driver and server versions that are compatible with one another." );
    }

    private static Throwable httpEndpointError()
    {
        return new ClientException(
                "Server responded HTTP. Make sure you are not trying to connect to the http endpoint (HTTP defaults to port 7474 whereas BOLT defaults to port 7687)" );
    }

    private static Throwable protocolNoSupportedByDriverError( int suggestedProtocolVersion )
    {
        return new ClientException( "Protocol error, server suggested unexpected protocol version: " + suggestedProtocolVersion );
    }

    private static Throwable transformError( Throwable error )
    {
        if ( error instanceof DecoderException && error.getCause() != null )
        {
            error = error.getCause();
        }

        if ( error instanceof ServiceUnavailableException )
        {
            return error;
        }
        else
        {
            return (Throwable) (error instanceof SSLHandshakeException ? new SecurityException( "Failed to establish secured connection with the server",
                    error ) : new ServiceUnavailableException( "Failed to establish connection with the server", error ));
        }
    }

    public void handlerAdded( ChannelHandlerContext ctx )
    {
        this.log = new ChannelActivityLogger( ctx.channel(), this.logging, this.getClass() );
    }

    protected void handlerRemoved0( ChannelHandlerContext ctx )
    {
        this.failed = false;
        this.log = null;
    }

    public void channelInactive( ChannelHandlerContext ctx )
    {
        this.log.debug( "Channel is inactive" );
        if ( !this.failed )
        {
            ServiceUnavailableException error = ErrorUtil.newConnectionTerminatedError();
            this.fail( ctx, error );
        }
    }

    public void exceptionCaught( ChannelHandlerContext ctx, Throwable error )
    {
        if ( this.failed )
        {
            this.log.warn( "Another fatal error occurred in the pipeline", error );
        }
        else
        {
            this.failed = true;
            Throwable cause = transformError( error );
            this.fail( ctx, cause );
        }
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out )
    {
        int serverSuggestedVersion = in.readInt();
        this.log.debug( "S: [Bolt Handshake] %d", serverSuggestedVersion );
        ctx.pipeline().remove( (ChannelHandler) this );
        BoltProtocol protocol = this.protocolForVersion( serverSuggestedVersion );
        if ( protocol != null )
        {
            this.protocolSelected( serverSuggestedVersion, protocol.createMessageFormat(), ctx );
        }
        else
        {
            this.handleUnknownSuggestedProtocolVersion( serverSuggestedVersion, ctx );
        }
    }

    private BoltProtocol protocolForVersion( int version )
    {
        try
        {
            return BoltProtocol.forVersion( version );
        }
        catch ( ClientException var3 )
        {
            return null;
        }
    }

    private void protocolSelected( int version, MessageFormat messageFormat, ChannelHandlerContext ctx )
    {
        ChannelAttributes.setProtocolVersion( ctx.channel(), version );
        this.pipelineBuilder.build( messageFormat, ctx.pipeline(), this.logging );
        this.handshakeCompletedPromise.setSuccess();
    }

    private void handleUnknownSuggestedProtocolVersion( int version, ChannelHandlerContext ctx )
    {
        switch ( version )
        {
        case 0:
            this.fail( ctx, protocolNoSupportedByServerError() );
            break;
        case 1213486160:
            this.fail( ctx, httpEndpointError() );
            break;
        default:
            this.fail( ctx, protocolNoSupportedByDriverError( version ) );
        }
    }

    private void fail( ChannelHandlerContext ctx, Throwable error )
    {
        ctx.close().addListener( ( future ) -> {
            this.handshakeCompletedPromise.tryFailure( error );
        } );
    }
}
