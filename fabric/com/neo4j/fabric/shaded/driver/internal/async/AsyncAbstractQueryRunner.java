package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.async.AsyncQueryRunner;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.internal.AbstractQueryRunner;

import java.util.Map;
import java.util.concurrent.CompletionStage;

public abstract class AsyncAbstractQueryRunner implements AsyncQueryRunner
{
    public final CompletionStage<ResultCursor> runAsync( String query, Value parameters )
    {
        return this.runAsync( new Query( query, parameters ) );
    }

    public final CompletionStage<ResultCursor> runAsync( String query, Map<String,Object> parameters )
    {
        return this.runAsync( query, AbstractQueryRunner.parameters( parameters ) );
    }

    public final CompletionStage<ResultCursor> runAsync( String query, Record parameters )
    {
        return this.runAsync( query, AbstractQueryRunner.parameters( parameters ) );
    }

    public final CompletionStage<ResultCursor> runAsync( String query )
    {
        return this.runAsync( query, Values.EmptyMap );
    }
}
