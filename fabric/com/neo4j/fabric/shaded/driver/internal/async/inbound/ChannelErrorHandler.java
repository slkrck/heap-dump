package com.neo4j.fabric.shaded.driver.internal.async.inbound;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.logging.ChannelActivityLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelInboundHandlerAdapter;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.codec.CodecException;
import com.neo4j.fabric.shaded.driver.internal.util.ErrorUtil;

import java.io.IOException;
import java.util.Objects;

public class ChannelErrorHandler extends ChannelInboundHandlerAdapter
{
    private final Logging logging;
    private InboundMessageDispatcher messageDispatcher;
    private Logger log;
    private boolean failed;

    public ChannelErrorHandler( Logging logging )
    {
        this.logging = logging;
    }

    private static Throwable transformError( Throwable error )
    {
        if ( error instanceof CodecException && error.getCause() != null )
        {
            error = error.getCause();
        }

        return (Throwable) (error instanceof IOException ? new ServiceUnavailableException( "Connection to the database failed", error ) : error);
    }

    public void handlerAdded( ChannelHandlerContext ctx )
    {
        this.messageDispatcher = (InboundMessageDispatcher) Objects.requireNonNull( ChannelAttributes.messageDispatcher( ctx.channel() ) );
        this.log = new ChannelActivityLogger( ctx.channel(), this.logging, this.getClass() );
    }

    public void handlerRemoved( ChannelHandlerContext ctx )
    {
        this.messageDispatcher = null;
        this.log = null;
        this.failed = false;
    }

    public void channelInactive( ChannelHandlerContext ctx )
    {
        this.log.debug( "Channel is inactive" );
        if ( !this.failed )
        {
            String terminationReason = ChannelAttributes.terminationReason( ctx.channel() );
            ServiceUnavailableException error = ErrorUtil.newConnectionTerminatedError( terminationReason );
            this.fail( ctx, error );
        }
    }

    public void exceptionCaught( ChannelHandlerContext ctx, Throwable error )
    {
        if ( this.failed )
        {
            this.log.warn( "Another fatal error occurred in the pipeline", error );
        }
        else
        {
            this.failed = true;
            this.log.warn( "Fatal error occurred in the pipeline", error );
            this.fail( ctx, error );
        }
    }

    private void fail( ChannelHandlerContext ctx, Throwable error )
    {
        Throwable cause = transformError( error );
        this.messageDispatcher.handleChannelError( cause );
        this.log.debug( "Closing channel because of a failure '%s'", error );
        ctx.close();
    }
}
