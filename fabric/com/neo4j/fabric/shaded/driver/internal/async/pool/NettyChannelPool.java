package com.neo4j.fabric.shaded.driver.internal.async.pool;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelConnector;
import com.neo4j.fabric.shaded.driver.internal.metrics.ListenerEvent;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.Bootstrap;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.pool.ChannelHealthChecker;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.pool.FixedChannelPool;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicBoolean;

public class NettyChannelPool implements ExtendedChannelPool
{
    private static final int MAX_PENDING_ACQUIRES = Integer.MAX_VALUE;
    private static final boolean RELEASE_HEALTH_CHECK = false;
    private final FixedChannelPool delegate;
    private final AtomicBoolean closed = new AtomicBoolean( false );
    private final String id;
    private final CompletableFuture<Void> closeFuture = new CompletableFuture();

    NettyChannelPool( final BoltServerAddress address, final ChannelConnector connector, Bootstrap bootstrap, final NettyChannelTracker handler,
            ChannelHealthChecker healthCheck, long acquireTimeoutMillis, int maxConnections )
    {
        Objects.requireNonNull( address );
        Objects.requireNonNull( connector );
        Objects.requireNonNull( handler );
        this.id = this.poolId( address );
        this.delegate = new FixedChannelPool( bootstrap, handler, healthCheck, FixedChannelPool.AcquireTimeoutAction.FAIL, acquireTimeoutMillis, maxConnections,
                Integer.MAX_VALUE, false )
        {
            protected ChannelFuture connectChannel( Bootstrap bootstrap )
            {
                ListenerEvent creatingEvent = handler.channelCreating( NettyChannelPool.this.id );
                ChannelFuture channelFuture = connector.connect( address, bootstrap );
                channelFuture.addListener( ( future ) -> {
                    if ( future.isSuccess() )
                    {
                        Channel channel = channelFuture.channel();
                        ChannelAttributes.setPoolId( channel, NettyChannelPool.this.id );
                        handler.channelCreated( channel, creatingEvent );
                    }
                    else
                    {
                        handler.channelFailedToCreate( NettyChannelPool.this.id );
                    }
                } );
                return channelFuture;
            }
        };
    }

    public CompletionStage<Void> close()
    {
        if ( this.closed.compareAndSet( false, true ) )
        {
            Futures.asCompletionStage( this.delegate.closeAsync(), this.closeFuture );
        }

        return this.closeFuture;
    }

    public CompletionStage<Channel> acquire()
    {
        return Futures.asCompletionStage( this.delegate.acquire() );
    }

    public CompletionStage<Void> release( Channel channel )
    {
        return Futures.asCompletionStage( this.delegate.release( channel ) );
    }

    public boolean isClosed()
    {
        return this.closed.get();
    }

    public String id()
    {
        return this.id;
    }

    private String poolId( BoltServerAddress serverAddress )
    {
        return String.format( "%s:%d-%d", serverAddress.host(), serverAddress.port(), this.hashCode() );
    }
}
