package com.neo4j.fabric.shaded.driver.internal.async.inbound;

import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandlerContext;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.handler.timeout.ReadTimeoutHandler;

import java.util.concurrent.TimeUnit;

public class ConnectTimeoutHandler extends ReadTimeoutHandler
{
    private final long timeoutMillis;
    private boolean triggered;

    public ConnectTimeoutHandler( long timeoutMillis )
    {
        super( timeoutMillis, TimeUnit.MILLISECONDS );
        this.timeoutMillis = timeoutMillis;
    }

    protected void readTimedOut( ChannelHandlerContext ctx )
    {
        if ( !this.triggered )
        {
            this.triggered = true;
            ctx.fireExceptionCaught( this.unableToConnectError() );
        }
    }

    private ServiceUnavailableException unableToConnectError()
    {
        return new ServiceUnavailableException( "Unable to establish connection in " + this.timeoutMillis + "ms" );
    }
}
