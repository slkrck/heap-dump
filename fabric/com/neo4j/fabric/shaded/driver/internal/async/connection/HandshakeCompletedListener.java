package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFutureListener;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPromise;

import java.util.Map;
import java.util.Objects;

public class HandshakeCompletedListener implements ChannelFutureListener
{
    private final String userAgent;
    private final Map<String,Value> authToken;
    private final ChannelPromise connectionInitializedPromise;

    public HandshakeCompletedListener( String userAgent, Map<String,Value> authToken, ChannelPromise connectionInitializedPromise )
    {
        this.userAgent = (String) Objects.requireNonNull( userAgent );
        this.authToken = (Map) Objects.requireNonNull( authToken );
        this.connectionInitializedPromise = (ChannelPromise) Objects.requireNonNull( connectionInitializedPromise );
    }

    public void operationComplete( ChannelFuture future )
    {
        if ( future.isSuccess() )
        {
            BoltProtocol protocol = BoltProtocol.forChannel( future.channel() );
            protocol.initializeChannel( this.userAgent, this.authToken, this.connectionInitializedPromise );
        }
        else
        {
            this.connectionInitializedPromise.setFailure( future.cause() );
        }
    }
}
