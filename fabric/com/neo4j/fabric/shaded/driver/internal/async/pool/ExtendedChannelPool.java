package com.neo4j.fabric.shaded.driver.internal.async.pool;

import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;

import java.util.concurrent.CompletionStage;

public interface ExtendedChannelPool
{
    CompletionStage<Channel> acquire();

    CompletionStage<Void> release( Channel var1 );

    boolean isClosed();

    String id();

    CompletionStage<Void> close();
}
