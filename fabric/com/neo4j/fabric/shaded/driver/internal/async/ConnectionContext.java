package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;

public interface ConnectionContext
{
    DatabaseName databaseName();

    AccessMode mode();

    Bookmark rediscoveryBookmark();
}
