package com.neo4j.fabric.shaded.driver.internal.async.inbound;

import com.neo4j.fabric.shaded.driver.internal.packstream.PackInput;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.buffer.ByteBuf;

import java.util.Objects;

public class ByteBufInput implements PackInput
{
    private ByteBuf buf;

    public void start( ByteBuf newBuf )
    {
        this.assertNotStarted();
        this.buf = (ByteBuf) Objects.requireNonNull( newBuf );
    }

    public void stop()
    {
        this.buf = null;
    }

    public byte readByte()
    {
        return this.buf.readByte();
    }

    public short readShort()
    {
        return this.buf.readShort();
    }

    public int readInt()
    {
        return this.buf.readInt();
    }

    public long readLong()
    {
        return this.buf.readLong();
    }

    public double readDouble()
    {
        return this.buf.readDouble();
    }

    public void readBytes( byte[] into, int offset, int toRead )
    {
        this.buf.readBytes( into, offset, toRead );
    }

    public byte peekByte()
    {
        return this.buf.getByte( this.buf.readerIndex() );
    }

    private void assertNotStarted()
    {
        if ( this.buf != null )
        {
            throw new IllegalStateException( "Already started" );
        }
    }
}
