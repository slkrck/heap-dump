package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.async.AsyncTransaction;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;

import java.util.concurrent.CompletionStage;

public class InternalAsyncTransaction extends AsyncAbstractQueryRunner implements AsyncTransaction
{
    private final UnmanagedTransaction tx;

    public InternalAsyncTransaction( UnmanagedTransaction tx )
    {
        this.tx = tx;
    }

    public CompletionStage<Void> commitAsync()
    {
        return this.tx.commitAsync();
    }

    public CompletionStage<Void> rollbackAsync()
    {
        return this.tx.rollbackAsync();
    }

    public CompletionStage<ResultCursor> runAsync( Query query )
    {
        return this.tx.runAsync( query, true );
    }

    public void markTerminated()
    {
        this.tx.markTerminated();
    }

    public boolean isOpen()
    {
        return this.tx.isOpen();
    }
}
