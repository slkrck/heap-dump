package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.ChannelErrorHandler;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.ChunkDecoder;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.InboundMessageHandler;
import com.neo4j.fabric.shaded.driver.internal.async.inbound.MessageDecoder;
import com.neo4j.fabric.shaded.driver.internal.async.outbound.OutboundMessageHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.MessageFormat;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelHandler;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelPipeline;

public class ChannelPipelineBuilderImpl implements ChannelPipelineBuilder
{
    public void build( MessageFormat messageFormat, ChannelPipeline pipeline, Logging logging )
    {
        pipeline.addLast( new ChunkDecoder( logging ) );
        pipeline.addLast( new MessageDecoder() );
        pipeline.addLast( new InboundMessageHandler( messageFormat, logging ) );
        pipeline.addLast( (String) OutboundMessageHandler.NAME, (ChannelHandler) (new OutboundMessageHandler( messageFormat, logging )) );
        pipeline.addLast( new ChannelErrorHandler( logging ) );
    }
}
