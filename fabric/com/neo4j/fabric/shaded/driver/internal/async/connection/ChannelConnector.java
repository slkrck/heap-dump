package com.neo4j.fabric.shaded.driver.internal.async.connection;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.bootstrap.Bootstrap;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.ChannelFuture;

public interface ChannelConnector
{
    ChannelFuture connect( BoltServerAddress var1, Bootstrap var2 );
}
