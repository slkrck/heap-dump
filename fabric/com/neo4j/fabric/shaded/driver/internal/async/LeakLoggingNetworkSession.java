package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.retry.RetryLogic;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionProvider;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

public class LeakLoggingNetworkSession extends NetworkSession
{
    private final String stackTrace = captureStackTrace();

    public LeakLoggingNetworkSession( ConnectionProvider connectionProvider, RetryLogic retryLogic, DatabaseName databaseName, AccessMode mode,
            BookmarkHolder bookmarkHolder, long fetchSize, Logging logging )
    {
        super( connectionProvider, retryLogic, databaseName, mode, bookmarkHolder, fetchSize, logging );
    }

    private static String captureStackTrace()
    {
        StringBuilder result = new StringBuilder();
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        StackTraceElement[] var2 = elements;
        int var3 = elements.length;

        for ( int var4 = 0; var4 < var3; ++var4 )
        {
            StackTraceElement element = var2[var4];
            result.append( "\t" ).append( element ).append( System.lineSeparator() );
        }

        return result.toString();
    }

    protected void finalize() throws Throwable
    {
        this.logLeakIfNeeded();
        super.finalize();
    }

    private void logLeakIfNeeded()
    {
        Boolean isOpen = (Boolean) Futures.blockingGet( this.currentConnectionIsOpen() );
        if ( isOpen )
        {
            this.logger.error(
                    "Neo4j Session object leaked, please ensure that your application fully consumes results in Sessions or explicitly calls `close` on Sessions before disposing of the objects.\nSession was create at:\n" +
                            this.stackTrace, (Throwable) null );
        }
    }
}
