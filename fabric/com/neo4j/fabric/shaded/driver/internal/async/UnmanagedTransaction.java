package com.neo4j.fabric.shaded.driver.internal.async;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.cursor.AsyncResultCursor;
import com.neo4j.fabric.shaded.driver.internal.cursor.RxResultCursor;
import com.neo4j.fabric.shaded.driver.internal.messaging.BoltProtocol;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.function.BiFunction;

public class UnmanagedTransaction
{
    private final Connection connection;
    private final BoltProtocol protocol;
    private final BookmarkHolder bookmarkHolder;
    private final ResultCursorsHolder resultCursors;
    private final long fetchSize;
    private volatile UnmanagedTransaction.State state;

    public UnmanagedTransaction( Connection connection, BookmarkHolder bookmarkHolder, long fetchSize )
    {
        this.state = UnmanagedTransaction.State.ACTIVE;
        this.connection = connection;
        this.protocol = connection.protocol();
        this.bookmarkHolder = bookmarkHolder;
        this.resultCursors = new ResultCursorsHolder();
        this.fetchSize = fetchSize;
    }

    private static BiFunction<Void,Throwable,Void> handleCommitOrRollback( Throwable cursorFailure )
    {
        return ( ignore, commitOrRollbackError ) -> {
            CompletionException combinedError = Futures.combineErrors( cursorFailure, commitOrRollbackError );
            if ( combinedError != null )
            {
                throw combinedError;
            }
            else
            {
                return null;
            }
        };
    }

    public CompletionStage<UnmanagedTransaction> beginAsync( Bookmark initialBookmark, TransactionConfig config )
    {
        return this.protocol.beginTransaction( this.connection, initialBookmark, config ).handle( ( ignore, beginError ) -> {
            if ( beginError != null )
            {
                this.connection.release();
                throw Futures.asCompletionException( beginError );
            }
            else
            {
                return this;
            }
        } );
    }

    public CompletionStage<Void> closeAsync()
    {
        return (CompletionStage) (this.isOpen() ? this.rollbackAsync() : Futures.completedWithNull());
    }

    public CompletionStage<Void> commitAsync()
    {
        if ( this.state == UnmanagedTransaction.State.COMMITTED )
        {
            return Futures.failedFuture( new ClientException( "Can't commit, transaction has been committed" ) );
        }
        else
        {
            return (CompletionStage) (this.state == UnmanagedTransaction.State.ROLLED_BACK ? Futures.failedFuture(
                    new ClientException( "Can't commit, transaction has been rolled back" ) ) : this.resultCursors.retrieveNotConsumedError().thenCompose(
                    ( error ) -> {
                        return this.doCommitAsync().handle( handleCommitOrRollback( error ) );
                    } ).whenComplete( ( ignore, error ) -> {
                this.transactionClosed( error == null );
            } ));
        }
    }

    public CompletionStage<Void> rollbackAsync()
    {
        if ( this.state == UnmanagedTransaction.State.COMMITTED )
        {
            return Futures.failedFuture( new ClientException( "Can't rollback, transaction has been committed" ) );
        }
        else
        {
            return (CompletionStage) (this.state == UnmanagedTransaction.State.ROLLED_BACK ? Futures.failedFuture(
                    new ClientException( "Can't rollback, transaction has been rolled back" ) ) : this.resultCursors.retrieveNotConsumedError().thenCompose(
                    ( error ) -> {
                        return this.doRollbackAsync().handle( handleCommitOrRollback( error ) );
                    } ).whenComplete( ( ignore, error ) -> {
                this.transactionClosed( false );
            } ));
        }
    }

    public CompletionStage<ResultCursor> runAsync( Query query, boolean waitForRunResponse )
    {
        this.ensureCanRunQueries();
        CompletionStage<AsyncResultCursor> cursorStage =
                this.protocol.runInUnmanagedTransaction( this.connection, query, this, waitForRunResponse, this.fetchSize ).asyncResult();
        this.resultCursors.add( cursorStage );
        return cursorStage.thenApply( ( cursor ) -> {
            return cursor;
        } );
    }

    public CompletionStage<RxResultCursor> runRx( Query query )
    {
        this.ensureCanRunQueries();
        CompletionStage<RxResultCursor> cursorStage = this.protocol.runInUnmanagedTransaction( this.connection, query, this, false, this.fetchSize ).rxResult();
        this.resultCursors.add( cursorStage );
        return cursorStage;
    }

    public boolean isOpen()
    {
        return this.state != UnmanagedTransaction.State.COMMITTED && this.state != UnmanagedTransaction.State.ROLLED_BACK;
    }

    public void markTerminated()
    {
        this.state = UnmanagedTransaction.State.TERMINATED;
    }

    public Connection connection()
    {
        return this.connection;
    }

    private void ensureCanRunQueries()
    {
        if ( this.state == UnmanagedTransaction.State.COMMITTED )
        {
            throw new ClientException( "Cannot run more queries in this transaction, it has been committed" );
        }
        else if ( this.state == UnmanagedTransaction.State.ROLLED_BACK )
        {
            throw new ClientException( "Cannot run more queries in this transaction, it has been rolled back" );
        }
        else if ( this.state == UnmanagedTransaction.State.TERMINATED )
        {
            throw new ClientException( "Cannot run more queries in this transaction, it has either experienced an fatal error or was explicitly terminated" );
        }
    }

    private CompletionStage<Void> doCommitAsync()
    {
        if ( this.state == UnmanagedTransaction.State.TERMINATED )
        {
            return Futures.failedFuture(
                    new ClientException( "Transaction can't be committed. It has been rolled back either because of an error or explicit termination" ) );
        }
        else
        {
            CompletionStage var10000 = this.protocol.commitTransaction( this.connection );
            BookmarkHolder var10001 = this.bookmarkHolder;
            var10001.getClass();
            return var10000.thenAccept( var10001::setBookmark );
        }
    }

    private CompletionStage<Void> doRollbackAsync()
    {
        return (CompletionStage) (this.state == UnmanagedTransaction.State.TERMINATED ? Futures.completedWithNull()
                                                                                      : this.protocol.rollbackTransaction( this.connection ));
    }

    private void transactionClosed( boolean isCommitted )
    {
        if ( isCommitted )
        {
            this.state = UnmanagedTransaction.State.COMMITTED;
        }
        else
        {
            this.state = UnmanagedTransaction.State.ROLLED_BACK;
        }

        this.connection.release();
    }

    private static enum State
    {
        ACTIVE,
        TERMINATED,
        COMMITTED,
        ROLLED_BACK;
    }
}
