package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.types.Point;

import java.util.Objects;

public class InternalPoint2D implements Point
{
    private final int srid;
    private final double x;
    private final double y;

    public InternalPoint2D( int srid, double x, double y )
    {
        this.srid = srid;
        this.x = x;
        this.y = y;
    }

    public int srid()
    {
        return this.srid;
    }

    public double x()
    {
        return this.x;
    }

    public double y()
    {
        return this.y;
    }

    public double z()
    {
        return Double.NaN;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalPoint2D that = (InternalPoint2D) o;
            return this.srid == that.srid && Double.compare( that.x, this.x ) == 0 && Double.compare( that.y, this.y ) == 0;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.srid, this.x, this.y} );
    }

    public String toString()
    {
        return "Point{srid=" + this.srid + ", x=" + this.x + ", y=" + this.y + '}';
    }
}
