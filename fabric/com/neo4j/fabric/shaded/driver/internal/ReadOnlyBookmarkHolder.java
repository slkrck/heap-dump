package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Bookmark;

public class ReadOnlyBookmarkHolder implements BookmarkHolder
{
    private final Bookmark bookmark;

    public ReadOnlyBookmarkHolder( Bookmark bookmark )
    {
        this.bookmark = bookmark;
    }

    public Bookmark getBookmark()
    {
        return this.bookmark;
    }

    public void setBookmark( Bookmark bookmark )
    {
    }
}
