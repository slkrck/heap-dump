package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.IsoDuration;
import com.neo4j.fabric.shaded.driver.types.Type;

public class DurationValue extends ObjectValueAdapter<IsoDuration>
{
    public DurationValue( IsoDuration duration )
    {
        super( duration );
    }

    public IsoDuration asIsoDuration()
    {
        return (IsoDuration) this.asObject();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.DURATION();
    }
}
