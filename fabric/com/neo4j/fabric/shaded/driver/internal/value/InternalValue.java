package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.AsValue;
import com.neo4j.fabric.shaded.driver.internal.types.TypeConstructor;

public interface InternalValue extends Value, AsValue
{
    TypeConstructor typeConstructor();
}
