package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.internal.util.Extract;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

public class ListValue extends ValueAdapter
{
    private final Value[] values;

    public ListValue( Value... values )
    {
        if ( values == null )
        {
            throw new IllegalArgumentException( "Cannot construct ListValue from null" );
        }
        else
        {
            this.values = values;
        }
    }

    public boolean isEmpty()
    {
        return this.values.length == 0;
    }

    public List<Object> asObject()
    {
        return this.asList( Values.ofObject() );
    }

    public List<Object> asList()
    {
        return Extract.list( this.values, Values.ofObject() );
    }

    public <T> List<T> asList( Function<Value,T> mapFunction )
    {
        return Extract.list( this.values, mapFunction );
    }

    public int size()
    {
        return this.values.length;
    }

    public Value get( int index )
    {
        return index >= 0 && index < this.values.length ? this.values[index] : Values.NULL;
    }

    public <T> Iterable<T> values( final Function<Value,T> mapFunction )
    {
        return new Iterable<T>()
        {
            public Iterator<T> iterator()
            {
                return new Iterator<T>()
                {
                    private int cursor = 0;

                    public boolean hasNext()
                    {
                        return this.cursor < ListValue.this.values.length;
                    }

                    public T next()
                    {
                        return mapFunction.apply( ListValue.this.values[this.cursor++] );
                    }

                    public void remove()
                    {
                    }
                };
            }
        };
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.LIST();
    }

    public String toString()
    {
        return Arrays.toString( this.values );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ListValue otherValues = (ListValue) o;
            return Arrays.equals( this.values, otherValues.values );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Arrays.hashCode( this.values );
    }
}
