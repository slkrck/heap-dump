package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Point;
import com.neo4j.fabric.shaded.driver.types.Type;

public class PointValue extends ObjectValueAdapter<Point>
{
    public PointValue( Point point )
    {
        super( point );
    }

    public Point asPoint()
    {
        return (Point) this.asObject();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.POINT();
    }
}
