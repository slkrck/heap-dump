package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.internal.util.Extract;
import com.neo4j.fabric.shaded.driver.internal.util.Format;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.util.Map;
import java.util.function.Function;

public class MapValue extends ValueAdapter
{
    private final Map<String,Value> val;

    public MapValue( Map<String,Value> val )
    {
        if ( val == null )
        {
            throw new IllegalArgumentException( "Cannot construct MapValue from null" );
        }
        else
        {
            this.val = val;
        }
    }

    public boolean isEmpty()
    {
        return this.val.isEmpty();
    }

    public Map<String,Object> asObject()
    {
        return this.asMap( Values.ofObject() );
    }

    public Map<String,Object> asMap()
    {
        return Extract.map( this.val, Values.ofObject() );
    }

    public <T> Map<String,T> asMap( Function<Value,T> mapFunction )
    {
        return Extract.map( this.val, mapFunction );
    }

    public int size()
    {
        return this.val.size();
    }

    public boolean containsKey( String key )
    {
        return this.val.containsKey( key );
    }

    public Iterable<String> keys()
    {
        return this.val.keySet();
    }

    public Iterable<Value> values()
    {
        return this.val.values();
    }

    public <T> Iterable<T> values( Function<Value,T> mapFunction )
    {
        return Extract.map( this.val, mapFunction ).values();
    }

    public Value get( String key )
    {
        Value value = (Value) this.val.get( key );
        return value == null ? Values.NULL : value;
    }

    public String toString()
    {
        return Format.formatPairs( this.asMap( Values.ofValue() ) );
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.MAP();
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            MapValue values = (MapValue) o;
            return this.val.equals( values.val );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.val.hashCode();
    }
}
