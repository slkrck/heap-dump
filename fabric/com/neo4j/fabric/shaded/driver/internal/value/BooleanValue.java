package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

public abstract class BooleanValue extends ValueAdapter
{
    public static BooleanValue TRUE = new BooleanValue.TrueValue();
    public static BooleanValue FALSE = new BooleanValue.FalseValue();

    private BooleanValue()
    {
    }

    public static BooleanValue fromBoolean( boolean value )
    {
        return value ? TRUE : FALSE;
    }

    public abstract Boolean asObject();

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.BOOLEAN();
    }

    public int hashCode()
    {
        Boolean value = this.asBoolean() ? Boolean.TRUE : Boolean.FALSE;
        return value.hashCode();
    }

    private static class FalseValue extends BooleanValue
    {
        private FalseValue()
        {
            super( null );
        }

        public Boolean asObject()
        {
            return Boolean.FALSE;
        }

        public boolean asBoolean()
        {
            return false;
        }

        public boolean isTrue()
        {
            return false;
        }

        public boolean isFalse()
        {
            return true;
        }

        public boolean equals( Object obj )
        {
            return obj == FALSE;
        }

        public String toString()
        {
            return "FALSE";
        }
    }

    private static class TrueValue extends BooleanValue
    {
        private TrueValue()
        {
            super( null );
        }

        public Boolean asObject()
        {
            return Boolean.TRUE;
        }

        public boolean asBoolean()
        {
            return true;
        }

        public boolean isTrue()
        {
            return true;
        }

        public boolean isFalse()
        {
            return false;
        }

        public boolean equals( Object obj )
        {
            return obj == TRUE;
        }

        public String toString()
        {
            return "TRUE";
        }
    }
}
