package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.util.Objects;

public class StringValue extends ValueAdapter
{
    private final String val;

    public StringValue( String val )
    {
        if ( val == null )
        {
            throw new IllegalArgumentException( "Cannot construct StringValue from null" );
        }
        else
        {
            this.val = val;
        }
    }

    public boolean isEmpty()
    {
        return this.val.isEmpty();
    }

    public int size()
    {
        return this.val.length();
    }

    public String asObject()
    {
        return this.asString();
    }

    public String asString()
    {
        return this.val;
    }

    public String toString()
    {
        return String.format( "\"%s\"", this.val.replace( "\"", "\\\"" ) );
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.STRING();
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            StringValue that = (StringValue) o;
            return Objects.equals( this.val, that.val );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.val.hashCode();
    }
}
