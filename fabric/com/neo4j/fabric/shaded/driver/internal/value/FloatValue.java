package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.exceptions.value.LossyCoercion;
import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

public class FloatValue extends NumberValueAdapter<Double>
{
    private final double val;

    public FloatValue( double val )
    {
        this.val = val;
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.FLOAT();
    }

    public Double asNumber()
    {
        return this.val;
    }

    public long asLong()
    {
        long longVal = (long) this.val;
        if ( (double) longVal != this.val )
        {
            throw new LossyCoercion( this.type().name(), "Java long" );
        }
        else
        {
            return longVal;
        }
    }

    public int asInt()
    {
        int intVal = (int) this.val;
        if ( (double) intVal != this.val )
        {
            throw new LossyCoercion( this.type().name(), "Java int" );
        }
        else
        {
            return intVal;
        }
    }

    public double asDouble()
    {
        return this.val;
    }

    public float asFloat()
    {
        float floatVal = (float) this.val;
        if ( (double) floatVal != this.val )
        {
            throw new LossyCoercion( this.type().name(), "Java float" );
        }
        else
        {
            return floatVal;
        }
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FloatValue values = (FloatValue) o;
            return Double.compare( values.val, this.val ) == 0;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        long temp = Double.doubleToLongBits( this.val );
        return (int) (temp ^ temp >>> 32);
    }

    public String toString()
    {
        return Double.toString( this.val );
    }
}
