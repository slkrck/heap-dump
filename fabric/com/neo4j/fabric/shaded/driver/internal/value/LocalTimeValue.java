package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.time.LocalTime;

public class LocalTimeValue extends ObjectValueAdapter<LocalTime>
{
    public LocalTimeValue( LocalTime time )
    {
        super( time );
    }

    public LocalTime asLocalTime()
    {
        return (LocalTime) this.asObject();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.LOCAL_TIME();
    }
}
