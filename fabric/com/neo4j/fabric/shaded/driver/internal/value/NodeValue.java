package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Type;

public class NodeValue extends EntityValueAdapter<Node>
{
    public NodeValue( Node adapted )
    {
        super( adapted );
    }

    public Node asNode()
    {
        return (Node) this.asEntity();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.NODE();
    }
}
