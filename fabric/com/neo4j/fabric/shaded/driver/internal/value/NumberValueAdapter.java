package com.neo4j.fabric.shaded.driver.internal.value;

public abstract class NumberValueAdapter<V extends Number> extends ValueAdapter
{
    public final V asObject()
    {
        return this.asNumber();
    }

    public abstract V asNumber();
}
