package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.time.OffsetTime;

public class TimeValue extends ObjectValueAdapter<OffsetTime>
{
    public TimeValue( OffsetTime time )
    {
        super( time );
    }

    public OffsetTime asOffsetTime()
    {
        return (OffsetTime) this.asObject();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.TIME();
    }
}
