package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.exceptions.value.LossyCoercion;
import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

public class IntegerValue extends NumberValueAdapter<Long>
{
    private final long val;

    public IntegerValue( long val )
    {
        this.val = val;
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.INTEGER();
    }

    public Long asNumber()
    {
        return this.val;
    }

    public long asLong()
    {
        return this.val;
    }

    public int asInt()
    {
        if ( this.val <= 2147483647L && this.val >= -2147483648L )
        {
            return (int) this.val;
        }
        else
        {
            throw new LossyCoercion( this.type().name(), "Java int" );
        }
    }

    public double asDouble()
    {
        double doubleVal = (double) this.val;
        if ( (long) doubleVal != this.val )
        {
            throw new LossyCoercion( this.type().name(), "Java double" );
        }
        else
        {
            return (double) this.val;
        }
    }

    public float asFloat()
    {
        return (float) this.val;
    }

    public String toString()
    {
        return Long.toString( this.val );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            IntegerValue values = (IntegerValue) o;
            return this.val == values.val;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return (int) (this.val ^ this.val >>> 32);
    }
}
