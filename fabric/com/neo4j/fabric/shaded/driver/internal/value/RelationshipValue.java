package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Relationship;
import com.neo4j.fabric.shaded.driver.types.Type;

public class RelationshipValue extends EntityValueAdapter<Relationship>
{
    public RelationshipValue( Relationship adapted )
    {
        super( adapted );
    }

    public Relationship asRelationship()
    {
        return (Relationship) this.asEntity();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.RELATIONSHIP();
    }
}
