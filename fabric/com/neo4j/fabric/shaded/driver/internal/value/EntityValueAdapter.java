package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.types.Entity;

import java.util.Map;
import java.util.function.Function;

public abstract class EntityValueAdapter<V extends Entity> extends ObjectValueAdapter<V>
{
    protected EntityValueAdapter( V adapted )
    {
        super( adapted );
    }

    public V asEntity()
    {
        return (Entity) this.asObject();
    }

    public Map<String,Object> asMap()
    {
        return this.asEntity().asMap();
    }

    public <T> Map<String,T> asMap( Function<Value,T> mapFunction )
    {
        return this.asEntity().asMap( mapFunction );
    }

    public int size()
    {
        return this.asEntity().size();
    }

    public Iterable<String> keys()
    {
        return this.asEntity().keys();
    }

    public Value get( String key )
    {
        return this.asEntity().get( key );
    }
}
