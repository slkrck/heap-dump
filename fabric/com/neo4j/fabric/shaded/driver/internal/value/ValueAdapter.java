package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.exceptions.value.NotMultiValued;
import com.neo4j.fabric.shaded.driver.exceptions.value.Uncoercible;
import com.neo4j.fabric.shaded.driver.exceptions.value.Unsizable;
import com.neo4j.fabric.shaded.driver.internal.types.InternalMapAccessorWithDefaultValue;
import com.neo4j.fabric.shaded.driver.internal.types.TypeConstructor;
import com.neo4j.fabric.shaded.driver.internal.types.TypeRepresentation;
import com.neo4j.fabric.shaded.driver.types.Entity;
import com.neo4j.fabric.shaded.driver.types.IsoDuration;
import com.neo4j.fabric.shaded.driver.types.MapAccessor;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Point;
import com.neo4j.fabric.shaded.driver.types.Relationship;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public abstract class ValueAdapter extends InternalMapAccessorWithDefaultValue implements InternalValue
{
    public Value asValue()
    {
        return this;
    }

    public boolean hasType( Type type )
    {
        return type.isTypeOf( this );
    }

    public boolean isTrue()
    {
        return false;
    }

    public boolean isFalse()
    {
        return false;
    }

    public boolean isNull()
    {
        return false;
    }

    public boolean containsKey( String key )
    {
        throw new NotMultiValued( this.type().name() + " is not a keyed collection" );
    }

    public String asString()
    {
        throw new Uncoercible( this.type().name(), "Java String" );
    }

    public boolean asBoolean( boolean defaultValue )
    {
        return (Boolean) this.computeOrDefault( Value::asBoolean, defaultValue );
    }

    public String asString( String defaultValue )
    {
        return (String) this.computeOrDefault( Value::asString, defaultValue );
    }

    public long asLong( long defaultValue )
    {
        return (Long) this.computeOrDefault( Value::asLong, defaultValue );
    }

    public int asInt( int defaultValue )
    {
        return (Integer) this.computeOrDefault( Value::asInt, defaultValue );
    }

    public double asDouble( double defaultValue )
    {
        return (Double) this.computeOrDefault( Value::asDouble, defaultValue );
    }

    public float asFloat( float defaultValue )
    {
        return (Float) this.computeOrDefault( Value::asFloat, defaultValue );
    }

    public long asLong()
    {
        throw new Uncoercible( this.type().name(), "Java long" );
    }

    public int asInt()
    {
        throw new Uncoercible( this.type().name(), "Java int" );
    }

    public float asFloat()
    {
        throw new Uncoercible( this.type().name(), "Java float" );
    }

    public double asDouble()
    {
        throw new Uncoercible( this.type().name(), "Java double" );
    }

    public boolean asBoolean()
    {
        throw new Uncoercible( this.type().name(), "Java boolean" );
    }

    public List<Object> asList()
    {
        return this.asList( Values.ofObject() );
    }

    public <T> List<T> asList( Function<Value,T> mapFunction )
    {
        throw new Uncoercible( this.type().name(), "Java List" );
    }

    public Map<String,Object> asMap()
    {
        return this.asMap( Values.ofObject() );
    }

    public <T> Map<String,T> asMap( Function<Value,T> mapFunction )
    {
        throw new Uncoercible( this.type().name(), "Java Map" );
    }

    public Object asObject()
    {
        throw new Uncoercible( this.type().name(), "Java Object" );
    }

    public <T> T computeOrDefault( Function<Value,T> mapper, T defaultValue )
    {
        return this.isNull() ? defaultValue : mapper.apply( this );
    }

    public Map<String,Object> asMap( Map<String,Object> defaultValue )
    {
        return (Map) this.computeOrDefault( MapAccessor::asMap, defaultValue );
    }

    public <T> Map<String,T> asMap( Function<Value,T> mapFunction, Map<String,T> defaultValue )
    {
        return (Map) this.computeOrDefault( ( value ) -> {
            return value.asMap( mapFunction );
        }, defaultValue );
    }

    public byte[] asByteArray( byte[] defaultValue )
    {
        return (byte[]) this.computeOrDefault( Value::asByteArray, defaultValue );
    }

    public List<Object> asList( List<Object> defaultValue )
    {
        return (List) this.computeOrDefault( Value::asList, defaultValue );
    }

    public <T> List<T> asList( Function<Value,T> mapFunction, List<T> defaultValue )
    {
        return (List) this.computeOrDefault( ( value ) -> {
            return value.asList( mapFunction );
        }, defaultValue );
    }

    public LocalDate asLocalDate( LocalDate defaultValue )
    {
        return (LocalDate) this.computeOrDefault( Value::asLocalDate, defaultValue );
    }

    public OffsetTime asOffsetTime( OffsetTime defaultValue )
    {
        return (OffsetTime) this.computeOrDefault( Value::asOffsetTime, defaultValue );
    }

    public LocalTime asLocalTime( LocalTime defaultValue )
    {
        return (LocalTime) this.computeOrDefault( Value::asLocalTime, defaultValue );
    }

    public LocalDateTime asLocalDateTime( LocalDateTime defaultValue )
    {
        return (LocalDateTime) this.computeOrDefault( Value::asLocalDateTime, defaultValue );
    }

    public OffsetDateTime asOffsetDateTime( OffsetDateTime defaultValue )
    {
        return (OffsetDateTime) this.computeOrDefault( Value::asOffsetDateTime, defaultValue );
    }

    public ZonedDateTime asZonedDateTime( ZonedDateTime defaultValue )
    {
        return (ZonedDateTime) this.computeOrDefault( Value::asZonedDateTime, defaultValue );
    }

    public IsoDuration asIsoDuration( IsoDuration defaultValue )
    {
        return (IsoDuration) this.computeOrDefault( Value::asIsoDuration, defaultValue );
    }

    public Point asPoint( Point defaultValue )
    {
        return (Point) this.computeOrDefault( Value::asPoint, defaultValue );
    }

    public byte[] asByteArray()
    {
        throw new Uncoercible( this.type().name(), "Byte array" );
    }

    public Number asNumber()
    {
        throw new Uncoercible( this.type().name(), "Java Number" );
    }

    public Entity asEntity()
    {
        throw new Uncoercible( this.type().name(), "Entity" );
    }

    public Node asNode()
    {
        throw new Uncoercible( this.type().name(), "Node" );
    }

    public Path asPath()
    {
        throw new Uncoercible( this.type().name(), "Path" );
    }

    public Relationship asRelationship()
    {
        throw new Uncoercible( this.type().name(), "Relationship" );
    }

    public LocalDate asLocalDate()
    {
        throw new Uncoercible( this.type().name(), "LocalDate" );
    }

    public OffsetTime asOffsetTime()
    {
        throw new Uncoercible( this.type().name(), "OffsetTime" );
    }

    public LocalTime asLocalTime()
    {
        throw new Uncoercible( this.type().name(), "LocalTime" );
    }

    public LocalDateTime asLocalDateTime()
    {
        throw new Uncoercible( this.type().name(), "LocalDateTime" );
    }

    public OffsetDateTime asOffsetDateTime()
    {
        throw new Uncoercible( this.type().name(), "OffsetDateTime" );
    }

    public ZonedDateTime asZonedDateTime()
    {
        throw new Uncoercible( this.type().name(), "ZonedDateTime" );
    }

    public IsoDuration asIsoDuration()
    {
        throw new Uncoercible( this.type().name(), "Duration" );
    }

    public Point asPoint()
    {
        throw new Uncoercible( this.type().name(), "Point" );
    }

    public Value get( int index )
    {
        throw new NotMultiValued( this.type().name() + " is not an indexed collection" );
    }

    public Value get( String key )
    {
        throw new NotMultiValued( this.type().name() + " is not a keyed collection" );
    }

    public int size()
    {
        throw new Unsizable( this.type().name() + " does not have size" );
    }

    public Iterable<String> keys()
    {
        return Collections.emptyList();
    }

    public boolean isEmpty()
    {
        return !this.values().iterator().hasNext();
    }

    public Iterable<Value> values()
    {
        return this.values( Values.ofValue() );
    }

    public <T> Iterable<T> values( Function<Value,T> mapFunction )
    {
        throw new NotMultiValued( this.type().name() + " is not iterable" );
    }

    public final TypeConstructor typeConstructor()
    {
        return ((TypeRepresentation) this.type()).constructor();
    }

    public abstract boolean equals( Object var1 );

    public abstract int hashCode();

    public abstract String toString();
}
