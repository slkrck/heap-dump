package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Type;

public class PathValue extends ObjectValueAdapter<Path>
{
    public PathValue( Path adapted )
    {
        super( adapted );
    }

    public Path asPath()
    {
        return (Path) this.asObject();
    }

    public int size()
    {
        return ((Path) this.asObject()).length();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.PATH();
    }
}
