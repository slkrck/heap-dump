package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

public final class NullValue extends ValueAdapter
{
    public static final Value NULL = new NullValue();

    private NullValue()
    {
    }

    public boolean isNull()
    {
        return true;
    }

    public Object asObject()
    {
        return null;
    }

    public String asString()
    {
        return "null";
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.NULL();
    }

    public boolean equals( Object obj )
    {
        return obj == NULL;
    }

    public int hashCode()
    {
        return 0;
    }

    public String toString()
    {
        return "NULL";
    }
}
