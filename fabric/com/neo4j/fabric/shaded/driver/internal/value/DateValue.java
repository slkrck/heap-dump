package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.time.LocalDate;

public class DateValue extends ObjectValueAdapter<LocalDate>
{
    public DateValue( LocalDate date )
    {
        super( date );
    }

    public LocalDate asLocalDate()
    {
        return (LocalDate) this.asObject();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.DATE();
    }
}
