package com.neo4j.fabric.shaded.driver.internal.value;

import java.util.Objects;

public abstract class ObjectValueAdapter<V> extends ValueAdapter
{
    private final V adapted;

    protected ObjectValueAdapter( V adapted )
    {
        if ( adapted == null )
        {
            throw new IllegalArgumentException( String.format( "Cannot construct %s from null", this.getClass().getSimpleName() ) );
        }
        else
        {
            this.adapted = adapted;
        }
    }

    public final V asObject()
    {
        return this.adapted;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ObjectValueAdapter<?> that = (ObjectValueAdapter) o;
            return Objects.equals( this.adapted, that.adapted );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.adapted.hashCode();
    }

    public String toString()
    {
        return this.adapted.toString();
    }
}
