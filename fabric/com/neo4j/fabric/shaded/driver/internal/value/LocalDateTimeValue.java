package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.time.LocalDateTime;

public class LocalDateTimeValue extends ObjectValueAdapter<LocalDateTime>
{
    public LocalDateTimeValue( LocalDateTime localDateTime )
    {
        super( localDateTime );
    }

    public LocalDateTime asLocalDateTime()
    {
        return (LocalDateTime) this.asObject();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.LOCAL_DATE_TIME();
    }
}
