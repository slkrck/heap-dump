package com.neo4j.fabric.shaded.driver.internal.value;

import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.types.Type;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

public class DateTimeValue extends ObjectValueAdapter<ZonedDateTime>
{
    public DateTimeValue( ZonedDateTime zonedDateTime )
    {
        super( zonedDateTime );
    }

    public OffsetDateTime asOffsetDateTime()
    {
        return this.asZonedDateTime().toOffsetDateTime();
    }

    public ZonedDateTime asZonedDateTime()
    {
        return (ZonedDateTime) this.asObject();
    }

    public Type type()
    {
        return InternalTypeSystem.TYPE_SYSTEM.DATE_TIME();
    }
}
