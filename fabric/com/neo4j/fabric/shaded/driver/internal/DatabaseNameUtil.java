package com.neo4j.fabric.shaded.driver.internal;

import java.util.Objects;
import java.util.Optional;

public final class DatabaseNameUtil
{
    public static final String SYSTEM_DATABASE_NAME = "system";
    static final String DEFAULT_DATABASE_NAME = null;
    private static final DatabaseName DEFAULT_DATABASE = new DatabaseName()
    {
        public Optional<String> databaseName()
        {
            return Optional.empty();
        }

        public String description()
        {
            return "<default database>";
        }
    };
    private static final DatabaseName SYSTEM_DATABASE = new InternalDatabaseName( "system" );

    public static DatabaseName defaultDatabase()
    {
        return DEFAULT_DATABASE;
    }

    public static DatabaseName systemDatabase()
    {
        return SYSTEM_DATABASE;
    }

    public static DatabaseName database( String name )
    {
        if ( Objects.equals( name, DEFAULT_DATABASE_NAME ) )
        {
            return defaultDatabase();
        }
        else
        {
            return (DatabaseName) (Objects.equals( name, "system" ) ? systemDatabase() : new InternalDatabaseName( name ));
        }
    }
}
