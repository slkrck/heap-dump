package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Result;
import com.neo4j.fabric.shaded.driver.Transaction;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.internal.async.UnmanagedTransaction;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

public class InternalTransaction extends AbstractQueryRunner implements Transaction
{
    private final UnmanagedTransaction tx;

    public InternalTransaction( UnmanagedTransaction tx )
    {
        this.tx = tx;
    }

    public void commit()
    {
        Futures.blockingGet( this.tx.commitAsync(), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while committing the transaction" );
        } );
    }

    public void rollback()
    {
        Futures.blockingGet( this.tx.rollbackAsync(), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while rolling back the transaction" );
        } );
    }

    public void close()
    {
        Futures.blockingGet( this.tx.closeAsync(), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while closing the transaction" );
        } );
    }

    public Result run( Query query )
    {
        ResultCursor cursor = (ResultCursor) Futures.blockingGet( this.tx.runAsync( query, false ), () -> {
            this.terminateConnectionOnThreadInterrupt( "Thread interrupted while running query in transaction" );
        } );
        return new InternalResult( this.tx.connection(), cursor );
    }

    public boolean isOpen()
    {
        return this.tx.isOpen();
    }

    private void terminateConnectionOnThreadInterrupt( String reason )
    {
        this.tx.connection().terminateAndRelease( reason );
    }
}
