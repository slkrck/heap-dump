package com.neo4j.fabric.shaded.driver.internal;

import java.util.concurrent.CompletionStage;

public interface FailableCursor
{
    CompletionStage<Throwable> discardAllFailureAsync();

    CompletionStage<Throwable> pullAllFailureAsync();
}
