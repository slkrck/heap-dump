package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.value.PathValue;
import com.neo4j.fabric.shaded.driver.types.Entity;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Relationship;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class InternalPath implements Path, AsValue
{
    private final List<Node> nodes;
    private final List<Relationship> relationships;
    private final List<Path.Segment> segments;

    public InternalPath( List<Entity> alternatingNodeAndRel )
    {
        this.nodes = this.newList( alternatingNodeAndRel.size() / 2 + 1 );
        this.relationships = this.newList( alternatingNodeAndRel.size() / 2 );
        this.segments = this.newList( alternatingNodeAndRel.size() / 2 );
        if ( alternatingNodeAndRel.size() % 2 == 0 )
        {
            throw new IllegalArgumentException( "An odd number of entities are required to build a path" );
        }
        else
        {
            Node lastNode = null;
            Relationship lastRelationship = null;
            int index = 0;

            for ( Iterator var5 = alternatingNodeAndRel.iterator(); var5.hasNext(); ++index )
            {
                Entity entity = (Entity) var5.next();
                if ( entity == null )
                {
                    throw new IllegalArgumentException( "Path entities cannot be null" );
                }

                String cls;
                if ( index % 2 == 0 )
                {
                    try
                    {
                        lastNode = (Node) entity;
                        if ( !this.nodes.isEmpty() && !isEndpoint( lastNode, lastRelationship ) )
                        {
                            throw new IllegalArgumentException( "Node argument " + index + " is not an endpoint of relationship argument " + (index - 1) );
                        }

                        this.nodes.add( lastNode );
                    }
                    catch ( ClassCastException var9 )
                    {
                        cls = entity.getClass().getName();
                        throw new IllegalArgumentException( "Expected argument " + index + " to be a node " + index + " but found a " + cls + " instead" );
                    }
                }
                else
                {
                    try
                    {
                        lastRelationship = (Relationship) entity;
                        if ( !isEndpoint( lastNode, lastRelationship ) )
                        {
                            throw new IllegalArgumentException( "Node argument " + (index - 1) + " is not an endpoint of relationship argument " + index );
                        }

                        this.relationships.add( lastRelationship );
                    }
                    catch ( ClassCastException var10 )
                    {
                        cls = entity.getClass().getName();
                        throw new IllegalArgumentException( "Expected argument " + index + " to be a relationship but found a " + cls + " instead" );
                    }
                }
            }

            this.buildSegments();
        }
    }

    public InternalPath( Entity... alternatingNodeAndRel )
    {
        this( Arrays.asList( alternatingNodeAndRel ) );
    }

    public InternalPath( List<Path.Segment> segments, List<Node> nodes, List<Relationship> relationships )
    {
        this.segments = segments;
        this.nodes = nodes;
        this.relationships = relationships;
    }

    private static boolean isEndpoint( Node node, Relationship relationship )
    {
        return node.id() == relationship.startNodeId() || node.id() == relationship.endNodeId();
    }

    private <T> List<T> newList( int size )
    {
        return (List) (size == 0 ? Collections.emptyList() : new ArrayList( size ));
    }

    public int length()
    {
        return this.relationships.size();
    }

    public boolean contains( Node node )
    {
        return this.nodes.contains( node );
    }

    public boolean contains( Relationship relationship )
    {
        return this.relationships.contains( relationship );
    }

    public Iterable<Node> nodes()
    {
        return this.nodes;
    }

    public Iterable<Relationship> relationships()
    {
        return this.relationships;
    }

    public Node start()
    {
        return (Node) this.nodes.get( 0 );
    }

    public Node end()
    {
        return (Node) this.nodes.get( this.nodes.size() - 1 );
    }

    public Iterator<Path.Segment> iterator()
    {
        return this.segments.iterator();
    }

    public Value asValue()
    {
        return new PathValue( this );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalPath segments1 = (InternalPath) o;
            return this.segments.equals( segments1.segments );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.segments.hashCode();
    }

    public String toString()
    {
        return "path" + this.segments;
    }

    private void buildSegments()
    {
        for ( int i = 0; i < this.relationships.size(); ++i )
        {
            this.segments.add( new InternalPath.SelfContainedSegment( (Node) this.nodes.get( i ), (Relationship) this.relationships.get( i ),
                    (Node) this.nodes.get( i + 1 ) ) );
        }
    }

    public static class SelfContainedSegment implements Path.Segment
    {
        private final Node start;
        private final Relationship relationship;
        private final Node end;

        public SelfContainedSegment( Node start, Relationship relationship, Node end )
        {
            this.start = start;
            this.relationship = relationship;
            this.end = end;
        }

        public Node start()
        {
            return this.start;
        }

        public Relationship relationship()
        {
            return this.relationship;
        }

        public Node end()
        {
            return this.end;
        }

        public boolean equals( Object other )
        {
            if ( this == other )
            {
                return true;
            }
            else if ( other != null && this.getClass() == other.getClass() )
            {
                InternalPath.SelfContainedSegment that = (InternalPath.SelfContainedSegment) other;
                return this.start.equals( that.start ) && this.end.equals( that.end ) && this.relationship.equals( that.relationship );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            int result = this.start.hashCode();
            result = 31 * result + this.relationship.hashCode();
            result = 31 * result + this.end.hashCode();
            return result;
        }

        public String toString()
        {
            return String.format( this.relationship.startNodeId() == this.start.id() ? "(%s)-[%s:%s]->(%s)" : "(%s)<-[%s:%s]-(%s)", this.start.id(),
                    this.relationship.id(), this.relationship.type(), this.end.id() );
        }
    }
}
