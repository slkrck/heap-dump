package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Driver;
import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.Metrics;
import com.neo4j.fabric.shaded.driver.Session;
import com.neo4j.fabric.shaded.driver.SessionConfig;
import com.neo4j.fabric.shaded.driver.async.AsyncSession;
import com.neo4j.fabric.shaded.driver.internal.async.InternalAsyncSession;
import com.neo4j.fabric.shaded.driver.internal.async.NetworkSession;
import com.neo4j.fabric.shaded.driver.internal.metrics.MetricsProvider;
import com.neo4j.fabric.shaded.driver.internal.reactive.InternalRxSession;
import com.neo4j.fabric.shaded.driver.internal.security.SecurityPlan;
import com.neo4j.fabric.shaded.driver.internal.types.InternalTypeSystem;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.reactive.RxSession;
import com.neo4j.fabric.shaded.driver.types.TypeSystem;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicBoolean;

public class InternalDriver implements Driver
{
    private final SecurityPlan securityPlan;
    private final SessionFactory sessionFactory;
    private final Logger log;
    private final MetricsProvider metricsProvider;
    private AtomicBoolean closed = new AtomicBoolean( false );

    InternalDriver( SecurityPlan securityPlan, SessionFactory sessionFactory, MetricsProvider metricsProvider, Logging logging )
    {
        this.securityPlan = securityPlan;
        this.sessionFactory = sessionFactory;
        this.metricsProvider = metricsProvider;
        this.log = logging.getLog( Driver.class.getSimpleName() );
    }

    private static RuntimeException driverCloseException()
    {
        return new IllegalStateException( "This driver instance has already been closed" );
    }

    public Session session()
    {
        return new InternalSession( this.newSession( SessionConfig.defaultConfig() ) );
    }

    public Session session( SessionConfig sessionConfig )
    {
        return new InternalSession( this.newSession( sessionConfig ) );
    }

    public RxSession rxSession()
    {
        return new InternalRxSession( this.newSession( SessionConfig.defaultConfig() ) );
    }

    public RxSession rxSession( SessionConfig sessionConfig )
    {
        return new InternalRxSession( this.newSession( sessionConfig ) );
    }

    public AsyncSession asyncSession()
    {
        return new InternalAsyncSession( this.newSession( SessionConfig.defaultConfig() ) );
    }

    public AsyncSession asyncSession( SessionConfig sessionConfig )
    {
        return new InternalAsyncSession( this.newSession( sessionConfig ) );
    }

    public Metrics metrics()
    {
        return this.metricsProvider.metrics();
    }

    public boolean isMetricsEnabled()
    {
        return this.metricsProvider.isMetricsEnabled();
    }

    public boolean isEncrypted()
    {
        this.assertOpen();
        return this.securityPlan.requiresEncryption();
    }

    public void close()
    {
        Futures.blockingGet( this.closeAsync() );
    }

    public CompletionStage<Void> closeAsync()
    {
        if ( this.closed.compareAndSet( false, true ) )
        {
            this.log.info( "Closing driver instance %s", this.hashCode() );
            return this.sessionFactory.close();
        }
        else
        {
            return Futures.completedWithNull();
        }
    }

    public final TypeSystem defaultTypeSystem()
    {
        return InternalTypeSystem.TYPE_SYSTEM;
    }

    public CompletionStage<Void> verifyConnectivityAsync()
    {
        return this.sessionFactory.verifyConnectivity();
    }

    public boolean supportsMultiDb()
    {
        return (Boolean) Futures.blockingGet( this.supportsMultiDbAsync() );
    }

    public CompletionStage<Boolean> supportsMultiDbAsync()
    {
        return this.sessionFactory.supportsMultiDb();
    }

    public void verifyConnectivity()
    {
        Futures.blockingGet( this.verifyConnectivityAsync() );
    }

    public SessionFactory getSessionFactory()
    {
        return this.sessionFactory;
    }

    public NetworkSession newSession( SessionConfig config )
    {
        this.assertOpen();
        NetworkSession session = this.sessionFactory.newInstance( config );
        if ( this.closed.get() )
        {
            throw driverCloseException();
        }
        else
        {
            return session;
        }
    }

    private void assertOpen()
    {
        if ( this.closed.get() )
        {
            throw driverCloseException();
        }
    }
}
