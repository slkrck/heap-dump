package com.neo4j.fabric.shaded.driver.internal.packstream;

import java.io.IOException;

public interface PackOutput
{
    PackOutput writeByte( byte var1 ) throws IOException;

    PackOutput writeBytes( byte[] var1 ) throws IOException;

    PackOutput writeShort( short var1 ) throws IOException;

    PackOutput writeInt( int var1 ) throws IOException;

    PackOutput writeLong( long var1 ) throws IOException;

    PackOutput writeDouble( double var1 ) throws IOException;
}
