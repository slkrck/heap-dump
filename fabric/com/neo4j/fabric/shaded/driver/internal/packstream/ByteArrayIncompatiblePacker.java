package com.neo4j.fabric.shaded.driver.internal.packstream;

import java.io.IOException;

public class ByteArrayIncompatiblePacker extends PackStream.Packer
{
    public ByteArrayIncompatiblePacker( PackOutput out )
    {
        super( out );
    }

    public void packBytesHeader( int size ) throws IOException
    {
        throw new PackStream.UnPackable( "Packing bytes is not supported as the current server this driver connected to does not support unpack bytes." );
    }
}
