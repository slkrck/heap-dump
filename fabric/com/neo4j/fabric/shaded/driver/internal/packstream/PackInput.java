package com.neo4j.fabric.shaded.driver.internal.packstream;

import java.io.IOException;

public interface PackInput
{
    byte readByte() throws IOException;

    short readShort() throws IOException;

    int readInt() throws IOException;

    long readLong() throws IOException;

    double readDouble() throws IOException;

    void readBytes( byte[] var1, int var2, int var3 ) throws IOException;

    byte peekByte() throws IOException;
}
