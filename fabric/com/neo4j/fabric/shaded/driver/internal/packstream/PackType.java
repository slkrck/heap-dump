package com.neo4j.fabric.shaded.driver.internal.packstream;

public enum PackType
{
    NULL,
    BOOLEAN,
    INTEGER,
    FLOAT,
    BYTES,
    STRING,
    LIST,
    MAP,
    STRUCT;
}
