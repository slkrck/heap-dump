package com.neo4j.fabric.shaded.driver.internal.packstream;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PackStream
{
    public static final byte TINY_STRING = -128;
    public static final byte TINY_LIST = -112;
    public static final byte TINY_MAP = -96;
    public static final byte TINY_STRUCT = -80;
    public static final byte NULL = -64;
    public static final byte FLOAT_64 = -63;
    public static final byte FALSE = -62;
    public static final byte TRUE = -61;
    public static final byte RESERVED_C4 = -60;
    public static final byte RESERVED_C5 = -59;
    public static final byte RESERVED_C6 = -58;
    public static final byte RESERVED_C7 = -57;
    public static final byte INT_8 = -56;
    public static final byte INT_16 = -55;
    public static final byte INT_32 = -54;
    public static final byte INT_64 = -53;
    public static final byte BYTES_8 = -52;
    public static final byte BYTES_16 = -51;
    public static final byte BYTES_32 = -50;
    public static final byte RESERVED_CF = -49;
    public static final byte STRING_8 = -48;
    public static final byte STRING_16 = -47;
    public static final byte STRING_32 = -46;
    public static final byte RESERVED_D3 = -45;
    public static final byte LIST_8 = -44;
    public static final byte LIST_16 = -43;
    public static final byte LIST_32 = -42;
    public static final byte RESERVED_D7 = -41;
    public static final byte MAP_8 = -40;
    public static final byte MAP_16 = -39;
    public static final byte MAP_32 = -38;
    public static final byte RESERVED_DB = -37;
    public static final byte STRUCT_8 = -36;
    public static final byte STRUCT_16 = -35;
    public static final byte RESERVED_DE = -34;
    public static final byte RESERVED_DF = -33;
    public static final byte RESERVED_E0 = -32;
    public static final byte RESERVED_E1 = -31;
    public static final byte RESERVED_E2 = -30;
    public static final byte RESERVED_E3 = -29;
    public static final byte RESERVED_E4 = -28;
    public static final byte RESERVED_E5 = -27;
    public static final byte RESERVED_E6 = -26;
    public static final byte RESERVED_E7 = -25;
    public static final byte RESERVED_E8 = -24;
    public static final byte RESERVED_E9 = -23;
    public static final byte RESERVED_EA = -22;
    public static final byte RESERVED_EB = -21;
    public static final byte RESERVED_EC = -20;
    public static final byte RESERVED_ED = -19;
    public static final byte RESERVED_EE = -18;
    public static final byte RESERVED_EF = -17;
    private static final long PLUS_2_TO_THE_31 = 2147483648L;
    private static final long PLUS_2_TO_THE_16 = 65536L;
    private static final long PLUS_2_TO_THE_15 = 32768L;
    private static final long PLUS_2_TO_THE_7 = 128L;
    private static final long MINUS_2_TO_THE_4 = -16L;
    private static final long MINUS_2_TO_THE_7 = -128L;
    private static final long MINUS_2_TO_THE_15 = -32768L;
    private static final long MINUS_2_TO_THE_31 = -2147483648L;
    private static final String EMPTY_STRING = "";
    private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
    private static final Charset UTF_8 = Charset.forName( "UTF-8" );

    private PackStream()
    {
    }

    public static class UnPackable extends PackStream.PackStreamException
    {
        private static final long serialVersionUID = 2408740707769711365L;

        public UnPackable( String message )
        {
            super( message );
        }
    }

    public static class Unexpected extends PackStream.PackStreamException
    {
        private static final long serialVersionUID = 5004685868740125469L;

        public Unexpected( String message )
        {
            super( message );
        }
    }

    public static class Overflow extends PackStream.PackStreamException
    {
        private static final long serialVersionUID = -923071934446993659L;

        public Overflow( String message )
        {
            super( message );
        }
    }

    public static class EndOfStream extends PackStream.PackStreamException
    {
        private static final long serialVersionUID = 5102836237108105603L;

        public EndOfStream( String message )
        {
            super( message );
        }
    }

    public static class PackStreamException extends IOException
    {
        private static final long serialVersionUID = -1491422133282345421L;

        protected PackStreamException( String message )
        {
            super( message );
        }
    }

    public static class Unpacker
    {
        private PackInput in;

        public Unpacker( PackInput in )
        {
            this.in = in;
        }

        public long unpackStructHeader() throws IOException
        {
            byte markerByte = this.in.readByte();
            byte markerHighNibble = (byte) (markerByte & 240);
            byte markerLowNibble = (byte) (markerByte & 15);
            if ( markerHighNibble == -80 )
            {
                return (long) markerLowNibble;
            }
            else
            {
                switch ( markerByte )
                {
                case -36:
                    return (long) this.unpackUINT8();
                case -35:
                    return (long) this.unpackUINT16();
                default:
                    throw new PackStream.Unexpected( "Expected a struct, but got: " + Integer.toHexString( markerByte ) );
                }
            }
        }

        public byte unpackStructSignature() throws IOException
        {
            return this.in.readByte();
        }

        public long unpackListHeader() throws IOException
        {
            byte markerByte = this.in.readByte();
            byte markerHighNibble = (byte) (markerByte & 240);
            byte markerLowNibble = (byte) (markerByte & 15);
            if ( markerHighNibble == -112 )
            {
                return (long) markerLowNibble;
            }
            else
            {
                switch ( markerByte )
                {
                case -44:
                    return (long) this.unpackUINT8();
                case -43:
                    return (long) this.unpackUINT16();
                case -42:
                    return this.unpackUINT32();
                default:
                    throw new PackStream.Unexpected( "Expected a list, but got: " + Integer.toHexString( markerByte & 255 ) );
                }
            }
        }

        public long unpackMapHeader() throws IOException
        {
            byte markerByte = this.in.readByte();
            byte markerHighNibble = (byte) (markerByte & 240);
            byte markerLowNibble = (byte) (markerByte & 15);
            if ( markerHighNibble == -96 )
            {
                return (long) markerLowNibble;
            }
            else
            {
                switch ( markerByte )
                {
                case -40:
                    return (long) this.unpackUINT8();
                case -39:
                    return (long) this.unpackUINT16();
                case -38:
                    return this.unpackUINT32();
                default:
                    throw new PackStream.Unexpected( "Expected a map, but got: " + Integer.toHexString( markerByte ) );
                }
            }
        }

        public long unpackLong() throws IOException
        {
            byte markerByte = this.in.readByte();
            if ( (long) markerByte >= -16L )
            {
                return (long) markerByte;
            }
            else
            {
                switch ( markerByte )
                {
                case -56:
                    return (long) this.in.readByte();
                case -55:
                    return (long) this.in.readShort();
                case -54:
                    return (long) this.in.readInt();
                case -53:
                    return this.in.readLong();
                default:
                    throw new PackStream.Unexpected( "Expected an integer, but got: " + Integer.toHexString( markerByte ) );
                }
            }
        }

        public double unpackDouble() throws IOException
        {
            byte markerByte = this.in.readByte();
            if ( markerByte == -63 )
            {
                return this.in.readDouble();
            }
            else
            {
                throw new PackStream.Unexpected( "Expected a double, but got: " + Integer.toHexString( markerByte ) );
            }
        }

        public byte[] unpackBytes() throws IOException
        {
            byte markerByte = this.in.readByte();
            switch ( markerByte )
            {
            case -52:
                return this.unpackRawBytes( this.unpackUINT8() );
            case -51:
                return this.unpackRawBytes( this.unpackUINT16() );
            case -50:
                long size = this.unpackUINT32();
                if ( size <= 2147483647L )
                {
                    return this.unpackRawBytes( (int) size );
                }

                throw new PackStream.Overflow( "BYTES_32 too long for Java" );
            default:
                throw new PackStream.Unexpected( "Expected bytes, but got: 0x" + Integer.toHexString( markerByte & 255 ) );
            }
        }

        public String unpackString() throws IOException
        {
            byte markerByte = this.in.readByte();
            return markerByte == -128 ? "" : new String( this.unpackUtf8( markerByte ), PackStream.UTF_8 );
        }

        public Object unpackNull() throws IOException
        {
            byte markerByte = this.in.readByte();
            if ( markerByte != -64 )
            {
                throw new PackStream.Unexpected( "Expected a null, but got: 0x" + Integer.toHexString( markerByte & 255 ) );
            }
            else
            {
                return null;
            }
        }

        private byte[] unpackUtf8( byte markerByte ) throws IOException
        {
            byte markerHighNibble = (byte) (markerByte & 240);
            byte markerLowNibble = (byte) (markerByte & 15);
            if ( markerHighNibble == -128 )
            {
                return this.unpackRawBytes( markerLowNibble );
            }
            else
            {
                switch ( markerByte )
                {
                case -48:
                    return this.unpackRawBytes( this.unpackUINT8() );
                case -47:
                    return this.unpackRawBytes( this.unpackUINT16() );
                case -46:
                    long size = this.unpackUINT32();
                    if ( size <= 2147483647L )
                    {
                        return this.unpackRawBytes( (int) size );
                    }

                    throw new PackStream.Overflow( "STRING_32 too long for Java" );
                default:
                    throw new PackStream.Unexpected( "Expected a string, but got: 0x" + Integer.toHexString( markerByte & 255 ) );
                }
            }
        }

        public boolean unpackBoolean() throws IOException
        {
            byte markerByte = this.in.readByte();
            switch ( markerByte )
            {
            case -62:
                return false;
            case -61:
                return true;
            default:
                throw new PackStream.Unexpected( "Expected a boolean, but got: 0x" + Integer.toHexString( markerByte & 255 ) );
            }
        }

        private int unpackUINT8() throws IOException
        {
            return this.in.readByte() & 255;
        }

        private int unpackUINT16() throws IOException
        {
            return this.in.readShort() & '\uffff';
        }

        private long unpackUINT32() throws IOException
        {
            return (long) this.in.readInt() & 4294967295L;
        }

        private byte[] unpackRawBytes( int size ) throws IOException
        {
            if ( size == 0 )
            {
                return PackStream.EMPTY_BYTE_ARRAY;
            }
            else
            {
                byte[] heapBuffer = new byte[size];
                this.in.readBytes( heapBuffer, 0, heapBuffer.length );
                return heapBuffer;
            }
        }

        public PackType peekNextType() throws IOException
        {
            byte markerByte = this.in.peekByte();
            byte markerHighNibble = (byte) (markerByte & 240);
            switch ( markerHighNibble )
            {
            case -128:
                return PackType.STRING;
            case -112:
                return PackType.LIST;
            case -96:
                return PackType.MAP;
            case -80:
                return PackType.STRUCT;
            default:
                switch ( markerByte )
                {
                case -64:
                    return PackType.NULL;
                case -63:
                    return PackType.FLOAT;
                case -62:
                case -61:
                    return PackType.BOOLEAN;
                case -60:
                case -59:
                case -58:
                case -57:
                case -56:
                case -55:
                case -54:
                case -53:
                case -49:
                case -45:
                case -41:
                case -37:
                default:
                    return PackType.INTEGER;
                case -52:
                case -51:
                case -50:
                    return PackType.BYTES;
                case -48:
                case -47:
                case -46:
                    return PackType.STRING;
                case -44:
                case -43:
                case -42:
                    return PackType.LIST;
                case -40:
                case -39:
                case -38:
                    return PackType.MAP;
                case -36:
                case -35:
                    return PackType.STRUCT;
                }
            }
        }
    }

    public static class Packer
    {
        private PackOutput out;

        public Packer( PackOutput out )
        {
            this.out = out;
        }

        private void packRaw( byte[] data ) throws IOException
        {
            this.out.writeBytes( data );
        }

        public void packNull() throws IOException
        {
            this.out.writeByte( (byte) -64 );
        }

        public void pack( boolean value ) throws IOException
        {
            this.out.writeByte( (byte) (value ? -61 : -62) );
        }

        public void pack( long value ) throws IOException
        {
            if ( value >= -16L && value < 128L )
            {
                this.out.writeByte( (byte) ((int) value) );
            }
            else if ( value >= -128L && value < -16L )
            {
                this.out.writeByte( (byte) -56 ).writeByte( (byte) ((int) value) );
            }
            else if ( value >= -32768L && value < 32768L )
            {
                this.out.writeByte( (byte) -55 ).writeShort( (short) ((int) value) );
            }
            else if ( value >= -2147483648L && value < 2147483648L )
            {
                this.out.writeByte( (byte) -54 ).writeInt( (int) value );
            }
            else
            {
                this.out.writeByte( (byte) -53 ).writeLong( value );
            }
        }

        public void pack( double value ) throws IOException
        {
            this.out.writeByte( (byte) -63 ).writeDouble( value );
        }

        public void pack( byte[] values ) throws IOException
        {
            if ( values == null )
            {
                this.packNull();
            }
            else
            {
                this.packBytesHeader( values.length );
                this.packRaw( values );
            }
        }

        public void pack( String value ) throws IOException
        {
            if ( value == null )
            {
                this.packNull();
            }
            else
            {
                byte[] utf8 = value.getBytes( PackStream.UTF_8 );
                this.packStringHeader( utf8.length );
                this.packRaw( utf8 );
            }
        }

        private void pack( List<?> values ) throws IOException
        {
            if ( values == null )
            {
                this.packNull();
            }
            else
            {
                this.packListHeader( values.size() );
                Iterator var2 = values.iterator();

                while ( var2.hasNext() )
                {
                    Object value = var2.next();
                    this.pack( value );
                }
            }
        }

        private void pack( Map<?,?> values ) throws IOException
        {
            if ( values == null )
            {
                this.packNull();
            }
            else
            {
                this.packMapHeader( values.size() );
                Iterator var2 = values.keySet().iterator();

                while ( var2.hasNext() )
                {
                    Object key = var2.next();
                    this.pack( key );
                    this.pack( values.get( key ) );
                }
            }
        }

        public void pack( Object value ) throws IOException
        {
            if ( value == null )
            {
                this.packNull();
            }
            else if ( value instanceof Boolean )
            {
                this.pack( (Boolean) value );
            }
            else if ( value instanceof boolean[] )
            {
                this.pack( Collections.singletonList( value ) );
            }
            else if ( value instanceof Byte )
            {
                this.pack( (long) (Byte) value );
            }
            else if ( value instanceof byte[] )
            {
                this.pack( (byte[]) ((byte[]) value) );
            }
            else if ( value instanceof Short )
            {
                this.pack( (long) (Short) value );
            }
            else if ( value instanceof short[] )
            {
                this.pack( Collections.singletonList( value ) );
            }
            else if ( value instanceof Integer )
            {
                this.pack( (long) (Integer) value );
            }
            else if ( value instanceof int[] )
            {
                this.pack( Collections.singletonList( value ) );
            }
            else if ( value instanceof Long )
            {
                this.pack( (Long) value );
            }
            else if ( value instanceof long[] )
            {
                this.pack( Collections.singletonList( value ) );
            }
            else if ( value instanceof Float )
            {
                this.pack( (double) (Float) value );
            }
            else if ( value instanceof float[] )
            {
                this.pack( Collections.singletonList( value ) );
            }
            else if ( value instanceof Double )
            {
                this.pack( (Double) value );
            }
            else if ( value instanceof double[] )
            {
                this.pack( Collections.singletonList( value ) );
            }
            else if ( value instanceof Character )
            {
                this.pack( Character.toString( (Character) value ) );
            }
            else if ( value instanceof char[] )
            {
                this.pack( new String( (char[]) ((char[]) value) ) );
            }
            else if ( value instanceof String )
            {
                this.pack( (String) value );
            }
            else if ( value instanceof String[] )
            {
                this.pack( Collections.singletonList( value ) );
            }
            else if ( value instanceof List )
            {
                this.pack( (List) value );
            }
            else
            {
                if ( !(value instanceof Map) )
                {
                    throw new PackStream.UnPackable( String.format( "Cannot pack object %s", value ) );
                }

                this.pack( (Map) value );
            }
        }

        public void packBytesHeader( int size ) throws IOException
        {
            if ( size <= 127 )
            {
                this.out.writeByte( (byte) -52 ).writeByte( (byte) size );
            }
            else if ( (long) size < 65536L )
            {
                this.out.writeByte( (byte) -51 ).writeShort( (short) size );
            }
            else
            {
                this.out.writeByte( (byte) -50 ).writeInt( size );
            }
        }

        private void packStringHeader( int size ) throws IOException
        {
            if ( size < 16 )
            {
                this.out.writeByte( (byte) (-128 | size) );
            }
            else if ( size <= 127 )
            {
                this.out.writeByte( (byte) -48 ).writeByte( (byte) size );
            }
            else if ( (long) size < 65536L )
            {
                this.out.writeByte( (byte) -47 ).writeShort( (short) size );
            }
            else
            {
                this.out.writeByte( (byte) -46 ).writeInt( size );
            }
        }

        public void packListHeader( int size ) throws IOException
        {
            if ( size < 16 )
            {
                this.out.writeByte( (byte) (-112 | size) );
            }
            else if ( size <= 127 )
            {
                this.out.writeByte( (byte) -44 ).writeByte( (byte) size );
            }
            else if ( (long) size < 65536L )
            {
                this.out.writeByte( (byte) -43 ).writeShort( (short) size );
            }
            else
            {
                this.out.writeByte( (byte) -42 ).writeInt( size );
            }
        }

        public void packMapHeader( int size ) throws IOException
        {
            if ( size < 16 )
            {
                this.out.writeByte( (byte) (-96 | size) );
            }
            else if ( size <= 127 )
            {
                this.out.writeByte( (byte) -40 ).writeByte( (byte) size );
            }
            else if ( (long) size < 65536L )
            {
                this.out.writeByte( (byte) -39 ).writeShort( (short) size );
            }
            else
            {
                this.out.writeByte( (byte) -38 ).writeInt( size );
            }
        }

        public void packStructHeader( int size, byte signature ) throws IOException
        {
            if ( size < 16 )
            {
                this.out.writeByte( (byte) (-80 | size) ).writeByte( signature );
            }
            else if ( size <= 127 )
            {
                this.out.writeByte( (byte) -36 ).writeByte( (byte) size ).writeByte( signature );
            }
            else
            {
                if ( (long) size >= 65536L )
                {
                    throw new PackStream.Overflow( "Structures cannot have more than 65535 fields" );
                }

                this.out.writeByte( (byte) -35 ).writeShort( (short) size ).writeByte( signature );
            }
        }
    }
}
