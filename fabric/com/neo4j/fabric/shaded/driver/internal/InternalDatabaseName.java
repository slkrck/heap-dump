package com.neo4j.fabric.shaded.driver.internal;

import java.util.Objects;
import java.util.Optional;

public class InternalDatabaseName implements DatabaseName
{
    private final String databaseName;

    InternalDatabaseName( String databaseName )
    {
        this.databaseName = (String) Objects.requireNonNull( databaseName );
    }

    public Optional<String> databaseName()
    {
        return Optional.of( this.databaseName );
    }

    public String description()
    {
        return this.databaseName;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalDatabaseName that = (InternalDatabaseName) o;
            return this.databaseName.equals( that.databaseName );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseName} );
    }

    public String toString()
    {
        return "InternalDatabaseName{databaseName='" + this.databaseName + '\'' + '}';
    }
}
