package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.AbstractInternalLogger;

import java.util.regex.Pattern;

public class NettyLogger extends AbstractInternalLogger
{
    private static final Pattern PLACE_HOLDER_PATTERN = Pattern.compile( "\\{\\}" );
    private Logger log;

    public NettyLogger( String name, Logger log )
    {
        super( name );
        this.log = log;
    }

    public boolean isTraceEnabled()
    {
        return this.log.isTraceEnabled();
    }

    public void trace( String msg )
    {
        this.log.trace( msg );
    }

    public void trace( String format, Object arg )
    {
        this.log.trace( this.toDriverLoggerFormat( format ), arg );
    }

    public void trace( String format, Object argA, Object argB )
    {
        this.log.trace( this.toDriverLoggerFormat( format ), argA, argB );
    }

    public void trace( String format, Object... arguments )
    {
        this.log.trace( this.toDriverLoggerFormat( format ), arguments );
    }

    public void trace( String msg, Throwable t )
    {
        this.log.trace( "%s%n%s", msg, t );
    }

    public boolean isDebugEnabled()
    {
        return this.log.isDebugEnabled();
    }

    public void debug( String msg )
    {
        this.log.debug( msg );
    }

    public void debug( String format, Object arg )
    {
        this.log.debug( this.toDriverLoggerFormat( format ), arg );
    }

    public void debug( String format, Object argA, Object argB )
    {
        this.log.debug( this.toDriverLoggerFormat( format ), argA, argB );
    }

    public void debug( String format, Object... arguments )
    {
        this.log.debug( this.toDriverLoggerFormat( format ), arguments );
    }

    public void debug( String msg, Throwable t )
    {
        this.log.debug( "%s%n%s", msg, t );
    }

    public boolean isInfoEnabled()
    {
        return true;
    }

    public void info( String msg )
    {
        this.log.info( msg );
    }

    public void info( String format, Object arg )
    {
        this.log.info( this.toDriverLoggerFormat( format ), arg );
    }

    public void info( String format, Object argA, Object argB )
    {
        this.log.info( this.toDriverLoggerFormat( format ), argA, argB );
    }

    public void info( String format, Object... arguments )
    {
        this.log.info( this.toDriverLoggerFormat( format ), arguments );
    }

    public void info( String msg, Throwable t )
    {
        this.log.info( "%s%n%s", msg, t );
    }

    public boolean isWarnEnabled()
    {
        return true;
    }

    public void warn( String msg )
    {
        this.log.warn( msg );
    }

    public void warn( String format, Object arg )
    {
        this.log.warn( this.toDriverLoggerFormat( format ), arg );
    }

    public void warn( String format, Object... arguments )
    {
        this.log.warn( this.toDriverLoggerFormat( format ), arguments );
    }

    public void warn( String format, Object argA, Object argB )
    {
        this.log.warn( this.toDriverLoggerFormat( format ), argA, argB );
    }

    public void warn( String msg, Throwable t )
    {
        this.log.warn( "%s%n%s", msg, t );
    }

    public boolean isErrorEnabled()
    {
        return true;
    }

    public void error( String msg )
    {
        this.log.error( msg, (Throwable) null );
    }

    public void error( String format, Object arg )
    {
        this.error( format, arg );
    }

    public void error( String format, Object argA, Object argB )
    {
        this.error( format, argA, argB );
    }

    public void error( String format, Object... arguments )
    {
        format = this.toDriverLoggerFormat( format );
        if ( arguments.length == 0 )
        {
            this.log.error( format, (Throwable) null );
        }
        else
        {
            Object arg = arguments[arguments.length - 1];
            if ( arg instanceof Throwable )
            {
                this.log.error( String.format( format, arguments ), (Throwable) arg );
            }
        }
    }

    public void error( String msg, Throwable t )
    {
        this.log.error( msg, t );
    }

    private String toDriverLoggerFormat( String format )
    {
        return PLACE_HOLDER_PATTERN.matcher( format ).replaceAll( "%s" );
    }
}
