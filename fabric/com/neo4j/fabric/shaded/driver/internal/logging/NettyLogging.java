package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLogger;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;

public class NettyLogging extends InternalLoggerFactory
{
    private Logging logging;

    public NettyLogging( Logging logging )
    {
        this.logging = logging;
    }

    protected InternalLogger newInstance( String name )
    {
        return new NettyLogger( name, this.logging.getLog( name ) );
    }
}
