package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;

import java.util.logging.Level;

public class JULogger implements Logger
{
    private final java.util.logging.Logger delegate;
    private final boolean debugEnabled;
    private final boolean traceEnabled;

    public JULogger( String name, Level loggingLevel )
    {
        this.delegate = java.util.logging.Logger.getLogger( name );
        this.delegate.setLevel( loggingLevel );
        this.debugEnabled = this.delegate.isLoggable( Level.FINE );
        this.traceEnabled = this.delegate.isLoggable( Level.FINEST );
    }

    public void error( String message, Throwable cause )
    {
        this.delegate.log( Level.SEVERE, message, cause );
    }

    public void info( String format, Object... params )
    {
        this.delegate.log( Level.INFO, String.format( format, params ) );
    }

    public void warn( String format, Object... params )
    {
        this.delegate.log( Level.WARNING, String.format( format, params ) );
    }

    public void warn( String message, Throwable cause )
    {
        this.delegate.log( Level.WARNING, message, cause );
    }

    public void debug( String format, Object... params )
    {
        if ( this.debugEnabled )
        {
            this.delegate.log( Level.FINE, String.format( format, params ) );
        }
    }

    public void trace( String format, Object... params )
    {
        if ( this.traceEnabled )
        {
            this.delegate.log( Level.FINEST, String.format( format, params ) );
        }
    }

    public boolean isTraceEnabled()
    {
        return this.traceEnabled;
    }

    public boolean isDebugEnabled()
    {
        return this.debugEnabled;
    }
}
