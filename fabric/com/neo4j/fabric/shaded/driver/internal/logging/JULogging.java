package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;

import java.util.logging.Level;

public class JULogging implements Logging
{
    private final Level loggingLevel;

    public JULogging( Level loggingLevel )
    {
        this.loggingLevel = loggingLevel;
    }

    public Logger getLog( String name )
    {
        return new JULogger( name, this.loggingLevel );
    }
}
