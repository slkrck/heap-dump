package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;

public class DevNullLogging implements Logging
{
    public static final Logging DEV_NULL_LOGGING = new DevNullLogging();

    private DevNullLogging()
    {
    }

    public Logger getLog( String name )
    {
        return DevNullLogger.DEV_NULL_LOGGER;
    }
}
