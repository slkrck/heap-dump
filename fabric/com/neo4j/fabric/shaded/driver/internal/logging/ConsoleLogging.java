package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class ConsoleLogging implements Logging
{
    private final Level level;

    public ConsoleLogging( Level level )
    {
        this.level = (Level) Objects.requireNonNull( level );
    }

    public Logger getLog( String name )
    {
        return new ConsoleLogging.ConsoleLogger( name, this.level );
    }

    private static class ShortFormatter extends Formatter
    {
        private ShortFormatter()
        {
        }

        public String format( LogRecord record )
        {
            return LocalDateTime.now().format( DateTimeFormatter.ISO_LOCAL_DATE_TIME ) + " " + record.getLevel() + " " + record.getLoggerName() + " - " +
                    this.formatMessage( record ) + "\n";
        }
    }

    public static class ConsoleLogger extends JULogger
    {
        private final ConsoleHandler handler;

        public ConsoleLogger( String name, Level level )
        {
            super( name, level );
            java.util.logging.Logger logger = java.util.logging.Logger.getLogger( name );
            logger.setUseParentHandlers( false );
            Handler[] handlers = logger.getHandlers();
            Handler[] var5 = handlers;
            int var6 = handlers.length;

            for ( int var7 = 0; var7 < var6; ++var7 )
            {
                Handler handlerToRemove = var5[var7];
                logger.removeHandler( handlerToRemove );
            }

            this.handler = new ConsoleHandler();
            this.handler.setFormatter( new ConsoleLogging.ShortFormatter() );
            this.handler.setLevel( level );
            logger.addHandler( this.handler );
            logger.setLevel( level );
        }
    }
}
