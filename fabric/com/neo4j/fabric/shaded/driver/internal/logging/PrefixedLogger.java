package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;

public class PrefixedLogger extends ReformattedLogger
{
    private final String messagePrefix;

    public PrefixedLogger( Logger delegate )
    {
        this( (String) null, delegate );
    }

    public PrefixedLogger( String messagePrefix, Logger delegate )
    {
        super( delegate );
        this.messagePrefix = messagePrefix;
    }

    protected String reformat( String message )
    {
        return this.messagePrefix == null ? message : String.format( "%s %s", this.messagePrefix, message );
    }
}
