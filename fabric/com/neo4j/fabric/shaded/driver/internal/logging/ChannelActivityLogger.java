package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.connection.ChannelAttributes;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.channel.Channel;
import com.neo4j.fabric.shaded.driver.internal.util.Format;

public class ChannelActivityLogger extends ReformattedLogger
{
    private final Channel channel;
    private final String localChannelId;
    private String dbConnectionId;
    private String serverAddress;

    public ChannelActivityLogger( Channel channel, Logging logging, Class<?> owner )
    {
        this( channel, logging.getLog( owner.getSimpleName() ) );
    }

    private ChannelActivityLogger( Channel channel, Logger delegate )
    {
        super( delegate );
        this.channel = channel;
        this.localChannelId = channel != null ? channel.id().toString() : null;
    }

    protected String reformat( String message )
    {
        if ( this.channel == null )
        {
            return message;
        }
        else
        {
            String dbConnectionId = this.getDbConnectionId();
            String serverAddress = this.getServerAddress();
            return String.format( "[0x%s][%s][%s] %s", this.localChannelId, Format.valueOrEmpty( serverAddress ), Format.valueOrEmpty( dbConnectionId ),
                    message );
        }
    }

    private String getDbConnectionId()
    {
        if ( this.dbConnectionId == null )
        {
            this.dbConnectionId = ChannelAttributes.connectionId( this.channel );
        }

        return this.dbConnectionId;
    }

    private String getServerAddress()
    {
        if ( this.serverAddress == null )
        {
            BoltServerAddress serverAddress = ChannelAttributes.serverAddress( this.channel );
            this.serverAddress = serverAddress != null ? serverAddress.toString() : null;
        }

        return this.serverAddress;
    }
}
