package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;

import java.util.Objects;

public abstract class ReformattedLogger implements Logger
{
    private final Logger delegate;

    protected ReformattedLogger( Logger delegate )
    {
        this.delegate = (Logger) Objects.requireNonNull( delegate );
    }

    public void error( String message, Throwable cause )
    {
        this.delegate.error( this.reformat( message ), cause );
    }

    public void info( String message, Object... params )
    {
        this.delegate.info( this.reformat( message ), params );
    }

    public void warn( String message, Object... params )
    {
        this.delegate.warn( this.reformat( message ), params );
    }

    public void warn( String message, Throwable cause )
    {
        this.delegate.warn( this.reformat( message ), cause );
    }

    public void debug( String message, Object... params )
    {
        if ( this.isDebugEnabled() )
        {
            this.delegate.debug( this.reformat( message ), params );
        }
    }

    public void trace( String message, Object... params )
    {
        if ( this.isTraceEnabled() )
        {
            this.delegate.trace( this.reformat( message ), params );
        }
    }

    public boolean isTraceEnabled()
    {
        return this.delegate.isTraceEnabled();
    }

    public boolean isDebugEnabled()
    {
        return this.delegate.isDebugEnabled();
    }

    protected abstract String reformat( String var1 );
}
