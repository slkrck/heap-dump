package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;

import java.util.Objects;

class Slf4jLogger implements Logger
{
    private final org.slf4j.Logger delegate;

    Slf4jLogger( org.slf4j.Logger delegate )
    {
        this.delegate = (org.slf4j.Logger) Objects.requireNonNull( delegate );
    }

    private static String formatMessage( String messageTemplate, Object... params )
    {
        return String.format( messageTemplate, params );
    }

    public void error( String message, Throwable cause )
    {
        if ( this.delegate.isErrorEnabled() )
        {
            this.delegate.error( message, cause );
        }
    }

    public void info( String message, Object... params )
    {
        if ( this.delegate.isInfoEnabled() )
        {
            this.delegate.info( formatMessage( message, params ) );
        }
    }

    public void warn( String message, Object... params )
    {
        if ( this.delegate.isWarnEnabled() )
        {
            this.delegate.warn( formatMessage( message, params ) );
        }
    }

    public void warn( String message, Throwable cause )
    {
        if ( this.delegate.isWarnEnabled() )
        {
            this.delegate.warn( message, cause );
        }
    }

    public void debug( String message, Object... params )
    {
        if ( this.isDebugEnabled() )
        {
            this.delegate.debug( formatMessage( message, params ) );
        }
    }

    public void trace( String message, Object... params )
    {
        if ( this.isTraceEnabled() )
        {
            this.delegate.trace( formatMessage( message, params ) );
        }
    }

    public boolean isTraceEnabled()
    {
        return this.delegate.isTraceEnabled();
    }

    public boolean isDebugEnabled()
    {
        return this.delegate.isDebugEnabled();
    }
}
