package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import org.slf4j.LoggerFactory;

public class Slf4jLogging implements Logging
{
    public static RuntimeException checkAvailability()
    {
        try
        {
            Class.forName( "org.slf4j.LoggerFactory" );
            return null;
        }
        catch ( Throwable var1 )
        {
            return new IllegalStateException( "SLF4J logging is not available. Please add dependencies on slf4j-api and SLF4J binding (Logback, Log4j, etc.)",
                    var1 );
        }
    }

    public Logger getLog( String name )
    {
        return new Slf4jLogger( LoggerFactory.getLogger( name ) );
    }
}
