package com.neo4j.fabric.shaded.driver.internal.logging;

import com.neo4j.fabric.shaded.driver.Logger;

public class DevNullLogger implements Logger
{
    public static final Logger DEV_NULL_LOGGER = new DevNullLogger();

    private DevNullLogger()
    {
    }

    public void error( String message, Throwable cause )
    {
    }

    public void info( String message, Object... params )
    {
    }

    public void warn( String message, Object... params )
    {
    }

    public void warn( String message, Throwable cause )
    {
    }

    public void debug( String message, Object... params )
    {
    }

    public void trace( String message, Object... params )
    {
    }

    public boolean isTraceEnabled()
    {
        return false;
    }

    public boolean isDebugEnabled()
    {
        return false;
    }
}
