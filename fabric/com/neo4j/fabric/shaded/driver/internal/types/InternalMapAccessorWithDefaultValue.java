package com.neo4j.fabric.shaded.driver.internal.types;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.AsValue;
import com.neo4j.fabric.shaded.driver.types.Entity;
import com.neo4j.fabric.shaded.driver.types.MapAccessorWithDefaultValue;
import com.neo4j.fabric.shaded.driver.types.Node;
import com.neo4j.fabric.shaded.driver.types.Path;
import com.neo4j.fabric.shaded.driver.types.Relationship;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public abstract class InternalMapAccessorWithDefaultValue implements MapAccessorWithDefaultValue
{
    public abstract Value get( String var1 );

    public Value get( String key, Value defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Value get( Value value, Value defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : ((AsValue) value).asValue();
    }

    public Object get( String key, Object defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Object get( Value value, Object defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asObject();
    }

    public Number get( String key, Number defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Number get( Value value, Number defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asNumber();
    }

    public Entity get( String key, Entity defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Entity get( Value value, Entity defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asEntity();
    }

    public Node get( String key, Node defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Node get( Value value, Node defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asNode();
    }

    public Path get( String key, Path defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Path get( Value value, Path defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asPath();
    }

    public Relationship get( String key, Relationship defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Relationship get( Value value, Relationship defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asRelationship();
    }

    public List<Object> get( String key, List<Object> defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private List<Object> get( Value value, List<Object> defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asList();
    }

    public <T> List<T> get( String key, List<T> defaultValue, Function<Value,T> mapFunc )
    {
        return this.get( this.get( key ), defaultValue, mapFunc );
    }

    private <T> List<T> get( Value value, List<T> defaultValue, Function<Value,T> mapFunc )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asList( mapFunc );
    }

    public Map<String,Object> get( String key, Map<String,Object> defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private Map<String,Object> get( Value value, Map<String,Object> defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asMap();
    }

    public <T> Map<String,T> get( String key, Map<String,T> defaultValue, Function<Value,T> mapFunc )
    {
        return this.get( this.get( key ), defaultValue, mapFunc );
    }

    private <T> Map<String,T> get( Value value, Map<String,T> defaultValue, Function<Value,T> mapFunc )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asMap( mapFunc );
    }

    public int get( String key, int defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private int get( Value value, int defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asInt();
    }

    public long get( String key, long defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private long get( Value value, long defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asLong();
    }

    public boolean get( String key, boolean defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private boolean get( Value value, boolean defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asBoolean();
    }

    public String get( String key, String defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private String get( Value value, String defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asString();
    }

    public float get( String key, float defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private float get( Value value, float defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asFloat();
    }

    public double get( String key, double defaultValue )
    {
        return this.get( this.get( key ), defaultValue );
    }

    private double get( Value value, double defaultValue )
    {
        return value.equals( Values.NULL ) ? defaultValue : value.asDouble();
    }
}
