package com.neo4j.fabric.shaded.driver.internal.types;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.types.Type;

public class TypeRepresentation implements Type
{
    private final TypeConstructor tyCon;

    public TypeRepresentation( TypeConstructor tyCon )
    {
        this.tyCon = tyCon;
    }

    public boolean isTypeOf( Value value )
    {
        return this.tyCon.covers( value );
    }

    public String name()
    {
        return this.tyCon == TypeConstructor.LIST ? "LIST OF ANY?" : this.tyCon.toString();
    }

    public TypeConstructor constructor()
    {
        return this.tyCon;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            TypeRepresentation that = (TypeRepresentation) o;
            return this.tyCon == that.tyCon;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.tyCon.hashCode();
    }
}
