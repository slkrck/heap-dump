package com.neo4j.fabric.shaded.driver.internal.types;

import com.neo4j.fabric.shaded.driver.types.Type;
import com.neo4j.fabric.shaded.driver.types.TypeSystem;

public class InternalTypeSystem implements TypeSystem
{
    public static InternalTypeSystem TYPE_SYSTEM = new InternalTypeSystem();
    private final TypeRepresentation anyType;
    private final TypeRepresentation booleanType;
    private final TypeRepresentation bytesType;
    private final TypeRepresentation stringType;
    private final TypeRepresentation numberType;
    private final TypeRepresentation integerType;
    private final TypeRepresentation floatType;
    private final TypeRepresentation listType;
    private final TypeRepresentation mapType;
    private final TypeRepresentation nodeType;
    private final TypeRepresentation relationshipType;
    private final TypeRepresentation pathType;
    private final TypeRepresentation pointType;
    private final TypeRepresentation dateType;
    private final TypeRepresentation timeType;
    private final TypeRepresentation localTimeType;
    private final TypeRepresentation localDateTimeType;
    private final TypeRepresentation dateTimeType;
    private final TypeRepresentation durationType;
    private final TypeRepresentation nullType;

    private InternalTypeSystem()
    {
        this.anyType = this.constructType( TypeConstructor.ANY );
        this.booleanType = this.constructType( TypeConstructor.BOOLEAN );
        this.bytesType = this.constructType( TypeConstructor.BYTES );
        this.stringType = this.constructType( TypeConstructor.STRING );
        this.numberType = this.constructType( TypeConstructor.NUMBER );
        this.integerType = this.constructType( TypeConstructor.INTEGER );
        this.floatType = this.constructType( TypeConstructor.FLOAT );
        this.listType = this.constructType( TypeConstructor.LIST );
        this.mapType = this.constructType( TypeConstructor.MAP );
        this.nodeType = this.constructType( TypeConstructor.NODE );
        this.relationshipType = this.constructType( TypeConstructor.RELATIONSHIP );
        this.pathType = this.constructType( TypeConstructor.PATH );
        this.pointType = this.constructType( TypeConstructor.POINT );
        this.dateType = this.constructType( TypeConstructor.DATE );
        this.timeType = this.constructType( TypeConstructor.TIME );
        this.localTimeType = this.constructType( TypeConstructor.LOCAL_TIME );
        this.localDateTimeType = this.constructType( TypeConstructor.LOCAL_DATE_TIME );
        this.dateTimeType = this.constructType( TypeConstructor.DATE_TIME );
        this.durationType = this.constructType( TypeConstructor.DURATION );
        this.nullType = this.constructType( TypeConstructor.NULL );
    }

    public Type ANY()
    {
        return this.anyType;
    }

    public Type BOOLEAN()
    {
        return this.booleanType;
    }

    public Type BYTES()
    {
        return this.bytesType;
    }

    public Type STRING()
    {
        return this.stringType;
    }

    public Type NUMBER()
    {
        return this.numberType;
    }

    public Type INTEGER()
    {
        return this.integerType;
    }

    public Type FLOAT()
    {
        return this.floatType;
    }

    public Type LIST()
    {
        return this.listType;
    }

    public Type MAP()
    {
        return this.mapType;
    }

    public Type NODE()
    {
        return this.nodeType;
    }

    public Type RELATIONSHIP()
    {
        return this.relationshipType;
    }

    public Type PATH()
    {
        return this.pathType;
    }

    public Type POINT()
    {
        return this.pointType;
    }

    public Type DATE()
    {
        return this.dateType;
    }

    public Type TIME()
    {
        return this.timeType;
    }

    public Type LOCAL_TIME()
    {
        return this.localTimeType;
    }

    public Type LOCAL_DATE_TIME()
    {
        return this.localDateTimeType;
    }

    public Type DATE_TIME()
    {
        return this.dateTimeType;
    }

    public Type DURATION()
    {
        return this.durationType;
    }

    public Type NULL()
    {
        return this.nullType;
    }

    private TypeRepresentation constructType( TypeConstructor tyCon )
    {
        return new TypeRepresentation( tyCon );
    }
}
