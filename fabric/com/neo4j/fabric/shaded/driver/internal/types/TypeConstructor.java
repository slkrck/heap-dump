package com.neo4j.fabric.shaded.driver.internal.types;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.value.InternalValue;

public enum TypeConstructor
{
    ANY
            {
                public boolean covers( Value value )
                {
                    return !value.isNull();
                }
            },
    BOOLEAN,
    BYTES,
    STRING,
    NUMBER
            {
                public boolean covers( Value value )
                {
                    TypeConstructor valueType = TypeConstructor.typeConstructorOf( value );
                    return valueType == this || valueType == INTEGER || valueType == FLOAT;
                }
            },
    INTEGER,
    FLOAT,
    LIST,
    MAP
            {
                public boolean covers( Value value )
                {
                    TypeConstructor valueType = TypeConstructor.typeConstructorOf( value );
                    return valueType == MAP || valueType == NODE || valueType == RELATIONSHIP;
                }
            },
    NODE,
    RELATIONSHIP,
    PATH,
    POINT,
    DATE,
    TIME,
    LOCAL_TIME,
    LOCAL_DATE_TIME,
    DATE_TIME,
    DURATION,
    NULL;

    private TypeConstructor()
    {
    }

    private static TypeConstructor typeConstructorOf( Value value )
    {
        return ((InternalValue) value).typeConstructor();
    }

    public boolean covers( Value value )
    {
        return this == typeConstructorOf( value );
    }
}
