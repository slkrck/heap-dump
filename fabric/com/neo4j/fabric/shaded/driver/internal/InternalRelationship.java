package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.value.RelationshipValue;
import com.neo4j.fabric.shaded.driver.types.Relationship;

import java.util.Collections;
import java.util.Map;

public class InternalRelationship extends InternalEntity implements Relationship
{
    private final String type;
    private long start;
    private long end;

    public InternalRelationship( long id, long start, long end, String type )
    {
        this( id, start, end, type, Collections.emptyMap() );
    }

    public InternalRelationship( long id, long start, long end, String type, Map<String,Value> properties )
    {
        super( id, properties );
        this.start = start;
        this.end = end;
        this.type = type;
    }

    public boolean hasType( String relationshipType )
    {
        return this.type().equals( relationshipType );
    }

    public void setStartAndEnd( long start, long end )
    {
        this.start = start;
        this.end = end;
    }

    public long startNodeId()
    {
        return this.start;
    }

    public long endNodeId()
    {
        return this.end;
    }

    public String type()
    {
        return this.type;
    }

    public Value asValue()
    {
        return new RelationshipValue( this );
    }

    public String toString()
    {
        return String.format( "relationship<%s>", this.id() );
    }
}
