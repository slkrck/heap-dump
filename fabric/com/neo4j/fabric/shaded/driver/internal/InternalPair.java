package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.util.Pair;

import java.util.Objects;

public class InternalPair<K, V> implements Pair<K,V>
{
    private final K key;
    private final V value;

    protected InternalPair( K key, V value )
    {
        Objects.requireNonNull( key );
        Objects.requireNonNull( value );
        this.key = key;
        this.value = value;
    }

    public static <K, V> Pair<K,V> of( K key, V value )
    {
        return new InternalPair( key, value );
    }

    public K key()
    {
        return this.key;
    }

    public V value()
    {
        return this.value;
    }

    public String toString()
    {
        return String.format( "%s: %s", Objects.toString( this.key ), Objects.toString( this.value ) );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalPair<?,?> that = (InternalPair) o;
            return this.key.equals( that.key ) && this.value.equals( that.value );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.key.hashCode();
        result = 31 * result + this.value.hashCode();
        return result;
    }
}
