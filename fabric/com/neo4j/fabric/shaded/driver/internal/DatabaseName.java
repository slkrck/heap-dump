package com.neo4j.fabric.shaded.driver.internal;

import java.util.Optional;

public interface DatabaseName
{
    Optional<String> databaseName();

    String description();
}
