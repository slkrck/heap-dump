package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.SessionConfig;
import com.neo4j.fabric.shaded.driver.internal.async.NetworkSession;

import java.util.concurrent.CompletionStage;

public interface SessionFactory
{
    NetworkSession newInstance( SessionConfig var1 );

    CompletionStage<Void> verifyConnectivity();

    CompletionStage<Void> close();

    CompletionStage<Boolean> supportsMultiDb();
}
