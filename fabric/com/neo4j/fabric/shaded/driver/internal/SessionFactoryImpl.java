package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Config;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.SessionConfig;
import com.neo4j.fabric.shaded.driver.internal.async.LeakLoggingNetworkSession;
import com.neo4j.fabric.shaded.driver.internal.async.NetworkSession;
import com.neo4j.fabric.shaded.driver.internal.retry.RetryLogic;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionProvider;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

public class SessionFactoryImpl implements SessionFactory
{
    private final ConnectionProvider connectionProvider;
    private final RetryLogic retryLogic;
    private final Logging logging;
    private final boolean leakedSessionsLoggingEnabled;
    private final long defaultFetchSize;

    SessionFactoryImpl( ConnectionProvider connectionProvider, RetryLogic retryLogic, Config config )
    {
        this.connectionProvider = connectionProvider;
        this.leakedSessionsLoggingEnabled = config.logLeakedSessions();
        this.retryLogic = retryLogic;
        this.logging = config.logging();
        this.defaultFetchSize = config.fetchSize();
    }

    public NetworkSession newInstance( SessionConfig sessionConfig )
    {
        BookmarkHolder bookmarkHolder = new DefaultBookmarkHolder( InternalBookmark.from( sessionConfig.bookmarks() ) );
        return this.createSession( this.connectionProvider, this.retryLogic, this.parseDatabaseName( sessionConfig ), sessionConfig.defaultAccessMode(),
                bookmarkHolder, this.parseFetchSize( sessionConfig ), this.logging );
    }

    private long parseFetchSize( SessionConfig sessionConfig )
    {
        return (Long) sessionConfig.fetchSize().orElse( this.defaultFetchSize );
    }

    private DatabaseName parseDatabaseName( SessionConfig sessionConfig )
    {
        return (DatabaseName) sessionConfig.database().flatMap( ( name ) -> {
            return Optional.of( DatabaseNameUtil.database( name ) );
        } ).orElse( DatabaseNameUtil.defaultDatabase() );
    }

    public CompletionStage<Void> verifyConnectivity()
    {
        return this.connectionProvider.verifyConnectivity();
    }

    public CompletionStage<Void> close()
    {
        return this.connectionProvider.close();
    }

    public CompletionStage<Boolean> supportsMultiDb()
    {
        return this.connectionProvider.supportsMultiDb();
    }

    public ConnectionProvider getConnectionProvider()
    {
        return this.connectionProvider;
    }

    private NetworkSession createSession( ConnectionProvider connectionProvider, RetryLogic retryLogic, DatabaseName databaseName, AccessMode mode,
            BookmarkHolder bookmarkHolder, long fetchSize, Logging logging )
    {
        return (NetworkSession) (this.leakedSessionsLoggingEnabled ? new LeakLoggingNetworkSession( connectionProvider, retryLogic, databaseName, mode,
                bookmarkHolder, fetchSize, logging ) : new NetworkSession( connectionProvider, retryLogic, databaseName, mode, bookmarkHolder, fetchSize,
                logging ));
    }
}
