package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.QueryRunner;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Result;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.util.Extract;
import com.neo4j.fabric.shaded.driver.internal.value.MapValue;

import java.util.Map;

public abstract class AbstractQueryRunner implements QueryRunner
{
    public static Value parameters( Record record )
    {
        return record == null ? Values.EmptyMap : parameters( record.asMap() );
    }

    public static Value parameters( Map<String,Object> map )
    {
        return (Value) (map != null && !map.isEmpty() ? new MapValue( Extract.mapOfValues( map ) ) : Values.EmptyMap);
    }

    public final Result run( String query, Value parameters )
    {
        return this.run( new Query( query, parameters ) );
    }

    public final Result run( String query, Map<String,Object> parameters )
    {
        return this.run( query, parameters( parameters ) );
    }

    public final Result run( String query, Record parameters )
    {
        return this.run( query, parameters( parameters ) );
    }

    public final Result run( String query )
    {
        return this.run( query, Values.EmptyMap );
    }
}
