package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.types.Point;

import java.util.Objects;

public class InternalPoint3D implements Point
{
    private final int srid;
    private final double x;
    private final double y;
    private final double z;

    public InternalPoint3D( int srid, double x, double y, double z )
    {
        this.srid = srid;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int srid()
    {
        return this.srid;
    }

    public double x()
    {
        return this.x;
    }

    public double y()
    {
        return this.y;
    }

    public double z()
    {
        return this.z;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InternalPoint3D that = (InternalPoint3D) o;
            return this.srid == that.srid && Double.compare( that.x, this.x ) == 0 && Double.compare( that.y, this.y ) == 0 &&
                    Double.compare( that.z, this.z ) == 0;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.srid, this.x, this.y, this.z} );
    }

    public String toString()
    {
        return "Point{srid=" + this.srid + ", x=" + this.x + ", y=" + this.y + ", z=" + this.z + '}';
    }
}
