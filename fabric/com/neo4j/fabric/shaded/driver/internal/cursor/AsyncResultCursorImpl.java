package com.neo4j.fabric.shaded.driver.internal.cursor;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.exceptions.NoSuchRecordException;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullAllResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import java.util.function.Function;

public class AsyncResultCursorImpl implements AsyncResultCursor
{
    private final RunResponseHandler runHandler;
    private final PullAllResponseHandler pullAllHandler;

    public AsyncResultCursorImpl( RunResponseHandler runHandler, PullAllResponseHandler pullAllHandler )
    {
        this.runHandler = runHandler;
        this.pullAllHandler = pullAllHandler;
    }

    public List<String> keys()
    {
        return this.runHandler.queryKeys();
    }

    public CompletionStage<ResultSummary> consumeAsync()
    {
        return this.pullAllHandler.consumeAsync();
    }

    public CompletionStage<Record> nextAsync()
    {
        return this.pullAllHandler.nextAsync();
    }

    public CompletionStage<Record> peekAsync()
    {
        return this.pullAllHandler.peekAsync();
    }

    public CompletionStage<Record> singleAsync()
    {
        return this.nextAsync().thenCompose( ( firstRecord ) -> {
            if ( firstRecord == null )
            {
                throw new NoSuchRecordException( "Cannot retrieve a single record, because this result is empty." );
            }
            else
            {
                return this.nextAsync().thenApply( ( secondRecord ) -> {
                    if ( secondRecord != null )
                    {
                        throw new NoSuchRecordException(
                                "Expected a result with a single record, but this result contains at least one more. Ensure your query returns only one record." );
                    }
                    else
                    {
                        return firstRecord;
                    }
                } );
            }
        } );
    }

    public CompletionStage<ResultSummary> forEachAsync( Consumer<Record> action )
    {
        CompletableFuture<Void> resultFuture = new CompletableFuture();
        this.internalForEachAsync( action, resultFuture );
        return resultFuture.thenCompose( ( ignore ) -> {
            return this.consumeAsync();
        } );
    }

    public CompletionStage<List<Record>> listAsync()
    {
        return this.listAsync( Function.identity() );
    }

    public <T> CompletionStage<List<T>> listAsync( Function<Record,T> mapFunction )
    {
        return this.pullAllHandler.listAsync( mapFunction );
    }

    public CompletionStage<Throwable> discardAllFailureAsync()
    {
        return this.consumeAsync().handle( ( summary, error ) -> {
            return error;
        } );
    }

    public CompletionStage<Throwable> pullAllFailureAsync()
    {
        return this.pullAllHandler.pullAllFailureAsync();
    }

    private void internalForEachAsync( Consumer<Record> action, CompletableFuture<Void> resultFuture )
    {
        CompletionStage<Record> recordFuture = this.nextAsync();
        recordFuture.whenCompleteAsync( ( record, completionError ) -> {
            Throwable error = Futures.completionExceptionCause( completionError );
            if ( error != null )
            {
                resultFuture.completeExceptionally( error );
            }
            else if ( record != null )
            {
                try
                {
                    action.accept( record );
                }
                catch ( Throwable var7 )
                {
                    resultFuture.completeExceptionally( var7 );
                    return;
                }

                this.internalForEachAsync( action, resultFuture );
            }
            else
            {
                resultFuture.complete( (Object) null );
            }
        } );
    }
}
