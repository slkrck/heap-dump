package com.neo4j.fabric.shaded.driver.internal.cursor;

import java.util.concurrent.CompletionStage;

public interface ResultCursorFactory
{
    CompletionStage<AsyncResultCursor> asyncResult();

    CompletionStage<RxResultCursor> rxResult();
}
