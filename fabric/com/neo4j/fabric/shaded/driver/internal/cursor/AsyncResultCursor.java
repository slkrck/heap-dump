package com.neo4j.fabric.shaded.driver.internal.cursor;

import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.internal.FailableCursor;

public interface AsyncResultCursor extends ResultCursor, FailableCursor
{
}
