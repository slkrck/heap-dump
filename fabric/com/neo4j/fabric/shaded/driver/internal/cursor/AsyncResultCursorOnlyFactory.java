package com.neo4j.fabric.shaded.driver.internal.cursor;

import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.internal.handlers.PullAllResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class AsyncResultCursorOnlyFactory implements ResultCursorFactory
{
    protected final Connection connection;
    protected final Message runMessage;
    protected final RunResponseHandler runHandler;
    protected final PullAllResponseHandler pullAllHandler;
    private final boolean waitForRunResponse;

    public AsyncResultCursorOnlyFactory( Connection connection, Message runMessage, RunResponseHandler runHandler, PullAllResponseHandler pullHandler,
            boolean waitForRunResponse )
    {
        Objects.requireNonNull( connection );
        Objects.requireNonNull( runMessage );
        Objects.requireNonNull( runHandler );
        Objects.requireNonNull( pullHandler );
        this.connection = connection;
        this.runMessage = runMessage;
        this.runHandler = runHandler;
        this.pullAllHandler = pullHandler;
        this.waitForRunResponse = waitForRunResponse;
    }

    public CompletionStage<AsyncResultCursor> asyncResult()
    {
        this.connection.write( this.runMessage, this.runHandler );
        this.pullAllHandler.prePopulateRecords();
        return this.waitForRunResponse ? this.runHandler.runFuture().thenApply( ( ignore ) -> {
            return new DisposableAsyncResultCursor( new AsyncResultCursorImpl( this.runHandler, this.pullAllHandler ) );
        } ) : CompletableFuture.completedFuture( new DisposableAsyncResultCursor( new AsyncResultCursorImpl( this.runHandler, this.pullAllHandler ) ) );
    }

    public CompletionStage<RxResultCursor> rxResult()
    {
        return Futures.failedFuture( new ClientException(
                "Driver is connected to the database that does not support driver reactive API. In order to use the driver reactive API, please upgrade to neo4j 4.0.0 or later." ) );
    }
}
