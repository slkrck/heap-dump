package com.neo4j.fabric.shaded.driver.internal.cursor;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.internal.FailableCursor;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.BiConsumer;

import org.reactivestreams.Subscription;

public interface RxResultCursor extends Subscription, FailableCursor
{
    List<String> keys();

    void installRecordConsumer( BiConsumer<Record,Throwable> var1 );

    CompletionStage<ResultSummary> summaryAsync();

    boolean isDone();
}
