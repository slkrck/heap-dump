package com.neo4j.fabric.shaded.driver.internal.cursor;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.internal.util.ErrorUtil;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import java.util.function.Function;

public class DisposableAsyncResultCursor implements AsyncResultCursor
{
    private final AsyncResultCursor delegate;
    private boolean isDisposed;

    public DisposableAsyncResultCursor( AsyncResultCursor delegate )
    {
        this.delegate = delegate;
    }

    public List<String> keys()
    {
        return this.delegate.keys();
    }

    public CompletionStage<ResultSummary> consumeAsync()
    {
        this.isDisposed = true;
        return this.delegate.consumeAsync();
    }

    public CompletionStage<Record> nextAsync()
    {
        return this.assertNotDisposed().thenCompose( ( ignored ) -> {
            return this.delegate.nextAsync();
        } );
    }

    public CompletionStage<Record> peekAsync()
    {
        return this.assertNotDisposed().thenCompose( ( ignored ) -> {
            return this.delegate.peekAsync();
        } );
    }

    public CompletionStage<Record> singleAsync()
    {
        return this.assertNotDisposed().thenCompose( ( ignored ) -> {
            return this.delegate.singleAsync();
        } );
    }

    public CompletionStage<ResultSummary> forEachAsync( Consumer<Record> action )
    {
        return this.assertNotDisposed().thenCompose( ( ignored ) -> {
            return this.delegate.forEachAsync( action );
        } );
    }

    public CompletionStage<List<Record>> listAsync()
    {
        return this.assertNotDisposed().thenCompose( ( ignored ) -> {
            return this.delegate.listAsync();
        } );
    }

    public <T> CompletionStage<List<T>> listAsync( Function<Record,T> mapFunction )
    {
        return this.assertNotDisposed().thenCompose( ( ignored ) -> {
            return this.delegate.listAsync( mapFunction );
        } );
    }

    public CompletionStage<Throwable> discardAllFailureAsync()
    {
        this.isDisposed = true;
        return this.delegate.discardAllFailureAsync();
    }

    public CompletionStage<Throwable> pullAllFailureAsync()
    {
        return this.delegate.pullAllFailureAsync();
    }

    private <T> CompletableFuture<T> assertNotDisposed()
    {
        return this.isDisposed ? Futures.failedFuture( ErrorUtil.newResultConsumedError() ) : Futures.completedWithNull();
    }

    boolean isDisposed()
    {
        return this.isDisposed;
    }
}
