package com.neo4j.fabric.shaded.driver.internal.cursor;

import com.neo4j.fabric.shaded.driver.internal.handlers.PullAllResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.PullResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.messaging.Message;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ResultCursorFactoryImpl implements ResultCursorFactory
{
    private final RunResponseHandler runHandler;
    private final Connection connection;
    private final PullResponseHandler pullHandler;
    private final PullAllResponseHandler pullAllHandler;
    private final boolean waitForRunResponse;
    private final Message runMessage;

    public ResultCursorFactoryImpl( Connection connection, Message runMessage, RunResponseHandler runHandler, PullResponseHandler pullHandler,
            PullAllResponseHandler pullAllHandler, boolean waitForRunResponse )
    {
        Objects.requireNonNull( connection );
        Objects.requireNonNull( runMessage );
        Objects.requireNonNull( runHandler );
        Objects.requireNonNull( pullHandler );
        Objects.requireNonNull( pullAllHandler );
        this.connection = connection;
        this.runMessage = runMessage;
        this.runHandler = runHandler;
        this.pullHandler = pullHandler;
        this.pullAllHandler = pullAllHandler;
        this.waitForRunResponse = waitForRunResponse;
    }

    public CompletionStage<AsyncResultCursor> asyncResult()
    {
        this.connection.write( this.runMessage, this.runHandler );
        this.pullAllHandler.prePopulateRecords();
        return this.waitForRunResponse ? this.runHandler.runFuture().thenApply( ( ignore ) -> {
            return new DisposableAsyncResultCursor( new AsyncResultCursorImpl( this.runHandler, this.pullAllHandler ) );
        } ) : CompletableFuture.completedFuture( new DisposableAsyncResultCursor( new AsyncResultCursorImpl( this.runHandler, this.pullAllHandler ) ) );
    }

    public CompletionStage<RxResultCursor> rxResult()
    {
        this.connection.writeAndFlush( this.runMessage, this.runHandler );
        return this.runHandler.runFuture().thenApply( this::composeRxCursor );
    }

    private RxResultCursor composeRxCursor( Throwable runError )
    {
        return new RxResultCursorImpl( runError, this.runHandler, this.pullHandler );
    }
}
