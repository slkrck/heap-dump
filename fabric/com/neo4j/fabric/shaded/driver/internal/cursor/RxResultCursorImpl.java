package com.neo4j.fabric.shaded.driver.internal.cursor;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.exceptions.TransactionNestingException;
import com.neo4j.fabric.shaded.driver.internal.handlers.RunResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.PullResponseHandler;
import com.neo4j.fabric.shaded.driver.internal.util.ErrorUtil;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.BiConsumer;

public class RxResultCursorImpl implements RxResultCursor
{
    static final BiConsumer<Record,Throwable> DISCARD_RECORD_CONSUMER = ( record, throwable ) -> {
    };
    private final RunResponseHandler runHandler;
    private final PullResponseHandler pullHandler;
    private final Throwable runResponseError;
    private final CompletableFuture<ResultSummary> summaryFuture;
    private boolean resultConsumed;
    private RxResultCursorImpl.RecordConsumerStatus consumerStatus;

    public RxResultCursorImpl( RunResponseHandler runHandler, PullResponseHandler pullHandler )
    {
        this( (Throwable) null, runHandler, pullHandler );
    }

    public RxResultCursorImpl( Throwable runError, RunResponseHandler runHandler, PullResponseHandler pullHandler )
    {
        this.summaryFuture = new CompletableFuture();
        this.consumerStatus = RxResultCursorImpl.RecordConsumerStatus.NOT_INSTALLED;
        Objects.requireNonNull( runHandler );
        Objects.requireNonNull( pullHandler );
        this.assertRunResponseArrived( runHandler );
        this.runResponseError = runError;
        this.runHandler = runHandler;
        this.pullHandler = pullHandler;
        this.installSummaryConsumer();
    }

    public List<String> keys()
    {
        return this.runHandler.queryKeys();
    }

    public void installRecordConsumer( BiConsumer<Record,Throwable> recordConsumer )
    {
        if ( this.resultConsumed )
        {
            throw ErrorUtil.newResultConsumedError();
        }
        else if ( !this.consumerStatus.isInstalled() )
        {
            this.consumerStatus = recordConsumer == DISCARD_RECORD_CONSUMER ? RxResultCursorImpl.RecordConsumerStatus.DISCARD_INSTALLED
                                                                            : RxResultCursorImpl.RecordConsumerStatus.INSTALLED;
            this.pullHandler.installRecordConsumer( recordConsumer );
            this.assertRunCompletedSuccessfully();
        }
    }

    public void request( long n )
    {
        this.pullHandler.request( n );
    }

    public void cancel()
    {
        this.pullHandler.cancel();
    }

    public CompletionStage<Throwable> discardAllFailureAsync()
    {
        return this.summaryAsync().thenApply( ( summary ) -> {
            return (Throwable) null;
        } ).exceptionally( ( error ) -> {
            return error;
        } );
    }

    public CompletionStage<Throwable> pullAllFailureAsync()
    {
        return (CompletionStage) (this.consumerStatus.isInstalled() && !this.isDone() ? CompletableFuture.completedFuture( new TransactionNestingException(
                "You cannot run another query or begin a new transaction in the same session before you've fully consumed the previous run result." ) )
                                                                                      : this.discardAllFailureAsync());
    }

    public CompletionStage<ResultSummary> summaryAsync()
    {
        if ( !this.isDone() && !this.resultConsumed )
        {
            this.installRecordConsumer( DISCARD_RECORD_CONSUMER );
            this.cancel();
            this.resultConsumed = true;
        }

        return this.summaryFuture;
    }

    public boolean isDone()
    {
        return this.summaryFuture.isDone();
    }

    private void assertRunCompletedSuccessfully()
    {
        if ( this.runResponseError != null )
        {
            this.pullHandler.onFailure( this.runResponseError );
        }
    }

    private void installSummaryConsumer()
    {
        this.pullHandler.installSummaryConsumer( ( summary, error ) -> {
            if ( error != null && this.consumerStatus.isDiscardConsumer() )
            {
                this.summaryFuture.completeExceptionally( error );
            }
            else if ( summary != null )
            {
                this.summaryFuture.complete( summary );
            }
        } );
    }

    private void assertRunResponseArrived( RunResponseHandler runHandler )
    {
        if ( !runHandler.runFuture().isDone() )
        {
            throw new IllegalStateException( "Should wait for response of RUN before allowing PULL." );
        }
    }

    static enum RecordConsumerStatus
    {
        NOT_INSTALLED( false, false ),
        INSTALLED( true, false ),
        DISCARD_INSTALLED( true, true );

        private final boolean isInstalled;
        private final boolean isDiscardConsumer;

        private RecordConsumerStatus( boolean isInstalled, boolean isDiscardConsumer )
        {
            this.isInstalled = isInstalled;
            this.isDiscardConsumer = isDiscardConsumer;
        }

        boolean isInstalled()
        {
            return this.isInstalled;
        }

        boolean isDiscardConsumer()
        {
            return this.isDiscardConsumer;
        }
    }
}
