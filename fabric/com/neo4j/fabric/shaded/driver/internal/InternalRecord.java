package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.types.InternalMapAccessorWithDefaultValue;
import com.neo4j.fabric.shaded.driver.internal.util.Extract;
import com.neo4j.fabric.shaded.driver.internal.util.Format;
import com.neo4j.fabric.shaded.driver.util.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Function;

public class InternalRecord extends InternalMapAccessorWithDefaultValue implements Record
{
    private final List<String> keys;
    private final Value[] values;
    private int hashCode = 0;

    public InternalRecord( List<String> keys, Value[] values )
    {
        this.keys = keys;
        this.values = values;
    }

    public List<String> keys()
    {
        return this.keys;
    }

    public List<Value> values()
    {
        return Arrays.asList( this.values );
    }

    public List<Pair<String,Value>> fields()
    {
        return Extract.fields( this, Values.ofValue() );
    }

    public int index( String key )
    {
        int result = this.keys.indexOf( key );
        if ( result == -1 )
        {
            throw new NoSuchElementException( "Unknown key: " + key );
        }
        else
        {
            return result;
        }
    }

    public boolean containsKey( String key )
    {
        return this.keys.contains( key );
    }

    public Value get( String key )
    {
        int fieldIndex = this.keys.indexOf( key );
        return fieldIndex == -1 ? Values.NULL : this.values[fieldIndex];
    }

    public Value get( int index )
    {
        return index >= 0 && index < this.values.length ? this.values[index] : Values.NULL;
    }

    public int size()
    {
        return this.values.length;
    }

    public Map<String,Object> asMap()
    {
        return Extract.map( (Record) this, Values.ofObject() );
    }

    public <T> Map<String,T> asMap( Function<Value,T> mapper )
    {
        return Extract.map( (Record) this, mapper );
    }

    public String toString()
    {
        return String.format( "Record<%s>", Format.formatPairs( this.asMap( Values.ofValue() ) ) );
    }

    public boolean equals( Object other )
    {
        if ( this == other )
        {
            return true;
        }
        else if ( other instanceof Record )
        {
            Record otherRecord = (Record) other;
            int size = this.size();
            if ( size != otherRecord.size() )
            {
                return false;
            }
            else if ( !this.keys.equals( otherRecord.keys() ) )
            {
                return false;
            }
            else
            {
                for ( int i = 0; i < size; ++i )
                {
                    Value value = this.get( i );
                    Value otherValue = otherRecord.get( i );
                    if ( !value.equals( otherValue ) )
                    {
                        return false;
                    }
                }

                return true;
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        if ( this.hashCode == 0 )
        {
            this.hashCode = 31 * this.keys.hashCode() + Arrays.hashCode( this.values );
        }

        return this.hashCode;
    }
}
