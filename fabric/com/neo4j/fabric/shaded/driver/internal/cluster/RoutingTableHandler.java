package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.RoutingErrorHandler;
import com.neo4j.fabric.shaded.driver.internal.async.ConnectionContext;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class RoutingTableHandler implements RoutingErrorHandler
{
    private final RoutingTable routingTable;
    private final DatabaseName databaseName;
    private final RoutingTableRegistry routingTableRegistry;
    private final ConnectionPool connectionPool;
    private final Rediscovery rediscovery;
    private final Logger log;
    private final long routingTablePurgeDelayMs;
    private volatile CompletableFuture<RoutingTable> refreshRoutingTableFuture;

    public RoutingTableHandler( RoutingTable routingTable, Rediscovery rediscovery, ConnectionPool connectionPool, RoutingTableRegistry routingTableRegistry,
            Logger log, long routingTablePurgeDelayMs )
    {
        this.routingTable = routingTable;
        this.databaseName = routingTable.database();
        this.rediscovery = rediscovery;
        this.connectionPool = connectionPool;
        this.routingTableRegistry = routingTableRegistry;
        this.log = log;
        this.routingTablePurgeDelayMs = routingTablePurgeDelayMs;
    }

    public void onConnectionFailure( BoltServerAddress address )
    {
        this.routingTable.forget( address );
    }

    public void onWriteFailure( BoltServerAddress address )
    {
        this.routingTable.forgetWriter( address );
    }

    synchronized CompletionStage<RoutingTable> refreshRoutingTable( ConnectionContext context )
    {
        if ( this.refreshRoutingTableFuture != null )
        {
            return this.refreshRoutingTableFuture;
        }
        else if ( this.routingTable.isStaleFor( context.mode() ) )
        {
            this.log.info( "Routing table for database '%s' is stale. %s", this.databaseName.description(), this.routingTable );
            CompletableFuture<RoutingTable> resultFuture = new CompletableFuture();
            this.refreshRoutingTableFuture = resultFuture;
            this.rediscovery.lookupClusterComposition( this.routingTable, this.connectionPool, context.rediscoveryBookmark() ).whenComplete(
                    ( composition, completionError ) -> {
                        Throwable error = Futures.completionExceptionCause( completionError );
                        if ( error != null )
                        {
                            this.clusterCompositionLookupFailed( error );
                        }
                        else
                        {
                            this.freshClusterCompositionFetched( composition );
                        }
                    } );
            return resultFuture;
        }
        else
        {
            return CompletableFuture.completedFuture( this.routingTable );
        }
    }

    private synchronized void freshClusterCompositionFetched( ClusterComposition composition )
    {
        try
        {
            this.routingTable.update( composition );
            this.routingTableRegistry.removeAged();
            this.connectionPool.retainAll( this.routingTableRegistry.allServers() );
            this.log.info( "Updated routing table for database '%s'. %s", this.databaseName.description(), this.routingTable );
            CompletableFuture<RoutingTable> routingTableFuture = this.refreshRoutingTableFuture;
            this.refreshRoutingTableFuture = null;
            routingTableFuture.complete( this.routingTable );
        }
        catch ( Throwable var3 )
        {
            this.clusterCompositionLookupFailed( var3 );
        }
    }

    private synchronized void clusterCompositionLookupFailed( Throwable error )
    {
        this.log.error( String.format( "Failed to update routing table for database '%s'. Current routing table: %s.", this.databaseName.description(),
                this.routingTable ), error );
        this.routingTableRegistry.remove( this.databaseName );
        CompletableFuture<RoutingTable> routingTableFuture = this.refreshRoutingTableFuture;
        this.refreshRoutingTableFuture = null;
        routingTableFuture.completeExceptionally( error );
    }

    public Set<BoltServerAddress> servers()
    {
        return this.routingTable.servers();
    }

    public boolean isRoutingTableAged()
    {
        return this.refreshRoutingTableFuture == null && this.routingTable.hasBeenStaleFor( this.routingTablePurgeDelayMs );
    }

    public RoutingTable routingTable()
    {
        return this.routingTable;
    }
}
