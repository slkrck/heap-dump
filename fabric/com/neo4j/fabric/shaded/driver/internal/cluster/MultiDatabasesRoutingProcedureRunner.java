package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.DatabaseNameUtil;
import com.neo4j.fabric.shaded.driver.internal.ReadOnlyBookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.async.connection.DirectConnection;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.HashMap;

public class MultiDatabasesRoutingProcedureRunner extends RoutingProcedureRunner
{
    static final String DATABASE_NAME = "database";
    static final String MULTI_DB_GET_ROUTING_TABLE = String.format( "CALL dbms.routing.getRoutingTable($%s, $%s)", "context", "database" );

    public MultiDatabasesRoutingProcedureRunner( RoutingContext context )
    {
        super( context );
    }

    BookmarkHolder bookmarkHolder( Bookmark bookmark )
    {
        return new ReadOnlyBookmarkHolder( bookmark );
    }

    Query procedureQuery( ServerVersion serverVersion, DatabaseName databaseName )
    {
        HashMap<String,Value> map = new HashMap();
        map.put( "context", Values.value( (Object) this.context.asMap() ) );
        map.put( "database", Values.value( databaseName.databaseName().orElse( (Object) null ) ) );
        return new Query( MULTI_DB_GET_ROUTING_TABLE, Values.value( (Object) map ) );
    }

    DirectConnection connection( Connection connection )
    {
        return new DirectConnection( connection, DatabaseNameUtil.systemDatabase(), AccessMode.READ );
    }
}
