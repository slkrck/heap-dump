package com.neo4j.fabric.shaded.driver.internal.cluster.loadbalancing;

import java.util.concurrent.atomic.AtomicInteger;

public class RoundRobinArrayIndex
{
    private final AtomicInteger offset;

    RoundRobinArrayIndex()
    {
        this( 0 );
    }

    RoundRobinArrayIndex( int initialOffset )
    {
        this.offset = new AtomicInteger( initialOffset );
    }

    public int next( int arrayLength )
    {
        if ( arrayLength == 0 )
        {
            return -1;
        }
        else
        {
            int nextOffset;
            while ( (nextOffset = this.offset.getAndIncrement()) < 0 )
            {
                this.offset.compareAndSet( nextOffset + 1, 0 );
            }

            return nextOffset % arrayLength;
        }
    }
}
