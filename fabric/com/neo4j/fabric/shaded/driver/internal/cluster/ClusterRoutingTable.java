package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class ClusterRoutingTable implements RoutingTable
{
    private static final int MIN_ROUTERS = 1;
    private final Clock clock;
    private final AddressSet readers;
    private final AddressSet writers;
    private final AddressSet routers;
    private final DatabaseName databaseName;
    private volatile long expirationTimestamp;
    private boolean preferInitialRouter;

    public ClusterRoutingTable( DatabaseName ofDatabase, Clock clock, BoltServerAddress... routingAddresses )
    {
        this( ofDatabase, clock );
        this.routers.update( new LinkedHashSet( Arrays.asList( routingAddresses ) ) );
    }

    private ClusterRoutingTable( DatabaseName ofDatabase, Clock clock )
    {
        this.databaseName = ofDatabase;
        this.clock = clock;
        this.expirationTimestamp = clock.millis() - 1L;
        this.preferInitialRouter = true;
        this.readers = new AddressSet();
        this.writers = new AddressSet();
        this.routers = new AddressSet();
    }

    public boolean isStaleFor( AccessMode mode )
    {
        return this.expirationTimestamp < this.clock.millis() || this.routers.size() < 1 || mode == AccessMode.READ && this.readers.size() == 0 ||
                mode == AccessMode.WRITE && this.writers.size() == 0;
    }

    public boolean hasBeenStaleFor( long extraTime )
    {
        long totalTime = this.expirationTimestamp + extraTime;
        if ( totalTime < 0L )
        {
            totalTime = Long.MAX_VALUE;
        }

        return totalTime < this.clock.millis();
    }

    public synchronized void update( ClusterComposition cluster )
    {
        this.expirationTimestamp = cluster.expirationTimestamp();
        this.readers.update( cluster.readers() );
        this.writers.update( cluster.writers() );
        this.routers.update( cluster.routers() );
        this.preferInitialRouter = !cluster.hasWriters();
    }

    public synchronized void forget( BoltServerAddress address )
    {
        this.routers.remove( address );
        this.readers.remove( address );
        this.writers.remove( address );
    }

    public AddressSet readers()
    {
        return this.readers;
    }

    public AddressSet writers()
    {
        return this.writers;
    }

    public AddressSet routers()
    {
        return this.routers;
    }

    public Set<BoltServerAddress> servers()
    {
        Set<BoltServerAddress> servers = new HashSet();
        Collections.addAll( servers, this.readers.toArray() );
        Collections.addAll( servers, this.writers.toArray() );
        Collections.addAll( servers, this.routers.toArray() );
        return servers;
    }

    public DatabaseName database()
    {
        return this.databaseName;
    }

    public void forgetWriter( BoltServerAddress toRemove )
    {
        this.writers.remove( toRemove );
    }

    public boolean preferInitialRouter()
    {
        return this.preferInitialRouter;
    }

    public synchronized String toString()
    {
        return String.format( "Ttl %s, currentTime %s, routers %s, writers %s, readers %s, database '%s'", this.expirationTimestamp, this.clock.millis(),
                this.routers, this.writers, this.readers, this.databaseName.description() );
    }
}
