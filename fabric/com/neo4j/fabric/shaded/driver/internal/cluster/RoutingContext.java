package com.neo4j.fabric.shaded.driver.internal.cluster;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RoutingContext
{
    public static final RoutingContext EMPTY = new RoutingContext();
    private final Map<String,String> context;

    private RoutingContext()
    {
        this.context = Collections.emptyMap();
    }

    public RoutingContext( URI uri )
    {
        this.context = Collections.unmodifiableMap( parseParameters( uri ) );
    }

    private static Map<String,String> parseParameters( URI uri )
    {
        String query = uri.getQuery();
        if ( query != null && !query.isEmpty() )
        {
            Map<String,String> parameters = new HashMap();
            String[] pairs = query.split( "&" );
            String[] var4 = pairs;
            int var5 = pairs.length;

            for ( int var6 = 0; var6 < var5; ++var6 )
            {
                String pair = var4[var6];
                String[] keyValue = pair.split( "=" );
                if ( keyValue.length != 2 )
                {
                    throw new IllegalArgumentException( "Invalid parameters: '" + pair + "' in URI '" + uri + "'" );
                }

                String key = trimAndVerify( keyValue[0], "key", uri );
                String value = trimAndVerify( keyValue[1], "value", uri );
                String previousValue = (String) parameters.put( key, value );
                if ( previousValue != null )
                {
                    throw new IllegalArgumentException( "Duplicated query parameters with key '" + key + "' in URI '" + uri + "'" );
                }
            }

            return parameters;
        }
        else
        {
            return Collections.emptyMap();
        }
    }

    private static String trimAndVerify( String string, String name, URI uri )
    {
        String result = string.trim();
        if ( result.isEmpty() )
        {
            throw new IllegalArgumentException( "Illegal empty " + name + " in URI query '" + uri + "'" );
        }
        else
        {
            return result;
        }
    }

    public boolean isDefined()
    {
        return !this.context.isEmpty();
    }

    public Map<String,String> asMap()
    {
        return this.context;
    }

    public String toString()
    {
        return "RoutingContext" + this.context;
    }
}
