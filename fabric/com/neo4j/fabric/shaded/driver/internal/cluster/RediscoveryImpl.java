package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.exceptions.DiscoveryException;
import com.neo4j.fabric.shaded.driver.exceptions.FatalDiscoveryException;
import com.neo4j.fabric.shaded.driver.exceptions.SecurityException;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutorGroup;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.net.ServerAddressResolver;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RediscoveryImpl implements Rediscovery
{
    private static final String NO_ROUTERS_AVAILABLE = "Could not perform discovery for database '%s'. No routing server available.";
    private static final String RECOVERABLE_ROUTING_ERROR = "Failed to update routing table with server '%s'.";
    private final BoltServerAddress initialRouter;
    private final RoutingSettings settings;
    private final Logger logger;
    private final ClusterCompositionProvider provider;
    private final ServerAddressResolver resolver;
    private final EventExecutorGroup eventExecutorGroup;

    public RediscoveryImpl( BoltServerAddress initialRouter, RoutingSettings settings, ClusterCompositionProvider provider,
            EventExecutorGroup eventExecutorGroup, ServerAddressResolver resolver, Logger logger )
    {
        this.initialRouter = initialRouter;
        this.settings = settings;
        this.logger = logger;
        this.provider = provider;
        this.resolver = resolver;
        this.eventExecutorGroup = eventExecutorGroup;
    }

    public CompletionStage<ClusterComposition> lookupClusterComposition( RoutingTable routingTable, ConnectionPool connectionPool, Bookmark bookmark )
    {
        CompletableFuture<ClusterComposition> result = new CompletableFuture();
        ServiceUnavailableException baseError = new ServiceUnavailableException(
                String.format( "Could not perform discovery for database '%s'. No routing server available.", routingTable.database().description() ) );
        this.lookupClusterComposition( routingTable, connectionPool, 0, 0L, result, bookmark, baseError );
        return result;
    }

    private void lookupClusterComposition( RoutingTable routingTable, ConnectionPool pool, int failures, long previousDelay,
            CompletableFuture<ClusterComposition> result, Bookmark bookmark, Throwable baseError )
    {
        this.lookup( routingTable, pool, bookmark, baseError ).whenComplete( ( composition, completionError ) -> {
            Throwable error = Futures.completionExceptionCause( completionError );
            if ( error != null )
            {
                result.completeExceptionally( error );
            }
            else if ( composition != null )
            {
                result.complete( composition );
            }
            else
            {
                int newFailures = failures + 1;
                if ( newFailures >= this.settings.maxRoutingFailures() )
                {
                    result.completeExceptionally( baseError );
                }
                else
                {
                    long nextDelay = Math.max( this.settings.retryTimeoutDelay(), previousDelay * 2L );
                    this.logger.info( "Unable to fetch new routing table, will try again in " + nextDelay + "ms" );
                    this.eventExecutorGroup.next().schedule( () -> {
                        this.lookupClusterComposition( routingTable, pool, newFailures, nextDelay, result, bookmark, baseError );
                    }, nextDelay, TimeUnit.MILLISECONDS );
                }
            }
        } );
    }

    private CompletionStage<ClusterComposition> lookup( RoutingTable routingTable, ConnectionPool connectionPool, Bookmark bookmark, Throwable baseError )
    {
        CompletionStage compositionStage;
        if ( routingTable.preferInitialRouter() )
        {
            compositionStage = this.lookupOnInitialRouterThenOnKnownRouters( routingTable, connectionPool, bookmark, baseError );
        }
        else
        {
            compositionStage = this.lookupOnKnownRoutersThenOnInitialRouter( routingTable, connectionPool, bookmark, baseError );
        }

        return compositionStage;
    }

    private CompletionStage<ClusterComposition> lookupOnKnownRoutersThenOnInitialRouter( RoutingTable routingTable, ConnectionPool connectionPool,
            Bookmark bookmark, Throwable baseError )
    {
        Set<BoltServerAddress> seenServers = new HashSet();
        return this.lookupOnKnownRouters( routingTable, connectionPool, seenServers, bookmark, baseError ).thenCompose( ( composition ) -> {
            return (CompletionStage) (composition != null ? CompletableFuture.completedFuture( composition )
                                                          : this.lookupOnInitialRouter( routingTable, connectionPool, seenServers, bookmark, baseError ));
        } );
    }

    private CompletionStage<ClusterComposition> lookupOnInitialRouterThenOnKnownRouters( RoutingTable routingTable, ConnectionPool connectionPool,
            Bookmark bookmark, Throwable baseError )
    {
        Set<BoltServerAddress> seenServers = Collections.emptySet();
        return this.lookupOnInitialRouter( routingTable, connectionPool, seenServers, bookmark, baseError ).thenCompose( ( composition ) -> {
            return (CompletionStage) (composition != null ? CompletableFuture.completedFuture( composition )
                                                          : this.lookupOnKnownRouters( routingTable, connectionPool, new HashSet(), bookmark, baseError ));
        } );
    }

    private CompletionStage<ClusterComposition> lookupOnKnownRouters( RoutingTable routingTable, ConnectionPool connectionPool,
            Set<BoltServerAddress> seenServers, Bookmark bookmark, Throwable baseError )
    {
        BoltServerAddress[] addresses = routingTable.routers().toArray();
        CompletableFuture<ClusterComposition> result = Futures.completedWithNull();
        BoltServerAddress[] var8 = addresses;
        int var9 = addresses.length;

        for ( int var10 = 0; var10 < var9; ++var10 )
        {
            BoltServerAddress address = var8[var10];
            result = result.thenCompose( ( composition ) -> {
                return (CompletionStage) (composition != null ? CompletableFuture.completedFuture( composition )
                                                              : this.lookupOnRouter( address, routingTable, connectionPool, bookmark, baseError ).whenComplete(
                                                                      ( ignore, error ) -> {
                                                                          seenServers.add( address );
                                                                      } ));
            } );
        }

        return result;
    }

    private CompletionStage<ClusterComposition> lookupOnInitialRouter( RoutingTable routingTable, ConnectionPool connectionPool,
            Set<BoltServerAddress> seenServers, Bookmark bookmark, Throwable baseError )
    {
        List addresses;
        try
        {
            addresses = this.resolve();
        }
        catch ( Throwable var10 )
        {
            return Futures.failedFuture( var10 );
        }

        addresses.removeAll( seenServers );
        CompletableFuture<ClusterComposition> result = Futures.completedWithNull();

        BoltServerAddress address;
        for ( Iterator var8 = addresses.iterator(); var8.hasNext(); result = result.thenCompose( ( composition ) -> {
            return (CompletionStage) (composition != null ? CompletableFuture.completedFuture( composition )
                                                          : this.lookupOnRouter( address, routingTable, connectionPool, bookmark, baseError ));
        } ) )
        {
            address = (BoltServerAddress) var8.next();
        }

        return result;
    }

    private CompletionStage<ClusterComposition> lookupOnRouter( BoltServerAddress routerAddress, RoutingTable routingTable, ConnectionPool connectionPool,
            Bookmark bookmark, Throwable baseError )
    {
        CompletionStage<Connection> connectionStage = connectionPool.acquire( routerAddress );
        return connectionStage.thenCompose( ( connection ) -> {
            return this.provider.getClusterComposition( connection, routingTable.database(), bookmark );
        } ).handle( ( response, error ) -> {
            Throwable cause = Futures.completionExceptionCause( error );
            return cause != null ? this.handleRoutingProcedureError( cause, routingTable, routerAddress, baseError ) : response;
        } );
    }

    private ClusterComposition handleRoutingProcedureError( Throwable error, RoutingTable routingTable, BoltServerAddress routerAddress, Throwable baseError )
    {
        if ( !(error instanceof SecurityException) && !(error instanceof FatalDiscoveryException) )
        {
            DiscoveryException discoveryError =
                    new DiscoveryException( String.format( "Failed to update routing table with server '%s'.", routerAddress ), error );
            Futures.combineErrors( baseError, discoveryError );
            this.logger.warn(
                    String.format( "Received a recoverable discovery error with server '%s', will continue discovery with other routing servers if available.",
                            routerAddress ), (Throwable) discoveryError );
            routingTable.forget( routerAddress );
            return null;
        }
        else
        {
            throw new CompletionException( error );
        }
    }

    public List<BoltServerAddress> resolve()
    {
        return (List) this.resolver.resolve( this.initialRouter ).stream().flatMap( ( resolved ) -> {
            return this.resolveAll( BoltServerAddress.from( resolved ) );
        } ).collect( Collectors.toList() );
    }

    private Stream<BoltServerAddress> resolveAll( BoltServerAddress address )
    {
        try
        {
            return address.resolveAll().stream();
        }
        catch ( UnknownHostException var3 )
        {
            this.logger.error( "Failed to resolve address `" + address + "` to IPs due to error: " + var3.getMessage(), var3 );
            return Stream.of( address );
        }
    }
}
