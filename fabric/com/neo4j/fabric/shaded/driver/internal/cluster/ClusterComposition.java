package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

public final class ClusterComposition
{
    private static final long MAX_TTL = 9223372036854775L;
    private static final Function<Value,BoltServerAddress> OF_BoltServerAddress = ( value ) -> {
        return new BoltServerAddress( value.asString() );
    };
    private final Set<BoltServerAddress> readers;
    private final Set<BoltServerAddress> writers;
    private final Set<BoltServerAddress> routers;
    private final long expirationTimestamp;

    private ClusterComposition( long expirationTimestamp )
    {
        this.readers = new LinkedHashSet();
        this.writers = new LinkedHashSet();
        this.routers = new LinkedHashSet();
        this.expirationTimestamp = expirationTimestamp;
    }

    public ClusterComposition( long expirationTimestamp, Set<BoltServerAddress> readers, Set<BoltServerAddress> writers, Set<BoltServerAddress> routers )
    {
        this( expirationTimestamp );
        this.readers.addAll( readers );
        this.writers.addAll( writers );
        this.routers.addAll( routers );
    }

    public static ClusterComposition parse( Record record, long now )
    {
        if ( record == null )
        {
            return null;
        }
        else
        {
            final ClusterComposition result = new ClusterComposition( expirationTimestamp( now, record ) );
            record.get( "servers" ).asList( new Function<Value,Void>()
            {
                public Void apply( Value value )
                {
                    result.servers( value.get( "role" ).asString() ).addAll( value.get( "addresses" ).asList( ClusterComposition.OF_BoltServerAddress ) );
                    return null;
                }
            } );
            return result;
        }
    }

    private static long expirationTimestamp( long now, Record record )
    {
        long ttl = record.get( "ttl" ).asLong();
        long expirationTimestamp = now + ttl * 1000L;
        if ( ttl < 0L || ttl >= 9223372036854775L || expirationTimestamp < 0L )
        {
            expirationTimestamp = Long.MAX_VALUE;
        }

        return expirationTimestamp;
    }

    public boolean hasWriters()
    {
        return !this.writers.isEmpty();
    }

    public boolean hasRoutersAndReaders()
    {
        return !this.routers.isEmpty() && !this.readers.isEmpty();
    }

    public Set<BoltServerAddress> readers()
    {
        return new LinkedHashSet( this.readers );
    }

    public Set<BoltServerAddress> writers()
    {
        return new LinkedHashSet( this.writers );
    }

    public Set<BoltServerAddress> routers()
    {
        return new LinkedHashSet( this.routers );
    }

    public long expirationTimestamp()
    {
        return this.expirationTimestamp;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ClusterComposition that = (ClusterComposition) o;
            return this.expirationTimestamp == that.expirationTimestamp && Objects.equals( this.readers, that.readers ) &&
                    Objects.equals( this.writers, that.writers ) && Objects.equals( this.routers, that.routers );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.readers, this.writers, this.routers, this.expirationTimestamp} );
    }

    public String toString()
    {
        return "ClusterComposition{readers=" + this.readers + ", writers=" + this.writers + ", routers=" + this.routers + ", expirationTimestamp=" +
                this.expirationTimestamp + '}';
    }

    private Set<BoltServerAddress> servers( String role )
    {
        byte var3 = -1;
        switch ( role.hashCode() )
        {
        case 2511254:
            if ( role.equals( "READ" ) )
            {
                var3 = 0;
            }
            break;
        case 78166569:
            if ( role.equals( "ROUTE" ) )
            {
                var3 = 2;
            }
            break;
        case 82862015:
            if ( role.equals( "WRITE" ) )
            {
                var3 = 1;
            }
        }

        switch ( var3 )
        {
        case 0:
            return this.readers;
        case 1:
            return this.writers;
        case 2:
            return this.routers;
        default:
            throw new IllegalArgumentException( "invalid server role: " + role );
        }
    }
}
