package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.net.ServerAddress;
import com.neo4j.fabric.shaded.driver.net.ServerAddressResolver;

import java.util.Collections;
import java.util.Set;

public class IdentityResolver implements ServerAddressResolver
{
    public static final IdentityResolver IDENTITY_RESOLVER = new IdentityResolver();

    private IdentityResolver()
    {
    }

    public Set<ServerAddress> resolve( ServerAddress initialRouter )
    {
        return Collections.singleton( initialRouter );
    }
}
