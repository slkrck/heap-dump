package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;

import java.util.List;

public class RoutingProcedureResponse
{
    private final Query procedure;
    private final List<Record> records;
    private final Throwable error;

    public RoutingProcedureResponse( Query procedure, List<Record> records )
    {
        this( procedure, records, (Throwable) null );
    }

    public RoutingProcedureResponse( Query procedure, Throwable error )
    {
        this( procedure, (List) null, error );
    }

    private RoutingProcedureResponse( Query procedure, List<Record> records, Throwable error )
    {
        this.procedure = procedure;
        this.records = records;
        this.error = error;
    }

    public boolean isSuccess()
    {
        return this.records != null;
    }

    public Query procedure()
    {
        return this.procedure;
    }

    public List<Record> records()
    {
        if ( !this.isSuccess() )
        {
            throw new IllegalStateException( "Can't access records of a failed result", this.error );
        }
        else
        {
            return this.records;
        }
    }

    public Throwable error()
    {
        if ( this.isSuccess() )
        {
            throw new IllegalStateException( "Can't access error of a succeeded result " + this.records );
        }
        else
        {
            return this.error;
        }
    }
}
