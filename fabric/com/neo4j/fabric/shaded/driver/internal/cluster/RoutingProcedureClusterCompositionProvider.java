package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.exceptions.ProtocolException;
import com.neo4j.fabric.shaded.driver.exceptions.value.ValueException;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.MultiDatabaseUtil;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

import java.util.List;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

public class RoutingProcedureClusterCompositionProvider implements ClusterCompositionProvider
{
    private static final String PROTOCOL_ERROR_MESSAGE = "Failed to parse '%s' result received from server due to ";
    private final Clock clock;
    private final RoutingProcedureRunner routingProcedureRunner;
    private final RoutingProcedureRunner multiDatabaseRoutingProcedureRunner;

    public RoutingProcedureClusterCompositionProvider( Clock clock, RoutingContext routingContext )
    {
        this( clock, new RoutingProcedureRunner( routingContext ), new MultiDatabasesRoutingProcedureRunner( routingContext ) );
    }

    RoutingProcedureClusterCompositionProvider( Clock clock, RoutingProcedureRunner routingProcedureRunner,
            MultiDatabasesRoutingProcedureRunner multiDatabaseRoutingProcedureRunner )
    {
        this.clock = clock;
        this.routingProcedureRunner = routingProcedureRunner;
        this.multiDatabaseRoutingProcedureRunner = multiDatabaseRoutingProcedureRunner;
    }

    private static String invokedProcedureString( RoutingProcedureResponse response )
    {
        Query query = response.procedure();
        return query.text() + " " + query.parameters();
    }

    public CompletionStage<ClusterComposition> getClusterComposition( Connection connection, DatabaseName databaseName, Bookmark bookmark )
    {
        RoutingProcedureRunner runner;
        if ( MultiDatabaseUtil.supportsMultiDatabase( connection ) )
        {
            runner = this.multiDatabaseRoutingProcedureRunner;
        }
        else
        {
            runner = this.routingProcedureRunner;
        }

        return runner.run( connection, databaseName, bookmark ).thenApply( this::processRoutingResponse );
    }

    private ClusterComposition processRoutingResponse( RoutingProcedureResponse response )
    {
        if ( !response.isSuccess() )
        {
            throw new CompletionException( String.format( "Failed to run '%s' on server. Please make sure that there is a Neo4j server or cluster up running.",
                    invokedProcedureString( response ) ), response.error() );
        }
        else
        {
            List<Record> records = response.records();
            long now = this.clock.millis();
            if ( records.size() != 1 )
            {
                throw new ProtocolException(
                        String.format( "Failed to parse '%s' result received from server due to records received '%s' is too few or too many.",
                                invokedProcedureString( response ), records.size() ) );
            }
            else
            {
                ClusterComposition cluster;
                try
                {
                    cluster = ClusterComposition.parse( (Record) records.get( 0 ), now );
                }
                catch ( ValueException var7 )
                {
                    throw new ProtocolException( String.format( "Failed to parse '%s' result received from server due to unparsable record received.",
                            invokedProcedureString( response ) ), var7 );
                }

                if ( !cluster.hasRoutersAndReaders() )
                {
                    throw new ProtocolException(
                            String.format( "Failed to parse '%s' result received from server due to no router or reader found in response.",
                                    invokedProcedureString( response ) ) );
                }
                else
                {
                    return cluster;
                }
            }
        }
    }
}
