package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;

import java.util.List;
import java.util.concurrent.CompletionStage;

public interface Rediscovery
{
    CompletionStage<ClusterComposition> lookupClusterComposition( RoutingTable var1, ConnectionPool var2, Bookmark var3 );

    List<BoltServerAddress> resolve();
}
