package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.async.ConnectionContext;

import java.util.Set;
import java.util.concurrent.CompletionStage;

public interface RoutingTableRegistry
{
    CompletionStage<RoutingTableHandler> refreshRoutingTable( ConnectionContext var1 );

    Set<BoltServerAddress> allServers();

    void remove( DatabaseName var1 );

    void removeAged();
}
