package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.TransactionConfig;
import com.neo4j.fabric.shaded.driver.Values;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.FatalDiscoveryException;
import com.neo4j.fabric.shaded.driver.internal.BookmarkHolder;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.DatabaseNameUtil;
import com.neo4j.fabric.shaded.driver.internal.async.connection.DirectConnection;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.internal.util.ServerVersion;

import java.util.List;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

public class RoutingProcedureRunner
{
    static final String ROUTING_CONTEXT = "context";
    static final String GET_ROUTING_TABLE = "CALL dbms.cluster.routing.getRoutingTable($context)";
    final RoutingContext context;

    public RoutingProcedureRunner( RoutingContext context )
    {
        this.context = context;
    }

    private static RoutingProcedureResponse processProcedureResponse( Query procedure, List<Record> records, Throwable error )
    {
        Throwable cause = Futures.completionExceptionCause( error );
        return cause != null ? handleError( procedure, cause ) : new RoutingProcedureResponse( procedure, records );
    }

    private static RoutingProcedureResponse handleError( Query procedure, Throwable error )
    {
        if ( error instanceof ClientException )
        {
            return new RoutingProcedureResponse( procedure, error );
        }
        else
        {
            throw new CompletionException( error );
        }
    }

    public CompletionStage<RoutingProcedureResponse> run( Connection connection, DatabaseName databaseName, Bookmark bookmark )
    {
        DirectConnection delegate = this.connection( connection );
        Query procedure = this.procedureQuery( connection.serverVersion(), databaseName );
        BookmarkHolder bookmarkHolder = this.bookmarkHolder( bookmark );
        return this.runProcedure( delegate, procedure, bookmarkHolder ).thenCompose( ( records ) -> {
            return this.releaseConnection( delegate, records );
        } ).handle( ( records, error ) -> {
            return processProcedureResponse( procedure, records, error );
        } );
    }

    DirectConnection connection( Connection connection )
    {
        return new DirectConnection( connection, DatabaseNameUtil.defaultDatabase(), AccessMode.WRITE );
    }

    Query procedureQuery( ServerVersion serverVersion, DatabaseName databaseName )
    {
        if ( databaseName.databaseName().isPresent() )
        {
            throw new FatalDiscoveryException( String.format(
                    "Refreshing routing table for multi-databases is not supported in server version lower than 4.0. Current server version: %s. Database name: '%s'",
                    serverVersion, databaseName.description() ) );
        }
        else
        {
            return new Query( "CALL dbms.cluster.routing.getRoutingTable($context)", Values.parameters( "context", this.context.asMap() ) );
        }
    }

    BookmarkHolder bookmarkHolder( Bookmark ignored )
    {
        return BookmarkHolder.NO_OP;
    }

    CompletionStage<List<Record>> runProcedure( Connection connection, Query procedure, BookmarkHolder bookmarkHolder )
    {
        return connection.protocol().runInAutoCommitTransaction( connection, procedure, bookmarkHolder, TransactionConfig.empty(), true,
                -1L ).asyncResult().thenCompose( ResultCursor::listAsync );
    }

    private CompletionStage<List<Record>> releaseConnection( Connection connection, List<Record> records )
    {
        return connection.release().thenApply( ( ignore ) -> {
            return records;
        } );
    }
}
