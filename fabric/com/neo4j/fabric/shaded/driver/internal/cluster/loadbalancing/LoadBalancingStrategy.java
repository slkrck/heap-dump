package com.neo4j.fabric.shaded.driver.internal.cluster.loadbalancing;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;

public interface LoadBalancingStrategy
{
    BoltServerAddress selectReader( BoltServerAddress[] var1 );

    BoltServerAddress selectWriter( BoltServerAddress[] var1 );
}
