package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Bookmark;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;

import java.util.concurrent.CompletionStage;

public interface ClusterCompositionProvider
{
    CompletionStage<ClusterComposition> getClusterComposition( Connection var1, DatabaseName var2, Bookmark var3 );
}
