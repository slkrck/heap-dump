package com.neo4j.fabric.shaded.driver.internal.cluster;

import java.util.concurrent.TimeUnit;

public class RoutingSettings
{
    public static final long STALE_ROUTING_TABLE_PURGE_DELAY_MS;
    public static final RoutingSettings DEFAULT;

    static
    {
        STALE_ROUTING_TABLE_PURGE_DELAY_MS = TimeUnit.SECONDS.toMillis( 30L );
        DEFAULT = new RoutingSettings( 1, TimeUnit.SECONDS.toMillis( 5L ), STALE_ROUTING_TABLE_PURGE_DELAY_MS );
    }

    private final int maxRoutingFailures;
    private final long retryTimeoutDelay;
    private final RoutingContext routingContext;
    private final long routingTablePurgeDelayMs;

    public RoutingSettings( int maxRoutingFailures, long retryTimeoutDelay, long routingTablePurgeDelayMs )
    {
        this( maxRoutingFailures, retryTimeoutDelay, routingTablePurgeDelayMs, RoutingContext.EMPTY );
    }

    public RoutingSettings( int maxRoutingFailures, long retryTimeoutDelay, long routingTablePurgeDelayMs, RoutingContext routingContext )
    {
        this.maxRoutingFailures = maxRoutingFailures;
        this.retryTimeoutDelay = retryTimeoutDelay;
        this.routingContext = routingContext;
        this.routingTablePurgeDelayMs = routingTablePurgeDelayMs;
    }

    public RoutingSettings withRoutingContext( RoutingContext newRoutingContext )
    {
        return new RoutingSettings( this.maxRoutingFailures, this.retryTimeoutDelay, this.routingTablePurgeDelayMs, newRoutingContext );
    }

    public int maxRoutingFailures()
    {
        return this.maxRoutingFailures;
    }

    public long retryTimeoutDelay()
    {
        return this.retryTimeoutDelay;
    }

    public RoutingContext routingContext()
    {
        return this.routingContext;
    }

    public long routingTablePurgeDelayMs()
    {
        return this.routingTablePurgeDelayMs;
    }
}
