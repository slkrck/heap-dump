package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;
import com.neo4j.fabric.shaded.driver.internal.async.ConnectionContext;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class RoutingTableRegistryImpl implements RoutingTableRegistry
{
    private final ConcurrentMap<DatabaseName,RoutingTableHandler> routingTableHandlers;
    private final RoutingTableRegistryImpl.RoutingTableHandlerFactory factory;
    private final Logger logger;

    public RoutingTableRegistryImpl( ConnectionPool connectionPool, Rediscovery rediscovery, Clock clock, Logger logger, long routingTablePurgeDelayMs )
    {
        this( new ConcurrentHashMap(),
                new RoutingTableRegistryImpl.RoutingTableHandlerFactory( connectionPool, rediscovery, clock, logger, routingTablePurgeDelayMs ), logger );
    }

    RoutingTableRegistryImpl( ConcurrentMap<DatabaseName,RoutingTableHandler> routingTableHandlers, RoutingTableRegistryImpl.RoutingTableHandlerFactory factory,
            Logger logger )
    {
        this.factory = factory;
        this.routingTableHandlers = routingTableHandlers;
        this.logger = logger;
    }

    public CompletionStage<RoutingTableHandler> refreshRoutingTable( ConnectionContext context )
    {
        RoutingTableHandler handler = this.getOrCreate( context.databaseName() );
        return handler.refreshRoutingTable( context ).thenApply( ( ignored ) -> {
            return handler;
        } );
    }

    public Set<BoltServerAddress> allServers()
    {
        Set<BoltServerAddress> servers = new HashSet();
        Iterator var2 = this.routingTableHandlers.values().iterator();

        while ( var2.hasNext() )
        {
            RoutingTableHandler tableHandler = (RoutingTableHandler) var2.next();
            servers.addAll( tableHandler.servers() );
        }

        return servers;
    }

    public void remove( DatabaseName databaseName )
    {
        this.routingTableHandlers.remove( databaseName );
        this.logger.debug( "Routing table handler for database '%s' is removed.", databaseName.description() );
    }

    public void removeAged()
    {
        this.routingTableHandlers.forEach( ( databaseName, handler ) -> {
            if ( handler.isRoutingTableAged() )
            {
                this.logger.info( "Routing table handler for database '%s' is removed because it has not been used for a long time. Routing table: %s",
                        databaseName.description(), handler.routingTable() );
                this.routingTableHandlers.remove( databaseName );
            }
        } );
    }

    public boolean contains( DatabaseName databaseName )
    {
        return this.routingTableHandlers.containsKey( databaseName );
    }

    private RoutingTableHandler getOrCreate( DatabaseName databaseName )
    {
        return (RoutingTableHandler) this.routingTableHandlers.computeIfAbsent( databaseName, ( name ) -> {
            RoutingTableHandler handler = this.factory.newInstance( name, this );
            this.logger.debug( "Routing table handler for database '%s' is added.", databaseName.description() );
            return handler;
        } );
    }

    static class RoutingTableHandlerFactory
    {
        private final ConnectionPool connectionPool;
        private final Rediscovery rediscovery;
        private final Logger log;
        private final Clock clock;
        private final long routingTablePurgeDelayMs;

        RoutingTableHandlerFactory( ConnectionPool connectionPool, Rediscovery rediscovery, Clock clock, Logger log, long routingTablePurgeDelayMs )
        {
            this.connectionPool = connectionPool;
            this.rediscovery = rediscovery;
            this.clock = clock;
            this.log = log;
            this.routingTablePurgeDelayMs = routingTablePurgeDelayMs;
        }

        RoutingTableHandler newInstance( DatabaseName databaseName, RoutingTableRegistry allTables )
        {
            ClusterRoutingTable routingTable = new ClusterRoutingTable( databaseName, this.clock, new BoltServerAddress[0] );
            return new RoutingTableHandler( routingTable, this.rediscovery, this.connectionPool, allTables, this.log, this.routingTablePurgeDelayMs );
        }
    }
}
