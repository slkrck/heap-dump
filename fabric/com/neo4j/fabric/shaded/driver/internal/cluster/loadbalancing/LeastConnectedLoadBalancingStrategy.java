package com.neo4j.fabric.shaded.driver.internal.cluster.loadbalancing;

import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;

public class LeastConnectedLoadBalancingStrategy implements LoadBalancingStrategy
{
    private static final String LOGGER_NAME = LeastConnectedLoadBalancingStrategy.class.getSimpleName();
    private final RoundRobinArrayIndex readersIndex = new RoundRobinArrayIndex();
    private final RoundRobinArrayIndex writersIndex = new RoundRobinArrayIndex();
    private final ConnectionPool connectionPool;
    private final Logger log;

    public LeastConnectedLoadBalancingStrategy( ConnectionPool connectionPool, Logging logging )
    {
        this.connectionPool = connectionPool;
        this.log = logging.getLog( LOGGER_NAME );
    }

    public BoltServerAddress selectReader( BoltServerAddress[] knownReaders )
    {
        return this.select( knownReaders, this.readersIndex, "reader" );
    }

    public BoltServerAddress selectWriter( BoltServerAddress[] knownWriters )
    {
        return this.select( knownWriters, this.writersIndex, "writer" );
    }

    private BoltServerAddress select( BoltServerAddress[] addresses, RoundRobinArrayIndex addressesIndex, String addressType )
    {
        int size = addresses.length;
        if ( size == 0 )
        {
            this.log.trace( "Unable to select %s, no known addresses given", addressType );
            return null;
        }
        else
        {
            int startIndex = addressesIndex.next( size );
            int index = startIndex;
            BoltServerAddress leastConnectedAddress = null;
            int leastActiveConnections = Integer.MAX_VALUE;

            do
            {
                BoltServerAddress address = addresses[index];
                int activeConnections = this.connectionPool.inUseConnections( address );
                if ( activeConnections < leastActiveConnections )
                {
                    leastConnectedAddress = address;
                    leastActiveConnections = activeConnections;
                }

                if ( index == size - 1 )
                {
                    index = 0;
                }
                else
                {
                    ++index;
                }
            }
            while ( index != startIndex );

            this.log.trace( "Selected %s with address: '%s' and active connections: %s", addressType, leastConnectedAddress, leastActiveConnections );
            return leastConnectedAddress;
        }
    }
}
