package com.neo4j.fabric.shaded.driver.internal.cluster.loadbalancing;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.Logger;
import com.neo4j.fabric.shaded.driver.Logging;
import com.neo4j.fabric.shaded.driver.exceptions.SecurityException;
import com.neo4j.fabric.shaded.driver.exceptions.ServiceUnavailableException;
import com.neo4j.fabric.shaded.driver.exceptions.SessionExpiredException;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.async.ConnectionContext;
import com.neo4j.fabric.shaded.driver.internal.async.ImmutableConnectionContext;
import com.neo4j.fabric.shaded.driver.internal.async.connection.RoutingConnection;
import com.neo4j.fabric.shaded.driver.internal.cluster.AddressSet;
import com.neo4j.fabric.shaded.driver.internal.cluster.ClusterCompositionProvider;
import com.neo4j.fabric.shaded.driver.internal.cluster.Rediscovery;
import com.neo4j.fabric.shaded.driver.internal.cluster.RediscoveryImpl;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingProcedureClusterCompositionProvider;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingSettings;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingTable;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingTableRegistry;
import com.neo4j.fabric.shaded.driver.internal.cluster.RoutingTableRegistryImpl;
import com.neo4j.fabric.shaded.driver.internal.messaging.request.MultiDatabaseUtil;
import com.neo4j.fabric.shaded.driver.internal.shaded.io.netty.util.concurrent.EventExecutorGroup;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionPool;
import com.neo4j.fabric.shaded.driver.internal.spi.ConnectionProvider;
import com.neo4j.fabric.shaded.driver.internal.util.Clock;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.net.ServerAddressResolver;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class LoadBalancer implements ConnectionProvider
{
    private static final String LOAD_BALANCER_LOG_NAME = "LoadBalancer";
    private final ConnectionPool connectionPool;
    private final RoutingTableRegistry routingTables;
    private final LoadBalancingStrategy loadBalancingStrategy;
    private final EventExecutorGroup eventExecutorGroup;
    private final Logger log;
    private final Rediscovery rediscovery;

    public LoadBalancer( BoltServerAddress initialRouter, RoutingSettings settings, ConnectionPool connectionPool, EventExecutorGroup eventExecutorGroup,
            Clock clock, Logging logging, LoadBalancingStrategy loadBalancingStrategy, ServerAddressResolver resolver )
    {
        this( connectionPool, createRediscovery( eventExecutorGroup, initialRouter, resolver, settings, clock, logging ), settings, loadBalancingStrategy,
                eventExecutorGroup, clock, loadBalancerLogger( logging ) );
    }

    private LoadBalancer( ConnectionPool connectionPool, Rediscovery rediscovery, RoutingSettings settings, LoadBalancingStrategy loadBalancingStrategy,
            EventExecutorGroup eventExecutorGroup, Clock clock, Logger log )
    {
        this( connectionPool, createRoutingTables( connectionPool, rediscovery, settings, clock, log ), rediscovery, loadBalancingStrategy, eventExecutorGroup,
                log );
    }

    LoadBalancer( ConnectionPool connectionPool, RoutingTableRegistry routingTables, Rediscovery rediscovery, LoadBalancingStrategy loadBalancingStrategy,
            EventExecutorGroup eventExecutorGroup, Logger log )
    {
        this.connectionPool = connectionPool;
        this.routingTables = routingTables;
        this.rediscovery = rediscovery;
        this.loadBalancingStrategy = loadBalancingStrategy;
        this.eventExecutorGroup = eventExecutorGroup;
        this.log = log;
    }

    private static AddressSet addressSet( AccessMode mode, RoutingTable routingTable )
    {
        switch ( mode )
        {
        case READ:
            return routingTable.readers();
        case WRITE:
            return routingTable.writers();
        default:
            throw unknownMode( mode );
        }
    }

    private static RoutingTableRegistry createRoutingTables( ConnectionPool connectionPool, Rediscovery rediscovery, RoutingSettings settings, Clock clock,
            Logger log )
    {
        return new RoutingTableRegistryImpl( connectionPool, rediscovery, clock, log, settings.routingTablePurgeDelayMs() );
    }

    private static Rediscovery createRediscovery( EventExecutorGroup eventExecutorGroup, BoltServerAddress initialRouter, ServerAddressResolver resolver,
            RoutingSettings settings, Clock clock, Logging logging )
    {
        Logger log = loadBalancerLogger( logging );
        ClusterCompositionProvider clusterCompositionProvider = new RoutingProcedureClusterCompositionProvider( clock, settings.routingContext() );
        return new RediscoveryImpl( initialRouter, settings, clusterCompositionProvider, eventExecutorGroup, resolver, log );
    }

    private static Logger loadBalancerLogger( Logging logging )
    {
        return logging.getLog( "LoadBalancer" );
    }

    private static RuntimeException unknownMode( AccessMode mode )
    {
        return new IllegalArgumentException( "Mode '" + mode + "' is not supported" );
    }

    public CompletionStage<Connection> acquireConnection( ConnectionContext context )
    {
        return this.routingTables.refreshRoutingTable( context ).thenCompose( ( handler ) -> {
            return this.acquire( context.mode(), handler.routingTable() ).thenApply( ( connection ) -> {
                return new RoutingConnection( connection, context.databaseName(), context.mode(), handler );
            } );
        } );
    }

    public CompletionStage<Void> verifyConnectivity()
    {
        return this.supportsMultiDb().thenCompose( ( supports ) -> {
            return this.routingTables.refreshRoutingTable( ImmutableConnectionContext.simple( supports ) );
        } ).handle( ( ignored, error ) -> {
            if ( error != null )
            {
                Throwable cause = Futures.completionExceptionCause( error );
                if ( cause instanceof ServiceUnavailableException )
                {
                    throw Futures.asCompletionException( new ServiceUnavailableException(
                            "Unable to connect to database management service, ensure the database is running and that there is a working network connection to it.",
                            cause ) );
                }
                else
                {
                    throw Futures.asCompletionException( cause );
                }
            }
            else
            {
                return null;
            }
        } );
    }

    public CompletionStage<Void> close()
    {
        return this.connectionPool.close();
    }

    public CompletionStage<Boolean> supportsMultiDb()
    {
        List addresses;
        try
        {
            addresses = this.rediscovery.resolve();
        }
        catch ( Throwable var6 )
        {
            return Futures.failedFuture( var6 );
        }

        CompletableFuture<Boolean> result = Futures.completedWithNull();
        Throwable baseError = new ServiceUnavailableException( "Failed to perform multi-databases feature detection with the following servers: " + addresses );

        BoltServerAddress address;
        for ( Iterator var4 = addresses.iterator(); var4.hasNext(); result = Futures.onErrorContinue( result, baseError, ( completionError ) -> {
            Throwable error = Futures.completionExceptionCause( completionError );
            return (CompletionStage) (error instanceof SecurityException ? Futures.failedFuture( error ) : this.supportsMultiDb( address ));
        } ) )
        {
            address = (BoltServerAddress) var4.next();
        }

        return Futures.onErrorContinue( result, baseError, ( completionError ) -> {
            Throwable error = Futures.completionExceptionCause( completionError );
            return error instanceof SecurityException ? Futures.failedFuture( error ) : Futures.failedFuture( baseError );
        } );
    }

    private CompletionStage<Boolean> supportsMultiDb( BoltServerAddress address )
    {
        return this.connectionPool.acquire( address ).thenCompose( ( conn ) -> {
            boolean supportsMultiDatabase = MultiDatabaseUtil.supportsMultiDatabase( conn );
            return conn.release().thenApply( ( ignored ) -> {
                return supportsMultiDatabase;
            } );
        } );
    }

    private CompletionStage<Connection> acquire( AccessMode mode, RoutingTable routingTable )
    {
        AddressSet addresses = addressSet( mode, routingTable );
        CompletableFuture<Connection> result = new CompletableFuture();
        this.acquire( mode, routingTable, addresses, result );
        return result;
    }

    private void acquire( AccessMode mode, RoutingTable routingTable, AddressSet addresses, CompletableFuture<Connection> result )
    {
        BoltServerAddress address = this.selectAddress( mode, addresses );
        if ( address == null )
        {
            result.completeExceptionally(
                    new SessionExpiredException( "Failed to obtain connection towards " + mode + " server. Known routing table is: " + routingTable ) );
        }
        else
        {
            this.connectionPool.acquire( address ).whenComplete( ( connection, completionError ) -> {
                Throwable error = Futures.completionExceptionCause( completionError );
                if ( error != null )
                {
                    if ( error instanceof ServiceUnavailableException )
                    {
                        SessionExpiredException errorToLog =
                                new SessionExpiredException( String.format( "Server at %s is no longer available", address ), error );
                        this.log.warn( "Failed to obtain a connection towards address " + address, (Throwable) errorToLog );
                        routingTable.forget( address );
                        this.eventExecutorGroup.next().execute( () -> {
                            this.acquire( mode, routingTable, addresses, result );
                        } );
                    }
                    else
                    {
                        result.completeExceptionally( error );
                    }
                }
                else
                {
                    result.complete( connection );
                }
            } );
        }
    }

    private BoltServerAddress selectAddress( AccessMode mode, AddressSet servers )
    {
        BoltServerAddress[] addresses = servers.toArray();
        switch ( mode )
        {
        case READ:
            return this.loadBalancingStrategy.selectReader( addresses );
        case WRITE:
            return this.loadBalancingStrategy.selectWriter( addresses );
        default:
            throw unknownMode( mode );
        }
    }
}
