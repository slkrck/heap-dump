package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;

import java.util.Arrays;
import java.util.Set;

public class AddressSet
{
    private static final BoltServerAddress[] NONE = new BoltServerAddress[0];
    private volatile BoltServerAddress[] addresses;

    public AddressSet()
    {
        this.addresses = NONE;
    }

    public BoltServerAddress[] toArray()
    {
        return this.addresses;
    }

    public int size()
    {
        return this.addresses.length;
    }

    public synchronized void update( Set<BoltServerAddress> addresses )
    {
        this.addresses = (BoltServerAddress[]) addresses.toArray( NONE );
    }

    public synchronized void remove( BoltServerAddress address )
    {
        BoltServerAddress[] addresses = this.addresses;
        if ( addresses != null )
        {
            for ( int i = 0; i < addresses.length; ++i )
            {
                if ( addresses[i].equals( address ) )
                {
                    if ( addresses.length == 1 )
                    {
                        this.addresses = NONE;
                        return;
                    }

                    BoltServerAddress[] copy = new BoltServerAddress[addresses.length - 1];
                    System.arraycopy( addresses, 0, copy, 0, i );
                    System.arraycopy( addresses, i + 1, copy, i, addresses.length - i - 1 );
                    this.addresses = copy;
                    return;
                }
            }
        }
    }

    public String toString()
    {
        return "AddressSet=" + Arrays.toString( this.addresses );
    }
}
