package com.neo4j.fabric.shaded.driver.internal.cluster;

import com.neo4j.fabric.shaded.driver.AccessMode;
import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;
import com.neo4j.fabric.shaded.driver.internal.DatabaseName;

import java.util.Set;

public interface RoutingTable
{
    boolean isStaleFor( AccessMode var1 );

    boolean hasBeenStaleFor( long var1 );

    void update( ClusterComposition var1 );

    void forget( BoltServerAddress var1 );

    AddressSet readers();

    AddressSet writers();

    AddressSet routers();

    Set<BoltServerAddress> servers();

    DatabaseName database();

    void forgetWriter( BoltServerAddress var1 );

    boolean preferInitialRouter();
}
