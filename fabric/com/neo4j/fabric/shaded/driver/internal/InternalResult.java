package com.neo4j.fabric.shaded.driver.internal;

import com.neo4j.fabric.shaded.driver.Record;
import com.neo4j.fabric.shaded.driver.Result;
import com.neo4j.fabric.shaded.driver.async.ResultCursor;
import com.neo4j.fabric.shaded.driver.exceptions.ClientException;
import com.neo4j.fabric.shaded.driver.exceptions.NoSuchRecordException;
import com.neo4j.fabric.shaded.driver.internal.spi.Connection;
import com.neo4j.fabric.shaded.driver.internal.util.Futures;
import com.neo4j.fabric.shaded.driver.summary.ResultSummary;

import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class InternalResult implements Result
{
    private final Connection connection;
    private final ResultCursor cursor;
    private List<String> keys;

    public InternalResult( Connection connection, ResultCursor cursor )
    {
        this.connection = connection;
        this.cursor = cursor;
    }

    public List<String> keys()
    {
        if ( this.keys == null )
        {
            this.blockingGet( this.cursor.peekAsync() );
            this.keys = this.cursor.keys();
        }

        return this.keys;
    }

    public boolean hasNext()
    {
        return this.blockingGet( this.cursor.peekAsync() ) != null;
    }

    public Record next()
    {
        Record record = (Record) this.blockingGet( this.cursor.nextAsync() );
        if ( record == null )
        {
            throw new NoSuchRecordException( "No more records" );
        }
        else
        {
            return record;
        }
    }

    public Record single()
    {
        return (Record) this.blockingGet( this.cursor.singleAsync() );
    }

    public Record peek()
    {
        Record record = (Record) this.blockingGet( this.cursor.peekAsync() );
        if ( record == null )
        {
            throw new NoSuchRecordException( "Cannot peek past the last record" );
        }
        else
        {
            return record;
        }
    }

    public Stream<Record> stream()
    {
        Spliterator<Record> spliterator = Spliterators.spliteratorUnknownSize( this, 1040 );
        return StreamSupport.stream( spliterator, false );
    }

    public List<Record> list()
    {
        return (List) this.blockingGet( this.cursor.listAsync() );
    }

    public <T> List<T> list( Function<Record,T> mapFunction )
    {
        return (List) this.blockingGet( this.cursor.listAsync( mapFunction ) );
    }

    public ResultSummary consume()
    {
        return (ResultSummary) this.blockingGet( this.cursor.consumeAsync() );
    }

    public void remove()
    {
        throw new ClientException( "Removing records from a result is not supported." );
    }

    private <T> T blockingGet( CompletionStage<T> stage )
    {
        return Futures.blockingGet( stage, this::terminateConnectionOnThreadInterrupt );
    }

    private void terminateConnectionOnThreadInterrupt()
    {
        this.connection.terminateAndRelease( "Thread interrupted while waiting for result to arrive" );
    }
}
