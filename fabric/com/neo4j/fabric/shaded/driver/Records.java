package com.neo4j.fabric.shaded.driver;

import java.util.function.Function;

public abstract class Records
{
    public static Function<Record,Value> column( int index )
    {
        return column( index, Values.ofValue() );
    }

    public static Function<Record,Value> column( String key )
    {
        return column( key, Values.ofValue() );
    }

    public static <T> Function<Record,T> column( final int index, final Function<Value,T> mapFunction )
    {
        return new Function<Record,T>()
        {
            public T apply( Record record )
            {
                return mapFunction.apply( record.get( index ) );
            }
        };
    }

    public static <T> Function<Record,T> column( final String key, final Function<Value,T> mapFunction )
    {
        return new Function<Record,T>()
        {
            public T apply( Record recordAccessor )
            {
                return mapFunction.apply( recordAccessor.get( key ) );
            }
        };
    }
}
