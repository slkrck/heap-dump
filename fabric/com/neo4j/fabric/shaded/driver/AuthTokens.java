package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.internal.security.InternalAuthToken;
import com.neo4j.fabric.shaded.driver.internal.util.Iterables;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class AuthTokens
{
    public static AuthToken basic( String username, String password )
    {
        return basic( username, password, (String) null );
    }

    public static AuthToken basic( String username, String password, String realm )
    {
        Objects.requireNonNull( username, "Username can't be null" );
        Objects.requireNonNull( password, "Password can't be null" );
        Map<String,Value> map = Iterables.newHashMapWithSize( 4 );
        map.put( "scheme", Values.value( "basic" ) );
        map.put( "principal", Values.value( username ) );
        map.put( "credentials", Values.value( password ) );
        if ( realm != null )
        {
            map.put( "realm", Values.value( realm ) );
        }

        return new InternalAuthToken( map );
    }

    public static AuthToken kerberos( String base64EncodedTicket )
    {
        Objects.requireNonNull( base64EncodedTicket, "Ticket can't be null" );
        Map<String,Value> map = Iterables.newHashMapWithSize( 3 );
        map.put( "scheme", Values.value( "kerberos" ) );
        map.put( "principal", Values.value( "" ) );
        map.put( "credentials", Values.value( base64EncodedTicket ) );
        return new InternalAuthToken( map );
    }

    public static AuthToken custom( String principal, String credentials, String realm, String scheme )
    {
        return custom( principal, credentials, realm, scheme, (Map) null );
    }

    public static AuthToken custom( String principal, String credentials, String realm, String scheme, Map<String,Object> parameters )
    {
        Objects.requireNonNull( principal, "Principal can't be null" );
        Objects.requireNonNull( credentials, "Credentials can't be null" );
        Objects.requireNonNull( scheme, "Scheme can't be null" );
        Map<String,Value> map = Iterables.newHashMapWithSize( 5 );
        map.put( "scheme", Values.value( scheme ) );
        map.put( "principal", Values.value( principal ) );
        map.put( "credentials", Values.value( credentials ) );
        if ( realm != null )
        {
            map.put( "realm", Values.value( realm ) );
        }

        if ( parameters != null )
        {
            map.put( "parameters", Values.value( parameters ) );
        }

        return new InternalAuthToken( map );
    }

    public static AuthToken none()
    {
        return new InternalAuthToken( Collections.singletonMap( "scheme", Values.value( "none" ) ) );
    }
}
