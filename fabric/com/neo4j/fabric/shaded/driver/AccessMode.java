package com.neo4j.fabric.shaded.driver;

public enum AccessMode
{
    READ,
    WRITE;
}
