package com.neo4j.fabric.shaded.driver;

import java.util.Map;

public interface QueryRunner
{
    Result run( String var1, Value var2 );

    Result run( String var1, Map<String,Object> var2 );

    Result run( String var1, Record var2 );

    Result run( String var1 );

    Result run( Query var1 );
}
