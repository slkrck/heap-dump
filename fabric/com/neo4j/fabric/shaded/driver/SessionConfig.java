package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.internal.handlers.pulln.FetchSizeUtil;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class SessionConfig
{
    private static final SessionConfig EMPTY = builder().build();
    private final Iterable<Bookmark> bookmarks;
    private final AccessMode defaultAccessMode;
    private final String database;
    private final Optional<Long> fetchSize;

    private SessionConfig( SessionConfig.Builder builder )
    {
        this.bookmarks = builder.bookmarks;
        this.defaultAccessMode = builder.defaultAccessMode;
        this.database = builder.database;
        this.fetchSize = builder.fetchSize;
    }

    public static SessionConfig.Builder builder()
    {
        return new SessionConfig.Builder();
    }

    public static SessionConfig defaultConfig()
    {
        return EMPTY;
    }

    public static SessionConfig forDatabase( String database )
    {
        return (new SessionConfig.Builder()).withDatabase( database ).build();
    }

    public Iterable<Bookmark> bookmarks()
    {
        return this.bookmarks;
    }

    public AccessMode defaultAccessMode()
    {
        return this.defaultAccessMode;
    }

    public Optional<String> database()
    {
        return Optional.ofNullable( this.database );
    }

    public Optional<Long> fetchSize()
    {
        return this.fetchSize;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            SessionConfig that = (SessionConfig) o;
            return Objects.equals( this.bookmarks, that.bookmarks ) && this.defaultAccessMode == that.defaultAccessMode &&
                    Objects.equals( this.database, that.database ) && Objects.equals( this.fetchSize, that.fetchSize );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.bookmarks, this.defaultAccessMode, this.database} );
    }

    public String toString()
    {
        return "SessionParameters{bookmarks=" + this.bookmarks + ", defaultAccessMode=" + this.defaultAccessMode + ", database='" + this.database + '\'' +
                ", fetchSize=" + this.fetchSize + '}';
    }

    public static class Builder
    {
        private Optional<Long> fetchSize;
        private Iterable<Bookmark> bookmarks;
        private AccessMode defaultAccessMode;
        private String database;

        private Builder()
        {
            this.fetchSize = Optional.empty();
            this.bookmarks = null;
            this.defaultAccessMode = AccessMode.WRITE;
            this.database = null;
        }

        public SessionConfig.Builder withBookmarks( Bookmark... bookmarks )
        {
            if ( bookmarks == null )
            {
                this.bookmarks = null;
            }
            else
            {
                this.bookmarks = Arrays.asList( bookmarks );
            }

            return this;
        }

        public SessionConfig.Builder withBookmarks( Iterable<Bookmark> bookmarks )
        {
            this.bookmarks = bookmarks;
            return this;
        }

        public SessionConfig.Builder withDefaultAccessMode( AccessMode mode )
        {
            this.defaultAccessMode = mode;
            return this;
        }

        public SessionConfig.Builder withDatabase( String database )
        {
            Objects.requireNonNull( database, "Database name should not be null." );
            if ( database.isEmpty() )
            {
                throw new IllegalArgumentException( String.format( "Illegal database name '%s'.", database ) );
            }
            else
            {
                this.database = database;
                return this;
            }
        }

        public SessionConfig.Builder withFetchSize( long size )
        {
            this.fetchSize = Optional.of( FetchSizeUtil.assertValidFetchSize( size ) );
            return this;
        }

        public SessionConfig build()
        {
            return new SessionConfig( this );
        }
    }
}
