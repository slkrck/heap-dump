package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.util.Experimental;

import java.util.Collection;

@Experimental
public interface Metrics
{
    Collection<ConnectionPoolMetrics> connectionPoolMetrics();
}
