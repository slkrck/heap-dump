package com.neo4j.fabric.shaded.driver.net;

import com.neo4j.fabric.shaded.driver.internal.BoltServerAddress;

public interface ServerAddress
{
    static ServerAddress of( String host, int port )
    {
        return new BoltServerAddress( host, port );
    }

    String host();

    int port();
}
