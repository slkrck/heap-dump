package com.neo4j.fabric.shaded.driver.net;

import java.util.Set;

@FunctionalInterface
public interface ServerAddressResolver
{
    Set<ServerAddress> resolve( ServerAddress var1 );
}
