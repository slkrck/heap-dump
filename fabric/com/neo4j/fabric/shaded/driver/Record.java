package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.types.MapAccessorWithDefaultValue;
import com.neo4j.fabric.shaded.driver.util.Immutable;
import com.neo4j.fabric.shaded.driver.util.Pair;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Immutable
public interface Record extends MapAccessorWithDefaultValue
{
    List<String> keys();

    List<Value> values();

    boolean containsKey( String var1 );

    int index( String var1 );

    Value get( String var1 );

    Value get( int var1 );

    int size();

    Map<String,Object> asMap();

    <T> Map<String,T> asMap( Function<Value,T> var1 );

    List<Pair<String,Value>> fields();
}
