package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.internal.logging.ConsoleLogging;
import com.neo4j.fabric.shaded.driver.internal.logging.DevNullLogging;
import com.neo4j.fabric.shaded.driver.internal.logging.JULogging;
import com.neo4j.fabric.shaded.driver.internal.logging.Slf4jLogging;

import java.util.logging.Level;

public interface Logging
{
    static Logging slf4j()
    {
        RuntimeException unavailabilityError = Slf4jLogging.checkAvailability();
        if ( unavailabilityError != null )
        {
            throw unavailabilityError;
        }
        else
        {
            return new Slf4jLogging();
        }
    }

    static Logging javaUtilLogging( Level level )
    {
        return new JULogging( level );
    }

    static Logging console( Level level )
    {
        return new ConsoleLogging( level );
    }

    static Logging none()
    {
        return DevNullLogging.DEV_NULL_LOGGING;
    }

    Logger getLog( String var1 );
}
