package com.neo4j.fabric.shaded.driver.summary;

import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
public interface Notification
{
    String code();

    String title();

    String description();

    InputPosition position();

    String severity();
}
