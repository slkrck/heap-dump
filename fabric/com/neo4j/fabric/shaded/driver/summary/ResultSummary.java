package com.neo4j.fabric.shaded.driver.summary;

import com.neo4j.fabric.shaded.driver.Query;
import com.neo4j.fabric.shaded.driver.util.Immutable;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Immutable
public interface ResultSummary
{
    Query query();

    SummaryCounters counters();

    QueryType queryType();

    boolean hasPlan();

    boolean hasProfile();

    Plan plan();

    ProfiledPlan profile();

    List<Notification> notifications();

    long resultAvailableAfter( TimeUnit var1 );

    long resultConsumedAfter( TimeUnit var1 );

    ServerInfo server();

    DatabaseInfo database();
}
