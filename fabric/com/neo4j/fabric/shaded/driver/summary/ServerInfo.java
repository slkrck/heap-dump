package com.neo4j.fabric.shaded.driver.summary;

public interface ServerInfo
{
    String address();

    String version();
}
