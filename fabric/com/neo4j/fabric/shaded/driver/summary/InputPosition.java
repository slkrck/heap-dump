package com.neo4j.fabric.shaded.driver.summary;

import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
public interface InputPosition
{
    int offset();

    int line();

    int column();
}
