package com.neo4j.fabric.shaded.driver.summary;

import com.neo4j.fabric.shaded.driver.Value;
import com.neo4j.fabric.shaded.driver.util.Immutable;

import java.util.List;
import java.util.Map;

@Immutable
public interface Plan
{
    String operatorType();

    Map<String,Value> arguments();

    List<String> identifiers();

    List<? extends Plan> children();
}
