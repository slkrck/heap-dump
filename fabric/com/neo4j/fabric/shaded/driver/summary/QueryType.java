package com.neo4j.fabric.shaded.driver.summary;

import com.neo4j.fabric.shaded.driver.exceptions.ClientException;

public enum QueryType
{
    READ_ONLY,
    READ_WRITE,
    WRITE_ONLY,
    SCHEMA_WRITE;

    public static QueryType fromCode( String type )
    {
        byte var2 = -1;
        switch ( type.hashCode() )
        {
        case 114:
            if ( type.equals( "r" ) )
            {
                var2 = 0;
            }
            break;
        case 115:
            if ( type.equals( "s" ) )
            {
                var2 = 3;
            }
            break;
        case 119:
            if ( type.equals( "w" ) )
            {
                var2 = 2;
            }
            break;
        case 3653:
            if ( type.equals( "rw" ) )
            {
                var2 = 1;
            }
        }

        switch ( var2 )
        {
        case 0:
            return READ_ONLY;
        case 1:
            return READ_WRITE;
        case 2:
            return WRITE_ONLY;
        case 3:
            return SCHEMA_WRITE;
        default:
            throw new ClientException( "Unknown query type: `" + type + "`." );
        }
    }
}
