package com.neo4j.fabric.shaded.driver.summary;

public interface DatabaseInfo
{
    String name();
}
