package com.neo4j.fabric.shaded.driver.summary;

import java.util.List;

public interface ProfiledPlan extends Plan
{
    long dbHits();

    long records();

    boolean hasPageCacheStats();

    long pageCacheHits();

    long pageCacheMisses();

    double pageCacheHitRatio();

    long time();

    List<ProfiledPlan> children();
}
