package com.neo4j.fabric.shaded.driver.summary;

import com.neo4j.fabric.shaded.driver.util.Immutable;

@Immutable
public interface SummaryCounters
{
    boolean containsUpdates();

    int nodesCreated();

    int nodesDeleted();

    int relationshipsCreated();

    int relationshipsDeleted();

    int propertiesSet();

    int labelsAdded();

    int labelsRemoved();

    int indexesAdded();

    int indexesRemoved();

    int constraintsAdded();

    int constraintsRemoved();

    boolean containsSystemUpdates();

    int systemUpdates();
}
