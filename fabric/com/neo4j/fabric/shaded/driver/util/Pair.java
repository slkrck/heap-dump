package com.neo4j.fabric.shaded.driver.util;

@Immutable
public interface Pair<K, V>
{
    K key();

    V value();
}
