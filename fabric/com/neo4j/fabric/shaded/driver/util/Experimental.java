package com.neo4j.fabric.shaded.driver.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Retention( RetentionPolicy.RUNTIME )
@Documented
@Target( {ElementType.TYPE, ElementType.METHOD} )
public @interface Experimental
{
}
