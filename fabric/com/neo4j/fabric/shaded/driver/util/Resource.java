package com.neo4j.fabric.shaded.driver.util;

public interface Resource extends AutoCloseable
{
    boolean isOpen();

    void close();
}
