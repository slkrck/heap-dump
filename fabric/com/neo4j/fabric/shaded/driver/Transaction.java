package com.neo4j.fabric.shaded.driver;

import com.neo4j.fabric.shaded.driver.util.Resource;

public interface Transaction extends Resource, QueryRunner
{
    void commit();

    void rollback();

    void close();
}
