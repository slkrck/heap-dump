package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;

public final class FabricPlan$ implements Serializable
{
    public static FabricPlan$ MODULE$;

    static
    {
        new FabricPlan$();
    }

    private final FabricPlan.ExecutionType EXECUTE;
    private final FabricPlan.ExecutionType EXPLAIN;
    private final FabricPlan.ExecutionType PROFILE;

    private FabricPlan$()
    {
        MODULE$ = this;
        this.EXECUTE = FabricPlan$Execute$.MODULE$;
        this.EXPLAIN = FabricPlan$Explain$.MODULE$;
        this.PROFILE = FabricPlan$Profile$.MODULE$;
    }

    public FabricPlan.ExecutionType EXECUTE()
    {
        return this.EXECUTE;
    }

    public FabricPlan.ExecutionType EXPLAIN()
    {
        return this.EXPLAIN;
    }

    public FabricPlan.ExecutionType PROFILE()
    {
        return this.PROFILE;
    }

    public FabricPlan apply( final FabricQuery query, final QueryType queryType, final FabricPlan.ExecutionType executionType,
            final FabricPlan.DebugOptions debugOptions )
    {
        return new FabricPlan( query, queryType, executionType, debugOptions );
    }

    public Option<Tuple4<FabricQuery,QueryType,FabricPlan.ExecutionType,FabricPlan.DebugOptions>> unapply( final FabricPlan x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.query(), x$0.queryType(), x$0.executionType(), x$0.debugOptions() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
