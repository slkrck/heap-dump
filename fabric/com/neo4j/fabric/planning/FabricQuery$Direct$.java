package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public class FabricQuery$Direct$ extends AbstractFunction2<FabricQuery,FabricQuery.Columns,FabricQuery.Direct> implements Serializable
{
    public static FabricQuery$Direct$ MODULE$;

    static
    {
        new FabricQuery$Direct$();
    }

    public FabricQuery$Direct$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Direct";
    }

    public FabricQuery.Direct apply( final FabricQuery query, final FabricQuery.Columns columns )
    {
        return new FabricQuery.Direct( query, columns );
    }

    public Option<Tuple2<FabricQuery,FabricQuery.Columns>> unapply( final FabricQuery.Direct x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.query(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
