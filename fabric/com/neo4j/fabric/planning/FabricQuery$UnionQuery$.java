package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public class FabricQuery$UnionQuery$ extends AbstractFunction4<FabricQuery,FabricQuery,Object,FabricQuery.Columns,FabricQuery.UnionQuery>
        implements Serializable
{
    public static FabricQuery$UnionQuery$ MODULE$;

    static
    {
        new FabricQuery$UnionQuery$();
    }

    public FabricQuery$UnionQuery$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "UnionQuery";
    }

    public FabricQuery.UnionQuery apply( final FabricQuery lhs, final FabricQuery rhs, final boolean distinct, final FabricQuery.Columns columns )
    {
        return new FabricQuery.UnionQuery( lhs, rhs, distinct, columns );
    }

    public Option<Tuple4<FabricQuery,FabricQuery,Object,FabricQuery.Columns>> unapply( final FabricQuery.UnionQuery x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.lhs(), x$0.rhs(), BoxesRunTime.boxToBoolean( x$0.distinct() ), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
