package com.neo4j.fabric.planning;

import com.neo4j.fabric.util.PrettyPrinting;
import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public interface Fragment
{
    static PrettyPrinting<Fragment> pretty()
    {
        return Fragment$.MODULE$.pretty();
    }

    FabricQuery.Columns columns();

    public static class Apply implements Fragment, Product, Serializable
    {
        private final Fragment fragment;
        private final FabricQuery.Columns columns;

        public Apply( final Fragment fragment, final FabricQuery.Columns columns )
        {
            this.fragment = fragment;
            this.columns = columns;
            Product.$init$( this );
        }

        public Fragment fragment()
        {
            return this.fragment;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Fragment.Apply copy( final Fragment fragment, final FabricQuery.Columns columns )
        {
            return new Fragment.Apply( fragment, columns );
        }

        public Fragment copy$default$1()
        {
            return this.fragment();
        }

        public FabricQuery.Columns copy$default$2()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "Apply";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.fragment();
                break;
            case 1:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Fragment.Apply;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof Fragment.Apply )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                Fragment.Apply var4 = (Fragment.Apply) x$1;
                                Fragment var10000 = this.fragment();
                                Fragment var5 = var4.fragment();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                FabricQuery.Columns var7 = this.columns();
                                FabricQuery.Columns var6 = var4.columns();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class Chain implements Fragment, Product, Serializable
    {
        private final Seq<Fragment> fragments;
        private final FabricQuery.Columns columns;

        public Chain( final Seq<Fragment> fragments, final FabricQuery.Columns columns )
        {
            this.fragments = fragments;
            this.columns = columns;
            Product.$init$( this );
        }

        public Seq<Fragment> fragments()
        {
            return this.fragments;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Fragment.Chain copy( final Seq<Fragment> fragments, final FabricQuery.Columns columns )
        {
            return new Fragment.Chain( fragments, columns );
        }

        public Seq<Fragment> copy$default$1()
        {
            return this.fragments();
        }

        public FabricQuery.Columns copy$default$2()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "Chain";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.fragments();
                break;
            case 1:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Fragment.Chain;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof Fragment.Chain )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                Fragment.Chain var4 = (Fragment.Chain) x$1;
                                Seq var10000 = this.fragments();
                                Seq var5 = var4.fragments();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                FabricQuery.Columns var7 = this.columns();
                                FabricQuery.Columns var6 = var4.columns();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class Direct implements Fragment, Product, Serializable
    {
        private final Fragment fragment;
        private final FabricQuery.Columns columns;

        public Direct( final Fragment fragment, final FabricQuery.Columns columns )
        {
            this.fragment = fragment;
            this.columns = columns;
            Product.$init$( this );
        }

        public Fragment fragment()
        {
            return this.fragment;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Fragment.Direct copy( final Fragment fragment, final FabricQuery.Columns columns )
        {
            return new Fragment.Direct( fragment, columns );
        }

        public Fragment copy$default$1()
        {
            return this.fragment();
        }

        public FabricQuery.Columns copy$default$2()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "Direct";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.fragment();
                break;
            case 1:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Fragment.Direct;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof Fragment.Direct )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                Fragment.Direct var4 = (Fragment.Direct) x$1;
                                Fragment var10000 = this.fragment();
                                Fragment var5 = var4.fragment();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                FabricQuery.Columns var7 = this.columns();
                                FabricQuery.Columns var6 = var4.columns();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class Leaf implements Fragment, Product, Serializable
    {
        private final Option<UseGraph> use;
        private final Seq<Clause> clauses;
        private final FabricQuery.Columns columns;

        public Leaf( final Option<UseGraph> use, final Seq<Clause> clauses, final FabricQuery.Columns columns )
        {
            this.use = use;
            this.clauses = clauses;
            this.columns = columns;
            Product.$init$( this );
        }

        public Option<UseGraph> use()
        {
            return this.use;
        }

        public Seq<Clause> clauses()
        {
            return this.clauses;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Fragment.Leaf copy( final Option<UseGraph> use, final Seq<Clause> clauses, final FabricQuery.Columns columns )
        {
            return new Fragment.Leaf( use, clauses, columns );
        }

        public Option<UseGraph> copy$default$1()
        {
            return this.use();
        }

        public Seq<Clause> copy$default$2()
        {
            return this.clauses();
        }

        public FabricQuery.Columns copy$default$3()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "Leaf";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.use();
                break;
            case 1:
                var10000 = this.clauses();
                break;
            case 2:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Fragment.Leaf;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof Fragment.Leaf )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                Fragment.Leaf var4 = (Fragment.Leaf) x$1;
                                Option var10000 = this.use();
                                Option var5 = var4.use();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                Seq var8 = this.clauses();
                                Seq var6 = var4.clauses();
                                if ( var8 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var6 ) )
                                {
                                    break label63;
                                }

                                FabricQuery.Columns var9 = this.columns();
                                FabricQuery.Columns var7 = var4.columns();
                                if ( var9 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var9.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var10 = true;
                                    break label54;
                                }
                            }

                            var10 = false;
                        }

                        if ( var10 )
                        {
                            break label72;
                        }
                    }

                    var10 = false;
                    return var10;
                }
            }

            var10 = true;
            return var10;
        }
    }

    public static class Union implements Fragment, Product, Serializable
    {
        private final boolean distinct;
        private final Fragment lhs;
        private final Fragment rhs;
        private final FabricQuery.Columns columns;

        public Union( final boolean distinct, final Fragment lhs, final Fragment rhs, final FabricQuery.Columns columns )
        {
            this.distinct = distinct;
            this.lhs = lhs;
            this.rhs = rhs;
            this.columns = columns;
            Product.$init$( this );
        }

        public boolean distinct()
        {
            return this.distinct;
        }

        public Fragment lhs()
        {
            return this.lhs;
        }

        public Fragment rhs()
        {
            return this.rhs;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Fragment.Union copy( final boolean distinct, final Fragment lhs, final Fragment rhs, final FabricQuery.Columns columns )
        {
            return new Fragment.Union( distinct, lhs, rhs, columns );
        }

        public boolean copy$default$1()
        {
            return this.distinct();
        }

        public Fragment copy$default$2()
        {
            return this.lhs();
        }

        public Fragment copy$default$3()
        {
            return this.rhs();
        }

        public FabricQuery.Columns copy$default$4()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "Union";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = BoxesRunTime.boxToBoolean( this.distinct() );
                break;
            case 1:
                var10000 = this.lhs();
                break;
            case 2:
                var10000 = this.rhs();
                break;
            case 3:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Fragment.Union;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, this.distinct() ? 1231 : 1237 );
            var1 = Statics.mix( var1, Statics.anyHash( this.lhs() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.rhs() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.columns() ) );
            return Statics.finalizeHash( var1, 4 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label74:
                {
                    boolean var2;
                    if ( x$1 instanceof Fragment.Union )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label56:
                        {
                            Fragment.Union var4 = (Fragment.Union) x$1;
                            if ( this.distinct() == var4.distinct() )
                            {
                                label65:
                                {
                                    Fragment var10000 = this.lhs();
                                    Fragment var5 = var4.lhs();
                                    if ( var10000 == null )
                                    {
                                        if ( var5 != null )
                                        {
                                            break label65;
                                        }
                                    }
                                    else if ( !var10000.equals( var5 ) )
                                    {
                                        break label65;
                                    }

                                    var10000 = this.rhs();
                                    Fragment var6 = var4.rhs();
                                    if ( var10000 == null )
                                    {
                                        if ( var6 != null )
                                        {
                                            break label65;
                                        }
                                    }
                                    else if ( !var10000.equals( var6 ) )
                                    {
                                        break label65;
                                    }

                                    FabricQuery.Columns var8 = this.columns();
                                    FabricQuery.Columns var7 = var4.columns();
                                    if ( var8 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label65;
                                        }
                                    }
                                    else if ( !var8.equals( var7 ) )
                                    {
                                        break label65;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var9 = true;
                                        break label56;
                                    }
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label74;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }
}
