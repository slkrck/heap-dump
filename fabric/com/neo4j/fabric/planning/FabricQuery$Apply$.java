package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public class FabricQuery$Apply$ extends AbstractFunction2<FabricQuery,FabricQuery.Columns,FabricQuery.Apply> implements Serializable
{
    public static FabricQuery$Apply$ MODULE$;

    static
    {
        new FabricQuery$Apply$();
    }

    public FabricQuery$Apply$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Apply";
    }

    public FabricQuery.Apply apply( final FabricQuery query, final FabricQuery.Columns columns )
    {
        return new FabricQuery.Apply( query, columns );
    }

    public Option<Tuple2<FabricQuery,FabricQuery.Columns>> unapply( final FabricQuery.Apply x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.query(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
