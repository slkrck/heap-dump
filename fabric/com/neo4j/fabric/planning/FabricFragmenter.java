package com.neo4j.fabric.planning;

import com.neo4j.fabric.util.Errors$;
import org.neo4j.cypher.internal.v4_0.ast.CatalogDDL;
import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.Command;
import org.neo4j.cypher.internal.v4_0.ast.Query;
import org.neo4j.cypher.internal.v4_0.ast.QueryPart;
import org.neo4j.cypher.internal.v4_0.ast.Return;
import org.neo4j.cypher.internal.v4_0.ast.SingleQuery;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.ast.SubQuery;
import org.neo4j.cypher.internal.v4_0.ast.Union;
import org.neo4j.cypher.internal.v4_0.ast.UnionAll;
import org.neo4j.cypher.internal.v4_0.ast.UnionDistinct;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import org.neo4j.cypher.internal.v4_0.ast.semantics.Scope;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.util.Either;
import scala.util.Left;
import scala.util.Right;

@JavaDocToJava
public class FabricFragmenter
{
    private final String queryString;
    private final Statement queryStatement;
    private final SemanticState semantics;
    private volatile FabricFragmenter$State$ State$module;

    public FabricFragmenter( final String queryString, final Statement queryStatement, final SemanticState semantics )
    {
        this.queryString = queryString;
        this.queryStatement = queryStatement;
        this.semantics = semantics;
    }

    private FabricFragmenter$State$ State()
    {
        if ( this.State$module == null )
        {
            this.State$lzycompute$1();
        }

        return this.State$module;
    }

    public Fragment fragment()
    {
        Statement var2 = this.queryStatement;
        if ( var2 instanceof Query )
        {
            Query var3 = (Query) var2;
            QueryPart part = var3.part();
            Fragment var1 = this.fragment( part, (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Option..
            MODULE$.empty());
            return var1;
        }
        else if ( var2 instanceof CatalogDDL )
        {
            CatalogDDL var5 = (CatalogDDL) var2;
            throw Errors$.MODULE$.ddlNotSupported( var5 );
        }
        else if ( var2 instanceof Command )
        {
            throw Errors$.MODULE$.notSupported( "Commands" );
        }
        else
        {
            throw new MatchError( var2 );
        }
    }

    private Fragment fragment( final QueryPart part, final Seq<String> incoming, final Seq<String> local, final Option<UseGraph> use )
    {
        Object var5;
        if ( part instanceof Union )
        {
            Union var9 = (Union) part;
            Fragment lhs = this.fragment( var9.part(), incoming, local, use );
            Fragment rhs = this.fragment( var9.query(), incoming, local, use );
            boolean var7;
            if ( var9 instanceof UnionAll )
            {
                var7 = false;
            }
            else
            {
                if ( !(var9 instanceof UnionDistinct) )
                {
                    throw new MatchError( var9 );
                }

                var7 = true;
            }

            var5 = new Fragment.Union( var7, lhs, rhs, rhs.columns() );
        }
        else
        {
            if ( !(part instanceof SingleQuery) )
            {
                throw new MatchError( part );
            }

            SingleQuery var14 = (SingleQuery) part;
            Seq imports = var14.importColumns();
            Option localUse = this.leadingUse( var14 ).orElse( () -> {
                return use;
            } );
            Seq parts = this.partitioned( var14.clauses() );
            FabricFragmenter.State initial = new FabricFragmenter.State( this, incoming, local, imports, this.State().apply$default$4() );
            FabricFragmenter.State result = (FabricFragmenter.State) parts.foldLeft( initial, ( x0$2, x1$1 ) -> {
                Tuple2 var5 = new Tuple2( x0$2, x1$1 );
                FabricFragmenter.State var4;
                if ( var5 != null )
                {
                    FabricFragmenter.State state = (FabricFragmenter.State) var5._1();
                    Either var7 = (Either) var5._2();
                    if ( var7 instanceof Right )
                    {
                        Right var8 = (Right) var7;
                        Seq clauses = (Seq) var8.value();
                        Seq var10003 = (Seq) clauses.filterNot( ( x$1 ) -> {
                            return BoxesRunTime.boxToBoolean( $anonfun$fragment$3( x$1 ) );
                        } );
                        FabricQuery.Columns qual$1 = state.columns();
                        Seq x$5 = this.produced( (Clause) clauses.last() );
                        Seq x$6 = qual$1.copy$default$1();
                        Seq x$7 = qual$1.copy$default$2();
                        Seq x$8 = qual$1.copy$default$3();
                        Fragment.Leaf leaf = new Fragment.Leaf( localUse, var10003, qual$1.copy( x$6, x$7, x$8, x$5 ) );
                        var4 = state.append( new Fragment.Direct( leaf, leaf.columns() ) );
                        return var4;
                    }
                }

                if ( var5 == null )
                {
                    throw new MatchError( var5 );
                }
                else
                {
                    FabricFragmenter.State statex = (FabricFragmenter.State) var5._1();
                    Either var17 = (Either) var5._2();
                    if ( !(var17 instanceof Left) )
                    {
                        throw new MatchError( var5 );
                    }
                    else
                    {
                        Left var18 = (Left) var17;
                        SubQuery sub = (SubQuery) var18.value();
                        if ( localUse.isDefined() )
                        {
                            throw Errors$.MODULE$.syntax( "Nested subqueries in remote query-parts is not supported", this.queryString, sub.position() );
                        }
                        else
                        {
                            Fragment frag = this.fragment( sub.part(), statex.incoming(), (Seq) scala.collection.Seq..MODULE$.empty(), localUse);
                            FabricQuery.Columns qual$2 = statex.columns();
                            Seq x$9 = (Seq) scala.collection.Seq..MODULE$.empty();
                            Seq x$10 = FabricQuery$Columns$.MODULE$.combine( statex.locals(), frag.columns().output() );
                            Seq x$11 = qual$2.copy$default$1();
                            Seq x$12 = qual$2.copy$default$2();
                            var4 = statex.append( new Fragment.Apply( frag, qual$2.copy( x$11, x$12, x$9, x$10 ) ) );
                            return var4;
                        }
                    }
                }
            } ); Seq var20 = result.fragments();
            Some var21 = scala.collection.Seq..MODULE$.unapplySeq( var20 );
            Object var6;
            if ( !var21.isEmpty() && var21.get() != null && ((SeqLike) var21.get()).lengthCompare( 1 ) == 0 )
            {
                Fragment single = (Fragment) ((SeqLike) var21.get()).apply( 0 );
                var6 = single;
            }
            else
            {
                var6 = new Fragment.Chain( var20, new FabricQuery.Columns( incoming, local, imports, ((Fragment) var20.last()).columns().output() ) );
            }

            var5 = var6;
        }

        return (Fragment) var5;
    }

    private Option<UseGraph> leadingUse( final SingleQuery sq )
    {
        Tuple2 var3;
        label19:
        {
            Seq clauses = sq.clausesExceptImportWith();
            Option var7 = clauses.headOption();
            if ( var7 instanceof Some )
            {
                Some var8 = (Some) var7;
                Clause u = (Clause) var8.value();
                if ( u instanceof UseGraph )
                {
                    UseGraph var10 = (UseGraph) u;
                    var3 = new Tuple2( new Some( var10 ), clauses.tail() );
                    break label19;
                }
            }

            var3 = new Tuple2( scala.None..MODULE$, clauses);
        }

        if ( var3 != null )
        {
            Option use = (Option) var3._1();
            Seq rest = (Seq) var3._2();
            Tuple2 var2 = new Tuple2( use, rest );
            Option use = (Option) var2._1();
            Seq rest = (Seq) var2._2();
            ((TraversableLike) rest.filter( ( x$3 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$leadingUse$1( x$3 ) );
            } )).map( ( clause ) -> {
                return Errors$.MODULE$.syntax( "USE can only appear at the beginning of a (sub-)query", this.queryString, clause.position() );
            }, scala.collection.Seq..MODULE$.canBuildFrom());
            return use;
        }
        else
        {
            throw new MatchError( var3 );
        }
    }

    private Seq<String> produced( final Clause clause )
    {
        Seq var2;
        if ( clause instanceof Return )
        {
            Return var4 = (Return) clause;
            var2 = (Seq) var4.returnColumns().map( ( x$4 ) -> {
                return x$4.name();
            }, scala.collection.immutable.List..MODULE$.canBuildFrom());
        }
        else
        {
            var2 = ((Scope) this.semantics.scope( clause ).get()).symbolNames().toSeq();
        }

        return var2;
    }

    private Seq<Either<SubQuery,Seq<Clause>>> partitioned( final Seq<Clause> clauses )
    {
        return this.partition( clauses, ( x0$1 ) -> {
            Object var1;
            if ( x0$1 instanceof SubQuery )
            {
                SubQuery var3 = (SubQuery) x0$1;
                var1 = scala.package..MODULE$.Left().apply( var3 );
            }
            else
            {
                var1 = scala.package..MODULE$.Right().apply( x0$1 );
            }

            return (Either) var1;
        } );
    }

    private <E, H, M> Seq<Either<H,Seq<M>>> partition( final Seq<E> es, final Function1<E,Either<H,M>> pred )
    {
        return (Seq) ((TraversableOnce) es.map( pred, scala.collection.Seq..MODULE$.canBuildFrom())).
        foldLeft( scala.collection.Seq..MODULE$.apply( scala.collection.immutable.Nil..MODULE$),( x0$3, x1$2 ) -> {
        Tuple2 var4 = new Tuple2( x0$3, x1$2 );
        Seq var2;
        if ( var4 != null )
        {
            Seq seq = (Seq) var4._1();
            Either var6 = (Either) var4._2();
            if ( var6 instanceof Left )
            {
                Left var7 = (Left) var6;
                Object hit = var7.value();
                var2 = (Seq) seq.$colon$plus( scala.package..MODULE$.Left().apply( hit ), scala.collection.Seq..MODULE$.canBuildFrom());
                return var2;
            }
        }

        if ( var4 == null )
        {
            throw new MatchError( var4 );
        }
        else
        {
            Seq seqx = (Seq) var4._1();
            Either var10 = (Either) var4._2();
            if ( !(var10 instanceof Right) )
            {
                throw new MatchError( var4 );
            }
            else
            {
                Right var11 = (Right) var10;
                Object miss = var11.value();
                boolean var13 = false;
                Some var14 = null;
                Option var15 = seqx.lastOption();
                Seq var3;
                if ( scala.None..MODULE$.equals( var15 )){
                var3 = (Seq) seqx.$colon$plus( scala.package..
                MODULE$.Right().apply( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.genericWrapArray( new Object[]{miss} ))),scala.collection.Seq..
                MODULE$.canBuildFrom());
            } else{
                label55:
                {
                    if ( var15 instanceof Some )
                    {
                        var13 = true;
                        var14 = (Some) var15;
                        Either var16 = (Either) var14.value();
                        if ( var16 instanceof Left )
                        {
                            var3 = (Seq) seqx.$colon$plus( scala.package..
                            MODULE$.Right().apply( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.genericWrapArray( new Object[]{miss} ))),
                            scala.collection.Seq..MODULE$.canBuildFrom());
                            break label55;
                        }
                    }

                    if ( !var13 )
                    {
                        throw new MatchError( var15 );
                    }

                    Either var17 = (Either) var14.value();
                    if ( !(var17 instanceof Right) )
                    {
                        throw new MatchError( var15 );
                    }

                    Right var18 = (Right) var17;
                    Seq ms = (Seq) var18.value();
                    var3 = (Seq) ((SeqLike) seqx.init()).$colon$plus( scala.package..
                    MODULE$.Right().apply( ms.$colon$plus( miss, scala.collection.Seq..MODULE$.canBuildFrom() )),scala.collection.Seq..MODULE$.canBuildFrom());
                }
            }

                var2 = var3;
                return var2;
            }
        }
    });
    }

    private final void State$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.State$module == null )
            {
                this.State$module = new FabricFragmenter$State$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class State implements Product, Serializable
    {
        private final Seq<String> incoming;
        private final Seq<String> locals;
        private final Seq<String> imports;
        private final Seq<Fragment> fragments;

        public State( final FabricFragmenter $outer, final Seq<String> incoming, final Seq<String> locals, final Seq<String> imports,
                final Seq<Fragment> fragments )
        {
            this.incoming = incoming;
            this.locals = locals;
            this.imports = imports;
            this.fragments = fragments;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
            }
        }

        public Seq<String> incoming()
        {
            return this.incoming;
        }

        public Seq<String> locals()
        {
            return this.locals;
        }

        public Seq<String> imports()
        {
            return this.imports;
        }

        public Seq<Fragment> fragments()
        {
            return this.fragments;
        }

        public FabricQuery.Columns columns()
        {
            return new FabricQuery.Columns( this.incoming(), this.locals(), this.imports(), (Seq) scala.collection.Seq..MODULE$.empty());
        }

        public FabricFragmenter.State append( final Fragment frag )
        {
            return this.copy( frag.columns().output(), frag.columns().output(), (Seq) scala.collection.Seq..MODULE$.empty(),
            (Seq) this.fragments().$colon$plus( frag, scala.collection.Seq..MODULE$.canBuildFrom()));
        }

        public FabricFragmenter.State copy( final Seq<String> incoming, final Seq<String> locals, final Seq<String> imports, final Seq<Fragment> fragments )
        {
            return this.com$neo4j$fabric$planning$FabricFragmenter$State$$$outer().new State( this.com$neo4j$fabric$planning$FabricFragmenter$State$$$outer(),
                    incoming, locals, imports, fragments );
        }

        public Seq<String> copy$default$1()
        {
            return this.incoming();
        }

        public Seq<String> copy$default$2()
        {
            return this.locals();
        }

        public Seq<String> copy$default$3()
        {
            return this.imports();
        }

        public Seq<Fragment> copy$default$4()
        {
            return this.fragments();
        }

        public String productPrefix()
        {
            return "State";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Seq var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.incoming();
                break;
            case 1:
                var10000 = this.locals();
                break;
            case 2:
                var10000 = this.imports();
                break;
            case 3:
                var10000 = this.fragments();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricFragmenter.State;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label86:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricFragmenter.State && ((FabricFragmenter.State) x$1).com$neo4j$fabric$planning$FabricFragmenter$State$$$outer() ==
                            this.com$neo4j$fabric$planning$FabricFragmenter$State$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label62:
                        {
                            label76:
                            {
                                FabricFragmenter.State var4 = (FabricFragmenter.State) x$1;
                                Seq var10000 = this.incoming();
                                Seq var5 = var4.incoming();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label76;
                                }

                                var10000 = this.locals();
                                Seq var6 = var4.locals();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label76;
                                }

                                var10000 = this.imports();
                                Seq var7 = var4.imports();
                                if ( var10000 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10000.equals( var7 ) )
                                {
                                    break label76;
                                }

                                var10000 = this.fragments();
                                Seq var8 = var4.fragments();
                                if ( var10000 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( !var10000.equals( var8 ) )
                                {
                                    break label76;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var9 = true;
                                    break label62;
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label86;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }
}
