package com.neo4j.fabric.planning;

import com.neo4j.fabric.util.PrettyPrinting$;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple4;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.Seq.;
import scala.runtime.BoxesRunTime;

public class FabricQuery$Columns$ implements Serializable
{
    public static FabricQuery$Columns$ MODULE$;

    static
    {
        new FabricQuery$Columns$();
    }

    public FabricQuery$Columns$()
    {
        MODULE$ = this;
    }

    public Seq<String> combine( final Seq<String> left, final Seq<String> right )
    {
        return (Seq) ((TraversableLike) left.filterNot( ( elem ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$combine$1( right, elem ) );
        } )).$plus$plus( right,.MODULE$.canBuildFrom());
    }

    public String paramName( final String varName )
    {
        return (new StringBuilder( 2 )).append( "@@" ).append( varName ).toString();
    }

    public Seq<Tuple2<String,String>> fields( final FabricQuery.Columns c )
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( "in" ),
                PrettyPrinting$.MODULE$.list( c.incoming() ) ),scala.Predef.ArrowAssoc..
        MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( "loc" ), PrettyPrinting$.MODULE$.list( c.local() )),scala.Predef.ArrowAssoc..
        MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( "imp" ), PrettyPrinting$.MODULE$.list( c.imports() )),scala.Predef.ArrowAssoc..
        MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( "out" ), PrettyPrinting$.MODULE$.list( c.output() ))})));
    }

    public FabricQuery.Columns apply( final Seq<String> incoming, final Seq<String> local, final Seq<String> imports, final Seq<String> output )
    {
        return new FabricQuery.Columns( incoming, local, imports, output );
    }

    public Option<Tuple4<Seq<String>,Seq<String>,Seq<String>,Seq<String>>> unapply( final FabricQuery.Columns x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple4( x$0.incoming(), x$0.local(), x$0.imports(), x$0.output() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
