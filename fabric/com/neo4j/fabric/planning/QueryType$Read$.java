package com.neo4j.fabric.planning;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;

public class QueryType$Read$ implements QueryType, Product, Serializable
{
    public static QueryType$Read$ MODULE$;

    static
    {
        new QueryType$Read$();
    }

    public QueryType$Read$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "Read";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof QueryType$Read$;
    }

    public int hashCode()
    {
        return 2543030;
    }

    public String toString()
    {
        return "Read";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
