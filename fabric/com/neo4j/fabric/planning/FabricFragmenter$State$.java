package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.runtime.AbstractFunction4;

public class FabricFragmenter$State$ extends AbstractFunction4<Seq<String>,Seq<String>,Seq<String>,Seq<Fragment>,FabricFragmenter.State> implements Serializable
{
    public FabricFragmenter$State$( final FabricFragmenter $outer )
    {
        if ( $outer == null )
        {
            throw null;
        }
        else
        {
            this.$outer = $outer;
            super();
        }
    }

    public Seq<Fragment> $lessinit$greater$default$4()
    {
        return (Seq).MODULE$.empty();
    }

    public final String toString()
    {
        return "State";
    }

    public FabricFragmenter.State apply( final Seq<String> incoming, final Seq<String> locals, final Seq<String> imports, final Seq<Fragment> fragments )
    {
        return this.$outer.new State( incoming, locals, imports, fragments );
    }

    public Seq<Fragment> apply$default$4()
    {
        return (Seq).MODULE$.empty();
    }

    public Option<Tuple4<Seq<String>,Seq<String>,Seq<String>,Seq<Fragment>>> unapply( final FabricFragmenter.State x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple4( x$0.incoming(), x$0.locals(), x$0.imports(), x$0.fragments() ) ));
    }
}
