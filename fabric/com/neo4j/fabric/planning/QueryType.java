package com.neo4j.fabric.planning;

import org.neo4j.cypher.internal.v4_0.ast.Query;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface QueryType
{
    static QueryType merge( final QueryType a, final QueryType b )
    {
        return QueryType$.MODULE$.merge( var0, var1 );
    }

    static QueryType of( final FabricQuery query )
    {
        return QueryType$.MODULE$.of( var0 );
    }

    static QueryType of( final Query query )
    {
        return QueryType$.MODULE$.of( var0 );
    }

    static QueryType WRITE()
    {
        return QueryType$.MODULE$.WRITE();
    }

    static QueryType READ_PLUS_UNRESOLVED()
    {
        return QueryType$.MODULE$.READ_PLUS_UNRESOLVED();
    }

    static QueryType READ()
    {
        return QueryType$.MODULE$.READ();
    }
}
