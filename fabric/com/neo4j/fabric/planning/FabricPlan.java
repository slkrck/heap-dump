package com.neo4j.fabric.planning;

import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class FabricPlan implements Product, Serializable
{
    private final FabricQuery query;
    private final QueryType queryType;
    private final FabricPlan.ExecutionType executionType;
    private final FabricPlan.DebugOptions debugOptions;

    public FabricPlan( final FabricQuery query, final QueryType queryType, final FabricPlan.ExecutionType executionType,
            final FabricPlan.DebugOptions debugOptions )
    {
        this.query = query;
        this.queryType = queryType;
        this.executionType = executionType;
        this.debugOptions = debugOptions;
        Product.$init$( this );
    }

    public static Option<Tuple4<FabricQuery,QueryType,FabricPlan.ExecutionType,FabricPlan.DebugOptions>> unapply( final FabricPlan x$0 )
    {
        return FabricPlan$.MODULE$.unapply( var0 );
    }

    public static FabricPlan apply( final FabricQuery query, final QueryType queryType, final FabricPlan.ExecutionType executionType,
            final FabricPlan.DebugOptions debugOptions )
    {
        return FabricPlan$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static FabricPlan.ExecutionType PROFILE()
    {
        return FabricPlan$.MODULE$.PROFILE();
    }

    public static FabricPlan.ExecutionType EXPLAIN()
    {
        return FabricPlan$.MODULE$.EXPLAIN();
    }

    public static FabricPlan.ExecutionType EXECUTE()
    {
        return FabricPlan$.MODULE$.EXECUTE();
    }

    public FabricQuery query()
    {
        return this.query;
    }

    public QueryType queryType()
    {
        return this.queryType;
    }

    public FabricPlan.ExecutionType executionType()
    {
        return this.executionType;
    }

    public FabricPlan.DebugOptions debugOptions()
    {
        return this.debugOptions;
    }

    public FabricPlan copy( final FabricQuery query, final QueryType queryType, final FabricPlan.ExecutionType executionType,
            final FabricPlan.DebugOptions debugOptions )
    {
        return new FabricPlan( query, queryType, executionType, debugOptions );
    }

    public FabricQuery copy$default$1()
    {
        return this.query();
    }

    public QueryType copy$default$2()
    {
        return this.queryType();
    }

    public FabricPlan.ExecutionType copy$default$3()
    {
        return this.executionType();
    }

    public FabricPlan.DebugOptions copy$default$4()
    {
        return this.debugOptions();
    }

    public String productPrefix()
    {
        return "FabricPlan";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.query();
            break;
        case 1:
            var10000 = this.queryType();
            break;
        case 2:
            var10000 = this.executionType();
            break;
        case 3:
            var10000 = this.debugOptions();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FabricPlan;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof FabricPlan )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            FabricPlan var4 = (FabricPlan) x$1;
                            FabricQuery var10000 = this.query();
                            FabricQuery var5 = var4.query();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            QueryType var9 = this.queryType();
                            QueryType var6 = var4.queryType();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            FabricPlan.ExecutionType var10 = this.executionType();
                            FabricPlan.ExecutionType var7 = var4.executionType();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            FabricPlan.DebugOptions var11 = this.debugOptions();
                            FabricPlan.DebugOptions var8 = var4.debugOptions();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }

    public interface ExecutionType
    {
    }

    public static class DebugOptions implements Product, Serializable
    {
        private final boolean logPlan;
        private final boolean logRecords;

        public DebugOptions( final boolean logPlan, final boolean logRecords )
        {
            this.logPlan = logPlan;
            this.logRecords = logRecords;
            Product.$init$( this );
        }

        public boolean logPlan()
        {
            return this.logPlan;
        }

        public boolean logRecords()
        {
            return this.logRecords;
        }

        public FabricPlan.DebugOptions copy( final boolean logPlan, final boolean logRecords )
        {
            return new FabricPlan.DebugOptions( logPlan, logRecords );
        }

        public boolean copy$default$1()
        {
            return this.logPlan();
        }

        public boolean copy$default$2()
        {
            return this.logRecords();
        }

        public String productPrefix()
        {
            return "DebugOptions";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Boolean var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = BoxesRunTime.boxToBoolean( this.logPlan() );
                break;
            case 1:
                var10000 = BoxesRunTime.boxToBoolean( this.logRecords() );
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricPlan.DebugOptions;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, this.logPlan() ? 1231 : 1237 );
            var1 = Statics.mix( var1, this.logRecords() ? 1231 : 1237 );
            return Statics.finalizeHash( var1, 2 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label51:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricPlan.DebugOptions )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        FabricPlan.DebugOptions var4 = (FabricPlan.DebugOptions) x$1;
                        if ( this.logPlan() == var4.logPlan() && this.logRecords() == var4.logRecords() && var4.canEqual( this ) )
                        {
                            break label51;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }
}
