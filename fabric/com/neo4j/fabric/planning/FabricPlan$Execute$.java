package com.neo4j.fabric.planning;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public class FabricPlan$Execute$ implements FabricPlan.ExecutionType, Product, Serializable
{
    public static FabricPlan$Execute$ MODULE$;

    static
    {
        new FabricPlan$Execute$();
    }

    public FabricPlan$Execute$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "Execute";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FabricPlan$Execute$;
    }

    public int hashCode()
    {
        return 345083733;
    }

    public String toString()
    {
        return "Execute";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
