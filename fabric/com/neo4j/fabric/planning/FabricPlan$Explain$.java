package com.neo4j.fabric.planning;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public class FabricPlan$Explain$ implements FabricPlan.ExecutionType, Product, Serializable
{
    public static FabricPlan$Explain$ MODULE$;

    static
    {
        new FabricPlan$Explain$();
    }

    public FabricPlan$Explain$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "Explain";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FabricPlan$Explain$;
    }

    public int hashCode()
    {
        return 355491031;
    }

    public String toString()
    {
        return "Explain";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
