package com.neo4j.fabric.planning;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public class FabricPlan$Profile$ implements FabricPlan.ExecutionType, Product, Serializable
{
    public static FabricPlan$Profile$ MODULE$;

    static
    {
        new FabricPlan$Profile$();
    }

    public FabricPlan$Profile$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "Profile";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FabricPlan$Profile$;
    }

    public int hashCode()
    {
        return 1355227529;
    }

    public String toString()
    {
        return "Profile";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
