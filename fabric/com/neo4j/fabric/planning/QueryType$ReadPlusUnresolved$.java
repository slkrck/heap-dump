package com.neo4j.fabric.planning;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;

public class QueryType$ReadPlusUnresolved$ implements QueryType, Product, Serializable
{
    public static QueryType$ReadPlusUnresolved$ MODULE$;

    static
    {
        new QueryType$ReadPlusUnresolved$();
    }

    public QueryType$ReadPlusUnresolved$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "ReadPlusUnresolved";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof QueryType$ReadPlusUnresolved$;
    }

    public int hashCode()
    {
        return -1263651295;
    }

    public String toString()
    {
        return "ReadPlusUnresolved";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
