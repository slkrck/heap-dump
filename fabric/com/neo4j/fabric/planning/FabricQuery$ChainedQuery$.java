package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction2;

public class FabricQuery$ChainedQuery$ extends AbstractFunction2<Seq<FabricQuery>,FabricQuery.Columns,FabricQuery.ChainedQuery> implements Serializable
{
    public static FabricQuery$ChainedQuery$ MODULE$;

    static
    {
        new FabricQuery$ChainedQuery$();
    }

    public FabricQuery$ChainedQuery$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ChainedQuery";
    }

    public FabricQuery.ChainedQuery apply( final Seq<FabricQuery> queries, final FabricQuery.Columns columns )
    {
        return new FabricQuery.ChainedQuery( queries, columns );
    }

    public Option<Tuple2<Seq<FabricQuery>,FabricQuery.Columns>> unapply( final FabricQuery.ChainedQuery x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.queries(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
