package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public class Fragment$Apply$ extends AbstractFunction2<Fragment,FabricQuery.Columns,Fragment.Apply> implements Serializable
{
    public static Fragment$Apply$ MODULE$;

    static
    {
        new Fragment$Apply$();
    }

    public Fragment$Apply$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Apply";
    }

    public Fragment.Apply apply( final Fragment fragment, final FabricQuery.Columns columns )
    {
        return new Fragment.Apply( fragment, columns );
    }

    public Option<Tuple2<Fragment,FabricQuery.Columns>> unapply( final Fragment.Apply x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.fragment(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
