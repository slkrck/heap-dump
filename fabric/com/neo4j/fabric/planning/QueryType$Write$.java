package com.neo4j.fabric.planning;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;

public class QueryType$Write$ implements QueryType, Product, Serializable
{
    public static QueryType$Write$ MODULE$;

    static
    {
        new QueryType$Write$();
    }

    public QueryType$Write$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "Write";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof QueryType$Write$;
    }

    public int hashCode()
    {
        return 83847103;
    }

    public String toString()
    {
        return "Write";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
