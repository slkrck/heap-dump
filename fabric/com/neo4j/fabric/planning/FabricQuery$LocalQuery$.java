package com.neo4j.fabric.planning;

import org.neo4j.cypher.internal.FullyParsedQuery;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public class FabricQuery$LocalQuery$ extends AbstractFunction3<FullyParsedQuery,FabricQuery.Columns,QueryType,FabricQuery.LocalQuery> implements Serializable
{
    public static FabricQuery$LocalQuery$ MODULE$;

    static
    {
        new FabricQuery$LocalQuery$();
    }

    public FabricQuery$LocalQuery$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "LocalQuery";
    }

    public FabricQuery.LocalQuery apply( final FullyParsedQuery query, final FabricQuery.Columns columns, final QueryType queryType )
    {
        return new FabricQuery.LocalQuery( query, columns, queryType );
    }

    public Option<Tuple3<FullyParsedQuery,FabricQuery.Columns,QueryType>> unapply( final FabricQuery.LocalQuery x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.query(), x$0.columns(), x$0.queryType() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
