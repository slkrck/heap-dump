package com.neo4j.fabric.planning;

import com.neo4j.fabric.util.PrettyPrinting;
import com.neo4j.fabric.util.PrettyPrintingUtils;
import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.ast.prettifier.Prettifier;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import scala.Function1;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Stream;
import scala.runtime.BoxesRunTime;

public final class Fragment$
{
    public static Fragment$ MODULE$;

    static
    {
        new Fragment$();
    }

    private final PrettyPrinting<Fragment> pretty;

    private Fragment$()
    {
        MODULE$ = this;
        this.pretty = new PrettyPrinting<Fragment>()
        {
            private final Prettifier com$neo4j$fabric$util$PrettyPrintingUtils$$printer;

            public
            {
                PrettyPrintingUtils.$init$( this );
                PrettyPrinting.$init$( this );
            }

            public Stream<String> node( final String name, final Seq<Tuple2<String,Object>> fields, final Seq<Fragment> children )
            {
                return PrettyPrinting.node$( this, name, fields, children );
            }

            public void pprint( final Object t )
            {
                PrettyPrinting.pprint$( this, t );
            }

            public String asString( final Object t )
            {
                return PrettyPrinting.asString$( this, t );
            }

            public Seq<Fragment> node$default$3()
            {
                return PrettyPrinting.node$default$3$( this );
            }

            public String expr( final Expression e )
            {
                return PrettyPrintingUtils.expr$( this, e );
            }

            public Stream<String> query( final Statement s )
            {
                return PrettyPrintingUtils.query$( this, (Statement) s );
            }

            public String clause( final Clause c )
            {
                return PrettyPrintingUtils.clause$( this, c );
            }

            public Stream<String> query( final Seq<Clause> cs )
            {
                return PrettyPrintingUtils.query$( this, (Seq) cs );
            }

            public String list( final Seq<Object> ss )
            {
                return PrettyPrintingUtils.list$( this, ss );
            }

            public Prettifier com$neo4j$fabric$util$PrettyPrintingUtils$$printer()
            {
                return this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer;
            }

            public final void com$neo4j$fabric$util$PrettyPrintingUtils$_setter_$com$neo4j$fabric$util$PrettyPrintingUtils$$printer_$eq( final Prettifier x$1 )
            {
                this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer = x$1;
            }

            public Function1<Fragment,Stream<String>> pretty()
            {
                return ( x0$1 ) -> {
                    Stream var2;
                    if ( x0$1 instanceof Fragment.Direct )
                    {
                        Fragment.Direct var4 = (Fragment.Direct) x0$1;
                        var2 = this.node( "direct", FabricQuery$Columns$.MODULE$.fields( var4.columns() ),
                                (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Fragment[]{var4.fragment()}) ) ));
                    }
                    else if ( x0$1 instanceof Fragment.Apply )
                    {
                        Fragment.Apply var5 = (Fragment.Apply) x0$1;
                        var2 = this.node( "apply", FabricQuery$Columns$.MODULE$.fields( var5.columns() ),
                                (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Fragment[]{var5.fragment()}) ) ));
                    }
                    else if ( x0$1 instanceof Fragment.Chain )
                    {
                        Fragment.Chain var6 = (Fragment.Chain) x0$1;
                        var2 = this.node( "chain", FabricQuery$Columns$.MODULE$.fields( var6.columns() ), var6.fragments() );
                    }
                    else if ( x0$1 instanceof Fragment.Union )
                    {
                        Fragment.Union var7 = (Fragment.Union) x0$1;
                        var2 = this.node( "union",
                                (Seq) FabricQuery$Columns$.MODULE$.fields( var7.columns() ).$plus$plus(.MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                                "dist" ), BoxesRunTime.boxToBoolean( var7.distinct() ) )}))), .MODULE$.canBuildFrom()),
                        (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Fragment[]{var7.lhs(), var7.rhs()}) )));
                    }
                    else
                    {
                        if ( !(x0$1 instanceof Fragment.Leaf) )
                        {
                            throw new MatchError( x0$1 );
                        }

                        Fragment.Leaf var8 = (Fragment.Leaf) x0$1;
                        var2 = this.node( "leaf",
                                (Seq) FabricQuery$Columns$.MODULE$.fields( var8.columns() ).$plus$plus(.MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                                "use" ), var8.use().map( ( x$1 ) -> {
                                            return x$1.expression();
                                        } ).map( ( e ) -> {
                                            return this.expr( e );
                                        } ).getOrElse( () -> {
                                            return "";
                                        } ) ), scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( "qry" ), this.query(
                                var8.clauses() ))}))), .MODULE$.canBuildFrom()),this.node$default$3());
                    }

                    return var2;
                };
            }
        };
    }

    public PrettyPrinting<Fragment> pretty()
    {
        return this.pretty;
    }
}
