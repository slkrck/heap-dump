package com.neo4j.fabric.planning;

import org.neo4j.cypher.internal.v4_0.ast.Query;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public class FabricQuery$RemoteQuery$ extends AbstractFunction4<UseGraph,Query,FabricQuery.Columns,QueryType,FabricQuery.RemoteQuery> implements Serializable
{
    public static FabricQuery$RemoteQuery$ MODULE$;

    static
    {
        new FabricQuery$RemoteQuery$();
    }

    public FabricQuery$RemoteQuery$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RemoteQuery";
    }

    public FabricQuery.RemoteQuery apply( final UseGraph use, final Query query, final FabricQuery.Columns columns, final QueryType queryType )
    {
        return new FabricQuery.RemoteQuery( use, query, columns, queryType );
    }

    public Option<Tuple4<UseGraph,Query,FabricQuery.Columns,QueryType>> unapply( final FabricQuery.RemoteQuery x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.use(), x$0.query(), x$0.columns(), x$0.queryType() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
