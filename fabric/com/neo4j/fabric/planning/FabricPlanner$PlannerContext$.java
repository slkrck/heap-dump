package com.neo4j.fabric.planning;

import com.neo4j.fabric.eval.Catalog;
import com.neo4j.fabric.pipeline.Pipeline;
import org.neo4j.cypher.internal.QueryOptions;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import org.neo4j.values.virtual.MapValue;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple7;
import scala.None.;
import scala.runtime.AbstractFunction7;

public class FabricPlanner$PlannerContext$
        extends AbstractFunction7<String,Statement,MapValue,SemanticState,QueryOptions,Catalog,Pipeline.Instance,FabricPlanner.PlannerContext>
        implements Serializable
{
    public FabricPlanner$PlannerContext$( final FabricPlanner $outer )
    {
        if ( $outer == null )
        {
            throw null;
        }
        else
        {
            this.$outer = $outer;
            super();
        }
    }

    public final String toString()
    {
        return "PlannerContext";
    }

    public FabricPlanner.PlannerContext apply( final String queryString, final Statement queryStatement, final MapValue parameters,
            final SemanticState semantics, final QueryOptions options, final Catalog catalog, final Pipeline.Instance pipeline )
    {
        return this.$outer.new PlannerContext( queryString, queryStatement, parameters, semantics, options, catalog, pipeline );
    }

    public Option<Tuple7<String,Statement,MapValue,SemanticState,QueryOptions,Catalog,Pipeline.Instance>> unapply( final FabricPlanner.PlannerContext x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple7( x$0.queryString(), x$0.queryStatement(), x$0.parameters(), x$0.semantics(), x$0.options(), x$0.catalog(), x$0.pipeline() ) ));
    }
}
