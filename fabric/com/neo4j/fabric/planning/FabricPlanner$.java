package com.neo4j.fabric.planning;

import com.neo4j.fabric.config.FabricConfig;
import org.neo4j.cypher.internal.CypherConfiguration;
import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.monitoring.Monitors;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class FabricPlanner$ extends AbstractFunction4<FabricConfig,CypherConfiguration,Monitors,ProcedureSignatureResolver,FabricPlanner>
        implements Serializable
{
    public static FabricPlanner$ MODULE$;

    static
    {
        new FabricPlanner$();
    }

    private FabricPlanner$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "FabricPlanner";
    }

    public FabricPlanner apply( final FabricConfig config, final CypherConfiguration cypherConfig, final Monitors monitors,
            final ProcedureSignatureResolver signatures )
    {
        return new FabricPlanner( config, cypherConfig, monitors, signatures );
    }

    public Option<Tuple4<FabricConfig,CypherConfiguration,Monitors,ProcedureSignatureResolver>> unapply( final FabricPlanner x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.config(), x$0.cypherConfig(), x$0.monitors(), x$0.signatures() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
