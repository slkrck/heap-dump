package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction2;

public class Fragment$Chain$ extends AbstractFunction2<Seq<Fragment>,FabricQuery.Columns,Fragment.Chain> implements Serializable
{
    public static Fragment$Chain$ MODULE$;

    static
    {
        new Fragment$Chain$();
    }

    public Fragment$Chain$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Chain";
    }

    public Fragment.Chain apply( final Seq<Fragment> fragments, final FabricQuery.Columns columns )
    {
        return new Fragment.Chain( fragments, columns );
    }

    public Option<Tuple2<Seq<Fragment>,FabricQuery.Columns>> unapply( final Fragment.Chain x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.fragments(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
