package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public class Fragment$Union$ extends AbstractFunction4<Object,Fragment,Fragment,FabricQuery.Columns,Fragment.Union> implements Serializable
{
    public static Fragment$Union$ MODULE$;

    static
    {
        new Fragment$Union$();
    }

    public Fragment$Union$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Union";
    }

    public Fragment.Union apply( final boolean distinct, final Fragment lhs, final Fragment rhs, final FabricQuery.Columns columns )
    {
        return new Fragment.Union( distinct, lhs, rhs, columns );
    }

    public Option<Tuple4<Object,Fragment,Fragment,FabricQuery.Columns>> unapply( final Fragment.Union x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( BoxesRunTime.boxToBoolean( x$0.distinct() ), x$0.lhs(), x$0.rhs(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
