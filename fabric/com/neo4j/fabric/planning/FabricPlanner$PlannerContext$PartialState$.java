package com.neo4j.fabric.planning;

import org.neo4j.cypher.internal.v4_0.ast.Statement;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public class FabricPlanner$PlannerContext$PartialState$ extends AbstractFunction1<Statement,FabricPlanner.PlannerContext.PartialState> implements Serializable
{
    public FabricPlanner$PlannerContext$PartialState$( final FabricPlanner.PlannerContext $outer )
    {
        if ( $outer == null )
        {
            throw null;
        }
        else
        {
            this.$outer = $outer;
            super();
        }
    }

    public final String toString()
    {
        return "PartialState";
    }

    public FabricPlanner.PlannerContext.PartialState apply( final Statement stmt )
    {
        return this.$outer.new PartialState( stmt );
    }

    public Option<Statement> unapply( final FabricPlanner.PlannerContext.PartialState x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.stmt() ));
    }
}
