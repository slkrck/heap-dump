package com.neo4j.fabric.planning;

import com.neo4j.fabric.util.PrettyPrinting;
import org.neo4j.cypher.internal.FullyParsedQuery;
import org.neo4j.cypher.internal.v4_0.ast.Query;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public interface FabricQuery
{
    static PrettyPrinting<FabricQuery> pretty()
    {
        return FabricQuery$.MODULE$.pretty();
    }

    FabricQuery.Columns columns();

    Seq<FabricQuery> children();

    public interface CompositeQuery extends FabricQuery
    {
    }

    public interface LeafQuery extends FabricQuery
    {
        QueryType queryType();
    }

    public static class Apply implements FabricQuery, Product, Serializable
    {
        private final FabricQuery query;
        private final FabricQuery.Columns columns;

        public Apply( final FabricQuery query, final FabricQuery.Columns columns )
        {
            this.query = query;
            this.columns = columns;
            Product.$init$( this );
        }

        public FabricQuery query()
        {
            return this.query;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Seq<FabricQuery> children()
        {
            return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new FabricQuery[]{this.query()}) ));
        }

        public FabricQuery.Apply copy( final FabricQuery query, final FabricQuery.Columns columns )
        {
            return new FabricQuery.Apply( query, columns );
        }

        public FabricQuery copy$default$1()
        {
            return this.query();
        }

        public FabricQuery.Columns copy$default$2()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "Apply";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.query();
                break;
            case 1:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricQuery.Apply;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricQuery.Apply )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                FabricQuery.Apply var4 = (FabricQuery.Apply) x$1;
                                FabricQuery var10000 = this.query();
                                FabricQuery var5 = var4.query();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                FabricQuery.Columns var7 = this.columns();
                                FabricQuery.Columns var6 = var4.columns();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class ChainedQuery implements FabricQuery.CompositeQuery, Product, Serializable
    {
        private final Seq<FabricQuery> queries;
        private final FabricQuery.Columns columns;

        public ChainedQuery( final Seq<FabricQuery> queries, final FabricQuery.Columns columns )
        {
            this.queries = queries;
            this.columns = columns;
            Product.$init$( this );
        }

        public Seq<FabricQuery> queries()
        {
            return this.queries;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Seq<FabricQuery> children()
        {
            return this.queries();
        }

        public FabricQuery.ChainedQuery copy( final Seq<FabricQuery> queries, final FabricQuery.Columns columns )
        {
            return new FabricQuery.ChainedQuery( queries, columns );
        }

        public Seq<FabricQuery> copy$default$1()
        {
            return this.queries();
        }

        public FabricQuery.Columns copy$default$2()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "ChainedQuery";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.queries();
                break;
            case 1:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricQuery.ChainedQuery;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricQuery.ChainedQuery )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                FabricQuery.ChainedQuery var4 = (FabricQuery.ChainedQuery) x$1;
                                Seq var10000 = this.queries();
                                Seq var5 = var4.queries();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                FabricQuery.Columns var7 = this.columns();
                                FabricQuery.Columns var6 = var4.columns();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class Columns implements Product, Serializable
    {
        private final Seq<String> incoming;
        private final Seq<String> local;
        private final Seq<String> imports;
        private final Seq<String> output;

        public Columns( final Seq<String> incoming, final Seq<String> local, final Seq<String> imports, final Seq<String> output )
        {
            this.incoming = incoming;
            this.local = local;
            this.imports = imports;
            this.output = output;
            Product.$init$( this );
        }

        public Seq<String> incoming()
        {
            return this.incoming;
        }

        public Seq<String> local()
        {
            return this.local;
        }

        public Seq<String> imports()
        {
            return this.imports;
        }

        public Seq<String> output()
        {
            return this.output;
        }

        public FabricQuery.Columns copy( final Seq<String> incoming, final Seq<String> local, final Seq<String> imports, final Seq<String> output )
        {
            return new FabricQuery.Columns( incoming, local, imports, output );
        }

        public Seq<String> copy$default$1()
        {
            return this.incoming();
        }

        public Seq<String> copy$default$2()
        {
            return this.local();
        }

        public Seq<String> copy$default$3()
        {
            return this.imports();
        }

        public Seq<String> copy$default$4()
        {
            return this.output();
        }

        public String productPrefix()
        {
            return "Columns";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Seq var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.incoming();
                break;
            case 1:
                var10000 = this.local();
                break;
            case 2:
                var10000 = this.imports();
                break;
            case 3:
                var10000 = this.output();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricQuery.Columns;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label81:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricQuery.Columns )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label63:
                        {
                            label72:
                            {
                                FabricQuery.Columns var4 = (FabricQuery.Columns) x$1;
                                Seq var10000 = this.incoming();
                                Seq var5 = var4.incoming();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label72;
                                }

                                var10000 = this.local();
                                Seq var6 = var4.local();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label72;
                                }

                                var10000 = this.imports();
                                Seq var7 = var4.imports();
                                if ( var10000 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10000.equals( var7 ) )
                                {
                                    break label72;
                                }

                                var10000 = this.output();
                                Seq var8 = var4.output();
                                if ( var10000 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10000.equals( var8 ) )
                                {
                                    break label72;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var9 = true;
                                    break label63;
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label81;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }

    public static class Direct implements FabricQuery, Product, Serializable
    {
        private final FabricQuery query;
        private final FabricQuery.Columns columns;

        public Direct( final FabricQuery query, final FabricQuery.Columns columns )
        {
            this.query = query;
            this.columns = columns;
            Product.$init$( this );
        }

        public FabricQuery query()
        {
            return this.query;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Seq<FabricQuery> children()
        {
            return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new FabricQuery[]{this.query()}) ));
        }

        public FabricQuery.Direct copy( final FabricQuery query, final FabricQuery.Columns columns )
        {
            return new FabricQuery.Direct( query, columns );
        }

        public FabricQuery copy$default$1()
        {
            return this.query();
        }

        public FabricQuery.Columns copy$default$2()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "Direct";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.query();
                break;
            case 1:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricQuery.Direct;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricQuery.Direct )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                FabricQuery.Direct var4 = (FabricQuery.Direct) x$1;
                                FabricQuery var10000 = this.query();
                                FabricQuery var5 = var4.query();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                FabricQuery.Columns var7 = this.columns();
                                FabricQuery.Columns var6 = var4.columns();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class LocalQuery implements FabricQuery.LeafQuery, Product, Serializable
    {
        private final FullyParsedQuery query;
        private final FabricQuery.Columns columns;
        private final QueryType queryType;

        public LocalQuery( final FullyParsedQuery query, final FabricQuery.Columns columns, final QueryType queryType )
        {
            this.query = query;
            this.columns = columns;
            this.queryType = queryType;
            Product.$init$( this );
        }

        public FullyParsedQuery query()
        {
            return this.query;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public QueryType queryType()
        {
            return this.queryType;
        }

        public Seq<String> input()
        {
            return FabricQuery$Columns$.MODULE$.combine( this.columns().local(), this.columns().imports() );
        }

        public Seq<FabricQuery> children()
        {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        }

        public FabricQuery.LocalQuery copy( final FullyParsedQuery query, final FabricQuery.Columns columns, final QueryType queryType )
        {
            return new FabricQuery.LocalQuery( query, columns, queryType );
        }

        public FullyParsedQuery copy$default$1()
        {
            return this.query();
        }

        public FabricQuery.Columns copy$default$2()
        {
            return this.columns();
        }

        public QueryType copy$default$3()
        {
            return this.queryType();
        }

        public String productPrefix()
        {
            return "LocalQuery";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.query();
                break;
            case 1:
                var10000 = this.columns();
                break;
            case 2:
                var10000 = this.queryType();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricQuery.LocalQuery;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricQuery.LocalQuery )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                FabricQuery.LocalQuery var4 = (FabricQuery.LocalQuery) x$1;
                                FullyParsedQuery var10000 = this.query();
                                FullyParsedQuery var5 = var4.query();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                FabricQuery.Columns var8 = this.columns();
                                FabricQuery.Columns var6 = var4.columns();
                                if ( var8 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var6 ) )
                                {
                                    break label63;
                                }

                                QueryType var9 = this.queryType();
                                QueryType var7 = var4.queryType();
                                if ( var9 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var9.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var10 = true;
                                    break label54;
                                }
                            }

                            var10 = false;
                        }

                        if ( var10 )
                        {
                            break label72;
                        }
                    }

                    var10 = false;
                    return var10;
                }
            }

            var10 = true;
            return var10;
        }
    }

    public static class RemoteQuery implements FabricQuery.LeafQuery, Product, Serializable
    {
        private final UseGraph use;
        private final Query query;
        private final FabricQuery.Columns columns;
        private final QueryType queryType;

        public RemoteQuery( final UseGraph use, final Query query, final FabricQuery.Columns columns, final QueryType queryType )
        {
            this.use = use;
            this.query = query;
            this.columns = columns;
            this.queryType = queryType;
            Product.$init$( this );
        }

        public UseGraph use()
        {
            return this.use;
        }

        public Query query()
        {
            return this.query;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public QueryType queryType()
        {
            return this.queryType;
        }

        public Map<String,String> parameters()
        {
            return ((TraversableOnce) FabricQuery$Columns$.MODULE$.combine( this.columns().local(), this.columns().imports() ).map( ( n ) -> {
                return scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( n ), FabricQuery$Columns$.MODULE$.paramName( n ));
            }, scala.collection.Seq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
        }

        public String queryString()
        {
            return FabricQuery$.MODULE$.com$neo4j$fabric$planning$FabricQuery$$renderer().asString( this.query() );
        }

        public Seq<FabricQuery> children()
        {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        }

        public FabricQuery.RemoteQuery copy( final UseGraph use, final Query query, final FabricQuery.Columns columns, final QueryType queryType )
        {
            return new FabricQuery.RemoteQuery( use, query, columns, queryType );
        }

        public UseGraph copy$default$1()
        {
            return this.use();
        }

        public Query copy$default$2()
        {
            return this.query();
        }

        public FabricQuery.Columns copy$default$3()
        {
            return this.columns();
        }

        public QueryType copy$default$4()
        {
            return this.queryType();
        }

        public String productPrefix()
        {
            return "RemoteQuery";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.use();
                break;
            case 1:
                var10000 = this.query();
                break;
            case 2:
                var10000 = this.columns();
                break;
            case 3:
                var10000 = this.queryType();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricQuery.RemoteQuery;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var12;
            if ( this != x$1 )
            {
                label81:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricQuery.RemoteQuery )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label63:
                        {
                            label72:
                            {
                                FabricQuery.RemoteQuery var4 = (FabricQuery.RemoteQuery) x$1;
                                UseGraph var10000 = this.use();
                                UseGraph var5 = var4.use();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label72;
                                }

                                Query var9 = this.query();
                                Query var6 = var4.query();
                                if ( var9 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var9.equals( var6 ) )
                                {
                                    break label72;
                                }

                                FabricQuery.Columns var10 = this.columns();
                                FabricQuery.Columns var7 = var4.columns();
                                if ( var10 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10.equals( var7 ) )
                                {
                                    break label72;
                                }

                                QueryType var11 = this.queryType();
                                QueryType var8 = var4.queryType();
                                if ( var11 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var11.equals( var8 ) )
                                {
                                    break label72;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var12 = true;
                                    break label63;
                                }
                            }

                            var12 = false;
                        }

                        if ( var12 )
                        {
                            break label81;
                        }
                    }

                    var12 = false;
                    return var12;
                }
            }

            var12 = true;
            return var12;
        }
    }

    public static class UnionQuery implements FabricQuery.CompositeQuery, Product, Serializable
    {
        private final FabricQuery lhs;
        private final FabricQuery rhs;
        private final boolean distinct;
        private final FabricQuery.Columns columns;

        public UnionQuery( final FabricQuery lhs, final FabricQuery rhs, final boolean distinct, final FabricQuery.Columns columns )
        {
            this.lhs = lhs;
            this.rhs = rhs;
            this.distinct = distinct;
            this.columns = columns;
            Product.$init$( this );
        }

        public FabricQuery lhs()
        {
            return this.lhs;
        }

        public FabricQuery rhs()
        {
            return this.rhs;
        }

        public boolean distinct()
        {
            return this.distinct;
        }

        public FabricQuery.Columns columns()
        {
            return this.columns;
        }

        public Seq<FabricQuery> children()
        {
            return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new FabricQuery[]{this.lhs(), this.rhs()}) ));
        }

        public FabricQuery.UnionQuery copy( final FabricQuery lhs, final FabricQuery rhs, final boolean distinct, final FabricQuery.Columns columns )
        {
            return new FabricQuery.UnionQuery( lhs, rhs, distinct, columns );
        }

        public FabricQuery copy$default$1()
        {
            return this.lhs();
        }

        public FabricQuery copy$default$2()
        {
            return this.rhs();
        }

        public boolean copy$default$3()
        {
            return this.distinct();
        }

        public FabricQuery.Columns copy$default$4()
        {
            return this.columns();
        }

        public String productPrefix()
        {
            return "UnionQuery";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.lhs();
                break;
            case 1:
                var10000 = this.rhs();
                break;
            case 2:
                var10000 = BoxesRunTime.boxToBoolean( this.distinct() );
                break;
            case 3:
                var10000 = this.columns();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricQuery.UnionQuery;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.anyHash( this.lhs() ) );
            var1 = Statics.mix( var1, Statics.anyHash( this.rhs() ) );
            var1 = Statics.mix( var1, this.distinct() ? 1231 : 1237 );
            var1 = Statics.mix( var1, Statics.anyHash( this.columns() ) );
            return Statics.finalizeHash( var1, 4 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label74:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricQuery.UnionQuery )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label56:
                        {
                            label65:
                            {
                                FabricQuery.UnionQuery var4 = (FabricQuery.UnionQuery) x$1;
                                FabricQuery var10000 = this.lhs();
                                FabricQuery var5 = var4.lhs();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label65;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label65;
                                }

                                var10000 = this.rhs();
                                FabricQuery var6 = var4.rhs();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label65;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label65;
                                }

                                if ( this.distinct() == var4.distinct() )
                                {
                                    label44:
                                    {
                                        FabricQuery.Columns var8 = this.columns();
                                        FabricQuery.Columns var7 = var4.columns();
                                        if ( var8 == null )
                                        {
                                            if ( var7 != null )
                                            {
                                                break label44;
                                            }
                                        }
                                        else if ( !var8.equals( var7 ) )
                                        {
                                            break label44;
                                        }

                                        if ( var4.canEqual( this ) )
                                        {
                                            var9 = true;
                                            break label56;
                                        }
                                    }
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label74;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }
}
