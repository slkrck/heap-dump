package com.neo4j.fabric.planning;

import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction3;

public class Fragment$Leaf$ extends AbstractFunction3<Option<UseGraph>,Seq<Clause>,FabricQuery.Columns,Fragment.Leaf> implements Serializable
{
    public static Fragment$Leaf$ MODULE$;

    static
    {
        new Fragment$Leaf$();
    }

    public Fragment$Leaf$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Leaf";
    }

    public Fragment.Leaf apply( final Option<UseGraph> use, final Seq<Clause> clauses, final FabricQuery.Columns columns )
    {
        return new Fragment.Leaf( use, clauses, columns );
    }

    public Option<Tuple3<Option<UseGraph>,Seq<Clause>,FabricQuery.Columns>> unapply( final Fragment.Leaf x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.use(), x$0.clauses(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
