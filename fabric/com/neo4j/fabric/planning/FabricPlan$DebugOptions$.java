package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.Tuple2.mcZZ.sp;
import scala.collection.immutable.Set;

public class FabricPlan$DebugOptions$ implements Serializable
{
    public static FabricPlan$DebugOptions$ MODULE$;

    static
    {
        new FabricPlan$DebugOptions$();
    }

    public FabricPlan$DebugOptions$()
    {
        MODULE$ = this;
    }

    public FabricPlan.DebugOptions from( final Set<String> debugOptions )
    {
        return new FabricPlan.DebugOptions( debugOptions.contains( "fabriclogplan" ), debugOptions.contains( "fabriclogrecords" ) );
    }

    public FabricPlan.DebugOptions apply( final boolean logPlan, final boolean logRecords )
    {
        return new FabricPlan.DebugOptions( logPlan, logRecords );
    }

    public Option<Tuple2<Object,Object>> unapply( final FabricPlan.DebugOptions x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new sp( x$0.logPlan(), x$0.logRecords() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
