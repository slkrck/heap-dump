package com.neo4j.fabric.planning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public class Fragment$Direct$ extends AbstractFunction2<Fragment,FabricQuery.Columns,Fragment.Direct> implements Serializable
{
    public static Fragment$Direct$ MODULE$;

    static
    {
        new Fragment$Direct$();
    }

    public Fragment$Direct$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Direct";
    }

    public Fragment.Direct apply( final Fragment fragment, final FabricQuery.Columns columns )
    {
        return new Fragment.Direct( fragment, columns );
    }

    public Option<Tuple2<Fragment,FabricQuery.Columns>> unapply( final Fragment.Direct x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.fragment(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
