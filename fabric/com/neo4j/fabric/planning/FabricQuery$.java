package com.neo4j.fabric.planning;

import com.neo4j.fabric.util.PrettyPrinting;
import com.neo4j.fabric.util.PrettyPrintingUtils;
import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.ast.prettifier.ExpressionStringifier;
import org.neo4j.cypher.internal.v4_0.ast.prettifier.Prettifier;
import org.neo4j.cypher.internal.v4_0.ast.prettifier.ExpressionStringifier.;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import scala.Function1;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.immutable.Stream;
import scala.runtime.BoxesRunTime;

public final class FabricQuery$
{
    public static FabricQuery$ MODULE$;

    static
    {
        new FabricQuery$();
    }

    private final Prettifier com$neo4j$fabric$planning$FabricQuery$$renderer;
    private final PrettyPrinting<FabricQuery> pretty;

    private FabricQuery$()
    {
        MODULE$ = this;
        boolean x$1 = true;
        boolean x$2 = true;
        Function1 x$3 = .MODULE$.apply$default$1();
        this.com$neo4j$fabric$planning$FabricQuery$$renderer = new Prettifier( new ExpressionStringifier( x$3, x$1, x$2 ) );
        this.pretty = new PrettyPrinting<FabricQuery>()
        {
            private final Prettifier com$neo4j$fabric$util$PrettyPrintingUtils$$printer;

            public
            {
                PrettyPrintingUtils.$init$( this );
                PrettyPrinting.$init$( this );
            }

            public Stream<String> node( final String name, final Seq<Tuple2<String,Object>> fields, final Seq<FabricQuery> children )
            {
                return PrettyPrinting.node$( this, name, fields, children );
            }

            public void pprint( final Object t )
            {
                PrettyPrinting.pprint$( this, t );
            }

            public String asString( final Object t )
            {
                return PrettyPrinting.asString$( this, t );
            }

            public Seq<FabricQuery> node$default$3()
            {
                return PrettyPrinting.node$default$3$( this );
            }

            public String expr( final Expression e )
            {
                return PrettyPrintingUtils.expr$( this, e );
            }

            public Stream<String> query( final Statement s )
            {
                return PrettyPrintingUtils.query$( this, (Statement) s );
            }

            public String clause( final Clause c )
            {
                return PrettyPrintingUtils.clause$( this, c );
            }

            public Stream<String> query( final Seq<Clause> cs )
            {
                return PrettyPrintingUtils.query$( this, (Seq) cs );
            }

            public String list( final Seq<Object> ss )
            {
                return PrettyPrintingUtils.list$( this, ss );
            }

            public Prettifier com$neo4j$fabric$util$PrettyPrintingUtils$$printer()
            {
                return this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer;
            }

            public final void com$neo4j$fabric$util$PrettyPrintingUtils$_setter_$com$neo4j$fabric$util$PrettyPrintingUtils$$printer_$eq( final Prettifier x$1 )
            {
                this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer = x$1;
            }

            public Function1<FabricQuery,Stream<String>> pretty()
            {
                return ( x0$1 ) -> {
                    Stream var2;
                    if ( x0$1 instanceof FabricQuery.Direct )
                    {
                        FabricQuery.Direct var4 = (FabricQuery.Direct) x0$1;
                        var2 = this.node( "direct", FabricQuery$Columns$.MODULE$.fields( var4.columns() ), (Seq) scala.collection.Seq..MODULE$.apply(
                                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new FabricQuery[]{var4.query()}) )));
                    }
                    else if ( x0$1 instanceof FabricQuery.Apply )
                    {
                        FabricQuery.Apply var5 = (FabricQuery.Apply) x0$1;
                        var2 = this.node( "apply", FabricQuery$Columns$.MODULE$.fields( var5.columns() ), (Seq) scala.collection.Seq..MODULE$.apply(
                                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new FabricQuery[]{var5.query()}) )));
                    }
                    else if ( x0$1 instanceof FabricQuery.LocalQuery )
                    {
                        FabricQuery.LocalQuery var6 = (FabricQuery.LocalQuery) x0$1;
                        var2 = this.node( "local", (Seq) FabricQuery$Columns$.MODULE$.fields( var6.columns() ).$plus$plus( scala.collection.Seq..MODULE$.apply(
                                scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( "qry" ),
                                this.query( var6.query().state().statement() ) )}))),scala.collection.Seq..MODULE$.canBuildFrom()),this.node$default$3());
                    }
                    else if ( x0$1 instanceof FabricQuery.RemoteQuery )
                    {
                        FabricQuery.RemoteQuery var7 = (FabricQuery.RemoteQuery) x0$1;
                        var2 = this.node( (new StringBuilder( 5 )).append( "use: " ).append( this.expr( var7.use().expression() ) ).toString(),
                                (Seq) FabricQuery$Columns$.MODULE$.fields( var7.columns() ).$plus$plus( scala.collection.Seq..MODULE$.apply(
                                        scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                                "params" ), this.list( var7.parameters().toSeq() ) ), scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension(
                                scala.Predef..MODULE$.ArrowAssoc( "qry" ), this.query( (Statement) var7.query() ))}))),scala.collection.Seq..
                        MODULE$.canBuildFrom()),this.node$default$3());
                    }
                    else if ( x0$1 instanceof FabricQuery.ChainedQuery )
                    {
                        FabricQuery.ChainedQuery var8 = (FabricQuery.ChainedQuery) x0$1;
                        var2 = this.node( "chain", FabricQuery$Columns$.MODULE$.fields( var8.columns() ), var8.queries() );
                    }
                    else
                    {
                        if ( !(x0$1 instanceof FabricQuery.UnionQuery) )
                        {
                            throw new MatchError( x0$1 );
                        }

                        FabricQuery.UnionQuery var9 = (FabricQuery.UnionQuery) x0$1;
                        var2 = this.node( "union", (Seq) FabricQuery$Columns$.MODULE$.fields( var9.columns() ).$plus$plus( scala.collection.Seq..MODULE$.apply(
                                scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                        "distinct" ), BoxesRunTime.boxToBoolean( var9.distinct() ) )}))),scala.collection.Seq..MODULE$.canBuildFrom()),
                        (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new FabricQuery[]{var9.lhs(), var9.rhs()}) )))
                        ;
                    }

                    return var2;
                };
            }
        };
    }

    public Prettifier com$neo4j$fabric$planning$FabricQuery$$renderer()
    {
        return this.com$neo4j$fabric$planning$FabricQuery$$renderer;
    }

    public PrettyPrinting<FabricQuery> pretty()
    {
        return this.pretty;
    }
}
