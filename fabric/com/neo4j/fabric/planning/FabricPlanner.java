package com.neo4j.fabric.planning;

import com.neo4j.fabric.cache.FabricQueryCache;
import com.neo4j.fabric.config.FabricConfig;
import com.neo4j.fabric.eval.Catalog;
import com.neo4j.fabric.eval.Catalog$;
import com.neo4j.fabric.executor.FabricException;
import com.neo4j.fabric.pipeline.Pipeline;
import com.neo4j.fabric.util.Errors$;
import com.neo4j.fabric.util.Rewritten$;
import com.neo4j.fabric.util.Rewritten$RewritingOps$;
import org.neo4j.cypher.CypherExecutionMode;
import org.neo4j.cypher.CypherInterpretedPipesFallbackOption;
import org.neo4j.cypher.CypherOperatorEngineOption;
import org.neo4j.cypher.CypherPlannerOption;
import org.neo4j.cypher.CypherUpdateStrategy;
import org.neo4j.cypher.CypherVersion;
import org.neo4j.cypher.internal.CypherConfiguration;
import org.neo4j.cypher.internal.FullyParsedQuery;
import org.neo4j.cypher.internal.PreParsedQuery;
import org.neo4j.cypher.internal.PreParser;
import org.neo4j.cypher.internal.QueryOptions;
import org.neo4j.cypher.internal.logical.plans.QualifiedName;
import org.neo4j.cypher.internal.logical.plans.ResolvedCall;
import org.neo4j.cypher.internal.logical.plans.ResolvedFunctionInvocation;
import org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver;
import org.neo4j.cypher.internal.v4_0.ast.AliasedReturnItem;
import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.InputDataStream;
import org.neo4j.cypher.internal.v4_0.ast.ProcedureResult;
import org.neo4j.cypher.internal.v4_0.ast.Query;
import org.neo4j.cypher.internal.v4_0.ast.Return;
import org.neo4j.cypher.internal.v4_0.ast.ReturnItems;
import org.neo4j.cypher.internal.v4_0.ast.SingleQuery;
import org.neo4j.cypher.internal.v4_0.ast.UnresolvedCall;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionName;
import org.neo4j.cypher.internal.v4_0.expressions.Namespace;
import org.neo4j.cypher.internal.v4_0.expressions.Parameter;
import org.neo4j.cypher.internal.v4_0.expressions.ProcedureName;
import org.neo4j.cypher.internal.v4_0.expressions.Variable;
import org.neo4j.cypher.internal.v4_0.frontend.PlannerName;
import org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState;
import org.neo4j.cypher.internal.v4_0.frontend.phases.Condition;
import org.neo4j.cypher.internal.v4_0.util.ASTNode;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.neo4j.monitoring.Monitors;
import org.neo4j.values.virtual.MapValue;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class FabricPlanner implements Product, Serializable
{
    private final FabricConfig config;
    private final CypherConfiguration cypherConfig;
    private final Monitors monitors;
    private final ProcedureSignatureResolver signatures;
    private final PreParser preParser;
    private final Catalog catalog;
    private final FabricQueryCache queryCache;
    private volatile FabricPlanner$PlannerContext$ PlannerContext$module;

    public FabricPlanner( final FabricConfig config, final CypherConfiguration cypherConfig, final Monitors monitors,
            final ProcedureSignatureResolver signatures )
    {
        this.config = config;
        this.cypherConfig = cypherConfig;
        this.monitors = monitors;
        this.signatures = signatures;
        Product.$init$( this );
        this.preParser = new PreParser( cypherConfig.version(), cypherConfig.planner(), cypherConfig.runtime(), cypherConfig.expressionEngineOption(),
                cypherConfig.operatorEngine(), cypherConfig.interpretedPipesFallback(), cypherConfig.queryCacheSize() );
        this.catalog = Catalog$.MODULE$.fromConfig( config );
        this.queryCache = new FabricQueryCache( cypherConfig.queryCacheSize() );
    }

    public static Option<Tuple4<FabricConfig,CypherConfiguration,Monitors,ProcedureSignatureResolver>> unapply( final FabricPlanner x$0 )
    {
        return FabricPlanner$.MODULE$.unapply( var0 );
    }

    public static FabricPlanner apply( final FabricConfig config, final CypherConfiguration cypherConfig, final Monitors monitors,
            final ProcedureSignatureResolver signatures )
    {
        return FabricPlanner$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<FabricConfig,CypherConfiguration,Monitors,ProcedureSignatureResolver>,FabricPlanner> tupled()
    {
        return FabricPlanner$.MODULE$.tupled();
    }

    public static Function1<FabricConfig,Function1<CypherConfiguration,Function1<Monitors,Function1<ProcedureSignatureResolver,FabricPlanner>>>> curried()
    {
        return FabricPlanner$.MODULE$.curried();
    }

    private static final void check$1( final String name, final Object a, final Object b )
    {
        if ( !BoxesRunTime.equals( a, b ) )
        {
            throw Errors$.MODULE$.notSupported( (new StringBuilder( 15 )).append( "Query option '" ).append( name ).append( "'" ).toString() );
        }
    }

    private FabricPlanner$PlannerContext$ PlannerContext()
    {
        if ( this.PlannerContext$module == null )
        {
            this.PlannerContext$lzycompute$1();
        }

        return this.PlannerContext$module;
    }

    public FabricConfig config()
    {
        return this.config;
    }

    public CypherConfiguration cypherConfig()
    {
        return this.cypherConfig;
    }

    public Monitors monitors()
    {
        return this.monitors;
    }

    public ProcedureSignatureResolver signatures()
    {
        return this.signatures;
    }

    private PreParser preParser()
    {
        return this.preParser;
    }

    private Catalog catalog()
    {
        return this.catalog;
    }

    public FabricQueryCache queryCache()
    {
        return this.queryCache;
    }

    public FabricPlan plan( final String query, final MapValue parameters )
    {
        return this.queryCache().computeIfAbsent( query, parameters, ( q, p ) -> {
            return this.prepare( q, p ).plan();
        } );
    }

    private FabricPlanner.PlannerContext prepare( final String query, final MapValue parameters )
    {
        Pipeline.Instance pipeline = new Pipeline.Instance( this.monitors(), query, this.signatures() );
        PreParsedQuery preParsed = this.preParse( query );
        BaseState state = pipeline.parseAndPrepare().process( preParsed.statement() );
        return new FabricPlanner.PlannerContext( this, query, state.statement(), parameters, state.semantics(), preParsed.options(), this.catalog(), pipeline );
    }

    private PreParsedQuery preParse( final String query )
    {
        PreParsedQuery preParsed = this.preParser().preParseQuery( query, this.preParser().preParseQuery$default$2() );
        this.assertNotPeriodicCommit( preParsed );
        this.assertOptionsNotSet( preParsed.options() );
        return preParsed;
    }

    private void assertNotPeriodicCommit( final PreParsedQuery preParsedStatement )
    {
        if ( preParsedStatement.options().isPeriodicCommit() )
        {
            throw new FabricException( Statement.SemanticError, "Periodic commit is not supported in Fabric", new Object[0] );
        }
    }

    private void assertOptionsNotSet( final QueryOptions options )
    {
        check$1( "version", options.version(), this.cypherConfig().version() );
        check$1( "planner", options.planner(), this.cypherConfig().planner() );
        check$1( "runtime", options.runtime(), this.cypherConfig().runtime() );
        check$1( "updateStrategy", options.updateStrategy(), org.neo4j.cypher.CypherUpdateStrategy.
        default..MODULE$);
        check$1( "expressionEngine", options.expressionEngine(), this.cypherConfig().expressionEngineOption() );
        check$1( "operatorEngine", options.operatorEngine(), this.cypherConfig().operatorEngine() );
        check$1( "interpretedPipesFallback", options.interpretedPipesFallback(), this.cypherConfig().interpretedPipesFallback() );
    }

    public FabricPlanner copy( final FabricConfig config, final CypherConfiguration cypherConfig, final Monitors monitors,
            final ProcedureSignatureResolver signatures )
    {
        return new FabricPlanner( config, cypherConfig, monitors, signatures );
    }

    public FabricConfig copy$default$1()
    {
        return this.config();
    }

    public CypherConfiguration copy$default$2()
    {
        return this.cypherConfig();
    }

    public Monitors copy$default$3()
    {
        return this.monitors();
    }

    public ProcedureSignatureResolver copy$default$4()
    {
        return this.signatures();
    }

    public String productPrefix()
    {
        return "FabricPlanner";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.config();
            break;
        case 1:
            var10000 = this.cypherConfig();
            break;
        case 2:
            var10000 = this.monitors();
            break;
        case 3:
            var10000 = this.signatures();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FabricPlanner;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof FabricPlanner )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            FabricPlanner var4 = (FabricPlanner) x$1;
                            FabricConfig var10000 = this.config();
                            FabricConfig var5 = var4.config();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            CypherConfiguration var9 = this.cypherConfig();
                            CypherConfiguration var6 = var4.cypherConfig();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            Monitors var10 = this.monitors();
                            Monitors var7 = var4.monitors();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            ProcedureSignatureResolver var11 = this.signatures();
                            ProcedureSignatureResolver var8 = var4.signatures();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }

    private final void PlannerContext$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.PlannerContext$module == null )
            {
                this.PlannerContext$module = new FabricPlanner$PlannerContext$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class PlannerContext implements Product, Serializable
    {
        private final String queryString;
        private final org.neo4j.cypher.internal.v4_0.ast.Statement queryStatement;
        private final MapValue parameters;
        private final SemanticState semantics;
        private final QueryOptions options;
        private final Catalog catalog;
        private final Pipeline.Instance pipeline;
        private final FabricFragmenter fragmenter;
        private volatile FabricPlanner$PlannerContext$PartialState$ PartialState$module;

        public PlannerContext( final FabricPlanner $outer, final String queryString, final org.neo4j.cypher.internal.v4_0.ast.Statement queryStatement,
                final MapValue parameters, final SemanticState semantics, final QueryOptions options, final Catalog catalog, final Pipeline.Instance pipeline )
        {
            this.queryString = queryString;
            this.queryStatement = queryStatement;
            this.parameters = parameters;
            this.semantics = semantics;
            this.options = options;
            this.catalog = catalog;
            this.pipeline = pipeline;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
                this.fragmenter = new FabricFragmenter( queryString, queryStatement, semantics );
            }
        }

        private FabricPlanner$PlannerContext$PartialState$ PartialState()
        {
            if ( this.PartialState$module == null )
            {
                this.PartialState$lzycompute$1();
            }

            return this.PartialState$module;
        }

        public String queryString()
        {
            return this.queryString;
        }

        public org.neo4j.cypher.internal.v4_0.ast.Statement queryStatement()
        {
            return this.queryStatement;
        }

        public MapValue parameters()
        {
            return this.parameters;
        }

        public SemanticState semantics()
        {
            return this.semantics;
        }

        public QueryOptions options()
        {
            return this.options;
        }

        public Catalog catalog()
        {
            return this.catalog;
        }

        public Pipeline.Instance pipeline()
        {
            return this.pipeline;
        }

        private FabricFragmenter fragmenter()
        {
            return this.fragmenter;
        }

        public FabricPlan plan()
        {
            FabricQuery fQuery = this.fabricQuery();
            FabricPlan var10000 = new FabricPlan; QueryType var10003 = QueryType$.MODULE$.of( fQuery );
            CypherExecutionMode var3 = this.options().executionMode();
            Object var1;
            if ( org.neo4j.cypher.CypherExecutionMode.normal..MODULE$.equals( var3 )){
            var1 = FabricPlan$Execute$.MODULE$;
        } else{
            if ( !org.neo4j.cypher.CypherExecutionMode.explain..MODULE$.equals( var3 )){
                if ( org.neo4j.cypher.CypherExecutionMode.profile..MODULE$.equals( var3 )){
                    throw Errors$.MODULE$.notSupported( "Query option: 'PROFILE'" );
                }

                throw new MatchError( var3 );
            }

            var1 = FabricPlan$Explain$.MODULE$;
        }

            var10000.<init> (fQuery, var10003, (FabricPlan.ExecutionType) var1, FabricPlan$DebugOptions$.MODULE$.from( this.options().debugOptions() ));
            return var10000;
        }

        private FabricQuery fabricQuery()
        {
            return this.fabricQuery( this.fragmenter().fragment() );
        }

        private FabricQuery fabricQuery( final Fragment fragment )
        {
            Object var2;
            if ( fragment instanceof Fragment.Chain )
            {
                Fragment.Chain var5 = (Fragment.Chain) fragment;
                var2 = new FabricQuery.ChainedQuery( (Seq) var5.fragments().map( ( fragmentx ) -> {
                    return this.fabricQuery( fragmentx );
                }, scala.collection.Seq..MODULE$.canBuildFrom() ), var5.columns());
            }
            else if ( fragment instanceof Fragment.Union )
            {
                Fragment.Union var6 = (Fragment.Union) fragment;
                var2 = new FabricQuery.UnionQuery( this.fabricQuery( var6.lhs() ), this.fabricQuery( var6.rhs() ), var6.distinct(), var6.columns() );
            }
            else if ( fragment instanceof Fragment.Direct )
            {
                Fragment.Direct var7 = (Fragment.Direct) fragment;
                var2 = new FabricQuery.Direct( this.fabricQuery( var7.fragment() ), var7.columns() );
            }
            else if ( fragment instanceof Fragment.Apply )
            {
                Fragment.Apply var8 = (Fragment.Apply) fragment;
                var2 = new FabricQuery.Apply( this.fabricQuery( var8.fragment() ), var8.columns() );
            }
            else
            {
                if ( !(fragment instanceof Fragment.Leaf) )
                {
                    throw new MatchError( fragment );
                }

                Fragment.Leaf var9 = (Fragment.Leaf) fragment;
                InputPosition pos = ((ASTNode) var9.clauses().head()).position();
                Query query = new Query( scala.None..MODULE$, new SingleQuery( var9.clauses(), pos ), pos);
                QueryType queryType = QueryType$.MODULE$.of( query );
                Option var14 = var9.use();
                Object var3;
                if ( var14 instanceof Some )
                {
                    Some var15 = (Some) var14;
                    UseGraph use = (UseGraph) var15.value();
                    var3 = new FabricQuery.RemoteQuery( use, query, var9.columns(), queryType );
                }
                else
                {
                    if ( !scala.None..MODULE$.equals( var14 )){
                    throw new MatchError( var14 );
                }

                    FabricPlanner.PlannerContext.PartialState var10004 = new FabricPlanner.PlannerContext.PartialState( this, query );
                    org.neo4j.cypher.CypherRuntimeOption.slotted.x$3 = org.neo4j.cypher.CypherRuntimeOption.slotted..MODULE$;
                    org.neo4j.cypher.CypherExpressionEngineOption.interpreted.x$4 = org.neo4j.cypher.CypherExpressionEngineOption.interpreted..MODULE$;
                    boolean x$5 = true;
                    InputPosition x$6 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$1(); boolean x$7 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$2(); CypherVersion x$8 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$3(); CypherExecutionMode x$9 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$4(); CypherPlannerOption x$10 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$5(); CypherUpdateStrategy x$11 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$7(); CypherOperatorEngineOption x$12 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$9(); CypherInterpretedPipesFallbackOption x$13 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$10(); Set x$14 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$11(); boolean x$15 = org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy$default$12(); var3 =
                            new FabricQuery.LocalQuery( new FullyParsedQuery( var10004, org.neo4j.cypher.internal.QueryOptions..MODULE$.
                    default
                        ().copy( x$6, x$7, x$8, x$9, x$10, x$3, x$11, x$4, x$12, x$13, x$14, x$15, x$5 )),var9.columns(), queryType);
                }

                var2 = (FabricQuery) Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps(
                        Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps(
                                Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps(
                                        Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps(
                                                Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps( var3 ) ).bottomUp(
                                                        new Serializable( this )
                                                        {
                                                            public static final long serialVersionUID = 0L;

                                                            public
                                                            {
                                                                if ( $outer == null )
                                                                {
                                                                    throw null;
                                                                }
                                                                else
                                                                {
                                                                    this.$outer = $outer;
                                                                }
                                                            }

                                                            public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                                                            {
                                                                Object var3;
                                                                if ( x1 instanceof FabricQuery.LocalQuery )
                                                                {
                                                                    FabricQuery.LocalQuery var5 = (FabricQuery.LocalQuery) x1;
                                                                    if ( var5.input().nonEmpty() )
                                                                    {
                                                                        var3 = Rewritten$RewritingOps$.MODULE$.rewritten$extension(
                                                                                Rewritten$.MODULE$.RewritingOps( var5 ) ).bottomUp(
                                                                                new Serializable( this, var5 )
                                                                                {
                                                                                    public static final long serialVersionUID = 0L;
                                                                                    private final FabricQuery.LocalQuery x2$1;

                                                                                    public
                                                                                    {
                                                                                        if ( $outer == null )
                                                                                        {
                                                                                            throw null;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            this.$outer = $outer;
                                                                                            this.x2$1 = x2$1;
                                                                                        }
                                                                                    }

                                                                                    public final <A1, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
                                                                                    {
                                                                                        Object var3;
                                                                                        if ( x2 instanceof SingleQuery )
                                                                                        {
                                                                                            SingleQuery var5 = (SingleQuery) x2;
                                                                                            var3 =
                                                                                                    this.$outer.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$anonfun$$$outer().com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$SingleQueryRewrites(
                                                                                                            var5 ).withInputDataStream( this.x2$1.input() );
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            var3 = var2.apply( x2 );
                                                                                        }

                                                                                        return var3;
                                                                                    }

                                                                                    public final boolean isDefinedAt( final Object x2 )
                                                                                    {
                                                                                        boolean var2;
                                                                                        if ( x2 instanceof SingleQuery )
                                                                                        {
                                                                                            var2 = true;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            var2 = false;
                                                                                        }

                                                                                        return var2;
                                                                                    }
                                                                                } ); return var3;
                                                                    }
                                                                }

                                                                var3 = var2.apply( x1 );
                                                                return var3;
                                                            }

                                                            public final boolean isDefinedAt( final Object x1 )
                                                            {
                                                                boolean var2;
                                                                if ( x1 instanceof FabricQuery.LocalQuery )
                                                                {
                                                                    FabricQuery.LocalQuery var4 = (FabricQuery.LocalQuery) x1;
                                                                    if ( var4.input().nonEmpty() )
                                                                    {
                                                                        var2 = true;
                                                                        return var2;
                                                                    }
                                                                }

                                                                var2 = false;
                                                                return var2;
                                                            }
                                                        } ) ) ).bottomUp( new Serializable( this )
                                        {
                                            public static final long serialVersionUID = 0L;

                                            public
                                            {
                                                if ( $outer == null )
                                                {
                                                    throw null;
                                                }
                                                else
                                                {
                                                    this.$outer = $outer;
                                                }
                                            }

                                            public final <A1, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
                                            {
                                                Object var3;
                                                if ( x3 instanceof FabricQuery.RemoteQuery )
                                                {
                                                    FabricQuery.RemoteQuery var5 = (FabricQuery.RemoteQuery) x3;
                                                    if ( var5.parameters().nonEmpty() )
                                                    {
                                                        var3 = Rewritten$RewritingOps$.MODULE$.rewritten$extension(
                                                                Rewritten$.MODULE$.RewritingOps( var5 ) ).bottomUp( new Serializable( this, var5 )
                                                        {
                                                            public static final long serialVersionUID = 0L;
                                                            private final FabricQuery.RemoteQuery x2$2;

                                                            public
                                                            {
                                                                if ( $outer == null )
                                                                {
                                                                    throw null;
                                                                }
                                                                else
                                                                {
                                                                    this.$outer = $outer;
                                                                    this.x2$2 = x2$2;
                                                                }
                                                            }

                                                            public final <A1, B1> B1 applyOrElse( final A1 x4, final Function1<A1,B1> default )
                                                            {
                                                                Object var3;
                                                                if ( x4 instanceof SingleQuery )
                                                                {
                                                                    SingleQuery var5 = (SingleQuery) x4;
                                                                    var3 =
                                                                            this.$outer.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$anonfun$$$outer().com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$SingleQueryRewrites(
                                                                                    var5 ).withParamBindings( this.x2$2.parameters() );
                                                                }
                                                                else
                                                                {
                                                                    var3 = var2.apply( x4 );
                                                                }

                                                                return var3;
                                                            }

                                                            public final boolean isDefinedAt( final Object x4 )
                                                            {
                                                                boolean var2;
                                                                if ( x4 instanceof SingleQuery )
                                                                {
                                                                    var2 = true;
                                                                }
                                                                else
                                                                {
                                                                    var2 = false;
                                                                }

                                                                return var2;
                                                            }
                                                        } ); return var3;
                                                    }
                                                }

                                                var3 = var2.apply( x3 );
                                                return var3;
                                            }

                                            public final boolean isDefinedAt( final Object x3 )
                                            {
                                                boolean var2;
                                                if ( x3 instanceof FabricQuery.RemoteQuery )
                                                {
                                                    FabricQuery.RemoteQuery var4 = (FabricQuery.RemoteQuery) x3;
                                                    if ( var4.parameters().nonEmpty() )
                                                    {
                                                        var2 = true;
                                                        return var2;
                                                    }
                                                }

                                                var2 = false;
                                                return var2;
                                            }
                                        } ) ) ).bottomUp( new Serializable( this )
                                {
                                    public static final long serialVersionUID = 0L;

                                    public
                                    {
                                        if ( $outer == null )
                                        {
                                            throw null;
                                        }
                                        else
                                        {
                                            this.$outer = $outer;
                                        }
                                    }

                                    public final <A1, B1> B1 applyOrElse( final A1 x5, final Function1<A1,B1> default )
                                    {
                                        Object var3;
                                        if ( x5 instanceof FabricQuery.LeafQuery )
                                        {
                                            FabricQuery.LeafQuery var5 = (FabricQuery.LeafQuery) x5;
                                            if ( var5.columns().output().nonEmpty() )
                                            {
                                                var3 = Rewritten$RewritingOps$.MODULE$.rewritten$extension( Rewritten$.MODULE$.RewritingOps( var5 ) ).bottomUp(
                                                        new Serializable( this, var5 )
                                                        {
                                                            public static final long serialVersionUID = 0L;
                                                            private final FabricQuery.LeafQuery x2$3;

                                                            public
                                                            {
                                                                if ( $outer == null )
                                                                {
                                                                    throw null;
                                                                }
                                                                else
                                                                {
                                                                    this.$outer = $outer;
                                                                    this.x2$3 = x2$3;
                                                                }
                                                            }

                                                            public final <A1, B1> B1 applyOrElse( final A1 x6, final Function1<A1,B1> default )
                                                            {
                                                                Object var3;
                                                                if ( x6 instanceof SingleQuery )
                                                                {
                                                                    SingleQuery var6 = (SingleQuery) x6;
                                                                    Clause var7 = (Clause) var6.clauses().last();
                                                                    SingleQuery var4;
                                                                    if ( var7 instanceof Return )
                                                                    {
                                                                        var4 = var6;
                                                                    }
                                                                    else
                                                                    {
                                                                        var4 =
                                                                                this.$outer.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$anonfun$$$outer().com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$SingleQueryRewrites(
                                                                                        var6 ).withReturnAliased( this.x2$3.columns().output() );
                                                                    }

                                                                    var3 = var4;
                                                                }
                                                                else
                                                                {
                                                                    var3 = var2.apply( x6 );
                                                                }

                                                                return var3;
                                                            }

                                                            public final boolean isDefinedAt( final Object x6 )
                                                            {
                                                                boolean var2;
                                                                if ( x6 instanceof SingleQuery )
                                                                {
                                                                    var2 = true;
                                                                }
                                                                else
                                                                {
                                                                    var2 = false;
                                                                }

                                                                return var2;
                                                            }
                                                        } ); return var3;
                                            }
                                        }

                                        var3 = var2.apply( x5 );
                                        return var3;
                                    }

                                    public final boolean isDefinedAt( final Object x5 )
                                    {
                                        boolean var2;
                                        if ( x5 instanceof FabricQuery.LeafQuery )
                                        {
                                            FabricQuery.LeafQuery var4 = (FabricQuery.LeafQuery) x5;
                                            if ( var4.columns().output().nonEmpty() )
                                            {
                                                var2 = true;
                                                return var2;
                                            }
                                        }

                                        var2 = false;
                                        return var2;
                                    }
                                } ) ) ).bottomUp( new Serializable( (FabricPlanner.PlannerContext) null )
                        {
                            public static final long serialVersionUID = 0L;

                            public final <A1, B1> B1 applyOrElse( final A1 x7, final Function1<A1,B1> default )
                            {
                                Object var3;
                                if ( x7 instanceof ResolvedCall )
                                {
                                    ResolvedCall var5 = (ResolvedCall) x7;
                                    InputPosition pos = var5.position();
                                    QualifiedName name = var5.signature().name();
                                    var3 = new UnresolvedCall( new Namespace( name.namespace().toList(), pos ), new ProcedureName( name.name(), pos ),
                                            (Option) (var5.declaredArguments() ? new Some( var5.callArguments() ) : scala.None..MODULE$ ), (Option) (
                                            var5.declaredResults() ? new Some( new ProcedureResult( var5.callResults(),
                                                    org.neo4j.cypher.internal.v4_0.ast.ProcedureResult..MODULE$.apply$default$2(), pos )) :scala.None..MODULE$),
                                    pos);
                                }
                                else if ( x7 instanceof ResolvedFunctionInvocation )
                                {
                                    ResolvedFunctionInvocation var8 = (ResolvedFunctionInvocation) x7;
                                    InputPosition posx = var8.position();
                                    QualifiedName namex = var8.qualifiedName();
                                    var3 = new FunctionInvocation( new Namespace( namex.namespace().toList(), posx ), new FunctionName( namex.name(), posx ),
                                            false, var8.arguments().toIndexedSeq(), posx );
                                }
                                else
                                {
                                    var3 = var2.apply( x7 );
                                }

                                return var3;
                            }

                            public final boolean isDefinedAt( final Object x7 )
                            {
                                boolean var2;
                                if ( x7 instanceof ResolvedCall )
                                {
                                    var2 = true;
                                }
                                else if ( x7 instanceof ResolvedFunctionInvocation )
                                {
                                    var2 = true;
                                }
                                else
                                {
                                    var2 = false;
                                }

                                return var2;
                            }
                        } ) ) ).bottomUp( new Serializable( this )
                {
                    public static final long serialVersionUID = 0L;

                    public
                    {
                        if ( $outer == null )
                        {
                            throw null;
                        }
                        else
                        {
                            this.$outer = $outer;
                        }
                    }

                    public final <A1, B1> B1 applyOrElse( final A1 x8, final Function1<A1,B1> default )
                    {
                        Object var3;
                        if ( x8 instanceof FabricPlanner.PlannerContext.PartialState &&
                                ((FabricPlanner.PlannerContext.PartialState) x8).com$neo4j$fabric$planning$FabricPlanner$PlannerContext$PartialState$$$outer() ==
                                        this.$outer )
                        {
                            FabricPlanner.PlannerContext.PartialState var5 = (FabricPlanner.PlannerContext.PartialState) x8;
                            org.neo4j.cypher.internal.v4_0.ast.Statement stmt = var5.stmt();
                            var3 = this.$outer.pipeline().checkAndFinalize().process( stmt );
                        }
                        else
                        {
                            var3 = var2.apply( x8 );
                        }

                        return var3;
                    }

                    public final boolean isDefinedAt( final Object x8 )
                    {
                        boolean var2;
                        if ( x8 instanceof FabricPlanner.PlannerContext.PartialState &&
                                ((FabricPlanner.PlannerContext.PartialState) x8).com$neo4j$fabric$planning$FabricPlanner$PlannerContext$PartialState$$$outer() ==
                                        this.$outer )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        return var2;
                    }
                } );
            }

            return (FabricQuery) var2;
        }

        public FabricPlanner.PlannerContext.SingleQueryRewrites com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$SingleQueryRewrites(
                final SingleQuery sq )
        {
            return new FabricPlanner.PlannerContext.SingleQueryRewrites( this, sq );
        }

        public FabricPlanner.PlannerContext copy( final String queryString, final org.neo4j.cypher.internal.v4_0.ast.Statement queryStatement,
                final MapValue parameters, final SemanticState semantics, final QueryOptions options, final Catalog catalog, final Pipeline.Instance pipeline )
        {
            return this.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$$outer().new PlannerContext(
                    this.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$$outer(), queryString, queryStatement, parameters, semantics, options, catalog,
                    pipeline );
        }

        public String copy$default$1()
        {
            return this.queryString();
        }

        public org.neo4j.cypher.internal.v4_0.ast.Statement copy$default$2()
        {
            return this.queryStatement();
        }

        public MapValue copy$default$3()
        {
            return this.parameters();
        }

        public SemanticState copy$default$4()
        {
            return this.semantics();
        }

        public QueryOptions copy$default$5()
        {
            return this.options();
        }

        public Catalog copy$default$6()
        {
            return this.catalog();
        }

        public Pipeline.Instance copy$default$7()
        {
            return this.pipeline();
        }

        public String productPrefix()
        {
            return "PlannerContext";
        }

        public int productArity()
        {
            return 7;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.queryString();
                break;
            case 1:
                var10000 = this.queryStatement();
                break;
            case 2:
                var10000 = this.parameters();
                break;
            case 3:
                var10000 = this.semantics();
                break;
            case 4:
                var10000 = this.options();
                break;
            case 5:
                var10000 = this.catalog();
                break;
            case 6:
                var10000 = this.pipeline();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof FabricPlanner.PlannerContext;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var18;
            if ( this != x$1 )
            {
                label113:
                {
                    boolean var2;
                    if ( x$1 instanceof FabricPlanner.PlannerContext &&
                            ((FabricPlanner.PlannerContext) x$1).com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$$outer() ==
                                    this.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label89:
                        {
                            label103:
                            {
                                FabricPlanner.PlannerContext var4 = (FabricPlanner.PlannerContext) x$1;
                                String var10000 = this.queryString();
                                String var5 = var4.queryString();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label103;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label103;
                                }

                                org.neo4j.cypher.internal.v4_0.ast.Statement var12 = this.queryStatement();
                                org.neo4j.cypher.internal.v4_0.ast.Statement var6 = var4.queryStatement();
                                if ( var12 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label103;
                                    }
                                }
                                else if ( !var12.equals( var6 ) )
                                {
                                    break label103;
                                }

                                MapValue var13 = this.parameters();
                                MapValue var7 = var4.parameters();
                                if ( var13 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label103;
                                    }
                                }
                                else if ( !var13.equals( var7 ) )
                                {
                                    break label103;
                                }

                                SemanticState var14 = this.semantics();
                                SemanticState var8 = var4.semantics();
                                if ( var14 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label103;
                                    }
                                }
                                else if ( !var14.equals( var8 ) )
                                {
                                    break label103;
                                }

                                QueryOptions var15 = this.options();
                                QueryOptions var9 = var4.options();
                                if ( var15 == null )
                                {
                                    if ( var9 != null )
                                    {
                                        break label103;
                                    }
                                }
                                else if ( !var15.equals( var9 ) )
                                {
                                    break label103;
                                }

                                Catalog var16 = this.catalog();
                                Catalog var10 = var4.catalog();
                                if ( var16 == null )
                                {
                                    if ( var10 != null )
                                    {
                                        break label103;
                                    }
                                }
                                else if ( !var16.equals( var10 ) )
                                {
                                    break label103;
                                }

                                Pipeline.Instance var17 = this.pipeline();
                                Pipeline.Instance var11 = var4.pipeline();
                                if ( var17 == null )
                                {
                                    if ( var11 != null )
                                    {
                                        break label103;
                                    }
                                }
                                else if ( !var17.equals( var11 ) )
                                {
                                    break label103;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var18 = true;
                                    break label89;
                                }
                            }

                            var18 = false;
                        }

                        if ( var18 )
                        {
                            break label113;
                        }
                    }

                    var18 = false;
                    return var18;
                }
            }

            var18 = true;
            return var18;
        }

        private final void PartialState$lzycompute$1()
        {
            synchronized ( this )
            {
            }

            try
            {
                if ( this.PartialState$module == null )
                {
                    this.PartialState$module = new FabricPlanner$PlannerContext$PartialState$( this );
                }
            }
            catch ( Throwable var3 )
            {
                throw var3;
            }
        }

        public class PartialState implements BaseState, Product, Serializable
        {
final String what
            private final org.neo4j.cypher.internal.v4_0.ast.Statement stmt;

            {
                return BaseState.fail$( this, what );
            }

            public PartialState( final FabricPlanner.PlannerContext $outer, final org.neo4j.cypher.internal.v4_0.ast.Statement stmt )
            {
                this.stmt = stmt;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    BaseState.$init$( this );
                    Product.$init$( this );
                }
            }

            public org.neo4j.cypher.internal.v4_0.ast.Statement statement()
            {
                return BaseState.statement$( this );
            }

            public Seq<String> returnColumns()
            {
                return BaseState.returnColumns$( this );
            }

         public scala.runtime.Nothing.fail(

            public SemanticState semantics()
            {
                return BaseState.semantics$( this );
            })

            public Map<String,Object> extractedParams()
            {
                return BaseState.extractedParams$( this );
            }

            public SemanticTable semanticTable()
            {
                return BaseState.semanticTable$( this );
            }

            public org.neo4j.cypher.internal.v4_0.ast.Statement stmt()
            {
                return this.stmt;
            }

            public Option<org.neo4j.cypher.internal.v4_0.ast.Statement> maybeStatement()
            {
                return new Some( this.stmt() );
            }

            public String queryText()
            {
                throw this.fail( "queryText" );
            }

            public Option<InputPosition> startPosition()
            {
                throw this.fail( "startPosition" );
            }

            public Map<String,CypherType> initialFields()
            {
                throw this.fail( "initialFields" );
            }

            public Option<SemanticState> maybeSemantics()
            {
                throw this.fail( "maybeSemantics" );
            }

            public Option<Map<String,Object>> maybeExtractedParams()
            {
                throw this.fail( "maybeExtractedParams" );
            }

            public Option<SemanticTable> maybeSemanticTable()
            {
                throw this.fail( "maybeSemanticTable" );
            }

            public Option<Seq<String>> maybeReturnColumns()
            {
                throw this.fail( "maybeReturnColumns" );
            }

            public Set<Condition> accumulatedConditions()
            {
                throw this.fail( "accumulatedConditions" );
            }

            public PlannerName plannerName()
            {
                throw this.fail( "plannerName" );
            }

            public BaseState withStatement( final org.neo4j.cypher.internal.v4_0.ast.Statement s )
            {
                throw this.fail( "withStatement" );
            }

            public BaseState withSemanticTable( final SemanticTable s )
            {
                throw this.fail( "withSemanticTable" );
            }

            public BaseState withSemanticState( final SemanticState s )
            {
                throw this.fail( "withSemanticState" );
            }

            public BaseState withReturnColumns( final Seq<String> cols )
            {
                throw this.fail( "withReturnColumns" );
            }

            public BaseState withParams( final Map<String,Object> p )
            {
                throw this.fail( "withParams" );
            }

            public FabricPlanner.PlannerContext.PartialState copy( final org.neo4j.cypher.internal.v4_0.ast.Statement stmt )
            {
                return this.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$PartialState$$$outer().new PartialState(
                        this.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$PartialState$$$outer(), stmt );
            }

            public org.neo4j.cypher.internal.v4_0.ast.Statement copy$default$1()
            {
                return this.stmt();
            }

            public String productPrefix()
            {
                return "PartialState";
            }

            public int productArity()
            {
                return 1;
            }

            public Object productElement( final int x$1 )
            {
                switch ( x$1 )
                {
                case 0:
                    return this.stmt();
                default:
                    throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
                }
            }

            public Iterator<Object> productIterator()
            {
                return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
            }

            public boolean canEqual( final Object x$1 )
            {
                return x$1 instanceof FabricPlanner.PlannerContext.PartialState;
            }

            public int hashCode()
            {
                return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
            }

            public String toString()
            {
                return scala.runtime.ScalaRunTime..MODULE$._toString( this );
            }

            public boolean equals( final Object x$1 )
            {
                boolean var6;
                if ( this != x$1 )
                {
                    label58:
                    {
                        boolean var2;
                        if ( x$1 instanceof FabricPlanner.PlannerContext.PartialState &&
                                ((FabricPlanner.PlannerContext.PartialState) x$1).com$neo4j$fabric$planning$FabricPlanner$PlannerContext$PartialState$$$outer() ==
                                        this.com$neo4j$fabric$planning$FabricPlanner$PlannerContext$PartialState$$$outer() )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        if ( var2 )
                        {
                            label35:
                            {
                                label34:
                                {
                                    FabricPlanner.PlannerContext.PartialState var4 = (FabricPlanner.PlannerContext.PartialState) x$1;
                                    org.neo4j.cypher.internal.v4_0.ast.Statement var10000 = this.stmt();
                                    org.neo4j.cypher.internal.v4_0.ast.Statement var5 = var4.stmt();
                                    if ( var10000 == null )
                                    {
                                        if ( var5 != null )
                                        {
                                            break label34;
                                        }
                                    }
                                    else if ( !var10000.equals( var5 ) )
                                    {
                                        break label34;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var6 = true;
                                        break label35;
                                    }
                                }

                                var6 = false;
                            }

                            if ( var6 )
                            {
                                break label58;
                            }
                        }

                        var6 = false;
                        return var6;
                    }
                }

                var6 = true;
                return var6;
            }
        }

        public class SingleQueryRewrites
        {
            private final SingleQuery sq;
            private final InputPosition pos;

            public SingleQueryRewrites( final FabricPlanner.PlannerContext $outer, final SingleQuery sq )
            {
                this.sq = sq;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    this.pos = org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE();
                }
            }

            private InputPosition pos()
            {
                return this.pos;
            }

            public SingleQuery append( final Clause clause )
            {
                return this.sq.copy( (Seq) this.sq.clauses().$colon$plus( clause, scala.collection.Seq..MODULE$.canBuildFrom() ),this.sq.position());
            }

            public SingleQuery prepend( final Clause clause )
            {
                return this.sq.copy( (Seq) this.sq.clauses().$plus$colon( clause, scala.collection.Seq..MODULE$.canBuildFrom() ),this.sq.position());
            }

            public SingleQuery withReturnNone()
            {
                return this.append( org.neo4j.cypher.internal.v4_0.ast.Return..MODULE$.apply(
                        new ReturnItems( false, (Seq) scala.collection.Seq..MODULE$.apply( scala.collection.immutable.Nil..MODULE$ ),this.pos()),this.pos()));
            }

            public SingleQuery withReturnAliased( final Seq<String> names )
            {
                return this.append( org.neo4j.cypher.internal.v4_0.ast.Return..MODULE$.apply( new ReturnItems( false, (Seq) names.map( ( n ) -> {
                    return new AliasedReturnItem( new Variable( n, this.pos() ), new Variable( n, this.pos() ), this.pos() );
                }, scala.collection.Seq..MODULE$.canBuildFrom() ), this.pos() ),this.pos()));
            }

            public SingleQuery withParamBindings( final Map<String,String> bindings )
            {
                Seq items = (Seq) bindings.toSeq().withFilter( ( check$ifrefutable$1 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$withParamBindings$1( check$ifrefutable$1 ) );
                } ).map( ( x$2 ) -> {
                    if ( x$2 != null )
                    {
                        String varName = (String) x$2._1();
                        String parName = (String) x$2._2();
                        AliasedReturnItem var2 = new AliasedReturnItem( new Parameter( parName, org.neo4j.cypher.internal.v4_0.util.symbols.package..
                        MODULE$.CTAny(), this.pos()),new Variable( varName, this.pos() ), this.pos());
                        return var2;
                    }
                    else
                    {
                        throw new MatchError( x$2 );
                    }
                }, scala.collection.Seq..MODULE$.canBuildFrom());
                return this.prepend( org.neo4j.cypher.internal.v4_0.ast.With..MODULE$.apply( new ReturnItems( false, items, this.pos() ), this.pos() ));
            }

            public SingleQuery withInputDataStream( final Seq<String> names )
            {
                return this.prepend( new InputDataStream( (Seq) names.map( ( name ) -> {
                    return new Variable( name, this.pos() );
                }, scala.collection.Seq..MODULE$.canBuildFrom() ), this.pos() ));
            }
        }
    }
}
