package com.neo4j.fabric.planning;

import com.neo4j.fabric.util.Folded;
import com.neo4j.fabric.util.Folded$;
import org.neo4j.cypher.internal.v4_0.ast.CallClause;
import org.neo4j.cypher.internal.v4_0.ast.Query;
import org.neo4j.cypher.internal.v4_0.ast.UnresolvedCall;
import org.neo4j.cypher.internal.v4_0.ast.UpdateClause;
import scala.Function1;
import scala.Serializable;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.runtime.BoxesRunTime;

public final class QueryType$
{
    public static QueryType$ MODULE$;

    static
    {
        new QueryType$();
    }

    private final QueryType READ;
    private final QueryType READ_PLUS_UNRESOLVED;
    private final QueryType WRITE;

    private QueryType$()
    {
        MODULE$ = this;
        this.READ = QueryType$Read$.MODULE$;
        this.READ_PLUS_UNRESOLVED = QueryType$ReadPlusUnresolved$.MODULE$;
        this.WRITE = QueryType$Write$.MODULE$;
    }

    public QueryType READ()
    {
        return this.READ;
    }

    public QueryType READ_PLUS_UNRESOLVED()
    {
        return this.READ_PLUS_UNRESOLVED;
    }

    public QueryType WRITE()
    {
        return this.WRITE;
    }

    public QueryType of( final Query query )
    {
        return (QueryType) Folded$.MODULE$.FoldableOps( query ).folded( QueryType$Read$.MODULE$, ( a, b ) -> {
            return MODULE$.merge( a, b );
        }, new Serializable()
        {
            public static final long serialVersionUID = 0L;

            public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x1 instanceof UpdateClause )
                {
                    var3 = new Folded.Stop( QueryType$Write$.MODULE$ );
                }
                else if ( x1 instanceof UnresolvedCall )
                {
                    var3 = new Folded.Stop( QueryType$ReadPlusUnresolved$.MODULE$ );
                }
                else if ( x1 instanceof CallClause )
                {
                    CallClause var5 = (CallClause) x1;
                    var3 = new Folded.Stop( var5.containsNoUpdates() ? QueryType$Read$.MODULE$ : QueryType$Write$.MODULE$ );
                }
                else
                {
                    var3 = var2.apply( x1 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final Object x1 )
            {
                boolean var2;
                if ( x1 instanceof UpdateClause )
                {
                    var2 = true;
                }
                else if ( x1 instanceof UnresolvedCall )
                {
                    var2 = true;
                }
                else if ( x1 instanceof CallClause )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        } );
    }

    public QueryType of( final FabricQuery query )
    {
        QueryType var2;
        if ( query instanceof FabricQuery.LeafQuery )
        {
            FabricQuery.LeafQuery var4 = (FabricQuery.LeafQuery) query;
            var2 = var4.queryType();
        }
        else
        {
            var2 = (QueryType) ((TraversableOnce) query.children().map( ( queryx ) -> {
                return MODULE$.of( queryx );
            },.MODULE$.canBuildFrom())).fold( QueryType$Read$.MODULE$, ( a, b ) -> {
            return MODULE$.merge( a, b );
        } );
        }

        return var2;
    }

    public QueryType merge( final QueryType a, final QueryType b )
    {
        return (QueryType) ((TraversableOnce).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new QueryType[]{a, b}) ))).maxBy( ( x0$1 ) -> {
        return BoxesRunTime.boxToInteger( $anonfun$merge$1( x0$1 ) );
    }, scala.math.Ordering.Int..MODULE$);
    }
}
