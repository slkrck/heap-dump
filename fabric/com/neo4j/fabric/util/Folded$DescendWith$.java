package com.neo4j.fabric.util;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;

public class Folded$DescendWith$ implements Serializable
{
    public static Folded$DescendWith$ MODULE$;

    static
    {
        new Folded$DescendWith$();
    }

    public Folded$DescendWith$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "DescendWith";
    }

    public <R> Folded.DescendWith<R> apply( final R r1, final R r2 )
    {
        return new Folded.DescendWith( r1, r2 );
    }

    public <R> Option<Tuple2<R,R>> unapply( final Folded.DescendWith<R> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.r1(), x$0.r2() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
