package com.neo4j.fabric.util;

import scala.runtime.BoxesRunTime;

public class Rewritten$RewritingOps$
{
    public static Rewritten$RewritingOps$ MODULE$;

    static
    {
        new Rewritten$RewritingOps$();
    }

    public Rewritten$RewritingOps$()
    {
        MODULE$ = this;
    }

    public final <T> Rewritten.Rewritten<T> rewritten$extension( final T $this )
    {
        return new Rewritten.Rewritten( $this, Rewritten$Rewritten$.MODULE$.apply$default$2() );
    }

    public final <T> int hashCode$extension( final T $this )
    {
        return $this.hashCode();
    }

    public final <T> boolean equals$extension( final T $this, final Object x$1 )
    {
        boolean var3;
        if ( x$1 instanceof Rewritten.RewritingOps )
        {
            var3 = true;
        }
        else
        {
            var3 = false;
        }

        boolean var10000;
        if ( var3 )
        {
            Object var5 = x$1 == null ? null : ((Rewritten.RewritingOps) x$1).that();
            if ( BoxesRunTime.equals( $this, var5 ) )
            {
                var10000 = true;
                return var10000;
            }
        }

        var10000 = false;
        return var10000;
    }
}
