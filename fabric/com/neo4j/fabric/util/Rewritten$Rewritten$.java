package com.neo4j.fabric.util;

import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;

public class Rewritten$Rewritten$ implements Serializable
{
    public static Rewritten$Rewritten$ MODULE$;

    static
    {
        new Rewritten$Rewritten$();
    }

    public Rewritten$Rewritten$()
    {
        MODULE$ = this;
    }

    public <T> Function1<Object,Object> $lessinit$greater$default$2()
    {
        return Rewritten$.MODULE$.com$neo4j$fabric$util$Rewritten$$never();
    }

    public final String toString()
    {
        return "Rewritten";
    }

    public <T> Rewritten.Rewritten<T> apply( final T that, final Function1<Object,Object> stopper )
    {
        return new Rewritten.Rewritten( that, stopper );
    }

    public <T> Function1<Object,Object> apply$default$2()
    {
        return Rewritten$.MODULE$.com$neo4j$fabric$util$Rewritten$$never();
    }

    public <T> Option<Tuple2<T,Function1<Object,Object>>> unapply( final Rewritten.Rewritten<T> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.that(), x$0.stopper() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
