package com.neo4j.fabric.util;

import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public final class Folded
{
    public static <T> Folded.FoldableOps<T> FoldableOps( final T f )
    {
        return Folded$.MODULE$.FoldableOps( var0 );
    }

    public interface Instruction<R>
    {
    }

    public static class Descend<R> implements Folded.Instruction<R>, Product, Serializable
    {
        private final R r;

        public Descend( final R r )
        {
            this.r = r;
            Product.$init$( this );
        }

        public R r()
        {
            return this.r;
        }

        public <R> Folded.Descend<R> copy( final R r )
        {
            return new Folded.Descend( r );
        }

        public <R> R copy$default$1()
        {
            return this.r();
        }

        public String productPrefix()
        {
            return "Descend";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.r();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Folded.Descend;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label49:
                {
                    boolean var2;
                    if ( x$1 instanceof Folded.Descend )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        Folded.Descend var4 = (Folded.Descend) x$1;
                        if ( BoxesRunTime.equals( this.r(), var4.r() ) && var4.canEqual( this ) )
                        {
                            break label49;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class DescendWith<R> implements Folded.Instruction<R>, Product, Serializable
    {
        private final R r1;
        private final R r2;

        public DescendWith( final R r1, final R r2 )
        {
            this.r1 = r1;
            this.r2 = r2;
            Product.$init$( this );
        }

        public R r1()
        {
            return this.r1;
        }

        public R r2()
        {
            return this.r2;
        }

        public <R> Folded.DescendWith<R> copy( final R r1, final R r2 )
        {
            return new Folded.DescendWith( r1, r2 );
        }

        public <R> R copy$default$1()
        {
            return this.r1();
        }

        public <R> R copy$default$2()
        {
            return this.r2();
        }

        public String productPrefix()
        {
            return "DescendWith";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.r1();
                break;
            case 1:
                var10000 = this.r2();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Folded.DescendWith;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label51:
                {
                    boolean var2;
                    if ( x$1 instanceof Folded.DescendWith )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        Folded.DescendWith var4 = (Folded.DescendWith) x$1;
                        if ( BoxesRunTime.equals( this.r1(), var4.r1() ) && BoxesRunTime.equals( this.r2(), var4.r2() ) && var4.canEqual( this ) )
                        {
                            break label51;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class FoldableOps<T>
    {
        private final T f;

        public FoldableOps( final T f )
        {
            this.f = f;
        }

        public <R> R folded( final R init, final Function2<R,R,R> merge, final PartialFunction<Object,Folded.Instruction<R>> instructions )
        {
            return org.neo4j.cypher.internal.v4_0.util.Foldable.FoldableAny..
            MODULE$.treeFold$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( this.f ), init, new Serializable(
                    (Folded.FoldableOps) null, merge, instructions )
            {
                public static final long serialVersionUID = 0L;
                private final Function2 merge$1;
                private final PartialFunction instructions$1;

                public
                {
                    this.merge$1 = merge$1;
                    this.instructions$1 = instructions$1;
                }

                public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                {
                    Object var3;
                    if ( x1 instanceof Object && this.instructions$1.isDefinedAt( x1 ) )
                    {
                        var3 = ( r ) -> {
                            Folded.Instruction var4 = (Folded.Instruction) this.instructions$1.apply( x1 );
                            Tuple2 var3;
                            if ( var4 instanceof Folded.Stop )
                            {
                                Folded.Stop var5 = (Folded.Stop) var4;
                                var3 = new Tuple2( this.merge$1.apply( r, var5.r() ), scala.None..MODULE$);
                            }
                            else if ( var4 instanceof Folded.Descend )
                            {
                                Folded.Descend var6 = (Folded.Descend) var4;
                                var3 = new Tuple2( this.merge$1.apply( r, var6.r() ), new Some( ( x ) -> {
                                    return scala.Predef..MODULE$.identity( x );
                                } ) );
                            }
                            else
                            {
                                if ( !(var4 instanceof Folded.DescendWith) )
                                {
                                    throw new MatchError( var4 );
                                }

                                Folded.DescendWith var7 = (Folded.DescendWith) var4;
                                var3 = new Tuple2( this.merge$1.apply( r, var7.r1() ), new Some( ( rb ) -> {
                                    return this.merge$1.apply( rb, var7.r2() );
                                } ) );
                            }

                            return var3;
                        };
                    }
                    else
                    {
                        var3 = var2.apply( x1 );
                    }

                    return var3;
                }

                public final boolean isDefinedAt( final Object x1 )
                {
                    boolean var2;
                    if ( x1 instanceof Object && this.instructions$1.isDefinedAt( x1 ) )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    return var2;
                }
            });
        }
    }

    public static class Stop<R> implements Folded.Instruction<R>, Product, Serializable
    {
        private final R r;

        public Stop( final R r )
        {
            this.r = r;
            Product.$init$( this );
        }

        public R r()
        {
            return this.r;
        }

        public <R> Folded.Stop<R> copy( final R r )
        {
            return new Folded.Stop( r );
        }

        public <R> R copy$default$1()
        {
            return this.r();
        }

        public String productPrefix()
        {
            return "Stop";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.r();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Folded.Stop;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label49:
                {
                    boolean var2;
                    if ( x$1 instanceof Folded.Stop )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        Folded.Stop var4 = (Folded.Stop) x$1;
                        if ( BoxesRunTime.equals( this.r(), var4.r() ) && var4.canEqual( this ) )
                        {
                            break label49;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }
}
