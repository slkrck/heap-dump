package com.neo4j.fabric.util;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;

public class Folded$Descend$ implements Serializable
{
    public static Folded$Descend$ MODULE$;

    static
    {
        new Folded$Descend$();
    }

    public Folded$Descend$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Descend";
    }

    public <R> Folded.Descend<R> apply( final R r )
    {
        return new Folded.Descend( r );
    }

    public <R> Option<R> unapply( final Folded.Descend<R> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.r() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
