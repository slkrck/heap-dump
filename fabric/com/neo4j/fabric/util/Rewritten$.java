package com.neo4j.fabric.util;

import scala.Function1;
import scala.PartialFunction;
import scala.Serializable;
import scala.runtime.BoxesRunTime;

public final class Rewritten$
{
    public static Rewritten$ MODULE$;

    static
    {
        new Rewritten$();
    }

    private final PartialFunction<Object,Object> com$neo4j$fabric$util$Rewritten$$never;

    private Rewritten$()
    {
        MODULE$ = this;
        this.com$neo4j$fabric$util$Rewritten$$never = new Serializable()
        {
            public static final long serialVersionUID = 0L;

            public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                Boolean var3 = BoxesRunTime.boxToBoolean( false );
                return var3;
            }

            public final boolean isDefinedAt( final Object x1 )
            {
                boolean var2 = true;
                return var2;
            }
        };
    }

    public <T> T RewritingOps( final T that )
    {
        return that;
    }

    public PartialFunction<Object,Object> com$neo4j$fabric$util$Rewritten$$never()
    {
        return this.com$neo4j$fabric$util$Rewritten$$never;
    }
}
