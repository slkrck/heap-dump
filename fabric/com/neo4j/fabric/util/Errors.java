package com.neo4j.fabric.util;

import com.neo4j.fabric.eval.Catalog;
import org.neo4j.cypher.internal.v4_0.ast.CatalogDDL;
import org.neo4j.cypher.internal.v4_0.ast.CatalogName;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticError;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef;
import org.neo4j.cypher.internal.v4_0.util.ASTNode;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.values.AnyValue;
import scala.Function0;
import scala.Function1;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public final class Errors
{
    final String kind
    final String needle
    final String feature
    final CatalogDDL ddl
    final String message
    final String msg

   public static scala.runtime.Nothing.entityNotFound(
    final String query, final InputPosition pos)
    final int exp

   public static scala.runtime.Nothing.notSupported(
    final int got)
    final InputPosition pos

   public static scala.runtime.Nothing.ddlNotSupported(
    final String exp)
    final String got

   public static scala.runtime.Nothing.semantic(
    final String qualifiedName)
    final InputPosition pos

   public static scala.runtime.Nothing.syntax(
    final String exp, final ASTNode got, final String exp)
    final String got

   public static scala.runtime.Nothing.wrongArity(
    final String in, final InputPosition pos, final String exp)
    final String got

   public static scala.runtime.Nothing.wrongType(
    final InputPosition pos, final String exp)
    final InputPosition pos

   public static scala.runtime.Nothing.openCypherUnknownFunction(
    final SemanticErrorDef error, final Seq<SemanticErrorDef> errors)
    final SemanticErrorDef error

   public static scala.runtime.Nothing.openCypherUnexpected(
    final Seq<SemanticErrorDef> errors,

    {
        return Errors$.MODULE$.entityNotFound( var0, var1 );
    })

    {
        return Errors$.MODULE$.notSupported( var0 );
    }

   public static scala.runtime.Nothing.openCypherUnexpected(

    {
        return Errors$.MODULE$.ddlNotSupported( var0 );
    },

    {
        return Errors$.MODULE$.semantic( var0 );
    },

    {
        return Errors$.MODULE$.syntax( var0, var1, var2 );
    },

    {
        return Errors$.MODULE$.wrongArity( var0, var1, var2 );
    })

    {
        return Errors$.MODULE$.wrongType( var0, var1 );
    }

   public static scala.runtime.Nothing.openCypherUnexpected(

    {
        return Errors$.MODULE$.openCypherUnknownFunction( var0, var1 );
    },

    {
        return Errors$.MODULE$.openCypherUnexpected( var0, var1 );
    },

    {
        return Errors$.MODULE$.openCypherUnexpected( var0, var1, var2, var3 );
    })

    {
        return Errors$.MODULE$.openCypherUnexpected( var0, var1, var2 );
    }

   public static scala.runtime.Nothing.openCypherUnexpected(

    {
        return Errors$.MODULE$.openCypherUnexpected( var0, var1 );
    },

    {
        return Errors$.MODULE$.openCypherFailure( var0 );
    })

    {
        return Errors$.MODULE$.openCypherFailure( var0 );
    }

   public static scala.runtime.Nothing.openCypherFailure(

    {
        return Errors$.MODULE$.openCypherInvalid( var0 );
    })

    {
        return Errors$.MODULE$.openCypherInvalid( var0 );
    }

   public static scala.runtime.Nothing.openCypherFailure(

    public static String show( final Seq<?> seq )
    {
        return Errors$.MODULE$.show( var0 );
    })

    public static String show( final Catalog.Arg<?> a )
    {
        return Errors$.MODULE$.show( var0 );
    }

   public static scala.runtime.Nothing.openCypherInvalid(

    public static String show( final AnyValue av )
    {
        return Errors$.MODULE$.show( var0 );
    })

    public static String show( final CypherType t )
    {
        return Errors$.MODULE$.show( var0 );
    }

   public static scala.runtime.Nothing.openCypherInvalid(

    public static String show( final CatalogName n )
    {
        return Errors$.MODULE$.show( var0 );
    })

    public static <T> T errorContext( final String query, final ASTNode node, final Function0<T> block )
    {
        return Errors$.MODULE$.errorContext( var0, var1, var2 );
    }

    public static void openCypherInvalidOnError( final Seq<SemanticErrorDef> errors )
    {
        Errors$.MODULE$.openCypherInvalidOnError( var0 );
    }

    public static SemanticError openCypherSemantic( final String msg, final ASTNode node )
    {
        return Errors$.MODULE$.openCypherSemantic( var0, var1 );
    }

    public interface HasErrors
    {
        Errors.HasErrors update( final Function1<SemanticErrorDef,SemanticErrorDef> upd );
    }

    public static class EvaluationFailedException extends RuntimeException implements Errors.HasErrors, Product, Serializable
    {
        private final Seq<SemanticErrorDef> errors;

        public EvaluationFailedException( final Seq<SemanticErrorDef> errors )
        {
            super( (new StringBuilder( 18 )).append( "Evaluation failed\n" ).append( ((TraversableOnce) errors.map( new Serializable()
            {
                public static final long serialVersionUID = 0L;

                public final String apply( final SemanticErrorDef e )
                {
                    return (new StringBuilder( 8 )).append( "- " ).append( e.msg() ).append( " [at " ).append( e.position() ).append( "]" ).toString();
                }
            }, scala.collection.Seq..MODULE$.canBuildFrom()) ).mkString( "\n" ) ).toString());
            this.errors = errors;
            Product.$init$( this );
        }

        public Seq<SemanticErrorDef> errors()
        {
            return this.errors;
        }

        public Errors.EvaluationFailedException update( final Function1<SemanticErrorDef,SemanticErrorDef> upd )
        {
            return this.copy( (Seq) this.errors().map( upd, scala.collection.Seq..MODULE$.canBuildFrom() ));
        }

        public Errors.EvaluationFailedException copy( final Seq<SemanticErrorDef> errors )
        {
            return new Errors.EvaluationFailedException( errors );
        }

        public Seq<SemanticErrorDef> copy$default$1()
        {
            return this.errors();
        }

        public String productPrefix()
        {
            return "EvaluationFailedException";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.errors();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Errors.EvaluationFailedException;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            label47:
            {
                if ( this != x$1 )
                {
                    boolean var2;
                    if ( x$1 instanceof Errors.EvaluationFailedException )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( !var2 )
                    {
                        break label47;
                    }

                    label35:
                    {
                        label34:
                        {
                            Errors.EvaluationFailedException var4 = (Errors.EvaluationFailedException) x$1;
                            Seq var10000 = this.errors();
                            Seq var5 = var4.errors();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label34;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label34;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label35;
                            }
                        }

                        var6 = false;
                    }

                    if ( !var6 )
                    {
                        break label47;
                    }
                }

                var6 = true;
                return var6;
            }

            var6 = false;
            return var6;
        }
    }

    public static class InvalidQueryException extends RuntimeException implements Errors.HasErrors, Product, Serializable
    {
        private final Seq<SemanticErrorDef> errors;

        public InvalidQueryException( final Seq<SemanticErrorDef> errors )
        {
            super( (new StringBuilder( 14 )).append( "Invalid query\n" ).append( ((TraversableOnce) errors.map( new Serializable()
            {
                public static final long serialVersionUID = 0L;

                public final String apply( final SemanticErrorDef e )
                {
                    return (new StringBuilder( 8 )).append( "- " ).append( e.msg() ).append( " [at " ).append( e.position() ).append( "]" ).toString();
                }
            }, scala.collection.Seq..MODULE$.canBuildFrom()) ).mkString( "\n" ) ).toString());
            this.errors = errors;
            Product.$init$( this );
        }

        public Seq<SemanticErrorDef> errors()
        {
            return this.errors;
        }

        public Errors.InvalidQueryException update( final Function1<SemanticErrorDef,SemanticErrorDef> upd )
        {
            return this.copy( (Seq) this.errors().map( upd, scala.collection.Seq..MODULE$.canBuildFrom() ));
        }

        public Errors.InvalidQueryException copy( final Seq<SemanticErrorDef> errors )
        {
            return new Errors.InvalidQueryException( errors );
        }

        public Seq<SemanticErrorDef> copy$default$1()
        {
            return this.errors();
        }

        public String productPrefix()
        {
            return "InvalidQueryException";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.errors();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof Errors.InvalidQueryException;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            label47:
            {
                if ( this != x$1 )
                {
                    boolean var2;
                    if ( x$1 instanceof Errors.InvalidQueryException )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( !var2 )
                    {
                        break label47;
                    }

                    label35:
                    {
                        label34:
                        {
                            Errors.InvalidQueryException var4 = (Errors.InvalidQueryException) x$1;
                            Seq var10000 = this.errors();
                            Seq var5 = var4.errors();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label34;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label34;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label35;
                            }
                        }

                        var6 = false;
                    }

                    if ( !var6 )
                    {
                        break label47;
                    }
                }

                var6 = true;
                return var6;
            }

            var6 = false;
            return var6;
        }
    }
}
