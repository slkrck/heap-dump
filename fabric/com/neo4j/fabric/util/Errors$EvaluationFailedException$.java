package com.neo4j.fabric.util;

import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction1;

public class Errors$EvaluationFailedException$ extends AbstractFunction1<Seq<SemanticErrorDef>,Errors.EvaluationFailedException> implements Serializable
{
    public static Errors$EvaluationFailedException$ MODULE$;

    static
    {
        new Errors$EvaluationFailedException$();
    }

    public Errors$EvaluationFailedException$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "EvaluationFailedException";
    }

    public Errors.EvaluationFailedException apply( final Seq<SemanticErrorDef> errors )
    {
        return new Errors.EvaluationFailedException( errors );
    }

    public Option<Seq<SemanticErrorDef>> unapply( final Errors.EvaluationFailedException x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.errors() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
