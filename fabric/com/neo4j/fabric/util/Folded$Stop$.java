package com.neo4j.fabric.util;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;

public class Folded$Stop$ implements Serializable
{
    public static Folded$Stop$ MODULE$;

    static
    {
        new Folded$Stop$();
    }

    public Folded$Stop$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Stop";
    }

    public <R> Folded.Stop<R> apply( final R r )
    {
        return new Folded.Stop( r );
    }

    public <R> Option<R> unapply( final Folded.Stop<R> x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.r() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
