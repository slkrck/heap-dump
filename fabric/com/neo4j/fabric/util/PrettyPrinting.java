package com.neo4j.fabric.util;

import scala.Function1;
import scala.MatchError;
import scala.Some;
import scala.Tuple2;
import scala.collection.IterableLike;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Stream;
import scala.collection.immutable.StringOps;
import scala.collection.immutable.Stream.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface PrettyPrinting<T> extends PrettyPrintingUtils
{
    private static Stream head$1( final String name )
    {
        return scala.package..MODULE$.Stream().apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new String[]{(new StringBuilder( 4 )).append( "[ " ).append( name ).append( " ]" ).toString()}) ));
    }

    private static Stream middle$1( final Seq fields )
    {
        int max = fields.nonEmpty() ? BoxesRunTime.unboxToInt( ((TraversableOnce) fields.map( ( x$1 ) -> {
            return BoxesRunTime.boxToInteger( $anonfun$node$1( x$1 ) );
        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).max( scala.math.Ordering.Int..MODULE$)) :0;
        return (Stream) fields.toStream().flatMap( ( x0$1 ) -> {
            Stream var2;
            if ( x0$1 != null )
            {
                String namex = (String) x0$1._1();
                Object vs = x0$1._2();
                if ( vs instanceof Stream )
                {
                    Stream var6 = (Stream) vs;
                    Stream text = (Stream) var6.map( ( e ) -> {
                        return (new StringBuilder( 4 )).append( "  ┊ " ).append( e.toString() ).toString();
                    },.MODULE$.canBuildFrom());
                    var2 = (Stream) scala.package..MODULE$.Stream().apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new String[]{(new StringBuilder( 3 )).append( "╞ " ).append( namex ).append( ":" ).toString()}) )).
                    $plus$plus( text,.MODULE$.canBuildFrom());
                    return var2;
                }
            }

            if ( x0$1 == null )
            {
                throw new MatchError( x0$1 );
            }
            else
            {
                String name = (String) x0$1._1();
                Object value = x0$1._2();
                String space = (new StringOps( scala.Predef..MODULE$.augmentString( " " ))).$times( max - name.length() );
                var2 = scala.package..MODULE$.Stream().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{
                    (new StringBuilder( 4 )).append( "╞ " ).append( name ).append( space ).append( ": " ).append( value ).toString()}) ));
                return var2;
            }
        },.MODULE$.canBuildFrom());
    }

    private static Stream framing$1( final Stream in, final String first, final String rest )
    {
        String head = (new StringBuilder( 0 )).append( first ).append( in.head() ).toString();
        Stream tail = (Stream) ((Stream) in.tail()).map( ( x$2 ) -> {
            return (new StringBuilder( 0 )).append( rest ).append( x$2 ).toString();
        },.MODULE$.canBuildFrom());
        return .MODULE$.consWrapper( () -> {
        return tail;
    } ).$hash$colon$colon( head );
    }

    static void $init$( final PrettyPrinting $this )
    {
    }

    Function1<T,Stream<String>> pretty();

    default Stream<String> node( final String name, final Seq<Tuple2<String,Object>> fields, final Seq<T> children )
    {
        return (Stream) ((Stream) head$1( name ).$plus$plus( middle$1( fields ),.MODULE$.canBuildFrom())).
        $plus$plus( this.rest$1( children ),.MODULE$.canBuildFrom());
    }

    default Seq<T> node$default$3()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply( scala.collection.immutable.Nil..MODULE$);
    }

    default void pprint( final T t )
    {
        ((Stream) this.pretty().apply( t )).foreach( ( x ) -> {
            $anonfun$pprint$1( x );
            return BoxedUnit.UNIT;
        } );
    }

    default String asString( final T t )
    {
        return ((Stream) this.pretty().apply( t )).mkString( System.lineSeparator() );
    }

    private default Stream rest$1( final Seq cs )
    {
        Some var4 = scala.collection.Seq..MODULE$.unapplySeq( cs );
        Stream var2;
        if ( !var4.isEmpty() && var4.get() != null && ((SeqLike) var4.get()).lengthCompare( 0 ) == 0 )
        {
            var2 = scala.package..MODULE$.Stream().apply( scala.collection.immutable.Nil..MODULE$);
        }
        else
        {
            var2 = (Stream) ((Stream) ((IterableLike) cs.init()).toStream().flatMap( ( c ) -> {
                return framing$1( (Stream) this.pretty().apply( c ), "├─ ", "│    " );
            },.MODULE$.canBuildFrom())).$plus$plus( framing$1( (Stream) this.pretty().apply( cs.last() ), "└─ ", "     " ),.MODULE$.canBuildFrom());
        }

        return var2;
    }
}
