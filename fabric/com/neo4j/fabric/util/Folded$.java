package com.neo4j.fabric.util;

public final class Folded$
{
    public static Folded$ MODULE$;

    static
    {
        new Folded$();
    }

    private Folded$()
    {
        MODULE$ = this;
    }

    public <T> Folded.FoldableOps<T> FoldableOps( final T f )
    {
        return new Folded.FoldableOps( f );
    }
}
