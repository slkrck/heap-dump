package com.neo4j.fabric.util;

import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.Query;
import org.neo4j.cypher.internal.v4_0.ast.SingleQuery;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.ast.prettifier.ExpressionStringifier;
import org.neo4j.cypher.internal.v4_0.ast.prettifier.Prettifier;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import scala.Predef.;
import scala.collection.Seq;
import scala.collection.immutable.Stream;
import scala.collection.immutable.StringOps;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface PrettyPrintingUtils
{
    static void $init$( final PrettyPrintingUtils $this )
    {
        $this.com$neo4j$fabric$util$PrettyPrintingUtils$_setter_$com$neo4j$fabric$util$PrettyPrintingUtils$$printer_$eq(
                new Prettifier( new ExpressionStringifier( org.neo4j.cypher.internal.v4_0.ast.prettifier.ExpressionStringifier..MODULE$.apply$default$1(),
                        org.neo4j.cypher.internal.v4_0.ast.prettifier.ExpressionStringifier..MODULE$.apply$default$2(),
                org.neo4j.cypher.internal.v4_0.ast.prettifier.ExpressionStringifier..MODULE$.apply$default$3())));
    }

    void com$neo4j$fabric$util$PrettyPrintingUtils$_setter_$com$neo4j$fabric$util$PrettyPrintingUtils$$printer_$eq( final Prettifier x$1 );

    Prettifier com$neo4j$fabric$util$PrettyPrintingUtils$$printer();

    default String expr( final Expression e )
    {
        return this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer().expr().apply( e );
    }

    default Stream<String> query( final Statement s )
    {
        return (new StringOps(.MODULE$.augmentString( this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer().asString( s ) ))).linesIterator().toStream();
    }

    default String clause( final Clause c )
    {
        return this.query( (Seq) scala.collection.Seq..MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new Clause[]{c}) ))).mkString( "\n" );
    }

    default Stream<String> query( final Seq<Clause> cs )
    {
        InputPosition pos = org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE();
        return this.query( (Statement) (new Query( scala.None..MODULE$, new SingleQuery( cs, pos ), pos )));
    }

    default String list( final Seq<Object> ss )
    {
        return ss.mkString( "," );
    }
}
