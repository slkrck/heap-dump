package com.neo4j.fabric.util;

import com.neo4j.fabric.eval.Catalog;
import org.neo4j.cypher.internal.v4_0.ast.CatalogDDL;
import org.neo4j.cypher.internal.v4_0.ast.CatalogName;
import org.neo4j.cypher.internal.v4_0.ast.semantics.FeatureError;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticError;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef;
import org.neo4j.cypher.internal.v4_0.util.ASTNode;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.exceptions.DatabaseAdministrationException;
import org.neo4j.exceptions.EntityNotFoundException;
import org.neo4j.exceptions.InvalidSemanticsException;
import org.neo4j.exceptions.SyntaxException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import scala.Function0;
import scala.MatchError;
import scala.Predef.;
import scala.collection.Seq;
import scala.collection.TraversableOnce;

public final class Errors$
{
    public static Errors$ MODULE$;

    static
    {
        new Errors$();
    }

    final Seq<SemanticErrorDef> errors
    final SemanticErrorDef error

   public scala.runtime.Nothing.openCypherInvalid(
    final Seq<SemanticErrorDef> errors)
    final SemanticErrorDef error

   public scala.runtime.Nothing.openCypherInvalid(
    final String exp)
    final InputPosition pos

   public scala.runtime.Nothing.openCypherFailure(
    final String exp)
    final String got

   public scala.runtime.Nothing.openCypherFailure(
    final InputPosition pos)
    final String exp

   public scala.runtime.Nothing.openCypherUnexpected(
    final String got, final String in)
    final InputPosition pos

   public scala.runtime.Nothing.openCypherUnexpected(
    final String exp, final ASTNode got, final String qualifiedName)
    final InputPosition pos

   public scala.runtime.Nothing.openCypherUnexpected(
    final String exp, final String got, final int exp, final int got)
    final InputPosition pos

   public scala.runtime.Nothing.openCypherUnexpected(
    final String msg, final String query)
    final InputPosition pos

   public scala.runtime.Nothing.openCypherUnknownFunction(
    final String message, final CatalogDDL ddl)
    final String feature

   public scala.runtime.Nothing.wrongType(
    final String kind, final String needle)

    {
        throw new Errors.InvalidQueryException( errors );
    }

   public scala.runtime.Nothing.wrongArity(

    {
        return this.openCypherInvalid( (Seq) scala.collection.Seq..MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new SemanticErrorDef[]{error}) )));
    },

    {
        throw new Errors.EvaluationFailedException( errors );
    },

    {
        return this.openCypherFailure( (Seq) scala.collection.Seq..MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new SemanticErrorDef[]{error}) )));
    })

    {
        return this.openCypherInvalid(
                (SemanticErrorDef) (new SemanticError( (new StringBuilder( 10 )).append( "Expected: " ).append( exp ).toString(), pos,.MODULE$.wrapRefArray(
                        (Object[]) (new InputPosition[0]) )) ));
    }

   public scala.runtime.Nothing.syntax(

    {
        return this.openCypherInvalid( (SemanticErrorDef) (new SemanticError(
                (new StringBuilder( 17 )).append( "Expected: " ).append( exp ).append( ", got: " ).append( got ).toString(), pos,.MODULE$.wrapRefArray(
                (Object[]) (new InputPosition[0]) )) ));
    },

    {
        return this.openCypherInvalid( (SemanticErrorDef) (new SemanticError(
                (new StringBuilder( 23 )).append( "Expected: " ).append( exp ).append( ", got: " ).append( got ).append( ", in: " ).append( in ).toString(),
                pos,.MODULE$.wrapRefArray( (Object[]) (new InputPosition[0]) )) ));
    },

    {
        return this.openCypherUnexpected( exp, got.position() );
    })

    {
        return this.openCypherFailure( (SemanticErrorDef) (new SemanticError(
                (new StringBuilder( 19 )).append( "Unknown function '" ).append( qualifiedName ).append( "'" ).toString(), pos,.MODULE$.wrapRefArray(
                (Object[]) (new InputPosition[0]) )) ));
    }

   public scala.runtime.Nothing.semantic(

    {
        throw new CypherTypeException( (new StringBuilder( 17 )).append( "Expected: " ).append( exp ).append( ", got: " ).append( got ).toString() );
    })

    {
        return this.syntax( (new StringBuilder( 10 )).append( exp ).append( " arguments" ).toString(),
                (new StringBuilder( 10 )).append( got ).append( " arguments" ).toString(), pos );
    }

   public scala.runtime.Nothing.ddlNotSupported(

    {
        throw new SyntaxException( msg, query, pos.offset() );
    })

    {
        throw new InvalidSemanticsException( message );
    }

   public scala.runtime.Nothing.notSupported(

    {
        throw new DatabaseAdministrationException(
                (new StringBuilder( 89 )).append( "This is an administration command and it should be executed against the system database: " ).append(
                        ddl.name() ).toString() );
    })

    {
        return this.semantic( (new StringBuilder( 33 )).append( feature ).append( " not supported in Fabric database" ).toString() );
    }

   public scala.runtime.Nothing.entityNotFound(

    {
        throw new EntityNotFoundException( (new StringBuilder( 12 )).append( kind ).append( " not found: " ).append( needle ).toString() );
    },

    private Errors$()
    {
        MODULE$ = this;
    })

    public SemanticError openCypherSemantic( final String msg, final ASTNode node )
    {
        return new SemanticError( msg, node.position(),.MODULE$.wrapRefArray( (Object[]) (new InputPosition[0]) ));
    }

    public void openCypherInvalidOnError( final Seq<SemanticErrorDef> errors )
    {
        if ( errors.nonEmpty() )
        {
            throw this.openCypherInvalid( errors );
        }
    }

    public <T> T errorContext( final String query, final ASTNode node, final Function0<T> block )
    {
        try
        {
            return block.apply();
        }
        catch ( Throwable var8 )
        {
            if ( var8 instanceof Errors.HasErrors )
            {
                Errors.HasErrors var7 = (Errors.HasErrors) var8;
                throw (Throwable) var7.update( ( x0$1 ) -> {
                    boolean var4 = false;
                    SemanticError var5 = null;
                    boolean var6 = false;
                    FeatureError var7 = null;
                    InputPosition var10000;
                    if ( x0$1 instanceof SemanticError )
                    {
                        var4 = true;
                        var5 = (SemanticError) x0$1;
                        String msgxxx = var5.msg();
                        InputPosition var10 = var5.position();
                        var10000 = org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE();
                        if ( var10000 == null )
                        {
                            if ( var10 == null )
                            {
                                throw MODULE$.syntax( msgxxx, query, node.position() );
                            }
                        }
                        else if ( var10000.equals( var10 ) )
                        {
                            throw MODULE$.syntax( msgxxx, query, node.position() );
                        }
                    }

                    if ( var4 )
                    {
                        String msg = var5.msg();
                        InputPosition pos = var5.position();
                        throw MODULE$.syntax( msg, query, pos );
                    }
                    else
                    {
                        if ( x0$1 instanceof FeatureError )
                        {
                            var6 = true;
                            var7 = (FeatureError) x0$1;
                            String msgx = var7.msg();
                            InputPosition var15 = var7.position();
                            var10000 = org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE();
                            if ( var10000 == null )
                            {
                                if ( var15 == null )
                                {
                                    throw MODULE$.syntax( msgx, query, node.position() );
                                }
                            }
                            else if ( var10000.equals( var15 ) )
                            {
                                throw MODULE$.syntax( msgx, query, node.position() );
                            }
                        }

                        if ( var6 )
                        {
                            String msgxx = var7.msg();
                            InputPosition posx = var7.position();
                            throw MODULE$.syntax( msgxx, query, posx );
                        }
                        else
                        {
                            return x0$1;
                        }
                    }
                } );
            }
            else
            {
                throw var8;
            }
        }
    }

    public String show( final CatalogName n )
    {
        return n.parts().mkString( "." );
    }

    public String show( final CypherType t )
    {
        return t.toNeoTypeString();
    }

    public String show( final AnyValue av )
    {
        String var2;
        if ( av instanceof Value )
        {
            Value var4 = (Value) av;
            var2 = var4.prettyPrint();
        }
        else
        {
            var2 = av.getTypeName();
        }

        return var2;
    }

    public String show( final Catalog.Arg<?> a )
    {
        return (new StringBuilder( 2 )).append( a.name() ).append( ": " ).append( a.tpe().getSimpleName() ).toString();
    }

    public String show( final Seq<?> seq )
    {
        return ((TraversableOnce) seq.map( ( x0$2 ) -> {
            String var1;
            if ( x0$2 instanceof AnyValue )
            {
                AnyValue var3 = (AnyValue) x0$2;
                var1 = MODULE$.show( var3 );
            }
            else
            {
                if ( !(x0$2 instanceof Catalog.Arg) )
                {
                    throw new MatchError( x0$2 );
                }

                Catalog.Arg var4 = (Catalog.Arg) x0$2;
                var1 = MODULE$.show( var4 );
            }

            return var1;
        }, scala.collection.Seq..MODULE$.canBuildFrom())).mkString( "(", ",", ")" );
    }
}
