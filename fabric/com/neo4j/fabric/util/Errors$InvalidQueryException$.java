package com.neo4j.fabric.util;

import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction1;

public class Errors$InvalidQueryException$ extends AbstractFunction1<Seq<SemanticErrorDef>,Errors.InvalidQueryException> implements Serializable
{
    public static Errors$InvalidQueryException$ MODULE$;

    static
    {
        new Errors$InvalidQueryException$();
    }

    public Errors$InvalidQueryException$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "InvalidQueryException";
    }

    public Errors.InvalidQueryException apply( final Seq<SemanticErrorDef> errors )
    {
        return new Errors.InvalidQueryException( errors );
    }

    public Option<Seq<SemanticErrorDef>> unapply( final Errors.InvalidQueryException x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.errors() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
