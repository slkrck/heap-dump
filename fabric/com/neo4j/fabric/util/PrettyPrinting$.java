package com.neo4j.fabric.util;

import org.neo4j.cypher.internal.v4_0.ast.Clause;
import org.neo4j.cypher.internal.v4_0.ast.Statement;
import org.neo4j.cypher.internal.v4_0.ast.prettifier.Prettifier;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import scala.collection.Seq;
import scala.collection.immutable.Stream;

public final class PrettyPrinting$ implements PrettyPrintingUtils
{
    public static PrettyPrinting$ MODULE$;

    static
    {
        new PrettyPrinting$();
    }

    private final Prettifier com$neo4j$fabric$util$PrettyPrintingUtils$$printer;

    private PrettyPrinting$()
    {
        MODULE$ = this;
        PrettyPrintingUtils.$init$( this );
    }

    public String expr( final Expression e )
    {
        return PrettyPrintingUtils.expr$( this, e );
    }

    public Stream<String> query( final Statement s )
    {
        return PrettyPrintingUtils.query$( this, (Statement) s );
    }

    public String clause( final Clause c )
    {
        return PrettyPrintingUtils.clause$( this, c );
    }

    public Stream<String> query( final Seq<Clause> cs )
    {
        return PrettyPrintingUtils.query$( this, (Seq) cs );
    }

    public String list( final Seq<Object> ss )
    {
        return PrettyPrintingUtils.list$( this, ss );
    }

    public Prettifier com$neo4j$fabric$util$PrettyPrintingUtils$$printer()
    {
        return this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer;
    }

    public final void com$neo4j$fabric$util$PrettyPrintingUtils$_setter_$com$neo4j$fabric$util$PrettyPrintingUtils$$printer_$eq( final Prettifier x$1 )
    {
        this.com$neo4j$fabric$util$PrettyPrintingUtils$$printer = x$1;
    }
}
