package com.neo4j.fabric.util;

import scala.Function1;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public final class Rewritten
{
    public static Object RewritingOps( final Object that )
    {
        return Rewritten$.MODULE$.RewritingOps( var0 );
    }

    public static final class RewritingOps<T>
    {
        private final T that;

        public RewritingOps( final T that )
        {
            this.that = that;
        }

        public T that()
        {
            return this.that;
        }

        public com.neo4j.fabric.util.Rewritten.Rewritten<T> rewritten()
        {
            return Rewritten$RewritingOps$.MODULE$.rewritten$extension( this.that() );
        }

        public int hashCode()
        {
            return Rewritten$RewritingOps$.MODULE$.hashCode$extension( this.that() );
        }

        public boolean equals( final Object x$1 )
        {
            return Rewritten$RewritingOps$.MODULE$.equals$extension( this.that(), x$1 );
        }
    }

    public static class Rewritten<T> implements Product, Serializable
    {
        private final T that;
        private final Function1<Object,Object> stopper;

        public Rewritten( final T that, final Function1<Object,Object> stopper )
        {
            this.that = that;
            this.stopper = stopper;
            Product.$init$( this );
        }

        public T that()
        {
            return this.that;
        }

        public Function1<Object,Object> stopper()
        {
            return this.stopper;
        }

        public com.neo4j.fabric.util.Rewritten.Rewritten<T> stoppingAt( final PartialFunction<Object,Object> stop )
        {
            PartialFunction x$1 = stop.orElse( Rewritten$.MODULE$.com$neo4j$fabric$util$Rewritten$$never() );
            Object x$2 = this.copy$default$1();
            return this.copy( x$2, x$1 );
        }

        public T bottomUp( final PartialFunction<Object,Object> pf )
        {
            return org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
            MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny(
                    this.that() ), org.neo4j.cypher.internal.v4_0.util.bottomUp..
            MODULE$.apply( org.neo4j.cypher.internal.v4_0.util.Rewriter..MODULE$.lift( pf ), this.stopper()));
        }

        public T topDown( final PartialFunction<Object,Object> pf )
        {
            return org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
            MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny(
                    this.that() ), org.neo4j.cypher.internal.v4_0.util.topDown..
            MODULE$.apply( org.neo4j.cypher.internal.v4_0.util.Rewriter..MODULE$.lift( pf ), this.stopper()));
        }

        public <T> com.neo4j.fabric.util.Rewritten.Rewritten<T> copy( final T that, final Function1<Object,Object> stopper )
        {
            return new com.neo4j.fabric.util.Rewritten.Rewritten( that, stopper );
        }

        public <T> T copy$default$1()
        {
            return this.that();
        }

        public <T> Function1<Object,Object> copy$default$2()
        {
            return this.stopper();
        }

        public String productPrefix()
        {
            return "Rewritten";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.that();
                break;
            case 1:
                var10000 = this.stopper();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof com.neo4j.fabric.util.Rewritten.Rewritten;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            if ( this != x$1 )
            {
                label55:
                {
                    boolean var2;
                    if ( x$1 instanceof com.neo4j.fabric.util.Rewritten.Rewritten )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label38:
                        {
                            com.neo4j.fabric.util.Rewritten.Rewritten var4 = (com.neo4j.fabric.util.Rewritten.Rewritten) x$1;
                            if ( BoxesRunTime.equals( this.that(), var4.that() ) )
                            {
                                label36:
                                {
                                    Function1 var10000 = this.stopper();
                                    Function1 var5 = var4.stopper();
                                    if ( var10000 == null )
                                    {
                                        if ( var5 != null )
                                        {
                                            break label36;
                                        }
                                    }
                                    else if ( !var10000.equals( var5 ) )
                                    {
                                        break label36;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var6 = true;
                                        break label38;
                                    }
                                }
                            }

                            var6 = false;
                        }

                        if ( var6 )
                        {
                            break label55;
                        }
                    }

                    var6 = false;
                    return var6;
                }
            }

            var6 = true;
            return var6;
        }
    }
}
