package org.neo4j.cypher.internal.runtime.compiled.expressions;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.operations.CypherFunctions;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class DefaultExpressionCompiler extends ExpressionCompiler
{
    public DefaultExpressionCompiler( final SlotConfiguration slots, final boolean readOnly, final CodeGenerationMode codeGenerationMode,
            final VariableNamer namer )
    {
        super( slots, readOnly, codeGenerationMode, namer );
    }

    public IntermediateRepresentation getLongAt( final int offset )
    {
        return this.getLongFromExecutionContext( offset, this.getLongFromExecutionContext$default$2() );
    }

    public IntermediateRepresentation getRefAt( final int offset )
    {
        return this.getRefFromExecutionContext( offset, this.getRefFromExecutionContext$default$2() );
    }

    public IntermediateRepresentation setRefAt( final int offset, final IntermediateRepresentation value )
    {
        return this.setRefInExecutionContext( offset, value );
    }

    public IntermediateRepresentation setLongAt( final int offset, final IntermediateRepresentation value )
    {
        return this.setLongInExecutionContext( offset, value );
    }

    public IntermediateRepresentation setCachedPropertyAt( final int offset, final IntermediateRepresentation value )
    {
        return this.setCachedPropertyInExecutionContext( offset, value );
    }

    public IntermediateRepresentation getCachedPropertyAt( final SlottedCachedProperty property, final IntermediateRepresentation getFromStore )
    {
        String variableName = super.namer().nextVariableName();
        return .MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{.MODULE$.declareAndAssign(.MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                    Value.class )), variableName,
            this.getCachedPropertyFromExecutionContext( property.cachedPropertyOffset(), this.getCachedPropertyFromExecutionContext$default$2() ) ), .
        MODULE$.condition(.MODULE$.isNull(.MODULE$.load( variableName )), .
        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.assign( variableName, getFromStore ),
                this.setCachedPropertyAt( property.cachedPropertyOffset(),.MODULE$.load( variableName ) )})))), .MODULE$.load( variableName )})));
    }

    public IntermediateRepresentation isLabelSetOnNode( final IntermediateRepresentation labelToken, final int offset )
    {
        return .MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),.MODULE$.method( "isLabelSetOnNode", scala.reflect.ManifestFactory..MODULE$.classType(
            DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
        MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{labelToken, this.getLongAt( offset ), ExpressionCompiler$.MODULE$.NODE_CURSOR()}) ));
    }

    public IntermediateRepresentation getNodeProperty( final IntermediateRepresentation propertyToken, final int offset )
    {
        return .MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), ExpressionCompiler$.MODULE$.NODE_PROPERTY(), scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ), propertyToken, ExpressionCompiler$.MODULE$.NODE_CURSOR(),
                    ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(),.MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation getRelationshipProperty( final IntermediateRepresentation propertyToken, final int offset )
    {
        return .
        MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), ExpressionCompiler$.MODULE$.RELATIONSHIP_PROPERTY(), scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ), propertyToken, ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                        ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(),.MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})));
    }

    public IntermediateRepresentation getProperty( final String key, final IntermediateRepresentation container )
    {
        return .MODULE$.invokeStatic(.MODULE$.method( "propertyGet", scala.reflect.ManifestFactory..MODULE$.classType(
            CypherFunctions.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( String.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{.MODULE$.constant( key ), container, ExpressionCompiler$.MODULE$.DB_ACCESS(),
                ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()})));
    }
}
