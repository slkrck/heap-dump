package org.neo4j.cypher.internal.runtime.compiled.expressions;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.Method;
import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.codegen.api.IntermediateRepresentation.;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.Manifest;

public final class ExpressionCompiler$
{
    public static ExpressionCompiler$ MODULE$;

    static
    {
        new ExpressionCompiler$();
    }

    private final AtomicLong COUNTER;
    private final Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$ASSERT_PREDICATE;
    private final IntermediateRepresentation DB_ACCESS;
    private final IntermediateRepresentation org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$CURSORS;
    private final IntermediateRepresentation NODE_CURSOR;
    private final LocalVariable vNODE_CURSOR;
    private final IntermediateRepresentation RELATIONSHIP_CURSOR;
    private final LocalVariable vRELATIONSHIP_CURSOR;
    private final IntermediateRepresentation PROPERTY_CURSOR;
    private final LocalVariable vPROPERTY_CURSOR;
    private final Seq<LocalVariable> org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS;
    private final IntermediateRepresentation org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT;
    private final Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_NODE_PROP;
    private final Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_RELATIONSHIP_PROP;
    private final Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_NODE_PROP;
    private final Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_RELATIONSHIP_PROP;
    private final Method NODE_PROPERTY;
    private final Method RELATIONSHIP_PROPERTY;
    private final String org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$PACKAGE_NAME;

    private ExpressionCompiler$()
    {
        MODULE$ = this;
        this.COUNTER = new AtomicLong( 0L );
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$ASSERT_PREDICATE = .
        MODULE$.method( "assertBooleanOrNoValue", scala.reflect.ManifestFactory..MODULE$.classType( CompiledHelpers.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
        this.DB_ACCESS = .MODULE$.load( "dbAccess" );
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$CURSORS = .MODULE$.load( "cursors" );
        this.NODE_CURSOR = .MODULE$.load( "nodeCursor" );
        this.vNODE_CURSOR = this.cursorVariable( "nodeCursor", scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ));
        this.RELATIONSHIP_CURSOR = .MODULE$.load( "relationshipScanCursor" );
        this.vRELATIONSHIP_CURSOR =
                this.cursorVariable( "relationshipScanCursor", scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ));
        this.PROPERTY_CURSOR = .MODULE$.load( "propertyCursor" );
        this.vPROPERTY_CURSOR = this.cursorVariable( "propertyCursor", scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class ));
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS = (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new LocalVariable[]{this.vNODE_CURSOR(), this.vRELATIONSHIP_CURSOR(), this.vPROPERTY_CURSOR()}) ));
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT = .MODULE$.load( "context" );
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_NODE_PROP = .
        MODULE$.method( "getTxStateNodePropertyOrNull", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int());
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_RELATIONSHIP_PROP = .
        MODULE$.method( "getTxStateRelationshipPropertyOrNull", scala.reflect.ManifestFactory..MODULE$.classType(
                DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
        MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int());
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_NODE_PROP = .
        MODULE$.method( "hasTxStatePropertyForCachedNodeProperty", scala.reflect.ManifestFactory..MODULE$.classType(
                DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( Optional.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int());
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_RELATIONSHIP_PROP = .
        MODULE$.method( "hasTxStatePropertyForCachedRelationshipProperty", scala.reflect.ManifestFactory..MODULE$.classType(
                DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( Optional.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int());
        this.NODE_PROPERTY = .MODULE$.method( "nodeProperty", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( PropertyCursor.class ), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.RELATIONSHIP_PROPERTY = .
        MODULE$.method( "relationshipProperty", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( PropertyCursor.class ), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$PACKAGE_NAME = "org.neo4j.codegen";
    }

    public VariableNamer $lessinit$greater$default$4()
    {
        return new VariableNamer();
    }

    public ExpressionCompiler defaultGenerator( final SlotConfiguration slots, final boolean readOnly, final CodeGenerationMode codeGenerationMode,
            final VariableNamer namer )
    {
        return new DefaultExpressionCompiler( slots, readOnly, codeGenerationMode, namer );
    }

    public VariableNamer defaultGenerator$default$4()
    {
        return new VariableNamer();
    }

    private AtomicLong COUNTER()
    {
        return this.COUNTER;
    }

    public Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$ASSERT_PREDICATE()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$ASSERT_PREDICATE;
    }

    public IntermediateRepresentation DB_ACCESS()
    {
        return this.DB_ACCESS;
    }

    public IntermediateRepresentation org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$CURSORS()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$CURSORS;
    }

    public IntermediateRepresentation NODE_CURSOR()
    {
        return this.NODE_CURSOR;
    }

    public LocalVariable vNODE_CURSOR()
    {
        return this.vNODE_CURSOR;
    }

    public IntermediateRepresentation RELATIONSHIP_CURSOR()
    {
        return this.RELATIONSHIP_CURSOR;
    }

    public LocalVariable vRELATIONSHIP_CURSOR()
    {
        return this.vRELATIONSHIP_CURSOR;
    }

    public IntermediateRepresentation PROPERTY_CURSOR()
    {
        return this.PROPERTY_CURSOR;
    }

    public LocalVariable vPROPERTY_CURSOR()
    {
        return this.vPROPERTY_CURSOR;
    }

    public Seq<LocalVariable> org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS;
    }

    public IntermediateRepresentation org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT;
    }

    public Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_NODE_PROP()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_NODE_PROP;
    }

    public Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_RELATIONSHIP_PROP()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_RELATIONSHIP_PROP;
    }

    public Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_NODE_PROP()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_NODE_PROP;
    }

    public Method org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_RELATIONSHIP_PROP()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_RELATIONSHIP_PROP;
    }

    public Method NODE_PROPERTY()
    {
        return this.NODE_PROPERTY;
    }

    public Method RELATIONSHIP_PROPERTY()
    {
        return this.RELATIONSHIP_PROPERTY;
    }

    public String org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$PACKAGE_NAME()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$PACKAGE_NAME;
    }

    public String org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$className()
    {
        return (new StringBuilder( 10 )).append( "Expression" ).append( this.COUNTER().getAndIncrement() ).toString();
    }

    private <T> LocalVariable cursorVariable( final String name, final Manifest<T> m )
    {
        return .MODULE$.variable( name,.MODULE$.invoke(.MODULE$.load( "cursors" ), .
        MODULE$.method( name, scala.reflect.ManifestFactory..MODULE$.classType( ExpressionCursors.class ), m),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),m);
    }

    public IntermediateRepresentation noValueOr( final Seq<IntermediateExpression> expressions, final IntermediateRepresentation onNotNull )
    {
        return this.nullCheck( expressions,.MODULE$.noValue(),onNotNull);
    }

    public IntermediateRepresentation nullCheck( final Seq<IntermediateExpression> expressions, final IntermediateRepresentation onNull,
            final IntermediateRepresentation onNotNull )
    {
        Set checks = (Set) expressions.foldLeft( scala.Predef..MODULE$.Set().empty(), ( acc, current ) -> {
        return (Set) acc.$plus$plus( current.nullChecks() );
    });
        return checks.nonEmpty() ? .MODULE$.ternary( (IntermediateRepresentation) checks.reduceLeft( ( lhs, rhs ) -> {
        return .MODULE$.or( lhs, rhs );
    } ), onNull, onNotNull ) :onNotNull;
    }

    public IntermediateRepresentation nullCheck$default$2( final Seq<IntermediateExpression> expressions )
    {
        return .MODULE$.noValue();
    }

    public IntermediateRepresentation nullCheckIfRequired( final IntermediateExpression expression, final IntermediateRepresentation onNull )
    {
        return expression.requireNullCheck() ? this.nullCheck( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{expression}) ),
        onNull, expression.ir()) :expression.ir();
    }

    public IntermediateRepresentation nullCheckIfRequired$default$2()
    {
        return .MODULE$.noValue();
    }
}
