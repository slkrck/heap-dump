package org.neo4j.cypher.internal.runtime.compiled.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class IntermediateGroupingExpression$
        extends AbstractFunction3<IntermediateExpression,IntermediateExpression,IntermediateExpression,IntermediateGroupingExpression> implements Serializable
{
    public static IntermediateGroupingExpression$ MODULE$;

    static
    {
        new IntermediateGroupingExpression$();
    }

    private IntermediateGroupingExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "IntermediateGroupingExpression";
    }

    public IntermediateGroupingExpression apply( final IntermediateExpression projectKey, final IntermediateExpression computeKey,
            final IntermediateExpression getKey )
    {
        return new IntermediateGroupingExpression( projectKey, computeKey, getKey );
    }

    public Option<Tuple3<IntermediateExpression,IntermediateExpression,IntermediateExpression>> unapply( final IntermediateGroupingExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.projectKey(), x$0.computeKey(), x$0.getKey() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
