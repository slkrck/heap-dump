package org.neo4j.cypher.internal.runtime.compiled.expressions;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class IntermediateExpression implements Product, Serializable
{
    private final IntermediateRepresentation ir;
    private final Seq<Field> fields;
    private final Seq<LocalVariable> variables;
    private final Set<IntermediateRepresentation> nullChecks;
    private final boolean requireNullCheck;

    public IntermediateExpression( final IntermediateRepresentation ir, final Seq<Field> fields, final Seq<LocalVariable> variables,
            final Set<IntermediateRepresentation> nullChecks, final boolean requireNullCheck )
    {
        this.ir = ir;
        this.fields = fields;
        this.variables = variables;
        this.nullChecks = nullChecks;
        this.requireNullCheck = requireNullCheck;
        Product.$init$( this );
    }

    public static boolean $lessinit$greater$default$5()
    {
        return IntermediateExpression$.MODULE$.$lessinit$greater$default$5();
    }

    public static boolean apply$default$5()
    {
        return IntermediateExpression$.MODULE$.apply$default$5();
    }

    public static Option<Tuple5<IntermediateRepresentation,Seq<Field>,Seq<LocalVariable>,Set<IntermediateRepresentation>,Object>> unapply(
            final IntermediateExpression x$0 )
    {
        return IntermediateExpression$.MODULE$.unapply( var0 );
    }

    public static IntermediateExpression apply( final IntermediateRepresentation ir, final Seq<Field> fields, final Seq<LocalVariable> variables,
            final Set<IntermediateRepresentation> nullChecks, final boolean requireNullCheck )
    {
        return IntermediateExpression$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public static Function1<Tuple5<IntermediateRepresentation,Seq<Field>,Seq<LocalVariable>,Set<IntermediateRepresentation>,Object>,IntermediateExpression> tupled()
    {
        return IntermediateExpression$.MODULE$.tupled();
    }

    public static Function1<IntermediateRepresentation,Function1<Seq<Field>,Function1<Seq<LocalVariable>,Function1<Set<IntermediateRepresentation>,Function1<Object,IntermediateExpression>>>>> curried()
    {
        return IntermediateExpression$.MODULE$.curried();
    }

    public IntermediateRepresentation ir()
    {
        return this.ir;
    }

    public Seq<Field> fields()
    {
        return this.fields;
    }

    public Seq<LocalVariable> variables()
    {
        return this.variables;
    }

    public Set<IntermediateRepresentation> nullChecks()
    {
        return this.nullChecks;
    }

    public boolean requireNullCheck()
    {
        return this.requireNullCheck;
    }

    public IntermediateExpression copy( final IntermediateRepresentation ir, final Seq<Field> fields, final Seq<LocalVariable> variables,
            final Set<IntermediateRepresentation> nullChecks, final boolean requireNullCheck )
    {
        return new IntermediateExpression( ir, fields, variables, nullChecks, requireNullCheck );
    }

    public IntermediateRepresentation copy$default$1()
    {
        return this.ir();
    }

    public Seq<Field> copy$default$2()
    {
        return this.fields();
    }

    public Seq<LocalVariable> copy$default$3()
    {
        return this.variables();
    }

    public Set<IntermediateRepresentation> copy$default$4()
    {
        return this.nullChecks();
    }

    public boolean copy$default$5()
    {
        return this.requireNullCheck();
    }

    public String productPrefix()
    {
        return "IntermediateExpression";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.ir();
            break;
        case 1:
            var10000 = this.fields();
            break;
        case 2:
            var10000 = this.variables();
            break;
        case 3:
            var10000 = this.nullChecks();
            break;
        case 4:
            var10000 = BoxesRunTime.boxToBoolean( this.requireNullCheck() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof IntermediateExpression;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.ir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fields() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.variables() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.nullChecks() ) );
        var1 = Statics.mix( var1, this.requireNullCheck() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 5 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label83:
            {
                boolean var2;
                if ( x$1 instanceof IntermediateExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label65:
                    {
                        label74:
                        {
                            IntermediateExpression var4 = (IntermediateExpression) x$1;
                            IntermediateRepresentation var10000 = this.ir();
                            IntermediateRepresentation var5 = var4.ir();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label74;
                            }

                            Seq var9 = this.fields();
                            Seq var6 = var4.fields();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label74;
                            }

                            var9 = this.variables();
                            Seq var7 = var4.variables();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label74;
                            }

                            Set var10 = this.nullChecks();
                            Set var8 = var4.nullChecks();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label74;
                            }

                            if ( this.requireNullCheck() == var4.requireNullCheck() && var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label65;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label83;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
