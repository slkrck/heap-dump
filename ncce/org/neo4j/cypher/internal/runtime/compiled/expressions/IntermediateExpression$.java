package org.neo4j.cypher.internal.runtime.compiled.expressions;

import org.neo4j.codegen.api.Field;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.runtime.AbstractFunction5;
import scala.runtime.BoxesRunTime;

public final class IntermediateExpression$
        extends AbstractFunction5<IntermediateRepresentation,Seq<Field>,Seq<LocalVariable>,Set<IntermediateRepresentation>,Object,IntermediateExpression>
        implements Serializable
{
    public static IntermediateExpression$ MODULE$;

    static
    {
        new IntermediateExpression$();
    }

    private IntermediateExpression$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$5()
    {
        return true;
    }

    public final String toString()
    {
        return "IntermediateExpression";
    }

    public IntermediateExpression apply( final IntermediateRepresentation ir, final Seq<Field> fields, final Seq<LocalVariable> variables,
            final Set<IntermediateRepresentation> nullChecks, final boolean requireNullCheck )
    {
        return new IntermediateExpression( ir, fields, variables, nullChecks, requireNullCheck );
    }

    public boolean apply$default$5()
    {
        return true;
    }

    public Option<Tuple5<IntermediateRepresentation,Seq<Field>,Seq<LocalVariable>,Set<IntermediateRepresentation>,Object>> unapply(
            final IntermediateExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple5( x$0.ir(), x$0.fields(), x$0.variables(), x$0.nullChecks(), BoxesRunTime.boxToBoolean( x$0.requireNullCheck() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
