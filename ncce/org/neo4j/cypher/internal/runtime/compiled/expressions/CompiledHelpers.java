package org.neo4j.cypher.internal.runtime.compiled.expressions;

import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.KernelAPISupport.;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.exceptions.ParameterWrongTypeException;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.ValueGroup;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.VirtualNodeValue;

public final class CompiledHelpers
{
    private static final ValueGroup[] RANGE_SEEKABLE_VALUE_GROUPS;

    static
    {
        RANGE_SEEKABLE_VALUE_GROUPS = .MODULE$.RANGE_SEEKABLE_VALUE_GROUPS();
    }

    private CompiledHelpers()
    {
        throw new UnsupportedOperationException( "do not instantiate" );
    }

    public static Value assertBooleanOrNoValue( AnyValue value )
    {
        if ( value != Values.NO_VALUE && !(value instanceof BooleanValue) )
        {
            throw new CypherTypeException( String.format( "Don't know how to treat a predicate: %s", value.toString() ), (Throwable) null );
        }
        else
        {
            return (Value) value;
        }
    }

    public static AnyValue nodeOrNoValue( ExecutionContext context, DbAccess dbAccess, int offset )
    {
        long nodeId = context.getLongAt( offset );
        return (AnyValue) (nodeId == -1L ? Values.NO_VALUE : dbAccess.nodeById( nodeId ));
    }

    public static AnyValue nodeOrNoValue( DbAccess dbAccess, long nodeId )
    {
        return (AnyValue) (nodeId == -1L ? Values.NO_VALUE : dbAccess.nodeById( nodeId ));
    }

    public static AnyValue relationshipOrNoValue( ExecutionContext context, DbAccess dbAccess, int offset )
    {
        long relationshipId = context.getLongAt( offset );
        return (AnyValue) (relationshipId == -1L ? Values.NO_VALUE : dbAccess.relationshipById( relationshipId ));
    }

    public static boolean possibleRangePredicate( IndexQuery query )
    {
        ValueGroup valueGroup = query.valueGroup();
        ValueGroup[] var2 = RANGE_SEEKABLE_VALUE_GROUPS;
        int var3 = var2.length;

        for ( int var4 = 0; var4 < var3; ++var4 )
        {
            ValueGroup rangeSeekableValueGroup = var2[var4];
            if ( valueGroup == rangeSeekableValueGroup )
            {
                return true;
            }
        }

        return false;
    }

    public static long nodeFromAnyValue( AnyValue value )
    {
        if ( value instanceof VirtualNodeValue )
        {
            return ((VirtualNodeValue) value).id();
        }
        else
        {
            throw new ParameterWrongTypeException( String.format( "Expected to find a node but found %s instead", value ) );
        }
    }

    public static long nodeIdOrNullFromAnyValue( AnyValue value )
    {
        return value instanceof VirtualNodeValue ? ((VirtualNodeValue) value).id() : -1L;
    }
}
