package org.neo4j.cypher.internal.runtime.compiled.expressions;

import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.values.AnyValue;

public interface CompiledGroupingExpression
{
    void projectGroupingKey( ExecutionContext var1, AnyValue var2 );

    AnyValue computeGroupingKey( ExecutionContext var1, DbAccess var2, AnyValue[] var3, ExpressionCursors var4, AnyValue[] var5 );

    AnyValue getGroupingKey( ExecutionContext var1 );
}
