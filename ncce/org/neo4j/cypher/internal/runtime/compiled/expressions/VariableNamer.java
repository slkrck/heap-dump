package org.neo4j.cypher.internal.runtime.compiled.expressions;

import scala.collection.mutable.Map;
import scala.collection.mutable.Map.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class VariableNamer
{
    private final Map<String,String> parameters;
    private final Map<String,String> variables;
    private int counter = 0;

    public VariableNamer()
    {
        this.parameters = .MODULE$.empty();
        this.variables = .MODULE$.empty();
    }

    private int counter()
    {
        return this.counter;
    }

    private void counter_$eq( final int x$1 )
    {
        this.counter = x$1;
    }

    private Map<String,String> parameters()
    {
        return this.parameters;
    }

    private Map<String,String> variables()
    {
        return this.variables;
    }

    public String nextVariableName()
    {
        String nextName = (new StringBuilder( 1 )).append( "v" ).append( this.counter() ).toString();
        this.counter_$eq( this.counter() + 1 );
        return nextName;
    }

    public String parameterName( final String name )
    {
        return (String) this.parameters().getOrElseUpdate( name, () -> {
            return this.nextVariableName();
        } );
    }

    public String variableName( final String name )
    {
        return (String) this.variables().getOrElseUpdate( name, () -> {
            return this.nextVariableName();
        } );
    }
}
