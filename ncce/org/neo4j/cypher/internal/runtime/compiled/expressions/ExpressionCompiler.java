package org.neo4j.cypher.internal.runtime.compiled.expressions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.regex.Pattern;

import org.neo4j.codegen.TypeReference;
import org.neo4j.codegen.api.AssignToLocalVariable;
import org.neo4j.codegen.api.ClassDeclaration;
import org.neo4j.codegen.api.InstanceField;
import org.neo4j.codegen.api.IntermediateRepresentation;
import org.neo4j.codegen.api.LocalVariable;
import org.neo4j.codegen.api.Method;
import org.neo4j.codegen.api.MethodDeclaration;
import org.neo4j.codegen.api.Parameter;
import org.neo4j.codegen.api.StaticField;
import org.neo4j.codegen.api.TryCatch;
import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.cypher.internal.logical.plans.CoerceToPredicate;
import org.neo4j.cypher.internal.logical.plans.NestedPlanExpression;
import org.neo4j.cypher.internal.logical.plans.ResolvedFunctionInvocation;
import org.neo4j.cypher.internal.logical.plans.UserFunctionSignature;
import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.TopLevelArgument$;
import org.neo4j.cypher.internal.physicalplanning.ast.GetDegreePrimitive;
import org.neo4j.cypher.internal.physicalplanning.ast.HasLabelsFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.IdFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.IsPrimitiveNull;
import org.neo4j.cypher.internal.physicalplanning.ast.LabelsFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.NodeFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.NodeProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExists;
import org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExistsLate;
import org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyLate;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheck;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckReferenceProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckVariable;
import org.neo4j.cypher.internal.physicalplanning.ast.PrimitiveEquals;
import org.neo4j.cypher.internal.physicalplanning.ast.ReferenceFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExists;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExistsLate;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyLate;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipTypeFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedPropertyWithPropertyToken;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedPropertyWithoutPropertyToken;
import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.ast.ExpressionVariable;
import org.neo4j.cypher.internal.runtime.ast.ParameterFromSlot;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.NestedPipeExpression;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import org.neo4j.cypher.internal.v4_0.expressions.Add;
import org.neo4j.cypher.internal.v4_0.expressions.AllIterablePredicate;
import org.neo4j.cypher.internal.v4_0.expressions.And;
import org.neo4j.cypher.internal.v4_0.expressions.AndedPropertyInequalities;
import org.neo4j.cypher.internal.v4_0.expressions.Ands;
import org.neo4j.cypher.internal.v4_0.expressions.AnyIterablePredicate;
import org.neo4j.cypher.internal.v4_0.expressions.CaseExpression;
import org.neo4j.cypher.internal.v4_0.expressions.CoerceTo;
import org.neo4j.cypher.internal.v4_0.expressions.ContainerIndex;
import org.neo4j.cypher.internal.v4_0.expressions.Contains;
import org.neo4j.cypher.internal.v4_0.expressions.DesugaredMapProjection;
import org.neo4j.cypher.internal.v4_0.expressions.Divide;
import org.neo4j.cypher.internal.v4_0.expressions.DoubleLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.EndsWith;
import org.neo4j.cypher.internal.v4_0.expressions.EntityType;
import org.neo4j.cypher.internal.v4_0.expressions.Equals;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.ExtractScope;
import org.neo4j.cypher.internal.v4_0.expressions.False;
import org.neo4j.cypher.internal.v4_0.expressions.FilterScope;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.GreaterThan;
import org.neo4j.cypher.internal.v4_0.expressions.GreaterThanOrEqual;
import org.neo4j.cypher.internal.v4_0.expressions.HasLabels;
import org.neo4j.cypher.internal.v4_0.expressions.In;
import org.neo4j.cypher.internal.v4_0.expressions.IntegerLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.IsNotNull;
import org.neo4j.cypher.internal.v4_0.expressions.IsNull;
import org.neo4j.cypher.internal.v4_0.expressions.LessThan;
import org.neo4j.cypher.internal.v4_0.expressions.LessThanOrEqual;
import org.neo4j.cypher.internal.v4_0.expressions.ListComprehension;
import org.neo4j.cypher.internal.v4_0.expressions.ListLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.ListSlice;
import org.neo4j.cypher.internal.v4_0.expressions.Literal;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.cypher.internal.v4_0.expressions.MapExpression;
import org.neo4j.cypher.internal.v4_0.expressions.MapProjection;
import org.neo4j.cypher.internal.v4_0.expressions.Modulo;
import org.neo4j.cypher.internal.v4_0.expressions.MultiRelationshipPathStep;
import org.neo4j.cypher.internal.v4_0.expressions.Multiply;
import org.neo4j.cypher.internal.v4_0.expressions.NodePathStep;
import org.neo4j.cypher.internal.v4_0.expressions.NoneIterablePredicate;
import org.neo4j.cypher.internal.v4_0.expressions.Not;
import org.neo4j.cypher.internal.v4_0.expressions.NotEquals;
import org.neo4j.cypher.internal.v4_0.expressions.Null;
import org.neo4j.cypher.internal.v4_0.expressions.Or;
import org.neo4j.cypher.internal.v4_0.expressions.Ors;
import org.neo4j.cypher.internal.v4_0.expressions.PathExpression;
import org.neo4j.cypher.internal.v4_0.expressions.PathStep;
import org.neo4j.cypher.internal.v4_0.expressions.PatternExpression;
import org.neo4j.cypher.internal.v4_0.expressions.Pow;
import org.neo4j.cypher.internal.v4_0.expressions.Property;
import org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName;
import org.neo4j.cypher.internal.v4_0.expressions.ReduceExpression;
import org.neo4j.cypher.internal.v4_0.expressions.ReduceScope;
import org.neo4j.cypher.internal.v4_0.expressions.RegexMatch;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.expressions.SingleIterablePredicate;
import org.neo4j.cypher.internal.v4_0.expressions.SingleRelationshipPathStep;
import org.neo4j.cypher.internal.v4_0.expressions.StartsWith;
import org.neo4j.cypher.internal.v4_0.expressions.StringLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.Subtract;
import org.neo4j.cypher.internal.v4_0.expressions.True;
import org.neo4j.cypher.internal.v4_0.expressions.UnaryAdd;
import org.neo4j.cypher.internal.v4_0.expressions.UnarySubtract;
import org.neo4j.cypher.internal.v4_0.expressions.Variable;
import org.neo4j.cypher.internal.v4_0.expressions.Xor;
import org.neo4j.cypher.internal.v4_0.expressions.functions.AggregatingFunction;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import org.neo4j.cypher.internal.v4_0.util.NonEmptyList;
import org.neo4j.cypher.internal.v4_0.util.symbols.AnyType;
import org.neo4j.cypher.internal.v4_0.util.symbols.BooleanType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.DateTimeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.DateType;
import org.neo4j.cypher.internal.v4_0.util.symbols.DurationType;
import org.neo4j.cypher.internal.v4_0.util.symbols.FloatType;
import org.neo4j.cypher.internal.v4_0.util.symbols.GeometryType;
import org.neo4j.cypher.internal.v4_0.util.symbols.IntegerType;
import org.neo4j.cypher.internal.v4_0.util.symbols.ListType;
import org.neo4j.cypher.internal.v4_0.util.symbols.LocalDateTimeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.LocalTimeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.MapType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NumberType;
import org.neo4j.cypher.internal.v4_0.util.symbols.PathType;
import org.neo4j.cypher.internal.v4_0.util.symbols.PointType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.symbols.StringType;
import org.neo4j.cypher.internal.v4_0.util.symbols.TimeType;
import org.neo4j.cypher.operations.CypherBoolean;
import org.neo4j.cypher.operations.CypherCoercions;
import org.neo4j.cypher.operations.CypherFunctions;
import org.neo4j.cypher.operations.CypherMath;
import org.neo4j.cypher.operations.PathValueBuilder;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.exceptions.InternalException;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.TextType;
import org.neo4j.kernel.impl.util.ValueUtils;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.DateTimeValue;
import org.neo4j.values.storable.DateValue;
import org.neo4j.values.storable.DoubleValue;
import org.neo4j.values.storable.DurationValue;
import org.neo4j.values.storable.FloatingPointValue;
import org.neo4j.values.storable.IntValue;
import org.neo4j.values.storable.IntegralValue;
import org.neo4j.values.storable.LocalDateTimeValue;
import org.neo4j.values.storable.LocalTimeValue;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.NumberValue;
import org.neo4j.values.storable.PointValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.TimeValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.MapValueBuilder;
import org.neo4j.values.virtual.NodeReference;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.PathValue;
import org.neo4j.values.virtual.RelationshipReference;
import org.neo4j.values.virtual.RelationshipValue;
import org.neo4j.values.virtual.VirtualNodeValue;
import org.neo4j.values.virtual.VirtualRelationshipValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.None.;
import scala.collection.GenIterable;
import scala.collection.GenTraversableOnce;
import scala.collection.IndexedSeq;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable..colon.colon;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class ExpressionCompiler
{
    private final SlotConfiguration slots;
    private final boolean readOnly;
    private final CodeGenerationMode codeGenerationMode;
    private final VariableNamer namer;

    public ExpressionCompiler( final SlotConfiguration slots, final boolean readOnly, final CodeGenerationMode codeGenerationMode, final VariableNamer namer )
    {
        this.slots = slots;
        this.readOnly = readOnly;
        this.codeGenerationMode = codeGenerationMode;
        this.namer = namer;
    }

    public static VariableNamer defaultGenerator$default$4()
    {
        return ExpressionCompiler$.MODULE$.defaultGenerator$default$4();
    }

    public static IntermediateRepresentation nullCheck$default$2( final Seq<IntermediateExpression> expressions )
    {
        return ExpressionCompiler$.MODULE$.nullCheck$default$2( var0 );
    }

    public static IntermediateRepresentation nullCheckIfRequired$default$2()
    {
        return ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2();
    }

    public static VariableNamer $lessinit$greater$default$4()
    {
        return ExpressionCompiler$.MODULE$.$lessinit$greater$default$4();
    }

    public static IntermediateRepresentation nullCheckIfRequired( final IntermediateExpression expression, final IntermediateRepresentation onNull )
    {
        return ExpressionCompiler$.MODULE$.nullCheckIfRequired( var0, var1 );
    }

    public static IntermediateRepresentation nullCheck( final Seq<IntermediateExpression> expressions, final IntermediateRepresentation onNull,
            final IntermediateRepresentation onNotNull )
    {
        return ExpressionCompiler$.MODULE$.nullCheck( var0, var1, var2 );
    }

    public static IntermediateRepresentation noValueOr( final Seq<IntermediateExpression> expressions, final IntermediateRepresentation onNotNull )
    {
        return ExpressionCompiler$.MODULE$.noValueOr( var0, var1 );
    }

    public static Method RELATIONSHIP_PROPERTY()
    {
        return ExpressionCompiler$.MODULE$.RELATIONSHIP_PROPERTY();
    }

    public static Method NODE_PROPERTY()
    {
        return ExpressionCompiler$.MODULE$.NODE_PROPERTY();
    }

    public static LocalVariable vPROPERTY_CURSOR()
    {
        return ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR();
    }

    public static IntermediateRepresentation PROPERTY_CURSOR()
    {
        return ExpressionCompiler$.MODULE$.PROPERTY_CURSOR();
    }

    public static LocalVariable vRELATIONSHIP_CURSOR()
    {
        return ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR();
    }

    public static IntermediateRepresentation RELATIONSHIP_CURSOR()
    {
        return ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR();
    }

    public static LocalVariable vNODE_CURSOR()
    {
        return ExpressionCompiler$.MODULE$.vNODE_CURSOR();
    }

    public static IntermediateRepresentation NODE_CURSOR()
    {
        return ExpressionCompiler$.MODULE$.NODE_CURSOR();
    }

    public static IntermediateRepresentation DB_ACCESS()
    {
        return ExpressionCompiler$.MODULE$.DB_ACCESS();
    }

    public static ExpressionCompiler defaultGenerator( final SlotConfiguration slots, final boolean readOnly, final CodeGenerationMode codeGenerationMode,
            final VariableNamer namer )
    {
        return ExpressionCompiler$.MODULE$.defaultGenerator( var0, var1, var2, var3 );
    }

    private static final IntermediateRepresentation declarations$1( final IntermediateExpression e )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( (Seq) ((TraversableLike) e.variables().distinct()).map( ( v ) -> {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign( v.typ(), v.name(), v.value() );
    }, scala.collection.Seq..MODULE$.canBuildFrom() ));
    }

    private static final IntermediateRepresentation loop$2( final List expressions, final String tempVariable$2 )
    {
        boolean var3 = false;
        colon var4 = null;
        if ( scala.collection.immutable.Nil..MODULE$.equals( expressions )){
        throw new InternalException( "we should never exhaust this loop" );
    } else{
        Object var2;
        if ( expressions instanceof colon )
        {
            var3 = true;
            var4 = (colon) expressions;
            IntermediateExpression expression = (IntermediateExpression) var4.head();
            List var7 = var4.tl$access$1();
            if ( scala.collection.immutable.Nil..MODULE$.equals( var7 )){
            var2 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( tempVariable$2,
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( expression, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) );
            return (IntermediateRepresentation) var2;
        }
        }

        if ( !var3 )
        {
            throw new MatchError( expressions );
        }
        else
        {
            IntermediateExpression expression = (IntermediateExpression) var4.head();
            List tail = var4.tl$access$1();
            var2 = expression.nullChecks().nonEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( tempVariable$2,
                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( expression, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                (IntermediateRepresentation) expression.nullChecks().reduceLeft( ( acc, current ) -> {
                    return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.or( acc, current );
                } ), loop$2( tail, tempVariable$2 ) )}))) :org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( tempVariable$2, expression.ir() );
            return (IntermediateRepresentation) var2;
        }
    }
    }

    private static final IntermediateRepresentation id$1( final IntermediateRepresentation value, final boolean nullable, final Manifest m )
    {
        IntermediateRepresentation getId = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( value, m ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "id", m, scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        return !nullable ? getId : org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( value,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ), getId);
    }

    private static final IntermediateRepresentation accessValue$1( final int i, final String listVar$1, final boolean singleValue$1 )
    {
        return singleValue$1 ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( "key" ) :org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( listVar$1 ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "value", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( i ) )})))
        ;
    }

    private static final IntermediateRepresentation loop$3( final List e, final Function1 ifNotBreakValue$1, final Function1 inner$1 )
    {
        boolean var4 = false;
        colon var5 = null;
        if ( scala.collection.immutable.Nil..MODULE$.equals( e )){
        throw new InternalException( "we should never get here" );
    } else{
        IntermediateRepresentation var3;
        if ( e instanceof colon )
        {
            var4 = true;
            var5 = (colon) e;
            IntermediateExpression a = (IntermediateExpression) var5.head();
            List var8 = var5.tl$access$1();
            if ( scala.collection.immutable.Nil..MODULE$.equals( var8 )){
            var3 = (IntermediateRepresentation) ifNotBreakValue$1.apply( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                    (Seq) inner$1.apply( a ) ));
            return var3;
        }
        }

        if ( !var4 )
        {
            throw new MatchError( e );
        }
        else
        {
            IntermediateExpression hd = (IntermediateExpression) var5.head();
            List tl = var5.tl$access$1();
            var3 = (IntermediateRepresentation) ifNotBreakValue$1.apply( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                    (Seq) ((SeqLike) inner$1.apply( hd )).$colon$plus( loop$3( tl, ifNotBreakValue$1, inner$1 ), scala.collection.Seq..MODULE$.canBuildFrom() )))
            ;
            return var3;
        }
    }
    }

    private static final IntermediateRepresentation loop$1( final List expressions, final String returnVariable$1, final Function1 conditionToCheck$1 )
    {
        boolean var4 = false;
        colon var5 = null;
        if ( scala.collection.immutable.Nil..MODULE$.equals( expressions )){
        throw new IllegalStateException();
    } else{
        IntermediateRepresentation var3;
        if ( expressions instanceof colon )
        {
            var4 = true;
            var5 = (colon) expressions;
            Tuple2 var7 = (Tuple2) var5.head();
            List var8 = var5.tl$access$1();
            if ( var7 != null )
            {
                IntermediateRepresentation toCheck = (IntermediateRepresentation) var7._1();
                IntermediateRepresentation toLoad = (IntermediateRepresentation) var7._2();
                if ( scala.collection.immutable.Nil..MODULE$.equals( var8 )){
                var3 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( (IntermediateRepresentation) conditionToCheck$1.apply( toCheck ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( returnVariable$1, toLoad ));
                return var3;
            }
            }
        }

        if ( !var4 )
        {
            throw new MatchError( expressions );
        }
        else
        {
            Tuple2 var11 = (Tuple2) var5.head();
            List tl = var5.tl$access$1();
            if ( var11 == null )
            {
                throw new MatchError( expressions );
            }
            else
            {
                IntermediateRepresentation toCheck = (IntermediateRepresentation) var11._1();
                IntermediateRepresentation toLoad = (IntermediateRepresentation) var11._2();
                var3 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ifElse( (IntermediateRepresentation) conditionToCheck$1.apply( toCheck ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( returnVariable$1, toLoad ), loop$1( tl, returnVariable$1,
                    conditionToCheck$1 ));
                return var3;
            }
        }
    }
    }

    private static final Tuple2 computeRepresentation$1( final IntermediateRepresentation ir, final Option nullCheck, final boolean nullable )
    {
        return new Tuple2( ir, nullable ? nullCheck :.MODULE$);
    }

    private static final Seq compileSteps$default$2$1()
    {
        return (Seq) scala.collection.mutable.ArrayBuffer..MODULE$.empty();
    }

    private static final Seq compileSteps$default$3$1()
    {
        return (Seq) scala.collection.mutable.ArrayBuffer..MODULE$.empty();
    }

    private static final Seq compileSteps$default$2$2()
    {
        return (Seq) scala.collection.mutable.ArrayBuffer..MODULE$.empty();
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public boolean readOnly()
    {
        return this.readOnly;
    }

    public CodeGenerationMode codeGenerationMode()
    {
        return this.codeGenerationMode;
    }

    public VariableNamer namer()
    {
        return this.namer;
    }

    public Option<CompiledExpression> compileExpression( final Expression e )
    {
        return this.intermediateCompileExpression( e ).map( ( expression ) -> {
            ClassDeclaration classDeclaration =
                    new ClassDeclaration( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$PACKAGE_NAME(),
                            ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$className(),.MODULE$,
            (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                            scala.reflect.ManifestFactory..MODULE$.classType( CompiledExpression.class ))}))),(Seq) scala.collection.Seq..
            MODULE$.empty(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop(), () -> {
                return expression.fields();
            }, (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new MethodDeclaration[]{
                    new MethodDeclaration( "evaluate", org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                            scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )), (Seq) scala.collection.Seq..MODULE$.apply(
                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Parameter[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "context",
                    scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class )),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "dbAccess", scala.reflect.ManifestFactory..MODULE$.classType(
                    DbAccess.class )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "params", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "cursors", scala.reflect.ManifestFactory..MODULE$.classType( ExpressionCursors.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "expressionVariables", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType(
                    AnyValue.class )))}))),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                            (Seq) ((TraversableLike) expression.variables().distinct()).map( ( v ) -> {
                                return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign( v.typ(), v.name(), v.value() );
                            }, scala.collection.Seq..MODULE$.canBuildFrom() )),
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( expression, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}))),
            org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
            MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7())}))));
            return (CompiledExpression) org.neo4j.codegen.api.CodeGeneration..
            MODULE$.compileAnonymousClass( classDeclaration, org.neo4j.codegen.api.CodeGeneration..MODULE$.createGenerator( this.codeGenerationMode() )).
            getDeclaredConstructor().newInstance();
        } );
    }

    public Option<CompiledProjection> compileProjection( final Map<String,Expression> projections )
    {
        return this.intermediateCompileProjection( projections ).map( ( expression ) -> {
            ClassDeclaration classDeclaration =
                    new ClassDeclaration( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$PACKAGE_NAME(),
                            ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$className(),.MODULE$,
            (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                            scala.reflect.ManifestFactory..MODULE$.classType( CompiledProjection.class ))}))),(Seq) scala.collection.Seq..
            MODULE$.empty(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop(), () -> {
                return expression.fields();
            }, (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new MethodDeclaration[]{
                    new MethodDeclaration( "project", org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                            scala.reflect.ManifestFactory..MODULE$.Unit()), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Parameter[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "context",
                            scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class )),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "dbAccess", scala.reflect.ManifestFactory..MODULE$.classType(
                    DbAccess.class )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "params", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "cursors", scala.reflect.ManifestFactory..MODULE$.classType( ExpressionCursors.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "expressionVariables", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType(
                    AnyValue.class )))}))),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                            (Seq) ((TraversableLike) expression.variables().distinct()).map( ( v ) -> {
                                return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign( v.typ(), v.name(), v.value() );
                            }, scala.collection.Seq..MODULE$.canBuildFrom() )), expression.ir()}))),org.neo4j.codegen.api.MethodDeclaration..
            MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..
            MODULE$.apply$default$7())}))));
            return (CompiledProjection) org.neo4j.codegen.api.CodeGeneration..
            MODULE$.compileAnonymousClass( classDeclaration, org.neo4j.codegen.api.CodeGeneration..MODULE$.createGenerator( this.codeGenerationMode() )).
            getDeclaredConstructor().newInstance();
        } );
    }

    public Option<CompiledGroupingExpression> compileGrouping( final Function1<SlotConfiguration,Seq<Tuple3<String,Expression,Object>>> orderedGroupings )
    {
        Seq orderedGroupingsBySlots = (Seq) orderedGroupings.apply( this.slots() );
        Seq compiled = (Seq) orderedGroupingsBySlots.withFilter( ( check$ifrefutable$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$compileGrouping$2( check$ifrefutable$1 ) );
        } ).flatMap( ( x$1 ) -> {
            if ( x$1 != null )
            {
                String k = (String) x$1._1();
                Expression v = (Expression) x$1._2();
                Iterable var2 = scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( v ).map( ( c ) -> {
                return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( this.slots().apply( k ) ), c);
            } ) ); return var2;
            }
            else
            {
                throw new MatchError( x$1 );
            }
        }, scala.collection.Seq..MODULE$.canBuildFrom());
        Object var10000;
        if ( compiled.size() < orderedGroupingsBySlots.size() )
        {
            var10000 = .MODULE$;
        }
        else
        {
            IntermediateGroupingExpression grouping = this.intermediateCompileGroupingExpression( compiled );
            ClassDeclaration classDeclaration =
                    new ClassDeclaration( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$PACKAGE_NAME(),
                            ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$className(),.MODULE$,
            (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                        scala.reflect.ManifestFactory..MODULE$.classType( CompiledGroupingExpression.class ))}))),(Seq) scala.collection.Seq..
            MODULE$.empty(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noop(), () -> {
            return (Seq) ((TraversableLike) grouping.projectKey().fields().$plus$plus( grouping.computeKey().fields(),
                    scala.collection.Seq..MODULE$.canBuildFrom())).$plus$plus( grouping.getKey().fields(), scala.collection.Seq..MODULE$.canBuildFrom());
        }, (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new MethodDeclaration[]{
                new MethodDeclaration( "projectGroupingKey", org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                        scala.reflect.ManifestFactory..MODULE$.Unit()), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Parameter[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "context",
                        scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class )),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "key", scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))}))),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{declarations$1( grouping.projectKey() ), grouping.projectKey().ir()}) )),
            org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
            MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7()),
            new MethodDeclaration( "computeGroupingKey", org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                    scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),(Seq) scala.collection.Seq..
            MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Parameter[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "context",
                            scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class )),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "dbAccess", scala.reflect.ManifestFactory..MODULE$.classType(
                    DbAccess.class )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "params", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "cursors", scala.reflect.ManifestFactory..MODULE$.classType( ExpressionCursors.class )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.param( "expressionVariables", scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType(
                    AnyValue.class )))}))),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{declarations$1( grouping.computeKey() ),
                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( grouping.computeKey(),
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )),org.neo4j.codegen.api.MethodDeclaration..
            MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..
            MODULE$.apply$default$7()),new MethodDeclaration( "getGroupingKey", org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),(Seq) scala.collection.Seq..
            MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Parameter[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.param( "context",
                            scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class ))}))),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{declarations$1( grouping.getKey() ),
                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( grouping.getKey(), ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )),
            org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$5(), org.neo4j.codegen.api.MethodDeclaration..
            MODULE$.apply$default$6(), org.neo4j.codegen.api.MethodDeclaration..MODULE$.apply$default$7())}))));
            var10000 = new Some( org.neo4j.codegen.api.CodeGeneration..MODULE$.compileAnonymousClass( classDeclaration,
                    org.neo4j.codegen.api.CodeGeneration..MODULE$.createGenerator( this.codeGenerationMode() )).getDeclaredConstructor().newInstance());
        }

        return (Option) var10000;
    }

    public Option<IntermediateExpression> intermediateCompileGroupingKey( final Seq<Expression> orderedGroupings )
    {
        Seq projections = (Seq) orderedGroupings.flatMap( ( expression ) -> {
            return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( expression ) );
        }, scala.collection.Seq..MODULE$.canBuildFrom());
        Object var10000;
        if ( projections.size() < orderedGroupings.size() )
        {
            var10000 = .MODULE$;
        }
        else
        {
            scala.Predef..MODULE$. assert (projections.nonEmpty());
            boolean singleValue = projections.size() == 1;
            Seq computeKeyOps = (Seq) projections.map( ( p ) -> {
                return ExpressionCompiler$.MODULE$.nullCheckIfRequired( p, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() );
            }, scala.collection.Seq..MODULE$.canBuildFrom());
            IntermediateRepresentation computeKey =
                    singleValue ? (IntermediateRepresentation) computeKeyOps.head() : org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "list", scala.reflect.ManifestFactory..MODULE$.classType(
                    VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
            MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( computeKeyOps,
                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))})));
            var10000 = new Some( new IntermediateExpression( computeKey, (Seq) projections.flatMap( ( x$2 ) -> {
                return x$2.fields();
            }, scala.collection.Seq..MODULE$.canBuildFrom() ), (Seq) projections.flatMap( ( x$3 ) -> {
                return x$3.variables();
            }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.Predef..MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
        }

        return (Option) var10000;
    }

    public Option<IntermediateExpression> intermediateCompileExpression( final Expression expression )
    {
        boolean var19 = false;
        FunctionInvocation var20 = null;
        boolean var21 = false;
        ListSlice var22 = null;
        boolean var23 = false;
        In var24 = null;
        boolean var25 = false;
        CaseExpression var26 = null;
        Object var2;
        if ( expression instanceof FunctionInvocation )
        {
            var19 = true;
            var20 = (FunctionInvocation) expression;
            if ( var20.function() instanceof AggregatingFunction )
            {
                var2 = .MODULE$;
                return (Option) var2;
            }
        }

        if ( var19 )
        {
            var2 = this.compileFunction( var20 );
        }
        else if ( expression instanceof Multiply )
        {
            Multiply var28 = (Multiply) expression;
            Expression lhs = var28.lhs();
            Expression rhs = var28.rhs();
            var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "multiply", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherMath.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )),
                    (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Set) l.nullChecks().$plus$plus( r.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        }
        else if ( expression instanceof Add )
        {
            Add var31 = (Add) expression;
            Expression lhs = var31.lhs();
            Expression rhs = var31.rhs();
            var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "add", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherMath.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )),
                    (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Set) l.nullChecks().$plus$plus( r.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        }
        else if ( expression instanceof UnaryAdd )
        {
            UnaryAdd var34 = (UnaryAdd) expression;
            Expression source = var34.rhs();
            var2 = this.intermediateCompileExpression( source );
        }
        else if ( expression instanceof Subtract )
        {
            Subtract var36 = (Subtract) expression;
            Expression lhs = var36.lhs();
            Expression rhs = var36.rhs();
            var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "subtract", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherMath.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )),
                    (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Set) l.nullChecks().$plus$plus( r.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        }
        else if ( expression instanceof UnarySubtract )
        {
            UnarySubtract var39 = (UnarySubtract) expression;
            Expression source = var39.rhs();
            var2 = this.intermediateCompileExpression( source ).map( ( argx ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "subtract", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherMath.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( "ZERO_INT",
                                scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                        IntegralValue.class )),argx.ir()}))),
                argx.fields(), argx.variables(), argx.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        }
        else if ( expression instanceof Divide )
        {
            Divide var41 = (Divide) expression;
            Expression lhs = var41.lhs();
            Expression rhs = var41.rhs();
            var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "divide", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherMath.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )),
                    (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                    MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "divideCheckForNull",
                                    scala.reflect.ManifestFactory..MODULE$.classType( CypherMath.class ),
                            scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{
                                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( l, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ),
                                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( r, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) ))}))),
                    IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        }
        else if ( expression instanceof Modulo )
        {
            Modulo var44 = (Modulo) expression;
            Expression lhs = var44.lhs();
            Expression rhs = var44.rhs();
            var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "modulo", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherMath.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )),
                    (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Set) l.nullChecks().$plus$plus( r.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        }
        else if ( expression instanceof Pow )
        {
            Pow var47 = (Pow) expression;
            Expression lhs = var47.lhs();
            Expression rhs = var47.rhs();
            var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "pow", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherMath.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )),
                    (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Set) l.nullChecks().$plus$plus( r.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        }
        else if ( expression instanceof DoubleLiteral )
        {
            DoubleLiteral var50 = (DoubleLiteral) expression;
            StaticField constant = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.staticConstant( this.namer().nextVariableName().toUpperCase(),
                Values.doubleValue( scala.Predef..MODULE$.Double2double( var50.value() ) ), scala.reflect.ManifestFactory..
            MODULE$.classType( DoubleValue.class ));
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( constant.name(),
                    scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ) ), (Seq) scala.collection.Seq..
            MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new StaticField[]{constant}) )),(Seq) scala.collection.Seq..
            MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), false));
        }
        else if ( expression instanceof IntegerLiteral )
        {
            IntegerLiteral var52 = (IntegerLiteral) expression;
            StaticField constant = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.staticConstant( this.namer().nextVariableName().toUpperCase(),
                Values.longValue( scala.Predef..MODULE$.Long2long( var52.value() ) ), scala.reflect.ManifestFactory..MODULE$.classType( LongValue.class ));
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( constant.name(),
                    scala.reflect.ManifestFactory..MODULE$.classType( LongValue.class ) ), (Seq) scala.collection.Seq..
            MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new StaticField[]{constant}) )),(Seq) scala.collection.Seq..
            MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), false));
        }
        else if ( expression instanceof StringLiteral )
        {
            StringLiteral var54 = (StringLiteral) expression;
            StaticField constant = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.staticConstant( this.namer().nextVariableName().toUpperCase(), Values.stringValue( var54.value() ),
                    scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ));
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( constant.name(),
                    scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ) ), (Seq) scala.collection.Seq..
            MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new StaticField[]{constant}) )),(Seq) scala.collection.Seq..
            MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), false));
        }
        else if ( expression instanceof Null )
        {
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(),
                    (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), (Set) scala.Predef..
            MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                            BoxesRunTime.boxToBoolean( true ) )}))),IntermediateExpression$.MODULE$.apply$default$5()));
        }
        else if ( expression instanceof True )
        {
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(),
                    (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), false));
        }
        else if ( expression instanceof False )
        {
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue(),
                    (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), false));
        }
        else
        {
            Object var10000;
            if ( expression instanceof ListLiteral )
            {
                ListLiteral var56 = (ListLiteral) expression;
                Seq args = var56.expressions();
                Seq in = (Seq) args.flatMap( ( expressionx ) -> {
                    return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( expressionx ) );
                }, scala.collection.Seq..MODULE$.canBuildFrom());
                if ( in.size() < args.size() )
                {
                    var10000 = .MODULE$;
                }
                else
                {
                    Seq fields = (Seq) in.foldLeft( scala.collection.Seq..MODULE$.empty(), ( a, b ) -> {
                    return (Seq) a.$plus$plus( b.fields(), scala.collection.Seq..MODULE$.canBuildFrom());
                });
                    Seq variables = (Seq) in.foldLeft( scala.collection.Seq..MODULE$.empty(), ( a, b ) -> {
                    return (Seq) a.$plus$plus( b.variables(), scala.collection.Seq..MODULE$.canBuildFrom());
                });
                    var10000 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "list", scala.reflect.ManifestFactory..MODULE$.classType(
                            VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( (Seq) in.map( ( i ) -> {
                            return ExpressionCompiler$.MODULE$.nullCheckIfRequired( i, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() );
                        }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))}))),
                    fields, variables, scala.Predef..MODULE$.Set().empty(), false));
                }

                var2 = var10000;
            }
            else if ( expression instanceof MapExpression )
            {
                MapExpression var61 = (MapExpression) expression;
                Seq items = var61.items();
                Map compiled = ((TraversableOnce) items.withFilter( ( check$ifrefutable$2 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileExpression$18( check$ifrefutable$2 ) );
                } ).flatMap( ( x$4 ) -> {
                    if ( x$4 != null )
                    {
                        PropertyKeyName k = (PropertyKeyName) x$4._1();
                        Expression v = (Expression) x$4._2();
                        Iterable var2 = scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( v ).map( ( c ) -> {
                        return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( k ), c);
                    } ) ); return var2;
                    }
                    else
                    {
                        throw new MatchError( x$4 );
                    }
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
                if ( compiled.size() < items.size() )
                {
                    var10000 = .MODULE$;
                }
                else
                {
                    String tempVariable = this.namer().nextVariableName();
                    Seq ops = (Seq) ((SeqLike) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new Product[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( tempVariable,
                                    scala.reflect.ManifestFactory..MODULE$.classType( MapValueBuilder.class )),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( tempVariable,
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.newInstance(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType(
                            MapValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                BoxesRunTime.boxToInteger( compiled.size() ) )}))))})))).$plus$plus( (GenTraversableOnce) compiled.map( ( x0$1 ) -> {
                    if ( x0$1 != null )
                    {
                        PropertyKeyName k = (PropertyKeyName) x0$1._1();
                        IntermediateExpression v = (IntermediateExpression) x0$1._2();
                        IntermediateRepresentation var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                tempVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.method( "add", scala.reflect.ManifestFactory..MODULE$.classType( MapValueBuilder.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( String.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( k.name() ),
                            ExpressionCompiler$.MODULE$.nullCheckIfRequired( v, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )})));
                        return var2;
                    }
                    else
                    {
                        throw new MatchError( x0$1 );
                    }
                }, scala.collection.immutable.Iterable..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom())).
                    $colon$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( tempVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.method( "build", scala.reflect.ManifestFactory..MODULE$.classType( MapValueBuilder.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( MapValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),
                    scala.collection.Seq..MODULE$.canBuildFrom());
                    var10000 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( ops ),
                            ((TraversableOnce) compiled.values().flatMap( ( x$5 ) -> {
                                return x$5.fields();
                            }, scala.collection.Iterable..MODULE$.canBuildFrom()) ).toSeq(), ((TraversableOnce) compiled.values().flatMap( ( x$6 ) -> {
                        return x$6.variables();
                    }, scala.collection.Iterable..MODULE$.canBuildFrom())).toSeq(), scala.Predef..MODULE$.Set().empty(), false));
                }

                var2 = var10000;
            }
            else
            {
                if ( expression instanceof MapProjection )
                {
                    throw new InternalException( "should have been rewritten away" );
                }

                if ( expression instanceof NestedPlanExpression )
                {
                    throw new InternalException( "should have been rewritten away" );
                }

                if ( expression instanceof DesugaredMapProjection )
                {
                    DesugaredMapProjection var66 = (DesugaredMapProjection) expression;
                    LogicalVariable name = var66.variable();
                    Seq items = var66.items();
                    boolean includeAllProps = var66.includeAllProps();
                    Seq expressions = (Seq) items.flatMap( ( i ) -> {
                        return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( i.exp() ) );
                    }, scala.collection.Seq..MODULE$.canBuildFrom());
                    if ( expressions.size() < items.size() )
                    {
                        var10000 = .MODULE$;
                    }
                    else
                    {
                        Seq expressionMap = (Seq) ((IterableLike) items.map( ( x$7 ) -> {
                            return x$7.key().name();
                        }, scala.collection.Seq..MODULE$.canBuildFrom())).zip( expressions, scala.collection.Seq..MODULE$.canBuildFrom());
                        String builderVar = this.namer().nextVariableName();
                        Seq buildMapValue =
                                expressionMap.nonEmpty() ? (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new Product[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( builderVar,
                                                scala.reflect.ManifestFactory..MODULE$.classType( MapValueBuilder.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( builderVar,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.newInstance(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType(
                                        MapValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                    BoxesRunTime.boxToInteger( expressionMap.size() ) )}))))})))).
                        $plus$plus( (GenTraversableOnce) expressionMap.map( ( x0$2 ) -> {
                            if ( x0$2 != null )
                            {
                                String key = (String) x0$2._1();
                                IntermediateExpression exp = (IntermediateExpression) x0$2._2();
                                IntermediateRepresentation var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        builderVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "add", scala.reflect.ManifestFactory..MODULE$.classType( MapValueBuilder.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( String.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( key ),
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( exp, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )})));
                                return var2;
                            }
                            else
                            {
                                throw new MatchError( x0$2 );
                            }
                        }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom()) :(Seq) scala.collection.Seq..
                        MODULE$.empty();
                        Tuple2 var75 = this.accessVariable( name.name() );
                        if ( var75 == null )
                        {
                            throw new MatchError( var75 );
                        }

                        IntermediateRepresentation accessName = (IntermediateRepresentation) var75._1();
                        Option maybeNullCheck = (Option) var75._2();
                        Tuple2 var18 = new Tuple2( accessName, maybeNullCheck );
                        IntermediateRepresentation accessName = (IntermediateRepresentation) var18._1();
                        Option maybeNullCheck = (Option) var18._2();
                        var10000 = !includeAllProps ? new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                (Seq) buildMapValue.$colon$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( builderVar ),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "build", scala.reflect.ManifestFactory..MODULE$.classType(
                                MapValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class )),scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.collection.Seq..MODULE$.canBuildFrom())),
                        (Seq) expressions.flatMap( ( x$9 ) -> {
                            return x$9.fields();
                        }, scala.collection.Seq..MODULE$.canBuildFrom()),(Seq) expressions.flatMap( ( x$10 ) -> {
                        return x$10.variables();
                    }, scala.collection.Seq..MODULE$.canBuildFrom()),scala.Option..
                        MODULE$.option2Iterable( maybeNullCheck ).toSet(), IntermediateExpression$.MODULE$.apply$default$5())) :
                        (buildMapValue.isEmpty() ? new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                (Seq) buildMapValue.$plus$plus( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asMapValue",
                                                scala.reflect.ManifestFactory..MODULE$.classType( CypherCoercions.class ),
                                        scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                                AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),
                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{accessName, ExpressionCompiler$.MODULE$.DB_ACCESS(),
                            ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                            ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}) ))}))),scala.collection.Seq..MODULE$.canBuildFrom())),
                        (Seq) expressions.flatMap( ( x$11 ) -> {
                            return x$11.fields();
                        }, scala.collection.Seq..MODULE$.canBuildFrom()),(Seq) ((TraversableLike) expressions.flatMap( ( x$12 ) -> {
                        return x$12.variables();
                    }, scala.collection.Seq..MODULE$.canBuildFrom())).
                        $plus$plus( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                                scala.collection.Seq..MODULE$.canBuildFrom()),scala.Option..
                        MODULE$.option2Iterable( maybeNullCheck ).toSet(), IntermediateExpression$.MODULE$.apply$default$5())) :new Some(
                            new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                    (Seq) buildMapValue.$plus$plus( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asMapValue",
                                                    scala.reflect.ManifestFactory..MODULE$.classType( CypherCoercions.class ),
                                            scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ),
                                    scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                            DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),
                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{accessName, ExpressionCompiler$.MODULE$.DB_ACCESS(),
                            ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                            ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}) )),org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.method( "updatedWith", scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( MapValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class )),scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( builderVar ),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "build", scala.reflect.ManifestFactory..MODULE$.classType(
                                MapValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class )),scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))})))}))),scala.collection.Seq..MODULE$.canBuildFrom())),
                        (Seq) expressions.flatMap( ( x$13 ) -> {
                            return x$13.fields();
                        }, scala.collection.Seq..MODULE$.canBuildFrom()),(Seq) ((TraversableLike) expressions.flatMap( ( x$14 ) -> {
                        return x$14.variables();
                    }, scala.collection.Seq..MODULE$.canBuildFrom())).
                        $plus$plus( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                                scala.collection.Seq..MODULE$.canBuildFrom()),scala.Option..
                        MODULE$.option2Iterable( maybeNullCheck ).toSet(), IntermediateExpression$.MODULE$.apply$default$5())));
                    }

                    var2 = var10000;
                }
                else
                {
                    if ( expression instanceof ListSlice )
                    {
                        var21 = true;
                        var22 = (ListSlice) expression;
                        Expression collection = var22.list();
                        Option var81 = var22.from();
                        Option var82 = var22.to();
                        if (.MODULE$.equals( var81 ) && .MODULE$.equals( var82 )){
                        var2 = this.intermediateCompileExpression( collection );
                        return (Option) var2;
                    }
                    }

                    if ( var21 )
                    {
                        Expression collection = var22.list();
                        Option var84 = var22.from();
                        Option var85 = var22.to();
                        if ( var84 instanceof Some )
                        {
                            Some var86 = (Some) var84;
                            Expression from = (Expression) var86.value();
                            if (.MODULE$.equals( var85 )){
                            var2 = this.intermediateCompileExpression( collection ).flatMap( ( c ) -> {
                                return this.intermediateCompileExpression( from ).map( ( f ) -> {
                                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "fromSlice",
                                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),scala.reflect.ManifestFactory..
                                    MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{c.ir(), f.ir()}) )),
                                    (Seq) c.fields().$plus$plus( f.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                    (Seq) c.variables().$plus$plus( f.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                    (Set) c.nullChecks().$plus$plus( f.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                                } );
                            } ); return (Option) var2;
                        }
                        }
                    }

                    if ( var21 )
                    {
                        Expression collection = var22.list();
                        Option var89 = var22.from();
                        Option var90 = var22.to();
                        if (.MODULE$.equals( var89 ) && var90 instanceof Some){
                        Some var91 = (Some) var90;
                        Expression to = (Expression) var91.value();
                        var2 = this.intermediateCompileExpression( collection ).flatMap( ( c ) -> {
                            return this.intermediateCompileExpression( to ).map( ( t ) -> {
                                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toSlice",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),scala.reflect.ManifestFactory..
                                MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{c.ir(), t.ir()}) )),
                                (Seq) c.fields().$plus$plus( t.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Seq) c.variables().$plus$plus( t.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Set) c.nullChecks().$plus$plus( t.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } ); return (Option) var2;
                    }
                    }

                    if ( var21 )
                    {
                        Expression collection = var22.list();
                        Option var94 = var22.from();
                        Option var95 = var22.to();
                        if ( var94 instanceof Some )
                        {
                            Some var96 = (Some) var94;
                            Expression from = (Expression) var96.value();
                            if ( var95 instanceof Some )
                            {
                                Some var98 = (Some) var95;
                                Expression to = (Expression) var98.value();
                                var2 = this.intermediateCompileExpression( collection ).flatMap( ( c ) -> {
                                    return this.intermediateCompileExpression( from ).flatMap( ( f ) -> {
                                        return this.intermediateCompileExpression( to ).map( ( t ) -> {
                                            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "fullSlice",
                                                    scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),scala.reflect.ManifestFactory..
                                            MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                                            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{c.ir(), f.ir(), t.ir()}) )),
                                            (Seq) ((TraversableLike) c.fields().$plus$plus( f.fields(), scala.collection.Seq..MODULE$.canBuildFrom())).
                                            $plus$plus( t.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                            (Seq) ((TraversableLike) c.variables().$plus$plus( f.variables(), scala.collection.Seq..MODULE$.canBuildFrom())).
                                            $plus$plus( t.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                            (Set) c.nullChecks().$plus$plus( f.nullChecks() ).$plus$plus(
                                                    t.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                                        } );
                                    } );
                                } ); return (Option) var2;
                            }
                        }
                    }

                    if ( expression instanceof Variable )
                    {
                        Variable var100 = (Variable) expression;
                        String name = var100.name();
                        Tuple2 var103 = this.accessVariable( name );
                        if ( var103 == null )
                        {
                            throw new MatchError( var103 );
                        }

                        IntermediateRepresentation variableAccess = (IntermediateRepresentation) var103._1();
                        Option nullCheck = (Option) var103._2();
                        Tuple2 var17 = new Tuple2( variableAccess, nullCheck );
                        IntermediateRepresentation variableAccess = (IntermediateRepresentation) var17._1();
                        Option nullCheck = (Option) var17._2();
                        var2 = new Some( new IntermediateExpression( variableAccess, (Seq) scala.collection.Seq..MODULE$.empty(),
                                (Seq) scala.collection.Seq..MODULE$.empty(), scala.Option..
                        MODULE$.option2Iterable( nullCheck ).toSet(), IntermediateExpression$.MODULE$.apply$default$5()));
                    }
                    else if ( expression instanceof ExpressionVariable )
                    {
                        ExpressionVariable var108 = (ExpressionVariable) expression;
                        IntermediateRepresentation varLoad = this.loadExpressionVariable( var108 );
                        var2 = new Some( new IntermediateExpression( varLoad, (Seq) scala.collection.Seq..MODULE$.empty(),
                                (Seq) scala.collection.Seq..MODULE$.empty(), (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( varLoad,
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())}))),IntermediateExpression$.MODULE$.apply$default$5()))
                        ;
                    }
                    else if ( expression instanceof SingleIterablePredicate )
                    {
                        SingleIterablePredicate var110 = (SingleIterablePredicate) expression;
                        FilterScope scope = var110.scope();
                        Expression collectionExpression = var110.expression();
                        ExpressionVariable innerVariable = org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast( scope.variable() );
                        String iterVariable = this.namer().nextVariableName();
                        var2 = this.intermediateCompileExpression( collectionExpression ).flatMap( ( collectionx ) -> {
                            return this.intermediateCompileExpression( (Expression) scope.innerPredicate().get() ).map( ( inner ) -> {
                                String listVar = this.namer().nextVariableName();
                                String currentValue = this.namer().nextVariableName();
                                String matches = this.namer().nextVariableName();
                                String isNull = this.namer().nextVariableName();
                                String isMatch = this.namer().nextVariableName();
                                String result = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),result, org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( listVar,
                                                scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( listVar,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{collectionx.ir()}) ))),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( matches, scala.reflect.ManifestFactory..MODULE$.Int()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( matches, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( 0 ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( isNull, scala.reflect.ManifestFactory..MODULE$.Boolean()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isNull, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( iterVariable, scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( iterVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        listVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.lessThan(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        matches ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( 2 ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare(
                                                currentValue, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( currentValue,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ))),
                                this.setExpressionVariable( innerVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( isMatch, scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( isMatch,
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( inner,
                                                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( matches, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.add(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        matches ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( 1 ) )))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isNull, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )))}))))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.lessThan(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        matches ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( 2 ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( isNull )),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.noValue(), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "booleanValue",
                                        scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( matches ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( 1 ) ))}))))})))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                result )})));
                                IntermediateRepresentation resultNullCheck = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( result ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops,
                                        (Seq) collectionx.fields().$plus$plus( inner.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) collectionx.variables().$plus$plus( inner.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Set) collectionx.nullChecks().$plus( resultNullCheck ), IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof NoneIterablePredicate )
                    {
                        NoneIterablePredicate var115 = (NoneIterablePredicate) expression;
                        FilterScope scope = var115.scope();
                        Expression collectionExpression = var115.expression();
                        ExpressionVariable innerVariable = org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast( scope.variable() );
                        String iterVariable = this.namer().nextVariableName();
                        var2 = this.intermediateCompileExpression( collectionExpression ).flatMap( ( collectionx ) -> {
                            return this.intermediateCompileExpression( (Expression) scope.innerPredicate().get() ).map( ( inner ) -> {
                                String listVar = this.namer().nextVariableName();
                                String currentValue = this.namer().nextVariableName();
                                String isMatch = this.namer().nextVariableName();
                                String isNull = this.namer().nextVariableName();
                                String result = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),result, org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( listVar,
                                                scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( listVar,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{collectionx.ir()}) ))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( iterVariable, scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( iterVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        listVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( isMatch, scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isMatch, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.noValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue())),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( isNull, scala.reflect.ManifestFactory..MODULE$.Boolean()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isNull, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare(
                                                currentValue, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( currentValue,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ))),
                                this.setExpressionVariable( innerVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( isMatch,
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( inner,
                                                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isNull, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )))}))))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isNull ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue())),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "booleanValue",
                                        scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( isMatch ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue())}))))})))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                result )})));
                                IntermediateRepresentation resultNullCheck = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( result ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops,
                                        (Seq) collectionx.fields().$plus$plus( inner.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) collectionx.variables().$plus$plus( inner.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Set) collectionx.nullChecks().$plus( resultNullCheck ), IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof AnyIterablePredicate )
                    {
                        AnyIterablePredicate var120 = (AnyIterablePredicate) expression;
                        FilterScope scope = var120.scope();
                        Expression collectionExpression = var120.expression();
                        ExpressionVariable innerVariable = org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast( scope.variable() );
                        String iterVariable = this.namer().nextVariableName();
                        var2 = this.intermediateCompileExpression( collectionExpression ).flatMap( ( collectionx ) -> {
                            return this.intermediateCompileExpression( (Expression) scope.innerPredicate().get() ).map( ( inner ) -> {
                                String listVar = this.namer().nextVariableName();
                                String currentValue = this.namer().nextVariableName();
                                String isMatch = this.namer().nextVariableName();
                                String isNull = this.namer().nextVariableName();
                                String result = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),result, org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( listVar,
                                                scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( listVar,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{collectionx.ir()}) ))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( isMatch, scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isMatch, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( isNull, scala.reflect.ManifestFactory..MODULE$.Boolean()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isNull, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( iterVariable, scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( iterVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        listVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare(
                                                currentValue, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( currentValue,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ))),
                                this.setExpressionVariable( innerVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( isMatch,
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( inner,
                                                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isNull, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )))}))))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isNull ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue())),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.load( isMatch ))})))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                result )})));
                                IntermediateRepresentation resultNullCheck = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( result ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops,
                                        (Seq) collectionx.fields().$plus$plus( inner.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) collectionx.variables().$plus$plus( inner.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Set) collectionx.nullChecks().$plus( resultNullCheck ), IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof AllIterablePredicate )
                    {
                        AllIterablePredicate var125 = (AllIterablePredicate) expression;
                        FilterScope scope = var125.scope();
                        Expression collectionExpression = var125.expression();
                        ExpressionVariable innerVariable = org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast( scope.variable() );
                        String iterVariable = this.namer().nextVariableName();
                        var2 = this.intermediateCompileExpression( collectionExpression ).flatMap( ( collectionx ) -> {
                            return this.intermediateCompileExpression( (Expression) scope.innerPredicate().get() ).map( ( inner ) -> {
                                String listVar = this.namer().nextVariableName();
                                String currentValue = this.namer().nextVariableName();
                                String isMatch = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( listVar,
                                                scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( listVar,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{collectionx.ir()}) ))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( isMatch, scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( isMatch, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.declare( iterVariable, scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.assign( iterVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        listVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        isMatch ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare(
                                                currentValue, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( currentValue,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ))),
                                this.setExpressionVariable( innerVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( isMatch,
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( inner, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) )})))))}))))
                                ;
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                isMatch )})));
                                IntermediateRepresentation resultNullCheck = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( isMatch ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops,
                                        (Seq) collectionx.fields().$plus$plus( inner.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) collectionx.variables().$plus$plus( inner.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Set) collectionx.nullChecks().$plus( resultNullCheck ), IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof ListComprehension )
                    {
                        boolean var16;
                        ExtractScope scope;
                        Expression list;
                        Option var134;
                        label517:
                        {
                            ListComprehension var130 = (ListComprehension) expression;
                            scope = var130.scope();
                            list = var130.expression();
                            var134 = scope.innerPredicate();
                            if ( var134 instanceof Some )
                            {
                                Some var135 = (Some) var134;
                                if ( var135.value() instanceof True )
                                {
                                    var16 = true;
                                    break label517;
                                }
                            }

                            if (.MODULE$.equals( var134 )){
                            var16 = true;
                        } else{
                            var16 = false;
                        }
                        }

                        Option var15;
                        if ( var16 )
                        {
                            var15 = this.intermediateCompileExpression( list );
                        }
                        else
                        {
                            if ( !(var134 instanceof Some) )
                            {
                                throw new MatchError( var134 );
                            }

                            Some var136 = (Some) var134;
                            Expression inner = (Expression) var136.value();
                            var15 = this.filterExpression( this.intermediateCompileExpression( list ), inner,
                                    org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast( scope.variable() ));
                        }

                        Option var138 = scope.extractExpression();
                        Option var14;
                        if (.MODULE$.equals( var138 )){
                        var14 = var15;
                    } else{
                        if ( !(var138 instanceof Some) )
                        {
                            throw new MatchError( var138 );
                        }

                        Some var139 = (Some) var138;
                        Expression extract = (Expression) var139.value();
                        var14 = this.extractExpression( var15, extract, org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast(
                                scope.variable() ));
                    }

                        var2 = var14;
                    }
                    else if ( expression instanceof ReduceExpression )
                    {
                        ReduceExpression var141 = (ReduceExpression) expression;
                        ReduceScope scope = var141.scope();
                        Expression initExpression = var141.init();
                        Expression collectionExpression = var141.list();
                        ExpressionVariable accVar = org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast( scope.accumulator() );
                        ExpressionVariable innerVar = org.neo4j.cypher.internal.runtime.ast.ExpressionVariable..MODULE$.cast( scope.variable() );
                        String iterVariable = this.namer().nextVariableName();
                        var2 = this.intermediateCompileExpression( collectionExpression ).flatMap( ( collectionx ) -> {
                            return this.intermediateCompileExpression( initExpression ).flatMap( ( init ) -> {
                                return this.intermediateCompileExpression( scope.expression() ).map( ( inner ) -> {
                                    String listVar = this.namer().nextVariableName();
                                    String currentValue = this.namer().nextVariableName();
                                    Seq ops = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare(
                                                    listVar, scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( listVar,
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList",
                                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{collectionx.ir()}) ))),
                                    this.setExpressionVariable( accVar, ExpressionCompiler$.MODULE$.nullCheckIfRequired( init,
                                            ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.declare( iterVariable, scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                            scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.assign( iterVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                            listVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType(
                                            ListValue.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                            iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                            scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.block( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare(
                                                    currentValue, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( currentValue,
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                            iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                                            scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
                                    MODULE$.classType( AnyValue.class ))),
                                    this.setExpressionVariable( innerVar, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )),
                                    this.setExpressionVariable( accVar, ExpressionCompiler$.MODULE$.nullCheckIfRequired( inner,
                                            ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) )}))))),this.loadExpressionVariable( accVar )})));
                                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( ops ),
                                    (Seq) ((TraversableLike) collectionx.fields().$plus$plus( inner.fields(), scala.collection.Seq..MODULE$.canBuildFrom())).
                                    $plus$plus( init.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                    (Seq) ((TraversableLike) collectionx.variables().$plus$plus( inner.variables(),
                                            scala.collection.Seq..MODULE$.canBuildFrom())).
                                    $plus$plus( init.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                    collectionx.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                } );
                            } );
                        } );
                    }
                    else if ( expression instanceof Or )
                    {
                        Or var148 = (Or) expression;
                        Expression lhs = var148.lhs();
                        Expression rhs = var148.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                IntermediateExpression left = org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..
                                MODULE$.isPredicate( lhs ) ? l : this.coerceToPredicate( l );
                                IntermediateExpression right = org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..
                                MODULE$.isPredicate( rhs ) ? r : this.coerceToPredicate( r );
                                return this.generateOrs( new colon( left, new colon( right, scala.collection.immutable.Nil..MODULE$ ) ));
                            } );
                        } );
                    }
                    else if ( expression instanceof Ors )
                    {
                        Ors var151 = (Ors) expression;
                        Set expressions = var151.exprs();
                        Option compiled = (Option) expressions.foldLeft( new Some( scala.collection.immutable.List..MODULE$.empty() ), ( acc, current ) -> {
                        return acc.flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( current ).map( ( e ) -> {
                                return (List) l.$colon$plus( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( e ),
                                BoxesRunTime.boxToBoolean( org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..MODULE$.isPredicate( current ))),
                                scala.collection.immutable.List..MODULE$.canBuildFrom());
                            } );
                        } );
                    });
                        var2 = compiled.map( ( ex ) -> {
                            IntermediateExpression var2;
                            if ( scala.collection.immutable.Nil..MODULE$.equals( ex )){
                                var2 = new IntermediateExpression(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), (Seq) scala.collection.Seq..
                                MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
                                MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5());
                            } else{
                                if ( ex instanceof colon )
                                {
                                    colon var4 = (colon) ex;
                                    Tuple2 var5 = (Tuple2) var4.head();
                                    List var6 = var4.tl$access$1();
                                    if ( var5 != null )
                                    {
                                        IntermediateExpression a = (IntermediateExpression) var5._1();
                                        boolean isPredicate = var5._2$mcZ$sp();
                                        if ( scala.collection.immutable.Nil..MODULE$.equals( var6 )){
                                        var2 = isPredicate ? a : this.coerceToPredicate( a );
                                        return var2;
                                    }
                                    }
                                }

                                List coerced = (List) ex.map( ( x0$3 ) -> {
                                    IntermediateExpression var2;
                                    if ( x0$3 != null )
                                    {
                                        IntermediateExpression px = (IntermediateExpression) x0$3._1();
                                        boolean var5 = x0$3._2$mcZ$sp();
                                        if ( var5 )
                                        {
                                            var2 = px;
                                            return var2;
                                        }
                                    }

                                    if ( x0$3 == null )
                                    {
                                        throw new MatchError( x0$3 );
                                    }
                                    else
                                    {
                                        IntermediateExpression p = (IntermediateExpression) x0$3._1();
                                        boolean var7 = x0$3._2$mcZ$sp();
                                        if ( var7 )
                                        {
                                            throw new MatchError( x0$3 );
                                        }
                                        else
                                        {
                                            var2 = this.coerceToPredicate( p );
                                            return var2;
                                        }
                                    }
                                }, scala.collection.immutable.List..MODULE$.canBuildFrom());
                                var2 = this.generateOrs( coerced );
                            }

                            return var2;
                        } );
                    }
                    else if ( expression instanceof Xor )
                    {
                        Xor var154 = (Xor) expression;
                        Expression lhs = var154.lhs();
                        Expression rhs = var154.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                IntermediateExpression left = org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..
                                MODULE$.isPredicate( lhs ) ? l : this.coerceToPredicate( l );
                                IntermediateExpression right = org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..
                                MODULE$.isPredicate( rhs ) ? r : this.coerceToPredicate( r );
                                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "xor",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ),scala.reflect.ManifestFactory..
                                MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{left.ir(), right.ir()}) )),
                                (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Set) l.nullChecks().$plus$plus( r.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof And )
                    {
                        And var157 = (And) expression;
                        Expression lhs = var157.lhs();
                        Expression rhs = var157.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                IntermediateExpression left = org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..
                                MODULE$.isPredicate( lhs ) ? l : this.coerceToPredicate( l );
                                IntermediateExpression right = org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..
                                MODULE$.isPredicate( rhs ) ? r : this.coerceToPredicate( r );
                                return this.generateAnds( new colon( left, new colon( right, scala.collection.immutable.Nil..MODULE$ ) ));
                            } );
                        } );
                    }
                    else if ( expression instanceof Ands )
                    {
                        Ands var160 = (Ands) expression;
                        Set expressions = var160.exprs();
                        Option compiled = (Option) expressions.foldLeft( new Some( scala.collection.immutable.List..MODULE$.empty() ), ( acc, current ) -> {
                        return acc.flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( current ).map( ( e ) -> {
                                return (List) l.$colon$plus( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( e ),
                                BoxesRunTime.boxToBoolean( org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..MODULE$.isPredicate( current ))),
                                scala.collection.immutable.List..MODULE$.canBuildFrom());
                            } );
                        } );
                    });
                        var2 = compiled.map( ( ex ) -> {
                            IntermediateExpression var2;
                            if ( scala.collection.immutable.Nil..MODULE$.equals( ex )){
                                var2 = new IntermediateExpression(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), (Seq) scala.collection.Seq..
                                MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
                                MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5());
                            } else{
                                if ( ex instanceof colon )
                                {
                                    colon var4 = (colon) ex;
                                    Tuple2 var5 = (Tuple2) var4.head();
                                    List var6 = var4.tl$access$1();
                                    if ( var5 != null )
                                    {
                                        IntermediateExpression a = (IntermediateExpression) var5._1();
                                        boolean isPredicate = var5._2$mcZ$sp();
                                        if ( scala.collection.immutable.Nil..MODULE$.equals( var6 )){
                                        var2 = isPredicate ? a : this.coerceToPredicate( a );
                                        return var2;
                                    }
                                    }
                                }

                                List coerced = (List) ex.map( ( x0$4 ) -> {
                                    IntermediateExpression var2;
                                    if ( x0$4 != null )
                                    {
                                        IntermediateExpression px = (IntermediateExpression) x0$4._1();
                                        boolean var5 = x0$4._2$mcZ$sp();
                                        if ( var5 )
                                        {
                                            var2 = px;
                                            return var2;
                                        }
                                    }

                                    if ( x0$4 == null )
                                    {
                                        throw new MatchError( x0$4 );
                                    }
                                    else
                                    {
                                        IntermediateExpression p = (IntermediateExpression) x0$4._1();
                                        boolean var7 = x0$4._2$mcZ$sp();
                                        if ( var7 )
                                        {
                                            throw new MatchError( x0$4 );
                                        }
                                        else
                                        {
                                            var2 = this.coerceToPredicate( p );
                                            return var2;
                                        }
                                    }
                                }, scala.collection.immutable.List..MODULE$.canBuildFrom());
                                var2 = this.generateAnds( coerced );
                            }

                            return var2;
                        } );
                    }
                    else if ( expression instanceof AndedPropertyInequalities )
                    {
                        AndedPropertyInequalities var163 = (AndedPropertyInequalities) expression;
                        NonEmptyList inequalities = var163.inequalities();
                        Seq compiledInequalities = (Seq) inequalities.toIndexedSeq().flatMap( ( i ) -> {
                            return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( (Expression) i ) );
                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                        var2 = compiledInequalities.size() < inequalities.size() ? .MODULE$:
                    new Some( this.generateAnds( compiledInequalities.toList() ) );
                    }
                    else if ( expression instanceof Not )
                    {
                        Not var166 = (Not) expression;
                        Expression arg = var166.rhs();
                        var2 = this.intermediateCompileExpression( arg ).map( ( a ) -> {
                            IntermediateExpression in = org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..
                            MODULE$.isPredicate( arg ) ? a : this.coerceToPredicate( a );
                            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "not", scala.reflect.ManifestFactory..MODULE$.classType(
                                    CypherBoolean.class ),scala.reflect.ManifestFactory..MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                        } );
                    }
                    else if ( expression instanceof Equals )
                    {
                        Equals var168 = (Equals) expression;
                        Expression lhs = var168.lhs();
                        Expression rhs = var168.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateExpression[]{l, r}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "equals",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops, (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                            } );
                        } );
                    }
                    else if ( expression instanceof NotEquals )
                    {
                        NotEquals var171 = (NotEquals) expression;
                        Expression lhs = var171.lhs();
                        Expression rhs = var171.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateExpression[]{l, r}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "notEquals",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops, (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                            } );
                        } );
                    }
                    else if ( expression instanceof CoerceToPredicate )
                    {
                        CoerceToPredicate var174 = (CoerceToPredicate) expression;
                        Expression inner = var174.inner();
                        var2 = this.intermediateCompileExpression( inner ).map( ( ex ) -> {
                            return this.coerceToPredicate( ex );
                        } );
                    }
                    else if ( expression instanceof RegexMatch )
                    {
                        RegexMatch var176 = (RegexMatch) expression;
                        Expression lhs = var176.lhs();
                        Expression rhs = var176.rhs();
                        Option var13;
                        if ( rhs instanceof StringLiteral )
                        {
                            StringLiteral var180 = (StringLiteral) rhs;
                            String name = var180.value();
                            var13 = this.intermediateCompileExpression( lhs ).map( ( ex ) -> {
                                InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.field( this.namer().nextVariableName(), scala.reflect.ManifestFactory..MODULE$.classType( Pattern.class ));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNull(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.setField( f,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "compile",
                                        scala.reflect.ManifestFactory..MODULE$.classType( Pattern.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Pattern.class ), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..
                                MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( name )}))))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "regex",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( Pattern.class )),scala.Predef..
                                MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( ex.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f )})))})));
                                return new IntermediateExpression( ops, (Seq) ex.fields().$colon$plus( f, scala.collection.Seq..MODULE$.canBuildFrom() ),
                                ex.variables(), (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( ex.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )) )}))),
                                IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        }
                        else
                        {
                            var13 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "regex",
                                            scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ),scala.reflect.ManifestFactory..
                                    MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),scala.Predef..
                                    MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( l.ir(),
                                                    scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asTextValue",
                                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{r.ir()}) ))}))),
                                    (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                    (Set) r.nullChecks().$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( l.ir(),
                                            scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )))),
                                    IntermediateExpression$.MODULE$.apply$default$5());
                                } );
                            } );
                        }

                        var2 = var13;
                    }
                    else if ( expression instanceof StartsWith )
                    {
                        StartsWith var182 = (StartsWith) expression;
                        Expression lhs = var182.lhs();
                        Expression rhs = var182.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "booleanValue",
                                        scala.reflect.ManifestFactory..MODULE$.classType( Values.class ),scala.reflect.ManifestFactory..
                                MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( l.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "startsWith",
                                        scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( r.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ))})))}))),
                                (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( l.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType(
                                                TextValue.class )) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.not( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( r.ir(),
                                        scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )))}))),
                                IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof EndsWith )
                    {
                        EndsWith var185 = (EndsWith) expression;
                        Expression lhs = var185.lhs();
                        Expression rhs = var185.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "booleanValue",
                                        scala.reflect.ManifestFactory..MODULE$.classType( Values.class ),scala.reflect.ManifestFactory..
                                MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( l.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "endsWith",
                                        scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( r.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ))})))}))),
                                (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( l.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType(
                                                TextValue.class )) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.not( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( r.ir(),
                                        scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )))}))),
                                IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof Contains )
                    {
                        Contains var188 = (Contains) expression;
                        Expression lhs = var188.lhs();
                        Expression rhs = var188.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "booleanValue",
                                        scala.reflect.ManifestFactory..MODULE$.classType( Values.class ),scala.reflect.ManifestFactory..
                                MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( l.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "contains",
                                        scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                                MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( r.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ))})))}))),
                                (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.not(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( l.ir(),
                                                scala.reflect.ManifestFactory..MODULE$.classType(
                                                TextValue.class )) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.not( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.instanceOf( r.ir(),
                                        scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )))}))),
                                IntermediateExpression$.MODULE$.apply$default$5());
                            } );
                        } );
                    }
                    else if ( expression instanceof IsNull )
                    {
                        IsNull var191 = (IsNull) expression;
                        Expression test = var191.lhs();
                        var2 = this.intermediateCompileExpression( test ).map( ( ex ) -> {
                            return new IntermediateExpression(
                                    ExpressionCompiler$.MODULE$.nullCheck( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{ex}) ),
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(),org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.falseValue()),ex.fields(), ex.variables(), scala.Predef..
                            MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5());
                        } );
                    }
                    else if ( expression instanceof IsNotNull )
                    {
                        IsNotNull var193 = (IsNotNull) expression;
                        Expression test = var193.lhs();
                        var2 = this.intermediateCompileExpression( test ).map( ( ex ) -> {
                            return new IntermediateExpression(
                                    ExpressionCompiler$.MODULE$.nullCheck( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{ex}) ),
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue(),org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.trueValue()),ex.fields(), ex.variables(), scala.Predef..
                            MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5());
                        } );
                    }
                    else if ( expression instanceof LessThan )
                    {
                        LessThan var195 = (LessThan) expression;
                        Expression lhs = var195.lhs();
                        Expression rhs = var195.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.oneTime(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateExpression[]{l, r}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "lessThan",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                                        AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                        scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                        scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops, (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                            } );
                        } );
                    }
                    else if ( expression instanceof LessThanOrEqual )
                    {
                        LessThanOrEqual var198 = (LessThanOrEqual) expression;
                        Expression lhs = var198.lhs();
                        Expression rhs = var198.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateExpression[]{l, r}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "lessThanOrEqual",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops, (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                            } );
                        } );
                    }
                    else if ( expression instanceof GreaterThan )
                    {
                        GreaterThan var201 = (GreaterThan) expression;
                        Expression lhs = var201.lhs();
                        Expression rhs = var201.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateExpression[]{l, r}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "greaterThan",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops, (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                            } );
                        } );
                    }
                    else if ( expression instanceof GreaterThanOrEqual )
                    {
                        GreaterThanOrEqual var204 = (GreaterThanOrEqual) expression;
                        Expression lhs = var204.lhs();
                        Expression rhs = var204.rhs();
                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateExpression[]{l, r}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "greaterThanOrEqual",
                                        scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(), r.ir()}) )))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                return new IntermediateExpression( ops, (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                            } );
                        } );
                    }
                    else
                    {
                        if ( expression instanceof In )
                        {
                            var23 = true;
                            var24 = (In) expression;
                            Expression var207 = var24.rhs();
                            if ( var207 instanceof ListLiteral )
                            {
                                ListLiteral var208 = (ListLiteral) var207;
                                Seq expressions = var208.expressions();
                                if ( expressions.isEmpty() )
                                {
                                    var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue(),
                                            (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
                                    MODULE$.Set().empty(), false));
                                    return (Option) var2;
                                }
                            }
                        }

                        if ( var23 )
                        {
                            Expression lhs = var24.lhs();
                            Expression var211 = var24.rhs();
                            if ( var211 instanceof ListLiteral )
                            {
                                ListLiteral var212 = (ListLiteral) var211;
                                Seq expressions = var212.expressions();
                                if ( expressions.forall( ( ex ) -> {
                                    return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileExpression$93( ex ) );
                                } ) )
                                {
                                    HashSet set = this.setFromLiterals( expressions );
                                    boolean containsNull = expressions.exists( ( x0$5 ) -> {
                                        return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileExpression$94( x0$5 ) );
                                    } );
                                    IntermediateRepresentation onNotFound = containsNull ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue() :
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue();
                                    var2 = this.intermediateCompileExpression( lhs ).map( ( l ) -> {
                                        StaticField setField = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.staticConstant( this.namer().nextVariableName(), set, scala.reflect.ManifestFactory..MODULE$.classType(
                                                HashSet.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )));
                                        return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( setField.name(),
                                                scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                                                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ),scala.Predef..
                                        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.method( "contains", scala.reflect.ManifestFactory..MODULE$.classType( java.util.Set.class,
                                                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                                        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..
                                        MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir()}) )),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), onNotFound),
                                        (Seq) l.fields().$colon$plus( setField, scala.collection.Seq..MODULE$.canBuildFrom()),
                                        l.variables(), l.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                    } ); return (Option) var2;
                                }
                            }
                        }

                        if ( var23 )
                        {
                            Expression lhs = var24.lhs();
                            Expression rhs = var24.rhs();
                            var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                                return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                    String variableName = this.namer().nextVariableName();
                                    IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                                            scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
                                    variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateExpression[]{r}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "in",
                                            scala.reflect.ManifestFactory..MODULE$.classType( CypherBoolean.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{l.ir(),
                                            ExpressionCompiler$.MODULE$.nullCheckIfRequired( r,
                                                    ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )))));
                                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{lazySet,
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})));
                                    IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{lazySet,
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                    return new IntermediateExpression( ops,
                                            (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                    (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                    MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false)
                                    ;
                                } );
                            } );
                        }
                        else if ( expression instanceof CoerceTo )
                        {
                            CoerceTo var219 = (CoerceTo) expression;
                            Expression expr = var219.expr();
                            CypherType typ = var219.typ();
                            var2 = this.intermediateCompileExpression( expr ).map( ( ex ) -> {
                                IntermediateExpression var3;
                                label312:
                                {
                                    AnyType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
                                    if ( var10000 == null )
                                    {
                                        if ( typ == null )
                                        {
                                            break label312;
                                        }
                                    }
                                    else if ( var10000.equals( typ ) )
                                    {
                                        break label312;
                                    }

                                    label313:
                                    {
                                        StringType var25 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
                                        if ( var25 == null )
                                        {
                                            if ( typ != null )
                                            {
                                                break label313;
                                            }
                                        }
                                        else if ( !var25.equals( typ ) )
                                        {
                                            break label313;
                                        }

                                        var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asTextValue",
                                                scala.reflect.ManifestFactory..MODULE$.classType( CypherCoercions.class ), scala.reflect.ManifestFactory..
                                        MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                        ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                        return var3;
                                    }

                                    label314:
                                    {
                                        NodeType var26 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                        if ( var26 == null )
                                        {
                                            if ( typ == null )
                                            {
                                                break label314;
                                            }
                                        }
                                        else if ( var26.equals( typ ) )
                                        {
                                            break label314;
                                        }

                                        label315:
                                        {
                                            RelationshipType var27 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                            if ( var27 == null )
                                            {
                                                if ( typ == null )
                                                {
                                                    break label315;
                                                }
                                            }
                                            else if ( var27.equals( typ ) )
                                            {
                                                break label315;
                                            }

                                            label316:
                                            {
                                                PathType var28 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTPath();
                                                if ( var28 == null )
                                                {
                                                    if ( typ != null )
                                                    {
                                                        break label316;
                                                    }
                                                }
                                                else if ( !var28.equals( typ ) )
                                                {
                                                    break label316;
                                                }

                                                var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asPathValue",
                                                        scala.reflect.ManifestFactory..MODULE$.classType(
                                                        CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                MODULE$.classType( PathValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                return var3;
                                            }

                                            label317:
                                            {
                                                IntegerType var29 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                                                if ( var29 == null )
                                                {
                                                    if ( typ == null )
                                                    {
                                                        break label317;
                                                    }
                                                }
                                                else if ( var29.equals( typ ) )
                                                {
                                                    break label317;
                                                }

                                                label318:
                                                {
                                                    FloatType var30 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                                                    if ( var30 == null )
                                                    {
                                                        if ( typ != null )
                                                        {
                                                            break label318;
                                                        }
                                                    }
                                                    else if ( !var30.equals( typ ) )
                                                    {
                                                        break label318;
                                                    }

                                                    var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asFloatingPointValue",
                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                            CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( FloatingPointValue.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( AnyValue.class )),scala.Predef..
                                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                    ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                    return var3;
                                                }

                                                label319:
                                                {
                                                    MapType var31 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTMap();
                                                    if ( var31 == null )
                                                    {
                                                        if ( typ != null )
                                                        {
                                                            break label319;
                                                        }
                                                    }
                                                    else if ( !var31.equals( typ ) )
                                                    {
                                                        break label319;
                                                    }

                                                    var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asMapValue",
                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                            CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( MapValue.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( PropertyCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                                                        (Object[]) (new IntermediateRepresentation[]{ex.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                                                ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                                                                ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}) )),ex.fields(), (Seq) ex.variables().$plus$plus(
                                                        ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                                                        scala.collection.Seq..MODULE$.canBuildFrom()),
                                                    ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                    return var3;
                                                }

                                                if ( typ instanceof ListType )
                                                {
                                                    ListType var13 = (ListType) typ;
                                                    IntermediateRepresentation typx = this.asNeoType( var13.innerType() );
                                                    var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList",
                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                            CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                                                        org.neo4j.internal.kernel.api.procs.Neo4jTypes.AnyType.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( ExpressionCursors.class )),scala.Predef..MODULE$.wrapRefArray(
                                                        (Object[]) (new IntermediateRepresentation[]{ex.ir(), typx, ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                                                ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$CURSORS()}) )),
                                                    ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                    return var3;
                                                }

                                                label320:
                                                {
                                                    BooleanType var32 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                                                    if ( var32 == null )
                                                    {
                                                        if ( typ != null )
                                                        {
                                                            break label320;
                                                        }
                                                    }
                                                    else if ( !var32.equals( typ ) )
                                                    {
                                                        break label320;
                                                    }

                                                    var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asBooleanValue",
                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                            CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                    ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                    return var3;
                                                }

                                                label321:
                                                {
                                                    NumberType var33 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNumber();
                                                    if ( var33 == null )
                                                    {
                                                        if ( typ != null )
                                                        {
                                                            break label321;
                                                        }
                                                    }
                                                    else if ( !var33.equals( typ ) )
                                                    {
                                                        break label321;
                                                    }

                                                    var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asNumberValue",
                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                            CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( NumberValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                    ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                    return var3;
                                                }

                                                label322:
                                                {
                                                    PointType var34 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTPoint();
                                                    if ( var34 == null )
                                                    {
                                                        if ( typ == null )
                                                        {
                                                            break label322;
                                                        }
                                                    }
                                                    else if ( var34.equals( typ ) )
                                                    {
                                                        break label322;
                                                    }

                                                    label323:
                                                    {
                                                        GeometryType var35 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTGeometry();
                                                        if ( var35 == null )
                                                        {
                                                            if ( typ == null )
                                                            {
                                                                break label323;
                                                            }
                                                        }
                                                        else if ( var35.equals( typ ) )
                                                        {
                                                            break label323;
                                                        }

                                                        label324:
                                                        {
                                                            DateType var36 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTDate();
                                                            if ( var36 == null )
                                                            {
                                                                if ( typ == null )
                                                                {
                                                                    break label324;
                                                                }
                                                            }
                                                            else if ( var36.equals( typ ) )
                                                            {
                                                                break label324;
                                                            }

                                                            label325:
                                                            {
                                                                LocalTimeType var37 = org.neo4j.cypher.internal.v4_0.util.symbols.package..
                                                                MODULE$.CTLocalTime();
                                                                if ( var37 == null )
                                                                {
                                                                    if ( typ != null )
                                                                    {
                                                                        break label325;
                                                                    }
                                                                }
                                                                else if ( !var37.equals( typ ) )
                                                                {
                                                                    break label325;
                                                                }

                                                                var3 = new IntermediateExpression(
                                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asLocalTimeValue",
                                                                        scala.reflect.ManifestFactory..MODULE$.classType(
                                                                        CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                                MODULE$.classType( LocalTimeValue.class ), scala.reflect.ManifestFactory..
                                                                MODULE$.classType( AnyValue.class )),scala.Predef..
                                                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                                ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5())
                                                                ;
                                                                return var3;
                                                            }

                                                            label326:
                                                            {
                                                                TimeType var38 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTTime();
                                                                if ( var38 == null )
                                                                {
                                                                    if ( typ != null )
                                                                    {
                                                                        break label326;
                                                                    }
                                                                }
                                                                else if ( !var38.equals( typ ) )
                                                                {
                                                                    break label326;
                                                                }

                                                                var3 = new IntermediateExpression(
                                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asTimeValue",
                                                                        scala.reflect.ManifestFactory..MODULE$.classType(
                                                                        CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                                MODULE$.classType( TimeValue.class ), scala.reflect.ManifestFactory..
                                                                MODULE$.classType( AnyValue.class )),scala.Predef..
                                                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                                ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5())
                                                                ;
                                                                return var3;
                                                            }

                                                            label327:
                                                            {
                                                                LocalDateTimeType var39 = org.neo4j.cypher.internal.v4_0.util.symbols.package..
                                                                MODULE$.CTLocalDateTime();
                                                                if ( var39 == null )
                                                                {
                                                                    if ( typ == null )
                                                                    {
                                                                        break label327;
                                                                    }
                                                                }
                                                                else if ( var39.equals( typ ) )
                                                                {
                                                                    break label327;
                                                                }

                                                                label328:
                                                                {
                                                                    DateTimeType var40 = org.neo4j.cypher.internal.v4_0.util.symbols.package..
                                                                    MODULE$.CTDateTime();
                                                                    if ( var40 == null )
                                                                    {
                                                                        if ( typ == null )
                                                                        {
                                                                            break label328;
                                                                        }
                                                                    }
                                                                    else if ( var40.equals( typ ) )
                                                                    {
                                                                        break label328;
                                                                    }

                                                                    DurationType var41 = org.neo4j.cypher.internal.v4_0.util.symbols.package..
                                                                    MODULE$.CTDuration();
                                                                    if ( var41 == null )
                                                                    {
                                                                        if ( typ != null )
                                                                        {
                                                                            throw new CypherTypeException(
                                                                                    (new StringBuilder( 16 )).append( "Can't coerce to " ).append(
                                                                                            typ ).toString() );
                                                                        }
                                                                    }
                                                                    else if ( !var41.equals( typ ) )
                                                                    {
                                                                        throw new CypherTypeException(
                                                                                (new StringBuilder( 16 )).append( "Can't coerce to " ).append(
                                                                                        typ ).toString() );
                                                                    }

                                                                    var3 = new IntermediateExpression(
                                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asDurationValue",
                                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                                            CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                                    MODULE$.classType( DurationValue.class ), scala.reflect.ManifestFactory..
                                                                    MODULE$.classType( AnyValue.class )),scala.Predef..
                                                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                                    ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5())
                                                                    ;
                                                                    return var3;
                                                                }

                                                                var3 = new IntermediateExpression(
                                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asDateTimeValue",
                                                                        scala.reflect.ManifestFactory..MODULE$.classType(
                                                                        CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                                MODULE$.classType( DateTimeValue.class ), scala.reflect.ManifestFactory..
                                                                MODULE$.classType( AnyValue.class )),scala.Predef..
                                                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                                ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5())
                                                                ;
                                                                return var3;
                                                            }

                                                            var3 = new IntermediateExpression(
                                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asLocalDateTimeValue",
                                                                    scala.reflect.ManifestFactory..MODULE$.classType(
                                                                    CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                            MODULE$.classType( LocalDateTimeValue.class ), scala.reflect.ManifestFactory..
                                                            MODULE$.classType( AnyValue.class )),scala.Predef..
                                                            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                            ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                            return var3;
                                                        }

                                                        var3 = new IntermediateExpression(
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asDateValue",
                                                                scala.reflect.ManifestFactory..MODULE$.classType(
                                                                CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                        MODULE$.classType( DateValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                        ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                        return var3;
                                                    }

                                                    var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asPointValue",
                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                            CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( PointValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                    ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                    return var3;
                                                }

                                                var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asPointValue",
                                                        scala.reflect.ManifestFactory..MODULE$.classType(
                                                        CypherCoercions.class ), scala.reflect.ManifestFactory..
                                                MODULE$.classType( PointValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                                ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                                return var3;
                                            }

                                            var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asIntegralValue",
                                                    scala.reflect.ManifestFactory..MODULE$.classType( CypherCoercions.class ), scala.reflect.ManifestFactory..
                                            MODULE$.classType( IntegralValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                            ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                            return var3;
                                        }

                                        var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asRelationshipValue",
                                                scala.reflect.ManifestFactory..MODULE$.classType( CypherCoercions.class ), scala.reflect.ManifestFactory..
                                        MODULE$.classType( RelationshipValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                        ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                        return var3;
                                    }

                                    var3 = new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asNodeValue",
                                            scala.reflect.ManifestFactory..MODULE$.classType( CypherCoercions.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ex.ir()}) )),
                                    ex.fields(), ex.variables(), ex.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                    return var3;
                                }

                                var3 = ex;
                                return var3;
                            } );
                        }
                        else if ( expression instanceof ContainerIndex )
                        {
                            ContainerIndex var222 = (ContainerIndex) expression;
                            Expression container = var222.expr();
                            Expression index = var222.idx();
                            var2 = this.containerIndexAccess( container, index, false );
                        }
                        else if ( expression instanceof ParameterFromSlot )
                        {
                            ParameterFromSlot var225 = (ParameterFromSlot) expression;
                            int offset = var225.offset();
                            String name = var225.name();
                            String parameterVariable = this.namer().parameterName( name );
                            LocalVariable local = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.variable( parameterVariable,
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayLoad(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( "params" ), offset),scala.reflect.ManifestFactory..
                            MODULE$.classType( AnyValue.class ));
                            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( parameterVariable ),
                                    (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new LocalVariable[]{local}) )),(Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( parameterVariable ),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())}))),false));
                        }
                        else
                        {
                            if ( expression instanceof CaseExpression )
                            {
                                var25 = true;
                                var26 = (CaseExpression) expression;
                                Option var230 = var26.expression();
                                IndexedSeq alternativeExpressions = var26.alternatives();
                                Option defaultExpression = var26.
                                default
                                    (); if ( var230 instanceof Some )
                                {
                                    Some var233 = (Some) var230;
                                    Expression innerExpression = (Expression) var233.value();
                                    Object var12;
                                    if ( defaultExpression instanceof Some )
                                    {
                                        Some var237 = (Some) defaultExpression;
                                        Expression e = (Expression) var237.value();
                                        var12 = this.intermediateCompileExpression( e );
                                    }
                                    else
                                    {
                                        if ( !.MODULE$.equals( defaultExpression )){
                                        throw new MatchError( defaultExpression );
                                    }

                                        var12 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(),
                                                (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), (Set) scala.Predef..
                                        MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                        BoxesRunTime.boxToBoolean( true ) )}))),IntermediateExpression$.MODULE$.apply$default$5()));
                                    }

                                    Tuple2 var240 = alternativeExpressions.unzip( scala.Predef..MODULE$.$conforms());
                                    if ( var240 == null )
                                    {
                                        throw new MatchError( var240 );
                                    }

                                    IndexedSeq checkExpressions = (IndexedSeq) var240._1();
                                    IndexedSeq loadExpressions = (IndexedSeq) var240._2();
                                    Tuple2 var11 = new Tuple2( checkExpressions, loadExpressions );
                                    IndexedSeq checkExpressions = (IndexedSeq) var11._1();
                                    IndexedSeq loadExpressions = (IndexedSeq) var11._2();
                                    IndexedSeq checks = (IndexedSeq) checkExpressions.flatMap( ( expressionx ) -> {
                                        return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( expressionx ) );
                                    }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                                    IndexedSeq loads = (IndexedSeq) loadExpressions.flatMap( ( expressionx ) -> {
                                        return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( expressionx ) );
                                    }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                                    var2 = checks.size() == loads.size() && !checks.isEmpty() && !((Option) var12).isEmpty()
                                           ? this.intermediateCompileExpression( innerExpression ).withFilter( ( check$ifrefutable$3 ) -> {
                                        return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileExpression$101( check$ifrefutable$3 ) );
                                    } ).map( ( innerx ) -> {
                                        IntermediateExpression var5 = (IntermediateExpression) var12.get();
                                        String returnVariable = this.namer().nextVariableName();
                                        LocalVariable local = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.variable( returnVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                (Object) null ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
                                        IntermediateRepresentation ops = this.caseExpression( returnVariable, (Seq) checks.map( ( x$17 ) -> {
                                            return x$17.ir();
                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), (Seq) loads.map( ( x$18 ) -> {
                                            return x$18.ir();
                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom()),var5.ir(), ( toCheck ) -> {
                                            return org.neo4j.codegen.api.IntermediateRepresentation..
                                            MODULE$.invoke( innerx.ir(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "equals",
                                                    scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                                            MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{toCheck}) ));
                                        });
                                        return new IntermediateExpression( ops, (Seq) ((TraversableLike) ((TraversableLike) innerx.fields().$plus$plus(
                                                (GenTraversableOnce) checks.flatMap( ( x$19 ) -> {
                                                    return x$19.fields();
                                                }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom())).
                                        $plus$plus( (GenTraversableOnce) loads.flatMap( ( x$20 ) -> {
                                            return x$20.fields();
                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom())).
                                        $plus$plus( var5.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                        (Seq) ((SeqLike) ((TraversableLike) ((TraversableLike) innerx.variables().$plus$plus(
                                                (GenTraversableOnce) checks.flatMap( ( x$21 ) -> {
                                                    return x$21.variables();
                                                }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom())).
                                        $plus$plus( (GenTraversableOnce) loads.flatMap( ( x$22 ) -> {
                                            return x$22.variables();
                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom())).
                                        $plus$plus( var5.variables(), scala.collection.Seq..MODULE$.canBuildFrom())).
                                        $colon$plus( local, scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                        MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnVariable ),
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})))}))),
                                        IntermediateExpression$.MODULE$.apply$default$5());
                                    } ) : .MODULE$;
                                    return (Option) var2;
                                }
                            }

                            if ( var25 )
                            {
                                Option var247 = var26.expression();
                                IndexedSeq alternativeExpressions = var26.alternatives();
                                Option defaultExpression = var26.
                                default
                                    (); if (.MODULE$.equals( var247 )){
                                    Object var10;
                                    if ( defaultExpression instanceof Some )
                                    {
                                        Some var252 = (Some) defaultExpression;
                                        Expression e = (Expression) var252.value();
                                        var10 = this.intermediateCompileExpression( e );
                                    }
                                    else
                                    {
                                        if ( !.MODULE$.equals( defaultExpression )){
                                        throw new MatchError( defaultExpression );
                                    }

                                        var10 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(),
                                                (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), (Set) scala.Predef..
                                        MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                        BoxesRunTime.boxToBoolean( true ) )}))),IntermediateExpression$.MODULE$.apply$default$5()));
                                    }

                                    Tuple2 var255 = alternativeExpressions.unzip( scala.Predef..MODULE$.$conforms());
                                    if ( var255 == null )
                                    {
                                        throw new MatchError( var255 );
                                    }

                                    IndexedSeq checkExpressions = (IndexedSeq) var255._1();
                                    IndexedSeq loadExpressions = (IndexedSeq) var255._2();
                                    Tuple2 var9 = new Tuple2( checkExpressions, loadExpressions );
                                    IndexedSeq checkExpressions = (IndexedSeq) var9._1();
                                    IndexedSeq loadExpressions = (IndexedSeq) var9._2();
                                    IndexedSeq checks = (IndexedSeq) checkExpressions.flatMap( ( ex ) -> {
                                        return org.neo4j.cypher.internal.compiler.helpers.PredicateHelper..MODULE$.isPredicate( ex ) ? scala.Option..
                                        MODULE$.option2Iterable( this.intermediateCompileExpression( ex ) ) :scala.Option..
                                        MODULE$.option2Iterable( this.intermediateCompileExpression( ex ).map( ( e ) -> {
                                            return this.coerceToPredicate( e );
                                        } ) );
                                    }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                                    IndexedSeq loads = (IndexedSeq) loadExpressions.flatMap( ( expressionx ) -> {
                                        return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( expressionx ) );
                                    }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                                    if ( checks.size() == loads.size() && !((Option) var10).isEmpty() )
                                    {
                                        IntermediateExpression var262 = (IntermediateExpression) ((Option) var10).get();
                                        String returnVariable = this.namer().nextVariableName();
                                        LocalVariable local = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.variable( returnVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                (Object) null ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
                                        IntermediateRepresentation ops = this.caseExpression( returnVariable, (Seq) checks.map( ( x$24 ) -> {
                                            return x$24.ir();
                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), (Seq) loads.map( ( x$25 ) -> {
                                        return x$25.ir();
                                    }, scala.collection.IndexedSeq..MODULE$.canBuildFrom()),var262.ir(), ( toCheck ) -> {
                                        return org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.equal( toCheck, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue());
                                    });
                                        var10000 = new Some(
                                                new IntermediateExpression( ops, (Seq) ((TraversableLike) ((TraversableLike) checks.flatMap( ( x$26 ) -> {
                                                    return x$26.fields();
                                                }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).$plus$plus(
                                                        (GenTraversableOnce) loads.flatMap( ( x$27 ) -> {
                                                            return x$27.fields();
                                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ),
                                                        scala.collection.IndexedSeq..MODULE$.canBuildFrom() )).
                                        $plus$plus( var262.fields(), scala.collection.IndexedSeq..MODULE$.canBuildFrom()),
                                        (Seq) ((SeqLike) ((TraversableLike) ((TraversableLike) checks.flatMap( ( x$28 ) -> {
                                            return x$28.variables();
                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).$plus$plus( (GenTraversableOnce) loads.flatMap( ( x$29 ) -> {
                                            return x$29.variables();
                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), scala.collection.IndexedSeq..MODULE$.canBuildFrom())).
                                        $plus$plus( var262.variables(), scala.collection.IndexedSeq..MODULE$.canBuildFrom())).
                                        $colon$plus( local, scala.collection.IndexedSeq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                                        MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnVariable ),
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})))}))),
                                        IntermediateExpression$.MODULE$.apply$default$5()));
                                    }
                                    else
                                    {
                                        var10000 = .MODULE$;
                                    }

                                    var2 = var10000;
                                    return (Option) var2;
                                }
                            }

                            if ( expression instanceof Property )
                            {
                                Property var266 = (Property) expression;
                                Expression targetExpression = var266.map();
                                PropertyKeyName var268 = var266.propertyKey();
                                if ( var268 != null )
                                {
                                    String key = var268.name();
                                    var2 = this.intermediateCompileExpression( targetExpression ).map( ( map ) -> {
                                        String variableName = this.namer().nextVariableName();
                                        IntermediateRepresentation propertyGet = this.getProperty( key, ExpressionCompiler$.MODULE$.nullCheckIfRequired( map,
                                                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) );
                                        IntermediateRepresentation call = ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new IntermediateExpression[]{map}) ), propertyGet);
                                        IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                                                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),variableName, call));
                                        IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{lazySet,
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})));
                                        IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{lazySet,
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                        return new IntermediateExpression( ops, map.fields(), (Seq) map.variables().$plus$plus(
                                                ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                                                scala.collection.Seq..MODULE$.canBuildFrom() ),(Set) scala.Predef..
                                        MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),
                                        false);
                                    } ); return (Option) var2;
                                }
                            }

                            if ( expression instanceof NodeProperty )
                            {
                                NodeProperty var270 = (NodeProperty) expression;
                                int offset = var270.offset();
                                int token = var270.propToken();
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, this.getNodeProperty( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                    BoxesRunTime.boxToInteger( token ) ), offset)));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                var2 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.empty(),
                                        (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR(),
                                                ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false));
                            }
                            else if ( expression instanceof SlottedCachedPropertyWithPropertyToken )
                            {
                                SlottedCachedPropertyWithPropertyToken var277 = (SlottedCachedPropertyWithPropertyToken) expression;
                                int offset = var277.offset();
                                boolean offsetIsForLongSlot = var277.offsetIsForLongSlot();
                                int token = var277.propToken();
                                EntityType entityType = var277.entityType();
                                boolean nullable = var277.nullable();
                                Some var439;
                                if ( token == -1 )
                                {
                                    var439 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(),
                                            (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
                                    MODULE$.Set().empty(), false));
                                }
                                else
                                {
                                    String variableName = this.namer().nextVariableName();
                                    IntermediateRepresentation entityId = this.getEntityId( offsetIsForLongSlot, offset, entityType, nullable );
                                    Tuple4 var286 = this.callPropertyGet( entityType );
                                    if ( var286 == null )
                                    {
                                        throw new MatchError( var286 );
                                    }

                                    Method propertyGet = (Method) var286._1();
                                    Method txStatePropertyGet = (Method) var286._2();
                                    IntermediateRepresentation cursor = (IntermediateRepresentation) var286._3();
                                    LocalVariable cursorVar = (LocalVariable) var286._4();
                                    Tuple4 var8 = new Tuple4( propertyGet, txStatePropertyGet, cursor, cursorVar );
                                    Method propertyGet = (Method) var8._1();
                                    Method txStatePropertyGet = (Method) var8._2();
                                    IntermediateRepresentation cursor = (IntermediateRepresentation) var8._3();
                                    LocalVariable cursorVar = (LocalVariable) var8._4();
                                    IntermediateRepresentation getAndCacheProperty = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                                                    scala.reflect.ManifestFactory..MODULE$.classType( Value.class )), variableName,
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
                                    org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual( entityId,
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
                                    this.checkPropertyTxState$1( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                            scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign(
                                                    variableName,
                                                    this.getCachedPropertyAt( var277, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                            ExpressionCompiler$.MODULE$.DB_ACCESS(), propertyGet, scala.Predef..MODULE$.wrapRefArray(
                                                            (Object[]) (new IntermediateRepresentation[]{entityId,
                                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                                    BoxesRunTime.boxToInteger( token ) ), cursor, ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(),
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                            BoxesRunTime.boxToBoolean( true ) )})))))}))),token, variableName, entityId, txStatePropertyGet))}))))
                                    ;
                                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{getAndCacheProperty,
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})));
                                    IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{getAndCacheProperty,
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                    var439 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.empty(),
                                            (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new LocalVariable[]{cursorVar, ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),(Set) scala.Predef..
                                    MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false))
                                    ;
                                }

                                var2 = var439;
                            }
                            else if ( expression instanceof NodePropertyLate )
                            {
                                NodePropertyLate var298 = (NodePropertyLate) expression;
                                int offset = var298.offset();
                                String key = var298.propKey();
                                InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.field( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.setField( f, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyKey",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( key )}))))),
                                this.getNodeProperty( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ), offset)})))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                var2 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new InstanceField[]{f}) ) ), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR(),
                                            ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false));
                            }
                            else if ( expression instanceof SlottedCachedPropertyWithoutPropertyToken )
                            {
                                SlottedCachedPropertyWithoutPropertyToken var306 = (SlottedCachedPropertyWithoutPropertyToken) expression;
                                int offset = var306.offset();
                                boolean offsetIsForLongSlot = var306.offsetIsForLongSlot();
                                String propKey = var306.propKey();
                                EntityType entityType = var306.entityType();
                                boolean nullable = var306.nullable();
                                InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.field( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation entityId = this.getEntityId( offsetIsForLongSlot, offset, entityType, nullable );
                                Tuple4 var316 = this.callPropertyGet( entityType );
                                if ( var316 == null )
                                {
                                    throw new MatchError( var316 );
                                }

                                Method propertyGet = (Method) var316._1();
                                Method txStatePropertyGet = (Method) var316._2();
                                IntermediateRepresentation cursor = (IntermediateRepresentation) var316._3();
                                LocalVariable cursorVar = (LocalVariable) var316._4();
                                Tuple4 var7 = new Tuple4( propertyGet, txStatePropertyGet, cursor, cursorVar );
                                Method propertyGet = (Method) var7._1();
                                Method txStatePropertyGet = (Method) var7._2();
                                IntermediateRepresentation cursor = (IntermediateRepresentation) var7._3();
                                LocalVariable cursorVar = (LocalVariable) var7._4();
                                IntermediateRepresentation getAndCacheProperty = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                                                scala.reflect.ManifestFactory..MODULE$.classType( Value.class )), variableName,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual( entityId,
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                        f ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) ))),
                                this.checkPropertyTxState$2( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                        scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign(
                                                variableName,
                                                this.getCachedPropertyAt( var306, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                        ExpressionCompiler$.MODULE$.DB_ACCESS(), propertyGet, scala.Predef..MODULE$.wrapRefArray(
                                                        (Object[]) (new IntermediateRepresentation[]{entityId,
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ), cursor,
                                                        ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(),
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )})))))}))),
                                f, variableName, entityId, txStatePropertyGet))})));
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.setField( f, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyKey",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( propKey )}))))),
                                getAndCacheProperty}))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                var2 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new InstanceField[]{f}) ) ), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new LocalVariable[]{cursorVar, ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false));
                            }
                            else if ( expression instanceof NodePropertyExists )
                            {
                                NodePropertyExists var329 = (NodePropertyExists) expression;
                                int offset = var329.offset();
                                int token = var329.propToken();
                                var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeHasProperty",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ),
                                        scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
                                MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                                MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),scala.Predef..
                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ),
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( token ) ),
                                        ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}))),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.falseValue()),(Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..
                                MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR(),
                                        ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),scala.Predef..
                                MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                            }
                            else if ( expression instanceof NodePropertyExistsLate )
                            {
                                NodePropertyExistsLate var332 = (NodePropertyExistsLate) expression;
                                int offset = var332.offset();
                                String key = var332.propKey();
                                InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.field( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
                                var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                        scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( -1 ) ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.setField( f, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyKey",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( key )}))))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeHasProperty",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
                                MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( PropertyCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ), ExpressionCompiler$.MODULE$.NODE_CURSOR(),
                                    ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}))),org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue())}))),(Seq) scala.collection.Seq..
                                MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{f}) )),(Seq) scala.collection.Seq..
                                MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR(),
                                        ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),scala.Predef..
                                MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                            }
                            else if ( expression instanceof RelationshipProperty )
                            {
                                RelationshipProperty var336 = (RelationshipProperty) expression;
                                int offset = var336.offset();
                                int token = var336.propToken();
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),
                                variableName, this.getRelationshipProperty( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( token ) ), offset)));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                var2 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.empty(),
                                        (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(),
                                                ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false));
                            }
                            else if ( expression instanceof RelationshipPropertyLate )
                            {
                                RelationshipPropertyLate var343 = (RelationshipPropertyLate) expression;
                                int offset = var343.offset();
                                String key = var343.propKey();
                                InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.field( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
                                String variableName = this.namer().nextVariableName();
                                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                        Value.class )),variableName, org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.setField( f, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyKey",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( key )}))))),
                                this.getRelationshipProperty( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ), offset)})))));
                                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                variableName )})));
                                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                var2 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new InstanceField[]{f}) ) ), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(),
                                            ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),(Set) scala.Predef..
                                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false));
                            }
                            else if ( expression instanceof RelationshipPropertyExists )
                            {
                                RelationshipPropertyExists var351 = (RelationshipPropertyExists) expression;
                                int offset = var351.offset();
                                int token = var351.propToken();
                                var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationshipHasProperty",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ),
                                        scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
                                MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                                MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),
                                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( token ) ),
                                    ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}))),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.falseValue()),(Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..
                                MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(),
                                                ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),scala.Predef..
                                MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                            }
                            else if ( expression instanceof RelationshipPropertyExistsLate )
                            {
                                RelationshipPropertyExistsLate var354 = (RelationshipPropertyExistsLate) expression;
                                int offset = var354.offset();
                                String key = var354.propKey();
                                InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.field( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
                                var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                        scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( -1 ) ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.setField( f, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyKey",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( key )}))))),
                                org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationshipHasProperty",
                                        scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
                                MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( PropertyCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                    ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}))),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.falseValue())}))),(Seq) scala.collection.Seq..
                                MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{f}) )),(Seq) scala.collection.Seq..
                                MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(),
                                                ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),scala.Predef..
                                MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                            }
                            else
                            {
                                if ( expression instanceof HasLabelsFromSlot )
                                {
                                    HasLabelsFromSlot var358 = (HasLabelsFromSlot) expression;
                                    int offset = var358.offset();
                                    Seq resolvedLabelTokens = var358.resolvedLabelTokens();
                                    Seq lateLabels = var358.lateLabels();
                                    if ( resolvedLabelTokens.nonEmpty() || lateLabels.nonEmpty() )
                                    {
                                        Tuple2 var363 = this.tokenFieldsForLabels( lateLabels );
                                        if ( var363 == null )
                                        {
                                            throw new MatchError( var363 );
                                        }

                                        Seq tokenFields = (Seq) var363._1();
                                        Seq inits = (Seq) var363._2();
                                        Tuple2 var6 = new Tuple2( tokenFields, inits );
                                        Seq tokenFields = (Seq) var6._1();
                                        Seq inits = (Seq) var6._2();
                                        IntermediateRepresentation predicate = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                                            (IntermediateRepresentation) ((TraversableOnce) ((TraversableLike) resolvedLabelTokens.map( ( tokenId ) -> {
                                                return $anonfun$intermediateCompileExpression$121( this, offset, BoxesRunTime.unboxToInt( tokenId ) );
                                            }, scala.collection.Seq..MODULE$.canBuildFrom())).$plus$plus(
                                                    (GenTraversableOnce) tokenFields.map( ( tokenField ) -> {
                                                        return this.isLabelSetOnNode( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField(
                                                                tokenField ),offset);
                                                    }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom())).
                                        reduceLeft( ( lhsx, rhsx ) -> {
                                            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( lhsx, rhsx );
                                        } ), org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue());
                                        var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                                (Seq) inits.$colon$plus( predicate,
                                                        scala.collection.Seq..MODULE$.canBuildFrom() ) ), tokenFields, (Seq) scala.collection.Seq..
                                        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR()}) )),scala.Predef..
                                        MODULE$.Set().empty(), false));
                                        return (Option) var2;
                                    }
                                }

                                if ( expression instanceof HasLabels )
                                {
                                    HasLabels var369 = (HasLabels) expression;
                                    Expression nodeExpression = var369.expression();
                                    Seq labels = var369.labels();
                                    if ( labels.nonEmpty() )
                                    {
                                        var2 = this.intermediateCompileExpression( nodeExpression ).map( ( node ) -> {
                                            Tuple2 var5 = this.tokenFieldsForLabels( (Seq) labels.map( ( x$33 ) -> {
                                                return x$33.name();
                                            }, scala.collection.Seq..MODULE$.canBuildFrom() ));
                                            if ( var5 != null )
                                            {
                                                Seq tokenFields = (Seq) var5._1();
                                                Seq inits = (Seq) var5._2();
                                                Tuple2 var3 = new Tuple2( tokenFields, inits );
                                                Seq tokenFieldsx = (Seq) var3._1();
                                                Seq initsx = (Seq) var3._2();
                                                IntermediateRepresentation predicate = org.neo4j.codegen.api.IntermediateRepresentation..
                                                MODULE$.ternary( (IntermediateRepresentation) ((TraversableOnce) tokenFieldsx.map( ( token ) -> {
                                                    return org.neo4j.codegen.api.IntermediateRepresentation..
                                                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "hasLabel",
                                                            scala.reflect.ManifestFactory..MODULE$.classType(
                                                            CypherFunctions.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.Boolean(), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.Int(), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
                                                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{node.ir(),
                                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( token ),
                                                            ExpressionCompiler$.MODULE$.DB_ACCESS(), ExpressionCompiler$.MODULE$.NODE_CURSOR()})));
                                                }, scala.collection.Seq..MODULE$.canBuildFrom()) ).reduceLeft( ( lhs, rhs ) -> {
                                                    return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and( lhs, rhs );
                                                } ), org.neo4j.codegen.api.IntermediateRepresentation..
                                                MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue());
                                                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                                        (Seq) initsx.$colon$plus( predicate, scala.collection.Seq..MODULE$.canBuildFrom() )),
                                                (Seq) node.fields().$plus$plus( tokenFieldsx, scala.collection.Seq..MODULE$.canBuildFrom()),
                                                (Seq) node.variables().$colon$plus( ExpressionCompiler$.MODULE$.vNODE_CURSOR(),
                                                        scala.collection.Seq..MODULE$.canBuildFrom()),
                                                node.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                                            }
                                            else
                                            {
                                                throw new MatchError( var5 );
                                            }
                                        } ); return (Option) var2;
                                    }
                                }

                                if ( expression instanceof NodeFromSlot )
                                {
                                    NodeFromSlot var372 = (NodeFromSlot) expression;
                                    int offset = var372.offset();
                                    var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                            ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                                            "nodeById", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ),
                                            scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
                                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset )}) )),
                                    (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
                                    MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                                }
                                else if ( expression instanceof RelationshipFromSlot )
                                {
                                    RelationshipFromSlot var374 = (RelationshipFromSlot) expression;
                                    int offset = var374.offset();
                                    var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                            ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                                            "relationshipById", scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ),
                                            scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ), scala.reflect.ManifestFactory..
                                    MODULE$.Long()),scala.Predef..
                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset )}) )),(Seq) scala.collection.Seq..
                                    MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
                                    MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                                }
                                else if ( expression instanceof GetDegreePrimitive )
                                {
                                    GetDegreePrimitive var376 = (GetDegreePrimitive) expression;
                                    int offset = var376.offset();
                                    Option typ = var376.typ();
                                    SemanticDirection dir = var376.direction();
                                    String var5;
                                    if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( dir )){
                                    var5 = "nodeGetOutgoingDegree";
                                } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( dir )){
                                    var5 = "nodeGetIncomingDegree";
                                } else{
                                    if ( !org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( dir )){
                                        throw new MatchError( dir );
                                    }

                                    var5 = "nodeGetTotalDegree";
                                }

                                    Some var4;
                                    if (.MODULE$.equals( typ )){
                                    var4 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "intValue",
                                            scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                                            IntValue.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                    ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                                                    var5, scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ),
                                            scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                                    MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ), ExpressionCompiler$.MODULE$.NODE_CURSOR()}) ))}))),
                                    (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR()}) )),scala.Predef..
                                    MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                                } else{
                                    if ( !(typ instanceof Some) )
                                    {
                                        throw new MatchError( typ );
                                    }

                                    Some var383 = (Some) typ;
                                    String t = (String) var383.value();
                                    InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.field( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                            BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
                                    var4 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                            scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                            BoxesRunTime.boxToInteger( -1 ) ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.setField( f, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                            ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                                            "relationshipType", scala.reflect.ManifestFactory..MODULE$.classType(
                                            DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                                    MODULE$.classType( String.class )),scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( t )}))))),
                                    org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "intValue",
                                            scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( IntValue.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                                    ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method(
                                                    var5, scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ),
                                            scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                                    MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..
                                    MODULE$.classType( NodeCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ),
                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                                            ExpressionCompiler$.MODULE$.NODE_CURSOR()})))})))}))),(Seq) scala.collection.Seq..
                                    MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new InstanceField[]{f}) )),(Seq) scala.collection.Seq..
                                    MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR()}) )),scala.Predef..
                                    MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                                }

                                    var2 = var4;
                                }
                                else
                                {
                                    if ( expression instanceof ResolvedFunctionInvocation )
                                    {
                                        ResolvedFunctionInvocation var386 = (ResolvedFunctionInvocation) expression;
                                        Option var387 = var386.fcnSignature();
                                        IndexedSeq args = var386.callArguments();
                                        if ( var387 instanceof Some )
                                        {
                                            Some var389 = (Some) var387;
                                            UserFunctionSignature signature = (UserFunctionSignature) var389.value();
                                            if ( !var386.isAggregate() )
                                            {
                                                IndexedSeq inputArgs = (IndexedSeq) ((TraversableLike) ((IterableLike) args.map( ( x$35 ) -> {
                                                    return new Some( x$35 );
                                                }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).zipAll(
                                                        (GenIterable) signature.inputSignature().map( ( x$36 ) -> {
                                                            return x$36.
                                                            default
                                                                ().map( ( x$37 ) -> {
                                                                    return x$37.value();
                                                                } );
                                                        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), .MODULE$, .
                                                MODULE$, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).flatMap( ( x0$6 ) -> {
                                                Iterable var2;
                                                if ( x0$6 != null )
                                                {
                                                    Option var4 = (Option) x0$6._1();
                                                    if ( var4 instanceof Some )
                                                    {
                                                        Some var5 = (Some) var4;
                                                        Expression given = (Expression) var5.value();
                                                        var2 = scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( given ) );
                                                        return var2;
                                                    }
                                                }

                                                if ( x0$6 != null )
                                                {
                                                    Option var7 = (Option) x0$6._2();
                                                    if ( var7 instanceof Some )
                                                    {
                                                        Some var8 = (Some) var7;
                                                        Object var9 = var8.value();
                                                        StaticField constant = org.neo4j.codegen.api.IntermediateRepresentation..
                                                        MODULE$.staticConstant( this.namer().nextVariableName().toUpperCase(), ValueUtils.asAnyValue( var9 ),
                                                                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
                                                        var2 = scala.Option..MODULE$.option2Iterable( new Some(
                                                                    new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic(
                                                                            constant.name(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ) ),
                                                            (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                                            (Object[]) (new StaticField[]{constant}) )),(Seq) scala.collection.Seq..
                                                        MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5())))
                                                        ;
                                                        return var2;
                                                    }
                                                }

                                                var2 = scala.Option..MODULE$.option2Iterable(.MODULE$);
                                                return var2;
                                            }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                                                if ( inputArgs.size() != signature.inputSignature().size() )
                                                {
                                                    var10000 = .MODULE$;
                                                }
                                                else
                                                {
                                                    String variableName = this.namer().nextVariableName();
                                                    StaticField allowed = org.neo4j.codegen.api.IntermediateRepresentation..
                                                    MODULE$.staticConstant( this.namer().nextVariableName(), signature.allowed(),
                                                            scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType(
                                                            String.class )));
                                                    IndexedSeq fields = (IndexedSeq) ((SeqLike) inputArgs.flatMap( ( x$38 ) -> {
                                                        return x$38.fields();
                                                    }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).
                                                    $colon$plus( allowed, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                                                    IndexedSeq variables = (IndexedSeq) inputArgs.flatMap( ( x$39 ) -> {
                                                        return x$39.variables();
                                                    }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
                                                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..
                                                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf(
                                                            scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                    variableName, org.neo4j.codegen.api.IntermediateRepresentation..
                                                    MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "callFunction",
                                                            scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                                                    MODULE$.Int(), scala.reflect.ManifestFactory..
                                                    MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                                                    scala.reflect.ManifestFactory..
                                                    MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))),scala.Predef..
                                                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                            BoxesRunTime.boxToInteger( signature.id() ) ),
                                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf(
                                                            (Seq) inputArgs.map( ( x$40 ) -> {
                                                                return x$40.ir();
                                                            }, scala.collection.IndexedSeq..MODULE$.canBuildFrom() ), scala.reflect.ManifestFactory..
                                                    MODULE$.classType( AnyValue.class )),org.neo4j.codegen.api.IntermediateRepresentation..
                                                    MODULE$.getStatic( allowed.name(), scala.reflect.ManifestFactory..MODULE$.arrayType(
                                                            scala.reflect.ManifestFactory..MODULE$.classType( String.class )))})))));
                                                    var10000 = new Some(
                                                            new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                                                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ops,
                                                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )}))),
                                                    fields, variables, (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                                        (Object[]) (new IntermediateRepresentation[]{
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                                                                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{ops,
                                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})))}))),false));
                                                }

                                                var2 = var10000;
                                                return (Option) var2;
                                            }
                                        }
                                    }

                                    if ( expression instanceof PathExpression )
                                    {
                                        PathExpression var397 = (PathExpression) expression;
                                        PathStep steps = var397.step();
                                        var2 = this.isStaticallyKnown$1( steps ) ? this.compileStaticPath( steps ) : this.compileDynamicPath( steps );
                                    }
                                    else if ( expression instanceof ReferenceFromSlot )
                                    {
                                        Set var3;
                                        IntermediateRepresentation loadRef;
                                        label522:
                                        {
                                            ReferenceFromSlot var399 = (ReferenceFromSlot) expression;
                                            int offset = var399.offset();
                                            String name = var399.name();
                                            loadRef = this.getRefAt( offset );
                                            Option var404 = this.slots().get( name );
                                            if ( var404 instanceof Some )
                                            {
                                                Some var405 = (Some) var404;
                                                Slot slot = (Slot) var405.value();
                                                if ( !slot.nullable() )
                                                {
                                                    var3 = scala.Predef..MODULE$.Set().empty();
                                                    break label522;
                                                }
                                            }

                                            var3 = (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                        this.getRefAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                                        }

                                        var2 = new Some( new IntermediateExpression( loadRef, (Seq) scala.collection.Seq..MODULE$.empty(),
                                                (Seq) scala.collection.Seq..MODULE$.empty(), var3, false));
                                    }
                                    else if ( expression instanceof IdFromSlot )
                                    {
                                        IdFromSlot var407 = (IdFromSlot) expression;
                                        int offset = var407.offset();
                                        Option nameOfSlot = this.slots().nameOfLongSlot( offset );
                                        Set nullCheck = scala.Option..MODULE$.option2Iterable( nameOfSlot.filter( ( n ) -> {
                                        return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileExpression$135( this, n ) );
                                    } ).map( ( x$41 ) -> {
                                        return org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.equal( this.getLongAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                BoxesRunTime.boxToLong( -1L ) ));
                                    } ) ).toSet(); IntermediateRepresentation value = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "longValue",
                                                scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..
                                        MODULE$.classType( LongValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
                                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset )}) ));
                                        var2 = new Some( new IntermediateExpression( value, (Seq) scala.collection.Seq..MODULE$.empty(),
                                                (Seq) scala.collection.Seq..MODULE$.empty(), nullCheck, IntermediateExpression$.MODULE$.apply$default$5()));
                                    }
                                    else if ( expression instanceof LabelsFromSlot )
                                    {
                                        LabelsFromSlot var412 = (LabelsFromSlot) expression;
                                        int offset = var412.offset();
                                        Option nameOfSlot = this.slots().nameOfLongSlot( offset );
                                        Set nullCheck = scala.Option..MODULE$.option2Iterable( nameOfSlot.filter( ( n ) -> {
                                        return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileExpression$137( this, n ) );
                                    } ).map( ( x$42 ) -> {
                                        return org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.equal( this.getLongAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                BoxesRunTime.boxToLong( -1L ) ));
                                    } ) ).toSet(); IntermediateRepresentation value = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "getLabelsForNode",
                                                scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                        MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
                                        MODULE$.classType( NodeCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ), ExpressionCompiler$.MODULE$.NODE_CURSOR()}) ))
                                        ;
                                        var2 = new Some( new IntermediateExpression( value, (Seq) scala.collection.Seq..MODULE$.empty(),
                                                (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vNODE_CURSOR()}) )),
                                        nullCheck, IntermediateExpression$.MODULE$.apply$default$5()));
                                    }
                                    else if ( expression instanceof RelationshipTypeFromSlot )
                                    {
                                        RelationshipTypeFromSlot var417 = (RelationshipTypeFromSlot) expression;
                                        int offset = var417.offset();
                                        Option nameOfSlot = this.slots().nameOfLongSlot( offset );
                                        Set nullCheck = scala.Option..MODULE$.option2Iterable( nameOfSlot.filter( ( n ) -> {
                                        return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileExpression$139( this, n ) );
                                    } ).map( ( x$43 ) -> {
                                        return org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.equal( this.getLongAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                BoxesRunTime.boxToLong( -1L ) ));
                                    } ) ).toSet(); IntermediateRepresentation value = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "getTypeForRelationship",
                                                scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                                        MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..MODULE$.Long(), scala.reflect.ManifestFactory..
                                        MODULE$.classType( RelationshipScanCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset ),
                                                    ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR()}) ));
                                        var2 = new Some( new IntermediateExpression( value, (Seq) scala.collection.Seq..MODULE$.empty(),
                                                (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new LocalVariable[]{ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR()}) )),
                                        nullCheck, IntermediateExpression$.MODULE$.apply$default$5()));
                                    }
                                    else if ( expression instanceof PrimitiveEquals )
                                    {
                                        PrimitiveEquals var422 = (PrimitiveEquals) expression;
                                        Expression lhs = var422.a();
                                        Expression rhs = var422.b();
                                        var2 = this.intermediateCompileExpression( lhs ).flatMap( ( l ) -> {
                                            return this.intermediateCompileExpression( rhs ).map( ( r ) -> {
                                                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( l.ir(),
                                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "equals",
                                                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ),scala.reflect.ManifestFactory..
                                                MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
                                                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{r.ir()}) )),
                                                org.neo4j.codegen.api.IntermediateRepresentation..
                                                MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue()),
                                                (Seq) l.fields().$plus$plus( r.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                                (Seq) l.variables().$plus$plus( r.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                                                (Set) l.nullChecks().$plus$plus( r.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                                            } );
                                        } );
                                    }
                                    else if ( expression instanceof NullCheck )
                                    {
                                        NullCheck var425 = (NullCheck) expression;
                                        int offset = var425.offset();
                                        Expression inner = var425.inner();
                                        var2 = this.intermediateCompileExpression( inner ).map( ( i ) -> {
                                            Set x$68 = (Set) i.nullChecks().$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                    this.getLongAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                    BoxesRunTime.boxToLong( -1L ) )));
                                            boolean x$69 = true;
                                            IntermediateRepresentation x$70 = i.copy$default$1();
                                            Seq x$71 = i.copy$default$2();
                                            Seq x$72 = i.copy$default$3();
                                            return i.copy( x$70, x$71, x$72, x$68, x$69 );
                                        } );
                                    }
                                    else if ( expression instanceof NullCheckVariable )
                                    {
                                        NullCheckVariable var428 = (NullCheckVariable) expression;
                                        int offset = var428.offset();
                                        LogicalVariable inner = var428.inner();
                                        var2 = this.intermediateCompileExpression( inner ).map( ( i ) -> {
                                            Set x$73 = (Set) i.nullChecks().$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                    this.getLongAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                    BoxesRunTime.boxToLong( -1L ) )));
                                            boolean x$74 = true;
                                            IntermediateRepresentation x$75 = i.copy$default$1();
                                            Seq x$76 = i.copy$default$2();
                                            Seq x$77 = i.copy$default$3();
                                            return i.copy( x$75, x$76, x$77, x$73, x$74 );
                                        } );
                                    }
                                    else if ( expression instanceof NullCheckProperty )
                                    {
                                        NullCheckProperty var431 = (NullCheckProperty) expression;
                                        int offset = var431.offset();
                                        LogicalProperty inner = var431.inner();
                                        var2 = this.intermediateCompileExpression( inner ).map( ( i ) -> {
                                            Set x$78 = (Set) i.nullChecks().$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                    this.getLongAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                    BoxesRunTime.boxToLong( -1L ) )));
                                            boolean x$79 = true;
                                            IntermediateRepresentation x$80 = i.copy$default$1();
                                            Seq x$81 = i.copy$default$2();
                                            Seq x$82 = i.copy$default$3();
                                            return i.copy( x$80, x$81, x$82, x$78, x$79 );
                                        } );
                                    }
                                    else if ( expression instanceof NullCheckReferenceProperty )
                                    {
                                        NullCheckReferenceProperty var434 = (NullCheckReferenceProperty) expression;
                                        int offset = var434.offset();
                                        LogicalProperty inner = var434.inner();
                                        var2 = this.intermediateCompileExpression( inner ).map( ( i ) -> {
                                            Set x$83 = (Set) i.nullChecks().$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                                    this.getRefAt( offset ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()));
                                            boolean x$84 = true;
                                            IntermediateRepresentation x$85 = i.copy$default$1();
                                            Seq x$86 = i.copy$default$2();
                                            Seq x$87 = i.copy$default$3();
                                            return i.copy( x$85, x$86, x$87, x$83, x$84 );
                                        } );
                                    }
                                    else if ( expression instanceof IsPrimitiveNull )
                                    {
                                        IsPrimitiveNull var437 = (IsPrimitiveNull) expression;
                                        int offset = var437.offset();
                                        var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getLongAt( offset ),
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                                BoxesRunTime.boxToLong( -1L ) ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue()),(Seq) scala.collection.Seq..
                                        MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
                                        MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
                                    }
                                    else
                                    {
                                        var2 = .MODULE$;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return (Option) var2;
    }

    private Tuple2<Seq<InstanceField>,Seq<IntermediateRepresentation>> tokenFieldsForLabels( final Seq<String> labels )
    {
        Seq tokensAndInits = (Seq) labels.map( ( label ) -> {
            InstanceField tokenField = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.field( this.namer().variableName( label ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
            IntermediateRepresentation init = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( tokenField ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.setField( tokenField, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeLabel", scala.reflect.ManifestFactory..MODULE$.classType(
                    DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),
            scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( label )})))))
            ;
            return new Tuple2( tokenField, init );
        }, scala.collection.Seq..MODULE$.canBuildFrom());
        return tokensAndInits.unzip( scala.Predef..MODULE$.$conforms());
    }

    public Option<IntermediateExpression> compileFunction( final FunctionInvocation c )
    {
        boolean var4 = false;
        Object var5 = null;
        boolean var6 = false;
        Object var7 = null;
        Function var8 = c.function();
        Object var2;
        if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Acos..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "acos", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Cos..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "cos", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Cot..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "cot", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Asin..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asin", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Haversin..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "haversin", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Sin..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "sin", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Atan..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "atan", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Atan2..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( y ) -> {
            return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).map( ( x ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "atan2", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{y.ir(), x.ir()}) )),
                (Seq) y.fields().$plus$plus( x.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                (Seq) y.variables().$plus$plus( x.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                (Set) y.nullChecks().$plus$plus( x.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Tan..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "tan", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Round..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "round", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Rand..MODULE$.equals( var8 )){
        var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "rand", scala.reflect.ManifestFactory..MODULE$.classType(
                CypherFunctions.class ), scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class )),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),(Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..
        MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Abs..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "abs", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( NumberValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Ceil..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "ceil", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Floor..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "floor", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Degrees..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toDegrees", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Exp..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "exp", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Log..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "log", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Log10..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "log10", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Radians..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toRadians", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Sign..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "signum", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( LongValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Sqrt..MODULE$.equals( var8 )){
        var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "sqrt", scala.reflect.ManifestFactory..MODULE$.classType(
                    CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
            in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
        } );
    } else{
        if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Range..MODULE$.equals( var8 )){
            var4 = true;
            if ( c.args().length() == 2 )
            {
                var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( start ) -> {
                    return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).map( ( end ) -> {
                        return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "range", scala.reflect.ManifestFactory..MODULE$.classType(
                                CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired( start, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ),
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired( end, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )),
                        (Seq) start.fields().$plus$plus( end.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                        (Seq) start.variables().$plus$plus( end.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),scala.Predef..
                        MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5());
                    } );
                } ); return (Option) var2;
            }
        }

        if ( var4 && c.args().length() == 3 )
        {
            var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( start ) -> {
                return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).flatMap( ( end ) -> {
                    return this.intermediateCompileExpression( (Expression) c.args().apply( 2 ) ).map( ( step ) -> {
                        return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "range", scala.reflect.ManifestFactory..MODULE$.classType(
                                CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired( start, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ),
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired( end, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ),
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired( step, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )),
                        (Seq) ((TraversableLike) start.fields().$plus$plus( end.fields(), scala.collection.Seq..MODULE$.canBuildFrom())).
                        $plus$plus( step.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                        (Seq) ((TraversableLike) start.variables().$plus$plus( end.variables(), scala.collection.Seq..MODULE$.canBuildFrom())).
                        $plus$plus( step.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),scala.Predef..
                        MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5());
                    } );
                } );
            } );
        }
        else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Pi..MODULE$.equals( var8 )){
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( "PI",
                    scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class )),
            (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
            MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.E..MODULE$.equals( var8 )){
            var2 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.getStatic( "E",
                    scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType( DoubleValue.class )),
            (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
            MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Coalesce..MODULE$.equals( var8 )){
            IndexedSeq args = (IndexedSeq) c.args().flatMap( ( expression ) -> {
                return scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( expression ) );
            }, scala.collection.IndexedSeq..MODULE$.canBuildFrom());
            Object var10000;
            if ( args.size() < c.args().size() )
            {
                var10000 = .MODULE$;
            }
            else
            {
                String tempVariable = this.namer().nextVariableName();
                LocalVariable local = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.variable( tempVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ));
                IntermediateRepresentation repr = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{loop$2( args.toList(), tempVariable ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( tempVariable )})));
                var10000 = new Some( new IntermediateExpression( repr, (Seq) args.foldLeft( scala.collection.Seq..MODULE$.empty(), ( a, b ) -> {
                    return (Seq) a.$plus$plus( b.fields(), scala.collection.Seq..MODULE$.canBuildFrom());
                } ), (Seq) ((SeqLike) args.foldLeft( scala.collection.Seq..MODULE$.empty(), ( a, b ) -> {
                    return (Seq) a.$plus$plus( b.variables(), scala.collection.Seq..MODULE$.canBuildFrom());
                } )).$colon$plus( local, scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( tempVariable ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())}))),IntermediateExpression$.MODULE$.apply$default$5()));
            }

            var2 = var10000;
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Distance..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( p1 ) -> {
                return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).map( ( p2 ) -> {
                    String variableName = this.namer().nextVariableName();
                    IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            AnyValue.class )),variableName, org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "distance",
                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{p1.ir(), p2.ir()}) ))))
                    ;
                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                    ;
                    IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                    return new IntermediateExpression( ops, (Seq) p1.fields().$plus$plus( p2.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                    (Seq) p1.variables().$plus$plus( p2.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                    MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),
                    IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.StartNode..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "startNode", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( RelationshipScanCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{in.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR()}) )),
                in.fields(), (Seq) in.variables().$colon$plus( ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(), scala.collection.Seq..MODULE$.canBuildFrom()),
                in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.EndNode..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "endNode", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( RelationshipScanCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{in.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR()}) )),
                in.fields(), (Seq) in.variables().$colon$plus( ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(), scala.collection.Seq..MODULE$.canBuildFrom()),
                in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Nodes..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodes", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Relationships..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationships", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Exists..MODULE$.equals( var8 )){
            Expression var13 = (Expression) c.arguments().head();
            Object var3;
            if ( var13 instanceof Property )
            {
                Property var14 = (Property) var13;
                var3 = this.intermediateCompileExpression( var14.map() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyExists",
                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),scala.reflect.ManifestFactory..
                    MODULE$.classType( BooleanValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( String.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                            var14.propertyKey().name() ), in.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(), ExpressionCompiler$.MODULE$.NODE_CURSOR(),
                            ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}))),
                    in.fields(), (Seq) in.variables().$plus$plus(
                            ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                            scala.collection.Seq..MODULE$.canBuildFrom()),in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            }
            else if ( var13 instanceof ASTCachedProperty )
            {
                ASTCachedProperty var15 = (ASTCachedProperty) var13;
                var3 = this.cachedExists( var15 );
            }
            else if ( var13 instanceof ContainerIndex )
            {
                ContainerIndex var16 = (ContainerIndex) var13;
                var3 = this.containerIndexAccess( var16.expr(), var16.idx(), true );
            }
            else if ( var13 instanceof PatternExpression )
            {
                var3 = .MODULE$;
            }
            else if ( var13 instanceof NestedPipeExpression )
            {
                var3 = .MODULE$;
            }
            else
            {
                if ( var13 instanceof NestedPlanExpression )
                {
                    throw new InternalException( "should have been rewritten away" );
                }

                var3 = .MODULE$;
            }

            var2 = var3;
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Head..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                String variableName = this.namer().nextVariableName();
                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateExpression[]{in}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "head",
                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )))));
                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                ;
                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                return new IntermediateExpression( ops, in.fields(), in.variables(), (Set) scala.Predef..MODULE$.Set().apply(
                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Id..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "id", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( LongValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Labels..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "labels", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( NodeCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{in.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                ExpressionCompiler$.MODULE$.NODE_CURSOR()}) )),
                in.fields(), (Seq) in.variables().$colon$plus( ExpressionCompiler$.MODULE$.vNODE_CURSOR(), scala.collection.Seq..MODULE$.canBuildFrom()),
                in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Type..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "type", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Last..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                String variableName = this.namer().nextVariableName();
                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateExpression[]{in}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "last",
                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )))));
                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                ;
                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                return new IntermediateExpression( ops, in.fields(), in.variables(), (Set) scala.Predef..MODULE$.Set().apply(
                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Left..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( in ) -> {
                return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).map( ( endPos ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "left", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir(), endPos.ir()}) )),
                    (Seq) in.fields().$plus$plus( endPos.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) in.variables().$plus$plus( endPos.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.LTrim..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "ltrim", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.RTrim..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "rtrim", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Trim..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "trim", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Replace..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( original ) -> {
                return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).flatMap( ( search ) -> {
                    return this.intermediateCompileExpression( (Expression) c.args().apply( 2 ) ).map( ( replaceWith ) -> {
                        return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "replace", scala.reflect.ManifestFactory..MODULE$.classType(
                                CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( AnyValue.class )),scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{original.ir(), search.ir(), replaceWith.ir()}) )),
                        (Seq) ((TraversableLike) original.fields().$plus$plus( search.fields(), scala.collection.Seq..MODULE$.canBuildFrom())).
                        $plus$plus( replaceWith.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                        (Seq) ((TraversableLike) original.variables().$plus$plus( search.variables(), scala.collection.Seq..MODULE$.canBuildFrom())).
                        $plus$plus( replaceWith.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                        (Set) original.nullChecks().$plus$plus( search.nullChecks() ).$plus$plus(
                                replaceWith.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                    } );
                } );
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Reverse..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "reverse", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Right..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( in ) -> {
                return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).map( ( len ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "right", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir(), len.ir()}) )),
                    (Seq) in.fields().$plus$plus( len.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) in.variables().$plus$plus( len.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Split..MODULE$.equals( var8 )){
            var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( original ) -> {
                return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).map( ( sep ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "split", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{original.ir(), sep.ir()}) )),
                    (Seq) original.fields().$plus$plus( sep.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Seq) original.variables().$plus$plus( sep.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                    (Set) original.nullChecks().$plus$plus( sep.nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } );
        } else{
            if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Substring..MODULE$.equals( var8 )){
                var6 = true;
                if ( c.args().size() == 2 )
                {
                    var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( original ) -> {
                        return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).map( ( start ) -> {
                            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "substring",
                                    scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),scala.reflect.ManifestFactory..
                            MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{original.ir(), start.ir()}) )),
                            (Seq) original.fields().$plus$plus( start.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                            (Seq) original.variables().$plus$plus( start.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                            original.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                        } );
                    } ); return (Option) var2;
                }
            }

            if ( var6 )
            {
                var2 = this.intermediateCompileExpression( (Expression) c.args().apply( 0 ) ).flatMap( ( original ) -> {
                    return this.intermediateCompileExpression( (Expression) c.args().apply( 1 ) ).flatMap( ( start ) -> {
                        return this.intermediateCompileExpression( (Expression) c.args().apply( 2 ) ).map( ( len ) -> {
                            return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "substring",
                                    scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),scala.reflect.ManifestFactory..
                            MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{original.ir(), start.ir(), len.ir()}) )),
                            (Seq) ((TraversableLike) original.fields().$plus$plus( start.fields(), scala.collection.Seq..MODULE$.canBuildFrom())).
                            $plus$plus( len.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                            (Seq) ((TraversableLike) original.variables().$plus$plus( start.variables(), scala.collection.Seq..MODULE$.canBuildFrom())).
                            $plus$plus( len.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                            original.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                        } );
                    } );
                } );
            }
            else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.ToLower..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toLower", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                    in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.ToUpper..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toUpper", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                    in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Point..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "point", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( ExpressionCursors.class )),scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{in.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                                    ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$CURSORS()}) )),
                    in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Keys..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "keys", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                            ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                            ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}) )),in.fields(), (Seq) in.variables().$plus$plus(
                            ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                            scala.collection.Seq..MODULE$.canBuildFrom()),in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Size..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "size", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( IntegralValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                    in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Length..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "length", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( IntegralValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                    in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Tail..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "tail", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                    in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.ToBoolean..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    String variableName = this.namer().nextVariableName();
                    IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            AnyValue.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateExpression[]{in}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toBoolean",
                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )))));
                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                    ;
                    IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                    return new IntermediateExpression( ops, in.fields(), in.variables(), (Set) scala.Predef..MODULE$.Set().apply(
                            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.ToFloat..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    String variableName = this.namer().nextVariableName();
                    IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            AnyValue.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateExpression[]{in}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toFloat",
                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )))));
                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                    ;
                    IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                    return new IntermediateExpression( ops, in.fields(), in.variables(), (Set) scala.Predef..MODULE$.Set().apply(
                            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.ToInteger..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    String variableName = this.namer().nextVariableName();
                    IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            AnyValue.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateExpression[]{in}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toInteger",
                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )))));
                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                    ;
                    IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                    return new IntermediateExpression( ops, in.fields(), in.variables(), (Set) scala.Predef..MODULE$.Set().apply(
                            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.ToString..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "toString", scala.reflect.ManifestFactory..MODULE$.classType(
                            CypherFunctions.class ),scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir()}) )),
                    in.fields(), in.variables(), in.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                } );
            } else if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Properties..MODULE$.equals( var8 )){
                var2 = this.intermediateCompileExpression( (Expression) c.args().head() ).map( ( in ) -> {
                    String variableName = this.namer().nextVariableName();
                    IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            AnyValue.class )),variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateExpression[]{in}) ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "properties",
                            scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( MapValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{in.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                            ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                            ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}) )))));
                    IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                    ;
                    IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                    return new IntermediateExpression( ops, in.fields(), (Seq) in.variables().$plus$plus(
                            ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                            scala.collection.Seq..MODULE$.canBuildFrom() ),(Set) scala.Predef..
                    MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
                } );
            } else{
                var2 = .MODULE$;
            }
        }
    }

        return (Option) var2;
    }

    public Option<IntermediateExpression> intermediateCompileProjection( final Map<String,Expression> projections )
    {
        int removed = projections.keys().count( ( x$44 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileProjection$1( this, x$44 ) );
        } );
        Map compiled = (Map) projections.withFilter( ( check$ifrefutable$4 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileProjection$2( check$ifrefutable$4 ) );
        } ).flatMap( ( x$45 ) -> {
            if ( x$45 != null )
            {
                String k = (String) x$45._1();
                Expression v = (Expression) x$45._2();
                Iterable var2 = scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( v ).withFilter( ( c ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$intermediateCompileProjection$4( this, k, c ) );
            } ).map( ( c ) -> {
                return scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( BoxesRunTime.boxToInteger( this.slots().apply( k ).offset() ) ), c);
            } ) ); return var2;
            }
            else
            {
                throw new MatchError( x$45 );
            }
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
        Object var10000;
        if ( compiled.size() + removed < projections.size() )
        {
            var10000 = .MODULE$;
        }
        else
        {
            Seq all = (Seq) compiled.toSeq().map( ( x0$7 ) -> {
                if ( x0$7 != null )
                {
                    int slot = x0$7._1$mcI$sp();
                    IntermediateExpression value = (IntermediateExpression) x0$7._2();
                    IntermediateRepresentation var2 = this.setRefAt( slot,
                            ExpressionCompiler$.MODULE$.nullCheckIfRequired( value, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) );
                    return var2;
                }
                else
                {
                    throw new MatchError( x0$7 );
                }
            }, scala.collection.Seq..MODULE$.canBuildFrom());
            var10000 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( all ),
                    ((TraversableOnce) compiled.values().flatMap( ( x$46 ) -> {
                        return x$46.fields();
                    }, scala.collection.Iterable..MODULE$.canBuildFrom()) ).toSeq(), ((TraversableOnce) compiled.values().flatMap( ( x$47 ) -> {
                return x$47.variables();
            }, scala.collection.Iterable..MODULE$.canBuildFrom())).toSeq(), scala.Predef..
            MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
        }

        return (Option) var10000;
    }

    private IntermediateGroupingExpression intermediateCompileGroupingExpression( final Seq<Tuple2<Slot,IntermediateExpression>> orderedGroupings )
    {
        scala.Predef..MODULE$. assert (orderedGroupings.nonEmpty());
        String listVar = this.namer().nextVariableName();
        boolean singleValue = orderedGroupings.size() == 1;
        Seq projectKeyOps = (Seq) ((TraversableLike) ((IterableLike) orderedGroupings.map( ( x$48 ) -> {
            return (Slot) x$48._1();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).zipWithIndex( scala.collection.Seq..MODULE$.canBuildFrom())).map( ( x0$8 ) -> {
        IntermediateRepresentation var4;
        int indexx;
        int offsetxx;
        boolean nullablex;
        label68:
        {
            if ( x0$8 != null )
            {
                Slot var6 = (Slot) x0$8._1();
                indexx = x0$8._2$mcI$sp();
                if ( var6 instanceof LongSlot )
                {
                    LongSlot var8 = (LongSlot) var6;
                    offsetxx = var8.offset();
                    nullablex = var8.nullable();
                    CypherType var11 = var8.typ();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var11 == null )
                        {
                            break label68;
                        }
                    }
                    else if ( var10000.equals( var11 ) )
                    {
                        break label68;
                    }
                }
            }

            int index;
            int offsetx;
            boolean nullable;
            label69:
            {
                if ( x0$8 != null )
                {
                    Slot var13 = (Slot) x0$8._1();
                    index = x0$8._2$mcI$sp();
                    if ( var13 instanceof LongSlot )
                    {
                        LongSlot var15 = (LongSlot) var13;
                        offsetx = var15.offset();
                        nullable = var15.nullable();
                        CypherType var18 = var15.typ();
                        RelationshipType var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                        if ( var24 == null )
                        {
                            if ( var18 == null )
                            {
                                break label69;
                            }
                        }
                        else if ( var24.equals( var18 ) )
                        {
                            break label69;
                        }
                    }
                }

                if ( x0$8 == null )
                {
                    throw new InternalException( (new StringBuilder( 40 )).append( "Do not know how to make setter for slot " ).append( x0$8 ).toString() );
                }

                Slot var20 = (Slot) x0$8._1();
                int indexxx = x0$8._2$mcI$sp();
                if ( !(var20 instanceof RefSlot) )
                {
                    throw new InternalException( (new StringBuilder( 40 )).append( "Do not know how to make setter for slot " ).append( x0$8 ).toString() );
                }

                RefSlot var22 = (RefSlot) var20;
                int offset = var22.offset();
                var4 = this.setRefAt( offset, accessValue$1( indexxx, listVar, singleValue ) );
                return var4;
            }

            var4 = this.setLongAt( offsetx, id$1( accessValue$1( index, listVar, singleValue ), nullable, scala.reflect.ManifestFactory..MODULE$.classType(
                    VirtualRelationshipValue.class ) ));
            return var4;
        }

        var4 = this.setLongAt( offsetxx,
                id$1( accessValue$1( indexx, listVar, singleValue ), nullablex, scala.reflect.ManifestFactory..MODULE$.classType( VirtualNodeValue.class ) ));
        return var4;
    }, scala.collection.Seq..MODULE$.canBuildFrom());
        Seq projectKey = singleValue ? projectKeyOps : (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Product[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( listVar,
                        scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign(
                listVar, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                "key" ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )))})))).
        $plus$plus( projectKeyOps, scala.collection.Seq..MODULE$.canBuildFrom());
        Seq getKeyOps = (Seq) ((TraversableLike) orderedGroupings.map( ( x$49 ) -> {
            return (Slot) x$49._1();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).map( ( x0$9 ) -> {
        IntermediateRepresentation var2;
        int offsetx;
        boolean nullablex;
        label68:
        {
            boolean var3 = false;
            LongSlot var4 = null;
            if ( x0$9 instanceof LongSlot )
            {
                var3 = true;
                var4 = (LongSlot) x0$9;
                offsetx = var4.offset();
                nullablex = var4.nullable();
                CypherType var8 = var4.typ();
                NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                if ( var10000 == null )
                {
                    if ( var8 == null )
                    {
                        break label68;
                    }
                }
                else if ( var10000.equals( var8 ) )
                {
                    break label68;
                }
            }

            int offset;
            boolean nullable;
            label69:
            {
                if ( var3 )
                {
                    offset = var4.offset();
                    nullable = var4.nullable();
                    CypherType var13 = var4.typ();
                    RelationshipType var18 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var18 == null )
                    {
                        if ( var13 == null )
                        {
                            break label69;
                        }
                    }
                    else if ( var18.equals( var13 ) )
                    {
                        break label69;
                    }
                }

                if ( !(x0$9 instanceof RefSlot) )
                {
                    throw new InternalException( (new StringBuilder( 40 )).append( "Do not know how to make getter for slot " ).append( x0$9 ).toString() );
                }

                RefSlot var16 = (RefSlot) x0$9;
                int offsetxx = var16.offset();
                var2 = this.getRefAt( offsetxx );
                return var2;
            }

            IntermediateRepresentation getter = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationship",
                    scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( RelationshipReference.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset )}) ));
            var2 = nullable ? org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getLongAt( offset ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(), getter) :getter;
            return var2;
        }

        IntermediateRepresentation getterx = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "node", scala.reflect.ManifestFactory..MODULE$.classType(
                VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeReference.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offsetx )}) ));
        var2 = nullablex ? org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getLongAt( offsetx ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(), getterx) :getterx;
        return var2;
    }, scala.collection.Seq..MODULE$.canBuildFrom());
        IntermediateRepresentation getKey = singleValue ? (IntermediateRepresentation) getKeyOps.head() : org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "list", scala.reflect.ManifestFactory..MODULE$.classType(
                VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( getKeyOps,
                    scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))})));
        IntermediateRepresentation[] computeKeyOps = (IntermediateRepresentation[]) ((TraversableOnce) ((TraversableLike) orderedGroupings.map( ( x$50 ) -> {
            return (IntermediateExpression) x$50._2();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).map( ( p ) -> {
            return ExpressionCompiler$.MODULE$.nullCheckIfRequired( p, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() );
        }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( IntermediateRepresentation.class ));
        IntermediateRepresentation computeKey =
                singleValue ? (IntermediateRepresentation) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) computeKeyOps ))).head() :
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "list", scala.reflect.ManifestFactory..MODULE$.classType(
                VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
        MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) computeKeyOps ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ))})));
        Seq orderedGroupingExpressions = (Seq) orderedGroupings.map( ( x$51 ) -> {
            return (IntermediateExpression) x$51._2();
        }, scala.collection.Seq..MODULE$.canBuildFrom());
        return new IntermediateGroupingExpression( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( projectKey ),
                (Seq) scala.collection.Seq..MODULE$.empty(),(Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
        MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()),
        new IntermediateExpression( computeKey, (Seq) orderedGroupingExpressions.flatMap( ( x$52 ) -> {
            return x$52.fields();
        }, scala.collection.Seq..MODULE$.canBuildFrom() ), (Seq) orderedGroupingExpressions.flatMap( ( x$53 ) -> {
            return x$53.variables();
        }, scala.collection.Seq..MODULE$.canBuildFrom()),scala.Predef..MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()),
        new IntermediateExpression( getKey, (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..
        MODULE$.Set().empty(), IntermediateExpression$.MODULE$.apply$default$5()));
    }

    public abstract IntermediateRepresentation getLongAt( final int offset );

    public abstract IntermediateRepresentation getRefAt( final int offset );

    public abstract IntermediateRepresentation setRefAt( final int offset, final IntermediateRepresentation value );

    public abstract IntermediateRepresentation setLongAt( final int offset, final IntermediateRepresentation value );

    public abstract IntermediateRepresentation setCachedPropertyAt( final int offset, final IntermediateRepresentation value );

    public abstract IntermediateRepresentation getCachedPropertyAt( final SlottedCachedProperty property, final IntermediateRepresentation getFromStore );

    public abstract IntermediateRepresentation isLabelSetOnNode( final IntermediateRepresentation labelToken, final int offset );

    public abstract IntermediateRepresentation getNodeProperty( final IntermediateRepresentation propertyToken, final int offset );

    public abstract IntermediateRepresentation getRelationshipProperty( final IntermediateRepresentation propertyToken, final int offset );

    public abstract IntermediateRepresentation getProperty( final String key, final IntermediateRepresentation container );

    public IntermediateRepresentation getArgumentAt( final int offset )
    {
        return offset == TopLevelArgument$.MODULE$.SLOT_OFFSET() ? org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToLong( 0L ) ) :this.getLongAt( offset );
    }

    public final IntermediateRepresentation getLongFromExecutionContext( final int offset, final IntermediateRepresentation context )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( context, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "getLongAt",
                scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class ), scala.reflect.ManifestFactory..
        MODULE$.Long(), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( offset ) )})));
    }

    public final IntermediateRepresentation getLongFromExecutionContext$default$2()
    {
        return ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT();
    }

    public final IntermediateRepresentation getRefFromExecutionContext( final int offset, final IntermediateRepresentation context )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( context, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "getRefAt", scala.reflect.ManifestFactory..MODULE$.classType(
                ExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.Int()),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
            BoxesRunTime.boxToInteger( offset ) )})));
    }

    public final IntermediateRepresentation getRefFromExecutionContext$default$2()
    {
        return ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT();
    }

    public final IntermediateRepresentation getCachedPropertyFromExecutionContext( final int offset, final IntermediateRepresentation context )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( context, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "getCachedPropertyAt",
                scala.reflect.ManifestFactory..MODULE$.classType( ExecutionContext.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..MODULE$.Int()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( offset ) )})));
    }

    public final IntermediateRepresentation getCachedPropertyFromExecutionContext$default$2()
    {
        return ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT();
    }

    public final IntermediateRepresentation setRefInExecutionContext( final int offset, final IntermediateRepresentation value )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeSideEffect( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "setRefAt", scala.reflect.ManifestFactory..MODULE$.classType(
                ExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( offset ) ), value})));
    }

    public final IntermediateRepresentation setLongInExecutionContext( final int offset, final IntermediateRepresentation value )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeSideEffect( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "setLongAt", scala.reflect.ManifestFactory..MODULE$.classType(
                ExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( offset ) ), value})));
    }

    public final IntermediateRepresentation setCachedPropertyInExecutionContext( final int offset, final IntermediateRepresentation value )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invokeSideEffect( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT(),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "setCachedPropertyAt", scala.reflect.ManifestFactory..MODULE$.classType(
                ExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
        MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( offset ) ), value})));
    }

    private IntermediateExpression coerceToPredicate( final IntermediateExpression e )
    {
        return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "coerceToBoolean", scala.reflect.ManifestFactory..MODULE$.classType(
                CypherBoolean.class ),scala.reflect.ManifestFactory..MODULE$.classType( Value.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{e.ir()}) )),
        e.fields(), e.variables(), e.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
    }

    private IntermediateExpression generateAnds( final List<IntermediateExpression> expressions )
    {
        return this.generateCompositeBoolean( expressions, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue());
    }

    private IntermediateExpression generateOrs( final List<IntermediateExpression> expressions )
    {
        return this.generateCompositeBoolean( expressions, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue());
    }

    private IntermediateExpression generateCompositeBoolean( final List<IntermediateExpression> expressions, final IntermediateRepresentation breakValue )
    {
        boolean nullable = expressions.exists( ( x$54 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$generateCompositeBoolean$1( x$54 ) );
        } );
        String returnValue = this.namer().nextVariableName();
        LocalVariable local = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.variable( returnValue, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null ), scala.reflect.ManifestFactory..
        MODULE$.classType( AnyValue.class ));
        String seenNull = this.namer().nextVariableName();
        String error = this.namer().nextVariableName();
        IntermediateRepresentation var9 = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnValue ), breakValue);
        Function1 ifNotBreakValue = ( onTrue ) -> {
            return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( var9, onTrue );
        }; Function1 inner = ( e ) -> {
        String exceptionName = this.namer().nextVariableName();
        TryCatch loadValue = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.tryCatch( exceptionName, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( returnValue,
                ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{e}) ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$ASSERT_PREDICATE(),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{e.ir()}) )))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( error, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( exceptionName )),scala.reflect.ManifestFactory..
        MODULE$.classType( RuntimeException.class ));
        return nullable ? (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Product[]{loadValue, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( seenNull,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnValue ),
                breakValue ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( seenNull ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( BoxesRunTime.boxToBoolean( true ) ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnValue ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.noValue()))))}))) :(Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TryCatch[]{loadValue}) ));
    }; IntermediateExpression firstExpression = (IntermediateExpression) expressions.head();
        Seq nullChecks = nullable ? (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new Product[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( seenNull,
                    scala.reflect.ManifestFactory..MODULE$.Boolean()), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( seenNull,
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( false ) ))}))) :(Seq) scala.collection.Seq..
        MODULE$.empty();
        Seq nullCheckAssign = firstExpression.nullChecks().nonEmpty() ? (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new AssignToLocalVariable[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( seenNull,
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                    returnValue ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()))}))) :(Seq) scala.collection.Seq..MODULE$.empty();
        String exceptionName = this.namer().nextVariableName();
        IntermediateRepresentation actualReturnValue = nullable ? org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( seenNull ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.noValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnValue )) :org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.load( returnValue );
        IntermediateRepresentation ir = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
            (Seq) ((TraversableLike) ((TraversableLike) nullChecks.$plus$plus( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Product[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( error,
                            scala.reflect.ManifestFactory..MODULE$.classType( RuntimeException.class )),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( error, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    (Object) null )), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.tryCatch( exceptionName,
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( returnValue,
            ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{firstExpression}) ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
            ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$ASSERT_PREDICATE(),
            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{firstExpression.ir()}) )))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( error, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( exceptionName )),
        scala.reflect.ManifestFactory..MODULE$.classType( RuntimeException.class ))}))),scala.collection.Seq..MODULE$.canBuildFrom())).
        $plus$plus( nullCheckAssign, scala.collection.Seq..MODULE$.canBuildFrom())).
        $plus$plus( scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{loop$3( (List) expressions.tail(), ifNotBreakValue, inner ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.and(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                        error ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null )),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.notEqual( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnValue ), breakValue)),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.fail( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( error ))),
        actualReturnValue}))),scala.collection.Seq..MODULE$.canBuildFrom()));
        return new IntermediateExpression( ir, (Seq) expressions.foldLeft( scala.collection.Seq..MODULE$.empty(), ( a, b ) -> {
            return (Seq) a.$plus$plus( b.fields(), scala.collection.Seq..MODULE$.canBuildFrom());
        } ),(Seq) ((SeqLike) expressions.foldLeft( scala.collection.Seq..MODULE$.empty(), ( a, b ) -> {
        return (Seq) a.$plus$plus( b.variables(), scala.collection.Seq..MODULE$.canBuildFrom());
    })).$colon$plus( local, scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnValue ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())}))),false);
    }

    private IntermediateRepresentation asNeoType( final CypherType ct )
    {
        Object var2;
        label312:
        {
            StringType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
            if ( var10000 == null )
            {
                if ( ct == null )
                {
                    break label312;
                }
            }
            else if ( var10000.equals( ct ) )
            {
                break label312;
            }

            label313:
            {
                IntegerType var26 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                if ( var26 == null )
                {
                    if ( ct != null )
                    {
                        break label313;
                    }
                }
                else if ( !var26.equals( ct ) )
                {
                    break label313;
                }

                var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.getStatic( "NTInteger", scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.IntegerType.class ));
                return (IntermediateRepresentation) var2;
            }

            label314:
            {
                FloatType var27 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                if ( var27 == null )
                {
                    if ( ct == null )
                    {
                        break label314;
                    }
                }
                else if ( var27.equals( ct ) )
                {
                    break label314;
                }

                label315:
                {
                    NumberType var28 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNumber();
                    if ( var28 == null )
                    {
                        if ( ct == null )
                        {
                            break label315;
                        }
                    }
                    else if ( var28.equals( ct ) )
                    {
                        break label315;
                    }

                    label316:
                    {
                        BooleanType var29 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                        if ( var29 == null )
                        {
                            if ( ct != null )
                            {
                                break label316;
                            }
                        }
                        else if ( !var29.equals( ct ) )
                        {
                            break label316;
                        }

                        var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.getStatic( "NTBoolean", scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.BooleanType.class ));
                        return (IntermediateRepresentation) var2;
                    }

                    if ( ct instanceof ListType )
                    {
                        ListType var9 = (ListType) ct;
                        var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "NTList",
                                scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.ListType.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.AnyType.class )),scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.asNeoType( var9.innerType() )}) ));
                        return (IntermediateRepresentation) var2;
                    }

                    label317:
                    {
                        DateTimeType var30 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTDateTime();
                        if ( var30 == null )
                        {
                            if ( ct == null )
                            {
                                break label317;
                            }
                        }
                        else if ( var30.equals( ct ) )
                        {
                            break label317;
                        }

                        label318:
                        {
                            LocalDateTimeType var31 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTLocalDateTime();
                            if ( var31 == null )
                            {
                                if ( ct != null )
                                {
                                    break label318;
                                }
                            }
                            else if ( !var31.equals( ct ) )
                            {
                                break label318;
                            }

                            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.getStatic( "NTLocalDateTime", scala.reflect.ManifestFactory..MODULE$.classType(
                                    Neo4jTypes.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.LocalDateTimeType.class ));
                            return (IntermediateRepresentation) var2;
                        }

                        label319:
                        {
                            DateType var32 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTDate();
                            if ( var32 == null )
                            {
                                if ( ct == null )
                                {
                                    break label319;
                                }
                            }
                            else if ( var32.equals( ct ) )
                            {
                                break label319;
                            }

                            label320:
                            {
                                TimeType var33 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTTime();
                                if ( var33 == null )
                                {
                                    if ( ct != null )
                                    {
                                        break label320;
                                    }
                                }
                                else if ( !var33.equals( ct ) )
                                {
                                    break label320;
                                }

                                var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.getStatic( "NTTime", scala.reflect.ManifestFactory..MODULE$.classType(
                                        Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.TimeType.class ));
                                return (IntermediateRepresentation) var2;
                            }

                            label321:
                            {
                                LocalTimeType var34 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTLocalTime();
                                if ( var34 == null )
                                {
                                    if ( ct != null )
                                    {
                                        break label321;
                                    }
                                }
                                else if ( !var34.equals( ct ) )
                                {
                                    break label321;
                                }

                                var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.getStatic( "NTLocalTime", scala.reflect.ManifestFactory..MODULE$.classType(
                                        Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.LocalTimeType.class ));
                                return (IntermediateRepresentation) var2;
                            }

                            label322:
                            {
                                DurationType var35 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTDuration();
                                if ( var35 == null )
                                {
                                    if ( ct == null )
                                    {
                                        break label322;
                                    }
                                }
                                else if ( var35.equals( ct ) )
                                {
                                    break label322;
                                }

                                label323:
                                {
                                    PointType var36 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTPoint();
                                    if ( var36 == null )
                                    {
                                        if ( ct == null )
                                        {
                                            break label323;
                                        }
                                    }
                                    else if ( var36.equals( ct ) )
                                    {
                                        break label323;
                                    }

                                    label324:
                                    {
                                        NodeType var37 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                        if ( var37 == null )
                                        {
                                            if ( ct == null )
                                            {
                                                break label324;
                                            }
                                        }
                                        else if ( var37.equals( ct ) )
                                        {
                                            break label324;
                                        }

                                        label325:
                                        {
                                            RelationshipType var38 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                            if ( var38 == null )
                                            {
                                                if ( ct != null )
                                                {
                                                    break label325;
                                                }
                                            }
                                            else if ( !var38.equals( ct ) )
                                            {
                                                break label325;
                                            }

                                            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                            MODULE$.getStatic( "NTRelationship", scala.reflect.ManifestFactory..MODULE$.classType(
                                                    Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                            MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.RelationshipType.class ));
                                            return (IntermediateRepresentation) var2;
                                        }

                                        label326:
                                        {
                                            PathType var39 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTPath();
                                            if ( var39 == null )
                                            {
                                                if ( ct != null )
                                                {
                                                    break label326;
                                                }
                                            }
                                            else if ( !var39.equals( ct ) )
                                            {
                                                break label326;
                                            }

                                            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                            MODULE$.getStatic( "NTPath", scala.reflect.ManifestFactory..MODULE$.classType(
                                                    Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                            MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.PathType.class ));
                                            return (IntermediateRepresentation) var2;
                                        }

                                        label327:
                                        {
                                            GeometryType var23 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTGeometry();
                                            if ( var23 == null )
                                            {
                                                if ( ct != null )
                                                {
                                                    break label327;
                                                }
                                            }
                                            else if ( !var23.equals( ct ) )
                                            {
                                                break label327;
                                            }

                                            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                            MODULE$.getStatic( "NTGeometry", scala.reflect.ManifestFactory..MODULE$.classType(
                                                    Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                            MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.GeometryType.class ));
                                            return (IntermediateRepresentation) var2;
                                        }

                                        label328:
                                        {
                                            MapType var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTMap();
                                            if ( var24 == null )
                                            {
                                                if ( ct == null )
                                                {
                                                    break label328;
                                                }
                                            }
                                            else if ( var24.equals( ct ) )
                                            {
                                                break label328;
                                            }

                                            AnyType var25 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
                                            if ( var25 == null )
                                            {
                                                if ( ct != null )
                                                {
                                                    throw new MatchError( ct );
                                                }
                                            }
                                            else if ( !var25.equals( ct ) )
                                            {
                                                throw new MatchError( ct );
                                            }

                                            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                            MODULE$.getStatic( "NTAny", scala.reflect.ManifestFactory..MODULE$.classType(
                                                    Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                            MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.AnyType.class ));
                                            return (IntermediateRepresentation) var2;
                                        }

                                        var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                        MODULE$.getStatic( "NTMap", scala.reflect.ManifestFactory..MODULE$.classType(
                                                Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                        MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.MapType.class ));
                                        return (IntermediateRepresentation) var2;
                                    }

                                    var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.getStatic( "NTNode", scala.reflect.ManifestFactory..MODULE$.classType(
                                            Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.NodeType.class ));
                                    return (IntermediateRepresentation) var2;
                                }

                                var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.getStatic( "NTPoint", scala.reflect.ManifestFactory..MODULE$.classType(
                                        Neo4jTypes.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.PointType.class ));
                                return (IntermediateRepresentation) var2;
                            }

                            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.getStatic( "NTDuration", scala.reflect.ManifestFactory..MODULE$.classType(
                                    Neo4jTypes.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.DurationType.class ));
                            return (IntermediateRepresentation) var2;
                        }

                        var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.getStatic( "NTDate", scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.DateType.class ));
                        return (IntermediateRepresentation) var2;
                    }

                    var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.getStatic( "NTDateTime", scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
                    MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.DateTimeType.class ));
                    return (IntermediateRepresentation) var2;
                }

                var2 = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.getStatic( "NTNumber", scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.NumberType.class ));
                return (IntermediateRepresentation) var2;
            }

            var2 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.getStatic( "NTFloat", scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( org.neo4j.internal.kernel.api.procs.Neo4jTypes.FloatType.class ));
            return (IntermediateRepresentation) var2;
        }

        var2 = org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.getStatic( "NTString", scala.reflect.ManifestFactory..MODULE$.classType( Neo4jTypes.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( TextType.class ));
        return (IntermediateRepresentation) var2;
    }

    private IntermediateRepresentation caseExpression( final String returnVariable, final Seq<IntermediateRepresentation> checks,
            final Seq<IntermediateRepresentation> loads, final IntermediateRepresentation default,
            final Function1<IntermediateRepresentation,IntermediateRepresentation> conditionToCheck )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.oneTime(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( returnVariable,
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( (Object) null )),
                    loop$1( ((TraversableOnce) checks.zip( loads, scala.collection.Seq..MODULE$.canBuildFrom()) ).toList(), returnVariable, conditionToCheck ),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.constant( (Object) null )),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( returnVariable, var4 ))})))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( returnVariable )})));
    }

    private Tuple2<IntermediateRepresentation,Option<IntermediateRepresentation>> accessVariable( final String name )
    {
        Tuple2 var2;
        int offset;
        boolean nullable;
        label66:
        {
            boolean var3 = false;
            Some var4 = null;
            Option var5 = this.slots().get( name );
            if ( var5 instanceof Some )
            {
                var3 = true;
                var4 = (Some) var5;
                Slot var6 = (Slot) var4.value();
                if ( var6 instanceof LongSlot )
                {
                    LongSlot var7 = (LongSlot) var6;
                    offset = var7.offset();
                    nullable = var7.nullable();
                    CypherType var10 = var7.typ();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var10 == null )
                        {
                            break label66;
                        }
                    }
                    else if ( var10000.equals( var10 ) )
                    {
                        break label66;
                    }
                }
            }

            int offset;
            boolean nullable;
            label67:
            {
                if ( var3 )
                {
                    Slot var12 = (Slot) var4.value();
                    if ( var12 instanceof LongSlot )
                    {
                        LongSlot var13 = (LongSlot) var12;
                        offset = var13.offset();
                        nullable = var13.nullable();
                        CypherType var16 = var13.typ();
                        RelationshipType var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                        if ( var24 == null )
                        {
                            if ( var16 == null )
                            {
                                break label67;
                            }
                        }
                        else if ( var24.equals( var16 ) )
                        {
                            break label67;
                        }
                    }
                }

                if ( var3 )
                {
                    Slot var18 = (Slot) var4.value();
                    if ( var18 instanceof RefSlot )
                    {
                        RefSlot var19 = (RefSlot) var18;
                        int offset = var19.offset();
                        boolean nullable = var19.nullable();
                        var2 = computeRepresentation$1( this.getRefAt( offset ),
                                new Some( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getRefAt( offset ),
                                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue() )),nullable);
                        return var2;
                    }
                }

                String varName = this.namer().nextVariableName();
                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                varName, org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invoke( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$LOAD_CONTEXT(),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "getByName", scala.reflect.ManifestFactory..MODULE$.classType(
                        ExecutionContext.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( String.class )),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( name )})))))
                ;
                var2 = computeRepresentation$1( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( varName )}))),
                new Some( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( varName ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})))),true);
                return var2;
            }

            var2 = computeRepresentation$1( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getLongAt( offset ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "relationshipById",
                    scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( RelationshipValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset )}) ))),
            new Some( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getLongAt( offset ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ))),nullable);
            return var2;
        }

        var2 = computeRepresentation$1( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getLongAt( offset ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue(), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "nodeById",
                scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{this.getLongAt( offset )}) ))),
        new Some( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getLongAt( offset ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ))),nullable);
        return var2;
    }

    private Option<IntermediateExpression> filterExpression( final Option<IntermediateExpression> collectionExpression, final Expression innerPredicate,
            final ExpressionVariable innerVariable )
    {
        String iterVariable = this.namer().nextVariableName();
        return collectionExpression.flatMap( ( collection ) -> {
            return this.intermediateCompileExpression( innerPredicate ).map( ( inner ) -> {
                String listVar = this.namer().nextVariableName();
                String filteredVars = this.namer().nextVariableName();
                String currentValue = this.namer().nextVariableName();
                String isFiltered = this.namer().nextVariableName();
                Seq ops = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( listVar,
                                scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( listVar,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{collection.ir()}) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.declare( filteredVars, scala.reflect.ManifestFactory..MODULE$.classType( ArrayList.class,
                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.assign( filteredVars, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.newInstance(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType(
                        ArrayList.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.declare( iterVariable, scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.assign( iterVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( listVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType(
                        AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.block( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( currentValue,
                                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( currentValue,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType(
                        AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ))),
                this.setExpressionVariable( innerVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( isFiltered, scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( isFiltered, ExpressionCompiler$.MODULE$.nullCheckIfRequired( inner,
                        ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ) ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( isFiltered ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.trueValue()),org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                        filteredVars ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "add", scala.reflect.ManifestFactory..MODULE$.classType( ArrayList.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                        scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
                MODULE$.Object()),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )}))))}))))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "fromList",
                        scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( java.util.List.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( filteredVars )})))})));
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( ops ),
                (Seq) collection.fields().$plus$plus( inner.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                (Seq) collection.variables().$plus$plus( inner.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                collection.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } );
    }

    private Option<IntermediateExpression> extractExpression( final Option<IntermediateExpression> collectionExpression, final Expression extractExpression,
            final ExpressionVariable innerVariable )
    {
        String iterVariable = this.namer().nextVariableName();
        return collectionExpression.flatMap( ( collection ) -> {
            return this.intermediateCompileExpression( extractExpression ).map( ( inner ) -> {
                String listVar = this.namer().nextVariableName();
                String extractedVars = this.namer().nextVariableName();
                String currentValue = this.namer().nextVariableName();
                Seq ops = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( listVar,
                                scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( listVar,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "asList", scala.reflect.ManifestFactory..MODULE$.classType(
                        CypherFunctions.class ), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{collection.ir()}) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.declare( extractedVars, scala.reflect.ManifestFactory..MODULE$.classType( ArrayList.class,
                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.assign( extractedVars, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.newInstance(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType(
                        ArrayList.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.declare( iterVariable, scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                        scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.assign( iterVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( listVar ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "iterator", scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.loop( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                        iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "hasNext", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType(
                        AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.block( (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( currentValue,
                                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( currentValue,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( iterVariable ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "next", scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType(
                        AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ))),
                this.setExpressionVariable( innerVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( currentValue )),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                        extractedVars ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.method( "add", scala.reflect.ManifestFactory..MODULE$.classType( ArrayList.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                        scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean(), scala.reflect.ManifestFactory..
                MODULE$.Object()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( inner, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) ))}))))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "fromList",
                        scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( java.util.List.class, scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( extractedVars )})))})));
                return new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( ops ),
                (Seq) collection.fields().$plus$plus( inner.fields(), scala.collection.Seq..MODULE$.canBuildFrom()),
                (Seq) collection.variables().$plus$plus( inner.variables(), scala.collection.Seq..MODULE$.canBuildFrom()),
                collection.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
            } );
        } );
    }

    private Option<IntermediateExpression> compileStaticPath( final PathStep steps )
    {
        return this.compileSteps$1( steps, compileSteps$default$2$1(), compileSteps$default$3$1() ).withFilter( ( check$ifrefutable$5 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$compileStaticPath$1( check$ifrefutable$5 ) );
        } ).map( ( x$61 ) -> {
            if ( x$61 == null )
            {
                throw new MatchError( x$61 );
            }
            else
            {
                Seq nodeOps = (Seq) x$61._1();
                Seq relOps = (Seq) x$61._2();
                String variableName = this.namer().nextVariableName();
                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                variableName, ExpressionCompiler$.MODULE$.noValueOr(
                        (Seq) nodeOps.$plus$plus( relOps, scala.collection.Seq..MODULE$.canBuildFrom() ), org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "path",
                        scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( PathValue.class ), scala.reflect.ManifestFactory..
                MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),scala.reflect.ManifestFactory..
                MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ))),scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf(
                            (Seq) nodeOps.map( ( n ) -> {
                                return org.neo4j.codegen.api.IntermediateRepresentation..
                                MODULE$.cast( n.ir(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ));
                            }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.arrayOf( (Seq) relOps.map( ( r ) -> {
                return org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.cast( r.ir(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ));
            }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ))}))))));
                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})));
                Set nullChecks = nodeOps.forall( ( x$55 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$compileStaticPath$5( x$55 ) );
                } ) && relOps.forall( ( x$56 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$compileStaticPath$6( x$56 ) );
                } ) ? scala.Predef..MODULE$.Set().empty() :(Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                            scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})))})));
                IntermediateExpression var2 = new IntermediateExpression( ops, (Seq) ((TraversableLike) nodeOps.flatMap( ( x$57 ) -> {
                    return x$57.fields();
                }, scala.collection.Seq..MODULE$.canBuildFrom()) ).$plus$plus( (GenTraversableOnce) relOps.flatMap( ( x$58 ) -> {
                    return x$58.fields();
            },scala.collection.Seq..MODULE$.canBuildFrom()),scala.collection.Seq..MODULE$.canBuildFrom()),
                (Seq) ((TraversableLike) nodeOps.flatMap( ( x$59 ) -> {
                    return x$59.variables();
                }, scala.collection.Seq..MODULE$.canBuildFrom())).$plus$plus( (GenTraversableOnce) relOps.flatMap( ( x$60 ) -> {
                return x$60.variables();
            }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom()),nullChecks, false);
                return var2;
            }
        } );
    }

    private Option<IntermediateExpression> compileDynamicPath( final PathStep steps )
    {
        String builderVar = this.namer().nextVariableName();
        return this.compileSteps$2( steps, compileSteps$default$2$2(), builderVar ).map( ( pathOps ) -> {
            String variableName = this.namer().nextVariableName();
            IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                    (Seq) ((SeqLike) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new Product[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declare( builderVar,
                                    scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class )),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( builderVar,
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.newInstance(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constructor( scala.reflect.ManifestFactory..MODULE$.classType(
                            PathValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( RelationshipScanCursor.class )),scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{ExpressionCompiler$.MODULE$.DB_ACCESS(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR()}) )))})))).
            $plus$plus( (GenTraversableOnce) pathOps.map( ( x$62 ) -> {
                return x$62.ir();
            }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom())).
            $colon$plus( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
            variableName, org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( builderVar ), org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.method( "build", scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ))),scala.collection.Seq..
            MODULE$.canBuildFrom())));
            IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})));
            Set nullChecks = pathOps.forall( ( x$63 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$compileDynamicPath$4( x$63 ) );
            } ) ? scala.Predef..MODULE$.Set().empty() :(Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block(
                            scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})))})));
            return new IntermediateExpression( ops, (Seq) pathOps.flatMap( ( x$64 ) -> {
                return x$64.fields();
            }, scala.collection.Seq..MODULE$.canBuildFrom() ),(Seq) ((SeqLike) pathOps.flatMap( ( x$65 ) -> {
                return x$65.variables();
            }, scala.collection.Seq..MODULE$.canBuildFrom())).
            $colon$plus( ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(), scala.collection.Seq..MODULE$.canBuildFrom()),nullChecks, false);
        } );
    }

    private HashSet<AnyValue> setFromLiterals( final Seq<Literal> literals )
    {
        HashSet set = new HashSet();
        literals.foreach( ( l ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$setFromLiterals$1( set, l ) );
        } );
        return set;
    }

    private Option<IntermediateExpression> containerIndexAccess( final Expression container, final Expression index, final boolean exists )
    {
        return this.intermediateCompileExpression( container ).flatMap( ( c ) -> {
            return this.intermediateCompileExpression( index ).map( ( idx ) -> {
                String variableName = this.namer().nextVariableName();
                IntermediateRepresentation invocation = exists ? org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "containerIndexExists",
                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                MODULE$.Boolean(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{c.ir(), idx.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                        ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                        ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}) )),org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue()) :org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.invokeStatic( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "containerIndex",
                        scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( RelationshipScanCursor.class ), scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class )),scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{c.ir(), idx.ir(), ExpressionCompiler$.MODULE$.DB_ACCESS(),
                        ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(),
                        ExpressionCompiler$.MODULE$.PROPERTY_CURSOR()}) ));
                IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                variableName, ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateExpression[]{c, idx}) ), invocation)));
                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName )})))
                ;
                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( variableName ),
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                return new IntermediateExpression( ops, (Seq) c.fields().$plus$plus( idx.fields(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                (Seq) ((TraversableLike) c.variables().$plus$plus( idx.variables(), scala.collection.Seq..MODULE$.canBuildFrom())).
                $plus$plus( ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$vCURSORS(),
                        scala.collection.Seq..MODULE$.canBuildFrom()),(Set) scala.Predef..
                MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false);
            } );
        } );
    }

    private Option<IntermediateExpression> cachedExists( final ASTCachedProperty property )
    {
        Object var2;
        if ( property instanceof SlottedCachedPropertyWithPropertyToken )
        {
            SlottedCachedPropertyWithPropertyToken var6 = (SlottedCachedPropertyWithPropertyToken) property;
            int entityOffset = var6.offset();
            boolean offsetIsForLongSlot = var6.offsetIsForLongSlot();
            int prop = var6.propToken();
            EntityType entityType = var6.entityType();
            boolean nullable = var6.nullable();
            Some var10000;
            if ( prop == -1 )
            {
                var10000 = new Some( new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue(),
                        (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty(), scala.Predef..MODULE$.Set().empty(), false));
            }
            else
            {
                String existsVariable = this.namer().nextVariableName();
                String propertyVariable = this.namer().nextVariableName();
                IntermediateRepresentation entityId = this.getEntityId( offsetIsForLongSlot, entityOffset, entityType, nullable );
                Tuple4 var16 = this.callPropertyExists( entityType );
                if ( var16 == null )
                {
                    throw new MatchError( var16 );
                }

                Method propertyGet = (Method) var16._1();
                Method txStateHasCachedProperty = (Method) var16._2();
                IntermediateRepresentation cursor = (IntermediateRepresentation) var16._3();
                LocalVariable cursorVar = (LocalVariable) var16._4();
                Tuple4 var4 = new Tuple4( propertyGet, txStateHasCachedProperty, cursor, cursorVar );
                Method propertyGet = (Method) var4._1();
                Method txStateHasCachedProperty = (Method) var4._2();
                IntermediateRepresentation cursor = (IntermediateRepresentation) var4._3();
                LocalVariable cursorVar = (LocalVariable) var4._4();
                IntermediateRepresentation getAndCacheProperty = org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                Value.class )), existsVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual( entityId,
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
                this.checkPropertyTxState$3( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                                Value.class )), propertyVariable,
                        this.getCachedPropertyAt( var6, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke(
                                ExpressionCompiler$.MODULE$.DB_ACCESS(), propertyGet, scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new IntermediateRepresentation[]{entityId, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                        BoxesRunTime.boxToInteger( prop ) ), cursor, ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToBoolean( true ) )}))))),
                org.neo4j.codegen.api.IntermediateRepresentation..
                MODULE$.assign( existsVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                        propertyVariable ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()))}))),
                prop, existsVariable, entityId, txStateHasCachedProperty))}))));
                IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{getAndCacheProperty, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                            existsVariable )})));
                IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{getAndCacheProperty, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( existsVariable ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
                var10000 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.apply(
                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{cursorVar, ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),
                (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false))
                ;
            }

            var2 = var10000;
        }
        else if ( property instanceof SlottedCachedPropertyWithoutPropertyToken )
        {
            SlottedCachedPropertyWithoutPropertyToken var28 = (SlottedCachedPropertyWithoutPropertyToken) property;
            int entityOffset = var28.offset();
            boolean offsetIsForLongSlot = var28.offsetIsForLongSlot();
            String prop = var28.propKey();
            EntityType entityType = var28.entityType();
            boolean nullable = var28.nullable();
            InstanceField f = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.field( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                    BoxesRunTime.boxToInteger( -1 ) ), scala.reflect.ManifestFactory..MODULE$.Int());
            String existsVariable = this.namer().nextVariableName();
            String propertyVariable = this.namer().nextVariableName();
            IntermediateRepresentation entityId = this.getEntityId( offsetIsForLongSlot, entityOffset, entityType, nullable );
            Tuple4 var39 = this.callPropertyExists( entityType );
            if ( var39 == null )
            {
                throw new MatchError( var39 );
            }

            Method propertyGet = (Method) var39._1();
            Method txStateHasCachedProperty = (Method) var39._2();
            IntermediateRepresentation cursor = (IntermediateRepresentation) var39._3();
            LocalVariable cursorVar = (LocalVariable) var39._4();
            Tuple4 var3 = new Tuple4( propertyGet, txStateHasCachedProperty, cursor, cursorVar );
            Method propertyGet = (Method) var3._1();
            Method txStateHasCachedProperty = (Method) var3._2();
            IntermediateRepresentation cursor = (IntermediateRepresentation) var3._3();
            LocalVariable cursorVar = (LocalVariable) var3._4();
            IntermediateRepresentation getAndCacheProperty = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            Value.class )), existsVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),
            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.notEqual( entityId,
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToLong( -1L ) )),
            this.checkPropertyTxState$4( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType(
                            Value.class )), propertyVariable,
                    this.getCachedPropertyAt( var28, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                            propertyGet, scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{entityId, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                            cursor, ExpressionCompiler$.MODULE$.PROPERTY_CURSOR(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                            BoxesRunTime.boxToBoolean( true ) )}))))),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.assign( existsVariable, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                    propertyVariable ), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.falseValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue()))}))),
            f, existsVariable, entityId, txStateHasCachedProperty))}))));
            IntermediateRepresentation lazySet = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.condition(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( BoxesRunTime.boxToInteger( -1 ) )),
            org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.setField( f, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "propertyKey", scala.reflect.ManifestFactory..MODULE$.classType(
                    DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.Int(), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),
            scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant( prop )}))))),
            getAndCacheProperty}))));
            IntermediateRepresentation ops = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( existsVariable )})));
            IntermediateRepresentation nullChecks = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{lazySet, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( existsVariable ),
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue())})));
            var2 = new Some( new IntermediateExpression( ops, (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new InstanceField[]{f}) ) ), (Seq) scala.collection.Seq..
            MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new LocalVariable[]{cursorVar, ExpressionCompiler$.MODULE$.vPROPERTY_CURSOR()}) )),
            (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{nullChecks}) )),false));
        }
        else
        {
            var2 = .MODULE$;
        }

        return (Option) var2;
    }

    private IntermediateRepresentation setExpressionVariable( final ExpressionVariable ev, final IntermediateRepresentation value )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.arraySet( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( "expressionVariables" ), ev.offset(), value);
    }

    private IntermediateRepresentation loadExpressionVariable( final ExpressionVariable ev )
    {
        return org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.arrayLoad( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( "expressionVariables" ), ev.offset());
    }

    private IntermediateRepresentation getEntityId( final boolean offsetIsForLongSlot, final int offset, final EntityType entityType, final boolean nullable )
    {
        IntermediateRepresentation var10000;
        if ( offsetIsForLongSlot )
        {
            var10000 = this.getLongAt( offset );
        }
        else
        {
            String entityVar = this.namer().nextVariableName();
            IntermediateRepresentation var5;
            if ( org.neo4j.cypher.internal.v4_0.expressions.NODE_TYPE..MODULE$.equals( entityType )){
            var5 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( this.getRefAt( offset ),
                    scala.reflect.ManifestFactory..MODULE$.classType( VirtualNodeValue.class )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.method( "id", scala.reflect.ManifestFactory..MODULE$.classType( VirtualNodeValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
            scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        } else{
            if ( !org.neo4j.cypher.internal.v4_0.expressions.RELATIONSHIP_TYPE..MODULE$.equals( entityType )){
                throw new MatchError( entityType );
            }

            var5 = org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( this.getRefAt( offset ),
                    scala.reflect.ManifestFactory..MODULE$.classType( VirtualRelationshipValue.class )),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.method( "id", scala.reflect.ManifestFactory..MODULE$.classType( VirtualRelationshipValue.class ), scala.reflect.ManifestFactory..
            MODULE$.Long()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) ));
        }

            IntermediateRepresentation nullChecked = nullable ? org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.ternary( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.equal( this.getRefAt( offset ),
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.noValue()),org.neo4j.codegen.api.IntermediateRepresentation..
            MODULE$.constant( BoxesRunTime.boxToLong( -1L ) ), var5) :var5;
            var10000 = org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.oneTime(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.Long()), entityVar,
                nullChecked )),org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( entityVar )})));
        }

        return var10000;
    }

    private Tuple4<Method,Method,IntermediateRepresentation,LocalVariable> callPropertyGet( final EntityType entityType )
    {
        Tuple4 var2;
        if ( org.neo4j.cypher.internal.v4_0.expressions.NODE_TYPE..MODULE$.equals( entityType )){
        var2 = new Tuple4( ExpressionCompiler$.MODULE$.NODE_PROPERTY(),
                ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_NODE_PROP(),
                ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.vNODE_CURSOR() );
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.RELATIONSHIP_TYPE..MODULE$.equals( entityType )){
            throw new MatchError( entityType );
        }

        var2 = new Tuple4( ExpressionCompiler$.MODULE$.RELATIONSHIP_PROPERTY(),
                ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$GET_TX_STATE_RELATIONSHIP_PROP(),
                ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR() );
    }

        return var2;
    }

    private Tuple4<Method,Method,IntermediateRepresentation,LocalVariable> callPropertyExists( final EntityType entityType )
    {
        Tuple4 var2;
        if ( org.neo4j.cypher.internal.v4_0.expressions.NODE_TYPE..MODULE$.equals( entityType )){
        var2 = new Tuple4( ExpressionCompiler$.MODULE$.NODE_PROPERTY(),
                ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_NODE_PROP(),
                ExpressionCompiler$.MODULE$.NODE_CURSOR(), ExpressionCompiler$.MODULE$.vNODE_CURSOR() );
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.RELATIONSHIP_TYPE..MODULE$.equals( entityType )){
            throw new MatchError( entityType );
        }

        var2 = new Tuple4( ExpressionCompiler$.MODULE$.RELATIONSHIP_PROPERTY(),
                ExpressionCompiler$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$expressions$ExpressionCompiler$$HAS_TX_STATE_RELATIONSHIP_PROP(),
                ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR(), ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR() );
    }

        return var2;
    }

    private final IntermediateRepresentation checkPropertyTxState$1( final IntermediateRepresentation continuation, final int token$1,
            final String variableName$1, final IntermediateRepresentation entityId$1, final Method txStatePropertyGet$1 )
    {
        return this.readOnly() ? continuation : org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( variableName$1,
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), txStatePropertyGet$1,
                    scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{entityId$1, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                            BoxesRunTime.boxToInteger( token$1 ) )})))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                variableName$1 )),continuation)})));
    }

    private final IntermediateRepresentation checkPropertyTxState$2( final IntermediateRepresentation continuation, final InstanceField f$1,
            final String variableName$2, final IntermediateRepresentation entityId$2, final Method txStatePropertyGet$2 )
    {
        return this.readOnly() ? continuation : org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( variableName$2,
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), txStatePropertyGet$2,
                    scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new IntermediateRepresentation[]{entityId$2, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f$1 )})))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.condition( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.isNull( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                variableName$2 )),continuation)})));
    }

    private final boolean isStaticallyKnown$1( final PathStep step )
    {
        while ( true )
        {
            if ( step instanceof NodePathStep )
            {
                NodePathStep var5 = (NodePathStep) step;
                PathStep next = var5.next();
                step = next;
            }
            else
            {
                if ( !(step instanceof SingleRelationshipPathStep) )
                {
                    boolean var3;
                    if ( step instanceof MultiRelationshipPathStep )
                    {
                        var3 = false;
                    }
                    else
                    {
                        if ( !org.neo4j.cypher.internal.v4_0.expressions.NilPathStep..MODULE$.equals( step )){
                        throw new MatchError( step );
                    }

                        var3 = true;
                    }

                    return var3;
                }

                SingleRelationshipPathStep var7 = (SingleRelationshipPathStep) step;
                PathStep next = var7.next();
                step = next;
            }
        }
    }

    private final Option compileSteps$1( final PathStep step, final Seq nodeOps, final Seq relOps )
    {
        while ( true )
        {
            boolean var9 = false;
            SingleRelationshipPathStep var10 = null;
            Seq var10001;
            if ( step instanceof NodePathStep )
            {
                NodePathStep var12 = (NodePathStep) step;
                LogicalVariable node = var12.node();
                PathStep next = var12.next();
                var10001 = (Seq) nodeOps.$plus$plus( scala.Option..MODULE$.option2Iterable( this.intermediateCompileExpression( node ) ), scala.collection.Seq..
                MODULE$.canBuildFrom());
                relOps = relOps;
                nodeOps = var10001;
                step = next;
            }
            else
            {
                Object var5;
                if ( step instanceof SingleRelationshipPathStep )
                {
                    var9 = true;
                    var10 = (SingleRelationshipPathStep) step;
                    LogicalVariable relExpression = var10.rel();
                    Option var16 = var10.toNode();
                    PathStep next = var10.next();
                    if ( var16 instanceof Some )
                    {
                        Some var18 = (Some) var16;
                        LogicalVariable targetExpression = (LogicalVariable) var18.value();
                        Tuple2 var20 =
                                new Tuple2( this.intermediateCompileExpression( relExpression ), this.intermediateCompileExpression( targetExpression ) );
                        if ( var20 != null )
                        {
                            Option var21 = (Option) var20._1();
                            Option var22 = (Option) var20._2();
                            if ( var21 instanceof Some )
                            {
                                Some var23 = (Some) var21;
                                IntermediateExpression rel = (IntermediateExpression) var23.value();
                                if ( var22 instanceof Some )
                                {
                                    Some var25 = (Some) var22;
                                    IntermediateExpression target = (IntermediateExpression) var25.value();
                                    var10001 = (Seq) nodeOps.$colon$plus( target, scala.collection.Seq..MODULE$.canBuildFrom());
                                    relOps = (Seq) relOps.$colon$plus( rel, scala.collection.Seq..MODULE$.canBuildFrom());
                                    nodeOps = var10001;
                                    step = next;
                                    continue;
                                }
                            }
                        }

                        var8 = .MODULE$;
                        var5 = var8;
                        return (Option) var5;
                    }
                }

                if ( var9 )
                {
                    LogicalVariable rel = var10.rel();
                    SemanticDirection var28 = var10.direction();
                    PathStep next = var10.next();
                    if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( var28 )){
                    Option var30 = this.intermediateCompileExpression( rel );
                    if ( var30 instanceof Some )
                    {
                        Some var31 = (Some) var30;
                        IntermediateExpression compiled = (IntermediateExpression) var31.value();
                        LocalVariable relVar = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.variable( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                (Object) null ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ));
                        IntermediateRepresentation lazyRel = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( relVar.name(),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( compiled.ir(), scala.reflect.ManifestFactory..MODULE$.classType(
                                RelationshipValue.class ))));
                        IntermediateExpression node =
                                new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazyRel,
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "otherNode",
                                                scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),
                                        scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( VirtualRelationshipValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( VirtualNodeValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class )),
                        scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( relVar.name() ),
                            scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class )),
                        ExpressionCompiler$.MODULE$.DB_ACCESS(), org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.cast( ((IntermediateExpression) nodeOps.last()).ir(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),
                        ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR()})))}))),
                        compiled.fields(), (Seq) ((SeqLike) compiled.variables().$colon$plus( relVar, scala.collection.Seq..MODULE$.canBuildFrom())).
                        $colon$plus( ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(), scala.collection.Seq..MODULE$.canBuildFrom()),
                        (Set) compiled.nullChecks().$plus$plus(
                                ((IntermediateExpression) nodeOps.last()).nullChecks() ), IntermediateExpression$.MODULE$.apply$default$5());
                        IntermediateExpression rel =
                                new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazyRel, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                relVar.name() )}))),
                        compiled.fields(), (Seq) compiled.variables().$colon$plus( relVar, scala.collection.Seq..MODULE$.canBuildFrom()),
                        compiled.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                        var10001 = (Seq) nodeOps.$colon$plus( node, scala.collection.Seq..MODULE$.canBuildFrom());
                        relOps = (Seq) relOps.$colon$plus( rel, scala.collection.Seq..MODULE$.canBuildFrom());
                        nodeOps = var10001;
                        step = next;
                        continue;
                    }

                    if ( !.MODULE$.equals( var30 )){
                        throw new MatchError( var30 );
                    }

                    var7 = .MODULE$;
                    var5 = var7;
                    return (Option) var5;
                }
                }

                if ( var9 )
                {
                    LogicalVariable rel = var10.rel();
                    SemanticDirection direction = var10.direction();
                    PathStep next = var10.next();
                    Option var40 = this.intermediateCompileExpression( rel );
                    if ( var40 instanceof Some )
                    {
                        String var10000;
                        IntermediateExpression compiled;
                        label77:
                        {
                            label76:
                            {
                                Some var41 = (Some) var40;
                                compiled = (IntermediateExpression) var41.value();
                                org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING.var44 =
                                        org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$;
                                if ( direction == null )
                                {
                                    if ( var44 == null )
                                    {
                                        break label76;
                                    }
                                }
                                else if ( direction.equals( var44 ) )
                                {
                                    break label76;
                                }

                                var10000 = "endNode";
                                break label77;
                            }

                            var10000 = "startNode";
                        }

                        String methodName = var10000;
                        LocalVariable relVar = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.variable( this.namer().nextVariableName(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                                (Object) null ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ));
                        IntermediateRepresentation lazyRel = org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.oneTime( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.assign( relVar.name(),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( compiled.ir(), scala.reflect.ManifestFactory..MODULE$.classType(
                                RelationshipValue.class ))));
                        IntermediateExpression node =
                                new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazyRel,
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeStatic(
                                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( methodName,
                                                scala.reflect.ManifestFactory..MODULE$.classType( CypherFunctions.class ),
                                        scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( VirtualRelationshipValue.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( DbAccess.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class )),scala.Predef..
                        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( relVar.name() ),
                                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class )),
                        ExpressionCompiler$.MODULE$.DB_ACCESS(), ExpressionCompiler$.MODULE$.RELATIONSHIP_CURSOR()})))}))),
                        compiled.fields(), (Seq) ((SeqLike) compiled.variables().$colon$plus( relVar, scala.collection.Seq..MODULE$.canBuildFrom())).
                        $colon$plus( ExpressionCompiler$.MODULE$.vRELATIONSHIP_CURSOR(), scala.collection.Seq..MODULE$.canBuildFrom()),
                        compiled.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                        IntermediateExpression rel =
                                new IntermediateExpression( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{lazyRel, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                                relVar.name() )}))),
                        compiled.fields(), (Seq) compiled.variables().$colon$plus( relVar, scala.collection.Seq..MODULE$.canBuildFrom()),
                        compiled.nullChecks(), IntermediateExpression$.MODULE$.apply$default$5());
                        var10001 = (Seq) nodeOps.$colon$plus( node, scala.collection.Seq..MODULE$.canBuildFrom());
                        relOps = (Seq) relOps.$colon$plus( rel, scala.collection.Seq..MODULE$.canBuildFrom());
                        nodeOps = var10001;
                        step = next;
                        continue;
                    }

                    if ( !.MODULE$.equals( var40 )){
                    throw new MatchError( var40 );
                }

                    var6 = .MODULE$;
                    var5 = var6;
                }
                else
                {
                    if ( step instanceof MultiRelationshipPathStep )
                    {
                        throw new IllegalStateException( "Cannot be used for static paths" );
                    }

                    if ( !org.neo4j.cypher.internal.v4_0.expressions.NilPathStep..MODULE$.equals( step )){
                    throw new MatchError( step );
                }

                    var5 = new Some( new Tuple2( nodeOps, relOps ) );
                }

                return (Option) var5;
            }
        }
    }

    private final Option compileSteps$2( final PathStep step, final Seq acc, final String builderVar$2 )
    {
        while ( true )
        {
            boolean var13 = false;
            SingleRelationshipPathStep var14 = null;
            Object var5;
            if ( step instanceof NodePathStep )
            {
                NodePathStep var16 = (NodePathStep) step;
                LogicalVariable node = var16.node();
                PathStep next = var16.next();
                Option var19 = this.intermediateCompileExpression( node );
                if ( var19 instanceof Some )
                {
                    Some var20 = (Some) var19;
                    IntermediateExpression nodeOps = (IntermediateExpression) var20.value();
                    IntermediateRepresentation addNode = nodeOps.nullChecks().isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                            builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.method( "addNode", scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                    MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( nodeOps.ir(),
                                scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ))}))) :org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                            builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                    MODULE$.method( "addNode", scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                    MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new IntermediateRepresentation[]{
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired( nodeOps, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) ));
                    acc = (Seq) acc.$colon$plus(
                            nodeOps.copy( addNode, nodeOps.copy$default$2(), nodeOps.copy$default$3(), nodeOps.copy$default$4(), nodeOps.copy$default$5() ),
                            scala.collection.Seq..MODULE$.canBuildFrom());
                    step = next;
                    continue;
                }

                if ( !.MODULE$.equals( var19 )){
                throw new MatchError( var19 );
            }

                var12 = .MODULE$;
                var5 = var12;
            }
            else
            {
                if ( step instanceof SingleRelationshipPathStep )
                {
                    var13 = true;
                    var14 = (SingleRelationshipPathStep) step;
                    LogicalVariable rel = var14.rel();
                    Option var24 = var14.toNode();
                    PathStep next = var14.next();
                    if ( var24 instanceof Some )
                    {
                        Some var26 = (Some) var24;
                        LogicalVariable targetExpression = (LogicalVariable) var26.value();
                        Tuple2 var28 = new Tuple2( this.intermediateCompileExpression( rel ), this.intermediateCompileExpression( targetExpression ) );
                        if ( var28 != null )
                        {
                            Option var29 = (Option) var28._1();
                            Option var30 = (Option) var28._2();
                            if ( var29 instanceof Some )
                            {
                                Some var31 = (Some) var29;
                                IntermediateExpression relOps = (IntermediateExpression) var31.value();
                                if ( var30 instanceof Some )
                                {
                                    Some var33 = (Some) var30;
                                    IntermediateExpression target = (IntermediateExpression) var33.value();
                                    IntermediateRepresentation addNodeAndRelationship = org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.block( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                                    target.nullChecks().isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invokeSideEffect(
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load( builderVar$2 ),
                                            org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.method( "addNode",
                                            scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                                    MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                                target.ir(), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ))}))) :
                                    org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                            builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.method( "addNode", scala.reflect.ManifestFactory..MODULE$.classType(
                                            PathValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
                                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( target,
                                                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) )),
                                    relOps.nullChecks().isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                            builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.method( "addRelationship", scala.reflect.ManifestFactory..MODULE$.classType(
                                            PathValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
                                    MODULE$.classType( RelationshipValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                                                relOps.ir(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ))}))) :
                                    org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                            builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                                    MODULE$.method( "addRelationship", scala.reflect.ManifestFactory..MODULE$.classType(
                                            PathValueBuilder.class ), scala.reflect.ManifestFactory..MODULE$.Unit(), scala.reflect.ManifestFactory..
                                    MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired( relOps,
                                                ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) ))})));
                                    acc = (Seq) acc.$colon$plus(
                                            relOps.copy( addNodeAndRelationship, relOps.copy$default$2(), relOps.copy$default$3(), relOps.copy$default$4(),
                                                    relOps.copy$default$5() ), scala.collection.Seq..MODULE$.canBuildFrom());
                                    step = next;
                                    continue;
                                }
                            }
                        }

                        var11 = .MODULE$;
                        var5 = var11;
                        return (Option) var5;
                    }
                }

                if ( var13 )
                {
                    LogicalVariable rel = var14.rel();
                    SemanticDirection direction = var14.direction();
                    PathStep next = var14.next();
                    Option var39 = this.intermediateCompileExpression( rel );
                    if ( var39 instanceof Some )
                    {
                        Some var40 = (Some) var39;
                        IntermediateExpression relOps = (IntermediateExpression) var40.value();
                        String var10;
                        if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( direction )){
                        var10 = "addIncoming";
                    } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( direction )){
                        var10 = "addOutgoing";
                    } else{
                        var10 = "addUndirected";
                    }

                        IntermediateRepresentation addRel = relOps.nullChecks().isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.method( var10, scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class )),scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( relOps.ir(),
                                    scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ))}))) :
                        org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                        MODULE$.method( var10, scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                        MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new IntermediateRepresentation[]{
                                    ExpressionCompiler$.MODULE$.nullCheckIfRequired( relOps, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) ));
                        acc = (Seq) acc.$colon$plus(
                                relOps.copy( addRel, relOps.copy$default$2(), relOps.copy$default$3(), relOps.copy$default$4(), relOps.copy$default$5() ),
                                scala.collection.Seq..MODULE$.canBuildFrom());
                        step = next;
                        continue;
                    }

                    if ( !.MODULE$.equals( var39 )){
                    throw new MatchError( var39 );
                }

                    var9 = .MODULE$;
                    var5 = var9;
                }
                else if ( step instanceof MultiRelationshipPathStep )
                {
                    MultiRelationshipPathStep var45 = (MultiRelationshipPathStep) step;
                    LogicalVariable rel = var45.rel();
                    SemanticDirection direction = var45.direction();
                    Option maybeTarget = var45.toNode();
                    PathStep next = var45.next();
                    Option var50 = this.intermediateCompileExpression( rel );
                    if ( var50 instanceof Some )
                    {
                        Some var51 = (Some) var50;
                        IntermediateExpression relOps = (IntermediateExpression) var51.value();
                        String var8;
                        if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( direction )){
                        var8 = "addMultipleIncoming";
                    } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( direction )){
                        var8 = "addMultipleOutgoing";
                    } else{
                        var8 = "addMultipleUndirected";
                    }

                        Option var56 = maybeTarget.flatMap( ( t ) -> {
                            return this.intermediateCompileExpression( t );
                        } );
                        IntermediateRepresentation var7;
                        if ( var56 instanceof Some )
                        {
                            Some var57 = (Some) var56;
                            IntermediateExpression target = (IntermediateExpression) var57.value();
                            var7 = relOps.nullChecks().isEmpty() && target.nullChecks().isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                    builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.method( var8, scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                            MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( NodeValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( relOps.ir(),
                                        scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),
                                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( target.ir(), scala.reflect.ManifestFactory..MODULE$.classType(
                                NodeValue.class ))}))) :org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                    builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.method( var8, scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                            MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[]{
                                ExpressionCompiler$.MODULE$.nullCheckIfRequired( relOps, ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() ),
                                ExpressionCompiler$.MODULE$.noValueOr( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new IntermediateExpression[]{target}) ),
                                target.ir())})));
                        }
                        else
                        {
                            if ( !.MODULE$.equals( var56 )){
                            throw new MatchError( var56 );
                        }

                            var7 = relOps.nullChecks().isEmpty() ? org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                    builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.method( var8, scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                            MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast( relOps.ir(),
                                        scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ))}))) :
                            org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.invokeSideEffect( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                                    builderVar$2 ), org.neo4j.codegen.api.IntermediateRepresentation..
                            MODULE$.method( var8, scala.reflect.ManifestFactory..MODULE$.classType( PathValueBuilder.class ), scala.reflect.ManifestFactory..
                            MODULE$.Unit(), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new IntermediateRepresentation[]{ExpressionCompiler$.MODULE$.nullCheckIfRequired( relOps,
                                        ExpressionCompiler$.MODULE$.nullCheckIfRequired$default$2() )}) ));
                        }

                        acc = (Seq) acc.$colon$plus(
                                relOps.copy( var7, relOps.copy$default$2(), relOps.copy$default$3(), relOps.copy$default$4(), relOps.copy$default$5() ),
                                scala.collection.Seq..MODULE$.canBuildFrom());
                        step = next;
                        continue;
                    }

                    if ( !.MODULE$.equals( var50 )){
                    throw new MatchError( var50 );
                }

                    var6 = .MODULE$;
                    var5 = var6;
                }
                else
                {
                    if ( !org.neo4j.cypher.internal.v4_0.expressions.NilPathStep..MODULE$.equals( step )){
                    throw new MatchError( step );
                }

                    var5 = new Some( acc );
                }
            }

            return (Option) var5;
        }
    }

    private final IntermediateRepresentation checkPropertyTxState$3( final IntermediateRepresentation onNoChanges, final int prop$1,
            final String existsVariable$1, final IntermediateRepresentation entityId$3, final Method txStateHasCachedProperty$1 )
    {
        String hasChanges = this.namer().nextVariableName();
        return this.readOnly() ? onNoChanges : org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( Optional.class,
                    scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
        hasChanges, org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), txStateHasCachedProperty$1, scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{entityId$3, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.constant(
                        BoxesRunTime.boxToInteger( prop$1 ) )})))),org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.ifElse( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                hasChanges ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "isEmpty", scala.reflect.ManifestFactory..MODULE$.classType( Optional.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),onNoChanges, org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( existsVariable$1, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.unbox( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                hasChanges ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "get", scala.reflect.ManifestFactory..MODULE$.classType( Optional.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue())))})));
    }

    private final IntermediateRepresentation checkPropertyTxState$4( final IntermediateRepresentation onNoChanges, final InstanceField f$2,
            final String existsVariable$2, final IntermediateRepresentation entityId$4, final Method txStateHasCachedProperty$2 )
    {
        String hasChanges = this.namer().nextVariableName();
        return this.readOnly() ? onNoChanges : org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.block( scala.Predef..MODULE$.wrapRefArray(
            (Object[]) (new IntermediateRepresentation[]{org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.declareAndAssign(
                    org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.typeRefOf( scala.reflect.ManifestFactory..MODULE$.classType( Optional.class,
                    scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
        hasChanges, org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.invoke( ExpressionCompiler$.MODULE$.DB_ACCESS(), txStateHasCachedProperty$2, scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new IntermediateRepresentation[]{entityId$4, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.loadField( f$2 )})))),
        org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.ifElse( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                hasChanges ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "isEmpty", scala.reflect.ManifestFactory..MODULE$.classType( Optional.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),onNoChanges, org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.assign( existsVariable$2, org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.ternary(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.unbox( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.cast(
                org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.invoke( org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.load(
                hasChanges ), org.neo4j.codegen.api.IntermediateRepresentation..
        MODULE$.method( "get", scala.reflect.ManifestFactory..MODULE$.classType( Optional.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new IntermediateRepresentation[0]) )),scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ))),
        org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.trueValue(), org.neo4j.codegen.api.IntermediateRepresentation..MODULE$.falseValue())))})));
    }
}
