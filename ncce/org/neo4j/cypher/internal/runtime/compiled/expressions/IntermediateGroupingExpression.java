package org.neo4j.cypher.internal.runtime.compiled.expressions;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class IntermediateGroupingExpression implements Product, Serializable
{
    private final IntermediateExpression projectKey;
    private final IntermediateExpression computeKey;
    private final IntermediateExpression getKey;

    public IntermediateGroupingExpression( final IntermediateExpression projectKey, final IntermediateExpression computeKey,
            final IntermediateExpression getKey )
    {
        this.projectKey = projectKey;
        this.computeKey = computeKey;
        this.getKey = getKey;
        Product.$init$( this );
    }

    public static Option<Tuple3<IntermediateExpression,IntermediateExpression,IntermediateExpression>> unapply( final IntermediateGroupingExpression x$0 )
    {
        return IntermediateGroupingExpression$.MODULE$.unapply( var0 );
    }

    public static IntermediateGroupingExpression apply( final IntermediateExpression projectKey, final IntermediateExpression computeKey,
            final IntermediateExpression getKey )
    {
        return IntermediateGroupingExpression$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<IntermediateExpression,IntermediateExpression,IntermediateExpression>,IntermediateGroupingExpression> tupled()
    {
        return IntermediateGroupingExpression$.MODULE$.tupled();
    }

    public static Function1<IntermediateExpression,Function1<IntermediateExpression,Function1<IntermediateExpression,IntermediateGroupingExpression>>> curried()
    {
        return IntermediateGroupingExpression$.MODULE$.curried();
    }

    public IntermediateExpression projectKey()
    {
        return this.projectKey;
    }

    public IntermediateExpression computeKey()
    {
        return this.computeKey;
    }

    public IntermediateExpression getKey()
    {
        return this.getKey;
    }

    public IntermediateGroupingExpression copy( final IntermediateExpression projectKey, final IntermediateExpression computeKey,
            final IntermediateExpression getKey )
    {
        return new IntermediateGroupingExpression( projectKey, computeKey, getKey );
    }

    public IntermediateExpression copy$default$1()
    {
        return this.projectKey();
    }

    public IntermediateExpression copy$default$2()
    {
        return this.computeKey();
    }

    public IntermediateExpression copy$default$3()
    {
        return this.getKey();
    }

    public String productPrefix()
    {
        return "IntermediateGroupingExpression";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        IntermediateExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.projectKey();
            break;
        case 1:
            var10000 = this.computeKey();
            break;
        case 2:
            var10000 = this.getKey();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof IntermediateGroupingExpression;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof IntermediateGroupingExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            IntermediateGroupingExpression var4 = (IntermediateGroupingExpression) x$1;
                            IntermediateExpression var10000 = this.projectKey();
                            IntermediateExpression var5 = var4.projectKey();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            var10000 = this.computeKey();
                            IntermediateExpression var6 = var4.computeKey();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label63;
                            }

                            var10000 = this.getKey();
                            IntermediateExpression var7 = var4.getKey();
                            if ( var10000 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label54;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label72;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
