package org.neo4j.cypher.internal.executionplan;

import org.neo4j.cypher.result.QueryResult.QueryResultVisitor;

public interface GeneratedQueryExecution
{
    <E extends Exception> void accept( QueryResultVisitor<E> var1 ) throws E;

    String[] fieldNames();
}
