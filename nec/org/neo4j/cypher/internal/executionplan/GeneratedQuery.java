package org.neo4j.cypher.internal.executionplan;

import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.values.virtual.MapValue;

public interface GeneratedQuery
{
    GeneratedQueryExecution execute( QueryContext var1, QueryProfiler var2, MapValue var3 );
}
