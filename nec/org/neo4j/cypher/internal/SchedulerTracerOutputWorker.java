package org.neo4j.cypher.internal;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.neo4j.cypher.internal.runtime.pipelined.tracing.DataPointFlusher;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SingleConsumerDataBuffers;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import scala.concurrent.duration.FiniteDuration;
import scala.concurrent.duration.Duration.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SchedulerTracerOutputWorker extends LifecycleAdapter
{
    private final DataPointFlusher dataWriter;
    private final SingleConsumerDataBuffers dataBuffers;
    private final ThreadFactory threadFactory;
    private final FiniteDuration threadJoinWait;
    private volatile boolean isTimeToStop;
    private volatile Thread thread;

    public SchedulerTracerOutputWorker( final DataPointFlusher dataWriter, final SingleConsumerDataBuffers dataBuffers, final ThreadFactory threadFactory )
    {
        this.dataWriter = dataWriter;
        this.dataBuffers = dataBuffers;
        this.threadFactory = threadFactory;
        this.threadJoinWait = .MODULE$.apply( 1L, TimeUnit.MINUTES );
        this.isTimeToStop = false;
    }

    private FiniteDuration threadJoinWait()
    {
        return this.threadJoinWait;
    }

    private boolean isTimeToStop()
    {
        return this.isTimeToStop;
    }

    private void isTimeToStop_$eq( final boolean x$1 )
    {
        this.isTimeToStop = x$1;
    }

    private Thread thread()
    {
        return this.thread;
    }

    private void thread_$eq( final Thread x$1 )
    {
        this.thread = x$1;
    }

    private void run()
    {
        try
        {
            while ( !this.isTimeToStop() )
            {
                this.dataBuffers.consume( this.dataWriter );
                Thread.sleep( 1L );
            }
        }
        finally
        {
            this.dataBuffers.consume( this.dataWriter );
            this.dataWriter.close();
        }
    }

    public void start()
    {
        this.isTimeToStop_$eq( false );
        this.thread_$eq( this.threadFactory.newThread( () -> {
            this.run();
        } ) );
        this.thread().start();
    }

    public void stop()
    {
        this.isTimeToStop_$eq( true );
        this.thread().join( this.threadJoinWait().toMillis() );
    }
}
