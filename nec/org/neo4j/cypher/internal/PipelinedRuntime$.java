package org.neo4j.cypher.internal;

public final class PipelinedRuntime$
{
    public static PipelinedRuntime$ MODULE$;

    static
    {
        new PipelinedRuntime$();
    }

    private final PipelinedRuntime PIPELINED;
    private final PipelinedRuntime PARALLEL;
    private final String CODE_GEN_FAILED_MESSAGE;

    private PipelinedRuntime$()
    {
        MODULE$ = this;
        this.PIPELINED = new PipelinedRuntime( false, "pipelined" );
        this.PARALLEL = new PipelinedRuntime( true, "parallel" );
        this.CODE_GEN_FAILED_MESSAGE = "Code generation failed. Retrying physical planning.";
    }

    public PipelinedRuntime PIPELINED()
    {
        return this.PIPELINED;
    }

    public PipelinedRuntime PARALLEL()
    {
        return this.PARALLEL;
    }

    public String CODE_GEN_FAILED_MESSAGE()
    {
        return this.CODE_GEN_FAILED_MESSAGE;
    }
}
