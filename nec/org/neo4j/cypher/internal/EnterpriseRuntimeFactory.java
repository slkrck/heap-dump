package org.neo4j.cypher.internal;

import org.neo4j.cypher.CypherRuntimeOption;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class EnterpriseRuntimeFactory
{
    {
        return EnterpriseRuntimeFactory$.MODULE$.
        default
            ();
    }

    public static FallbackRuntime<EnterpriseRuntimeContext>default()

    public static CypherRuntime<EnterpriseRuntimeContext> getRuntime( final CypherRuntimeOption cypherRuntime, final boolean disallowFallback )
    {
        return EnterpriseRuntimeFactory$.MODULE$.getRuntime( var0, var1 );
    }

    public static FallbackRuntime<EnterpriseRuntimeContext> parallelWithoutFallback()
    {
        return EnterpriseRuntimeFactory$.MODULE$.parallelWithoutFallback();
    }

    public static FallbackRuntime<EnterpriseRuntimeContext> pipelined()
    {
        return EnterpriseRuntimeFactory$.MODULE$.pipelined();
    }

    public static FallbackRuntime<EnterpriseRuntimeContext> pipelinedWithoutFallback()
    {
        return EnterpriseRuntimeFactory$.MODULE$.pipelinedWithoutFallback();
    }

    public static FallbackRuntime<EnterpriseRuntimeContext> compiled()
    {
        return EnterpriseRuntimeFactory$.MODULE$.compiled();
    }

    public static FallbackRuntime<EnterpriseRuntimeContext> compiledWithoutFallback()
    {
        return EnterpriseRuntimeFactory$.MODULE$.compiledWithoutFallback();
    }

    public static FallbackRuntime<EnterpriseRuntimeContext> slotted()
    {
        return EnterpriseRuntimeFactory$.MODULE$.slotted();
    }

    public static FallbackRuntime<EnterpriseRuntimeContext> interpreted()
    {
        return EnterpriseRuntimeFactory$.MODULE$.interpreted();
    }
}
