package org.neo4j.cypher.internal.codegen;

import java.util.ArrayList;
import java.util.Comparator;

public class DefaultFullSortTable<T extends Comparable<?>> extends ArrayList<T>
{
    public DefaultFullSortTable( int initialCapacity )
    {
        super( initialCapacity );
    }

    public void sort()
    {
        this.sort( (Comparator) null );
    }
}
