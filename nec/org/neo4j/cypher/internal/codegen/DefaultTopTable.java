package org.neo4j.cypher.internal.codegen;

import java.util.Comparator;

import org.neo4j.cypher.internal.DefaultComparatorTopTable;

public class DefaultTopTable<T extends Comparable<Object>> extends DefaultComparatorTopTable<T>
{
    public DefaultTopTable( int totalCount )
    {
        super( Comparator.naturalOrder(), totalCount );
    }
}
