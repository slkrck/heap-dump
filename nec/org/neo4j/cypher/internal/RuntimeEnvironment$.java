package org.neo4j.cypher.internal;

import java.io.File;
import java.util.concurrent.ThreadFactory;

import org.neo4j.cypher.internal.NoSchedulerTracing.;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerManagement;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerResourceProvider;
import org.neo4j.cypher.internal.runtime.pipelined.execution.CallingThreadQueryExecutor;
import org.neo4j.cypher.internal.runtime.pipelined.execution.FixedWorkersQueryExecutor;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.CsvFileDataWriter;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.CsvStdOutDataWriter;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.DataPointFlusher;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.DataPointSchedulerTracer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer$;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SingleConsumerDataBuffers;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SingleConsumerDataBuffers$;
import org.neo4j.exceptions.InternalException;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import scala.Function0;

public final class RuntimeEnvironment$
{
    public static RuntimeEnvironment$ MODULE$;

    static
    {
        new RuntimeEnvironment$();
    }

    private RuntimeEnvironment$()
    {
        MODULE$ = this;
    }

    public RuntimeEnvironment of( final CypherRuntimeConfiguration config, final JobScheduler jobScheduler, final CursorFactory cursors,
            final LifeSupport lifeSupport, final WorkerManagement workerManager )
    {
        return new RuntimeEnvironment( config, this.createPipelinedQueryExecutor( cursors ),
                this.createParallelQueryExecutor( cursors, lifeSupport, workerManager ), this.createTracer( config, jobScheduler, lifeSupport ), cursors );
    }

    private CallingThreadQueryExecutor createPipelinedQueryExecutor( final CursorFactory cursors )
    {
        return new CallingThreadQueryExecutor( cursors );
    }

    private QueryExecutor createParallelQueryExecutor( final CursorFactory cursors, final LifeSupport lifeSupport, final WorkerManagement workerManager )
    {
        Function0 resourceFactory = () -> {
            return new QueryResources( cursors );
        };
        WorkerResourceProvider workerResourceProvider = new WorkerResourceProvider( workerManager.numberOfWorkers(), resourceFactory );
        lifeSupport.add( workerResourceProvider );
        FixedWorkersQueryExecutor queryExecutor = new FixedWorkersQueryExecutor( workerResourceProvider, workerManager );
        return queryExecutor;
    }

    public SchedulerTracer createTracer( final CypherRuntimeConfiguration config, final JobScheduler jobScheduler, final LifeSupport lifeSupport )
    {
        Object var13;
        label35:
        {
            SchedulerTracingConfiguration var10000 = config.schedulerTracing();
            var5 = .MODULE$;
            if ( var10000 == null )
            {
                if ( var5 != null )
                {
                    break label35;
                }
            }
            else if ( !var10000.equals( var5 ) )
            {
                break label35;
            }

            var13 = SchedulerTracer$.MODULE$.NoSchedulerTracer();
            return (SchedulerTracer) var13;
        }

        SchedulerTracingConfiguration var7 = config.schedulerTracing();
        Object var4;
        if ( org.neo4j.cypher.internal.StdOutSchedulerTracing..MODULE$.equals( var7 )){
        var4 = new CsvStdOutDataWriter();
    } else{
        if ( !(var7 instanceof FileSchedulerTracing) )
        {
            throw new InternalException( (new StringBuilder( 27 )).append( "Unknown scheduler tracing: " ).append( var7 ).toString() );
        }

        FileSchedulerTracing var8 = (FileSchedulerTracing) var7;
        File file = var8.file();
        var4 = new CsvFileDataWriter( file );
    }

        SingleConsumerDataBuffers dataTracer = new SingleConsumerDataBuffers( SingleConsumerDataBuffers$.MODULE$.$lessinit$greater$default$1(),
                SingleConsumerDataBuffers$.MODULE$.$lessinit$greater$default$2() );
        ThreadFactory threadFactory = jobScheduler.threadFactory( Group.CYPHER_WORKER );
        SchedulerTracerOutputWorker tracerWorker = new SchedulerTracerOutputWorker( (DataPointFlusher) var4, dataTracer, threadFactory );
        lifeSupport.add( tracerWorker );
        var13 = new DataPointSchedulerTracer( dataTracer );
        return (SchedulerTracer) var13;
    }
}
