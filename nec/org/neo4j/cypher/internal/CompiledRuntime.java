package org.neo4j.cypher.internal;

import org.neo4j.cypher.internal.plandescription.Argument;
import org.neo4j.cypher.internal.profiling.ProfilingTracer;
import org.neo4j.cypher.internal.runtime.ExecutionMode;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.ProfileMode.;
import org.neo4j.cypher.internal.runtime.compiled.CompiledPlan;
import org.neo4j.cypher.internal.v4_0.util.InternalNotification;
import org.neo4j.cypher.result.RuntimeResult;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.virtual.MapValue;
import scala.Option;
import scala.Some;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class CompiledRuntime
{
    public static SecurityContext compileToExecutable$default$3()
    {
        return CompiledRuntime$.MODULE$.compileToExecutable$default$3();
    }

    public static ExecutionPlan compileToExecutable( final LogicalQuery query, final EnterpriseRuntimeContext context, final SecurityContext securityContext )
            throws CantCompileQueryException
    {
        return CompiledRuntime$.MODULE$.compileToExecutable( var0, var1, var2 );
    }

    public static String name()
    {
        return CompiledRuntime$.MODULE$.name();
    }

    public static class CompiledExecutionPlan extends ExecutionPlan
    {
        private final CompiledPlan compiled;
        private final RuntimeName runtimeName;

        public CompiledExecutionPlan( final CompiledPlan compiled )
        {
            this.compiled = compiled;
            this.runtimeName = org.neo4j.cypher.internal.CompiledRuntimeName..MODULE$;
        }

        public CompiledPlan compiled()
        {
            return this.compiled;
        }

        public RuntimeResult run( final QueryContext queryContext, final ExecutionMode executionMode, final MapValue params, final boolean prePopulateResults,
                final InputDataStream input, final QuerySubscriber subscriber )
        {
            boolean var10000;
            label22:
            {
                label21:
                {
                    var8 = .MODULE$;
                    if ( executionMode == null )
                    {
                        if ( var8 == null )
                        {
                            break label21;
                        }
                    }
                    else if ( executionMode.equals( var8 ) )
                    {
                        break label21;
                    }

                    var10000 = false;
                    break label22;
                }

                var10000 = true;
            }

            boolean doProfile = var10000;
            Option tracer = doProfile ? new Some( new ProfilingTracer( queryContext.transactionalContext().kernelStatisticProvider() ) ) : scala.None..MODULE$;
            return this.compiled().executionResultBuilder().apply( queryContext, executionMode, (Option) tracer, params, prePopulateResults, subscriber );
        }

        public RuntimeName runtimeName()
        {
            return this.runtimeName;
        }

        public Seq<Argument> metadata()
        {
            return this.compiled().executionResultBuilder().metadata();
        }

        public Set<InternalNotification> notifications()
        {
            return scala.Predef..MODULE$.Set().empty();
        }
    }
}
