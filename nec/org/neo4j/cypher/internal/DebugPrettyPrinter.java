package org.neo4j.cypher.internal;

import org.bitbucket.inkytonik.kiama.output.PrettyPrinter.Doc;
import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.CypherException;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.exceptions.InternalException;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface DebugPrettyPrinter
{
    private static String prettyPlanName$1( final LogicalPlan plan, final String planAnsiPre$1, final String planAnsiPost$1 )
    {
        return (new StringBuilder( 0 )).append( planAnsiPre$1 ).append( plan.productPrefix() ).append( planAnsiPost$1 ).toString();
    }

    private static String prettyId$1( final int id )
    {
        return (new StringBuilder( 19 )).append( "\u001b[4m\u001b[35m" ).append( new Id( id ) ).append( "\u001b[24m\u001b[35m" ).toString();
    }

    private static Doc show$1( final Object v, final String planAnsiPre$1, final String planAnsiPost$1 )
    {
        org.bitbucket.inkytonik.kiama.output.PrettyPrinter.var10000 = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$;
        Doc var3;
        if ( v instanceof Id )
        {
            int var6 = ((Id) v).x();
            var3 = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.text( prettyId$1( var6 ) );
        }
        else if ( !(v instanceof LogicalPlan) )
        {
            var3 = (Doc) org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.any( v );
        }
        else
        {
            Doc var4;
            label60:
            {
                LogicalPlan var7 = (LogicalPlan) v;
                Tuple2 var8 = new Tuple2( var7.lhs(), var7.rhs() );
                org.bitbucket.inkytonik.kiama.output.PrettyPrinter.var10002;
                if ( var8 != null )
                {
                    Option var9 = (Option) var8._1();
                    Option var10 = (Option) var8._2();
                    if ( scala.None..MODULE$.equals( var9 ) && scala.None..MODULE$.equals( var10 )){
                    List elements = var7.productIterator().toList();
                    var10002 = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$;
                    int var12 = var7.id();
                    var4 = (Doc) var10002.list( elements.$colon$colon( new Id( var12 ) ), prettyPlanName$1( var7, planAnsiPre$1, planAnsiPost$1 ), ( vx ) -> {
                        return show$1( vx, planAnsiPre$1, planAnsiPost$1 );
                    }, org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.list$default$4(), org.bitbucket.inkytonik.kiama.output.PrettyPrinter..
                    MODULE$.list$default$5());
                    break label60;
                }
                }

                if ( var8 != null )
                {
                    Option var13 = (Option) var8._1();
                    Option var14 = (Option) var8._2();
                    if ( var13 instanceof Some )
                    {
                        Some var15 = (Some) var13;
                        LogicalPlan lhs = (LogicalPlan) var15.value();
                        if ( scala.None..MODULE$.equals( var14 )){
                        List otherElements = (List) var7.productIterator().toList().filter( ( x0$1 ) -> {
                            return BoxesRunTime.boxToBoolean( $anonfun$prettyPrintLogicalPlan$2( lhs, x0$1 ) );
                        } );
                        var10002 = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$;
                        int var18 = var7.id();
                        var4 = ((Doc) var10002.list( otherElements.$colon$colon( new Id( var18 ) ), prettyPlanName$1( var7, planAnsiPre$1, planAnsiPost$1 ),
                                ( vx ) -> {
                                    return show$1( vx, planAnsiPre$1, planAnsiPost$1 );
                                },
                                org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.list$default$4(), org.bitbucket.inkytonik.kiama.output.PrettyPrinter..
                        MODULE$.list$default$5())).$less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.line()).
                        $less$greater( show$1( lhs, planAnsiPre$1, planAnsiPost$1 ) );
                        break label60;
                    }
                    }
                }

                if ( var8 == null )
                {
                    throw new InternalException( "Invalid logical plan structure" );
                }

                Option var19 = (Option) var8._1();
                Option var20 = (Option) var8._2();
                if ( !(var19 instanceof Some) )
                {
                    throw new InternalException( "Invalid logical plan structure" );
                }

                Some var21 = (Some) var19;
                LogicalPlan lhs = (LogicalPlan) var21.value();
                if ( !(var20 instanceof Some) )
                {
                    throw new InternalException( "Invalid logical plan structure" );
                }

                Some var23 = (Some) var20;
                LogicalPlan rhs = (LogicalPlan) var23.value();
                List otherElements = (List) var7.productIterator().toList().filter( ( x0$2 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$prettyPrintLogicalPlan$4( lhs, rhs, x0$2 ) );
                } );
                Doc lhsDoc = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..
                MODULE$.text( "[LHS]" ).$less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.line()).
                $less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.nest( show$1( lhs, planAnsiPre$1, planAnsiPost$1 ), 2 ));
                Doc rhsDoc = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.text(
                    (new StringBuilder( 12 )).append( "[RHS of " ).append( var7.getClass().getSimpleName() ).append( " (" ).append(
                            new Id( var7.id() ) ).append( ")]" ).toString() ).$less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.line()).
                $less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.nest( show$1( rhs, planAnsiPre$1, planAnsiPost$1 ), 2 ));
                var10002 = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$;
                int var28 = var7.id();
                var4 = ((Doc) var10002.list( otherElements.$colon$colon( new Id( var28 ) ), prettyPlanName$1( var7, planAnsiPre$1, planAnsiPost$1 ), ( vx ) -> {
                    return show$1( vx, planAnsiPre$1, planAnsiPost$1 );
                }, org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.list$default$4(), org.bitbucket.inkytonik.kiama.output.PrettyPrinter..
                MODULE$.list$default$5())).$less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.line()).
                $less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.nest( lhsDoc, 2 )).
                $less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.line()).
                $less$greater( org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.nest( rhsDoc, 2 ));
            }

            var3 = var4;
        }

        return var10000.link( v, var3 );
    }

    static void $init$( final DebugPrettyPrinter $this )
    {
        $this.org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_QUERY_TEXT_$eq( true );
        $this.org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_LOGICAL_PLAN_$eq( true );
        $this.org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_REWRITTEN_LOGICAL_PLAN_$eq( true );
        $this.org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_PIPELINE_INFO_$eq( true );
        $this.org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_FAILURE_STACK_TRACE_$eq( true );
    }

    void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_QUERY_TEXT_$eq( final boolean x$1 );

    void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_LOGICAL_PLAN_$eq( final boolean x$1 );

    void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_REWRITTEN_LOGICAL_PLAN_$eq( final boolean x$1 );

    void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_PIPELINE_INFO_$eq( final boolean x$1 );

    void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_FAILURE_STACK_TRACE_$eq( final boolean x$1 );

    boolean PRINT_QUERY_TEXT();

    boolean PRINT_LOGICAL_PLAN();

    boolean PRINT_REWRITTEN_LOGICAL_PLAN();

    boolean PRINT_PIPELINE_INFO();

    boolean PRINT_FAILURE_STACK_TRACE();

    default void printPlanInfo( final LogicalQuery logicalQuery )
    {
      .MODULE$.println( "\n========================================================================" );
        if ( this.PRINT_QUERY_TEXT() )
        {
         .MODULE$.println( (new StringBuilder( 14 )).append( "\u001b[32m[QUERY]\n\n" ).append( logicalQuery.queryText() ).toString() );
        }

        if ( this.PRINT_LOGICAL_PLAN() )
        {
         .MODULE$.println( "\n\u001b[35m[LOGICAL PLAN]\n" );
            this.prettyPrintLogicalPlan( logicalQuery.logicalPlan() );
        }

      .MODULE$.println( "\u001b[30m" );
    }

    default void printRewrittenPlanInfo( final LogicalPlan logicalPlan )
    {
        if ( this.PRINT_REWRITTEN_LOGICAL_PLAN() )
        {
         .MODULE$.println( "\n\u001b[35m[REWRITTEN LOGICAL PLAN]\n" );
            this.prettyPrintLogicalPlan( logicalPlan );
        }

      .MODULE$.println( "\u001b[30m" );
    }

    default void printPipe( final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations, final Pipe pipe )
    {
        if ( this.PRINT_PIPELINE_INFO() )
        {
         .MODULE$.println( "\n\u001b[36m[SLOT CONFIGURATIONS]\n" );
            this.prettyPrintPipelines( slotConfigurations );
            if ( pipe != null )
            {
            .MODULE$.println( "\n\u001b[34m[PIPE INFO]\n" );
                this.prettyPrintPipe( pipe );
            }
        }

      .MODULE$.println( "\u001b[30m" );
    }

    default Pipe printPipe$default$2()
    {
        return null;
    }

    default void printFailureStackTrace( final CypherException e )
    {
        if ( this.PRINT_FAILURE_STACK_TRACE() )
        {
         .MODULE$.println( "------------------------------------------------" );
         .MODULE$.println( "<<< Slotted failed because:\u001b[31m" );
            e.printStackTrace( System.out );
         .MODULE$.println( "\u001b[30m>>>" );
         .MODULE$.println( "------------------------------------------------" );
        }
    }

    private default void prettyPrintLogicalPlan( final LogicalPlan plan )
    {
        String planAnsiPre = "\u001b[1m\u001b[35m";
        String planAnsiPost = "\u001b[21m\u001b[35m";
        Document prettyDoc = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.pretty( show$1( plan, planAnsiPre, planAnsiPost ), 120 );
      .MODULE$.println( prettyDoc.layout() );
    }

    default void prettyPrintPipelines( final PhysicalPlanningAttributes.SlotConfigurations pipelines )
    {
        Seq transformedPipelines = (Seq) ((SeqLike) pipelines.iterator().foldLeft( scala.collection.Seq..MODULE$.empty(), ( x0$3, x1$1 ) -> {
        Tuple2 var3 = new Tuple2( x0$3, x1$1 );
        if ( var3 != null )
        {
            Seq acc = (Seq) var3._1();
            Tuple2 var5 = (Tuple2) var3._2();
            if ( var5 != null )
            {
                int k = ((Id) var5._1()).x();
                SlotConfiguration v = (SlotConfiguration) var5._2();
                if ( new Id( k ) instanceof Id )
                {
                    Seq var2 = (Seq) acc.$colon$plus( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension(.MODULE$.ArrowAssoc(
                            BoxesRunTime.boxToInteger( k ) ), v),scala.collection.Seq..MODULE$.canBuildFrom());
                    return var2;
                }
            }
        }

        throw new MatchError( var3 );
    })).sortBy( ( x0$4 ) -> {
        return BoxesRunTime.boxToInteger( $anonfun$prettyPrintPipelines$2( x0$4 ) );
    }, scala.math.Ordering.Int..MODULE$);
        Document prettyDoc = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..
        MODULE$.pretty( (Doc) org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.any( transformedPipelines ), 120);
      .MODULE$.println( prettyDoc.layout() );
    }

    default void prettyPrintPipe( final Pipe pipe )
    {
        Document prettyDoc = org.bitbucket.inkytonik.kiama.output.PrettyPrinter..
        MODULE$.pretty( (Doc) org.bitbucket.inkytonik.kiama.output.PrettyPrinter..MODULE$.any( pipe ), 120);
      .MODULE$.println( prettyDoc.layout() );
    }
}
