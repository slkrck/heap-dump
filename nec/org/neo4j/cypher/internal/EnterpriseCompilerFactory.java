package org.neo4j.cypher.internal;

import org.neo4j.common.DependencyResolver;
import org.neo4j.cypher.CypherPlannerOption;
import org.neo4j.cypher.CypherRuntimeOption;
import org.neo4j.cypher.CypherUpdateStrategy;
import org.neo4j.cypher.CypherVersion;
import org.neo4j.cypher.CypherVersion.v3_5.;
import org.neo4j.cypher.internal.compiler.CypherPlannerConfiguration;
import org.neo4j.cypher.internal.planning.CypherPlanner;
import org.neo4j.cypher.internal.runtime.pipelined.WorkerManagement;
import org.neo4j.cypher.internal.spi.codegen.GeneratedQueryStructure$;
import org.neo4j.kernel.GraphDatabaseQueryService;
import org.neo4j.kernel.impl.query.QueryEngineProvider.SPI;
import org.neo4j.logging.Log;
import scala.Function0;
import scala.MatchError;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class EnterpriseCompilerFactory implements CompilerFactory
{
    private final GraphDatabaseQueryService graph;
    private final SPI spi;
    private final CypherPlannerConfiguration plannerConfig;
    private final CypherRuntimeConfiguration runtimeConfig;
    private final RuntimeEnvironment runtimeEnvironment;
    private final Log log;

    public EnterpriseCompilerFactory( final GraphDatabaseQueryService graph, final SPI spi, final CypherPlannerConfiguration plannerConfig,
            final CypherRuntimeConfiguration runtimeConfig )
    {
        this.graph = graph;
        this.spi = spi;
        this.plannerConfig = plannerConfig;
        this.runtimeConfig = runtimeConfig;
        DependencyResolver resolver = graph.getDependencyResolver();
        WorkerManagement workerManager = (WorkerManagement) resolver.resolveDependency( WorkerManagement.class );
        this.runtimeEnvironment = RuntimeEnvironment$.MODULE$.of( runtimeConfig, spi.jobScheduler(), spi.kernel().cursors(), spi.lifeSupport(), workerManager );
        this.log = spi.logProvider().getLog( this.getClass() );
    }

    private RuntimeEnvironment runtimeEnvironment()
    {
        return this.runtimeEnvironment;
    }

    private Log log()
    {
        return this.log;
    }

    public Compiler createCompiler( final CypherVersion cypherVersion, final CypherPlannerOption cypherPlanner, final CypherRuntimeOption cypherRuntime,
            final CypherUpdateStrategy cypherUpdateStrategy, final Function0<ExecutionEngine> executionEngineProvider )
    {
        boolean var6;
        if (.MODULE$.equals( cypherVersion )){
        var6 = true;
    } else{
        if ( !org.neo4j.cypher.CypherVersion.v4_0..MODULE$.equals( cypherVersion )){
            throw new MatchError( cypherVersion );
        }

        var6 = false;
    }

        CypherPlanner planner = new CypherPlanner( this.plannerConfig, org.neo4j.cypher.internal.MasterCompiler..MODULE$.CLOCK(),
        this.spi.monitors(), this.log(), cypherPlanner, cypherUpdateStrategy, new LastCommittedTxIdProvider( this.graph ), var6);
        CypherRuntime runtime =
                this.plannerConfig.planSystemCommands() ? new EnterpriseAdministrationCommandRuntime( (ExecutionEngine) executionEngineProvider.apply(),
                        this.graph.getDependencyResolver() ) : EnterpriseRuntimeFactory$.MODULE$.getRuntime( cypherRuntime,
                        this.plannerConfig.useErrorsOverWarnings() );
        return new CypherCurrentCompiler( planner, (CypherRuntime) runtime,
                new EnterpriseRuntimeContextManager( GeneratedQueryStructure$.MODULE$, this.log(), this.runtimeConfig, this.runtimeEnvironment() ),
                this.spi.monitors() );
    }
}
