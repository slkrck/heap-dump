package org.neo4j.cypher.internal;

import java.time.Clock;

import org.neo4j.cypher.CypherInterpretedPipesFallbackOption;
import org.neo4j.cypher.CypherOperatorEngineOption;
import org.neo4j.cypher.internal.executionplan.GeneratedQuery;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure;
import org.neo4j.internal.kernel.api.SchemaRead;
import org.neo4j.logging.Log;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple12;
import scala.collection.Iterator;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class EnterpriseRuntimeContext extends RuntimeContext implements Product, Serializable
{
    private final TokenContext tokenContext;
    private final SchemaRead schemaRead;
    private final CodeStructure<GeneratedQuery> codeStructure;
    private final Log log;
    private final Clock clock;
    private final Set<String> debugOptions;
    private final CypherRuntimeConfiguration config;
    private final RuntimeEnvironment runtimeEnvironment;
    private final boolean compileExpressions;
    private final boolean materializedEntitiesMode;
    private final CypherOperatorEngineOption operatorEngine;
    private final CypherInterpretedPipesFallbackOption interpretedPipesFallback;

    public EnterpriseRuntimeContext( final TokenContext tokenContext, final SchemaRead schemaRead, final CodeStructure<GeneratedQuery> codeStructure,
            final Log log, final Clock clock, final Set<String> debugOptions, final CypherRuntimeConfiguration config,
            final RuntimeEnvironment runtimeEnvironment, final boolean compileExpressions, final boolean materializedEntitiesMode,
            final CypherOperatorEngineOption operatorEngine, final CypherInterpretedPipesFallbackOption interpretedPipesFallback )
    {
        this.tokenContext = tokenContext;
        this.schemaRead = schemaRead;
        this.codeStructure = codeStructure;
        this.log = log;
        this.clock = clock;
        this.debugOptions = debugOptions;
        this.config = config;
        this.runtimeEnvironment = runtimeEnvironment;
        this.compileExpressions = compileExpressions;
        this.materializedEntitiesMode = materializedEntitiesMode;
        this.operatorEngine = operatorEngine;
        this.interpretedPipesFallback = interpretedPipesFallback;
        Product.$init$( this );
    }

    public static Option<Tuple12<TokenContext,SchemaRead,CodeStructure<GeneratedQuery>,Log,Clock,Set<String>,CypherRuntimeConfiguration,RuntimeEnvironment,Object,Object,CypherOperatorEngineOption,CypherInterpretedPipesFallbackOption>> unapply(
            final EnterpriseRuntimeContext x$0 )
    {
        return EnterpriseRuntimeContext$.MODULE$.unapply( var0 );
    }

    public static EnterpriseRuntimeContext apply( final TokenContext tokenContext, final SchemaRead schemaRead,
            final CodeStructure<GeneratedQuery> codeStructure, final Log log, final Clock clock, final Set<String> debugOptions,
            final CypherRuntimeConfiguration config, final RuntimeEnvironment runtimeEnvironment, final boolean compileExpressions,
            final boolean materializedEntitiesMode, final CypherOperatorEngineOption operatorEngine,
            final CypherInterpretedPipesFallbackOption interpretedPipesFallback )
    {
        return EnterpriseRuntimeContext$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11 );
    }

    public static Function1<Tuple12<TokenContext,SchemaRead,CodeStructure<GeneratedQuery>,Log,Clock,Set<String>,CypherRuntimeConfiguration,RuntimeEnvironment,Object,Object,CypherOperatorEngineOption,CypherInterpretedPipesFallbackOption>,EnterpriseRuntimeContext> tupled()
    {
        return EnterpriseRuntimeContext$.MODULE$.tupled();
    }

    public static Function1<TokenContext,Function1<SchemaRead,Function1<CodeStructure<GeneratedQuery>,Function1<Log,Function1<Clock,Function1<Set<String>,Function1<CypherRuntimeConfiguration,Function1<RuntimeEnvironment,Function1<Object,Function1<Object,Function1<CypherOperatorEngineOption,Function1<CypherInterpretedPipesFallbackOption,EnterpriseRuntimeContext>>>>>>>>>>>> curried()
    {
        return EnterpriseRuntimeContext$.MODULE$.curried();
    }

    public TokenContext tokenContext()
    {
        return this.tokenContext;
    }

    public SchemaRead schemaRead()
    {
        return this.schemaRead;
    }

    public CodeStructure<GeneratedQuery> codeStructure()
    {
        return this.codeStructure;
    }

    public Log log()
    {
        return this.log;
    }

    public Clock clock()
    {
        return this.clock;
    }

    public Set<String> debugOptions()
    {
        return this.debugOptions;
    }

    public CypherRuntimeConfiguration config()
    {
        return this.config;
    }

    public RuntimeEnvironment runtimeEnvironment()
    {
        return this.runtimeEnvironment;
    }

    public boolean compileExpressions()
    {
        return this.compileExpressions;
    }

    public boolean materializedEntitiesMode()
    {
        return this.materializedEntitiesMode;
    }

    public CypherOperatorEngineOption operatorEngine()
    {
        return this.operatorEngine;
    }

    public CypherInterpretedPipesFallbackOption interpretedPipesFallback()
    {
        return this.interpretedPipesFallback;
    }

    public EnterpriseRuntimeContext copy( final TokenContext tokenContext, final SchemaRead schemaRead, final CodeStructure<GeneratedQuery> codeStructure,
            final Log log, final Clock clock, final Set<String> debugOptions, final CypherRuntimeConfiguration config,
            final RuntimeEnvironment runtimeEnvironment, final boolean compileExpressions, final boolean materializedEntitiesMode,
            final CypherOperatorEngineOption operatorEngine, final CypherInterpretedPipesFallbackOption interpretedPipesFallback )
    {
        return new EnterpriseRuntimeContext( tokenContext, schemaRead, codeStructure, log, clock, debugOptions, config, runtimeEnvironment, compileExpressions,
                materializedEntitiesMode, operatorEngine, interpretedPipesFallback );
    }

    public TokenContext copy$default$1()
    {
        return this.tokenContext();
    }

    public boolean copy$default$10()
    {
        return this.materializedEntitiesMode();
    }

    public CypherOperatorEngineOption copy$default$11()
    {
        return this.operatorEngine();
    }

    public CypherInterpretedPipesFallbackOption copy$default$12()
    {
        return this.interpretedPipesFallback();
    }

    public SchemaRead copy$default$2()
    {
        return this.schemaRead();
    }

    public CodeStructure<GeneratedQuery> copy$default$3()
    {
        return this.codeStructure();
    }

    public Log copy$default$4()
    {
        return this.log();
    }

    public Clock copy$default$5()
    {
        return this.clock();
    }

    public Set<String> copy$default$6()
    {
        return this.debugOptions();
    }

    public CypherRuntimeConfiguration copy$default$7()
    {
        return this.config();
    }

    public RuntimeEnvironment copy$default$8()
    {
        return this.runtimeEnvironment();
    }

    public boolean copy$default$9()
    {
        return this.compileExpressions();
    }

    public String productPrefix()
    {
        return "EnterpriseRuntimeContext";
    }

    public int productArity()
    {
        return 12;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.tokenContext();
            break;
        case 1:
            var10000 = this.schemaRead();
            break;
        case 2:
            var10000 = this.codeStructure();
            break;
        case 3:
            var10000 = this.log();
            break;
        case 4:
            var10000 = this.clock();
            break;
        case 5:
            var10000 = this.debugOptions();
            break;
        case 6:
            var10000 = this.config();
            break;
        case 7:
            var10000 = this.runtimeEnvironment();
            break;
        case 8:
            var10000 = BoxesRunTime.boxToBoolean( this.compileExpressions() );
            break;
        case 9:
            var10000 = BoxesRunTime.boxToBoolean( this.materializedEntitiesMode() );
            break;
        case 10:
            var10000 = this.operatorEngine();
            break;
        case 11:
            var10000 = this.interpretedPipesFallback();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EnterpriseRuntimeContext;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.tokenContext() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.schemaRead() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.codeStructure() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.log() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.clock() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.debugOptions() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.config() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.runtimeEnvironment() ) );
        var1 = Statics.mix( var1, this.compileExpressions() ? 1231 : 1237 );
        var1 = Statics.mix( var1, this.materializedEntitiesMode() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.operatorEngine() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.interpretedPipesFallback() ) );
        return Statics.finalizeHash( var1, 12 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var24;
        if ( this != x$1 )
        {
            label140:
            {
                boolean var2;
                if ( x$1 instanceof EnterpriseRuntimeContext )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label121:
                    {
                        label130:
                        {
                            EnterpriseRuntimeContext var4 = (EnterpriseRuntimeContext) x$1;
                            TokenContext var10000 = this.tokenContext();
                            TokenContext var5 = var4.tokenContext();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label130;
                            }

                            SchemaRead var15 = this.schemaRead();
                            SchemaRead var6 = var4.schemaRead();
                            if ( var15 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var15.equals( var6 ) )
                            {
                                break label130;
                            }

                            CodeStructure var16 = this.codeStructure();
                            CodeStructure var7 = var4.codeStructure();
                            if ( var16 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var16.equals( var7 ) )
                            {
                                break label130;
                            }

                            Log var17 = this.log();
                            Log var8 = var4.log();
                            if ( var17 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var17.equals( var8 ) )
                            {
                                break label130;
                            }

                            Clock var18 = this.clock();
                            Clock var9 = var4.clock();
                            if ( var18 == null )
                            {
                                if ( var9 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var18.equals( var9 ) )
                            {
                                break label130;
                            }

                            Set var19 = this.debugOptions();
                            Set var10 = var4.debugOptions();
                            if ( var19 == null )
                            {
                                if ( var10 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var19.equals( var10 ) )
                            {
                                break label130;
                            }

                            CypherRuntimeConfiguration var20 = this.config();
                            CypherRuntimeConfiguration var11 = var4.config();
                            if ( var20 == null )
                            {
                                if ( var11 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var20.equals( var11 ) )
                            {
                                break label130;
                            }

                            RuntimeEnvironment var21 = this.runtimeEnvironment();
                            RuntimeEnvironment var12 = var4.runtimeEnvironment();
                            if ( var21 == null )
                            {
                                if ( var12 != null )
                                {
                                    break label130;
                                }
                            }
                            else if ( !var21.equals( var12 ) )
                            {
                                break label130;
                            }

                            if ( this.compileExpressions() == var4.compileExpressions() && this.materializedEntitiesMode() == var4.materializedEntitiesMode() )
                            {
                                label131:
                                {
                                    CypherOperatorEngineOption var22 = this.operatorEngine();
                                    CypherOperatorEngineOption var13 = var4.operatorEngine();
                                    if ( var22 == null )
                                    {
                                        if ( var13 != null )
                                        {
                                            break label131;
                                        }
                                    }
                                    else if ( !var22.equals( var13 ) )
                                    {
                                        break label131;
                                    }

                                    CypherInterpretedPipesFallbackOption var23 = this.interpretedPipesFallback();
                                    CypherInterpretedPipesFallbackOption var14 = var4.interpretedPipesFallback();
                                    if ( var23 == null )
                                    {
                                        if ( var14 != null )
                                        {
                                            break label131;
                                        }
                                    }
                                    else if ( !var23.equals( var14 ) )
                                    {
                                        break label131;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var24 = true;
                                        break label121;
                                    }
                                }
                            }
                        }

                        var24 = false;
                    }

                    if ( var24 )
                    {
                        break label140;
                    }
                }

                var24 = false;
                return var24;
            }
        }

        var24 = true;
        return var24;
    }
}
