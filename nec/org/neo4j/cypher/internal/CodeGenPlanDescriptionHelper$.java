package org.neo4j.cypher.internal;

import org.neo4j.codegen.api.CodeGeneration.CodeSaver;
import org.neo4j.cypher.internal.plandescription.Argument;
import org.neo4j.cypher.internal.plandescription.Arguments.ByteCode;
import org.neo4j.cypher.internal.plandescription.Arguments.SourceCode;
import scala.MatchError;
import scala.collection.GenTraversableOnce;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.Seq.;

public final class CodeGenPlanDescriptionHelper$
{
    public static CodeGenPlanDescriptionHelper$ MODULE$;

    static
    {
        new CodeGenPlanDescriptionHelper$();
    }

    private CodeGenPlanDescriptionHelper$()
    {
        MODULE$ = this;
    }

    public Seq<Argument> metadata( final CodeSaver saver )
    {
        return (Seq) ((TraversableLike) saver.sourceCode().map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                String className = (String) x0$1._1();
                String sourceCode = (String) x0$1._2();
                SourceCode var1 = new SourceCode( className, sourceCode );
                return var1;
            }
            else
            {
                throw new MatchError( x0$1 );
            }
        },.MODULE$.canBuildFrom())).$plus$plus( (GenTraversableOnce) saver.bytecode().map( ( x0$2 ) -> {
        if ( x0$2 != null )
        {
            String className = (String) x0$2._1();
            String byteCode = (String) x0$2._2();
            ByteCode var1 = new ByteCode( className, byteCode );
            return var1;
        }
        else
        {
            throw new MatchError( x0$2 );
        }
    },.MODULE$.canBuildFrom() ), .MODULE$.canBuildFrom());
    }
}
