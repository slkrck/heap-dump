package org.neo4j.cypher.internal;

import org.neo4j.cypher.CypherRuntimeOption;
import org.neo4j.cypher.CypherRuntimeOption.interpreted.;
import scala.MatchError;
import scala.collection.immutable..colon.colon;

public final class EnterpriseRuntimeFactory$
{
    public static EnterpriseRuntimeFactory$ MODULE$;

    static
    {
        new EnterpriseRuntimeFactory$();
    }

    private final FallbackRuntime<EnterpriseRuntimeContext> interpreted;
    private final FallbackRuntime<EnterpriseRuntimeContext> slotted;
    private final FallbackRuntime<EnterpriseRuntimeContext> compiledWithoutFallback;
    private final FallbackRuntime<EnterpriseRuntimeContext> compiled;
    private final FallbackRuntime<EnterpriseRuntimeContext> pipelinedWithoutFallback;
    private final FallbackRuntime<EnterpriseRuntimeContext> pipelined;
    private final FallbackRuntime<EnterpriseRuntimeContext>default;
    private final FallbackRuntime<EnterpriseRuntimeContext> parallelWithoutFallback;

    {
        return this.
        default
            ;
    }

    private EnterpriseRuntimeFactory$()
    {
        MODULE$ = this;
        this.interpreted = new FallbackRuntime( new colon( org.neo4j.cypher.internal.SchemaCommandRuntime..MODULE$,
                new colon( org.neo4j.cypher.internal.InterpretedRuntime..MODULE$, scala.collection.immutable.Nil..MODULE$)), .MODULE$);
        this.slotted = new FallbackRuntime( new colon( org.neo4j.cypher.internal.SchemaCommandRuntime..MODULE$,
                new colon( SlottedRuntime$.MODULE$, scala.collection.immutable.Nil..MODULE$ )),org.neo4j.cypher.CypherRuntimeOption.slotted..MODULE$);
        this.compiledWithoutFallback = new FallbackRuntime( new colon( org.neo4j.cypher.internal.SchemaCommandRuntime..MODULE$,
                new colon( CompiledRuntime$.MODULE$, scala.collection.immutable.Nil..MODULE$ )),org.neo4j.cypher.CypherRuntimeOption.compiled..MODULE$);
        this.compiled = new FallbackRuntime( new colon( org.neo4j.cypher.internal.SchemaCommandRuntime..MODULE$,
                new colon( CompiledRuntime$.MODULE$, new colon( SlottedRuntime$.MODULE$, scala.collection.immutable.Nil..MODULE$ ) )),
        org.neo4j.cypher.CypherRuntimeOption.compiled..MODULE$);
        this.pipelinedWithoutFallback = new FallbackRuntime( new colon( org.neo4j.cypher.internal.SchemaCommandRuntime..MODULE$,
                new colon( PipelinedRuntime$.MODULE$.PIPELINED(), scala.collection.immutable.Nil..MODULE$ )),org.neo4j.cypher.CypherRuntimeOption.pipelined..
        MODULE$);
        this.pipelined = new FallbackRuntime( new colon( org.neo4j.cypher.internal.SchemaCommandRuntime..MODULE$,
                new colon( PipelinedRuntime$.MODULE$.PIPELINED(), new colon( SlottedRuntime$.MODULE$, scala.collection.immutable.Nil..MODULE$ ) )),
        org.neo4j.cypher.CypherRuntimeOption.pipelined..MODULE$);
        this.parallelWithoutFallback = new FallbackRuntime(
                new colon( PipelinedRuntime$.MODULE$.PARALLEL(), scala.collection.immutable.Nil..MODULE$ ), org.neo4j.cypher.CypherRuntimeOption.parallel..
        MODULE$);
        this.
        default =new FallbackRuntime( new colon( org.neo4j.cypher.internal.SchemaCommandRuntime..MODULE$,
            new colon( PipelinedRuntime$.MODULE$.PIPELINED(), new colon( SlottedRuntime$.MODULE$, scala.collection.immutable.Nil..MODULE$ ) )),
        org.neo4j.cypher.CypherRuntimeOption.
        default..MODULE$);
    }

    public FallbackRuntime<EnterpriseRuntimeContext> interpreted()
    {
        return this.interpreted;
    }

    public FallbackRuntime<EnterpriseRuntimeContext> slotted()
    {
        return this.slotted;
    }

    public FallbackRuntime<EnterpriseRuntimeContext> compiledWithoutFallback()
    {
        return this.compiledWithoutFallback;
    }

    public FallbackRuntime<EnterpriseRuntimeContext> compiled()
    {
        return this.compiled;
    }

    public FallbackRuntime<EnterpriseRuntimeContext> pipelinedWithoutFallback()
    {
        return this.pipelinedWithoutFallback;
    }

    public FallbackRuntime<EnterpriseRuntimeContext>default()

    public FallbackRuntime<EnterpriseRuntimeContext> pipelined()
    {
        return this.pipelined;
    }

    public FallbackRuntime<EnterpriseRuntimeContext> parallelWithoutFallback()
    {
        return this.parallelWithoutFallback;
    }

    public CypherRuntime<EnterpriseRuntimeContext> getRuntime( final CypherRuntimeOption cypherRuntime, final boolean disallowFallback )
    {
        boolean var4 = false;
        Object var5 = null;
        boolean var6 = false;
        Object var7 = null;
        FallbackRuntime var3;
        if (.MODULE$.equals( cypherRuntime )){
        var3 = this.interpreted();
    } else if ( org.neo4j.cypher.CypherRuntimeOption.slotted..MODULE$.equals( cypherRuntime )){
        var3 = this.slotted();
    } else{
        if ( org.neo4j.cypher.CypherRuntimeOption.compiled..MODULE$.equals( cypherRuntime )){
            var4 = true;
            if ( disallowFallback )
            {
                var3 = this.compiledWithoutFallback();
                return var3;
            }
        }

        if ( var4 )
        {
            var3 = this.compiled();
        }
        else
        {
            if ( org.neo4j.cypher.CypherRuntimeOption.pipelined..MODULE$.equals( cypherRuntime )){
            var6 = true;
            if ( disallowFallback )
            {
                var3 = this.pipelinedWithoutFallback();
                return var3;
            }
        }

            if ( var6 )
            {
                var3 = this.pipelined();
            }
            else if ( org.neo4j.cypher.CypherRuntimeOption.
            default..MODULE$.equals( cypherRuntime )){
                var3 = this.
                default
                    ();
            } else{
                if ( !org.neo4j.cypher.CypherRuntimeOption.parallel..MODULE$.equals( cypherRuntime )){
                    throw new MatchError( cypherRuntime );
                }

                var3 = this.parallelWithoutFallback();
            }
        }
    }

        return var3;
    }
}
