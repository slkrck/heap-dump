package org.neo4j.cypher.internal;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.runtime.compiled.CompiledPlan;
import org.neo4j.cypher.internal.runtime.compiled.removeCachedProperties$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenConfiguration$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenerator;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import scala.MatchError;
import scala.Tuple2;
import scala.Predef.;

public final class CompiledRuntime$ implements CypherRuntime<EnterpriseRuntimeContext>
{
    public static CompiledRuntime$ MODULE$;

    static
    {
        new CompiledRuntime$();
    }

    private CompiledRuntime$()
    {
        MODULE$ = this;
    }

    public SecurityContext compileToExecutable$default$3()
    {
        return CypherRuntime.compileToExecutable$default$3$( this );
    }

    public String name()
    {
        return "legacy_compiled";
    }

    public ExecutionPlan compileToExecutable( final LogicalQuery query, final EnterpriseRuntimeContext context, final SecurityContext securityContext )
            throws CantCompileQueryException
    {
        Tuple2 var6 = removeCachedProperties$.MODULE$.apply( query.logicalPlan(), query.semanticTable() );
        if ( var6 != null )
        {
            LogicalPlan newPlan = (LogicalPlan) var6._1();
            SemanticTable newSemanticTable = (SemanticTable) var6._2();
            Tuple2 var4 = new Tuple2( newPlan, newSemanticTable );
            LogicalPlan newPlan = (LogicalPlan) var4._1();
            SemanticTable newSemanticTable = (SemanticTable) var4._2();
            CodeGenerator codeGen =
                    new CodeGenerator( context.codeStructure(), context.clock(), CodeGenConfiguration$.MODULE$.apply( context.debugOptions() ) );
            CompiledPlan compiled =
                    codeGen.generate( newPlan, context.tokenContext(), newSemanticTable, query.readOnly(), query.cardinalities(),.MODULE$.wrapRefArray(
                            (Object[]) query.resultColumns() ));
            return new CompiledRuntime.CompiledExecutionPlan( compiled );
        }
        else
        {
            throw new MatchError( var6 );
        }
    }
}
