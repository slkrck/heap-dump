package org.neo4j.cypher.internal;

import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode.;
import org.neo4j.cypher.internal.InterpretedRuntime.InterpretedExecutionPlan;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanner$;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes;
import org.neo4j.cypher.internal.runtime.QueryIndexRegistrator;
import org.neo4j.cypher.internal.runtime.interpreted.InterpretedPipeMapper;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.CommunityExpressionConverter;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeTreeBuilder;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionResultBuilderFactory;
import org.neo4j.cypher.internal.runtime.slotted.SlottedPipeMapper;
import org.neo4j.cypher.internal.runtime.slotted.SlottedPipelineBreakingPolicy$;
import org.neo4j.cypher.internal.runtime.slotted.expressions.CompiledExpressionConverter;
import org.neo4j.cypher.internal.runtime.slotted.expressions.CompiledExpressionConverter$;
import org.neo4j.cypher.internal.runtime.slotted.expressions.MaterializedEntitiesExpressionConverter;
import org.neo4j.cypher.internal.runtime.slotted.expressions.SlottedExpressionConverters;
import org.neo4j.cypher.internal.v4_0.util.CypherException;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.immutable..colon.colon;

public final class SlottedRuntime$ implements CypherRuntime<EnterpriseRuntimeContext>, DebugPrettyPrinter
{
    public static SlottedRuntime$ MODULE$;

    static
    {
        new SlottedRuntime$();
    }

    private final boolean ENABLE_DEBUG_PRINTS;
    private final boolean PRINT_PLAN_INFO_EARLY;
    private final boolean PRINT_QUERY_TEXT;
    private final boolean PRINT_LOGICAL_PLAN;
    private final boolean PRINT_REWRITTEN_LOGICAL_PLAN;
    private final boolean PRINT_PIPELINE_INFO;
    private final boolean PRINT_FAILURE_STACK_TRACE;

    private SlottedRuntime$()
    {
        MODULE$ = this;
        DebugPrettyPrinter.$init$( this );
        this.ENABLE_DEBUG_PRINTS = false;
        this.PRINT_PLAN_INFO_EARLY = true;
        this.PRINT_QUERY_TEXT = true;
        this.PRINT_LOGICAL_PLAN = true;
        this.PRINT_REWRITTEN_LOGICAL_PLAN = true;
        this.PRINT_PIPELINE_INFO = true;
        this.PRINT_FAILURE_STACK_TRACE = true;
    }

    public void printPlanInfo( final LogicalQuery logicalQuery )
    {
        DebugPrettyPrinter.printPlanInfo$( this, logicalQuery );
    }

    public void printRewrittenPlanInfo( final LogicalPlan logicalPlan )
    {
        DebugPrettyPrinter.printRewrittenPlanInfo$( this, logicalPlan );
    }

    public void printPipe( final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations, final Pipe pipe )
    {
        DebugPrettyPrinter.printPipe$( this, slotConfigurations, pipe );
    }

    public void printFailureStackTrace( final CypherException e )
    {
        DebugPrettyPrinter.printFailureStackTrace$( this, e );
    }

    public void prettyPrintPipelines( final PhysicalPlanningAttributes.SlotConfigurations pipelines )
    {
        DebugPrettyPrinter.prettyPrintPipelines$( this, pipelines );
    }

    public void prettyPrintPipe( final Pipe pipe )
    {
        DebugPrettyPrinter.prettyPrintPipe$( this, pipe );
    }

    public Pipe printPipe$default$2()
    {
        return DebugPrettyPrinter.printPipe$default$2$( this );
    }

    public SecurityContext compileToExecutable$default$3()
    {
        return CypherRuntime.compileToExecutable$default$3$( this );
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_QUERY_TEXT_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_LOGICAL_PLAN_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_REWRITTEN_LOGICAL_PLAN_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_PIPELINE_INFO_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_FAILURE_STACK_TRACE_$eq( final boolean x$1 )
    {
    }

    public String name()
    {
        return "slotted";
    }

    public boolean ENABLE_DEBUG_PRINTS()
    {
        return this.ENABLE_DEBUG_PRINTS;
    }

    public boolean PRINT_PLAN_INFO_EARLY()
    {
        return this.PRINT_PLAN_INFO_EARLY;
    }

    public boolean PRINT_QUERY_TEXT()
    {
        return this.PRINT_QUERY_TEXT;
    }

    public boolean PRINT_LOGICAL_PLAN()
    {
        return this.PRINT_LOGICAL_PLAN;
    }

    public boolean PRINT_REWRITTEN_LOGICAL_PLAN()
    {
        return this.PRINT_REWRITTEN_LOGICAL_PLAN;
    }

    public boolean PRINT_PIPELINE_INFO()
    {
        return this.PRINT_PIPELINE_INFO;
    }

    public boolean PRINT_FAILURE_STACK_TRACE()
    {
        return this.PRINT_FAILURE_STACK_TRACE;
    }

    public ExecutionPlan compileToExecutable( final LogicalQuery query, final EnterpriseRuntimeContext context, final SecurityContext securityContext )
            throws CantCompileQueryException
    {
        try
        {
            if ( this.ENABLE_DEBUG_PRINTS() && this.PRINT_PLAN_INFO_EARLY() )
            {
                this.printPlanInfo( query );
            }

            PhysicalPlan physicalPlan =
                    PhysicalPlanner$.MODULE$.plan( context.tokenContext(), query.logicalPlan(), query.semanticTable(), SlottedPipelineBreakingPolicy$.MODULE$,
                            PhysicalPlanner$.MODULE$.plan$default$5() );
            if ( this.ENABLE_DEBUG_PRINTS() && this.PRINT_PLAN_INFO_EARLY() )
            {
                this.printRewrittenPlanInfo( physicalPlan.logicalPlan() );
            }

            CodeGenerationMode codeGenerationMode = .MODULE$.fromDebugOptions( context.debugOptions() );
            List baseConverters = new colon( new SlottedExpressionConverters( physicalPlan ),
                    new colon( new CommunityExpressionConverter( context.tokenContext() ), scala.collection.immutable.Nil..MODULE$ ));
            Object var10000;
            if ( context.materializedEntitiesMode() )
            {
                MaterializedEntitiesExpressionConverter var8 = new MaterializedEntitiesExpressionConverter( context.tokenContext() );
                var10000 = (List) baseConverters.$plus$colon( var8, scala.collection.immutable.List..MODULE$.canBuildFrom());
            }
            else if ( context.compileExpressions() )
            {
                CompiledExpressionConverter var9 =
                        new CompiledExpressionConverter( context.log(), physicalPlan, context.tokenContext(), query.readOnly(), codeGenerationMode,
                                CompiledExpressionConverter$.MODULE$.$lessinit$greater$default$6() );
                var10000 = (List) baseConverters.$plus$colon( var9, scala.collection.immutable.List..MODULE$.canBuildFrom());
            }
            else
            {
                var10000 = baseConverters;
            }

            List allConverters = var10000;
            ExpressionConverters converters = new ExpressionConverters( (Seq) allConverters );
            QueryIndexRegistrator queryIndexRegistrator = new QueryIndexRegistrator( context.schemaRead() );
            InterpretedPipeMapper fallback =
                    new InterpretedPipeMapper( query.readOnly(), converters, context.tokenContext(), queryIndexRegistrator, query.semanticTable() );
            SlottedPipeMapper pipeBuilder =
                    new SlottedPipeMapper( fallback, converters, physicalPlan, query.readOnly(), queryIndexRegistrator, query.semanticTable() );
            PipeTreeBuilder pipeTreeBuilder = new PipeTreeBuilder( pipeBuilder );
            LogicalPlan logicalPlanWithConvertedNestedPlans = org.neo4j.cypher.internal.runtime.interpreted.pipes.NestedPipeExpressions..
            MODULE$.build( pipeTreeBuilder, physicalPlan.logicalPlan(), physicalPlan.availableExpressionVariables() );
            Pipe pipe = pipeTreeBuilder.build( logicalPlanWithConvertedNestedPlans );
            String[] columns = query.resultColumns();
            SlottedExecutionResultBuilderFactory resultBuilderFactory =
                    new SlottedExecutionResultBuilderFactory( pipe, queryIndexRegistrator.result(), physicalPlan.nExpressionSlots(), query.readOnly(),
                            scala.Predef..MODULE$.wrapRefArray( (Object[]) columns ), physicalPlan.
            logicalPlan(), physicalPlan.slotConfigurations(), physicalPlan.parameterMapping(), context.config().lenientCreateRelationship(), context.config().memoryTrackingController(), query.hasLoadCSV())
            ;
            if ( this.ENABLE_DEBUG_PRINTS() )
            {
                if ( !this.PRINT_PLAN_INFO_EARLY() )
                {
                    this.printPlanInfo( query );
                    this.printRewrittenPlanInfo( physicalPlan.logicalPlan() );
                }

                this.printPipe( physicalPlan.slotConfigurations(), pipe );
            }

            Seq metadata = CodeGenPlanDescriptionHelper$.MODULE$.metadata( codeGenerationMode.saver() );
            return new InterpretedExecutionPlan( query.periodicCommitInfo(), resultBuilderFactory, org.neo4j.cypher.internal.SlottedRuntimeName..MODULE$,
            query.readOnly(), metadata);
        }
        catch ( CypherException var21 )
        {
            if ( this.ENABLE_DEBUG_PRINTS() )
            {
                this.printFailureStackTrace( var21 );
                if ( !this.PRINT_PLAN_INFO_EARLY() )
                {
                    this.printPlanInfo( query );
                }
            }

            throw var21;
        }
    }
}
