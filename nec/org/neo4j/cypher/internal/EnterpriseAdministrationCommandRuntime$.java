package org.neo4j.cypher.internal;

import org.neo4j.common.DependencyResolver;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class EnterpriseAdministrationCommandRuntime$ extends AbstractFunction2<ExecutionEngine,DependencyResolver,EnterpriseAdministrationCommandRuntime>
        implements Serializable
{
    public static EnterpriseAdministrationCommandRuntime$ MODULE$;

    static
    {
        new EnterpriseAdministrationCommandRuntime$();
    }

    private EnterpriseAdministrationCommandRuntime$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "EnterpriseAdministrationCommandRuntime";
    }

    public EnterpriseAdministrationCommandRuntime apply( final ExecutionEngine normalExecutionEngine, final DependencyResolver resolver )
    {
        return new EnterpriseAdministrationCommandRuntime( normalExecutionEngine, resolver );
    }

    public Option<Tuple2<ExecutionEngine,DependencyResolver>> unapply( final EnterpriseAdministrationCommandRuntime x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.normalExecutionEngine(), x$0.resolver() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
