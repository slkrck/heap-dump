package org.neo4j.cypher.internal;

import org.neo4j.codegen.api.CodeGeneration.CodeSaver;
import org.neo4j.cypher.internal.plandescription.Argument;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class CodeGenPlanDescriptionHelper
{
    public static Seq<Argument> metadata( final CodeSaver saver )
    {
        return CodeGenPlanDescriptionHelper$.MODULE$.metadata( var0 );
    }
}
