package org.neo4j.cypher.internal;

import java.util.Optional;

import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.cypher.CypherInterpretedPipesFallbackOption;
import org.neo4j.cypher.CypherOperatorEngineOption;
import org.neo4j.cypher.internal.compiler.CodeGenerationFailedNotification;
import org.neo4j.cypher.internal.compiler.ExperimentalFeatureNotification;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition;
import org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy;
import org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy$;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanner$;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes;
import org.neo4j.cypher.internal.physicalplanning.PipelineBuilder$;
import org.neo4j.cypher.internal.plandescription.Argument;
import org.neo4j.cypher.internal.runtime.ExecutionMode;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.MemoryTracking;
import org.neo4j.cypher.internal.runtime.MemoryTrackingController;
import org.neo4j.cypher.internal.runtime.ParameterMapping;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryIndexRegistrator;
import org.neo4j.cypher.internal.runtime.QueryIndexes;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.QueryStatistics;
import org.neo4j.cypher.internal.runtime.ThreadSafeResourceManager;
import org.neo4j.cypher.internal.runtime.debug.DebugLog.;
import org.neo4j.cypher.internal.runtime.interpreted.InterpretedPipeMapper;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.CommunityExpressionConverter;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverter;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline;
import org.neo4j.cypher.internal.runtime.pipelined.FuseOperators;
import org.neo4j.cypher.internal.runtime.pipelined.InterpretedPipesFallbackPolicy;
import org.neo4j.cypher.internal.runtime.pipelined.InterpretedPipesFallbackPolicy$;
import org.neo4j.cypher.internal.runtime.pipelined.OperatorFactory;
import org.neo4j.cypher.internal.runtime.pipelined.PipelinedPipelineBreakingPolicy;
import org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutionGraphSchedulingPolicy;
import org.neo4j.cypher.internal.runtime.pipelined.execution.LazyScheduling$;
import org.neo4j.cypher.internal.runtime.pipelined.execution.ProfiledQuerySubscription;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor;
import org.neo4j.cypher.internal.runtime.pipelined.expressions.PipelinedBlacklist$;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer;
import org.neo4j.cypher.internal.runtime.slotted.SlottedPipeMapper;
import org.neo4j.cypher.internal.runtime.slotted.expressions.CompiledExpressionConverter;
import org.neo4j.cypher.internal.runtime.slotted.expressions.CompiledExpressionConverter$;
import org.neo4j.cypher.internal.runtime.slotted.expressions.SlottedExpressionConverters;
import org.neo4j.cypher.internal.v4_0.util.Cardinality;
import org.neo4j.cypher.internal.v4_0.util.CypherException;
import org.neo4j.cypher.internal.v4_0.util.InternalNotification;
import org.neo4j.cypher.result.QueryProfile;
import org.neo4j.cypher.result.RuntimeResult;
import org.neo4j.cypher.result.RuntimeResult.ConsumptionState;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.kernel.impl.query.QuerySubscription;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.MapValue;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class PipelinedRuntime implements CypherRuntime<EnterpriseRuntimeContext>, DebugPrettyPrinter
{
    public final boolean org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution;
    private final String name;
    private final RuntimeName org$neo4j$cypher$internal$PipelinedRuntime$$runtimeName;
    private final boolean ENABLE_DEBUG_PRINTS;
    private final boolean PRINT_PLAN_INFO_EARLY;
    private final boolean PRINT_QUERY_TEXT;
    private final boolean PRINT_LOGICAL_PLAN;
    private final boolean PRINT_REWRITTEN_LOGICAL_PLAN;
    private final boolean PRINT_PIPELINE_INFO;
    private final boolean PRINT_FAILURE_STACK_TRACE;

    public PipelinedRuntime( final boolean parallelExecution, final String name )
    {
        this.org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution = parallelExecution;
        this.name = name;
        DebugPrettyPrinter.$init$( this );
        this.org$neo4j$cypher$internal$PipelinedRuntime$$runtimeName = org.neo4j.cypher.internal.RuntimeName..MODULE$.apply( name );
        this.ENABLE_DEBUG_PRINTS = false;
        this.PRINT_PLAN_INFO_EARLY = false;
        this.PRINT_QUERY_TEXT = true;
        this.PRINT_LOGICAL_PLAN = true;
        this.PRINT_REWRITTEN_LOGICAL_PLAN = true;
        this.PRINT_PIPELINE_INFO = false;
        this.PRINT_FAILURE_STACK_TRACE = true;
    }

    public static String CODE_GEN_FAILED_MESSAGE()
    {
        return PipelinedRuntime$.MODULE$.CODE_GEN_FAILED_MESSAGE();
    }

    public static PipelinedRuntime PARALLEL()
    {
        return PipelinedRuntime$.MODULE$.PARALLEL();
    }

    public static PipelinedRuntime PIPELINED()
    {
        return PipelinedRuntime$.MODULE$.PIPELINED();
    }

    public void printPlanInfo( final LogicalQuery logicalQuery )
    {
        DebugPrettyPrinter.printPlanInfo$( this, logicalQuery );
    }

    public void printRewrittenPlanInfo( final LogicalPlan logicalPlan )
    {
        DebugPrettyPrinter.printRewrittenPlanInfo$( this, logicalPlan );
    }

    public void printPipe( final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations, final Pipe pipe )
    {
        DebugPrettyPrinter.printPipe$( this, slotConfigurations, pipe );
    }

    public void printFailureStackTrace( final CypherException e )
    {
        DebugPrettyPrinter.printFailureStackTrace$( this, e );
    }

    public void prettyPrintPipelines( final PhysicalPlanningAttributes.SlotConfigurations pipelines )
    {
        DebugPrettyPrinter.prettyPrintPipelines$( this, pipelines );
    }

    public void prettyPrintPipe( final Pipe pipe )
    {
        DebugPrettyPrinter.prettyPrintPipe$( this, pipe );
    }

    public Pipe printPipe$default$2()
    {
        return DebugPrettyPrinter.printPipe$default$2$( this );
    }

    public SecurityContext compileToExecutable$default$3()
    {
        return CypherRuntime.compileToExecutable$default$3$( this );
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_QUERY_TEXT_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_LOGICAL_PLAN_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_REWRITTEN_LOGICAL_PLAN_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_PIPELINE_INFO_$eq( final boolean x$1 )
    {
    }

    public void org$neo4j$cypher$internal$DebugPrettyPrinter$_setter_$PRINT_FAILURE_STACK_TRACE_$eq( final boolean x$1 )
    {
    }

    public String name()
    {
        return this.name;
    }

    public RuntimeName org$neo4j$cypher$internal$PipelinedRuntime$$runtimeName()
    {
        return this.org$neo4j$cypher$internal$PipelinedRuntime$$runtimeName;
    }

    public boolean ENABLE_DEBUG_PRINTS()
    {
        return this.ENABLE_DEBUG_PRINTS;
    }

    public boolean PRINT_PLAN_INFO_EARLY()
    {
        return this.PRINT_PLAN_INFO_EARLY;
    }

    public boolean PRINT_QUERY_TEXT()
    {
        return this.PRINT_QUERY_TEXT;
    }

    public boolean PRINT_LOGICAL_PLAN()
    {
        return this.PRINT_LOGICAL_PLAN;
    }

    public boolean PRINT_REWRITTEN_LOGICAL_PLAN()
    {
        return this.PRINT_REWRITTEN_LOGICAL_PLAN;
    }

    public boolean PRINT_PIPELINE_INFO()
    {
        return this.PRINT_PIPELINE_INFO;
    }

    public boolean PRINT_FAILURE_STACK_TRACE()
    {
        return this.PRINT_FAILURE_STACK_TRACE;
    }

    public ExecutionPlan compileToExecutable( final LogicalQuery query, final EnterpriseRuntimeContext context, final SecurityContext securityContext )
    {
      .MODULE$.log( "PipelinedRuntime.compileToExecutable()" );
        if ( this.ENABLE_DEBUG_PRINTS() && this.PRINT_PLAN_INFO_EARLY() )
        {
            this.printPlanInfo( query );
        }

        try
        {
            if ( query.periodicCommitInfo().isDefined() )
            {
                throw new CantCompileQueryException( "Periodic commit is not supported by Pipelined runtime" );
            }
            else
            {
                boolean var12;
                label48:
                {
                    label47:
                    {
                        CypherOperatorEngineOption var10000 = context.operatorEngine();
                        org.neo4j.cypher.CypherOperatorEngineOption.compiled.var6 = org.neo4j.cypher.CypherOperatorEngineOption.compiled..MODULE$;
                        if ( var10000 == null )
                        {
                            if ( var6 == null )
                            {
                                break label47;
                            }
                        }
                        else if ( var10000.equals( var6 ) )
                        {
                            break label47;
                        }

                        var12 = false;
                        break label48;
                    }

                    var12 = true;
                }

                boolean shouldFuseOperators = var12;
                OperatorFusionPolicy operatorFusionPolicy =
                        OperatorFusionPolicy$.MODULE$.apply( shouldFuseOperators, !this.org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution );
                return this.compilePlan( operatorFusionPolicy, query, context, new QueryIndexRegistrator( context.schemaRead() ),
                        scala.Predef..MODULE$.Set().empty());
            }
        }
        catch ( Throwable var11 )
        {
            if ( var11 instanceof CypherException )
            {
                CypherException var10 = (CypherException) var11;
                if ( this.ENABLE_DEBUG_PRINTS() )
                {
                    this.printFailureStackTrace( var10 );
                    if ( !this.PRINT_PLAN_INFO_EARLY() )
                    {
                        this.printPlanInfo( query );
                    }

                    throw var10;
                }
            }

            throw var11;
        }
    }

    private int selectBatchSize( final LogicalQuery query, final EnterpriseRuntimeContext context )
    {
        Cardinality maxCardinality = (Cardinality) ((TraversableOnce) query.logicalPlan().flatten().map( ( plan ) -> {
            return (Cardinality) query.cardinalities().get( plan.id() );
        }, scala.collection.Seq..MODULE$.canBuildFrom())).max( scala.math.Ordering..MODULE$.ordered( scala.Predef..MODULE$.$conforms()));
        int batchSize = (long) maxCardinality.amount() > (long) context.config().pipelinedBatchSizeBig() ? context.config().pipelinedBatchSizeBig()
                                                                                                         : context.config().pipelinedBatchSizeSmall();
        return batchSize;
    }

    private PipelinedRuntime.PipelinedExecutionPlan compilePlan( final OperatorFusionPolicy operatorFusionPolicy, final LogicalQuery query,
            final EnterpriseRuntimeContext context, final QueryIndexRegistrator queryIndexRegistrator, final Set<InternalNotification> warnings )
    {
        InterpretedPipesFallbackPolicy interpretedPipesFallbackPolicy = InterpretedPipesFallbackPolicy$.MODULE$.apply( context.interpretedPipesFallback(),
                this.org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution );
        PipelinedPipelineBreakingPolicy breakingPolicy = new PipelinedPipelineBreakingPolicy( operatorFusionPolicy, interpretedPipesFallbackPolicy );
        PhysicalPlan physicalPlan = PhysicalPlanner$.MODULE$.plan( context.tokenContext(), query.logicalPlan(), query.semanticTable(), breakingPolicy, true );
        if ( this.ENABLE_DEBUG_PRINTS() && this.PRINT_PLAN_INFO_EARLY() )
        {
            this.printRewrittenPlanInfo( physicalPlan.logicalPlan() );
        }

        CodeGenerationMode codeGenerationMode;
        ExpressionConverters converters;
        Object var32;
        label64:
        {
            label63:
            {
                codeGenerationMode = org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode..MODULE$.fromDebugOptions( context.debugOptions() );
                converters = context.compileExpressions() ? new ExpressionConverters( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new ExpressionConverter[]{
                        new CompiledExpressionConverter( context.log(), physicalPlan, context.tokenContext(), query.readOnly(), codeGenerationMode,
                                CompiledExpressionConverter$.MODULE$.$lessinit$greater$default$6() ), new SlottedExpressionConverters( physicalPlan ),
                        new CommunityExpressionConverter( context.tokenContext() )}) )) :new ExpressionConverters( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new ExpressionConverter[]{new SlottedExpressionConverters( physicalPlan ),
                            new CommunityExpressionConverter( context.tokenContext() )}) ));
                PipelinedBlacklist$.MODULE$.throwOnUnsupportedPlan( query.logicalPlan(), this.org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution,
                        query.providedOrders() );
                CypherInterpretedPipesFallbackOption var10000 = context.interpretedPipesFallback();
                org.neo4j.cypher.CypherInterpretedPipesFallbackOption.disabled.var13 = org.neo4j.cypher.CypherInterpretedPipesFallbackOption.disabled..MODULE$;
                if ( var10000 == null )
                {
                    if ( var13 != null )
                    {
                        break label63;
                    }
                }
                else if ( !var10000.equals( var13 ) )
                {
                    break label63;
                }

                var32 = scala.None..MODULE$;
                break label64;
            }

            InterpretedPipeMapper slottedPipeBuilderFallback =
                    new InterpretedPipeMapper( query.readOnly(), converters, context.tokenContext(), queryIndexRegistrator, query.semanticTable() );
            var32 = new Some( new SlottedPipeMapper( slottedPipeBuilderFallback, converters, physicalPlan, query.readOnly(), queryIndexRegistrator,
                    query.semanticTable() ) );
        }

        Option slottedPipeBuilder = var32;
      .MODULE$.logDiff( "PhysicalPlanner.plan" );
        ExecutionGraphDefinition executionGraphDefinition = PipelineBuilder$.MODULE$.build( breakingPolicy, operatorFusionPolicy, physicalPlan );
        boolean readOnly = interpretedPipesFallbackPolicy.readOnly();
        OperatorFactory operatorFactory = new OperatorFactory( executionGraphDefinition, converters, readOnly, queryIndexRegistrator, query.semanticTable(),
                interpretedPipesFallbackPolicy, (Option) slottedPipeBuilder );
      .MODULE$.logDiff( "PipelineBuilder" );
        ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy = LazyScheduling$.MODULE$.executionGraphSchedulingPolicy( executionGraphDefinition );
      .MODULE$.logDiff( "Scheduling" );
        FuseOperators fuseOperators =
                new FuseOperators( operatorFactory, context.tokenContext(), this.org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution,
                        codeGenerationMode );

        PipelinedRuntime.PipelinedExecutionPlan var33;
        try
        {
            IndexedSeq executablePipelines = fuseOperators.compilePipelines( executionGraphDefinition );
         .MODULE$.logDiff( "FuseOperators" );
            QueryExecutor executor = context.runtimeEnvironment().getQueryExecutor( this.org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution );
            int batchSize = this.selectBatchSize( query, context );
            if ( this.org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution )
            {
                ResourceManagerFactory resourceManagerFactory = ( x$1 ) -> {
                    return new ThreadSafeResourceManager( x$1 );
                };
                var32 = new Some( new Tuple2( context.runtimeEnvironment().cursors(), resourceManagerFactory ) );
            }
            else
            {
                var32 = scala.None..MODULE$;
            }

            Option maybeThreadSafeExecutionResources = var32;
            Seq metadata = CodeGenPlanDescriptionHelper$.MODULE$.metadata( codeGenerationMode.saver() );
            if ( this.ENABLE_DEBUG_PRINTS() )
            {
                if ( !this.PRINT_PLAN_INFO_EARLY() )
                {
                    this.printPlanInfo( query );
                    this.printRewrittenPlanInfo( physicalPlan.logicalPlan() );
                }

                this.printPipe( physicalPlan.slotConfigurations(), this.printPipe$default$2() );
            }

            var33 = new PipelinedRuntime.PipelinedExecutionPlan( this, executablePipelines, executionGraphDefinition, queryIndexRegistrator.result(),
                    physicalPlan.nExpressionSlots(), physicalPlan.logicalPlan(), physicalPlan.parameterMapping(), query.resultColumns(), executor,
                    context.runtimeEnvironment().tracer(), batchSize, context.config().memoryTrackingController(), (Option) maybeThreadSafeExecutionResources,
                    metadata, warnings, executionGraphSchedulingPolicy );
        }
        catch ( Throwable var31 )
        {
            if ( var31 instanceof Exception )
            {
                Exception var28 = (Exception) var31;
                if ( operatorFusionPolicy.fusionEnabled() )
                {
                    context.log().debug( PipelinedRuntime$.MODULE$.CODE_GEN_FAILED_MESSAGE(), var28 );
               .MODULE$.log( "Could not compile pipeline because of %s", var28 );
                    CodeGenerationFailedNotification warning = new CodeGenerationFailedNotification( var28.getMessage() );
                    OperatorFusionPolicy nextOperatorFusionPolicy =
                            operatorFusionPolicy.fusionOverPipelineEnabled() ? OperatorFusionPolicy$.MODULE$.apply( true, false )
                                                                             : OperatorFusionPolicy$.MODULE$.apply( false, false );
                    PipelinedRuntime.PipelinedExecutionPlan var6 =
                            this.compilePlan( nextOperatorFusionPolicy, query, context, queryIndexRegistrator, (Set) warnings.$plus( warning ) );
                    var33 = var6;
                    return var33;
                }
            }

            throw var31;
        }

        return var33;
    }

    public class PipelinedExecutionPlan extends ExecutionPlan
    {
        private final IndexedSeq<ExecutablePipeline> executablePipelines;
        private final ExecutionGraphDefinition executionGraphDefinition;
        private final QueryIndexes queryIndexes;
        private final int nExpressionSlots;
        private final LogicalPlan logicalPlan;
        private final ParameterMapping parameterMapping;
        private final String[] fieldNames;
        private final QueryExecutor queryExecutor;
        private final SchedulerTracer schedulerTracer;
        private final int batchSize;
        private final MemoryTrackingController memoryTrackingController;
        private final Option<Tuple2<CursorFactory,ResourceManagerFactory>> maybeThreadSafeExecutionResources;
        private final Seq<Argument> metadata;
        private final Set<InternalNotification> warnings;
        private final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy;

        public PipelinedExecutionPlan( final PipelinedRuntime $outer, final IndexedSeq<ExecutablePipeline> executablePipelines,
                final ExecutionGraphDefinition executionGraphDefinition, final QueryIndexes queryIndexes, final int nExpressionSlots,
                final LogicalPlan logicalPlan, final ParameterMapping parameterMapping, final String[] fieldNames, final QueryExecutor queryExecutor,
                final SchedulerTracer schedulerTracer, final int batchSize, final MemoryTrackingController memoryTrackingController,
                final Option<Tuple2<CursorFactory,ResourceManagerFactory>> maybeThreadSafeExecutionResources, final Seq<Argument> metadata,
                final Set<InternalNotification> warnings, final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy )
        {
            this.executablePipelines = executablePipelines;
            this.executionGraphDefinition = executionGraphDefinition;
            this.queryIndexes = queryIndexes;
            this.nExpressionSlots = nExpressionSlots;
            this.logicalPlan = logicalPlan;
            this.parameterMapping = parameterMapping;
            this.fieldNames = fieldNames;
            this.queryExecutor = queryExecutor;
            this.schedulerTracer = schedulerTracer;
            this.batchSize = batchSize;
            this.memoryTrackingController = memoryTrackingController;
            this.maybeThreadSafeExecutionResources = maybeThreadSafeExecutionResources;
            this.metadata = metadata;
            this.warnings = warnings;
            this.executionGraphSchedulingPolicy = executionGraphSchedulingPolicy;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public Seq<Argument> metadata()
        {
            return this.metadata;
        }

        public RuntimeResult run( final QueryContext queryContext, final ExecutionMode executionMode, final MapValue params, final boolean prePopulateResults,
                final InputDataStream inputDataStream, final QuerySubscriber subscriber )
        {
            boolean var10016;
            PipelinedRuntime.PipelinedRuntimeResult var10000;
            PipelinedRuntime var10002;
            IndexedSeq var10003;
            ExecutionGraphDefinition var10004;
            IndexReadSession[] var10005;
            int var10006;
            LogicalPlan var10009;
            AnyValue[] var10011;
            String[] var10012;
            QueryExecutor var10013;
            SchedulerTracer var10014;
            label17:
            {
                label16:
                {
                    var10000 = new PipelinedRuntime.PipelinedRuntimeResult;
                    var10002 = this.org$neo4j$cypher$internal$PipelinedRuntime$PipelinedExecutionPlan$$$outer();
                    var10003 = this.executablePipelines;
                    var10004 = this.executionGraphDefinition;
                    var10005 = this.queryIndexes.initiateLabelAndSchemaIndexes( queryContext );
                    var10006 = this.nExpressionSlots;
                    var10009 = this.logicalPlan;
                    var10011 = org.neo4j.cypher.internal.runtime.createParameterArray..MODULE$.apply( params, this.parameterMapping );
                    var10012 = this.fieldNames;
                    var10013 = this.queryExecutor;
                    var10014 = this.schedulerTracer;
                    org.neo4j.cypher.internal.runtime.ProfileMode.var7 = org.neo4j.cypher.internal.runtime.ProfileMode..MODULE$;
                    if ( executionMode == null )
                    {
                        if ( var7 == null )
                        {
                            break label16;
                        }
                    }
                    else if ( executionMode.equals( var7 ) )
                    {
                        break label16;
                    }

                    var10016 = false;
                    break label17;
                }

                var10016 = true;
            }

            var10000.<init>
            (var10002, var10003, var10004, var10005, var10006, prePopulateResults, inputDataStream, var10009, queryContext, var10011, var10012, var10013, var10014, subscriber, var10016, this.batchSize, this.memoryTrackingController.memoryTracking(), this.executionGraphSchedulingPolicy)
            ;
            return var10000;
        }

        public RuntimeName runtimeName()
        {
            return this.org$neo4j$cypher$internal$PipelinedRuntime$PipelinedExecutionPlan$$$outer().org$neo4j$cypher$internal$PipelinedRuntime$$runtimeName();
        }

        public Set<InternalNotification> notifications()
        {
            return this.org$neo4j$cypher$internal$PipelinedRuntime$PipelinedExecutionPlan$$$outer().org$neo4j$cypher$internal$PipelinedRuntime$$parallelExecution
                   ? (Set) this.warnings.$plus( new ExperimentalFeatureNotification(
                    "The parallel runtime is experimental and might suffer from instability and potentially correctness issues." ) ) : this.warnings;
        }

        public Option<Tuple2<CursorFactory,ResourceManagerFactory>> threadSafeExecutionResources()
        {
            return this.maybeThreadSafeExecutionResources;
        }
    }

    public class PipelinedRuntimeResult implements RuntimeResult
    {
        private final IndexedSeq<ExecutablePipeline> executablePipelines;
        private final ExecutionGraphDefinition executionGraphDefinition;
        private final IndexReadSession[] queryIndexes;
        private final int nExpressionSlots;
        private final boolean prePopulateResults;
        private final InputDataStream inputDataStream;
        private final QueryContext queryContext;
        private final AnyValue[] params;
        private final String[] fieldNames;
        private final QueryExecutor queryExecutor;
        private final SchedulerTracer schedulerTracer;
        private final QuerySubscriber subscriber;
        private final boolean doProfile;
        private final int batchSize;
        private final MemoryTracking memoryTracking;
        private final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy;
        private QuerySubscription querySubscription;
        private QueryProfile _queryProfile;
        private QueryMemoryTracker _memoryTracker;

        public PipelinedRuntimeResult( final PipelinedRuntime $outer, final IndexedSeq<ExecutablePipeline> executablePipelines,
                final ExecutionGraphDefinition executionGraphDefinition, final IndexReadSession[] queryIndexes, final int nExpressionSlots,
                final boolean prePopulateResults, final InputDataStream inputDataStream, final LogicalPlan logicalPlan, final QueryContext queryContext,
                final AnyValue[] params, final String[] fieldNames, final QueryExecutor queryExecutor, final SchedulerTracer schedulerTracer,
                final QuerySubscriber subscriber, final boolean doProfile, final int batchSize, final MemoryTracking memoryTracking,
                final ExecutionGraphSchedulingPolicy executionGraphSchedulingPolicy )
        {
            this.executablePipelines = executablePipelines;
            this.executionGraphDefinition = executionGraphDefinition;
            this.queryIndexes = queryIndexes;
            this.nExpressionSlots = nExpressionSlots;
            this.prePopulateResults = prePopulateResults;
            this.inputDataStream = inputDataStream;
            this.queryContext = queryContext;
            this.params = params;
            this.fieldNames = fieldNames;
            this.queryExecutor = queryExecutor;
            this.schedulerTracer = schedulerTracer;
            this.subscriber = subscriber;
            this.doProfile = doProfile;
            this.batchSize = batchSize;
            this.memoryTracking = memoryTracking;
            this.executionGraphSchedulingPolicy = executionGraphSchedulingPolicy;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public String[] fieldNames()
        {
            return this.fieldNames;
        }

        private QuerySubscription querySubscription()
        {
            return this.querySubscription;
        }

        private void querySubscription_$eq( final QuerySubscription x$1 )
        {
            this.querySubscription = x$1;
        }

        private QueryProfile _queryProfile()
        {
            return this._queryProfile;
        }

        private void _queryProfile_$eq( final QueryProfile x$1 )
        {
            this._queryProfile = x$1;
        }

        private QueryMemoryTracker _memoryTracker()
        {
            return this._memoryTracker;
        }

        private void _memoryTracker_$eq( final QueryMemoryTracker x$1 )
        {
            this._memoryTracker = x$1;
        }

        public QueryStatistics queryStatistics()
        {
            return (QueryStatistics) this.queryContext.getOptStatistics().getOrElse( () -> {
                return new QueryStatistics( org.neo4j.cypher.internal.runtime.QueryStatistics..MODULE$.apply$default$1(),
                org.neo4j.cypher.internal.runtime.QueryStatistics..MODULE$.apply$default$2(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$3(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$4(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$5(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$6(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$7(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$8(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$9(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$10(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$11(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$12(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$13(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$14(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$15(), org.neo4j.cypher.internal.runtime.QueryStatistics..
                MODULE$.apply$default$16(), org.neo4j.cypher.internal.runtime.QueryStatistics..MODULE$.apply$default$17());
            } );
        }

        public Optional<Long> totalAllocatedMemory()
        {
            this.ensureQuerySubscription();
            return this._memoryTracker().totalAllocatedMemory();
        }

        public ConsumptionState consumptionState()
        {
            return this.querySubscription() == null ? ConsumptionState.NOT_STARTED : ConsumptionState.EXHAUSTED;
        }

        public void close()
        {
        }

        public QueryProfile queryProfile()
        {
            return this._queryProfile();
        }

        public void request( final long numberOfRecords )
        {
            this.ensureQuerySubscription();
            this.querySubscription().request( numberOfRecords );
        }

        public void cancel()
        {
            this.ensureQuerySubscription();
            this.querySubscription().cancel();
        }

        public boolean await()
        {
            this.ensureQuerySubscription();
            return this.querySubscription().await();
        }

        private void ensureQuerySubscription()
        {
            if ( this.querySubscription() == null )
            {
                this.subscriber.onResult( this.fieldNames().length );
                ProfiledQuerySubscription var3 =
                        this.queryExecutor.execute( this.executablePipelines, this.executionGraphDefinition, this.inputDataStream, this.queryContext,
                                this.params, this.schedulerTracer, this.queryIndexes, this.nExpressionSlots, this.prePopulateResults, this.subscriber,
                                this.doProfile, this.batchSize, this.memoryTracking, this.executionGraphSchedulingPolicy );
                if ( var3 == null )
                {
                    throw new MatchError( var3 );
                }

                QuerySubscription subx = var3.subscription();
                QueryProfile profx = var3.profile();
                QueryMemoryTracker memTrack = var3.memoryTracker();
                Tuple3 var1 = new Tuple3( subx, profx, memTrack );
                QuerySubscription sub = (QuerySubscription) var1._1();
                QueryProfile prof = (QueryProfile) var1._2();
                QueryMemoryTracker memTrackx = (QueryMemoryTracker) var1._3();
                this.querySubscription_$eq( sub );
                this._queryProfile_$eq( prof );
                this._memoryTracker_$eq( memTrackx );
            }
        }
    }
}
