package org.neo4j.cypher.internal;

import java.time.Clock;

import org.neo4j.cypher.CypherInterpretedPipesFallbackOption;
import org.neo4j.cypher.CypherOperatorEngineOption;
import org.neo4j.cypher.internal.executionplan.GeneratedQuery;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure;
import org.neo4j.internal.kernel.api.SchemaRead;
import org.neo4j.logging.Log;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class EnterpriseRuntimeContextManager implements RuntimeContextManager<EnterpriseRuntimeContext>, Product, Serializable
{
    private final CodeStructure<GeneratedQuery> codeStructure;
    private final Log log;
    private final CypherRuntimeConfiguration config;
    private final RuntimeEnvironment runtimeEnvironment;

    public EnterpriseRuntimeContextManager( final CodeStructure<GeneratedQuery> codeStructure, final Log log, final CypherRuntimeConfiguration config,
            final RuntimeEnvironment runtimeEnvironment )
    {
        this.codeStructure = codeStructure;
        this.log = log;
        this.config = config;
        this.runtimeEnvironment = runtimeEnvironment;
        Product.$init$( this );
    }

    public static Option<Tuple4<CodeStructure<GeneratedQuery>,Log,CypherRuntimeConfiguration,RuntimeEnvironment>> unapply(
            final EnterpriseRuntimeContextManager x$0 )
    {
        return EnterpriseRuntimeContextManager$.MODULE$.unapply( var0 );
    }

    public static EnterpriseRuntimeContextManager apply( final CodeStructure<GeneratedQuery> codeStructure, final Log log,
            final CypherRuntimeConfiguration config, final RuntimeEnvironment runtimeEnvironment )
    {
        return EnterpriseRuntimeContextManager$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<CodeStructure<GeneratedQuery>,Log,CypherRuntimeConfiguration,RuntimeEnvironment>,EnterpriseRuntimeContextManager> tupled()
    {
        return EnterpriseRuntimeContextManager$.MODULE$.tupled();
    }

    public static Function1<CodeStructure<GeneratedQuery>,Function1<Log,Function1<CypherRuntimeConfiguration,Function1<RuntimeEnvironment,EnterpriseRuntimeContextManager>>>> curried()
    {
        return EnterpriseRuntimeContextManager$.MODULE$.curried();
    }

    public CodeStructure<GeneratedQuery> codeStructure()
    {
        return this.codeStructure;
    }

    public Log log()
    {
        return this.log;
    }

    public CypherRuntimeConfiguration config()
    {
        return this.config;
    }

    public RuntimeEnvironment runtimeEnvironment()
    {
        return this.runtimeEnvironment;
    }

    public EnterpriseRuntimeContext create( final TokenContext tokenContext, final SchemaRead schemaRead, final Clock clock, final Set<String> debugOptions,
            final boolean compileExpressions, final boolean materializedEntitiesMode, final CypherOperatorEngineOption operatorEngine,
            final CypherInterpretedPipesFallbackOption interpretedPipesFallback )
    {
        return new EnterpriseRuntimeContext( tokenContext, schemaRead, this.codeStructure(), this.log(), clock, debugOptions, this.config(),
                this.runtimeEnvironment(), compileExpressions, materializedEntitiesMode, operatorEngine, interpretedPipesFallback );
    }

    public void assertAllReleased()
    {
        this.runtimeEnvironment().getQueryExecutor( true ).assertAllReleased();
    }

    public EnterpriseRuntimeContextManager copy( final CodeStructure<GeneratedQuery> codeStructure, final Log log, final CypherRuntimeConfiguration config,
            final RuntimeEnvironment runtimeEnvironment )
    {
        return new EnterpriseRuntimeContextManager( codeStructure, log, config, runtimeEnvironment );
    }

    public CodeStructure<GeneratedQuery> copy$default$1()
    {
        return this.codeStructure();
    }

    public Log copy$default$2()
    {
        return this.log();
    }

    public CypherRuntimeConfiguration copy$default$3()
    {
        return this.config();
    }

    public RuntimeEnvironment copy$default$4()
    {
        return this.runtimeEnvironment();
    }

    public String productPrefix()
    {
        return "EnterpriseRuntimeContextManager";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.codeStructure();
            break;
        case 1:
            var10000 = this.log();
            break;
        case 2:
            var10000 = this.config();
            break;
        case 3:
            var10000 = this.runtimeEnvironment();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EnterpriseRuntimeContextManager;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof EnterpriseRuntimeContextManager )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            EnterpriseRuntimeContextManager var4 = (EnterpriseRuntimeContextManager) x$1;
                            CodeStructure var10000 = this.codeStructure();
                            CodeStructure var5 = var4.codeStructure();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            Log var9 = this.log();
                            Log var6 = var4.log();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            CypherRuntimeConfiguration var10 = this.config();
                            CypherRuntimeConfiguration var7 = var4.config();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            RuntimeEnvironment var11 = this.runtimeEnvironment();
                            RuntimeEnvironment var8 = var4.runtimeEnvironment();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
