package org.neo4j.cypher.internal;

import org.neo4j.cypher.internal.runtime.pipelined.WorkerManagement;
import org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor;
import org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.scheduler.JobScheduler;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class RuntimeEnvironment
{
    private final QueryExecutor pipelinedQueryExecutor;
    private final QueryExecutor parallelQueryExecutor;
    private final SchedulerTracer tracer;
    private final CursorFactory cursors;

    public RuntimeEnvironment( final CypherRuntimeConfiguration config, final QueryExecutor pipelinedQueryExecutor, final QueryExecutor parallelQueryExecutor,
            final SchedulerTracer tracer, final CursorFactory cursors )
    {
        this.pipelinedQueryExecutor = pipelinedQueryExecutor;
        this.parallelQueryExecutor = parallelQueryExecutor;
        this.tracer = tracer;
        this.cursors = cursors;
    }

    public static SchedulerTracer createTracer( final CypherRuntimeConfiguration config, final JobScheduler jobScheduler, final LifeSupport lifeSupport )
    {
        return RuntimeEnvironment$.MODULE$.createTracer( var0, var1, var2 );
    }

    public static RuntimeEnvironment of( final CypherRuntimeConfiguration config, final JobScheduler jobScheduler, final CursorFactory cursors,
            final LifeSupport lifeSupport, final WorkerManagement workerManager )
    {
        return RuntimeEnvironment$.MODULE$.of( var0, var1, var2, var3, var4 );
    }

    public SchedulerTracer tracer()
    {
        return this.tracer;
    }

    public CursorFactory cursors()
    {
        return this.cursors;
    }

    public QueryExecutor getQueryExecutor( final boolean parallelExecution )
    {
        return parallelExecution ? this.parallelQueryExecutor : this.pipelinedQueryExecutor;
    }
}
