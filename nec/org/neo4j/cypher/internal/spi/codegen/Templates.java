package org.neo4j.cypher.internal.spi.codegen;

import org.neo4j.codegen.ClassGenerator;
import org.neo4j.codegen.ClassHandle;
import org.neo4j.codegen.CodeBlock;
import org.neo4j.codegen.Expression;
import org.neo4j.codegen.MethodTemplate;
import org.neo4j.codegen.Parameter;
import org.neo4j.codegen.TypeReference;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Namer;
import scala.Function1;
import scala.Tuple2;
import scala.collection.Seq;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public final class Templates
{
    public static MethodTemplate FIELD_NAMES()
    {
        return Templates$.MODULE$.FIELD_NAMES();
    }

    public static void propertyCursor( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.propertyCursor( var0, var1 );
    }

    public static void closeCursors( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.closeCursors( var0, var1 );
    }

    public static void relationshipScanCursor( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.relationshipScanCursor( var0, var1 );
    }

    public static void nodeCursor( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.nodeCursor( var0, var1 );
    }

    public static void getOrLoadSchemaRead( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.getOrLoadSchemaRead( var0, var1 );
    }

    public static void getOrLoadTokenRead( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.getOrLoadTokenRead( var0, var1 );
    }

    public static void getOrLoadDataRead( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.getOrLoadDataRead( var0, var1 );
    }

    public static void getOrLoadCursors( final ClassGenerator clazz, final Fields fields )
    {
        Templates$.MODULE$.getOrLoadCursors( var0, var1 );
    }

    public static MethodTemplate constructor( final ClassHandle classHandle )
    {
        return Templates$.MODULE$.constructor( var0 );
    }

    public static Expression anyValueComparator()
    {
        return Templates$.MODULE$.anyValueComparator();
    }

    public static Expression valueComparator()
    {
        return Templates$.MODULE$.valueComparator();
    }

    public static Expression newRelationshipDataExtractor()
    {
        return Templates$.MODULE$.newRelationshipDataExtractor();
    }

    public static Expression newResultRow()
    {
        return Templates$.MODULE$.newResultRow();
    }

    public static Expression both()
    {
        return Templates$.MODULE$.both();
    }

    public static Expression outgoing()
    {
        return Templates$.MODULE$.outgoing();
    }

    public static Expression incoming()
    {
        return Templates$.MODULE$.incoming();
    }

    public static Expression noValue()
    {
        return Templates$.MODULE$.noValue();
    }

    public static void tryCatch( final CodeBlock generate, final Function1<CodeBlock,BoxedUnit> tryBlock, final Parameter exception,
            final Function1<CodeBlock,BoxedUnit> catchBlock )
    {
        Templates$.MODULE$.tryCatch( var0, var1, var2, var3 );
    }

    public static <V> V handleKernelExceptions( final CodeBlock generate, final Fields fields, final Namer namer, final Function1<CodeBlock,V> block )
    {
        return Templates$.MODULE$.handleKernelExceptions( var0, var1, var2, var3 );
    }

    public static <V> V handleEntityNotFound( final CodeBlock generate, final Fields fields, final Namer namer, final Function1<CodeBlock,V> happyPath,
            final Function1<CodeBlock,V> onFailure )
    {
        return Templates$.MODULE$.handleEntityNotFound( var0, var1, var2, var3, var4 );
    }

    public static Expression asIntStream( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asIntStream( var0 );
    }

    public static Expression asDoubleStream( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asDoubleStream( var0 );
    }

    public static Expression asLongStream( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asLongStream( var0 );
    }

    public static Expression asPrimitiveRelationshipStream( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asPrimitiveRelationshipStream( var0 );
    }

    public static Expression asPrimitiveNodeStream( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asPrimitiveNodeStream( var0 );
    }

    public static Expression asAnyValueList( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asAnyValueList( var0 );
    }

    public static <T> Expression asList( final Seq<Expression> values, final Manifest<T> manifest )
    {
        return Templates$.MODULE$.asList( var0, var1 );
    }

    public static Expression createNewRelationshipValueFromPrimitive( final Expression proxySpi, final Expression expression )
    {
        return Templates$.MODULE$.createNewRelationshipValueFromPrimitive( var0, var1 );
    }

    public static Expression createNewNodeValueFromPrimitive( final Expression proxySpi, final Expression expression )
    {
        return Templates$.MODULE$.createNewNodeValueFromPrimitive( var0, var1 );
    }

    public static Expression createNewRelationshipReference( final Expression expression )
    {
        return Templates$.MODULE$.createNewRelationshipReference( var0 );
    }

    public static Expression createNewNodeReference( final Expression expression )
    {
        return Templates$.MODULE$.createNewNodeReference( var0 );
    }

    public static Expression newCountingMap()
    {
        return Templates$.MODULE$.newCountingMap();
    }

    public static Expression createNewInstance( final TypeReference valueType, final Seq<Tuple2<TypeReference,Expression>> args )
    {
        return Templates$.MODULE$.createNewInstance( var0, var1 );
    }
}
