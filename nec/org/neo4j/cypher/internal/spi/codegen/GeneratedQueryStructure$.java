package org.neo4j.cypher.internal.spi.codegen;

import java.util.ArrayList;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.neo4j.codegen.Binding;
import org.neo4j.codegen.ClassGenerator;
import org.neo4j.codegen.ClassHandle;
import org.neo4j.codegen.CodeBlock;
import org.neo4j.codegen.CodeGenerationStrategy;
import org.neo4j.codegen.CodeGenerator;
import org.neo4j.codegen.CodeGeneratorOption;
import org.neo4j.codegen.Expression;
import org.neo4j.codegen.FieldReference;
import org.neo4j.codegen.MethodDeclaration;
import org.neo4j.codegen.MethodReference;
import org.neo4j.codegen.Parameter;
import org.neo4j.codegen.TypeReference;
import org.neo4j.codegen.api.CodeGeneration.CodeSaver;
import org.neo4j.codegen.bytecode.ByteCode;
import org.neo4j.codegen.source.SourceCode;
import org.neo4j.cypher.internal.CodeGenPlanDescriptionHelper$;
import org.neo4j.cypher.internal.codegen.PrimitiveNodeStream;
import org.neo4j.cypher.internal.codegen.PrimitiveRelationshipStream;
import org.neo4j.cypher.internal.executionplan.GeneratedQuery;
import org.neo4j.cypher.internal.executionplan.GeneratedQueryExecution;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ByteCodeMode$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenMode;
import org.neo4j.cypher.internal.runtime.compiled.codegen.SourceCodeMode$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.AnyValueType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.BoolType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.FloatType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.JavaCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ListReferenceType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.LongType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.cypher.internal.v4_0.util.symbols.BooleanType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.FloatType;
import org.neo4j.cypher.internal.v4_0.util.symbols.IntegerType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.result.QueryResult.QueryResultVisitor;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.SchemaRead;
import org.neo4j.internal.kernel.api.TokenRead;
import org.neo4j.kernel.impl.core.TransactionalEntityFactory;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.MapValue;
import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.Option;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.generic.TraversableForwarder;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.mutable.ListBuffer;
import scala.collection.mutable.ListBuffer.;
import scala.reflect.Manifest;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class GeneratedQueryStructure$ implements CodeStructure<GeneratedQuery>
{
    public static GeneratedQueryStructure$ MODULE$;

    static
    {
        new GeneratedQueryStructure$();
    }

    private GeneratedQueryStructure$()
    {
        MODULE$ = this;
    }

    private CodeGenerator createGenerator( final CodeGenConfiguration conf, final CodeSaver code )
    {
        CodeGenMode var5 = conf.mode();
        CodeGenerationStrategy var3;
        if ( SourceCodeMode$.MODULE$.equals( var5 ) )
        {
            var3 = SourceCode.SOURCECODE;
        }
        else
        {
            if ( !ByteCodeMode$.MODULE$.equals( var5 ) )
            {
                throw new MatchError( var5 );
            }

            var3 = ByteCode.BYTECODE;
        }

        ListBuffer options = (ListBuffer).MODULE$.empty();
        if ( this.getClass().desiredAssertionStatus() )
        {
            options.$plus$eq( ByteCode.VERIFY_GENERATED_BYTECODE );
        }
        else
        {
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        conf.saveSource().foreach( ( path ) -> {
            return options.$plus$eq( SourceCode.sourceLocation( path ) );
        } );
        return CodeGenerator.generateCode( CodeStructure.class.getClassLoader(), var3,
                (CodeGeneratorOption[]) ((TraversableForwarder) options.$plus$plus( code.options() )).toArray( scala.reflect.ClassTag..MODULE$.apply(
                        CodeGeneratorOption.class ) ));
    }

    public GeneratedQueryStructure.GeneratedQueryStructureResult generateQuery( final String className, final Seq<String> columns,
            final Map<String,Id> operatorIds, final CodeGenConfiguration conf, final Function1<MethodStructure<?>,BoxedUnit> methodStructure,
            final CodeGenContext codeGenContext )
    {
        CodeSaver saver = new CodeSaver( conf.showSource(), conf.showByteCode() );
        CodeGenerator generator = this.createGenerator( conf, saver );
        ClassGenerator x$5 = generator.generateClass( conf.packageName(), (new StringBuilder( 9 )).append( className ).append( "Execution" ).toString(),
                new TypeReference[]{this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( GeneratedQueryExecution.class ) )});
        Function1 x$6 = ( clazzx ) -> {
            Fields fields = MODULE$.createFields( columns, clazzx );
            MODULE$.setOperatorIds( clazzx, operatorIds );
            MODULE$.addSimpleMethods( clazzx, fields );
            MODULE$.addAccept( methodStructure, generator, clazzx, fields, conf, codeGenContext );
            return clazzx.handle();
        };
        Function1 x$7 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$5, x$6 );
        Function2 x$8 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$5, x$6 );
        ClassHandle execution = (ClassHandle) org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$5, x$6, x$7, x$8 );
        ClassGenerator x$13 = generator.generateClass( conf.packageName(), className,
                new TypeReference[]{this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( GeneratedQuery.class ) )});
        Function1 x$14 = ( clazzx ) -> {
            CodeBlock x$9 = clazzx.generateMethod( MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( GeneratedQueryExecution.class ) ),
            "execute", new Parameter[]{MODULE$.param( "queryContext", scala.reflect.ManifestFactory..MODULE$.classType( QueryContext.class )),
            MODULE$.param( "tracer", scala.reflect.ManifestFactory..MODULE$.classType( QueryProfiler.class )),
            MODULE$.param( "params", scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ))});
            Function1 x$10 = ( execute ) -> {
                $anonfun$generateQuery$3( execution, execute );
                return BoxedUnit.UNIT;
            };
            Function1 x$11 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$9, x$10 );
            Function2 x$12 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$9, x$10 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$9, x$10, x$11, x$12 );
            return clazzx.handle();
        }; Function1 x$15 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$13, x$14 );
        Function2 x$16 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$13, x$14 );
        GeneratedQuery query = (GeneratedQuery) ((ClassHandle) org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$13, x$14, x$15, x$16 )).
        newInstance();
        Class clazz = execution.loadClass();
        operatorIds.foreach( ( x0$1 ) -> {
            $anonfun$generateQuery$4( clazz, x0$1 );
            return BoxedUnit.UNIT;
        } );
        return new GeneratedQueryStructure.GeneratedQueryStructureResult( query, CodeGenPlanDescriptionHelper$.MODULE$.metadata( saver ) );
    }

    private void addAccept( final Function1<MethodStructure<?>,BoxedUnit> methodStructure, final CodeGenerator generator, final ClassGenerator clazz,
            final Fields fields, final CodeGenConfiguration conf, final CodeGenContext codeGenContext )
    {
        String exceptionVar = codeGenContext.namer().newVarName();
        CodeBlock x$1 = clazz.generate( MethodDeclaration.method( this.typeRef( scala.reflect.ManifestFactory..MODULE$.Unit() ), "accept", new Parameter[]{
                Parameter.param( TypeReference.parameterizedType( QueryResultVisitor.class, new TypeReference[]{TypeReference.typeParameter( "E" )} ),
                        "visitor" )} ).parameterizedWith( "E",
                TypeReference.extending( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Exception.class ) ) )).
        throwsException( TypeReference.typeParameter( "E" ) ));
        Function1 x$2 = ( b ) -> {
            $anonfun$addAccept$1( methodStructure, generator, fields, conf, codeGenContext, exceptionVar, b );
            return BoxedUnit.UNIT;
        };
        Function1 x$3 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$1, x$2 );
        Function2 x$4 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$1, x$2 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$1, x$2, x$3, x$4 );
    }

    private MethodReference addSimpleMethods( final ClassGenerator clazz, final Fields fields )
    {
        clazz.generate( Templates$.MODULE$.constructor( clazz.handle() ), new Binding[0] );
        Templates$.MODULE$.getOrLoadDataRead( clazz, fields );
        Templates$.MODULE$.getOrLoadTokenRead( clazz, fields );
        Templates$.MODULE$.getOrLoadSchemaRead( clazz, fields );
        Templates$.MODULE$.getOrLoadCursors( clazz, fields );
        Templates$.MODULE$.nodeCursor( clazz, fields );
        Templates$.MODULE$.relationshipScanCursor( clazz, fields );
        Templates$.MODULE$.propertyCursor( clazz, fields );
        Templates$.MODULE$.closeCursors( clazz, fields );
        return clazz.generate( Templates$.MODULE$.FIELD_NAMES(), new Binding[0] );
    }

    private void setOperatorIds( final ClassGenerator clazz, final Map<String,Id> operatorIds )
    {
        operatorIds.keys().foreach( ( opId ) -> {
            return clazz.publicStaticField( MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Id.class ) ),opId);
        } );
    }

    private Fields createFields( final Seq<String> columns, final ClassGenerator clazz )
    {
        clazz.privateStaticFinalField( TypeReference.typeReference( String[].class ), "COLUMNS",
                Expression.newInitializedArray( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ) ),
                (Expression[]) ((TraversableOnce) columns.map( ( key ) -> {
                    return Expression.constant( key );
                }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ))));
        return new Fields( clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TransactionalEntityFactory.class ) ), "proxySpi" ),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( QueryProfiler.class ) ), "tracer"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ) ), "params"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( QueryContext.class ) ), "queryContext"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorFactory.class ) ), "cursors"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ) ), "nodeCursor"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ) ), "relationshipScanCursor"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class ) ), "propertyCursor"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class ) ), "dataRead"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ) ), "tokenRead"),
        clazz.field( this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( SchemaRead.class ) ), "schemaRead"),clazz.field(
            this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( ArrayList.class, scala.reflect.ManifestFactory..MODULE$.classType(
                    AutoCloseable.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),"closeables"));
    }

    public <O, R> MethodReference method( final String name, final Seq<TypeReference> params, final Manifest<O> owner, final Manifest<R> returns )
    {
        return MethodReference.methodReference( this.typeReference( owner ), this.typeReference( returns ), name, 1,
                (TypeReference[]) params.toArray( scala.reflect.ClassTag..MODULE$.apply( TypeReference.class ) ));
    }

    public <O, R> FieldReference staticField( final String name, final Manifest<O> owner, final Manifest<R> fieldType )
    {
        return FieldReference.staticField( this.typeReference( owner ), this.typeReference( fieldType ), name );
    }

    public <T> Parameter param( final String name, final Manifest<T> manifest )
    {
        return Parameter.param( this.typeReference( manifest ), name );
    }

    public <T> TypeReference typeRef( final Manifest<T> manifest )
    {
        return this.typeReference( manifest );
    }

    public TypeReference typeReference( final Manifest<?> manifest )
    {
        List arguments = manifest.typeArguments();
        TypeReference base = TypeReference.typeReference( manifest.runtimeClass() );
        return arguments.nonEmpty() ? TypeReference.parameterizedType( base, (TypeReference[]) ((TraversableOnce) arguments.map( ( manifestx ) -> {
            return MODULE$.typeReference( manifestx );
        }, scala.collection.immutable.List..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( TypeReference.class ))) :base;
    }

    public TypeReference lowerType( final CodeGenType cType )
    {
        boolean var3 = false;
        CypherCodeGenType var4 = null;
        TypeReference var2;
        NodeType var10000;
        if ( cType instanceof CypherCodeGenType )
        {
            label204:
            {
                var3 = true;
                var4 = (CypherCodeGenType) cType;
                CypherType var6 = var4.ct();
                RepresentationType var7 = var4.repr();
                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                if ( var10000 == null )
                {
                    if ( var6 != null )
                    {
                        break label204;
                    }
                }
                else if ( !var10000.equals( var6 ) )
                {
                    break label204;
                }

                if ( LongType$.MODULE$.equals( var7 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Long());
                    return var2;
                }
            }
        }

        RelationshipType var51;
        if ( var3 )
        {
            label195:
            {
                CypherType var9 = var4.ct();
                RepresentationType var10 = var4.repr();
                var51 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                if ( var51 == null )
                {
                    if ( var9 != null )
                    {
                        break label195;
                    }
                }
                else if ( !var51.equals( var9 ) )
                {
                    break label195;
                }

                if ( LongType$.MODULE$.equals( var10 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Long());
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label186:
            {
                CypherType var12 = var4.ct();
                RepresentationType var13 = var4.repr();
                IntegerType var52 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                if ( var52 == null )
                {
                    if ( var12 != null )
                    {
                        break label186;
                    }
                }
                else if ( !var52.equals( var12 ) )
                {
                    break label186;
                }

                if ( LongType$.MODULE$.equals( var13 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Long());
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label177:
            {
                CypherType var15 = var4.ct();
                RepresentationType var16 = var4.repr();
                FloatType var53 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                if ( var53 == null )
                {
                    if ( var15 != null )
                    {
                        break label177;
                    }
                }
                else if ( !var53.equals( var15 ) )
                {
                    break label177;
                }

                if ( FloatType$.MODULE$.equals( var16 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Double());
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label168:
            {
                CypherType var18 = var4.ct();
                RepresentationType var19 = var4.repr();
                BooleanType var54 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                if ( var54 == null )
                {
                    if ( var18 != null )
                    {
                        break label168;
                    }
                }
                else if ( !var54.equals( var18 ) )
                {
                    break label168;
                }

                if ( BoolType$.MODULE$.equals( var19 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean());
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            CypherType var21 = var4.ct();
            RepresentationType var22 = var4.repr();
            Option var23 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var21 );
            if ( !var23.isEmpty() )
            {
                label158:
                {
                    CypherType var24 = (CypherType) var23.get();
                    var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var24 != null )
                        {
                            break label158;
                        }
                    }
                    else if ( !var10000.equals( var24 ) )
                    {
                        break label158;
                    }

                    if ( var22 instanceof ListReferenceType )
                    {
                        ListReferenceType var26 = (ListReferenceType) var22;
                        RepresentationType var27 = var26.inner();
                        if ( LongType$.MODULE$.equals( var27 ) )
                        {
                            var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveNodeStream.class ));
                            return var2;
                        }
                    }
                }
            }
        }

        if ( var3 )
        {
            CypherType var28 = var4.ct();
            RepresentationType var29 = var4.repr();
            Option var30 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var28 );
            if ( !var30.isEmpty() )
            {
                label147:
                {
                    CypherType var31 = (CypherType) var30.get();
                    var51 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var51 == null )
                    {
                        if ( var31 != null )
                        {
                            break label147;
                        }
                    }
                    else if ( !var51.equals( var31 ) )
                    {
                        break label147;
                    }

                    if ( var29 instanceof ListReferenceType )
                    {
                        ListReferenceType var33 = (ListReferenceType) var29;
                        RepresentationType var34 = var33.inner();
                        if ( LongType$.MODULE$.equals( var34 ) )
                        {
                            var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveRelationshipStream.class ));
                            return var2;
                        }
                    }
                }
            }
        }

        if ( var3 )
        {
            CypherType var35 = var4.ct();
            RepresentationType var36 = var4.repr();
            Option var37 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var35 );
            if ( !var37.isEmpty() && var36 instanceof ListReferenceType )
            {
                ListReferenceType var38 = (ListReferenceType) var36;
                RepresentationType var39 = var38.inner();
                if ( LongType$.MODULE$.equals( var39 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongStream.class ));
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            CypherType var40 = var4.ct();
            RepresentationType var41 = var4.repr();
            Option var42 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var40 );
            if ( !var42.isEmpty() && var41 instanceof ListReferenceType )
            {
                ListReferenceType var43 = (ListReferenceType) var41;
                RepresentationType var44 = var43.inner();
                if ( FloatType$.MODULE$.equals( var44 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( DoubleStream.class ));
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            CypherType var45 = var4.ct();
            RepresentationType var46 = var4.repr();
            Option var47 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var45 );
            if ( !var47.isEmpty() && var46 instanceof ListReferenceType )
            {
                ListReferenceType var48 = (ListReferenceType) var46;
                RepresentationType var49 = var48.inner();
                if ( BoolType$.MODULE$.equals( var49 ) )
                {
                    var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( IntStream.class ));
                    return var2;
                }
            }
        }

        label243:
        {
            JavaCodeGenType var55 = CodeGenType$.MODULE$.javaInt();
            if ( var55 == null )
            {
                if ( cType != null )
                {
                    break label243;
                }
            }
            else if ( !var55.equals( cType ) )
            {
                break label243;
            }

            var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Int());
            return var2;
        }

        if ( var3 && var4.repr() instanceof AnyValueType )
        {
            var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
        }
        else
        {
            var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Object());
        }

        return var2;
    }

    public TypeReference lowerTypeScalarSubset( final CodeGenType cType )
    {
        boolean var3 = false;
        CypherCodeGenType var4 = null;
        TypeReference var2;
        if ( cType instanceof CypherCodeGenType )
        {
            label114:
            {
                var3 = true;
                var4 = (CypherCodeGenType) cType;
                CypherType var6 = var4.ct();
                RepresentationType var7 = var4.repr();
                NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                if ( var10000 == null )
                {
                    if ( var6 != null )
                    {
                        break label114;
                    }
                }
                else if ( !var10000.equals( var6 ) )
                {
                    break label114;
                }

                if ( LongType$.MODULE$.equals( var7 ) )
                {
                    var2 = this.lowerType( cType );
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label105:
            {
                CypherType var9 = var4.ct();
                RepresentationType var10 = var4.repr();
                RelationshipType var22 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                if ( var22 == null )
                {
                    if ( var9 != null )
                    {
                        break label105;
                    }
                }
                else if ( !var22.equals( var9 ) )
                {
                    break label105;
                }

                if ( LongType$.MODULE$.equals( var10 ) )
                {
                    var2 = this.lowerType( cType );
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label96:
            {
                CypherType var12 = var4.ct();
                RepresentationType var13 = var4.repr();
                IntegerType var23 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                if ( var23 == null )
                {
                    if ( var12 != null )
                    {
                        break label96;
                    }
                }
                else if ( !var23.equals( var12 ) )
                {
                    break label96;
                }

                if ( LongType$.MODULE$.equals( var13 ) )
                {
                    var2 = this.lowerType( cType );
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label87:
            {
                CypherType var15 = var4.ct();
                RepresentationType var16 = var4.repr();
                FloatType var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                if ( var24 == null )
                {
                    if ( var15 != null )
                    {
                        break label87;
                    }
                }
                else if ( !var24.equals( var15 ) )
                {
                    break label87;
                }

                if ( FloatType$.MODULE$.equals( var16 ) )
                {
                    var2 = this.lowerType( cType );
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label78:
            {
                CypherType var18 = var4.ct();
                RepresentationType var19 = var4.repr();
                BooleanType var25 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                if ( var25 == null )
                {
                    if ( var18 != null )
                    {
                        break label78;
                    }
                }
                else if ( !var25.equals( var18 ) )
                {
                    break label78;
                }

                if ( BoolType$.MODULE$.equals( var19 ) )
                {
                    var2 = this.lowerType( cType );
                    return var2;
                }
            }
        }

        label138:
        {
            JavaCodeGenType var26 = CodeGenType$.MODULE$.javaInt();
            if ( var26 == null )
            {
                if ( cType != null )
                {
                    break label138;
                }
            }
            else if ( !var26.equals( cType ) )
            {
                break label138;
            }

            var2 = this.lowerType( cType );
            return var2;
        }

        if ( var3 && var4.repr() instanceof AnyValueType )
        {
            var2 = this.lowerType( cType );
        }
        else
        {
            var2 = this.typeRef( scala.reflect.ManifestFactory..MODULE$.Object());
        }

        return var2;
    }

    public Expression nullValue( final CodeGenType cType )
    {
        boolean var3 = false;
        CypherCodeGenType var4 = null;
        Expression var2;
        if ( cType instanceof CypherCodeGenType )
        {
            label47:
            {
                var3 = true;
                var4 = (CypherCodeGenType) cType;
                CypherType var6 = var4.ct();
                RepresentationType var7 = var4.repr();
                NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                if ( var10000 == null )
                {
                    if ( var6 != null )
                    {
                        break label47;
                    }
                }
                else if ( !var10000.equals( var6 ) )
                {
                    break label47;
                }

                if ( LongType$.MODULE$.equals( var7 ) )
                {
                    var2 = Expression.constant( BoxesRunTime.boxToLong( -1L ) );
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            label38:
            {
                CypherType var9 = var4.ct();
                RepresentationType var10 = var4.repr();
                RelationshipType var12 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                if ( var12 == null )
                {
                    if ( var9 != null )
                    {
                        break label38;
                    }
                }
                else if ( !var12.equals( var9 ) )
                {
                    break label38;
                }

                if ( LongType$.MODULE$.equals( var10 ) )
                {
                    var2 = Expression.constant( BoxesRunTime.boxToLong( -1L ) );
                    return var2;
                }
            }
        }

        if ( var3 && var4.repr() instanceof AnyValueType )
        {
            var2 = Templates$.MODULE$.noValue();
        }
        else
        {
            var2 = Expression.constant( (Object) null );
        }

        return var2;
    }
}
