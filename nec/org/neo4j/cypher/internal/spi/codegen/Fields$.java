package org.neo4j.cypher.internal.spi.codegen;

import org.neo4j.codegen.FieldReference;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple12;
import scala.None.;
import scala.runtime.AbstractFunction12;

public final class Fields$ extends
        AbstractFunction12<FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,Fields>
        implements Serializable
{
    public static Fields$ MODULE$;

    static
    {
        new Fields$();
    }

    private Fields$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Fields";
    }

    public Fields apply( final FieldReference entityAccessor, final FieldReference tracer, final FieldReference params, final FieldReference queryContext,
            final FieldReference cursors, final FieldReference nodeCursor, final FieldReference relationshipScanCursor, final FieldReference propertyCursor,
            final FieldReference dataRead, final FieldReference tokenRead, final FieldReference schemaRead, final FieldReference closeables )
    {
        return new Fields( entityAccessor, tracer, params, queryContext, cursors, nodeCursor, relationshipScanCursor, propertyCursor, dataRead, tokenRead,
                schemaRead, closeables );
    }

    public Option<Tuple12<FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference>> unapply(
            final Fields x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple12( x$0.entityAccessor(), x$0.tracer(), x$0.params(), x$0.queryContext(), x$0.cursors(), x$0.nodeCursor(), x$0.relationshipScanCursor(),
                    x$0.propertyCursor(), x$0.dataRead(), x$0.tokenRead(), x$0.schemaRead(), x$0.closeables() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
