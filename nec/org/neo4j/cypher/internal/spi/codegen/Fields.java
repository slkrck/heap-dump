package org.neo4j.cypher.internal.spi.codegen;

import org.neo4j.codegen.FieldReference;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple12;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class Fields implements Product, Serializable
{
    private final FieldReference entityAccessor;
    private final FieldReference tracer;
    private final FieldReference params;
    private final FieldReference queryContext;
    private final FieldReference cursors;
    private final FieldReference nodeCursor;
    private final FieldReference relationshipScanCursor;
    private final FieldReference propertyCursor;
    private final FieldReference dataRead;
    private final FieldReference tokenRead;
    private final FieldReference schemaRead;
    private final FieldReference closeables;

    public Fields( final FieldReference entityAccessor, final FieldReference tracer, final FieldReference params, final FieldReference queryContext,
            final FieldReference cursors, final FieldReference nodeCursor, final FieldReference relationshipScanCursor, final FieldReference propertyCursor,
            final FieldReference dataRead, final FieldReference tokenRead, final FieldReference schemaRead, final FieldReference closeables )
    {
        this.entityAccessor = entityAccessor;
        this.tracer = tracer;
        this.params = params;
        this.queryContext = queryContext;
        this.cursors = cursors;
        this.nodeCursor = nodeCursor;
        this.relationshipScanCursor = relationshipScanCursor;
        this.propertyCursor = propertyCursor;
        this.dataRead = dataRead;
        this.tokenRead = tokenRead;
        this.schemaRead = schemaRead;
        this.closeables = closeables;
        Product.$init$( this );
    }

    public static Option<Tuple12<FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference>> unapply(
            final Fields x$0 )
    {
        return Fields$.MODULE$.unapply( var0 );
    }

    public static Fields apply( final FieldReference entityAccessor, final FieldReference tracer, final FieldReference params,
            final FieldReference queryContext, final FieldReference cursors, final FieldReference nodeCursor, final FieldReference relationshipScanCursor,
            final FieldReference propertyCursor, final FieldReference dataRead, final FieldReference tokenRead, final FieldReference schemaRead,
            final FieldReference closeables )
    {
        return Fields$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11 );
    }

    public static Function1<Tuple12<FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference,FieldReference>,Fields> tupled()
    {
        return Fields$.MODULE$.tupled();
    }

    public static Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Function1<FieldReference,Fields>>>>>>>>>>>> curried()
    {
        return Fields$.MODULE$.curried();
    }

    public FieldReference entityAccessor()
    {
        return this.entityAccessor;
    }

    public FieldReference tracer()
    {
        return this.tracer;
    }

    public FieldReference params()
    {
        return this.params;
    }

    public FieldReference queryContext()
    {
        return this.queryContext;
    }

    public FieldReference cursors()
    {
        return this.cursors;
    }

    public FieldReference nodeCursor()
    {
        return this.nodeCursor;
    }

    public FieldReference relationshipScanCursor()
    {
        return this.relationshipScanCursor;
    }

    public FieldReference propertyCursor()
    {
        return this.propertyCursor;
    }

    public FieldReference dataRead()
    {
        return this.dataRead;
    }

    public FieldReference tokenRead()
    {
        return this.tokenRead;
    }

    public FieldReference schemaRead()
    {
        return this.schemaRead;
    }

    public FieldReference closeables()
    {
        return this.closeables;
    }

    public Fields copy( final FieldReference entityAccessor, final FieldReference tracer, final FieldReference params, final FieldReference queryContext,
            final FieldReference cursors, final FieldReference nodeCursor, final FieldReference relationshipScanCursor, final FieldReference propertyCursor,
            final FieldReference dataRead, final FieldReference tokenRead, final FieldReference schemaRead, final FieldReference closeables )
    {
        return new Fields( entityAccessor, tracer, params, queryContext, cursors, nodeCursor, relationshipScanCursor, propertyCursor, dataRead, tokenRead,
                schemaRead, closeables );
    }

    public FieldReference copy$default$1()
    {
        return this.entityAccessor();
    }

    public FieldReference copy$default$10()
    {
        return this.tokenRead();
    }

    public FieldReference copy$default$11()
    {
        return this.schemaRead();
    }

    public FieldReference copy$default$12()
    {
        return this.closeables();
    }

    public FieldReference copy$default$2()
    {
        return this.tracer();
    }

    public FieldReference copy$default$3()
    {
        return this.params();
    }

    public FieldReference copy$default$4()
    {
        return this.queryContext();
    }

    public FieldReference copy$default$5()
    {
        return this.cursors();
    }

    public FieldReference copy$default$6()
    {
        return this.nodeCursor();
    }

    public FieldReference copy$default$7()
    {
        return this.relationshipScanCursor();
    }

    public FieldReference copy$default$8()
    {
        return this.propertyCursor();
    }

    public FieldReference copy$default$9()
    {
        return this.dataRead();
    }

    public String productPrefix()
    {
        return "Fields";
    }

    public int productArity()
    {
        return 12;
    }

    public Object productElement( final int x$1 )
    {
        FieldReference var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.entityAccessor();
            break;
        case 1:
            var10000 = this.tracer();
            break;
        case 2:
            var10000 = this.params();
            break;
        case 3:
            var10000 = this.queryContext();
            break;
        case 4:
            var10000 = this.cursors();
            break;
        case 5:
            var10000 = this.nodeCursor();
            break;
        case 6:
            var10000 = this.relationshipScanCursor();
            break;
        case 7:
            var10000 = this.propertyCursor();
            break;
        case 8:
            var10000 = this.dataRead();
            break;
        case 9:
            var10000 = this.tokenRead();
            break;
        case 10:
            var10000 = this.schemaRead();
            break;
        case 11:
            var10000 = this.closeables();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Fields;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var17;
        if ( this != x$1 )
        {
            label153:
            {
                boolean var2;
                if ( x$1 instanceof Fields )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label135:
                    {
                        label144:
                        {
                            Fields var4 = (Fields) x$1;
                            FieldReference var10000 = this.entityAccessor();
                            FieldReference var5 = var4.entityAccessor();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label144;
                            }

                            var10000 = this.tracer();
                            FieldReference var6 = var4.tracer();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label144;
                            }

                            var10000 = this.params();
                            FieldReference var7 = var4.params();
                            if ( var10000 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var7 ) )
                            {
                                break label144;
                            }

                            var10000 = this.queryContext();
                            FieldReference var8 = var4.queryContext();
                            if ( var10000 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var8 ) )
                            {
                                break label144;
                            }

                            var10000 = this.cursors();
                            FieldReference var9 = var4.cursors();
                            if ( var10000 == null )
                            {
                                if ( var9 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var9 ) )
                            {
                                break label144;
                            }

                            var10000 = this.nodeCursor();
                            FieldReference var10 = var4.nodeCursor();
                            if ( var10000 == null )
                            {
                                if ( var10 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var10 ) )
                            {
                                break label144;
                            }

                            var10000 = this.relationshipScanCursor();
                            FieldReference var11 = var4.relationshipScanCursor();
                            if ( var10000 == null )
                            {
                                if ( var11 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var11 ) )
                            {
                                break label144;
                            }

                            var10000 = this.propertyCursor();
                            FieldReference var12 = var4.propertyCursor();
                            if ( var10000 == null )
                            {
                                if ( var12 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var12 ) )
                            {
                                break label144;
                            }

                            var10000 = this.dataRead();
                            FieldReference var13 = var4.dataRead();
                            if ( var10000 == null )
                            {
                                if ( var13 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var13 ) )
                            {
                                break label144;
                            }

                            var10000 = this.tokenRead();
                            FieldReference var14 = var4.tokenRead();
                            if ( var10000 == null )
                            {
                                if ( var14 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var14 ) )
                            {
                                break label144;
                            }

                            var10000 = this.schemaRead();
                            FieldReference var15 = var4.schemaRead();
                            if ( var10000 == null )
                            {
                                if ( var15 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var15 ) )
                            {
                                break label144;
                            }

                            var10000 = this.closeables();
                            FieldReference var16 = var4.closeables();
                            if ( var10000 == null )
                            {
                                if ( var16 != null )
                                {
                                    break label144;
                                }
                            }
                            else if ( !var10000.equals( var16 ) )
                            {
                                break label144;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var17 = true;
                                break label135;
                            }
                        }

                        var17 = false;
                    }

                    if ( var17 )
                    {
                        break label153;
                    }
                }

                var17 = false;
                return var17;
            }
        }

        var17 = true;
        return var17;
    }
}
