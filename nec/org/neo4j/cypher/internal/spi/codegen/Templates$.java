package org.neo4j.cypher.internal.spi.codegen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.eclipse.collections.impl.map.mutable.primitive.LongIntHashMap;
import org.neo4j.codegen.ClassGenerator;
import org.neo4j.codegen.ClassHandle;
import org.neo4j.codegen.CodeBlock;
import org.neo4j.codegen.Expression;
import org.neo4j.codegen.ExpressionTemplate;
import org.neo4j.codegen.MethodDeclaration;
import org.neo4j.codegen.MethodReference;
import org.neo4j.codegen.MethodTemplate;
import org.neo4j.codegen.Parameter;
import org.neo4j.codegen.TypeReference;
import org.neo4j.codegen.MethodDeclaration.Builder;
import org.neo4j.common.TokenNameLookup;
import org.neo4j.cypher.internal.codegen.PrimitiveNodeStream;
import org.neo4j.cypher.internal.codegen.PrimitiveRelationshipStream;
import org.neo4j.cypher.internal.javacompat.ResultRowImpl;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Namer;
import org.neo4j.exceptions.CypherExecutionException;
import org.neo4j.exceptions.EntityNotFoundException;
import org.neo4j.exceptions.KernelException;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.SchemaRead;
import org.neo4j.internal.kernel.api.TokenRead;
import org.neo4j.kernel.api.SilentTokenNameLookup;
import org.neo4j.kernel.impl.api.RelationshipDataExtractor;
import org.neo4j.kernel.impl.core.TransactionalEntityFactory;
import org.neo4j.kernel.impl.util.ValueUtils;
import org.neo4j.values.AnyValue;
import org.neo4j.values.AnyValues;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.ValueComparator;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.NodeReference;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipReference;
import org.neo4j.values.virtual.RelationshipValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Function2;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.Manifest;
import scala.runtime.BoxedUnit;
import scala.runtime.ObjectRef;

public final class Templates$
{
    public static Templates$ MODULE$;

    static
    {
        new Templates$();
    }

    private final Expression newCountingMap;
    private final Expression noValue;
    private final Expression incoming;
    private final Expression outgoing;
    private final Expression both;
    private final Expression newResultRow;
    private final Expression newRelationshipDataExtractor;
    private final Expression valueComparator;
    private final Expression anyValueComparator;
    private final MethodTemplate FIELD_NAMES;

    private Templates$()
    {
        MODULE$ = this;
        this.newCountingMap = this.createNewInstance(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongIntHashMap.class ) ), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ));
        this.noValue = Expression.getStatic(
                GeneratedQueryStructure$.MODULE$.staticField( "NO_VALUE", scala.reflect.ManifestFactory..MODULE$.classType( Values.class ),
                scala.reflect.ManifestFactory..MODULE$.classType( Value.class )));
        this.incoming = Expression.getStatic(
                GeneratedQueryStructure$.MODULE$.staticField( Direction.INCOMING.name(), scala.reflect.ManifestFactory..MODULE$.classType( Direction.class ),
                scala.reflect.ManifestFactory..MODULE$.classType( Direction.class )));
        this.outgoing = Expression.getStatic(
                GeneratedQueryStructure$.MODULE$.staticField( Direction.OUTGOING.name(), scala.reflect.ManifestFactory..MODULE$.classType( Direction.class ),
                scala.reflect.ManifestFactory..MODULE$.classType( Direction.class )));
        this.both = Expression.getStatic(
                GeneratedQueryStructure$.MODULE$.staticField( Direction.BOTH.name(), scala.reflect.ManifestFactory..MODULE$.classType( Direction.class ),
                scala.reflect.ManifestFactory..MODULE$.classType( Direction.class )));
        this.newResultRow = Expression.invoke( Expression.newInstance(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        ResultRowImpl.class ) ) ), MethodReference.constructorReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( ResultRowImpl.class ) ), new TypeReference[0]),
        new Expression[0]);
        this.newRelationshipDataExtractor = Expression.invoke( Expression.newInstance(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        RelationshipDataExtractor.class ) ) ), MethodReference.constructorReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        RelationshipDataExtractor.class ) ), new TypeReference[0]),new Expression[0]);
        this.valueComparator = Expression.getStatic(
                GeneratedQueryStructure$.MODULE$.staticField( "COMPARATOR", scala.reflect.ManifestFactory..MODULE$.classType( Values.class ),
                scala.reflect.ManifestFactory..MODULE$.classType( ValueComparator.class )));
        this.anyValueComparator = Expression.getStatic(
                GeneratedQueryStructure$.MODULE$.staticField( "COMPARATOR", scala.reflect.ManifestFactory..MODULE$.classType( AnyValues.class ),
                scala.reflect.ManifestFactory..MODULE$.classType( Comparator.class, scala.reflect.ManifestFactory..MODULE$.classType(
                AnyValue.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))));
        this.FIELD_NAMES = MethodTemplate.method( TypeReference.typeReference( String[].class ), "fieldNames", new Parameter[0] ).returns(
                ExpressionTemplate.get( TypeReference.typeReference( String[].class ), "COLUMNS" ) ).build();
    }

    public Expression createNewInstance( final TypeReference valueType, final Seq<Tuple2<TypeReference,Expression>> args )
    {
        Seq argTypes = (Seq) args.map( ( x$1 ) -> {
            return (TypeReference) x$1._1();
        },.MODULE$.canBuildFrom());
        Seq argExpression = (Seq) args.map( ( x$2 ) -> {
            return (Expression) x$2._2();
        },.MODULE$.canBuildFrom());
        return Expression.invoke( Expression.newInstance( valueType ), MethodReference.constructorReference( valueType,
                (TypeReference[]) argTypes.toArray( scala.reflect.ClassTag..MODULE$.apply( TypeReference.class ) ) ),
        (Expression[]) argExpression.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )));
    }

    public Expression newCountingMap()
    {
        return this.newCountingMap;
    }

    public Expression createNewNodeReference( final Expression expression )
    {
        return Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "node", scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
        scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeReference.class )),
        new Expression[]{expression});
    }

    public Expression createNewRelationshipReference( final Expression expression )
    {
        return Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "relationship", scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
        scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipReference.class )),
        new Expression[]{expression});
    }

    public Expression createNewNodeValueFromPrimitive( final Expression proxySpi, final Expression expression )
    {
        return Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "fromNodeEntity", scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Node.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),
        new Expression[]{Expression.invoke( proxySpi, Methods$.MODULE$.newNodeEntityById(), new Expression[]{expression} )});
    }

    public Expression createNewRelationshipValueFromPrimitive( final Expression proxySpi, final Expression expression )
    {
        return Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "fromRelationshipEntity", scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Relationship.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class )),
        new Expression[]{Expression.invoke( proxySpi, Methods$.MODULE$.newRelationshipEntityById(), new Expression[]{expression} )});
    }

    public <T> Expression asList( final Seq<Expression> values, final Manifest<T> manifest )
    {
        return Expression.invoke(
                MethodReference.methodReference( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Arrays.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( List.class, manifest,
                        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ) )),"asList", new TypeReference[]{
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Object()))}),
        new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( manifest ),
                (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ) ))});
    }

    public Expression asAnyValueList( final Seq<Expression> values )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( VirtualValues.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( ListValue.class ) ),"list", new TypeReference[]{
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.classType(
                    AnyValue.class )))}),new Expression[]{
            Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ) ),
            (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))});
    }

    public Expression asPrimitiveNodeStream( final Seq<Expression> values )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveNodeStream.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveNodeStream.class ) ),
        "of", new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Long()))}),
        new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ),
                (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))});
    }

    public Expression asPrimitiveRelationshipStream( final Seq<Expression> values )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveRelationshipStream.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveRelationshipStream.class ) ),
        "of", new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Long()))}),
        new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ),
                (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))});
    }

    public Expression asLongStream( final Seq<Expression> values )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongStream.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongStream.class ) ),"of", new TypeReference[]{
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Long()))}),
        new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ),
                (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))});
    }

    public Expression asDoubleStream( final Seq<Expression> values )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( DoubleStream.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( DoubleStream.class ) ),"of", new TypeReference[]{
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Double()))}),
        new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Double() ),
                (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))});
    }

    public Expression asIntStream( final Seq<Expression> values )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( IntStream.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( IntStream.class ) ),"of", new TypeReference[]{
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()))}),
        new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
                (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))});
    }

    public <V> V handleEntityNotFound( final CodeBlock generate, final Fields fields, final Namer namer, final Function1<CodeBlock,V> happyPath,
            final Function1<CodeBlock,V> onFailure )
    {
        ObjectRef result = ObjectRef.create( (Object) null );
        generate.tryCatch( ( innerBody ) -> {
            result.elem = happyPath.apply( innerBody );
        }, ( innerError ) -> {
            result.elem = onFailure.apply( innerError );
            innerError.continueIfPossible();
        }, GeneratedQueryStructure$.MODULE$.param( namer.newVarName(), scala.reflect.ManifestFactory..MODULE$.classType( EntityNotFoundException.class ) ));
        return result.elem;
    }

    public <V> V handleKernelExceptions( final CodeBlock generate, final Fields fields, final Namer namer, final Function1<CodeBlock,V> block )
    {
        ObjectRef result = ObjectRef.create( (Object) null );
        String e = namer.newVarName();
        generate.tryCatch( ( body ) -> {
            result.elem = block.apply( body );
        }, ( handle ) -> {
            handle.throwException( Expression.invoke( Expression.newInstance(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CypherExecutionException.class ) ) ),
                    MethodReference.constructorReference(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CypherExecutionException.class ) ),
                    new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                            String.class ) ), GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Throwable.class ))}),
            new Expression[]{Expression.invoke( handle.load( e ), GeneratedQueryStructure$.MODULE$.method( "getUserMessage", scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TokenNameLookup.class ))})),
            scala.reflect.ManifestFactory..MODULE$.classType( KernelException.class ), scala.reflect.ManifestFactory..MODULE$.classType( String.class )),
            new Expression[]{Expression.invoke( Expression.newInstance(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( SilentTokenNameLookup.class ) ) ),
                    MethodReference.constructorReference(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( SilentTokenNameLookup.class ) ),
                    new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ))}),
            new Expression[]{Expression.get( handle.self(), fields.tokenRead() )})}),handle.load( e )}));
        }, GeneratedQueryStructure$.MODULE$.param( e, scala.reflect.ManifestFactory..MODULE$.classType( KernelException.class ) ));
        return result.elem;
    }

    public void tryCatch( final CodeBlock generate, final Function1<CodeBlock,BoxedUnit> tryBlock, final Parameter exception,
            final Function1<CodeBlock,BoxedUnit> catchBlock )
    {
        generate.tryCatch( ( body ) -> {
            tryBlock.apply( body );
        }, ( handle ) -> {
            catchBlock.apply( handle );
        }, exception );
    }

    public Expression noValue()
    {
        return this.noValue;
    }

    public Expression incoming()
    {
        return this.incoming;
    }

    public Expression outgoing()
    {
        return this.outgoing;
    }

    public Expression both()
    {
        return this.both;
    }

    public Expression newResultRow()
    {
        return this.newResultRow;
    }

    public Expression newRelationshipDataExtractor()
    {
        return this.newRelationshipDataExtractor;
    }

    public Expression valueComparator()
    {
        return this.valueComparator;
    }

    public Expression anyValueComparator()
    {
        return this.anyValueComparator;
    }

    public MethodTemplate constructor( final ClassHandle classHandle )
    {
        return MethodTemplate.constructor(
                new Parameter[]{GeneratedQueryStructure$.MODULE$.param( "queryContext", scala.reflect.ManifestFactory..MODULE$.classType( QueryContext.class ) ),
        GeneratedQueryStructure$.MODULE$.param( "tracer", scala.reflect.ManifestFactory..MODULE$.classType( QueryProfiler.class )),
        GeneratedQueryStructure$.MODULE$.param( "params", scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ))}).
        invokeSuper().put( ExpressionTemplate.self( classHandle ), GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                QueryContext.class ) ), "queryContext", ExpressionTemplate.load( "queryContext",
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( QueryContext.class ) ))).
        put( ExpressionTemplate.self( classHandle ), GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                QueryProfiler.class ) ), "tracer", ExpressionTemplate.load( "tracer",
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( QueryProfiler.class ) ))).
        put( ExpressionTemplate.self( classHandle ), GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                MapValue.class ) ), "params", ExpressionTemplate.load( "params",
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ) ))).
        put( ExpressionTemplate.self( classHandle ), GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                TransactionalEntityFactory.class ) ), "proxySpi", ExpressionTemplate.invoke( ExpressionTemplate.load( "queryContext",
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        QueryContext.class ) ) ), GeneratedQueryStructure$.MODULE$.method( "entityAccessor", scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..MODULE$.classType( QueryContext.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( TransactionalEntityFactory.class )),new ExpressionTemplate[0])).put( ExpressionTemplate.self( classHandle ),
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( ArrayList.class,
                    scala.reflect.ManifestFactory..MODULE$.classType( AutoCloseable.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
        "closeables", this.createNewInstance( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( ArrayList.class,
                scala.reflect.ManifestFactory..MODULE$.classType( AutoCloseable.class ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
        scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ))).build();
    }

    public void getOrLoadCursors( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder =
                MethodDeclaration.method( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorFactory.class ) ),
        "getOrLoadCursors", new Parameter[0]);
        CodeBlock x$31 = clazz.generate( methodBuilder );
        Function1 x$32 = ( generate ) -> {
            $anonfun$getOrLoadCursors$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$33 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$31, x$32 );
        Function2 x$34 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$31, x$32 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$31, x$32, x$33, x$34 );
    }

    public void getOrLoadDataRead( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder =
                MethodDeclaration.method( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class ) ),
        "getOrLoadDataRead", new Parameter[0]);
        CodeBlock x$7 = clazz.generate( methodBuilder );
        Function1 x$8 = ( generate ) -> {
            $anonfun$getOrLoadDataRead$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$9 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$7, x$8 );
        Function2 x$10 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$7, x$8 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$7, x$8, x$9, x$10 );
    }

    public void getOrLoadTokenRead( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder =
                MethodDeclaration.method( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ) ),
        "getOrLoadTokenRead", new Parameter[0]);
        CodeBlock x$15 = clazz.generate( methodBuilder );
        Function1 x$16 = ( generate ) -> {
            $anonfun$getOrLoadTokenRead$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$17 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$15, x$16 );
        Function2 x$18 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$15, x$16 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$15, x$16, x$17, x$18 );
    }

    public void getOrLoadSchemaRead( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder =
                MethodDeclaration.method( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( SchemaRead.class ) ),
        "getOrLoadSchemaRead", new Parameter[0]);
        CodeBlock x$23 = clazz.generate( methodBuilder );
        Function1 x$24 = ( generate ) -> {
            $anonfun$getOrLoadSchemaRead$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$25 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$23, x$24 );
        Function2 x$26 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$23, x$24 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$23, x$24, x$25, x$26 );
    }

    public void nodeCursor( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder =
                MethodDeclaration.method( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ) ),
        "nodeCursor", new Parameter[0]);
        CodeBlock x$39 = clazz.generate( methodBuilder );
        Function1 x$40 = ( generate ) -> {
            $anonfun$nodeCursor$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$41 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$39, x$40 );
        Function2 x$42 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$39, x$40 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$39, x$40, x$41, x$42 );
    }

    public void relationshipScanCursor( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder = MethodDeclaration.method(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ) ),
        "relationshipScanCursor", new Parameter[0]);
        CodeBlock x$47 = clazz.generate( methodBuilder );
        Function1 x$48 = ( generate ) -> {
            $anonfun$relationshipScanCursor$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$49 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$47, x$48 );
        Function2 x$50 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$47, x$48 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$47, x$48, x$49, x$50 );
    }

    public void closeCursors( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder = MethodDeclaration.method( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Unit() ),
        "closeCursors", new Parameter[0]);
        CodeBlock x$63 = clazz.generate( methodBuilder );
        Function1 x$64 = ( generate ) -> {
            $anonfun$closeCursors$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$65 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$63, x$64 );
        Function2 x$66 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$63, x$64 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$63, x$64, x$65, x$66 );
    }

    public void propertyCursor( final ClassGenerator clazz, final Fields fields )
    {
        Builder methodBuilder =
                MethodDeclaration.method( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class ) ),
        "propertyCursor", new Parameter[0]);
        CodeBlock x$71 = clazz.generate( methodBuilder );
        Function1 x$72 = ( generate ) -> {
            $anonfun$propertyCursor$1( fields, generate );
            return BoxedUnit.UNIT;
        };
        Function1 x$73 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$71, x$72 );
        Function2 x$74 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$71, x$72 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$71, x$72, x$73, x$74 );
    }

    public MethodTemplate FIELD_NAMES()
    {
        return this.FIELD_NAMES;
    }
}
