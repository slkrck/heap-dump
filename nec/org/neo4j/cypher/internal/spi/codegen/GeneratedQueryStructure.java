package org.neo4j.cypher.internal.spi.codegen;

import org.neo4j.codegen.Expression;
import org.neo4j.codegen.FieldReference;
import org.neo4j.codegen.MethodReference;
import org.neo4j.codegen.Parameter;
import org.neo4j.codegen.TypeReference;
import org.neo4j.cypher.internal.executionplan.GeneratedQuery;
import org.neo4j.cypher.internal.plandescription.Argument;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructureResult;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public final class GeneratedQueryStructure
{
    public static Expression nullValue( final CodeGenType cType )
    {
        return GeneratedQueryStructure$.MODULE$.nullValue( var0 );
    }

    public static TypeReference lowerTypeScalarSubset( final CodeGenType cType )
    {
        return GeneratedQueryStructure$.MODULE$.lowerTypeScalarSubset( var0 );
    }

    public static TypeReference lowerType( final CodeGenType cType )
    {
        return GeneratedQueryStructure$.MODULE$.lowerType( var0 );
    }

    public static TypeReference typeReference( final Manifest<?> manifest )
    {
        return GeneratedQueryStructure$.MODULE$.typeReference( var0 );
    }

    public static <T> TypeReference typeRef( final Manifest<T> manifest )
    {
        return GeneratedQueryStructure$.MODULE$.typeRef( var0 );
    }

    public static <T> Parameter param( final String name, final Manifest<T> manifest )
    {
        return GeneratedQueryStructure$.MODULE$.param( var0, var1 );
    }

    public static <O, R> FieldReference staticField( final String name, final Manifest<O> owner, final Manifest<R> fieldType )
    {
        return GeneratedQueryStructure$.MODULE$.staticField( var0, var1, var2 );
    }

    public static <O, R> MethodReference method( final String name, final Seq<TypeReference> params, final Manifest<O> owner, final Manifest<R> returns )
    {
        return GeneratedQueryStructure$.MODULE$.method( var0, var1, var2, var3 );
    }

    public static GeneratedQueryStructure.GeneratedQueryStructureResult generateQuery( final String className, final Seq<String> columns,
            final Map<String,Id> operatorIds, final CodeGenConfiguration conf, final Function1<MethodStructure<?>,BoxedUnit> methodStructure,
            final CodeGenContext codeGenContext )
    {
        return GeneratedQueryStructure$.MODULE$.generateQuery( var0, var1, var2, var3, var4, var5 );
    }

    public static class GeneratedQueryStructureResult implements CodeStructureResult<GeneratedQuery>, Product, Serializable
    {
        private final GeneratedQuery query;
        private final Seq<Argument> code;

        public GeneratedQueryStructureResult( final GeneratedQuery query, final Seq<Argument> code )
        {
            this.query = query;
            this.code = code;
            Product.$init$( this );
        }

        public GeneratedQuery query()
        {
            return this.query;
        }

        public Seq<Argument> code()
        {
            return this.code;
        }

        public GeneratedQueryStructure.GeneratedQueryStructureResult copy( final GeneratedQuery query, final Seq<Argument> code )
        {
            return new GeneratedQueryStructure.GeneratedQueryStructureResult( query, code );
        }

        public GeneratedQuery copy$default$1()
        {
            return this.query();
        }

        public Seq<Argument> copy$default$2()
        {
            return this.code();
        }

        public String productPrefix()
        {
            return "GeneratedQueryStructureResult";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.query();
                break;
            case 1:
                var10000 = this.code();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof GeneratedQueryStructure.GeneratedQueryStructureResult;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof GeneratedQueryStructure.GeneratedQueryStructureResult )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                GeneratedQueryStructure.GeneratedQueryStructureResult var4 = (GeneratedQueryStructure.GeneratedQueryStructureResult) x$1;
                                GeneratedQuery var10000 = this.query();
                                GeneratedQuery var5 = var4.query();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                Seq var7 = this.code();
                                Seq var6 = var4.code();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class GeneratedQueryStructureResult$
            extends AbstractFunction2<GeneratedQuery,Seq<Argument>,GeneratedQueryStructure.GeneratedQueryStructureResult> implements Serializable
    {
        public static GeneratedQueryStructure.GeneratedQueryStructureResult$ MODULE$;

        static
        {
            new GeneratedQueryStructure.GeneratedQueryStructureResult$();
        }

        public GeneratedQueryStructureResult$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "GeneratedQueryStructureResult";
        }

        public GeneratedQueryStructure.GeneratedQueryStructureResult apply( final GeneratedQuery query, final Seq<Argument> code )
        {
            return new GeneratedQueryStructure.GeneratedQueryStructureResult( query, code );
        }

        public Option<Tuple2<GeneratedQuery,Seq<Argument>>> unapply( final GeneratedQueryStructure.GeneratedQueryStructureResult x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.query(), x$0.code() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
