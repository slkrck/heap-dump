package org.neo4j.cypher.internal.spi.codegen;

import org.neo4j.codegen.Binding;
import org.neo4j.codegen.ClassGenerator;
import org.neo4j.codegen.ClassHandle;
import org.neo4j.codegen.CodeBlock;
import org.neo4j.codegen.CodeGenerator;
import org.neo4j.codegen.Expression;
import org.neo4j.codegen.ExpressionTemplate;
import org.neo4j.codegen.FieldReference;
import org.neo4j.codegen.LocalVariable;
import org.neo4j.codegen.MethodTemplate;
import org.neo4j.codegen.Parameter;
import org.neo4j.codegen.TypeReference;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Ascending$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Descending$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.HashableTupleDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.OrderableTupleDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SimpleTupleDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortOrder;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor;
import org.neo4j.cypher.internal.v4_0.frontend.helpers.package.;
import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.mutable.Map;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class AuxGenerator
{
    private final String packageName;
    private final CodeGenerator generator;
    private final Map<? super TupleDescriptor,TypeReference> types;
    private int nameId;

    public AuxGenerator( final String packageName, final CodeGenerator generator, final CodeGenContext context )
    {
        this.packageName = packageName;
        this.generator = generator;
        this.types = scala.collection.mutable.Map..MODULE$.empty();
        this.nameId = 0;
    }

    private static final Tuple2 extractFields$1( final CodeBlock block, final RepresentationType reprType, final String otherTupleName$1,
            final FieldReference fieldReference$1 )
    {
        boolean isPrimitive = RepresentationType$.MODULE$.isPrimitive( reprType );
        Expression thisField = Expression.get( block.self(), fieldReference$1 );
        Expression otherField = Expression.get( block.load( otherTupleName$1 ), fieldReference$1 );
        return isPrimitive ? new Tuple2( thisField, otherField ) : new Tuple2( Expression.box( thisField ), Expression.box( otherField ) );
    }

    private static final Tuple2 assignComparatorVariablesFor$1( final CodeBlock block, final RepresentationType reprType, final String otherTupleName$1,
            final TypeReference fieldType$1, final FieldReference fieldReference$1, final String thisValueName$1, final String otherValueName$1 )
    {
        Tuple2 var9 = extractFields$1( block, reprType, otherTupleName$1, fieldReference$1 );
        if ( var9 != null )
        {
            Expression thisField = (Expression) var9._1();
            Expression otherField = (Expression) var9._2();
            Tuple2 var7 = new Tuple2( thisField, otherField );
            Expression thisField = (Expression) var7._1();
            Expression otherField = (Expression) var7._2();
            LocalVariable thisValueVariable = block.declare( fieldType$1, thisValueName$1 );
            LocalVariable otherValueVariable = block.declare( fieldType$1, otherValueName$1 );
            block.assign( thisValueVariable, thisField );
            block.assign( otherValueVariable, otherField );
            return new Tuple2( thisValueVariable, otherValueVariable );
        }
        else
        {
            throw new MatchError( var9 );
        }
    }

    private static final Tuple2 rearrangeInSortOrder$1( final Expression lhs, final Expression rhs, final SortOrder sortOrder )
    {
        Tuple2 var3;
        if ( Ascending$.MODULE$.equals( sortOrder ) )
        {
            var3 = new Tuple2( lhs, rhs );
        }
        else
        {
            if ( !Descending$.MODULE$.equals( sortOrder ) )
            {
                throw new MatchError( sortOrder );
            }

            var3 = new Tuple2( rhs, lhs );
        }

        return var3;
    }

    public String packageName()
    {
        return this.packageName;
    }

    public CodeGenerator generator()
    {
        return this.generator;
    }

    private Map<? super TupleDescriptor,TypeReference> types()
    {
        return this.types;
    }

    private int nameId()
    {
        return this.nameId;
    }

    private void nameId_$eq( final int x$1 )
    {
        this.nameId = x$1;
    }

    public TypeReference typeReference( final TupleDescriptor tupleDescriptor )
    {
        TypeReference var2;
        if ( tupleDescriptor instanceof SimpleTupleDescriptor )
        {
            SimpleTupleDescriptor var4 = (SimpleTupleDescriptor) tupleDescriptor;
            var2 = this.simpleTypeReference( var4 );
        }
        else if ( tupleDescriptor instanceof HashableTupleDescriptor )
        {
            HashableTupleDescriptor var5 = (HashableTupleDescriptor) tupleDescriptor;
            var2 = this.hashableTypeReference( var5 );
        }
        else
        {
            if ( !(tupleDescriptor instanceof OrderableTupleDescriptor) )
            {
                throw new MatchError( tupleDescriptor );
            }

            OrderableTupleDescriptor var6 = (OrderableTupleDescriptor) tupleDescriptor;
            var2 = this.comparableTypeReference( var6 );
        }

        return var2;
    }

    public TypeReference simpleTypeReference( final SimpleTupleDescriptor tupleDescriptor )
    {
        return (TypeReference) this.types().getOrElseUpdate( tupleDescriptor, () -> {
            ClassGenerator x$9 = this.generator().generateClass( this.packageName(), this.newSimpleTupleTypeName(), new TypeReference[0] );
            Function1 x$10 = ( clazz ) -> {
                tupleDescriptor.structure().foreach( ( x0$1 ) -> {
                    if ( x0$1 != null )
                    {
                        String fieldName = (String) x0$1._1();
                        CodeGenType fieldType = (CodeGenType) x0$1._2();
                        if ( fieldType != null )
                        {
                            FieldReference var2 = clazz.field( GeneratedQueryStructure$.MODULE$.lowerType( fieldType ), fieldName );
                            return var2;
                        }
                    }

                    throw new MatchError( x0$1 );
                } );
                return clazz.handle();
            };
            Function1 x$11 = .MODULE$.using$default$3( x$9, x$10 );
            Function2 x$12 = .MODULE$.using$default$4( x$9, x$10 );
            return (ClassHandle).MODULE$.using( x$9, x$10, x$11, x$12 );
        } );
    }

    public TypeReference hashableTypeReference( final HashableTupleDescriptor tupleDescriptor )
    {
        return (TypeReference) this.types().getOrElseUpdate( tupleDescriptor, () -> {
            ClassGenerator x$17 = this.generator().generateClass( this.packageName(), this.newHashableTupleTypeName(), new TypeReference[0] );
            Function1 x$18 = ( clazz ) -> {
                tupleDescriptor.structure().foreach( ( x0$2 ) -> {
                    if ( x0$2 != null )
                    {
                        String fieldName = (String) x0$2._1();
                        CodeGenType fieldType = (CodeGenType) x0$2._2();
                        FieldReference var2 = clazz.field( GeneratedQueryStructure$.MODULE$.lowerType( fieldType ), fieldName );
                        return var2;
                    }
                    else
                    {
                        throw new MatchError( x0$2 );
                    }
                } );
                clazz.field( Integer.TYPE, "hashCode" );
                clazz.generate( MethodTemplate.method( Integer.TYPE, "hashCode", new Parameter[0] ).returns(
                        ExpressionTemplate.get( ExpressionTemplate.self( clazz.handle() ), Integer.TYPE, "hashCode" ) ).build(), new Binding[0] );
                CodeBlock x$13 = clazz.generateMethod( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean() ),
                "equals", new Parameter[]{Parameter.param( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() ), "other")})
                ;
                Function1 x$14 = ( body ) -> {
                    $anonfun$hashableTypeReference$4( this, tupleDescriptor, clazz, body );
                    return BoxedUnit.UNIT;
                };
                Function1 x$15 = .MODULE$.using$default$3( x$13, x$14 );
                Function2 x$16 = .MODULE$.using$default$4( x$13, x$14 );
            .MODULE$.using( x$13, x$14, x$15, x$16 );
                return clazz.handle();
            }; Function1 x$19 = .MODULE$.using$default$3( x$17, x$18 );
            Function2 x$20 = .MODULE$.using$default$4( x$17, x$18 );
            return (ClassHandle).MODULE$.using( x$17, x$18, x$19, x$20 );
        } );
    }

    private int sortResultSign( final SortOrder sortOrder )
    {
        byte var2;
        if ( Ascending$.MODULE$.equals( sortOrder ) )
        {
            var2 = -1;
        }
        else
        {
            if ( !Descending$.MODULE$.equals( sortOrder ) )
            {
                throw new MatchError( sortOrder );
            }

            var2 = 1;
        }

        return var2;
    }

    private int lessThanSortResult( final SortOrder sortOrder )
    {
        return this.sortResultSign( sortOrder );
    }

    private int greaterThanSortResult( final SortOrder sortOrder )
    {
        return -this.sortResultSign( sortOrder );
    }

    public TypeReference comparableTypeReference( final OrderableTupleDescriptor tupleDescriptor )
    {
        return (TypeReference) this.types().getOrElseUpdate( tupleDescriptor, () -> {
            ClassGenerator x$49 = this.generator().generateClass( this.packageName(), this.newComparableTupleTypeName(), new TypeReference[]{
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Comparable.class,
                            scala.reflect.ManifestFactory..MODULE$.wildcardType( scala.reflect.ManifestFactory..MODULE$.Nothing(),
                    scala.reflect.ManifestFactory..MODULE$.Any() ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )))});
            Function1 x$50 = ( clazz ) -> {
                tupleDescriptor.structure().foreach( ( x0$4 ) -> {
                    if ( x0$4 != null )
                    {
                        String fieldName = (String) x0$4._1();
                        CodeGenType fieldType = (CodeGenType) x0$4._2();
                        if ( fieldType != null )
                        {
                            FieldReference var2 = clazz.field( GeneratedQueryStructure$.MODULE$.lowerType( fieldType ), fieldName );
                            return var2;
                        }
                    }

                    throw new MatchError( x0$4 );
                } );
                CodeBlock x$45 = clazz.generateMethod( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
                "compareTo", new Parameter[]{Parameter.param( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() ),
                        "other")});
                Function1 x$46 = ( l1 ) -> {
                    $anonfun$comparableTypeReference$4( this, tupleDescriptor, clazz, l1 );
                    return BoxedUnit.UNIT;
                };
                Function1 x$47 = .MODULE$.using$default$3( x$45, x$46 );
                Function2 x$48 = .MODULE$.using$default$4( x$45, x$46 );
            .MODULE$.using( x$45, x$46, x$47, x$48 );
                return clazz.handle();
            }; Function1 x$51 = .MODULE$.using$default$3( x$49, x$50 );
            Function2 x$52 = .MODULE$.using$default$4( x$49, x$50 );
            return (ClassHandle).MODULE$.using( x$49, x$50, x$51, x$52 );
        } );
    }

    private String newSimpleTupleTypeName()
    {
        String name = (new StringBuilder( 15 )).append( "SimpleTupleType" ).append( this.nameId() ).toString();
        this.nameId_$eq( this.nameId() + 1 );
        return name;
    }

    private String newHashableTupleTypeName()
    {
        String name = (new StringBuilder( 17 )).append( "HashableTupleType" ).append( this.nameId() ).toString();
        this.nameId_$eq( this.nameId() + 1 );
        return name;
    }

    private String newComparableTupleTypeName()
    {
        String name = (new StringBuilder( 19 )).append( "ComparableTupleType" ).append( this.nameId() ).toString();
        this.nameId_$eq( this.nameId() + 1 );
        return name;
    }
}
