package org.neo4j.cypher.internal.spi.codegen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.collections.api.iterator.LongIterator;
import org.eclipse.collections.impl.map.mutable.primitive.LongIntHashMap;
import org.neo4j.codegen.MethodReference;
import org.neo4j.codegen.TypeReference;
import org.neo4j.cypher.internal.codegen.CompiledConversionUtils;
import org.neo4j.cypher.internal.codegen.CompiledExpandUtils;
import org.neo4j.cypher.internal.codegen.CompiledMathHelper;
import org.neo4j.cypher.internal.codegen.CompiledConversionUtils.CompositeKey;
import org.neo4j.cypher.internal.javacompat.ResultRecord;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.RelationshipIterator;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.cypher.result.QueryResult.QueryResultVisitor;
import org.neo4j.cypher.result.QueryResult.Record;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.internal.helpers.collection.MapUtil;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.TokenRead;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.kernel.impl.api.RelationshipDataExtractor;
import org.neo4j.kernel.impl.core.TransactionalEntityFactory;
import org.neo4j.storageengine.api.RelationshipVisitor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import org.neo4j.values.virtual.VirtualNodeValue;
import org.neo4j.values.virtual.VirtualRelationshipValue;
import scala.Predef.;
import scala.reflect.Manifest;

public final class Methods$
{
    public static Methods$ MODULE$;

    static
    {
        new Methods$();
    }

    private final MethodReference countingTableIncrement;
    private final MethodReference countingTableCompositeKeyPut;
    private final MethodReference countingTableGet;
    private final MethodReference countingTableCompositeKeyGet;
    private final MethodReference compositeKey;
    private final MethodReference hasNextLong;
    private final MethodReference hasMoreRelationship;
    private final MethodReference createMap;
    private final MethodReference createAnyValueMap;
    private final MethodReference format;
    private final MethodReference relationshipVisit;
    private final MethodReference getRelationship;
    private final MethodReference startNode;
    private final MethodReference endNode;
    private final MethodReference typeOf;
    private final MethodReference allConnectingRelationships;
    private final MethodReference connectingRelationships;
    private final MethodReference mathAdd;
    private final MethodReference mathSub;
    private final MethodReference mathMul;
    private final MethodReference mathPow;
    private final MethodReference mathDiv;
    private final MethodReference mathMod;
    private final MethodReference mathCastToInt;
    private final MethodReference mathCastToLong;
    private final MethodReference mathCastToLongOrFail;
    private final MethodReference mapGet;
    private final MethodReference mapContains;
    private final MethodReference setContains;
    private final MethodReference setAdd;
    private final MethodReference listAdd;
    private final MethodReference labelGetForName;
    private final MethodReference propertyKeyGetForName;
    private final MethodReference coerceToPredicate;
    private final MethodReference ternaryEquals;
    private final MethodReference equals;
    private final MethodReference or;
    private final MethodReference not;
    private final MethodReference relationshipTypeGetForName;
    private final MethodReference relationshipTypeGetName;
    private final MethodReference nodeExists;
    private final MethodReference countsForNode;
    private final MethodReference countsForRel;
    private final MethodReference nextLong;
    private final MethodReference fetchNextRelationship;
    private final MethodReference newNodeEntityById;
    private final MethodReference newRelationshipEntityById;
    private final MethodReference materializeAnyResult;
    private final MethodReference materializeAnyValueResult;
    private final MethodReference materializeNodeValue;
    private final MethodReference materializeRelationshipValue;
    private final MethodReference nodeId;
    private final MethodReference relId;
    private final MethodReference set;
    private final MethodReference visit;
    private final MethodReference executeOperator;
    private final MethodReference dbHit;
    private final MethodReference row;
    private final MethodReference unboxInteger;
    private final MethodReference unboxBoolean;
    private final MethodReference unboxLong;
    private final MethodReference unboxDouble;
    private final MethodReference unboxNode;
    private final MethodReference unboxRel;
    private final MethodReference reboxValue;

    private Methods$()
    {
        MODULE$ = this;
        this.countingTableIncrement = GeneratedQueryStructure$.MODULE$.method( "addToValue",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( LongIntHashMap.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.countingTableCompositeKeyPut = GeneratedQueryStructure$.MODULE$.method( "put",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( HashMap.class, scala.reflect.ManifestFactory..MODULE$.classType( CompositeKey.class ), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Integer.class )}))),scala.reflect.ManifestFactory..
        MODULE$.Object());
        this.countingTableGet = GeneratedQueryStructure$.MODULE$.method( "get",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
        scala.reflect.ManifestFactory..MODULE$.classType( LongIntHashMap.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.countingTableCompositeKeyGet = GeneratedQueryStructure$.MODULE$.method( "get",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( HashMap.class, scala.reflect.ManifestFactory..MODULE$.classType( CompositeKey.class ), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Integer.class )}))),scala.reflect.ManifestFactory..
        MODULE$.Object());
        this.compositeKey = GeneratedQueryStructure$.MODULE$.method( "compositeKey",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Long()) )})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( CompositeKey.class ));
        this.hasNextLong =
                GeneratedQueryStructure$.MODULE$.method( "hasNext",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( LongIterator.class ), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.hasMoreRelationship =
                GeneratedQueryStructure$.MODULE$.method( "hasNext",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipIterator.class ), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.createMap = GeneratedQueryStructure$.MODULE$.method( "map",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Object()) )})),
        scala.reflect.ManifestFactory..MODULE$.classType( MapUtil.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Map.class, scala.reflect.ManifestFactory..MODULE$.classType( String.class ), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.Object()}))));
        this.createAnyValueMap = GeneratedQueryStructure$.MODULE$.method( "genericMap",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Object()) )})),
        scala.reflect.ManifestFactory..MODULE$.classType( MapUtil.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Map.class, scala.reflect.ManifestFactory..MODULE$.classType( String.class ), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )}))));
        this.format = GeneratedQueryStructure$.MODULE$.method( "format",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class )),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Object() ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( String.class ), scala.reflect.ManifestFactory..MODULE$.classType( String.class ));
        this.relationshipVisit = GeneratedQueryStructure$.MODULE$.method( "relationshipVisit",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( RelationshipVisitor.class,
                        scala.reflect.ManifestFactory..MODULE$.classType( RuntimeException.class ),.MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )))})),
        scala.reflect.ManifestFactory..MODULE$.classType( RelationshipIterator.class ), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.getRelationship = GeneratedQueryStructure$.MODULE$.method( "relationship",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipDataExtractor.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.startNode =
                GeneratedQueryStructure$.MODULE$.method( "startNode",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipDataExtractor.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.endNode =
                GeneratedQueryStructure$.MODULE$.method( "endNode",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipDataExtractor.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.typeOf = GeneratedQueryStructure$.MODULE$.method( "type",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipDataExtractor.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.allConnectingRelationships = GeneratedQueryStructure$.MODULE$.method( "connectingRelationships",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        CursorFactory.class ) ), GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Direction.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledExpandUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ));
        this.connectingRelationships = GeneratedQueryStructure$.MODULE$.method( "connectingRelationships",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        CursorFactory.class ) ), GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Direction.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()))})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledExpandUtils.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipSelectionCursor.class ));
        this.mathAdd = GeneratedQueryStructure$.MODULE$.method( "add",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Object());
        this.mathSub = GeneratedQueryStructure$.MODULE$.method( "subtract",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Object());
        this.mathMul = GeneratedQueryStructure$.MODULE$.method( "multiply",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Object());
        this.mathPow = GeneratedQueryStructure$.MODULE$.method( "pow",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Object());
        this.mathDiv = GeneratedQueryStructure$.MODULE$.method( "divide",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Object());
        this.mathMod = GeneratedQueryStructure$.MODULE$.method( "modulo",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Object());
        this.mathCastToInt = GeneratedQueryStructure$.MODULE$.method( "transformToInt",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.mathCastToLong = GeneratedQueryStructure$.MODULE$.method( "transformToLong",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.mathCastToLongOrFail = GeneratedQueryStructure$.MODULE$.method( "transformToLongOrFail",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ) )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledMathHelper.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.mapGet = GeneratedQueryStructure$.MODULE$.method( "get",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( Map.class, scala.reflect.ManifestFactory..MODULE$.classType( String.class ), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.Object()}))),scala.reflect.ManifestFactory..MODULE$.Object());
        this.mapContains = GeneratedQueryStructure$.MODULE$.method( "containsKey",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( Map.class, scala.reflect.ManifestFactory..MODULE$.classType( String.class ), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.Object()}))),scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.setContains = GeneratedQueryStructure$.MODULE$.method( "contains",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( Set.class, scala.reflect.ManifestFactory..MODULE$.Object(), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.setAdd = GeneratedQueryStructure$.MODULE$.method( "add",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( Set.class, scala.reflect.ManifestFactory..MODULE$.Object(), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.listAdd = GeneratedQueryStructure$.MODULE$.method( "add",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( List.class, scala.reflect.ManifestFactory..MODULE$.Object(), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.labelGetForName = GeneratedQueryStructure$.MODULE$.method( "nodeLabel",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.propertyKeyGetForName = GeneratedQueryStructure$.MODULE$.method( "propertyKey",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.coerceToPredicate = GeneratedQueryStructure$.MODULE$.method( "coerceToPredicate",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.ternaryEquals = GeneratedQueryStructure$.MODULE$.method( "equals",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ));
        this.equals = GeneratedQueryStructure$.MODULE$.method( "equals",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.Object(), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.or = GeneratedQueryStructure$.MODULE$.method( "or",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ));
        this.not = GeneratedQueryStructure$.MODULE$.method( "not",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ));
        this.relationshipTypeGetForName = GeneratedQueryStructure$.MODULE$.method( "relationshipType",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.relationshipTypeGetName = GeneratedQueryStructure$.MODULE$.method( "relationshipTypeName",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int())})),
        scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ), scala.reflect.ManifestFactory..MODULE$.classType( String.class ));
        this.nodeExists = GeneratedQueryStructure$.MODULE$.method( "nodeExists",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
        scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.countsForNode = GeneratedQueryStructure$.MODULE$.method( "countsForNode",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int())})),
        scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.countsForRel = GeneratedQueryStructure$.MODULE$.method( "countsForRelationship",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ), GeneratedQueryStructure$.MODULE$.typeRef(
                scala.reflect.ManifestFactory..MODULE$.Int())})),scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..
        MODULE$.Long());
        this.nextLong =
                GeneratedQueryStructure$.MODULE$.method( "next",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( LongIterator.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.fetchNextRelationship =
                GeneratedQueryStructure$.MODULE$.method( "next",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( RelationshipIterator.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.newNodeEntityById = GeneratedQueryStructure$.MODULE$.method( "newNodeEntity",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
        scala.reflect.ManifestFactory..MODULE$.classType( TransactionalEntityFactory.class ), scala.reflect.ManifestFactory..MODULE$.classType( Node.class ));
        this.newRelationshipEntityById = GeneratedQueryStructure$.MODULE$.method( "newRelationshipEntity",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
        scala.reflect.ManifestFactory..MODULE$.classType( TransactionalEntityFactory.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Relationship.class ));
        this.materializeAnyResult = GeneratedQueryStructure$.MODULE$.method( "materializeAnyResult",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TransactionalEntityFactory.class )),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
        this.materializeAnyValueResult = GeneratedQueryStructure$.MODULE$.method( "materializeAnyValueResult",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TransactionalEntityFactory.class )),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ));
        this.materializeNodeValue = GeneratedQueryStructure$.MODULE$.method( "materializeNodeValue",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TransactionalEntityFactory.class )),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class ));
        this.materializeRelationshipValue = GeneratedQueryStructure$.MODULE$.method( "materializeRelationshipValue",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TransactionalEntityFactory.class )),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( RelationshipValue.class ));
        this.nodeId = GeneratedQueryStructure$.MODULE$.method( "id",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( VirtualNodeValue.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.relId = GeneratedQueryStructure$.MODULE$.method( "id",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( VirtualRelationshipValue.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.set = GeneratedQueryStructure$.MODULE$.method( "set",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int()),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ) )})),scala.reflect.ManifestFactory..
        MODULE$.classType( ResultRecord.class ), scala.reflect.ManifestFactory..MODULE$.Unit());
        this.visit = GeneratedQueryStructure$.MODULE$.method( "visit",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Record.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( QueryResultVisitor.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
            scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.classType( Exception.class )), .
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean());
        this.executeOperator = GeneratedQueryStructure$.MODULE$.method( "executeOperator",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Id.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( QueryProfiler.class ), scala.reflect.ManifestFactory..MODULE$.classType( OperatorProfileEvent.class ))
        ;
        this.dbHit = GeneratedQueryStructure$.MODULE$.method( "dbHit",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit());
        this.row = GeneratedQueryStructure$.MODULE$.method( "row",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( OperatorProfileEvent.class ), scala.reflect.ManifestFactory..MODULE$.Unit());
        this.unboxInteger =
                GeneratedQueryStructure$.MODULE$.method( "intValue",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( Integer.class ), scala.reflect.ManifestFactory..MODULE$.Int());
        this.unboxBoolean = GeneratedQueryStructure$.MODULE$.method( "booleanValue",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ), scala.reflect.ManifestFactory..
        MODULE$.Boolean());
        this.unboxLong =
                GeneratedQueryStructure$.MODULE$.method( "longValue",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( Long.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.unboxDouble = GeneratedQueryStructure$.MODULE$.method( "doubleValue",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..MODULE$.classType( Double.class ), scala.reflect.ManifestFactory..
        MODULE$.Double());
        this.unboxNode = GeneratedQueryStructure$.MODULE$.method( "unboxNodeOrNull",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( VirtualNodeValue.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.unboxRel = GeneratedQueryStructure$.MODULE$.method( "unboxRelationshipOrNull",.MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( VirtualRelationshipValue.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..MODULE$.Long());
        this.reboxValue = GeneratedQueryStructure$.MODULE$.method( "asObject",.MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Value.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.Object());
    }

    public MethodReference countingTableIncrement()
    {
        return this.countingTableIncrement;
    }

    public MethodReference countingTableCompositeKeyPut()
    {
        return this.countingTableCompositeKeyPut;
    }

    public MethodReference countingTableGet()
    {
        return this.countingTableGet;
    }

    public MethodReference countingTableCompositeKeyGet()
    {
        return this.countingTableCompositeKeyGet;
    }

    public MethodReference compositeKey()
    {
        return this.compositeKey;
    }

    public MethodReference hasNextLong()
    {
        return this.hasNextLong;
    }

    public MethodReference hasMoreRelationship()
    {
        return this.hasMoreRelationship;
    }

    public MethodReference createMap()
    {
        return this.createMap;
    }

    public MethodReference createAnyValueMap()
    {
        return this.createAnyValueMap;
    }

    public MethodReference format()
    {
        return this.format;
    }

    public MethodReference relationshipVisit()
    {
        return this.relationshipVisit;
    }

    public MethodReference getRelationship()
    {
        return this.getRelationship;
    }

    public MethodReference startNode()
    {
        return this.startNode;
    }

    public MethodReference endNode()
    {
        return this.endNode;
    }

    public MethodReference typeOf()
    {
        return this.typeOf;
    }

    public MethodReference allConnectingRelationships()
    {
        return this.allConnectingRelationships;
    }

    public MethodReference connectingRelationships()
    {
        return this.connectingRelationships;
    }

    public MethodReference mathAdd()
    {
        return this.mathAdd;
    }

    public MethodReference mathSub()
    {
        return this.mathSub;
    }

    public MethodReference mathMul()
    {
        return this.mathMul;
    }

    public MethodReference mathPow()
    {
        return this.mathPow;
    }

    public MethodReference mathDiv()
    {
        return this.mathDiv;
    }

    public MethodReference mathMod()
    {
        return this.mathMod;
    }

    public MethodReference mathCastToInt()
    {
        return this.mathCastToInt;
    }

    public MethodReference mathCastToLong()
    {
        return this.mathCastToLong;
    }

    public MethodReference mathCastToLongOrFail()
    {
        return this.mathCastToLongOrFail;
    }

    public MethodReference mapGet()
    {
        return this.mapGet;
    }

    public MethodReference mapContains()
    {
        return this.mapContains;
    }

    public MethodReference setContains()
    {
        return this.setContains;
    }

    public MethodReference setAdd()
    {
        return this.setAdd;
    }

    public MethodReference listAdd()
    {
        return this.listAdd;
    }

    public MethodReference labelGetForName()
    {
        return this.labelGetForName;
    }

    public MethodReference propertyKeyGetForName()
    {
        return this.propertyKeyGetForName;
    }

    public MethodReference coerceToPredicate()
    {
        return this.coerceToPredicate;
    }

    public MethodReference ternaryEquals()
    {
        return this.ternaryEquals;
    }

    public MethodReference equals()
    {
        return this.equals;
    }

    public MethodReference or()
    {
        return this.or;
    }

    public MethodReference not()
    {
        return this.not;
    }

    public MethodReference relationshipTypeGetForName()
    {
        return this.relationshipTypeGetForName;
    }

    public MethodReference relationshipTypeGetName()
    {
        return this.relationshipTypeGetName;
    }

    public MethodReference nodeExists()
    {
        return this.nodeExists;
    }

    public MethodReference countsForNode()
    {
        return this.countsForNode;
    }

    public MethodReference countsForRel()
    {
        return this.countsForRel;
    }

    public MethodReference nextLong()
    {
        return this.nextLong;
    }

    public MethodReference fetchNextRelationship()
    {
        return this.fetchNextRelationship;
    }

    public MethodReference newNodeEntityById()
    {
        return this.newNodeEntityById;
    }

    public MethodReference newRelationshipEntityById()
    {
        return this.newRelationshipEntityById;
    }

    public MethodReference materializeAnyResult()
    {
        return this.materializeAnyResult;
    }

    public MethodReference materializeAnyValueResult()
    {
        return this.materializeAnyValueResult;
    }

    public MethodReference materializeNodeValue()
    {
        return this.materializeNodeValue;
    }

    public MethodReference materializeRelationshipValue()
    {
        return this.materializeRelationshipValue;
    }

    public MethodReference nodeId()
    {
        return this.nodeId;
    }

    public MethodReference relId()
    {
        return this.relId;
    }

    public MethodReference set()
    {
        return this.set;
    }

    public MethodReference visit()
    {
        return this.visit;
    }

    public MethodReference executeOperator()
    {
        return this.executeOperator;
    }

    public MethodReference dbHit()
    {
        return this.dbHit;
    }

    public MethodReference row()
    {
        return this.row;
    }

    public MethodReference unboxInteger()
    {
        return this.unboxInteger;
    }

    public MethodReference unboxBoolean()
    {
        return this.unboxBoolean;
    }

    public MethodReference unboxLong()
    {
        return this.unboxLong;
    }

    public MethodReference unboxDouble()
    {
        return this.unboxDouble;
    }

    public MethodReference unboxNode()
    {
        return this.unboxNode;
    }

    public MethodReference unboxRel()
    {
        return this.unboxRel;
    }

    public MethodReference reboxValue()
    {
        return this.reboxValue;
    }
}
