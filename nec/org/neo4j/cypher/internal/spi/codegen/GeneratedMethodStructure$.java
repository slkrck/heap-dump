package org.neo4j.cypher.internal.spi.codegen;

import org.neo4j.codegen.LocalVariable;
import scala.collection.immutable.List;
import scala.collection.immutable.List.;
import scala.collection.mutable.Map;

public final class GeneratedMethodStructure$
{
    public static GeneratedMethodStructure$ MODULE$;

    static
    {
        new GeneratedMethodStructure$();
    }

    private GeneratedMethodStructure$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$4()
    {
        return true;
    }

    public List<String> $lessinit$greater$default$5()
    {
        return .MODULE$.empty();
    }

    public Map<String,LocalVariable> $lessinit$greater$default$6()
    {
        return scala.collection.mutable.Map..MODULE$.empty();
    }
}
