package org.neo4j.cypher.internal.spi.codegen;

import org.neo4j.codegen.MethodReference;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class Methods
{
    public static MethodReference reboxValue()
    {
        return Methods$.MODULE$.reboxValue();
    }

    public static MethodReference unboxRel()
    {
        return Methods$.MODULE$.unboxRel();
    }

    public static MethodReference unboxNode()
    {
        return Methods$.MODULE$.unboxNode();
    }

    public static MethodReference unboxDouble()
    {
        return Methods$.MODULE$.unboxDouble();
    }

    public static MethodReference unboxLong()
    {
        return Methods$.MODULE$.unboxLong();
    }

    public static MethodReference unboxBoolean()
    {
        return Methods$.MODULE$.unboxBoolean();
    }

    public static MethodReference unboxInteger()
    {
        return Methods$.MODULE$.unboxInteger();
    }

    public static MethodReference row()
    {
        return Methods$.MODULE$.row();
    }

    public static MethodReference dbHit()
    {
        return Methods$.MODULE$.dbHit();
    }

    public static MethodReference executeOperator()
    {
        return Methods$.MODULE$.executeOperator();
    }

    public static MethodReference visit()
    {
        return Methods$.MODULE$.visit();
    }

    public static MethodReference set()
    {
        return Methods$.MODULE$.set();
    }

    public static MethodReference relId()
    {
        return Methods$.MODULE$.relId();
    }

    public static MethodReference nodeId()
    {
        return Methods$.MODULE$.nodeId();
    }

    public static MethodReference materializeRelationshipValue()
    {
        return Methods$.MODULE$.materializeRelationshipValue();
    }

    public static MethodReference materializeNodeValue()
    {
        return Methods$.MODULE$.materializeNodeValue();
    }

    public static MethodReference materializeAnyValueResult()
    {
        return Methods$.MODULE$.materializeAnyValueResult();
    }

    public static MethodReference materializeAnyResult()
    {
        return Methods$.MODULE$.materializeAnyResult();
    }

    public static MethodReference newRelationshipEntityById()
    {
        return Methods$.MODULE$.newRelationshipEntityById();
    }

    public static MethodReference newNodeEntityById()
    {
        return Methods$.MODULE$.newNodeEntityById();
    }

    public static MethodReference fetchNextRelationship()
    {
        return Methods$.MODULE$.fetchNextRelationship();
    }

    public static MethodReference nextLong()
    {
        return Methods$.MODULE$.nextLong();
    }

    public static MethodReference countsForRel()
    {
        return Methods$.MODULE$.countsForRel();
    }

    public static MethodReference countsForNode()
    {
        return Methods$.MODULE$.countsForNode();
    }

    public static MethodReference nodeExists()
    {
        return Methods$.MODULE$.nodeExists();
    }

    public static MethodReference relationshipTypeGetName()
    {
        return Methods$.MODULE$.relationshipTypeGetName();
    }

    public static MethodReference relationshipTypeGetForName()
    {
        return Methods$.MODULE$.relationshipTypeGetForName();
    }

    public static MethodReference not()
    {
        return Methods$.MODULE$.not();
    }

    public static MethodReference or()
    {
        return Methods$.MODULE$.or();
    }

    public static MethodReference equals()
    {
        return Methods$.MODULE$.equals();
    }

    public static MethodReference ternaryEquals()
    {
        return Methods$.MODULE$.ternaryEquals();
    }

    public static MethodReference coerceToPredicate()
    {
        return Methods$.MODULE$.coerceToPredicate();
    }

    public static MethodReference propertyKeyGetForName()
    {
        return Methods$.MODULE$.propertyKeyGetForName();
    }

    public static MethodReference labelGetForName()
    {
        return Methods$.MODULE$.labelGetForName();
    }

    public static MethodReference listAdd()
    {
        return Methods$.MODULE$.listAdd();
    }

    public static MethodReference setAdd()
    {
        return Methods$.MODULE$.setAdd();
    }

    public static MethodReference setContains()
    {
        return Methods$.MODULE$.setContains();
    }

    public static MethodReference mapContains()
    {
        return Methods$.MODULE$.mapContains();
    }

    public static MethodReference mapGet()
    {
        return Methods$.MODULE$.mapGet();
    }

    public static MethodReference mathCastToLongOrFail()
    {
        return Methods$.MODULE$.mathCastToLongOrFail();
    }

    public static MethodReference mathCastToLong()
    {
        return Methods$.MODULE$.mathCastToLong();
    }

    public static MethodReference mathCastToInt()
    {
        return Methods$.MODULE$.mathCastToInt();
    }

    public static MethodReference mathMod()
    {
        return Methods$.MODULE$.mathMod();
    }

    public static MethodReference mathDiv()
    {
        return Methods$.MODULE$.mathDiv();
    }

    public static MethodReference mathPow()
    {
        return Methods$.MODULE$.mathPow();
    }

    public static MethodReference mathMul()
    {
        return Methods$.MODULE$.mathMul();
    }

    public static MethodReference mathSub()
    {
        return Methods$.MODULE$.mathSub();
    }

    public static MethodReference mathAdd()
    {
        return Methods$.MODULE$.mathAdd();
    }

    public static MethodReference connectingRelationships()
    {
        return Methods$.MODULE$.connectingRelationships();
    }

    public static MethodReference allConnectingRelationships()
    {
        return Methods$.MODULE$.allConnectingRelationships();
    }

    public static MethodReference typeOf()
    {
        return Methods$.MODULE$.typeOf();
    }

    public static MethodReference endNode()
    {
        return Methods$.MODULE$.endNode();
    }

    public static MethodReference startNode()
    {
        return Methods$.MODULE$.startNode();
    }

    public static MethodReference getRelationship()
    {
        return Methods$.MODULE$.getRelationship();
    }

    public static MethodReference relationshipVisit()
    {
        return Methods$.MODULE$.relationshipVisit();
    }

    public static MethodReference format()
    {
        return Methods$.MODULE$.format();
    }

    public static MethodReference createAnyValueMap()
    {
        return Methods$.MODULE$.createAnyValueMap();
    }

    public static MethodReference createMap()
    {
        return Methods$.MODULE$.createMap();
    }

    public static MethodReference hasMoreRelationship()
    {
        return Methods$.MODULE$.hasMoreRelationship();
    }

    public static MethodReference hasNextLong()
    {
        return Methods$.MODULE$.hasNextLong();
    }

    public static MethodReference compositeKey()
    {
        return Methods$.MODULE$.compositeKey();
    }

    public static MethodReference countingTableCompositeKeyGet()
    {
        return Methods$.MODULE$.countingTableCompositeKeyGet();
    }

    public static MethodReference countingTableGet()
    {
        return Methods$.MODULE$.countingTableGet();
    }

    public static MethodReference countingTableCompositeKeyPut()
    {
        return Methods$.MODULE$.countingTableCompositeKeyPut();
    }

    public static MethodReference countingTableIncrement()
    {
        return Methods$.MODULE$.countingTableIncrement();
    }
}
