package org.neo4j.cypher.internal.spi.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Map.Entry;
import java.util.PrimitiveIterator.OfDouble;
import java.util.PrimitiveIterator.OfInt;
import java.util.PrimitiveIterator.OfLong;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.eclipse.collections.api.iterator.LongIterator;
import org.eclipse.collections.api.iterator.MutableLongIterator;
import org.eclipse.collections.impl.map.mutable.primitive.LongIntHashMap;
import org.eclipse.collections.impl.map.mutable.primitive.LongObjectHashMap;
import org.eclipse.collections.impl.set.mutable.primitive.LongHashSet;
import org.neo4j.codegen.CodeBlock;
import org.neo4j.codegen.Expression;
import org.neo4j.codegen.FieldReference;
import org.neo4j.codegen.LocalVariable;
import org.neo4j.codegen.MethodReference;
import org.neo4j.codegen.Parameter;
import org.neo4j.codegen.TypeReference;
import org.neo4j.cypher.internal.codegen.CompiledConversionUtils;
import org.neo4j.cypher.internal.codegen.CompiledEquivalenceUtils;
import org.neo4j.cypher.internal.codegen.DefaultFullSortTable;
import org.neo4j.cypher.internal.codegen.DefaultTopTable;
import org.neo4j.cypher.internal.codegen.PrimitiveNodeStream;
import org.neo4j.cypher.internal.codegen.PrimitiveRelationshipStream;
import org.neo4j.cypher.internal.codegen.CompiledConversionUtils.CompositeKey;
import org.neo4j.cypher.internal.profiling.OperatorProfileEvent;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.AnyValueType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.BoolType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.FloatType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ListReferenceType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.LongType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ReferenceType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ReferenceType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Comparator;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CountingJoinTableType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Equal$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.FullSortTableDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.GreaterThan$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.GreaterThanEqual$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.HashableTupleDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LessThan$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LessThanEqual$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongToCountTable$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongToListTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongsToCountTable$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongsToListTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.OrderableTupleDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.RecordingJoinTableType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TopTableDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.cypher.internal.v4_0.util.symbols.BooleanType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.FloatType;
import org.neo4j.cypher.internal.v4_0.util.symbols.IntegerType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.symbols.StringType;
import org.neo4j.cypher.operations.CursorUtils;
import org.neo4j.exceptions.ParameterNotFoundException;
import org.neo4j.graphdb.Direction;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.NodeLabelIndexCursor;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.SchemaRead;
import org.neo4j.internal.kernel.api.TokenRead;
import org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor;
import org.neo4j.internal.schema.IndexDescriptor;
import org.neo4j.kernel.impl.util.ValueUtils;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.DoubleValue;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.NumberValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import org.neo4j.values.virtual.VirtualNodeValue;
import org.neo4j.values.virtual.VirtualRelationshipValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple6;
import scala.collection.IndexedSeq;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.mutable.Map;
import scala.reflect.Manifest;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction6;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class GeneratedMethodStructure implements MethodStructure<Expression>
{
    private final Fields fields;
    private final CodeBlock generator;
    private final AuxGenerator aux;
    private final boolean tracing;
    private final List<String> events;
    private final Map<String,LocalVariable> locals;
    private final CodeGenContext context;
    private volatile GeneratedMethodStructure.HashTable$ HashTable$module;

    public GeneratedMethodStructure( final Fields fields, final CodeBlock generator, final AuxGenerator aux, final boolean tracing, final List<String> events,
            final Map<String,LocalVariable> locals, final CodeGenContext context )
    {
        this.fields = fields;
        this.generator = generator;
        this.aux = aux;
        this.tracing = tracing;
        this.events = events;
        this.locals = locals;
        this.context = context;
        MethodStructure.$init$( this );
    }

    public static Map<String,LocalVariable> $lessinit$greater$default$6()
    {
        return GeneratedMethodStructure$.MODULE$.$lessinit$greater$default$6();
    }

    public static List<String> $lessinit$greater$default$5()
    {
        return GeneratedMethodStructure$.MODULE$.$lessinit$greater$default$5();
    }

    public static boolean $lessinit$greater$default$4()
    {
        return GeneratedMethodStructure$.MODULE$.$lessinit$greater$default$4();
    }

    public void assign( final Variable v, final Object value )
    {
        MethodStructure.assign$( this, v, value );
    }

    public Object constantPrimitiveExpression( final Object value )
    {
        return MethodStructure.constantPrimitiveExpression$( this, value );
    }

    private GeneratedMethodStructure.HashTable$ HashTable()
    {
        if ( this.HashTable$module == null )
        {
            this.HashTable$lzycompute$1();
        }

        return this.HashTable$module;
    }

    public Fields fields()
    {
        return this.fields;
    }

    public CodeBlock generator()
    {
        return this.generator;
    }

    private GeneratedMethodStructure copy( final Fields fields, final CodeBlock generator, final AuxGenerator aux, final boolean tracing,
            final List<String> events, final Map<String,LocalVariable> locals )
    {
        return new GeneratedMethodStructure( fields, generator, aux, tracing, events, locals, this.context );
    }

    private Fields copy$default$1()
    {
        return this.fields();
    }

    private CodeBlock copy$default$2()
    {
        return this.generator();
    }

    private AuxGenerator copy$default$3()
    {
        return this.aux;
    }

    private boolean copy$default$4()
    {
        return this.tracing;
    }

    private List<String> copy$default$5()
    {
        return this.events;
    }

    private Map<String,LocalVariable> copy$default$6()
    {
        return this.locals;
    }

    private GeneratedMethodStructure.HashTable extractHashTable( final RecordingJoinTableType tableType )
    {
        GeneratedMethodStructure.HashTable var2;
        if ( tableType instanceof LongToListTable )
        {
            LongToListTable var4 = (LongToListTable) tableType;
            TupleDescriptor tupleDescriptor = var4.tupleDescriptor();
            TypeReference valueType = this.aux.typeReference( tupleDescriptor );
            TypeReference listType = TypeReference.parameterizedType( ArrayList.class, new TypeReference[]{valueType} );
            TypeReference tableType = TypeReference.parameterizedType( LongObjectHashMap.class, new TypeReference[]{valueType} );
            MethodReference get =
                    MethodReference.methodReference( tableType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() ),
            "get", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())});
            MethodReference put =
                    MethodReference.methodReference( tableType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() ),
            "put", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())});
            MethodReference add =
                    MethodReference.methodReference( listType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean() ),
            "add", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())});
            var2 = new GeneratedMethodStructure.HashTable( this, valueType, listType, tableType, get, put, add );
        }
        else
        {
            if ( !(tableType instanceof LongsToListTable) )
            {
                throw new MatchError( tableType );
            }

            LongsToListTable var12 = (LongsToListTable) tableType;
            TupleDescriptor tupleDescriptor = var12.tupleDescriptor();
            TypeReference valueType = this.aux.typeReference( tupleDescriptor );
            TypeReference listType = TypeReference.parameterizedType( ArrayList.class, new TypeReference[]{valueType} );
            TypeReference tableType = TypeReference.parameterizedType( HashMap.class,
                    new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CompositeKey.class ) ),
                    valueType});
            MethodReference get =
                    MethodReference.methodReference( tableType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() ),
            "get", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())});
            MethodReference put =
                    MethodReference.methodReference( tableType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() ),
            "put", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())});
            MethodReference add =
                    MethodReference.methodReference( listType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean() ),
            "add", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())});
            var2 = new GeneratedMethodStructure.HashTable( this, valueType, listType, tableType, get, put, add );
        }

        return var2;
    }

    public void nodeFromNodeValueIndexCursor( final String targetVar, final String iterVar )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), targetVar, Expression.invoke(
                this.generator().load( iterVar ),
                GeneratedQueryStructure$.MODULE$.method( "nodeReference", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),new Expression[0]))
        ;
    }

    public void nodeFromNodeCursor( final String targetVar, final String iterVar )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), targetVar, Expression.invoke(
                this.generator().load( iterVar ),
                GeneratedQueryStructure$.MODULE$.method( "nodeReference", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),new Expression[0]));
    }

    public void nodeFromNodeLabelIndexCursor( final String targetVar, final String iterVar )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), targetVar, Expression.invoke(
                this.generator().load( iterVar ),
                GeneratedQueryStructure$.MODULE$.method( "nodeReference", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),new Expression[0]))
        ;
    }

    public void nextRelationshipAndNode( final String toNodeVar, final String iterVar, final SemanticDirection direction, final String fromNodeVar,
            final String relVar )
    {
        String cursor = this.relCursor( relVar );
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), toNodeVar, Expression.invoke(
                this.generator().load( cursor ),
                GeneratedQueryStructure$.MODULE$.method( "otherNodeReference", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        new Expression[0]));
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), relVar, Expression.invoke(
                this.generator().load( cursor ),
                GeneratedQueryStructure$.MODULE$.method( "relationshipReference", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        new Expression[0]));
    }

    private String relCursor( final String relVar )
    {
        return (new StringBuilder( 4 )).append( relVar ).append( "Iter" ).toString();
    }

    public void nextRelationship( final String cursorName, final SemanticDirection ignored, final String relVar )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), relVar, Expression.invoke(
                this.generator().load( this.relCursor( relVar ) ),
                GeneratedQueryStructure$.MODULE$.method( "relationshipReference", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Long()),
        new Expression[0]));
    }

    public void allNodesScan( final String cursorName )
    {
        this.generator().assign(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ) ), cursorName, Expression.invoke(
                this.cursors(),
                GeneratedQueryStructure$.MODULE$.method( "allocateNodeCursor", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( CursorFactory.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
        new Expression[0]));
        this.generator().expression( Expression.pop(
                Expression.invoke( Expression.get( this.generator().self(), this.fields().closeables() ), Methods$.MODULE$.listAdd(),
                        new Expression[]{this.generator().load( cursorName )} ) ) );
        this.generator().expression( Expression.invoke( this.dataRead(),
                GeneratedQueryStructure$.MODULE$.method( "allNodesScan", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ))})),
        scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),
        new Expression[]{this.generator().load( cursorName )}));
    }

    public void labelScan( final String cursorName, final String labelIdVar )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                NodeLabelIndexCursor.class ) ), cursorName, Expression.invoke( this.cursors(),
                GeneratedQueryStructure$.MODULE$.method( "allocateNodeLabelIndexCursor", scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..MODULE$.classType(
                CursorFactory.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class )),new Expression[0]));
        this.generator().expression( Expression.pop(
                Expression.invoke( Expression.get( this.generator().self(), this.fields().closeables() ), Methods$.MODULE$.listAdd(),
                        new Expression[]{this.generator().load( cursorName )} ) ) );
        this.generator().expression( Expression.invoke( this.dataRead(),
                GeneratedQueryStructure$.MODULE$.method( "nodeLabelScan", scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int()),
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ) )})),
        scala.reflect.ManifestFactory..MODULE$.classType( Read.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),
        new Expression[]{this.generator().load( labelIdVar ), this.generator().load( cursorName )}));
    }

    public void lookupLabelId( final String labelIdVar, final String labelName )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ), labelIdVar, Expression.invoke(
                this.tokenRead(), Methods$.MODULE$.labelGetForName(), new Expression[]{Expression.constant( labelName )} ));
    }

    public Expression lookupLabelIdE( final String labelName )
    {
        return Expression.invoke( this.tokenRead(), Methods$.MODULE$.labelGetForName(), new Expression[]{Expression.constant( labelName )} );
    }

    public void lookupRelationshipTypeId( final String typeIdVar, final String typeName )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ), typeIdVar, Expression.invoke(
                this.tokenRead(), Methods$.MODULE$.relationshipTypeGetForName(), new Expression[]{Expression.constant( typeName )} ));
    }

    public Expression lookupRelationshipTypeIdE( final String typeName )
    {
        return Expression.invoke( this.tokenRead(), Methods$.MODULE$.relationshipTypeGetForName(), new Expression[]{Expression.constant( typeName )} );
    }

    public Expression advanceNodeCursor( final String cursorName )
    {
        return Expression.invoke( this.generator().load( cursorName ),
                GeneratedQueryStructure$.MODULE$.method( "next", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ),scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[0]);
    }

    public void closeNodeCursor( final String cursorName )
    {
        this.generator().expression( Expression.invoke( this.generator().load( cursorName ),
                GeneratedQueryStructure$.MODULE$.method( "close", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),new Expression[0]));
    }

    public Expression advanceNodeLabelIndexCursor( final String cursorName )
    {
        return Expression.invoke( this.generator().load( cursorName ),
                GeneratedQueryStructure$.MODULE$.method( "next", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ),scala.reflect.ManifestFactory..MODULE$.Boolean()),
        new Expression[0]);
    }

    public void closeNodeLabelIndexCursor( final String cursorName )
    {
        this.generator().expression( Expression.invoke( this.generator().load( cursorName ),
                GeneratedQueryStructure$.MODULE$.method( "close", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeLabelIndexCursor.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),new Expression[0]))
        ;
    }

    public Expression advanceRelationshipSelectionCursor( final String cursorName )
    {
        return Expression.invoke( this.generator().load( cursorName ),
                GeneratedQueryStructure$.MODULE$.method( "next", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ),scala.reflect.ManifestFactory..MODULE$.Boolean()),
        new Expression[0]);
    }

    public void closeRelationshipSelectionCursor( final String cursorName )
    {
        this.generator().expression( Expression.invoke( this.generator().load( cursorName ),
                GeneratedQueryStructure$.MODULE$.method( "close", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Unit()),
        new Expression[0]));
    }

    public Expression advanceNodeValueIndexCursor( final String cursorName )
    {
        return Expression.invoke( this.generator().load( cursorName ),
                GeneratedQueryStructure$.MODULE$.method( "next", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ),scala.reflect.ManifestFactory..MODULE$.Boolean()),
        new Expression[0]);
    }

    public void closeNodeValueIndexCursor( final String cursorName )
    {
        CodeBlock x$10 = this.generator().ifStatement( Expression.notNull( this.generator().load( cursorName ) ) );
        Function1 x$11 = ( inner ) -> {
            $anonfun$closeNodeValueIndexCursor$1( cursorName, inner );
            return BoxedUnit.UNIT;
        };
        Function1 x$12 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$10, x$11 );
        Function2 x$13 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$10, x$11 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$10, x$11, x$12, x$13 );
    }

    public void whileLoop( final Expression test, final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        CodeBlock x$20 = this.generator().whileLoop( test );
        Function1 x$21 = ( body ) -> {
            $anonfun$whileLoop$1( this, block, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$22 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$20, x$21 );
        Function2 x$23 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$20, x$21 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$20, x$21, x$22, x$23 );
    }

    public void forEach( final String varName, final CodeGenType codeGenType, final Expression iterable,
            final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        CodeBlock x$30 = this.generator().forEach( Parameter.param( GeneratedQueryStructure$.MODULE$.lowerType( codeGenType ), varName ), iterable );
        Function1 x$31 = ( body ) -> {
            $anonfun$forEach$1( this, block, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$32 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$30, x$31 );
        Function2 x$33 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$30, x$31 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$30, x$31, x$32, x$33 );
    }

    public void ifStatement( final Expression test, final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        CodeBlock x$40 = this.generator().ifStatement( test );
        Function1 x$41 = ( body ) -> {
            $anonfun$ifStatement$1( this, block, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$42 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$40, x$41 );
        Function2 x$43 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$40, x$41 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$40, x$41, x$42, x$43 );
    }

    public void throwException( final Expression exception )
    {
        this.generator().throwException( exception );
    }

    public void ifNotStatement( final Expression test, final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        CodeBlock x$50 = this.generator().ifStatement( Expression.not( test ) );
        Function1 x$51 = ( body ) -> {
            $anonfun$ifNotStatement$1( this, block, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$52 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$50, x$51 );
        Function2 x$53 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$50, x$51 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$50, x$51, x$52, x$53 );
    }

    public void ifNonNullStatement( final Expression test, final CodeGenType codeGenType, final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        boolean var5;
        label61:
        {
            CypherCodeGenType var10000;
            label57:
            {
                var10000 = CodeGenType$.MODULE$.primitiveNode();
                if ( var10000 == null )
                {
                    if ( codeGenType != null )
                    {
                        break label57;
                    }
                }
                else if ( !var10000.equals( codeGenType ) )
                {
                    break label57;
                }

                var5 = true;
                break label61;
            }

            label62:
            {
                var10000 = CodeGenType$.MODULE$.primitiveRel();
                if ( var10000 == null )
                {
                    if ( codeGenType == null )
                    {
                        break label62;
                    }
                }
                else if ( var10000.equals( codeGenType ) )
                {
                    break label62;
                }

                if ( codeGenType instanceof CypherCodeGenType )
                {
                    CypherCodeGenType var10 = (CypherCodeGenType) codeGenType;
                    if ( var10.repr() instanceof AnyValueType )
                    {
                        var5 = true;
                        break label61;
                    }
                }

                var5 = false;
                break label61;
            }

            var5 = true;
        }

        Expression var4;
        if ( var5 )
        {
            var4 = Expression.notEqual( test, GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ) );
        }
        else
        {
            if ( !(codeGenType instanceof CypherCodeGenType) )
            {
                throw new IllegalArgumentException(
                        (new StringBuilder( 39 )).append( "CodeGenType " ).append( codeGenType ).append( " does not have a null value" ).toString() );
            }

            CypherCodeGenType var11 = (CypherCodeGenType) codeGenType;
            RepresentationType var12 = var11.repr();
            if ( !ReferenceType$.MODULE$.equals( var12 ) )
            {
                throw new IllegalArgumentException(
                        (new StringBuilder( 39 )).append( "CodeGenType " ).append( codeGenType ).append( " does not have a null value" ).toString() );
            }

            var4 = Expression.and( Expression.notNull( test ), Expression.notEqual( test, this.noValue() ) );
        }

        CodeBlock x$60 = this.generator().ifStatement( var4 );
        Function1 x$61 = ( body ) -> {
            $anonfun$ifNonNullStatement$1( this, block, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$62 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$60, x$61 );
        Function2 x$63 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$60, x$61 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$60, x$61, x$62, x$63 );
    }

    public Expression ternaryOperator( final Expression test, final Expression onTrue, final Expression onFalse )
    {
        return Expression.ternary( test, onTrue, onFalse );
    }

    public void returnSuccessfully()
    {
        this.events.foreach( ( event ) -> {
            $anonfun$returnSuccessfully$1( this, event );
            return BoxedUnit.UNIT;
        } );
        this.generator().expression( Expression.invoke( this.generator().self(),
                MethodReference.methodReference( this.generator().owner(), TypeReference.VOID, "closeCursors", new TypeReference[0] ), new Expression[0] ) );
        this.generator().returns();
    }

    public void declareCounter( final String name, final Expression initialValue, final String errorOnFloatingPoint )
    {
        LocalVariable variable = this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), name);
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( name ), variable));
        this.generator().assign( variable,
                Expression.invoke( Methods$.MODULE$.mathCastToLongOrFail(), new Expression[]{initialValue, Expression.constant( errorOnFloatingPoint )} ) );
    }

    public void decrementInteger( final String name )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( name );
        this.generator().assign( local, Expression.subtract( local, Expression.constant( BoxesRunTime.boxToLong( 1L ) ) ) );
    }

    public void incrementInteger( final String name )
    {
        this.incrementInteger( name, Expression.constant( BoxesRunTime.boxToLong( 1L ) ) );
    }

    public void incrementInteger( final String name, final Expression value )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( name );
        this.generator().assign( local, Expression.add( local, value ) );
    }

    public Expression checkInteger( final String name, final Comparator comparator, final long value )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( name );
        Expression var5;
        if ( Equal$.MODULE$.equals( comparator ) )
        {
            var5 = Expression.equal( local, Expression.constant( BoxesRunTime.boxToLong( value ) ) );
        }
        else if ( LessThan$.MODULE$.equals( comparator ) )
        {
            var5 = Expression.lt( local, Expression.constant( BoxesRunTime.boxToLong( value ) ) );
        }
        else if ( LessThanEqual$.MODULE$.equals( comparator ) )
        {
            var5 = Expression.lte( local, Expression.constant( BoxesRunTime.boxToLong( value ) ) );
        }
        else if ( GreaterThan$.MODULE$.equals( comparator ) )
        {
            var5 = Expression.gt( local, Expression.constant( BoxesRunTime.boxToLong( value ) ) );
        }
        else
        {
            if ( !GreaterThanEqual$.MODULE$.equals( comparator ) )
            {
                throw new MatchError( comparator );
            }

            var5 = Expression.gte( local, Expression.constant( BoxesRunTime.boxToLong( value ) ) );
        }

        return var5;
    }

    public void setInRow( final int column, final Expression value )
    {
        this.generator().expression( Expression.invoke( this.resultRow(), Methods$.MODULE$.set(),
                new Expression[]{Expression.constant( BoxesRunTime.boxToInteger( column ) ), value} ) );
    }

    public Expression toMaterializedAnyValue( final Expression expression, final CodeGenType codeGenType )
    {
        return this.toAnyValue( expression, codeGenType, true );
    }

    public Expression toAnyValue( final Expression expression, final CodeGenType codeGenType )
    {
        return this.toAnyValue( expression, codeGenType, false );
    }

    private Expression toAnyValue( final Expression expression, final CodeGenType codeGenType, final boolean materializeEntities )
    {
        boolean var5 = false;
        CypherCodeGenType var6 = null;
        Expression var4;
        if ( codeGenType instanceof CypherCodeGenType )
        {
            var5 = true;
            var6 = (CypherCodeGenType) codeGenType;
            if ( var6.repr() instanceof AnyValueType )
            {
                var4 = Expression.cast(
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ) ), expression);
                return var4;
            }
        }

        label239:
        {
            CypherCodeGenType var10000 = CodeGenType$.MODULE$.primitiveNode();
            if ( var10000 == null )
            {
                if ( codeGenType == null )
                {
                    break label239;
                }
            }
            else if ( var10000.equals( codeGenType ) )
            {
                break label239;
            }

            label240:
            {
                if ( var5 )
                {
                    CypherType var9 = var6.ct();
                    NodeType var25 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var25 == null )
                    {
                        if ( var9 == null )
                        {
                            break label240;
                        }
                    }
                    else if ( var25.equals( var9 ) )
                    {
                        break label240;
                    }
                }

                label241:
                {
                    var10000 = CodeGenType$.MODULE$.primitiveRel();
                    if ( var10000 == null )
                    {
                        if ( codeGenType != null )
                        {
                            break label241;
                        }
                    }
                    else if ( !var10000.equals( codeGenType ) )
                    {
                        break label241;
                    }

                    var4 = materializeEntities ? Templates$.MODULE$.createNewRelationshipValueFromPrimitive( this.nodeManager(), expression )
                                               : Templates$.MODULE$.createNewRelationshipReference( expression );
                    return var4;
                }

                label242:
                {
                    if ( var5 )
                    {
                        CypherType var12 = var6.ct();
                        RelationshipType var26 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                        if ( var26 == null )
                        {
                            if ( var12 == null )
                            {
                                break label242;
                            }
                        }
                        else if ( var26.equals( var12 ) )
                        {
                            break label242;
                        }
                    }

                    label243:
                    {
                        var10000 = CodeGenType$.MODULE$.primitiveInt();
                        if ( var10000 == null )
                        {
                            if ( codeGenType != null )
                            {
                                break label243;
                            }
                        }
                        else if ( !var10000.equals( codeGenType ) )
                        {
                            break label243;
                        }

                        var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "longValue", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
                        scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType( LongValue.class )),
                        new Expression[]{expression});
                        return var4;
                    }

                    label244:
                    {
                        if ( var5 )
                        {
                            CypherType var15 = var6.ct();
                            IntegerType var27 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                            if ( var27 == null )
                            {
                                if ( var15 == null )
                                {
                                    break label244;
                                }
                            }
                            else if ( var27.equals( var15 ) )
                            {
                                break label244;
                            }
                        }

                        label245:
                        {
                            var10000 = CodeGenType$.MODULE$.primitiveFloat();
                            if ( var10000 == null )
                            {
                                if ( codeGenType != null )
                                {
                                    break label245;
                                }
                            }
                            else if ( !var10000.equals( codeGenType ) )
                            {
                                break label245;
                            }

                            var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "doubleValue", scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Double())})),
                            scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( DoubleValue.class )),new Expression[]{expression});
                            return var4;
                        }

                        label246:
                        {
                            if ( var5 )
                            {
                                CypherType var18 = var6.ct();
                                FloatType var28 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                                if ( var28 == null )
                                {
                                    if ( var18 == null )
                                    {
                                        break label246;
                                    }
                                }
                                else if ( var28.equals( var18 ) )
                                {
                                    break label246;
                                }
                            }

                            label247:
                            {
                                var10000 = CodeGenType$.MODULE$.primitiveBool();
                                if ( var10000 == null )
                                {
                                    if ( codeGenType != null )
                                    {
                                        break label247;
                                    }
                                }
                                else if ( !var10000.equals( codeGenType ) )
                                {
                                    break label247;
                                }

                                var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "booleanValue", scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new TypeReference[]{
                                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean())})),
                                scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( BooleanValue.class )),new Expression[]{expression});
                                return var4;
                            }

                            if ( var5 )
                            {
                                label248:
                                {
                                    CypherType var21 = var6.ct();
                                    BooleanType var29 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                                    if ( var29 == null )
                                    {
                                        if ( var21 != null )
                                        {
                                            break label248;
                                        }
                                    }
                                    else if ( !var29.equals( var21 ) )
                                    {
                                        break label248;
                                    }

                                    var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "asBooleanValue", scala.Predef..MODULE$.wrapRefArray(
                                            (Object[]) (new TypeReference[]{
                                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                                    scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..
                                    MODULE$.classType( BooleanValue.class )),new Expression[]{expression});
                                    return var4;
                                }
                            }

                            label249:
                            {
                                if ( var5 )
                                {
                                    CypherType var23 = var6.ct();
                                    StringType var30 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
                                    if ( var30 == null )
                                    {
                                        if ( var23 == null )
                                        {
                                            break label249;
                                        }
                                    }
                                    else if ( var30.equals( var23 ) )
                                    {
                                        break label249;
                                    }
                                }

                                var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "asAnyValue", scala.Predef..MODULE$.wrapRefArray(
                                        (Object[]) (new TypeReference[]{
                                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                                scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..
                                MODULE$.classType( AnyValue.class )),new Expression[]{expression});
                                return var4;
                            }

                            var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "asTextValue", scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                            scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..
                            MODULE$.classType( TextValue.class )),new Expression[]{expression});
                            return var4;
                        }

                        var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "asDoubleValue", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                        scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..
                        MODULE$.classType( DoubleValue.class )),new Expression[]{expression});
                        return var4;
                    }

                    var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "asLongValue", scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                    scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( LongValue.class )),
                    new Expression[]{expression});
                    return var4;
                }

                var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "asRelationshipValue", scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..
                MODULE$.classType( RelationshipValue.class )),new Expression[]{expression});
                return var4;
            }

            var4 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "asNodeValue", scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
            scala.reflect.ManifestFactory..MODULE$.classType( ValueUtils.class ), scala.reflect.ManifestFactory..MODULE$.classType( NodeValue.class )),
            new Expression[]{expression});
            return var4;
        }

        var4 = materializeEntities ? Templates$.MODULE$.createNewNodeValueFromPrimitive( this.nodeManager(), expression )
                                   : Templates$.MODULE$.createNewNodeReference( expression );
        return var4;
    }

    public void visitorAccept()
    {
        Templates$.MODULE$.tryCatch( this.generator(), ( onSuccess ) -> {
            $anonfun$visitorAccept$1( this, onSuccess );
            return BoxedUnit.UNIT;
        }, GeneratedQueryStructure$.MODULE$.param( "e", scala.reflect.ManifestFactory..MODULE$.classType( Throwable.class ) ), ( onError ) -> {
            $anonfun$visitorAccept$5( this, onError );
            return BoxedUnit.UNIT;
        });
    }

    public Expression materializeNode( final String nodeIdVar, final CodeGenType codeGenType )
    {
        return codeGenType.isPrimitive() ? Expression.invoke( this.nodeManager(), Methods$.MODULE$.newNodeEntityById(),
                new Expression[]{this.generator().load( nodeIdVar )} ) : (codeGenType.isAnyValue() ? Expression.invoke( Methods$.MODULE$.materializeNodeValue(),
                new Expression[]{this.nodeManager(), this.generator().load( nodeIdVar )} ) : Expression.invoke( this.nodeManager(),
                Methods$.MODULE$.newNodeEntityById(), new Expression[]{Expression.invoke(
                        Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( VirtualNodeValue.class ) ),
                        this.generator().load( nodeIdVar ) ), Methods$.MODULE$.nodeId(), new Expression[0] )}));
    }

    public Expression node( final String nodeIdVar, final CodeGenType codeGenType )
    {
        return codeGenType.isPrimitive() ? this.generator().load( nodeIdVar ) : Expression.invoke(
                Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( VirtualNodeValue.class ) ),
                this.generator().load( nodeIdVar ) ),Methods$.MODULE$.nodeId(), new Expression[0]);
    }

    public Expression nullablePrimitive( final String varName, final CodeGenType codeGenType, final Expression onSuccess )
    {
        boolean var5;
        label50:
        {
            if ( codeGenType instanceof CypherCodeGenType )
            {
                label45:
                {
                    CypherCodeGenType var7 = (CypherCodeGenType) codeGenType;
                    CypherType var8 = var7.ct();
                    RepresentationType var9 = var7.repr();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var8 != null )
                        {
                            break label45;
                        }
                    }
                    else if ( !var10000.equals( var8 ) )
                    {
                        break label45;
                    }

                    if ( LongType$.MODULE$.equals( var9 ) )
                    {
                        var5 = true;
                        break label50;
                    }
                }
            }

            if ( codeGenType instanceof CypherCodeGenType )
            {
                label36:
                {
                    CypherCodeGenType var11 = (CypherCodeGenType) codeGenType;
                    CypherType var12 = var11.ct();
                    RepresentationType var13 = var11.repr();
                    RelationshipType var15 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var15 == null )
                    {
                        if ( var12 != null )
                        {
                            break label36;
                        }
                    }
                    else if ( !var15.equals( var12 ) )
                    {
                        break label36;
                    }

                    if ( LongType$.MODULE$.equals( var13 ) )
                    {
                        var5 = true;
                        break label50;
                    }
                }
            }

            var5 = false;
        }

        Expression var4;
        if ( var5 )
        {
            var4 = Expression.ternary( Expression.equal( GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ), this.generator().load( varName ) ),
                    GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ), onSuccess );
        }
        else
        {
            var4 = Expression.ternary( Expression.isNull( this.generator().load( varName ) ), Expression.constant( (Object) null ), onSuccess );
        }

        return var4;
    }

    public Expression nullableReference( final String varName, final CodeGenType codeGenType, final Expression onSuccess )
    {
        boolean var5;
        label59:
        {
            if ( codeGenType instanceof CypherCodeGenType )
            {
                label54:
                {
                    CypherCodeGenType var7 = (CypherCodeGenType) codeGenType;
                    CypherType var8 = var7.ct();
                    RepresentationType var9 = var7.repr();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var8 != null )
                        {
                            break label54;
                        }
                    }
                    else if ( !var10000.equals( var8 ) )
                    {
                        break label54;
                    }

                    if ( LongType$.MODULE$.equals( var9 ) )
                    {
                        var5 = true;
                        break label59;
                    }
                }
            }

            if ( codeGenType instanceof CypherCodeGenType )
            {
                label45:
                {
                    CypherCodeGenType var11 = (CypherCodeGenType) codeGenType;
                    CypherType var12 = var11.ct();
                    RepresentationType var13 = var11.repr();
                    RelationshipType var16 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var16 == null )
                    {
                        if ( var12 != null )
                        {
                            break label45;
                        }
                    }
                    else if ( !var16.equals( var12 ) )
                    {
                        break label45;
                    }

                    if ( LongType$.MODULE$.equals( var13 ) )
                    {
                        var5 = true;
                        break label59;
                    }
                }
            }

            if ( codeGenType instanceof CypherCodeGenType )
            {
                CypherCodeGenType var15 = (CypherCodeGenType) codeGenType;
                if ( var15.repr() instanceof AnyValueType )
                {
                    var5 = true;
                    break label59;
                }
            }

            var5 = false;
        }

        Expression var4;
        if ( var5 )
        {
            var4 = Expression.ternary( Expression.equal( GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ), this.generator().load( varName ) ),
                    Expression.constant( (Object) null ), onSuccess );
        }
        else
        {
            var4 = Expression.ternary( Expression.isNull( this.generator().load( varName ) ), Expression.constant( (Object) null ), onSuccess );
        }

        return var4;
    }

    public Expression materializeRelationship( final String relIdVar, final CodeGenType codeGenType )
    {
        return codeGenType.isPrimitive() ? Expression.invoke( this.nodeManager(), Methods$.MODULE$.newRelationshipEntityById(),
                new Expression[]{this.generator().load( relIdVar )} )
                                         : (codeGenType.isAnyValue() ? Expression.invoke( Methods$.MODULE$.materializeRelationshipValue(),
                                                 new Expression[]{this.nodeManager(), this.generator().load( relIdVar )} )
                                                                     : Expression.invoke( this.nodeManager(), Methods$.MODULE$.newRelationshipEntityById(),
                                                                             new Expression[]{Expression.invoke( Expression.cast(
                                                                                     GeneratedQueryStructure$.MODULE$.typeRef(
                                                                                             scala.reflect.ManifestFactory..MODULE$.classType(
                                                                                             VirtualRelationshipValue.class ) ),
                                                                                     this.generator().load( relIdVar ) ), Methods$.MODULE$.relId(),
                                                                                     new Expression[0] )}));
    }

    public Expression relationship( final String relIdVar, final CodeGenType codeGenType )
    {
        return this.generator().load( relIdVar );
    }

    public Expression materializeAny( final Expression expression, final CodeGenType codeGenType )
    {
        Expression var3;
        if ( codeGenType instanceof CypherCodeGenType )
        {
            CypherCodeGenType var5 = (CypherCodeGenType) codeGenType;
            if ( var5.repr() instanceof AnyValueType )
            {
                var3 = Expression.invoke( Methods$.MODULE$.materializeAnyValueResult(), new Expression[]{this.nodeManager(), expression} );
                return var3;
            }
        }

        var3 = Expression.invoke( Methods$.MODULE$.materializeAnyResult(), new Expression[]{this.nodeManager(), expression} );
        return var3;
    }

    public <V> V trace( final String planStepId, final Option<String> maybeSuffix, final Function1<MethodStructure<Expression>,V> block )
    {
        Object var10000;
        if ( !this.tracing )
        {
            var10000 = block.apply( this );
        }
        else
        {
            String suffix = (String) maybeSuffix.map( ( x$1 ) -> {
                return (new StringBuilder( 1 )).append( "_" ).append( x$1 ).toString();
            } ).getOrElse( () -> {
                return "";
            } );
            String eventName = (new StringBuilder( 6 )).append( "event_" ).append( planStepId ).append( suffix ).toString();
            this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                    OperatorProfileEvent.class ) ), eventName, this.traceEvent( planStepId ));
            List x$80 = this.events.$colon$colon( eventName );
            CodeBlock x$81 = this.generator();
            Fields x$82 = this.copy$default$1();
            AuxGenerator x$83 = this.copy$default$3();
            boolean x$84 = this.copy$default$4();
            Map x$85 = this.copy$default$6();
            Object result = block.apply( this.copy( x$82, x$81, x$83, x$84, x$80, x$85 ) );
            CodeBlock x$86 = this.generator().ifStatement( Expression.notNull( this.generator().load( eventName ) ) );
            Function1 x$87 = ( inner ) -> {
                $anonfun$trace$3( eventName, inner );
                return BoxedUnit.UNIT;
            };
            Function1 x$88 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$86, x$87 );
            Function2 x$89 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$86, x$87 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$86, x$87, x$88, x$89 );
            var10000 = result;
        }

        return var10000;
    }

    public <V> Option<String> trace$default$2()
    {
        return scala.None..MODULE$;
    }

    private Expression traceEvent( final String planStepId )
    {
        return Expression.invoke( this.tracer(), Methods$.MODULE$.executeOperator(), new Expression[]{Expression.getStatic(
                FieldReference.staticField( this.generator().owner(),
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Id.class ) ), planStepId ) )});
    }

    public void incrementDbHits()
    {
        if ( this.tracing )
        {
            CodeBlock x$90 = this.generator().ifStatement( Expression.notNull( this.loadEvent() ) );
            Function1 x$91 = ( inner ) -> {
                $anonfun$incrementDbHits$1( this, inner );
                return BoxedUnit.UNIT;
            };
            Function1 x$92 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$90, x$91 );
            Function2 x$93 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$90, x$91 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$90, x$91, x$92, x$93 );
        }
    }

    public void incrementRows()
    {
        if ( this.tracing )
        {
            CodeBlock x$94 = this.generator().ifStatement( Expression.notNull( this.loadEvent() ) );
            Function1 x$95 = ( inner ) -> {
                $anonfun$incrementRows$1( this, inner );
                return BoxedUnit.UNIT;
            };
            Function1 x$96 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$94, x$95 );
            Function2 x$97 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$94, x$95 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$94, x$95, x$96, x$97 );
        }
    }

    private Expression loadEvent()
    {
        return this.generator().load( (String) this.events.headOption().getOrElse( () -> {
            throw new IllegalStateException( "no current trace event" );
        } ) );
    }

    public void expectParameter( final String key, final String variableName, final CodeGenType codeGenType )
    {
        Expression var4;
        CodeBlock var10000;
        TypeReference var10001;
        label92:
        {
            Expression invokeLoadParameter;
            boolean var10;
            CypherCodeGenType var11;
            CypherCodeGenType var10003;
            label89:
            {
                CodeBlock x$98 = this.generator().ifStatement( Expression.not( Expression.invoke( this.params(),
                        GeneratedQueryStructure$.MODULE$.method( "containsKey", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))})),
                scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),
                new Expression[]{Expression.constant( key )})));
                Function1 x$99 = ( block ) -> {
                    $anonfun$expectParameter$1( this, key, block );
                    return BoxedUnit.UNIT;
                };
                Function1 x$100 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$98, x$99 );
                Function2 x$101 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$98, x$99 );
                org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$98, x$99, x$100, x$101 );
                invokeLoadParameter = Expression.invoke( this.params(), GeneratedQueryStructure$.MODULE$.method( "get", scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[]{
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))})),
                scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class ), scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class )),
                new Expression[]{this.constantExpression( key )});
                var10000 = this.generator();
                var10001 = GeneratedQueryStructure$.MODULE$.lowerType( codeGenType );
                var10 = false;
                var11 = null;
                var10003 = CodeGenType$.MODULE$.primitiveNode();
                if ( var10003 == null )
                {
                    if ( codeGenType != null )
                    {
                        break label89;
                    }
                }
                else if ( !var10003.equals( codeGenType ) )
                {
                    break label89;
                }

                var4 = this.unbox( (Expression) Expression.cast(
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( VirtualNodeValue.class ) ),
                        invokeLoadParameter ), new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..
                MODULE$.CTNode(), ReferenceType$.MODULE$));
                break label92;
            }

            label93:
            {
                var10003 = CodeGenType$.MODULE$.primitiveRel();
                if ( var10003 == null )
                {
                    if ( codeGenType == null )
                    {
                        break label93;
                    }
                }
                else if ( var10003.equals( codeGenType ) )
                {
                    break label93;
                }

                label94:
                {
                    var10003 = CodeGenType$.MODULE$.primitiveInt();
                    if ( var10003 == null )
                    {
                        if ( codeGenType == null )
                        {
                            break label94;
                        }
                    }
                    else if ( var10003.equals( codeGenType ) )
                    {
                        break label94;
                    }

                    label95:
                    {
                        var10003 = CodeGenType$.MODULE$.primitiveFloat();
                        if ( var10003 == null )
                        {
                            if ( codeGenType == null )
                            {
                                break label95;
                            }
                        }
                        else if ( var10003.equals( codeGenType ) )
                        {
                            break label95;
                        }

                        label61:
                        {
                            var10003 = CodeGenType$.MODULE$.primitiveBool();
                            if ( var10003 == null )
                            {
                                if ( codeGenType != null )
                                {
                                    break label61;
                                }
                            }
                            else if ( !var10003.equals( codeGenType ) )
                            {
                                break label61;
                            }

                            var4 = Expression.unbox( Expression.cast(
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Boolean.class ) ),
                                    invokeLoadParameter ));
                            break label92;
                        }

                        if ( codeGenType instanceof CypherCodeGenType )
                        {
                            var10 = true;
                            var11 = (CypherCodeGenType) codeGenType;
                            RepresentationType var18 = var11.repr();
                            if ( var18 instanceof ListReferenceType )
                            {
                                ListReferenceType var19 = (ListReferenceType) var18;
                                RepresentationType repr = var19.inner();
                                if ( RepresentationType$.MODULE$.isPrimitive( repr ) )
                                {
                                    var4 = this.asPrimitiveStream( invokeLoadParameter, codeGenType );
                                    break label92;
                                }
                            }
                        }

                        if ( var10 && var11.repr() instanceof AnyValueType )
                        {
                            var4 = Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                    AnyValue.class ) ), invokeLoadParameter);
                            break label92;
                        }

                        var4 = invokeLoadParameter;
                        break label92;
                    }

                    var4 = Expression.unbox(
                            Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Double.class ) ),
                            invokeLoadParameter ));
                    break label92;
                }

                var4 = Expression.unbox(
                        Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Long.class ) ),
                        invokeLoadParameter ));
                break label92;
            }

            var4 = this.unbox( (Expression) Expression.cast(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( VirtualRelationshipValue.class ) ),
                    invokeLoadParameter ), new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..
            MODULE$.CTRelationship(), ReferenceType$.MODULE$));
        }

        var10000.assign( var10001, variableName, var4 );
    }

    public Expression mapGetExpression( final Expression map, final String key )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() ),
        "mapGetProperty", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))}),
        new Expression[]{map, this.constantExpression( key )});
    }

    public Expression constantExpression( final Object value )
    {
        return Expression.constant( value );
    }

    public Expression constantValueExpression( final Object value, final CodeGenType codeGenType )
    {
        Expression var10000;
        if ( codeGenType.isPrimitive() )
        {
            var10000 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "of", scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
            scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
            new Expression[]{this.box( Expression.constant( value ), codeGenType )});
        }
        else
        {
            Expression var3;
            label24:
            {
                label23:
                {
                    if ( codeGenType instanceof CypherCodeGenType )
                    {
                        CypherCodeGenType var5 = (CypherCodeGenType) codeGenType;
                        CypherType var6 = var5.ct();
                        StringType var8 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
                        if ( var8 == null )
                        {
                            if ( var6 == null )
                            {
                                break label23;
                            }
                        }
                        else if ( var8.equals( var6 ) )
                        {
                            break label23;
                        }
                    }

                    var3 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "of", scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                    scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType( Value.class )),
                    new Expression[]{Expression.constant( value )});
                    break label24;
                }

                var3 = Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "stringValue", scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[]{
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))})),
                scala.reflect.ManifestFactory..MODULE$.classType( Values.class ), scala.reflect.ManifestFactory..MODULE$.classType( TextValue.class )),
                new Expression[]{Expression.constant( value )});
            }

            var10000 = var3;
        }

        return var10000;
    }

    public Expression notExpression( final Expression value )
    {
        return Expression.not( value );
    }

    public Expression threeValuedNotExpression( final Expression value )
    {
        return Expression.invoke( Methods$.MODULE$.not(), new Expression[]{value} );
    }

    public Expression threeValuedEqualsExpression( final Expression lhs, final Expression rhs )
    {
        return Expression.invoke( Methods$.MODULE$.ternaryEquals(), new Expression[]{lhs, rhs} );
    }

    public Expression threeValuedPrimitiveEqualsExpression( final Expression lhs, final Expression rhs, final CodeGenType codeGenType )
    {
        scala.Predef.var10000;
        boolean var10001;
        label25:
        {
            label27:
            {
                var10000 = scala.Predef..MODULE$;
                CypherCodeGenType var4 = CodeGenType$.MODULE$.primitiveNode();
                if ( codeGenType == null )
                {
                    if ( var4 == null )
                    {
                        break label27;
                    }
                }
                else if ( codeGenType.equals( var4 ) )
                {
                    break label27;
                }

                CypherCodeGenType var5 = CodeGenType$.MODULE$.primitiveRel();
                if ( codeGenType == null )
                {
                    if ( var5 == null )
                    {
                        break label27;
                    }
                }
                else if ( codeGenType.equals( var5 ) )
                {
                    break label27;
                }

                var10001 = false;
                break label25;
            }

            var10001 = true;
        }

        var10000. assert (var10001);
        return Expression.ternary( Expression.or( Expression.equal( GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ), lhs ),
                Expression.equal( GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ), rhs ) ), Expression.constant( (Object) null ),
                this.box( (Expression) Expression.equal( lhs, rhs ), CodeGenType$.MODULE$.primitiveBool() ) );
    }

    public Expression equalityExpression( final Expression lhs, final Expression rhs, final CodeGenType codeGenType )
    {
        return codeGenType.isPrimitive() ? Expression.equal( lhs, rhs ) : Expression.invoke( lhs, Methods$.MODULE$.equals(), new Expression[]{rhs} );
    }

    public Expression primitiveEquals( final Expression lhs, final Expression rhs )
    {
        return Expression.equal( lhs, rhs );
    }

    public Expression orExpression( final Expression lhs, final Expression rhs )
    {
        return Expression.or( lhs, rhs );
    }

    public Expression threeValuedOrExpression( final Expression lhs, final Expression rhs )
    {
        return Expression.invoke( Methods$.MODULE$.or(), new Expression[]{lhs, rhs} );
    }

    public void markAsNull( final String varName, final CodeGenType codeGenType )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.lowerType( codeGenType ), varName,
                GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ) );
    }

    public Expression isNull( final String varName, final CodeGenType codeGenType )
    {
        return this.isNull( this.generator().load( varName ), codeGenType );
    }

    public Expression isNull( final Expression expr, final CodeGenType codeGenType )
    {
        return Expression.equal( GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ), expr );
    }

    public Expression notNull( final Expression expr, final CodeGenType codeGenType )
    {
        return Expression.not( this.isNull( expr, codeGenType ) );
    }

    public Expression notNull( final String varName, final CodeGenType codeGenType )
    {
        return this.notNull( this.generator().load( varName ), codeGenType );
    }

    public Expression ifNullThenNoValue( final Expression expr )
    {
        return Expression.ternary( Expression.isNull( expr ), this.noValue(), expr );
    }

    public Expression box( final Expression expression, final CodeGenType codeGenType )
    {
        boolean var4 = false;
        CypherCodeGenType var5 = null;
        Expression var3;
        if ( codeGenType instanceof CypherCodeGenType )
        {
            label39:
            {
                var4 = true;
                var5 = (CypherCodeGenType) codeGenType;
                CypherType var7 = var5.ct();
                RepresentationType var8 = var5.repr();
                NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                if ( var10000 == null )
                {
                    if ( var7 != null )
                    {
                        break label39;
                    }
                }
                else if ( !var10000.equals( var7 ) )
                {
                    break label39;
                }

                if ( LongType$.MODULE$.equals( var8 ) )
                {
                    var3 = Templates$.MODULE$.createNewNodeReference( expression );
                    return var3;
                }
            }
        }

        if ( var4 )
        {
            label30:
            {
                CypherType var10 = var5.ct();
                RepresentationType var11 = var5.repr();
                RelationshipType var13 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                if ( var13 == null )
                {
                    if ( var10 != null )
                    {
                        break label30;
                    }
                }
                else if ( !var13.equals( var10 ) )
                {
                    break label30;
                }

                if ( LongType$.MODULE$.equals( var11 ) )
                {
                    var3 = Templates$.MODULE$.createNewRelationshipReference( expression );
                    return var3;
                }
            }
        }

        var3 = Expression.box( expression );
        return var3;
    }

    public Expression unbox( final Expression expression, final CodeGenType codeGenType )
    {
        boolean var4 = false;
        CypherCodeGenType var5 = null;
        Expression var3;
        if ( codeGenType.isPrimitive() )
        {
            var3 = expression;
        }
        else
        {
            if ( codeGenType instanceof CypherCodeGenType )
            {
                label42:
                {
                    var4 = true;
                    var5 = (CypherCodeGenType) codeGenType;
                    CypherType var7 = var5.ct();
                    RepresentationType var8 = var5.repr();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var7 != null )
                        {
                            break label42;
                        }
                    }
                    else if ( !var10000.equals( var7 ) )
                    {
                        break label42;
                    }

                    if ( ReferenceType$.MODULE$.equals( var8 ) )
                    {
                        var3 = Expression.invoke( Methods$.MODULE$.unboxNode(), new Expression[]{expression} );
                        return var3;
                    }
                }
            }

            if ( var4 )
            {
                label33:
                {
                    CypherType var10 = var5.ct();
                    RepresentationType var11 = var5.repr();
                    RelationshipType var13 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var13 == null )
                    {
                        if ( var10 != null )
                        {
                            break label33;
                        }
                    }
                    else if ( !var13.equals( var10 ) )
                    {
                        break label33;
                    }

                    if ( ReferenceType$.MODULE$.equals( var11 ) )
                    {
                        var3 = Expression.invoke( Methods$.MODULE$.unboxRel(), new Expression[]{expression} );
                        return var3;
                    }
                }
            }

            var3 = Expression.unbox( expression );
        }

        return var3;
    }

    public Expression toFloat( final Expression expression )
    {
        return Expression.toDouble( expression );
    }

    public void nodeGetRelationshipsWithDirection( final String iterVar, final String nodeVar, final CodeGenType nodeVarType,
            final SemanticDirection direction )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ) ), iterVar, Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorUtils.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        RelationshipSelectionCursor.class ) ), "nodeGetRelationships", new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorFactory.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Direction.class ))}),
        new Expression[]{this.dataRead(), this.cursors(), this.nodeCursor(), this.forceLong( nodeVar, nodeVarType ), this.dir( direction )}));
        this.generator().expression( Expression.pop(
                Expression.invoke( Expression.get( this.generator().self(), this.fields().closeables() ), Methods$.MODULE$.listAdd(),
                        new Expression[]{this.generator().load( iterVar )} ) ) );
    }

    public void nodeGetRelationshipsWithDirectionAndTypes( final String iterVar, final String nodeVar, final CodeGenType nodeVarType,
            final SemanticDirection direction, final Seq<String> typeVars )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ) ), iterVar, Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorUtils.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        RelationshipSelectionCursor.class ) ), "nodeGetRelationships", new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorFactory.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Direction.class )),
        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType( scala.reflect.ManifestFactory..MODULE$.Int()))}),
        new Expression[]{this.dataRead(), this.cursors(), this.nodeCursor(), this.forceLong( nodeVar, nodeVarType ), this.dir( direction ),
                Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
                (Expression[]) ((TraversableOnce) typeVars.map( ( x$1 ) -> {
                    return this.generator().load( x$1 );
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))}));
        this.generator().expression( Expression.pop(
                Expression.invoke( Expression.get( this.generator().self(), this.fields().closeables() ), Methods$.MODULE$.listAdd(),
                        new Expression[]{this.generator().load( iterVar )} ) ) );
    }

    public void connectingRelationships( final String iterVar, final String fromNode, final CodeGenType fromNodeType, final SemanticDirection direction,
            final String toNode, final CodeGenType toNodeType )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ) ), iterVar, Expression.invoke( Methods$.MODULE$.allConnectingRelationships(),
                new Expression[]{this.dataRead(), this.cursors(), this.nodeCursor(), this.forceLong( fromNode, fromNodeType ), this.dir( direction ),
                        this.forceLong( toNode, toNodeType )} ));
        this.generator().expression( Expression.pop(
                Expression.invoke( Expression.get( this.generator().self(), this.fields().closeables() ), Methods$.MODULE$.listAdd(),
                        new Expression[]{this.generator().load( iterVar )} ) ) );
    }

    public void connectingRelationships( final String iterVar, final String fromNode, final CodeGenType fromNodeType, final SemanticDirection direction,
            final Seq<String> typeVars, final String toNode, final CodeGenType toNodeType )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                RelationshipSelectionCursor.class ) ), iterVar, Expression.invoke( Methods$.MODULE$.connectingRelationships(),
                new Expression[]{this.dataRead(), this.cursors(), this.nodeCursor(), this.forceLong( fromNode, fromNodeType ), this.dir( direction ),
                        this.forceLong( toNode, toNodeType ),
                        Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
                        (Expression[]) ((TraversableOnce) typeVars.map( ( x$1 ) -> {
                            return this.generator().load( x$1 );
                        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))}));
        this.generator().expression( Expression.pop(
                Expression.invoke( Expression.get( this.generator().self(), this.fields().closeables() ), Methods$.MODULE$.listAdd(),
                        new Expression[]{this.generator().load( iterVar )} ) ) );
    }

    public Expression loadVariable( final String varName )
    {
        return this.generator().load( varName );
    }

    public Expression multiplyPrimitive( final Expression lhs, final Expression rhs )
    {
        return Expression.multiply( lhs, rhs );
    }

    public Expression addExpression( final Expression lhs, final Expression rhs )
    {
        return this.math( Methods$.MODULE$.mathAdd(), lhs, rhs );
    }

    public Expression subtractExpression( final Expression lhs, final Expression rhs )
    {
        return this.math( Methods$.MODULE$.mathSub(), lhs, rhs );
    }

    public Expression multiplyExpression( final Expression lhs, final Expression rhs )
    {
        return this.math( Methods$.MODULE$.mathMul(), lhs, rhs );
    }

    public Expression divideExpression( final Expression lhs, final Expression rhs )
    {
        return this.math( Methods$.MODULE$.mathDiv(), lhs, rhs );
    }

    public Expression modulusExpression( final Expression lhs, final Expression rhs )
    {
        return this.math( Methods$.MODULE$.mathMod(), lhs, rhs );
    }

    public Expression powExpression( final Expression lhs, final Expression rhs )
    {
        return this.math( Methods$.MODULE$.mathPow(), lhs, rhs );
    }

    private Expression math( final MethodReference method, final Expression lhs, final Expression rhs )
    {
        return Expression.invoke( method, new Expression[]{lhs, rhs} );
    }

    private Expression dataRead()
    {
        return Expression.invoke( this.generator().self(), MethodReference.methodReference( this.generator().owner(),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class ) ), "getOrLoadDataRead",
                new TypeReference[0] ),new Expression[0]);
    }

    private Expression tokenRead()
    {
        return Expression.invoke( this.generator().self(), MethodReference.methodReference( this.generator().owner(),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( TokenRead.class ) ), "getOrLoadTokenRead",
                new TypeReference[0] ),new Expression[0]);
    }

    private Expression schemaRead()
    {
        return Expression.invoke( this.generator().self(), MethodReference.methodReference( this.generator().owner(),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( SchemaRead.class ) ), "getOrLoadSchemaRead",
                new TypeReference[0] ),new Expression[0]);
    }

    private Expression cursors()
    {
        return Expression.invoke( this.generator().self(), MethodReference.methodReference( this.generator().owner(),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorFactory.class ) ), "getOrLoadCursors",
                new TypeReference[0] ),new Expression[0]);
    }

    private Expression nodeCursor()
    {
        return Expression.invoke( this.generator().self(), MethodReference.methodReference( this.generator().owner(),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class ) ), "nodeCursor",
                new TypeReference[0] ),new Expression[0]);
    }

    private Expression relationshipScanCursor()
    {
        return Expression.invoke( this.generator().self(), MethodReference.methodReference( this.generator().owner(),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( RelationshipScanCursor.class ) ),
                "relationshipScanCursor", new TypeReference[0] ),new Expression[0]);
    }

    private Expression propertyCursor()
    {
        return Expression.invoke( this.generator().self(), MethodReference.methodReference( this.generator().owner(),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PropertyCursor.class ) ), "propertyCursor",
                new TypeReference[0] ),new Expression[0]);
    }

    private Expression nodeManager()
    {
        return Expression.get( this.generator().self(), this.fields().entityAccessor() );
    }

    private Expression resultRow()
    {
        return this.generator().load( "row" );
    }

    private Expression tracer()
    {
        return Expression.get( this.generator().self(), this.fields().tracer() );
    }

    private Expression params()
    {
        return Expression.get( this.generator().self(), this.fields().params() );
    }

    private Expression parameterNotFoundException( final String key )
    {
        return Expression.invoke( Expression.newInstance(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( ParameterNotFoundException.class ) ) ),
        MethodReference.constructorReference( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                ParameterNotFoundException.class ) ), new TypeReference[]{
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ))}),
        new Expression[]{Expression.constant( (new StringBuilder( 27 )).append( "Expected a parameter named " ).append( key ).toString() )});
    }

    private Expression dir( final SemanticDirection dir )
    {
        Expression var2;
        if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( dir )){
        var2 = Templates$.MODULE$.incoming();
    } else if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( dir )){
        var2 = Templates$.MODULE$.outgoing();
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( dir )){
            throw new MatchError( dir );
        }

        var2 = Templates$.MODULE$.both();
    }

        return var2;
    }

    public Expression asList( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asList( values, scala.reflect.ManifestFactory..MODULE$.Object());
    }

    public Expression asAnyValueList( final Seq<Expression> values )
    {
        return Templates$.MODULE$.asAnyValueList( values );
    }

    public Expression asPrimitiveStream( final Expression publicTypeList, final CodeGenType codeGenType )
    {
        boolean var4 = false;
        CypherCodeGenType var5 = null;
        Expression var3;
        if ( codeGenType instanceof CypherCodeGenType )
        {
            var4 = true;
            var5 = (CypherCodeGenType) codeGenType;
            CypherType var7 = var5.ct();
            RepresentationType var8 = var5.repr();
            Option var9 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var7 );
            if ( !var9.isEmpty() )
            {
                label86:
                {
                    CypherType var10 = (CypherType) var9.get();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var10 != null )
                        {
                            break label86;
                        }
                    }
                    else if ( !var10000.equals( var10 ) )
                    {
                        break label86;
                    }

                    if ( var8 instanceof ListReferenceType )
                    {
                        ListReferenceType var12 = (ListReferenceType) var8;
                        RepresentationType var13 = var12.inner();
                        if ( LongType$.MODULE$.equals( var13 ) )
                        {
                            var3 = Expression.invoke( MethodReference.methodReference(
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveNodeStream.class ) ),
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                            PrimitiveNodeStream.class ) ), "of", new TypeReference[]{
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())}),
                            new Expression[]{publicTypeList});
                            return var3;
                        }
                    }
                }
            }
        }

        if ( var4 )
        {
            CypherType var14 = var5.ct();
            RepresentationType var15 = var5.repr();
            Option var16 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var14 );
            if ( !var16.isEmpty() )
            {
                label78:
                {
                    CypherType var17 = (CypherType) var16.get();
                    RelationshipType var30 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var30 == null )
                    {
                        if ( var17 != null )
                        {
                            break label78;
                        }
                    }
                    else if ( !var30.equals( var17 ) )
                    {
                        break label78;
                    }

                    if ( var15 instanceof ListReferenceType )
                    {
                        ListReferenceType var19 = (ListReferenceType) var15;
                        RepresentationType var20 = var19.inner();
                        if ( LongType$.MODULE$.equals( var20 ) )
                        {
                            var3 = Expression.invoke( MethodReference.methodReference(
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                            PrimitiveRelationshipStream.class ) ),
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                            PrimitiveRelationshipStream.class ) ), "of", new TypeReference[]{
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())}),
                            new Expression[]{publicTypeList});
                            return var3;
                        }
                    }
                }
            }
        }

        if ( var4 )
        {
            RepresentationType var21 = var5.repr();
            if ( var21 instanceof ListReferenceType )
            {
                ListReferenceType var22 = (ListReferenceType) var21;
                RepresentationType var23 = var22.inner();
                if ( LongType$.MODULE$.equals( var23 ) )
                {
                    var3 = Expression.invoke( MethodReference.methodReference(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ) ),
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                    LongStream.class ) ), "toLongStream", new TypeReference[]{
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())}),new Expression[]{publicTypeList});
                    return var3;
                }
            }
        }

        if ( var4 )
        {
            RepresentationType var24 = var5.repr();
            if ( var24 instanceof ListReferenceType )
            {
                ListReferenceType var25 = (ListReferenceType) var24;
                RepresentationType var26 = var25.inner();
                if ( FloatType$.MODULE$.equals( var26 ) )
                {
                    var3 = Expression.invoke( MethodReference.methodReference(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ) ),
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                    DoubleStream.class ) ), "toDoubleStream", new TypeReference[]{
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())}),new Expression[]{publicTypeList});
                    return var3;
                }
            }
        }

        if ( !var4 )
        {
            throw new IllegalArgumentException(
                    (new StringBuilder( 46 )).append( "CodeGenType " ).append( codeGenType ).append( " not supported as primitive stream" ).toString() );
        }
        else
        {
            RepresentationType var27 = var5.repr();
            if ( !(var27 instanceof ListReferenceType) )
            {
                throw new IllegalArgumentException(
                        (new StringBuilder( 46 )).append( "CodeGenType " ).append( codeGenType ).append( " not supported as primitive stream" ).toString() );
            }
            else
            {
                ListReferenceType var28 = (ListReferenceType) var27;
                RepresentationType var29 = var28.inner();
                if ( !BoolType$.MODULE$.equals( var29 ) )
                {
                    throw new IllegalArgumentException( (new StringBuilder( 46 )).append( "CodeGenType " ).append( codeGenType ).append(
                            " not supported as primitive stream" ).toString() );
                }
                else
                {
                    var3 = Expression.invoke( MethodReference.methodReference(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ) ),
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                    IntStream.class ) ), "toBooleanStream", new TypeReference[]{
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())}),new Expression[]{publicTypeList});
                    return var3;
                }
            }
        }
    }

    public Expression asPrimitiveStream( final Seq<Expression> values, final CodeGenType codeGenType )
    {
        boolean var4 = false;
        CypherCodeGenType var5 = null;
        Expression var3;
        if ( codeGenType instanceof CypherCodeGenType )
        {
            var4 = true;
            var5 = (CypherCodeGenType) codeGenType;
            CypherType var7 = var5.ct();
            RepresentationType var8 = var5.repr();
            Option var9 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var7 );
            if ( !var9.isEmpty() )
            {
                label86:
                {
                    CypherType var10 = (CypherType) var9.get();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var10 != null )
                        {
                            break label86;
                        }
                    }
                    else if ( !var10000.equals( var10 ) )
                    {
                        break label86;
                    }

                    if ( var8 instanceof ListReferenceType )
                    {
                        ListReferenceType var12 = (ListReferenceType) var8;
                        RepresentationType var13 = var12.inner();
                        if ( LongType$.MODULE$.equals( var13 ) )
                        {
                            var3 = Templates$.MODULE$.asPrimitiveNodeStream( values );
                            return var3;
                        }
                    }
                }
            }
        }

        if ( var4 )
        {
            CypherType var14 = var5.ct();
            RepresentationType var15 = var5.repr();
            Option var16 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var14 );
            if ( !var16.isEmpty() )
            {
                label78:
                {
                    CypherType var17 = (CypherType) var16.get();
                    RelationshipType var30 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var30 == null )
                    {
                        if ( var17 != null )
                        {
                            break label78;
                        }
                    }
                    else if ( !var30.equals( var17 ) )
                    {
                        break label78;
                    }

                    if ( var15 instanceof ListReferenceType )
                    {
                        ListReferenceType var19 = (ListReferenceType) var15;
                        RepresentationType var20 = var19.inner();
                        if ( LongType$.MODULE$.equals( var20 ) )
                        {
                            var3 = Templates$.MODULE$.asPrimitiveRelationshipStream( values );
                            return var3;
                        }
                    }
                }
            }
        }

        if ( var4 )
        {
            RepresentationType var21 = var5.repr();
            if ( var21 instanceof ListReferenceType )
            {
                ListReferenceType var22 = (ListReferenceType) var21;
                RepresentationType var23 = var22.inner();
                if ( LongType$.MODULE$.equals( var23 ) )
                {
                    var3 = Templates$.MODULE$.asLongStream( values );
                    return var3;
                }
            }
        }

        if ( var4 )
        {
            RepresentationType var24 = var5.repr();
            if ( var24 instanceof ListReferenceType )
            {
                ListReferenceType var25 = (ListReferenceType) var24;
                RepresentationType var26 = var25.inner();
                if ( FloatType$.MODULE$.equals( var26 ) )
                {
                    var3 = Templates$.MODULE$.asDoubleStream( values );
                    return var3;
                }
            }
        }

        if ( !var4 )
        {
            throw new IllegalArgumentException(
                    (new StringBuilder( 46 )).append( "CodeGenType " ).append( codeGenType ).append( " not supported as primitive stream" ).toString() );
        }
        else
        {
            RepresentationType var27 = var5.repr();
            if ( !(var27 instanceof ListReferenceType) )
            {
                throw new IllegalArgumentException(
                        (new StringBuilder( 46 )).append( "CodeGenType " ).append( codeGenType ).append( " not supported as primitive stream" ).toString() );
            }
            else
            {
                ListReferenceType var28 = (ListReferenceType) var27;
                RepresentationType var29 = var28.inner();
                if ( !BoolType$.MODULE$.equals( var29 ) )
                {
                    throw new IllegalArgumentException( (new StringBuilder( 46 )).append( "CodeGenType " ).append( codeGenType ).append(
                            " not supported as primitive stream" ).toString() );
                }
                else
                {
                    var3 = Templates$.MODULE$.asIntStream( (Seq) values.map( ( x$3 ) -> {
                        return Expression.ternary( x$3, Expression.constant( BoxesRunTime.boxToInteger( 1 ) ),
                                Expression.constant( BoxesRunTime.boxToInteger( 0 ) ) );
                    }, scala.collection.Seq..MODULE$.canBuildFrom() ));
                    return var3;
                }
            }
        }
    }

    public void declarePrimitiveIterator( final String name, final CodeGenType iterableCodeGenType )
    {
        LocalVariable var3;
        label52:
        {
            boolean var5 = false;
            CypherCodeGenType var6 = null;
            if ( iterableCodeGenType instanceof CypherCodeGenType )
            {
                var5 = true;
                var6 = (CypherCodeGenType) iterableCodeGenType;
                CypherType var8 = var6.ct();
                RepresentationType var9 = var6.repr();
                Option var10 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var8 );
                if ( !var10.isEmpty() && var9 instanceof ListReferenceType )
                {
                    ListReferenceType var11 = (ListReferenceType) var9;
                    RepresentationType var12 = var11.inner();
                    if ( LongType$.MODULE$.equals( var12 ) )
                    {
                        var3 = this.generator().declare(
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( OfLong.class ) ), name);
                        break label52;
                    }
                }
            }

            if ( var5 )
            {
                CypherType var13 = var6.ct();
                RepresentationType var14 = var6.repr();
                Option var15 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var13 );
                if ( !var15.isEmpty() && var14 instanceof ListReferenceType )
                {
                    ListReferenceType var16 = (ListReferenceType) var14;
                    RepresentationType var17 = var16.inner();
                    if ( FloatType$.MODULE$.equals( var17 ) )
                    {
                        var3 = this.generator().declare(
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( OfDouble.class ) ), name);
                        break label52;
                    }
                }
            }

            if ( !var5 )
            {
                throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                        " not supported as primitive iterator" ).toString() );
            }

            CypherType var18 = var6.ct();
            RepresentationType var19 = var6.repr();
            Option var20 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var18 );
            if ( var20.isEmpty() || !(var19 instanceof ListReferenceType) )
            {
                throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                        " not supported as primitive iterator" ).toString() );
            }

            ListReferenceType var21 = (ListReferenceType) var19;
            RepresentationType var22 = var21.inner();
            if ( !BoolType$.MODULE$.equals( var22 ) )
            {
                throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                        " not supported as primitive iterator" ).toString() );
            }

            var3 = this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( OfInt.class ) ), name);
        }

        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( name ), var3));
    }

    public Expression primitiveIteratorFrom( final Expression iterable, final CodeGenType iterableCodeGenType )
    {
        boolean var4 = false;
        CypherCodeGenType var5 = null;
        Expression var3;
        if ( iterableCodeGenType instanceof CypherCodeGenType )
        {
            var4 = true;
            var5 = (CypherCodeGenType) iterableCodeGenType;
            CypherType var7 = var5.ct();
            RepresentationType var8 = var5.repr();
            Option var9 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var7 );
            if ( !var9.isEmpty() )
            {
                label93:
                {
                    CypherType var10 = (CypherType) var9.get();
                    NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var10 != null )
                        {
                            break label93;
                        }
                    }
                    else if ( !var10000.equals( var10 ) )
                    {
                        break label93;
                    }

                    if ( var8 instanceof ListReferenceType )
                    {
                        ListReferenceType var12 = (ListReferenceType) var8;
                        RepresentationType var13 = var12.inner();
                        if ( LongType$.MODULE$.equals( var13 ) )
                        {
                            var3 = Expression.invoke( Expression.cast(
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( PrimitiveNodeStream.class ) ),
                                    iterable ), GeneratedQueryStructure$.MODULE$.method( "primitiveIterator", scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
                            MODULE$.classType( PrimitiveNodeStream.class ), scala.reflect.ManifestFactory..MODULE$.classType( OfLong.class )),new Expression[0])
                            ;
                            return var3;
                        }
                    }
                }
            }
        }

        if ( var4 )
        {
            CypherType var14 = var5.ct();
            RepresentationType var15 = var5.repr();
            Option var16 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var14 );
            if ( !var16.isEmpty() )
            {
                label85:
                {
                    CypherType var17 = (CypherType) var16.get();
                    RelationshipType var36 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var36 == null )
                    {
                        if ( var17 != null )
                        {
                            break label85;
                        }
                    }
                    else if ( !var36.equals( var17 ) )
                    {
                        break label85;
                    }

                    if ( var15 instanceof ListReferenceType )
                    {
                        ListReferenceType var19 = (ListReferenceType) var15;
                        RepresentationType var20 = var19.inner();
                        if ( LongType$.MODULE$.equals( var20 ) )
                        {
                            var3 = Expression.invoke( Expression.cast(
                                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                                            PrimitiveRelationshipStream.class ) ), iterable ), GeneratedQueryStructure$.MODULE$.method( "primitiveIterator",
                                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
                            MODULE$.classType( PrimitiveRelationshipStream.class ), scala.reflect.ManifestFactory..MODULE$.classType( OfLong.class )),
                            new Expression[0]);
                            return var3;
                        }
                    }
                }
            }
        }

        if ( var4 )
        {
            CypherType var21 = var5.ct();
            RepresentationType var22 = var5.repr();
            Option var23 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var21 );
            if ( !var23.isEmpty() && var22 instanceof ListReferenceType )
            {
                ListReferenceType var24 = (ListReferenceType) var22;
                RepresentationType var25 = var24.inner();
                if ( LongType$.MODULE$.equals( var25 ) )
                {
                    var3 = Expression.invoke(
                            Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongStream.class ) ),
                            iterable ), GeneratedQueryStructure$.MODULE$.method( "iterator", scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
                    MODULE$.classType( LongStream.class ), scala.reflect.ManifestFactory..MODULE$.classType( OfLong.class )),new Expression[0]);
                    return var3;
                }
            }
        }

        if ( var4 )
        {
            CypherType var26 = var5.ct();
            RepresentationType var27 = var5.repr();
            Option var28 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var26 );
            if ( !var28.isEmpty() && var27 instanceof ListReferenceType )
            {
                ListReferenceType var29 = (ListReferenceType) var27;
                RepresentationType var30 = var29.inner();
                if ( FloatType$.MODULE$.equals( var30 ) )
                {
                    var3 = Expression.invoke(
                            Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( DoubleStream.class ) ),
                            iterable ), GeneratedQueryStructure$.MODULE$.method( "iterator", scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
                    MODULE$.classType( DoubleStream.class ), scala.reflect.ManifestFactory..MODULE$.classType( OfDouble.class )),new Expression[0]);
                    return var3;
                }
            }
        }

        if ( !var4 )
        {
            throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                    " not supported as primitive iterator" ).toString() );
        }
        else
        {
            CypherType var31 = var5.ct();
            RepresentationType var32 = var5.repr();
            Option var33 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var31 );
            if ( var33.isEmpty() || !(var32 instanceof ListReferenceType) )
            {
                throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                        " not supported as primitive iterator" ).toString() );
            }
            else
            {
                ListReferenceType var34 = (ListReferenceType) var32;
                RepresentationType var35 = var34.inner();
                if ( !BoolType$.MODULE$.equals( var35 ) )
                {
                    throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                            " not supported as primitive iterator" ).toString() );
                }
                else
                {
                    var3 = Expression.invoke(
                            Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( IntStream.class ) ),
                            iterable ), GeneratedQueryStructure$.MODULE$.method( "iterator", scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
                    MODULE$.classType( IntStream.class ), scala.reflect.ManifestFactory..MODULE$.classType( OfInt.class )),new Expression[0]);
                    return var3;
                }
            }
        }
    }

    public Expression primitiveIteratorNext( final Expression iterator, final CodeGenType iterableCodeGenType )
    {
        boolean var4 = false;
        CypherCodeGenType var5 = null;
        Expression var3;
        if ( iterableCodeGenType instanceof CypherCodeGenType )
        {
            var4 = true;
            var5 = (CypherCodeGenType) iterableCodeGenType;
            CypherType var7 = var5.ct();
            RepresentationType var8 = var5.repr();
            Option var9 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var7 );
            if ( !var9.isEmpty() && var8 instanceof ListReferenceType )
            {
                ListReferenceType var10 = (ListReferenceType) var8;
                RepresentationType var11 = var10.inner();
                if ( LongType$.MODULE$.equals( var11 ) )
                {
                    var3 = Expression.invoke( iterator,
                            GeneratedQueryStructure$.MODULE$.method( "nextLong", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                            scala.reflect.ManifestFactory..MODULE$.classType( OfLong.class ), scala.reflect.ManifestFactory..MODULE$.Long()),new Expression[0]);
                    return var3;
                }
            }
        }

        if ( var4 )
        {
            CypherType var12 = var5.ct();
            RepresentationType var13 = var5.repr();
            Option var14 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var12 );
            if ( !var14.isEmpty() && var13 instanceof ListReferenceType )
            {
                ListReferenceType var15 = (ListReferenceType) var13;
                RepresentationType var16 = var15.inner();
                if ( FloatType$.MODULE$.equals( var16 ) )
                {
                    var3 = Expression.invoke( iterator,
                            GeneratedQueryStructure$.MODULE$.method( "nextDouble", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                            scala.reflect.ManifestFactory..MODULE$.classType( OfDouble.class ), scala.reflect.ManifestFactory..MODULE$.Double()),
                    new Expression[0]);
                    return var3;
                }
            }
        }

        if ( !var4 )
        {
            throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                    " not supported as primitive iterator" ).toString() );
        }
        else
        {
            CypherType var17 = var5.ct();
            RepresentationType var18 = var5.repr();
            Option var19 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var17 );
            if ( var19.isEmpty() || !(var18 instanceof ListReferenceType) )
            {
                throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                        " not supported as primitive iterator" ).toString() );
            }
            else
            {
                ListReferenceType var20 = (ListReferenceType) var18;
                RepresentationType var21 = var20.inner();
                if ( !BoolType$.MODULE$.equals( var21 ) )
                {
                    throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                            " not supported as primitive iterator" ).toString() );
                }
                else
                {
                    var3 = Expression.not( Expression.equal( Expression.constant( BoxesRunTime.boxToInteger( 0 ) ), Expression.invoke( iterator,
                            GeneratedQueryStructure$.MODULE$.method( "nextInt", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                            scala.reflect.ManifestFactory..MODULE$.classType( OfInt.class ), scala.reflect.ManifestFactory..MODULE$.Int() ), new Expression[0])))
                    ;
                    return var3;
                }
            }
        }
    }

    public Expression primitiveIteratorHasNext( final Expression iterator, final CodeGenType iterableCodeGenType )
    {
        boolean var4 = false;
        CypherCodeGenType var5 = null;
        Expression var3;
        if ( iterableCodeGenType instanceof CypherCodeGenType )
        {
            var4 = true;
            var5 = (CypherCodeGenType) iterableCodeGenType;
            CypherType var7 = var5.ct();
            RepresentationType var8 = var5.repr();
            Option var9 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var7 );
            if ( !var9.isEmpty() && var8 instanceof ListReferenceType )
            {
                ListReferenceType var10 = (ListReferenceType) var8;
                RepresentationType var11 = var10.inner();
                if ( LongType$.MODULE$.equals( var11 ) )
                {
                    var3 = Expression.invoke( iterator,
                            GeneratedQueryStructure$.MODULE$.method( "hasNext", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                            scala.reflect.ManifestFactory..MODULE$.classType( OfLong.class ), scala.reflect.ManifestFactory..MODULE$.Long()),new Expression[0]);
                    return var3;
                }
            }
        }

        if ( var4 )
        {
            CypherType var12 = var5.ct();
            RepresentationType var13 = var5.repr();
            Option var14 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var12 );
            if ( !var14.isEmpty() && var13 instanceof ListReferenceType )
            {
                ListReferenceType var15 = (ListReferenceType) var13;
                RepresentationType var16 = var15.inner();
                if ( FloatType$.MODULE$.equals( var16 ) )
                {
                    var3 = Expression.invoke( iterator,
                            GeneratedQueryStructure$.MODULE$.method( "hasNext", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                            scala.reflect.ManifestFactory..MODULE$.classType( OfDouble.class ), scala.reflect.ManifestFactory..MODULE$.Double()),
                    new Expression[0]);
                    return var3;
                }
            }
        }

        if ( !var4 )
        {
            throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                    " not supported as primitive iterator" ).toString() );
        }
        else
        {
            CypherType var17 = var5.ct();
            RepresentationType var18 = var5.repr();
            Option var19 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var17 );
            if ( var19.isEmpty() || !(var18 instanceof ListReferenceType) )
            {
                throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                        " not supported as primitive iterator" ).toString() );
            }
            else
            {
                ListReferenceType var20 = (ListReferenceType) var18;
                RepresentationType var21 = var20.inner();
                if ( !BoolType$.MODULE$.equals( var21 ) )
                {
                    throw new IllegalArgumentException( (new StringBuilder( 48 )).append( "CodeGenType " ).append( iterableCodeGenType ).append(
                            " not supported as primitive iterator" ).toString() );
                }
                else
                {
                    var3 = Expression.invoke( iterator,
                            GeneratedQueryStructure$.MODULE$.method( "hasNext", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                            scala.reflect.ManifestFactory..MODULE$.classType( OfInt.class ), scala.reflect.ManifestFactory..MODULE$.Int()),new Expression[0]);
                    return var3;
                }
            }
        }
    }

    public void declareIterator( final String name )
    {
        LocalVariable variable = this.generator().declare(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                        scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),name);
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( name ), variable));
    }

    public void declareIterator( final String name, final CodeGenType elementCodeGenType )
    {
        LocalVariable variable = this.generator().declare(
                TypeReference.parameterizedType( Iterator.class, new TypeReference[]{GeneratedQueryStructure$.MODULE$.lowerType( elementCodeGenType )} ),
                name );
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( name ), variable));
    }

    public Expression iteratorFrom( final Expression iterable )
    {
        return Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "iteratorFrom", scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
        scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ), scala.reflect.ManifestFactory..
        MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),new Expression[]{iterable});
    }

    public Expression iteratorNext( final Expression iterator, final CodeGenType codeGenType )
    {
        return Expression.cast( GeneratedQueryStructure$.MODULE$.lowerType( codeGenType ), Expression.invoke( iterator,
                GeneratedQueryStructure$.MODULE$.method( "next", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(), scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),new Expression[0]));
    }

    public Expression iteratorHasNext( final Expression iterator )
    {
        return Expression.invoke( iterator,
                GeneratedQueryStructure$.MODULE$.method( "hasNext", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.wildcardType(
                scala.reflect.ManifestFactory..MODULE$.Nothing(),scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[0]);
    }

    public Expression toSet( final Expression value )
    {
        return Expression.invoke( MethodReference.methodReference(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ) ),
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Set.class,
                        scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),
        "toSet", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())}),new Expression[]{value});
    }

    public void newDistinctSet( final String name, final Iterable<CodeGenType> codeGenTypes )
    {
        label25:
        {
            if ( codeGenTypes.size() == 1 )
            {
                RepresentationType var10000 = ((CodeGenType) codeGenTypes.head()).repr();
                LongType$ var3 = LongType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var3 == null )
                    {
                        break label25;
                    }
                }
                else if ( var10000.equals( var3 ) )
                {
                    break label25;
                }
            }

            this.generator().assign( this.generator().declare(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                            scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ) )),name),
            Templates$.MODULE$.createNewInstance( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                    scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) )));
            return;
        }

        this.generator().assign(
                this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ) ),
                name ), Templates$.MODULE$.createNewInstance(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ) ), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) )));
    }

    public void distinctSetIfNotContains( final String name, final scala.collection.immutable.Map<String,Tuple2<CodeGenType,Expression>> structure,
            final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        label41:
        {
            if ( structure.size() == 1 )
            {
                RepresentationType var10000 = ((CodeGenType) ((Tuple2) ((Tuple2) structure.head())._2())._1()).repr();
                LongType$ var5 = LongType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var5 == null )
                    {
                        break label41;
                    }
                }
                else if ( var10000.equals( var5 ) )
                {
                    break label41;
                }
            }

            String tmpName = this.context.namer().newVarName();
            this.newUniqueAggregationKey( tmpName, structure );
            CodeBlock x$118 = this.generator().ifStatement( Expression.not(
                    Expression.invoke( this.generator().load( name ), Methods$.MODULE$.setContains(), new Expression[]{this.generator().load( tmpName )} ) ) );
            Function1 x$119 = ( body ) -> {
                $anonfun$distinctSetIfNotContains$2( this, name, block, tmpName, body );
                return BoxedUnit.UNIT;
            };
            Function1 x$120 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$118, x$119 );
            Function2 x$121 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$118, x$119 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$118, x$119, x$120, x$121 );
            return;
        }

        Tuple2 var7 = (Tuple2) structure.head();
        if ( var7 == null )
        {
            throw new MatchError( var7 );
        }
        else
        {
            Tuple2 var8 = (Tuple2) var7._2();
            if ( var8 == null )
            {
                throw new MatchError( var7 );
            }
            else
            {
                Expression value = (Expression) var8._2();
                CodeBlock x$108 = this.generator().ifStatement( Expression.not( Expression.invoke( this.generator().load( name ),
                        GeneratedQueryStructure$.MODULE$.method( "contains", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
                scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[]{value})))
                ;
                Function1 x$109 = ( body ) -> {
                    $anonfun$distinctSetIfNotContains$1( this, name, block, value, body );
                    return BoxedUnit.UNIT;
                };
                Function1 x$110 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$108, x$109 );
                Function2 x$111 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$108, x$109 );
                org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$108, x$109, x$110, x$111 );
            }
        }
    }

    public void distinctSetIterate( final String name, final HashableTupleDescriptor keyTupleDescriptor,
            final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        scala.collection.immutable.Map key;
        label32:
        {
            key = keyTupleDescriptor.structure();
            if ( key.size() == 1 )
            {
                RepresentationType var10000 = ((CodeGenType) ((Tuple2) key.head())._2()).repr();
                LongType$ var6 = LongType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var6 == null )
                    {
                        break label32;
                    }
                }
                else if ( var10000.equals( var6 ) )
                {
                    break label32;
                }
            }

            String localName = this.context.namer().newVarName();
            String next = this.context.namer().newVarName();
            LocalVariable variable = this.generator().declare(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                            scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),localName);
            TypeReference keyStruct = this.aux.hashableTypeReference( keyTupleDescriptor );
            this.generator().assign( variable, Expression.invoke( this.generator().load( name ),
                    GeneratedQueryStructure$.MODULE$.method( "iterator", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                    scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class, scala.reflect.ManifestFactory..MODULE$.Object(),
                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..
            MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),new Expression[0]));
            CodeBlock x$138 = this.generator().whileLoop( Expression.invoke( this.generator().load( localName ),
                    GeneratedQueryStructure$.MODULE$.method( "hasNext", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                    scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.Object(),
                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[0]));
            Function1 x$139 = ( body ) -> {
                $anonfun$distinctSetIterate$2( this, block, key, localName, next, keyStruct, body );
                return BoxedUnit.UNIT;
            };
            Function1 x$140 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$138, x$139 );
            Function2 x$141 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$138, x$139 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$138, x$139, x$140, x$141 );
            return;
        }

        Tuple2 var8 = (Tuple2) key.head();
        if ( var8 == null )
        {
            throw new MatchError( var8 );
        }
        else
        {
            String keyName = (String) var8._1();
            CodeGenType keyType = (CodeGenType) var8._2();
            Tuple2 var4 = new Tuple2( keyName, keyType );
            String keyName = (String) var4._1();
            CodeGenType keyType = (CodeGenType) var4._2();
            String localName = this.context.namer().newVarName();
            LocalVariable variable = this.generator().declare(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongIterator.class ) ), localName);
            this.generator().assign( variable, Expression.invoke( this.generator().load( name ),
                    GeneratedQueryStructure$.MODULE$.method( "longIterator", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                    scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ), scala.reflect.ManifestFactory..MODULE$.classType(
                    MutableLongIterator.class )),new Expression[0]));
            CodeBlock x$128 = this.generator().whileLoop( Expression.invoke( this.generator().load( localName ),
                    GeneratedQueryStructure$.MODULE$.method( "hasNext", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                    scala.reflect.ManifestFactory..MODULE$.classType( LongIterator.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[0]))
            ;
            Function1 x$129 = ( body ) -> {
                $anonfun$distinctSetIterate$1( this, block, keyName, localName, body );
                return BoxedUnit.UNIT;
            };
            Function1 x$130 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$128, x$129 );
            Function2 x$131 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$128, x$129 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$128, x$129, x$130, x$131 );
        }
    }

    public void newUniqueAggregationKey( final String varName, final scala.collection.immutable.Map<String,Tuple2<CodeGenType,Expression>> structure )
    {
        TypeReference typ = this.aux.hashableTypeReference( new HashableTupleDescriptor( (scala.collection.immutable.Map) structure.map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                String n = (String) x0$1._1();
                Tuple2 var4 = (Tuple2) x0$1._2();
                if ( var4 != null )
                {
                    CodeGenType t = (CodeGenType) var4._1();
                    Tuple2 var1 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( n ), t);
                    return var1;
                }
            }

            throw new MatchError( x0$1 );
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom() ) ));
        LocalVariable local = this.generator().declare( typ, varName );
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( varName ), local));
        this.generator().assign( local, Templates$.MODULE$.createNewInstance( typ, scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ) ));
        structure.foreach( ( x0$2 ) -> {
            $anonfun$newUniqueAggregationKey$2( this, varName, typ, x0$2 );
            return BoxedUnit.UNIT;
        } );
        if ( structure.size() == 1 )
        {
            Tuple2 var7 = (Tuple2) structure.values().head();
            if ( var7 == null )
            {
                throw new MatchError( var7 );
            }

            CodeGenType cgType = (CodeGenType) var7._1();
            Expression expression = (Expression) var7._2();
            Tuple2 var3 = new Tuple2( cgType, expression );
            CodeGenType cgType = (CodeGenType) var3._1();
            Expression expression = (Expression) var3._2();
            this.generator().put( this.generator().load( varName ),
                    FieldReference.field( typ, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
                    "hashCode" ), Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "hashCode", scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
            scala.reflect.ManifestFactory..MODULE$.classType( CompiledEquivalenceUtils.class ), scala.reflect.ManifestFactory..MODULE$.Int()),
            new Expression[]{this.box( expression, cgType )}));
        }
        else
        {
            TypeReference elementType = this.deriveCommonType( (Iterable) structure.values().map( ( x$6 ) -> {
                return (CodeGenType) x$6._1();
            }, scala.collection.Iterable..MODULE$.canBuildFrom() ));
            this.generator().put( this.generator().load( varName ),
                    FieldReference.field( typ, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
                    "hashCode" ), Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "hashCode", scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{TypeReference.arrayOf( elementType )}) ), scala.reflect.ManifestFactory..MODULE$.classType(
                    CompiledEquivalenceUtils.class ), scala.reflect.ManifestFactory..MODULE$.Int()),new Expression[]{
                elementType.isPrimitive() ? Expression.newInitializedArray( elementType, (Expression[]) ((TraversableOnce) structure.values().map( ( x$7 ) -> {
                    return (Expression) x$7._2();
                }, scala.collection.Iterable..MODULE$.canBuildFrom()) ).toSeq().toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ))) :
            Expression.newInitializedArray( elementType, (Expression[]) ((TraversableOnce) structure.values().map( ( e ) -> {
                return Expression.box( (Expression) e._2() );
            }, scala.collection.Iterable..MODULE$.canBuildFrom()) ).toSeq().toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))}));
        }
    }

    public void newAggregationMap( final String name, final IndexedSeq<CodeGenType> keyTypes )
    {
        LocalVariable local = this.generator().declare(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LinkedHashMap.class,
                        scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )})))),name);
        this.generator().assign( local, Templates$.MODULE$.createNewInstance(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LinkedHashMap.class,
                        scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )})))),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) )));
    }

    public void newMapOfSets( final String name, final IndexedSeq<CodeGenType> keyTypes, final CodeGenType elementType )
    {
        RepresentationType var10000;
        TypeReference var9;
        label33:
        {
            label32:
            {
                var10000 = elementType.repr();
                LongType$ var5 = LongType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var5 == null )
                    {
                        break label32;
                    }
                }
                else if ( var10000.equals( var5 ) )
                {
                    break label32;
                }

                var9 = GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                        scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )));
                break label33;
            }

            var9 = GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ));
        }

        TypeReference setType;
        label40:
        {
            setType = var9;
            if ( keyTypes.size() == 1 )
            {
                var10000 = ((CodeGenType) keyTypes.head()).repr();
                LongType$ var6 = LongType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var6 == null )
                    {
                        break label40;
                    }
                }
                else if ( var10000.equals( var6 ) )
                {
                    break label40;
                }
            }

            TypeReference typ = TypeReference.parameterizedType(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashMap.class,
                            scala.reflect.ManifestFactory..MODULE$.wildcardType( scala.reflect.ManifestFactory..MODULE$.Nothing(),
                    scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.wildcardType( scala.reflect.ManifestFactory..MODULE$.Nothing(),
                scala.reflect.ManifestFactory..MODULE$.Any())})))),
            new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),setType});
            this.generator().assign( this.generator().declare( typ, name ),
                    Templates$.MODULE$.createNewInstance( typ, scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ) ));
            return;
        }

        TypeReference typ = TypeReference.parameterizedType(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongObjectHashMap.class,
                        scala.reflect.ManifestFactory..MODULE$.wildcardType( scala.reflect.ManifestFactory..MODULE$.Nothing(),
                scala.reflect.ManifestFactory..MODULE$.Any()),scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),new TypeReference[]{setType})
        ;
        this.generator().assign( this.generator().declare( typ, name ),
                Templates$.MODULE$.createNewInstance( typ, scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ) ));
    }

    public void allocateSortTable( final String name, final SortTableDescriptor tableDescriptor, final Expression count )
    {
        TypeReference tableType = this.sortTableType( tableDescriptor );
        LocalVariable localVariable = this.generator().declare( tableType, name );
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( name ), localVariable));
        Expression boxedInteger = this.box( (Expression) count, CodeGenType$.MODULE$.Any() );
        this.generator().assign( localVariable, Templates$.MODULE$.createNewInstance( tableType, scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Tuple2[]{new Tuple2( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
                        Expression.invoke( Methods$.MODULE$.mathCastToInt(), new Expression[]{boxedInteger} ))}))));
    }

    public void sortTableAdd( final String name, final SortTableDescriptor tableDescriptor, final Expression value )
    {
        TypeReference tableType = this.sortTableType( tableDescriptor );
        this.generator().expression( Expression.pop( Expression.invoke( this.generator().load( name ),
                MethodReference.methodReference( tableType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean() ), "add",
                new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )}),
        new Expression[]{this.box( (Expression) value, CodeGenType$.MODULE$.Any() )})));
    }

    public void sortTableSort( final String name, final SortTableDescriptor tableDescriptor )
    {
        TypeReference tableType = this.sortTableType( tableDescriptor );
        this.generator().expression( Expression.invoke( this.generator().load( name ),
                MethodReference.methodReference( tableType, GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Unit() ), "sort",
                new TypeReference[0] ), new Expression[0] ));
    }

    public void sortTableIterate( final String tableName, final SortTableDescriptor tableDescriptor,
            final scala.collection.immutable.Map<String,String> varNameToField, final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        TupleDescriptor tupleDescriptor = tableDescriptor.tupleDescriptor();
        TypeReference tupleType = this.aux.typeReference( tupleDescriptor );
        String elementName = this.context.namer().newVarName();
        CodeBlock x$148 = this.generator().forEach( Parameter.param( tupleType, elementName ), this.generator().load( tableName ) );
        Function1 x$149 = ( body ) -> {
            $anonfun$sortTableIterate$1( this, varNameToField, block, tupleDescriptor, tupleType, elementName, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$150 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$148, x$149 );
        Function2 x$151 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$148, x$149 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$148, x$149, x$150, x$151 );
    }

    public void aggregationMapGet( final String mapName, final String valueVarName,
            final scala.collection.immutable.Map<String,Tuple2<CodeGenType,Expression>> key, final String keyVar )
    {
        LocalVariable local = this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), valueVarName)
        ;
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( valueVarName ), local));
        this.newUniqueAggregationKey( keyVar, key );
        this.generator().assign( local, this.unbox(
                (Expression) Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Long.class ) ),
                Expression.invoke( this.generator().load( mapName ),
                        GeneratedQueryStructure$.MODULE$.method( "getOrDefault", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( HashMap.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),scala.reflect.ManifestFactory..
        MODULE$.Object()),
        new Expression[]{this.generator().load( keyVar ), this.box( (Expression) Expression.constantLong( 0L ), CodeGenType$.MODULE$.javaLong() )})),
        new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger(), ReferenceType$.MODULE$)));
    }

    public void checkDistinct( final String name, final scala.collection.immutable.Map<String,Tuple2<CodeGenType,Expression>> key, final String keyVar,
            final Expression value, final CodeGenType valueType, final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        RepresentationType var10000;
        label82:
        {
            if ( key.size() == 1 )
            {
                var10000 = ((CodeGenType) ((Tuple2) ((Tuple2) key.head())._2())._1()).repr();
                LongType$ var8 = LongType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var8 == null )
                    {
                        break label82;
                    }
                }
                else if ( var10000.equals( var8 ) )
                {
                    break label82;
                }
            }

            String setVar;
            label83:
            {
                setVar = this.context.namer().newVarName();
                var10000 = valueType.repr();
                LongType$ var34 = LongType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var34 != null )
                    {
                        break label83;
                    }
                }
                else if ( !var10000.equals( var34 ) )
                {
                    break label83;
                }

                LocalVariable localVariable = this.generator().declare(
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ) ), setVar);
                if ( !this.locals.contains( keyVar ) )
                {
                    this.newUniqueAggregationKey( keyVar, key );
                }

                this.generator().assign( localVariable,
                        Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ) ),
                        Expression.invoke( this.generator().load( name ), GeneratedQueryStructure$.MODULE$.method( "get", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                scala.reflect.ManifestFactory..MODULE$.classType( HashMap.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class )}))),
                scala.reflect.ManifestFactory..MODULE$.Object()),new Expression[]{this.generator().load( keyVar )})));
                CodeBlock x$180 = this.generator().ifStatement( Expression.isNull( this.generator().load( setVar ) ) );
                Function1 x$181 = ( inner ) -> {
                    $anonfun$checkDistinct$5( this, name, keyVar, setVar, localVariable, inner );
                    return BoxedUnit.UNIT;
                };
                Function1 x$182 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$180, x$181 );
                Function2 x$183 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$180, x$181 );
                org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$180, x$181, x$182, x$183 );
                CodeBlock x$190 = this.generator().ifStatement( Expression.not( Expression.invoke( this.generator().load( setVar ),
                        GeneratedQueryStructure$.MODULE$.method( "contains", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
                scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[]{value})))
                ;
                Function1 x$191 = ( inner ) -> {
                    $anonfun$checkDistinct$6( this, value, block, setVar, inner );
                    return BoxedUnit.UNIT;
                };
                Function1 x$192 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$190, x$191 );
                Function2 x$193 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$190, x$191 );
                org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$190, x$191, x$192, x$193 );
                return;
            }

            LocalVariable localVariable = this.generator().declare(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                            scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),setVar);
            if ( !this.locals.contains( keyVar ) )
            {
                this.newUniqueAggregationKey( keyVar, key );
            }

            this.generator().assign( localVariable, Expression.cast(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                            scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ) )),
            Expression.invoke( this.generator().load( name ), GeneratedQueryStructure$.MODULE$.method( "get", scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
            scala.reflect.ManifestFactory..MODULE$.classType( HashMap.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
            MODULE$.wrapRefArray(
                    (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class, scala.reflect.ManifestFactory..MODULE$.Object(),
                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))}))),scala.reflect.ManifestFactory..MODULE$.Object()),
            new Expression[]{this.generator().load( keyVar )})));
            CodeBlock x$194 = this.generator().ifStatement( Expression.isNull( this.generator().load( setVar ) ) );
            Function1 x$195 = ( inner ) -> {
                $anonfun$checkDistinct$7( this, name, keyVar, setVar, localVariable, inner );
                return BoxedUnit.UNIT;
            };
            Function1 x$196 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$194, x$195 );
            Function2 x$197 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$194, x$195 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$194, x$195, x$196, x$197 );
            String valueVar = this.context.namer().newVarName();
            this.newUniqueAggregationKey( valueVar, (scala.collection.immutable.Map) scala.Predef..MODULE$.Map().apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                            this.context.namer().newVarName() ), scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                    valueType ), value))}))));
            CodeBlock x$204 = this.generator().ifStatement( Expression.not( Expression.invoke( this.generator().load( setVar ),
                    GeneratedQueryStructure$.MODULE$.method( "contains", scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
            scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
            MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),
            new Expression[]{this.generator().load( valueVar )})));
            Function1 x$205 = ( inner ) -> {
                $anonfun$checkDistinct$8( this, block, setVar, valueVar, inner );
                return BoxedUnit.UNIT;
            };
            Function1 x$206 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$204, x$205 );
            Function2 x$207 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$204, x$205 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$204, x$205, x$206, x$207 );
            return;
        }

        Tuple2 var10 = (Tuple2) key.head();
        if ( var10 == null )
        {
            throw new MatchError( var10 );
        }
        else
        {
            Tuple2 var11 = (Tuple2) var10._2();
            if ( var11 == null )
            {
                throw new MatchError( var10 );
            }
            else
            {
                Expression keyExpression;
                String tmp;
                label84:
                {
                    keyExpression = (Expression) var11._2();
                    tmp = this.context.namer().newVarName();
                    var10000 = valueType.repr();
                    LongType$ var14 = LongType$.MODULE$;
                    if ( var10000 == null )
                    {
                        if ( var14 == null )
                        {
                            break label84;
                        }
                    }
                    else if ( var10000.equals( var14 ) )
                    {
                        break label84;
                    }

                    LocalVariable localVariable = this.generator().declare(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                                    scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),tmp);
                    this.generator().assign( localVariable, Expression.cast(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class,
                                    scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ) )),
                    Expression.invoke( this.generator().load( name ), GeneratedQueryStructure$.MODULE$.method( "get", scala.Predef..MODULE$.wrapRefArray(
                            (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
                    scala.reflect.ManifestFactory..MODULE$.classType( LongObjectHashMap.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),new Expression[]{keyExpression})));
                    CodeBlock x$166 = this.generator().ifStatement( Expression.isNull( this.generator().load( tmp ) ) );
                    Function1 x$167 = ( inner ) -> {
                        $anonfun$checkDistinct$3( this, name, keyExpression, tmp, localVariable, inner );
                        return BoxedUnit.UNIT;
                    };
                    Function1 x$168 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$166, x$167 );
                    Function2 x$169 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$166, x$167 );
                    org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$166, x$167, x$168, x$169 );
                    CodeBlock x$176 = this.generator().ifStatement( Expression.not( Expression.invoke( this.generator().load( tmp ),
                            GeneratedQueryStructure$.MODULE$.method( "contains", scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                    scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[]{value})));
                    Function1 x$177 = ( inner ) -> {
                        $anonfun$checkDistinct$4( this, block, inner );
                        return BoxedUnit.UNIT;
                    };
                    Function1 x$178 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$176, x$177 );
                    Function2 x$179 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$176, x$177 );
                    org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$176, x$177, x$178, x$179 );
                    this.generator().expression( Expression.pop( Expression.invoke( this.generator().load( tmp ),
                            GeneratedQueryStructure$.MODULE$.method( "add", scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())})),
                    scala.reflect.ManifestFactory..MODULE$.classType( HashSet.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
                    MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[]{value})));
                    return;
                }

                LocalVariable localVariable = this.generator().declare(
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ) ), tmp);
                this.generator().assign( localVariable,
                        Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ) ),
                        Expression.invoke( this.generator().load( name ), GeneratedQueryStructure$.MODULE$.method( "get", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
                scala.reflect.ManifestFactory..MODULE$.classType( LongObjectHashMap.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
                MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Object()),new Expression[]{keyExpression})));
                CodeBlock x$152 = this.generator().ifStatement( Expression.isNull( this.generator().load( tmp ) ) );
                Function1 x$153 = ( inner ) -> {
                    $anonfun$checkDistinct$1( this, name, keyExpression, tmp, localVariable, inner );
                    return BoxedUnit.UNIT;
                };
                Function1 x$154 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$152, x$153 );
                Function2 x$155 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$152, x$153 );
                org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$152, x$153, x$154, x$155 );
                CodeBlock x$162 = this.generator().ifStatement( Expression.not( Expression.invoke( this.generator().load( tmp ),
                        GeneratedQueryStructure$.MODULE$.method( "contains", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
                scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[]{value})))
                ;
                Function1 x$163 = ( inner ) -> {
                    $anonfun$checkDistinct$2( this, block, inner );
                    return BoxedUnit.UNIT;
                };
                Function1 x$164 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$162, x$163 );
                Function2 x$165 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$162, x$163 );
                org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$162, x$163, x$164, x$165 );
                this.generator().expression( Expression.pop( Expression.invoke( this.generator().load( tmp ),
                        GeneratedQueryStructure$.MODULE$.method( "add", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long())})),
                scala.reflect.ManifestFactory..MODULE$.classType( LongHashSet.class ), scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[]{value})))
                ;
            }
        }
    }

    public void aggregationMapPut( final String name, final scala.collection.immutable.Map<String,Tuple2<CodeGenType,Expression>> key, final String keyVar,
            final Expression value )
    {
        if ( !this.locals.contains( keyVar ) )
        {
            this.newUniqueAggregationKey( keyVar, key );
        }

        this.generator().expression( Expression.pop( Expression.invoke( this.generator().load( name ),
                GeneratedQueryStructure$.MODULE$.method( "put", scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object()),
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object() )})),scala.reflect.ManifestFactory..
        MODULE$.classType( HashMap.class, scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),scala.reflect.ManifestFactory..
        MODULE$.Object()),new Expression[]{this.generator().load( keyVar ), this.box( (Expression) value, CodeGenType$.MODULE$.javaLong() )})));
    }

    public void aggregationMapIterate( final String name, final HashableTupleDescriptor keyTupleDescriptor, final String valueVar,
            final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        scala.collection.immutable.Map key = keyTupleDescriptor.structure();
        String localName = this.context.namer().newVarName();
        String next = this.context.namer().newVarName();
        LocalVariable variable = this.generator().declare(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class,
                        scala.reflect.ManifestFactory..MODULE$.classType( Entry.class, scala.reflect.ManifestFactory..MODULE$.Object(),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),localName);
        TypeReference keyStruct = this.aux.hashableTypeReference( keyTupleDescriptor );
        this.generator().assign( variable, Expression.invoke( Expression.invoke( this.generator().load( name ),
                GeneratedQueryStructure$.MODULE$.method( "entrySet", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( HashMap.class, scala.reflect.ManifestFactory..MODULE$.Object(),
                scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),
        scala.reflect.ManifestFactory..MODULE$.classType( Set.class, scala.reflect.ManifestFactory..MODULE$.classType( Entry.class,
            scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),new Expression[0]),
        GeneratedQueryStructure$.MODULE$.method( "iterator", scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
        MODULE$.classType( Set.class, scala.reflect.ManifestFactory..MODULE$.classType( Entry.class,
                scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..
        MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( Entry.class,
                scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) ))),new Expression[0]));
        CodeBlock x$214 = this.generator().whileLoop( Expression.invoke( this.generator().load( localName ),
                GeneratedQueryStructure$.MODULE$.method( "hasNext", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( Iterator.class, scala.reflect.ManifestFactory..MODULE$.classType( Entry.class,
                scala.reflect.ManifestFactory..MODULE$.Object(), scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Manifest[]{scala.reflect.ManifestFactory..MODULE$.classType( Long.class )}))),scala.Predef..
        MODULE$.wrapRefArray( (Object[]) (new Manifest[0]) )),scala.reflect.ManifestFactory..MODULE$.Boolean()),new Expression[0]));
        Function1 x$215 = ( body ) -> {
            $anonfun$aggregationMapIterate$1( this, valueVar, block, key, localName, next, keyStruct, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$216 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$214, x$215 );
        Function2 x$217 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$214, x$215 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$214, x$215, x$216, x$217 );
    }

    public Expression asMap( final scala.collection.immutable.Map<String,Expression> map )
    {
        Tuple2 var4 = map.toSeq().unzip( scala.Predef..MODULE$.$conforms());
        if ( var4 != null )
        {
            Seq keys = (Seq) var4._1();
            Seq values = (Seq) var4._2();
            if ( keys != null && values != null )
            {
                Tuple2 var2 = new Tuple2( keys, values );
                Seq keys = (Seq) var2._1();
                Seq values = (Seq) var2._2();
                return Expression.invoke( GeneratedQueryStructure$.MODULE$.method( "map", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[]{
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType(
                                        scala.reflect.ManifestFactory..MODULE$.classType( String.class )) ),
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType(
                                scala.reflect.ManifestFactory..MODULE$.classType( AnyValue.class ) ))})),scala.reflect.ManifestFactory..
                MODULE$.classType( VirtualValues.class ), scala.reflect.ManifestFactory..MODULE$.classType( MapValue.class )),new Expression[]{
                    Expression.newInitializedArray(
                            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( String.class ) ),
                    (Expression[]) ((TraversableOnce) keys.map( ( x$1 ) -> {
                        return Expression.constant( x$1 );
                    }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ))),
                Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                        AnyValue.class ) ), (Expression[]) values.toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))});
            }
        }

        throw new MatchError( var4 );
    }

    public void invokeMethod( final JoinTableType resultType, final String resultVar, final String methodName,
            final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        TypeReference returnType = this.joinTableType( resultType );
        this.generator().assign( returnType, resultVar, Expression.invoke( this.generator().self(),
                MethodReference.methodReference( this.generator().owner(), returnType, methodName, new TypeReference[0] ), new Expression[0] ) );
        CodeBlock x$224 = this.generator().classGenerator().generateMethod( returnType, methodName, new Parameter[0] );
        Function1 x$225 = ( body ) -> {
            $anonfun$invokeMethod$1( this, resultVar, block, body );
            return BoxedUnit.UNIT;
        };
        Function1 x$226 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$224, x$225 );
        Function2 x$227 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$224, x$225 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$224, x$225, x$226, x$227 );
    }

    public void allocateProbeTable( final String tableVar, final JoinTableType tableType )
    {
        this.generator().assign( this.joinTableType( tableType ), tableVar, this.allocate( tableType ) );
    }

    private TypeReference joinTableType( final JoinTableType resultType )
    {
        TypeReference var2;
        if ( LongToCountTable$.MODULE$.equals( resultType ) )
        {
            var2 = GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( LongIntHashMap.class ));
        }
        else if ( LongsToCountTable$.MODULE$.equals( resultType ) )
        {
            var2 = TypeReference.parameterizedType( HashMap.class, new Class[]{CompositeKey.class, Integer.class} );
        }
        else if ( resultType instanceof LongToListTable )
        {
            LongToListTable var5 = (LongToListTable) resultType;
            TupleDescriptor tupleDescriptor = var5.tupleDescriptor();
            var2 = TypeReference.parameterizedType( LongObjectHashMap.class,
                    new TypeReference[]{TypeReference.parameterizedType( ArrayList.class, new TypeReference[]{this.aux.typeReference( tupleDescriptor )} )} );
        }
        else
        {
            if ( !(resultType instanceof LongsToListTable) )
            {
                throw new MatchError( resultType );
            }

            LongsToListTable var7 = (LongsToListTable) resultType;
            TupleDescriptor tupleDescriptor = var7.tupleDescriptor();
            var2 = TypeReference.parameterizedType( HashMap.class, new TypeReference[]{
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                            CompositeKey.class ) ), TypeReference.parameterizedType( ArrayList.class,
                    new TypeReference[]{this.aux.typeReference( tupleDescriptor )} )});
        }

        return var2;
    }

    private Expression allocate( final JoinTableType resultType )
    {
        Expression var2;
        if ( LongToCountTable$.MODULE$.equals( resultType ) )
        {
            var2 = Templates$.MODULE$.newCountingMap();
        }
        else if ( resultType instanceof LongToListTable )
        {
            LongToListTable var4 = (LongToListTable) resultType;
            TupleDescriptor tupleDescriptor = var4.tupleDescriptor();
            var2 = Templates$.MODULE$.createNewInstance( TypeReference.parameterizedType( LongObjectHashMap.class,
                    new TypeReference[]{TypeReference.parameterizedType( ArrayList.class, new TypeReference[]{this.aux.typeReference( tupleDescriptor )} )} ),
                    scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ));
        }
        else if ( LongsToCountTable$.MODULE$.equals( resultType ) )
        {
            var2 = Templates$.MODULE$.createNewInstance( this.joinTableType( LongsToCountTable$.MODULE$ ), scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Tuple2[0]) ));
        }
        else
        {
            if ( !(resultType instanceof LongsToListTable) )
            {
                throw new MatchError( resultType );
            }

            LongsToListTable var6 = (LongsToListTable) resultType;
            var2 = Templates$.MODULE$.createNewInstance( this.joinTableType( var6 ), scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ));
        }

        return var2;
    }

    public void updateProbeTableCount( final String tableVar, final CountingJoinTableType tableType, final Seq<String> keyVars )
    {
        BoxedUnit var4;
        if ( LongToCountTable$.MODULE$.equals( tableType ) )
        {
            scala.Predef..MODULE$. assert (keyVars.size() == 1);
            String keyVar = (String) keyVars.head();
            this.generator().expression( Expression.pop( Expression.invoke( this.generator().load( tableVar ), Methods$.MODULE$.countingTableIncrement(),
                    new Expression[]{this.generator().load( keyVar ), Expression.constant( BoxesRunTime.boxToInteger( 1 ) )} ) ) );
            var4 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !LongsToCountTable$.MODULE$.equals( tableType ) )
            {
                throw new MatchError( tableType );
            }

            String countName = this.context.namer().newVarName();
            String keyName = this.context.namer().newVarName();
            this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                    CompositeKey.class ) ), keyName, Expression.invoke( Methods$.MODULE$.compositeKey(),
                    new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ),
                            (Expression[]) ((TraversableOnce) keyVars.map( ( x$1 ) -> {
                                return this.generator().load( x$1 );
                            }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))}));
            this.generator().assign(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Integer.class ) ), countName, Expression.cast(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Integer.class ) ), Expression.invoke(
                    this.generator().load( tableVar ), Methods$.MODULE$.countingTableCompositeKeyGet(), new Expression[]{this.generator().load( keyName )} )));
            this.generator().expression( Expression.pop( Expression.invoke( this.generator().load( tableVar ), Methods$.MODULE$.countingTableCompositeKeyPut(),
                    new Expression[]{this.generator().load( keyName ), Expression.ternary( Expression.isNull( this.generator().load( countName ) ),
                            this.box( (Expression) Expression.constantInt( 1 ), CodeGenType$.MODULE$.javaInt() ), this.box( (Expression) Expression.add(
                                    Expression.invoke( this.generator().load( countName ), Methods$.MODULE$.unboxInteger(), new Expression[0] ),
                                    Expression.constantInt( 1 ) ), CodeGenType$.MODULE$.javaInt() ) )} ) ) );
            var4 = BoxedUnit.UNIT;
        }
    }

    public void probe( final String tableVar, final JoinTableType tableType, final Seq<String> keyVars,
            final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        BoxedUnit var5;
        if ( LongToCountTable$.MODULE$.equals( tableType ) )
        {
            scala.Predef..MODULE$. assert (keyVars.size() == 1);
            String keyVar = (String) keyVars.head();
            LocalVariable times = this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
            this.context.namer().newVarName());
            this.generator().assign( times, Expression.invoke( this.generator().load( tableVar ), Methods$.MODULE$.countingTableGet(),
                    new Expression[]{this.generator().load( keyVar )} ) );
            CodeBlock x$234 = this.generator().whileLoop( Expression.gt( times, Expression.constant( BoxesRunTime.boxToInteger( 0 ) ) ) );
            Function1 x$235 = ( body ) -> {
                $anonfun$probe$1( this, block, times, body );
                return BoxedUnit.UNIT;
            };
            Function1 x$236 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$234, x$235 );
            Function2 x$237 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$234, x$235 );
            var5 = (BoxedUnit) org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$234, x$235, x$236, x$237 );
        }
        else if ( LongsToCountTable$.MODULE$.equals( tableType ) )
        {
            LocalVariable times = this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
            this.context.namer().newVarName());
            LocalVariable intermediate =
                    this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Integer.class ) ),
            this.context.namer().newVarName());
            this.generator().assign( intermediate,
                    Expression.cast( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Integer.class ) ),
                    Expression.invoke( this.generator().load( tableVar ), Methods$.MODULE$.countingTableCompositeKeyGet(), new Expression[]{
                            Expression.invoke( Methods$.MODULE$.compositeKey(), new Expression[]{
                                    Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ),
                                    (Expression[]) ((TraversableOnce) keyVars.map( ( x$1 ) -> {
                                        return this.generator().load( x$1 );
                                    }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ) ) )})})))
            ;
            this.generator().assign( times, Expression.ternary( Expression.isNull( intermediate ), Expression.constant( BoxesRunTime.boxToInteger( -1 ) ),
                    Expression.invoke( intermediate, Methods$.MODULE$.unboxInteger(), new Expression[0] ) ) );
            CodeBlock x$244 = this.generator().whileLoop( Expression.gt( times, Expression.constant( BoxesRunTime.boxToInteger( 0 ) ) ) );
            Function1 x$245 = ( body ) -> {
                $anonfun$probe$3( this, block, times, body );
                return BoxedUnit.UNIT;
            };
            Function1 x$246 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$244, x$245 );
            Function2 x$247 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$244, x$245 );
            var5 = (BoxedUnit) org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$244, x$245, x$246, x$247 );
        }
        else if ( tableType instanceof LongToListTable )
        {
            LongToListTable var19 = (LongToListTable) tableType;
            TupleDescriptor tupleDescriptor = var19.tupleDescriptor();
            scala.collection.immutable.Map localVars = var19.localMap();
            scala.Predef..MODULE$. assert (keyVars.size() == 1);
            String keyVar = (String) keyVars.head();
            GeneratedMethodStructure.HashTable hashTable = this.extractHashTable( var19 );
            LocalVariable list = this.generator().declare( hashTable.listType(), this.context.namer().newVarName() );
            String elementName = this.context.namer().newVarName();
            this.generator().assign( list,
                    Expression.invoke( this.generator().load( tableVar ), hashTable.get(), new Expression[]{this.generator().load( keyVar )} ) );
            CodeBlock x$258 = this.generator().ifStatement( Expression.notNull( list ) );
            Function1 x$259 = ( onTrue ) -> {
                $anonfun$probe$4( this, block, tupleDescriptor, localVars, hashTable, list, elementName, onTrue );
                return BoxedUnit.UNIT;
            };
            Function1 x$260 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$258, x$259 );
            Function2 x$261 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$258, x$259 );
            var5 = (BoxedUnit) org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$258, x$259, x$260, x$261 );
        }
        else
        {
            if ( !(tableType instanceof LongsToListTable) )
            {
                throw new MatchError( tableType );
            }

            LongsToListTable var30 = (LongsToListTable) tableType;
            TupleDescriptor tupleDescriptor = var30.tupleDescriptor();
            scala.collection.immutable.Map localVars = var30.localMap();
            GeneratedMethodStructure.HashTable hashTable = this.extractHashTable( var30 );
            LocalVariable list = this.generator().declare( hashTable.listType(), this.context.namer().newVarName() );
            String elementName = this.context.namer().newVarName();
            this.generator().assign( list, Expression.cast( hashTable.listType(), Expression.invoke( this.generator().load( tableVar ), hashTable.get(),
                    new Expression[]{Expression.invoke( Methods$.MODULE$.compositeKey(), new Expression[]{
                            Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ),
                            (Expression[]) ((TraversableOnce) keyVars.map( ( x$1 ) -> {
                                return this.generator().load( x$1 );
                            }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ) ) )})})));
            CodeBlock x$272 = this.generator().ifStatement( Expression.notNull( list ) );
            Function1 x$273 = ( onTrue ) -> {
                $anonfun$probe$8( this, block, tupleDescriptor, localVars, hashTable, list, elementName, onTrue );
                return BoxedUnit.UNIT;
            };
            Function1 x$274 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$272, x$273 );
            Function2 x$275 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$272, x$273 );
            var5 = (BoxedUnit) org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$272, x$273, x$274, x$275 );
        }
    }

    public void putField( final TupleDescriptor tupleDescriptor, final Expression value, final String fieldName, final String localVar )
    {
        this.generator().put( value, this.field( tupleDescriptor, fieldName ), this.generator().load( localVar ) );
    }

    public void updateProbeTable( final TupleDescriptor tupleDescriptor, final String tableVar, final RecordingJoinTableType tableType,
            final Seq<String> keyVars, final Expression element )
    {
        BoxedUnit var6;
        if ( tableType instanceof LongToListTable )
        {
            scala.Predef..MODULE$. assert (keyVars.size() == 1);
            String keyVar = (String) keyVars.head();
            GeneratedMethodStructure.HashTable hashTable = this.extractHashTable( tableType );
            String listName = this.context.namer().newVarName();
            LocalVariable list = this.generator().declare( hashTable.listType(), listName );
            this.generator().assign( list, Expression.cast( hashTable.listType(),
                    Expression.invoke( this.generator().load( tableVar ), hashTable.get(), new Expression[]{this.generator().load( keyVar )} ) ) );
            CodeBlock x$276 = this.generator().ifStatement( Expression.isNull( list ) );
            Function1 x$277 = ( onTrue ) -> {
                $anonfun$updateProbeTable$1( this, tableVar, keyVar, hashTable, listName, list, onTrue );
                return BoxedUnit.UNIT;
            };
            Function1 x$278 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$276, x$277 );
            Function2 x$279 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$276, x$277 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$276, x$277, x$278, x$279 );
            this.generator().expression( Expression.pop( Expression.invoke( list, hashTable.add(), new Expression[]{element} ) ) );
            var6 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !(tableType instanceof LongsToListTable) )
            {
                throw new MatchError( tableType );
            }

            GeneratedMethodStructure.HashTable hashTable = this.extractHashTable( tableType );
            String listName = this.context.namer().newVarName();
            String keyName = this.context.namer().newVarName();
            LocalVariable list = this.generator().declare( hashTable.listType(), listName );
            this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                    CompositeKey.class ) ), keyName, Expression.invoke( Methods$.MODULE$.compositeKey(),
                    new Expression[]{Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ),
                            (Expression[]) ((TraversableOnce) keyVars.map( ( x$1 ) -> {
                                return this.generator().load( x$1 );
                            }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class )))}));
            this.generator().assign( list, Expression.cast( hashTable.listType(),
                    Expression.invoke( this.generator().load( tableVar ), hashTable.get(), new Expression[]{this.generator().load( keyName )} ) ) );
            CodeBlock x$280 = this.generator().ifStatement( Expression.isNull( this.generator().load( listName ) ) );
            Function1 x$281 = ( onTrue ) -> {
                $anonfun$updateProbeTable$3( this, tableVar, hashTable, listName, keyName, list, onTrue );
                return BoxedUnit.UNIT;
            };
            Function1 x$282 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$280, x$281 );
            Function2 x$283 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$280, x$281 );
            org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$280, x$281, x$282, x$283 );
            this.generator().expression( Expression.pop( Expression.invoke( list, hashTable.add(), new Expression[]{element} ) ) );
            var6 = BoxedUnit.UNIT;
        }
    }

    public void declareProperty( final String propertyVar )
    {
        LocalVariable localVariable =
                this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Value.class ) ),
                propertyVar);
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( propertyVar ), localVariable));
        this.generator().assign( localVariable, this.noValue() );
    }

    public void declareAndInitialize( final String varName, final CodeGenType codeGenType )
    {
        LocalVariable localVariable = this.generator().declare( GeneratedQueryStructure$.MODULE$.lowerType( codeGenType ), varName );
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( varName ), localVariable));
        boolean var5 = false;
        CypherCodeGenType var6 = null;
        BoxedUnit var3;
        if ( codeGenType instanceof CypherCodeGenType )
        {
            label57:
            {
                var5 = true;
                var6 = (CypherCodeGenType) codeGenType;
                CypherType var8 = var6.ct();
                RepresentationType var9 = var6.repr();
                IntegerType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                if ( var10000 == null )
                {
                    if ( var8 != null )
                    {
                        break label57;
                    }
                }
                else if ( !var10000.equals( var8 ) )
                {
                    break label57;
                }

                if ( LongType$.MODULE$.equals( var9 ) )
                {
                    Expression.constant( BoxesRunTime.boxToLong( 0L ) );
                    var3 = BoxedUnit.UNIT;
                    return;
                }
            }
        }

        if ( var5 )
        {
            label48:
            {
                CypherType var11 = var6.ct();
                RepresentationType var12 = var6.repr();
                FloatType var17 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                if ( var17 == null )
                {
                    if ( var11 != null )
                    {
                        break label48;
                    }
                }
                else if ( !var17.equals( var11 ) )
                {
                    break label48;
                }

                if ( FloatType$.MODULE$.equals( var12 ) )
                {
                    Expression.constant( BoxesRunTime.boxToDouble( 0.0D ) );
                    var3 = BoxedUnit.UNIT;
                    return;
                }
            }
        }

        if ( var5 )
        {
            label39:
            {
                CypherType var14 = var6.ct();
                RepresentationType var15 = var6.repr();
                BooleanType var18 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                if ( var18 == null )
                {
                    if ( var14 != null )
                    {
                        break label39;
                    }
                }
                else if ( !var18.equals( var14 ) )
                {
                    break label39;
                }

                if ( BoolType$.MODULE$.equals( var15 ) )
                {
                    Expression.constant( BoxesRunTime.boxToBoolean( false ) );
                    var3 = BoxedUnit.UNIT;
                    return;
                }
            }
        }

        this.generator().assign( localVariable, GeneratedQueryStructure$.MODULE$.nullValue( codeGenType ) );
        var3 = BoxedUnit.UNIT;
    }

    public void declare( final String varName, final CodeGenType codeGenType )
    {
        LocalVariable localVariable = this.generator().declare( GeneratedQueryStructure$.MODULE$.lowerType( codeGenType ), varName );
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( varName ), localVariable));
    }

    public void assign( final String varName, final CodeGenType codeGenType, final Expression expression )
    {
        Option maybeVariable = this.locals.get( varName );
        if ( maybeVariable.nonEmpty() )
        {
            this.generator().assign( (LocalVariable) maybeVariable.get(), expression );
        }
        else
        {
            LocalVariable variable = this.generator().declare( GeneratedQueryStructure$.MODULE$.lowerType( codeGenType ), varName );
            this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( varName ), variable));
            this.generator().assign( variable, expression );
        }
    }

    public Expression hasLabel( final String nodeVar, final String labelVar, final String predVar )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( predVar );
        return (Expression) Templates$.MODULE$.handleEntityNotFound( this.generator(), this.fields(), this.context.namer(), ( inner ) -> {
            Expression invoked = Expression.invoke( MethodReference.methodReference(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CursorUtils.class ) ),
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean() ),
            "nodeHasLabel", new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( Read.class )),
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeCursor.class )),
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long()),
            GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int())}),
            new Expression[]{this.dataRead(), this.nodeCursor(), inner.load( nodeVar ), inner.load( labelVar )});
            inner.assign( local, invoked );
            return this.generator().load( predVar );
        }, ( fail ) -> {
            fail.assign( local, Expression.constant( BoxesRunTime.boxToBoolean( false ) ) );
            return this.generator().load( predVar );
        } );
    }

    public void relType( final String relVar, final String typeVar )
    {
        LocalVariable variable = (LocalVariable) this.locals.apply( typeVar );
        Expression typeOfRel = Expression.invoke( this.generator().load( this.relCursor( relVar ) ),
                GeneratedQueryStructure$.MODULE$.method( "type", scala.Predef..MODULE$.wrapRefArray( (Object[]) (new TypeReference[0]) ),
                scala.reflect.ManifestFactory..MODULE$.classType( RelationshipSelectionCursor.class ), scala.reflect.ManifestFactory..MODULE$.Int()),
        new Expression[0]);
        Templates$.MODULE$.handleKernelExceptions( this.generator(), this.fields(), this.context.namer(), ( inner ) -> {
            Expression res = Expression.invoke( this.tokenRead(), Methods$.MODULE$.relationshipTypeGetName(), new Expression[]{typeOfRel} );
            inner.assign( variable, res );
            return this.generator().load( variable.name() );
        } );
    }

    public void localVariable( final String variable, final Expression e, final CodeGenType codeGenType )
    {
        LocalVariable local = this.generator().declare( GeneratedQueryStructure$.MODULE$.lowerType( codeGenType ), variable );
        this.generator().assign( local, e );
    }

    public void declareFlag( final String name, final boolean initialValue )
    {
        LocalVariable localVariable = this.generator().declare( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean() ),
                name);
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( name ), localVariable));
        this.generator().assign( localVariable, Expression.constant( BoxesRunTime.boxToBoolean( initialValue ) ) );
    }

    public void updateFlag( final String name, final boolean newValue )
    {
        this.generator().assign( (LocalVariable) this.locals.apply( name ), Expression.constant( BoxesRunTime.boxToBoolean( newValue ) ) );
    }

    public void declarePredicate( final String name )
    {
        this.locals.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( name ), this.generator().declare(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Boolean() ), name)));
    }

    public void nodeGetPropertyForVar( final String nodeVar, final CodeGenType nodeVarType, final String propIdVar, final String propValueVar )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( propValueVar );
        Templates$.MODULE$.handleEntityNotFound( this.generator(), this.fields(), this.context.namer(), ( body ) -> {
            $anonfun$nodeGetPropertyForVar$1( this, nodeVar, nodeVarType, propIdVar, local, body );
            return BoxedUnit.UNIT;
        }, ( fail ) -> {
            $anonfun$nodeGetPropertyForVar$2( this, local, fail );
            return BoxedUnit.UNIT;
        } );
    }

    public void nodeGetPropertyById( final String nodeVar, final CodeGenType nodeVarType, final int propId, final String propValueVar )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( propValueVar );
        Templates$.MODULE$.handleEntityNotFound( this.generator(), this.fields(), this.context.namer(), ( body ) -> {
            $anonfun$nodeGetPropertyById$1( this, nodeVar, nodeVarType, propId, local, body );
            return BoxedUnit.UNIT;
        }, ( fail ) -> {
            $anonfun$nodeGetPropertyById$2( this, local, fail );
            return BoxedUnit.UNIT;
        } );
    }

    public void nodeIdSeek( final String nodeIdVar, final Expression expression, final CodeGenType codeGenType,
            final Function1<MethodStructure<Expression>,BoxedUnit> block )
    {
        label62:
        {
            boolean var6 = false;
            CypherCodeGenType var7 = null;
            BoxedUnit var5;
            IntegerType var10000;
            if ( codeGenType instanceof CypherCodeGenType )
            {
                label57:
                {
                    var6 = true;
                    var7 = (CypherCodeGenType) codeGenType;
                    CypherType var9 = var7.ct();
                    RepresentationType var10 = var7.repr();
                    var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                    if ( var10000 == null )
                    {
                        if ( var9 != null )
                        {
                            break label57;
                        }
                    }
                    else if ( !var10000.equals( var9 ) )
                    {
                        break label57;
                    }

                    if ( LongType$.MODULE$.equals( var10 ) )
                    {
                        this.generator().assign(
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), nodeIdVar, expression);
                        var5 = BoxedUnit.UNIT;
                        break label62;
                    }
                }
            }

            if ( var6 )
            {
                label50:
                {
                    CypherType var12 = var7.ct();
                    var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                    if ( var10000 == null )
                    {
                        if ( var12 != null )
                        {
                            break label50;
                        }
                    }
                    else if ( !var10000.equals( var12 ) )
                    {
                        break label50;
                    }

                    if ( var7.repr() instanceof AnyValueType )
                    {
                        this.generator().assign(
                                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), nodeIdVar, Expression.invoke(
                                Expression.cast(
                                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NumberValue.class ) ),
                                expression ), GeneratedQueryStructure$.MODULE$.method( "longValue", scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new TypeReference[0]) ), scala.reflect.ManifestFactory..
                        MODULE$.classType( NumberValue.class ), scala.reflect.ManifestFactory..MODULE$.Long()),new Expression[0]));
                        var5 = BoxedUnit.UNIT;
                        break label62;
                    }
                }
            }

            if ( !var6 )
            {
                throw new IllegalArgumentException(
                        (new StringBuilder( 41 )).append( "CodeGenType " ).append( codeGenType ).append( " can not be converted to long" ).toString() );
            }

            CypherType var14 = var7.ct();
            RepresentationType var15 = var7.repr();
            var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
            if ( var10000 == null )
            {
                if ( var14 != null )
                {
                    throw new IllegalArgumentException(
                            (new StringBuilder( 41 )).append( "CodeGenType " ).append( codeGenType ).append( " can not be converted to long" ).toString() );
                }
            }
            else if ( !var10000.equals( var14 ) )
            {
                throw new IllegalArgumentException(
                        (new StringBuilder( 41 )).append( "CodeGenType " ).append( codeGenType ).append( " can not be converted to long" ).toString() );
            }

            if ( !ReferenceType$.MODULE$.equals( var15 ) )
            {
                throw new IllegalArgumentException(
                        (new StringBuilder( 41 )).append( "CodeGenType " ).append( codeGenType ).append( " can not be converted to long" ).toString() );
            }

            this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), nodeIdVar, Expression.invoke(
                    Methods$.MODULE$.mathCastToLong(), new Expression[]{expression} ));
            var5 = BoxedUnit.UNIT;
        }

        CodeBlock x$290 = this.generator().ifStatement(
                Expression.and( Expression.gt( this.generator().load( nodeIdVar ), Expression.constant( BoxesRunTime.boxToLong( -1L ) ) ),
                        Expression.invoke( this.dataRead(), Methods$.MODULE$.nodeExists(), new Expression[]{this.generator().load( nodeIdVar )} ) ) );
        Function1 x$291 = ( ifBody ) -> {
            $anonfun$nodeIdSeek$1( this, block, ifBody );
            return BoxedUnit.UNIT;
        };
        Function1 x$292 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$3( x$290, x$291 );
        Function2 x$293 = org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using$default$4( x$290, x$291 );
        org.neo4j.cypher.internal.v4_0.frontend.helpers.package..MODULE$.using( x$290, x$291, x$292, x$293 );
    }

    public void relationshipGetPropertyForVar( final String relIdVar, final CodeGenType relVarType, final String propIdVar, final String propValueVar )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( propValueVar );
        Templates$.MODULE$.handleEntityNotFound( this.generator(), this.fields(), this.context.namer(), ( body ) -> {
            $anonfun$relationshipGetPropertyForVar$1( this, relIdVar, relVarType, propIdVar, local, body );
            return BoxedUnit.UNIT;
        }, ( fail ) -> {
            $anonfun$relationshipGetPropertyForVar$2( this, local, fail );
            return BoxedUnit.UNIT;
        } );
    }

    public void relationshipGetPropertyById( final String relIdVar, final CodeGenType relVarType, final int propId, final String propValueVar )
    {
        LocalVariable local = (LocalVariable) this.locals.apply( propValueVar );
        Templates$.MODULE$.handleEntityNotFound( this.generator(), this.fields(), this.context.namer(), ( body ) -> {
            $anonfun$relationshipGetPropertyById$1( this, relIdVar, relVarType, propId, local, body );
            return BoxedUnit.UNIT;
        }, ( fail ) -> {
            $anonfun$relationshipGetPropertyById$2( this, local, fail );
            return BoxedUnit.UNIT;
        } );
    }

    public void lookupPropertyKey( final String propName, final String propIdVar )
    {
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ), propIdVar, Expression.invoke(
                this.tokenRead(), Methods$.MODULE$.propertyKeyGetForName(), new Expression[]{Expression.constant( propName )} ));
    }

    public void newIndexReference( final String referenceVar, final String labelVar, final String propKeyVar )
    {
        Expression propertyIdsExpr = Expression.newInitializedArray( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int() ),
        new Expression[]{this.generator().load( propKeyVar )});
        this.generator().assign( GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType(
                IndexDescriptor.class ) ), referenceVar, Expression.invoke( this.schemaRead(),
                GeneratedQueryStructure$.MODULE$.method( "indexGetForLabelAndPropertiesForCompiledRuntime", scala.Predef..MODULE$.wrapRefArray(
                        (Object[]) (new TypeReference[]{GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Int()),
                        GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.arrayType(
                                scala.reflect.ManifestFactory..MODULE$.Int() ) )})),scala.reflect.ManifestFactory..
        MODULE$.classType( SchemaRead.class ), scala.reflect.ManifestFactory..MODULE$.classType( IndexDescriptor.class )),
        new Expression[]{this.generator().load( labelVar ), propertyIdsExpr}));
    }

    public void indexSeek( final String cursorName, final String indexReference, final Expression value, final CodeGenType codeGenType )
    {
        LocalVariable local = this.generator().declare(
                GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( NodeValueIndexCursor.class ) ), cursorName);
        this.generator().assign( local, Expression.constant( (Object) null ) );
        Expression boxedValue = codeGenType.isPrimitive() ? Expression.box( value ) : value;
        Templates$.MODULE$.handleKernelExceptions( this.generator(), this.fields(), this.context.namer(), ( body ) -> {
            $anonfun$indexSeek$1( this, cursorName, indexReference, local, boxedValue, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Expression token( final int t )
    {
        return Expression.constant( BoxesRunTime.boxToInteger( t ) );
    }

    public Expression wildCardToken()
    {
        return Expression.constant( BoxesRunTime.boxToInteger( -1 ) );
    }

    public Expression nodeCountFromCountStore( final Expression expression )
    {
        return Expression.invoke( this.dataRead(), Methods$.MODULE$.countsForNode(), new Expression[]{expression} );
    }

    public Expression relCountFromCountStore( final Expression start, final Expression end, final Seq<Expression> types )
    {
        return types.isEmpty() ? Expression.invoke( this.dataRead(), Methods$.MODULE$.countsForRel(), new Expression[]{start, this.wildCardToken(), end} )
                               : (Expression) ((TraversableOnce) types.map( ( x$9 ) -> {
                                   return Expression.invoke( this.dataRead(), Methods$.MODULE$.countsForRel(), new Expression[]{start, x$9, end} );
                               }, scala.collection.Seq..MODULE$.canBuildFrom())).reduceLeft( ( x$1, x$2 ) -> {
        return Expression.add( x$1, x$2 );
    } );
    }

    public Expression coerceToBoolean( final Expression propertyExpression )
    {
        return Expression.invoke( Methods$.MODULE$.coerceToPredicate(), new Expression[]{propertyExpression} );
    }

    public Expression newTableValue( final String targetVar, final TupleDescriptor tupleDescriptor )
    {
        TypeReference tupleType = this.aux.typeReference( tupleDescriptor );
        this.generator().assign( tupleType, targetVar,
                Templates$.MODULE$.createNewInstance( tupleType, scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[0]) ) ));
        return this.generator().load( targetVar );
    }

    public Expression noValue()
    {
        return Templates$.MODULE$.noValue();
    }

    private FieldReference field( final TupleDescriptor tupleDescriptor, final String fieldName )
    {
        CodeGenType fieldType = (CodeGenType) tupleDescriptor.structure().apply( fieldName );
        return FieldReference.field( this.aux.typeReference( tupleDescriptor ), GeneratedQueryStructure$.MODULE$.lowerType( fieldType ), fieldName );
    }

    private TypeReference sortTableType( final SortTableDescriptor tableDescriptor )
    {
        TypeReference var2;
        if ( tableDescriptor instanceof FullSortTableDescriptor )
        {
            FullSortTableDescriptor var4 = (FullSortTableDescriptor) tableDescriptor;
            OrderableTupleDescriptor tupleDescriptor = var4.tupleDescriptor();
            TypeReference tupleType = this.aux.comparableTypeReference( tupleDescriptor );
            var2 = TypeReference.parameterizedType( DefaultFullSortTable.class, new TypeReference[]{tupleType} );
        }
        else
        {
            if ( !(tableDescriptor instanceof TopTableDescriptor) )
            {
                throw new MatchError( tableDescriptor );
            }

            TopTableDescriptor var7 = (TopTableDescriptor) tableDescriptor;
            OrderableTupleDescriptor tupleDescriptor = var7.tupleDescriptor();
            TypeReference tupleType = this.aux.comparableTypeReference( tupleDescriptor );
            var2 = TypeReference.parameterizedType( DefaultTopTable.class, new TypeReference[]{tupleType} );
        }

        return var2;
    }

    private Expression forceLong( final String name, final CodeGenType typ )
    {
        RepresentationType var4 = typ.repr();
        Expression var3;
        if ( LongType$.MODULE$.equals( var4 ) )
        {
            var3 = this.generator().load( name );
        }
        else
        {
            if ( !(var4 instanceof ReferenceType) )
            {
                throw new IllegalStateException( (new StringBuilder( 48 )).append( name ).append( " has type " ).append( typ ).append(
                        " which cannot be represented as a long" ).toString() );
            }

            var3 = Expression.invoke( MethodReference.methodReference(
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.classType( CompiledConversionUtils.class ) ),
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Long() ), "extractLong", new TypeReference[]{
                    GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object())}),new Expression[]{this.generator().load( name )})
            ;
        }

        return var3;
    }

    private TypeReference deriveCommonType( final Iterable<CodeGenType> codeGenTypes )
    {
        Iterable lowerTypes = (Iterable) codeGenTypes.map( ( cType ) -> {
            return GeneratedQueryStructure$.MODULE$.lowerTypeScalarSubset( cType );
        }, scala.collection.Iterable..MODULE$.canBuildFrom());
        return (TypeReference) lowerTypes.reduce( ( a, b ) -> {
            TypeReference var10000;
            label23:
            {
                if ( a == null )
                {
                    if ( b == null )
                    {
                        break label23;
                    }
                }
                else if ( a.equals( b ) )
                {
                    break label23;
                }

                var10000 = GeneratedQueryStructure$.MODULE$.typeRef( scala.reflect.ManifestFactory..MODULE$.Object());
                return var10000;
            }

            var10000 = a;
            return var10000;
        } );
    }

    private final void HashTable$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.HashTable$module == null )
            {
                this.HashTable$module = new GeneratedMethodStructure.HashTable$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class HashTable implements Product, Serializable
    {
        private final TypeReference valueType;
        private final TypeReference listType;
        private final TypeReference tableType;
        private final MethodReference get;
        private final MethodReference put;
        private final MethodReference add;

        public HashTable( final GeneratedMethodStructure $outer, final TypeReference valueType, final TypeReference listType, final TypeReference tableType,
                final MethodReference get, final MethodReference put, final MethodReference add )
        {
            this.valueType = valueType;
            this.listType = listType;
            this.tableType = tableType;
            this.get = get;
            this.put = put;
            this.add = add;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
                Product.$init$( this );
            }
        }

        public TypeReference valueType()
        {
            return this.valueType;
        }

        public TypeReference listType()
        {
            return this.listType;
        }

        public TypeReference tableType()
        {
            return this.tableType;
        }

        public MethodReference get()
        {
            return this.get;
        }

        public MethodReference put()
        {
            return this.put;
        }

        public MethodReference add()
        {
            return this.add;
        }

        public GeneratedMethodStructure.HashTable copy( final TypeReference valueType, final TypeReference listType, final TypeReference tableType,
                final MethodReference get, final MethodReference put, final MethodReference add )
        {
            return this.org$neo4j$cypher$internal$spi$codegen$GeneratedMethodStructure$HashTable$$$outer().new HashTable(
                    this.org$neo4j$cypher$internal$spi$codegen$GeneratedMethodStructure$HashTable$$$outer(), valueType, listType, tableType, get, put, add );
        }

        public TypeReference copy$default$1()
        {
            return this.valueType();
        }

        public TypeReference copy$default$2()
        {
            return this.listType();
        }

        public TypeReference copy$default$3()
        {
            return this.tableType();
        }

        public MethodReference copy$default$4()
        {
            return this.get();
        }

        public MethodReference copy$default$5()
        {
            return this.put();
        }

        public MethodReference copy$default$6()
        {
            return this.add();
        }

        public String productPrefix()
        {
            return "HashTable";
        }

        public int productArity()
        {
            return 6;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.valueType();
                break;
            case 1:
                var10000 = this.listType();
                break;
            case 2:
                var10000 = this.tableType();
                break;
            case 3:
                var10000 = this.get();
                break;
            case 4:
                var10000 = this.put();
                break;
            case 5:
                var10000 = this.add();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public scala.collection.Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof GeneratedMethodStructure.HashTable;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var12;
            if ( this != x$1 )
            {
                label104:
                {
                    boolean var2;
                    if ( x$1 instanceof GeneratedMethodStructure.HashTable &&
                            ((GeneratedMethodStructure.HashTable) x$1).org$neo4j$cypher$internal$spi$codegen$GeneratedMethodStructure$HashTable$$$outer() ==
                                    this.org$neo4j$cypher$internal$spi$codegen$GeneratedMethodStructure$HashTable$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label80:
                        {
                            label94:
                            {
                                GeneratedMethodStructure.HashTable var4 = (GeneratedMethodStructure.HashTable) x$1;
                                TypeReference var10000 = this.valueType();
                                TypeReference var5 = var4.valueType();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label94;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label94;
                                }

                                var10000 = this.listType();
                                TypeReference var6 = var4.listType();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label94;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label94;
                                }

                                var10000 = this.tableType();
                                TypeReference var7 = var4.tableType();
                                if ( var10000 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label94;
                                    }
                                }
                                else if ( !var10000.equals( var7 ) )
                                {
                                    break label94;
                                }

                                MethodReference var11 = this.get();
                                MethodReference var8 = var4.get();
                                if ( var11 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label94;
                                    }
                                }
                                else if ( !var11.equals( var8 ) )
                                {
                                    break label94;
                                }

                                var11 = this.put();
                                MethodReference var9 = var4.put();
                                if ( var11 == null )
                                {
                                    if ( var9 != null )
                                    {
                                        break label94;
                                    }
                                }
                                else if ( !var11.equals( var9 ) )
                                {
                                    break label94;
                                }

                                var11 = this.add();
                                MethodReference var10 = var4.add();
                                if ( var11 == null )
                                {
                                    if ( var10 != null )
                                    {
                                        break label94;
                                    }
                                }
                                else if ( !var11.equals( var10 ) )
                                {
                                    break label94;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var12 = true;
                                    break label80;
                                }
                            }

                            var12 = false;
                        }

                        if ( var12 )
                        {
                            break label104;
                        }
                    }

                    var12 = false;
                    return var12;
                }
            }

            var12 = true;
            return var12;
        }
    }

    public class HashTable$ extends
            AbstractFunction6<TypeReference,TypeReference,TypeReference,MethodReference,MethodReference,MethodReference,GeneratedMethodStructure.HashTable>
            implements Serializable
    {
        public HashTable$( final GeneratedMethodStructure $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public final String toString()
        {
            return "HashTable";
        }

        public GeneratedMethodStructure.HashTable apply( final TypeReference valueType, final TypeReference listType, final TypeReference tableType,
                final MethodReference get, final MethodReference put, final MethodReference add )
        {
            return this.$outer.new HashTable( this.$outer, valueType, listType, tableType, get, put, add );
        }

        public Option<Tuple6<TypeReference,TypeReference,TypeReference,MethodReference,MethodReference,MethodReference>> unapply(
                final GeneratedMethodStructure.HashTable x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :
            new Some( new Tuple6( x$0.valueType(), x$0.listType(), x$0.tableType(), x$0.get(), x$0.put(), x$0.add() ) ));
        }
    }
}
