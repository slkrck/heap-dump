package org.neo4j.cypher.internal;

import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class SlottedRuntime
{
    public static SecurityContext compileToExecutable$default$3()
    {
        return SlottedRuntime$.MODULE$.compileToExecutable$default$3();
    }

    public static void prettyPrintPipe( final Pipe pipe )
    {
        SlottedRuntime$.MODULE$.prettyPrintPipe( var0 );
    }

    public static void prettyPrintPipelines( final PhysicalPlanningAttributes.SlotConfigurations pipelines )
    {
        SlottedRuntime$.MODULE$.prettyPrintPipelines( var0 );
    }

    public static ExecutionPlan compileToExecutable( final LogicalQuery query, final EnterpriseRuntimeContext context, final SecurityContext securityContext )
            throws CantCompileQueryException
    {
        return SlottedRuntime$.MODULE$.compileToExecutable( var0, var1, var2 );
    }

    public static boolean PRINT_FAILURE_STACK_TRACE()
    {
        return SlottedRuntime$.MODULE$.PRINT_FAILURE_STACK_TRACE();
    }

    public static boolean PRINT_PIPELINE_INFO()
    {
        return SlottedRuntime$.MODULE$.PRINT_PIPELINE_INFO();
    }

    public static boolean PRINT_REWRITTEN_LOGICAL_PLAN()
    {
        return SlottedRuntime$.MODULE$.PRINT_REWRITTEN_LOGICAL_PLAN();
    }

    public static boolean PRINT_LOGICAL_PLAN()
    {
        return SlottedRuntime$.MODULE$.PRINT_LOGICAL_PLAN();
    }

    public static boolean PRINT_QUERY_TEXT()
    {
        return SlottedRuntime$.MODULE$.PRINT_QUERY_TEXT();
    }

    public static boolean PRINT_PLAN_INFO_EARLY()
    {
        return SlottedRuntime$.MODULE$.PRINT_PLAN_INFO_EARLY();
    }

    public static boolean ENABLE_DEBUG_PRINTS()
    {
        return SlottedRuntime$.MODULE$.ENABLE_DEBUG_PRINTS();
    }

    public static String name()
    {
        return SlottedRuntime$.MODULE$.name();
    }
}
