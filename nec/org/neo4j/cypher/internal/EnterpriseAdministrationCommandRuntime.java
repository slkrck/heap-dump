package org.neo4j.cypher.internal;

import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.kernel.enterprise.api.security.EnterpriseAuthManager;
import com.neo4j.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import com.neo4j.server.security.enterprise.auth.Resource;
import com.neo4j.server.security.enterprise.auth.ResourcePrivilege;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.neo4j.common.DependencyResolver;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.NormalizedDatabaseName;
import org.neo4j.cypher.internal.compiler.phases.LogicalPlanState;
import org.neo4j.cypher.internal.logical.plans.AlterUser;
import org.neo4j.cypher.internal.logical.plans.AssertValidRevoke;
import org.neo4j.cypher.internal.logical.plans.CheckFrozenRole;
import org.neo4j.cypher.internal.logical.plans.CopyRolePrivileges;
import org.neo4j.cypher.internal.logical.plans.CreateDatabase;
import org.neo4j.cypher.internal.logical.plans.CreateRole;
import org.neo4j.cypher.internal.logical.plans.CreateUser;
import org.neo4j.cypher.internal.logical.plans.DenyDatabaseAction;
import org.neo4j.cypher.internal.logical.plans.DenyDbmsAction;
import org.neo4j.cypher.internal.logical.plans.DenyRead;
import org.neo4j.cypher.internal.logical.plans.DenyTraverse;
import org.neo4j.cypher.internal.logical.plans.DenyWrite;
import org.neo4j.cypher.internal.logical.plans.DropDatabase;
import org.neo4j.cypher.internal.logical.plans.DropRole;
import org.neo4j.cypher.internal.logical.plans.EnsureValidNonSystemDatabase;
import org.neo4j.cypher.internal.logical.plans.EnsureValidNumberOfDatabases;
import org.neo4j.cypher.internal.logical.plans.GrantDatabaseAction;
import org.neo4j.cypher.internal.logical.plans.GrantDbmsAction;
import org.neo4j.cypher.internal.logical.plans.GrantRead;
import org.neo4j.cypher.internal.logical.plans.GrantRoleToUser;
import org.neo4j.cypher.internal.logical.plans.GrantTraverse;
import org.neo4j.cypher.internal.logical.plans.GrantWrite;
import org.neo4j.cypher.internal.logical.plans.LogSystemCommand;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.MultiDatabaseLogicalPlan;
import org.neo4j.cypher.internal.logical.plans.RequireRole;
import org.neo4j.cypher.internal.logical.plans.RevokeDatabaseAction;
import org.neo4j.cypher.internal.logical.plans.RevokeDbmsAction;
import org.neo4j.cypher.internal.logical.plans.RevokeRead;
import org.neo4j.cypher.internal.logical.plans.RevokeRoleFromUser;
import org.neo4j.cypher.internal.logical.plans.RevokeTraverse;
import org.neo4j.cypher.internal.logical.plans.RevokeWrite;
import org.neo4j.cypher.internal.logical.plans.ShowPrivileges;
import org.neo4j.cypher.internal.logical.plans.ShowRoles;
import org.neo4j.cypher.internal.logical.plans.ShowUsers;
import org.neo4j.cypher.internal.logical.plans.StartDatabase;
import org.neo4j.cypher.internal.logical.plans.StopDatabase;
import org.neo4j.cypher.internal.procs.AuthorizationPredicateExecutionPlan;
import org.neo4j.cypher.internal.procs.LoggingSystemCommandExecutionPlan;
import org.neo4j.cypher.internal.procs.QueryHandler;
import org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan;
import org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan;
import org.neo4j.cypher.internal.runtime.ParameterMapping;
import org.neo4j.cypher.internal.security.SecureHasher;
import org.neo4j.cypher.internal.security.SystemGraphCredential;
import org.neo4j.cypher.internal.v4_0.ast.ActionResource;
import org.neo4j.cypher.internal.v4_0.ast.AdminAction;
import org.neo4j.cypher.internal.v4_0.ast.AllGraphsScope;
import org.neo4j.cypher.internal.v4_0.ast.AllQualifier;
import org.neo4j.cypher.internal.v4_0.ast.AllResource;
import org.neo4j.cypher.internal.v4_0.ast.DatabaseResource;
import org.neo4j.cypher.internal.v4_0.ast.GraphScope;
import org.neo4j.cypher.internal.v4_0.ast.LabelAllQualifier;
import org.neo4j.cypher.internal.v4_0.ast.LabelQualifier;
import org.neo4j.cypher.internal.v4_0.ast.NamedGraphScope;
import org.neo4j.cypher.internal.v4_0.ast.NoResource;
import org.neo4j.cypher.internal.v4_0.ast.PrivilegeQualifier;
import org.neo4j.cypher.internal.v4_0.ast.PropertyResource;
import org.neo4j.cypher.internal.v4_0.ast.RelationshipAllQualifier;
import org.neo4j.cypher.internal.v4_0.ast.RelationshipQualifier;
import org.neo4j.cypher.internal.v4_0.ast.RevokeBothType;
import org.neo4j.cypher.internal.v4_0.ast.RevokeDenyType;
import org.neo4j.cypher.internal.v4_0.ast.RevokeGrantType;
import org.neo4j.cypher.internal.v4_0.ast.RevokeType;
import org.neo4j.cypher.internal.v4_0.ast.ShowAllPrivileges;
import org.neo4j.cypher.internal.v4_0.ast.ShowPrivilegeScope;
import org.neo4j.cypher.internal.v4_0.ast.ShowRolePrivileges;
import org.neo4j.cypher.internal.v4_0.ast.ShowUserPrivileges;
import org.neo4j.dbms.api.DatabaseExistsException;
import org.neo4j.dbms.api.DatabaseNotFoundException;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.exceptions.DatabaseAdministrationException;
import org.neo4j.exceptions.DatabaseAdministrationOnFollowerException;
import org.neo4j.exceptions.InternalException;
import org.neo4j.internal.kernel.api.security.PrivilegeAction;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;
import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.api.exceptions.Status.HasStatus;
import org.neo4j.kernel.api.exceptions.schema.UniquePropertyValueValidationException;
import org.neo4j.kernel.impl.store.format.standard.Standard;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.TextArray;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Function3;
import scala.MatchError;
import scala.Option;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.Set;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing.;
import scala.util.Failure;
import scala.util.Success;
import scala.util.Try;

@JavaDocToJava
public class EnterpriseAdministrationCommandRuntime implements AdministrationCommandRuntime, Product, Serializable
{
    private final ExecutionEngine normalExecutionEngine;
    private final DependencyResolver resolver;
    private final CommunityAdministrationCommandRuntime communityCommandRuntime;
    private final long org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$maxDBLimit;
    private final SecureHasher org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$secureHasher;
    private final String followerError;
    private EnterpriseAuthManager org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$authManager;
    private volatile boolean bitmap$0;

    public org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile( final LogicalPlan unknownPlan )
    {
        throw new CantCompileQueryException( (new StringBuilder( 58 )).append( "Plan is not a recognized database administration command: " ).append(
                unknownPlan.getClass().getSimpleName() ).toString() );
    }

    public EnterpriseAdministrationCommandRuntime( final ExecutionEngine normalExecutionEngine, final DependencyResolver resolver )
    {
        this.normalExecutionEngine = normalExecutionEngine;
        this.resolver = resolver;
        AdministrationCommandRuntime.$init$( this );
        Product.$init$( this );
        this.communityCommandRuntime = new CommunityAdministrationCommandRuntime( normalExecutionEngine, resolver, this.logicalToExecutable() );
        this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$maxDBLimit = scala.Predef..
        MODULE$.Long2long( (Long) ((Config) resolver.resolveDependency( Config.class )).get( EnterpriseEditionSettings.maxNumberOfDatabases ) );
        this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$secureHasher = new SecureHasher();
    }

    public static Option<Tuple2<ExecutionEngine,DependencyResolver>> unapply( final EnterpriseAdministrationCommandRuntime x$0 )
    {
        return EnterpriseAdministrationCommandRuntime$.MODULE$.unapply( var0 );
    }

    public static EnterpriseAdministrationCommandRuntime apply( final ExecutionEngine normalExecutionEngine, final DependencyResolver resolver )
    {
        return EnterpriseAdministrationCommandRuntime$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<ExecutionEngine,DependencyResolver>,EnterpriseAdministrationCommandRuntime> tupled()
    {
        return EnterpriseAdministrationCommandRuntime$.MODULE$.tupled();
    }

    public static Function1<ExecutionEngine,Function1<DependencyResolver,EnterpriseAdministrationCommandRuntime>> curried()
    {
        return EnterpriseAdministrationCommandRuntime$.MODULE$.curried();
    }

    public byte[] validatePassword( final byte[] password )
    {
        return AdministrationCommandRuntime.validatePassword$( this, password );
    }

    public SecurityContext compileToExecutable$default$3()
    {
        return CypherRuntime.compileToExecutable$default$3$( this );
    }

    public String followerError()
    {
        return this.followerError;
    }

    public void org$neo4j$cypher$internal$AdministrationCommandRuntime$_setter_$followerError_$eq( final String x$1 )
    {
        this.followerError = x$1;
    }

    public ExecutionEngine normalExecutionEngine()
    {
        return this.normalExecutionEngine;
    }

    public DependencyResolver resolver()
    {
        return this.resolver;
    }

    private CommunityAdministrationCommandRuntime communityCommandRuntime()
    {
        return this.communityCommandRuntime;
    }

    public long org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$maxDBLimit()
    {
        return this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$maxDBLimit;
    }

    public String name()
    {
        return "enterprise administration-commands";
    }

    public ExecutionPlan compileToExecutable( final LogicalQuery state, final RuntimeContext context, final SecurityContext securityContext )
    {
        Tuple2 var6 = org.neo4j.cypher.internal.runtime.slottedParameters..MODULE$.apply( state.logicalPlan() );
        if ( var6 != null )
        {
            LogicalPlan planWithSlottedParameters = (LogicalPlan) var6._1();
            ParameterMapping parameterMapping = (ParameterMapping) var6._2();
            Tuple2 var4 = new Tuple2( planWithSlottedParameters, parameterMapping );
            LogicalPlan planWithSlottedParameters = (LogicalPlan) var4._1();
            ParameterMapping parameterMapping = (ParameterMapping) var4._2();
            return (ExecutionPlan) ((Function3) this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                    planWithSlottedParameters, ( unknownPlan ) -> {
                        return this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile( unknownPlan );
                    } )).apply( context, parameterMapping, securityContext );
        }
        else
        {
            throw new MatchError( var6 );
        }
    }

    private EnterpriseAuthManager authManager$lzycompute()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( !this.bitmap$0 )
            {
                this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$authManager =
                        (EnterpriseAuthManager) this.resolver().resolveDependency( EnterpriseAuthManager.class );
                this.bitmap$0 = true;
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }

        return this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$authManager;
    }

    public EnterpriseAuthManager org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$authManager()
    {
        return !this.bitmap$0 ? this.authManager$lzycompute() : this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$authManager;
    }

    public SecureHasher org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$secureHasher()
    {
        return this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$secureHasher;
    }

    public PartialFunction<LogicalPlan,Function3<RuntimeContext,ParameterMapping,SecurityContext,ExecutionPlan>> org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable()
    {
        return this.logicalToExecutable().orElse( this.communityCommandRuntime().logicalToExecutable() );
    }

    private PartialFunction<LogicalPlan,Function3<RuntimeContext,ParameterMapping,SecurityContext,ExecutionPlan>> logicalToExecutable()
    {
        return new Serializable( this )
        {
            public static final long serialVersionUID = 0L;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                }
            }

            public final <A1 extends LogicalPlan, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                boolean var4 = false;
                AlterUser var5 = null;
                Object var3;
                if ( x1 instanceof CheckFrozenRole )
                {
                    CheckFrozenRole var7 = (CheckFrozenRole) x1;
                    Option sourcex = var7.source();
                    String roleNamexx = var7.roleName();
                    var3 = ( context, parameterMapping, securityContext ) -> {
                        return new AuthorizationPredicateExecutionPlan( () -> {
                            boolean var10000;
                            label23:
                            {
                                String var1 = "admin";
                                if ( roleNamexx == null )
                                {
                                    if ( var1 != null )
                                    {
                                        break label23;
                                    }
                                }
                                else if ( !roleNamexx.equals( var1 ) )
                                {
                                    break label23;
                                }

                                var10000 = false;
                                return var10000;
                            }

                            var10000 = true;
                            return var10000;
                        }, sourcex.map( ( x$2 ) -> {
                            return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                    x$2, ( unknownPlan ) -> {
                                        return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile( unknownPlan );
                                    } )).apply( context, parameterMapping, securityContext );
                        } ), org.neo4j.cypher.internal.procs.AuthorizationPredicateExecutionPlan..MODULE$.apply$default$3());
                    };
                }
                else if ( x1 instanceof ShowUsers )
                {
                    ShowUsers var10 = (ShowUsers) x1;
                    Option sourcexxx = var10.source();
                    var3 = ( context, parameterMapping, securityContext ) -> {
                        String x$51 = "ShowUsers";
                        ExecutionEngine x$52 = this.$outer.normalExecutionEngine();
                        String x$53 = (new StringOps( scala.Predef..MODULE$.augmentString(
                                "MATCH (u:User)\n          |OPTIONAL MATCH (u)-[:HAS_ROLE]->(r:Role)\n          |RETURN u.name as user, collect(r.name) as roles, u.passwordChangeRequired AS passwordChangeRequired, u.suspended AS suspended" ))).
                        stripMargin();
                        MapValue x$54 = VirtualValues.EMPTY_MAP;
                        Option x$55 = sourcexxx.map( ( x$3 ) -> {
                            return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                    x$3, ( unknownPlan ) -> {
                                        return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile( unknownPlan );
                                    } )).apply( context, parameterMapping, securityContext );
                        } );
                        QueryHandler x$56 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$5();
                        boolean x$57 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$7();
                        Function1 x$58 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$8();
                        return new SystemCommandExecutionPlan( x$51, x$52, x$53, x$54, x$56, x$55, x$57, x$58 );
                    };
                }
                else
                {
                    if ( x1 instanceof CreateUser )
                    {
                        CreateUser var12 = (CreateUser) x1;
                        Option sourcexxxx = var12.source();
                        String userName = var12.userName();
                        Option var15 = var12.initialStringPassword();
                        Option var16 = var12.initialParameterPassword();
                        boolean requirePasswordChange = var12.requirePasswordChange();
                        Option suspendedOptional = var12.suspended();
                        if ( var15 instanceof Some )
                        {
                            Some var19 = (Some) var15;
                            byte[] initialPassword = (byte[]) var19.value();
                            if ( scala.None..MODULE$.equals( var16 )){
                            var3 = ( context, parameterMapping, securityContext ) -> {
                                boolean suspended = BoxesRunTime.unboxToBoolean( suspendedOptional.getOrElse( () -> {
                                    return false;
                                } ) );

                                UpdatingSystemCommandExecutionPlan var10000;
                                try
                                {
                                    this.$outer.validatePassword( initialPassword );
                                    var10000 = new UpdatingSystemCommandExecutionPlan( "CreateUser", this.$outer.normalExecutionEngine(),
                                            (new StringOps( scala.Predef..MODULE$.augmentString(
                                                    "CREATE (u:User {name: $name, credentials: $credentials, passwordChangeRequired: $passwordChangeRequired, suspended: $suspended})\n            |RETURN u.name" )) ).stripMargin(), VirtualValues.map(
                                            (String[]) ((Object[]) (new String[]{"name", "credentials", "passwordChangeRequired", "suspended"})),
                                            (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( userName ), Values.stringValue(
                                                    SystemGraphCredential.createCredentialForPassword( initialPassword,
                                                            this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$secureHasher() ).serialize() ),
                                                    Values.booleanValue( requirePasswordChange ),
                                                    Values.booleanValue( suspended )})) ), org.neo4j.cypher.internal.procs.QueryHandler..
                                    MODULE$.handleNoResult( () -> {
                                        return new Some( new IllegalStateException(
                                                (new StringBuilder( 39 )).append( "Failed to create the specified user '" ).append( userName ).append(
                                                        "'." ).toString() ) );
                                    } ).handleError( ( error ) -> {
                                        Tuple2 var4 = new Tuple2( error, error.getCause() );
                                        Object var3;
                                        if ( var4 != null && var4._2() instanceof UniquePropertyValueValidationException )
                                        {
                                            var3 = new InvalidArgumentsException(
                                                    (new StringBuilder( 60 )).append( "Failed to create the specified user '" ).append( userName ).append(
                                                            "': User already exists." ).toString(), error );
                                        }
                                        else
                                        {
                                            label40:
                                            {
                                                if ( var4 != null )
                                                {
                                                    Throwable e = (Throwable) var4._1();
                                                    if ( e instanceof HasStatus )
                                                    {
                                                        Status var10000 = ((HasStatus) e).status();
                                                        Cluster var7 = Cluster.NotALeader;
                                                        if ( var10000 == null )
                                                        {
                                                            if ( var7 == null )
                                                            {
                                                                break label40;
                                                            }
                                                        }
                                                        else if ( var10000.equals( var7 ) )
                                                        {
                                                            break label40;
                                                        }
                                                    }
                                                }

                                                var3 = new IllegalStateException(
                                                        (new StringBuilder( 39 )).append( "Failed to create the specified user '" ).append( userName ).append(
                                                                "'." ).toString(), error );
                                                return (Throwable) var3;
                                            }

                                            var3 = new DatabaseAdministrationOnFollowerException(
                                                    (new StringBuilder( 40 )).append( "Failed to create the specified user '" ).append( userName ).append(
                                                            "': " ).append( this.$outer.followerError() ).toString(), error );
                                        }

                                        return (Throwable) var3;
                                    } ), sourcexxxx.map( ( x$4 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$4, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, securityContext );
                                    } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                                }
                                finally
                                {
                                    if ( initialPassword != null )
                                    {
                                        Arrays.fill( initialPassword, (byte) 0 );
                                    }
                                }

                                return var10000;
                            }; return var3;
                        }
                        }
                    }

                    if ( x1 instanceof AlterUser )
                    {
                        var4 = true;
                        var5 = (AlterUser) x1;
                        Option sourcexxxxxx = var5.source();
                        String userNamex = var5.userName();
                        Option initialPasswordx = var5.initialStringPassword();
                        Option var24 = var5.initialParameterPassword();
                        Option requirePasswordChangex = var5.requirePasswordChange();
                        Option suspended = var5.suspended();
                        if ( scala.None..MODULE$.equals( var24 )){
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            Seq params = (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                    (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                            initialPasswordx ), "credentials" ), scala.Predef.ArrowAssoc..
                            MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( requirePasswordChangex ), "passwordChangeRequired"),
                            scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( suspended ), "suspended")})))).
                            flatMap( ( param ) -> {
                                boolean var3 = false;
                                Some var4 = null;
                                Option var5 = (Option) param._1();
                                Seq var2;
                                if ( scala.None..MODULE$.equals( var5 )){
                                    var2 = (Seq) scala.collection.Seq..MODULE$.empty();
                                    return var2;
                                } else{
                                    if ( var5 instanceof Some )
                                    {
                                        var3 = true;
                                        var4 = (Some) var5;
                                        Object value = var4.value();
                                        if ( value instanceof Boolean )
                                        {
                                            boolean var7 = BoxesRunTime.unboxToBoolean( value );
                                            var2 = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new Tuple2[]{new Tuple2( param._2(), Values.booleanValue( var7 ) )}) ));
                                            return var2;
                                        }
                                    }

                                    if ( var3 )
                                    {
                                        Object valuex = var4.value();
                                        if ( valuex instanceof byte[] )
                                        {
                                            byte[] var9 = (byte[]) valuex;
                                            var2 = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Tuple2[]{
                                                new Tuple2( param._2(), Values.stringValue(
                                                        SystemGraphCredential.createCredentialForPassword( this.$outer.validatePassword( var9 ),
                                                                this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$secureHasher() ).serialize() ) )}) ))
                                            ;
                                            return var2;
                                        }
                                    }

                                    if ( var3 )
                                    {
                                        Object p = var4.value();
                                        throw new InvalidArgumentsException( (new StringBuilder( 76 )).append(
                                                "Invalid option type for ALTER USER, expected byte array or boolean but got: " ).append(
                                                p.getClass().getSimpleName() ).toString() );
                                    }
                                    else
                                    {
                                        throw new MatchError( var5 );
                                    }
                                }
                            }, scala.collection.Seq..MODULE$.canBuildFrom());
                            Tuple3 var12 = (Tuple3) params.foldLeft(
                                    new Tuple3( "MATCH (user:User {name: $name}) WITH user, user.credentials AS oldCredentials", scala.Array..MODULE$.empty(
                                            scala.reflect.ClassTag..MODULE$.apply( String.class ) ), scala.Array..
                            MODULE$.empty( scala.reflect.ClassTag..MODULE$.apply( AnyValue.class ))),( acc, param ) -> {
                                String key = (String) param._1();
                                return new Tuple3( (new StringBuilder( 0 )).append( (String) acc._1() ).append(
                                        (new StringBuilder( 14 )).append( " SET user." ).append( key ).append( " = $" ).append( key ).toString() ).toString(),
                                        (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) acc._2() )) ).$colon$plus( key,
                                        scala.reflect.ClassTag..MODULE$.apply( String.class )),
                                (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) acc._3() ))).
                                $colon$plus( param._2(), scala.reflect.ClassTag..MODULE$.apply( AnyValue.class )));
                            });
                            if ( var12 != null )
                            {
                                String query = (String) var12._1();
                                String[] keys = (String[]) var12._2();
                                AnyValue[] values = (AnyValue[]) var12._3();
                                Tuple3 var9 = new Tuple3( query, keys, values );
                                String queryx = (String) var9._1();
                                String[] keysx = (String[]) var9._2();
                                AnyValue[] valuesx = (AnyValue[]) var9._3();
                                return new UpdatingSystemCommandExecutionPlan( "AlterUser", this.$outer.normalExecutionEngine(),
                                        (new StringBuilder( 22 )).append( queryx ).append( " RETURN oldCredentials" ).toString(),
                                        VirtualValues.map( (String[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) keysx )) ).$colon$plus( "name",
                                                scala.reflect.ClassTag..MODULE$.apply( String.class ) ),
                                (AnyValue[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) valuesx ))).
                                $colon$plus( Values.stringValue( userNamex ), scala.reflect.ClassTag..MODULE$.apply( AnyValue.class ))),
                                org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleNoResult( () -> {
                                return new Some( new InvalidArgumentsException(
                                        (new StringBuilder( 59 )).append( "Failed to alter the specified user '" ).append( userNamex ).append(
                                                "': User does not exist." ).toString() ) );
                            } ).handleError( ( x0$1 ) -> {
                                Object var3;
                                label27:
                                {
                                    if ( x0$1 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$1).status();
                                        Cluster var6 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var6 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var6 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var3 = new IllegalStateException(
                                            (new StringBuilder( 38 )).append( "Failed to alter the specified user '" ).append( userNamex ).append(
                                                    "'." ).toString(), x0$1 );
                                    return (Throwable) var3;
                                }

                                var3 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 39 )).append( "Failed to alter the specified user '" ).append( userNamex ).append( "': " ).append(
                                                this.$outer.followerError() ).toString(), x0$1 );
                                return (Throwable) var3;
                            } ).handleResult( ( x$6, value ) -> {
                                return $anonfun$applyOrElse$19( this, userNamex, initialPasswordx, BoxesRunTime.unboxToInt( x$6 ), value );
                            } ), sourcexxxxxx.map( ( x$7 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$7, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                            }
                            else
                            {
                                throw new MatchError( var12 );
                            }
                        }; return var3;
                    }
                    }

                    if ( var4 )
                    {
                        String userNamexx = var5.userName();
                        Option var28 = var5.initialParameterPassword();
                        if ( var28 instanceof Some )
                        {
                            throw new IllegalStateException(
                                    (new StringBuilder( 76 )).append( "Failed to alter the specified user '" ).append( userNamexx ).append(
                                            "': Did not resolve parameters correctly." ).toString() );
                        }
                    }

                    if ( x1 instanceof ShowRoles )
                    {
                        ShowRoles var29 = (ShowRoles) x1;
                        Option sourcexxxxxxxxx = var29.source();
                        boolean withUsers = var29.withUsers();
                        boolean showAll = var29.showAll();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            TextArray predefinedRoles = Values.stringArray( new String[]{"admin", "architect", "publisher", "editor", "reader"} );
                            String query = showAll ? (new StringOps( scala.Predef..MODULE$.augmentString(
                                    "MATCH (r:Role)\n          |OPTIONAL MATCH (u:User)-[:HAS_ROLE]->(r)\n          |RETURN DISTINCT r.name as role,\n          |CASE\n          | WHEN r.name IN $predefined THEN true\n          | ELSE false\n          |END as isBuiltIn\n        " ))).
                            stripMargin() :(new StringOps( scala.Predef..MODULE$.augmentString(
                                    "MATCH (r:Role)<-[:HAS_ROLE]-(u:User)\n          |RETURN DISTINCT r.name as role,\n          |CASE\n          | WHEN r.name IN $predefined THEN true\n          | ELSE false\n          |END as isBuiltIn\n        " ))).
                            stripMargin();
                            SystemCommandExecutionPlan var10000;
                            if ( withUsers )
                            {
                                String x$59 = "ShowRoles";
                                ExecutionEngine x$60 = this.$outer.normalExecutionEngine();
                                String x$61 = (new StringBuilder( 18 )).append( query ).append( ", u.name as member" ).toString();
                                MapValue x$62 = VirtualValues.map( (String[]) ((Object[]) (new String[]{"predefined"})),
                                        (AnyValue[]) ((Object[]) (new AnyValue[]{predefinedRoles})) );
                                Option x$63 = sourcexxxxxxxxx.map( ( x$8 ) -> {
                                    return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                            x$8, ( unknownPlan ) -> {
                                                return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                        unknownPlan );
                                            } )).apply( context, parameterMapping, securityContext );
                                } );
                                QueryHandler x$64 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$5();
                                boolean x$65 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$7();
                                Function1 x$66 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$8();
                                var10000 = new SystemCommandExecutionPlan( x$59, x$60, x$61, x$62, x$64, x$63, x$65, x$66 );
                            }
                            else
                            {
                                String x$67 = "ShowRoles";
                                ExecutionEngine x$68 = this.$outer.normalExecutionEngine();
                                MapValue x$70 = VirtualValues.map( (String[]) ((Object[]) (new String[]{"predefined"})),
                                        (AnyValue[]) ((Object[]) (new AnyValue[]{predefinedRoles})) );
                                Option x$71 = sourcexxxxxxxxx.map( ( x$9 ) -> {
                                    return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                            x$9, ( unknownPlan ) -> {
                                                return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                        unknownPlan );
                                            } )).apply( context, parameterMapping, securityContext );
                                } );
                                QueryHandler x$72 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$5();
                                boolean x$73 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$7();
                                Function1 x$74 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$8();
                                var10000 = new SystemCommandExecutionPlan( x$67, x$68, query, x$70, x$72, x$71, x$73, x$74 );
                            }

                            return var10000;
                        };
                    }
                    else if ( x1 instanceof CreateRole )
                    {
                        CreateRole var33 = (CreateRole) x1;
                        Option sourcexxxxxxxxxxx = var33.source();
                        String roleNamexxxxxxx = var33.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return new UpdatingSystemCommandExecutionPlan( "CreateRole", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "CREATE (new:Role {name: $name})\n          |RETURN new.name\n        " )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"name"})), (AnyValue[]) ((Object[]) (new AnyValue[]{
                                    Values.stringValue( roleNamexxxxxxx )})) ), org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleNoResult( () -> {
                                return new Some( new IllegalStateException(
                                        (new StringBuilder( 39 )).append( "Failed to create the specified role '" ).append( roleNamexxxxxxx ).append(
                                                "'." ).toString() ) );
                            } ).handleError( ( error ) -> {
                                Tuple2 var4 = new Tuple2( error, error.getCause() );
                                Object var3;
                                if ( var4 != null && var4._2() instanceof UniquePropertyValueValidationException )
                                {
                                    var3 = new InvalidArgumentsException(
                                            (new StringBuilder( 60 )).append( "Failed to create the specified role '" ).append( roleNamexxxxxxx ).append(
                                                    "': Role already exists." ).toString(), error );
                                }
                                else
                                {
                                    label40:
                                    {
                                        if ( var4 != null )
                                        {
                                            Throwable e = (Throwable) var4._1();
                                            if ( e instanceof HasStatus )
                                            {
                                                Status var10000 = ((HasStatus) e).status();
                                                Cluster var7 = Cluster.NotALeader;
                                                if ( var10000 == null )
                                                {
                                                    if ( var7 == null )
                                                    {
                                                        break label40;
                                                    }
                                                }
                                                else if ( var10000.equals( var7 ) )
                                                {
                                                    break label40;
                                                }
                                            }
                                        }

                                        var3 = new IllegalStateException(
                                                (new StringBuilder( 39 )).append( "Failed to create the specified role '" ).append( roleNamexxxxxxx ).append(
                                                        "'." ).toString(), error );
                                        return (Throwable) var3;
                                    }

                                    var3 = new DatabaseAdministrationOnFollowerException(
                                            (new StringBuilder( 40 )).append( "Failed to create the specified role '" ).append( roleNamexxxxxxx ).append(
                                                    "': " ).append( this.$outer.followerError() ).toString(), error );
                                }

                                return (Throwable) var3;
                            } ), sourcexxxxxxxxxxx.map( ( x$10 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$10, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof RequireRole )
                    {
                        RequireRole var36 = (RequireRole) x1;
                        Option sourcexxxxxxxxxxxx = var36.source();
                        String roleNamexxxxxxxxx = var36.name();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return new UpdatingSystemCommandExecutionPlan( "RequireRole", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "MATCH (role:Role {name: $name})\n          |RETURN role.name" )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"name"})), (AnyValue[]) ((Object[]) (new AnyValue[]{
                                    Values.stringValue( roleNamexxxxxxxxx )})) ), org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleNoResult( () -> {
                                return new Some( new InvalidArgumentsException(
                                        (new StringBuilder( 59 )).append( "Failed to create a role as copy of '" ).append( roleNamexxxxxxxxx ).append(
                                                "': Role does not exist." ).toString() ) );
                            } ).handleError( ( x0$2 ) -> {
                                Object var3;
                                label27:
                                {
                                    if ( x0$2 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$2).status();
                                        Cluster var6 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var6 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var6 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var3 = new IllegalStateException(
                                            (new StringBuilder( 38 )).append( "Failed to create a role as copy of '" ).append( roleNamexxxxxxxxx ).append(
                                                    "'." ).toString(), x0$2 );
                                    return (Throwable) var3;
                                }

                                var3 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 39 )).append( "Failed to create a role as copy of '" ).append( roleNamexxxxxxxxx ).append(
                                                "': " ).append( this.$outer.followerError() ).toString(), x0$2 );
                                return (Throwable) var3;
                            } ), sourcexxxxxxxxxxxx.map( ( x$11 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$11, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof CopyRolePrivileges )
                    {
                        CopyRolePrivileges var39 = (CopyRolePrivileges) x1;
                        Option sourcexxxxxxxxxxxxx = var39.source();
                        String to = var39.to();
                        String from = var39.from();
                        String grantDeny = var39.grantDeny();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return new UpdatingSystemCommandExecutionPlan( "CopyPrivileges", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString( (new StringBuilder( 171 )).append(
                                            "MATCH (to:Role {name: $to})\n           |MATCH (from:Role {name: $from})-[:" ).append( grantDeny ).append(
                                            "]->(p:Privilege)\n           |MERGE (to)-[g:" ).append( grantDeny ).append(
                                            "]->(p)\n           |RETURN from.name, to.name, count(g)" ).toString() )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"from", "to"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( from ),
                                            Values.stringValue( to )})) ), org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleError( ( e ) -> {
                                return new IllegalStateException(
                                        (new StringBuilder( 66 )).append( "Failed to create role '" ).append( to ).append( "' as copy of '" ).append(
                                                from ).append( "': Failed to copy privileges." ).toString(), e );
                            } ), sourcexxxxxxxxxxxxx.map( ( x$12 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$12, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof DropRole )
                    {
                        DropRole var44 = (DropRole) x1;
                        Option sourcexxxxxxxxxxxxxxx = var44.source();
                        String roleNamexxxxxxxxxxx = var44.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return new UpdatingSystemCommandExecutionPlan( "DropRole", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "MATCH (role:Role {name: $name}) DETACH DELETE role\n          |RETURN 1 AS ignore" )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"name"})), (AnyValue[]) ((Object[]) (new AnyValue[]{
                                    Values.stringValue( roleNamexxxxxxxxxxx )})) ), org.neo4j.cypher.internal.procs.QueryHandler..
                            MODULE$.handleError( ( x0$3 ) -> {
                                Object var3;
                                label27:
                                {
                                    if ( x0$3 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$3).status();
                                        Cluster var6 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var6 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var6 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var3 = new IllegalStateException(
                                            (new StringBuilder( 39 )).append( "Failed to delete the specified role '" ).append( roleNamexxxxxxxxxxx ).append(
                                                    "'." ).toString(), x0$3 );
                                    return (Throwable) var3;
                                }

                                var3 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 40 )).append( "Failed to delete the specified role '" ).append( roleNamexxxxxxxxxxx ).append(
                                                "': " ).append( this.$outer.followerError() ).toString(), x0$3 );
                                return (Throwable) var3;
                            } ), sourcexxxxxxxxxxxxxxx.map( ( x$13 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$13, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof GrantRoleToUser )
                    {
                        GrantRoleToUser var47 = (GrantRoleToUser) x1;
                        Option sourcexxxxxxxxxxxxxxxxx = var47.source();
                        String roleNamexxxxxxxxxxxx = var47.roleName();
                        String userNamexxx = var47.userName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return new UpdatingSystemCommandExecutionPlan( "GrantRoleToUser", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "MATCH (r:Role {name: $role})\n          |OPTIONAL MATCH (u:User {name: $user})\n          |WITH r, u\n          |MERGE (u)-[a:HAS_ROLE]->(r)\n          |RETURN u.name AS user" )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"role", "user"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( roleNamexxxxxxxxxxxx ),
                                            Values.stringValue( userNamexxx )})) ), org.neo4j.cypher.internal.procs.QueryHandler..
                            MODULE$.handleNoResult( () -> {
                                return new Some( new InvalidArgumentsException(
                                        (new StringBuilder( 56 )).append( "Failed to grant role '" ).append( roleNamexxxxxxxxxxxx ).append(
                                                "' to user '" ).append( userNamexxx ).append( "': Role does not exist." ).toString() ) );
                            } ).handleError( ( x0$4 ) -> {
                                Object var4;
                                if ( x0$4 instanceof InternalException )
                                {
                                    InternalException var6 = (InternalException) x0$4;
                                    if ( var6.getMessage().contains( "ignore rows where a relationship node is missing" ) )
                                    {
                                        var4 = new InvalidArgumentsException(
                                                (new StringBuilder( 56 )).append( "Failed to grant role '" ).append( roleNamexxxxxxxxxxxx ).append(
                                                        "' to user '" ).append( userNamexxx ).append( "': User does not exist." ).toString(), var6 );
                                        return (Throwable) var4;
                                    }
                                }

                                label40:
                                {
                                    if ( x0$4 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$4).status();
                                        Cluster var8 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var8 == null )
                                            {
                                                break label40;
                                            }
                                        }
                                        else if ( var10000.equals( var8 ) )
                                        {
                                            break label40;
                                        }
                                    }

                                    var4 = new IllegalStateException(
                                            (new StringBuilder( 35 )).append( "Failed to grant role '" ).append( roleNamexxxxxxxxxxxx ).append(
                                                    "' to user '" ).append( userNamexxx ).append( "'." ).toString(), x0$4 );
                                    return (Throwable) var4;
                                }

                                var4 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 36 )).append( "Failed to grant role '" ).append( roleNamexxxxxxxxxxxx ).append(
                                                "' to user '" ).append( userNamexxx ).append( "': " ).append( this.$outer.followerError() ).toString(), x0$4 );
                                return (Throwable) var4;
                            } ), sourcexxxxxxxxxxxxxxxxx.map( ( x$14 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$14, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof RevokeRoleFromUser )
                    {
                        RevokeRoleFromUser var51 = (RevokeRoleFromUser) x1;
                        Option sourcexxxxxxxxxxxxxxxxxx = var51.source();
                        String roleNamexxxxxxxxxxxxxx = var51.roleName();
                        String userNamexxxx = var51.userNames();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return new UpdatingSystemCommandExecutionPlan( "RevokeRoleFromUser", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "MATCH (r:Role {name: $role})\n          |OPTIONAL MATCH (u:User {name: $user})\n          |WITH r, u\n          |OPTIONAL MATCH (u)-[a:HAS_ROLE]->(r)\n          |DELETE a\n          |RETURN u.name AS user" )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"role", "user"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( roleNamexxxxxxxxxxxxxx ),
                                            Values.stringValue( userNamexxxx )})) ), org.neo4j.cypher.internal.procs.QueryHandler..
                            MODULE$.handleError( ( x0$5 ) -> {
                                Object var4;
                                label27:
                                {
                                    if ( x0$5 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$5).status();
                                        Cluster var7 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var7 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var7 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var4 = new IllegalStateException(
                                            (new StringBuilder( 38 )).append( "Failed to revoke role '" ).append( roleNamexxxxxxxxxxxxxx ).append(
                                                    "' from user '" ).append( userNamexxxx ).append( "'." ).toString(), x0$5 );
                                    return (Throwable) var4;
                                }

                                var4 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 39 )).append( "Failed to revoke role '" ).append( roleNamexxxxxxxxxxxxxx ).append(
                                                "' from user '" ).append( userNamexxxx ).append( "': " ).append( this.$outer.followerError() ).toString(),
                                        x0$5 );
                                return (Throwable) var4;
                            } ), sourcexxxxxxxxxxxxxxxxxx.map( ( x$15 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$15, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof GrantDbmsAction )
                    {
                        GrantDbmsAction var55 = (GrantDbmsAction) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxx = var55.source();
                        AdminAction actionx = var55.action();
                        String roleNamexxxxxxxxxxxxxxxx = var55.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbmsAction = org.neo4j.cypher.internal.procs.AdminActionMapper..MODULE$.asKernelAction( actionx ).toString();
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan( dbmsAction,
                                    new DatabaseResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            new AllGraphsScope( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            new AllQualifier( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            roleNamexxxxxxxxxxxxxxxx, sourcexxxxxxxxxxxxxxxxxxxx.map( ( x$16 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$16, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), ResourcePrivilege.GrantOrDeny.GRANT, (new StringBuilder( 41 )).append( "Failed to grant dbms privilege to role '" ).append(
                                    roleNamexxxxxxxxxxxxxxxx ).append( "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof DenyDbmsAction )
                    {
                        DenyDbmsAction var59 = (DenyDbmsAction) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxx = var59.source();
                        AdminAction actionxx = var59.action();
                        String roleNamexxxxxxxxxxxxxxxxx = var59.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbmsAction = org.neo4j.cypher.internal.procs.AdminActionMapper..MODULE$.asKernelAction( actionxx ).toString();
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan( dbmsAction,
                                    new DatabaseResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            new AllGraphsScope( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            new AllQualifier( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            roleNamexxxxxxxxxxxxxxxxx, sourcexxxxxxxxxxxxxxxxxxxxxx.map( ( x$17 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$17, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), ResourcePrivilege.GrantOrDeny.DENY, (new StringBuilder( 40 )).append( "Failed to deny dbms privilege to role '" ).append(
                                    roleNamexxxxxxxxxxxxxxxxx ).append( "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof RevokeDbmsAction )
                    {
                        RevokeDbmsAction var63 = (RevokeDbmsAction) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxx = var63.source();
                        AdminAction actionxxx = var63.action();
                        String roleNamexxxxxxxxxxxxxxxxxx = var63.roleName();
                        String revokeTypexxxx = var63.revokeType();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbmsAction = org.neo4j.cypher.internal.procs.AdminActionMapper..MODULE$.asKernelAction( actionxxx ).toString();
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeRevokeExecutionPlan( dbmsAction,
                                    new DatabaseResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            new AllGraphsScope( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            new AllQualifier( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            roleNamexxxxxxxxxxxxxxxxxx, revokeTypexxxx, sourcexxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$18 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$18, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), (new StringBuilder( 44 )).append( "Failed to revoke dbms privilege from role '" ).append( roleNamexxxxxxxxxxxxxxxxxx ).append(
                                    "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof AssertValidRevoke )
                    {
                        AssertValidRevoke var68 = (AssertValidRevoke) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxxx = var68.source();
                        AdminAction actionxxxx = var68.action();
                        GraphScope scopex = var68.scope();
                        String roleNamexxxxxxxxxxxxxxxxxxx = var68.roleName();
                        RevokeType revokeTypexxxxx = var68.revokeType();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            Tuple2 var11;
                            if ( scopex instanceof AllGraphsScope )
                            {
                                var11 = new Tuple2( "d:DatabaseAll", Values.of( "*" ) );
                            }
                            else
                            {
                                if ( !(scopex instanceof NamedGraphScope) )
                                {
                                    throw new MatchError( scopex );
                                }

                                NamedGraphScope var15 = (NamedGraphScope) scopex;
                                String database = var15.database();
                                var11 = new Tuple2( "d.name = $database", Values.of( database ) );
                            }

                            if ( var11 != null )
                            {
                                String dbPredicate = (String) var11._1();
                                Value dbValue = (Value) var11._2();
                                Tuple2 var10 = new Tuple2( dbPredicate, dbValue );
                                String dbPredicatex = (String) var10._1();
                                Value dbValuex = (Value) var10._2();
                                String var9;
                                if ( revokeTypexxxxx instanceof RevokeBothType )
                                {
                                    var9 = "";
                                }
                                else if ( revokeTypexxxxx instanceof RevokeGrantType )
                                {
                                    var9 = "AND type(g) = 'GRANTED'";
                                }
                                else
                                {
                                    if ( !(revokeTypexxxxx instanceof RevokeDenyType) )
                                    {
                                        throw new MatchError( revokeTypexxxxx );
                                    }

                                    var9 = "AND type(g) = 'DENIED'";
                                }

                                String query = (new StringOps( scala.Predef..MODULE$.augmentString( (new StringBuilder( 179 )).append(
                                        "\n           |MATCH (r:Role)-[g]->(p:Privilege)-[:SCOPE]->(s:Segment)-[:FOR]->(d)\n           |WHERE r.name = $grantee AND " ).append(
                                        dbPredicatex ).append( " " ).append( var9 ).append(
                                        "\n           |RETURN collect(p.action) as actions\n        " ).toString() ))).stripMargin();
                                Value grantee = Values.of( roleNamexxxxxxxxxxxxxxxxxxx );
                                PrivilegeAction privilegeAction = org.neo4j.cypher.internal.procs.AdminActionMapper..MODULE$.asKernelAction( actionxxxx );
                                return new UpdatingSystemCommandExecutionPlan( "AssertValidRevoke", this.$outer.normalExecutionEngine(), query,
                                        VirtualValues.map( (String[]) ((Object[]) (new String[]{"grantee", "database"})),
                                                (AnyValue[]) ((Object[]) (new AnyValue[]{grantee, dbValuex})) ),
                                        org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleResult( ( i, value ) -> {
                                    return $anonfun$applyOrElse$64( actionxxxx, privilegeAction, BoxesRunTime.unboxToInt( i ), value );
                                } ),sourcexxxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$20 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$20, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                            }
                            else
                            {
                                throw new MatchError( var11 );
                            }
                        };
                    }
                    else if ( x1 instanceof GrantDatabaseAction )
                    {
                        GrantDatabaseAction var74 = (GrantDatabaseAction) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxx = var74.source();
                        AdminAction actionxxxxx = var74.action();
                        GraphScope databasexxxxxxxxxx = var74.database();
                        String roleNamexxxxxxxxxxxxxxxxxxxx = var74.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String databaseAction = org.neo4j.cypher.internal.procs.AdminActionMapper..MODULE$.asKernelAction( actionxxxxx ).toString();
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan( databaseAction,
                                    new DatabaseResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            databasexxxxxxxxxx, new AllQualifier( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            roleNamexxxxxxxxxxxxxxxxxxxx, sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$21 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$21, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), ResourcePrivilege.GrantOrDeny.GRANT, (new StringBuilder( 43 )).append( "Failed to grant access privilege to role '" ).append(
                                    roleNamexxxxxxxxxxxxxxxxxxxx ).append( "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof DenyDatabaseAction )
                    {
                        DenyDatabaseAction var79 = (DenyDatabaseAction) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx = var79.source();
                        AdminAction actionxxxxxxx = var79.action();
                        GraphScope databasexxxxxxxxxxx = var79.database();
                        String roleNamexxxxxxxxxxxxxxxxxxxxx = var79.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String databaseAction = org.neo4j.cypher.internal.procs.AdminActionMapper..MODULE$.asKernelAction( actionxxxxxxx ).toString();
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan( databaseAction,
                                    new DatabaseResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            databasexxxxxxxxxxx, new AllQualifier( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            roleNamexxxxxxxxxxxxxxxxxxxxx, sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$22 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$22, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), ResourcePrivilege.GrantOrDeny.DENY, (new StringBuilder( 42 )).append( "Failed to deny access privilege to role '" ).append(
                                    roleNamexxxxxxxxxxxxxxxxxxxxx ).append( "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof RevokeDatabaseAction )
                    {
                        RevokeDatabaseAction var84 = (RevokeDatabaseAction) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx = var84.source();
                        AdminAction action = var84.action();
                        GraphScope database = var84.database();
                        String roleName = var84.roleName();
                        String revokeType = var84.revokeType();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String databaseAction = org.neo4j.cypher.internal.procs.AdminActionMapper..MODULE$.asKernelAction( action ).toString();
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeRevokeExecutionPlan( databaseAction,
                                    new DatabaseResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            database, new AllQualifier( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE()),
                            roleName, revokeType, sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$23 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$23, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), (new StringBuilder( 46 )).append( "Failed to revoke access privilege from role '" ).append( roleName ).append( "'" ).toString())
                            ;
                        };
                    }
                    else if ( x1 instanceof GrantTraverse )
                    {
                        GrantTraverse var90 = (GrantTraverse) x1;
                        Option source = var90.source();
                        GraphScope databasex = var90.database();
                        PrivilegeQualifier qualifier = var90.qualifier();
                        String roleNamex = var90.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan(
                                    PrivilegeAction.TRAVERSE.toString(), new NoResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            databasex, qualifier, roleNamex, source.map( ( x$24 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$24, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), ResourcePrivilege.GrantOrDeny.GRANT, (new StringBuilder( 46 )).append(
                                    "Failed to grant traversal privilege to role '" ).append( roleNamex ).append( "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof DenyTraverse )
                    {
                        DenyTraverse var95 = (DenyTraverse) x1;
                        Option sourcexx = var95.source();
                        GraphScope databasexx = var95.database();
                        PrivilegeQualifier qualifierx = var95.qualifier();
                        String roleNamexxx = var95.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan(
                                    PrivilegeAction.TRAVERSE.toString(), new NoResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            databasexx, qualifierx, roleNamexxx, sourcexx.map( ( x$25 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$25, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), ResourcePrivilege.GrantOrDeny.DENY, (new StringBuilder( 45 )).append( "Failed to deny traversal privilege to role '" ).append(
                                    roleNamexxx ).append( "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof RevokeTraverse )
                    {
                        RevokeTraverse var100 = (RevokeTraverse) x1;
                        Option sourcexxxxx = var100.source();
                        GraphScope databasexxx = var100.database();
                        PrivilegeQualifier qualifierxx = var100.qualifier();
                        String roleNamexxxx = var100.roleName();
                        String revokeTypex = var100.revokeType();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeRevokeExecutionPlan(
                                    PrivilegeAction.TRAVERSE.toString(), new NoResource( org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ),
                            databasexxx, qualifierxx, roleNamexxxx, revokeTypex, sourcexxxxx.map( ( x$26 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$26, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), (new StringBuilder( 49 )).append( "Failed to revoke traversal privilege from role '" ).append( roleNamexxxx ).append(
                                    "'" ).toString());
                        };
                    }
                    else if ( x1 instanceof GrantRead )
                    {
                        GrantRead var106 = (GrantRead) x1;
                        Option sourcexxxxxxx = var106.source();
                        ActionResource resource = var106.resource();
                        GraphScope databasexxxx = var106.database();
                        PrivilegeQualifier qualifierxxx = var106.qualifier();
                        String roleNamexxxxx = var106.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan(
                                    PrivilegeAction.READ.toString(), resource, databasexxxx, qualifierxxx, roleNamexxxxx, sourcexxxxxxx.map( ( x$27 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$27, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, securityContext );
                                    } ), ResourcePrivilege.GrantOrDeny.GRANT,
                                    (new StringBuilder( 41 )).append( "Failed to grant read privilege to role '" ).append( roleNamexxxxx ).append(
                                            "'" ).toString() );
                        };
                    }
                    else if ( x1 instanceof DenyRead )
                    {
                        DenyRead var112 = (DenyRead) x1;
                        Option sourcexxxxxxxx = var112.source();
                        ActionResource resourcex = var112.resource();
                        GraphScope databasexxxxx = var112.database();
                        PrivilegeQualifier qualifierxxxx = var112.qualifier();
                        String roleNamexxxxxx = var112.roleName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan(
                                    PrivilegeAction.READ.toString(), resourcex, databasexxxxx, qualifierxxxx, roleNamexxxxxx, sourcexxxxxxxx.map( ( x$28 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$28, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, securityContext );
                                    } ), ResourcePrivilege.GrantOrDeny.DENY,
                                    (new StringBuilder( 40 )).append( "Failed to deny read privilege to role '" ).append( roleNamexxxxxx ).append(
                                            "'" ).toString() );
                        };
                    }
                    else if ( x1 instanceof RevokeRead )
                    {
                        RevokeRead var118 = (RevokeRead) x1;
                        Option sourcexxxxxxxxxx = var118.source();
                        ActionResource resourcexx = var118.resource();
                        GraphScope databasexxxxxx = var118.database();
                        PrivilegeQualifier qualifierxxxxx = var118.qualifier();
                        String roleNamexxxxxxxx = var118.roleName();
                        String revokeTypexx = var118.revokeType();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeRevokeExecutionPlan(
                                    PrivilegeAction.READ.toString(), resourcexx, databasexxxxxx, qualifierxxxxx, roleNamexxxxxxxx, revokeTypexx,
                                    sourcexxxxxxxxxx.map( ( x$29 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$29, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, securityContext );
                                    } ), (new StringBuilder( 44 )).append( "Failed to revoke read privilege from role '" ).append( roleNamexxxxxxxx ).append(
                                            "'" ).toString() );
                        };
                    }
                    else if ( x1 instanceof GrantWrite )
                    {
                        GrantWrite var125 = (GrantWrite) x1;
                        Option sourcexxxxxxxxxxxxxx = var125.source();
                        ActionResource resourcexxx = var125.resource();
                        GraphScope databasexxxxxxx = var125.database();
                        PrivilegeQualifier qualifierxxxxxx = var125.qualifier();
                        String roleNamexxxxxxxxxx = var125.roleName();
                        var3 = ( context, parameterMapping, currentUser ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan(
                                    PrivilegeAction.WRITE.toString(), resourcexxx, databasexxxxxxx, qualifierxxxxxx, roleNamexxxxxxxxxx,
                                    sourcexxxxxxxxxxxxxx.map( ( x$30 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$30, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, currentUser );
                                    } ), ResourcePrivilege.GrantOrDeny.GRANT,
                                    (new StringBuilder( 42 )).append( "Failed to grant write privilege to role '" ).append( roleNamexxxxxxxxxx ).append(
                                            "'" ).toString() );
                        };
                    }
                    else if ( x1 instanceof DenyWrite )
                    {
                        DenyWrite var131 = (DenyWrite) x1;
                        Option sourcexxxxxxxxxxxxxxxx = var131.source();
                        ActionResource resourcexxxx = var131.resource();
                        GraphScope databasexxxxxxxx = var131.database();
                        PrivilegeQualifier qualifierxxxxxxx = var131.qualifier();
                        String roleNamexxxxxxxxxxxxx = var131.roleName();
                        var3 = ( context, parameterMapping, currentUser ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan(
                                    PrivilegeAction.WRITE.toString(), resourcexxxx, databasexxxxxxxx, qualifierxxxxxxx, roleNamexxxxxxxxxxxxx,
                                    sourcexxxxxxxxxxxxxxxx.map( ( x$31 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$31, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, currentUser );
                                    } ), ResourcePrivilege.GrantOrDeny.DENY,
                                    (new StringBuilder( 41 )).append( "Failed to deny write privilege to role '" ).append( roleNamexxxxxxxxxxxxx ).append(
                                            "'" ).toString() );
                        };
                    }
                    else if ( x1 instanceof RevokeWrite )
                    {
                        RevokeWrite var137 = (RevokeWrite) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxx = var137.source();
                        ActionResource resourcexxxxx = var137.resource();
                        GraphScope databasexxxxxxxxx = var137.database();
                        PrivilegeQualifier qualifierxxxxxxxx = var137.qualifier();
                        String roleNamexxxxxxxxxxxxxxx = var137.roleName();
                        String revokeTypexxx = var137.revokeType();
                        var3 = ( context, parameterMapping, currentUser ) -> {
                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeRevokeExecutionPlan(
                                    PrivilegeAction.WRITE.toString(), resourcexxxxx, databasexxxxxxxxx, qualifierxxxxxxxx, roleNamexxxxxxxxxxxxxxx,
                                    revokeTypexxx, sourcexxxxxxxxxxxxxxxxxxx.map( ( x$32 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$32, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, currentUser );
                                    } ),
                                    (new StringBuilder( 45 )).append( "Failed to revoke write privilege from role '" ).append( roleNamexxxxxxxxxxxxxxx ).append(
                                            "'" ).toString() );
                        };
                    }
                    else if ( x1 instanceof ShowPrivileges )
                    {
                        ShowPrivileges var144 = (ShowPrivileges) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxx = var144.source();
                        ShowPrivilegeScope scope = var144.scope();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String privilegeMatch = (new StringOps( scala.Predef..MODULE$.augmentString(
                                    "\n          |MATCH (r)-[g]->(p:Privilege)-[:SCOPE]->(s:Segment),\n          |    (p)-[:APPLIES_TO]->(res:Resource),\n          |    (s)-[:FOR]->(d),\n          |    (s)-[:QUALIFIED]->(q)\n          |WHERE d:Database OR d:DatabaseAll\n        " ))).
                            stripMargin();
                            String segmentColumn = (new StringOps( scala.Predef..MODULE$.augmentString(
                                    "\n          |CASE q.type\n          |  WHEN 'node' THEN 'NODE('+q.label+')'\n          |  WHEN 'relationship' THEN 'RELATIONSHIP('+q.label+')'\n          |  WHEN 'database' THEN 'database'\n          |  ELSE 'ELEMENT('+q.label+')'\n          |END\n        " ))).
                            stripMargin();
                            String resourceColumn = (new StringOps( scala.Predef..MODULE$.augmentString(
                                    "\n          |CASE res.type\n          |  WHEN 'property' THEN 'property('+res.arg1+')'\n          |  ELSE res.type\n          |END\n        " ))).
                            stripMargin();
                            String returnColumns = (new StringOps( scala.Predef..MODULE$.augmentString(
                                    (new StringBuilder( 147 )).append( "\n          |RETURN type(g) AS access, p.action AS action, " ).append(
                                            resourceColumn ).append(
                                            " AS resource,\n          |coalesce(d.name, '*') AS graph, segment, r.name AS role\n        " ).toString() ))).
                            stripMargin();
                            String orderBy = (new StringOps( scala.Predef..MODULE$.augmentString(
                                    "\n          |ORDER BY role, graph, segment, resource, action\n        " ))).stripMargin();
                            Tuple3 var7;
                            if ( scope instanceof ShowRolePrivileges )
                            {
                                ShowRolePrivileges var16 = (ShowRolePrivileges) scope;
                                String name = var16.role();
                                var7 = new Tuple3( Values.stringValue( name ), (new StringOps( scala.Predef..MODULE$.augmentString(
                                        (new StringBuilder( 206 )).append(
                                                "\n             |OPTIONAL MATCH (r:Role) WHERE r.name = $grantee WITH r\n             |" ).append(
                                                privilegeMatch ).append( "\n             |WITH g, p, res, d, " ).append( segmentColumn ).append(
                                                " AS segment, r ORDER BY d.name, r.name, segment\n             |" ).append( returnColumns ).append(
                                                "\n             |" ).append( orderBy ).append(
                                                "\n          " ).toString() )) ).stripMargin(), sourcexxxxxxxxxxxxxxxxxxxxx);
                            }
                            else if ( scope instanceof ShowUserPrivileges )
                            {
                                ShowUserPrivileges var18 = (ShowUserPrivileges) scope;
                                String namex = var18.user();
                                String currentUser = securityContext.subject().username();
                                Option newSource = namex.equals( currentUser ) ? scala.None..MODULE$:
                            sourcexxxxxxxxxxxxxxxxxxxxx;
                                var7 = new Tuple3( Values.stringValue( namex ), (new StringOps( scala.Predef..MODULE$.augmentString(
                                        (new StringBuilder( 258 )).append(
                                                "\n             |OPTIONAL MATCH (u:User)-[:HAS_ROLE]->(r:Role) WHERE u.name = $grantee WITH r, u\n             |" ).append(
                                                privilegeMatch ).append( "\n             |WITH g, p, res, d, " ).append( segmentColumn ).append(
                                                " AS segment, r, u ORDER BY d.name, u.name, r.name, segment\n             |" ).append( returnColumns ).append(
                                                ", u.name AS user\n             |" ).append( orderBy ).append(
                                                "\n          " ).toString() )) ).stripMargin(), newSource);
                            }
                            else
                            {
                                if ( !(scope instanceof ShowAllPrivileges) )
                                {
                                    throw new IllegalStateException(
                                            (new StringBuilder( 31 )).append( "Invalid show privilege scope '" ).append( scope ).append( "'" ).toString() );
                                }

                                var7 = new Tuple3( Values.NO_VALUE, (new StringOps( scala.Predef..MODULE$.augmentString(
                                        (new StringBuilder( 182 )).append( "\n             |OPTIONAL MATCH (r:Role) WITH r\n             |" ).append(
                                                privilegeMatch ).append( "\n             |WITH g, p, res, d, " ).append( segmentColumn ).append(
                                                " AS segment, r ORDER BY d.name, r.name, segment\n             |" ).append( returnColumns ).append(
                                                "\n             |" ).append( orderBy ).append(
                                                "\n          " ).toString() )) ).stripMargin(), sourcexxxxxxxxxxxxxxxxxxxxx);
                            }

                            if ( var7 != null )
                            {
                                Value grantee = (Value) var7._1();
                                String query = (String) var7._2();
                                Option updatedSource = (Option) var7._3();
                                if ( grantee != null )
                                {
                                    Tuple3 var6 = new Tuple3( grantee, query, updatedSource );
                                    Value granteex = (Value) var6._1();
                                    String queryx = (String) var6._2();
                                    Option updatedSourcex = (Option) var6._3();
                                    String x$75 = "ShowPrivileges";
                                    ExecutionEngine x$76 = this.$outer.normalExecutionEngine();
                                    MapValue x$78 = VirtualValues.map( (String[]) ((Object[]) (new String[]{"grantee"})),
                                            (AnyValue[]) ((Object[]) (new AnyValue[]{granteex})) );
                                    Option x$79 = updatedSourcex.map( ( x$34 ) -> {
                                        return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                                x$34, ( unknownPlan ) -> {
                                                    return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                            unknownPlan );
                                                } )).apply( context, parameterMapping, securityContext );
                                    } );
                                    QueryHandler x$80 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$5();
                                    boolean x$81 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$7();
                                    Function1 x$82 = org.neo4j.cypher.internal.procs.SystemCommandExecutionPlan..MODULE$.apply$default$8();
                                    return new SystemCommandExecutionPlan( x$75, x$76, queryx, x$78, x$80, x$79, x$81, x$82 );
                                }
                            }

                            throw new MatchError( var7 );
                        };
                    }
                    else if ( x1 instanceof CreateDatabase )
                    {
                        CreateDatabase var147 = (CreateDatabase) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxx = var147.source();
                        NormalizedDatabaseName normalizedName = var147.normalizedName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbName = normalizedName.name();
                            String defaultDbName =
                                    (String) ((Config) this.$outer.resolver().resolveDependency( Config.class )).get( GraphDatabaseSettings.default_database );
                            boolean var12 = dbName.equals( defaultDbName );
                            Try var14 = scala.util.Try..MODULE$.apply( () -> {
                                return (RaftMachine) this.$outer.resolver().resolveDependency( RaftMachine.class );
                            } );
                            Object var9;
                            if ( var14 instanceof Success )
                            {
                                Success var15 = (Success) var14;
                                RaftMachine raftMachine = (RaftMachine) var15.value();
                                Set initialMembersSet = (Set) scala.collection.JavaConverters..
                                MODULE$.asScalaSetConverter( raftMachine.coreState().committed().members() ).asScala();
                                Tuple4 optsx = new Tuple4( ((TraversableOnce) initialMembersSet.map( ( x$35 ) -> {
                                    return x$35.getUuid().toString();
                                }, scala.collection.mutable.Set..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( String.class )),
                                BoxesRunTime.boxToLong( System.currentTimeMillis() ), BoxesRunTime.boxToLong(
                                        ThreadLocalRandom.current().nextLong() ), Standard.LATEST_STORE_VERSION);
                                var9 = new Some( optsx );
                            }
                            else
                            {
                                if ( !(var14 instanceof Failure) )
                                {
                                    throw new MatchError( var14 );
                                }

                                var9 = scala.None..MODULE$;
                            }

                            String[] virtualKeys = (String[]) ((Object[]) (new String[]{"name", "status", "default", "uuid"}));
                            AnyValue[] virtualValues = (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( dbName ),
                                    org.neo4j.cypher.internal.DatabaseStatus..MODULE$.Online(), Values.
                            booleanValue( var12 ), Values.stringValue( UUID.randomUUID().toString() )}));
                            String clusterProperties = (new StringOps( scala.Predef..MODULE$.augmentString(
                                    "\n          |  , // needed since it might be empty string instead\n          |  d.initial_members = $initialMembers,\n          |  d.store_creation_time = $creationTime,\n          |  d.store_random_id = $randomId,\n          |  d.store_version = $storeVersion " ))).
                            stripMargin();
                            Tuple2 var7;
                            if ( var9 instanceof Some )
                            {
                                Some var25 = (Some) var9;
                                Tuple4 opts = (Tuple4) var25.value();
                                if ( opts == null )
                                {
                                    throw new MatchError( opts );
                                }

                                String[] initial = (String[]) opts._1();
                                long creation = BoxesRunTime.unboxToLong( opts._2() );
                                long randomId = BoxesRunTime.unboxToLong( opts._3() );
                                String storeVersion = (String) opts._4();
                                Tuple4 var8 = new Tuple4( initial, BoxesRunTime.boxToLong( creation ), BoxesRunTime.boxToLong( randomId ), storeVersion );
                                String[] initialx = (String[]) var8._1();
                                long creationx = BoxesRunTime.unboxToLong( var8._2() );
                                long randomIdx = BoxesRunTime.unboxToLong( var8._3() );
                                String storeVersionx = (String) var8._4();
                                var7 = new Tuple2( clusterProperties,
                                        VirtualValues.map( (String[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) virtualKeys )) ).$plus$plus(
                                                new ofRef( scala.Predef..MODULE$.refArrayOps(
                                                        (Object[]) ((Object[]) (new String[]{"initialMembers", "creationTime", "randomId",
                                                                "storeVersion"})) ) ), scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply(
                                        String.class ))),(AnyValue[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) virtualValues ))).$plus$plus(
                                    new ofRef( scala.Predef..MODULE$.refArrayOps(
                                            (Object[]) ((Object[]) (new Value[]{Values.stringArray( initialx ), Values.longValue( creationx ),
                                                    Values.longValue( randomIdx ), Values.stringValue( storeVersionx )})) ) ), scala.Array..
                                MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( AnyValue.class )))));
                            }
                            else
                            {
                                if ( !scala.None..MODULE$.equals( var9 )){
                                throw new MatchError( var9 );
                            }

                                var7 = new Tuple2( "", VirtualValues.map( virtualKeys, virtualValues ) );
                            }

                            if ( var7 != null )
                            {
                                String queryAdditionsx = (String) var7._1();
                                MapValue virtualMapx = (MapValue) var7._2();
                                Tuple2 var6 = new Tuple2( queryAdditionsx, virtualMapx );
                                String queryAdditions = (String) var6._1();
                                MapValue virtualMap = (MapValue) var6._2();
                                return new UpdatingSystemCommandExecutionPlan( "CreateDatabase", this.$outer.normalExecutionEngine(),
                                        (new StringOps( scala.Predef..MODULE$.augmentString( (new StringBuilder( 276 )).append(
                                                "CREATE (d:Database {name: $name})\n          |SET\n          |  d.status = $status,\n          |  d.default = $default,\n          |  d.created_at = datetime(),\n          |  d.uuid = $uuid\n          |  " ).append(
                                                queryAdditions ).append(
                                                "\n          |RETURN d.name as name, d.status as status, d.uuid as uuid\n        " ).toString() )) ).stripMargin(),
                                virtualMap, org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleError( ( error ) -> {
                                Tuple2 var4 = new Tuple2( error, error.getCause() );
                                Object var3;
                                if ( var4 != null && var4._2() instanceof UniquePropertyValueValidationException )
                                {
                                    var3 = new DatabaseExistsException(
                                            (new StringBuilder( 68 )).append( "Failed to create the specified database '" ).append( dbName ).append(
                                                    "': Database already exists." ).toString(), error );
                                }
                                else
                                {
                                    label40:
                                    {
                                        if ( var4 != null )
                                        {
                                            Throwable e = (Throwable) var4._1();
                                            if ( e instanceof HasStatus )
                                            {
                                                Status var10000 = ((HasStatus) e).status();
                                                Cluster var7 = Cluster.NotALeader;
                                                if ( var10000 == null )
                                                {
                                                    if ( var7 == null )
                                                    {
                                                        break label40;
                                                    }
                                                }
                                                else if ( var10000.equals( var7 ) )
                                                {
                                                    break label40;
                                                }
                                            }
                                        }

                                        var3 = new IllegalStateException(
                                                (new StringBuilder( 43 )).append( "Failed to create the specified database '" ).append( dbName ).append(
                                                        "'." ).toString(), error );
                                        return (Throwable) var3;
                                    }

                                    var3 = new DatabaseAdministrationOnFollowerException(
                                            (new StringBuilder( 44 )).append( "Failed to create the specified database '" ).append( dbName ).append(
                                                    "': " ).append( this.$outer.followerError() ).toString(), error );
                                }

                                return (Throwable) var3;
                            } ), sourcexxxxxxxxxxxxxxxxxxxxxxx.map( ( x$38 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$38, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                            }
                            else
                            {
                                throw new MatchError( var7 );
                            }
                        };
                    }
                    else if ( x1 instanceof EnsureValidNumberOfDatabases )
                    {
                        EnsureValidNumberOfDatabases var150 = (EnsureValidNumberOfDatabases) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxx = var150.source();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbName = ((CreateDatabase) sourcexxxxxxxxxxxxxxxxxxxxxxxxx.get()).normalizedName().name();
                            String query = (new StringOps( scala.Predef..MODULE$.augmentString(
                                    "MATCH (d:Database)\n          |RETURN count(d) as numberOfDatabases\n        " ))).stripMargin();
                            return new UpdatingSystemCommandExecutionPlan( "EnsureValidNumberOfDatabases", this.$outer.normalExecutionEngine(), query,
                                    VirtualValues.EMPTY_MAP, org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleResult(
                                    ( x$39, numberOfDatabases ) -> {
                                        return $anonfun$applyOrElse$114( this, dbName, BoxesRunTime.unboxToInt( x$39 ), numberOfDatabases );
                                    } ),sourcexxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$40 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$40, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof DropDatabase )
                    {
                        DropDatabase var152 = (DropDatabase) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxx = var152.source();
                        NormalizedDatabaseName normalizedNamex = var152.normalizedName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbName = normalizedNamex.name();
                            return new UpdatingSystemCommandExecutionPlan( "DropDatabase", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "MATCH (d:Database {name: $name})\n          |REMOVE d:Database\n          |SET d:DeletedDatabase\n          |SET d.deleted_at = datetime()\n          |RETURN d.name as name, d.status as status" )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"name"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( dbName )})) ), org.neo4j.cypher.internal.procs.QueryHandler..
                            MODULE$.handleError( ( x0$8 ) -> {
                                Object var3;
                                label27:
                                {
                                    if ( x0$8 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$8).status();
                                        Cluster var6 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var6 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var6 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var3 = new IllegalStateException(
                                            (new StringBuilder( 43 )).append( "Failed to delete the specified database '" ).append( dbName ).append(
                                                    "'." ).toString(), x0$8 );
                                    return (Throwable) var3;
                                }

                                var3 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 44 )).append( "Failed to delete the specified database '" ).append( dbName ).append( "': " ).append(
                                                this.$outer.followerError() ).toString(), x0$8 );
                                return (Throwable) var3;
                            } ), sourcexxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$41 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$41, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof StartDatabase )
                    {
                        StartDatabase var155 = (StartDatabase) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxx = var155.source();
                        NormalizedDatabaseName normalizedNamexx = var155.normalizedName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbName = normalizedNamexx.name();
                            return new UpdatingSystemCommandExecutionPlan( "StartDatabase", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "OPTIONAL MATCH (d:Database {name: $name})\n          |OPTIONAL MATCH (d2:Database {name: $name, status: $oldStatus})\n          |SET d2.status = $status\n          |SET d2.started_at = datetime()\n          |RETURN d2.name as name, d2.status as status, d.name as db" )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"name", "oldStatus", "status"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( dbName ),
                                            org.neo4j.cypher.internal.DatabaseStatus..MODULE$.Offline(),
                                    org.neo4j.cypher.internal.DatabaseStatus..MODULE$.Online()}))),org.neo4j.cypher.internal.procs.QueryHandler..
                            MODULE$.handleResult( ( offset, value ) -> {
                                return $anonfun$applyOrElse$122( dbName, BoxesRunTime.unboxToInt( offset ), value );
                            } ).handleError( ( x0$9 ) -> {
                                Object var3;
                                label27:
                                {
                                    if ( x0$9 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$9).status();
                                        Cluster var6 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var6 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var6 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var3 = new IllegalStateException(
                                            (new StringBuilder( 42 )).append( "Failed to start the specified database '" ).append( dbName ).append(
                                                    "'." ).toString(), x0$9 );
                                    return (Throwable) var3;
                                }

                                var3 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 43 )).append( "Failed to start the specified database '" ).append( dbName ).append( "': " ).append(
                                                this.$outer.followerError() ).toString(), x0$9 );
                                return (Throwable) var3;
                            } ), sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$42 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$42, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof StopDatabase )
                    {
                        StopDatabase var158 = (StopDatabase) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxx = var158.source();
                        NormalizedDatabaseName normalizedNamexxx = var158.normalizedName();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbName = normalizedNamexxx.name();
                            return new UpdatingSystemCommandExecutionPlan( "StopDatabase", this.$outer.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString(
                                            "OPTIONAL MATCH (d2:Database {name: $name, status: $oldStatus})\n          |SET d2.status = $status\n          |SET d2.stopped_at = datetime()\n          |RETURN d2.name as name, d2.status as status" )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"name", "oldStatus", "status"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{Values.stringValue( dbName ),
                                            org.neo4j.cypher.internal.DatabaseStatus..MODULE$.Online(),
                                    org.neo4j.cypher.internal.DatabaseStatus..MODULE$.Offline()}))),org.neo4j.cypher.internal.procs.QueryHandler..
                            MODULE$.handleError( ( x0$10 ) -> {
                                Object var3;
                                label27:
                                {
                                    if ( x0$10 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$10).status();
                                        Cluster var6 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var6 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var6 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var3 = new IllegalStateException(
                                            (new StringBuilder( 41 )).append( "Failed to stop the specified database '" ).append( dbName ).append(
                                                    "'." ).toString(), x0$10 );
                                    return (Throwable) var3;
                                }

                                var3 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 42 )).append( "Failed to stop the specified database '" ).append( dbName ).append( "': " ).append(
                                                this.$outer.followerError() ).toString(), x0$10 );
                                return (Throwable) var3;
                            } ), sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$43 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$43, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        };
                    }
                    else if ( x1 instanceof EnsureValidNonSystemDatabase )
                    {
                        EnsureValidNonSystemDatabase var161 = (EnsureValidNonSystemDatabase) x1;
                        Option sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx = var161.source();
                        NormalizedDatabaseName normalizedNamexxxx = var161.normalizedName();
                        String actionxxxxxx = var161.action();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            String dbName = normalizedNamexxxx.name();
                            if ( dbName.equals( "system" ) )
                            {
                                throw new DatabaseAdministrationException(
                                        (new StringBuilder( 32 )).append( "Not allowed to " ).append( actionxxxxxx ).append( " system database." ).toString() );
                            }
                            else
                            {
                                return new UpdatingSystemCommandExecutionPlan( "EnsureValidNonSystemDatabase", this.$outer.normalExecutionEngine(),
                                        (new StringOps( scala.Predef..MODULE$.augmentString(
                                                "MATCH (db:Database {name: $name})\n          |RETURN db.name AS name" )) ).stripMargin(),
                                VirtualValues.map( (String[]) ((Object[]) (new String[]{"name"})), (AnyValue[]) ((Object[]) (new AnyValue[]{
                                        Values.stringValue( dbName )})) ), org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleNoResult( () -> {
                                return new Some( new DatabaseNotFoundException(
                                        (new StringBuilder( 62 )).append( "Failed to " ).append( actionxxxxxx ).append( " the specified database '" ).append(
                                                dbName ).append( "': Database does not exist." ).toString() ) );
                            } ).handleError( ( x0$11 ) -> {
                                Object var4;
                                label27:
                                {
                                    if ( x0$11 instanceof HasStatus )
                                    {
                                        Status var10000 = ((HasStatus) x0$11).status();
                                        Cluster var7 = Cluster.NotALeader;
                                        if ( var10000 == null )
                                        {
                                            if ( var7 == null )
                                            {
                                                break label27;
                                            }
                                        }
                                        else if ( var10000.equals( var7 ) )
                                        {
                                            break label27;
                                        }
                                    }

                                    var4 = new IllegalStateException( (new StringBuilder( 37 )).append( "Failed to " ).append( actionxxxxxx ).append(
                                            " the specified database '" ).append( dbName ).append( "'." ).toString(), x0$11 );
                                    return (Throwable) var4;
                                }

                                var4 = new DatabaseAdministrationOnFollowerException(
                                        (new StringBuilder( 38 )).append( "Failed to " ).append( actionxxxxxx ).append( " the specified database '" ).append(
                                                dbName ).append( "': " ).append( this.$outer.followerError() ).toString(), x0$11 );
                                return (Throwable) var4;
                            } ), sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.map( ( x$44 ) -> {
                                return (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                        x$44, ( unknownPlan ) -> {
                                            return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                    unknownPlan );
                                        } )).apply( context, parameterMapping, securityContext );
                            } ), org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                            }
                        };
                    }
                    else if ( x1 instanceof LogSystemCommand )
                    {
                        LogSystemCommand var165 = (LogSystemCommand) x1;
                        MultiDatabaseLogicalPlan sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx = var165.source();
                        String command = var165.command();
                        var3 = ( context, parameterMapping, securityContext ) -> {
                            return new LoggingSystemCommandExecutionPlan(
                                    (ExecutionPlan) ((Function3) this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().applyOrElse(
                                            sourcexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, ( unknownPlan ) -> {
                                                return this.$outer.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$throwCantCompile(
                                                        unknownPlan );
                                            } )).apply( context, parameterMapping, securityContext ), command, securityContext,
                                    ( message, securityContextx ) -> {
                                        $anonfun$applyOrElse$137( this, message, securityContextx );
                                        return BoxedUnit.UNIT;
                                    } );
                        };
                    }
                    else
                    {
                        var3 = var2.apply( x1 );
                    }
                }

                return var3;
            }

            public final boolean isDefinedAt( final LogicalPlan x1 )
            {
                boolean var3 = false;
                AlterUser var4 = null;
                boolean var2;
                if ( x1 instanceof CheckFrozenRole )
                {
                    var2 = true;
                }
                else if ( x1 instanceof ShowUsers )
                {
                    var2 = true;
                }
                else
                {
                    if ( x1 instanceof CreateUser )
                    {
                        CreateUser var6 = (CreateUser) x1;
                        Option var7 = var6.initialStringPassword();
                        Option var8 = var6.initialParameterPassword();
                        if ( var7 instanceof Some && scala.None..MODULE$.equals( var8 )){
                        var2 = true;
                        return var2;
                    }
                    }

                    if ( x1 instanceof AlterUser )
                    {
                        var3 = true;
                        var4 = (AlterUser) x1;
                        Option var9 = var4.initialParameterPassword();
                        if ( scala.None..MODULE$.equals( var9 )){
                        var2 = true;
                        return var2;
                    }
                    }

                    if ( var3 )
                    {
                        Option var10 = var4.initialParameterPassword();
                        if ( var10 instanceof Some )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( x1 instanceof ShowRoles )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof CreateRole )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof RequireRole )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof CopyRolePrivileges )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof DropRole )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof GrantRoleToUser )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof RevokeRoleFromUser )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof GrantDbmsAction )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof DenyDbmsAction )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof RevokeDbmsAction )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof AssertValidRevoke )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof GrantDatabaseAction )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof DenyDatabaseAction )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof RevokeDatabaseAction )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof GrantTraverse )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof DenyTraverse )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof RevokeTraverse )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof GrantRead )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof DenyRead )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof RevokeRead )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof GrantWrite )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof DenyWrite )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof RevokeWrite )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof ShowPrivileges )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof CreateDatabase )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof EnsureValidNumberOfDatabases )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof DropDatabase )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof StartDatabase )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof StopDatabase )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof EnsureValidNonSystemDatabase )
                    {
                        var2 = true;
                    }
                    else if ( x1 instanceof LogSystemCommand )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }
                }

                return var2;
            }
        };
    }

    private Tuple3<Value,Value,String> getResourcePart( final ActionResource resource, final String startOfErrorMessage, final String grantName,
            final String matchOrMerge )
    {
        Tuple3 var5;
        if ( resource instanceof DatabaseResource )
        {
            var5 = new Tuple3( Values.NO_VALUE, Values.stringValue( Resource.Type.DATABASE.toString() ),
                    (new StringBuilder( 33 )).append( matchOrMerge ).append( " (res:Resource {type: $resource})" ).toString() );
        }
        else if ( resource instanceof PropertyResource )
        {
            PropertyResource var7 = (PropertyResource) resource;
            String name = var7.property();
            var5 = new Tuple3( Values.stringValue( name ), Values.stringValue( Resource.Type.PROPERTY.toString() ),
                    (new StringBuilder( 50 )).append( matchOrMerge ).append( " (res:Resource {type: $resource, arg1: $property})" ).toString() );
        }
        else if ( resource instanceof NoResource )
        {
            var5 = new Tuple3( Values.NO_VALUE, Values.stringValue( Resource.Type.GRAPH.toString() ),
                    (new StringBuilder( 33 )).append( matchOrMerge ).append( " (res:Resource {type: $resource})" ).toString() );
        }
        else
        {
            if ( !(resource instanceof AllResource) )
            {
                throw new IllegalStateException(
                        (new StringBuilder( 35 )).append( startOfErrorMessage ).append( ": Invalid privilege " ).append( grantName ).append(
                                " resource type " ).append( resource ).toString() );
            }

            var5 = new Tuple3( Values.NO_VALUE, Values.stringValue( Resource.Type.ALL_PROPERTIES.toString() ),
                    (new StringBuilder( 33 )).append( matchOrMerge ).append( " (res:Resource {type: $resource})" ).toString() );
        }

        return var5;
    }

    private Tuple2<Value,String> getQualifierPart( final PrivilegeQualifier qualifier, final String startOfErrorMessage, final String grantName,
            final String matchOrMerge )
    {
        Tuple2 var5;
        if ( qualifier instanceof AllQualifier )
        {
            var5 = new Tuple2( Values.NO_VALUE,
                    (new StringBuilder( 52 )).append( matchOrMerge ).append( " (q:DatabaseQualifier {type: 'database', label: ''})" ).toString() );
        }
        else if ( qualifier instanceof LabelQualifier )
        {
            LabelQualifier var7 = (LabelQualifier) qualifier;
            String name = var7.label();
            var5 = new Tuple2( Values.stringValue( name ),
                    (new StringBuilder( 49 )).append( matchOrMerge ).append( " (q:LabelQualifier {type: 'node', label: $label})" ).toString() );
        }
        else if ( qualifier instanceof LabelAllQualifier )
        {
            var5 = new Tuple2( Values.NO_VALUE,
                    (new StringBuilder( 49 )).append( matchOrMerge ).append( " (q:LabelQualifierAll {type: 'node', label: '*'})" ).toString() );
        }
        else if ( qualifier instanceof RelationshipQualifier )
        {
            RelationshipQualifier var9 = (RelationshipQualifier) qualifier;
            String name = var9.reltype();
            var5 = new Tuple2( Values.stringValue( name ),
                    (new StringBuilder( 64 )).append( matchOrMerge ).append( " (q:RelationshipQualifier {type: 'relationship', label: $label})" ).toString() );
        }
        else
        {
            if ( !(qualifier instanceof RelationshipAllQualifier) )
            {
                throw new IllegalStateException(
                        (new StringBuilder( 31 )).append( startOfErrorMessage ).append( ": Invalid privilege " ).append( grantName ).append(
                                " qualifier " ).append( qualifier ).toString() );
            }

            var5 = new Tuple2( Values.NO_VALUE,
                    (new StringBuilder( 64 )).append( matchOrMerge ).append( " (q:RelationshipQualifierAll {type: 'relationship', label: '*'})" ).toString() );
        }

        return var5;
    }

    public UpdatingSystemCommandExecutionPlan org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeGrantOrDenyExecutionPlan(
            final String actionName, final ActionResource resource, final GraphScope database, final PrivilegeQualifier qualifier, final String roleName,
            final Option<ExecutionPlan> source, final ResourcePrivilege.GrantOrDeny grant, final String startOfErrorMessage )
    {
        String commandName = grant.isGrant() ? "GrantPrivilege" : "DenyPrivilege";
        TextValue action = Values.stringValue( actionName );
        TextValue role = Values.stringValue( roleName );
        Tuple3 var17 = this.getResourcePart( resource, startOfErrorMessage, grant.name, "MERGE" );
        if ( var17 != null )
        {
            Value property = (Value) var17._1();
            Value resourceType = (Value) var17._2();
            String resourceMerge = (String) var17._3();
            if ( property != null && resourceType != null && resourceMerge != null )
            {
                Tuple3 var12 = new Tuple3( property, resourceType, resourceMerge );
                Value property = (Value) var12._1();
                Value resourceType = (Value) var12._2();
                String resourceMerge = (String) var12._3();
                Tuple2 var28 = this.getQualifierPart( qualifier, startOfErrorMessage, grant.name, "MERGE" );
                if ( var28 != null )
                {
                    Value label = (Value) var28._1();
                    String qualifierMerge = (String) var28._2();
                    if ( label != null && qualifierMerge != null )
                    {
                        Tuple2 var11 = new Tuple2( label, qualifierMerge );
                        Value label = (Value) var11._1();
                        String qualifierMerge = (String) var11._2();
                        Tuple4 var10;
                        if ( database instanceof NamedGraphScope )
                        {
                            NamedGraphScope var38 = (NamedGraphScope) database;
                            String name = var38.database();
                            var10 = new Tuple4( Values.stringValue( name ), name, "MATCH (d:Database {name: $database})",
                                    "MERGE (d)<-[:FOR]-(s:Segment)-[:QUALIFIED]->(q)" );
                        }
                        else
                        {
                            if ( !(database instanceof AllGraphsScope) )
                            {
                                throw new IllegalStateException(
                                        (new StringBuilder( 36 )).append( startOfErrorMessage ).append( ": Invalid privilege " ).append( grant.name ).append(
                                                " scope database " ).append( database ).toString() );
                            }

                            var10 = new Tuple4( Values.NO_VALUE, "*", "MERGE (d:DatabaseAll {name: '*'})", "MERGE (d)<-[:FOR]-(s:Segment)-[:QUALIFIED]->(q)" );
                        }

                        if ( var10 != null )
                        {
                            Value dbName = (Value) var10._1();
                            String db = (String) var10._2();
                            String databaseMerge = (String) var10._3();
                            String scopeMerge = (String) var10._4();
                            Tuple4 var9 = new Tuple4( dbName, db, databaseMerge, scopeMerge );
                            Value dbName = (Value) var9._1();
                            String db = (String) var9._2();
                            String databaseMerge = (String) var9._3();
                            String scopeMerge = (String) var9._4();
                            return new UpdatingSystemCommandExecutionPlan( commandName, this.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString( (new StringBuilder( 952 )).append(
                                            "\n         |// Find or create the segment scope qualifier (eg. label qualifier, or all labels)\n         |" ).append(
                                            qualifierMerge ).append(
                                            "\n         |WITH q\n         |\n         |// Find the specified database, or find/create the special DatabaseAll node for '*'\n         |" ).append(
                                            databaseMerge ).append(
                                            "\n         |WITH q, d\n         |\n         |// Create a new scope connecting the database to the qualifier using a :Segment node\n         |" ).append(
                                            scopeMerge ).append(
                                            "\n         |\n         |// Find or create the appropriate resource type (eg. 'graph') and then connect it to the scope through a :Privilege\n         |" ).append(
                                            resourceMerge ).append(
                                            "\n         |MERGE (res)<-[:APPLIES_TO]-(p:Privilege {action: $action})-[:SCOPE]->(s)\n         |WITH q, d, p\n         |\n         |// Connect the role to the action to complete the privilege assignment\n         |OPTIONAL MATCH (r:Role {name: $role})\n         |MERGE (r)-[:" ).append(
                                            grant.relType ).append( "]->(p)\n         |\n         |// Return the table of results\n         |RETURN '" ).append(
                                            grant.prefix ).append(
                                            "' AS grant, p.action AS action, d.name AS database, q.label AS label, r.name AS role" ).toString() )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"action", "resource", "property", "database", "label", "role"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{action, resourceType, property, dbName, label,
                                            role})) ), org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleNoResult( () -> {
                            return new Some( new DatabaseNotFoundException(
                                    (new StringBuilder( 29 )).append( startOfErrorMessage ).append( ": Database '" ).append( db ).append(
                                            "' does not exist." ).toString() ) );
                        } ).handleError( ( x0$12 ) -> {
                            Object var4;
                            if ( x0$12 instanceof InternalException )
                            {
                                InternalException var6 = (InternalException) x0$12;
                                if ( var6.getMessage().contains( "ignore rows where a relationship node is missing" ) )
                                {
                                    var4 = new InvalidArgumentsException(
                                            (new StringBuilder( 25 )).append( startOfErrorMessage ).append( ": Role '" ).append( roleName ).append(
                                                    "' does not exist." ).toString(), var6 );
                                    return (Throwable) var4;
                                }
                            }

                            label40:
                            {
                                if ( x0$12 instanceof HasStatus )
                                {
                                    Status var10000 = ((HasStatus) x0$12).status();
                                    Cluster var8 = Cluster.NotALeader;
                                    if ( var10000 == null )
                                    {
                                        if ( var8 == null )
                                        {
                                            break label40;
                                        }
                                    }
                                    else if ( var10000.equals( var8 ) )
                                    {
                                        break label40;
                                    }
                                }

                                var4 = new IllegalStateException( (new StringBuilder( 1 )).append( startOfErrorMessage ).append( "." ).toString(), x0$12 );
                                return (Throwable) var4;
                            }

                            var4 = new DatabaseAdministrationOnFollowerException(
                                    (new StringBuilder( 2 )).append( startOfErrorMessage ).append( ": " ).append( this.followerError() ).toString(), x0$12 );
                            return (Throwable) var4;
                        } ), source, org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        }

                        throw new MatchError( var10 );
                    }
                }

                throw new MatchError( var28 );
            }
        }

        throw new MatchError( var17 );
    }

    public UpdatingSystemCommandExecutionPlan org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$makeRevokeExecutionPlan(
            final String actionName, final ActionResource resource, final GraphScope database, final PrivilegeQualifier qualifier, final String roleName,
            final String revokeType, final Option<ExecutionPlan> source, final String startOfErrorMessage )
    {
        TextValue action = Values.stringValue( actionName );
        TextValue role = Values.stringValue( roleName );
        Tuple3 var16 = this.getResourcePart( resource, startOfErrorMessage, "revoke", "MATCH" );
        if ( var16 != null )
        {
            Value property = (Value) var16._1();
            Value resourceType = (Value) var16._2();
            String resourceMatch = (String) var16._3();
            if ( property != null && resourceType != null && resourceMatch != null )
            {
                Tuple3 var12 = new Tuple3( property, resourceType, resourceMatch );
                Value property = (Value) var12._1();
                Value resourceType = (Value) var12._2();
                String resourceMatch = (String) var12._3();
                Tuple2 var27 = this.getQualifierPart( qualifier, startOfErrorMessage, "revoke", "MATCH" );
                if ( var27 != null )
                {
                    Value label = (Value) var27._1();
                    String qualifierMatch = (String) var27._2();
                    if ( label != null && qualifierMatch != null )
                    {
                        Tuple2 var11 = new Tuple2( label, qualifierMatch );
                        Value label = (Value) var11._1();
                        String qualifierMatch = (String) var11._2();
                        Tuple3 var10;
                        if ( database instanceof NamedGraphScope )
                        {
                            NamedGraphScope var37 = (NamedGraphScope) database;
                            String name = var37.database();
                            var10 = new Tuple3( Values.stringValue( name ), name,
                                    "MATCH (d:Database {name: $database})<-[:FOR]-(s:Segment)-[:QUALIFIED]->(q)" );
                        }
                        else
                        {
                            if ( !(database instanceof AllGraphsScope) )
                            {
                                throw new IllegalStateException(
                                        (new StringBuilder( 42 )).append( startOfErrorMessage ).append( ": Invalid privilege revoke scope database " ).append(
                                                database ).toString() );
                            }

                            var10 = new Tuple3( Values.NO_VALUE, "*", "MATCH (d:DatabaseAll {name: '*'})<-[:FOR]-(s:Segment)-[:QUALIFIED]->(q)" );
                        }

                        if ( var10 != null )
                        {
                            Value dbName = (Value) var10._1();
                            String scopeMatch = (String) var10._3();
                            Tuple2 var9 = new Tuple2( dbName, scopeMatch );
                            Value dbName = (Value) var9._1();
                            String scopeMatch = (String) var9._2();
                            return new UpdatingSystemCommandExecutionPlan( "RevokePrivilege", this.normalExecutionEngine(),
                                    (new StringOps( scala.Predef..MODULE$.augmentString( (new StringBuilder( 667 )).append(
                                            "\n         |// Find the segment scope qualifier (eg. label qualifier, or all labels)\n         |" ).append(
                                            qualifierMatch ).append(
                                            "\n         |\n         |// Find the segment connecting the database to the qualifier\n         |" ).append(
                                            scopeMatch ).append(
                                            "\n         |\n         |// Find the action connecting the resource and segment\n         |" ).append(
                                            resourceMatch ).append(
                                            "\n         |MATCH (res)<-[:APPLIES_TO]-(p:Privilege {action: $action})-[:SCOPE]->(s)\n         |\n         |// Find the privilege assignment connecting the role to the action\n         |OPTIONAL MATCH (r:Role {name: $role})\n         |WITH p, r, d, q\n         |OPTIONAL MATCH (r)-[g:" ).append(
                                            revokeType ).append(
                                            "]->(p)\n         |\n         |// Remove the assignment\n         |DELETE g\n         |RETURN r.name AS role, g AS grant" ).toString() )) ).stripMargin(),
                            VirtualValues.map( (String[]) ((Object[]) (new String[]{"action", "resource", "property", "database", "label", "role"})),
                                    (AnyValue[]) ((Object[]) (new AnyValue[]{action, resourceType, property, dbName, label,
                                            role})) ), org.neo4j.cypher.internal.procs.QueryHandler..MODULE$.handleError( ( x0$6 ) -> {
                            Object var3;
                            label27:
                            {
                                if ( x0$6 instanceof HasStatus )
                                {
                                    Status var10000 = ((HasStatus) x0$6).status();
                                    Cluster var6 = Cluster.NotALeader;
                                    if ( var10000 == null )
                                    {
                                        if ( var6 == null )
                                        {
                                            break label27;
                                        }
                                    }
                                    else if ( var10000.equals( var6 ) )
                                    {
                                        break label27;
                                    }
                                }

                                var3 = new IllegalStateException( (new StringBuilder( 1 )).append( startOfErrorMessage ).append( "." ).toString(), x0$6 );
                                return (Throwable) var3;
                            }

                            var3 = new DatabaseAdministrationOnFollowerException(
                                    (new StringBuilder( 2 )).append( startOfErrorMessage ).append( ": " ).append( this.followerError() ).toString(), x0$6 );
                            return (Throwable) var3;
                        } ), source, org.neo4j.cypher.internal.procs.UpdatingSystemCommandExecutionPlan..MODULE$.apply$default$7());
                        }

                        throw new MatchError( var10 );
                    }
                }

                throw new MatchError( var27 );
            }
        }

        throw new MatchError( var16 );
    }

    public boolean isApplicableAdministrationCommand( final LogicalPlanState logicalPlanState )
    {
        return this.org$neo4j$cypher$internal$EnterpriseAdministrationCommandRuntime$$fullLogicalToExecutable().isDefinedAt(
                logicalPlanState.maybeLogicalPlan().get() );
    }

    public EnterpriseAdministrationCommandRuntime copy( final ExecutionEngine normalExecutionEngine, final DependencyResolver resolver )
    {
        return new EnterpriseAdministrationCommandRuntime( normalExecutionEngine, resolver );
    }

    public ExecutionEngine copy$default$1()
    {
        return this.normalExecutionEngine();
    }

    public DependencyResolver copy$default$2()
    {
        return this.resolver();
    }

    public String productPrefix()
    {
        return "EnterpriseAdministrationCommandRuntime";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.normalExecutionEngine();
            break;
        case 1:
            var10000 = this.resolver();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EnterpriseAdministrationCommandRuntime;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof EnterpriseAdministrationCommandRuntime )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            EnterpriseAdministrationCommandRuntime var4 = (EnterpriseAdministrationCommandRuntime) x$1;
                            ExecutionEngine var10000 = this.normalExecutionEngine();
                            ExecutionEngine var5 = var4.normalExecutionEngine();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            DependencyResolver var7 = this.resolver();
                            DependencyResolver var6 = var4.resolver();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
