package org.neo4j.cypher.internal.javacompat;

import org.neo4j.cypher.internal.CompilerFactory;
import org.neo4j.cypher.internal.CypherRuntimeConfiguration;
import org.neo4j.cypher.internal.EnterpriseCompilerFactory;
import org.neo4j.cypher.internal.compiler.CypherPlannerConfiguration;
import org.neo4j.kernel.impl.query.QueryEngineProvider.SPI;

public class EnterpriseCypherEngineProvider extends CommunityCypherEngineProvider
{
    protected int enginePriority()
    {
        return 1;
    }

    protected CompilerFactory makeCompilerFactory( GraphDatabaseCypherService queryService, SPI spi, CypherPlannerConfiguration plannerConfig,
            CypherRuntimeConfiguration runtimeConfig )
    {
        return new EnterpriseCompilerFactory( queryService, spi, plannerConfig, runtimeConfig );
    }
}
