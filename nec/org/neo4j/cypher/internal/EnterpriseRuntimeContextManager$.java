package org.neo4j.cypher.internal;

import org.neo4j.cypher.internal.executionplan.GeneratedQuery;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure;
import org.neo4j.logging.Log;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class EnterpriseRuntimeContextManager$
        extends AbstractFunction4<CodeStructure<GeneratedQuery>,Log,CypherRuntimeConfiguration,RuntimeEnvironment,EnterpriseRuntimeContextManager>
        implements Serializable
{
    public static EnterpriseRuntimeContextManager$ MODULE$;

    static
    {
        new EnterpriseRuntimeContextManager$();
    }

    private EnterpriseRuntimeContextManager$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "EnterpriseRuntimeContextManager";
    }

    public EnterpriseRuntimeContextManager apply( final CodeStructure<GeneratedQuery> codeStructure, final Log log, final CypherRuntimeConfiguration config,
            final RuntimeEnvironment runtimeEnvironment )
    {
        return new EnterpriseRuntimeContextManager( codeStructure, log, config, runtimeEnvironment );
    }

    public Option<Tuple4<CodeStructure<GeneratedQuery>,Log,CypherRuntimeConfiguration,RuntimeEnvironment>> unapply( final EnterpriseRuntimeContextManager x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.codeStructure(), x$0.log(), x$0.config(), x$0.runtimeEnvironment() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
