package org.neo4j.cypher.internal.runtime.compiled;

import java.util.Optional;

import org.neo4j.cypher.internal.executionplan.GeneratedQueryExecution;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryStatistics;
import org.neo4j.cypher.internal.runtime.ValuePopulation;
import org.neo4j.cypher.internal.runtime.QueryStatistics.;
import org.neo4j.cypher.result.NaiveQuerySubscription;
import org.neo4j.cypher.result.QueryProfile;
import org.neo4j.cypher.result.QueryResult.QueryResultVisitor;
import org.neo4j.cypher.result.RuntimeResult.ConsumptionState;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class CompiledExecutionResult extends NaiveQuerySubscription
{
    private final GeneratedQueryExecution compiledCode;
    private final QueryProfile queryProfile;
    private final boolean prePopulateResults;
    private final String[] fieldNames;
    private boolean resultRequested;

    public CompiledExecutionResult( final QueryContext context, final GeneratedQueryExecution compiledCode, final QueryProfile queryProfile,
            final boolean prePopulateResults, final QuerySubscriber subscriber, final String[] fieldNames )
    {
        super( subscriber );
        this.compiledCode = compiledCode;
        this.queryProfile = queryProfile;
        this.prePopulateResults = prePopulateResults;
        this.fieldNames = fieldNames;
        this.resultRequested = false;
    }

    public QueryProfile queryProfile()
    {
        return this.queryProfile;
    }

    public String[] fieldNames()
    {
        return this.fieldNames;
    }

    private boolean resultRequested()
    {
        return this.resultRequested;
    }

    private void resultRequested_$eq( final boolean x$1 )
    {
        this.resultRequested = x$1;
    }

    public <EX extends Exception> void accept( final QueryResultVisitor<EX> visitor )
    {
        if ( this.prePopulateResults )
        {
            this.compiledCode.accept( ( row ) -> {
                AnyValue[] fields = row.fields();

                for ( int i = 0; i < fields.length; ++i )
                {
                    ValuePopulation.populate( fields[i] );
                }

                return visitor.visit( row );
            } );
        }
        else
        {
            this.compiledCode.accept( visitor );
        }

        this.resultRequested_$eq( true );
    }

    public QueryStatistics queryStatistics()
    {
        return new QueryStatistics(.MODULE$.apply$default$1(), .MODULE$.apply$default$2(), .MODULE$.apply$default$3(), .MODULE$.apply$default$4(), .
        MODULE$.apply$default$5(), .MODULE$.apply$default$6(), .MODULE$.apply$default$7(), .MODULE$.apply$default$8(), .MODULE$.apply$default$9(), .
        MODULE$.apply$default$10(), .MODULE$.apply$default$11(), .MODULE$.apply$default$12(), .MODULE$.apply$default$13(), .MODULE$.apply$default$14(), .
        MODULE$.apply$default$15(), .MODULE$.apply$default$16(), .MODULE$.apply$default$17());
    }

    public Optional<Long> totalAllocatedMemory()
    {
        return Optional.empty();
    }

    public ConsumptionState consumptionState()
    {
        return !this.resultRequested() ? ConsumptionState.NOT_STARTED : ConsumptionState.EXHAUSTED;
    }

    public void close()
    {
    }
}
