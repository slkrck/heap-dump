package org.neo4j.cypher.internal.runtime.compiled;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class CompiledPlan$ extends AbstractFunction3<Object,Seq<String>,RunnablePlan,CompiledPlan> implements Serializable
{
    public static CompiledPlan$ MODULE$;

    static
    {
        new CompiledPlan$();
    }

    private CompiledPlan$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CompiledPlan";
    }

    public CompiledPlan apply( final boolean updating, final Seq<String> columns, final RunnablePlan executionResultBuilder )
    {
        return new CompiledPlan( updating, columns, executionResultBuilder );
    }

    public Option<Tuple3<Object,Seq<String>,RunnablePlan>> unapply( final CompiledPlan x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( BoxesRunTime.boxToBoolean( x$0.updating() ), x$0.columns(), x$0.executionResultBuilder() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
