package org.neo4j.cypher.internal.runtime.compiled;

import org.neo4j.cypher.internal.plandescription.Argument;
import org.neo4j.cypher.internal.profiling.ProfilingTracer;
import org.neo4j.cypher.internal.runtime.ExecutionMode;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.result.RuntimeResult;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.virtual.MapValue;
import scala.Option;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface RunnablePlan
{
    RuntimeResult apply( final QueryContext queryContext, final ExecutionMode execMode, final Option<ProfilingTracer> tracer, final MapValue params,
            final boolean prePopulateResults, final QuerySubscriber subscriber );

    Seq<Argument> metadata();
}
