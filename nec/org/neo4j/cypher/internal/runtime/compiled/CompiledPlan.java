package org.neo4j.cypher.internal.runtime.compiled;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CompiledPlan implements Product, Serializable
{
    private final boolean updating;
    private final Seq<String> columns;
    private final RunnablePlan executionResultBuilder;

    public CompiledPlan( final boolean updating, final Seq<String> columns, final RunnablePlan executionResultBuilder )
    {
        this.updating = updating;
        this.columns = columns;
        this.executionResultBuilder = executionResultBuilder;
        Product.$init$( this );
    }

    public static Option<Tuple3<Object,Seq<String>,RunnablePlan>> unapply( final CompiledPlan x$0 )
    {
        return CompiledPlan$.MODULE$.unapply( var0 );
    }

    public static CompiledPlan apply( final boolean updating, final Seq<String> columns, final RunnablePlan executionResultBuilder )
    {
        return CompiledPlan$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Object,Seq<String>,RunnablePlan>,CompiledPlan> tupled()
    {
        return CompiledPlan$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Seq<String>,Function1<RunnablePlan,CompiledPlan>>> curried()
    {
        return CompiledPlan$.MODULE$.curried();
    }

    public boolean updating()
    {
        return this.updating;
    }

    public Seq<String> columns()
    {
        return this.columns;
    }

    public RunnablePlan executionResultBuilder()
    {
        return this.executionResultBuilder;
    }

    public CompiledPlan copy( final boolean updating, final Seq<String> columns, final RunnablePlan executionResultBuilder )
    {
        return new CompiledPlan( updating, columns, executionResultBuilder );
    }

    public boolean copy$default$1()
    {
        return this.updating();
    }

    public Seq<String> copy$default$2()
    {
        return this.columns();
    }

    public RunnablePlan copy$default$3()
    {
        return this.executionResultBuilder();
    }

    public String productPrefix()
    {
        return "CompiledPlan";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToBoolean( this.updating() );
            break;
        case 1:
            var10000 = this.columns();
            break;
        case 2:
            var10000 = this.executionResultBuilder();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CompiledPlan;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.updating() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.columns() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.executionResultBuilder() ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof CompiledPlan )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        CompiledPlan var4 = (CompiledPlan) x$1;
                        if ( this.updating() == var4.updating() )
                        {
                            label56:
                            {
                                Seq var10000 = this.columns();
                                Seq var5 = var4.columns();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label56;
                                }

                                RunnablePlan var7 = this.executionResultBuilder();
                                RunnablePlan var6 = var4.executionResultBuilder();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label56;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label47;
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label65;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
