package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ScanAllNodes implements LoopDataGenerator, Product, Serializable
{
    private final String opName;

    public ScanAllNodes( final String opName )
    {
        this.opName = opName;
        Product.$init$( this );
    }

    public static Option<String> unapply( final ScanAllNodes x$0 )
    {
        return ScanAllNodes$.MODULE$.unapply( var0 );
    }

    public static ScanAllNodes apply( final String opName )
    {
        return ScanAllNodes$.MODULE$.apply( var0 );
    }

    public static <A> Function1<String,A> andThen( final Function1<ScanAllNodes,A> g )
    {
        return ScanAllNodes$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ScanAllNodes> compose( final Function1<A,String> g )
    {
        return ScanAllNodes$.MODULE$.compose( var0 );
    }

    public String opName()
    {
        return this.opName;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> void produceLoopData( final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.allNodesScan( cursorName );
        generator.incrementDbHits();
    }

    public <E> void getNext( final Variable nextVar, final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.incrementDbHits();
        generator.nodeFromNodeCursor( nextVar.name(), cursorName );
    }

    public <E> E checkNext( final MethodStructure<E> generator, final String cursorName )
    {
        return generator.advanceNodeCursor( cursorName );
    }

    public <E> void close( final String cursorName, final MethodStructure<E> generator )
    {
        generator.closeNodeCursor( cursorName );
    }

    public ScanAllNodes copy( final String opName )
    {
        return new ScanAllNodes( opName );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public String productPrefix()
    {
        return "ScanAllNodes";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.opName();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ScanAllNodes;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof ScanAllNodes )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        ScanAllNodes var4 = (ScanAllNodes) x$1;
                        String var10000 = this.opName();
                        String var5 = var4.opName();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
