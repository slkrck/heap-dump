package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CodeGenFunction1
{
    CodeGenExpression apply( final CodeGenExpression arg );
}
