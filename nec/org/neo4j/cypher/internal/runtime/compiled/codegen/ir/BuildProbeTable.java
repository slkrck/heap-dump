package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.Nothing.;

@JavaDocToJava
public interface BuildProbeTable extends Instruction
{
    static BuildProbeTable apply( final String id, final String name, final IndexedSeq<Variable> nodes, final Map<String,Variable> valueSymbols,
            final CodeGenContext context )
    {
        return BuildProbeTable$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    static void $init$( final BuildProbeTable $this )
    {
    }

    default <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.allocateProbeTable( this.name(), this.tableType() );
    }

    String name();

    JoinData joinData();

    JoinTableType tableType();

    default Seq<> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }
}
