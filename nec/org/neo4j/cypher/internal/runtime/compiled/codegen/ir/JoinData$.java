package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction4;

public final class JoinData$ extends AbstractFunction4<Map<String,VariableData>,String,JoinTableType,String,JoinData> implements Serializable
{
    public static JoinData$ MODULE$;

    static
    {
        new JoinData$();
    }

    private JoinData$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "JoinData";
    }

    public JoinData apply( final Map<String,VariableData> vars, final String tableVar, final JoinTableType tableType, final String id )
    {
        return new JoinData( vars, tableVar, tableType, id );
    }

    public Option<Tuple4<Map<String,VariableData>,String,JoinTableType,String>> unapply( final JoinData x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.vars(), x$0.tableVar(), x$0.tableType(), x$0.id() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
