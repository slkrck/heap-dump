package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction5;

public final class IndexSeek$ extends AbstractFunction5<String,String,Seq<String>,String,CodeGenExpression,IndexSeek> implements Serializable
{
    public static IndexSeek$ MODULE$;

    static
    {
        new IndexSeek$();
    }

    private IndexSeek$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "IndexSeek";
    }

    public IndexSeek apply( final String opName, final String labelName, final Seq<String> propNames, final String descriptorVar,
            final CodeGenExpression expression )
    {
        return new IndexSeek( opName, labelName, propNames, descriptorVar, expression );
    }

    public Option<Tuple5<String,String,Seq<String>,String,CodeGenExpression>> unapply( final IndexSeek x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple5( x$0.opName(), x$0.labelName(), x$0.propNames(), x$0.descriptorVar(), x$0.expression() ) ))
        ;
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
