package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class RelationshipExpression implements CodeGenExpression, Product, Serializable
{
    private final Variable relId;

    public RelationshipExpression( final Variable relId )
    {
        scala.Predef.var10000;
        boolean var3;
        label17:
        {
            label16:
            {
                this.relId = relId;
                super();
                CodeGenExpression.$init$( this );
                Product.$init$( this );
                var10000 = scala.Predef..MODULE$;
                CodeGenType var10001 = relId.codeGenType();
                CypherCodeGenType var2 = CodeGenType$.MODULE$.primitiveRel();
                if ( var10001 == null )
                {
                    if ( var2 == null )
                    {
                        break label16;
                    }
                }
                else if ( var10001.equals( var2 ) )
                {
                    break label16;
                }

                var3 = false;
                break label17;
            }

            var3 = true;
        }

        var10000. assert (var3);
    }

    public static Option<Variable> unapply( final RelationshipExpression x$0 )
    {
        return RelationshipExpression$.MODULE$.unapply( var0 );
    }

    public static RelationshipExpression apply( final Variable relId )
    {
        return RelationshipExpression$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Variable,A> andThen( final Function1<RelationshipExpression,A> g )
    {
        return RelationshipExpression$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,RelationshipExpression> compose( final Function1<A,Variable> g )
    {
        return RelationshipExpression$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable relId()
    {
        return this.relId;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.relationship( this.relId().name(), this.relId().codeGenType() );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.relId().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return CodeGenType$.MODULE$.primitiveRel();
    }

    public RelationshipExpression copy( final Variable relId )
    {
        return new RelationshipExpression( relId );
    }

    public Variable copy$default$1()
    {
        return this.relId();
    }

    public String productPrefix()
    {
        return "RelationshipExpression";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.relId();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipExpression;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof RelationshipExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        RelationshipExpression var4 = (RelationshipExpression) x$1;
                        Variable var10000 = this.relId();
                        Variable var5 = var4.relId();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
