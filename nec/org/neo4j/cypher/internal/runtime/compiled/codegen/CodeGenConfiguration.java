package org.neo4j.cypher.internal.runtime.compiled.codegen;

import java.nio.file.Path;

import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.collection.Iterator;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CodeGenConfiguration implements Product, Serializable
{
    private final CodeGenMode mode;
    private final boolean showSource;
    private final boolean showByteCode;
    private final Option<Path> saveSource;
    private final String packageName;

    public CodeGenConfiguration( final CodeGenMode mode, final boolean showSource, final boolean showByteCode, final Option<Path> saveSource,
            final String packageName )
    {
        this.mode = mode;
        this.showSource = showSource;
        this.showByteCode = showByteCode;
        this.saveSource = saveSource;
        this.packageName = packageName;
        Product.$init$( this );
    }

    public static String $lessinit$greater$default$5()
    {
        return CodeGenConfiguration$.MODULE$.$lessinit$greater$default$5();
    }

    public static Option<Path> $lessinit$greater$default$4()
    {
        return CodeGenConfiguration$.MODULE$.$lessinit$greater$default$4();
    }

    public static boolean $lessinit$greater$default$3()
    {
        return CodeGenConfiguration$.MODULE$.$lessinit$greater$default$3();
    }

    public static boolean $lessinit$greater$default$2()
    {
        return CodeGenConfiguration$.MODULE$.$lessinit$greater$default$2();
    }

    public static CodeGenMode $lessinit$greater$default$1()
    {
        return CodeGenConfiguration$.MODULE$.$lessinit$greater$default$1();
    }

    public static String apply$default$5()
    {
        return CodeGenConfiguration$.MODULE$.apply$default$5();
    }

    public static Option<Path> apply$default$4()
    {
        return CodeGenConfiguration$.MODULE$.apply$default$4();
    }

    public static boolean apply$default$3()
    {
        return CodeGenConfiguration$.MODULE$.apply$default$3();
    }

    public static boolean apply$default$2()
    {
        return CodeGenConfiguration$.MODULE$.apply$default$2();
    }

    public static CodeGenMode apply$default$1()
    {
        return CodeGenConfiguration$.MODULE$.apply$default$1();
    }

    public static Option<Tuple5<CodeGenMode,Object,Object,Option<Path>,String>> unapply( final CodeGenConfiguration x$0 )
    {
        return CodeGenConfiguration$.MODULE$.unapply( var0 );
    }

    public static CodeGenConfiguration apply( final CodeGenMode mode, final boolean showSource, final boolean showByteCode, final Option<Path> saveSource,
            final String packageName )
    {
        return CodeGenConfiguration$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public static CodeGenConfiguration apply( final Set<String> debugOptions )
    {
        return CodeGenConfiguration$.MODULE$.apply( var0 );
    }

    public CodeGenMode mode()
    {
        return this.mode;
    }

    public boolean showSource()
    {
        return this.showSource;
    }

    public boolean showByteCode()
    {
        return this.showByteCode;
    }

    public Option<Path> saveSource()
    {
        return this.saveSource;
    }

    public String packageName()
    {
        return this.packageName;
    }

    public CodeGenConfiguration copy( final CodeGenMode mode, final boolean showSource, final boolean showByteCode, final Option<Path> saveSource,
            final String packageName )
    {
        return new CodeGenConfiguration( mode, showSource, showByteCode, saveSource, packageName );
    }

    public CodeGenMode copy$default$1()
    {
        return this.mode();
    }

    public boolean copy$default$2()
    {
        return this.showSource();
    }

    public boolean copy$default$3()
    {
        return this.showByteCode();
    }

    public Option<Path> copy$default$4()
    {
        return this.saveSource();
    }

    public String copy$default$5()
    {
        return this.packageName();
    }

    public String productPrefix()
    {
        return "CodeGenConfiguration";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.mode();
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.showSource() );
            break;
        case 2:
            var10000 = BoxesRunTime.boxToBoolean( this.showByteCode() );
            break;
        case 3:
            var10000 = this.saveSource();
            break;
        case 4:
            var10000 = this.packageName();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CodeGenConfiguration;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.mode() ) );
        var1 = Statics.mix( var1, this.showSource() ? 1231 : 1237 );
        var1 = Statics.mix( var1, this.showByteCode() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.saveSource() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.packageName() ) );
        return Statics.finalizeHash( var1, 5 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label76:
            {
                boolean var2;
                if ( x$1 instanceof CodeGenConfiguration )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label58:
                    {
                        label57:
                        {
                            CodeGenConfiguration var4 = (CodeGenConfiguration) x$1;
                            CodeGenMode var10000 = this.mode();
                            CodeGenMode var5 = var4.mode();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label57;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label57;
                            }

                            if ( this.showSource() == var4.showSource() && this.showByteCode() == var4.showByteCode() )
                            {
                                label67:
                                {
                                    Option var8 = this.saveSource();
                                    Option var6 = var4.saveSource();
                                    if ( var8 == null )
                                    {
                                        if ( var6 != null )
                                        {
                                            break label67;
                                        }
                                    }
                                    else if ( !var8.equals( var6 ) )
                                    {
                                        break label67;
                                    }

                                    String var9 = this.packageName();
                                    String var7 = var4.packageName();
                                    if ( var9 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label67;
                                        }
                                    }
                                    else if ( !var9.equals( var7 ) )
                                    {
                                        break label67;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var10 = true;
                                        break label58;
                                    }
                                }
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label76;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
