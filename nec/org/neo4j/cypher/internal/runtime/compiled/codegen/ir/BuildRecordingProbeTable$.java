package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.collection.immutable.Map;

public final class BuildRecordingProbeTable$ implements Serializable
{
    public static BuildRecordingProbeTable$ MODULE$;

    static
    {
        new BuildRecordingProbeTable$();
    }

    private BuildRecordingProbeTable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "BuildRecordingProbeTable";
    }

    public BuildRecordingProbeTable apply( final String id, final String name, final IndexedSeq<Variable> nodes, final Map<String,Variable> valueSymbols,
            final CodeGenContext context )
    {
        return new BuildRecordingProbeTable( id, name, nodes, valueSymbols, context );
    }

    public Option<Tuple4<String,String,IndexedSeq<Variable>,Map<String,Variable>>> unapply( final BuildRecordingProbeTable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.id(), x$0.name(), x$0.nodes(), x$0.valueSymbols() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
