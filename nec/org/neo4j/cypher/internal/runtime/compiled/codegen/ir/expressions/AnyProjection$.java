package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class AnyProjection$ extends AbstractFunction1<Variable,AnyProjection> implements Serializable
{
    public static AnyProjection$ MODULE$;

    static
    {
        new AnyProjection$();
    }

    private AnyProjection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "AnyProjection";
    }

    public AnyProjection apply( final Variable variable )
    {
        return new AnyProjection( variable );
    }

    public Option<Variable> unapply( final AnyProjection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.variable() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
