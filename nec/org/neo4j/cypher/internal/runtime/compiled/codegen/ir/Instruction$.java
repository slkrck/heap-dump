package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.runtime.Nothing.;

public final class Instruction$
{
    public static Instruction$ MODULE$;

    static
    {
        new Instruction$();
    }

    private final Instruction empty;

    private Instruction$()
    {
        MODULE$ = this;
        this.empty = new Instruction()
        {
            public
            {
                Instruction.$init$( this );
            }

            public final Set<String> allOperatorIds()
            {
                return Instruction.allOperatorIds$( this );
            }

            public Set<String> operatorId()
            {
                return Instruction.operatorId$( this );
            }

            public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
            {
            }

            public Seq<> children()
            {
                return (Seq) scala.collection.Seq..MODULE$.empty();
            }

            public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
            {
            }
        };
    }

    public Instruction empty()
    {
        return this.empty;
    }
}
