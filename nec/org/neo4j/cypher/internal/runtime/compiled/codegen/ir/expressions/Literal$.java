package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class Literal$ extends AbstractFunction1<Object,Literal> implements Serializable
{
    public static Literal$ MODULE$;

    static
    {
        new Literal$();
    }

    private Literal$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Literal";
    }

    public Literal apply( final Object value )
    {
        return new Literal( value );
    }

    public Option<Object> unapply( final Literal x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.value() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
