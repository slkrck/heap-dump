package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class MapProperty$ extends AbstractFunction2<CodeGenExpression,String,MapProperty> implements Serializable
{
    public static MapProperty$ MODULE$;

    static
    {
        new MapProperty$();
    }

    private MapProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MapProperty";
    }

    public MapProperty apply( final CodeGenExpression mapExpression, final String propertyKeyName )
    {
        return new MapProperty( mapExpression, propertyKeyName );
    }

    public Option<Tuple2<CodeGenExpression,String>> unapply( final MapProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.mapExpression(), x$0.propertyKeyName() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
