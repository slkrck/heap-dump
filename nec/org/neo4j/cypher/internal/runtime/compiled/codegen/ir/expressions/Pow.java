package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Pow implements CodeGenExpression, BinaryOperator, Product, Serializable
{
    private final CodeGenExpression lhs;
    private final CodeGenExpression rhs;

    public Pow( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        this.lhs = lhs;
        this.rhs = rhs;
        CodeGenExpression.$init$( this );
        BinaryOperator.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Pow x$0 )
    {
        return Pow$.MODULE$.unapply( var0 );
    }

    public static Pow apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return Pow$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CodeGenExpression,CodeGenExpression>,Pow> tupled()
    {
        return Pow$.MODULE$.tupled();
    }

    public static Function1<CodeGenExpression,Function1<CodeGenExpression,Pow>> curried()
    {
        return Pow$.MODULE$.curried();
    }

    public final <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        BinaryOperator.init$( this, generator, context );
    }

    public final <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return BinaryOperator.generateExpression$( this, structure, context );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression lhs()
    {
        return this.lhs;
    }

    public CodeGenExpression rhs()
    {
        return this.rhs;
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.lhs().nullable( context ) || this.rhs().nullable( context );
    }

    public <E> Function2<E,E,E> generator( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return ( lhs, rhs ) -> {
            return structure.powExpression( lhs, rhs );
        };
    }

    public String name()
    {
        return "pow";
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        Tuple2 var3 = new Tuple2( this.lhs().codeGenType( context ).ct(), this.rhs().codeGenType( context ).ct() );
        CypherCodeGenType var2;
        if ( var3 != null )
        {
            CypherType var4 = (CypherType) var3._1();
            CypherType var5 = (CypherType) var3._2();
            Option var6 = Number$.MODULE$.unapply( var4 );
            if ( !var6.isEmpty() )
            {
                Option var7 = Number$.MODULE$.unapply( var5 );
                if ( !var7.isEmpty() )
                {
                    var2 = new CypherCodeGenType(.MODULE$.CTFloat(), ReferenceType$.MODULE$);
                    return var2;
                }
            }
        }

        var2 = CodeGenType$.MODULE$.Any();
        return var2;
    }

    public Pow copy( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Pow( lhs, rhs );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.lhs();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.rhs();
    }

    public String productPrefix()
    {
        return "Pow";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        CodeGenExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Pow;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof Pow )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            Pow var4 = (Pow) x$1;
                            CodeGenExpression var10000 = this.lhs();
                            CodeGenExpression var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.rhs();
                            CodeGenExpression var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
