package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class JoinTableMethod$ extends AbstractFunction2<String,JoinTableType,JoinTableMethod> implements Serializable
{
    public static JoinTableMethod$ MODULE$;

    static
    {
        new JoinTableMethod$();
    }

    private JoinTableMethod$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "JoinTableMethod";
    }

    public JoinTableMethod apply( final String name, final JoinTableType tableType )
    {
        return new JoinTableMethod( name, tableType );
    }

    public Option<Tuple2<String,JoinTableType>> unapply( final JoinTableMethod x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.name(), x$0.tableType() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
