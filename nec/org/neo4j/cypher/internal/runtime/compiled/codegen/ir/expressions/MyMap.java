package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.ListType.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class MyMap implements CodeGenExpression, Product, Serializable
{
    private final Map<String,CodeGenExpression> instructions;

    public MyMap( final Map<String,CodeGenExpression> instructions )
    {
        this.instructions = instructions;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Map<String,CodeGenExpression>> unapply( final MyMap x$0 )
    {
        return MyMap$.MODULE$.unapply( var0 );
    }

    public static MyMap apply( final Map<String,CodeGenExpression> instructions )
    {
        return MyMap$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Map<String,CodeGenExpression>,A> andThen( final Function1<MyMap,A> g )
    {
        return MyMap$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,MyMap> compose( final Function1<A,Map<String,CodeGenExpression>> g )
    {
        return MyMap$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Map<String,CodeGenExpression> instructions()
    {
        return this.instructions;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.instructions().values().foreach( ( instruction ) -> {
            $anonfun$init$1( generator, context, instruction );
            return BoxedUnit.UNIT;
        } );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.asMap( this.instructions().mapValues( ( e ) -> {
            CypherCodeGenType var4 = e.codeGenType( context );
            Object var3;
            if ( var4 != null )
            {
                CypherType var5 = var4.ct();
                RepresentationType var6 = var4.repr();
                Option var7 = .MODULE$.unapply( var5 );
                if ( !var7.isEmpty() && var6 instanceof ListReferenceType )
                {
                    ListReferenceType var8 = (ListReferenceType) var6;
                    RepresentationType innerRepr = var8.inner();
                    if ( RepresentationType$.MODULE$.isPrimitive( innerRepr ) )
                    {
                        var3 = structure.toMaterializedAnyValue( structure.iteratorFrom( e.generateExpression( structure, context ) ),
                                e.codeGenType( context ) );
                        return var3;
                    }
                }
            }

            var3 = structure.toMaterializedAnyValue( e.generateExpression( structure, context ), e.codeGenType( context ) );
            return var3;
        } ) );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return false;
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTMap(), AnyValueType$.MODULE$);
    }

    public MyMap copy( final Map<String,CodeGenExpression> instructions )
    {
        return new MyMap( instructions );
    }

    public Map<String,CodeGenExpression> copy$default$1()
    {
        return this.instructions();
    }

    public String productPrefix()
    {
        return "MyMap";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.instructions();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MyMap;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof MyMap )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        MyMap var4 = (MyMap) x$1;
                        Map var10000 = this.instructions();
                        Map var5 = var4.instructions();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
