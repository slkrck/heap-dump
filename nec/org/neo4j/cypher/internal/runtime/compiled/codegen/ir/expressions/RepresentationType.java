package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface RepresentationType
{
    static boolean isAnyValue( final RepresentationType repr )
    {
        return RepresentationType$.MODULE$.isAnyValue( var0 );
    }

    static boolean isValue( final RepresentationType repr )
    {
        return RepresentationType$.MODULE$.isValue( var0 );
    }

    static boolean isPrimitive( final RepresentationType repr )
    {
        return RepresentationType$.MODULE$.isPrimitive( var0 );
    }
}
