package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class GetMatchesFromProbeTable implements Instruction, Product, Serializable
{
    private final IndexedSeq<Variable> keys;
    private final JoinData code;
    private final Instruction action;

    public GetMatchesFromProbeTable( final IndexedSeq<Variable> keys, final JoinData code, final Instruction action )
    {
        this.keys = keys;
        this.code = code;
        this.action = action;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<IndexedSeq<Variable>,JoinData,Instruction>> unapply( final GetMatchesFromProbeTable x$0 )
    {
        return GetMatchesFromProbeTable$.MODULE$.unapply( var0 );
    }

    public static GetMatchesFromProbeTable apply( final IndexedSeq<Variable> keys, final JoinData code, final Instruction action )
    {
        return GetMatchesFromProbeTable$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<IndexedSeq<Variable>,JoinData,Instruction>,GetMatchesFromProbeTable> tupled()
    {
        return GetMatchesFromProbeTable$.MODULE$.tupled();
    }

    public static Function1<IndexedSeq<Variable>,Function1<JoinData,Function1<Instruction,GetMatchesFromProbeTable>>> curried()
    {
        return GetMatchesFromProbeTable$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public Set<String> operatorId()
    {
        return Instruction.operatorId$( this );
    }

    public IndexedSeq<Variable> keys()
    {
        return this.keys;
    }

    public JoinData code()
    {
        return this.code;
    }

    public Instruction action()
    {
        return this.action;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.code().id(), new Some( this.getClass().getSimpleName() ), ( traced ) -> {
            $anonfun$body$1( this, context, traced );
            return BoxedUnit.UNIT;
        } );
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.action().init( generator, context );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.action()}) ));
    }

    public GetMatchesFromProbeTable copy( final IndexedSeq<Variable> keys, final JoinData code, final Instruction action )
    {
        return new GetMatchesFromProbeTable( keys, code, action );
    }

    public IndexedSeq<Variable> copy$default$1()
    {
        return this.keys();
    }

    public JoinData copy$default$2()
    {
        return this.code();
    }

    public Instruction copy$default$3()
    {
        return this.action();
    }

    public String productPrefix()
    {
        return "GetMatchesFromProbeTable";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.keys();
            break;
        case 1:
            var10000 = this.code();
            break;
        case 2:
            var10000 = this.action();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof GetMatchesFromProbeTable;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof GetMatchesFromProbeTable )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            GetMatchesFromProbeTable var4 = (GetMatchesFromProbeTable) x$1;
                            IndexedSeq var10000 = this.keys();
                            IndexedSeq var5 = var4.keys();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            JoinData var8 = this.code();
                            JoinData var6 = var4.code();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            Instruction var9 = this.action();
                            Instruction var7 = var4.action();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
