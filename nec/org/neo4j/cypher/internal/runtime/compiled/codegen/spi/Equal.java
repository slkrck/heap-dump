package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class Equal
{
    public static String toString()
    {
        return Equal$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return Equal$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return Equal$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return Equal$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return Equal$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return Equal$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return Equal$.MODULE$.productPrefix();
    }
}
