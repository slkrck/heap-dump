package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class NodeExpression implements CodeGenExpression, Product, Serializable
{
    private final Variable nodeIdVar;

    public NodeExpression( final Variable nodeIdVar )
    {
        scala.Predef.var10000;
        boolean var4;
        label25:
        {
            label27:
            {
                this.nodeIdVar = nodeIdVar;
                super();
                CodeGenExpression.$init$( this );
                Product.$init$( this );
                var10000 = scala.Predef..MODULE$;
                CodeGenType var10001 = nodeIdVar.codeGenType();
                CypherCodeGenType var2 = CodeGenType$.MODULE$.primitiveNode();
                if ( var10001 == null )
                {
                    if ( var2 == null )
                    {
                        break label27;
                    }
                }
                else if ( var10001.equals( var2 ) )
                {
                    break label27;
                }

                var10001 = nodeIdVar.codeGenType();
                CypherCodeGenType var3 = CodeGenType$.MODULE$.Any();
                if ( var10001 == null )
                {
                    if ( var3 == null )
                    {
                        break label27;
                    }
                }
                else if ( var10001.equals( var3 ) )
                {
                    break label27;
                }

                var4 = false;
                break label25;
            }

            var4 = true;
        }

        var10000. assert (var4);
    }

    public static Option<Variable> unapply( final NodeExpression x$0 )
    {
        return NodeExpression$.MODULE$.unapply( var0 );
    }

    public static NodeExpression apply( final Variable nodeIdVar )
    {
        return NodeExpression$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Variable,A> andThen( final Function1<NodeExpression,A> g )
    {
        return NodeExpression$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,NodeExpression> compose( final Function1<A,Variable> g )
    {
        return NodeExpression$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable nodeIdVar()
    {
        return this.nodeIdVar;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.node( this.nodeIdVar().name(), this.nodeIdVar().codeGenType() );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.nodeIdVar().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return CodeGenType$.MODULE$.primitiveNode();
    }

    public NodeExpression copy( final Variable nodeIdVar )
    {
        return new NodeExpression( nodeIdVar );
    }

    public Variable copy$default$1()
    {
        return this.nodeIdVar();
    }

    public String productPrefix()
    {
        return "NodeExpression";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.nodeIdVar();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NodeExpression;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof NodeExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        NodeExpression var4 = (NodeExpression) x$1;
                        Variable var10000 = this.nodeIdVar();
                        Variable var5 = var4.nodeIdVar();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
