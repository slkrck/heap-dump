package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.exceptions.InternalException;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class AnyProjection implements CodeGenExpression, Product, Serializable
{
    private final Variable variable;

    public AnyProjection( final Variable variable )
    {
        this.variable = variable;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Variable> unapply( final AnyProjection x$0 )
    {
        return AnyProjection$.MODULE$.unapply( var0 );
    }

    public static AnyProjection apply( final Variable variable )
    {
        return AnyProjection$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Variable,A> andThen( final Function1<AnyProjection,A> g )
    {
        return AnyProjection$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,AnyProjection> compose( final Function1<A,Variable> g )
    {
        return AnyProjection$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable variable()
    {
        return this.variable;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.materializeAny( structure.loadVariable( this.variable().name() ), this.codeGenType( context ) );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.variable().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        CodeGenType var3 = this.variable().codeGenType();
        if ( var3 instanceof CypherCodeGenType )
        {
            CypherCodeGenType var4 = (CypherCodeGenType) var3;
            return var4;
        }
        else
        {
            throw new InternalException( "Tried to create a Cypher value from a non-cypher-value variable" );
        }
    }

    public AnyProjection copy( final Variable variable )
    {
        return new AnyProjection( variable );
    }

    public Variable copy$default$1()
    {
        return this.variable();
    }

    public String productPrefix()
    {
        return "AnyProjection";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.variable();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AnyProjection;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof AnyProjection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        AnyProjection var4 = (AnyProjection) x$1;
                        Variable var10000 = this.variable();
                        Variable var5 = var4.variable();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
