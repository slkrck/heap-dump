package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class Ascending
{
    public static String toString()
    {
        return Ascending$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return Ascending$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return Ascending$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return Ascending$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return Ascending$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return Ascending$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return Ascending$.MODULE$.productPrefix();
    }
}
