package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Not implements CodeGenExpression, Product, Serializable
{
    private final CodeGenExpression inner;

    public Not( final CodeGenExpression inner )
    {
        this.inner = inner;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<CodeGenExpression> unapply( final Not x$0 )
    {
        return Not$.MODULE$.unapply( var0 );
    }

    public static Not apply( final CodeGenExpression inner )
    {
        return Not$.MODULE$.apply( var0 );
    }

    public static <A> Function1<CodeGenExpression,A> andThen( final Function1<Not,A> g )
    {
        return Not$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,Not> compose( final Function1<A,CodeGenExpression> g )
    {
        return Not$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression inner()
    {
        return this.inner;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.inner().init( generator, context );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        Object var10000;
        if ( !this.nullable( context ) )
        {
            CypherCodeGenType var4 = this.inner().codeGenType( context );
            Object var3;
            if ( var4.isPrimitive() )
            {
                var3 = structure.notExpression( this.inner().generateExpression( structure, context ) );
            }
            else
            {
                var3 = structure.unbox( structure.threeValuedNotExpression(
                        structure.box( this.inner().generateExpression( structure, context ), this.inner().codeGenType( context ) ) ),
                        CodeGenType$.MODULE$.primitiveBool() );
            }

            var10000 = var3;
        }
        else
        {
            var10000 = structure.threeValuedNotExpression(
                    structure.box( this.inner().generateExpression( structure, context ), this.inner().codeGenType( context ) ) );
        }

        return var10000;
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.inner().nullable( context );
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return !this.nullable( context ) ? CodeGenType$.MODULE$.primitiveBool() : new CypherCodeGenType(.MODULE$.CTBoolean(),ReferenceType$.MODULE$);
    }

    public Not copy( final CodeGenExpression inner )
    {
        return new Not( inner );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.inner();
    }

    public String productPrefix()
    {
        return "Not";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.inner();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Not;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof Not )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        Not var4 = (Not) x$1;
                        CodeGenExpression var10000 = this.inner();
                        CodeGenExpression var5 = var4.inner();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
