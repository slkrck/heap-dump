package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SortInstruction implements Instruction, Product, Serializable
{
    private final String opName;
    private final SortTableInfo sortTableInfo;

    public SortInstruction( final String opName, final SortTableInfo sortTableInfo )
    {
        this.opName = opName;
        this.sortTableInfo = sortTableInfo;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<String,SortTableInfo>> unapply( final SortInstruction x$0 )
    {
        return SortInstruction$.MODULE$.unapply( var0 );
    }

    public static SortInstruction apply( final String opName, final SortTableInfo sortTableInfo )
    {
        return SortInstruction$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<String,SortTableInfo>,SortInstruction> tupled()
    {
        return SortInstruction$.MODULE$.tupled();
    }

    public static Function1<String,Function1<SortTableInfo,SortInstruction>> curried()
    {
        return SortInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String opName()
    {
        return this.opName;
    }

    public SortTableInfo sortTableInfo()
    {
        return this.sortTableInfo;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.opName(), new Some( this.getClass().getSimpleName() ), ( body ) -> {
            $anonfun$body$1( this, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.empty();
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.opName()}) ));
    }

    public SortInstruction copy( final String opName, final SortTableInfo sortTableInfo )
    {
        return new SortInstruction( opName, sortTableInfo );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public SortTableInfo copy$default$2()
    {
        return this.sortTableInfo();
    }

    public String productPrefix()
    {
        return "SortInstruction";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.sortTableInfo();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SortInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof SortInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            SortInstruction var4 = (SortInstruction) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            SortTableInfo var7 = this.sortTableInfo();
                            SortTableInfo var6 = var4.sortTableInfo();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
