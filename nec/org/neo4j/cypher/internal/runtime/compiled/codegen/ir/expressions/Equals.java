package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.MapType;
import org.neo4j.cypher.internal.v4_0.util.symbols.StringType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import org.neo4j.exceptions.IncomparableValuesException;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Equals implements CodeGenExpression, Product, Serializable
{
    private final CodeGenExpression lhs;
    private final CodeGenExpression rhs;

    public Equals( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        this.lhs = lhs;
        this.rhs = rhs;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Equals x$0 )
    {
        return Equals$.MODULE$.unapply( var0 );
    }

    public static Equals apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return Equals$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CodeGenExpression,CodeGenExpression>,Equals> tupled()
    {
        return Equals$.MODULE$.tupled();
    }

    public static Function1<CodeGenExpression,Function1<CodeGenExpression,Equals>> curried()
    {
        return Equals$.MODULE$.curried();
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression lhs()
    {
        return this.lhs;
    }

    public CodeGenExpression rhs()
    {
        return this.rhs;
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.lhs().nullable( context ) || this.rhs().nullable( context ) || this.isCollectionOfNonPrimitives( this.lhs(), context ) ||
                this.isCollectionOfNonPrimitives( this.rhs(), context );
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return this.nullable( context ) ? new CypherCodeGenType(.MODULE$.CTBoolean(),ReferenceType$.MODULE$) :CodeGenType$.MODULE$.primitiveBool();
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.lhs().init( generator, context );
        this.rhs().init( generator, context );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        Tuple3 var4 = new Tuple3( this.lhs(), this.rhs(), BoxesRunTime.boxToBoolean( this.nullable( context ) ) );
        Object var3;
        if ( var4 != null )
        {
            CodeGenExpression var5 = (CodeGenExpression) var4._1();
            CodeGenExpression var6 = (CodeGenExpression) var4._2();
            boolean var7 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( var5 instanceof NodeExpression )
            {
                NodeExpression var8 = (NodeExpression) var5;
                Variable v1 = var8.nodeIdVar();
                if ( var6 instanceof NodeExpression )
                {
                    NodeExpression var10 = (NodeExpression) var6;
                    Variable v2 = var10.nodeIdVar();
                    if ( !var7 )
                    {
                        var3 = structure.equalityExpression( structure.loadVariable( v1.name() ), structure.loadVariable( v2.name() ),
                                CodeGenType$.MODULE$.primitiveNode() );
                        return var3;
                    }
                }
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression var12 = (CodeGenExpression) var4._1();
            CodeGenExpression var13 = (CodeGenExpression) var4._2();
            boolean var14 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( var12 instanceof NodeExpression )
            {
                NodeExpression var15 = (NodeExpression) var12;
                Variable v1 = var15.nodeIdVar();
                if ( var13 instanceof NodeExpression )
                {
                    NodeExpression var17 = (NodeExpression) var13;
                    Variable v2 = var17.nodeIdVar();
                    if ( var14 )
                    {
                        var3 = structure.threeValuedPrimitiveEqualsExpression( structure.loadVariable( v1.name() ), structure.loadVariable( v2.name() ),
                                CodeGenType$.MODULE$.primitiveNode() );
                        return var3;
                    }
                }
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression var19 = (CodeGenExpression) var4._1();
            CodeGenExpression var20 = (CodeGenExpression) var4._2();
            boolean var21 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( var19 instanceof RelationshipExpression )
            {
                RelationshipExpression var22 = (RelationshipExpression) var19;
                Variable v1 = var22.relId();
                if ( var20 instanceof RelationshipExpression )
                {
                    RelationshipExpression var24 = (RelationshipExpression) var20;
                    Variable v2 = var24.relId();
                    if ( !var21 )
                    {
                        var3 = structure.equalityExpression( structure.loadVariable( v1.name() ), structure.loadVariable( v2.name() ),
                                CodeGenType$.MODULE$.primitiveRel() );
                        return var3;
                    }
                }
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression var26 = (CodeGenExpression) var4._1();
            CodeGenExpression var27 = (CodeGenExpression) var4._2();
            boolean var28 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( var26 instanceof RelationshipExpression )
            {
                RelationshipExpression var29 = (RelationshipExpression) var26;
                Variable v1 = var29.relId();
                if ( var27 instanceof RelationshipExpression )
                {
                    RelationshipExpression var31 = (RelationshipExpression) var27;
                    Variable v2 = var31.relId();
                    if ( var28 )
                    {
                        var3 = structure.threeValuedPrimitiveEqualsExpression( structure.loadVariable( v1.name() ), structure.loadVariable( v2.name() ),
                                CodeGenType$.MODULE$.primitiveRel() );
                        return var3;
                    }
                }
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression var33 = (CodeGenExpression) var4._1();
            CodeGenExpression var34 = (CodeGenExpression) var4._2();
            if ( var33 instanceof NodeExpression && var34 instanceof RelationshipExpression )
            {
                throw new IncomparableValuesException(.MODULE$.CTNode().toString(), .MODULE$.CTRelationship().toString());
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression var35 = (CodeGenExpression) var4._1();
            CodeGenExpression var36 = (CodeGenExpression) var4._2();
            if ( var35 instanceof RelationshipExpression && var36 instanceof NodeExpression )
            {
                throw new IncomparableValuesException(.MODULE$.CTNode().toString(), .MODULE$.CTRelationship().toString());
            }
        }

        if ( var4 != null )
        {
            boolean var37 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( var37 )
            {
                var3 = structure.threeValuedEqualsExpression(
                        structure.box( this.lhs().generateExpression( structure, context ), this.lhs().codeGenType( context ) ),
                        structure.box( this.rhs().generateExpression( structure, context ), this.rhs().codeGenType( context ) ) );
                return var3;
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression t1 = (CodeGenExpression) var4._1();
            CodeGenExpression t2 = (CodeGenExpression) var4._2();
            boolean var40 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( !var40 )
            {
                label148:
                {
                    CypherCodeGenType var10000 = t1.codeGenType( context );
                    CypherCodeGenType var41 = t2.codeGenType( context );
                    if ( var10000 == null )
                    {
                        if ( var41 != null )
                        {
                            break label148;
                        }
                    }
                    else if ( !var10000.equals( var41 ) )
                    {
                        break label148;
                    }

                    if ( t1.codeGenType( context ).isPrimitive() )
                    {
                        var3 = structure.equalityExpression( this.lhs().generateExpression( structure, context ),
                                this.rhs().generateExpression( structure, context ), t1.codeGenType( context ) );
                        return var3;
                    }
                }
            }
        }

        CypherType var55;
        if ( var4 != null )
        {
            CodeGenExpression t1 = (CodeGenExpression) var4._1();
            CodeGenExpression t2 = (CodeGenExpression) var4._2();
            boolean var44 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( !var44 )
            {
                label138:
                {
                    var55 = t1.codeGenType( context ).ct();
                    CypherType var45 = t2.codeGenType( context ).ct();
                    if ( var55 == null )
                    {
                        if ( var45 != null )
                        {
                            break label138;
                        }
                    }
                    else if ( !var55.equals( var45 ) )
                    {
                        break label138;
                    }

                    if ( t1.codeGenType( context ).isPrimitive() )
                    {
                        var3 = structure.equalityExpression( this.lhs().generateExpression( structure, context ),
                                structure.unbox( this.rhs().generateExpression( structure, context ), t2.codeGenType( context ) ), t1.codeGenType( context ) );
                        return var3;
                    }
                }
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression t1 = (CodeGenExpression) var4._1();
            CodeGenExpression t2 = (CodeGenExpression) var4._2();
            boolean var48 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( !var48 )
            {
                label128:
                {
                    var55 = t1.codeGenType( context ).ct();
                    CypherType var49 = t2.codeGenType( context ).ct();
                    if ( var55 == null )
                    {
                        if ( var49 != null )
                        {
                            break label128;
                        }
                    }
                    else if ( !var55.equals( var49 ) )
                    {
                        break label128;
                    }

                    if ( t2.codeGenType( context ).isPrimitive() )
                    {
                        var3 = structure.equalityExpression( structure.unbox( this.lhs().generateExpression( structure, context ), t1.codeGenType( context ) ),
                                this.rhs().generateExpression( structure, context ), t1.codeGenType( context ) );
                        return var3;
                    }
                }
            }
        }

        if ( var4 != null )
        {
            CodeGenExpression t1 = (CodeGenExpression) var4._1();
            CodeGenExpression t2 = (CodeGenExpression) var4._2();
            boolean var52 = BoxesRunTime.unboxToBoolean( var4._3() );
            if ( !var52 )
            {
                label217:
                {
                    var55 = t1.codeGenType( context ).ct();
                    CypherType var53 = t2.codeGenType( context ).ct();
                    if ( var55 == null )
                    {
                        if ( var53 != null )
                        {
                            break label217;
                        }
                    }
                    else if ( !var55.equals( var53 ) )
                    {
                        break label217;
                    }

                    var55 = t1.codeGenType( context ).ct();
                    StringType var54 = .MODULE$.CTString();
                    if ( var55 == null )
                    {
                        if ( var54 != null )
                        {
                            break label217;
                        }
                    }
                    else if ( !var55.equals( var54 ) )
                    {
                        break label217;
                    }

                    var3 = structure.equalityExpression( this.lhs().generateExpression( structure, context ),
                            this.rhs().generateExpression( structure, context ), t1.codeGenType( context ) );
                    return var3;
                }
            }
        }

        var3 = structure.unbox(
                structure.threeValuedEqualsExpression( structure.box( this.lhs().generateExpression( structure, context ), this.lhs().codeGenType( context ) ),
                        structure.box( this.rhs().generateExpression( structure, context ), this.rhs().codeGenType( context ) ) ),
                new CypherCodeGenType(.MODULE$.CTBoolean(), ReferenceType$.MODULE$ ));
        return var3;
    }

    private boolean isCollectionOfNonPrimitives( final CodeGenExpression e, final CodeGenContext context )
    {
        CypherCodeGenType var4 = e.codeGenType( context );
        boolean var3;
        if ( var4 != null )
        {
            CypherType var5 = var4.ct();
            RepresentationType var6 = var4.repr();
            Option var7 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var5 );
            if ( !var7.isEmpty() && var6 instanceof ListReferenceType )
            {
                ListReferenceType var8 = (ListReferenceType) var6;
                RepresentationType inner = var8.inner();
                if ( !RepresentationType$.MODULE$.isPrimitive( inner ) )
                {
                    var3 = true;
                    return var3;
                }
            }
        }

        if ( var4 != null )
        {
            label49:
            {
                CypherType var10 = var4.ct();
                MapType var10000 = .MODULE$.CTMap();
                if ( var10000 == null )
                {
                    if ( var10 != null )
                    {
                        break label49;
                    }
                }
                else if ( !var10000.equals( var10 ) )
                {
                    break label49;
                }

                var3 = true;
                return var3;
            }
        }

        var3 = false;
        return var3;
    }

    public Equals copy( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Equals( lhs, rhs );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.lhs();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.rhs();
    }

    public String productPrefix()
    {
        return "Equals";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        CodeGenExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Equals;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof Equals )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            Equals var4 = (Equals) x$1;
                            CodeGenExpression var10000 = this.lhs();
                            CodeGenExpression var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.rhs();
                            CodeGenExpression var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
