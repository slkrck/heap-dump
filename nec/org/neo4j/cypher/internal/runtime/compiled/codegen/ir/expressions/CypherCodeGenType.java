package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class CypherCodeGenType implements CodeGenType, Product, Serializable
{
    private final CypherType ct;
    private final RepresentationType repr;

    public CypherCodeGenType( final CypherType ct, final RepresentationType repr )
    {
        this.ct = ct;
        this.repr = repr;
        Product.$init$( this );
    }

    public static Option<Tuple2<CypherType,RepresentationType>> unapply( final CypherCodeGenType x$0 )
    {
        return CypherCodeGenType$.MODULE$.unapply( var0 );
    }

    public static CypherCodeGenType apply( final CypherType ct, final RepresentationType repr )
    {
        return CypherCodeGenType$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CypherType,RepresentationType>,CypherCodeGenType> tupled()
    {
        return CypherCodeGenType$.MODULE$.tupled();
    }

    public static Function1<CypherType,Function1<RepresentationType,CypherCodeGenType>> curried()
    {
        return CypherCodeGenType$.MODULE$.curried();
    }

    public CypherType ct()
    {
        return this.ct;
    }

    public RepresentationType repr()
    {
        return this.repr;
    }

    public boolean isPrimitive()
    {
        return RepresentationType$.MODULE$.isPrimitive( this.repr() );
    }

    public boolean isValue()
    {
        return RepresentationType$.MODULE$.isValue( this.repr() );
    }

    public boolean isAnyValue()
    {
        return RepresentationType$.MODULE$.isAnyValue( this.repr() );
    }

    public boolean canBeNullable()
    {
        boolean var3;
        if ( this.isPrimitive() )
        {
            label37:
            {
                CypherType var10000 = this.ct();
                NodeType var1 = .MODULE$.CTNode();
                if ( var10000 == null )
                {
                    if ( var1 == null )
                    {
                        break label37;
                    }
                }
                else if ( var10000.equals( var1 ) )
                {
                    break label37;
                }

                var10000 = this.ct();
                RelationshipType var2 = .MODULE$.CTRelationship();
                if ( var10000 == null )
                {
                    if ( var2 == null )
                    {
                        break label37;
                    }
                }
                else if ( var10000.equals( var2 ) )
                {
                    break label37;
                }

                var3 = false;
                return var3;
            }
        }

        var3 = true;
        return var3;
    }

    public CypherCodeGenType copy( final CypherType ct, final RepresentationType repr )
    {
        return new CypherCodeGenType( ct, repr );
    }

    public CypherType copy$default$1()
    {
        return this.ct();
    }

    public RepresentationType copy$default$2()
    {
        return this.repr();
    }

    public String productPrefix()
    {
        return "CypherCodeGenType";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.ct();
            break;
        case 1:
            var10000 = this.repr();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CypherCodeGenType;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof CypherCodeGenType )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            CypherCodeGenType var4 = (CypherCodeGenType) x$1;
                            CypherType var10000 = this.ct();
                            CypherType var5 = var4.ct();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            RepresentationType var7 = this.repr();
                            RepresentationType var6 = var4.repr();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
