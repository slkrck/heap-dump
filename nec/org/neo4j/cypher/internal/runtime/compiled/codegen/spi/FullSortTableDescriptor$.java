package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class FullSortTableDescriptor$ extends AbstractFunction1<OrderableTupleDescriptor,FullSortTableDescriptor> implements Serializable
{
    public static FullSortTableDescriptor$ MODULE$;

    static
    {
        new FullSortTableDescriptor$();
    }

    private FullSortTableDescriptor$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "FullSortTableDescriptor";
    }

    public FullSortTableDescriptor apply( final OrderableTupleDescriptor tupleDescriptor )
    {
        return new FullSortTableDescriptor( tupleDescriptor );
    }

    public Option<OrderableTupleDescriptor> unapply( final FullSortTableDescriptor x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.tupleDescriptor() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
