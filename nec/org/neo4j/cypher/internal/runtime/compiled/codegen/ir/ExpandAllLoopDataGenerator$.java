package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple6;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction6;

public final class ExpandAllLoopDataGenerator$
        extends AbstractFunction6<String,Variable,SemanticDirection,Map<String,String>,Variable,Variable,ExpandAllLoopDataGenerator> implements Serializable
{
    public static ExpandAllLoopDataGenerator$ MODULE$;

    static
    {
        new ExpandAllLoopDataGenerator$();
    }

    private ExpandAllLoopDataGenerator$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ExpandAllLoopDataGenerator";
    }

    public ExpandAllLoopDataGenerator apply( final String opName, final Variable fromVar, final SemanticDirection dir, final Map<String,String> types,
            final Variable toVar, final Variable relVar )
    {
        return new ExpandAllLoopDataGenerator( opName, fromVar, dir, types, toVar, relVar );
    }

    public Option<Tuple6<String,Variable,SemanticDirection,Map<String,String>,Variable,Variable>> unapply( final ExpandAllLoopDataGenerator x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple6( x$0.opName(), x$0.fromVar(), x$0.dir(), x$0.types(), x$0.toVar(), x$0.relVar() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
