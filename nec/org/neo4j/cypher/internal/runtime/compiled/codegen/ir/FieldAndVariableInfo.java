package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class FieldAndVariableInfo implements Product, Serializable
{
    private final String fieldName;
    private final String queryVariableName;
    private final Variable incomingVariable;
    private final Variable outgoingVariable;

    public FieldAndVariableInfo( final String fieldName, final String queryVariableName, final Variable incomingVariable, final Variable outgoingVariable )
    {
        this.fieldName = fieldName;
        this.queryVariableName = queryVariableName;
        this.incomingVariable = incomingVariable;
        this.outgoingVariable = outgoingVariable;
        Product.$init$( this );
    }

    public static Option<Tuple4<String,String,Variable,Variable>> unapply( final FieldAndVariableInfo x$0 )
    {
        return FieldAndVariableInfo$.MODULE$.unapply( var0 );
    }

    public static FieldAndVariableInfo apply( final String fieldName, final String queryVariableName, final Variable incomingVariable,
            final Variable outgoingVariable )
    {
        return FieldAndVariableInfo$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<String,String,Variable,Variable>,FieldAndVariableInfo> tupled()
    {
        return FieldAndVariableInfo$.MODULE$.tupled();
    }

    public static Function1<String,Function1<String,Function1<Variable,Function1<Variable,FieldAndVariableInfo>>>> curried()
    {
        return FieldAndVariableInfo$.MODULE$.curried();
    }

    public String fieldName()
    {
        return this.fieldName;
    }

    public String queryVariableName()
    {
        return this.queryVariableName;
    }

    public Variable incomingVariable()
    {
        return this.incomingVariable;
    }

    public Variable outgoingVariable()
    {
        return this.outgoingVariable;
    }

    public FieldAndVariableInfo copy( final String fieldName, final String queryVariableName, final Variable incomingVariable, final Variable outgoingVariable )
    {
        return new FieldAndVariableInfo( fieldName, queryVariableName, incomingVariable, outgoingVariable );
    }

    public String copy$default$1()
    {
        return this.fieldName();
    }

    public String copy$default$2()
    {
        return this.queryVariableName();
    }

    public Variable copy$default$3()
    {
        return this.incomingVariable();
    }

    public Variable copy$default$4()
    {
        return this.outgoingVariable();
    }

    public String productPrefix()
    {
        return "FieldAndVariableInfo";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.fieldName();
            break;
        case 1:
            var10000 = this.queryVariableName();
            break;
        case 2:
            var10000 = this.incomingVariable();
            break;
        case 3:
            var10000 = this.outgoingVariable();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FieldAndVariableInfo;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof FieldAndVariableInfo )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            FieldAndVariableInfo var4 = (FieldAndVariableInfo) x$1;
                            String var10000 = this.fieldName();
                            String var5 = var4.fieldName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            var10000 = this.queryVariableName();
                            String var6 = var4.queryVariableName();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label72;
                            }

                            Variable var9 = this.incomingVariable();
                            Variable var7 = var4.incomingVariable();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label72;
                            }

                            var9 = this.outgoingVariable();
                            Variable var8 = var4.outgoingVariable();
                            if ( var9 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label63;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label81;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
