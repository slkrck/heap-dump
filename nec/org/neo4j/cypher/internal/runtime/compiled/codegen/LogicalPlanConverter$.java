package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.AllNodesScan;
import org.neo4j.cypher.internal.logical.plans.Apply;
import org.neo4j.cypher.internal.logical.plans.Argument;
import org.neo4j.cypher.internal.logical.plans.Ascending;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.ColumnOrder;
import org.neo4j.cypher.internal.logical.plans.CompositeQueryExpression;
import org.neo4j.cypher.internal.logical.plans.Descending;
import org.neo4j.cypher.internal.logical.plans.Distinct;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.ManyQueryExpression;
import org.neo4j.cypher.internal.logical.plans.ManySeekableArgs;
import org.neo4j.cypher.internal.logical.plans.NodeByIdSeek;
import org.neo4j.cypher.internal.logical.plans.NodeByLabelScan;
import org.neo4j.cypher.internal.logical.plans.NodeCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.NodeHashJoin;
import org.neo4j.cypher.internal.logical.plans.NodeIndexSeek;
import org.neo4j.cypher.internal.logical.plans.NodeUniqueIndexSeek;
import org.neo4j.cypher.internal.logical.plans.PartialSort;
import org.neo4j.cypher.internal.logical.plans.PartialTop;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.logical.plans.QueryExpression;
import org.neo4j.cypher.internal.logical.plans.RangeQueryExpression;
import org.neo4j.cypher.internal.logical.plans.RelationshipCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.SeekableArgs;
import org.neo4j.cypher.internal.logical.plans.Selection;
import org.neo4j.cypher.internal.logical.plans.SingleQueryExpression;
import org.neo4j.cypher.internal.logical.plans.SingleSeekableArg;
import org.neo4j.cypher.internal.logical.plans.Skip;
import org.neo4j.cypher.internal.logical.plans.Sort;
import org.neo4j.cypher.internal.logical.plans.Top;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.planner.spi.PlanningAttributes.Cardinalities;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.AcceptVisitor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.AggregationInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.ApplyInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.BuildProbeTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.BuildProbeTable$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.BuildSortTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.BuildTopTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.CartesianProductInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.DecreaseAndReturnWhenZero;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.ExpandAllLoopDataGenerator;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.ExpandIntoLoopDataGenerator;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.ForEachExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.GetMatchesFromProbeTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.GetSortedResult;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.If;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.IndexSeek;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.JoinData;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.LoopDataGenerator;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.MethodInvocation;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.NodeCountFromCountStoreInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.RelationshipCountFromCountStoreInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.ScanAllNodes;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.ScanForLabel;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.SeekNodeById;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.SelectionInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.SkipInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.SortInstruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.SortTableInfo;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.UnwindPrimitiveCollection;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.WhileLoop;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.AggregateExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.AggregationConverter$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ExpressionConverter$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ListReferenceType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.LoadVariable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ReferenceType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ToSet;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Ascending$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Descending$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortItem;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.ListLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import org.neo4j.cypher.internal.v4_0.util.Cardinality;
import org.neo4j.cypher.internal.v4_0.util.One;
import org.neo4j.cypher.internal.v4_0.util.ZeroOneOrMany;
import org.neo4j.cypher.internal.v4_0.util.Foldable.FoldableAny.;
import org.neo4j.cypher.internal.v4_0.util.attribution.SameId;
import org.neo4j.cypher.internal.v4_0.util.symbols.AnyType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.exceptions.InternalException;
import scala.Function1;
import scala.Function5;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.SetLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable..colon.colon;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class LogicalPlanConverter$
{
    public static LogicalPlanConverter$ MODULE$;

    static
    {
        new LogicalPlanConverter$();
    }

    private LogicalPlanConverter$()
    {
        MODULE$ = this;
    }

    private static final WhileLoop indexSeekFun$1( final String opName, final String descriptorVar, final CodeGenExpression expression, final Variable nodeVar,
            final Instruction actions, final NodeIndexSeek indexSeek$1 )
    {
        return new WhileLoop( nodeVar, new IndexSeek( opName, indexSeek$1.label().name(), (Seq) indexSeek$1.properties().map( ( x$12 ) -> {
            return x$12.propertyKeyToken().name();
        }, scala.collection.Seq..MODULE$.canBuildFrom() ), descriptorVar, expression ),actions);
    }

    private static final WhileLoop indexSeekFun$3( final String opName, final String descriptorVar, final CodeGenExpression expression, final Variable nodeVar,
            final Instruction actions, final NodeUniqueIndexSeek indexSeek$3 )
    {
        return new WhileLoop( nodeVar, new IndexSeek( opName, indexSeek$3.label().name(), (Seq) indexSeek$3.properties().map( ( x$13 ) -> {
            return x$13.propertyKeyToken().name();
        }, scala.collection.Seq..MODULE$.canBuildFrom() ), descriptorVar, expression ),actions);
    }

    public CodeGenPlan asCodeGenPlan( final LogicalPlan logicalPlan )
    {
        boolean var3 = false;
        CartesianProduct var4 = null;
        boolean var5 = false;
        Top var6 = null;
        boolean var7 = false;
        PartialTop var8 = null;
        boolean var9 = false;
        Aggregation var10 = null;
        boolean var11 = false;
        Distinct var12 = null;
        boolean var13 = false;
        Sort var14 = null;
        boolean var15 = false;
        PartialSort var16 = null;
        Object var2;
        if ( logicalPlan instanceof Argument )
        {
            Argument var18 = (Argument) logicalPlan;
            var2 = this.argumentAsCodeGenPlan( var18 );
        }
        else if ( logicalPlan instanceof AllNodesScan )
        {
            AllNodesScan var19 = (AllNodesScan) logicalPlan;
            var2 = this.allNodesScanAsCodeGenPlan( var19 );
        }
        else if ( logicalPlan instanceof NodeByLabelScan )
        {
            NodeByLabelScan var20 = (NodeByLabelScan) logicalPlan;
            var2 = this.nodeByLabelScanAsCodeGenPlan( var20 );
        }
        else if ( logicalPlan instanceof NodeIndexSeek )
        {
            NodeIndexSeek var21 = (NodeIndexSeek) logicalPlan;
            var2 = this.nodeIndexSeekAsCodeGenPlan( var21 );
        }
        else if ( logicalPlan instanceof NodeByIdSeek )
        {
            NodeByIdSeek var22 = (NodeByIdSeek) logicalPlan;
            var2 = this.nodeByIdSeekAsCodeGenPlan( var22 );
        }
        else if ( logicalPlan instanceof NodeUniqueIndexSeek )
        {
            NodeUniqueIndexSeek var23 = (NodeUniqueIndexSeek) logicalPlan;
            var2 = this.nodeUniqueIndexSeekAsCodeGen( var23 );
        }
        else if ( logicalPlan instanceof Expand )
        {
            Expand var24 = (Expand) logicalPlan;
            var2 = this.expandAsCodeGenPlan( var24 );
        }
        else if ( logicalPlan instanceof NodeHashJoin )
        {
            NodeHashJoin var25 = (NodeHashJoin) logicalPlan;
            var2 = this.nodeHashJoinAsCodeGenPlan( var25 );
        }
        else
        {
            if ( logicalPlan instanceof CartesianProduct )
            {
                var3 = true;
                var4 = (CartesianProduct) logicalPlan;
                if (.MODULE$.findByAllClass$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( var4 ), scala.reflect.ClassTag..
                MODULE$.apply( NodeHashJoin.class )).nonEmpty()){
                throw new CantCompileQueryException(
                        (new StringBuilder( 39 )).append( "This logicalPlan is not yet supported: " ).append( logicalPlan ).toString() );
            }
            }

            if ( var3 )
            {
                var2 = this.cartesianProductAsCodeGenPlan( var4 );
            }
            else if ( logicalPlan instanceof Selection )
            {
                Selection var26 = (Selection) logicalPlan;
                var2 = this.selectionAsCodeGenPlan( var26 );
            }
            else
            {
                if ( logicalPlan instanceof Top )
                {
                    var5 = true;
                    var6 = (Top) logicalPlan;
                    if ( this.hasStandaloneLimit( var6 ) )
                    {
                        throw new CantCompileQueryException( (new StringBuilder( 30 )).append( "Not able to combine LIMIT and " ).append( var6 ).toString() );
                    }
                }

                if ( var5 )
                {
                    var2 = this.topAsCodeGenPlan( var6 );
                }
                else
                {
                    if ( logicalPlan instanceof PartialTop )
                    {
                        var7 = true;
                        var8 = (PartialTop) logicalPlan;
                        if ( this.hasStandaloneLimit( var8 ) )
                        {
                            throw new CantCompileQueryException(
                                    (new StringBuilder( 30 )).append( "Not able to combine LIMIT and " ).append( var8 ).toString() );
                        }
                    }

                    if ( var7 )
                    {
                        var2 = this.topAsCodeGenPlan( new Top( var8.source(),
                                        (Seq) var8.alreadySortedPrefix().$plus$plus( var8.stillToSortSuffix(), scala.collection.Seq..MODULE$.canBuildFrom() ),
                                var8.limit(), new SameId( var8.id() ) ));
                    }
                    else if ( logicalPlan instanceof Limit )
                    {
                        Limit var27 = (Limit) logicalPlan;
                        var2 = this.limitAsCodeGenPlan( var27 );
                    }
                    else if ( logicalPlan instanceof Skip )
                    {
                        Skip var28 = (Skip) logicalPlan;
                        var2 = this.skipAsCodeGenPlan( var28 );
                    }
                    else if ( logicalPlan instanceof ProduceResult )
                    {
                        ProduceResult var29 = (ProduceResult) logicalPlan;
                        var2 = this.produceResultsAsCodeGenPlan( var29 );
                    }
                    else if ( logicalPlan instanceof Projection )
                    {
                        Projection var30 = (Projection) logicalPlan;
                        var2 = this.projectionAsCodeGenPlan( var30 );
                    }
                    else
                    {
                        if ( logicalPlan instanceof Aggregation )
                        {
                            var9 = true;
                            var10 = (Aggregation) logicalPlan;
                            if ( this.hasLimit( var10 ) || this.hasMultipleAggregations( var10 ) )
                            {
                                throw new CantCompileQueryException(
                                        (new StringBuilder( 37 )).append( "Not able to combine aggregation with " ).append( var10 ).toString() );
                            }
                        }

                        if ( var9 )
                        {
                            var2 = this.aggregationAsCodeGenPlan( var10 );
                        }
                        else
                        {
                            if ( logicalPlan instanceof Distinct )
                            {
                                var11 = true;
                                var12 = (Distinct) logicalPlan;
                                if ( this.hasStandaloneLimit( var12 ) )
                                {
                                    throw new CantCompileQueryException(
                                            (new StringBuilder( 30 )).append( "Not able to combine LIMIT and " ).append( var12 ).toString() );
                                }
                            }

                            if ( var11 )
                            {
                                var2 = this.distinctAsCodeGenPlan( var12 );
                            }
                            else if ( logicalPlan instanceof NodeCountFromCountStore )
                            {
                                NodeCountFromCountStore var31 = (NodeCountFromCountStore) logicalPlan;
                                var2 = this.nodeCountFromCountStore( var31 );
                            }
                            else if ( logicalPlan instanceof RelationshipCountFromCountStore )
                            {
                                RelationshipCountFromCountStore var32 = (RelationshipCountFromCountStore) logicalPlan;
                                var2 = this.relCountFromCountStore( var32 );
                            }
                            else if ( logicalPlan instanceof UnwindCollection )
                            {
                                UnwindCollection var33 = (UnwindCollection) logicalPlan;
                                var2 = this.unwindAsCodeGenPlan( var33 );
                            }
                            else
                            {
                                if ( logicalPlan instanceof Sort )
                                {
                                    var13 = true;
                                    var14 = (Sort) logicalPlan;
                                    if ( this.hasStandaloneLimit( var14 ) )
                                    {
                                        throw new CantCompileQueryException(
                                                (new StringBuilder( 30 )).append( "Not able to combine LIMIT and " ).append( var14 ).toString() );
                                    }
                                }

                                if ( var13 )
                                {
                                    var2 = this.sortAsCodeGenPlan( var14 );
                                }
                                else
                                {
                                    if ( logicalPlan instanceof PartialSort )
                                    {
                                        var15 = true;
                                        var16 = (PartialSort) logicalPlan;
                                        if ( this.hasStandaloneLimit( var16 ) )
                                        {
                                            throw new CantCompileQueryException(
                                                    (new StringBuilder( 30 )).append( "Not able to combine LIMIT and " ).append( var16 ).toString() );
                                        }
                                    }

                                    if ( var15 )
                                    {
                                        var2 = this.sortAsCodeGenPlan( new Sort( var16.source(),
                                                (Seq) var16.alreadySortedPrefix().$plus$plus( var16.stillToSortSuffix(),
                                                        scala.collection.Seq..MODULE$.canBuildFrom() ), new SameId( var16.id() ) ));
                                    }
                                    else
                                    {
                                        if ( !(logicalPlan instanceof Apply) )
                                        {
                                            throw new CantCompileQueryException(
                                                    (new StringBuilder( 39 )).append( "This logicalPlan is not yet supported: " ).append(
                                                            logicalPlan.getClass().getSimpleName() ).toString() );
                                        }

                                        Apply var34 = (Apply) logicalPlan;
                                        var2 = this.applyAsCodeGenPlan( var34 );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return (CodeGenPlan) var2;
    }

    private LeafCodeGenPlan argumentAsCodeGenPlan( final Argument argument )
    {
        return new LeafCodeGenPlan( argument )
        {
            private final LogicalPlan logicalPlan;

            public
            {
                LeafCodeGenPlan.$init$( this );
                this.logicalPlan = argument$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                return LeafCodeGenPlan.consume$( this, context, child, cardinalities );
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                Tuple2 var5 = context.popParent().consume( context, this, cardinalities );
                if ( var5 != null )
                {
                    Option methodHandle = (Option) var5._1();
                    List actions = (List) var5._2();
                    Tuple2 var3 = new Tuple2( methodHandle, actions );
                    Option methodHandlex = (Option) var3._1();
                    List actionsx = (List) var3._2();
                    return new Tuple2( methodHandlex, actionsx );
                }
                else
                {
                    throw new MatchError( var5 );
                }
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }
        };
    }

    private boolean hasLimit( final LogicalPlan p )
    {
        return .MODULE$.treeExists$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( p ), new Serializable()
    {
        public static final long serialVersionUID = 0L;

        public final <A1, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
        {
            Object var3;
            if ( x3 instanceof Limit )
            {
                var3 = BoxesRunTime.boxToBoolean( true );
            }
            else if ( x3 instanceof Top )
            {
                var3 = BoxesRunTime.boxToBoolean( true );
            }
            else
            {
                var3 = var2.apply( x3 );
            }

            return var3;
        }

        public final boolean isDefinedAt( final Object x3 )
        {
            boolean var2;
            if ( x3 instanceof Limit )
            {
                var2 = true;
            }
            else if ( x3 instanceof Top )
            {
                var2 = true;
            }
            else
            {
                var2 = false;
            }

            return var2;
        }
    });
    }

    private boolean hasStandaloneLimit( final LogicalPlan p )
    {
        return .MODULE$.treeExists$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( p ), new Serializable()
    {
        public static final long serialVersionUID = 0L;

        public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
        {
            Object var3;
            if ( x1 instanceof Limit )
            {
                var3 = BoxesRunTime.boxToBoolean( true );
            }
            else
            {
                var3 = var2.apply( x1 );
            }

            return var3;
        }

        public final boolean isDefinedAt( final Object x1 )
        {
            boolean var2;
            if ( x1 instanceof Limit )
            {
                var2 = true;
            }
            else
            {
                var2 = false;
            }

            return var2;
        }
    });
    }

    private boolean hasMultipleAggregations( final Aggregation p )
    {
        return .MODULE$.treeCount$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny(
            p.aggregationExpression().values().toList() ), new Serializable()
    {
        public static final long serialVersionUID = 0L;

        public final <A1, B1> B1 applyOrElse( final A1 x4, final Function1<A1,B1> default )
        {
            Boolean var3;
            label27:
            {
                if ( x4 instanceof FunctionInvocation )
                {
                    FunctionInvocation var5 = (FunctionInvocation) x4;
                    Function var10000 = var5.function();
                    org.neo4j.cypher.internal.v4_0.expressions.functions.Count.var6 = org.neo4j.cypher.internal.v4_0.expressions.functions.Count..MODULE$;
                    if ( var10000 == null )
                    {
                        if ( var6 == null )
                        {
                            break label27;
                        }
                    }
                    else if ( var10000.equals( var6 ) )
                    {
                        break label27;
                    }
                }

                var3 = BoxesRunTime.boxToBoolean( false );
                return var3;
            }

            var3 = BoxesRunTime.boxToBoolean( true );
            return var3;
        }

        public final boolean isDefinedAt( final Object x4 )
        {
            boolean var2;
            label27:
            {
                if ( x4 instanceof FunctionInvocation )
                {
                    FunctionInvocation var4 = (FunctionInvocation) x4;
                    Function var10000 = var4.function();
                    org.neo4j.cypher.internal.v4_0.expressions.functions.Count.var5 = org.neo4j.cypher.internal.v4_0.expressions.functions.Count..MODULE$;
                    if ( var10000 == null )
                    {
                        if ( var5 == null )
                        {
                            break label27;
                        }
                    }
                    else if ( var10000.equals( var5 ) )
                    {
                        break label27;
                    }
                }

                var2 = true;
                return var2;
            }

            var2 = true;
            return var2;
        }
    }) >1;
    }

    private CodeGenPlan projectionAsCodeGenPlan( final Projection projection )
    {
        return new CodeGenPlan( projection )
        {
            private final Projection logicalPlan;
            private final Projection projection$1;

            public
            {
                this.projection$1 = projection$1;
                this.logicalPlan = projection$1;
            }

            public Projection logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                context.pushParent( this );
                return LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.projection$1.lhs().get() ).produce( context, cardinalities );
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String projectionOpName = context.registerOperator( this.projection$1 );
                Map columns = org.neo4j.cypher.internal.v4_0.util.Eagerly..MODULE$.immutableMapValues( this.projection$1.projectExpressions(), ( e ) -> {
                return ExpressionConverter$.MODULE$.createExpression( e, context );
            } );
                context.retainProjectedVariables( this.projection$1.projectExpressions().keySet() );
                Map vars = (Map) columns.collect( new Serializable( null, context )
                {
                    public static final long serialVersionUID = 0L;
                    private final CodeGenContext context$8;

                    public
                    {
                        this.context$8 = context$8;
                    }

                    public final <A1 extends Tuple2<String,CodeGenExpression>, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
                    {
                        Object var3;
                        if ( x2 != null )
                        {
                            String name = (String) x2._1();
                            CodeGenExpression expr = (CodeGenExpression) x2._2();
                            if ( !this.context$8.hasVariable( name ) )
                            {
                                Variable variable = new Variable( this.context$8.namer().newVarName(), expr.codeGenType( this.context$8 ),
                                        expr.nullable( this.context$8 ) );
                                this.context$8.addVariable( name, variable );
                                this.context$8.addProjectedVariable( name, variable );
                                var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( variable ), expr);
                                return var3;
                            }
                        }

                        var3 = var2.apply( x2 );
                        return var3;
                    }

                    public final boolean isDefinedAt( final Tuple2<String,CodeGenExpression> x2 )
                    {
                        boolean var2;
                        if ( x2 != null )
                        {
                            String name = (String) x2._1();
                            if ( !this.context$8.hasVariable( name ) )
                            {
                                var2 = true;
                                return var2;
                            }
                        }

                        var2 = false;
                        return var2;
                    }
                }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                columns.foreach( ( x0$4 ) -> {
                    $anonfun$consume$2( context, x0$4 );
                    return BoxedUnit.UNIT;
                } );
                Tuple2 var9 = context.popParent().consume( context, this, cardinalities );
                if ( var9 != null )
                {
                    Option methodHandlex = (Option) var9._1();
                    List var11 = (List) var9._2();
                    if ( var11 instanceof colon )
                    {
                        colon var12 = (colon) var11;
                        Instruction action = (Instruction) var12.head();
                        List tl = var12.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandlex, action, tl );
                        Option methodHandle = (Option) var4._1();
                        Instruction actionx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Projection var18 =
                                new org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Projection( projectionOpName, vars, actionx );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var18 ) );
                    }
                }

                throw new MatchError( var9 );
            }
        };
    }

    private CodeGenPlan produceResultsAsCodeGenPlan( final ProduceResult produceResults )
    {
        return new CodeGenPlan( produceResults )
        {
            private final ProduceResult logicalPlan;
            private final ProduceResult produceResults$1;

            public
            {
                this.produceResults$1 = produceResults$1;
                this.logicalPlan = produceResults$1;
            }

            public ProduceResult logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                context.pushParent( this );
                return LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.produceResults$1.lhs().get() ).produce( context, cardinalities );
            }

            public Tuple2<scala.None .,List<AcceptVisitor>> consume( final CodeGenContext context, final CodeGenPlan child, final Cardinalities cardinalities )
            {
                String produceResultOpName = context.registerOperator( this.produceResults$1 );
                Map projections = ((TraversableOnce) this.produceResults$1.columns().map( ( c ) -> {
                    return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                            c ), ExpressionConverter$.MODULE$.createMaterializeExpressionForVariable( c, context ));
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
                return new Tuple2( scala.None..MODULE$,new colon( new AcceptVisitor( produceResultOpName, projections ), scala.collection.immutable.Nil..MODULE$))
                ;
            }
        };
    }

    private LeafCodeGenPlan allNodesScanAsCodeGenPlan( final AllNodesScan allNodesScan )
    {
        return new LeafCodeGenPlan( allNodesScan )
        {
            private final LogicalPlan logicalPlan;
            private final AllNodesScan allNodesScan$1;

            public
            {
                this.allNodesScan$1 = allNodesScan$1;
                LeafCodeGenPlan.$init$( this );
                this.logicalPlan = allNodesScan$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                return LeafCodeGenPlan.consume$( this, context, child, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                Variable variable = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveNode(), Variable$.MODULE$.apply$default$3() );
                context.addVariable( this.allNodesScan$1.idName(), variable );
                Tuple2 var6 = context.popParent().consume( context, this, cardinalities );
                if ( var6 != null )
                {
                    Option methodHandlex = (Option) var6._1();
                    List var8 = (List) var6._2();
                    if ( var8 instanceof colon )
                    {
                        colon var9 = (colon) var8;
                        Instruction actionsx = (Instruction) var9.head();
                        List tl = var9.tl$access$1();
                        Tuple3 var3 = new Tuple3( methodHandlex, actionsx, tl );
                        Option methodHandle = (Option) var3._1();
                        Instruction actions = (Instruction) var3._2();
                        List tlx = (List) var3._3();
                        String opName = context.registerOperator( this.logicalPlan() );
                        WhileLoop var16 = new WhileLoop( variable, new ScanAllNodes( opName ), actions );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var16 ) );
                    }
                }

                throw new MatchError( var6 );
            }
        };
    }

    private LeafCodeGenPlan nodeByLabelScanAsCodeGenPlan( final NodeByLabelScan nodeByLabelScan )
    {
        return new LeafCodeGenPlan( nodeByLabelScan )
        {
            private final LogicalPlan logicalPlan;
            private final NodeByLabelScan nodeByLabelScan$1;

            public
            {
                this.nodeByLabelScan$1 = nodeByLabelScan$1;
                LeafCodeGenPlan.$init$( this );
                this.logicalPlan = nodeByLabelScan$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                return LeafCodeGenPlan.consume$( this, context, child, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                Variable nodeVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveNode(), Variable$.MODULE$.apply$default$3() );
                String labelVar = context.namer().newVarName();
                context.addVariable( this.nodeByLabelScan$1.idName(), nodeVar );
                Tuple2 var7 = context.popParent().consume( context, this, cardinalities );
                if ( var7 != null )
                {
                    Option methodHandlex = (Option) var7._1();
                    List var9 = (List) var7._2();
                    if ( var9 instanceof colon )
                    {
                        colon var10 = (colon) var9;
                        Instruction actions = (Instruction) var10.head();
                        List tl = var10.tl$access$1();
                        Tuple3 var3 = new Tuple3( methodHandlex, actions, tl );
                        Option methodHandle = (Option) var3._1();
                        Instruction actionsx = (Instruction) var3._2();
                        List tlx = (List) var3._3();
                        String opName = context.registerOperator( this.logicalPlan() );
                        WhileLoop var17 = new WhileLoop( nodeVar, new ScanForLabel( opName, this.nodeByLabelScan$1.label().name(), labelVar ), actionsx );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var17 ) );
                    }
                }

                throw new MatchError( var7 );
            }
        };
    }

    private LeafCodeGenPlan sharedIndexSeekAsCodeGenPlan( final Function5<String,String,CodeGenExpression,Variable,Instruction,Instruction> indexSeekFun,
            final String idName, final QueryExpression<Expression> valueExpr, final LogicalPlan indexSeek )
    {
        return new LeafCodeGenPlan( indexSeekFun, idName, valueExpr, indexSeek )
        {
            private final LogicalPlan logicalPlan;
            private final Function5 indexSeekFun$2;
            private final String idName$1;
            private final QueryExpression valueExpr$1;

            public
            {
                this.indexSeekFun$2 = indexSeekFun$2;
                this.idName$1 = idName$1;
                this.valueExpr$1 = valueExpr$1;
                LeafCodeGenPlan.$init$( this );
                this.logicalPlan = indexSeek$2;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                return LeafCodeGenPlan.consume$( this, context, child, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                Variable nodeVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveNode(), Variable$.MODULE$.apply$default$3() );
                context.addVariable( this.idName$1, nodeVar );
                Tuple2 var7 = context.popParent().consume( context, this, cardinalities );
                if ( var7 != null )
                {
                    Option methodHandle = (Option) var7._1();
                    List var9 = (List) var7._2();
                    if ( var9 instanceof colon )
                    {
                        colon var10 = (colon) var9;
                        Instruction actions = (Instruction) var10.head();
                        List tl = var10.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandle, actions, tl );
                        Option methodHandlex = (Option) var4._1();
                        Instruction actionsx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        String opName = context.registerOperator( this.logicalPlan() );
                        QueryExpression var18 = this.valueExpr$1;
                        Object var3;
                        if ( var18 instanceof SingleQueryExpression )
                        {
                            SingleQueryExpression var19 = (SingleQueryExpression) var18;
                            Expression ex = (Expression) var19.expression();
                            CodeGenExpression expressionx = ExpressionConverter$.MODULE$.createExpression( ex, context );
                            var3 = (Instruction) this.indexSeekFun$2.apply( opName, context.namer().newVarName(), expressionx, nodeVar, actionsx );
                        }
                        else
                        {
                            if ( !(var18 instanceof ManyQueryExpression) )
                            {
                                if ( var18 instanceof CompositeQueryExpression )
                                {
                                    CompositeQueryExpression var26 = (CompositeQueryExpression) var18;
                                    if ( var26.inner() instanceof ListLiteral )
                                    {
                                        throw new CantCompileQueryException( "To be done" );
                                    }
                                }

                                if ( var18 instanceof RangeQueryExpression )
                                {
                                    throw new CantCompileQueryException( "To be done" );
                                }

                                throw new CantCompileQueryException(
                                        (new StringBuilder( 31 )).append( var18 ).append( " is not a valid QueryExpression" ).toString() );
                            }

                            ManyQueryExpression var22 = (ManyQueryExpression) var18;
                            Expression e = (Expression) var22.expression();
                            ToSet expression = new ToSet( ExpressionConverter$.MODULE$.createExpression( e, context ) );
                            Variable expressionVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.Any(), false );
                            var3 = new ForEachExpression( expressionVar, expression,
                                    (Instruction) this.indexSeekFun$2.apply( opName, context.namer().newVarName(), new LoadVariable( expressionVar ), nodeVar,
                                            actionsx ) );
                        }

                        return new Tuple2( methodHandlex, tlx.$colon$colon( var3 ) );
                    }
                }

                throw new MatchError( var7 );
            }
        };
    }

    private LeafCodeGenPlan nodeByIdSeekAsCodeGenPlan( final NodeByIdSeek seek )
    {
        return new LeafCodeGenPlan( seek )
        {
            private final LogicalPlan logicalPlan;
            private final NodeByIdSeek seek$1;

            public
            {
                this.seek$1 = seek$1;
                LeafCodeGenPlan.$init$( this );
                this.logicalPlan = seek$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                return LeafCodeGenPlan.consume$( this, context, child, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                Variable nodeVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveNode(), Variable$.MODULE$.apply$default$3() );
                context.addVariable( this.seek$1.idName(), nodeVar );
                Tuple2 var9 = context.popParent().consume( context, this, cardinalities );
                if ( var9 != null )
                {
                    Option methodHandle = (Option) var9._1();
                    List var11 = (List) var9._2();
                    if ( var11 instanceof colon )
                    {
                        colon var12 = (colon) var11;
                        Instruction actions = (Instruction) var12.head();
                        List tl = var12.tl$access$1();
                        Tuple3 var6 = new Tuple3( methodHandle, actions, tl );
                        Option methodHandlex = (Option) var6._1();
                        Instruction actionsx = (Instruction) var6._2();
                        List tlx = (List) var6._3();
                        String opName = context.registerOperator( this.logicalPlan() );
                        SeekableArgs var20 = this.seek$1.nodeIds();
                        Object var3;
                        if ( var20 instanceof SingleSeekableArg )
                        {
                            SingleSeekableArg var21 = (SingleSeekableArg) var20;
                            Expression e = var21.expr();
                            var3 = new SeekNodeById( opName, nodeVar, ExpressionConverter$.MODULE$.createExpression( e, context ), actionsx );
                        }
                        else
                        {
                            if ( !(var20 instanceof ManySeekableArgs) )
                            {
                                throw new MatchError( var20 );
                            }

                            ManySeekableArgs var23 = (ManySeekableArgs) var20;
                            Expression ex = var23.expr();
                            Object var4;
                            if ( ex instanceof ListLiteral )
                            {
                                ListLiteral var26 = (ListLiteral) ex;
                                ZeroOneOrMany var27 = org.neo4j.cypher.internal.v4_0.util.ZeroOneOrMany..MODULE$.apply( var26.expressions() );
                                Object var5;
                                if ( var27 instanceof One )
                                {
                                    One var28 = (One) var27;
                                    Expression value = (Expression) var28.value();
                                    var5 = new SeekNodeById( opName, nodeVar, ExpressionConverter$.MODULE$.createExpression( value, context ), actionsx );
                                }
                                else
                                {
                                    CodeGenExpression expression = ExpressionConverter$.MODULE$.createExpression( ex, context );
                                    Variable expressionVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.Any(), false );
                                    var5 = new ForEachExpression( expressionVar, expression,
                                            new SeekNodeById( opName, nodeVar, new LoadVariable( expressionVar ), actionsx ) );
                                }

                                var4 = var5;
                            }
                            else
                            {
                                ToSet expressionx = new ToSet( ExpressionConverter$.MODULE$.createExpression( ex, context ) );
                                Variable expressionVarx = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.Any(), false );
                                var4 = new ForEachExpression( expressionVarx, expressionx,
                                        new SeekNodeById( opName, nodeVar, new LoadVariable( expressionVarx ), actionsx ) );
                            }

                            var3 = var4;
                        }

                        return new Tuple2( methodHandlex, tlx.$colon$colon( var3 ) );
                    }
                }

                throw new MatchError( var9 );
            }
        };
    }

    private LeafCodeGenPlan nodeIndexSeekAsCodeGenPlan( final NodeIndexSeek indexSeek )
    {
        return this.sharedIndexSeekAsCodeGenPlan( ( opName, descriptorVar, expression, nodeVar, actions ) -> {
            return indexSeekFun$1( opName, descriptorVar, expression, nodeVar, actions, indexSeek );
        }, indexSeek.idName(), indexSeek.valueExpr(), indexSeek );
    }

    private LeafCodeGenPlan nodeUniqueIndexSeekAsCodeGen( final NodeUniqueIndexSeek indexSeek )
    {
        return this.sharedIndexSeekAsCodeGenPlan( ( opName, descriptorVar, expression, nodeVar, actions ) -> {
            return indexSeekFun$3( opName, descriptorVar, expression, nodeVar, actions, indexSeek );
        }, indexSeek.idName(), indexSeek.valueExpr(), indexSeek );
    }

    private CodeGenPlan nodeHashJoinAsCodeGenPlan( final NodeHashJoin nodeHashJoin )
    {
        return new CodeGenPlan( nodeHashJoin )
        {
            private final LogicalPlan logicalPlan;
            private final NodeHashJoin nodeHashJoin$1;

            public
            {
                this.nodeHashJoin$1 = nodeHashJoin$1;
                this.logicalPlan = nodeHashJoin$1;
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                context.pushParent( this );
                Tuple2 var6 = LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.logicalPlan().lhs().get() ).produce( context, cardinalities );
                if ( var6 != null )
                {
                    Option var7 = (Option) var6._1();
                    List leftInstructionsx = (List) var6._2();
                    if ( var7 instanceof Some )
                    {
                        Some var9 = (Some) var7;
                        JoinTableMethod symbolx = (JoinTableMethod) var9.value();
                        Tuple2 var4 = new Tuple2( symbolx, leftInstructionsx );
                        JoinTableMethod symbol = (JoinTableMethod) var4._1();
                        List leftInstructions = (List) var4._2();
                        String opName = context.registerOperator( this.logicalPlan() );
                        MethodInvocation lhsMethod = new MethodInvocation( (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new String[]{opName}) )),symbol, context.namer().newMethodName(), leftInstructions);
                        context.pushParent( this );
                        Tuple2 var16 =
                                LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.logicalPlan().rhs().get() ).produce( context, cardinalities );
                        if ( var16 != null )
                        {
                            Option otherSymbol = (Option) var16._1();
                            List rightInstructions = (List) var16._2();
                            Tuple2 var3 = new Tuple2( otherSymbol, rightInstructions );
                            Option otherSymbolx = (Option) var3._1();
                            List rightInstructionsx = (List) var3._2();
                            return new Tuple2( otherSymbolx, rightInstructionsx.$colon$colon( lhsMethod ) );
                        }

                        throw new MatchError( var16 );
                    }
                }

                throw new MatchError( var6 );
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                IndexedSeq nodes = this.nodeHashJoin$1.nodes().toIndexedSeq();
                Tuple2 var10000;
                if ( child.logicalPlan() == this.logicalPlan().lhs().get() )
                {
                    IndexedSeq joinNodes = (IndexedSeq) nodes.map( ( n ) -> {
                        return context.getVariable( n );
                    }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
                    String probeTableName = context.namer().newVarName();
                    Set lhsSymbols = this.nodeHashJoin$1.left().availableSymbols();
                    Set nodeNames = this.nodeHashJoin$1.nodes();
                    Set notNodeSymbols = (Set) ((SetLike) lhsSymbols.intersect( context.variableQueryVariables() )).diff( nodeNames );
                    Map symbols = ((TraversableOnce) notNodeSymbols.map( ( s ) -> {
                        return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( s ), context.getVariable( s ));
                    }, scala.collection.immutable.Set..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
                    String opName = context.registerOperator( this.nodeHashJoin$1 );
                    BuildProbeTable probeTable = BuildProbeTable$.MODULE$.apply( opName, probeTableName, joinNodes, symbols, context );
                    JoinTableMethod probeTableSymbol = new JoinTableMethod( probeTableName, probeTable.tableType() );
                    context.addProbeTable( this, probeTable.joinData() );
                    var10000 = new Tuple2( new Some( probeTableSymbol ), new colon( probeTable, scala.collection.immutable.Nil..MODULE$ ));
                }
                else
                {
                    if ( child.logicalPlan() != this.logicalPlan().rhs().get() )
                    {
                        throw new InternalException( (new StringBuilder( 27 )).append( "Unexpected consume call by " ).append( child ).toString() );
                    }

                    IndexedSeq joinNodesx = (IndexedSeq) nodes.map( ( n ) -> {
                        return context.getVariable( n );
                    }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
                    JoinData joinData = context.getProbeTable( this );
                    joinData.vars().foreach( ( x0$1 ) -> {
                        $anonfun$consume$7( context, x0$1 );
                        return BoxedUnit.UNIT;
                    } );
                    Tuple2 var18 = context.popParent().consume( context, this, cardinalities );
                    if ( var18 == null )
                    {
                        throw new MatchError( var18 );
                    }

                    Option methodHandlex = (Option) var18._1();
                    List var20 = (List) var18._2();
                    if ( !(var20 instanceof colon) )
                    {
                        throw new MatchError( var18 );
                    }

                    colon var21 = (colon) var20;
                    Instruction actions = (Instruction) var21.head();
                    List tl = var21.tl$access$1();
                    Tuple3 var4 = new Tuple3( methodHandlex, actions, tl );
                    Option methodHandle = (Option) var4._1();
                    Instruction actionsx = (Instruction) var4._2();
                    List tlx = (List) var4._3();
                    GetMatchesFromProbeTable var27 = new GetMatchesFromProbeTable( joinNodesx, joinData, actionsx );
                    var10000 = new Tuple2( methodHandle, tlx.$colon$colon( var27 ) );
                }

                return var10000;
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan expandAsCodeGenPlan( final Expand expand )
    {
        return new LogicalPlanConverter.SingleChildPlan( expand )
        {
            private final LogicalPlan logicalPlan;
            private final Expand expand$1;

            public
            {
                this.expand$1 = expand$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = expand$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                ExpansionMode var5 = this.expand$1.mode();
                Tuple2 var4;
                if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( var5 )){
                var4 = this.expandAllConsume( context, child, cardinalities );
            } else{
                if ( !org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$.equals( var5 )){
                    throw new MatchError( var5 );
                }

                var4 = this.expandIntoConsume( context, child, cardinalities );
            }

                return var4;
            }

            private Tuple2<Option<JoinTableMethod>,List<Instruction>> expandAllConsume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                Variable relVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveRel(), Variable$.MODULE$.apply$default$3() );
                Variable fromNodeVar = context.getVariable( this.expand$1.from() );
                Variable toNodeVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveNode(), Variable$.MODULE$.apply$default$3() );
                context.addVariable( this.expand$1.relName(), relVar );
                context.addVariable( this.expand$1.to(), toNodeVar );
                Tuple2 var9 = context.popParent().consume( context, this, cardinalities );
                if ( var9 != null )
                {
                    Option methodHandlex = (Option) var9._1();
                    List var11 = (List) var9._2();
                    if ( var11 instanceof colon )
                    {
                        colon var12 = (colon) var11;
                        Instruction action = (Instruction) var12.head();
                        List tl = var12.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandlex, action, tl );
                        Option methodHandle = (Option) var4._1();
                        Instruction actionx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        Map typeVar2TypeName = ((TraversableOnce) this.expand$1.types().map( ( t ) -> {
                            return scala.Predef.ArrowAssoc..
                            MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( context.namer().newVarName() ), t.name());
                        }, scala.collection.Seq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
                        String opName = context.registerOperator( this.expand$1 );
                        ExpandAllLoopDataGenerator expandGenerator =
                                new ExpandAllLoopDataGenerator( opName, fromNodeVar, this.expand$1.dir(), typeVar2TypeName, toNodeVar, relVar );
                        WhileLoop var21 = new WhileLoop( relVar, expandGenerator, actionx );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var21 ) );
                    }
                }

                throw new MatchError( var9 );
            }

            private Tuple2<Option<JoinTableMethod>,List<Instruction>> expandIntoConsume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                Variable relVar = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveRel(), Variable$.MODULE$.apply$default$3() );
                context.addVariable( this.expand$1.relName(), relVar );
                Variable fromNodeVar = context.getVariable( this.expand$1.from() );
                Variable toNodeVar = context.getVariable( this.expand$1.to() );
                Tuple2 var9 = context.popParent().consume( context, this, cardinalities );
                if ( var9 != null )
                {
                    Option methodHandlex = (Option) var9._1();
                    List var11 = (List) var9._2();
                    if ( var11 instanceof colon )
                    {
                        colon var12 = (colon) var11;
                        Instruction action = (Instruction) var12.head();
                        List tl = var12.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandlex, action, tl );
                        Option methodHandle = (Option) var4._1();
                        Instruction actionx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        Map typeVar2TypeName = ((TraversableOnce) this.expand$1.types().map( ( t ) -> {
                            return scala.Predef.ArrowAssoc..
                            MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( context.namer().newVarName() ), t.name());
                        }, scala.collection.Seq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
                        String opName = context.registerOperator( this.expand$1 );
                        ExpandIntoLoopDataGenerator expandGenerator =
                                new ExpandIntoLoopDataGenerator( opName, fromNodeVar, this.expand$1.dir(), typeVar2TypeName, toNodeVar, relVar );
                        WhileLoop var21 = new WhileLoop( relVar, expandGenerator, actionx );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var21 ) );
                    }
                }

                throw new MatchError( var9 );
            }
        };
    }

    private CodeGenPlan cartesianProductAsCodeGenPlan( final CartesianProduct cartesianProduct )
    {
        return new CodeGenPlan( cartesianProduct )
        {
            private final LogicalPlan logicalPlan;
            private final CartesianProduct cartesianProduct$1;

            public
            {
                this.cartesianProduct$1 = cartesianProduct$1;
                this.logicalPlan = cartesianProduct$1;
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                context.pushParent( this );
                return LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.cartesianProduct$1.lhs().get() ).produce( context, cardinalities );
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                Tuple2 var10000;
                if ( child.logicalPlan() == this.cartesianProduct$1.lhs().get() )
                {
                    context.pushParent( this );
                    Tuple2 var7 =
                            LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.cartesianProduct$1.rhs().get() ).produce( context, cardinalities );
                    if ( var7 == null )
                    {
                        throw new MatchError( var7 );
                    }
                    else
                    {
                        Option mx = (Option) var7._1();
                        List actionsx = (List) var7._2();
                        Tuple2 var5 = new Tuple2( mx, actionsx );
                        Option mxxx = (Option) var5._1();
                        List actions = (List) var5._2();
                        if ( actions.isEmpty() )
                        {
                            throw new InternalException( "Illegal call chain" );
                        }
                        else
                        {
                            var10000 = new Tuple2( mxxx, actions );
                            return var10000;
                        }
                    }
                }
                else if ( child.logicalPlan() != this.cartesianProduct$1.rhs().get() )
                {
                    throw new InternalException( (new StringBuilder( 27 )).append( "Unexpected consume call by " ).append( child ).toString() );
                }
                else
                {
                    String opName = context.registerOperator( this.cartesianProduct$1 );
                    Tuple2 var14 = context.popParent().consume( context, this, cardinalities );
                    if ( var14 != null )
                    {
                        Option m = (Option) var14._1();
                        List var16 = (List) var14._2();
                        if ( var16 instanceof colon )
                        {
                            colon var17 = (colon) var16;
                            Instruction instruction = (Instruction) var17.head();
                            List tlx = var17.tl$access$1();
                            Tuple3 var4 = new Tuple3( m, instruction, tlx );
                            Option mxx = (Option) var4._1();
                            Instruction instructionx = (Instruction) var4._2();
                            List tl = (List) var4._3();
                            CartesianProductInstruction var23 = new CartesianProductInstruction( opName, instructionx );
                            var10000 = new Tuple2( mxx, tl.$colon$colon( var23 ) );
                            return var10000;
                        }
                    }

                    throw new MatchError( var14 );
                }
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan selectionAsCodeGenPlan( final Selection selection )
    {
        return new LogicalPlanConverter.SingleChildPlan( selection )
        {
            private final LogicalPlan logicalPlan;
            private final Selection selection$1;

            public
            {
                this.selection$1 = selection$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = selection$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String opName = context.registerOperator( this.selection$1 );
                Seq predicates = ((SetLike) this.selection$1.predicate().exprs().map( ( x$26 ) -> {
                    return ExpressionConverter$.MODULE$.createPredicate( x$26, context );
                }, scala.collection.immutable.Set..MODULE$.canBuildFrom())).toSeq();
                Tuple2 var8 = context.popParent().consume( context, this, cardinalities );
                if ( var8 != null )
                {
                    Option methodHandlex = (Option) var8._1();
                    List var10 = (List) var8._2();
                    if ( var10 instanceof colon )
                    {
                        colon var11 = (colon) var10;
                        Instruction innerBlock = (Instruction) var11.head();
                        List tl = var11.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandlex, innerBlock, tl );
                        Option methodHandle = (Option) var4._1();
                        Instruction innerBlockx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        Instruction instruction = (Instruction) ((TraversableOnce) predicates.reverse()).foldLeft( innerBlockx, ( x0$2, x1$1 ) -> {
                            Tuple2 var3 = new Tuple2( x0$2, x1$1 );
                            if ( var3 != null )
                            {
                                Instruction acc = (Instruction) var3._1();
                                CodeGenExpression predicate = (CodeGenExpression) var3._2();
                                If var2 = new If( predicate, acc );
                                return var2;
                            }
                            else
                            {
                                throw new MatchError( var3 );
                            }
                        } );
                        SelectionInstruction var18 = new SelectionInstruction( opName, instruction );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var18 ) );
                    }
                }

                throw new MatchError( var8 );
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan limitAsCodeGenPlan( final Limit limit )
    {
        return new LogicalPlanConverter.SingleChildPlan( limit )
        {
            private final LogicalPlan logicalPlan;
            private final Limit limit$1;

            public
            {
                this.limit$1 = limit$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = limit$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String opName = context.registerOperator( this.limit$1 );
                CodeGenExpression count = ExpressionConverter$.MODULE$.createExpression( this.limit$1.count(), context );
                String counterName = context.namer().newVarName();
                Tuple2 var9 = context.popParent().consume( context, this, cardinalities );
                if ( var9 != null )
                {
                    Option methodHandlex = (Option) var9._1();
                    List var11 = (List) var9._2();
                    if ( var11 instanceof colon )
                    {
                        colon var12 = (colon) var11;
                        Instruction innerBlock = (Instruction) var12.head();
                        List tl = var12.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandlex, innerBlock, tl );
                        Option methodHandle = (Option) var4._1();
                        Instruction innerBlockx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        DecreaseAndReturnWhenZero instruction = new DecreaseAndReturnWhenZero( opName, counterName, innerBlockx, count );
                        return new Tuple2( methodHandle, tlx.$colon$colon( instruction ) );
                    }
                }

                throw new MatchError( var9 );
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan skipAsCodeGenPlan( final Skip skip )
    {
        return new LogicalPlanConverter.SingleChildPlan( skip )
        {
            private final LogicalPlan logicalPlan;
            private final Skip skip$1;

            public
            {
                this.skip$1 = skip$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = skip$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String opName = context.registerOperator( this.skip$1 );
                CodeGenExpression numberToSkip = ExpressionConverter$.MODULE$.createExpression( this.skip$1.count(), context );
                String counterName = context.namer().newVarName();
                Tuple2 var9 = context.popParent().consume( context, this, cardinalities );
                if ( var9 != null )
                {
                    Option methodHandlex = (Option) var9._1();
                    List var11 = (List) var9._2();
                    if ( var11 instanceof colon )
                    {
                        colon var12 = (colon) var11;
                        Instruction innerBlock = (Instruction) var12.head();
                        List tl = var12.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandlex, innerBlock, tl );
                        Option methodHandle = (Option) var4._1();
                        Instruction innerBlockx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        SkipInstruction instruction = new SkipInstruction( opName, counterName, innerBlockx, numberToSkip );
                        return new Tuple2( methodHandle, tlx.$colon$colon( instruction ) );
                    }
                }

                throw new MatchError( var9 );
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan aggregationAsCodeGenPlan( final Aggregation aggregation )
    {
        return new LogicalPlanConverter.SingleChildPlan( aggregation )
        {
            private final LogicalPlan logicalPlan;
            private final Aggregation aggregation$1;

            public
            {
                this.aggregation$1 = aggregation$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = aggregation$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String opName = context.registerOperator( this.aggregation$1 );
                Map groupingVariables = (Map) this.aggregation$1.groupingExpressions().map( ( x0$5 ) -> {
                    if ( x0$5 != null )
                    {
                        String name = (String) x0$5._1();
                        Expression e = (Expression) x0$5._2();
                        CodeGenExpression expr = ExpressionConverter$.MODULE$.createExpression( e, context );
                        Variable variable = new Variable( context.namer().newVarName(), expr.codeGenType( context ), expr.nullable( context ) );
                        context.addVariable( name, variable );
                        context.addProjectedVariable( name, variable );
                        Tuple2 var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( variable.name() ), expr);
                        return var3;
                    }
                    else
                    {
                        throw new MatchError( x0$5 );
                    }
                }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                context.retainProjectedVariables( this.aggregation$1.groupingExpressions().keySet() );
                Iterable aggregationExpression = this.aggregation$1.aggregationExpression().isEmpty() ? (Iterable) scala.collection.Seq..
                MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.Distinct[]{
                        new org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.Distinct( opName, context.namer().newVarName(),
                                groupingVariables )}) )) :(Iterable) this.aggregation$1.aggregationExpression().map( ( x0$6 ) -> {
                if ( x0$6 != null )
                {
                    String name = (String) x0$6._1();
                    Expression e = (Expression) x0$6._2();
                    AggregateExpression var4 = AggregationConverter$.MODULE$.aggregateExpressionConverter( opName, groupingVariables, name, e, context );
                    return var4;
                }
                else
                {
                    throw new MatchError( x0$6 );
                }
            }, scala.collection.immutable.Iterable..MODULE$.canBuildFrom());
                AggregationInstruction instruction = new AggregationInstruction( opName, aggregationExpression );
                Tuple2 var11 = context.popParent().consume( context, this, cardinalities );
                if ( var11 != null )
                {
                    Option methodHandle = (Option) var11._1();
                    List var13 = (List) var11._2();
                    if ( var13 instanceof colon )
                    {
                        colon var14 = (colon) var13;
                        Instruction innerBlock = (Instruction) var14.head();
                        List tl = var14.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandle, innerBlock, tl );
                        Option methodHandlex = (Option) var4._1();
                        Instruction innerBlockx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        Instruction continuation = (Instruction) aggregationExpression.foldLeft( innerBlockx, ( x0$7, x1$2 ) -> {
                            Tuple2 var3 = new Tuple2( x0$7, x1$2 );
                            if ( var3 != null )
                            {
                                Instruction acc = (Instruction) var3._1();
                                AggregateExpression curr = (AggregateExpression) var3._2();
                                Instruction var2 = curr.continuation( acc );
                                return var2;
                            }
                            else
                            {
                                throw new MatchError( var3 );
                            }
                        } );
                        return new Tuple2( methodHandlex, tlx.$colon$colon( continuation ).$colon$colon( instruction ) );
                    }
                }

                throw new MatchError( var11 );
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan distinctAsCodeGenPlan( final Distinct distinct )
    {
        return new LogicalPlanConverter.SingleChildPlan( distinct )
        {
            private final LogicalPlan logicalPlan;
            private final Distinct distinct$1;

            public
            {
                this.distinct$1 = distinct$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = distinct$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String opName = context.registerOperator( this.distinct$1 );
                Map groupingVariables = (Map) this.distinct$1.groupingExpressions().map( ( x0$8 ) -> {
                    if ( x0$8 != null )
                    {
                        String name = (String) x0$8._1();
                        Expression e = (Expression) x0$8._2();
                        CodeGenExpression expr = ExpressionConverter$.MODULE$.createExpression( e, context );
                        Variable variable = new Variable( context.namer().newVarName(), expr.codeGenType( context ), expr.nullable( context ) );
                        context.addVariable( name, variable );
                        context.addProjectedVariable( name, variable );
                        Tuple2 var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( variable.name() ), expr);
                        return var3;
                    }
                    else
                    {
                        throw new MatchError( x0$8 );
                    }
                }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                context.retainProjectedVariables( this.distinct$1.groupingExpressions().keySet() );
                Seq distinctExpression = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.Distinct[]{
                            new org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.Distinct( opName, context.namer().newVarName(),
                                    groupingVariables )}) ));
                AggregationInstruction instruction = new AggregationInstruction( opName, distinctExpression );
                Tuple2 var11 = context.popParent().consume( context, this, cardinalities );
                if ( var11 != null )
                {
                    Option methodHandle = (Option) var11._1();
                    List var13 = (List) var11._2();
                    if ( var13 instanceof colon )
                    {
                        colon var14 = (colon) var13;
                        Instruction innerBlock = (Instruction) var14.head();
                        List tl = var14.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandle, innerBlock, tl );
                        Option methodHandlex = (Option) var4._1();
                        Instruction innerBlockx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        Instruction continuation = (Instruction) distinctExpression.foldLeft( innerBlockx, ( x0$9, x1$3 ) -> {
                            Tuple2 var3 = new Tuple2( x0$9, x1$3 );
                            if ( var3 != null )
                            {
                                Instruction acc = (Instruction) var3._1();
                                org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.Distinct curr =
                                        (org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.Distinct) var3._2();
                                Instruction var2 = curr.continuation( acc );
                                return var2;
                            }
                            else
                            {
                                throw new MatchError( var3 );
                            }
                        } );
                        return new Tuple2( methodHandlex, tlx.$colon$colon( continuation ).$colon$colon( instruction ) );
                    }
                }

                throw new MatchError( var11 );
            }
        };
    }

    private LeafCodeGenPlan nodeCountFromCountStore( final NodeCountFromCountStore nodeCount )
    {
        return new LeafCodeGenPlan( nodeCount )
        {
            private final LogicalPlan logicalPlan;
            private final NodeCountFromCountStore nodeCount$1;

            public
            {
                this.nodeCount$1 = nodeCount$1;
                LeafCodeGenPlan.$init$( this );
                this.logicalPlan = nodeCount$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                return LeafCodeGenPlan.consume$( this, context, child, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                Variable variable = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveInt(), Variable$.MODULE$.apply$default$3() );
                context.addVariable( this.nodeCount$1.idName(), variable );
                context.retainProjectedVariables( scala.Predef..MODULE$.Set().empty());
                context.addProjectedVariable( this.nodeCount$1.idName(), variable );
                Tuple2 var6 = context.popParent().consume( context, this, cardinalities );
                if ( var6 != null )
                {
                    Option methodHandlex = (Option) var6._1();
                    List var8 = (List) var6._2();
                    if ( var8 instanceof colon )
                    {
                        colon var9 = (colon) var8;
                        Instruction actionsx = (Instruction) var9.head();
                        List tl = var9.tl$access$1();
                        Tuple3 var3 = new Tuple3( methodHandlex, actionsx, tl );
                        Option methodHandle = (Option) var3._1();
                        Instruction actions = (Instruction) var3._2();
                        List tlx = (List) var3._3();
                        String opName = context.registerOperator( this.logicalPlan() );
                        List label = (List) this.nodeCount$1.labelNames().map( ( ll ) -> {
                            return ll.map( ( l ) -> {
                                return scala.Predef.ArrowAssoc..
                                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( context.semanticTable().id( l ).map( ( x$40 ) -> {
                                    return BoxesRunTime.boxToInteger( $anonfun$produce$3( x$40 ) );
                                } ) ), l.name());
                            } );
                        }, scala.collection.immutable.List..MODULE$.canBuildFrom());
                        NodeCountFromCountStoreInstruction var17 = new NodeCountFromCountStoreInstruction( opName, variable, label, actions );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var17 ) );
                    }
                }

                throw new MatchError( var6 );
            }
        };
    }

    private LeafCodeGenPlan relCountFromCountStore( final RelationshipCountFromCountStore relCount )
    {
        return new LeafCodeGenPlan( relCount )
        {
            private final LogicalPlan logicalPlan;
            private final RelationshipCountFromCountStore relCount$1;

            public
            {
                this.relCount$1 = relCount$1;
                LeafCodeGenPlan.$init$( this );
                this.logicalPlan = relCount$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                return LeafCodeGenPlan.consume$( this, context, child, cardinalities );
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                Variable variable = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveInt(), Variable$.MODULE$.apply$default$3() );
                context.addVariable( this.relCount$1.idName(), variable );
                context.retainProjectedVariables( scala.Predef..MODULE$.Set().empty());
                context.addProjectedVariable( this.relCount$1.idName(), variable );
                Tuple2 var6 = context.popParent().consume( context, this, cardinalities );
                if ( var6 != null )
                {
                    Option methodHandlex = (Option) var6._1();
                    List var8 = (List) var6._2();
                    if ( var8 instanceof colon )
                    {
                        colon var9 = (colon) var8;
                        Instruction actionsx = (Instruction) var9.head();
                        List tl = var9.tl$access$1();
                        Tuple3 var3 = new Tuple3( methodHandlex, actionsx, tl );
                        Option methodHandle = (Option) var3._1();
                        Instruction actions = (Instruction) var3._2();
                        List tlx = (List) var3._3();
                        String opName = context.registerOperator( this.logicalPlan() );
                        Option startLabel = this.relCount$1.startLabel().map( ( l ) -> {
                            return scala.Predef.ArrowAssoc..
                            MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( context.semanticTable().id( l ).map( ( x$43 ) -> {
                                return BoxesRunTime.boxToInteger( $anonfun$produce$5( x$43 ) );
                            } ) ), l.name());
                        } ); Option endLabel = this.relCount$1.endLabel().map( ( l ) -> {
                        return scala.Predef.ArrowAssoc..
                        MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( context.semanticTable().id( l ).map( ( x$44 ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$produce$7( x$44 ) );
                        } ) ), l.name());
                    } ); Seq types = (Seq) this.relCount$1.typeNames().map( ( t ) -> {
                        return scala.Predef.ArrowAssoc..
                        MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( context.semanticTable().id( t ).map( ( x$45 ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$produce$9( x$45 ) );
                        } ) ), t.name());
                    }, scala.collection.Seq..MODULE$.canBuildFrom());
                        RelationshipCountFromCountStoreInstruction var19 =
                                new RelationshipCountFromCountStoreInstruction( opName, variable, startLabel, types, endLabel, actions );
                        return new Tuple2( methodHandle, tlx.$colon$colon( var19 ) );
                    }
                }

                throw new MatchError( var6 );
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan unwindAsCodeGenPlan( final UnwindCollection unwind )
    {
        return new LogicalPlanConverter.SingleChildPlan( unwind )
        {
            private final UnwindCollection logicalPlan;
            private final UnwindCollection unwind$1;

            public
            {
                this.unwind$1 = unwind$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = unwind$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public UnwindCollection logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                CypherCodeGenType var5;
                CodeGenExpression collection;
                String opName;
                label57:
                {
                    collection = ExpressionConverter$.MODULE$.createExpression( this.unwind$1.expression(), context );
                    CypherCodeGenType collectionCodeGenType = collection.codeGenType( context );
                    opName = context.registerOperator( this.logicalPlan() );
                    if ( collectionCodeGenType != null )
                    {
                        CypherType var11 = collectionCodeGenType.ct();
                        RepresentationType var12 = collectionCodeGenType.repr();
                        Option var13 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var11 );
                        if ( !var13.isEmpty() )
                        {
                            CypherType innerType = (CypherType) var13.get();
                            if ( var12 instanceof ListReferenceType )
                            {
                                ListReferenceType var15 = (ListReferenceType) var12;
                                RepresentationType innerReprType = var15.inner();
                                var5 = new CypherCodeGenType( innerType, innerReprType );
                                break label57;
                            }
                        }
                    }

                    if ( collectionCodeGenType != null )
                    {
                        CypherType var17 = collectionCodeGenType.ct();
                        Option var18 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var17 );
                        if ( !var18.isEmpty() )
                        {
                            CypherType innerTypex = (CypherType) var18.get();
                            var5 = new CypherCodeGenType( innerTypex, ReferenceType$.MODULE$ );
                            break label57;
                        }
                    }

                    if ( collectionCodeGenType == null )
                    {
                        throw new CantCompileQueryException(
                                (new StringBuilder( 37 )).append( "Unwind collection type " ).append( collectionCodeGenType ).append(
                                        " not supported" ).toString() );
                    }

                    CypherType var20 = collectionCodeGenType.ct();
                    AnyType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
                    if ( var10000 == null )
                    {
                        if ( var20 != null )
                        {
                            throw new CantCompileQueryException(
                                    (new StringBuilder( 37 )).append( "Unwind collection type " ).append( collectionCodeGenType ).append(
                                            " not supported" ).toString() );
                        }
                    }
                    else if ( !var10000.equals( var20 ) )
                    {
                        throw new CantCompileQueryException(
                                (new StringBuilder( 37 )).append( "Unwind collection type " ).append( collectionCodeGenType ).append(
                                        " not supported" ).toString() );
                    }

                    var5 = new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny(), ReferenceType$.MODULE$);
                }

                Product loopDataGenerator = var5.isPrimitive() ? new UnwindPrimitiveCollection( opName, collection )
                                                               : new org.neo4j.cypher.internal.runtime.compiled.codegen.ir.UnwindCollection( opName, collection,
                                                                       var5 );
                String variableName = context.namer().newVarName();
                Variable variable = new Variable( variableName, var5, var5.canBeNullable() );
                context.addVariable( this.unwind$1.variable(), variable );
                context.addProjectedVariable( this.unwind$1.variable(), variable );
                Tuple2 var26 = context.popParent().consume( context, this, cardinalities );
                if ( var26 != null )
                {
                    Option methodHandle = (Option) var26._1();
                    List var28 = (List) var26._2();
                    if ( var28 instanceof colon )
                    {
                        colon var29 = (colon) var28;
                        Instruction actions = (Instruction) var29.head();
                        List tl = var29.tl$access$1();
                        Tuple3 var4 = new Tuple3( methodHandle, actions, tl );
                        Option methodHandlex = (Option) var4._1();
                        Instruction actionsx = (Instruction) var4._2();
                        List tlx = (List) var4._3();
                        WhileLoop var35 = new WhileLoop( variable, (LoopDataGenerator) loopDataGenerator, actionsx );
                        return new Tuple2( methodHandlex, tlx.$colon$colon( var35 ) );
                    }
                }

                throw new MatchError( var26 );
            }
        };
    }

    private CodeGenPlan applyAsCodeGenPlan( final Apply apply )
    {
        return new CodeGenPlan( apply )
        {
            private final LogicalPlan logicalPlan;
            private final Apply apply$1;

            public
            {
                this.apply$1 = apply$1;
                this.logicalPlan = apply$1;
            }

            public LogicalPlan logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                context.pushParent( this );
                return LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.apply$1.lhs().get() ).produce( context, cardinalities );
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                Tuple2 var10000;
                if ( child.logicalPlan() == this.apply$1.lhs().get() )
                {
                    context.pushParent( this );
                    Tuple2 var7 = LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.apply$1.rhs().get() ).produce( context, cardinalities );
                    if ( var7 == null )
                    {
                        throw new MatchError( var7 );
                    }
                    else
                    {
                        Option mx = (Option) var7._1();
                        List actionsx = (List) var7._2();
                        Tuple2 var5 = new Tuple2( mx, actionsx );
                        Option mxxx = (Option) var5._1();
                        List actions = (List) var5._2();
                        if ( actions.isEmpty() )
                        {
                            throw new InternalException( "Illegal call chain" );
                        }
                        else
                        {
                            var10000 = new Tuple2( mxxx, actions );
                            return var10000;
                        }
                    }
                }
                else if ( child.logicalPlan() != this.apply$1.rhs().get() )
                {
                    throw new InternalException( (new StringBuilder( 27 )).append( "Unexpected consume call by " ).append( child ).toString() );
                }
                else
                {
                    String opName = context.registerOperator( this.apply$1 );
                    Tuple2 var14 = context.popParent().consume( context, this, cardinalities );
                    if ( var14 != null )
                    {
                        Option m = (Option) var14._1();
                        List var16 = (List) var14._2();
                        if ( var16 instanceof colon )
                        {
                            colon var17 = (colon) var16;
                            Instruction instruction = (Instruction) var17.head();
                            List tlx = var17.tl$access$1();
                            Tuple3 var4 = new Tuple3( m, instruction, tlx );
                            Option mxx = (Option) var4._1();
                            Instruction instructionx = (Instruction) var4._2();
                            List tl = (List) var4._3();
                            ApplyInstruction var23 = new ApplyInstruction( opName, instructionx );
                            var10000 = new Tuple2( mxx, tl.$colon$colon( var23 ) );
                            return var10000;
                        }
                    }

                    throw new MatchError( var14 );
                }
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan sortAsCodeGenPlan( final Sort sort )
    {
        return new LogicalPlanConverter.SingleChildPlan( sort )
        {
            private final Sort logicalPlan;
            private final Sort sort$1;

            public
            {
                this.sort$1 = sort$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = sort$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public Sort logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String opName = context.registerOperator( this.logicalPlan() );
                Tuple4 var8 =
                        LogicalPlanConverter$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$codegen$LogicalPlanConverter$$prepareSortTableInfo( context,
                                this.sort$1.availableSymbols(), this.sort$1.sortItems() );
                if ( var8 != null )
                {
                    Map variablesToKeep = (Map) var8._1();
                    Seq sortItems = (Seq) var8._2();
                    Map tupleVariables = (Map) var8._3();
                    String sortTableName = (String) var8._4();
                    if ( variablesToKeep != null && sortItems != null && tupleVariables != null && sortTableName != null )
                    {
                        Tuple4 var5 = new Tuple4( variablesToKeep, sortItems, tupleVariables, sortTableName );
                        Map variablesToKeepx = (Map) var5._1();
                        Seq sortItemsx = (Seq) var5._2();
                        Map tupleVariablesx = (Map) var5._3();
                        String sortTableNamex = (String) var5._4();
                        double estimatedCardinality = ((Cardinality) cardinalities.get( this.sort$1.id() )).amount();
                        BuildSortTable buildSortTableInstruction =
                                new BuildSortTable( opName, sortTableNamex, tupleVariablesx, sortItemsx, estimatedCardinality, context );
                        LogicalPlanConverter$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$codegen$LogicalPlanConverter$$updateContextWithSortTableInfo(
                                context, buildSortTableInstruction.sortTableInfo() );
                        Tuple2 var25 = context.popParent().consume( context, this, cardinalities );
                        if ( var25 != null )
                        {
                            Option methodHandle = (Option) var25._1();
                            List var27 = (List) var25._2();
                            if ( var27 instanceof colon )
                            {
                                colon var28 = (colon) var27;
                                Instruction innerBlock = (Instruction) var28.head();
                                List tl = var28.tl$access$1();
                                Tuple3 var4 = new Tuple3( methodHandle, innerBlock, tl );
                                Option methodHandlex = (Option) var4._1();
                                Instruction innerBlockx = (Instruction) var4._2();
                                List tlx = (List) var4._3();
                                SortInstruction sortInstruction = new SortInstruction( opName, buildSortTableInstruction.sortTableInfo() );
                                GetSortedResult continuation =
                                        new GetSortedResult( opName, variablesToKeepx, buildSortTableInstruction.sortTableInfo(), innerBlockx );
                                return new Tuple2( methodHandlex,
                                        tlx.$colon$colon( continuation ).$colon$colon( sortInstruction ).$colon$colon( buildSortTableInstruction ) );
                            }
                        }

                        throw new MatchError( var25 );
                    }
                }

                throw new MatchError( var8 );
            }
        };
    }

    private LogicalPlanConverter.SingleChildPlan topAsCodeGenPlan( final Top top )
    {
        return new LogicalPlanConverter.SingleChildPlan( top )
        {
            private final Top logicalPlan;
            private final Top top$1;

            public
            {
                this.top$1 = top$1;
                LogicalPlanConverter.SingleChildPlan.$init$( this );
                this.logicalPlan = top$1;
            }

            public final Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
            {
                return LogicalPlanConverter.SingleChildPlan.produce$( this, context, cardinalities );
            }

            public Top logicalPlan()
            {
                return this.logicalPlan;
            }

            public Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
                    final Cardinalities cardinalities )
            {
                String opName = context.registerOperator( this.logicalPlan() );
                Tuple4 var8 =
                        LogicalPlanConverter$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$codegen$LogicalPlanConverter$$prepareSortTableInfo( context,
                                this.top$1.availableSymbols(), this.top$1.sortItems() );
                if ( var8 != null )
                {
                    Map variablesToKeep = (Map) var8._1();
                    Seq sortItems = (Seq) var8._2();
                    Map tupleVariables = (Map) var8._3();
                    String sortTableName = (String) var8._4();
                    if ( variablesToKeep != null && sortItems != null && tupleVariables != null && sortTableName != null )
                    {
                        Tuple4 var5 = new Tuple4( variablesToKeep, sortItems, tupleVariables, sortTableName );
                        Map variablesToKeepx = (Map) var5._1();
                        Seq sortItemsx = (Seq) var5._2();
                        Map tupleVariablesx = (Map) var5._3();
                        String sortTableNamex = (String) var5._4();
                        CodeGenExpression countExpression = ExpressionConverter$.MODULE$.createExpression( this.top$1.limit(), context );
                        BuildTopTable buildTopTableInstruction =
                                new BuildTopTable( opName, sortTableNamex, countExpression, tupleVariablesx, sortItemsx, context );
                        LogicalPlanConverter$.MODULE$.org$neo4j$cypher$internal$runtime$compiled$codegen$LogicalPlanConverter$$updateContextWithSortTableInfo(
                                context, buildTopTableInstruction.sortTableInfo() );
                        Tuple2 var24 = context.popParent().consume( context, this, cardinalities );
                        if ( var24 != null )
                        {
                            Option methodHandle = (Option) var24._1();
                            List var26 = (List) var24._2();
                            if ( var26 instanceof colon )
                            {
                                colon var27 = (colon) var26;
                                Instruction innerBlock = (Instruction) var27.head();
                                List tl = var27.tl$access$1();
                                Tuple3 var4 = new Tuple3( methodHandle, innerBlock, tl );
                                Option methodHandlex = (Option) var4._1();
                                Instruction innerBlockx = (Instruction) var4._2();
                                List tlx = (List) var4._3();
                                SortInstruction sortInstruction = new SortInstruction( opName, buildTopTableInstruction.sortTableInfo() );
                                GetSortedResult continuation =
                                        new GetSortedResult( opName, variablesToKeepx, buildTopTableInstruction.sortTableInfo(), innerBlockx );
                                return new Tuple2( methodHandlex,
                                        tlx.$colon$colon( continuation ).$colon$colon( sortInstruction ).$colon$colon( buildTopTableInstruction ) );
                            }
                        }

                        throw new MatchError( var24 );
                    }
                }

                throw new MatchError( var8 );
            }
        };
    }

    public Tuple4<Map<String,Variable>,Seq<SortItem>,Map<String,Variable>,String> org$neo4j$cypher$internal$runtime$compiled$codegen$LogicalPlanConverter$$prepareSortTableInfo(
            final CodeGenContext context, final Set<String> available, final Seq<ColumnOrder> inputSortItems )
    {
        Map variablesToKeep = ((TraversableOnce) available.map( ( a ) -> {
            return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( a ), context.getVariable( a ));
        }, scala.collection.immutable.Set..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
        Seq sortItems = (Seq) inputSortItems.map( ( x0$10 ) -> {
            SortItem var2;
            if ( x0$10 instanceof Ascending )
            {
                Ascending var4 = (Ascending) x0$10;
                String namex = var4.id();
                var2 = new SortItem( context.getVariable( namex ).name(), Ascending$.MODULE$ );
            }
            else
            {
                if ( !(x0$10 instanceof Descending) )
                {
                    throw new MatchError( x0$10 );
                }

                Descending var6 = (Descending) x0$10;
                String name = var6.id();
                var2 = new SortItem( context.getVariable( name ).name(), Descending$.MODULE$ );
            }

            return var2;
        }, scala.collection.Seq..MODULE$.canBuildFrom());
        Seq additionalSortVariables = (Seq) inputSortItems.collect( new Serializable( context, variablesToKeep )
        {
            public static final long serialVersionUID = 0L;
            private final CodeGenContext context$5;
            private final Map variablesToKeep$1;

            public
            {
                this.context$5 = context$5;
                this.variablesToKeep$1 = variablesToKeep$1;
            }

            public final <A1 extends ColumnOrder, B1> B1 applyOrElse( final A1 x5, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x5 instanceof Ascending )
                {
                    Ascending var5 = (Ascending) x5;
                    String name = var5.id();
                    if ( !this.variablesToKeep$1.isDefinedAt( name ) )
                    {
                        var3 = new Tuple2( name, this.context$5.getVariable( name ) );
                        return var3;
                    }
                }

                if ( x5 instanceof Descending )
                {
                    Descending var7 = (Descending) x5;
                    String namex = var7.id();
                    if ( !this.variablesToKeep$1.isDefinedAt( namex ) )
                    {
                        var3 = new Tuple2( namex, this.context$5.getVariable( namex ) );
                        return var3;
                    }
                }

                var3 = var2.apply( x5 );
                return var3;
            }

            public final boolean isDefinedAt( final ColumnOrder x5 )
            {
                boolean var2;
                if ( x5 instanceof Ascending )
                {
                    Ascending var4 = (Ascending) x5;
                    String namex = var4.id();
                    if ( !this.variablesToKeep$1.isDefinedAt( namex ) )
                    {
                        var2 = true;
                        return var2;
                    }
                }

                if ( x5 instanceof Descending )
                {
                    Descending var6 = (Descending) x5;
                    String name = var6.id();
                    if ( !this.variablesToKeep$1.isDefinedAt( name ) )
                    {
                        var2 = true;
                        return var2;
                    }
                }

                var2 = false;
                return var2;
            }
        }, scala.collection.Seq..MODULE$.canBuildFrom());
        Map tupleVariables = variablesToKeep.$plus$plus( additionalSortVariables );
        String sortTableName = context.namer().newVarName();
        return new Tuple4( variablesToKeep, sortItems, tupleVariables, sortTableName );
    }

    public void org$neo4j$cypher$internal$runtime$compiled$codegen$LogicalPlanConverter$$updateContextWithSortTableInfo( final CodeGenContext context,
            final SortTableInfo sortTableInfo )
    {
        sortTableInfo.fieldToVariableInfo().foreach( ( x0$3 ) -> {
            $anonfun$updateContextWithSortTableInfo$1( context, x0$3 );
            return BoxedUnit.UNIT;
        } );
    }
}
