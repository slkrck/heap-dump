package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.runtime.compiled.helpers.LiteralTypeSupport$;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Predef.;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Literal implements CodeGenExpression, Product, Serializable
{
    private final Object value;

    public Literal( final Object value )
    {
        this.value = value;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Object> unapply( final Literal x$0 )
    {
        return Literal$.MODULE$.unapply( var0 );
    }

    public static Literal apply( final Object value )
    {
        return Literal$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Object,A> andThen( final Function1<Literal,A> g )
    {
        return Literal$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,Literal> compose( final Function1<A,Object> g )
    {
        return Literal$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Object value()
    {
        return this.value;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        var10000 = .MODULE$;
        Object var5 = this.value();
        boolean var3;
        if ( var5 instanceof Byte )
        {
            var3 = true;
        }
        else if ( var5 instanceof Short )
        {
            var3 = true;
        }
        else if ( var5 instanceof Character )
        {
            var3 = true;
        }
        else if ( var5 instanceof Integer )
        {
            var3 = true;
        }
        else if ( var5 instanceof Float )
        {
            var3 = true;
        }
        else
        {
            var3 = false;
        }

        var10000. assert (!var3);
        CypherCodeGenType ct = this.codeGenType( context );
        return this.value() == null ? structure.noValue()
                                    : (ct.isPrimitive() ? structure.constantExpression( this.value() ) : structure.constantValueExpression( this.value(), ct ));
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.value() == null;
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return LiteralTypeSupport$.MODULE$.deriveCodeGenType( this.value() );
    }

    public Literal copy( final Object value )
    {
        return new Literal( value );
    }

    public Object copy$default$1()
    {
        return this.value();
    }

    public String productPrefix()
    {
        return "Literal";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.value();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Literal;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label49:
            {
                boolean var2;
                if ( x$1 instanceof Literal )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    Literal var4 = (Literal) x$1;
                    if ( BoxesRunTime.equals( this.value(), var4.value() ) && var4.canEqual( this ) )
                    {
                        break label49;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
