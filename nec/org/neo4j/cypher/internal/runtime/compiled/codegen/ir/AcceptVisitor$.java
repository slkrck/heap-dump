package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction2;

public final class AcceptVisitor$ extends AbstractFunction2<String,Map<String,CodeGenExpression>,AcceptVisitor> implements Serializable
{
    public static AcceptVisitor$ MODULE$;

    static
    {
        new AcceptVisitor$();
    }

    private AcceptVisitor$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "AcceptVisitor";
    }

    public AcceptVisitor apply( final String produceResultOpName, final Map<String,CodeGenExpression> columns )
    {
        return new AcceptVisitor( produceResultOpName, columns );
    }

    public Option<Tuple2<String,Map<String,CodeGenExpression>>> unapply( final AcceptVisitor x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.produceResultOpName(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
