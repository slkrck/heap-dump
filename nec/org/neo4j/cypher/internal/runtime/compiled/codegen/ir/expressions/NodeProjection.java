package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeProjection implements CodeGenExpression, Product, Serializable
{
    private final Variable nodeIdVar;

    public NodeProjection( final Variable nodeIdVar )
    {
        scala.Predef.var10000;
        boolean var3;
        label17:
        {
            label16:
            {
                this.nodeIdVar = nodeIdVar;
                super();
                CodeGenExpression.$init$( this );
                Product.$init$( this );
                var10000 = scala.Predef..MODULE$;
                CypherType var10001 = ((CypherCodeGenType) nodeIdVar.codeGenType()).ct();
                NodeType var2 = .MODULE$.CTNode();
                if ( var10001 == null )
                {
                    if ( var2 == null )
                    {
                        break label16;
                    }
                }
                else if ( var10001.equals( var2 ) )
                {
                    break label16;
                }

                var3 = false;
                break label17;
            }

            var3 = true;
        }

        var10000. assert (var3);
    }

    public static Option<Variable> unapply( final NodeProjection x$0 )
    {
        return NodeProjection$.MODULE$.unapply( var0 );
    }

    public static NodeProjection apply( final Variable nodeIdVar )
    {
        return NodeProjection$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Variable,A> andThen( final Function1<NodeProjection,A> g )
    {
        return NodeProjection$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,NodeProjection> compose( final Function1<A,Variable> g )
    {
        return NodeProjection$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable nodeIdVar()
    {
        return this.nodeIdVar;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return this.nodeIdVar().nullable() ? structure.nullableReference( this.nodeIdVar().name(), CodeGenType$.MODULE$.primitiveNode(),
                structure.materializeNode( this.nodeIdVar().name(), this.nodeIdVar().codeGenType() ) )
                                           : structure.materializeNode( this.nodeIdVar().name(), this.nodeIdVar().codeGenType() );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.nodeIdVar().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return new CypherCodeGenType(.MODULE$.CTNode(),ReferenceType$.MODULE$);
    }

    public NodeProjection copy( final Variable nodeIdVar )
    {
        return new NodeProjection( nodeIdVar );
    }

    public Variable copy$default$1()
    {
        return this.nodeIdVar();
    }

    public String productPrefix()
    {
        return "NodeProjection";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.nodeIdVar();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NodeProjection;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof NodeProjection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        NodeProjection var4 = (NodeProjection) x$1;
                        Variable var10000 = this.nodeIdVar();
                        Variable var5 = var4.nodeIdVar();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
