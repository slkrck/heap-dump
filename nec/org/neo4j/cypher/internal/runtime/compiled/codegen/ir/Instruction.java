package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public interface Instruction
{
    static Instruction empty()
    {
        return Instruction$.MODULE$.empty();
    }

    static void $init$( final Instruction $this )
    {
    }

    default <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.children().foreach( ( x$1 ) -> {
            $anonfun$init$1( generator, context, x$1 );
            return BoxedUnit.UNIT;
        } );
    }

    <E> void body( final MethodStructure<E> generator, final CodeGenContext context );

    Seq<Instruction> children();

    private default Seq<Instruction> treeView()
    {
        return (Seq) this.children().foldLeft(.MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this}) )),( acc, child ) -> {
        return (Seq) acc.$plus$plus( child.treeView(),.MODULE$.canBuildFrom());
    });
    }

    default Set<String> allOperatorIds()
    {
        return ((TraversableOnce) this.treeView().flatMap( ( x$2 ) -> {
            return x$2.operatorId();
        },.MODULE$.canBuildFrom())).toSet();
    }

    default Set<String> operatorId()
    {
        return scala.Predef..MODULE$.Set().empty();
    }
}
