package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import scala.Function1;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class functionConverter
{
    public static CodeGenExpression apply( final FunctionInvocation fcn, final Function1<Expression,CodeGenExpression> callback, final CodeGenContext context )
    {
        return functionConverter$.MODULE$.apply( var0, var1, var2 );
    }
}
