package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ListReferenceType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.ListType.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class UnwindPrimitiveCollection implements LoopDataGenerator, Product, Serializable
{
    private final String opName;
    private final CodeGenExpression collection;

    public UnwindPrimitiveCollection( final String opName, final CodeGenExpression collection )
    {
        this.opName = opName;
        this.collection = collection;
        Product.$init$( this );
    }

    public static Option<Tuple2<String,CodeGenExpression>> unapply( final UnwindPrimitiveCollection x$0 )
    {
        return UnwindPrimitiveCollection$.MODULE$.unapply( var0 );
    }

    public static UnwindPrimitiveCollection apply( final String opName, final CodeGenExpression collection )
    {
        return UnwindPrimitiveCollection$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<String,CodeGenExpression>,UnwindPrimitiveCollection> tupled()
    {
        return UnwindPrimitiveCollection$.MODULE$.tupled();
    }

    public static Function1<String,Function1<CodeGenExpression,UnwindPrimitiveCollection>> curried()
    {
        return UnwindPrimitiveCollection$.MODULE$.curried();
    }

    public String opName()
    {
        return this.opName;
    }

    public CodeGenExpression collection()
    {
        return this.collection;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.collection().init( generator, context );
    }

    public <E> void produceLoopData( final String iterVar, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.declarePrimitiveIterator( iterVar, this.collection().codeGenType( context ) );
        Object iterator =
                generator.primitiveIteratorFrom( this.collection().generateExpression( generator, context ), this.collection().codeGenType( context ) );
        generator.assign( iterVar, CodeGenType$.MODULE$.Any(), iterator );
    }

    public <E> void getNext( final Variable nextVar, final String iterVar, final MethodStructure<E> generator, final CodeGenContext context )
    {
        CypherCodeGenType var7 = this.collection().codeGenType( context );
        if ( var7 != null )
        {
            CypherType var8 = var7.ct();
            RepresentationType var9 = var7.repr();
            Option var10 = .MODULE$.unapply( var8 );
            if ( !var10.isEmpty() )
            {
                CypherType innerCt = (CypherType) var10.get();
                if ( var9 instanceof ListReferenceType )
                {
                    ListReferenceType var12 = (ListReferenceType) var9;
                    RepresentationType innerRepr = var12.inner();
                    CypherCodeGenType var5 = new CypherCodeGenType( innerCt, innerRepr );
                    Object next = generator.primitiveIteratorNext( generator.loadVariable( iterVar ), this.collection().codeGenType( context ) );
                    generator.assign( nextVar.name(), var5, next );
                    return;
                }
            }
        }

        throw new IllegalArgumentException( (new StringBuilder( 60 )).append( "CodeGenType " ).append( this.collection() ).append(
                ".codeGenType not supported as primitive iterator" ).toString() );
    }

    public <E> E checkNext( final MethodStructure<E> generator, final String iterVar )
    {
        return generator.iteratorHasNext( generator.loadVariable( iterVar ) );
    }

    public <E> void close( final String iterVarName, final MethodStructure<E> generator )
    {
    }

    public UnwindPrimitiveCollection copy( final String opName, final CodeGenExpression collection )
    {
        return new UnwindPrimitiveCollection( opName, collection );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.collection();
    }

    public String productPrefix()
    {
        return "UnwindPrimitiveCollection";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.collection();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof UnwindPrimitiveCollection;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof UnwindPrimitiveCollection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            UnwindPrimitiveCollection var4 = (UnwindPrimitiveCollection) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            CodeGenExpression var7 = this.collection();
                            CodeGenExpression var6 = var4.collection();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
