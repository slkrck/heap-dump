package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class HashableTupleDescriptor implements TupleDescriptor, Product, Serializable
{
    private final Map<String,CodeGenType> structure;

    public HashableTupleDescriptor( final Map<String,CodeGenType> structure )
    {
        this.structure = structure;
        Product.$init$( this );
    }

    public static Option<Map<String,CodeGenType>> unapply( final HashableTupleDescriptor x$0 )
    {
        return HashableTupleDescriptor$.MODULE$.unapply( var0 );
    }

    public static HashableTupleDescriptor apply( final Map<String,CodeGenType> structure )
    {
        return HashableTupleDescriptor$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Map<String,CodeGenType>,A> andThen( final Function1<HashableTupleDescriptor,A> g )
    {
        return HashableTupleDescriptor$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,HashableTupleDescriptor> compose( final Function1<A,Map<String,CodeGenType>> g )
    {
        return HashableTupleDescriptor$.MODULE$.compose( var0 );
    }

    public Map<String,CodeGenType> structure()
    {
        return this.structure;
    }

    public HashableTupleDescriptor copy( final Map<String,CodeGenType> structure )
    {
        return new HashableTupleDescriptor( structure );
    }

    public Map<String,CodeGenType> copy$default$1()
    {
        return this.structure();
    }

    public String productPrefix()
    {
        return "HashableTupleDescriptor";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.structure();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof HashableTupleDescriptor;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof HashableTupleDescriptor )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        HashableTupleDescriptor var4 = (HashableTupleDescriptor) x$1;
                        Map var10000 = this.structure();
                        Map var5 = var4.structure();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
