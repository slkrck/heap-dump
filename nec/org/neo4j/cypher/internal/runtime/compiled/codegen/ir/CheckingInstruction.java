package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class CheckingInstruction implements Instruction, Product, Serializable
{
    private final Instruction inner;
    private final String yieldedFlagVar;

    public CheckingInstruction( final Instruction inner, final String yieldedFlagVar )
    {
        this.inner = inner;
        this.yieldedFlagVar = yieldedFlagVar;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<Instruction,String>> unapply( final CheckingInstruction x$0 )
    {
        return CheckingInstruction$.MODULE$.unapply( var0 );
    }

    public static CheckingInstruction apply( final Instruction inner, final String yieldedFlagVar )
    {
        return CheckingInstruction$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<Instruction,String>,CheckingInstruction> tupled()
    {
        return CheckingInstruction$.MODULE$.tupled();
    }

    public static Function1<Instruction,Function1<String,CheckingInstruction>> curried()
    {
        return CheckingInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public Set<String> operatorId()
    {
        return Instruction.operatorId$( this );
    }

    public Instruction inner()
    {
        return this.inner;
    }

    public String yieldedFlagVar()
    {
        return this.yieldedFlagVar;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.inner().init( generator, context );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.updateFlag( this.yieldedFlagVar(), true );
        this.inner().body( generator, context );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.inner()}) ));
    }

    public CheckingInstruction copy( final Instruction inner, final String yieldedFlagVar )
    {
        return new CheckingInstruction( inner, yieldedFlagVar );
    }

    public Instruction copy$default$1()
    {
        return this.inner();
    }

    public String copy$default$2()
    {
        return this.yieldedFlagVar();
    }

    public String productPrefix()
    {
        return "CheckingInstruction";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.inner();
            break;
        case 1:
            var10000 = this.yieldedFlagVar();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CheckingInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof CheckingInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            CheckingInstruction var4 = (CheckingInstruction) x$1;
                            Instruction var10000 = this.inner();
                            Instruction var5 = var4.inner();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            String var7 = this.yieldedFlagVar();
                            String var6 = var4.yieldedFlagVar();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
