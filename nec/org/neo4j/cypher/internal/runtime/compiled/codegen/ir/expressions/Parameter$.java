package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class Parameter$ extends AbstractFunction3<String,String,CypherCodeGenType,Parameter> implements Serializable
{
    public static Parameter$ MODULE$;

    static
    {
        new Parameter$();
    }

    private Parameter$()
    {
        MODULE$ = this;
    }

    public CypherCodeGenType $lessinit$greater$default$3()
    {
        return CodeGenType$.MODULE$.AnyValue();
    }

    public final String toString()
    {
        return "Parameter";
    }

    public Parameter apply( final String key, final String variableName, final CypherCodeGenType cType )
    {
        return new Parameter( key, variableName, cType );
    }

    public CypherCodeGenType apply$default$3()
    {
        return CodeGenType$.MODULE$.AnyValue();
    }

    public Option<Tuple3<String,String,CypherCodeGenType>> unapply( final Parameter x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.key(), x$0.variableName(), x$0.cType() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
