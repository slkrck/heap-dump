package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LessThanEqual
{
    public static String toString()
    {
        return LessThanEqual$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return LessThanEqual$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return LessThanEqual$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return LessThanEqual$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return LessThanEqual$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return LessThanEqual$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return LessThanEqual$.MODULE$.productPrefix();
    }
}
