package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LongToCountTable
{
    public static String toString()
    {
        return LongToCountTable$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return LongToCountTable$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return LongToCountTable$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return LongToCountTable$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return LongToCountTable$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return LongToCountTable$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return LongToCountTable$.MODULE$.productPrefix();
    }
}
