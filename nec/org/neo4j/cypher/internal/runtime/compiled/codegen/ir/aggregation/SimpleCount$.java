package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class SimpleCount$ extends AbstractFunction4<String,Variable,CodeGenExpression,Object,SimpleCount> implements Serializable
{
    public static SimpleCount$ MODULE$;

    static
    {
        new SimpleCount$();
    }

    private SimpleCount$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SimpleCount";
    }

    public SimpleCount apply( final String opName, final Variable variable, final CodeGenExpression expression, final boolean distinct )
    {
        return new SimpleCount( opName, variable, expression, distinct );
    }

    public Option<Tuple4<String,Variable,CodeGenExpression,Object>> unapply( final SimpleCount x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple4( x$0.opName(), x$0.variable(), x$0.expression(), BoxesRunTime.boxToBoolean( x$0.distinct() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
