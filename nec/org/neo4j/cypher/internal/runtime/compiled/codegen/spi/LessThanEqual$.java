package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class LessThanEqual$ implements Comparator, Product, Serializable
{
    public static LessThanEqual$ MODULE$;

    static
    {
        new LessThanEqual$();
    }

    private LessThanEqual$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "LessThanEqual";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LessThanEqual$;
    }

    public int hashCode()
    {
        return 479719514;
    }

    public String toString()
    {
        return "LessThanEqual";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
