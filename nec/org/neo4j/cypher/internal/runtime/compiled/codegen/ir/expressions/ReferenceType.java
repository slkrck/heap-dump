package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ReferenceType extends RepresentationType
{
    static boolean canEqual( final Object x$1 )
    {
        return ReferenceType$.MODULE$.canEqual( var0 );
    }

    static Iterator<Object> productIterator()
    {
        return ReferenceType$.MODULE$.productIterator();
    }

    static Object productElement( final int x$1 )
    {
        return ReferenceType$.MODULE$.productElement( var0 );
    }

    static int productArity()
    {
        return ReferenceType$.MODULE$.productArity();
    }

    static String productPrefix()
    {
        return ReferenceType$.MODULE$.productPrefix();
    }
}
