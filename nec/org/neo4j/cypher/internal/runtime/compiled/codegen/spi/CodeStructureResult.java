package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.plandescription.Argument;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CodeStructureResult<T>
{
    T query();

    Seq<Argument> code();
}
