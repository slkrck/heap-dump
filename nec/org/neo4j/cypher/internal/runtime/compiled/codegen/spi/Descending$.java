package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class Descending$ implements SortOrder, Product, Serializable
{
    public static Descending$ MODULE$;

    static
    {
        new Descending$();
    }

    private Descending$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "Descending";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Descending$;
    }

    public int hashCode()
    {
        return 877168408;
    }

    public String toString()
    {
        return "Descending";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
