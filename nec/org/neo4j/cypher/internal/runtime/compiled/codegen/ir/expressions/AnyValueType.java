package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface AnyValueType extends ReferenceType
{
    static boolean canEqual( final Object x$1 )
    {
        return AnyValueType$.MODULE$.canEqual( var0 );
    }

    static Iterator<Object> productIterator()
    {
        return AnyValueType$.MODULE$.productIterator();
    }

    static Object productElement( final int x$1 )
    {
        return AnyValueType$.MODULE$.productElement( var0 );
    }

    static int productArity()
    {
        return AnyValueType$.MODULE$.productArity();
    }

    static String productPrefix()
    {
        return AnyValueType$.MODULE$.productPrefix();
    }
}
