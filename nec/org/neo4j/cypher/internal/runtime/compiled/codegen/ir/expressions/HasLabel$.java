package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class HasLabel$ extends AbstractFunction3<Variable,String,String,HasLabel> implements Serializable
{
    public static HasLabel$ MODULE$;

    static
    {
        new HasLabel$();
    }

    private HasLabel$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "HasLabel";
    }

    public HasLabel apply( final Variable nodeVariable, final String labelVariable, final String labelName )
    {
        return new HasLabel( nodeVariable, labelVariable, labelName );
    }

    public Option<Tuple3<Variable,String,String>> unapply( final HasLabel x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.nodeVariable(), x$0.labelVariable(), x$0.labelName() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
