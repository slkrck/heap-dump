package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CodeGenExpression
{
    static void $init$( final CodeGenExpression $this )
    {
    }

    <E> void init( final MethodStructure<E> generator, final CodeGenContext context );

    <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context );

    boolean nullable( final CodeGenContext context );

    CypherCodeGenType codeGenType( final CodeGenContext context );

    default boolean needsJavaNullCheck( final CodeGenContext context )
    {
        boolean var3;
        label25:
        {
            if ( this.nullable( context ) )
            {
                RepresentationType var10000 = this.codeGenType( context ).repr();
                ReferenceType$ var2 = ReferenceType$.MODULE$;
                if ( var10000 == null )
                {
                    if ( var2 == null )
                    {
                        break label25;
                    }
                }
                else if ( var10000.equals( var2 ) )
                {
                    break label25;
                }
            }

            var3 = false;
            return var3;
        }

        var3 = true;
        return var3;
    }
}
