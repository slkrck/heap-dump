package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction4;

public final class GetSortedResult$ extends AbstractFunction4<String,Map<String,Variable>,SortTableInfo,Instruction,GetSortedResult> implements Serializable
{
    public static GetSortedResult$ MODULE$;

    static
    {
        new GetSortedResult$();
    }

    private GetSortedResult$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "GetSortedResult";
    }

    public GetSortedResult apply( final String opName, final Map<String,Variable> variablesToKeep, final SortTableInfo sortTableInfo, final Instruction action )
    {
        return new GetSortedResult( opName, variablesToKeep, sortTableInfo, action );
    }

    public Option<Tuple4<String,Map<String,Variable>,SortTableInfo,Instruction>> unapply( final GetSortedResult x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.opName(), x$0.variablesToKeep(), x$0.sortTableInfo(), x$0.action() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
