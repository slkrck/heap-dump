package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class CartesianProductInstruction implements Instruction, Product, Serializable
{
    private final String id;
    private final Instruction instruction;

    public CartesianProductInstruction( final String id, final Instruction instruction )
    {
        this.id = id;
        this.instruction = instruction;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<String,Instruction>> unapply( final CartesianProductInstruction x$0 )
    {
        return CartesianProductInstruction$.MODULE$.unapply( var0 );
    }

    public static CartesianProductInstruction apply( final String id, final Instruction instruction )
    {
        return CartesianProductInstruction$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<String,Instruction>,CartesianProductInstruction> tupled()
    {
        return CartesianProductInstruction$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Instruction,CartesianProductInstruction>> curried()
    {
        return CartesianProductInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String id()
    {
        return this.id;
    }

    public Instruction instruction()
    {
        return this.instruction;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        Instruction.init$( this, generator, context );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.id(), generator.trace$default$2(), ( body ) -> {
            $anonfun$body$1( this, context, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.instruction()}) ));
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.id()}) ));
    }

    public CartesianProductInstruction copy( final String id, final Instruction instruction )
    {
        return new CartesianProductInstruction( id, instruction );
    }

    public String copy$default$1()
    {
        return this.id();
    }

    public Instruction copy$default$2()
    {
        return this.instruction();
    }

    public String productPrefix()
    {
        return "CartesianProductInstruction";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.id();
            break;
        case 1:
            var10000 = this.instruction();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CartesianProductInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof CartesianProductInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            CartesianProductInstruction var4 = (CartesianProductInstruction) x$1;
                            String var10000 = this.id();
                            String var5 = var4.id();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Instruction var7 = this.instruction();
                            Instruction var6 = var4.instruction();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
