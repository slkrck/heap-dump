package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class LongToCountTable$ implements CountingJoinTableType, Product, Serializable
{
    public static LongToCountTable$ MODULE$;

    static
    {
        new LongToCountTable$();
    }

    private LongToCountTable$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "LongToCountTable";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LongToCountTable$;
    }

    public int hashCode()
    {
        return 1063525782;
    }

    public String toString()
    {
        return "LongToCountTable";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
