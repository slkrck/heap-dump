package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.AggregateExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.Iterable;
import scala.runtime.AbstractFunction2;

public final class AggregationInstruction$ extends AbstractFunction2<String,Iterable<AggregateExpression>,AggregationInstruction> implements Serializable
{
    public static AggregationInstruction$ MODULE$;

    static
    {
        new AggregationInstruction$();
    }

    private AggregationInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "AggregationInstruction";
    }

    public AggregationInstruction apply( final String opName, final Iterable<AggregateExpression> aggregationFunctions )
    {
        return new AggregationInstruction( opName, aggregationFunctions );
    }

    public Option<Tuple2<String,Iterable<AggregateExpression>>> unapply( final AggregationInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.opName(), x$0.aggregationFunctions() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
