package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public interface AggregateExpression
{
    static void $init$( final AggregateExpression $this )
    {
    }

    <E> void init( final MethodStructure<E> generator, final CodeGenContext context );

    <E> void update( final MethodStructure<E> generator, final CodeGenContext context );

    String opName();

    default Instruction continuation( final Instruction instruction )
    {
        return new Instruction( this, instruction )
        {
            private final Instruction instruction$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.instruction$1 = instruction$1;
                    Instruction.$init$( this );
                }
            }

            public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
            {
                Instruction.init$( this, generator, context );
            }

            public final Set<String> allOperatorIds()
            {
                return Instruction.allOperatorIds$( this );
            }

            public Seq<Instruction> children()
            {
                return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.instruction$1}) ));
            }

            public Set<String> operatorId()
            {
                return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.$outer.opName()}) ));
            }

            public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
            {
                generator.trace( this.$outer.opName(), generator.trace$default$2(), ( inner ) -> {
                    $anonfun$body$1( this, context, inner );
                    return BoxedUnit.UNIT;
                } );
            }
        };
    }
}
