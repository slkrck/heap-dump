package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class False$ implements BooleanConstant, Product, Serializable
{
    public static False$ MODULE$;

    static
    {
        new False$();
    }

    private False$()
    {
        MODULE$ = this;
        CodeGenExpression.$init$( this );
        BooleanConstant.$init$( this );
        Product.$init$( this );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return BooleanConstant.nullable$( this, context );
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return BooleanConstant.codeGenType$( this, context );
    }

    public final <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        BooleanConstant.init$( this, generator, context );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.constantExpression( BoxesRunTime.boxToBoolean( false ) );
    }

    public String productPrefix()
    {
        return "False";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof False$;
    }

    public int hashCode()
    {
        return 67643651;
    }

    public String toString()
    {
        return "False";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
