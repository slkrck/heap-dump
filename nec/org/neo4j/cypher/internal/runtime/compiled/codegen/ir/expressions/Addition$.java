package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class Addition$ extends AbstractFunction2<CodeGenExpression,CodeGenExpression,Addition> implements Serializable
{
    public static Addition$ MODULE$;

    static
    {
        new Addition$();
    }

    private Addition$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Addition";
    }

    public Addition apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Addition( lhs, rhs );
    }

    public Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Addition x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.lhs(), x$0.rhs() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
