package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction1;

public final class SimpleTupleDescriptor$ extends AbstractFunction1<Map<String,CodeGenType>,SimpleTupleDescriptor> implements Serializable
{
    public static SimpleTupleDescriptor$ MODULE$;

    static
    {
        new SimpleTupleDescriptor$();
    }

    private SimpleTupleDescriptor$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SimpleTupleDescriptor";
    }

    public SimpleTupleDescriptor apply( final Map<String,CodeGenType> structure )
    {
        return new SimpleTupleDescriptor( structure );
    }

    public Option<Map<String,CodeGenType>> unapply( final SimpleTupleDescriptor x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.structure() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
