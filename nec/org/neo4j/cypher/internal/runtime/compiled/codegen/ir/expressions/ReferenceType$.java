package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class ReferenceType$ implements ReferenceType, Product, Serializable
{
    public static ReferenceType$ MODULE$;

    static
    {
        new ReferenceType$();
    }

    private ReferenceType$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "ReferenceType";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ReferenceType$;
    }

    public int hashCode()
    {
        return -1895049979;
    }

    public String toString()
    {
        return "ReferenceType";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
