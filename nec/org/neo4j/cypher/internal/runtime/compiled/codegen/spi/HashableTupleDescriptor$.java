package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction1;

public final class HashableTupleDescriptor$ extends AbstractFunction1<Map<String,CodeGenType>,HashableTupleDescriptor> implements Serializable
{
    public static HashableTupleDescriptor$ MODULE$;

    static
    {
        new HashableTupleDescriptor$();
    }

    private HashableTupleDescriptor$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "HashableTupleDescriptor";
    }

    public HashableTupleDescriptor apply( final Map<String,CodeGenType> structure )
    {
        return new HashableTupleDescriptor( structure );
    }

    public Option<Map<String,CodeGenType>> unapply( final HashableTupleDescriptor x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.structure() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
