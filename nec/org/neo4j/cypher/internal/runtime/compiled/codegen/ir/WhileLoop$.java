package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class WhileLoop$ extends AbstractFunction3<Variable,LoopDataGenerator,Instruction,WhileLoop> implements Serializable
{
    public static WhileLoop$ MODULE$;

    static
    {
        new WhileLoop$();
    }

    private WhileLoop$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "WhileLoop";
    }

    public WhileLoop apply( final Variable variable, final LoopDataGenerator producer, final Instruction action )
    {
        return new WhileLoop( variable, producer, action );
    }

    public Option<Tuple3<Variable,LoopDataGenerator,Instruction>> unapply( final WhileLoop x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.variable(), x$0.producer(), x$0.action() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
