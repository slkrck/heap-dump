package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NullingInstruction implements Instruction, Product, Serializable
{
    private final Instruction loop;
    private final String yieldedFlagVar;
    private final Instruction alternativeAction;
    private final Seq<Variable> nullableVars;

    public NullingInstruction( final Instruction loop, final String yieldedFlagVar, final Instruction alternativeAction, final Seq<Variable> nullableVars )
    {
        this.loop = loop;
        this.yieldedFlagVar = yieldedFlagVar;
        this.alternativeAction = alternativeAction;
        this.nullableVars = nullableVars;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<Instruction,String,Instruction,Seq<Variable>>> unapplySeq( final NullingInstruction x$0 )
    {
        return NullingInstruction$.MODULE$.unapplySeq( var0 );
    }

    public static NullingInstruction apply( final Instruction loop, final String yieldedFlagVar, final Instruction alternativeAction,
            final Seq<Variable> nullableVars )
    {
        return NullingInstruction$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<Instruction,String,Instruction,Seq<Variable>>,NullingInstruction> tupled()
    {
        return NullingInstruction$.MODULE$.tupled();
    }

    public static Function1<Instruction,Function1<String,Function1<Instruction,Function1<Seq<Variable>,NullingInstruction>>>> curried()
    {
        return NullingInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public Set<String> operatorId()
    {
        return Instruction.operatorId$( this );
    }

    public Instruction loop()
    {
        return this.loop;
    }

    public String yieldedFlagVar()
    {
        return this.yieldedFlagVar;
    }

    public Instruction alternativeAction()
    {
        return this.alternativeAction;
    }

    public Seq<Variable> nullableVars()
    {
        return this.nullableVars;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.loop().init( generator, context );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.declareFlag( this.yieldedFlagVar(), false );
        this.loop().body( generator, context );
        generator.ifNotStatement( generator.loadVariable( this.yieldedFlagVar() ), ( ifBody ) -> {
            $anonfun$body$1( this, context, ifBody );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.loop()}) ));
    }

    public String productPrefix()
    {
        return "NullingInstruction";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.loop();
            break;
        case 1:
            var10000 = this.yieldedFlagVar();
            break;
        case 2:
            var10000 = this.alternativeAction();
            break;
        case 3:
            var10000 = this.nullableVars();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NullingInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof NullingInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            NullingInstruction var4 = (NullingInstruction) x$1;
                            Instruction var10000 = this.loop();
                            Instruction var5 = var4.loop();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            String var9 = this.yieldedFlagVar();
                            String var6 = var4.yieldedFlagVar();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            var10000 = this.alternativeAction();
                            Instruction var7 = var4.alternativeAction();
                            if ( var10000 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var7 ) )
                            {
                                break label72;
                            }

                            Seq var10 = this.nullableVars();
                            Seq var8 = var4.nullableVars();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label63;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label81;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
