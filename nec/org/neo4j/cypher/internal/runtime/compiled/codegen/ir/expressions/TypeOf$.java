package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class TypeOf$ extends AbstractFunction1<Variable,TypeOf> implements Serializable
{
    public static TypeOf$ MODULE$;

    static
    {
        new TypeOf$();
    }

    private TypeOf$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "TypeOf";
    }

    public TypeOf apply( final Variable relId )
    {
        return new TypeOf( relId );
    }

    public Option<Variable> unapply( final TypeOf x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.relId() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
