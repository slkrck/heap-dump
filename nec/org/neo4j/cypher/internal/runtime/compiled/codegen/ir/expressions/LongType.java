package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LongType
{
    public static String toString()
    {
        return LongType$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return LongType$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return LongType$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return LongType$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return LongType$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return LongType$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return LongType$.MODULE$.productPrefix();
    }
}
