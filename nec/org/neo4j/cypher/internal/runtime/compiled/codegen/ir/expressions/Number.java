package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import scala.Option;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class Number
{
    public static Option<CypherType> unapply( final CypherType x )
    {
        return Number$.MODULE$.unapply( var0 );
    }
}
