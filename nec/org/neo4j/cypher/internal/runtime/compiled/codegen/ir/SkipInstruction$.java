package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class SkipInstruction$ extends AbstractFunction4<String,String,Instruction,CodeGenExpression,SkipInstruction> implements Serializable
{
    public static SkipInstruction$ MODULE$;

    static
    {
        new SkipInstruction$();
    }

    private SkipInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SkipInstruction";
    }

    public SkipInstruction apply( final String opName, final String variableName, final Instruction action, final CodeGenExpression numberToSkip )
    {
        return new SkipInstruction( opName, variableName, action, numberToSkip );
    }

    public Option<Tuple4<String,String,Instruction,CodeGenExpression>> unapply( final SkipInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.opName(), x$0.variableName(), x$0.action(), x$0.numberToSkip() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
