package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class TopTableDescriptor$ extends AbstractFunction1<OrderableTupleDescriptor,TopTableDescriptor> implements Serializable
{
    public static TopTableDescriptor$ MODULE$;

    static
    {
        new TopTableDescriptor$();
    }

    private TopTableDescriptor$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "TopTableDescriptor";
    }

    public TopTableDescriptor apply( final OrderableTupleDescriptor tupleDescriptor )
    {
        return new TopTableDescriptor( tupleDescriptor );
    }

    public Option<OrderableTupleDescriptor> unapply( final TopTableDescriptor x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.tupleDescriptor() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
