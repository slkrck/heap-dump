package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.planner.spi.PlanningAttributes.Cardinalities;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import scala.Option;
import scala.Tuple2;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LogicalPlanConverter
{
    public static CodeGenPlan asCodeGenPlan( final LogicalPlan logicalPlan )
    {
        return LogicalPlanConverter$.MODULE$.asCodeGenPlan( var0 );
    }

    public interface SingleChildPlan extends CodeGenPlan
    {
        static void $init$( final LogicalPlanConverter.SingleChildPlan $this )
        {
        }

        default Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities )
        {
            context.pushParent( this );
            return LogicalPlanConverter$.MODULE$.asCodeGenPlan( (LogicalPlan) this.logicalPlan().lhs().get() ).produce( context, cardinalities );
        }
    }
}
