package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class GreaterThan
{
    public static String toString()
    {
        return GreaterThan$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return GreaterThan$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return GreaterThan$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return GreaterThan$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return GreaterThan$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return GreaterThan$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return GreaterThan$.MODULE$.productPrefix();
    }
}
