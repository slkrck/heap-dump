package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class JoinData implements Product, Serializable
{
    private final Map<String,VariableData> vars;
    private final String tableVar;
    private final JoinTableType tableType;
    private final String id;

    public JoinData( final Map<String,VariableData> vars, final String tableVar, final JoinTableType tableType, final String id )
    {
        this.vars = vars;
        this.tableVar = tableVar;
        this.tableType = tableType;
        this.id = id;
        Product.$init$( this );
    }

    public static Option<Tuple4<Map<String,VariableData>,String,JoinTableType,String>> unapply( final JoinData x$0 )
    {
        return JoinData$.MODULE$.unapply( var0 );
    }

    public static JoinData apply( final Map<String,VariableData> vars, final String tableVar, final JoinTableType tableType, final String id )
    {
        return JoinData$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<Map<String,VariableData>,String,JoinTableType,String>,JoinData> tupled()
    {
        return JoinData$.MODULE$.tupled();
    }

    public static Function1<Map<String,VariableData>,Function1<String,Function1<JoinTableType,Function1<String,JoinData>>>> curried()
    {
        return JoinData$.MODULE$.curried();
    }

    public Map<String,VariableData> vars()
    {
        return this.vars;
    }

    public String tableVar()
    {
        return this.tableVar;
    }

    public JoinTableType tableType()
    {
        return this.tableType;
    }

    public String id()
    {
        return this.id;
    }

    public JoinData copy( final Map<String,VariableData> vars, final String tableVar, final JoinTableType tableType, final String id )
    {
        return new JoinData( vars, tableVar, tableType, id );
    }

    public Map<String,VariableData> copy$default$1()
    {
        return this.vars();
    }

    public String copy$default$2()
    {
        return this.tableVar();
    }

    public JoinTableType copy$default$3()
    {
        return this.tableType();
    }

    public String copy$default$4()
    {
        return this.id();
    }

    public String productPrefix()
    {
        return "JoinData";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.vars();
            break;
        case 1:
            var10000 = this.tableVar();
            break;
        case 2:
            var10000 = this.tableType();
            break;
        case 3:
            var10000 = this.id();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof JoinData;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof JoinData )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            JoinData var4 = (JoinData) x$1;
                            Map var10000 = this.vars();
                            Map var5 = var4.vars();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            String var9 = this.tableVar();
                            String var6 = var4.tableVar();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            JoinTableType var10 = this.tableType();
                            JoinTableType var7 = var4.tableType();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            var9 = this.id();
                            String var8 = var4.id();
                            if ( var9 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label63;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label81;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
