package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SortItem implements Product, Serializable
{
    private final String fieldName;
    private final SortOrder sortOrder;

    public SortItem( final String fieldName, final SortOrder sortOrder )
    {
        this.fieldName = fieldName;
        this.sortOrder = sortOrder;
        Product.$init$( this );
    }

    public static Option<Tuple2<String,SortOrder>> unapply( final SortItem x$0 )
    {
        return SortItem$.MODULE$.unapply( var0 );
    }

    public static SortItem apply( final String fieldName, final SortOrder sortOrder )
    {
        return SortItem$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<String,SortOrder>,SortItem> tupled()
    {
        return SortItem$.MODULE$.tupled();
    }

    public static Function1<String,Function1<SortOrder,SortItem>> curried()
    {
        return SortItem$.MODULE$.curried();
    }

    public String fieldName()
    {
        return this.fieldName;
    }

    public SortOrder sortOrder()
    {
        return this.sortOrder;
    }

    public SortItem copy( final String fieldName, final SortOrder sortOrder )
    {
        return new SortItem( fieldName, sortOrder );
    }

    public String copy$default$1()
    {
        return this.fieldName();
    }

    public SortOrder copy$default$2()
    {
        return this.sortOrder();
    }

    public String productPrefix()
    {
        return "SortItem";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.fieldName();
            break;
        case 1:
            var10000 = this.sortOrder();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SortItem;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof SortItem )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            SortItem var4 = (SortItem) x$1;
                            String var10000 = this.fieldName();
                            String var5 = var4.fieldName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            SortOrder var7 = this.sortOrder();
                            SortOrder var6 = var4.sortOrder();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
