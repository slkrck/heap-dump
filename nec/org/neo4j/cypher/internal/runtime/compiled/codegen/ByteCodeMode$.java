package org.neo4j.cypher.internal.runtime.compiled.codegen;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class ByteCodeMode$ implements CodeGenMode, Product, Serializable
{
    public static ByteCodeMode$ MODULE$;

    static
    {
        new ByteCodeMode$();
    }

    private ByteCodeMode$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "ByteCodeMode";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ByteCodeMode$;
    }

    public int hashCode()
    {
        return 1548168824;
    }

    public String toString()
    {
        return "ByteCodeMode";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
