package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CountingJoinTableType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongToCountTable$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongsToCountTable$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing.;

@JavaDocToJava
public class BuildCountingProbeTable implements BuildProbeTable, Product, Serializable
{
    private final String id;
    private final String name;
    private final IndexedSeq<Variable> nodes;
    private final CountingJoinTableType tableType;

    public BuildCountingProbeTable( final String id, final String name, final IndexedSeq<Variable> nodes )
    {
        this.id = id;
        this.name = name;
        this.nodes = nodes;
        Instruction.$init$( this );
        BuildProbeTable.$init$( this );
        Product.$init$( this );
        this.tableType = (CountingJoinTableType) (nodes.size() == 1 ? LongToCountTable$.MODULE$ : LongsToCountTable$.MODULE$);
    }

    public static Option<Tuple3<String,String,IndexedSeq<Variable>>> unapply( final BuildCountingProbeTable x$0 )
    {
        return BuildCountingProbeTable$.MODULE$.unapply( var0 );
    }

    public static BuildCountingProbeTable apply( final String id, final String name, final IndexedSeq<Variable> nodes )
    {
        return BuildCountingProbeTable$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,String,IndexedSeq<Variable>>,BuildCountingProbeTable> tupled()
    {
        return BuildCountingProbeTable$.MODULE$.tupled();
    }

    public static Function1<String,Function1<String,Function1<IndexedSeq<Variable>,BuildCountingProbeTable>>> curried()
    {
        return BuildCountingProbeTable$.MODULE$.curried();
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        BuildProbeTable.init$( this, generator, context );
    }

    public Seq<> children()
    {
        return BuildProbeTable.children$( this );
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String id()
    {
        return this.id;
    }

    public String name()
    {
        return this.name;
    }

    public IndexedSeq<Variable> nodes()
    {
        return this.nodes;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.id(), new Some( this.getClass().getSimpleName() ), ( body ) -> {
            $anonfun$body$4( this, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.id()}) ));
    }

    public CountingJoinTableType tableType()
    {
        return this.tableType;
    }

    public JoinData joinData()
    {
        return new JoinData( scala.Predef..MODULE$.Map().empty(),this.name(), this.tableType(), this.id());
    }

    public BuildCountingProbeTable copy( final String id, final String name, final IndexedSeq<Variable> nodes )
    {
        return new BuildCountingProbeTable( id, name, nodes );
    }

    public String copy$default$1()
    {
        return this.id();
    }

    public String copy$default$2()
    {
        return this.name();
    }

    public IndexedSeq<Variable> copy$default$3()
    {
        return this.nodes();
    }

    public String productPrefix()
    {
        return "BuildCountingProbeTable";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.id();
            break;
        case 1:
            var10000 = this.name();
            break;
        case 2:
            var10000 = this.nodes();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof BuildCountingProbeTable;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var9;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof BuildCountingProbeTable )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            BuildCountingProbeTable var4 = (BuildCountingProbeTable) x$1;
                            String var10000 = this.id();
                            String var5 = var4.id();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            var10000 = this.name();
                            String var6 = var4.name();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label63;
                            }

                            IndexedSeq var8 = this.nodes();
                            IndexedSeq var7 = var4.nodes();
                            if ( var8 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var9 = true;
                                break label54;
                            }
                        }

                        var9 = false;
                    }

                    if ( var9 )
                    {
                        break label72;
                    }
                }

                var9 = false;
                return var9;
            }
        }

        var9 = true;
        return var9;
    }
}
