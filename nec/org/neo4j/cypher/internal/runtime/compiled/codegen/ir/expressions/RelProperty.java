package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class RelProperty extends ElementProperty implements Product, Serializable
{
    private final Option<Object> token;
    private final String propName;
    private final Variable relIdVar;
    private final String propKeyVar;

    public RelProperty( final Option<Object> token, final String propName, final Variable relIdVar, final String propKeyVar )
    {
        super( token, propName, relIdVar.name(), propKeyVar );
        this.token = token;
        this.propName = propName;
        this.relIdVar = relIdVar;
        this.propKeyVar = propKeyVar;
        Product.$init$( this );
    }

    public static Option<Tuple4<Option<Object>,String,Variable,String>> unapply( final RelProperty x$0 )
    {
        return RelProperty$.MODULE$.unapply( var0 );
    }

    public static RelProperty apply( final Option<Object> token, final String propName, final Variable relIdVar, final String propKeyVar )
    {
        return RelProperty$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<Option<Object>,String,Variable,String>,RelProperty> tupled()
    {
        return RelProperty$.MODULE$.tupled();
    }

    public static Function1<Option<Object>,Function1<String,Function1<Variable,Function1<String,RelProperty>>>> curried()
    {
        return RelProperty$.MODULE$.curried();
    }

    public Option<Object> token()
    {
        return this.token;
    }

    public String propName()
    {
        return this.propName;
    }

    public Variable relIdVar()
    {
        return this.relIdVar;
    }

    public String propKeyVar()
    {
        return this.propKeyVar;
    }

    public <E> void propertyByName( final MethodStructure<E> body, final String localName )
    {
        if ( this.relIdVar().nullable() )
        {
            body.ifNotStatement( body.isNull( (String) this.relIdVar().name(), CodeGenType$.MODULE$.primitiveRel() ), ( ifBody ) -> {
                $anonfun$propertyByName$2( this, localName, ifBody );
                return BoxedUnit.UNIT;
            } );
        }
        else
        {
            body.relationshipGetPropertyForVar( this.relIdVar().name(), this.relIdVar().codeGenType(), this.propKeyVar(), localName );
        }
    }

    public <E> void propertyById( final MethodStructure<E> body, final String localName )
    {
        if ( this.relIdVar().nullable() )
        {
            body.ifNotStatement( body.isNull( (String) this.relIdVar().name(), CodeGenType$.MODULE$.primitiveRel() ), ( ifBody ) -> {
                $anonfun$propertyById$2( this, localName, ifBody );
                return BoxedUnit.UNIT;
            } );
        }
        else
        {
            body.relationshipGetPropertyById( this.relIdVar().name(), this.relIdVar().codeGenType(), BoxesRunTime.unboxToInt( this.token().get() ), localName );
        }
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return CodeGenType$.MODULE$.Value();
    }

    public RelProperty copy( final Option<Object> token, final String propName, final Variable relIdVar, final String propKeyVar )
    {
        return new RelProperty( token, propName, relIdVar, propKeyVar );
    }

    public Option<Object> copy$default$1()
    {
        return this.token();
    }

    public String copy$default$2()
    {
        return this.propName();
    }

    public Variable copy$default$3()
    {
        return this.relIdVar();
    }

    public String copy$default$4()
    {
        return this.propKeyVar();
    }

    public String productPrefix()
    {
        return "RelProperty";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.token();
            break;
        case 1:
            var10000 = this.propName();
            break;
        case 2:
            var10000 = this.relIdVar();
            break;
        case 3:
            var10000 = this.propKeyVar();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelProperty;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof RelProperty )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            RelProperty var4 = (RelProperty) x$1;
                            Option var10000 = this.token();
                            Option var5 = var4.token();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            String var9 = this.propName();
                            String var6 = var4.propName();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            Variable var10 = this.relIdVar();
                            Variable var7 = var4.relIdVar();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            var9 = this.propKeyVar();
                            String var8 = var4.propKeyVar();
                            if ( var9 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label63;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label81;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
