package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortItem;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.Iterable;
import scala.collection.immutable.Map;
import scala.runtime.BoxesRunTime;

public final class BuildSortTable$ implements Serializable
{
    public static BuildSortTable$ MODULE$;

    static
    {
        new BuildSortTable$();
    }

    private BuildSortTable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "BuildSortTable";
    }

    public BuildSortTable apply( final String opName, final String tableName, final Map<String,Variable> columnVariables, final Iterable<SortItem> sortItems,
            final double estimateCardinality, final CodeGenContext context )
    {
        return new BuildSortTable( opName, tableName, columnVariables, sortItems, estimateCardinality, context );
    }

    public Option<Tuple5<String,String,Map<String,Variable>,Iterable<SortItem>,Object>> unapply( final BuildSortTable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple5( x$0.opName(), x$0.tableName(), x$0.columnVariables(), x$0.sortItems(), BoxesRunTime.boxToDouble( x$0.estimateCardinality() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
