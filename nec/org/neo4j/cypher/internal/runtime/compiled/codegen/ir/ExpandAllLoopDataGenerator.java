package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple6;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ExpandAllLoopDataGenerator implements LoopDataGenerator, Product, Serializable
{
    private final String opName;
    private final Variable fromVar;
    private final SemanticDirection dir;
    private final Map<String,String> types;
    private final Variable toVar;
    private final Variable relVar;

    public ExpandAllLoopDataGenerator( final String opName, final Variable fromVar, final SemanticDirection dir, final Map<String,String> types,
            final Variable toVar, final Variable relVar )
    {
        this.opName = opName;
        this.fromVar = fromVar;
        this.dir = dir;
        this.types = types;
        this.toVar = toVar;
        this.relVar = relVar;
        Product.$init$( this );
    }

    public static Option<Tuple6<String,Variable,SemanticDirection,Map<String,String>,Variable,Variable>> unapply( final ExpandAllLoopDataGenerator x$0 )
    {
        return ExpandAllLoopDataGenerator$.MODULE$.unapply( var0 );
    }

    public static ExpandAllLoopDataGenerator apply( final String opName, final Variable fromVar, final SemanticDirection dir, final Map<String,String> types,
            final Variable toVar, final Variable relVar )
    {
        return ExpandAllLoopDataGenerator$.MODULE$.apply( var0, var1, var2, var3, var4, var5 );
    }

    public static Function1<Tuple6<String,Variable,SemanticDirection,Map<String,String>,Variable,Variable>,ExpandAllLoopDataGenerator> tupled()
    {
        return ExpandAllLoopDataGenerator$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Variable,Function1<SemanticDirection,Function1<Map<String,String>,Function1<Variable,Function1<Variable,ExpandAllLoopDataGenerator>>>>>> curried()
    {
        return ExpandAllLoopDataGenerator$.MODULE$.curried();
    }

    public String opName()
    {
        return this.opName;
    }

    public Variable fromVar()
    {
        return this.fromVar;
    }

    public SemanticDirection dir()
    {
        return this.dir;
    }

    public Map<String,String> types()
    {
        return this.types;
    }

    public Variable toVar()
    {
        return this.toVar;
    }

    public Variable relVar()
    {
        return this.relVar;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.types().foreach( ( x0$1 ) -> {
            $anonfun$init$1( generator, x0$1 );
            return BoxedUnit.UNIT;
        } );
    }

    public <E> void produceLoopData( final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        if ( this.types().isEmpty() )
        {
            generator.nodeGetRelationshipsWithDirection( cursorName, this.fromVar().name(), this.fromVar().codeGenType(), this.dir() );
        }
        else
        {
            generator.nodeGetRelationshipsWithDirectionAndTypes( cursorName, this.fromVar().name(), this.fromVar().codeGenType(), this.dir(),
                    this.types().keys().toIndexedSeq() );
        }

        generator.incrementDbHits();
    }

    public <E> void getNext( final Variable nextVar, final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.incrementDbHits();
        generator.nextRelationshipAndNode( this.toVar().name(), cursorName, this.dir(), this.fromVar().name(), this.relVar().name() );
    }

    public <E> E checkNext( final MethodStructure<E> generator, final String cursorName )
    {
        return generator.advanceRelationshipSelectionCursor( cursorName );
    }

    public <E> void close( final String cursorName, final MethodStructure<E> generator )
    {
        generator.closeRelationshipSelectionCursor( cursorName );
    }

    public ExpandAllLoopDataGenerator copy( final String opName, final Variable fromVar, final SemanticDirection dir, final Map<String,String> types,
            final Variable toVar, final Variable relVar )
    {
        return new ExpandAllLoopDataGenerator( opName, fromVar, dir, types, toVar, relVar );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public Variable copy$default$2()
    {
        return this.fromVar();
    }

    public SemanticDirection copy$default$3()
    {
        return this.dir();
    }

    public Map<String,String> copy$default$4()
    {
        return this.types();
    }

    public Variable copy$default$5()
    {
        return this.toVar();
    }

    public Variable copy$default$6()
    {
        return this.relVar();
    }

    public String productPrefix()
    {
        return "ExpandAllLoopDataGenerator";
    }

    public int productArity()
    {
        return 6;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.fromVar();
            break;
        case 2:
            var10000 = this.dir();
            break;
        case 3:
            var10000 = this.types();
            break;
        case 4:
            var10000 = this.toVar();
            break;
        case 5:
            var10000 = this.relVar();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ExpandAllLoopDataGenerator;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var14;
        if ( this != x$1 )
        {
            label99:
            {
                boolean var2;
                if ( x$1 instanceof ExpandAllLoopDataGenerator )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label81:
                    {
                        label90:
                        {
                            ExpandAllLoopDataGenerator var4 = (ExpandAllLoopDataGenerator) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label90;
                            }

                            Variable var11 = this.fromVar();
                            Variable var6 = var4.fromVar();
                            if ( var11 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var11.equals( var6 ) )
                            {
                                break label90;
                            }

                            SemanticDirection var12 = this.dir();
                            SemanticDirection var7 = var4.dir();
                            if ( var12 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var12.equals( var7 ) )
                            {
                                break label90;
                            }

                            Map var13 = this.types();
                            Map var8 = var4.types();
                            if ( var13 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var13.equals( var8 ) )
                            {
                                break label90;
                            }

                            var11 = this.toVar();
                            Variable var9 = var4.toVar();
                            if ( var11 == null )
                            {
                                if ( var9 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var11.equals( var9 ) )
                            {
                                break label90;
                            }

                            var11 = this.relVar();
                            Variable var10 = var4.relVar();
                            if ( var11 == null )
                            {
                                if ( var10 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var11.equals( var10 ) )
                            {
                                break label90;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var14 = true;
                                break label81;
                            }
                        }

                        var14 = false;
                    }

                    if ( var14 )
                    {
                        break label99;
                    }
                }

                var14 = false;
                return var14;
            }
        }

        var14 = true;
        return var14;
    }
}
