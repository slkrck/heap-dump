package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.Iterable.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Distinct implements AggregateExpression, Product, Serializable
{
    private final String opName;
    private final String setName;
    private final Iterable<Tuple2<String,CodeGenExpression>> vars;

    public Distinct( final String opName, final String setName, final Iterable<Tuple2<String,CodeGenExpression>> vars )
    {
        this.opName = opName;
        this.setName = setName;
        this.vars = vars;
        AggregateExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<String,String,Iterable<Tuple2<String,CodeGenExpression>>>> unapply( final Distinct x$0 )
    {
        return Distinct$.MODULE$.unapply( var0 );
    }

    public static Distinct apply( final String opName, final String setName, final Iterable<Tuple2<String,CodeGenExpression>> vars )
    {
        return Distinct$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,String,Iterable<Tuple2<String,CodeGenExpression>>>,Distinct> tupled()
    {
        return Distinct$.MODULE$.tupled();
    }

    public static Function1<String,Function1<String,Function1<Iterable<Tuple2<String,CodeGenExpression>>,Distinct>>> curried()
    {
        return Distinct$.MODULE$.curried();
    }

    public String opName()
    {
        return this.opName;
    }

    public String setName()
    {
        return this.setName;
    }

    public Iterable<Tuple2<String,CodeGenExpression>> vars()
    {
        return this.vars;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.vars().foreach( ( x0$1 ) -> {
            $anonfun$init$1( generator, context, x0$1 );
            return BoxedUnit.UNIT;
        } );
        generator.newDistinctSet( this.setName(), (Iterable) this.vars().map( ( x$1 ) -> {
            return ((CodeGenExpression) x$1._2()).codeGenType( context );
        },.MODULE$.canBuildFrom() ));
    }

    public <E> void update( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.vars().foreach( ( x0$2 ) -> {
            $anonfun$update$1( generator, context, x0$2 );
            return BoxedUnit.UNIT;
        } );
        generator.distinctSetIfNotContains( this.setName(), ((TraversableOnce) this.vars().map( ( v ) -> {
            return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( v._1() ), scala.Predef.ArrowAssoc..
            MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( ((CodeGenExpression) v._2()).codeGenType( context ) ), generator.loadVariable(
                    (String) v._1() )));
        },.MODULE$.canBuildFrom()) ).toMap( scala.Predef..MODULE$.$conforms()),( x$2 ) -> {
        $anonfun$update$3( x$2 );
        return BoxedUnit.UNIT;
    });
    }

    public Instruction continuation( final Instruction instruction )
    {
        return new Instruction( this, instruction )
        {
            private final Instruction instruction$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.instruction$1 = instruction$1;
                    Instruction.$init$( this );
                }
            }

            public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
            {
                Instruction.init$( this, generator, context );
            }

            public final Set<String> allOperatorIds()
            {
                return Instruction.allOperatorIds$( this );
            }

            public Seq<Instruction> children()
            {
                return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.instruction$1}) ));
            }

            public Set<String> operatorId()
            {
                return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.$outer.opName()}) ));
            }

            public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
            {
                generator.trace( this.$outer.opName(), generator.trace$default$2(), ( body ) -> {
                    $anonfun$body$1( this, context, body );
                    return BoxedUnit.UNIT;
                } );
            }
        };
    }

    public Distinct copy( final String opName, final String setName, final Iterable<Tuple2<String,CodeGenExpression>> vars )
    {
        return new Distinct( opName, setName, vars );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public String copy$default$2()
    {
        return this.setName();
    }

    public Iterable<Tuple2<String,CodeGenExpression>> copy$default$3()
    {
        return this.vars();
    }

    public String productPrefix()
    {
        return "Distinct";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.setName();
            break;
        case 2:
            var10000 = this.vars();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Distinct;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var9;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof Distinct )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            Distinct var4 = (Distinct) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            var10000 = this.setName();
                            String var6 = var4.setName();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label63;
                            }

                            Iterable var8 = this.vars();
                            Iterable var7 = var4.vars();
                            if ( var8 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var9 = true;
                                break label54;
                            }
                        }

                        var9 = false;
                    }

                    if ( var9 )
                    {
                        break label72;
                    }
                }

                var9 = false;
                return var9;
            }
        }

        var9 = true;
        return var9;
    }
}
