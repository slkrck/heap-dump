package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.AggregateExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class AggregationInstruction implements Instruction, Product, Serializable
{
    private final String opName;
    private final Iterable<AggregateExpression> aggregationFunctions;

    public AggregationInstruction( final String opName, final Iterable<AggregateExpression> aggregationFunctions )
    {
        this.opName = opName;
        this.aggregationFunctions = aggregationFunctions;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<String,Iterable<AggregateExpression>>> unapply( final AggregationInstruction x$0 )
    {
        return AggregationInstruction$.MODULE$.unapply( var0 );
    }

    public static AggregationInstruction apply( final String opName, final Iterable<AggregateExpression> aggregationFunctions )
    {
        return AggregationInstruction$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<String,Iterable<AggregateExpression>>,AggregationInstruction> tupled()
    {
        return AggregationInstruction$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Iterable<AggregateExpression>,AggregationInstruction>> curried()
    {
        return AggregationInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String opName()
    {
        return this.opName;
    }

    public Iterable<AggregateExpression> aggregationFunctions()
    {
        return this.aggregationFunctions;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.aggregationFunctions().foreach( ( x$1 ) -> {
            $anonfun$init$1( generator, context, x$1 );
            return BoxedUnit.UNIT;
        } );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.opName(), generator.trace$default$2(), ( l1 ) -> {
            $anonfun$body$1( this, context, l1 );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.empty();
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.opName()}) ));
    }

    public AggregationInstruction copy( final String opName, final Iterable<AggregateExpression> aggregationFunctions )
    {
        return new AggregationInstruction( opName, aggregationFunctions );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public Iterable<AggregateExpression> copy$default$2()
    {
        return this.aggregationFunctions();
    }

    public String productPrefix()
    {
        return "AggregationInstruction";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.aggregationFunctions();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AggregationInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof AggregationInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            AggregationInstruction var4 = (AggregationInstruction) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Iterable var7 = this.aggregationFunctions();
                            Iterable var6 = var4.aggregationFunctions();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
