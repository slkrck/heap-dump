package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class ForEachExpression$ extends AbstractFunction3<Variable,CodeGenExpression,Instruction,ForEachExpression> implements Serializable
{
    public static ForEachExpression$ MODULE$;

    static
    {
        new ForEachExpression$();
    }

    private ForEachExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ForEachExpression";
    }

    public ForEachExpression apply( final Variable varName, final CodeGenExpression expression, final Instruction body )
    {
        return new ForEachExpression( varName, expression, body );
    }

    public Option<Tuple3<Variable,CodeGenExpression,Instruction>> unapply( final ForEachExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.varName(), x$0.expression(), x$0.body() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
