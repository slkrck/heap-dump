package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction4;

public final class NullingInstruction$ extends AbstractFunction4<Instruction,String,Instruction,Seq<Variable>,NullingInstruction> implements Serializable
{
    public static NullingInstruction$ MODULE$;

    static
    {
        new NullingInstruction$();
    }

    private NullingInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NullingInstruction";
    }

    public NullingInstruction apply( final Instruction loop, final String yieldedFlagVar, final Instruction alternativeAction,
            final Seq<Variable> nullableVars )
    {
        return new NullingInstruction( loop, yieldedFlagVar, alternativeAction, nullableVars );
    }

    public Option<Tuple4<Instruction,String,Instruction,Seq<Variable>>> unapplySeq( final NullingInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.loop(), x$0.yieldedFlagVar(), x$0.alternativeAction(), x$0.nullableVars() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
