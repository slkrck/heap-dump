package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class ToSet$ extends AbstractFunction1<CodeGenExpression,ToSet> implements Serializable
{
    public static ToSet$ MODULE$;

    static
    {
        new ToSet$();
    }

    private ToSet$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ToSet";
    }

    public ToSet apply( final CodeGenExpression expression )
    {
        return new ToSet( expression );
    }

    public Option<CodeGenExpression> unapply( final ToSet x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.expression() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
