package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SeekNodeById implements Instruction, Product, Serializable
{
    private final String opName;
    private final Variable nodeVar;
    private final CodeGenExpression expression;
    private final Instruction action;

    public SeekNodeById( final String opName, final Variable nodeVar, final CodeGenExpression expression, final Instruction action )
    {
        this.opName = opName;
        this.nodeVar = nodeVar;
        this.expression = expression;
        this.action = action;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<String,Variable,CodeGenExpression,Instruction>> unapply( final SeekNodeById x$0 )
    {
        return SeekNodeById$.MODULE$.unapply( var0 );
    }

    public static SeekNodeById apply( final String opName, final Variable nodeVar, final CodeGenExpression expression, final Instruction action )
    {
        return SeekNodeById$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<String,Variable,CodeGenExpression,Instruction>,SeekNodeById> tupled()
    {
        return SeekNodeById$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Variable,Function1<CodeGenExpression,Function1<Instruction,SeekNodeById>>>> curried()
    {
        return SeekNodeById$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public Set<String> operatorId()
    {
        return Instruction.operatorId$( this );
    }

    public String opName()
    {
        return this.opName;
    }

    public Variable nodeVar()
    {
        return this.nodeVar;
    }

    public CodeGenExpression expression()
    {
        return this.expression;
    }

    public Instruction action()
    {
        return this.action;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.opName(), generator.trace$default$2(), ( body ) -> {
            $anonfun$body$1( this, context, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.action()}) ));
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        Instruction.init$( this, generator, context );
        this.expression().init( generator, context );
    }

    public SeekNodeById copy( final String opName, final Variable nodeVar, final CodeGenExpression expression, final Instruction action )
    {
        return new SeekNodeById( opName, nodeVar, expression, action );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public Variable copy$default$2()
    {
        return this.nodeVar();
    }

    public CodeGenExpression copy$default$3()
    {
        return this.expression();
    }

    public Instruction copy$default$4()
    {
        return this.action();
    }

    public String productPrefix()
    {
        return "SeekNodeById";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.nodeVar();
            break;
        case 2:
            var10000 = this.expression();
            break;
        case 3:
            var10000 = this.action();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SeekNodeById;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof SeekNodeById )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            SeekNodeById var4 = (SeekNodeById) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            Variable var9 = this.nodeVar();
                            Variable var6 = var4.nodeVar();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            CodeGenExpression var10 = this.expression();
                            CodeGenExpression var7 = var4.expression();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            Instruction var11 = this.action();
                            Instruction var8 = var4.action();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
