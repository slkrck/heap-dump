package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructureResult;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.runtime.BoxedUnit;

public final class CodeGenerator$
{
    public static CodeGenerator$ MODULE$;

    static
    {
        new CodeGenerator$();
    }

    private CodeGenerator$()
    {
        MODULE$ = this;
    }

    public CodeGenConfiguration $lessinit$greater$default$3()
    {
        return new CodeGenConfiguration( CodeGenConfiguration$.MODULE$.apply$default$1(), CodeGenConfiguration$.MODULE$.apply$default$2(),
                CodeGenConfiguration$.MODULE$.apply$default$3(), CodeGenConfiguration$.MODULE$.apply$default$4(),
                CodeGenConfiguration$.MODULE$.apply$default$5() );
    }

    public <T> CodeStructureResult<T> generateCode( final CodeStructure<T> structure, final Seq<Instruction> instructions, final Map<String,Id> operatorIds,
            final Seq<String> columns, final CodeGenConfiguration conf, final CodeGenContext context )
    {
        return structure.generateQuery( Namer$.MODULE$.newClassName(), columns, operatorIds, conf, ( accept ) -> {
            $anonfun$generateCode$1( instructions, context, accept );
            return BoxedUnit.UNIT;
        }, context );
    }
}
