package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class RelProperty$ extends AbstractFunction4<Option<Object>,String,Variable,String,RelProperty> implements Serializable
{
    public static RelProperty$ MODULE$;

    static
    {
        new RelProperty$();
    }

    private RelProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelProperty";
    }

    public RelProperty apply( final Option<Object> token, final String propName, final Variable relIdVar, final String propKeyVar )
    {
        return new RelProperty( token, propName, relIdVar, propKeyVar );
    }

    public Option<Tuple4<Option<Object>,String,Variable,String>> unapply( final RelProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.token(), x$0.propName(), x$0.relIdVar(), x$0.propKeyVar() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
