package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple6;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction6;

public final class RelationshipCountFromCountStoreInstruction$ extends
        AbstractFunction6<String,Variable,Option<Tuple2<Option<Object>,String>>,Seq<Tuple2<Option<Object>,String>>,Option<Tuple2<Option<Object>,String>>,Instruction,RelationshipCountFromCountStoreInstruction>
        implements Serializable
{
    public static RelationshipCountFromCountStoreInstruction$ MODULE$;

    static
    {
        new RelationshipCountFromCountStoreInstruction$();
    }

    private RelationshipCountFromCountStoreInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipCountFromCountStoreInstruction";
    }

    public RelationshipCountFromCountStoreInstruction apply( final String opName, final Variable variable,
            final Option<Tuple2<Option<Object>,String>> startLabel, final Seq<Tuple2<Option<Object>,String>> relTypes,
            final Option<Tuple2<Option<Object>,String>> endLabel, final Instruction inner )
    {
        return new RelationshipCountFromCountStoreInstruction( opName, variable, startLabel, relTypes, endLabel, inner );
    }

    public Option<Tuple6<String,Variable,Option<Tuple2<Option<Object>,String>>,Seq<Tuple2<Option<Object>,String>>,Option<Tuple2<Option<Object>,String>>,Instruction>> unapply(
            final RelationshipCountFromCountStoreInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple6( x$0.opName(), x$0.variable(), x$0.startLabel(), x$0.relTypes(), x$0.endLabel(), x$0.inner() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
