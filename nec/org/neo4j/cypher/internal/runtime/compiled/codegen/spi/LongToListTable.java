package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class LongToListTable implements RecordingJoinTableType, Product, Serializable
{
    private final TupleDescriptor tupleDescriptor;
    private final Map<String,String> localMap;

    public LongToListTable( final TupleDescriptor tupleDescriptor, final Map<String,String> localMap )
    {
        this.tupleDescriptor = tupleDescriptor;
        this.localMap = localMap;
        Product.$init$( this );
    }

    public static Option<Tuple2<TupleDescriptor,Map<String,String>>> unapply( final LongToListTable x$0 )
    {
        return LongToListTable$.MODULE$.unapply( var0 );
    }

    public static LongToListTable apply( final TupleDescriptor tupleDescriptor, final Map<String,String> localMap )
    {
        return LongToListTable$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<TupleDescriptor,Map<String,String>>,LongToListTable> tupled()
    {
        return LongToListTable$.MODULE$.tupled();
    }

    public static Function1<TupleDescriptor,Function1<Map<String,String>,LongToListTable>> curried()
    {
        return LongToListTable$.MODULE$.curried();
    }

    public TupleDescriptor tupleDescriptor()
    {
        return this.tupleDescriptor;
    }

    public Map<String,String> localMap()
    {
        return this.localMap;
    }

    public LongToListTable copy( final TupleDescriptor tupleDescriptor, final Map<String,String> localMap )
    {
        return new LongToListTable( tupleDescriptor, localMap );
    }

    public TupleDescriptor copy$default$1()
    {
        return this.tupleDescriptor();
    }

    public Map<String,String> copy$default$2()
    {
        return this.localMap();
    }

    public String productPrefix()
    {
        return "LongToListTable";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.tupleDescriptor();
            break;
        case 1:
            var10000 = this.localMap();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LongToListTable;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof LongToListTable )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            LongToListTable var4 = (LongToListTable) x$1;
                            TupleDescriptor var10000 = this.tupleDescriptor();
                            TupleDescriptor var5 = var4.tupleDescriptor();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Map var7 = this.localMap();
                            Map var6 = var4.localMap();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
