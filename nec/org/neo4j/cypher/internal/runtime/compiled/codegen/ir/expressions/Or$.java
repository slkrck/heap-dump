package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class Or$ extends AbstractFunction2<CodeGenExpression,CodeGenExpression,Or> implements Serializable
{
    public static Or$ MODULE$;

    static
    {
        new Or$();
    }

    private Or$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Or";
    }

    public Or apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Or( lhs, rhs );
    }

    public Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Or x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.lhs(), x$0.rhs() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
