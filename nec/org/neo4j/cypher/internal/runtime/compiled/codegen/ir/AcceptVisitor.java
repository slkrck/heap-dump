package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class AcceptVisitor implements Instruction, Product, Serializable
{
    private final String produceResultOpName;
    private final Map<String,CodeGenExpression> columns;

    public AcceptVisitor( final String produceResultOpName, final Map<String,CodeGenExpression> columns )
    {
        this.produceResultOpName = produceResultOpName;
        this.columns = columns;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<String,Map<String,CodeGenExpression>>> unapply( final AcceptVisitor x$0 )
    {
        return AcceptVisitor$.MODULE$.unapply( var0 );
    }

    public static AcceptVisitor apply( final String produceResultOpName, final Map<String,CodeGenExpression> columns )
    {
        return AcceptVisitor$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<String,Map<String,CodeGenExpression>>,AcceptVisitor> tupled()
    {
        return AcceptVisitor$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Map<String,CodeGenExpression>,AcceptVisitor>> curried()
    {
        return AcceptVisitor$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String produceResultOpName()
    {
        return this.produceResultOpName;
    }

    public Map<String,CodeGenExpression> columns()
    {
        return this.columns;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.produceResultOpName(), generator.trace$default$2(), ( body ) -> {
            $anonfun$body$1( this, context, body );
            return BoxedUnit.UNIT;
        } );
    }

    private <E> E anyValue( final MethodStructure<E> generator, final CodeGenExpression v, final CodeGenContext context )
    {
        Object var10000;
        if ( v.needsJavaNullCheck( context ) )
        {
            String variable = context.namer().newVarName();
            generator.localVariable( variable, v.generateExpression( generator, context ), v.codeGenType( context ) );
            var10000 =
                    generator.ternaryOperator( generator.isNull( (Object) generator.loadVariable( variable ), v.codeGenType( context ) ), generator.noValue(),
                            generator.toMaterializedAnyValue( generator.loadVariable( variable ), v.codeGenType( context ) ) );
        }
        else
        {
            var10000 = generator.toMaterializedAnyValue( v.generateExpression( generator, context ), v.codeGenType( context ) );
        }

        return var10000;
    }

    public Set<String> operatorId()
    {
        return (Set).MODULE$.Set().apply(.MODULE$.wrapRefArray( (Object[]) (new String[]{this.produceResultOpName()}) ));
    }

    public Seq<scala.runtime.Nothing .> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.columns().values().foreach( ( x$1 ) -> {
            $anonfun$init$1( generator, context, x$1 );
            return BoxedUnit.UNIT;
        } );
        Instruction.init$( this, generator, context );
    }

    public AcceptVisitor copy( final String produceResultOpName, final Map<String,CodeGenExpression> columns )
    {
        return new AcceptVisitor( produceResultOpName, columns );
    }

    public String copy$default$1()
    {
        return this.produceResultOpName();
    }

    public Map<String,CodeGenExpression> copy$default$2()
    {
        return this.columns();
    }

    public String productPrefix()
    {
        return "AcceptVisitor";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.produceResultOpName();
            break;
        case 1:
            var10000 = this.columns();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AcceptVisitor;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof AcceptVisitor )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            AcceptVisitor var4 = (AcceptVisitor) x$1;
                            String var10000 = this.produceResultOpName();
                            String var5 = var4.produceResultOpName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Map var7 = this.columns();
                            Map var6 = var4.columns();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
