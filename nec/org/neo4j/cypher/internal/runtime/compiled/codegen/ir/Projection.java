package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.Predef.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Projection implements Instruction, Product, Serializable
{
    private final String projectionOpName;
    private final Map<Variable,CodeGenExpression> variables;
    private final Instruction action;

    public Projection( final String projectionOpName, final Map<Variable,CodeGenExpression> variables, final Instruction action )
    {
        this.projectionOpName = projectionOpName;
        this.variables = variables;
        this.action = action;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<String,Map<Variable,CodeGenExpression>,Instruction>> unapply( final Projection x$0 )
    {
        return Projection$.MODULE$.unapply( var0 );
    }

    public static Projection apply( final String projectionOpName, final Map<Variable,CodeGenExpression> variables, final Instruction action )
    {
        return Projection$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,Map<Variable,CodeGenExpression>,Instruction>,Projection> tupled()
    {
        return Projection$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Map<Variable,CodeGenExpression>,Function1<Instruction,Projection>>> curried()
    {
        return Projection$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String projectionOpName()
    {
        return this.projectionOpName;
    }

    public Map<Variable,CodeGenExpression> variables()
    {
        return this.variables;
    }

    public Instruction action()
    {
        return this.action;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        Instruction.init$( this, generator, context );
        this.variables().foreach( ( x0$1 ) -> {
            $anonfun$init$1( generator, context, x0$1 );
            return BoxedUnit.UNIT;
        } );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.projectionOpName(), generator.trace$default$2(), ( body ) -> {
            $anonfun$body$1( this, context, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Set<String> operatorId()
    {
        return (Set).MODULE$.Set().apply(.MODULE$.wrapRefArray( (Object[]) (new String[]{this.projectionOpName()}) ));
    }

    public Seq<Instruction> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.action()}) ));
    }

    public Projection copy( final String projectionOpName, final Map<Variable,CodeGenExpression> variables, final Instruction action )
    {
        return new Projection( projectionOpName, variables, action );
    }

    public String copy$default$1()
    {
        return this.projectionOpName();
    }

    public Map<Variable,CodeGenExpression> copy$default$2()
    {
        return this.variables();
    }

    public Instruction copy$default$3()
    {
        return this.action();
    }

    public String productPrefix()
    {
        return "Projection";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.projectionOpName();
            break;
        case 1:
            var10000 = this.variables();
            break;
        case 2:
            var10000 = this.action();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Projection;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof Projection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            Projection var4 = (Projection) x$1;
                            String var10000 = this.projectionOpName();
                            String var5 = var4.projectionOpName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            Map var8 = this.variables();
                            Map var6 = var4.variables();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            Instruction var9 = this.action();
                            Instruction var7 = var4.action();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
