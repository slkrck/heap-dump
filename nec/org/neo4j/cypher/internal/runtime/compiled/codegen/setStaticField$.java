package org.neo4j.cypher.internal.runtime.compiled.codegen;

public final class setStaticField$
{
    public static setStaticField$ MODULE$;

    static
    {
        new setStaticField$();
    }

    private setStaticField$()
    {
        MODULE$ = this;
    }

    public void apply( final Class<?> clazz, final String name, final Object value )
    {
        clazz.getDeclaredField( name ).set( (Object) null, value );
    }
}
