package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction1;

public final class MyMap$ extends AbstractFunction1<Map<String,CodeGenExpression>,MyMap> implements Serializable
{
    public static MyMap$ MODULE$;

    static
    {
        new MyMap$();
    }

    private MyMap$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MyMap";
    }

    public MyMap apply( final Map<String,CodeGenExpression> instructions )
    {
        return new MyMap( instructions );
    }

    public Option<Map<String,CodeGenExpression>> unapply( final MyMap x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.instructions() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
