package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class NodeExpression$ extends AbstractFunction1<Variable,NodeExpression> implements Serializable
{
    public static NodeExpression$ MODULE$;

    static
    {
        new NodeExpression$();
    }

    private NodeExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodeExpression";
    }

    public NodeExpression apply( final Variable nodeIdVar )
    {
        return new NodeExpression( nodeIdVar );
    }

    public Option<Variable> unapply( final NodeExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.nodeIdVar() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
