package org.neo4j.cypher.internal.runtime.compiled.codegen;

import java.util.concurrent.atomic.AtomicInteger;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public class Namer
{
    private final String varPrefix;
    private final String methodPrefix;
    private final String operationPrefix;
    private int methodNameCounter;
    private int varNameCounter;
    private int opNameCounter;

    public Namer( final AtomicInteger classNameCounter, final String varPrefix, final String methodPrefix, final String operationPrefix )
    {
        this.varPrefix = varPrefix;
        this.methodPrefix = methodPrefix;
        this.operationPrefix = operationPrefix;
        this.methodNameCounter = 0;
        this.varNameCounter = 0;
        this.opNameCounter = 0;
    }

    public static String $lessinit$greater$default$4()
    {
        return Namer$.MODULE$.$lessinit$greater$default$4();
    }

    public static String $lessinit$greater$default$3()
    {
        return Namer$.MODULE$.$lessinit$greater$default$3();
    }

    public static String $lessinit$greater$default$2()
    {
        return Namer$.MODULE$.$lessinit$greater$default$2();
    }

    public static String newClassName()
    {
        return Namer$.MODULE$.newClassName();
    }

    public static Namer apply()
    {
        return Namer$.MODULE$.apply();
    }

    private int methodNameCounter()
    {
        return this.methodNameCounter;
    }

    private void methodNameCounter_$eq( final int x$1 )
    {
        this.methodNameCounter = x$1;
    }

    private int varNameCounter()
    {
        return this.varNameCounter;
    }

    private void varNameCounter_$eq( final int x$1 )
    {
        this.varNameCounter = x$1;
    }

    private int opNameCounter()
    {
        return this.opNameCounter;
    }

    private void opNameCounter_$eq( final int x$1 )
    {
        this.opNameCounter = x$1;
    }

    public String newMethodName()
    {
        this.methodNameCounter_$eq( this.methodNameCounter() + 1 );
        return (new StringBuilder( 0 )).append( this.methodPrefix ).append( this.methodNameCounter() ).toString();
    }

    public String newVarName()
    {
        this.varNameCounter_$eq( this.varNameCounter() + 1 );
        return (new StringBuilder( 0 )).append( this.varPrefix ).append( this.varNameCounter() ).toString();
    }

    public String newOpName( final String planName )
    {
        this.opNameCounter_$eq( this.opNameCounter() + 1 );
        return (new StringBuilder( 1 )).append( this.operationPrefix ).append( this.opNameCounter() ).append( "_" ).append( planName ).toString();
    }
}
