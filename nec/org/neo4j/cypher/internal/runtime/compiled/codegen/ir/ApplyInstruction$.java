package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class ApplyInstruction$ extends AbstractFunction2<String,Instruction,ApplyInstruction> implements Serializable
{
    public static ApplyInstruction$ MODULE$;

    static
    {
        new ApplyInstruction$();
    }

    private ApplyInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ApplyInstruction";
    }

    public ApplyInstruction apply( final String id, final Instruction instruction )
    {
        return new ApplyInstruction( id, instruction );
    }

    public Option<Tuple2<String,Instruction>> unapply( final ApplyInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.id(), x$0.instruction() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
