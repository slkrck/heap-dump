package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class ValueType
{
    public static String toString()
    {
        return ValueType$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return ValueType$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return ValueType$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return ValueType$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return ValueType$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return ValueType$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return ValueType$.MODULE$.productPrefix();
    }
}
