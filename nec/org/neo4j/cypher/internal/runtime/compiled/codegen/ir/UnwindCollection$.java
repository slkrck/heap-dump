package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class UnwindCollection$ extends AbstractFunction3<String,CodeGenExpression,CodeGenType,UnwindCollection> implements Serializable
{
    public static UnwindCollection$ MODULE$;

    static
    {
        new UnwindCollection$();
    }

    private UnwindCollection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "UnwindCollection";
    }

    public UnwindCollection apply( final String opName, final CodeGenExpression collection, final CodeGenType elementCodeGenType )
    {
        return new UnwindCollection( opName, collection, elementCodeGenType );
    }

    public Option<Tuple3<String,CodeGenExpression,CodeGenType>> unapply( final UnwindCollection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.opName(), x$0.collection(), x$0.elementCodeGenType() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
