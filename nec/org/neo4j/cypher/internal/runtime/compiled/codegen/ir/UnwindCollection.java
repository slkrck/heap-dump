package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.Predef.;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class UnwindCollection implements LoopDataGenerator, Product, Serializable
{
    private final String opName;
    private final CodeGenExpression collection;
    private final CodeGenType elementCodeGenType;

    public UnwindCollection( final String opName, final CodeGenExpression collection, final CodeGenType elementCodeGenType )
    {
        this.opName = opName;
        this.collection = collection;
        this.elementCodeGenType = elementCodeGenType;
        Product.$init$( this );
    }

    public static Option<Tuple3<String,CodeGenExpression,CodeGenType>> unapply( final UnwindCollection x$0 )
    {
        return UnwindCollection$.MODULE$.unapply( var0 );
    }

    public static UnwindCollection apply( final String opName, final CodeGenExpression collection, final CodeGenType elementCodeGenType )
    {
        return UnwindCollection$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,CodeGenExpression,CodeGenType>,UnwindCollection> tupled()
    {
        return UnwindCollection$.MODULE$.tupled();
    }

    public static Function1<String,Function1<CodeGenExpression,Function1<CodeGenType,UnwindCollection>>> curried()
    {
        return UnwindCollection$.MODULE$.curried();
    }

    public String opName()
    {
        return this.opName;
    }

    public CodeGenExpression collection()
    {
        return this.collection;
    }

    public CodeGenType elementCodeGenType()
    {
        return this.elementCodeGenType;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.collection().init( generator, context );
    }

    public <E> void produceLoopData( final String iterVar, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.declareIterator( iterVar, this.elementCodeGenType() );
        Object iterator = generator.iteratorFrom( this.collection().generateExpression( generator, context ) );
        generator.assign( iterVar, this.elementCodeGenType(), iterator );
    }

    public <E> void getNext( final Variable nextVar, final String iterVar, final MethodStructure<E> generator, final CodeGenContext context )
    {
        var10000;
        boolean var7;
        label17:
        {
            label16:
            {
                var10000 = .MODULE$;
                CodeGenType var10001 = this.elementCodeGenType();
                CodeGenType var5 = nextVar.codeGenType();
                if ( var10001 == null )
                {
                    if ( var5 == null )
                    {
                        break label16;
                    }
                }
                else if ( var10001.equals( var5 ) )
                {
                    break label16;
                }

                var7 = false;
                break label17;
            }

            var7 = true;
        }

        var10000. assert (var7);
        Object next = generator.iteratorNext( generator.loadVariable( iterVar ), nextVar.codeGenType() );
        generator.assign( nextVar, next );
    }

    public <E> E checkNext( final MethodStructure<E> generator, final String iterVar )
    {
        return generator.iteratorHasNext( generator.loadVariable( iterVar ) );
    }

    public <E> void close( final String iterVarName, final MethodStructure<E> generator )
    {
    }

    public UnwindCollection copy( final String opName, final CodeGenExpression collection, final CodeGenType elementCodeGenType )
    {
        return new UnwindCollection( opName, collection, elementCodeGenType );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.collection();
    }

    public CodeGenType copy$default$3()
    {
        return this.elementCodeGenType();
    }

    public String productPrefix()
    {
        return "UnwindCollection";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.collection();
            break;
        case 2:
            var10000 = this.elementCodeGenType();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof UnwindCollection;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof UnwindCollection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            UnwindCollection var4 = (UnwindCollection) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            CodeGenExpression var8 = this.collection();
                            CodeGenExpression var6 = var4.collection();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            CodeGenType var9 = this.elementCodeGenType();
                            CodeGenType var7 = var4.elementCodeGenType();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
