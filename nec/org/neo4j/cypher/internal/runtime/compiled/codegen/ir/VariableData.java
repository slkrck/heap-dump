package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class VariableData implements Product, Serializable
{
    private final String variable;
    private final Variable incoming;
    private final Variable outgoing;

    public VariableData( final String variable, final Variable incoming, final Variable outgoing )
    {
        this.variable = variable;
        this.incoming = incoming;
        this.outgoing = outgoing;
        Product.$init$( this );
    }

    public static Option<Tuple3<String,Variable,Variable>> unapply( final VariableData x$0 )
    {
        return VariableData$.MODULE$.unapply( var0 );
    }

    public static VariableData apply( final String variable, final Variable incoming, final Variable outgoing )
    {
        return VariableData$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,Variable,Variable>,VariableData> tupled()
    {
        return VariableData$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Variable,Function1<Variable,VariableData>>> curried()
    {
        return VariableData$.MODULE$.curried();
    }

    public String variable()
    {
        return this.variable;
    }

    public Variable incoming()
    {
        return this.incoming;
    }

    public Variable outgoing()
    {
        return this.outgoing;
    }

    public VariableData copy( final String variable, final Variable incoming, final Variable outgoing )
    {
        return new VariableData( variable, incoming, outgoing );
    }

    public String copy$default$1()
    {
        return this.variable();
    }

    public Variable copy$default$2()
    {
        return this.incoming();
    }

    public Variable copy$default$3()
    {
        return this.outgoing();
    }

    public String productPrefix()
    {
        return "VariableData";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.variable();
            break;
        case 1:
            var10000 = this.incoming();
            break;
        case 2:
            var10000 = this.outgoing();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof VariableData;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var9;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof VariableData )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            VariableData var4 = (VariableData) x$1;
                            String var10000 = this.variable();
                            String var5 = var4.variable();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            Variable var8 = this.incoming();
                            Variable var6 = var4.incoming();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            var8 = this.outgoing();
                            Variable var7 = var4.outgoing();
                            if ( var8 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var9 = true;
                                break label54;
                            }
                        }

                        var9 = false;
                    }

                    if ( var9 )
                    {
                        break label72;
                    }
                }

                var9 = false;
                return var9;
            }
        }

        var9 = true;
        return var9;
    }
}
