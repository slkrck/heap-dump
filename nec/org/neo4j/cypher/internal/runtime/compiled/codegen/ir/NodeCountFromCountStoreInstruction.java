package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable..colon.colon;
import scala.collection.immutable.List.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeCountFromCountStoreInstruction implements Instruction, Product, Serializable
{
    private final String opName;
    private final Variable variable;
    private final List<Option<Tuple2<Option<Object>,String>>> labels;
    private final Instruction inner;

    public NodeCountFromCountStoreInstruction( final String opName, final Variable variable, final List<Option<Tuple2<Option<Object>,String>>> labels,
            final Instruction inner )
    {
        this.opName = opName;
        this.variable = variable;
        this.labels = labels;
        this.inner = inner;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<String,Variable,List<Option<Tuple2<Option<Object>,String>>>,Instruction>> unapply(
            final NodeCountFromCountStoreInstruction x$0 )
    {
        return NodeCountFromCountStoreInstruction$.MODULE$.unapply( var0 );
    }

    public static NodeCountFromCountStoreInstruction apply( final String opName, final Variable variable,
            final List<Option<Tuple2<Option<Object>,String>>> labels, final Instruction inner )
    {
        return NodeCountFromCountStoreInstruction$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<String,Variable,List<Option<Tuple2<Option<Object>,String>>>,Instruction>,NodeCountFromCountStoreInstruction> tupled()
    {
        return NodeCountFromCountStoreInstruction$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Variable,Function1<List<Option<Tuple2<Option<Object>,String>>>,Function1<Instruction,NodeCountFromCountStoreInstruction>>>> curried()
    {
        return NodeCountFromCountStoreInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String opName()
    {
        return this.opName;
    }

    public Variable variable()
    {
        return this.variable;
    }

    public List<Option<Tuple2<Option<Object>,String>>> labels()
    {
        return this.labels;
    }

    public Instruction inner()
    {
        return this.inner;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        List ops = (List) this.labels().map( ( label ) -> {
            return this.findOps( label );
        },.MODULE$.canBuildFrom());
        generator.trace( this.opName(), generator.trace$default$2(), ( body ) -> {
            $anonfun$body$2( this, context, ops, body );
            return BoxedUnit.UNIT;
        } );
    }

    private <E> E multiplyAll( final List<Function1<MethodStructure<E>,E>> ops, final MethodStructure<E> body )
    {
        while ( true )
        {
            boolean var5 = false;
            colon var6 = null;
            if ( scala.collection.immutable.Nil..MODULE$.equals( ops )){
            throw new IllegalStateException( "At least one operation must be present at this stage" );
        }

            Object var4;
            if ( ops instanceof colon )
            {
                var5 = true;
                var6 = (colon) ops;
                Function1 f = (Function1) var6.head();
                List var9 = var6.tl$access$1();
                if ( scala.collection.immutable.Nil..MODULE$.equals( var9 )){
                var4 = f.apply( body );
                return var4;
            }
            }

            if ( var5 )
            {
                Function1 a = (Function1) var6.head();
                List var11 = var6.tl$access$1();
                if ( var11 instanceof colon )
                {
                    colon var12 = (colon) var11;
                    Function1 b = (Function1) var12.head();
                    List var14 = var12.tl$access$1();
                    if ( scala.collection.immutable.Nil..MODULE$.equals( var14 )){
                    var4 = body.multiplyPrimitive( a.apply( body ), b.apply( body ) );
                    return var4;
                }
                }
            }

            if ( var5 )
            {
                Function1 a = (Function1) var6.head();
                List var16 = var6.tl$access$1();
                if ( var16 instanceof colon )
                {
                    colon var17 = (colon) var16;
                    Function1 b = (Function1) var17.head();
                    List tl = var17.tl$access$1();
                    Function1 var20 = ( bb ) -> {
                        return bb.multiplyPrimitive( a.apply( bb ), b.apply( bb ) );
                    };
                    List var10000 = tl.$colon$colon( var20 );
                    body = body;
                    ops = var10000;
                    continue;
                }
            }

            throw new MatchError( ops );
        }
    }

    private <E> Function1<MethodStructure<E>,E> findOps( final Option<Tuple2<Option<Object>,String>> label )
    {
        boolean var3 = false;
        Some var4 = null;
        Function1 var2;
        if ( scala.None..MODULE$.equals( label )){
        var2 = ( body ) -> {
            return body.nodeCountFromCountStore( body.wildCardToken() );
        };
    } else{
        if ( label instanceof Some )
        {
            var3 = true;
            var4 = (Some) label;
            Tuple2 var6 = (Tuple2) var4.value();
            if ( var6 != null )
            {
                Option var7 = (Option) var6._1();
                if ( var7 instanceof Some )
                {
                    Some var8 = (Some) var7;
                    int token = BoxesRunTime.unboxToInt( var8.value() );
                    var2 = ( body ) -> {
                        Object tokenConstant = body.token( scala.Predef..MODULE$.Integer2int( BoxesRunTime.boxToInteger( token ) ));
                        return body.nodeCountFromCountStore( tokenConstant );
                    }; return var2;
                }
            }
        }

        if ( !var3 )
        {
            throw new MatchError( label );
        }

        Tuple2 var10 = (Tuple2) var4.value();
        if ( var10 == null )
        {
            throw new MatchError( label );
        }

        Option var11 = (Option) var10._1();
        String labelName = (String) var10._2();
        if ( !scala.None..MODULE$.equals( var11 )){
            throw new MatchError( label );
        }

        var2 = ( body ) -> {
            Object isMissing = body.primitiveEquals( body.loadVariable( this.tokenVar( labelName ) ), body.wildCardToken() );
            Object zero = body.constantExpression( BoxesRunTime.boxToLong( 0L ) );
            Object getFromCountStore = body.nodeCountFromCountStore( body.loadVariable( this.tokenVar( labelName ) ) );
            return body.ternaryOperator( isMissing, zero, getFromCountStore );
        };
    }

        return var2;
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.opName()}) ));
    }

    public Seq<Instruction> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.inner()}) ));
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        Instruction.init$( this, generator, context );
        this.labelNames().foreach( ( x0$1 ) -> {
            $anonfun$init$1( this, generator, x0$1 );
            return BoxedUnit.UNIT;
        } );
    }

    private Set<Tuple2<Option<Object>,String>> labelNames()
    {
        return ((TraversableOnce) ((GenericTraversableTemplate) this.labels().filter( ( x$3 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$labelNames$1( x$3 ) );
        } )).flatten( ( xo ) -> {
            return scala.Option..MODULE$.option2Iterable( xo );
        } )).toSet();
    }

    private String tokenVar( final String label )
    {
        return (new StringBuilder( 5 )).append( label ).append( "Token" ).toString();
    }

    public NodeCountFromCountStoreInstruction copy( final String opName, final Variable variable, final List<Option<Tuple2<Option<Object>,String>>> labels,
            final Instruction inner )
    {
        return new NodeCountFromCountStoreInstruction( opName, variable, labels, inner );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public Variable copy$default$2()
    {
        return this.variable();
    }

    public List<Option<Tuple2<Option<Object>,String>>> copy$default$3()
    {
        return this.labels();
    }

    public Instruction copy$default$4()
    {
        return this.inner();
    }

    public String productPrefix()
    {
        return "NodeCountFromCountStoreInstruction";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.variable();
            break;
        case 2:
            var10000 = this.labels();
            break;
        case 3:
            var10000 = this.inner();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NodeCountFromCountStoreInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof NodeCountFromCountStoreInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            NodeCountFromCountStoreInstruction var4 = (NodeCountFromCountStoreInstruction) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            Variable var9 = this.variable();
                            Variable var6 = var4.variable();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            List var10 = this.labels();
                            List var7 = var4.labels();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            Instruction var11 = this.inner();
                            Instruction var8 = var4.inner();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
