package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class AggregationConverter
{
    public static AggregateExpression aggregateExpressionConverter( final String opName, final Iterable<Tuple2<String,CodeGenExpression>> groupingVariables,
            final String name, final Expression e, final CodeGenContext context )
    {
        return AggregationConverter$.MODULE$.aggregateExpressionConverter( var0, var1, var2, var3, var4 );
    }
}
