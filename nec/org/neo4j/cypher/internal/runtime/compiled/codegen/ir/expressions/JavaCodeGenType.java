package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class JavaCodeGenType implements CodeGenType, Product, Serializable
{
    private final RepresentationType repr;

    public JavaCodeGenType( final RepresentationType repr )
    {
        this.repr = repr;
        Product.$init$( this );
    }

    public static Option<RepresentationType> unapply( final JavaCodeGenType x$0 )
    {
        return JavaCodeGenType$.MODULE$.unapply( var0 );
    }

    public static JavaCodeGenType apply( final RepresentationType repr )
    {
        return JavaCodeGenType$.MODULE$.apply( var0 );
    }

    public static <A> Function1<RepresentationType,A> andThen( final Function1<JavaCodeGenType,A> g )
    {
        return JavaCodeGenType$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,JavaCodeGenType> compose( final Function1<A,RepresentationType> g )
    {
        return JavaCodeGenType$.MODULE$.compose( var0 );
    }

    public RepresentationType repr()
    {
        return this.repr;
    }

    public boolean isPrimitive()
    {
        return RepresentationType$.MODULE$.isPrimitive( this.repr() );
    }

    public boolean isValue()
    {
        return RepresentationType$.MODULE$.isValue( this.repr() );
    }

    public boolean isAnyValue()
    {
        return RepresentationType$.MODULE$.isAnyValue( this.repr() );
    }

    public boolean canBeNullable()
    {
        return false;
    }

    public JavaCodeGenType copy( final RepresentationType repr )
    {
        return new JavaCodeGenType( repr );
    }

    public RepresentationType copy$default$1()
    {
        return this.repr();
    }

    public String productPrefix()
    {
        return "JavaCodeGenType";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.repr();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof JavaCodeGenType;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof JavaCodeGenType )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        JavaCodeGenType var4 = (JavaCodeGenType) x$1;
                        RepresentationType var10000 = this.repr();
                        RepresentationType var5 = var4.repr();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
