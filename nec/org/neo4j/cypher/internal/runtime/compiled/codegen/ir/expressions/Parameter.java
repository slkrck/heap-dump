package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class Parameter implements CodeGenExpression, Product, Serializable
{
    private final String key;
    private final String variableName;
    private final CypherCodeGenType cType;

    public Parameter( final String key, final String variableName, final CypherCodeGenType cType )
    {
        this.key = key;
        this.variableName = variableName;
        this.cType = cType;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static CypherCodeGenType $lessinit$greater$default$3()
    {
        return Parameter$.MODULE$.$lessinit$greater$default$3();
    }

    public static CypherCodeGenType apply$default$3()
    {
        return Parameter$.MODULE$.apply$default$3();
    }

    public static Option<Tuple3<String,String,CypherCodeGenType>> unapply( final Parameter x$0 )
    {
        return Parameter$.MODULE$.unapply( var0 );
    }

    public static Parameter apply( final String key, final String variableName, final CypherCodeGenType cType )
    {
        return Parameter$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,String,CypherCodeGenType>,Parameter> tupled()
    {
        return Parameter$.MODULE$.tupled();
    }

    public static Function1<String,Function1<String,Function1<CypherCodeGenType,Parameter>>> curried()
    {
        return Parameter$.MODULE$.curried();
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public String key()
    {
        return this.key;
    }

    public String variableName()
    {
        return this.variableName;
    }

    public CypherCodeGenType cType()
    {
        return this.cType;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.expectParameter( this.key(), this.variableName(), this.cType() );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.loadVariable( this.variableName() );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.cType().canBeNullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return this.cType();
    }

    public Parameter copy( final String key, final String variableName, final CypherCodeGenType cType )
    {
        return new Parameter( key, variableName, cType );
    }

    public String copy$default$1()
    {
        return this.key();
    }

    public String copy$default$2()
    {
        return this.variableName();
    }

    public CypherCodeGenType copy$default$3()
    {
        return this.cType();
    }

    public String productPrefix()
    {
        return "Parameter";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.key();
            break;
        case 1:
            var10000 = this.variableName();
            break;
        case 2:
            var10000 = this.cType();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Parameter;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var9;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof Parameter )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            Parameter var4 = (Parameter) x$1;
                            String var10000 = this.key();
                            String var5 = var4.key();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            var10000 = this.variableName();
                            String var6 = var4.variableName();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label63;
                            }

                            CypherCodeGenType var8 = this.cType();
                            CypherCodeGenType var7 = var4.cType();
                            if ( var8 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var9 = true;
                                break label54;
                            }
                        }

                        var9 = false;
                    }

                    if ( var9 )
                    {
                        break label72;
                    }
                }

                var9 = false;
                return var9;
            }
        }

        var9 = true;
        return var9;
    }
}
