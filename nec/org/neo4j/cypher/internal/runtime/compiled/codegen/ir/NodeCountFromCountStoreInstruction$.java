package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple4;
import scala.None.;
import scala.collection.immutable.List;
import scala.runtime.AbstractFunction4;

public final class NodeCountFromCountStoreInstruction$
        extends AbstractFunction4<String,Variable,List<Option<Tuple2<Option<Object>,String>>>,Instruction,NodeCountFromCountStoreInstruction>
        implements Serializable
{
    public static NodeCountFromCountStoreInstruction$ MODULE$;

    static
    {
        new NodeCountFromCountStoreInstruction$();
    }

    private NodeCountFromCountStoreInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodeCountFromCountStoreInstruction";
    }

    public NodeCountFromCountStoreInstruction apply( final String opName, final Variable variable, final List<Option<Tuple2<Option<Object>,String>>> labels,
            final Instruction inner )
    {
        return new NodeCountFromCountStoreInstruction( opName, variable, labels, inner );
    }

    public Option<Tuple4<String,Variable,List<Option<Tuple2<Option<Object>,String>>>,Instruction>> unapply( final NodeCountFromCountStoreInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.opName(), x$0.variable(), x$0.labels(), x$0.inner() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
