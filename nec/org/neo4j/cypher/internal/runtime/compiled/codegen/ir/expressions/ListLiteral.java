package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.runtime.compiled.helpers.LiteralTypeSupport$;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.ListType.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ListLiteral implements CodeGenExpression, Product, Serializable
{
    private final Seq<CodeGenExpression> expressions;

    public ListLiteral( final Seq<CodeGenExpression> expressions )
    {
        this.expressions = expressions;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Seq<CodeGenExpression>> unapply( final ListLiteral x$0 )
    {
        return ListLiteral$.MODULE$.unapply( var0 );
    }

    public static ListLiteral apply( final Seq<CodeGenExpression> expressions )
    {
        return ListLiteral$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Seq<CodeGenExpression>,A> andThen( final Function1<ListLiteral,A> g )
    {
        return ListLiteral$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ListLiteral> compose( final Function1<A,Seq<CodeGenExpression>> g )
    {
        return ListLiteral$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Seq<CodeGenExpression> expressions()
    {
        return this.expressions;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.expressions().foreach( ( instruction ) -> {
            $anonfun$init$1( generator, context, instruction );
            return BoxedUnit.UNIT;
        } );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        CypherCodeGenType var4 = this.codeGenType( context );
        Object var3;
        if ( var4 != null )
        {
            CypherType var5 = var4.ct();
            RepresentationType var6 = var4.repr();
            Option var7 = .MODULE$.unapply( var5 );
            if ( !var7.isEmpty() && var6 instanceof ListReferenceType )
            {
                ListReferenceType var8 = (ListReferenceType) var6;
                RepresentationType innerRepr = var8.inner();
                if ( RepresentationType$.MODULE$.isPrimitive( innerRepr ) )
                {
                    var3 = structure.asPrimitiveStream( (Seq) ((Seq) this.expressions().map( ( e ) -> {
                        return e.generateExpression( structure, context );
                    }, scala.collection.Seq..MODULE$.canBuildFrom()) ), var4);
                    return var3;
                }
            }
        }

        var3 = structure.asAnyValueList( (Seq) this.expressions().map( ( e ) -> {
            CypherCodeGenType var4 = e.codeGenType( context );
            Object var3;
            if ( var4 != null )
            {
                CypherType var5 = var4.ct();
                RepresentationType var6 = var4.repr();
                Option var7 = .MODULE$.unapply( var5 );
                if ( !var7.isEmpty() && var6 instanceof ListReferenceType )
                {
                    ListReferenceType var8 = (ListReferenceType) var6;
                    RepresentationType innerRepr = var8.inner();
                    if ( RepresentationType$.MODULE$.isPrimitive( innerRepr ) )
                    {
                        var3 = structure.toMaterializedAnyValue( structure.iteratorFrom( e.generateExpression( structure, context ) ),
                                e.codeGenType( context ) );
                        return var3;
                    }
                }
            }

            var3 = structure.toMaterializedAnyValue( e.generateExpression( structure, context ), e.codeGenType( context ) );
            return var3;
        }, scala.collection.Seq..MODULE$.canBuildFrom() ));
        return var3;
    }

    public boolean nullable( final CodeGenContext context )
    {
        return false;
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        CypherType commonType = this.expressions().nonEmpty() ? (CypherType) ((TraversableOnce) this.expressions().map( ( x$1 ) -> {
            return x$1.codeGenType( context ).ct();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).reduce( ( x$2, x$3 ) -> {
        return x$2.leastUpperBound( x$3 );
    } ) :org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
        RepresentationType representationType = this.expressions().nonEmpty() ? LiteralTypeSupport$.MODULE$.selectRepresentationType( (CypherType) commonType,
                (Seq) this.expressions().map( ( x$4 ) -> {
                    return x$4.codeGenType( context ).repr();
                }, scala.collection.Seq..MODULE$.canBuildFrom() )) :AnyValueType$.MODULE$;
        return new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..
        MODULE$.CTList( (CypherType) commonType ), new ListReferenceType( (RepresentationType) representationType ));
    }

    public ListLiteral copy( final Seq<CodeGenExpression> expressions )
    {
        return new ListLiteral( expressions );
    }

    public Seq<CodeGenExpression> copy$default$1()
    {
        return this.expressions();
    }

    public String productPrefix()
    {
        return "ListLiteral";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.expressions();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ListLiteral;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof ListLiteral )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        ListLiteral var4 = (ListLiteral) x$1;
                        Seq var10000 = this.expressions();
                        Seq var5 = var4.expressions();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
