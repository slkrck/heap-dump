package org.neo4j.cypher.internal.runtime.compiled.codegen;

public final class CodeGenMode$
{
    public static CodeGenMode$ MODULE$;
    private final ByteCodeMode$ default;

    static
    {
        new CodeGenMode$();
    }

    public ByteCodeMode$ default()

    {
        return this.
        default
            ;
    }

    private CodeGenMode$()
    {
        MODULE$ = this;
        this.
        default =ByteCodeMode$.MODULE$;
    }
}
