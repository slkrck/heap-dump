package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple6;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction6;

public final class ExpandIntoLoopDataGenerator$
        extends AbstractFunction6<String,Variable,SemanticDirection,Map<String,String>,Variable,Variable,ExpandIntoLoopDataGenerator> implements Serializable
{
    public static ExpandIntoLoopDataGenerator$ MODULE$;

    static
    {
        new ExpandIntoLoopDataGenerator$();
    }

    private ExpandIntoLoopDataGenerator$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ExpandIntoLoopDataGenerator";
    }

    public ExpandIntoLoopDataGenerator apply( final String opName, final Variable fromVar, final SemanticDirection dir, final Map<String,String> types,
            final Variable toVar, final Variable relVar )
    {
        return new ExpandIntoLoopDataGenerator( opName, fromVar, dir, types, toVar, relVar );
    }

    public Option<Tuple6<String,Variable,SemanticDirection,Map<String,String>,Variable,Variable>> unapply( final ExpandIntoLoopDataGenerator x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple6( x$0.opName(), x$0.fromVar(), x$0.dir(), x$0.types(), x$0.toVar(), x$0.relVar() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
