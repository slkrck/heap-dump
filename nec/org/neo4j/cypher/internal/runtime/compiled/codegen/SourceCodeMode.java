package org.neo4j.cypher.internal.runtime.compiled.codegen;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class SourceCodeMode
{
    public static String toString()
    {
        return SourceCodeMode$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return SourceCodeMode$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return SourceCodeMode$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return SourceCodeMode$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return SourceCodeMode$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return SourceCodeMode$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return SourceCodeMode$.MODULE$.productPrefix();
    }
}
