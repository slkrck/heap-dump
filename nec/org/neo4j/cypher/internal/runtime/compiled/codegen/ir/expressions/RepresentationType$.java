package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

public final class RepresentationType$
{
    public static RepresentationType$ MODULE$;

    static
    {
        new RepresentationType$();
    }

    private RepresentationType$()
    {
        MODULE$ = this;
    }

    public boolean isPrimitive( final RepresentationType repr )
    {
        boolean var3;
        if ( IntType$.MODULE$.equals( repr ) )
        {
            var3 = true;
        }
        else if ( LongType$.MODULE$.equals( repr ) )
        {
            var3 = true;
        }
        else if ( FloatType$.MODULE$.equals( repr ) )
        {
            var3 = true;
        }
        else if ( BoolType$.MODULE$.equals( repr ) )
        {
            var3 = true;
        }
        else
        {
            var3 = false;
        }

        boolean var2;
        if ( var3 )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public boolean isValue( final RepresentationType repr )
    {
        boolean var2;
        if ( ValueType$.MODULE$.equals( repr ) )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public boolean isAnyValue( final RepresentationType repr )
    {
        boolean var3;
        if ( AnyValueType$.MODULE$.equals( repr ) )
        {
            var3 = true;
        }
        else if ( ValueType$.MODULE$.equals( repr ) )
        {
            var3 = true;
        }
        else
        {
            var3 = false;
        }

        boolean var2;
        if ( var3 )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }
}
