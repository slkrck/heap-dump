package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class IdCodeGenFunction
{
    public static String toString()
    {
        return IdCodeGenFunction$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return IdCodeGenFunction$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return IdCodeGenFunction$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return IdCodeGenFunction$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return IdCodeGenFunction$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return IdCodeGenFunction$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return IdCodeGenFunction$.MODULE$.productPrefix();
    }

    public static CodeGenExpression apply( final CodeGenExpression arg )
    {
        return IdCodeGenFunction$.MODULE$.apply( var0 );
    }
}
