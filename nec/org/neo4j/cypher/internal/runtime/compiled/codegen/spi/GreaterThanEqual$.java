package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class GreaterThanEqual$ implements Comparator, Product, Serializable
{
    public static GreaterThanEqual$ MODULE$;

    static
    {
        new GreaterThanEqual$();
    }

    private GreaterThanEqual$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "GreaterThanEqual";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof GreaterThanEqual$;
    }

    public int hashCode()
    {
        return -1214294183;
    }

    public String toString()
    {
        return "GreaterThanEqual";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
