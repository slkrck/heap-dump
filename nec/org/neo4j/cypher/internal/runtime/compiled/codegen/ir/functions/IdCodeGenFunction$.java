package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.IdOf;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.NodeExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.NodeProjection;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RelationshipExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RelationshipProjection;
import org.neo4j.exceptions.InternalException;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class IdCodeGenFunction$ implements CodeGenFunction1, Product, Serializable
{
    public static IdCodeGenFunction$ MODULE$;

    static
    {
        new IdCodeGenFunction$();
    }

    private IdCodeGenFunction$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public CodeGenExpression apply( final CodeGenExpression arg )
    {
        IdOf var2;
        if ( arg instanceof NodeExpression )
        {
            NodeExpression var4 = (NodeExpression) arg;
            Variable n = var4.nodeIdVar();
            var2 = new IdOf( n );
        }
        else if ( arg instanceof NodeProjection )
        {
            NodeProjection var6 = (NodeProjection) arg;
            Variable n = var6.nodeIdVar();
            var2 = new IdOf( n );
        }
        else if ( arg instanceof RelationshipExpression )
        {
            RelationshipExpression var8 = (RelationshipExpression) arg;
            Variable r = var8.relId();
            var2 = new IdOf( r );
        }
        else
        {
            if ( !(arg instanceof RelationshipProjection) )
            {
                throw new InternalException(
                        (new StringBuilder( 52 )).append( "id function only accepts nodes or relationships not " ).append( arg ).toString() );
            }

            RelationshipProjection var10 = (RelationshipProjection) arg;
            Variable r = var10.relId();
            var2 = new IdOf( r );
        }

        return var2;
    }

    public String productPrefix()
    {
        return "IdCodeGenFunction";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof IdCodeGenFunction$;
    }

    public int hashCode()
    {
        return 2101328992;
    }

    public String toString()
    {
        return "IdCodeGenFunction";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
