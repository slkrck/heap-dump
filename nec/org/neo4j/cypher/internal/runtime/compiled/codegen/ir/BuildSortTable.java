package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.FullSortTableDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortItem;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class BuildSortTable extends BuildSortTableBase implements Product, Serializable
{
    private final String opName;
    private final String tableName;
    private final Map<String,Variable> columnVariables;
    private final Iterable<SortItem> sortItems;
    private final double estimateCardinality;

    public BuildSortTable( final String opName, final String tableName, final Map<String,Variable> columnVariables, final Iterable<SortItem> sortItems,
            final double estimateCardinality, final CodeGenContext context )
    {
        super( opName, tableName, columnVariables, sortItems, context );
        this.opName = opName;
        this.tableName = tableName;
        this.columnVariables = columnVariables;
        this.sortItems = sortItems;
        this.estimateCardinality = estimateCardinality;
        Product.$init$( this );
    }

    public static Option<Tuple5<String,String,Map<String,Variable>,Iterable<SortItem>,Object>> unapply( final BuildSortTable x$0 )
    {
        return BuildSortTable$.MODULE$.unapply( var0 );
    }

    public static BuildSortTable apply( final String opName, final String tableName, final Map<String,Variable> columnVariables,
            final Iterable<SortItem> sortItems, final double estimateCardinality, final CodeGenContext context )
    {
        return BuildSortTable$.MODULE$.apply( var0, var1, var2, var3, var4, var6 );
    }

    public String opName()
    {
        return this.opName;
    }

    public String tableName()
    {
        return this.tableName;
    }

    public Map<String,Variable> columnVariables()
    {
        return this.columnVariables;
    }

    public Iterable<SortItem> sortItems()
    {
        return this.sortItems;
    }

    public double estimateCardinality()
    {
        return this.estimateCardinality;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        int initialCapacity = (int) Math.max( 128.0D, Math.min( this.estimateCardinality(), (double) 1073741823 ) );
        generator.allocateSortTable( this.tableName(), this.tableDescriptor(), generator.constantExpression( BoxesRunTime.boxToInteger( initialCapacity ) ) );
    }

    public FullSortTableDescriptor tableDescriptor()
    {
        return new FullSortTableDescriptor( this.tupleDescriptor() );
    }

    public BuildSortTable copy( final String opName, final String tableName, final Map<String,Variable> columnVariables, final Iterable<SortItem> sortItems,
            final double estimateCardinality, final CodeGenContext context )
    {
        return new BuildSortTable( opName, tableName, columnVariables, sortItems, estimateCardinality, context );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public String copy$default$2()
    {
        return this.tableName();
    }

    public Map<String,Variable> copy$default$3()
    {
        return this.columnVariables();
    }

    public Iterable<SortItem> copy$default$4()
    {
        return this.sortItems();
    }

    public double copy$default$5()
    {
        return this.estimateCardinality();
    }

    public String productPrefix()
    {
        return "BuildSortTable";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.tableName();
            break;
        case 2:
            var10000 = this.columnVariables();
            break;
        case 3:
            var10000 = this.sortItems();
            break;
        case 4:
            var10000 = BoxesRunTime.boxToDouble( this.estimateCardinality() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof BuildSortTable;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.opName() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.tableName() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.columnVariables() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.sortItems() ) );
        var1 = Statics.mix( var1, Statics.doubleHash( this.estimateCardinality() ) );
        return Statics.finalizeHash( var1, 5 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label83:
            {
                boolean var2;
                if ( x$1 instanceof BuildSortTable )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label65:
                    {
                        label74:
                        {
                            BuildSortTable var4 = (BuildSortTable) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label74;
                            }

                            var10000 = this.tableName();
                            String var6 = var4.tableName();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label74;
                            }

                            Map var9 = this.columnVariables();
                            Map var7 = var4.columnVariables();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label74;
                            }

                            Iterable var10 = this.sortItems();
                            Iterable var8 = var4.sortItems();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label74;
                            }

                            if ( this.estimateCardinality() == var4.estimateCardinality() && var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label65;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label83;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
