package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CodeGenType
{
    static JavaCodeGenType javaLong()
    {
        return CodeGenType$.MODULE$.javaLong();
    }

    static JavaCodeGenType javaInt()
    {
        return CodeGenType$.MODULE$.javaInt();
    }

    static CypherCodeGenType primitiveBool()
    {
        return CodeGenType$.MODULE$.primitiveBool();
    }

    static CypherCodeGenType primitiveFloat()
    {
        return CodeGenType$.MODULE$.primitiveFloat();
    }

    static CypherCodeGenType primitiveInt()
    {
        return CodeGenType$.MODULE$.primitiveInt();
    }

    static CypherCodeGenType primitiveRel()
    {
        return CodeGenType$.MODULE$.primitiveRel();
    }

    static CypherCodeGenType primitiveNode()
    {
        return CodeGenType$.MODULE$.primitiveNode();
    }

    static CypherCodeGenType Value()
    {
        return CodeGenType$.MODULE$.Value();
    }

    static CypherCodeGenType AnyValue()
    {
        return CodeGenType$.MODULE$.AnyValue();
    }

    static CypherCodeGenType Any()
    {
        return CodeGenType$.MODULE$.Any();
    }

    boolean isPrimitive();

    boolean isValue();

    boolean isAnyValue();

    boolean canBeNullable();

    RepresentationType repr();
}
