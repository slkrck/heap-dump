package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.JoinTableMethod;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.runtime.AbstractFunction4;

public final class MethodInvocation$ extends AbstractFunction4<Set<String>,JoinTableMethod,String,Seq<Instruction>,MethodInvocation> implements Serializable
{
    public static MethodInvocation$ MODULE$;

    static
    {
        new MethodInvocation$();
    }

    private MethodInvocation$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MethodInvocation";
    }

    public MethodInvocation apply( final Set<String> operatorId, final JoinTableMethod symbol, final String methodName, final Seq<Instruction> statements )
    {
        return new MethodInvocation( operatorId, symbol, methodName, statements );
    }

    public Option<Tuple4<Set<String>,JoinTableMethod,String,Seq<Instruction>>> unapply( final MethodInvocation x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.operatorId(), x$0.symbol(), x$0.methodName(), x$0.statements() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
