package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class JavaCodeGenType$ extends AbstractFunction1<RepresentationType,JavaCodeGenType> implements Serializable
{
    public static JavaCodeGenType$ MODULE$;

    static
    {
        new JavaCodeGenType$();
    }

    private JavaCodeGenType$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "JavaCodeGenType";
    }

    public JavaCodeGenType apply( final RepresentationType repr )
    {
        return new JavaCodeGenType( repr );
    }

    public Option<RepresentationType> unapply( final JavaCodeGenType x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.repr() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
