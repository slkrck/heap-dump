package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.planner.spi.PlanningAttributes.Cardinalities;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import scala.Option;
import scala.Tuple2;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface LeafCodeGenPlan extends CodeGenPlan
{
    static void $init$( final LeafCodeGenPlan $this )
    {
    }

    default Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child,
            final Cardinalities cardinalities )
    {
        throw new UnsupportedOperationException( "Leaf plan does not consume" );
    }
}
