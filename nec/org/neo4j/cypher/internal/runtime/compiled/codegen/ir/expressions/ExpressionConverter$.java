package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.logical.plans.CoerceToPredicate;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions.functionConverter$;
import org.neo4j.cypher.internal.v4_0.expressions.Add;
import org.neo4j.cypher.internal.v4_0.expressions.Divide;
import org.neo4j.cypher.internal.v4_0.expressions.DoubleLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.HasLabels;
import org.neo4j.cypher.internal.v4_0.expressions.IntegerLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.LabelName;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.cypher.internal.v4_0.expressions.MapExpression;
import org.neo4j.cypher.internal.v4_0.expressions.Multiply;
import org.neo4j.cypher.internal.v4_0.expressions.Property;
import org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName;
import org.neo4j.cypher.internal.v4_0.expressions.StringLiteral;
import org.neo4j.cypher.internal.v4_0.expressions.Subtract;
import org.neo4j.cypher.internal.v4_0.util.symbols.AnyType;
import org.neo4j.cypher.internal.v4_0.util.symbols.BooleanType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.IntegerType;
import org.neo4j.cypher.internal.v4_0.util.symbols.MapType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.symbols.StringType;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable..colon.colon;
import scala.collection.immutable.Nil.;
import scala.runtime.BoxesRunTime;

public final class ExpressionConverter$
{
    public static ExpressionConverter$ MODULE$;

    static
    {
        new ExpressionConverter$();
    }

    private ExpressionConverter$()
    {
        MODULE$ = this;
    }

    public ExpressionConverter.ExpressionToPredicate ExpressionToPredicate( final CodeGenExpression expression )
    {
        return new ExpressionConverter.ExpressionToPredicate( expression );
    }

    public CodeGenExpression createPredicate( final Expression expression, final CodeGenContext context )
    {
        Object var4;
        while ( true )
        {
            boolean var5 = false;
            Property var6 = null;
            if ( expression instanceof HasLabels )
            {
                HasLabels var8 = (HasLabels) expression;
                Expression x = var8.expression();
                Seq var10 = var8.labels();
                if ( x instanceof LogicalVariable )
                {
                    LogicalVariable var11 = (LogicalVariable) x;
                    if ( var10 instanceof colon )
                    {
                        colon var12 = (colon) var10;
                        LabelName label = (LabelName) var12.head();
                        List var14 = var12.tl$access$1();
                        if (.MODULE$.equals( var14 )){
                        String labelIdVariable = context.namer().newVarName();
                        Variable nodeVariable = context.getVariable( var11.name() );
                        var4 = this.ExpressionToPredicate( new HasLabel( nodeVariable, labelIdVariable, label.name() ) ).asPredicate();
                        break;
                    }
                    }
                }
            }

            if ( expression instanceof Property )
            {
                var5 = true;
                var6 = (Property) expression;
                Expression x = var6.map();
                if ( x instanceof LogicalVariable )
                {
                    LogicalVariable var18 = (LogicalVariable) x;
                    if ( context.semanticTable().isNode( var18 ) )
                    {
                        var4 = this.ExpressionToPredicate( this.createExpression( var6, context ) ).asPredicate();
                        break;
                    }
                }
            }

            if ( var5 )
            {
                Expression x = var6.map();
                if ( x instanceof LogicalVariable )
                {
                    LogicalVariable var20 = (LogicalVariable) x;
                    if ( context.semanticTable().isRelationship( var20 ) )
                    {
                        var4 = this.ExpressionToPredicate( this.createExpression( var6, context ) ).asPredicate();
                        break;
                    }
                }
            }

            if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Not )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Not var21 = (org.neo4j.cypher.internal.v4_0.expressions.Not) expression;
                Expression e = var21.rhs();
                var4 = this.ExpressionToPredicate( new Not( this.createExpression( e, context ) ) ).asPredicate();
                break;
            }
            else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Equals )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Equals var23 = (org.neo4j.cypher.internal.v4_0.expressions.Equals) expression;
                Expression lhs = var23.lhs();
                Expression rhs = var23.rhs();
                var4 = this.ExpressionToPredicate( new Equals( this.createExpression( lhs, context ), this.createExpression( rhs, context ) ) ).asPredicate();
                break;
            }
            else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Or )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Or var26 = (org.neo4j.cypher.internal.v4_0.expressions.Or) expression;
                Expression lhs = var26.lhs();
                Expression rhs = var26.rhs();
                var4 = this.ExpressionToPredicate( new Or( this.createExpression( lhs, context ), this.createExpression( rhs, context ) ) ).asPredicate();
                break;
            }
            else if ( expression instanceof LogicalVariable )
            {
                LogicalVariable var29 = (LogicalVariable) expression;
                var4 = this.ExpressionToPredicate( this.createExpression( var29, context ) ).asPredicate();
                break;
            }
            else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.False )
            {
                var4 = False$.MODULE$;
                break;
            }
            else if ( !(expression instanceof org.neo4j.cypher.internal.v4_0.expressions.True) )
            {
                if ( !(expression instanceof CoerceToPredicate) )
                {
                    throw new CantCompileQueryException(
                            (new StringBuilder( 31 )).append( "Predicate of " ).append( expression ).append( " not yet supported" ).toString() );
                }

                CoerceToPredicate var30 = (CoerceToPredicate) expression;
                Expression inner = var30.inner();
                context = context;
                expression = inner;
            }
            else
            {
                var4 = True$.MODULE$;
                break;
            }
        }

        return (CodeGenExpression) var4;
    }

    public CodeGenExpression createExpression( final Expression expression, final CodeGenContext context )
    {
        return this.expressionConverter( expression, ( expressionx ) -> {
            return MODULE$.createExpression( expressionx, context );
        }, context );
    }

    public CodeGenExpression createMaterializeExpressionForVariable( final String variableQueryVariable, final CodeGenContext context )
    {
        Object var3;
        Variable variable;
        label363:
        {
            variable = context.getVariable( variableQueryVariable );
            boolean var7 = false;
            CypherCodeGenType var8 = null;
            CodeGenType var9 = variable.codeGenType();
            if ( var9 instanceof CypherCodeGenType )
            {
                var7 = true;
                var8 = (CypherCodeGenType) var9;
                CypherType var10 = var8.ct();
                NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                if ( var10000 == null )
                {
                    if ( var10 == null )
                    {
                        break label363;
                    }
                }
                else if ( var10000.equals( var10 ) )
                {
                    break label363;
                }
            }

            if ( var7 )
            {
                label364:
                {
                    CypherType var12 = var8.ct();
                    RelationshipType var73 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                    if ( var73 == null )
                    {
                        if ( var12 != null )
                        {
                            break label364;
                        }
                    }
                    else if ( !var73.equals( var12 ) )
                    {
                        break label364;
                    }

                    var3 = new RelationshipProjection( variable );
                    return (CodeGenExpression) var3;
                }
            }

            boolean var5;
            StringType var74;
            BooleanType var75;
            IntegerType var76;
            org.neo4j.cypher.internal.v4_0.util.symbols.FloatType var77;
            label291:
            {
                label329:
                {
                    if ( var9 instanceof CypherCodeGenType )
                    {
                        CypherCodeGenType var14 = (CypherCodeGenType) var9;
                        CypherType var15 = var14.ct();
                        var74 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
                        if ( var74 == null )
                        {
                            if ( var15 == null )
                            {
                                break label329;
                            }
                        }
                        else if ( var74.equals( var15 ) )
                        {
                            break label329;
                        }
                    }

                    if ( var9 instanceof CypherCodeGenType )
                    {
                        label323:
                        {
                            CypherCodeGenType var17 = (CypherCodeGenType) var9;
                            CypherType var18 = var17.ct();
                            var75 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                            if ( var75 == null )
                            {
                                if ( var18 != null )
                                {
                                    break label323;
                                }
                            }
                            else if ( !var75.equals( var18 ) )
                            {
                                break label323;
                            }

                            var5 = true;
                            break label291;
                        }
                    }

                    label330:
                    {
                        if ( var9 instanceof CypherCodeGenType )
                        {
                            CypherCodeGenType var20 = (CypherCodeGenType) var9;
                            CypherType var21 = var20.ct();
                            var76 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                            if ( var76 == null )
                            {
                                if ( var21 == null )
                                {
                                    break label330;
                                }
                            }
                            else if ( var76.equals( var21 ) )
                            {
                                break label330;
                            }
                        }

                        if ( var9 instanceof CypherCodeGenType )
                        {
                            label324:
                            {
                                CypherCodeGenType var23 = (CypherCodeGenType) var9;
                                CypherType var24 = var23.ct();
                                var77 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                                if ( var77 == null )
                                {
                                    if ( var24 != null )
                                    {
                                        break label324;
                                    }
                                }
                                else if ( !var77.equals( var24 ) )
                                {
                                    break label324;
                                }

                                var5 = true;
                                break label291;
                            }
                        }

                        var5 = false;
                        break label291;
                    }

                    var5 = true;
                    break label291;
                }

                var5 = true;
            }

            if ( var5 )
            {
                var3 = new LoadVariable( variable );
                return (CodeGenExpression) var3;
            }

            if ( var7 )
            {
                CypherType var26 = var8.ct();
                RepresentationType var27 = var8.repr();
                Option var28 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var26 );
                if ( !var28.isEmpty() )
                {
                    label253:
                    {
                        CypherType var29 = (CypherType) var28.get();
                        var76 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                        if ( var76 == null )
                        {
                            if ( var29 != null )
                            {
                                break label253;
                            }
                        }
                        else if ( !var76.equals( var29 ) )
                        {
                            break label253;
                        }

                        if ( var27 instanceof ListReferenceType )
                        {
                            ListReferenceType var31 = (ListReferenceType) var27;
                            RepresentationType var32 = var31.inner();
                            if ( LongType$.MODULE$.equals( var32 ) )
                            {
                                var3 = new AnyProjection( variable );
                                return (CodeGenExpression) var3;
                            }
                        }
                    }
                }
            }

            if ( var7 )
            {
                CypherType var33 = var8.ct();
                RepresentationType var34 = var8.repr();
                Option var35 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var33 );
                if ( !var35.isEmpty() )
                {
                    label245:
                    {
                        CypherType var36 = (CypherType) var35.get();
                        var77 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                        if ( var77 == null )
                        {
                            if ( var36 != null )
                            {
                                break label245;
                            }
                        }
                        else if ( !var77.equals( var36 ) )
                        {
                            break label245;
                        }

                        if ( var34 instanceof ListReferenceType )
                        {
                            ListReferenceType var38 = (ListReferenceType) var34;
                            RepresentationType var39 = var38.inner();
                            if ( FloatType$.MODULE$.equals( var39 ) )
                            {
                                var3 = new AnyProjection( variable );
                                return (CodeGenExpression) var3;
                            }
                        }
                    }
                }
            }

            if ( var7 )
            {
                CypherType var40 = var8.ct();
                RepresentationType var41 = var8.repr();
                Option var42 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var40 );
                if ( !var42.isEmpty() )
                {
                    label237:
                    {
                        CypherType var43 = (CypherType) var42.get();
                        var75 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                        if ( var75 == null )
                        {
                            if ( var43 != null )
                            {
                                break label237;
                            }
                        }
                        else if ( !var75.equals( var43 ) )
                        {
                            break label237;
                        }

                        if ( var41 instanceof ListReferenceType )
                        {
                            ListReferenceType var45 = (ListReferenceType) var41;
                            RepresentationType var46 = var45.inner();
                            if ( BoolType$.MODULE$.equals( var46 ) )
                            {
                                var3 = new AnyProjection( variable );
                                return (CodeGenExpression) var3;
                            }
                        }
                    }
                }
            }

            boolean var4;
            label231:
            {
                label331:
                {
                    if ( var9 instanceof CypherCodeGenType )
                    {
                        CypherCodeGenType var47 = (CypherCodeGenType) var9;
                        CypherType var48 = var47.ct();
                        Option var49 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var48 );
                        if ( !var49.isEmpty() )
                        {
                            CypherType var50 = (CypherType) var49.get();
                            var74 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
                            if ( var74 == null )
                            {
                                if ( var50 == null )
                                {
                                    break label331;
                                }
                            }
                            else if ( var74.equals( var50 ) )
                            {
                                break label331;
                            }
                        }
                    }

                    if ( var9 instanceof CypherCodeGenType )
                    {
                        CypherCodeGenType var52 = (CypherCodeGenType) var9;
                        CypherType var53 = var52.ct();
                        Option var54 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var53 );
                        if ( !var54.isEmpty() )
                        {
                            label326:
                            {
                                CypherType var55 = (CypherType) var54.get();
                                var75 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean();
                                if ( var75 == null )
                                {
                                    if ( var55 != null )
                                    {
                                        break label326;
                                    }
                                }
                                else if ( !var75.equals( var55 ) )
                                {
                                    break label326;
                                }

                                var4 = true;
                                break label231;
                            }
                        }
                    }

                    label319:
                    {
                        if ( var9 instanceof CypherCodeGenType )
                        {
                            CypherCodeGenType var57 = (CypherCodeGenType) var9;
                            CypherType var58 = var57.ct();
                            Option var59 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var58 );
                            if ( !var59.isEmpty() )
                            {
                                CypherType var60 = (CypherType) var59.get();
                                var76 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                                if ( var76 == null )
                                {
                                    if ( var60 == null )
                                    {
                                        break label319;
                                    }
                                }
                                else if ( var76.equals( var60 ) )
                                {
                                    break label319;
                                }
                            }
                        }

                        label201:
                        {
                            if ( var9 instanceof CypherCodeGenType )
                            {
                                CypherCodeGenType var62 = (CypherCodeGenType) var9;
                                CypherType var63 = var62.ct();
                                Option var64 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var63 );
                                if ( !var64.isEmpty() )
                                {
                                    CypherType var65 = (CypherType) var64.get();
                                    var77 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat();
                                    if ( var77 == null )
                                    {
                                        if ( var65 == null )
                                        {
                                            break label201;
                                        }
                                    }
                                    else if ( var77.equals( var65 ) )
                                    {
                                        break label201;
                                    }
                                }
                            }

                            var4 = false;
                            break label231;
                        }

                        var4 = true;
                        break label231;
                    }

                    var4 = true;
                    break label231;
                }

                var4 = true;
            }

            if ( var4 )
            {
                var3 = new LoadVariable( variable );
                return (CodeGenExpression) var3;
            }

            if ( var7 )
            {
                label365:
                {
                    CypherType var67 = var8.ct();
                    AnyType var78 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
                    if ( var78 == null )
                    {
                        if ( var67 != null )
                        {
                            break label365;
                        }
                    }
                    else if ( !var78.equals( var67 ) )
                    {
                        break label365;
                    }

                    var3 = new AnyProjection( variable );
                    return (CodeGenExpression) var3;
                }
            }

            label366:
            {
                if ( var7 )
                {
                    CypherType var69 = var8.ct();
                    MapType var79 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTMap();
                    if ( var79 == null )
                    {
                        if ( var69 == null )
                        {
                            break label366;
                        }
                    }
                    else if ( var79.equals( var69 ) )
                    {
                        break label366;
                    }
                }

                if ( !var7 )
                {
                    throw new CantCompileQueryException( (new StringBuilder( 51 )).append( "The compiled runtime cannot handle results of type " ).append(
                            variable.codeGenType() ).toString() );
                }

                CypherType var71 = var8.ct();
                Option var72 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var71 );
                if ( var72.isEmpty() )
                {
                    throw new CantCompileQueryException( (new StringBuilder( 51 )).append( "The compiled runtime cannot handle results of type " ).append(
                            variable.codeGenType() ).toString() );
                }

                var3 = new AnyProjection( variable );
                return (CodeGenExpression) var3;
            }

            var3 = new AnyProjection( variable );
            return (CodeGenExpression) var3;
        }

        var3 = new NodeProjection( variable );
        return (CodeGenExpression) var3;
    }

    private CodeGenExpression expressionConverter( final Expression expression, final Function1<Expression,CodeGenExpression> callback,
            final CodeGenContext context )
    {
        boolean var5 = false;
        LogicalVariable var6 = null;
        boolean var7 = false;
        Property var8 = null;
        Object var4;
        if ( expression instanceof LogicalVariable )
        {
            var5 = true;
            var6 = (LogicalVariable) expression;
            if ( context.semanticTable().isNode( var6 ) )
            {
                var4 = new NodeExpression( context.getVariable( var6.name() ) );
                return (CodeGenExpression) var4;
            }
        }

        if ( var5 && context.semanticTable().isRelationship( var6 ) )
        {
            var4 = new RelationshipExpression( context.getVariable( var6.name() ) );
        }
        else
        {
            if ( expression instanceof Property )
            {
                var7 = true;
                var8 = (Property) expression;
                Expression node = var8.map();
                PropertyKeyName propKey = var8.propertyKey();
                if ( node instanceof LogicalVariable )
                {
                    LogicalVariable var12 = (LogicalVariable) node;
                    if ( context.semanticTable().isNode( var12 ) )
                    {
                        Option token = context.semanticTable().id( propKey ).map( ( x$1 ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$expressionConverter$1( x$1 ) );
                        } );
                        var4 = new NodeProperty( token, propKey.name(), context.getVariable( var12.name() ), context.namer().newVarName() );
                        return (CodeGenExpression) var4;
                    }
                }
            }

            if ( var7 )
            {
                Expression rel = var8.map();
                PropertyKeyName propKey = var8.propertyKey();
                if ( rel instanceof LogicalVariable )
                {
                    LogicalVariable var16 = (LogicalVariable) rel;
                    if ( context.semanticTable().isRelationship( var16 ) )
                    {
                        Option token = context.semanticTable().id( propKey ).map( ( x$2 ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$expressionConverter$2( x$2 ) );
                        } );
                        var4 = new RelProperty( token, propKey.name(), context.getVariable( var16.name() ), context.namer().newVarName() );
                        return (CodeGenExpression) var4;
                    }
                }
            }

            if ( var7 )
            {
                Expression mapExpression = var8.map();
                PropertyKeyName var19 = var8.propertyKey();
                if ( var19 != null )
                {
                    String propKeyName = var19.name();
                    var4 = new MapProperty( (CodeGenExpression) callback.apply( mapExpression ), propKeyName );
                    return (CodeGenExpression) var4;
                }
            }

            if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Parameter )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Parameter var21 = (org.neo4j.cypher.internal.v4_0.expressions.Parameter) expression;
                String name = var21.name();
                CypherType cypherType = var21.parameterType();
                var4 = new Parameter( name, context.namer().newVarName(), new CypherCodeGenType( cypherType, AnyValueType$.MODULE$ ) );
            }
            else if ( expression instanceof IntegerLiteral )
            {
                IntegerLiteral var24 = (IntegerLiteral) expression;
                var4 = new Literal( var24.value() );
            }
            else if ( expression instanceof DoubleLiteral )
            {
                DoubleLiteral var25 = (DoubleLiteral) expression;
                var4 = new Literal( var25.value() );
            }
            else if ( expression instanceof StringLiteral )
            {
                StringLiteral var26 = (StringLiteral) expression;
                var4 = new Literal( var26.value() );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Literal )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Literal var27 = (org.neo4j.cypher.internal.v4_0.expressions.Literal) expression;
                var4 = new Literal( var27.value() );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.ListLiteral )
            {
                org.neo4j.cypher.internal.v4_0.expressions.ListLiteral var28 = (org.neo4j.cypher.internal.v4_0.expressions.ListLiteral) expression;
                Seq exprs = var28.expressions();
                var4 = new ListLiteral( (Seq) exprs.map( ( e ) -> {
                    return (CodeGenExpression) callback.apply( e );
                }, scala.collection.Seq..MODULE$.canBuildFrom() ));
            }
            else if ( expression instanceof Add )
            {
                Add var30 = (Add) expression;
                Expression lhs = var30.lhs();
                Expression rhs = var30.rhs();
                CodeGenExpression leftOp = (CodeGenExpression) callback.apply( lhs );
                CodeGenExpression rightOp = (CodeGenExpression) callback.apply( rhs );
                var4 = new Addition( leftOp, rightOp );
            }
            else if ( expression instanceof Subtract )
            {
                Subtract var35 = (Subtract) expression;
                Expression lhs = var35.lhs();
                Expression rhs = var35.rhs();
                CodeGenExpression leftOp = (CodeGenExpression) callback.apply( lhs );
                CodeGenExpression rightOp = (CodeGenExpression) callback.apply( rhs );
                var4 = new Subtraction( leftOp, rightOp );
            }
            else if ( expression instanceof Multiply )
            {
                Multiply var40 = (Multiply) expression;
                Expression lhs = var40.lhs();
                Expression rhs = var40.rhs();
                CodeGenExpression leftOp = (CodeGenExpression) callback.apply( lhs );
                CodeGenExpression rightOp = (CodeGenExpression) callback.apply( rhs );
                var4 = new Multiplication( leftOp, rightOp );
            }
            else if ( expression instanceof Divide )
            {
                Divide var45 = (Divide) expression;
                Expression lhs = var45.lhs();
                Expression rhs = var45.rhs();
                CodeGenExpression leftOp = (CodeGenExpression) callback.apply( lhs );
                CodeGenExpression rightOp = (CodeGenExpression) callback.apply( rhs );
                var4 = new Division( leftOp, rightOp );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Modulo )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Modulo var50 = (org.neo4j.cypher.internal.v4_0.expressions.Modulo) expression;
                Expression lhs = var50.lhs();
                Expression rhs = var50.rhs();
                CodeGenExpression leftOp = (CodeGenExpression) callback.apply( lhs );
                CodeGenExpression rightOp = (CodeGenExpression) callback.apply( rhs );
                var4 = new Modulo( leftOp, rightOp );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Pow )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Pow var55 = (org.neo4j.cypher.internal.v4_0.expressions.Pow) expression;
                Expression lhs = var55.lhs();
                Expression rhs = var55.rhs();
                CodeGenExpression leftOp = (CodeGenExpression) callback.apply( lhs );
                CodeGenExpression rightOp = (CodeGenExpression) callback.apply( rhs );
                var4 = new Pow( leftOp, rightOp );
            }
            else if ( expression instanceof MapExpression )
            {
                MapExpression var60 = (MapExpression) expression;
                Seq items = var60.items();
                Map map = ((TraversableOnce) items.map( ( x0$1 ) -> {
                    if ( x0$1 != null )
                    {
                        PropertyKeyName key = (PropertyKeyName) x0$1._1();
                        Expression expr = (Expression) x0$1._2();
                        Tuple2 var2 = new Tuple2( key.name(), callback.apply( expr ) );
                        return var2;
                    }
                    else
                    {
                        throw new MatchError( x0$1 );
                    }
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
                var4 = new MyMap( map );
            }
            else
            {
                if ( expression instanceof HasLabels )
                {
                    HasLabels var63 = (HasLabels) expression;
                    Expression x = var63.expression();
                    Seq var65 = var63.labels();
                    if ( x instanceof LogicalVariable )
                    {
                        LogicalVariable var66 = (LogicalVariable) x;
                        if ( var65 instanceof colon )
                        {
                            colon var67 = (colon) var65;
                            LabelName label = (LabelName) var67.head();
                            List var69 = var67.tl$access$1();
                            if (.MODULE$.equals( var69 )){
                            String labelIdVariable = context.namer().newVarName();
                            Variable nodeVariable = context.getVariable( var66.name() );
                            var4 = new HasLabel( nodeVariable, labelIdVariable, label.name() );
                            return (CodeGenExpression) var4;
                        }
                        }
                    }
                }

                if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Equals )
                {
                    org.neo4j.cypher.internal.v4_0.expressions.Equals var72 = (org.neo4j.cypher.internal.v4_0.expressions.Equals) expression;
                    Expression lhs = var72.lhs();
                    Expression rhs = var72.rhs();
                    var4 = new Equals( (CodeGenExpression) callback.apply( lhs ), (CodeGenExpression) callback.apply( rhs ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Or )
                {
                    org.neo4j.cypher.internal.v4_0.expressions.Or var75 = (org.neo4j.cypher.internal.v4_0.expressions.Or) expression;
                    Expression lhs = var75.lhs();
                    Expression rhs = var75.rhs();
                    var4 = new Or( (CodeGenExpression) callback.apply( lhs ), (CodeGenExpression) callback.apply( rhs ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.v4_0.expressions.Not )
                {
                    org.neo4j.cypher.internal.v4_0.expressions.Not var78 = (org.neo4j.cypher.internal.v4_0.expressions.Not) expression;
                    Expression inner = var78.rhs();
                    var4 = new Not( (CodeGenExpression) callback.apply( inner ) );
                }
                else if ( expression instanceof FunctionInvocation )
                {
                    FunctionInvocation var80 = (FunctionInvocation) expression;
                    var4 = functionConverter$.MODULE$.apply( var80, callback, context );
                }
                else
                {
                    if ( !var5 )
                    {
                        throw new CantCompileQueryException(
                                (new StringBuilder( 32 )).append( "Expression of " ).append( expression ).append( " not yet supported" ).toString() );
                    }

                    var4 = new LoadVariable( context.getVariable( var6.name() ) );
                }
            }
        }

        return (CodeGenExpression) var4;
    }
}
