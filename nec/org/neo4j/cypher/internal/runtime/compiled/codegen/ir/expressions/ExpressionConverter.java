package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class ExpressionConverter
{
    public static CodeGenExpression createMaterializeExpressionForVariable( final String variableQueryVariable, final CodeGenContext context )
    {
        return ExpressionConverter$.MODULE$.createMaterializeExpressionForVariable( var0, var1 );
    }

    public static CodeGenExpression createExpression( final Expression expression, final CodeGenContext context )
    {
        return ExpressionConverter$.MODULE$.createExpression( var0, var1 );
    }

    public static CodeGenExpression createPredicate( final Expression expression, final CodeGenContext context )
    {
        return ExpressionConverter$.MODULE$.createPredicate( var0, var1 );
    }

    public static ExpressionConverter.ExpressionToPredicate ExpressionToPredicate( final CodeGenExpression expression )
    {
        return ExpressionConverter$.MODULE$.ExpressionToPredicate( var0 );
    }

    public static class ExpressionToPredicate
    {
        public final CodeGenExpression org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression;

        public ExpressionToPredicate( final CodeGenExpression expression )
        {
            this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression = expression;
        }

        public CodeGenExpression asPredicate()
        {
            return new CodeGenExpression( this )
            {
                public
                {
                    if ( $outer == null )
                    {
                        throw null;
                    }
                    else
                    {
                        this.$outer = $outer;
                        CodeGenExpression.$init$( this );
                    }
                }

                public boolean needsJavaNullCheck( final CodeGenContext context )
                {
                    return CodeGenExpression.needsJavaNullCheck$( this, context );
                }

                public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
                {
                    return !this.$outer.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression.nullable(
                            context ) &&
                                   this.$outer.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression.codeGenType(
                                           context ).isPrimitive()
                           ? this.$outer.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression.generateExpression(
                            structure, context ) : structure.coerceToBoolean(
                            this.$outer.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression.generateExpression(
                                    structure, context ) );
                }

                public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
                {
                    this.$outer.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression.init(
                            generator, context );
                }

                public boolean nullable( final CodeGenContext context )
                {
                    return false;
                }

                public CypherCodeGenType codeGenType( final CodeGenContext context )
                {
                    return this.nullable( context ) ? new CypherCodeGenType(.MODULE$.CTBoolean(),ReferenceType$.MODULE$) :
                    this.$outer.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$expressions$ExpressionConverter$ExpressionToPredicate$$expression.codeGenType(
                            context );
                }
            };
        }
    }
}
