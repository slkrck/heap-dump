package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ToSet implements CodeGenExpression, Product, Serializable
{
    private final CodeGenExpression expression;

    public ToSet( final CodeGenExpression expression )
    {
        this.expression = expression;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<CodeGenExpression> unapply( final ToSet x$0 )
    {
        return ToSet$.MODULE$.unapply( var0 );
    }

    public static ToSet apply( final CodeGenExpression expression )
    {
        return ToSet$.MODULE$.apply( var0 );
    }

    public static <A> Function1<CodeGenExpression,A> andThen( final Function1<ToSet,A> g )
    {
        return ToSet$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ToSet> compose( final Function1<A,CodeGenExpression> g )
    {
        return ToSet$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression expression()
    {
        return this.expression;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.expression().init( generator, context );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.toSet( this.expression().generateExpression( structure, context ) );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return false;
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return CodeGenType$.MODULE$.Any();
    }

    public ToSet copy( final CodeGenExpression expression )
    {
        return new ToSet( expression );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.expression();
    }

    public String productPrefix()
    {
        return "ToSet";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.expression();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ToSet;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof ToSet )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        ToSet var4 = (ToSet) x$1;
                        CodeGenExpression var10000 = this.expression();
                        CodeGenExpression var5 = var4.expression();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
