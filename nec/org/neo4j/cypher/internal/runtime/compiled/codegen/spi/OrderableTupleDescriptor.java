package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class OrderableTupleDescriptor implements TupleDescriptor, Product, Serializable
{
    private final Map<String,CodeGenType> structure;
    private final Iterable<SortItem> sortItems;

    public OrderableTupleDescriptor( final Map<String,CodeGenType> structure, final Iterable<SortItem> sortItems )
    {
        this.structure = structure;
        this.sortItems = sortItems;
        Product.$init$( this );
    }

    public static Option<Tuple2<Map<String,CodeGenType>,Iterable<SortItem>>> unapply( final OrderableTupleDescriptor x$0 )
    {
        return OrderableTupleDescriptor$.MODULE$.unapply( var0 );
    }

    public static OrderableTupleDescriptor apply( final Map<String,CodeGenType> structure, final Iterable<SortItem> sortItems )
    {
        return OrderableTupleDescriptor$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<Map<String,CodeGenType>,Iterable<SortItem>>,OrderableTupleDescriptor> tupled()
    {
        return OrderableTupleDescriptor$.MODULE$.tupled();
    }

    public static Function1<Map<String,CodeGenType>,Function1<Iterable<SortItem>,OrderableTupleDescriptor>> curried()
    {
        return OrderableTupleDescriptor$.MODULE$.curried();
    }

    public Map<String,CodeGenType> structure()
    {
        return this.structure;
    }

    public Iterable<SortItem> sortItems()
    {
        return this.sortItems;
    }

    public OrderableTupleDescriptor copy( final Map<String,CodeGenType> structure, final Iterable<SortItem> sortItems )
    {
        return new OrderableTupleDescriptor( structure, sortItems );
    }

    public Map<String,CodeGenType> copy$default$1()
    {
        return this.structure();
    }

    public Iterable<SortItem> copy$default$2()
    {
        return this.sortItems();
    }

    public String productPrefix()
    {
        return "OrderableTupleDescriptor";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.structure();
            break;
        case 1:
            var10000 = this.sortItems();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof OrderableTupleDescriptor;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof OrderableTupleDescriptor )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            OrderableTupleDescriptor var4 = (OrderableTupleDescriptor) x$1;
                            Map var10000 = this.structure();
                            Map var5 = var4.structure();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Iterable var7 = this.sortItems();
                            Iterable var6 = var4.sortItems();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
