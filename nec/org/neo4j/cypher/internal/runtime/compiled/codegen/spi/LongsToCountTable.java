package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LongsToCountTable
{
    public static String toString()
    {
        return LongsToCountTable$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return LongsToCountTable$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return LongsToCountTable$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return LongsToCountTable$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return LongsToCountTable$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return LongsToCountTable$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return LongsToCountTable$.MODULE$.productPrefix();
    }
}
