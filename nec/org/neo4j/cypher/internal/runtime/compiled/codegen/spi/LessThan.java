package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LessThan
{
    public static String toString()
    {
        return LessThan$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return LessThan$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return LessThan$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return LessThan$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return LessThan$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return LessThan$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return LessThan$.MODULE$.productPrefix();
    }
}
