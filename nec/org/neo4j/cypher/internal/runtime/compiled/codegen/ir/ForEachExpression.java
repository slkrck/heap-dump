package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ForEachExpression implements Instruction, Product, Serializable
{
    private final Variable varName;
    private final CodeGenExpression expression;
    private final Instruction body;

    public ForEachExpression( final Variable varName, final CodeGenExpression expression, final Instruction body )
    {
        this.varName = varName;
        this.expression = expression;
        this.body = body;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<Variable,CodeGenExpression,Instruction>> unapply( final ForEachExpression x$0 )
    {
        return ForEachExpression$.MODULE$.unapply( var0 );
    }

    public static ForEachExpression apply( final Variable varName, final CodeGenExpression expression, final Instruction body )
    {
        return ForEachExpression$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Variable,CodeGenExpression,Instruction>,ForEachExpression> tupled()
    {
        return ForEachExpression$.MODULE$.tupled();
    }

    public static Function1<Variable,Function1<CodeGenExpression,Function1<Instruction,ForEachExpression>>> curried()
    {
        return ForEachExpression$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public Set<String> operatorId()
    {
        return Instruction.operatorId$( this );
    }

    public Variable varName()
    {
        return this.varName;
    }

    public CodeGenExpression expression()
    {
        return this.expression;
    }

    public Instruction body()
    {
        return this.body;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.expression().init( generator, context );
        this.body().init( generator, context );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.forEach( this.varName().name(), this.varName().codeGenType(), this.expression().generateExpression( generator, context ), ( forBody ) -> {
            $anonfun$body$1( this, context, forBody );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.body()}) ));
    }

    public ForEachExpression copy( final Variable varName, final CodeGenExpression expression, final Instruction body )
    {
        return new ForEachExpression( varName, expression, body );
    }

    public Variable copy$default$1()
    {
        return this.varName();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.expression();
    }

    public Instruction copy$default$3()
    {
        return this.body();
    }

    public String productPrefix()
    {
        return "ForEachExpression";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.varName();
            break;
        case 1:
            var10000 = this.expression();
            break;
        case 2:
            var10000 = this.body();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ForEachExpression;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof ForEachExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            ForEachExpression var4 = (ForEachExpression) x$1;
                            Variable var10000 = this.varName();
                            Variable var5 = var4.varName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            CodeGenExpression var8 = this.expression();
                            CodeGenExpression var6 = var4.expression();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            Instruction var9 = this.body();
                            Instruction var7 = var4.body();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
