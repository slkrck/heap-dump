package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Id.;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.Function1;

public final class functionConverter$
{
    public static functionConverter$ MODULE$;

    static
    {
        new functionConverter$();
    }

    private functionConverter$()
    {
        MODULE$ = this;
    }

    public CodeGenExpression apply( final FunctionInvocation fcn, final Function1<Expression,CodeGenExpression> callback, final CodeGenContext context )
    {
        Function var5 = fcn.function();
        CodeGenExpression var4;
        if (.MODULE$.equals( var5 )){
        scala.Predef..MODULE$. assert (fcn.args().size() == 1);
        var4 = IdCodeGenFunction$.MODULE$.apply( (CodeGenExpression) callback.apply( fcn.args().apply( 0 ) ) );
    } else{
        if ( !org.neo4j.cypher.internal.v4_0.expressions.functions.Type..MODULE$.equals( var5 )){
            throw new CantCompileQueryException( (new StringBuilder( 27 )).append( "Function " ).append( var5 ).append( " not yet supported" ).toString() );
        }

        scala.Predef..MODULE$. assert (fcn.args().size() == 1);
        var4 = TypeCodeGenFunction$.MODULE$.apply( (CodeGenExpression) callback.apply( fcn.args().apply( 0 ) ) );
    }

        return var4;
    }
}
