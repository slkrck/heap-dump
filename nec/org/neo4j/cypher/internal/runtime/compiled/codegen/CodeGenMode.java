package org.neo4j.cypher.internal.runtime.compiled.codegen;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CodeGenMode
{
    static ByteCodeMode$ default()

    {
        return CodeGenMode$.MODULE$.
        default
            ();
    }
}
