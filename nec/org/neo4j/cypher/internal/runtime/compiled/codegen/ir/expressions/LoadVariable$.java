package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class LoadVariable$ extends AbstractFunction1<Variable,LoadVariable> implements Serializable
{
    public static LoadVariable$ MODULE$;

    static
    {
        new LoadVariable$();
    }

    private LoadVariable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "LoadVariable";
    }

    public LoadVariable apply( final Variable variable )
    {
        return new LoadVariable( variable );
    }

    public Option<Variable> unapply( final LoadVariable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.variable() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
