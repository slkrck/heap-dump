package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class LessThan$ implements Comparator, Product, Serializable
{
    public static LessThan$ MODULE$;

    static
    {
        new LessThan$();
    }

    private LessThan$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "LessThan";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LessThan$;
    }

    public int hashCode()
    {
        return -2140646662;
    }

    public String toString()
    {
        return "LessThan";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
