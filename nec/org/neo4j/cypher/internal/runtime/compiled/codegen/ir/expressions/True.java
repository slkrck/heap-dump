package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class True
{
    public static boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return True$.MODULE$.needsJavaNullCheck( var0 );
    }

    public static <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        True$.MODULE$.init( var0, var1 );
    }

    public static CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return True$.MODULE$.codeGenType( var0 );
    }

    public static boolean nullable( final CodeGenContext context )
    {
        return True$.MODULE$.nullable( var0 );
    }

    public static String toString()
    {
        return True$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return True$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return True$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return True$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return True$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return True$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return True$.MODULE$.productPrefix();
    }

    public static <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return True$.MODULE$.generateExpression( var0, var1 );
    }
}
