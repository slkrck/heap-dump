package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class Division implements CodeGenExpression, BinaryOperator, Product, Serializable
{
    private final CodeGenExpression lhs;
    private final CodeGenExpression rhs;

    public Division( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        this.lhs = lhs;
        this.rhs = rhs;
        CodeGenExpression.$init$( this );
        BinaryOperator.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Division x$0 )
    {
        return Division$.MODULE$.unapply( var0 );
    }

    public static Division apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return Division$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CodeGenExpression,CodeGenExpression>,Division> tupled()
    {
        return Division$.MODULE$.tupled();
    }

    public static Function1<CodeGenExpression,Function1<CodeGenExpression,Division>> curried()
    {
        return Division$.MODULE$.curried();
    }

    public final <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        BinaryOperator.init$( this, generator, context );
    }

    public final <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return BinaryOperator.generateExpression$( this, structure, context );
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return BinaryOperator.codeGenType$( this, context );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression lhs()
    {
        return this.lhs;
    }

    public CodeGenExpression rhs()
    {
        return this.rhs;
    }

    public <E> Function2<E,E,E> generator( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return ( lhs, rhs ) -> {
            return structure.divideExpression( lhs, rhs );
        };
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.lhs().nullable( context ) || this.rhs().nullable( context );
    }

    public String name()
    {
        return "divide";
    }

    public Division copy( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Division( lhs, rhs );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.lhs();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.rhs();
    }

    public String productPrefix()
    {
        return "Division";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        CodeGenExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Division;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof Division )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            Division var4 = (Division) x$1;
                            CodeGenExpression var10000 = this.lhs();
                            CodeGenExpression var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.rhs();
                            CodeGenExpression var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
