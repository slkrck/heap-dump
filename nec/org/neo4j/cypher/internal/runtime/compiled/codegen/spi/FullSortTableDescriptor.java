package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class FullSortTableDescriptor implements SortTableDescriptor, Product, Serializable
{
    private final OrderableTupleDescriptor tupleDescriptor;

    public FullSortTableDescriptor( final OrderableTupleDescriptor tupleDescriptor )
    {
        this.tupleDescriptor = tupleDescriptor;
        Product.$init$( this );
    }

    public static Option<OrderableTupleDescriptor> unapply( final FullSortTableDescriptor x$0 )
    {
        return FullSortTableDescriptor$.MODULE$.unapply( var0 );
    }

    public static FullSortTableDescriptor apply( final OrderableTupleDescriptor tupleDescriptor )
    {
        return FullSortTableDescriptor$.MODULE$.apply( var0 );
    }

    public static <A> Function1<OrderableTupleDescriptor,A> andThen( final Function1<FullSortTableDescriptor,A> g )
    {
        return FullSortTableDescriptor$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,FullSortTableDescriptor> compose( final Function1<A,OrderableTupleDescriptor> g )
    {
        return FullSortTableDescriptor$.MODULE$.compose( var0 );
    }

    public OrderableTupleDescriptor tupleDescriptor()
    {
        return this.tupleDescriptor;
    }

    public FullSortTableDescriptor copy( final OrderableTupleDescriptor tupleDescriptor )
    {
        return new FullSortTableDescriptor( tupleDescriptor );
    }

    public OrderableTupleDescriptor copy$default$1()
    {
        return this.tupleDescriptor();
    }

    public String productPrefix()
    {
        return "FullSortTableDescriptor";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.tupleDescriptor();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FullSortTableDescriptor;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof FullSortTableDescriptor )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        FullSortTableDescriptor var4 = (FullSortTableDescriptor) x$1;
                        OrderableTupleDescriptor var10000 = this.tupleDescriptor();
                        OrderableTupleDescriptor var5 = var4.tupleDescriptor();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
