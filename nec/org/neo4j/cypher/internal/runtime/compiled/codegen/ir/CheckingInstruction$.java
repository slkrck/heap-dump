package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class CheckingInstruction$ extends AbstractFunction2<Instruction,String,CheckingInstruction> implements Serializable
{
    public static CheckingInstruction$ MODULE$;

    static
    {
        new CheckingInstruction$();
    }

    private CheckingInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CheckingInstruction";
    }

    public CheckingInstruction apply( final Instruction inner, final String yieldedFlagVar )
    {
        return new CheckingInstruction( inner, yieldedFlagVar );
    }

    public Option<Tuple2<Instruction,String>> unapply( final CheckingInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.inner(), x$0.yieldedFlagVar() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
