package org.neo4j.cypher.internal.runtime.compiled.codegen;

import java.util.concurrent.atomic.AtomicInteger;

public final class Namer$
{
    public static Namer$ MODULE$;

    static
    {
        new Namer$();
    }

    private final AtomicInteger classNameCounter;

    private Namer$()
    {
        MODULE$ = this;
        this.classNameCounter = new AtomicInteger();
    }

    public String $lessinit$greater$default$2()
    {
        return "v";
    }

    public String $lessinit$greater$default$3()
    {
        return "m";
    }

    public String $lessinit$greater$default$4()
    {
        return "OP";
    }

    private AtomicInteger classNameCounter()
    {
        return this.classNameCounter;
    }

    public Namer apply()
    {
        return new Namer( this.classNameCounter(), this.$lessinit$greater$default$2(), this.$lessinit$greater$default$3(), this.$lessinit$greater$default$4() );
    }

    public String newClassName()
    {
        return (new StringBuilder( 22 )).append( "GeneratedExecutionPlan" ).append( System.nanoTime() ).toString();
    }
}
