package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LessThan$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LessThanEqual$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortItem;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TopTableDescriptor;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class BuildTopTable extends BuildSortTableBase implements Product, Serializable
{
    private final String opName;
    private final String tableName;
    private final CodeGenExpression countExpression;
    private final Map<String,Variable> columnVariables;
    private final Iterable<SortItem> sortItems;

    public BuildTopTable( final String opName, final String tableName, final CodeGenExpression countExpression, final Map<String,Variable> columnVariables,
            final Iterable<SortItem> sortItems, final CodeGenContext context )
    {
        super( opName, tableName, columnVariables, sortItems, context );
        this.opName = opName;
        this.tableName = tableName;
        this.countExpression = countExpression;
        this.columnVariables = columnVariables;
        this.sortItems = sortItems;
        Product.$init$( this );
    }

    public static Option<Tuple5<String,String,CodeGenExpression,Map<String,Variable>,Iterable<SortItem>>> unapply( final BuildTopTable x$0 )
    {
        return BuildTopTable$.MODULE$.unapply( var0 );
    }

    public static BuildTopTable apply( final String opName, final String tableName, final CodeGenExpression countExpression,
            final Map<String,Variable> columnVariables, final Iterable<SortItem> sortItems, final CodeGenContext context )
    {
        return BuildTopTable$.MODULE$.apply( var0, var1, var2, var3, var4, var5 );
    }

    public String opName()
    {
        return this.opName;
    }

    public String tableName()
    {
        return this.tableName;
    }

    public CodeGenExpression countExpression()
    {
        return this.countExpression;
    }

    public Map<String,Variable> columnVariables()
    {
        return this.columnVariables;
    }

    public Iterable<SortItem> sortItems()
    {
        return this.sortItems;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.countExpression().init( generator, context );
        String variableName = context.namer().newVarName();
        generator.declareCounter( variableName,
                generator.box( this.countExpression().generateExpression( generator, context ), this.countExpression().codeGenType( context ) ),
                "LIMIT: Invalid input. Got a floating-point number. Must be a non-negative integer." );
        generator.ifStatement( generator.checkInteger( variableName, LessThan$.MODULE$, 0L ), ( onTrue ) -> {
            $anonfun$init$1( onTrue );
            return BoxedUnit.UNIT;
        } );
        generator.ifStatement( generator.checkInteger( variableName, LessThanEqual$.MODULE$, 0L ), ( onTrue ) -> {
            $anonfun$init$2( onTrue );
            return BoxedUnit.UNIT;
        } );
        generator.allocateSortTable( this.tableName(), this.tableDescriptor(), generator.loadVariable( variableName ) );
    }

    public TopTableDescriptor tableDescriptor()
    {
        return new TopTableDescriptor( this.tupleDescriptor() );
    }

    public BuildTopTable copy( final String opName, final String tableName, final CodeGenExpression countExpression, final Map<String,Variable> columnVariables,
            final Iterable<SortItem> sortItems, final CodeGenContext context )
    {
        return new BuildTopTable( opName, tableName, countExpression, columnVariables, sortItems, context );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public String copy$default$2()
    {
        return this.tableName();
    }

    public CodeGenExpression copy$default$3()
    {
        return this.countExpression();
    }

    public Map<String,Variable> copy$default$4()
    {
        return this.columnVariables();
    }

    public Iterable<SortItem> copy$default$5()
    {
        return this.sortItems();
    }

    public String productPrefix()
    {
        return "BuildTopTable";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.tableName();
            break;
        case 2:
            var10000 = this.countExpression();
            break;
        case 3:
            var10000 = this.columnVariables();
            break;
        case 4:
            var10000 = this.sortItems();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof BuildTopTable;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var13;
        label84:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof BuildTopTable )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label84;
                }

                label71:
                {
                    label79:
                    {
                        BuildTopTable var4 = (BuildTopTable) x$1;
                        String var10000 = this.opName();
                        String var5 = var4.opName();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label79;
                        }

                        var10000 = this.tableName();
                        String var6 = var4.tableName();
                        if ( var10000 == null )
                        {
                            if ( var6 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10000.equals( var6 ) )
                        {
                            break label79;
                        }

                        CodeGenExpression var10 = this.countExpression();
                        CodeGenExpression var7 = var4.countExpression();
                        if ( var10 == null )
                        {
                            if ( var7 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10.equals( var7 ) )
                        {
                            break label79;
                        }

                        Map var11 = this.columnVariables();
                        Map var8 = var4.columnVariables();
                        if ( var11 == null )
                        {
                            if ( var8 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var11.equals( var8 ) )
                        {
                            break label79;
                        }

                        Iterable var12 = this.sortItems();
                        Iterable var9 = var4.sortItems();
                        if ( var12 == null )
                        {
                            if ( var9 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var12.equals( var9 ) )
                        {
                            break label79;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var13 = true;
                            break label71;
                        }
                    }

                    var13 = false;
                }

                if ( !var13 )
                {
                    break label84;
                }
            }

            var13 = true;
            return var13;
        }

        var13 = false;
        return var13;
    }
}
