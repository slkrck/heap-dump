package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.Iterable.;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public class DynamicCount extends BaseAggregateExpression
{
    public final Variable org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$variable;
    public final Iterable<Tuple2<String,CodeGenExpression>> org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$groupingKey;
    private final String opName;
    private final CodeGenExpression expression;
    private final boolean distinct;
    private String org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName;
    private String keyVar;

    public DynamicCount( final String opName, final Variable variable, final CodeGenExpression expression,
            final Iterable<Tuple2<String,CodeGenExpression>> groupingKey, final boolean distinct )
    {
        super( expression, distinct );
        this.opName = opName;
        this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$variable = variable;
        this.expression = expression;
        this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$groupingKey = groupingKey;
        this.distinct = distinct;
    }

    public String opName()
    {
        return this.opName;
    }

    public String org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName()
    {
        return this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName;
    }

    private void org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName_$eq( final String x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName = x$1;
    }

    private String keyVar()
    {
        return this.keyVar;
    }

    private void keyVar_$eq( final String x$1 )
    {
        this.keyVar = x$1;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.expression.init( generator, context );
        this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName_$eq( context.namer().newVarName() );
        IndexedSeq key = ((TraversableOnce) this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$groupingKey.map( ( x$1 ) -> {
            return ((CodeGenExpression) x$1._2()).codeGenType( context );
        },.MODULE$.canBuildFrom())).toIndexedSeq();
        this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$groupingKey.foreach( ( x0$1 ) -> {
            $anonfun$init$2( generator, context, x0$1 );
            return BoxedUnit.UNIT;
        } );
        generator.newAggregationMap( this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName(), key );
        if ( this.distinct )
        {
            generator.newMapOfSets( this.seenSet(), key, this.expression.codeGenType( context ) );
        }
    }

    public <E> void update( final MethodStructure<E> structure, final CodeGenContext context )
    {
        this.keyVar_$eq( context.namer().newVarName() );
        String valueVar = context.namer().newVarName();
        this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$groupingKey.foreach( ( x0$2 ) -> {
            $anonfun$update$1( structure, context, x0$2 );
            return BoxedUnit.UNIT;
        } );
        structure.aggregationMapGet( this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName(), valueVar,
                this.createKey( structure, context ), this.keyVar() );
        this.ifNotNull( structure, ( inner ) -> {
            $anonfun$update$2( valueVar, inner );
            return BoxedUnit.UNIT;
        }, context );
        structure.aggregationMapPut( this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName(),
                this.createKey( structure, context ), this.keyVar(), structure.loadVariable( valueVar ) );
    }

    public <E> void distinctCondition( final E value, final CodeGenType valueType, final MethodStructure<E> structure,
            final Function1<MethodStructure<E>,BoxedUnit> block, final CodeGenContext context )
    {
        structure.checkDistinct( this.seenSet(), this.createKey( structure, context ), this.keyVar(), value, valueType, ( inner ) -> {
            $anonfun$distinctCondition$1( block, inner );
            return BoxedUnit.UNIT;
        } );
    }

    private String seenSet()
    {
        return (new StringBuilder( 4 )).append( this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$mapName() ).append(
                "Seen" ).toString();
    }

    private <E> Map<String,Tuple2<CypherCodeGenType,E>> createKey( final MethodStructure<E> body, final CodeGenContext context )
    {
        return ((TraversableOnce) this.org$neo4j$cypher$internal$runtime$compiled$codegen$ir$aggregation$DynamicCount$$groupingKey.map( ( e ) -> {
            return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( e._1() ), scala.Predef.ArrowAssoc..
            MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( ((CodeGenExpression) e._2()).codeGenType( context ) ), body.loadVariable(
                    (String) e._1() )));
        },.MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
    }

    public Instruction continuation( final Instruction instruction )
    {
        return new Instruction( this, instruction )
        {
            private final Instruction instruction$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.instruction$1 = instruction$1;
                    Instruction.$init$( this );
                }
            }

            public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
            {
                Instruction.init$( this, generator, context );
            }

            public final Set<String> allOperatorIds()
            {
                return Instruction.allOperatorIds$( this );
            }

            public Seq<Instruction> children()
            {
                return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.instruction$1}) ));
            }

            public Set<String> operatorId()
            {
                return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.$outer.opName()}) ));
            }

            public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
            {
                generator.trace( this.$outer.opName(), generator.trace$default$2(), ( body ) -> {
                    $anonfun$body$1( this, context, body );
                    return BoxedUnit.UNIT;
                } );
            }
        };
    }
}
