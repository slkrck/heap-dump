package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.collection.IndexedSeq;
import scala.collection.immutable.Map;

public final class BuildProbeTable$
{
    public static BuildProbeTable$ MODULE$;

    static
    {
        new BuildProbeTable$();
    }

    private BuildProbeTable$()
    {
        MODULE$ = this;
    }

    public BuildProbeTable apply( final String id, final String name, final IndexedSeq<Variable> nodes, final Map<String,Variable> valueSymbols,
            final CodeGenContext context )
    {
        return (BuildProbeTable) (valueSymbols.isEmpty() ? new BuildCountingProbeTable( id, name, nodes )
                                                         : new BuildRecordingProbeTable( id, name, nodes, valueSymbols, context ));
    }
}
