package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class RelationshipExpression$ extends AbstractFunction1<Variable,RelationshipExpression> implements Serializable
{
    public static RelationshipExpression$ MODULE$;

    static
    {
        new RelationshipExpression$();
    }

    private RelationshipExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipExpression";
    }

    public RelationshipExpression apply( final Variable relId )
    {
        return new RelationshipExpression( relId );
    }

    public Option<Variable> unapply( final RelationshipExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.relId() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
