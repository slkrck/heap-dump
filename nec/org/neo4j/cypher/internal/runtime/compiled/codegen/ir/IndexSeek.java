package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.Predef.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class IndexSeek implements LoopDataGenerator, Product, Serializable
{
    private final String opName;
    private final String labelName;
    private final Seq<String> propNames;
    private final String descriptorVar;
    private final CodeGenExpression expression;

    public IndexSeek( final String opName, final String labelName, final Seq<String> propNames, final String descriptorVar, final CodeGenExpression expression )
    {
        this.opName = opName;
        this.labelName = labelName;
        this.propNames = propNames;
        this.descriptorVar = descriptorVar;
        this.expression = expression;
        Product.$init$( this );
    }

    public static Option<Tuple5<String,String,Seq<String>,String,CodeGenExpression>> unapply( final IndexSeek x$0 )
    {
        return IndexSeek$.MODULE$.unapply( var0 );
    }

    public static IndexSeek apply( final String opName, final String labelName, final Seq<String> propNames, final String descriptorVar,
            final CodeGenExpression expression )
    {
        return IndexSeek$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public static Function1<Tuple5<String,String,Seq<String>,String,CodeGenExpression>,IndexSeek> tupled()
    {
        return IndexSeek$.MODULE$.tupled();
    }

    public static Function1<String,Function1<String,Function1<Seq<String>,Function1<String,Function1<CodeGenExpression,IndexSeek>>>>> curried()
    {
        return IndexSeek$.MODULE$.curried();
    }

    public String opName()
    {
        return this.opName;
    }

    public String labelName()
    {
        return this.labelName;
    }

    public Seq<String> propNames()
    {
        return this.propNames;
    }

    public String descriptorVar()
    {
        return this.descriptorVar;
    }

    public CodeGenExpression expression()
    {
        return this.expression;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
      .MODULE$. assert (this.propNames().length() == 1);
        this.expression().init( generator, context );
        String labelVar = context.namer().newVarName();
        String propKeyVar = context.namer().newVarName();
        generator.lookupLabelId( labelVar, this.labelName() );
        generator.lookupPropertyKey( (String) this.propNames().head(), propKeyVar );
        generator.newIndexReference( this.descriptorVar(), labelVar, propKeyVar );
    }

    public <E> void produceLoopData( final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.indexSeek( cursorName, this.descriptorVar(), this.expression().generateExpression( generator, context ),
                this.expression().codeGenType( context ) );
        generator.incrementDbHits();
    }

    public <E> void getNext( final Variable nextVar, final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.incrementDbHits();
        generator.nodeFromNodeValueIndexCursor( nextVar.name(), cursorName );
    }

    public <E> E checkNext( final MethodStructure<E> generator, final String cursorName )
    {
        return generator.advanceNodeValueIndexCursor( cursorName );
    }

    public <E> void close( final String cursorName, final MethodStructure<E> generator )
    {
        generator.closeNodeValueIndexCursor( cursorName );
    }

    public IndexSeek copy( final String opName, final String labelName, final Seq<String> propNames, final String descriptorVar,
            final CodeGenExpression expression )
    {
        return new IndexSeek( opName, labelName, propNames, descriptorVar, expression );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public String copy$default$2()
    {
        return this.labelName();
    }

    public Seq<String> copy$default$3()
    {
        return this.propNames();
    }

    public String copy$default$4()
    {
        return this.descriptorVar();
    }

    public CodeGenExpression copy$default$5()
    {
        return this.expression();
    }

    public String productPrefix()
    {
        return "IndexSeek";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.labelName();
            break;
        case 2:
            var10000 = this.propNames();
            break;
        case 3:
            var10000 = this.descriptorVar();
            break;
        case 4:
            var10000 = this.expression();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof IndexSeek;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        label84:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof IndexSeek )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label84;
                }

                label71:
                {
                    label79:
                    {
                        IndexSeek var4 = (IndexSeek) x$1;
                        String var10000 = this.opName();
                        String var5 = var4.opName();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label79;
                        }

                        var10000 = this.labelName();
                        String var6 = var4.labelName();
                        if ( var10000 == null )
                        {
                            if ( var6 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10000.equals( var6 ) )
                        {
                            break label79;
                        }

                        Seq var10 = this.propNames();
                        Seq var7 = var4.propNames();
                        if ( var10 == null )
                        {
                            if ( var7 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10.equals( var7 ) )
                        {
                            break label79;
                        }

                        var10000 = this.descriptorVar();
                        String var8 = var4.descriptorVar();
                        if ( var10000 == null )
                        {
                            if ( var8 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10000.equals( var8 ) )
                        {
                            break label79;
                        }

                        CodeGenExpression var11 = this.expression();
                        CodeGenExpression var9 = var4.expression();
                        if ( var11 == null )
                        {
                            if ( var9 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var11.equals( var9 ) )
                        {
                            break label79;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var12 = true;
                            break label71;
                        }
                    }

                    var12 = false;
                }

                if ( !var12 )
                {
                    break label84;
                }
            }

            var12 = true;
            return var12;
        }

        var12 = false;
        return var12;
    }
}
