package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.runtime.AbstractFunction3;

public final class GetMatchesFromProbeTable$ extends AbstractFunction3<IndexedSeq<Variable>,JoinData,Instruction,GetMatchesFromProbeTable>
        implements Serializable
{
    public static GetMatchesFromProbeTable$ MODULE$;

    static
    {
        new GetMatchesFromProbeTable$();
    }

    private GetMatchesFromProbeTable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "GetMatchesFromProbeTable";
    }

    public GetMatchesFromProbeTable apply( final IndexedSeq<Variable> keys, final JoinData code, final Instruction action )
    {
        return new GetMatchesFromProbeTable( keys, code, action );
    }

    public Option<Tuple3<IndexedSeq<Variable>,JoinData,Instruction>> unapply( final GetMatchesFromProbeTable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.keys(), x$0.code(), x$0.action() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
