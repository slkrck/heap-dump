package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction4;

public final class SortTableInfo$
        extends AbstractFunction4<String,Map<String,FieldAndVariableInfo>,Map<String,FieldAndVariableInfo>,SortTableDescriptor,SortTableInfo>
        implements Serializable
{
    public static SortTableInfo$ MODULE$;

    static
    {
        new SortTableInfo$();
    }

    private SortTableInfo$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SortTableInfo";
    }

    public SortTableInfo apply( final String tableName, final Map<String,FieldAndVariableInfo> fieldToVariableInfo,
            final Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo, final SortTableDescriptor tableDescriptor )
    {
        return new SortTableInfo( tableName, fieldToVariableInfo, outgoingVariableNameToVariableInfo, tableDescriptor );
    }

    public Option<Tuple4<String,Map<String,FieldAndVariableInfo>,Map<String,FieldAndVariableInfo>,SortTableDescriptor>> unapply( final SortTableInfo x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple4( x$0.tableName(), x$0.fieldToVariableInfo(), x$0.outgoingVariableNameToVariableInfo(), x$0.tableDescriptor() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
