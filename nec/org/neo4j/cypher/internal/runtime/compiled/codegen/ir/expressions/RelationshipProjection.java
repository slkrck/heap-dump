package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class RelationshipProjection implements CodeGenExpression, Product, Serializable
{
    private final Variable relId;

    public RelationshipProjection( final Variable relId )
    {
        scala.Predef.var10000;
        boolean var3;
        label17:
        {
            label16:
            {
                this.relId = relId;
                super();
                CodeGenExpression.$init$( this );
                Product.$init$( this );
                var10000 = scala.Predef..MODULE$;
                CypherType var10001 = ((CypherCodeGenType) relId.codeGenType()).ct();
                RelationshipType var2 = .MODULE$.CTRelationship();
                if ( var10001 == null )
                {
                    if ( var2 == null )
                    {
                        break label16;
                    }
                }
                else if ( var10001.equals( var2 ) )
                {
                    break label16;
                }

                var3 = false;
                break label17;
            }

            var3 = true;
        }

        var10000. assert (var3);
    }

    public static Option<Variable> unapply( final RelationshipProjection x$0 )
    {
        return RelationshipProjection$.MODULE$.unapply( var0 );
    }

    public static RelationshipProjection apply( final Variable relId )
    {
        return RelationshipProjection$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Variable,A> andThen( final Function1<RelationshipProjection,A> g )
    {
        return RelationshipProjection$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,RelationshipProjection> compose( final Function1<A,Variable> g )
    {
        return RelationshipProjection$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable relId()
    {
        return this.relId;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return this.relId().nullable() ? structure.nullableReference( this.relId().name(), CodeGenType$.MODULE$.primitiveRel(),
                structure.materializeRelationship( this.relId().name(), this.relId().codeGenType() ) )
                                       : structure.materializeRelationship( this.relId().name(), this.relId().codeGenType() );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.relId().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return new CypherCodeGenType(.MODULE$.CTRelationship(),ReferenceType$.MODULE$);
    }

    public RelationshipProjection copy( final Variable relId )
    {
        return new RelationshipProjection( relId );
    }

    public Variable copy$default$1()
    {
        return this.relId();
    }

    public String productPrefix()
    {
        return "RelationshipProjection";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.relId();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipProjection;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof RelationshipProjection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        RelationshipProjection var4 = (RelationshipProjection) x$1;
                        Variable var10000 = this.relId();
                        Variable var5 = var4.relId();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
