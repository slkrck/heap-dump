package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class SeekNodeById$ extends AbstractFunction4<String,Variable,CodeGenExpression,Instruction,SeekNodeById> implements Serializable
{
    public static SeekNodeById$ MODULE$;

    static
    {
        new SeekNodeById$();
    }

    private SeekNodeById$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SeekNodeById";
    }

    public SeekNodeById apply( final String opName, final Variable nodeVar, final CodeGenExpression expression, final Instruction action )
    {
        return new SeekNodeById( opName, nodeVar, expression, action );
    }

    public Option<Tuple4<String,Variable,CodeGenExpression,Instruction>> unapply( final SeekNodeById x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.opName(), x$0.nodeVar(), x$0.expression(), x$0.action() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
