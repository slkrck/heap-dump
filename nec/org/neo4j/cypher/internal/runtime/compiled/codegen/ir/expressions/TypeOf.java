package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class TypeOf implements CodeGenExpression, Product, Serializable
{
    private final Variable relId;

    public TypeOf( final Variable relId )
    {
        this.relId = relId;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Variable> unapply( final TypeOf x$0 )
    {
        return TypeOf$.MODULE$.unapply( var0 );
    }

    public static TypeOf apply( final Variable relId )
    {
        return TypeOf$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Variable,A> andThen( final Function1<TypeOf,A> g )
    {
        return TypeOf$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,TypeOf> compose( final Function1<A,Variable> g )
    {
        return TypeOf$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable relId()
    {
        return this.relId;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        String typeName = context.namer().newVarName();
        structure.declareAndInitialize( typeName, new CypherCodeGenType( (CypherType).MODULE$.CTString(), ReferenceType$.MODULE$ ) );
        Object var10000;
        if ( this.nullable( context ) )
        {
            structure.ifNotStatement( structure.isNull( (String) this.relId().name(), CodeGenType$.MODULE$.primitiveRel() ), ( body ) -> {
                $anonfun$generateExpression$1( this, typeName, body );
                return BoxedUnit.UNIT;
            } );
            var10000 = structure.loadVariable( typeName );
        }
        else
        {
            structure.relType( this.relId().name(), typeName );
            var10000 = structure.loadVariable( typeName );
        }

        return var10000;
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.relId().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return new CypherCodeGenType( (CypherType).MODULE$.CTString(), ReferenceType$.MODULE$ );
    }

    public TypeOf copy( final Variable relId )
    {
        return new TypeOf( relId );
    }

    public Variable copy$default$1()
    {
        return this.relId();
    }

    public String productPrefix()
    {
        return "TypeOf";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.relId();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof TypeOf;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof TypeOf )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        TypeOf var4 = (TypeOf) x$1;
                        Variable var10000 = this.relId();
                        Variable var5 = var4.relId();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
