package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.OrderableTupleDescriptor;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortItem;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor;
import scala.MatchError;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.Nothing.;

@JavaDocToJava
public abstract class BuildSortTableBase implements Instruction
{
    private final String opName;
    private final String tableName;
    private final CodeGenContext context;
    private final Map<String,FieldAndVariableInfo> fieldToVariableInfo;
    private final Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo;
    private final OrderableTupleDescriptor tupleDescriptor;
    private final SortTableInfo sortTableInfo;

    public BuildSortTableBase( final String opName, final String tableName, final Map<String,Variable> columnVariables, final Iterable<SortItem> sortItems,
            final CodeGenContext context )
    {
        this.opName = opName;
        this.tableName = tableName;
        this.context = context;
        Instruction.$init$( this );
        this.fieldToVariableInfo = (Map) columnVariables.map( ( x0$2 ) -> {
            if ( x0$2 != null )
            {
                String queryVariableName = (String) x0$2._1();
                Variable incoming = (Variable) x0$2._2();
                if ( queryVariableName != null && incoming != null )
                {
                    String fieldName = incoming.name();
                    Tuple2 var2 = new Tuple2( fieldName, new FieldAndVariableInfo( fieldName, queryVariableName, incoming,
                            incoming.copy( this.context.namer().newVarName(), incoming.copy$default$2(), incoming.copy$default$3() ) ) );
                    return var2;
                }
            }

            throw new MatchError( x0$2 );
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
        this.outgoingVariableNameToVariableInfo = (Map) this.fieldToVariableInfo().map( ( x0$3 ) -> {
            if ( x0$3 != null )
            {
                FieldAndVariableInfo info = (FieldAndVariableInfo) x0$3._2();
                Tuple2 var1 = scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( info.outgoingVariable().name() ), info);
                return var1;
            }
            else
            {
                throw new MatchError( x0$3 );
            }
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
        this.tupleDescriptor = new OrderableTupleDescriptor( this.fieldToVariableInfo().mapValues( ( c ) -> {
            return c.outgoingVariable().codeGenType();
        } ), sortItems );
        this.sortTableInfo = new SortTableInfo( tableName, this.fieldToVariableInfo(), this.outgoingVariableNameToVariableInfo(), this.tableDescriptor() );
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        Instruction.init$( this, generator, context );
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext ignored )
    {
        generator.trace( this.opName, new Some( this.getClass().getSimpleName() ), ( body ) -> {
            $anonfun$body$1( this, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.opName}) ));
    }

    public abstract SortTableDescriptor tableDescriptor();

    private Map<String,FieldAndVariableInfo> fieldToVariableInfo()
    {
        return this.fieldToVariableInfo;
    }

    private Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo()
    {
        return this.outgoingVariableNameToVariableInfo;
    }

    public OrderableTupleDescriptor tupleDescriptor()
    {
        return this.tupleDescriptor;
    }

    public SortTableInfo sortTableInfo()
    {
        return this.sortTableInfo;
    }
}
