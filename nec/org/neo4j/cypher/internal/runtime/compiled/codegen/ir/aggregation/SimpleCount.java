package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.Tuple4;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq.;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class SimpleCount extends BaseAggregateExpression implements Product, Serializable
{
    private final String opName;
    private final Variable variable;
    private final CodeGenExpression expression;
    private final boolean distinct;

    public SimpleCount( final String opName, final Variable variable, final CodeGenExpression expression, final boolean distinct )
    {
        super( expression, distinct );
        this.opName = opName;
        this.variable = variable;
        this.expression = expression;
        this.distinct = distinct;
        Product.$init$( this );
    }

    public static Option<Tuple4<String,Variable,CodeGenExpression,Object>> unapply( final SimpleCount x$0 )
    {
        return SimpleCount$.MODULE$.unapply( var0 );
    }

    public static SimpleCount apply( final String opName, final Variable variable, final CodeGenExpression expression, final boolean distinct )
    {
        return SimpleCount$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<String,Variable,CodeGenExpression,Object>,SimpleCount> tupled()
    {
        return SimpleCount$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Variable,Function1<CodeGenExpression,Function1<Object,SimpleCount>>>> curried()
    {
        return SimpleCount$.MODULE$.curried();
    }

    public String opName()
    {
        return this.opName;
    }

    public Variable variable()
    {
        return this.variable;
    }

    public CodeGenExpression expression()
    {
        return this.expression;
    }

    public boolean distinct()
    {
        return this.distinct;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.expression().init( generator, context );
        generator.assign( this.variable().name(), CodeGenType$.MODULE$.primitiveInt(), generator.constantExpression( BoxesRunTime.boxToLong( 0L ) ) );
        if ( this.distinct() )
        {
            generator.newDistinctSet( this.setName( this.variable() ), (Iterable).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new CypherCodeGenType[]{this.expression().codeGenType( context )}) ) ));
        }
    }

    public <E> void update( final MethodStructure<E> structure, final CodeGenContext context )
    {
        this.ifNotNull( structure, ( inner ) -> {
            $anonfun$update$1( this, inner );
            return BoxedUnit.UNIT;
        }, context );
    }

    public <E> void distinctCondition( final E value, final CodeGenType valueType, final MethodStructure<E> structure,
            final Function1<MethodStructure<E>,BoxedUnit> block, final CodeGenContext context )
    {
        structure.distinctSetIfNotContains( this.setName( this.variable() ), (Map) scala.Predef..MODULE$.Map().apply( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new Tuple2[]{scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                        this.typeName( this.variable() ) ), scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                this.expression().codeGenType( context ) ), this.expression().generateExpression( structure, context )))}))),block);
    }

    private String setName( final Variable variable )
    {
        return (new StringBuilder( 3 )).append( variable.name() ).append( "Set" ).toString();
    }

    private String typeName( final Variable variable )
    {
        return (new StringBuilder( 4 )).append( variable.name() ).append( "Type" ).toString();
    }

    public SimpleCount copy( final String opName, final Variable variable, final CodeGenExpression expression, final boolean distinct )
    {
        return new SimpleCount( opName, variable, expression, distinct );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public Variable copy$default$2()
    {
        return this.variable();
    }

    public CodeGenExpression copy$default$3()
    {
        return this.expression();
    }

    public boolean copy$default$4()
    {
        return this.distinct();
    }

    public String productPrefix()
    {
        return "SimpleCount";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.variable();
            break;
        case 2:
            var10000 = this.expression();
            break;
        case 3:
            var10000 = BoxesRunTime.boxToBoolean( this.distinct() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SimpleCount;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.opName() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.variable() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.expression() ) );
        var1 = Statics.mix( var1, this.distinct() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 4 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label74:
            {
                boolean var2;
                if ( x$1 instanceof SimpleCount )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label56:
                    {
                        label65:
                        {
                            SimpleCount var4 = (SimpleCount) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label65;
                            }

                            Variable var8 = this.variable();
                            Variable var6 = var4.variable();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label65;
                            }

                            CodeGenExpression var9 = this.expression();
                            CodeGenExpression var7 = var4.expression();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label65;
                            }

                            if ( this.distinct() == var4.distinct() && var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label56;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label74;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
