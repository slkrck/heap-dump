package org.neo4j.cypher.internal.runtime.compiled.codegen;

import java.time.Clock;

import org.neo4j.cypher.internal.executionplan.GeneratedQuery;
import org.neo4j.cypher.internal.executionplan.GeneratedQueryExecution;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.plandescription.Argument;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.planner.spi.PlanningAttributes.Cardinalities;
import org.neo4j.cypher.internal.profiling.ProfilingTracer;
import org.neo4j.cypher.internal.profiling.QueryProfiler;
import org.neo4j.cypher.internal.runtime.ExecutionMode;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.compiled.CompiledExecutionResult;
import org.neo4j.cypher.internal.runtime.compiled.CompiledPlan;
import org.neo4j.cypher.internal.runtime.compiled.RunnablePlan;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructureResult;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.cypher.result.QueryProfile;
import org.neo4j.cypher.result.RuntimeResult;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.virtual.MapValue;
import scala.MatchError;
import scala.Option;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.reflect.ClassTag.;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class CodeGenerator
{
    private final CodeStructure<GeneratedQuery> structure;
    private final CodeGenConfiguration conf;

    public CodeGenerator( final CodeStructure<GeneratedQuery> structure, final Clock clock, final CodeGenConfiguration conf )
    {
        this.structure = structure;
        this.conf = conf;
    }

    public static CodeGenConfiguration $lessinit$greater$default$3()
    {
        return CodeGenerator$.MODULE$.$lessinit$greater$default$3();
    }

    public static <T> CodeStructureResult<T> generateCode( final CodeStructure<T> structure, final Seq<Instruction> instructions,
            final Map<String,Id> operatorIds, final Seq<String> columns, final CodeGenConfiguration conf, final CodeGenContext context )
    {
        return CodeGenerator$.MODULE$.generateCode( var0, var1, var2, var3, var4, var5 );
    }

    public CodeStructure<GeneratedQuery> structure()
    {
        return this.structure;
    }

    public CompiledPlan generate( final LogicalPlan plan, final TokenContext tokenContext, final SemanticTable semanticTable, final boolean readOnly,
            final Cardinalities cardinalities, final Seq<String> originalReturnColumns )
    {
        if ( plan instanceof ProduceResult )
        {
            ProduceResult var9 = (ProduceResult) plan;

            CodeStructureResult var10000;
            try
            {
                var10000 = this.generateQuery( plan, semanticTable, var9.columns(), this.conf, cardinalities );
            }
            catch ( CantCompileQueryException var14 )
            {
                throw var14;
            }
            catch ( Exception var15 )
            {
                throw new CantCompileQueryException( var15.getMessage(), var15 );
            }

            CodeStructureResult query = var10000;
            RunnablePlan builder = new RunnablePlan( (CodeGenerator) null, originalReturnColumns, query )
            {
                private final Seq originalReturnColumns$1;
                private final CodeStructureResult query$1;

                public
                {
                    this.originalReturnColumns$1 = originalReturnColumns$1;
                    this.query$1 = query$1;
                }

                public RuntimeResult apply( final QueryContext queryContext, final ExecutionMode execMode, final Option<ProfilingTracer> tracer,
                        final MapValue params, final boolean prePopulateResults, final QuerySubscriber subscriber )
                {
                    GeneratedQueryExecution execution = ((GeneratedQuery) this.query$1.query()).execute( queryContext, (QueryProfiler) tracer.getOrElse( () -> {
                        return QueryProfiler.NONE;
                    } ), params );
                    return new CompiledExecutionResult( queryContext, execution, (QueryProfile) tracer.getOrElse( () -> {
                        return QueryProfile.NONE;
                    } ), prePopulateResults, subscriber, (String[]) this.originalReturnColumns$1.toArray(.MODULE$.apply( String.class ) ));
                }

                public Seq<Argument> metadata()
                {
                    return this.query$1.code();
                }
            }; CompiledPlan var7 = new CompiledPlan( false, var9.columns(), builder );
            return var7;
        }
        else
        {
            throw new CantCompileQueryException( "Can only compile plans with ProduceResult on top" );
        }
    }

    private CodeStructureResult<GeneratedQuery> generateQuery( final LogicalPlan plan, final SemanticTable semantics, final Seq<String> columns,
            final CodeGenConfiguration conf, final Cardinalities cardinalities )
    {
        Map lookup = ((TraversableOnce) columns.indices().map( ( i ) -> {
            return $anonfun$generateQuery$1( columns, BoxesRunTime.unboxToInt( i ) );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
        CodeGenContext context = new CodeGenContext( semantics, lookup, CodeGenContext$.MODULE$.$lessinit$greater$default$3() );
        Tuple2 var10 = LogicalPlanConverter$.MODULE$.asCodeGenPlan( plan ).produce( context, cardinalities );
        if ( var10 != null )
        {
            List instructions = (List) var10._2();
            return CodeGenerator$.MODULE$.generateCode( this.structure(), instructions, context.operatorIds().toMap( scala.Predef..MODULE$.$conforms() ),
            columns, conf, context);
        }
        else
        {
            throw new MatchError( var10 );
        }
    }

    private Object javaValue( final Object value )
    {
        Object var2;
        if ( value == null )
        {
            var2 = null;
        }
        else if ( value instanceof Seq )
        {
            Seq var4 = (Seq) value;
            var2 = scala.collection.JavaConverters..MODULE$.seqAsJavaListConverter( (Seq) var4.map( ( valuex ) -> {
            return this.javaValue( valuex );
        }, scala.collection.Seq..MODULE$.canBuildFrom() )).asJava();
        }
        else if ( value instanceof scala.collection.Map )
        {
            scala.collection.Map var5 = (scala.collection.Map) value;
            var2 = scala.collection.JavaConverters..
            MODULE$.mapAsJavaMapConverter( org.neo4j.cypher.internal.v4_0.util.Eagerly..MODULE$.immutableMapValues( var5, ( valuex ) -> {
                return this.javaValue( valuex );
            } )).asJava();
        }
        else
        {
            if ( !(value instanceof Object) )
            {
                throw new MatchError( value );
            }

            var2 = value;
        }

        return var2;
    }
}
