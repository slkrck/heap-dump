package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class DecreaseAndReturnWhenZero$ extends AbstractFunction4<String,String,Instruction,CodeGenExpression,DecreaseAndReturnWhenZero>
        implements Serializable
{
    public static DecreaseAndReturnWhenZero$ MODULE$;

    static
    {
        new DecreaseAndReturnWhenZero$();
    }

    private DecreaseAndReturnWhenZero$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "DecreaseAndReturnWhenZero";
    }

    public DecreaseAndReturnWhenZero apply( final String opName, final String variableName, final Instruction action, final CodeGenExpression startValue )
    {
        return new DecreaseAndReturnWhenZero( opName, variableName, action, startValue );
    }

    public Option<Tuple4<String,String,Instruction,CodeGenExpression>> unapply( final DecreaseAndReturnWhenZero x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.opName(), x$0.variableName(), x$0.action(), x$0.startValue() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
