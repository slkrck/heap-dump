package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class LongType$ implements RepresentationType, Product, Serializable
{
    public static LongType$ MODULE$;

    static
    {
        new LongType$();
    }

    private LongType$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "LongType";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LongType$;
    }

    public int hashCode()
    {
        return -2009755658;
    }

    public String toString()
    {
        return "LongType";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
