package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class NodeProperty$ extends AbstractFunction4<Option<Object>,String,Variable,String,NodeProperty> implements Serializable
{
    public static NodeProperty$ MODULE$;

    static
    {
        new NodeProperty$();
    }

    private NodeProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodeProperty";
    }

    public NodeProperty apply( final Option<Object> token, final String propName, final Variable nodeIdVar, final String propKeyVar )
    {
        return new NodeProperty( token, propName, nodeIdVar, propKeyVar );
    }

    public Option<Tuple4<Option<Object>,String,Variable,String>> unapply( final NodeProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.token(), x$0.propName(), x$0.nodeIdVar(), x$0.propKeyVar() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
