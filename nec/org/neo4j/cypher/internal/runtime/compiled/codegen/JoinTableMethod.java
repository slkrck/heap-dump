package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class JoinTableMethod implements Product, Serializable
{
    private final String name;
    private final JoinTableType tableType;

    public JoinTableMethod( final String name, final JoinTableType tableType )
    {
        this.name = name;
        this.tableType = tableType;
        Product.$init$( this );
    }

    public static Option<Tuple2<String,JoinTableType>> unapply( final JoinTableMethod x$0 )
    {
        return JoinTableMethod$.MODULE$.unapply( var0 );
    }

    public static JoinTableMethod apply( final String name, final JoinTableType tableType )
    {
        return JoinTableMethod$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<String,JoinTableType>,JoinTableMethod> tupled()
    {
        return JoinTableMethod$.MODULE$.tupled();
    }

    public static Function1<String,Function1<JoinTableType,JoinTableMethod>> curried()
    {
        return JoinTableMethod$.MODULE$.curried();
    }

    public String name()
    {
        return this.name;
    }

    public JoinTableType tableType()
    {
        return this.tableType;
    }

    public JoinTableMethod copy( final String name, final JoinTableType tableType )
    {
        return new JoinTableMethod( name, tableType );
    }

    public String copy$default$1()
    {
        return this.name();
    }

    public JoinTableType copy$default$2()
    {
        return this.tableType();
    }

    public String productPrefix()
    {
        return "JoinTableMethod";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.name();
            break;
        case 1:
            var10000 = this.tableType();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof JoinTableMethod;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof JoinTableMethod )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            JoinTableMethod var4 = (JoinTableMethod) x$1;
                            String var10000 = this.name();
                            String var5 = var4.name();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            JoinTableType var7 = this.tableType();
                            JoinTableType var6 = var4.tableType();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
