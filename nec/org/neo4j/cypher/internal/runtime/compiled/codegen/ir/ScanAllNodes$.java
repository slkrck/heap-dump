package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class ScanAllNodes$ extends AbstractFunction1<String,ScanAllNodes> implements Serializable
{
    public static ScanAllNodes$ MODULE$;

    static
    {
        new ScanAllNodes$();
    }

    private ScanAllNodes$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ScanAllNodes";
    }

    public ScanAllNodes apply( final String opName )
    {
        return new ScanAllNodes( opName );
    }

    public Option<String> unapply( final ScanAllNodes x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.opName() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
