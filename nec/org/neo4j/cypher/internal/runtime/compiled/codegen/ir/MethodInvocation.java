package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.JoinTableMethod;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MethodInvocation implements Instruction, Product, Serializable
{
    private final Set<String> operatorId;
    private final JoinTableMethod symbol;
    private final String methodName;
    private final Seq<Instruction> statements;

    public MethodInvocation( final Set<String> operatorId, final JoinTableMethod symbol, final String methodName, final Seq<Instruction> statements )
    {
        this.operatorId = operatorId;
        this.symbol = symbol;
        this.methodName = methodName;
        this.statements = statements;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<Set<String>,JoinTableMethod,String,Seq<Instruction>>> unapply( final MethodInvocation x$0 )
    {
        return MethodInvocation$.MODULE$.unapply( var0 );
    }

    public static MethodInvocation apply( final Set<String> operatorId, final JoinTableMethod symbol, final String methodName,
            final Seq<Instruction> statements )
    {
        return MethodInvocation$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<Set<String>,JoinTableMethod,String,Seq<Instruction>>,MethodInvocation> tupled()
    {
        return MethodInvocation$.MODULE$.tupled();
    }

    public static Function1<Set<String>,Function1<JoinTableMethod,Function1<String,Function1<Seq<Instruction>,MethodInvocation>>>> curried()
    {
        return MethodInvocation$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public Set<String> operatorId()
    {
        return this.operatorId;
    }

    public JoinTableMethod symbol()
    {
        return this.symbol;
    }

    public String methodName()
    {
        return this.methodName;
    }

    public Seq<Instruction> statements()
    {
        return this.statements;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.invokeMethod( this.symbol().tableType(), this.symbol().name(), this.methodName(), ( body ) -> {
            $anonfun$body$1( this, context, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return this.statements();
    }

    public MethodInvocation copy( final Set<String> operatorId, final JoinTableMethod symbol, final String methodName, final Seq<Instruction> statements )
    {
        return new MethodInvocation( operatorId, symbol, methodName, statements );
    }

    public Set<String> copy$default$1()
    {
        return this.operatorId();
    }

    public JoinTableMethod copy$default$2()
    {
        return this.symbol();
    }

    public String copy$default$3()
    {
        return this.methodName();
    }

    public Seq<Instruction> copy$default$4()
    {
        return this.statements();
    }

    public String productPrefix()
    {
        return "MethodInvocation";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.operatorId();
            break;
        case 1:
            var10000 = this.symbol();
            break;
        case 2:
            var10000 = this.methodName();
            break;
        case 3:
            var10000 = this.statements();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MethodInvocation;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof MethodInvocation )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            MethodInvocation var4 = (MethodInvocation) x$1;
                            Set var10000 = this.operatorId();
                            Set var5 = var4.operatorId();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            JoinTableMethod var9 = this.symbol();
                            JoinTableMethod var6 = var4.symbol();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            String var10 = this.methodName();
                            String var7 = var4.methodName();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            Seq var11 = this.statements();
                            Seq var8 = var4.statements();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
