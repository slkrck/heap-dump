package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction3;

public final class Projection$ extends AbstractFunction3<String,Map<Variable,CodeGenExpression>,Instruction,Projection> implements Serializable
{
    public static Projection$ MODULE$;

    static
    {
        new Projection$();
    }

    private Projection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Projection";
    }

    public Projection apply( final String projectionOpName, final Map<Variable,CodeGenExpression> variables, final Instruction action )
    {
        return new Projection( projectionOpName, variables, action );
    }

    public Option<Tuple3<String,Map<Variable,CodeGenExpression>,Instruction>> unapply( final Projection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.projectionOpName(), x$0.variables(), x$0.action() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
