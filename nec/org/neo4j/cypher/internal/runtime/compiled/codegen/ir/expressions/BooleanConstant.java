package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface BooleanConstant extends CodeGenExpression
{
    static void $init$( final BooleanConstant $this )
    {
    }

    default boolean nullable( final CodeGenContext context )
    {
        return false;
    }

    default CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return CodeGenType$.MODULE$.primitiveBool();
    }

    default <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }
}
