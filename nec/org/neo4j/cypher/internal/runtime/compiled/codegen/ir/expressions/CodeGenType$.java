package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.v4_0.util.symbols.package.;

public final class CodeGenType$
{
    public static CodeGenType$ MODULE$;

    static
    {
        new CodeGenType$();
    }

    private final CypherCodeGenType Any;
    private final CypherCodeGenType AnyValue;
    private final CypherCodeGenType Value;
    private final CypherCodeGenType primitiveNode;
    private final CypherCodeGenType primitiveRel;
    private final CypherCodeGenType primitiveInt;
    private final CypherCodeGenType primitiveFloat;
    private final CypherCodeGenType primitiveBool;
    private final JavaCodeGenType javaInt;
    private final JavaCodeGenType javaLong;

    private CodeGenType$()
    {
        MODULE$ = this;
        this.Any = new CypherCodeGenType(.MODULE$.CTAny(), ReferenceType$.MODULE$);
        this.AnyValue = new CypherCodeGenType(.MODULE$.CTAny(), AnyValueType$.MODULE$);
        this.Value = new CypherCodeGenType(.MODULE$.CTAny(), ValueType$.MODULE$);
        this.primitiveNode = new CypherCodeGenType(.MODULE$.CTNode(), LongType$.MODULE$);
        this.primitiveRel = new CypherCodeGenType(.MODULE$.CTRelationship(), LongType$.MODULE$);
        this.primitiveInt = new CypherCodeGenType(.MODULE$.CTInteger(), LongType$.MODULE$);
        this.primitiveFloat = new CypherCodeGenType(.MODULE$.CTFloat(), FloatType$.MODULE$);
        this.primitiveBool = new CypherCodeGenType(.MODULE$.CTBoolean(), BoolType$.MODULE$);
        this.javaInt = new JavaCodeGenType( IntType$.MODULE$ );
        this.javaLong = new JavaCodeGenType( LongType$.MODULE$ );
    }

    public CypherCodeGenType Any()
    {
        return this.Any;
    }

    public CypherCodeGenType AnyValue()
    {
        return this.AnyValue;
    }

    public CypherCodeGenType Value()
    {
        return this.Value;
    }

    public CypherCodeGenType primitiveNode()
    {
        return this.primitiveNode;
    }

    public CypherCodeGenType primitiveRel()
    {
        return this.primitiveRel;
    }

    public CypherCodeGenType primitiveInt()
    {
        return this.primitiveInt;
    }

    public CypherCodeGenType primitiveFloat()
    {
        return this.primitiveFloat;
    }

    public CypherCodeGenType primitiveBool()
    {
        return this.primitiveBool;
    }

    public JavaCodeGenType javaInt()
    {
        return this.javaInt;
    }

    public JavaCodeGenType javaLong()
    {
        return this.javaLong;
    }
}
