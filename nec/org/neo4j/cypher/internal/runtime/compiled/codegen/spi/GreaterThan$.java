package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class GreaterThan$ implements Comparator, Product, Serializable
{
    public static GreaterThan$ MODULE$;

    static
    {
        new GreaterThan$();
    }

    private GreaterThan$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "GreaterThan";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof GreaterThan$;
    }

    public int hashCode()
    {
        return -1701951333;
    }

    public String toString()
    {
        return "GreaterThan";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
