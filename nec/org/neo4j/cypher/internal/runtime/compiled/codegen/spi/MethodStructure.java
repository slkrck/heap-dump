package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.codegen.Expression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Function1;
import scala.Option;
import scala.Tuple2;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public interface MethodStructure<E>
{
    static void $init$( final MethodStructure $this )
    {
    }

    void localVariable( final String variable, final E e, final CodeGenType codeGenType );

    void declareFlag( final String name, final boolean initialValue );

    void updateFlag( final String name, final boolean newValue );

    void declarePredicate( final String name );

    void assign( final String varName, final CodeGenType codeGenType, final E value );

    default void assign( final Variable v, final E value )
    {
        this.assign( v.name(), v.codeGenType(), value );
    }

    void declareAndInitialize( final String varName, final CodeGenType codeGenType );

    void declare( final String varName, final CodeGenType codeGenType );

    void declareProperty( final String name );

    void declareCounter( final String name, final E initialValue, final String errorOnFloatingPoint );

    void putField( final TupleDescriptor tupleDescriptor, final E value, final String fieldName, final String localVar );

    void updateProbeTable( final TupleDescriptor tupleDescriptor, final String tableVar, final RecordingJoinTableType tableType, final Seq<String> keyVars,
            final E element );

    void probe( final String tableVar, final JoinTableType tableType, final Seq<String> keyVars, final Function1<MethodStructure<E>,BoxedUnit> block );

    void updateProbeTableCount( final String tableVar, final CountingJoinTableType tableType, final Seq<String> keyVar );

    void allocateProbeTable( final String tableVar, final JoinTableType tableType );

    void invokeMethod( final JoinTableType resultType, final String resultVar, final String methodName, final Function1<MethodStructure<E>,BoxedUnit> block );

    E coerceToBoolean( final E propertyExpression );

    void incrementInteger( final String name );

    void decrementInteger( final String name );

    void incrementInteger( final String name, final E value );

    E checkInteger( final String variableName, final Comparator comparator, final long value );

    E newTableValue( final String targetVar, final TupleDescriptor tupleDescriptor );

    E noValue();

    E constantExpression( final Object value );

    E constantValueExpression( final Object value, final CodeGenType codeGenType );

    default E constantPrimitiveExpression( final Object value )
    {
        return this.constantExpression( value );
    }

    E asMap( final Map<String,E> map );

    E asList( final Seq<E> values );

    E asAnyValueList( final Seq<E> values );

    E asPrimitiveStream( final E values, final CodeGenType codeGenType );

    E asPrimitiveStream( final Seq<E> values, final CodeGenType codeGenType );

    void declarePrimitiveIterator( final String name, final CodeGenType iterableCodeGenType );

    E primitiveIteratorFrom( final E iterable, final CodeGenType iterableCodeGenType );

    E primitiveIteratorNext( final E iterator, final CodeGenType iterableCodeGenType );

    E primitiveIteratorHasNext( final E iterator, final CodeGenType iterableCodeGenType );

    void declareIterator( final String name );

    void declareIterator( final String name, final CodeGenType codeGenType );

    E iteratorFrom( final E iterable );

    E iteratorNext( final E iterator, final CodeGenType codeGenType );

    E iteratorHasNext( final E iterator );

    E toSet( final E value );

    void newDistinctSet( final String name, final Iterable<CodeGenType> codeGenTypes );

    void distinctSetIfNotContains( final String name, final Map<String,Tuple2<CodeGenType,E>> structure, final Function1<MethodStructure<E>,BoxedUnit> block );

    void distinctSetIterate( final String name, final HashableTupleDescriptor key, final Function1<MethodStructure<E>,BoxedUnit> block );

    void newUniqueAggregationKey( final String varName, final Map<String,Tuple2<CodeGenType,E>> structure );

    void newAggregationMap( final String name, final IndexedSeq<CodeGenType> keyTypes );

    void aggregationMapGet( final String name, final String varName, final Map<String,Tuple2<CodeGenType,E>> key, final String keyVar );

    void aggregationMapPut( final String name, final Map<String,Tuple2<CodeGenType,E>> key, final String keyVar, final E value );

    void aggregationMapIterate( final String name, final HashableTupleDescriptor key, final String valueVar,
            final Function1<MethodStructure<E>,BoxedUnit> block );

    void newMapOfSets( final String name, final IndexedSeq<CodeGenType> keyTypes, final CodeGenType elementType );

    void checkDistinct( final String name, final Map<String,Tuple2<CodeGenType,E>> key, final String keyVar, final E value, final CodeGenType valueType,
            final Function1<MethodStructure<E>,BoxedUnit> block );

    void allocateSortTable( final String name, final SortTableDescriptor tableDescriptor, final E count );

    void sortTableAdd( final String name, final SortTableDescriptor tableDescriptor, final E value );

    void sortTableSort( final String name, final SortTableDescriptor tableDescriptor );

    void sortTableIterate( final String name, final SortTableDescriptor tableDescriptor, final Map<String,String> varNameToField,
            final Function1<MethodStructure<E>,BoxedUnit> block );

    E loadVariable( final String varName );

    E multiplyPrimitive( final E lhs, final E rhs );

    E addExpression( final E lhs, final E rhs );

    E subtractExpression( final E lhs, final E rhs );

    E multiplyExpression( final E lhs, final E rhs );

    E divideExpression( final E lhs, final E rhs );

    E modulusExpression( final E lhs, final E rhs );

    E powExpression( final E lhs, final E rhs );

    E threeValuedNotExpression( final E value );

    E notExpression( final E value );

    E threeValuedEqualsExpression( final E lhs, final E rhs );

    E threeValuedPrimitiveEqualsExpression( final E lhs, final E rhs, final CodeGenType codeGenType );

    E equalityExpression( final E lhs, final E rhs, final CodeGenType codeGenType );

    E primitiveEquals( final E lhs, final E rhs );

    E orExpression( final E lhs, final E rhs );

    E threeValuedOrExpression( final E lhs, final E rhs );

    void markAsNull( final String varName, final CodeGenType codeGenType );

    E nullablePrimitive( final String varName, final CodeGenType codeGenType, final E onSuccess );

    E nullableReference( final String varName, final CodeGenType codeGenType, final E onSuccess );

    E isNull( final E expr, final CodeGenType codeGenType );

    E isNull( final String name, final CodeGenType codeGenType );

    E notNull( final E expr, final CodeGenType codeGenType );

    E notNull( final String name, final CodeGenType codeGenType );

    E ifNullThenNoValue( final E expr );

    E box( final E expression, final CodeGenType codeGenType );

    E unbox( final E expression, final CodeGenType codeGenType );

    E toFloat( final E expression );

    void expectParameter( final String key, final String variableName, final CodeGenType codeGenType );

    E mapGetExpression( final E map, final String key );

    <V> V trace( final String planStepId, final Option<String> maybeSuffix, final Function1<MethodStructure<E>,V> block );

    default <V> Option<String> trace$default$2()
    {
        return .MODULE$;
    }

    void incrementDbHits();

    void incrementRows();

    void labelScan( final String iterVar, final String labelIdVar );

    E hasLabel( final String nodeVar, final String labelVar, final String predVar );

    void allNodesScan( final String iterVar );

    void lookupLabelId( final String labelIdVar, final String labelName );

    E lookupLabelIdE( final String labelName );

    void lookupRelationshipTypeId( final String typeIdVar, final String typeName );

    E lookupRelationshipTypeIdE( final String typeName );

    void nodeGetRelationshipsWithDirection( final String iterVar, final String nodeVar, final CodeGenType nodeVarType, final SemanticDirection direction );

    void nodeGetRelationshipsWithDirectionAndTypes( final String iterVar, final String nodeVar, final CodeGenType nodeVarType,
            final SemanticDirection direction, final Seq<String> typeVars );

    void connectingRelationships( final String iterVar, final String fromNode, final CodeGenType fromNodeType, final SemanticDirection dir, final String toNode,
            final CodeGenType toNodeType );

    void connectingRelationships( final String iterVar, final String fromNode, final CodeGenType fromNodeType, final SemanticDirection dir,
            final Seq<String> types, final String toNode, final CodeGenType toNodeType );

    void nodeFromNodeValueIndexCursor( final String targetVar, final String iterVar );

    void nodeFromNodeCursor( final String targetVar, final String iterVar );

    void nodeFromNodeLabelIndexCursor( final String targetVar, final String iterVar );

    void nextRelationshipAndNode( final String toNodeVar, final String iterVar, final SemanticDirection direction, final String fromNodeVar,
            final String relVar );

    void nextRelationship( final String iterVar, final SemanticDirection direction, final String relVar );

    E advanceNodeCursor( final String cursorName );

    void closeNodeCursor( final String cursorName );

    E advanceNodeLabelIndexCursor( final String cursorName );

    void closeNodeLabelIndexCursor( final String cursorName );

    E advanceRelationshipSelectionCursor( final String cursorName );

    void closeRelationshipSelectionCursor( final String cursorName );

    E advanceNodeValueIndexCursor( final String cursorName );

    void closeNodeValueIndexCursor( final String cursorName );

    void nodeGetPropertyById( final String nodeVar, final CodeGenType nodeVarType, final int propId, final String propValueVar );

    void nodeGetPropertyForVar( final String nodeVar, final CodeGenType nodeVarType, final String propIdVar, final String propValueVar );

    void nodeIdSeek( final String nodeIdVar, final E expression, final CodeGenType codeGenType, final Function1<MethodStructure<E>,BoxedUnit> block );

    void relationshipGetPropertyById( final String relIdVar, final CodeGenType relVarType, final int propId, final String propValueVar );

    void relationshipGetPropertyForVar( final String relIdVar, final CodeGenType relVarType, final String propIdVar, final String propValueVar );

    void lookupPropertyKey( final String propName, final String propVar );

    void indexSeek( final String iterVar, final String descriptorVar, final E value, final CodeGenType codeGenType );

    void relType( final String relIdVar, final String typeVar );

    void newIndexReference( final String descriptorVar, final String labelVar, final String propKeyVar );

    E nodeCountFromCountStore( final E expression );

    E relCountFromCountStore( final E start, final E end, final Seq<E> types );

    E token( final int t );

    E wildCardToken();

    void whileLoop( final E test, final Function1<MethodStructure<E>,BoxedUnit> block );

    void forEach( final String varName, final CodeGenType codeGenType, final E iterable, final Function1<MethodStructure<E>,BoxedUnit> block );

    void ifStatement( final E test, final Function1<MethodStructure<E>,BoxedUnit> block );

    void ifNotStatement( final E test, final Function1<MethodStructure<E>,BoxedUnit> block );

    void ifNonNullStatement( final E test, final CodeGenType codeGenType, final Function1<MethodStructure<E>,BoxedUnit> block );

    E ternaryOperator( final E test, final E onTrue, final E onFalse );

    void returnSuccessfully();

    void throwException( final Expression exception );

    E materializeNode( final String nodeIdVar, final CodeGenType codeGenType );

    E node( final String nodeIdVar, final CodeGenType codeGenType );

    E materializeRelationship( final String relIdVar, final CodeGenType codeGenType );

    E relationship( final String relIdVar, final CodeGenType codeGenType );

    E materializeAny( final E expression, final CodeGenType codeGenType );

    void visitorAccept();

    void setInRow( final int column, final E value );

    E toAnyValue( final E e, final CodeGenType t );

    E toMaterializedAnyValue( final E e, final CodeGenType t );
}
