package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class Variable implements Product, Serializable
{
    private final String name;
    private final CodeGenType codeGenType;
    private final boolean nullable;

    public Variable( final String name, final CodeGenType codeGenType, final boolean nullable )
    {
        this.name = name;
        this.codeGenType = codeGenType;
        this.nullable = nullable;
        Product.$init$( this );
    }

    public static boolean apply$default$3()
    {
        return Variable$.MODULE$.apply$default$3();
    }

    public static boolean $lessinit$greater$default$3()
    {
        return Variable$.MODULE$.$lessinit$greater$default$3();
    }

    public static Option<Tuple3<String,CodeGenType,Object>> unapply( final Variable x$0 )
    {
        return Variable$.MODULE$.unapply( var0 );
    }

    public static Variable apply( final String name, final CodeGenType codeGenType, final boolean nullable )
    {
        return Variable$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,CodeGenType,Object>,Variable> tupled()
    {
        return Variable$.MODULE$.tupled();
    }

    public static Function1<String,Function1<CodeGenType,Function1<Object,Variable>>> curried()
    {
        return Variable$.MODULE$.curried();
    }

    public String name()
    {
        return this.name;
    }

    public CodeGenType codeGenType()
    {
        return this.codeGenType;
    }

    public boolean nullable()
    {
        return this.nullable;
    }

    public Variable copy( final String name, final CodeGenType codeGenType, final boolean nullable )
    {
        return new Variable( name, codeGenType, nullable );
    }

    public String copy$default$1()
    {
        return this.name();
    }

    public CodeGenType copy$default$2()
    {
        return this.codeGenType();
    }

    public boolean copy$default$3()
    {
        return this.nullable();
    }

    public String productPrefix()
    {
        return "Variable";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.name();
            break;
        case 1:
            var10000 = this.codeGenType();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToBoolean( this.nullable() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Variable;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.name() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.codeGenType() ) );
        var1 = Statics.mix( var1, this.nullable() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof Variable )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        label56:
                        {
                            Variable var4 = (Variable) x$1;
                            String var10000 = this.name();
                            String var5 = var4.name();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label56;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label56;
                            }

                            CodeGenType var7 = this.codeGenType();
                            CodeGenType var6 = var4.codeGenType();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label56;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label56;
                            }

                            if ( this.nullable() == var4.nullable() && var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label47;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label65;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
