package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class BoolType
{
    public static String toString()
    {
        return BoolType$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return BoolType$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return BoolType$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return BoolType$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return BoolType$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return BoolType$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return BoolType$.MODULE$.productPrefix();
    }
}
