package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.AnyType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.IntegerType;
import org.neo4j.cypher.internal.v4_0.util.symbols.StringType;
import org.neo4j.cypher.internal.v4_0.util.symbols.ListType.;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Addition implements CodeGenExpression, BinaryOperator, Product, Serializable
{
    private final CodeGenExpression lhs;
    private final CodeGenExpression rhs;

    public Addition( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        this.lhs = lhs;
        this.rhs = rhs;
        CodeGenExpression.$init$( this );
        BinaryOperator.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Addition x$0 )
    {
        return Addition$.MODULE$.unapply( var0 );
    }

    public static Addition apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return Addition$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CodeGenExpression,CodeGenExpression>,Addition> tupled()
    {
        return Addition$.MODULE$.tupled();
    }

    public static Function1<CodeGenExpression,Function1<CodeGenExpression,Addition>> curried()
    {
        return Addition$.MODULE$.curried();
    }

    public final <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        BinaryOperator.init$( this, generator, context );
    }

    public final <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return BinaryOperator.generateExpression$( this, structure, context );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression lhs()
    {
        return this.lhs;
    }

    public CodeGenExpression rhs()
    {
        return this.rhs;
    }

    public <E> Function2<E,E,E> generator( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return ( lhs, rhs ) -> {
            return structure.addExpression( lhs, rhs );
        };
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.lhs().nullable( context ) || this.rhs().nullable( context );
    }

    public String name()
    {
        return "add";
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        Tuple2 var3 = new Tuple2( this.lhs().codeGenType( context ).ct(), this.rhs().codeGenType( context ).ct() );
        CypherCodeGenType var2;
        if ( var3 != null )
        {
            CypherType var4 = (CypherType) var3._1();
            CypherType var5 = (CypherType) var3._2();
            Option var6 = .MODULE$.unapply( var4 );
            if ( !var6.isEmpty() )
            {
                CypherType left = (CypherType) var6.get();
                Option var8 = .MODULE$.unapply( var5 );
                if ( !var8.isEmpty() )
                {
                    CypherType right = (CypherType) var8.get();
                    var2 = new CypherCodeGenType(.MODULE$.apply( left.leastUpperBound( right ) ), ReferenceType$.MODULE$);
                    return var2;
                }
            }
        }

        if ( var3 != null )
        {
            CypherType var10 = (CypherType) var3._1();
            CypherType singleElement = (CypherType) var3._2();
            Option var12 = .MODULE$.unapply( var10 );
            if ( !var12.isEmpty() )
            {
                CypherType innerType = (CypherType) var12.get();
                var2 = new CypherCodeGenType(.MODULE$.apply( innerType.leastUpperBound( singleElement ) ), ReferenceType$.MODULE$);
                return var2;
            }
        }

        if ( var3 != null )
        {
            CypherType singleElement = (CypherType) var3._1();
            CypherType var15 = (CypherType) var3._2();
            Option var16 = .MODULE$.unapply( var15 );
            if ( !var16.isEmpty() )
            {
                CypherType innerType = (CypherType) var16.get();
                var2 = new CypherCodeGenType(.MODULE$.apply( innerType.leastUpperBound( singleElement ) ), ReferenceType$.MODULE$);
                return var2;
            }
        }

        label183:
        {
            AnyType var10000;
            if ( var3 != null )
            {
                CypherType var18 = (CypherType) var3._1();
                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
                if ( var10000 == null )
                {
                    if ( var18 == null )
                    {
                        break label183;
                    }
                }
                else if ( var10000.equals( var18 ) )
                {
                    break label183;
                }
            }

            if ( var3 != null )
            {
                label184:
                {
                    CypherType var20 = (CypherType) var3._2();
                    var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny();
                    if ( var10000 == null )
                    {
                        if ( var20 != null )
                        {
                            break label184;
                        }
                    }
                    else if ( !var10000.equals( var20 ) )
                    {
                        break label184;
                    }

                    var2 = new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny(), ReferenceType$.MODULE$);
                    return var2;
                }
            }

            StringType var34;
            if ( var3 != null )
            {
                label185:
                {
                    CypherType var22 = (CypherType) var3._1();
                    var34 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
                    if ( var34 == null )
                    {
                        if ( var22 != null )
                        {
                            break label185;
                        }
                    }
                    else if ( !var34.equals( var22 ) )
                    {
                        break label185;
                    }

                    var2 = new CypherCodeGenType( (CypherType) org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString(), ReferenceType$.MODULE$);
                    return var2;
                }
            }

            if ( var3 != null )
            {
                label186:
                {
                    CypherType var24 = (CypherType) var3._2();
                    var34 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString();
                    if ( var34 == null )
                    {
                        if ( var24 != null )
                        {
                            break label186;
                        }
                    }
                    else if ( !var34.equals( var24 ) )
                    {
                        break label186;
                    }

                    var2 = new CypherCodeGenType( (CypherType) org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTString(), ReferenceType$.MODULE$);
                    return var2;
                }
            }

            label187:
            {
                if ( var3 != null )
                {
                    label152:
                    {
                        CypherType var26 = (CypherType) var3._1();
                        CypherType var27 = (CypherType) var3._2();
                        IntegerType var35 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                        if ( var35 == null )
                        {
                            if ( var26 != null )
                            {
                                break label152;
                            }
                        }
                        else if ( !var35.equals( var26 ) )
                        {
                            break label152;
                        }

                        var35 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger();
                        if ( var35 == null )
                        {
                            if ( var27 == null )
                            {
                                break label187;
                            }
                        }
                        else if ( var35.equals( var27 ) )
                        {
                            break label187;
                        }
                    }
                }

                if ( var3 != null )
                {
                    CypherType var30 = (CypherType) var3._1();
                    CypherType var31 = (CypherType) var3._2();
                    Option var32 = Number$.MODULE$.unapply( var30 );
                    if ( !var32.isEmpty() )
                    {
                        Option var33 = Number$.MODULE$.unapply( var31 );
                        if ( !var33.isEmpty() )
                        {
                            var2 = new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTFloat(), ReferenceType$.MODULE$);
                            return var2;
                        }
                    }
                }

                var2 = CodeGenType$.MODULE$.Any();
                return var2;
            }

            var2 = new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger(), ReferenceType$.MODULE$);
            return var2;
        }

        var2 = new CypherCodeGenType( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny(), ReferenceType$.MODULE$);
        return var2;
    }

    public Addition copy( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Addition( lhs, rhs );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.lhs();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.rhs();
    }

    public String productPrefix()
    {
        return "Addition";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        CodeGenExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Addition;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof Addition )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            Addition var4 = (Addition) x$1;
                            CodeGenExpression var10000 = this.lhs();
                            CodeGenExpression var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.rhs();
                            CodeGenExpression var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
