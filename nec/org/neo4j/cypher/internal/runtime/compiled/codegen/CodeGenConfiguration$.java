package org.neo4j.cypher.internal.runtime.compiled.codegen;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.neo4j.exceptions.InternalException;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.immutable.Set;
import scala.runtime.BoxesRunTime;

public final class CodeGenConfiguration$ implements Serializable
{
    public static CodeGenConfiguration$ MODULE$;

    static
    {
        new CodeGenConfiguration$();
    }

    private CodeGenConfiguration$()
    {
        MODULE$ = this;
    }

    public CodeGenMode $lessinit$greater$default$1()
    {
        return CodeGenMode$.MODULE$.
        default
        ();
    }

    public boolean $lessinit$greater$default$2()
    {
        return false;
    }

    public boolean $lessinit$greater$default$3()
    {
        return false;
    }

    public Option<Path> $lessinit$greater$default$4()
    {
        return .MODULE$;
    }

    public String $lessinit$greater$default$5()
    {
        return "org.neo4j.cypher.internal.compiler.v4_0.generated";
    }

    public CodeGenConfiguration apply( final Set<String> debugOptions )
    {
        Product mode = debugOptions.contains( "generate_java_source" ) ? SourceCodeMode$.MODULE$ : ByteCodeMode$.MODULE$;
        boolean show_java_source = debugOptions.contains( "show_java_source" );
        if ( show_java_source )
        {
            SourceCodeMode$ var4 = SourceCodeMode$.MODULE$;
            if ( mode == null )
            {
                if ( var4 != null )
                {
                    throw new InternalException( "Can only 'debug=show_java_source' if 'debug=generate_java_source'." );
                }
            }
            else if ( !mode.equals( var4 ) )
            {
                throw new InternalException( "Can only 'debug=show_java_source' if 'debug=generate_java_source'." );
            }
        }

        boolean show_bytecode = debugOptions.contains( "show_bytecode" );
        Option saveSource = scala.Option..MODULE$.apply( System.getProperty( "org.neo4j.cypher.DEBUG.generated_source_location" ) ).map( ( x$1 ) -> {
        return Paths.get( x$1 );
    } );
        return new CodeGenConfiguration( (CodeGenMode) mode, show_java_source, show_bytecode, saveSource, this.apply$default$5() );
    }

    public CodeGenMode apply$default$1()
    {
        return CodeGenMode$.MODULE$.
        default
        ();
    }

    public boolean apply$default$2()
    {
        return false;
    }

    public boolean apply$default$3()
    {
        return false;
    }

    public Option<Path> apply$default$4()
    {
        return .MODULE$;
    }

    public String apply$default$5()
    {
        return "org.neo4j.cypher.internal.compiler.v4_0.generated";
    }

    public CodeGenConfiguration apply( final CodeGenMode mode, final boolean showSource, final boolean showByteCode, final Option<Path> saveSource,
            final String packageName )
    {
        return new CodeGenConfiguration( mode, showSource, showByteCode, saveSource, packageName );
    }

    public Option<Tuple5<CodeGenMode,Object,Object,Option<Path>,String>> unapply( final CodeGenConfiguration x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple5( x$0.mode(), BoxesRunTime.boxToBoolean( x$0.showSource() ), BoxesRunTime.boxToBoolean( x$0.showByteCode() ), x$0.saveSource(),
                    x$0.packageName() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
