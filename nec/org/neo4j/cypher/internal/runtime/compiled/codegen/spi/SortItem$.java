package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class SortItem$ extends AbstractFunction2<String,SortOrder,SortItem> implements Serializable
{
    public static SortItem$ MODULE$;

    static
    {
        new SortItem$();
    }

    private SortItem$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SortItem";
    }

    public SortItem apply( final String fieldName, final SortOrder sortOrder )
    {
        return new SortItem( fieldName, sortOrder );
    }

    public Option<Tuple2<String,SortOrder>> unapply( final SortItem x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.fieldName(), x$0.sortOrder() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
