package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class Descending
{
    public static String toString()
    {
        return Descending$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return Descending$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return Descending$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return Descending$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return Descending$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return Descending$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return Descending$.MODULE$.productPrefix();
    }
}
