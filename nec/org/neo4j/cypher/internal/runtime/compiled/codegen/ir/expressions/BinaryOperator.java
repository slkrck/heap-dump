package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.BooleanType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.IntegerType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import org.neo4j.exceptions.CypherTypeException;
import scala.Function2;
import scala.Option;
import scala.Tuple2;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface BinaryOperator
{
    private static boolean isListOf$1( final CodeGenType codeGenType, final CypherType cType )
    {
        boolean var2;
        label30:
        {
            if ( codeGenType instanceof CypherCodeGenType )
            {
                CypherCodeGenType var4 = (CypherCodeGenType) codeGenType;
                CypherType var5 = var4.ct();
                Option var6 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( var5 );
                if ( !var6.isEmpty() )
                {
                    CypherType inner = (CypherType) var6.get();
                    if ( inner == null )
                    {
                        if ( cType == null )
                        {
                            break label30;
                        }
                    }
                    else if ( inner.equals( cType ) )
                    {
                        break label30;
                    }
                }
            }

            var2 = false;
            return var2;
        }

        var2 = true;
        return var2;
    }

    static void $init$( final BinaryOperator $this )
    {
    }

    CodeGenExpression lhs();

    CodeGenExpression rhs();

    String name();

    default <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.lhs().init( generator, context );
        this.rhs().init( generator, context );
    }

    default <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        Tuple2 var4 = new Tuple2( this.lhs().codeGenType( context ), this.rhs().codeGenType( context ) );
        BooleanType var10000;
        if ( var4 != null )
        {
            CypherCodeGenType var5 = (CypherCodeGenType) var4._1();
            CypherCodeGenType t = (CypherCodeGenType) var4._2();
            if ( var5 != null )
            {
                label76:
                {
                    CypherType var7 = var5.ct();
                    var10000 = .MODULE$.CTBoolean();
                    if ( var10000 == null )
                    {
                        if ( var7 != null )
                        {
                            break label76;
                        }
                    }
                    else if ( !var10000.equals( var7 ) )
                    {
                        break label76;
                    }

                    if ( !isListOf$1( t,.MODULE$.CTAny() ) &&!isListOf$1( t,.MODULE$.CTBoolean())){
                    throw new CypherTypeException( (new StringBuilder( 22 )).append( "Cannot " ).append( this.name() ).append( " a boolean and " ).append(
                            this.rhs().codeGenType( context ).ct() ).toString() );
                }
                }
            }
        }

        if ( var4 != null )
        {
            CypherCodeGenType t = (CypherCodeGenType) var4._1();
            CypherCodeGenType var10 = (CypherCodeGenType) var4._2();
            if ( var10 != null )
            {
                label68:
                {
                    CypherType var11 = var10.ct();
                    var10000 = .MODULE$.CTBoolean();
                    if ( var10000 == null )
                    {
                        if ( var11 != null )
                        {
                            break label68;
                        }
                    }
                    else if ( !var10000.equals( var11 ) )
                    {
                        break label68;
                    }

                    if ( !isListOf$1( t,.MODULE$.CTAny() ) &&!isListOf$1( t,.MODULE$.CTBoolean())){
                    throw new CypherTypeException( (new StringBuilder( 24 )).append( "Cannot " ).append( this.name() ).append( " a " ).append(
                            this.rhs().codeGenType( context ).ct() ).append( " and a boolean" ).toString() );
                }
                }
            }
        }

        Object var3;
        if ( var4 != null )
        {
            CypherCodeGenType t1 = (CypherCodeGenType) var4._1();
            CypherCodeGenType t2 = (CypherCodeGenType) var4._2();
            if ( t1.isPrimitive() && t2.isPrimitive() )
            {
                var3 = this.generator( structure, context ).apply(
                        structure.box( this.lhs().generateExpression( structure, context ), this.lhs().codeGenType( context ) ),
                        structure.box( this.rhs().generateExpression( structure, context ), this.rhs().codeGenType( context ) ) );
                return var3;
            }
        }

        if ( var4 != null )
        {
            CypherCodeGenType t = (CypherCodeGenType) var4._1();
            if ( t.isPrimitive() )
            {
                var3 = this.generator( structure, context ).apply(
                        structure.box( this.lhs().generateExpression( structure, context ), this.lhs().codeGenType( context ) ),
                        this.rhs().generateExpression( structure, context ) );
                return var3;
            }
        }

        if ( var4 != null )
        {
            CypherCodeGenType t = (CypherCodeGenType) var4._2();
            if ( t.isPrimitive() )
            {
                var3 = this.generator( structure, context ).apply( this.lhs().generateExpression( structure, context ),
                        structure.box( this.rhs().generateExpression( structure, context ), this.rhs().codeGenType( context ) ) );
                return var3;
            }
        }

        var3 = this.generator( structure, context ).apply( this.lhs().generateExpression( structure, context ),
                this.rhs().generateExpression( structure, context ) );
        return var3;
    }

    default CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        CypherCodeGenType var2;
        label56:
        {
            Tuple2 var3 = new Tuple2( this.lhs().codeGenType( context ).ct(), this.rhs().codeGenType( context ).ct() );
            if ( var3 != null )
            {
                label47:
                {
                    CypherType var4 = (CypherType) var3._1();
                    CypherType var5 = (CypherType) var3._2();
                    IntegerType var10000 = .MODULE$.CTInteger();
                    if ( var10000 == null )
                    {
                        if ( var4 != null )
                        {
                            break label47;
                        }
                    }
                    else if ( !var10000.equals( var4 ) )
                    {
                        break label47;
                    }

                    var10000 = .MODULE$.CTInteger();
                    if ( var10000 == null )
                    {
                        if ( var5 == null )
                        {
                            break label56;
                        }
                    }
                    else if ( var10000.equals( var5 ) )
                    {
                        break label56;
                    }
                }
            }

            if ( var3 != null )
            {
                CypherType var8 = (CypherType) var3._1();
                CypherType var9 = (CypherType) var3._2();
                Option var10 = Number$.MODULE$.unapply( var8 );
                if ( !var10.isEmpty() )
                {
                    Option var11 = Number$.MODULE$.unapply( var9 );
                    if ( !var11.isEmpty() )
                    {
                        var2 = new CypherCodeGenType(.MODULE$.CTFloat(), ReferenceType$.MODULE$);
                        return var2;
                    }
                }
            }

            var2 = CodeGenType$.MODULE$.Any();
            return var2;
        }

        var2 = new CypherCodeGenType(.MODULE$.CTInteger(), ReferenceType$.MODULE$);
        return var2;
    }

    <E> Function2<E,E,E> generator( final MethodStructure<E> structure, final CodeGenContext context );
}
