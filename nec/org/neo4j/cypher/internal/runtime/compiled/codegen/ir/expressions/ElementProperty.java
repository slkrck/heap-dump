package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Option;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class ElementProperty implements CodeGenExpression
{
    private final Option<Object> token;
    private final String propName;
    private final String propKeyVar;

    public ElementProperty( final Option<Object> token, final String propName, final String elementIdVar, final String propKeyVar )
    {
        this.token = token;
        this.propName = propName;
        this.propKeyVar = propKeyVar;
        CodeGenExpression.$init$( this );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        if ( this.token.isEmpty() )
        {
            generator.lookupPropertyKey( this.propName, this.propKeyVar );
        }
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        String localName = context.namer().newVarName();
        structure.declareProperty( localName );
        if ( this.token.isEmpty() )
        {
            this.propertyByName( structure, localName );
        }
        else
        {
            this.propertyById( structure, localName );
        }

        structure.incrementDbHits();
        return structure.loadVariable( localName );
    }

    public abstract <E> void propertyByName( final MethodStructure<E> body, final String localName );

    public abstract <E> void propertyById( final MethodStructure<E> body, final String localName );

    public boolean nullable( final CodeGenContext context )
    {
        return true;
    }
}
