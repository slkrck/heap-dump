package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ListReferenceType implements RepresentationType, Product, Serializable
{
    private final RepresentationType inner;

    public ListReferenceType( final RepresentationType inner )
    {
        this.inner = inner;
        Product.$init$( this );
    }

    public static Option<RepresentationType> unapply( final ListReferenceType x$0 )
    {
        return ListReferenceType$.MODULE$.unapply( var0 );
    }

    public static ListReferenceType apply( final RepresentationType inner )
    {
        return ListReferenceType$.MODULE$.apply( var0 );
    }

    public static <A> Function1<RepresentationType,A> andThen( final Function1<ListReferenceType,A> g )
    {
        return ListReferenceType$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ListReferenceType> compose( final Function1<A,RepresentationType> g )
    {
        return ListReferenceType$.MODULE$.compose( var0 );
    }

    public RepresentationType inner()
    {
        return this.inner;
    }

    public ListReferenceType copy( final RepresentationType inner )
    {
        return new ListReferenceType( inner );
    }

    public RepresentationType copy$default$1()
    {
        return this.inner();
    }

    public String productPrefix()
    {
        return "ListReferenceType";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.inner();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ListReferenceType;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof ListReferenceType )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        ListReferenceType var4 = (ListReferenceType) x$1;
                        RepresentationType var10000 = this.inner();
                        RepresentationType var5 = var4.inner();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
