package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class SelectionInstruction$ extends AbstractFunction2<String,Instruction,SelectionInstruction> implements Serializable
{
    public static SelectionInstruction$ MODULE$;

    static
    {
        new SelectionInstruction$();
    }

    private SelectionInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SelectionInstruction";
    }

    public SelectionInstruction apply( final String id, final Instruction instruction )
    {
        return new SelectionInstruction( id, instruction );
    }

    public Option<Tuple2<String,Instruction>> unapply( final SelectionInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.id(), x$0.instruction() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
