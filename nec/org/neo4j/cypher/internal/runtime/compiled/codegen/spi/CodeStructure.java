package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenConfiguration;
import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public interface CodeStructure<T>
{
    CodeStructureResult<T> generateQuery( final String className, final Seq<String> columns, final Map<String,Id> operatorIds, final CodeGenConfiguration conf,
            final Function1<MethodStructure<?>,BoxedUnit> block, final CodeGenContext codeGenContext );
}
