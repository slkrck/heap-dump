package org.neo4j.cypher.internal.runtime.compiled.codegen;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class setStaticField
{
    public static void apply( final Class<?> clazz, final String name, final Object value )
    {
        setStaticField$.MODULE$.apply( var0, var1, var2 );
    }
}
