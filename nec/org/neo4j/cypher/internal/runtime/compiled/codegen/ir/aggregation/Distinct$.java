package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.None.;
import scala.collection.Iterable;
import scala.runtime.AbstractFunction3;

public final class Distinct$ extends AbstractFunction3<String,String,Iterable<Tuple2<String,CodeGenExpression>>,Distinct> implements Serializable
{
    public static Distinct$ MODULE$;

    static
    {
        new Distinct$();
    }

    private Distinct$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Distinct";
    }

    public Distinct apply( final String opName, final String setName, final Iterable<Tuple2<String,CodeGenExpression>> vars )
    {
        return new Distinct( opName, setName, vars );
    }

    public Option<Tuple3<String,String,Iterable<Tuple2<String,CodeGenExpression>>>> unapply( final Distinct x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.opName(), x$0.setName(), x$0.vars() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
