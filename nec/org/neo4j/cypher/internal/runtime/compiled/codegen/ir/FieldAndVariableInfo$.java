package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class FieldAndVariableInfo$ extends AbstractFunction4<String,String,Variable,Variable,FieldAndVariableInfo> implements Serializable
{
    public static FieldAndVariableInfo$ MODULE$;

    static
    {
        new FieldAndVariableInfo$();
    }

    private FieldAndVariableInfo$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "FieldAndVariableInfo";
    }

    public FieldAndVariableInfo apply( final String fieldName, final String queryVariableName, final Variable incomingVariable,
            final Variable outgoingVariable )
    {
        return new FieldAndVariableInfo( fieldName, queryVariableName, incomingVariable, outgoingVariable );
    }

    public Option<Tuple4<String,String,Variable,Variable>> unapply( final FieldAndVariableInfo x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple4( x$0.fieldName(), x$0.queryVariableName(), x$0.incomingVariable(), x$0.outgoingVariable() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
