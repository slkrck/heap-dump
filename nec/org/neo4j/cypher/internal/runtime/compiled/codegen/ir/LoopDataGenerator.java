package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface LoopDataGenerator
{
    <E> E checkNext( final MethodStructure<E> generator, final String iterVar );

    <E> void init( final MethodStructure<E> generator, final CodeGenContext context );

    <E> void getNext( final Variable nextVar, final String iterVar, final MethodStructure<E> generator, final CodeGenContext context );

    <E> void produceLoopData( final String iterVarName, final MethodStructure<E> generator, final CodeGenContext context );

    <E> void close( final String iterVarName, final MethodStructure<E> generator );

    String opName();
}
