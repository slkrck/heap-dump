package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class UnwindPrimitiveCollection$ extends AbstractFunction2<String,CodeGenExpression,UnwindPrimitiveCollection> implements Serializable
{
    public static UnwindPrimitiveCollection$ MODULE$;

    static
    {
        new UnwindPrimitiveCollection$();
    }

    private UnwindPrimitiveCollection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "UnwindPrimitiveCollection";
    }

    public UnwindPrimitiveCollection apply( final String opName, final CodeGenExpression collection )
    {
        return new UnwindPrimitiveCollection( opName, collection );
    }

    public Option<Tuple2<String,CodeGenExpression>> unapply( final UnwindPrimitiveCollection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.opName(), x$0.collection() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
