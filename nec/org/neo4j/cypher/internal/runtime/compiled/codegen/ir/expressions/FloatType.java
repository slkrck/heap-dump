package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class FloatType
{
    public static String toString()
    {
        return FloatType$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return FloatType$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return FloatType$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return FloatType$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return FloatType$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return FloatType$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return FloatType$.MODULE$.productPrefix();
    }
}
