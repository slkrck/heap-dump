package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RelationshipExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RelationshipProjection;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.TypeOf;
import org.neo4j.exceptions.InternalException;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class TypeCodeGenFunction$ implements CodeGenFunction1, Product, Serializable
{
    public static TypeCodeGenFunction$ MODULE$;

    static
    {
        new TypeCodeGenFunction$();
    }

    private TypeCodeGenFunction$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public CodeGenExpression apply( final CodeGenExpression arg )
    {
        TypeOf var2;
        if ( arg instanceof RelationshipExpression )
        {
            RelationshipExpression var4 = (RelationshipExpression) arg;
            var2 = new TypeOf( var4.relId() );
        }
        else
        {
            if ( !(arg instanceof RelationshipProjection) )
            {
                throw new InternalException( (new StringBuilder( 41 )).append( "type function only accepts relationships " ).append( arg ).toString() );
            }

            RelationshipProjection var5 = (RelationshipProjection) arg;
            var2 = new TypeOf( var5.relId() );
        }

        return var2;
    }

    public String productPrefix()
    {
        return "TypeCodeGenFunction";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof TypeCodeGenFunction$;
    }

    public int hashCode()
    {
        return 227434593;
    }

    public String toString()
    {
        return "TypeCodeGenFunction";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
