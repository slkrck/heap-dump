package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class SortInstruction$ extends AbstractFunction2<String,SortTableInfo,SortInstruction> implements Serializable
{
    public static SortInstruction$ MODULE$;

    static
    {
        new SortInstruction$();
    }

    private SortInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SortInstruction";
    }

    public SortInstruction apply( final String opName, final SortTableInfo sortTableInfo )
    {
        return new SortInstruction( opName, sortTableInfo );
    }

    public Option<Tuple2<String,SortTableInfo>> unapply( final SortInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.opName(), x$0.sortTableInfo() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
