package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class HasLabel implements CodeGenExpression, Product, Serializable
{
    private final Variable nodeVariable;
    private final String labelVariable;
    private final String labelName;

    public HasLabel( final Variable nodeVariable, final String labelVariable, final String labelName )
    {
        this.nodeVariable = nodeVariable;
        this.labelVariable = labelVariable;
        this.labelName = labelName;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<Variable,String,String>> unapply( final HasLabel x$0 )
    {
        return HasLabel$.MODULE$.unapply( var0 );
    }

    public static HasLabel apply( final Variable nodeVariable, final String labelVariable, final String labelName )
    {
        return HasLabel$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Variable,String,String>,HasLabel> tupled()
    {
        return HasLabel$.MODULE$.tupled();
    }

    public static Function1<Variable,Function1<String,Function1<String,HasLabel>>> curried()
    {
        return HasLabel$.MODULE$.curried();
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable nodeVariable()
    {
        return this.nodeVariable;
    }

    public String labelVariable()
    {
        return this.labelVariable;
    }

    public String labelName()
    {
        return this.labelName;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.lookupLabelId( this.labelVariable(), this.labelName() );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        String localName = context.namer().newVarName();
        structure.declarePredicate( localName );
        structure.incrementDbHits();
        return this.nodeVariable().nullable() ? structure.nullableReference( this.nodeVariable().name(), CodeGenType$.MODULE$.primitiveNode(),
                structure.box( structure.hasLabel( this.nodeVariable().name(), this.labelVariable(), localName ), CodeGenType$.MODULE$.primitiveBool() ) )
                                              : structure.hasLabel( this.nodeVariable().name(), this.labelVariable(), localName );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.nodeVariable().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return this.nullable( context ) ? new CypherCodeGenType(.MODULE$.CTBoolean(),ReferenceType$.MODULE$) :CodeGenType$.MODULE$.primitiveBool();
    }

    public HasLabel copy( final Variable nodeVariable, final String labelVariable, final String labelName )
    {
        return new HasLabel( nodeVariable, labelVariable, labelName );
    }

    public Variable copy$default$1()
    {
        return this.nodeVariable();
    }

    public String copy$default$2()
    {
        return this.labelVariable();
    }

    public String copy$default$3()
    {
        return this.labelName();
    }

    public String productPrefix()
    {
        return "HasLabel";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.nodeVariable();
            break;
        case 1:
            var10000 = this.labelVariable();
            break;
        case 2:
            var10000 = this.labelName();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof HasLabel;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var9;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof HasLabel )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            HasLabel var4 = (HasLabel) x$1;
                            Variable var10000 = this.nodeVariable();
                            Variable var5 = var4.nodeVariable();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            String var8 = this.labelVariable();
                            String var6 = var4.labelVariable();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            var8 = this.labelName();
                            String var7 = var4.labelName();
                            if ( var8 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var9 = true;
                                break label54;
                            }
                        }

                        var9 = false;
                    }

                    if ( var9 )
                    {
                        break label72;
                    }
                }

                var9 = false;
                return var9;
            }
        }

        var9 = true;
        return var9;
    }
}
