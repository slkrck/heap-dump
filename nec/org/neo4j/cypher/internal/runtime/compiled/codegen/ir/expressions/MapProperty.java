package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MapProperty implements CodeGenExpression, Product, Serializable
{
    private final CodeGenExpression mapExpression;
    private final String propertyKeyName;

    public MapProperty( final CodeGenExpression mapExpression, final String propertyKeyName )
    {
        this.mapExpression = mapExpression;
        this.propertyKeyName = propertyKeyName;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<CodeGenExpression,String>> unapply( final MapProperty x$0 )
    {
        return MapProperty$.MODULE$.unapply( var0 );
    }

    public static MapProperty apply( final CodeGenExpression mapExpression, final String propertyKeyName )
    {
        return MapProperty$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CodeGenExpression,String>,MapProperty> tupled()
    {
        return MapProperty$.MODULE$.tupled();
    }

    public static Function1<CodeGenExpression,Function1<String,MapProperty>> curried()
    {
        return MapProperty$.MODULE$.curried();
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression mapExpression()
    {
        return this.mapExpression;
    }

    public String propertyKeyName()
    {
        return this.propertyKeyName;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.mapExpression().init( generator, context );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return structure.mapGetExpression( this.mapExpression().generateExpression( structure, context ), this.propertyKeyName() );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return true;
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return CodeGenType$.MODULE$.Any();
    }

    public MapProperty copy( final CodeGenExpression mapExpression, final String propertyKeyName )
    {
        return new MapProperty( mapExpression, propertyKeyName );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.mapExpression();
    }

    public String copy$default$2()
    {
        return this.propertyKeyName();
    }

    public String productPrefix()
    {
        return "MapProperty";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.mapExpression();
            break;
        case 1:
            var10000 = this.propertyKeyName();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MapProperty;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof MapProperty )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            MapProperty var4 = (MapProperty) x$1;
                            CodeGenExpression var10000 = this.mapExpression();
                            CodeGenExpression var5 = var4.mapExpression();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            String var7 = this.propertyKeyName();
                            String var6 = var4.propertyKeyName();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
