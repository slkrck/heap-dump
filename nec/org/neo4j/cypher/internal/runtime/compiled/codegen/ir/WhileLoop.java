package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.Predef.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class WhileLoop implements Instruction, Product, Serializable
{
    private final Variable variable;
    private final LoopDataGenerator producer;
    private final Instruction action;

    public WhileLoop( final Variable variable, final LoopDataGenerator producer, final Instruction action )
    {
        this.variable = variable;
        this.producer = producer;
        this.action = action;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<Variable,LoopDataGenerator,Instruction>> unapply( final WhileLoop x$0 )
    {
        return WhileLoop$.MODULE$.unapply( var0 );
    }

    public static WhileLoop apply( final Variable variable, final LoopDataGenerator producer, final Instruction action )
    {
        return WhileLoop$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Variable,LoopDataGenerator,Instruction>,WhileLoop> tupled()
    {
        return WhileLoop$.MODULE$.tupled();
    }

    public static Function1<Variable,Function1<LoopDataGenerator,Function1<Instruction,WhileLoop>>> curried()
    {
        return WhileLoop$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public Variable variable()
    {
        return this.variable;
    }

    public LoopDataGenerator producer()
    {
        return this.producer;
    }

    public Instruction action()
    {
        return this.action;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        String iterator = (new StringBuilder( 4 )).append( this.variable().name() ).append( "Iter" ).toString();
        generator.trace( this.producer().opName(), generator.trace$default$2(), ( body ) -> {
            $anonfun$body$1( this, generator, context, iterator, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Set<String> operatorId()
    {
        return (Set).MODULE$.Set().apply(.MODULE$.wrapRefArray( (Object[]) (new String[]{this.producer().opName()}) ));
    }

    public Seq<Instruction> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.action()}) ));
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        Instruction.init$( this, generator, context );
        this.producer().init( generator, context );
    }

    public WhileLoop copy( final Variable variable, final LoopDataGenerator producer, final Instruction action )
    {
        return new WhileLoop( variable, producer, action );
    }

    public Variable copy$default$1()
    {
        return this.variable();
    }

    public LoopDataGenerator copy$default$2()
    {
        return this.producer();
    }

    public Instruction copy$default$3()
    {
        return this.action();
    }

    public String productPrefix()
    {
        return "WhileLoop";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.variable();
            break;
        case 1:
            var10000 = this.producer();
            break;
        case 2:
            var10000 = this.action();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof WhileLoop;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof WhileLoop )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            WhileLoop var4 = (WhileLoop) x$1;
                            Variable var10000 = this.variable();
                            Variable var5 = var4.variable();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            LoopDataGenerator var8 = this.producer();
                            LoopDataGenerator var6 = var4.producer();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            Instruction var9 = this.action();
                            Instruction var7 = var4.action();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
