package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.planner.spi.PlanningAttributes.Cardinalities;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import scala.Option;
import scala.Tuple2;
import scala.collection.immutable.List;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface CodeGenPlan
{
    LogicalPlan logicalPlan();

    Tuple2<Option<JoinTableMethod>,List<Instruction>> produce( final CodeGenContext context, final Cardinalities cardinalities );

    Tuple2<Option<JoinTableMethod>,List<Instruction>> consume( final CodeGenContext context, final CodeGenPlan child, final Cardinalities cardinalities );
}
