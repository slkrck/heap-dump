package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class ListReferenceType$ extends AbstractFunction1<RepresentationType,ListReferenceType> implements Serializable
{
    public static ListReferenceType$ MODULE$;

    static
    {
        new ListReferenceType$();
    }

    private ListReferenceType$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ListReferenceType";
    }

    public ListReferenceType apply( final RepresentationType inner )
    {
        return new ListReferenceType( inner );
    }

    public Option<RepresentationType> unapply( final ListReferenceType x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.inner() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
