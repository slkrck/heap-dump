package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class GreaterThanEqual
{
    public static String toString()
    {
        return GreaterThanEqual$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return GreaterThanEqual$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return GreaterThanEqual$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return GreaterThanEqual$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return GreaterThanEqual$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return GreaterThanEqual$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return GreaterThanEqual$.MODULE$.productPrefix();
    }
}
