package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class Or implements CodeGenExpression, Product, Serializable
{
    private final CodeGenExpression lhs;
    private final CodeGenExpression rhs;

    public Or( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        this.lhs = lhs;
        this.rhs = rhs;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Or x$0 )
    {
        return Or$.MODULE$.unapply( var0 );
    }

    public static Or apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return Or$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CodeGenExpression,CodeGenExpression>,Or> tupled()
    {
        return Or$.MODULE$.tupled();
    }

    public static Function1<CodeGenExpression,Function1<CodeGenExpression,Or>> curried()
    {
        return Or$.MODULE$.curried();
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public CodeGenExpression lhs()
    {
        return this.lhs;
    }

    public CodeGenExpression rhs()
    {
        return this.rhs;
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.lhs().nullable( context ) || this.rhs().nullable( context );
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return !this.nullable( context ) ? CodeGenType$.MODULE$.primitiveBool() : new CypherCodeGenType(.MODULE$.CTBoolean(),ReferenceType$.MODULE$);
    }

    public final <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.lhs().init( generator, context );
        this.rhs().init( generator, context );
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        Object var10000;
        if ( !this.nullable( context ) )
        {
            Object var3;
            label39:
            {
                Tuple2 var4 = new Tuple2( this.lhs().codeGenType( context ), this.rhs().codeGenType( context ) );
                if ( var4 != null )
                {
                    CypherCodeGenType t1 = (CypherCodeGenType) var4._1();
                    CypherCodeGenType t2 = (CypherCodeGenType) var4._2();
                    if ( t1.isPrimitive() && t2.isPrimitive() )
                    {
                        var3 = structure.orExpression( this.lhs().generateExpression( structure, context ),
                                this.rhs().generateExpression( structure, context ) );
                        break label39;
                    }
                }

                if ( var4 != null )
                {
                    CypherCodeGenType t1 = (CypherCodeGenType) var4._1();
                    CypherCodeGenType t2 = (CypherCodeGenType) var4._2();
                    if ( t1.isPrimitive() )
                    {
                        var3 = structure.orExpression( this.lhs().generateExpression( structure, context ),
                                structure.unbox( this.rhs().generateExpression( structure, context ), t2 ) );
                        break label39;
                    }
                }

                if ( var4 != null )
                {
                    CypherCodeGenType t1 = (CypherCodeGenType) var4._1();
                    CypherCodeGenType t2 = (CypherCodeGenType) var4._2();
                    if ( t2.isPrimitive() )
                    {
                        var3 = structure.orExpression( structure.unbox( this.lhs().generateExpression( structure, context ), t1 ),
                                this.rhs().generateExpression( structure, context ) );
                        break label39;
                    }
                }

                var3 = structure.unbox( structure.threeValuedOrExpression(
                        structure.box( this.lhs().generateExpression( structure, context ), this.lhs().codeGenType( context ) ),
                        structure.box( this.rhs().generateExpression( structure, context ), this.rhs().codeGenType( context ) ) ),
                        new CypherCodeGenType(.MODULE$.CTBoolean(), ReferenceType$.MODULE$ ));
            }

            var10000 = var3;
        }
        else
        {
            var10000 =
                    structure.threeValuedOrExpression( structure.box( this.lhs().generateExpression( structure, context ), this.lhs().codeGenType( context ) ),
                            structure.box( this.rhs().generateExpression( structure, context ), this.rhs().codeGenType( context ) ) );
        }

        return var10000;
    }

    public Or copy( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Or( lhs, rhs );
    }

    public CodeGenExpression copy$default$1()
    {
        return this.lhs();
    }

    public CodeGenExpression copy$default$2()
    {
        return this.rhs();
    }

    public String productPrefix()
    {
        return "Or";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        CodeGenExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Or;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof Or )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            Or var4 = (Or) x$1;
                            CodeGenExpression var10000 = this.lhs();
                            CodeGenExpression var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.rhs();
                            CodeGenExpression var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
