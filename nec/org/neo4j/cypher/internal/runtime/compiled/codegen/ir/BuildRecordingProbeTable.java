package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongToListTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LongsToListTable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.RecordingJoinTableType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SimpleTupleDescriptor;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple4;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing.;

@JavaDocToJava
public class BuildRecordingProbeTable implements BuildProbeTable, Product, Serializable
{
    private final String id;
    private final String name;
    private final IndexedSeq<Variable> nodes;
    private final Map<String,Variable> valueSymbols;
    private final CodeGenContext context;
    private final Map<String,VariableData> fieldToVarName;
    private final Map<String,String> varNameToField;
    private final SimpleTupleDescriptor tupleDescriptor;
    private final RecordingJoinTableType tableType;
    private final JoinData joinData;

    public BuildRecordingProbeTable( final String id, final String name, final IndexedSeq<Variable> nodes, final Map<String,Variable> valueSymbols,
            final CodeGenContext context )
    {
        this.id = id;
        this.name = name;
        this.nodes = nodes;
        this.valueSymbols = valueSymbols;
        this.context = context;
        Instruction.$init$( this );
        BuildProbeTable.$init$( this );
        Product.$init$( this );
        this.fieldToVarName = (Map) valueSymbols.map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                String variable = (String) x0$1._1();
                Variable incoming = (Variable) x0$1._2();
                Tuple2 var2 = new Tuple2( this.context.namer().newVarName(), new VariableData( variable, incoming,
                        incoming.copy( this.context.namer().newVarName(), incoming.copy$default$2(), incoming.copy$default$3() ) ) );
                return var2;
            }
            else
            {
                throw new MatchError( x0$1 );
            }
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
        this.varNameToField = (Map) this.fieldToVarName().map( ( x0$3 ) -> {
            if ( x0$3 != null )
            {
                String fieldName = (String) x0$3._1();
                VariableData localName = (VariableData) x0$3._2();
                Tuple2 var1 = scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( localName.outgoing().name() ), fieldName);
                return var1;
            }
            else
            {
                throw new MatchError( x0$3 );
            }
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
        this.tupleDescriptor = new SimpleTupleDescriptor( this.fieldToVarName().mapValues( ( c ) -> {
            return c.outgoing().codeGenType();
        } ) );
        this.tableType = (RecordingJoinTableType) (nodes.size() == 1 ? new LongToListTable( this.tupleDescriptor(), this.varNameToField() )
                                                                     : new LongsToListTable( this.tupleDescriptor(), this.varNameToField() ));
        this.joinData = new JoinData( this.fieldToVarName(), name, this.tableType(), id );
    }

    public static Option<Tuple4<String,String,IndexedSeq<Variable>,Map<String,Variable>>> unapply( final BuildRecordingProbeTable x$0 )
    {
        return BuildRecordingProbeTable$.MODULE$.unapply( var0 );
    }

    public static BuildRecordingProbeTable apply( final String id, final String name, final IndexedSeq<Variable> nodes, final Map<String,Variable> valueSymbols,
            final CodeGenContext context )
    {
        return BuildRecordingProbeTable$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        BuildProbeTable.init$( this, generator, context );
    }

    public Seq<> children()
    {
        return BuildProbeTable.children$( this );
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String id()
    {
        return this.id;
    }

    public String name()
    {
        return this.name;
    }

    public IndexedSeq<Variable> nodes()
    {
        return this.nodes;
    }

    public Map<String,Variable> valueSymbols()
    {
        return this.valueSymbols;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext ignored )
    {
        generator.trace( this.id(), new Some( this.getClass().getSimpleName() ), ( body ) -> {
            $anonfun$body$1( this, body );
            return BoxedUnit.UNIT;
        } );
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.id()}) ));
    }

    private Map<String,VariableData> fieldToVarName()
    {
        return this.fieldToVarName;
    }

    private Map<String,String> varNameToField()
    {
        return this.varNameToField;
    }

    private SimpleTupleDescriptor tupleDescriptor()
    {
        return this.tupleDescriptor;
    }

    public RecordingJoinTableType tableType()
    {
        return this.tableType;
    }

    public JoinData joinData()
    {
        return this.joinData;
    }

    public BuildRecordingProbeTable copy( final String id, final String name, final IndexedSeq<Variable> nodes, final Map<String,Variable> valueSymbols,
            final CodeGenContext context )
    {
        return new BuildRecordingProbeTable( id, name, nodes, valueSymbols, context );
    }

    public String copy$default$1()
    {
        return this.id();
    }

    public String copy$default$2()
    {
        return this.name();
    }

    public IndexedSeq<Variable> copy$default$3()
    {
        return this.nodes();
    }

    public Map<String,Variable> copy$default$4()
    {
        return this.valueSymbols();
    }

    public String productPrefix()
    {
        return "BuildRecordingProbeTable";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.id();
            break;
        case 1:
            var10000 = this.name();
            break;
        case 2:
            var10000 = this.nodes();
            break;
        case 3:
            var10000 = this.valueSymbols();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof BuildRecordingProbeTable;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof BuildRecordingProbeTable )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            BuildRecordingProbeTable var4 = (BuildRecordingProbeTable) x$1;
                            String var10000 = this.id();
                            String var5 = var4.id();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            var10000 = this.name();
                            String var6 = var4.name();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label72;
                            }

                            IndexedSeq var9 = this.nodes();
                            IndexedSeq var7 = var4.nodes();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label72;
                            }

                            Map var10 = this.valueSymbols();
                            Map var8 = var4.valueSymbols();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label63;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label81;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
