package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.NodeExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RelationshipExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class BaseAggregateExpression implements AggregateExpression
{
    private final CodeGenExpression expression;
    private final boolean distinct;

    public BaseAggregateExpression( final CodeGenExpression expression, final boolean distinct )
    {
        this.expression = expression;
        this.distinct = distinct;
        AggregateExpression.$init$( this );
    }

    public Instruction continuation( final Instruction instruction )
    {
        return AggregateExpression.continuation$( this, instruction );
    }

    public abstract <E> void distinctCondition( final E value, final CodeGenType valueType, final MethodStructure<E> structure,
            final Function1<MethodStructure<E>,BoxedUnit> block, final CodeGenContext context );

    public <E> void ifNotNull( final MethodStructure<E> structure, final Function1<MethodStructure<E>,BoxedUnit> block, final CodeGenContext context )
    {
        CodeGenExpression var5 = this.expression;
        BoxedUnit var4;
        if ( var5 instanceof NodeExpression )
        {
            NodeExpression var6 = (NodeExpression) var5;
            Variable v = var6.nodeIdVar();
            this.primitiveIfNot( v, structure, ( x$1 ) -> {
                $anonfun$ifNotNull$1( block, x$1 );
                return BoxedUnit.UNIT;
            }, context );
            var4 = BoxedUnit.UNIT;
        }
        else if ( var5 instanceof RelationshipExpression )
        {
            RelationshipExpression var8 = (RelationshipExpression) var5;
            Variable v = var8.relId();
            this.primitiveIfNot( v, structure, ( x$2 ) -> {
                $anonfun$ifNotNull$2( block, x$2 );
                return BoxedUnit.UNIT;
            }, context );
            var4 = BoxedUnit.UNIT;
        }
        else
        {
            String tmpName = context.namer().newVarName();
            structure.assign( tmpName, this.expression.codeGenType( context ), this.expression.generateExpression( structure, context ) );
            Function1 var10000;
            if ( var5.nullable( context ) )
            {
                Object var12 = structure.loadVariable( tmpName );
                CypherCodeGenType var13 = this.expression.codeGenType( context );
                var10000 = ( blockx ) -> {
                    $anonfun$ifNotNull$3( structure, var12, var13, blockx );
                    return BoxedUnit.UNIT;
                };
            }
            else
            {
                var10000 = ( x$3 ) -> {
                    $anonfun$ifNotNull$4( structure, x$3 );
                    return BoxedUnit.UNIT;
                };
            }

            Function1 perhapsCheckForNotNullStatement = var10000;
            var4 = (BoxedUnit) perhapsCheckForNotNullStatement.apply( ( body ) -> {
                $anonfun$ifNotNull$5( this, structure, block, context, tmpName, body );
                return BoxedUnit.UNIT;
            } );
        }
    }

    private <E> void primitiveIfNot( final Variable v, final MethodStructure<E> structure, final Function1<MethodStructure<E>,BoxedUnit> block,
            final CodeGenContext context )
    {
        structure.ifNotStatement(
                structure.equalityExpression( structure.loadVariable( v.name() ), structure.constantExpression( BoxesRunTime.boxToLong( -1L ) ),
                        CodeGenType$.MODULE$.primitiveInt() ), ( body ) -> {
                    $anonfun$primitiveIfNot$1( this, v, structure, block, context, body );
                    return BoxedUnit.UNIT;
                } );
    }
}
