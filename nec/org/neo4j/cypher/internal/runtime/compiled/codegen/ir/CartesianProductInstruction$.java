package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class CartesianProductInstruction$ extends AbstractFunction2<String,Instruction,CartesianProductInstruction> implements Serializable
{
    public static CartesianProductInstruction$ MODULE$;

    static
    {
        new CartesianProductInstruction$();
    }

    private CartesianProductInstruction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CartesianProductInstruction";
    }

    public CartesianProductInstruction apply( final String id, final Instruction instruction )
    {
        return new CartesianProductInstruction( id, instruction );
    }

    public Option<Tuple2<String,Instruction>> unapply( final CartesianProductInstruction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.id(), x$0.instruction() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
