package org.neo4j.cypher.internal.runtime.compiled.codegen;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class ByteCodeMode
{
    public static String toString()
    {
        return ByteCodeMode$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return ByteCodeMode$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return ByteCodeMode$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return ByteCodeMode$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return ByteCodeMode$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return ByteCodeMode$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return ByteCodeMode$.MODULE$.productPrefix();
    }
}
