package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.Iterable;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction2;

public final class OrderableTupleDescriptor$ extends AbstractFunction2<Map<String,CodeGenType>,Iterable<SortItem>,OrderableTupleDescriptor>
        implements Serializable
{
    public static OrderableTupleDescriptor$ MODULE$;

    static
    {
        new OrderableTupleDescriptor$();
    }

    private OrderableTupleDescriptor$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "OrderableTupleDescriptor";
    }

    public OrderableTupleDescriptor apply( final Map<String,CodeGenType> structure, final Iterable<SortItem> sortItems )
    {
        return new OrderableTupleDescriptor( structure, sortItems );
    }

    public Option<Tuple2<Map<String,CodeGenType>,Iterable<SortItem>>> unapply( final OrderableTupleDescriptor x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.structure(), x$0.sortItems() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
