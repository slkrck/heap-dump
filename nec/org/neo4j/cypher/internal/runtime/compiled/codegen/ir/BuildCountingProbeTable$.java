package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.runtime.AbstractFunction3;

public final class BuildCountingProbeTable$ extends AbstractFunction3<String,String,IndexedSeq<Variable>,BuildCountingProbeTable> implements Serializable
{
    public static BuildCountingProbeTable$ MODULE$;

    static
    {
        new BuildCountingProbeTable$();
    }

    private BuildCountingProbeTable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "BuildCountingProbeTable";
    }

    public BuildCountingProbeTable apply( final String id, final String name, final IndexedSeq<Variable> nodes )
    {
        return new BuildCountingProbeTable( id, name, nodes );
    }

    public Option<Tuple3<String,String,IndexedSeq<Variable>>> unapply( final BuildCountingProbeTable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.id(), x$0.name(), x$0.nodes() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
