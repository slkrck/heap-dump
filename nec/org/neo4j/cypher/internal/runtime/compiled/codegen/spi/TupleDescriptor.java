package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface TupleDescriptor
{
    Map<String,CodeGenType> structure();
}
