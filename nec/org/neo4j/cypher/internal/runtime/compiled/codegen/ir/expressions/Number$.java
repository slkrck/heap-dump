package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import scala.Option;
import scala.Some;
import scala.runtime.BoxesRunTime;

public final class Number$
{
    public static Number$ MODULE$;

    static
    {
        new Number$();
    }

    private Number$()
    {
        MODULE$ = this;
    }

    public Option<CypherType> unapply( final CypherType x )
    {
        return (new Some( x )).filter( ( other ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$unapply$1( other ) );
        } );
    }
}
