package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class IntType
{
    public static String toString()
    {
        return IntType$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return IntType$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return IntType$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return IntType$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return IntType$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return IntType$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return IntType$.MODULE$.productPrefix();
    }
}
