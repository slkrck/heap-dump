package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class NodeProjection$ extends AbstractFunction1<Variable,NodeProjection> implements Serializable
{
    public static NodeProjection$ MODULE$;

    static
    {
        new NodeProjection$();
    }

    private NodeProjection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodeProjection";
    }

    public NodeProjection apply( final Variable nodeIdVar )
    {
        return new NodeProjection( nodeIdVar );
    }

    public Option<Variable> unapply( final NodeProjection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.nodeIdVar() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
