package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class TypeCodeGenFunction
{
    public static String toString()
    {
        return TypeCodeGenFunction$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return TypeCodeGenFunction$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return TypeCodeGenFunction$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return TypeCodeGenFunction$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return TypeCodeGenFunction$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return TypeCodeGenFunction$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return TypeCodeGenFunction$.MODULE$.productPrefix();
    }

    public static CodeGenExpression apply( final CodeGenExpression arg )
    {
        return TypeCodeGenFunction$.MODULE$.apply( var0 );
    }
}
