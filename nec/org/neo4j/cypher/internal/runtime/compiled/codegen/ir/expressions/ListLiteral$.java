package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction1;

public final class ListLiteral$ extends AbstractFunction1<Seq<CodeGenExpression>,ListLiteral> implements Serializable
{
    public static ListLiteral$ MODULE$;

    static
    {
        new ListLiteral$();
    }

    private ListLiteral$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ListLiteral";
    }

    public ListLiteral apply( final Seq<CodeGenExpression> expressions )
    {
        return new ListLiteral( expressions );
    }

    public Option<Seq<CodeGenExpression>> unapply( final ListLiteral x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.expressions() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
