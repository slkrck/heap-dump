package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class ScanForLabel$ extends AbstractFunction3<String,String,String,ScanForLabel> implements Serializable
{
    public static ScanForLabel$ MODULE$;

    static
    {
        new ScanForLabel$();
    }

    private ScanForLabel$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ScanForLabel";
    }

    public ScanForLabel apply( final String opName, final String labelName, final String labelVar )
    {
        return new ScanForLabel( opName, labelName, labelVar );
    }

    public Option<Tuple3<String,String,String>> unapply( final ScanForLabel x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.opName(), x$0.labelName(), x$0.labelVar() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
