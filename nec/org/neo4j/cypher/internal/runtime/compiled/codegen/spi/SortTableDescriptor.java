package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SortTableDescriptor
{
    TupleDescriptor tupleDescriptor();
}
