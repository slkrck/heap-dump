package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class Not$ extends AbstractFunction1<CodeGenExpression,Not> implements Serializable
{
    public static Not$ MODULE$;

    static
    {
        new Not$();
    }

    private Not$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Not";
    }

    public Not apply( final CodeGenExpression inner )
    {
        return new Not( inner );
    }

    public Option<CodeGenExpression> unapply( final Not x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.inner() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
