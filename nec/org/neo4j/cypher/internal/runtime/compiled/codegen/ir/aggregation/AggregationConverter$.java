package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ExpressionConverter$;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Count.;
import org.neo4j.exceptions.CantCompileQueryException;
import scala.Tuple2;
import scala.collection.Iterable;

public final class AggregationConverter$
{
    public static AggregationConverter$ MODULE$;

    static
    {
        new AggregationConverter$();
    }

    private AggregationConverter$()
    {
        MODULE$ = this;
    }

    public AggregateExpression aggregateExpressionConverter( final String opName, final Iterable<Tuple2<String,CodeGenExpression>> groupingVariables,
            final String name, final Expression e, final CodeGenContext context )
    {
        Variable variable = new Variable( context.namer().newVarName(), CodeGenType$.MODULE$.primitiveInt(), Variable$.MODULE$.apply$default$3() );
        context.addVariable( name, variable );
        context.addProjectedVariable( name, variable );
        if ( !(e instanceof FunctionInvocation) )
        {
            throw new CantCompileQueryException( (new StringBuilder( 17 )).append( e ).append( " is not supported" ).toString() );
        }
        else
        {
            FunctionInvocation var10 = (FunctionInvocation) e;
            boolean var11 = false;
            Object var12 = null;
            Function var13 = var10.function();
            Object var7;
            if (.MODULE$.equals( var13 )){
            var11 = true;
            if ( groupingVariables.isEmpty() )
            {
                var7 = new SimpleCount( opName, variable, ExpressionConverter$.MODULE$.createExpression( (Expression) var10.args().apply( 0 ), context ),
                        var10.distinct() );
                return (AggregateExpression) var7;
            }
        }

            if ( !var11 )
            {
                throw new CantCompileQueryException( (new StringBuilder( 17 )).append( var13 ).append( " is not supported" ).toString() );
            }
            else
            {
                var7 = new DynamicCount( opName, variable, ExpressionConverter$.MODULE$.createExpression( (Expression) var10.args().apply( 0 ), context ),
                        groupingVariables, var10.distinct() );
                return (AggregateExpression) var7;
            }
        }
    }
}
