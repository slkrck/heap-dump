package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class Variable$ extends AbstractFunction3<String,CodeGenType,Object,Variable> implements Serializable
{
    public static Variable$ MODULE$;

    static
    {
        new Variable$();
    }

    private Variable$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$3()
    {
        return false;
    }

    public final String toString()
    {
        return "Variable";
    }

    public Variable apply( final String name, final CodeGenType codeGenType, final boolean nullable )
    {
        return new Variable( name, codeGenType, nullable );
    }

    public boolean apply$default$3()
    {
        return false;
    }

    public Option<Tuple3<String,CodeGenType,Object>> unapply( final Variable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.name(), x$0.codeGenType(), BoxesRunTime.boxToBoolean( x$0.nullable() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
