package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple6;
import scala.Predef.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class RelationshipCountFromCountStoreInstruction implements Instruction, Product, Serializable
{
    private final String opName;
    private final Variable variable;
    private final Option<Tuple2<Option<Object>,String>> startLabel;
    private final Seq<Tuple2<Option<Object>,String>> relTypes;
    private final Option<Tuple2<Option<Object>,String>> endLabel;
    private final Instruction inner;
    private final String hasTokens;

    public RelationshipCountFromCountStoreInstruction( final String opName, final Variable variable, final Option<Tuple2<Option<Object>,String>> startLabel,
            final Seq<Tuple2<Option<Object>,String>> relTypes, final Option<Tuple2<Option<Object>,String>> endLabel, final Instruction inner )
    {
        this.opName = opName;
        this.variable = variable;
        this.startLabel = startLabel;
        this.relTypes = relTypes;
        this.endLabel = endLabel;
        this.inner = inner;
        Instruction.$init$( this );
        Product.$init$( this );
        this.hasTokens = (new StringBuilder( 9 )).append( opName ).append( "hasTokens" ).toString();
    }

    public static Option<Tuple6<String,Variable,Option<Tuple2<Option<Object>,String>>,Seq<Tuple2<Option<Object>,String>>,Option<Tuple2<Option<Object>,String>>,Instruction>> unapply(
            final RelationshipCountFromCountStoreInstruction x$0 )
    {
        return RelationshipCountFromCountStoreInstruction$.MODULE$.unapply( var0 );
    }

    public static RelationshipCountFromCountStoreInstruction apply( final String opName, final Variable variable,
            final Option<Tuple2<Option<Object>,String>> startLabel, final Seq<Tuple2<Option<Object>,String>> relTypes,
            final Option<Tuple2<Option<Object>,String>> endLabel, final Instruction inner )
    {
        return RelationshipCountFromCountStoreInstruction$.MODULE$.apply( var0, var1, var2, var3, var4, var5 );
    }

    public static Function1<Tuple6<String,Variable,Option<Tuple2<Option<Object>,String>>,Seq<Tuple2<Option<Object>,String>>,Option<Tuple2<Option<Object>,String>>,Instruction>,RelationshipCountFromCountStoreInstruction> tupled()
    {
        return RelationshipCountFromCountStoreInstruction$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Variable,Function1<Option<Tuple2<Option<Object>,String>>,Function1<Seq<Tuple2<Option<Object>,String>>,Function1<Option<Tuple2<Option<Object>,String>>,Function1<Instruction,RelationshipCountFromCountStoreInstruction>>>>>> curried()
    {
        return RelationshipCountFromCountStoreInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String opName()
    {
        return this.opName;
    }

    public Variable variable()
    {
        return this.variable;
    }

    public Option<Tuple2<Option<Object>,String>> startLabel()
    {
        return this.startLabel;
    }

    public Seq<Tuple2<Option<Object>,String>> relTypes()
    {
        return this.relTypes;
    }

    public Option<Tuple2<Option<Object>,String>> endLabel()
    {
        return this.endLabel;
    }

    public Instruction inner()
    {
        return this.inner;
    }

    private String hasTokens()
    {
        return this.hasTokens;
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.assign( this.variable(), generator.constantPrimitiveExpression( BoxesRunTime.boxToLong( 0L ) ) );
        generator.trace( this.opName(), generator.trace$default$2(), ( body ) -> {
            $anonfun$body$1( this, body );
            return BoxedUnit.UNIT;
        } );
        this.inner().body( generator, context );
    }

    public Set<String> operatorId()
    {
        return (Set).MODULE$.Set().apply(.MODULE$.wrapRefArray( (Object[]) (new String[]{this.opName()}) ));
    }

    public Seq<Instruction> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply(.MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.inner()}) ));
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        Instruction.init$( this, generator, context );
        generator.assign( this.hasTokens(), CodeGenType$.MODULE$.primitiveBool(), generator.constantPrimitiveExpression( BoxesRunTime.boxToBoolean( true ) ) );
        this.startLabel().foreach( ( x0$2 ) -> {
            $anonfun$init$2( this, generator, x0$2 );
            return BoxedUnit.UNIT;
        } );
        this.endLabel().foreach( ( x0$3 ) -> {
            $anonfun$init$3( this, generator, x0$3 );
            return BoxedUnit.UNIT;
        } );
        this.relTypes().foreach( ( x0$4 ) -> {
            $anonfun$init$4( this, generator, x0$4 );
            return BoxedUnit.UNIT;
        } );
    }

    public RelationshipCountFromCountStoreInstruction copy( final String opName, final Variable variable,
            final Option<Tuple2<Option<Object>,String>> startLabel, final Seq<Tuple2<Option<Object>,String>> relTypes,
            final Option<Tuple2<Option<Object>,String>> endLabel, final Instruction inner )
    {
        return new RelationshipCountFromCountStoreInstruction( opName, variable, startLabel, relTypes, endLabel, inner );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public Variable copy$default$2()
    {
        return this.variable();
    }

    public Option<Tuple2<Option<Object>,String>> copy$default$3()
    {
        return this.startLabel();
    }

    public Seq<Tuple2<Option<Object>,String>> copy$default$4()
    {
        return this.relTypes();
    }

    public Option<Tuple2<Option<Object>,String>> copy$default$5()
    {
        return this.endLabel();
    }

    public Instruction copy$default$6()
    {
        return this.inner();
    }

    public String productPrefix()
    {
        return "RelationshipCountFromCountStoreInstruction";
    }

    public int productArity()
    {
        return 6;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.variable();
            break;
        case 2:
            var10000 = this.startLabel();
            break;
        case 3:
            var10000 = this.relTypes();
            break;
        case 4:
            var10000 = this.endLabel();
            break;
        case 5:
            var10000 = this.inner();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipCountFromCountStoreInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var15;
        if ( this != x$1 )
        {
            label99:
            {
                boolean var2;
                if ( x$1 instanceof RelationshipCountFromCountStoreInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label81:
                    {
                        label90:
                        {
                            RelationshipCountFromCountStoreInstruction var4 = (RelationshipCountFromCountStoreInstruction) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label90;
                            }

                            Variable var11 = this.variable();
                            Variable var6 = var4.variable();
                            if ( var11 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var11.equals( var6 ) )
                            {
                                break label90;
                            }

                            Option var12 = this.startLabel();
                            Option var7 = var4.startLabel();
                            if ( var12 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var12.equals( var7 ) )
                            {
                                break label90;
                            }

                            Seq var13 = this.relTypes();
                            Seq var8 = var4.relTypes();
                            if ( var13 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var13.equals( var8 ) )
                            {
                                break label90;
                            }

                            var12 = this.endLabel();
                            Option var9 = var4.endLabel();
                            if ( var12 == null )
                            {
                                if ( var9 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var12.equals( var9 ) )
                            {
                                break label90;
                            }

                            Instruction var14 = this.inner();
                            Instruction var10 = var4.inner();
                            if ( var14 == null )
                            {
                                if ( var10 != null )
                                {
                                    break label90;
                                }
                            }
                            else if ( !var14.equals( var10 ) )
                            {
                                break label90;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var15 = true;
                                break label81;
                            }
                        }

                        var15 = false;
                    }

                    if ( var15 )
                    {
                        break label99;
                    }
                }

                var15 = false;
                return var15;
            }
        }

        var15 = true;
        return var15;
    }

    private final Object labelToken$1( final Option lbl, final String varName, final MethodStructure ifBody$1 )
    {
        boolean var5 = false;
        Some var6 = null;
        Object var4;
        if ( lbl instanceof Some )
        {
            var5 = true;
            var6 = (Some) lbl;
            Tuple2 var8 = (Tuple2) var6.value();
            if ( var8 != null )
            {
                Option var9 = (Option) var8._1();
                if ( var9 instanceof Some )
                {
                    Some var10 = (Some) var9;
                    int token = BoxesRunTime.unboxToInt( var10.value() );
                    var4 = ifBody$1.constantExpression( BoxesRunTime.boxToInteger( token ) );
                    return var4;
                }
            }
        }

        if ( var5 )
        {
            Tuple2 var12 = (Tuple2) var6.value();
            if ( var12 != null )
            {
                Option var13 = (Option) var12._1();
                String labelName = (String) var12._2();
                if ( scala.None..MODULE$.equals( var13 )){
                String variableName = (new StringBuilder( 0 )).append( this.variable().name() ).append( varName ).append( labelName ).toString();
                var4 = ifBody$1.loadVariable( variableName );
                return var4;
            }
            }
        }

        var4 = ifBody$1.wildCardToken();
        return var4;
    }

    private final void loadLabelToken$1( final String labelName, final String varName, final MethodStructure generator$1 )
    {
        String variableName = (new StringBuilder( 0 )).append( this.variable().name() ).append( varName ).append( labelName ).toString();
        generator$1.assign( variableName, CodeGenType$.MODULE$.javaInt(), generator$1.lookupLabelIdE( labelName ) );
        Object isTokenMissing =
                generator$1.equalityExpression( generator$1.loadVariable( variableName ), generator$1.wildCardToken(), CodeGenType$.MODULE$.primitiveBool() );
        generator$1.ifStatement( isTokenMissing, ( block ) -> {
            $anonfun$init$1( this, block );
            return BoxedUnit.UNIT;
        } );
    }
}
