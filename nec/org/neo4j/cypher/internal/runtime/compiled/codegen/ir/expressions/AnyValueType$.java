package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class AnyValueType$ implements AnyValueType, Product, Serializable
{
    public static AnyValueType$ MODULE$;

    static
    {
        new AnyValueType$();
    }

    private AnyValueType$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "AnyValueType";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AnyValueType$;
    }

    public int hashCode()
    {
        return -1360734241;
    }

    public String toString()
    {
        return "AnyValueType";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
