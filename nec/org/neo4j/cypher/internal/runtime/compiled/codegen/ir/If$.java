package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class If$ extends AbstractFunction2<CodeGenExpression,Instruction,If> implements Serializable
{
    public static If$ MODULE$;

    static
    {
        new If$();
    }

    private If$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "If";
    }

    public If apply( final CodeGenExpression predicate, final Instruction block )
    {
        return new If( predicate, block );
    }

    public Option<Tuple2<CodeGenExpression,Instruction>> unapply( final If x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.predicate(), x$0.block() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
