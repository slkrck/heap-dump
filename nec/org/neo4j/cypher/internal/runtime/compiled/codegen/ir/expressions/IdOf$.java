package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class IdOf$ extends AbstractFunction1<Variable,IdOf> implements Serializable
{
    public static IdOf$ MODULE$;

    static
    {
        new IdOf$();
    }

    private IdOf$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "IdOf";
    }

    public IdOf apply( final Variable variable )
    {
        return new IdOf( variable );
    }

    public Option<Variable> unapply( final IdOf x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.variable() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
