package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ScanForLabel implements LoopDataGenerator, Product, Serializable
{
    private final String opName;
    private final String labelName;
    private final String labelVar;

    public ScanForLabel( final String opName, final String labelName, final String labelVar )
    {
        this.opName = opName;
        this.labelName = labelName;
        this.labelVar = labelVar;
        Product.$init$( this );
    }

    public static Option<Tuple3<String,String,String>> unapply( final ScanForLabel x$0 )
    {
        return ScanForLabel$.MODULE$.unapply( var0 );
    }

    public static ScanForLabel apply( final String opName, final String labelName, final String labelVar )
    {
        return ScanForLabel$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<String,String,String>,ScanForLabel> tupled()
    {
        return ScanForLabel$.MODULE$.tupled();
    }

    public static Function1<String,Function1<String,Function1<String,ScanForLabel>>> curried()
    {
        return ScanForLabel$.MODULE$.curried();
    }

    public String opName()
    {
        return this.opName;
    }

    public String labelName()
    {
        return this.labelName;
    }

    public String labelVar()
    {
        return this.labelVar;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.lookupLabelId( this.labelVar(), this.labelName() );
    }

    public <E> void produceLoopData( final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.labelScan( cursorName, this.labelVar() );
        generator.incrementDbHits();
    }

    public <E> void getNext( final Variable nextVar, final String cursorName, final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.incrementDbHits();
        generator.nodeFromNodeLabelIndexCursor( nextVar.name(), cursorName );
    }

    public <E> E checkNext( final MethodStructure<E> generator, final String cursorName )
    {
        return generator.advanceNodeLabelIndexCursor( cursorName );
    }

    public <E> void close( final String cursorName, final MethodStructure<E> generator )
    {
        generator.closeNodeLabelIndexCursor( cursorName );
    }

    public ScanForLabel copy( final String opName, final String labelName, final String labelVar )
    {
        return new ScanForLabel( opName, labelName, labelVar );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public String copy$default$2()
    {
        return this.labelName();
    }

    public String copy$default$3()
    {
        return this.labelVar();
    }

    public String productPrefix()
    {
        return "ScanForLabel";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        String var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.labelName();
            break;
        case 2:
            var10000 = this.labelVar();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ScanForLabel;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof ScanForLabel )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            ScanForLabel var4 = (ScanForLabel) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            var10000 = this.labelName();
                            String var6 = var4.labelName();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label63;
                            }

                            var10000 = this.labelVar();
                            String var7 = var4.labelVar();
                            if ( var10000 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label54;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label72;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
