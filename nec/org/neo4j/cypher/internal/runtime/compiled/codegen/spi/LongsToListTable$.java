package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction2;

public final class LongsToListTable$ extends AbstractFunction2<TupleDescriptor,Map<String,String>,LongsToListTable> implements Serializable
{
    public static LongsToListTable$ MODULE$;

    static
    {
        new LongsToListTable$();
    }

    private LongsToListTable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "LongsToListTable";
    }

    public LongsToListTable apply( final TupleDescriptor tupleDescriptor, final Map<String,String> localMap )
    {
        return new LongsToListTable( tupleDescriptor, localMap );
    }

    public Option<Tuple2<TupleDescriptor,Map<String,String>>> unapply( final LongsToListTable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.tupleDescriptor(), x$0.localMap() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
