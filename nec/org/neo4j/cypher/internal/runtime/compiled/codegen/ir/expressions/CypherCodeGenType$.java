package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class CypherCodeGenType$ extends AbstractFunction2<CypherType,RepresentationType,CypherCodeGenType> implements Serializable
{
    public static CypherCodeGenType$ MODULE$;

    static
    {
        new CypherCodeGenType$();
    }

    private CypherCodeGenType$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CypherCodeGenType";
    }

    public CypherCodeGenType apply( final CypherType ct, final RepresentationType repr )
    {
        return new CypherCodeGenType( ct, repr );
    }

    public Option<Tuple2<CypherType,RepresentationType>> unapply( final CypherCodeGenType x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.ct(), x$0.repr() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
