package org.neo4j.cypher.internal.runtime.compiled.codegen.spi;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class LongsToCountTable$ implements CountingJoinTableType, Product, Serializable
{
    public static LongsToCountTable$ MODULE$;

    static
    {
        new LongsToCountTable$();
    }

    private LongsToCountTable$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "LongsToCountTable";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LongsToCountTable$;
    }

    public int hashCode()
    {
        return -1342456879;
    }

    public String toString()
    {
        return "LongsToCountTable";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
