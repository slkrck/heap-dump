package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class IdOf implements CodeGenExpression, Product, Serializable
{
    private final Variable variable;

    public IdOf( final Variable variable )
    {
        this.variable = variable;
        CodeGenExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Variable> unapply( final IdOf x$0 )
    {
        return IdOf$.MODULE$.unapply( var0 );
    }

    public static IdOf apply( final Variable variable )
    {
        return IdOf$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Variable,A> andThen( final Function1<IdOf,A> g )
    {
        return IdOf$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,IdOf> compose( final Function1<A,Variable> g )
    {
        return IdOf$.MODULE$.compose( var0 );
    }

    public boolean needsJavaNullCheck( final CodeGenContext context )
    {
        return CodeGenExpression.needsJavaNullCheck$( this, context );
    }

    public Variable variable()
    {
        return this.variable;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
    }

    public <E> E generateExpression( final MethodStructure<E> structure, final CodeGenContext context )
    {
        return this.nullable( context ) ? structure.nullableReference( this.variable().name(), this.variable().codeGenType(),
                structure.box( structure.loadVariable( this.variable().name() ), new CypherCodeGenType(.MODULE$.CTInteger(), ReferenceType$.MODULE$ ) )) :
        structure.loadVariable( this.variable().name() );
    }

    public boolean nullable( final CodeGenContext context )
    {
        return this.variable().nullable();
    }

    public CypherCodeGenType codeGenType( final CodeGenContext context )
    {
        return this.nullable( context ) ? new CypherCodeGenType(.MODULE$.CTInteger(),ReferenceType$.MODULE$) :CodeGenType$.MODULE$.primitiveInt();
    }

    public IdOf copy( final Variable variable )
    {
        return new IdOf( variable );
    }

    public Variable copy$default$1()
    {
        return this.variable();
    }

    public String productPrefix()
    {
        return "IdOf";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.variable();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof IdOf;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof IdOf )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        IdOf var4 = (IdOf) x$1;
                        Variable var10000 = this.variable();
                        Variable var5 = var4.variable();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
