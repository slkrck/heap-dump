package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class VariableData$ extends AbstractFunction3<String,Variable,Variable,VariableData> implements Serializable
{
    public static VariableData$ MODULE$;

    static
    {
        new VariableData$();
    }

    private VariableData$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "VariableData";
    }

    public VariableData apply( final String variable, final Variable incoming, final Variable outgoing )
    {
        return new VariableData( variable, incoming, outgoing );
    }

    public Option<Tuple3<String,Variable,Variable>> unapply( final VariableData x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.variable(), x$0.incoming(), x$0.outgoing() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
