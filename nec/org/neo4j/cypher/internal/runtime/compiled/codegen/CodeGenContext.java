package org.neo4j.cypher.internal.runtime.compiled.codegen;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.JoinData;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.exceptions.InternalException;
import scala.Predef.;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.mutable.Stack;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class CodeGenContext
{
    private final SemanticTable semanticTable;
    private final Map<String,Object> lookup;
    private final Namer namer;
    private final scala.collection.mutable.Map<String,Variable> variables;
    private final scala.collection.mutable.Map<String,Variable> projectedVariables;
    private final scala.collection.mutable.Map<CodeGenPlan,JoinData> probeTables;
    private final Stack<CodeGenPlan> parents;
    private final scala.collection.mutable.Map<String,Id> operatorIds;

    public CodeGenContext( final SemanticTable semanticTable, final Map<String,Object> lookup, final Namer namer )
    {
        this.semanticTable = semanticTable;
        this.lookup = lookup;
        this.namer = namer;
        this.variables = (scala.collection.mutable.Map) scala.collection.mutable.Map..MODULE$.apply( scala.collection.immutable.Nil..MODULE$);
        this.projectedVariables = scala.collection.mutable.Map..MODULE$.empty();
        this.probeTables = (scala.collection.mutable.Map) scala.collection.mutable.Map..MODULE$.apply( scala.collection.immutable.Nil..MODULE$);
        this.parents = (Stack) scala.collection.mutable.Stack..MODULE$.apply( scala.collection.immutable.Nil..MODULE$);
        this.operatorIds = (scala.collection.mutable.Map) scala.collection.mutable.Map..MODULE$.apply( scala.collection.immutable.Nil..MODULE$);
    }

    public static Namer $lessinit$greater$default$3()
    {
        return CodeGenContext$.MODULE$.$lessinit$greater$default$3();
    }

    public SemanticTable semanticTable()
    {
        return this.semanticTable;
    }

    public Namer namer()
    {
        return this.namer;
    }

    private scala.collection.mutable.Map<String,Variable> variables()
    {
        return this.variables;
    }

    private scala.collection.mutable.Map<String,Variable> projectedVariables()
    {
        return this.projectedVariables;
    }

    private scala.collection.mutable.Map<CodeGenPlan,JoinData> probeTables()
    {
        return this.probeTables;
    }

    private Stack<CodeGenPlan> parents()
    {
        return this.parents;
    }

    public scala.collection.mutable.Map<String,Id> operatorIds()
    {
        return this.operatorIds;
    }

    public void addVariable( final String queryVariable, final Variable variable )
    {
        this.variables().put( queryVariable, variable );
    }

    public int numberOfColumns()
    {
        return this.lookup.size();
    }

    public int nameToIndex( final String name )
    {
        return BoxesRunTime.unboxToInt( this.lookup.getOrElse( name, () -> {
            throw new InternalException( (new StringBuilder( 23 )).append( name ).append( " is not a mapped column" ).toString() );
        } ) );
    }

    public void updateVariable( final String queryVariable, final Variable variable )
    {
      .MODULE$. assert (this.variables().isDefinedAt( queryVariable ),() -> {
        return (new StringBuilder( 11 )).append( "undefined: " ).append( queryVariable ).toString();
    });
        this.variables().put( queryVariable, variable );
    }

    public Variable getVariable( final String queryVariable )
    {
        return (Variable) this.variables().apply( queryVariable );
    }

    public boolean hasVariable( final String queryVariable )
    {
        return this.variables().isDefinedAt( queryVariable );
    }

    public boolean isProjectedVariable( final String queryVariable )
    {
        return this.projectedVariables().contains( queryVariable );
    }

    public Set<String> variableQueryVariables()
    {
        return this.variables().keySet().toSet();
    }

    public void addProjectedVariable( final String queryVariable, final Variable variable )
    {
        this.projectedVariables().put( queryVariable, variable );
    }

    public void retainProjectedVariables( final Set<String> queryVariablesToRetain )
    {
        this.projectedVariables().retain( ( key, x$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$retainProjectedVariables$1( queryVariablesToRetain, key, x$1 ) );
        } );
    }

    public Map<String,Variable> getProjectedVariables()
    {
        return this.projectedVariables().toMap(.MODULE$.$conforms());
    }

    public void addProbeTable( final CodeGenPlan plan, final JoinData codeThunk )
    {
        this.probeTables().put( plan, codeThunk );
    }

    public JoinData getProbeTable( final CodeGenPlan plan )
    {
        return (JoinData) this.probeTables().apply( plan );
    }

    public void pushParent( final CodeGenPlan plan )
    {
        if ( plan instanceof LeafCodeGenPlan )
        {
            throw new IllegalArgumentException( (new StringBuilder( 24 )).append( "Leafs can't be parents: " ).append( plan ).toString() );
        }
        else
        {
            this.parents().push( plan );
        }
    }

    public CodeGenPlan popParent()
    {
        return (CodeGenPlan) this.parents().pop();
    }

    public String registerOperator( final LogicalPlan plan )
    {
        String name = this.namer().newOpName( plan.getClass().getSimpleName() );
        this.operatorIds().put( name, new Id( plan.id() ) ).foreach( ( oldId ) -> {
            return $anonfun$registerOperator$1( plan, ((Id) oldId).x() );
        } );
        return name;
    }
}
