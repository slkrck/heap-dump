package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortItem;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.Iterable;
import scala.collection.immutable.Map;

public final class BuildTopTable$ implements Serializable
{
    public static BuildTopTable$ MODULE$;

    static
    {
        new BuildTopTable$();
    }

    private BuildTopTable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "BuildTopTable";
    }

    public BuildTopTable apply( final String opName, final String tableName, final CodeGenExpression countExpression,
            final Map<String,Variable> columnVariables, final Iterable<SortItem> sortItems, final CodeGenContext context )
    {
        return new BuildTopTable( opName, tableName, countExpression, columnVariables, sortItems, context );
    }

    public Option<Tuple5<String,String,CodeGenExpression,Map<String,Variable>,Iterable<SortItem>>> unapply( final BuildTopTable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple5( x$0.opName(), x$0.tableName(), x$0.countExpression(), x$0.columnVariables(), x$0.sortItems() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
