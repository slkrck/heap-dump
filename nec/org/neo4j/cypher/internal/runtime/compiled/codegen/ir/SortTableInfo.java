package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SortTableInfo implements Product, Serializable
{
    private final String tableName;
    private final Map<String,FieldAndVariableInfo> fieldToVariableInfo;
    private final Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo;
    private final SortTableDescriptor tableDescriptor;

    public SortTableInfo( final String tableName, final Map<String,FieldAndVariableInfo> fieldToVariableInfo,
            final Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo, final SortTableDescriptor tableDescriptor )
    {
        this.tableName = tableName;
        this.fieldToVariableInfo = fieldToVariableInfo;
        this.outgoingVariableNameToVariableInfo = outgoingVariableNameToVariableInfo;
        this.tableDescriptor = tableDescriptor;
        Product.$init$( this );
    }

    public static Option<Tuple4<String,Map<String,FieldAndVariableInfo>,Map<String,FieldAndVariableInfo>,SortTableDescriptor>> unapply(
            final SortTableInfo x$0 )
    {
        return SortTableInfo$.MODULE$.unapply( var0 );
    }

    public static SortTableInfo apply( final String tableName, final Map<String,FieldAndVariableInfo> fieldToVariableInfo,
            final Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo, final SortTableDescriptor tableDescriptor )
    {
        return SortTableInfo$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<String,Map<String,FieldAndVariableInfo>,Map<String,FieldAndVariableInfo>,SortTableDescriptor>,SortTableInfo> tupled()
    {
        return SortTableInfo$.MODULE$.tupled();
    }

    public static Function1<String,Function1<Map<String,FieldAndVariableInfo>,Function1<Map<String,FieldAndVariableInfo>,Function1<SortTableDescriptor,SortTableInfo>>>> curried()
    {
        return SortTableInfo$.MODULE$.curried();
    }

    public String tableName()
    {
        return this.tableName;
    }

    public Map<String,FieldAndVariableInfo> fieldToVariableInfo()
    {
        return this.fieldToVariableInfo;
    }

    public Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo()
    {
        return this.outgoingVariableNameToVariableInfo;
    }

    public SortTableDescriptor tableDescriptor()
    {
        return this.tableDescriptor;
    }

    public SortTableInfo copy( final String tableName, final Map<String,FieldAndVariableInfo> fieldToVariableInfo,
            final Map<String,FieldAndVariableInfo> outgoingVariableNameToVariableInfo, final SortTableDescriptor tableDescriptor )
    {
        return new SortTableInfo( tableName, fieldToVariableInfo, outgoingVariableNameToVariableInfo, tableDescriptor );
    }

    public String copy$default$1()
    {
        return this.tableName();
    }

    public Map<String,FieldAndVariableInfo> copy$default$2()
    {
        return this.fieldToVariableInfo();
    }

    public Map<String,FieldAndVariableInfo> copy$default$3()
    {
        return this.outgoingVariableNameToVariableInfo();
    }

    public SortTableDescriptor copy$default$4()
    {
        return this.tableDescriptor();
    }

    public String productPrefix()
    {
        return "SortTableInfo";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.tableName();
            break;
        case 1:
            var10000 = this.fieldToVariableInfo();
            break;
        case 2:
            var10000 = this.outgoingVariableNameToVariableInfo();
            break;
        case 3:
            var10000 = this.tableDescriptor();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SortTableInfo;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof SortTableInfo )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            SortTableInfo var4 = (SortTableInfo) x$1;
                            String var10000 = this.tableName();
                            String var5 = var4.tableName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            Map var9 = this.fieldToVariableInfo();
                            Map var6 = var4.fieldToVariableInfo();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            var9 = this.outgoingVariableNameToVariableInfo();
                            Map var7 = var4.outgoingVariableNameToVariableInfo();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label72;
                            }

                            SortTableDescriptor var10 = this.tableDescriptor();
                            SortTableDescriptor var8 = var4.tableDescriptor();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label63;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label81;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
