package org.neo4j.cypher.internal.runtime.compiled.codegen.ir;

import org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.LessThan$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SkipInstruction implements Instruction, Product, Serializable
{
    private final String opName;
    private final String variableName;
    private final Instruction action;
    private final CodeGenExpression numberToSkip;

    public SkipInstruction( final String opName, final String variableName, final Instruction action, final CodeGenExpression numberToSkip )
    {
        this.opName = opName;
        this.variableName = variableName;
        this.action = action;
        this.numberToSkip = numberToSkip;
        Instruction.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<String,String,Instruction,CodeGenExpression>> unapply( final SkipInstruction x$0 )
    {
        return SkipInstruction$.MODULE$.unapply( var0 );
    }

    public static SkipInstruction apply( final String opName, final String variableName, final Instruction action, final CodeGenExpression numberToSkip )
    {
        return SkipInstruction$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<String,String,Instruction,CodeGenExpression>,SkipInstruction> tupled()
    {
        return SkipInstruction$.MODULE$.tupled();
    }

    public static Function1<String,Function1<String,Function1<Instruction,Function1<CodeGenExpression,SkipInstruction>>>> curried()
    {
        return SkipInstruction$.MODULE$.curried();
    }

    public final Set<String> allOperatorIds()
    {
        return Instruction.allOperatorIds$( this );
    }

    public String opName()
    {
        return this.opName;
    }

    public String variableName()
    {
        return this.variableName;
    }

    public Instruction action()
    {
        return this.action;
    }

    public CodeGenExpression numberToSkip()
    {
        return this.numberToSkip;
    }

    public <E> void init( final MethodStructure<E> generator, final CodeGenContext context )
    {
        this.numberToSkip().init( generator, context );
        Object expression = generator.box( this.numberToSkip().generateExpression( generator, context ), this.numberToSkip().codeGenType( context ) );
        generator.declareCounter( this.variableName(), expression, "SKIP: Invalid input. Got a floating-point number. Must be a non-negative integer." );
        generator.ifStatement( generator.checkInteger( this.variableName(), LessThan$.MODULE$, 0L ), ( onTrue ) -> {
            $anonfun$init$1( onTrue );
            return BoxedUnit.UNIT;
        } );
        this.action().init( generator, context );
    }

    public <E> void body( final MethodStructure<E> generator, final CodeGenContext context )
    {
        generator.trace( this.opName(), generator.trace$default$2(), ( l1 ) -> {
            $anonfun$body$1( this, context, l1 );
            return BoxedUnit.UNIT;
        } );
    }

    public Seq<Instruction> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Instruction[]{this.action()}) ));
    }

    public Set<String> operatorId()
    {
        return (Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new String[]{this.opName()}) ));
    }

    public SkipInstruction copy( final String opName, final String variableName, final Instruction action, final CodeGenExpression numberToSkip )
    {
        return new SkipInstruction( opName, variableName, action, numberToSkip );
    }

    public String copy$default$1()
    {
        return this.opName();
    }

    public String copy$default$2()
    {
        return this.variableName();
    }

    public Instruction copy$default$3()
    {
        return this.action();
    }

    public CodeGenExpression copy$default$4()
    {
        return this.numberToSkip();
    }

    public String productPrefix()
    {
        return "SkipInstruction";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.opName();
            break;
        case 1:
            var10000 = this.variableName();
            break;
        case 2:
            var10000 = this.action();
            break;
        case 3:
            var10000 = this.numberToSkip();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SkipInstruction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof SkipInstruction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            SkipInstruction var4 = (SkipInstruction) x$1;
                            String var10000 = this.opName();
                            String var5 = var4.opName();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            var10000 = this.variableName();
                            String var6 = var4.variableName();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label72;
                            }

                            Instruction var9 = this.action();
                            Instruction var7 = var4.action();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label72;
                            }

                            CodeGenExpression var10 = this.numberToSkip();
                            CodeGenExpression var8 = var4.numberToSkip();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label63;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label81;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
