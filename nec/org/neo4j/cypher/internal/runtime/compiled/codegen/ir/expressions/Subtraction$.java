package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class Subtraction$ extends AbstractFunction2<CodeGenExpression,CodeGenExpression,Subtraction> implements Serializable
{
    public static Subtraction$ MODULE$;

    static
    {
        new Subtraction$();
    }

    private Subtraction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Subtraction";
    }

    public Subtraction apply( final CodeGenExpression lhs, final CodeGenExpression rhs )
    {
        return new Subtraction( lhs, rhs );
    }

    public Option<Tuple2<CodeGenExpression,CodeGenExpression>> unapply( final Subtraction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.lhs(), x$0.rhs() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
