package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions;

import org.neo4j.cypher.internal.runtime.compiled.codegen.Variable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class RelationshipProjection$ extends AbstractFunction1<Variable,RelationshipProjection> implements Serializable
{
    public static RelationshipProjection$ MODULE$;

    static
    {
        new RelationshipProjection$();
    }

    private RelationshipProjection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipProjection";
    }

    public RelationshipProjection apply( final Variable relId )
    {
        return new RelationshipProjection( relId );
    }

    public Option<Variable> unapply( final RelationshipProjection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.relId() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
