package org.neo4j.cypher.internal.runtime.compiled.codegen;

public final class CodeGenContext$
{
    public static CodeGenContext$ MODULE$;

    static
    {
        new CodeGenContext$();
    }

    private CodeGenContext$()
    {
        MODULE$ = this;
    }

    public Namer $lessinit$greater$default$3()
    {
        return Namer$.MODULE$.apply();
    }
}
