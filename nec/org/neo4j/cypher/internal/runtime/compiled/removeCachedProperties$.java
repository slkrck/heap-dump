package org.neo4j.cypher.internal.runtime.compiled;

import org.neo4j.cypher.internal.logical.plans.IndexLeafPlan;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.v4_0.ast.ASTAnnotationMap;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.expressions.CachedProperty;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.cypher.internal.v4_0.expressions.Property;
import org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName;
import org.neo4j.cypher.internal.v4_0.expressions.Variable;
import org.neo4j.cypher.internal.v4_0.util.attribution.SameId;
import org.neo4j.cypher.internal.v4_0.util.topDown.;
import scala.Function1;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Map;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

public final class removeCachedProperties$ implements Product, Serializable
{
    public static removeCachedProperties$ MODULE$;

    static
    {
        new removeCachedProperties$();
    }

    private removeCachedProperties$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public Tuple2<LogicalPlan,SemanticTable> apply( final LogicalPlan plan, final SemanticTable semanticTable )
    {
        LogicalPlan rewrittenPlan;
        SemanticTable var8;
        label17:
        {
            label16:
            {
                ObjectRef currentTypes = ObjectRef.create( semanticTable.types() );
                Function1 rewriter = .MODULE$.apply( org.neo4j.cypher.internal.v4_0.util.Rewriter..MODULE$.lift( new Serializable( currentTypes )
            {
                public static final long serialVersionUID = 0L;
                private final ObjectRef currentTypes$1;

                public
                {
                    this.currentTypes$1 = currentTypes$1;
                }

                public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                {
                    Object var3;
                    if ( x1 instanceof CachedProperty )
                    {
                        CachedProperty var5 = (CachedProperty) x1;
                        LogicalVariable varUsed = var5.entityVariable();
                        PropertyKeyName propertyKeyName = var5.propertyKey();
                        var3 = new Property( varUsed, propertyKeyName, var5.position() );
                    }
                    else
                    {
                        if ( x1 instanceof IndexLeafPlan )
                        {
                            IndexLeafPlan var8 = (IndexLeafPlan) x1;
                            if ( var8.cachedProperties().nonEmpty() )
                            {
                                Map projections = ((TraversableOnce) var8.cachedProperties().map( ( cachedProperty ) -> {
                                    return scala.Predef.ArrowAssoc..
                                    MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( cachedProperty.propertyAccessString() ), new Property(
                                            new Variable( cachedProperty.entityName(),
                                                    org.neo4j.cypher.internal.v4_0.util.InputPosition..MODULE$.NONE() ), cachedProperty.propertyKey(), org.neo4j.cypher.internal.v4_0.util.InputPosition..
                                    MODULE$.NONE()));
                                }, scala.collection.Seq..MODULE$.canBuildFrom())).toMap( scala.Predef..MODULE$.$conforms());
                                projections.values().foreach( ( prop ) -> {
                                    $anonfun$applyOrElse$2( this, prop );
                                    return BoxedUnit.UNIT;
                                } );
                                IndexLeafPlan newIndexPlan = var8.copyWithoutGettingValues();
                                var3 = new Projection( newIndexPlan, projections, new SameId( var8.id() ) );
                                return var3;
                            }
                        }

                        var3 = var2.apply( x1 );
                    }

                    return var3;
                }

                public final boolean isDefinedAt( final Object x1 )
                {
                    boolean var2;
                    if ( x1 instanceof CachedProperty )
                    {
                        var2 = true;
                    }
                    else
                    {
                        if ( x1 instanceof IndexLeafPlan )
                        {
                            IndexLeafPlan var4 = (IndexLeafPlan) x1;
                            if ( var4.cachedProperties().nonEmpty() )
                            {
                                var2 = true;
                                return var2;
                            }
                        }

                        var2 = false;
                    }

                    return var2;
                }
            } ), .MODULE$.apply$default$2());
                rewrittenPlan = (LogicalPlan) rewriter.apply( plan );
                ASTAnnotationMap var10000 = (ASTAnnotationMap) currentTypes.elem;
                ASTAnnotationMap var7 = semanticTable.types();
                if ( var10000 == null )
                {
                    if ( var7 == null )
                    {
                        break label16;
                    }
                }
                else if ( var10000.equals( var7 ) )
                {
                    break label16;
                }

                var8 = semanticTable.copy( (ASTAnnotationMap) currentTypes.elem, semanticTable.copy$default$2(), semanticTable.copy$default$3(),
                        semanticTable.copy$default$4(), semanticTable.copy$default$5() );
                break label17;
            }

            var8 = semanticTable;
        }

        SemanticTable newSemanticTable = var8;
        return new Tuple2( rewrittenPlan, newSemanticTable );
    }

    public String productPrefix()
    {
        return "removeCachedProperties";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof removeCachedProperties$;
    }

    public int hashCode()
    {
        return -531833703;
    }

    public String toString()
    {
        return "removeCachedProperties";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
