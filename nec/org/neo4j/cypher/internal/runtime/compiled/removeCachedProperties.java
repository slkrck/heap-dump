package org.neo4j.cypher.internal.runtime.compiled;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class removeCachedProperties
{
    public static String toString()
    {
        return removeCachedProperties$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return removeCachedProperties$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return removeCachedProperties$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return removeCachedProperties$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return removeCachedProperties$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return removeCachedProperties$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return removeCachedProperties$.MODULE$.productPrefix();
    }

    public static Tuple2<LogicalPlan,SemanticTable> apply( final LogicalPlan plan, final SemanticTable semanticTable )
    {
        return removeCachedProperties$.MODULE$.apply( var0, var1 );
    }
}
