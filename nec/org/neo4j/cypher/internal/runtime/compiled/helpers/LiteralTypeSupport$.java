package org.neo4j.cypher.internal.runtime.compiled.helpers;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.AnyValueType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.BoolType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.FloatType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ListReferenceType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.LongType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ReferenceType$;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.ValueType$;
import org.neo4j.cypher.internal.v4_0.util.symbols.BooleanType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.FloatType;
import org.neo4j.cypher.internal.v4_0.util.symbols.IntegerType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import org.neo4j.values.AnyValue;
import scala.MatchError;
import scala.Option;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.TraversableOnce;

public final class LiteralTypeSupport$
{
    public static LiteralTypeSupport$ MODULE$;

    static
    {
        new LiteralTypeSupport$();
    }

    private LiteralTypeSupport$()
    {
        MODULE$ = this;
    }

    public CypherType deriveCypherType( final Object obj )
    {
        Object var2;
        if ( obj instanceof String )
        {
            var2 = (CypherType).MODULE$.CTString();
        }
        else if ( obj instanceof Character )
        {
            var2 = (CypherType).MODULE$.CTString();
        }
        else
        {
            boolean var3;
            if ( obj instanceof Integer )
            {
                var3 = true;
            }
            else if ( obj instanceof Long )
            {
                var3 = true;
            }
            else if ( obj instanceof Integer )
            {
                var3 = true;
            }
            else if ( obj instanceof Long )
            {
                var3 = true;
            }
            else if ( obj instanceof Short )
            {
                var3 = true;
            }
            else if ( obj instanceof Byte )
            {
                var3 = true;
            }
            else
            {
                var3 = false;
            }

            if ( var3 )
            {
                var2 = .MODULE$.CTInteger();
            }
            else if ( obj instanceof Number )
            {
                var2 = .MODULE$.CTFloat();
            }
            else if ( obj instanceof Boolean )
            {
                var2 = .MODULE$.CTBoolean();
            }
            else
            {
                if ( obj instanceof AnyValue )
                {
                    AnyValue var5 = (AnyValue) obj;
                    Option var6 = org.neo4j.cypher.internal.runtime.interpreted.IsMap..MODULE$.unapply( var5 );
                    if ( !var6.isEmpty() )
                    {
                        var2 = .MODULE$.CTMap();
                        return (CypherType) var2;
                    }
                }

                Option var7 = org.neo4j.cypher.internal.compiler.helpers.IsList..MODULE$.unapply( obj );
                if ( !var7.isEmpty() )
                {
                    Iterable coll = (Iterable) var7.get();
                    if ( coll.isEmpty() )
                    {
                        var2 = .MODULE$.CTList(.MODULE$.CTAny());
                        return (CypherType) var2;
                    }
                }

                Option var9 = org.neo4j.cypher.internal.compiler.helpers.IsList..MODULE$.unapply( obj );
                if ( !var9.isEmpty() )
                {
                    Iterable coll = (Iterable) var9.get();
                    var2 = .MODULE$.CTList( (CypherType) ((TraversableOnce) coll.map( ( objx ) -> {
                    return MODULE$.deriveCypherType( objx );
                }, scala.collection.Iterable..MODULE$.canBuildFrom()) ).reduce( ( x$1, x$2 ) -> {
                    return x$1.leastUpperBound( x$2 );
                } ));
                }
                else
                {
                    var2 = .MODULE$.CTAny();
                }
            }
        }

        return (CypherType) var2;
    }

    public CypherCodeGenType deriveCodeGenType( final Object obj )
    {
        return this.deriveCodeGenType( this.deriveCypherType( obj ) );
    }

    public CypherCodeGenType deriveCodeGenType( final CypherType ct )
    {
        Option var4 = org.neo4j.cypher.internal.v4_0.util.symbols.ListType..MODULE$.unapply( ct );
        CypherCodeGenType var2;
        if ( !var4.isEmpty() )
        {
            CypherType innerCt = (CypherType) var4.get();
            var2 = new CypherCodeGenType(.MODULE$.CTList( innerCt ), new ListReferenceType( this.toRepresentationType( innerCt ) ));
        }
        else
        {
            var2 = new CypherCodeGenType( ct, this.toRepresentationType( ct ) );
        }

        return var2;
    }

    private RepresentationType toRepresentationType( final CypherType ct )
    {
        Object var2;
        label90:
        {
            IntegerType var10000 = .MODULE$.CTInteger();
            if ( var10000 == null )
            {
                if ( ct == null )
                {
                    break label90;
                }
            }
            else if ( var10000.equals( ct ) )
            {
                break label90;
            }

            label91:
            {
                FloatType var9 = .MODULE$.CTFloat();
                if ( var9 == null )
                {
                    if ( ct != null )
                    {
                        break label91;
                    }
                }
                else if ( !var9.equals( ct ) )
                {
                    break label91;
                }

                var2 = FloatType$.MODULE$;
                return (RepresentationType) var2;
            }

            label92:
            {
                BooleanType var10 = .MODULE$.CTBoolean();
                if ( var10 == null )
                {
                    if ( ct != null )
                    {
                        break label92;
                    }
                }
                else if ( !var10.equals( ct ) )
                {
                    break label92;
                }

                var2 = BoolType$.MODULE$;
                return (RepresentationType) var2;
            }

            label93:
            {
                NodeType var11 = .MODULE$.CTNode();
                if ( var11 == null )
                {
                    if ( ct == null )
                    {
                        break label93;
                    }
                }
                else if ( var11.equals( ct ) )
                {
                    break label93;
                }

                label94:
                {
                    RelationshipType var12 = .MODULE$.CTRelationship();
                    if ( var12 == null )
                    {
                        if ( ct == null )
                        {
                            break label94;
                        }
                    }
                    else if ( var12.equals( ct ) )
                    {
                        break label94;
                    }

                    var2 = ValueType$.MODULE$;
                    return (RepresentationType) var2;
                }

                var2 = LongType$.MODULE$;
                return (RepresentationType) var2;
            }

            var2 = LongType$.MODULE$;
            return (RepresentationType) var2;
        }

        var2 = LongType$.MODULE$;
        return (RepresentationType) var2;
    }

    public RepresentationType selectRepresentationType( final CypherType ct, final Seq<RepresentationType> reprTypes )
    {
        return (RepresentationType) reprTypes.reduce( ( x0$1, x1$1 ) -> {
            Tuple2 var6 = new Tuple2( x0$1, x1$1 );
            Object var3;
            if ( var6 != null )
            {
                RepresentationType var7 = (RepresentationType) var6._1();
                if ( ReferenceType$.MODULE$.equals( var7 ) )
                {
                    var3 = ReferenceType$.MODULE$;
                    return (RepresentationType) var3;
                }
            }

            if ( var6 != null )
            {
                RepresentationType var8 = (RepresentationType) var6._2();
                if ( ReferenceType$.MODULE$.equals( var8 ) )
                {
                    var3 = ReferenceType$.MODULE$;
                    return (RepresentationType) var3;
                }
            }

            if ( var6 != null )
            {
                RepresentationType var9 = (RepresentationType) var6._1();
                if ( AnyValueType$.MODULE$.equals( var9 ) )
                {
                    var3 = AnyValueType$.MODULE$;
                    return (RepresentationType) var3;
                }
            }

            if ( var6 != null )
            {
                RepresentationType var10 = (RepresentationType) var6._2();
                if ( AnyValueType$.MODULE$.equals( var10 ) )
                {
                    var3 = AnyValueType$.MODULE$;
                    return (RepresentationType) var3;
                }
            }

            if ( var6 != null )
            {
                RepresentationType var11 = (RepresentationType) var6._1();
                if ( ValueType$.MODULE$.equals( var11 ) )
                {
                    var3 = ValueType$.MODULE$;
                    return (RepresentationType) var3;
                }
            }

            if ( var6 != null )
            {
                RepresentationType var12 = (RepresentationType) var6._2();
                if ( ValueType$.MODULE$.equals( var12 ) )
                {
                    var3 = ValueType$.MODULE$;
                    return (RepresentationType) var3;
                }
            }

            if ( var6 != null )
            {
                RepresentationType var13 = (RepresentationType) var6._1();
                RepresentationType var14 = (RepresentationType) var6._2();
                if ( LongType$.MODULE$.equals( var13 ) && LongType$.MODULE$.equals( var14 ) )
                {
                    boolean var5;
                    label97:
                    {
                        label140:
                        {
                            NodeType var22 = .MODULE$.CTNode();
                            if ( var22 == null )
                            {
                                if ( ct == null )
                                {
                                    break label140;
                                }
                            }
                            else if ( var22.equals( ct ) )
                            {
                                break label140;
                            }

                            label90:
                            {
                                RelationshipType var23 = .MODULE$.CTRelationship();
                                if ( var23 == null )
                                {
                                    if ( ct != null )
                                    {
                                        break label90;
                                    }
                                }
                                else if ( !var23.equals( ct ) )
                                {
                                    break label90;
                                }

                                var5 = true;
                                break label97;
                            }

                            label83:
                            {
                                IntegerType var24 = .MODULE$.CTInteger();
                                if ( var24 == null )
                                {
                                    if ( ct == null )
                                    {
                                        break label83;
                                    }
                                }
                                else if ( var24.equals( ct ) )
                                {
                                    break label83;
                                }

                                var5 = false;
                                break label97;
                            }

                            var5 = true;
                            break label97;
                        }

                        var5 = true;
                    }

                    Object var4;
                    if ( var5 )
                    {
                        var4 = LongType$.MODULE$;
                    }
                    else
                    {
                        var4 = ReferenceType$.MODULE$;
                    }

                    var3 = var4;
                    return (RepresentationType) var3;
                }
            }

            if ( var6 == null )
            {
                throw new MatchError( var6 );
            }
            else
            {
                RepresentationType var10000;
                label105:
                {
                    label104:
                    {
                        RepresentationType t1 = (RepresentationType) var6._1();
                        RepresentationType t2 = (RepresentationType) var6._2();
                        if ( t1 == null )
                        {
                            if ( t2 != null )
                            {
                                break label104;
                            }
                        }
                        else if ( !t1.equals( t2 ) )
                        {
                            break label104;
                        }

                        var10000 = t1;
                        break label105;
                    }

                    var10000 = MODULE$.toRepresentationType( ct );
                }

                var3 = var10000;
                return (RepresentationType) var3;
            }
        } );
    }
}
