package org.neo4j.cypher.internal.runtime.compiled.helpers;

import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType;
import org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.RepresentationType;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class LiteralTypeSupport
{
    public static RepresentationType selectRepresentationType( final CypherType ct, final Seq<RepresentationType> reprTypes )
    {
        return LiteralTypeSupport$.MODULE$.selectRepresentationType( var0, var1 );
    }

    public static CypherCodeGenType deriveCodeGenType( final CypherType ct )
    {
        return LiteralTypeSupport$.MODULE$.deriveCodeGenType( var0 );
    }

    public static CypherCodeGenType deriveCodeGenType( final Object obj )
    {
        return LiteralTypeSupport$.MODULE$.deriveCodeGenType( var0 );
    }

    public static CypherType deriveCypherType( final Object obj )
    {
        return LiteralTypeSupport$.MODULE$.deriveCypherType( var0 );
    }
}
