package org.neo4j.cypher.internal;

import java.time.Clock;

import org.neo4j.cypher.CypherInterpretedPipesFallbackOption;
import org.neo4j.cypher.CypherOperatorEngineOption;
import org.neo4j.cypher.internal.executionplan.GeneratedQuery;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure;
import org.neo4j.internal.kernel.api.SchemaRead;
import org.neo4j.logging.Log;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple12;
import scala.None.;
import scala.collection.immutable.Set;
import scala.runtime.AbstractFunction12;
import scala.runtime.BoxesRunTime;

public final class EnterpriseRuntimeContext$ extends
        AbstractFunction12<TokenContext,SchemaRead,CodeStructure<GeneratedQuery>,Log,Clock,Set<String>,CypherRuntimeConfiguration,RuntimeEnvironment,Object,Object,CypherOperatorEngineOption,CypherInterpretedPipesFallbackOption,EnterpriseRuntimeContext>
        implements Serializable
{
    public static EnterpriseRuntimeContext$ MODULE$;

    static
    {
        new EnterpriseRuntimeContext$();
    }

    private EnterpriseRuntimeContext$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "EnterpriseRuntimeContext";
    }

    public EnterpriseRuntimeContext apply( final TokenContext tokenContext, final SchemaRead schemaRead, final CodeStructure<GeneratedQuery> codeStructure,
            final Log log, final Clock clock, final Set<String> debugOptions, final CypherRuntimeConfiguration config,
            final RuntimeEnvironment runtimeEnvironment, final boolean compileExpressions, final boolean materializedEntitiesMode,
            final CypherOperatorEngineOption operatorEngine, final CypherInterpretedPipesFallbackOption interpretedPipesFallback )
    {
        return new EnterpriseRuntimeContext( tokenContext, schemaRead, codeStructure, log, clock, debugOptions, config, runtimeEnvironment, compileExpressions,
                materializedEntitiesMode, operatorEngine, interpretedPipesFallback );
    }

    public Option<Tuple12<TokenContext,SchemaRead,CodeStructure<GeneratedQuery>,Log,Clock,Set<String>,CypherRuntimeConfiguration,RuntimeEnvironment,Object,Object,CypherOperatorEngineOption,CypherInterpretedPipesFallbackOption>> unapply(
            final EnterpriseRuntimeContext x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple12( x$0.tokenContext(), x$0.schemaRead(), x$0.codeStructure(), x$0.log(), x$0.clock(), x$0.debugOptions(), x$0.config(),
                    x$0.runtimeEnvironment(), BoxesRunTime.boxToBoolean( x$0.compileExpressions() ),
                    BoxesRunTime.boxToBoolean( x$0.materializedEntitiesMode() ), x$0.operatorEngine(), x$0.interpretedPipesFallback() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
