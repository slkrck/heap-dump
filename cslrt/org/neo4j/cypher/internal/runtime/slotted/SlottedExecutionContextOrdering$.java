package org.neo4j.cypher.internal.runtime.slotted;

import java.util.Comparator;

import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.MatchError;
import scala.Some;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.math.Ordering;
import scala.math.PartialOrdering;
import scala.math.Ordering.Ops;

public final class SlottedExecutionContextOrdering$
{
    public static SlottedExecutionContextOrdering$ MODULE$;

    static
    {
        new SlottedExecutionContextOrdering$();
    }

    private SlottedExecutionContextOrdering$()
    {
        MODULE$ = this;
    }

    public Ordering<ExecutionContext> comparator( final ColumnOrder order )
    {
        boolean var3 = false;
        LongSlot var4 = null;
        Slot var5 = order.slot();
        Ordering var2;
        if ( var5 instanceof LongSlot )
        {
            var3 = true;
            var4 = (LongSlot) var5;
            int offset = var4.offset();
            boolean var7 = var4.nullable();
            if ( var7 )
            {
                var2 = new Ordering<ExecutionContext>( order, offset )
                {
                    private final ColumnOrder order$1;
                    private final int offset$1;

                    public
                    {
                        this.order$1 = order$1;
                        this.offset$1 = offset$1;
                        PartialOrdering.$init$( this );
                        Ordering.$init$( this );
                    }

                    public Some tryCompare( final Object x, final Object y )
                    {
                        return Ordering.tryCompare$( this, x, y );
                    }

                    public boolean lteq( final Object x, final Object y )
                    {
                        return Ordering.lteq$( this, x, y );
                    }

                    public boolean gteq( final Object x, final Object y )
                    {
                        return Ordering.gteq$( this, x, y );
                    }

                    public boolean lt( final Object x, final Object y )
                    {
                        return Ordering.lt$( this, x, y );
                    }

                    public boolean gt( final Object x, final Object y )
                    {
                        return Ordering.gt$( this, x, y );
                    }

                    public boolean equiv( final Object x, final Object y )
                    {
                        return Ordering.equiv$( this, x, y );
                    }

                    public Object max( final Object x, final Object y )
                    {
                        return Ordering.max$( this, x, y );
                    }

                    public Object min( final Object x, final Object y )
                    {
                        return Ordering.min$( this, x, y );
                    }

                    public Ordering<ExecutionContext> reverse()
                    {
                        return Ordering.reverse$( this );
                    }

                    public <U> Ordering<U> on( final Function1<U,ExecutionContext> f )
                    {
                        return Ordering.on$( this, f );
                    }

                    public Ops mkOrderingOps( final Object lhs )
                    {
                        return Ordering.mkOrderingOps$( this, lhs );
                    }

                    public int compare( final ExecutionContext a, final ExecutionContext b )
                    {
                        long aVal = a.getLongAt( this.offset$1 );
                        long bVal = b.getLongAt( this.offset$1 );
                        return this.order$1.compareNullableLongs( aVal, bVal );
                    }
                };
                return var2;
            }
        }

        if ( var3 )
        {
            int offset = var4.offset();
            boolean var9 = var4.nullable();
            if ( !var9 )
            {
                var2 = new Ordering<ExecutionContext>( order, offset )
                {
                    private final ColumnOrder order$1;
                    private final int offset$2;

                    public
                    {
                        this.order$1 = order$1;
                        this.offset$2 = offset$2;
                        PartialOrdering.$init$( this );
                        Ordering.$init$( this );
                    }

                    public Some tryCompare( final Object x, final Object y )
                    {
                        return Ordering.tryCompare$( this, x, y );
                    }

                    public boolean lteq( final Object x, final Object y )
                    {
                        return Ordering.lteq$( this, x, y );
                    }

                    public boolean gteq( final Object x, final Object y )
                    {
                        return Ordering.gteq$( this, x, y );
                    }

                    public boolean lt( final Object x, final Object y )
                    {
                        return Ordering.lt$( this, x, y );
                    }

                    public boolean gt( final Object x, final Object y )
                    {
                        return Ordering.gt$( this, x, y );
                    }

                    public boolean equiv( final Object x, final Object y )
                    {
                        return Ordering.equiv$( this, x, y );
                    }

                    public Object max( final Object x, final Object y )
                    {
                        return Ordering.max$( this, x, y );
                    }

                    public Object min( final Object x, final Object y )
                    {
                        return Ordering.min$( this, x, y );
                    }

                    public Ordering<ExecutionContext> reverse()
                    {
                        return Ordering.reverse$( this );
                    }

                    public <U> Ordering<U> on( final Function1<U,ExecutionContext> f )
                    {
                        return Ordering.on$( this, f );
                    }

                    public Ops mkOrderingOps( final Object lhs )
                    {
                        return Ordering.mkOrderingOps$( this, lhs );
                    }

                    public int compare( final ExecutionContext a, final ExecutionContext b )
                    {
                        long aVal = a.getLongAt( this.offset$2 );
                        long bVal = b.getLongAt( this.offset$2 );
                        return this.order$1.compareLongs( aVal, bVal );
                    }
                };
                return var2;
            }
        }

        if ( !(var5 instanceof RefSlot) )
        {
            throw new MatchError( var5 );
        }
        else
        {
            RefSlot var10 = (RefSlot) var5;
            int offset = var10.offset();
            var2 = new Ordering<ExecutionContext>( order, offset )
            {
                private final ColumnOrder order$1;
                private final int offset$3;

                public
                {
                    this.order$1 = order$1;
                    this.offset$3 = offset$3;
                    PartialOrdering.$init$( this );
                    Ordering.$init$( this );
                }

                public Some tryCompare( final Object x, final Object y )
                {
                    return Ordering.tryCompare$( this, x, y );
                }

                public boolean lteq( final Object x, final Object y )
                {
                    return Ordering.lteq$( this, x, y );
                }

                public boolean gteq( final Object x, final Object y )
                {
                    return Ordering.gteq$( this, x, y );
                }

                public boolean lt( final Object x, final Object y )
                {
                    return Ordering.lt$( this, x, y );
                }

                public boolean gt( final Object x, final Object y )
                {
                    return Ordering.gt$( this, x, y );
                }

                public boolean equiv( final Object x, final Object y )
                {
                    return Ordering.equiv$( this, x, y );
                }

                public Object max( final Object x, final Object y )
                {
                    return Ordering.max$( this, x, y );
                }

                public Object min( final Object x, final Object y )
                {
                    return Ordering.min$( this, x, y );
                }

                public Ordering<ExecutionContext> reverse()
                {
                    return Ordering.reverse$( this );
                }

                public <U> Ordering<U> on( final Function1<U,ExecutionContext> f )
                {
                    return Ordering.on$( this, f );
                }

                public Ops mkOrderingOps( final Object lhs )
                {
                    return Ordering.mkOrderingOps$( this, lhs );
                }

                public int compare( final ExecutionContext a, final ExecutionContext b )
                {
                    AnyValue aVal = a.getRefAt( this.offset$3 );
                    AnyValue bVal = b.getRefAt( this.offset$3 );
                    return this.order$1.compareValues( aVal, bVal );
                }
            };
            return var2;
        }
    }

    public Comparator<ExecutionContext> asComparator( final Seq<ColumnOrder> orderBy )
    {
        return (Comparator) ((TraversableOnce) orderBy.map( ( order ) -> {
            return MODULE$.comparator( order );
        },.MODULE$.canBuildFrom())).reduceLeft( ( a, b ) -> {
        return a.thenComparing( b );
    } );
    }
}
