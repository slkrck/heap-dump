package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.InCheckContainer;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Some;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SlottedQueryState extends QueryState
{
    public SlottedQueryState( final QueryContext query, final ExternalCSVResource resources, final AnyValue[] params, final ExpressionCursors cursors,
            final IndexReadSession[] queryIndexes, final AnyValue[] expressionVariables, final QuerySubscriber subscriber,
            final QueryMemoryTracker memoryTracker, final PipeDecorator decorator, final Option<ExecutionContext> initialContext,
            final SingleThreadedLRUCache<Object,InCheckContainer> cachedIn, final boolean lenientCreateRelationship, final boolean prePopulateResults,
            final InputDataStream input )
    {
        super( query, resources, params, cursors, queryIndexes, expressionVariables, subscriber, memoryTracker, decorator, initialContext, cachedIn,
                lenientCreateRelationship, prePopulateResults, input );
    }

    public static InputDataStream $lessinit$greater$default$14()
    {
        return SlottedQueryState$.MODULE$.$lessinit$greater$default$14();
    }

    public static boolean $lessinit$greater$default$13()
    {
        return SlottedQueryState$.MODULE$.$lessinit$greater$default$13();
    }

    public static boolean $lessinit$greater$default$12()
    {
        return SlottedQueryState$.MODULE$.$lessinit$greater$default$12();
    }

    public static SingleThreadedLRUCache<Object,InCheckContainer> $lessinit$greater$default$11()
    {
        return SlottedQueryState$.MODULE$.$lessinit$greater$default$11();
    }

    public static Option<ExecutionContext> $lessinit$greater$default$10()
    {
        return SlottedQueryState$.MODULE$.$lessinit$greater$default$10();
    }

    public static PipeDecorator $lessinit$greater$default$9()
    {
        return SlottedQueryState$.MODULE$.$lessinit$greater$default$9();
    }

    public SlottedQueryState withDecorator( final PipeDecorator decorator )
    {
        return new SlottedQueryState( super.query(), super.resources(), super.params(), super.cursors(), super.queryIndexes(), super.expressionVariables(),
                super.subscriber(), super.memoryTracker(), decorator, super.initialContext(), super.cachedIn(), super.lenientCreateRelationship(),
                super.prePopulateResults(), super.input() );
    }

    public SlottedQueryState withInitialContext( final ExecutionContext initialContext )
    {
        return new SlottedQueryState( super.query(), super.resources(), super.params(), super.cursors(), super.queryIndexes(), super.expressionVariables(),
                super.subscriber(), super.memoryTracker(), super.decorator(), new Some( initialContext ), super.cachedIn(), super.lenientCreateRelationship(),
                super.prePopulateResults(), super.input() );
    }

    public SlottedQueryState withQueryContext( final QueryContext query )
    {
        return new SlottedQueryState( query, super.resources(), super.params(), super.cursors(), super.queryIndexes(), super.expressionVariables(),
                super.subscriber(), super.memoryTracker(), super.decorator(), super.initialContext(), super.cachedIn(), super.lenientCreateRelationship(),
                super.prePopulateResults(), super.input() );
    }
}
