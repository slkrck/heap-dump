package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.EntityById;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ResourceLinenumber;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.exceptions.InternalException;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.NodeReference;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipReference;
import org.neo4j.values.virtual.RelationshipValue;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.IndexedSeqLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Map;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.StringBuilder;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedExecutionContext implements ExecutionContext, Product, Serializable
{
    private final SlotConfiguration slots;
    private final long[] longs;
    private final AnyValue[] refs;
    private Option<ResourceLinenumber> org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber;

    {
        throw new InternalException( "Tried using a slotted context as a map" );
    }

    public SlottedExecutionContext( final SlotConfiguration slots )
    {
        this.slots = slots;
        ExecutionContext.$init$( this );
        Product.$init$( this );
        this.longs = new long[slots.numberOfLongs()];
        this.refs = new AnyValue[slots.numberOfReferences()];
    }

    public static Option<SlotConfiguration> unapply( final SlottedExecutionContext x$0 )
    {
        return SlottedExecutionContext$.MODULE$.unapply( var0 );
    }

    public static SlottedExecutionContext apply( final SlotConfiguration slots )
    {
        return SlottedExecutionContext$.MODULE$.apply( var0 );
    }

    public static boolean DEBUG()
    {
        return SlottedExecutionContext$.MODULE$.DEBUG();
    }

    public static SlottedExecutionContext empty()
    {
        return SlottedExecutionContext$.MODULE$.empty();
    }

    public void setLinenumber( final String file, final long line, final boolean last )
    {
        ExecutionContext.setLinenumber$( this, file, line, last );
    }

    public Option<ResourceLinenumber> getLinenumber()
    {
        return ExecutionContext.getLinenumber$( this );
    }

    public void setLinenumber( final Option<ResourceLinenumber> line )
    {
        ExecutionContext.setLinenumber$( this, line );
    }

    public boolean setLinenumber$default$3()
    {
        return ExecutionContext.setLinenumber$default$3$( this );
    }

    public Option<ResourceLinenumber> org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber()
    {
        return this.org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber;
    }

    public void org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber_$eq( final Option<ResourceLinenumber> x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber = x$1;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public long[] longs()
    {
        return this.longs;
    }

    public AnyValue[] refs()
    {
        return this.refs;
    }

    public String toString()
    {
        Iterator iter = this.iterator();
        StringBuilder s = .MODULE$.StringBuilder().newBuilder();
        s.$plus$plus$eq( (new java.lang.StringBuilder( 31 )).append( "\nSlottedExecutionContext {\n    " ).append( this.slots() ).toString() );

        while ( iter.hasNext() )
        {
            Tuple2 slotValue = (Tuple2) iter.next();
            Object arg$macro$1 = slotValue._1();
            Object arg$macro$2 = slotValue._2();
            s.$plus$plus$eq( (new StringOps( "\n    %-40s = %s" )).format( scala.Predef..MODULE$.genericWrapArray( new Object[]{arg$macro$1, arg$macro$2} ) ));
        }

        s.$plus$plus$eq( "\n}\n" );
        return s.result();
    }

    public void copyTo( final ExecutionContext target, final int sourceLongOffset, final int sourceRefOffset, final int targetLongOffset,
            final int targetRefOffset )
    {
        if ( target instanceof SlottedExecutionContext )
        {
            SlottedExecutionContext var8 = (SlottedExecutionContext) target;
            SlotConfiguration otherPipeline = var8.slots();
            if ( this.slots().numberOfLongs() <= otherPipeline.numberOfLongs() && this.slots().numberOfReferences() <= otherPipeline.numberOfReferences() )
            {
                System.arraycopy( this.longs(), sourceLongOffset, var8.longs(), targetLongOffset, this.slots().numberOfLongs() - sourceLongOffset );
                System.arraycopy( this.refs(), sourceRefOffset, var8.refs(), targetRefOffset, this.slots().numberOfReferences() - sourceRefOffset );
                var8.setLinenumber( this.getLinenumber() );
                BoxedUnit var6 = BoxedUnit.UNIT;
            }
            else
            {
                throw new InternalException( (new StringOps( scala.Predef..MODULE$.augmentString( (new java.lang.StringBuilder( 156 )).append(
                        "A bug has occurred in the slotted runtime: The target slotted execution context cannot hold the data to copy\n               |From : " ).append(
                        this.slots() ).append( "\n               |To :   " ).append( otherPipeline ).toString() )) ).stripMargin());
            }
        }
        else
        {
            throw this.fail();
        }
    }

    public int copyTo$default$2()
    {
        return 0;
    }

    public int copyTo$default$3()
    {
        return 0;
    }

    public int copyTo$default$4()
    {
        return 0;
    }

    public int copyTo$default$5()
    {
        return 0;
    }

    public void copyFrom( final ExecutionContext input, final int nLongs, final int nRefs )
    {
        if ( nLongs <= this.slots().numberOfLongs() && nRefs <= this.slots().numberOfReferences() )
        {
            BoxedUnit var4;
            if ( input instanceof SlottedExecutionContext )
            {
                SlottedExecutionContext var6 = (SlottedExecutionContext) input;
                System.arraycopy( var6.longs(), 0, this.longs(), 0, nLongs );
                System.arraycopy( var6.refs(), 0, this.refs(), 0, nRefs );
                this.setLinenumber( var6.getLinenumber() );
                var4 = BoxedUnit.UNIT;
            }
            else
            {
                if ( !(input instanceof SlottedCompatible) )
                {
                    throw this.fail();
                }

                ((SlottedCompatible) input).copyToSlottedExecutionContext( this, nLongs, nRefs );
                var4 = BoxedUnit.UNIT;
            }
        }
        else
        {
            throw new InternalException( "A bug has occurred in the slotted runtime: The target slotted execution context cannot hold the data to copy." );
        }
    }

    public void setLongAt( final int offset, final long value )
    {
        this.longs()[offset] = value;
    }

    public long getLongAt( final int offset )
    {
        return this.longs()[offset];
    }

    public void setRefAt( final int offset, final AnyValue value )
    {
        this.refs()[offset] = value;
    }

    public AnyValue getRefAt( final int offset )
    {
        AnyValue value = this.refs()[offset];
        if ( SlottedExecutionContext$.MODULE$.DEBUG() && value == null )
        {
            throw new InternalException(
                    (new java.lang.StringBuilder( 46 )).append( "Reference value not initialised at offset " ).append( offset ).append( " in " ).append(
                            this ).toString() );
        }
        else
        {
            return value;
        }
    }

    public AnyValue getByName( final String name )
    {
        return (AnyValue) this.slots().maybeGetter( name ).map( ( g ) -> {
            return (AnyValue) g.apply( this );
        } ).getOrElse( () -> {
            throw new NotFoundException( (new java.lang.StringBuilder( 20 )).append( "Unknown variable `" ).append( name ).append( "`." ).toString() );
        } );
    }

    public int numberOfColumns()
    {
        return this.refs().length + this.longs().length;
    }

    public boolean containsName( final String name )
    {
        return this.slots().maybeGetter( name ).map( ( g ) -> {
            return (AnyValue) g.apply( this );
        } ).isDefined();
    }

    public void setCachedPropertyAt( final int offset, final Value value )
    {
        this.refs()[offset] = value;
    }

    public void invalidateCachedNodeProperties( final long node )
    {
        this.slots().foreachCachedSlot( ( x0$1 ) -> {
            $anonfun$invalidateCachedNodeProperties$1( this, node, x0$1 );
            return BoxedUnit.UNIT;
        } );
    }

    public void invalidateCachedRelationshipProperties( final long rel )
    {
        this.slots().foreachCachedSlot( ( x0$2 ) -> {
            $anonfun$invalidateCachedRelationshipProperties$1( this, rel, x0$2 );
            return BoxedUnit.UNIT;
        } );
    }

    public void setCachedProperty( final ASTCachedProperty key, final Value value )
    {
        this.setCachedPropertyAt( this.slots().getCachedPropertyOffsetFor( key ), value );
    }

    public Value getCachedPropertyAt( final int offset )
    {
        return (Value) this.refs()[offset];
    }

   private scala.runtime.Nothing.fail()

    public Value getCachedProperty( final ASTCachedProperty key )
    {
        throw this.fail();
    }

    public long estimatedHeapUsage()
    {
        long usage = (long) this.longs().length * 8L;

        for ( int i = 0; i < this.refs().length; ++i )
        {
            AnyValue ref = this.refs()[i];
            if ( ref != null )
            {
                usage += ref.estimatedHeapUsage();
            }
        }

        return usage;
    }

    public void set( final Seq<Tuple2<String,AnyValue>> newEntries )
    {
        newEntries.foreach( ( x0$3 ) -> {
            $anonfun$set$1( this, x0$3 );
            return BoxedUnit.UNIT;
        } );
    }

    public void set( final String key1, final AnyValue value1 )
    {
        this.setValue( key1, value1 );
    }

    public void set( final String key1, final AnyValue value1, final String key2, final AnyValue value2 )
    {
        this.setValue( key1, value1 );
        this.setValue( key2, value2 );
    }

    public void set( final String key1, final AnyValue value1, final String key2, final AnyValue value2, final String key3, final AnyValue value3 )
    {
        this.setValue( key1, value1 );
        this.setValue( key2, value2 );
        this.setValue( key3, value3 );
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1 )
    {
        SlottedExecutionContext newCopy = new SlottedExecutionContext( this.slots() );
        this.copyTo( newCopy, this.copyTo$default$2(), this.copyTo$default$3(), this.copyTo$default$4(), this.copyTo$default$5() );
        newCopy.setValue( key1, value1 );
        return newCopy;
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1, final String key2, final AnyValue value2 )
    {
        throw new UnsupportedOperationException( "Use ExecutionContextFactory.copyWith instead to get the correct slot configuration" );
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1, final String key2, final AnyValue value2, final String key3,
            final AnyValue value3 )
    {
        throw new UnsupportedOperationException( "Use ExecutionContextFactory.copyWith instead to get the correct slot configuration" );
    }

    public ExecutionContext copyWith( final Seq<Tuple2<String,AnyValue>> newEntries )
    {
        throw new UnsupportedOperationException( "Use ExecutionContextFactory.copyWith instead to get the correct slot configuration" );
    }

    private void setValue( final String key1, final AnyValue value1 )
    {
        ((Function2) this.slots().maybeSetter( key1 ).getOrElse( () -> {
            throw new InternalException(
                    (new java.lang.StringBuilder( 42 )).append( "Ouch, no suitable slot for key " ).append( key1 ).append( " = " ).append( value1 ).append(
                            "\nSlots: " ).append( this.slots() ).toString() );
        } )).apply( this, value1 );
    }

    public boolean isRefInitialized( final int offset )
    {
        return this.refs()[offset] != null;
    }

    public AnyValue getRefAtWithoutCheckingInitialized( final int offset )
    {
        return this.refs()[offset];
    }

    public void mergeWith( final ExecutionContext other, final EntityById entityById )
    {
        if ( other instanceof SlottedExecutionContext )
        {
            SlottedExecutionContext var5 = (SlottedExecutionContext) other;
            var5.slots().foreachSlot( ( x0$4 ) -> {
                $anonfun$mergeWith$1( this, other, entityById, var5, x0$4 );
                return BoxedUnit.UNIT;
            }, ( x0$5 ) -> {
                $anonfun$mergeWith$6( this, other, x0$5 );
                return BoxedUnit.UNIT;
            } );
            this.setLinenumber( var5.getLinenumber() );
            BoxedUnit var3 = BoxedUnit.UNIT;
        }
        else
        {
            throw new InternalException( "Well well, isn't this a delicate situation?" );
        }
    }

    public ExecutionContext createClone()
    {
        SlottedExecutionContext clone = new SlottedExecutionContext( this.slots() );
        this.copyTo( clone, this.copyTo$default$2(), this.copyTo$default$3(), this.copyTo$default$4(), this.copyTo$default$5() );
        return clone;
    }

    public Map<String,AnyValue> boundEntities( final Function1<Object,AnyValue> materializeNode, final Function1<Object,AnyValue> materializeRelationship )
    {
        scala.collection.mutable.Map entities = scala.collection.mutable.Map..MODULE$.empty();
        this.slots().foreachSlot( ( x0$6 ) -> {
            Object var5;
            if ( x0$6 != null )
            {
                String keyx = (String) x0$6._1();
                Slot var10 = (Slot) x0$6._2();
                if ( var10 instanceof RefSlot )
                {
                    RefSlot var11 = (RefSlot) var10;
                    int offsetxx = var11.offset();
                    Object var50;
                    if ( this.isRefInitialized( offsetxx ) )
                    {
                        AnyValue entity = this.getRefAtWithoutCheckingInitialized( offsetxx );
                        boolean var7;
                        if ( entity instanceof NodeValue )
                        {
                            var7 = true;
                        }
                        else if ( entity instanceof RelationshipValue )
                        {
                            var7 = true;
                        }
                        else
                        {
                            var7 = false;
                        }

                        Object var6;
                        if ( var7 )
                        {
                            var6 = entities.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                    keyx ), entity));
                        }
                        else if ( entity instanceof NodeReference )
                        {
                            NodeReference var15 = (NodeReference) entity;
                            var6 = entities.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                    keyx ), materializeNode.apply( BoxesRunTime.boxToLong( var15.id() ) )));
                        }
                        else if ( entity instanceof RelationshipReference )
                        {
                            RelationshipReference var16 = (RelationshipReference) entity;
                            var6 = entities.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                    keyx ), materializeRelationship.apply( BoxesRunTime.boxToLong( var16.id() ) )));
                        }
                        else
                        {
                            var6 = BoxedUnit.UNIT;
                        }

                        var50 = var6;
                    }
                    else
                    {
                        var50 = BoxedUnit.UNIT;
                    }

                    var5 = var50;
                    return var5;
                }
            }

            String keyxxx;
            int offsetxxxx;
            label172:
            {
                NodeType var10000;
                if ( x0$6 != null )
                {
                    keyxxx = (String) x0$6._1();
                    Slot var18 = (Slot) x0$6._2();
                    if ( var18 instanceof LongSlot )
                    {
                        LongSlot var19 = (LongSlot) var18;
                        offsetxxxx = var19.offset();
                        boolean var21 = var19.nullable();
                        CypherType var22 = var19.typ();
                        if ( !var21 )
                        {
                            var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                            if ( var10000 == null )
                            {
                                if ( var22 == null )
                                {
                                    break label172;
                                }
                            }
                            else if ( var10000.equals( var22 ) )
                            {
                                break label172;
                            }
                        }
                    }
                }

                RelationshipType var49;
                if ( x0$6 != null )
                {
                    String key = (String) x0$6._1();
                    Slot var25 = (Slot) x0$6._2();
                    if ( var25 instanceof LongSlot )
                    {
                        LongSlot var26 = (LongSlot) var25;
                        int offsetx = var26.offset();
                        boolean var28 = var26.nullable();
                        CypherType var29 = var26.typ();
                        if ( !var28 )
                        {
                            label173:
                            {
                                var49 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                if ( var49 == null )
                                {
                                    if ( var29 != null )
                                    {
                                        break label173;
                                    }
                                }
                                else if ( !var49.equals( var29 ) )
                                {
                                    break label173;
                                }

                                var5 = entities.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                        key ), materializeRelationship.apply( BoxesRunTime.boxToLong( this.getLongAt( offsetx ) ) )));
                                return var5;
                            }
                        }
                    }
                }

                String keyxx;
                int offsetxxx;
                label174:
                {
                    if ( x0$6 != null )
                    {
                        keyxx = (String) x0$6._1();
                        Slot var32 = (Slot) x0$6._2();
                        if ( var32 instanceof LongSlot )
                        {
                            LongSlot var33 = (LongSlot) var32;
                            offsetxxx = var33.offset();
                            boolean var35 = var33.nullable();
                            CypherType var36 = var33.typ();
                            if ( var35 )
                            {
                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                if ( var10000 == null )
                                {
                                    if ( var36 == null )
                                    {
                                        break label174;
                                    }
                                }
                                else if ( var10000.equals( var36 ) )
                                {
                                    break label174;
                                }
                            }
                        }
                    }

                    String keyxxxx;
                    int offset;
                    label175:
                    {
                        if ( x0$6 != null )
                        {
                            keyxxxx = (String) x0$6._1();
                            Slot var41 = (Slot) x0$6._2();
                            if ( var41 instanceof LongSlot )
                            {
                                LongSlot var42 = (LongSlot) var41;
                                offset = var42.offset();
                                boolean var44 = var42.nullable();
                                CypherType var45 = var42.typ();
                                if ( var44 )
                                {
                                    var49 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                    if ( var49 == null )
                                    {
                                        if ( var45 == null )
                                        {
                                            break label175;
                                        }
                                    }
                                    else if ( var49.equals( var45 ) )
                                    {
                                        break label175;
                                    }
                                }
                            }
                        }

                        var5 = BoxedUnit.UNIT;
                        return var5;
                    }

                    long entityId = this.getLongAt( offset );
                    var5 = entityId >= 0L ? entities.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                            keyxxxx ), materializeRelationship.apply( BoxesRunTime.boxToLong( this.getLongAt( offset ) ) ))) :BoxedUnit.UNIT;
                    return var5;
                }

                long entityIdx = this.getLongAt( offsetxxx );
                var5 = entityIdx >= 0L ? entities.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                        keyxx ), materializeNode.apply( BoxesRunTime.boxToLong( this.getLongAt( offsetxxx ) ) ))) :BoxedUnit.UNIT;
                return var5;
            }

            var5 = entities.$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                    keyxxx ), materializeNode.apply( BoxesRunTime.boxToLong( this.getLongAt( offsetxxxx ) ) )));
            return var5;
        }, ( ignoreCachedNodeProperties ) -> {
            $anonfun$boundEntities$2( ignoreCachedNodeProperties );
            return BoxedUnit.UNIT;
        } ); return entities.toMap( scala.Predef..MODULE$.$conforms());
    }

    public boolean isNull( final String key )
    {
        boolean var3 = false;
        Some var4 = null;
        Option var5 = this.slots().get( key );
        boolean var2;
        if ( var5 instanceof Some )
        {
            var3 = true;
            var4 = (Some) var5;
            Slot var6 = (Slot) var4.value();
            if ( var6 instanceof RefSlot )
            {
                RefSlot var7 = (RefSlot) var6;
                int offset = var7.offset();
                boolean var9 = var7.nullable();
                if ( var9 && this.isRefInitialized( offset ) )
                {
                    var2 = this.getRefAtWithoutCheckingInitialized( offset ) == Values.NO_VALUE;
                    return var2;
                }
            }
        }

        int offset;
        label90:
        {
            if ( var3 )
            {
                Slot var10 = (Slot) var4.value();
                if ( var10 instanceof LongSlot )
                {
                    LongSlot var11 = (LongSlot) var10;
                    offset = var11.offset();
                    boolean var13 = var11.nullable();
                    CypherType var14 = var11.typ();
                    if ( var13 )
                    {
                        NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                        if ( var10000 == null )
                        {
                            if ( var14 == null )
                            {
                                break label90;
                            }
                        }
                        else if ( var10000.equals( var14 ) )
                        {
                            break label90;
                        }
                    }
                }
            }

            if ( var3 )
            {
                Slot var16 = (Slot) var4.value();
                if ( var16 instanceof LongSlot )
                {
                    LongSlot var17 = (LongSlot) var16;
                    int offset = var17.offset();
                    boolean var19 = var17.nullable();
                    CypherType var20 = var17.typ();
                    if ( var19 )
                    {
                        label91:
                        {
                            RelationshipType var22 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                            if ( var22 == null )
                            {
                                if ( var20 != null )
                                {
                                    break label91;
                                }
                            }
                            else if ( !var22.equals( var20 ) )
                            {
                                break label91;
                            }

                            var2 = NullChecker$.MODULE$.entityIsNull( this.getLongAt( offset ) );
                            return var2;
                        }
                    }
                }
            }

            var2 = false;
            return var2;
        }

        var2 = NullChecker$.MODULE$.entityIsNull( this.getLongAt( offset ) );
        return var2;
    }

    private Iterator<Tuple2<String,AnyValue>> iterator()
    {
        IndexedSeq longSlots = this.slots().getLongSlots();
        IndexedSeq longSlotValues = (IndexedSeq) longSlots.map( ( x ) -> {
            return new Tuple2( x.toString(), Values.longValue( this.longs()[x.slot().offset()] ) );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
        IndexedSeq refSlots = this.slots().getRefSlots();
        IndexedSeq refSlotValues = (IndexedSeq) refSlots.map( ( x ) -> {
            return new Tuple2( x.toString(), this.refs()[x.slot().offset()] );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
        IndexedSeq cachedSlots = this.slots().getCachedPropertySlots();
        IndexedSeq cachedPropertySlotValues = (IndexedSeq) cachedSlots.map( ( x ) -> {
            return new Tuple2( x.toString(), this.refs()[x.slot().offset()] );
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom());
        return ((IndexedSeqLike) ((TraversableLike) longSlotValues.$plus$plus( refSlotValues,
                scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).$plus$plus( cachedPropertySlotValues,
                scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).iterator();
    }

    public SlottedExecutionContext copy( final SlotConfiguration slots )
    {
        return new SlottedExecutionContext( slots );
    }

    public SlotConfiguration copy$default$1()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "SlottedExecutionContext";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.slots();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedExecutionContext;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof SlottedExecutionContext )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        SlottedExecutionContext var4 = (SlottedExecutionContext) x$1;
                        SlotConfiguration var10000 = this.slots();
                        SlotConfiguration var5 = var4.slots();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
