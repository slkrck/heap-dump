package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.values.AnyValue;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface ColumnOrder
{
    Slot slot();

    int compareValues( final AnyValue a, final AnyValue b );

    int compareLongs( final long a, final long b );

    int compareNullableLongs( final long a, final long b );
}
