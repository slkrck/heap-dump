package org.neo4j.cypher.internal.runtime.slotted;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SlottedCompatible
{
    void copyToSlottedExecutionContext( final SlottedExecutionContext target, final int nLongs, final int nRefs );
}
