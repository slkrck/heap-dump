package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction1;

public final class ArrayResultExecutionContextFactory$ extends AbstractFunction1<Seq<Tuple2<String,Expression>>,ArrayResultExecutionContextFactory>
        implements Serializable
{
    public static ArrayResultExecutionContextFactory$ MODULE$;

    static
    {
        new ArrayResultExecutionContextFactory$();
    }

    private ArrayResultExecutionContextFactory$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ArrayResultExecutionContextFactory";
    }

    public ArrayResultExecutionContextFactory apply( final Seq<Tuple2<String,Expression>> columns )
    {
        return new ArrayResultExecutionContextFactory( columns );
    }

    public Option<Seq<Tuple2<String,Expression>>> unapply( final ArrayResultExecutionContextFactory x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.columns() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
