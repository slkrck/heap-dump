package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.runtime.EntityById;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ResourceLinenumber;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import org.neo4j.cypher.result.QueryResult.Record;
import org.neo4j.exceptions.InternalException;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Value;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.Map;
import scala.collection.Seq;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing.;

@JavaDocToJava
public class ArrayResultExecutionContext implements ExecutionContext, Record, Product, Serializable
{
    private final AnyValue[] resultArray;
    private final Map<String,Object> columnIndexMap;
    private final ArrayResultExecutionContextFactory factory;
    private Option<ResourceLinenumber> org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber;

    private fail()
    {
        throw new InternalException( "Tried using a result context as an execution context" );
    }

    public ArrayResultExecutionContext( final AnyValue[] resultArray, final Map<String,Object> columnIndexMap,
            final ArrayResultExecutionContextFactory factory )
    {
        this.resultArray = resultArray;
        this.columnIndexMap = columnIndexMap;
        this.factory = factory;
        ExecutionContext.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<AnyValue[],Map<String,Object>,ArrayResultExecutionContextFactory>> unapply( final ArrayResultExecutionContext x$0 )
    {
        return ArrayResultExecutionContext$.MODULE$.unapply( var0 );
    }

    public static ArrayResultExecutionContext apply( final AnyValue[] resultArray, final Map<String,Object> columnIndexMap,
            final ArrayResultExecutionContextFactory factory )
    {
        return ArrayResultExecutionContext$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<AnyValue[],Map<String,Object>,ArrayResultExecutionContextFactory>,ArrayResultExecutionContext> tupled()
    {
        return ArrayResultExecutionContext$.MODULE$.tupled();
    }

    public static Function1<AnyValue[],Function1<Map<String,Object>,Function1<ArrayResultExecutionContextFactory,ArrayResultExecutionContext>>> curried()
    {
        return ArrayResultExecutionContext$.MODULE$.curried();
    }

    public int copyTo$default$2()
    {
        return ExecutionContext.copyTo$default$2$( this );
    }

    public int copyTo$default$3()
    {
        return ExecutionContext.copyTo$default$3$( this );
    }

    public int copyTo$default$4()
    {
        return ExecutionContext.copyTo$default$4$( this );
    }

    public int copyTo$default$5()
    {
        return ExecutionContext.copyTo$default$5$( this );
    }

    public Option<ResourceLinenumber> org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber()
    {
        return this.org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber;
    }

    public void org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber_$eq( final Option<ResourceLinenumber> x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$ExecutionContext$$linenumber = x$1;
    }

    public AnyValue[] resultArray()
    {
        return this.resultArray;
    }

    public Map<String,Object> columnIndexMap()
    {
        return this.columnIndexMap;
    }

    public ArrayResultExecutionContextFactory factory()
    {
        return this.factory;
    }

    public void release()
    {
        this.factory().releaseExecutionContext( this );
    }

    public AnyValue[] fields()
    {
        return this.resultArray();
    }

    public AnyValue getByName( final String key )
    {
        Option var3 = this.columnIndexMap().get( key );
        if ( var3 instanceof Some )
        {
            Some var4 = (Some) var3;
            int index = BoxesRunTime.unboxToInt( var4.value() );
            AnyValue var2 = this.resultArray()[index];
            return var2;
        }
        else
        {
            throw new NotFoundException( (new StringBuilder( 20 )).append( "Unknown variable `" ).append( key ).append( "`." ).toString() );
        }
    }

    public int numberOfColumns()
    {
        return this.columnIndexMap().size();
    }

    public boolean containsName( final String key )
    {
        return this.columnIndexMap().get( key ).isDefined();
    }

    public void copyTo( final ExecutionContext target, final int sourceLongOffset, final int sourceRefOffset, final int targetLongOffset,
            final int targetRefOffset )
    {
        throw this.fail();
    }

    public void copyFrom( final ExecutionContext input, final int nLongs, final int nRefs )
    {
        throw this.fail();
    }

    public void setLongAt( final int offset, final long value )
    {
        throw this.fail();
    }

    public long getLongAt( final int offset )
    {
        throw this.fail();
    }

    public void setRefAt( final int offset, final AnyValue value )
    {
        throw this.fail();
    }

    public AnyValue getRefAt( final int offset )
    {
        throw this.fail();
    }

    public void set( final Seq<Tuple2<String,AnyValue>> newEntries )
    {
        throw this.fail();
    }

    public void set( final String key, final AnyValue value )
    {
        throw this.fail();
    }

    public void set( final String key1, final AnyValue value1, final String key2, final AnyValue value2 )
    {
        throw this.fail();
    }

    public void set( final String key1, final AnyValue value1, final String key2, final AnyValue value2, final String key3, final AnyValue value3 )
    {
        throw this.fail();
    }

    public void mergeWith( final ExecutionContext other, final EntityById entityById )
    {
        throw this.fail();
    }

    public ExecutionContext createClone()
    {
        throw this.fail();
    }

    public ExecutionContext copyWith( final String key, final AnyValue value )
    {
        throw this.fail();
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1, final String key2, final AnyValue value2 )
    {
        throw this.fail();
    }

    public ExecutionContext copyWith( final String key1, final AnyValue value1, final String key2, final AnyValue value2, final String key3,
            final AnyValue value3 )
    {
        throw this.fail();
    }

    public ExecutionContext copyWith( final Seq<Tuple2<String,AnyValue>> newEntries )
    {
        throw this.fail();
    }

    public scala.collection.immutable.Map<String,AnyValue> boundEntities( final Function1<Object,AnyValue> materializeNode,
            final Function1<Object,AnyValue> materializeRelationship )
    {
        throw this.fail();
    }

    public boolean isNull( final String key )
    {
        throw this.fail();
    }

    public void setCachedProperty( final ASTCachedProperty key, final Value value )
    {
        throw this.fail();
    }

    public void setCachedPropertyAt( final int offset, final Value value )
    {
        throw this.fail();
    }

    public Value getCachedProperty( final ASTCachedProperty key )
    {
        throw this.fail();
    }

    public Value getCachedPropertyAt( final int offset )
    {
        throw this.fail();
    }

    public void invalidateCachedNodeProperties( final long node )
    {
        throw this.fail();
    }

    public void invalidateCachedRelationshipProperties( final long rel )
    {
        throw this.fail();
    }

    public void setLinenumber( final String file, final long line, final boolean last )
    {
        throw this.fail();
    }

    public boolean setLinenumber$default$3()
    {
        return false;
    }

    public Option<ResourceLinenumber> getLinenumber()
    {
        throw this.fail();
    }

    public void setLinenumber( final Option<ResourceLinenumber> line )
    {
        throw this.fail();
    }

    public long estimatedHeapUsage()
    {
        return BoxesRunTime.unboxToLong( (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.resultArray() )) ).foldLeft(
                BoxesRunTime.boxToLong( 0L ), ( x$2, x$3 ) -> {
                    return BoxesRunTime.boxToLong( $anonfun$estimatedHeapUsage$1( BoxesRunTime.unboxToLong( x$2 ), x$3 ) );
                } ));
    }

    public ArrayResultExecutionContext copy( final AnyValue[] resultArray, final Map<String,Object> columnIndexMap,
            final ArrayResultExecutionContextFactory factory )
    {
        return new ArrayResultExecutionContext( resultArray, columnIndexMap, factory );
    }

    public AnyValue[] copy$default$1()
    {
        return this.resultArray();
    }

    public Map<String,Object> copy$default$2()
    {
        return this.columnIndexMap();
    }

    public ArrayResultExecutionContextFactory copy$default$3()
    {
        return this.factory();
    }

    public String productPrefix()
    {
        return "ArrayResultExecutionContext";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.resultArray();
            break;
        case 1:
            var10000 = this.columnIndexMap();
            break;
        case 2:
            var10000 = this.factory();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ArrayResultExecutionContext;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof ArrayResultExecutionContext )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        ArrayResultExecutionContext var4 = (ArrayResultExecutionContext) x$1;
                        if ( this.resultArray() == var4.resultArray() )
                        {
                            label56:
                            {
                                Map var10000 = this.columnIndexMap();
                                Map var5 = var4.columnIndexMap();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label56;
                                }

                                ArrayResultExecutionContextFactory var7 = this.factory();
                                ArrayResultExecutionContextFactory var6 = var4.factory();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label56;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label47;
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label65;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
