package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlottedExecutionContextFactory implements ExecutionContextFactory, Product, Serializable
{
    private final SlotConfiguration slots;

    public SlottedExecutionContextFactory( final SlotConfiguration slots )
    {
        this.slots = slots;
        Product.$init$( this );
    }

    public static Option<SlotConfiguration> unapply( final SlottedExecutionContextFactory x$0 )
    {
        return SlottedExecutionContextFactory$.MODULE$.unapply( var0 );
    }

    public static SlottedExecutionContextFactory apply( final SlotConfiguration slots )
    {
        return SlottedExecutionContextFactory$.MODULE$.apply( var0 );
    }

    public static <A> Function1<SlotConfiguration,A> andThen( final Function1<SlottedExecutionContextFactory,A> g )
    {
        return SlottedExecutionContextFactory$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,SlottedExecutionContextFactory> compose( final Function1<A,SlotConfiguration> g )
    {
        return SlottedExecutionContextFactory$.MODULE$.compose( var0 );
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public ExecutionContext newExecutionContext()
    {
        return new SlottedExecutionContext( this.slots() );
    }

    public ExecutionContext copyWith( final ExecutionContext row )
    {
        SlottedExecutionContext newCtx = new SlottedExecutionContext( this.slots() );
        row.copyTo( newCtx, row.copyTo$default$2(), row.copyTo$default$3(), row.copyTo$default$4(), row.copyTo$default$5() );
        return newCtx;
    }

    public ExecutionContext copyWith( final ExecutionContext row, final Seq<Tuple2<String,AnyValue>> newEntries )
    {
        SlottedExecutionContext newCopy = new SlottedExecutionContext( this.slots() );
        row.copyTo( newCopy, row.copyTo$default$2(), row.copyTo$default$3(), row.copyTo$default$4(), row.copyTo$default$5() );
        newEntries.withFilter( ( check$ifrefutable$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$copyWith$1( check$ifrefutable$1 ) );
        } ).foreach( ( x$1 ) -> {
            $anonfun$copyWith$2( newCopy, x$1 );
            return BoxedUnit.UNIT;
        } );
        return newCopy;
    }

    public ExecutionContext copyWith( final ExecutionContext row, final String key, final AnyValue value )
    {
        SlottedExecutionContext newCtx = new SlottedExecutionContext( this.slots() );
        row.copyTo( newCtx, row.copyTo$default$2(), row.copyTo$default$3(), row.copyTo$default$4(), row.copyTo$default$5() );
        newCtx.set( key, value );
        return newCtx;
    }

    public ExecutionContext copyWith( final ExecutionContext row, final String key1, final AnyValue value1, final String key2, final AnyValue value2 )
    {
        SlottedExecutionContext newCopy = new SlottedExecutionContext( this.slots() );
        row.copyTo( newCopy, row.copyTo$default$2(), row.copyTo$default$3(), row.copyTo$default$4(), row.copyTo$default$5() );
        newCopy.set( key1, value1 );
        newCopy.set( key2, value2 );
        return newCopy;
    }

    public ExecutionContext copyWith( final ExecutionContext row, final String key1, final AnyValue value1, final String key2, final AnyValue value2,
            final String key3, final AnyValue value3 )
    {
        SlottedExecutionContext newCopy = new SlottedExecutionContext( this.slots() );
        row.copyTo( newCopy, row.copyTo$default$2(), row.copyTo$default$3(), row.copyTo$default$4(), row.copyTo$default$5() );
        newCopy.set( key1, value1 );
        newCopy.set( key2, value2 );
        newCopy.set( key3, value3 );
        return newCopy;
    }

    public SlottedExecutionContextFactory copy( final SlotConfiguration slots )
    {
        return new SlottedExecutionContextFactory( slots );
    }

    public SlotConfiguration copy$default$1()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "SlottedExecutionContextFactory";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.slots();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedExecutionContextFactory;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof SlottedExecutionContextFactory )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        SlottedExecutionContextFactory var4 = (SlottedExecutionContextFactory) x$1;
                        SlotConfiguration var10000 = this.slots();
                        SlotConfiguration var5 = var4.slots();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
