package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.Map;
import scala.runtime.AbstractFunction3;

public final class ArrayResultExecutionContext$
        extends AbstractFunction3<AnyValue[],Map<String,Object>,ArrayResultExecutionContextFactory,ArrayResultExecutionContext> implements Serializable
{
    public static ArrayResultExecutionContext$ MODULE$;

    static
    {
        new ArrayResultExecutionContext$();
    }

    private ArrayResultExecutionContext$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ArrayResultExecutionContext";
    }

    public ArrayResultExecutionContext apply( final AnyValue[] resultArray, final Map<String,Object> columnIndexMap,
            final ArrayResultExecutionContextFactory factory )
    {
        return new ArrayResultExecutionContext( resultArray, columnIndexMap, factory );
    }

    public Option<Tuple3<AnyValue[],Map<String,Object>,ArrayResultExecutionContextFactory>> unapply( final ArrayResultExecutionContext x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.resultArray(), x$0.columnIndexMap(), x$0.factory() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
