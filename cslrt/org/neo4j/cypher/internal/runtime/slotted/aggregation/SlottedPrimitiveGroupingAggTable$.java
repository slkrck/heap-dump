package org.neo4j.cypher.internal.runtime.slotted.aggregation;

public final class SlottedPrimitiveGroupingAggTable$
{
    public static SlottedPrimitiveGroupingAggTable$ MODULE$;

    static
    {
        new SlottedPrimitiveGroupingAggTable$();
    }

    private SlottedPrimitiveGroupingAggTable$()
    {
        MODULE$ = this;
    }
}
