package org.neo4j.cypher.internal.runtime.slotted.aggregation;

public final class SlottedOrderedNonGroupingAggTable$
{
    public static SlottedOrderedNonGroupingAggTable$ MODULE$;

    static
    {
        new SlottedOrderedNonGroupingAggTable$();
    }

    private SlottedOrderedNonGroupingAggTable$()
    {
        MODULE$ = this;
    }
}
