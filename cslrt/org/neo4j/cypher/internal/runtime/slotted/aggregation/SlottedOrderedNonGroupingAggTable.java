package org.neo4j.cypher.internal.runtime.slotted.aggregation;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.OrderedAggregationTableFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.OrderedChunkReceiver;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTable;
import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedOrderedNonGroupingAggTable extends SlottedNonGroupingAggTable implements OrderedChunkReceiver
{
    private final GroupingExpression orderedGroupingColumns;
    private final QueryState state;
    private AnyValue currentGroupKey;

    public SlottedOrderedNonGroupingAggTable( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns,
            final Map<Object,AggregationExpression> aggregations, final QueryState state )
    {
        super( slots, aggregations, state );
        this.orderedGroupingColumns = orderedGroupingColumns;
        this.state = state;
    }

    private AnyValue currentGroupKey()
    {
        return this.currentGroupKey;
    }

    private void currentGroupKey_$eq( final AnyValue x$1 )
    {
        this.currentGroupKey = x$1;
    }

    public void clear()
    {
        this.currentGroupKey_$eq( (AnyValue) null );
        super.clear();
    }

    public boolean isSameChunk( final ExecutionContext first, final ExecutionContext current )
    {
        if ( this.currentGroupKey() == null )
        {
            this.currentGroupKey_$eq( this.orderedGroupingColumns.computeGroupingKey( first, this.state ) );
        }

        boolean var4;
        if ( current != first )
        {
            label34:
            {
                AnyValue var10000 = this.currentGroupKey();
                AnyValue var3 = this.orderedGroupingColumns.computeGroupingKey( current, this.state );
                if ( var10000 == null )
                {
                    if ( var3 == null )
                    {
                        break label34;
                    }
                }
                else if ( var10000.equals( var3 ) )
                {
                    break label34;
                }

                var4 = false;
                return var4;
            }
        }

        var4 = true;
        return var4;
    }

    public Iterator<ExecutionContext> result()
    {
        ExecutionContext row = this.resultRow();
        this.orderedGroupingColumns.project( row, this.currentGroupKey() );
        return scala.package..MODULE$.Iterator().single( row );
    }

    public boolean processNextChunk()
    {
        return true;
    }

    public static class Factory implements OrderedAggregationTableFactory, Product, Serializable
    {
        private final SlotConfiguration slots;
        private final GroupingExpression orderedGroupingColumns;
        private final Map<Object,AggregationExpression> aggregations;

        public Factory( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns, final Map<Object,AggregationExpression> aggregations )
        {
            this.slots = slots;
            this.orderedGroupingColumns = orderedGroupingColumns;
            this.aggregations = aggregations;
            Product.$init$( this );
        }

        public SlotConfiguration slots()
        {
            return this.slots;
        }

        public GroupingExpression orderedGroupingColumns()
        {
            return this.orderedGroupingColumns;
        }

        public Map<Object,AggregationExpression> aggregations()
        {
            return this.aggregations;
        }

        public AggregationTable table( final QueryState state, final ExecutionContextFactory executionContextFactory )
        {
            return new SlottedOrderedNonGroupingAggTable( this.slots(), this.orderedGroupingColumns(), this.aggregations(), state );
        }

        public void registerOwningPipe( final Pipe pipe )
        {
            this.orderedGroupingColumns().registerOwningPipe( pipe );
            this.aggregations().values().foreach( ( x$1 ) -> {
                $anonfun$registerOwningPipe$1( pipe, x$1 );
                return BoxedUnit.UNIT;
            } );
        }

        public SlottedOrderedNonGroupingAggTable.Factory copy( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns,
                final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedOrderedNonGroupingAggTable.Factory( slots, orderedGroupingColumns, aggregations );
        }

        public SlotConfiguration copy$default$1()
        {
            return this.slots();
        }

        public GroupingExpression copy$default$2()
        {
            return this.orderedGroupingColumns();
        }

        public Map<Object,AggregationExpression> copy$default$3()
        {
            return this.aggregations();
        }

        public String productPrefix()
        {
            return "Factory";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.slots();
                break;
            case 1:
                var10000 = this.orderedGroupingColumns();
                break;
            case 2:
                var10000 = this.aggregations();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedOrderedNonGroupingAggTable.Factory;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedOrderedNonGroupingAggTable.Factory )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                SlottedOrderedNonGroupingAggTable.Factory var4 = (SlottedOrderedNonGroupingAggTable.Factory) x$1;
                                SlotConfiguration var10000 = this.slots();
                                SlotConfiguration var5 = var4.slots();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                GroupingExpression var8 = this.orderedGroupingColumns();
                                GroupingExpression var6 = var4.orderedGroupingColumns();
                                if ( var8 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var6 ) )
                                {
                                    break label63;
                                }

                                Map var9 = this.aggregations();
                                Map var7 = var4.aggregations();
                                if ( var9 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var9.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var10 = true;
                                    break label54;
                                }
                            }

                            var10 = false;
                        }

                        if ( var10 )
                        {
                            break label72;
                        }
                    }

                    var10 = false;
                    return var10;
                }
            }

            var10 = true;
            return var10;
        }
    }

    public static class Factory$
            extends AbstractFunction3<SlotConfiguration,GroupingExpression,Map<Object,AggregationExpression>,SlottedOrderedNonGroupingAggTable.Factory>
            implements Serializable
    {
        public static SlottedOrderedNonGroupingAggTable.Factory$ MODULE$;

        static
        {
            new SlottedOrderedNonGroupingAggTable.Factory$();
        }

        public Factory$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "Factory";
        }

        public SlottedOrderedNonGroupingAggTable.Factory apply( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns,
                final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedOrderedNonGroupingAggTable.Factory( slots, orderedGroupingColumns, aggregations );
        }

        public Option<Tuple3<SlotConfiguration,GroupingExpression,Map<Object,AggregationExpression>>> unapply(
                final SlottedOrderedNonGroupingAggTable.Factory x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.slots(), x$0.orderedGroupingColumns(), x$0.aggregations() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
