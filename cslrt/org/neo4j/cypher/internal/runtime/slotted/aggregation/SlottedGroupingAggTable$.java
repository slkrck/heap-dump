package org.neo4j.cypher.internal.runtime.slotted.aggregation;

public final class SlottedGroupingAggTable$
{
    public static SlottedGroupingAggTable$ MODULE$;

    static
    {
        new SlottedGroupingAggTable$();
    }

    private SlottedGroupingAggTable$()
    {
        MODULE$ = this;
    }
}
