package org.neo4j.cypher.internal.runtime.slotted.aggregation;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.OrderedAggregationTableFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.OrderedChunkReceiver;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTable;
import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedOrderedGroupingAggTable extends SlottedGroupingAggTable implements OrderedChunkReceiver
{
    private final GroupingExpression orderedGroupingColumns;
    private final QueryState state;
    private AnyValue currentGroupKey;

    public SlottedOrderedGroupingAggTable( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns,
            final GroupingExpression unorderedGroupingColumns, final Map<Object,AggregationExpression> aggregations, final QueryState state )
    {
        super( slots, unorderedGroupingColumns, aggregations, state );
        this.orderedGroupingColumns = orderedGroupingColumns;
        this.state = state;
    }

    private AnyValue currentGroupKey()
    {
        return this.currentGroupKey;
    }

    private void currentGroupKey_$eq( final AnyValue x$1 )
    {
        this.currentGroupKey = x$1;
    }

    public void clear()
    {
        this.currentGroupKey_$eq( (AnyValue) null );
        super.clear();
    }

    public boolean isSameChunk( final ExecutionContext first, final ExecutionContext current )
    {
        if ( this.currentGroupKey() == null )
        {
            this.currentGroupKey_$eq( this.orderedGroupingColumns.computeGroupingKey( first, this.state ) );
        }

        boolean var4;
        if ( current != first )
        {
            label34:
            {
                AnyValue var10000 = this.currentGroupKey();
                AnyValue var3 = this.orderedGroupingColumns.computeGroupingKey( current, this.state );
                if ( var10000 == null )
                {
                    if ( var3 == null )
                    {
                        break label34;
                    }
                }
                else if ( var10000.equals( var3 ) )
                {
                    break label34;
                }

                var4 = false;
                return var4;
            }
        }

        var4 = true;
        return var4;
    }

    public Iterator<ExecutionContext> result()
    {
        return super.result().map( ( row ) -> {
            this.orderedGroupingColumns.project( row, this.currentGroupKey() );
            return row;
        } );
    }

    public boolean processNextChunk()
    {
        return true;
    }

    public static class Factory implements OrderedAggregationTableFactory, Product, Serializable
    {
        private final SlotConfiguration slots;
        private final GroupingExpression orderedGroupingColumns;
        private final GroupingExpression unorderedGroupingColumns;
        private final Map<Object,AggregationExpression> aggregations;

        public Factory( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns, final GroupingExpression unorderedGroupingColumns,
                final Map<Object,AggregationExpression> aggregations )
        {
            this.slots = slots;
            this.orderedGroupingColumns = orderedGroupingColumns;
            this.unorderedGroupingColumns = unorderedGroupingColumns;
            this.aggregations = aggregations;
            Product.$init$( this );
        }

        public SlotConfiguration slots()
        {
            return this.slots;
        }

        public GroupingExpression orderedGroupingColumns()
        {
            return this.orderedGroupingColumns;
        }

        public GroupingExpression unorderedGroupingColumns()
        {
            return this.unorderedGroupingColumns;
        }

        public Map<Object,AggregationExpression> aggregations()
        {
            return this.aggregations;
        }

        public AggregationTable table( final QueryState state, final ExecutionContextFactory executionContextFactory )
        {
            return new SlottedOrderedGroupingAggTable( this.slots(), this.orderedGroupingColumns(), this.unorderedGroupingColumns(), this.aggregations(),
                    state );
        }

        public void registerOwningPipe( final Pipe pipe )
        {
            this.orderedGroupingColumns().registerOwningPipe( pipe );
            this.unorderedGroupingColumns().registerOwningPipe( pipe );
            this.aggregations().values().foreach( ( x$1 ) -> {
                $anonfun$registerOwningPipe$1( pipe, x$1 );
                return BoxedUnit.UNIT;
            } );
        }

        public SlottedOrderedGroupingAggTable.Factory copy( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns,
                final GroupingExpression unorderedGroupingColumns, final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedOrderedGroupingAggTable.Factory( slots, orderedGroupingColumns, unorderedGroupingColumns, aggregations );
        }

        public SlotConfiguration copy$default$1()
        {
            return this.slots();
        }

        public GroupingExpression copy$default$2()
        {
            return this.orderedGroupingColumns();
        }

        public GroupingExpression copy$default$3()
        {
            return this.unorderedGroupingColumns();
        }

        public Map<Object,AggregationExpression> copy$default$4()
        {
            return this.aggregations();
        }

        public String productPrefix()
        {
            return "Factory";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.slots();
                break;
            case 1:
                var10000 = this.orderedGroupingColumns();
                break;
            case 2:
                var10000 = this.unorderedGroupingColumns();
                break;
            case 3:
                var10000 = this.aggregations();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedOrderedGroupingAggTable.Factory;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var11;
            if ( this != x$1 )
            {
                label81:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedOrderedGroupingAggTable.Factory )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label63:
                        {
                            label72:
                            {
                                SlottedOrderedGroupingAggTable.Factory var4 = (SlottedOrderedGroupingAggTable.Factory) x$1;
                                SlotConfiguration var10000 = this.slots();
                                SlotConfiguration var5 = var4.slots();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label72;
                                }

                                GroupingExpression var9 = this.orderedGroupingColumns();
                                GroupingExpression var6 = var4.orderedGroupingColumns();
                                if ( var9 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var9.equals( var6 ) )
                                {
                                    break label72;
                                }

                                var9 = this.unorderedGroupingColumns();
                                GroupingExpression var7 = var4.unorderedGroupingColumns();
                                if ( var9 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var9.equals( var7 ) )
                                {
                                    break label72;
                                }

                                Map var10 = this.aggregations();
                                Map var8 = var4.aggregations();
                                if ( var10 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10.equals( var8 ) )
                                {
                                    break label72;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var11 = true;
                                    break label63;
                                }
                            }

                            var11 = false;
                        }

                        if ( var11 )
                        {
                            break label81;
                        }
                    }

                    var11 = false;
                    return var11;
                }
            }

            var11 = true;
            return var11;
        }
    }

    public static class Factory$ extends
            AbstractFunction4<SlotConfiguration,GroupingExpression,GroupingExpression,Map<Object,AggregationExpression>,SlottedOrderedGroupingAggTable.Factory>
            implements Serializable
    {
        public static SlottedOrderedGroupingAggTable.Factory$ MODULE$;

        static
        {
            new SlottedOrderedGroupingAggTable.Factory$();
        }

        public Factory$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "Factory";
        }

        public SlottedOrderedGroupingAggTable.Factory apply( final SlotConfiguration slots, final GroupingExpression orderedGroupingColumns,
                final GroupingExpression unorderedGroupingColumns, final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedOrderedGroupingAggTable.Factory( slots, orderedGroupingColumns, unorderedGroupingColumns, aggregations );
        }

        public Option<Tuple4<SlotConfiguration,GroupingExpression,GroupingExpression,Map<Object,AggregationExpression>>> unapply(
                final SlottedOrderedGroupingAggTable.Factory x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :
            new Some( new Tuple4( x$0.slots(), x$0.orderedGroupingColumns(), x$0.unorderedGroupingColumns(), x$0.aggregations() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
