package org.neo4j.cypher.internal.runtime.slotted.aggregation;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTable;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTableFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.aggregation.AggregationFunction;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.values.AnyValue;
import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.Option;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Predef..less.colon.less;
import scala.collection.BufferedIterator;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Traversable;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Vector;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassTag;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedGroupingAggTable implements AggregationTable
{
    public final SlotConfiguration org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$slots;
    public final GroupingExpression org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$groupingColumns;
    public final QueryState org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$state;
    private final int[] org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$aggregationOffsets;
    private final AggregationExpression[] aggregationExpressions;
    private LinkedHashMap<AnyValue,AggregationFunction[]> resultMap;

    public SlottedGroupingAggTable( final SlotConfiguration slots, final GroupingExpression groupingColumns,
            final Map<Object,AggregationExpression> aggregations, final QueryState state )
    {
        this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$slots = slots;
        this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$groupingColumns = groupingColumns;
        this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$state = state;
        Tuple2 var9 = aggregations.unzip( scala.Predef..MODULE$.$conforms());
        if ( var9 != null )
        {
            scala.collection.immutable.Iterable a = (scala.collection.immutable.Iterable) var9._1();
            scala.collection.immutable.Iterable b = (scala.collection.immutable.Iterable) var9._2();
            Tuple2 var6 = new Tuple2( a, b );
            scala.collection.immutable.Iterable a = (scala.collection.immutable.Iterable) var6._1();
            scala.collection.immutable.Iterable b = (scala.collection.immutable.Iterable) var6._2();
            Tuple2 var7 = new Tuple2( a.toArray( scala.reflect.ClassTag..MODULE$.Int() ), b.
            toArray( scala.reflect.ClassTag..MODULE$.apply( AggregationExpression.class )));
            if ( var7 != null )
            {
                int[] aggregationOffsets = (int[]) var7._1();
                AggregationExpression[] aggregationExpressions = (AggregationExpression[]) var7._2();
                if ( aggregationOffsets != null && aggregationExpressions != null )
                {
                    Tuple2 var5 = new Tuple2( aggregationOffsets, aggregationExpressions );
                    this.x$2 = var5;
                    this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$aggregationOffsets = (int[]) this.x$2._1();
                    this.aggregationExpressions = (AggregationExpression[]) this.x$2._2();
                    return;
                }
            }

            throw new MatchError( var7 );
        }
        else
        {
            throw new MatchError( var9 );
        }
    }

    private LinkedHashMap<AnyValue,AggregationFunction[]> resultMap()
    {
        return this.resultMap;
    }

    private void resultMap_$eq( final LinkedHashMap<AnyValue,AggregationFunction[]> x$1 )
    {
        this.resultMap = x$1;
    }

    public int[] org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$aggregationOffsets()
    {
        return this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$aggregationOffsets;
    }

    private AggregationExpression[] aggregationExpressions()
    {
        return this.aggregationExpressions;
    }

    public void clear()
    {
        this.resultMap_$eq( new LinkedHashMap() );
    }

    public void processRow( final ExecutionContext row )
    {
        AnyValue groupingValue = this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$groupingColumns.computeGroupingKey( row,
                this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$state );
        AggregationFunction[] functions = (AggregationFunction[]) this.resultMap().computeIfAbsent( groupingValue, ( x$3 ) -> {
            this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$state.memoryTracker().allocated( groupingValue );
            AggregationFunction[] functions = new AggregationFunction[this.aggregationExpressions().length];

            for ( int i = 0; i < this.aggregationExpressions().length; ++i )
            {
                functions[i] = this.aggregationExpressions()[i].createAggregationFunction();
            }

            return functions;
        } );

        for ( int i = 0; i < functions.length; ++i )
        {
            functions[i].apply( row, this.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$state );
        }
    }

    public Iterator<ExecutionContext> result()
    {
        java.util.Iterator innerIterator = this.resultMap().entrySet().iterator();
        return new Iterator<ExecutionContext>( this, innerIterator )
        {
            private final java.util.Iterator innerIterator$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.innerIterator$1 = innerIterator$1;
                    GenTraversableOnce.$init$( this );
                    TraversableOnce.$init$( this );
                    Iterator.$init$( this );
                }
            }

            public Iterator<ExecutionContext> seq()
            {
                return Iterator.seq$( this );
            }

            public boolean isEmpty()
            {
                return Iterator.isEmpty$( this );
            }

            public boolean isTraversableAgain()
            {
                return Iterator.isTraversableAgain$( this );
            }

            public boolean hasDefiniteSize()
            {
                return Iterator.hasDefiniteSize$( this );
            }

            public Iterator<ExecutionContext> take( final int n )
            {
                return Iterator.take$( this, n );
            }

            public Iterator<ExecutionContext> drop( final int n )
            {
                return Iterator.drop$( this, n );
            }

            public Iterator<ExecutionContext> slice( final int from, final int until )
            {
                return Iterator.slice$( this, from, until );
            }

            public Iterator<ExecutionContext> sliceIterator( final int from, final int until )
            {
                return Iterator.sliceIterator$( this, from, until );
            }

            public <B> Iterator<B> map( final Function1<ExecutionContext,B> f )
            {
                return Iterator.map$( this, f );
            }

            public <B> Iterator<B> $plus$plus( final Function0<GenTraversableOnce<B>> that )
            {
                return Iterator.$plus$plus$( this, that );
            }

            public <B> Iterator<B> flatMap( final Function1<ExecutionContext,GenTraversableOnce<B>> f )
            {
                return Iterator.flatMap$( this, f );
            }

            public Iterator<ExecutionContext> filter( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.filter$( this, p );
            }

            public <B> boolean corresponds( final GenTraversableOnce<B> that, final Function2<ExecutionContext,B,Object> p )
            {
                return Iterator.corresponds$( this, that, p );
            }

            public Iterator<ExecutionContext> withFilter( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.withFilter$( this, p );
            }

            public Iterator<ExecutionContext> filterNot( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.filterNot$( this, p );
            }

            public <B> Iterator<B> collect( final PartialFunction<ExecutionContext,B> pf )
            {
                return Iterator.collect$( this, pf );
            }

            public <B> Iterator<B> scanLeft( final B z, final Function2<B,ExecutionContext,B> op )
            {
                return Iterator.scanLeft$( this, z, op );
            }

            public <B> Iterator<B> scanRight( final B z, final Function2<ExecutionContext,B,B> op )
            {
                return Iterator.scanRight$( this, z, op );
            }

            public Iterator<ExecutionContext> takeWhile( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.takeWhile$( this, p );
            }

            public Tuple2<Iterator<ExecutionContext>,Iterator<ExecutionContext>> partition( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.partition$( this, p );
            }

            public Tuple2<Iterator<ExecutionContext>,Iterator<ExecutionContext>> span( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.span$( this, p );
            }

            public Iterator<ExecutionContext> dropWhile( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.dropWhile$( this, p );
            }

            public <B> Iterator<Tuple2<ExecutionContext,B>> zip( final Iterator<B> that )
            {
                return Iterator.zip$( this, that );
            }

            public <A1> Iterator<A1> padTo( final int len, final A1 elem )
            {
                return Iterator.padTo$( this, len, elem );
            }

            public Iterator<Tuple2<ExecutionContext,Object>> zipWithIndex()
            {
                return Iterator.zipWithIndex$( this );
            }

            public <B, A1, B1> Iterator<Tuple2<A1,B1>> zipAll( final Iterator<B> that, final A1 thisElem, final B1 thatElem )
            {
                return Iterator.zipAll$( this, that, thisElem, thatElem );
            }

            public <U> void foreach( final Function1<ExecutionContext,U> f )
            {
                Iterator.foreach$( this, f );
            }

            public boolean forall( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.forall$( this, p );
            }

            public boolean exists( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.exists$( this, p );
            }

            public boolean contains( final Object elem )
            {
                return Iterator.contains$( this, elem );
            }

            public Option<ExecutionContext> find( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.find$( this, p );
            }

            public int indexWhere( final Function1<ExecutionContext,Object> p )
            {
                return Iterator.indexWhere$( this, p );
            }

            public int indexWhere( final Function1<ExecutionContext,Object> p, final int from )
            {
                return Iterator.indexWhere$( this, p, from );
            }

            public <B> int indexOf( final B elem )
            {
                return Iterator.indexOf$( this, elem );
            }

            public <B> int indexOf( final B elem, final int from )
            {
                return Iterator.indexOf$( this, elem, from );
            }

            public BufferedIterator<ExecutionContext> buffered()
            {
                return Iterator.buffered$( this );
            }

            public <B> Iterator<ExecutionContext>.GroupedIterator<B> grouped( final int size )
            {
                return Iterator.grouped$( this, size );
            }

            public <B> Iterator<ExecutionContext>.GroupedIterator<B> sliding( final int size, final int step )
            {
                return Iterator.sliding$( this, size, step );
            }

            public int length()
            {
                return Iterator.length$( this );
            }

            public Tuple2<Iterator<ExecutionContext>,Iterator<ExecutionContext>> duplicate()
            {
                return Iterator.duplicate$( this );
            }

            public <B> Iterator<B> patch( final int from, final Iterator<B> patchElems, final int replaced )
            {
                return Iterator.patch$( this, from, patchElems, replaced );
            }

            public <B> void copyToArray( final Object xs, final int start, final int len )
            {
                Iterator.copyToArray$( this, xs, start, len );
            }

            public boolean sameElements( final Iterator<?> that )
            {
                return Iterator.sameElements$( this, that );
            }

            public Traversable<ExecutionContext> toTraversable()
            {
                return Iterator.toTraversable$( this );
            }

            public Iterator<ExecutionContext> toIterator()
            {
                return Iterator.toIterator$( this );
            }

            public Stream<ExecutionContext> toStream()
            {
                return Iterator.toStream$( this );
            }

            public String toString()
            {
                return Iterator.toString$( this );
            }

            public <B> int sliding$default$2()
            {
                return Iterator.sliding$default$2$( this );
            }

            public List<ExecutionContext> reversed()
            {
                return TraversableOnce.reversed$( this );
            }

            public int size()
            {
                return TraversableOnce.size$( this );
            }

            public boolean nonEmpty()
            {
                return TraversableOnce.nonEmpty$( this );
            }

            public int count( final Function1<ExecutionContext,Object> p )
            {
                return TraversableOnce.count$( this, p );
            }

            public <B> Option<B> collectFirst( final PartialFunction<ExecutionContext,B> pf )
            {
                return TraversableOnce.collectFirst$( this, pf );
            }

            public <B> B $div$colon( final B z, final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.$div$colon$( this, z, op );
            }

            public <B> B $colon$bslash( final B z, final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.$colon$bslash$( this, z, op );
            }

            public <B> B foldLeft( final B z, final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.foldLeft$( this, z, op );
            }

            public <B> B foldRight( final B z, final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.foldRight$( this, z, op );
            }

            public <B> B reduceLeft( final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.reduceLeft$( this, op );
            }

            public <B> B reduceRight( final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.reduceRight$( this, op );
            }

            public <B> Option<B> reduceLeftOption( final Function2<B,ExecutionContext,B> op )
            {
                return TraversableOnce.reduceLeftOption$( this, op );
            }

            public <B> Option<B> reduceRightOption( final Function2<ExecutionContext,B,B> op )
            {
                return TraversableOnce.reduceRightOption$( this, op );
            }

            public <A1> A1 reduce( final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.reduce$( this, op );
            }

            public <A1> Option<A1> reduceOption( final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.reduceOption$( this, op );
            }

            public <A1> A1 fold( final A1 z, final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.fold$( this, z, op );
            }

            public <B> B aggregate( final Function0<B> z, final Function2<B,ExecutionContext,B> seqop, final Function2<B,B,B> combop )
            {
                return TraversableOnce.aggregate$( this, z, seqop, combop );
            }

            public <B> B sum( final Numeric<B> num )
            {
                return TraversableOnce.sum$( this, num );
            }

            public <B> B product( final Numeric<B> num )
            {
                return TraversableOnce.product$( this, num );
            }

            public Object min( final Ordering cmp )
            {
                return TraversableOnce.min$( this, cmp );
            }

            public Object max( final Ordering cmp )
            {
                return TraversableOnce.max$( this, cmp );
            }

            public Object maxBy( final Function1 f, final Ordering cmp )
            {
                return TraversableOnce.maxBy$( this, f, cmp );
            }

            public Object minBy( final Function1 f, final Ordering cmp )
            {
                return TraversableOnce.minBy$( this, f, cmp );
            }

            public <B> void copyToBuffer( final Buffer<B> dest )
            {
                TraversableOnce.copyToBuffer$( this, dest );
            }

            public <B> void copyToArray( final Object xs, final int start )
            {
                TraversableOnce.copyToArray$( this, xs, start );
            }

            public <B> void copyToArray( final Object xs )
            {
                TraversableOnce.copyToArray$( this, xs );
            }

            public <B> Object toArray( final ClassTag<B> evidence$1 )
            {
                return TraversableOnce.toArray$( this, evidence$1 );
            }

            public List<ExecutionContext> toList()
            {
                return TraversableOnce.toList$( this );
            }

            public Iterable<ExecutionContext> toIterable()
            {
                return TraversableOnce.toIterable$( this );
            }

            public Seq<ExecutionContext> toSeq()
            {
                return TraversableOnce.toSeq$( this );
            }

            public IndexedSeq<ExecutionContext> toIndexedSeq()
            {
                return TraversableOnce.toIndexedSeq$( this );
            }

            public <B> Buffer<B> toBuffer()
            {
                return TraversableOnce.toBuffer$( this );
            }

            public <B> Set<B> toSet()
            {
                return TraversableOnce.toSet$( this );
            }

            public Vector<ExecutionContext> toVector()
            {
                return TraversableOnce.toVector$( this );
            }

            public <Col> Col to( final CanBuildFrom<scala.runtime.Nothing .,ExecutionContext,Col> cbf )
            {
                return TraversableOnce.to$( this, cbf );
            }

            public <T, U> Map<T,U> toMap( final less<ExecutionContext,Tuple2<T,U>> ev )
            {
                return TraversableOnce.toMap$( this, ev );
            }

            public String mkString( final String start, final String sep, final String end )
            {
                return TraversableOnce.mkString$( this, start, sep, end );
            }

            public String mkString( final String sep )
            {
                return TraversableOnce.mkString$( this, sep );
            }

            public String mkString()
            {
                return TraversableOnce.mkString$( this );
            }

            public StringBuilder addString( final StringBuilder b, final String start, final String sep, final String end )
            {
                return TraversableOnce.addString$( this, b, start, sep, end );
            }

            public StringBuilder addString( final StringBuilder b, final String sep )
            {
                return TraversableOnce.addString$( this, b, sep );
            }

            public StringBuilder addString( final StringBuilder b )
            {
                return TraversableOnce.addString$( this, b );
            }

            public int sizeHintIfCheap()
            {
                return GenTraversableOnce.sizeHintIfCheap$( this );
            }

            public boolean hasNext()
            {
                return this.innerIterator$1.hasNext();
            }

            public ExecutionContext next()
            {
                Entry entry = (Entry) this.innerIterator$1.next();
                AnyValue unorderedGroupingValue = (AnyValue) entry.getKey();
                AggregationFunction[] aggregateFunctions = (AggregationFunction[]) entry.getValue();
                SlottedExecutionContext row =
                        new SlottedExecutionContext( this.$outer.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$slots );
                this.$outer.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$groupingColumns.project( row,
                        unorderedGroupingValue );

                for ( int i = 0; i < aggregateFunctions.length; ++i )
                {
                    row.setRefAt( this.$outer.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$aggregationOffsets()[i],
                            aggregateFunctions[i].result( this.$outer.org$neo4j$cypher$internal$runtime$slotted$aggregation$SlottedGroupingAggTable$$state ) );
                }

                return row;
            }
        };
    }

    public static class Factory implements AggregationTableFactory, Product, Serializable
    {
        private final SlotConfiguration slots;
        private final GroupingExpression groupingColumns;
        private final Map<Object,AggregationExpression> aggregations;

        public Factory( final SlotConfiguration slots, final GroupingExpression groupingColumns, final Map<Object,AggregationExpression> aggregations )
        {
            this.slots = slots;
            this.groupingColumns = groupingColumns;
            this.aggregations = aggregations;
            Product.$init$( this );
        }

        public SlotConfiguration slots()
        {
            return this.slots;
        }

        public GroupingExpression groupingColumns()
        {
            return this.groupingColumns;
        }

        public Map<Object,AggregationExpression> aggregations()
        {
            return this.aggregations;
        }

        public AggregationTable table( final QueryState state, final ExecutionContextFactory executionContextFactory )
        {
            return new SlottedGroupingAggTable( this.slots(), this.groupingColumns(), this.aggregations(), state );
        }

        public void registerOwningPipe( final Pipe pipe )
        {
            this.aggregations().values().foreach( ( x$4 ) -> {
                $anonfun$registerOwningPipe$1( pipe, x$4 );
                return BoxedUnit.UNIT;
            } );
            this.groupingColumns().registerOwningPipe( pipe );
        }

        public SlottedGroupingAggTable.Factory copy( final SlotConfiguration slots, final GroupingExpression groupingColumns,
                final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedGroupingAggTable.Factory( slots, groupingColumns, aggregations );
        }

        public SlotConfiguration copy$default$1()
        {
            return this.slots();
        }

        public GroupingExpression copy$default$2()
        {
            return this.groupingColumns();
        }

        public Map<Object,AggregationExpression> copy$default$3()
        {
            return this.aggregations();
        }

        public String productPrefix()
        {
            return "Factory";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.slots();
                break;
            case 1:
                var10000 = this.groupingColumns();
                break;
            case 2:
                var10000 = this.aggregations();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedGroupingAggTable.Factory;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedGroupingAggTable.Factory )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                SlottedGroupingAggTable.Factory var4 = (SlottedGroupingAggTable.Factory) x$1;
                                SlotConfiguration var10000 = this.slots();
                                SlotConfiguration var5 = var4.slots();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                GroupingExpression var8 = this.groupingColumns();
                                GroupingExpression var6 = var4.groupingColumns();
                                if ( var8 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var6 ) )
                                {
                                    break label63;
                                }

                                Map var9 = this.aggregations();
                                Map var7 = var4.aggregations();
                                if ( var9 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var9.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var10 = true;
                                    break label54;
                                }
                            }

                            var10 = false;
                        }

                        if ( var10 )
                        {
                            break label72;
                        }
                    }

                    var10 = false;
                    return var10;
                }
            }

            var10 = true;
            return var10;
        }
    }

    public static class Factory$
            extends AbstractFunction3<SlotConfiguration,GroupingExpression,Map<Object,AggregationExpression>,SlottedGroupingAggTable.Factory>
            implements Serializable
    {
        public static SlottedGroupingAggTable.Factory$ MODULE$;

        static
        {
            new SlottedGroupingAggTable.Factory$();
        }

        public Factory$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "Factory";
        }

        public SlottedGroupingAggTable.Factory apply( final SlotConfiguration slots, final GroupingExpression groupingColumns,
                final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedGroupingAggTable.Factory( slots, groupingColumns, aggregations );
        }

        public Option<Tuple3<SlotConfiguration,GroupingExpression,Map<Object,AggregationExpression>>> unapply( final SlottedGroupingAggTable.Factory x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.slots(), x$0.groupingColumns(), x$0.aggregations() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
