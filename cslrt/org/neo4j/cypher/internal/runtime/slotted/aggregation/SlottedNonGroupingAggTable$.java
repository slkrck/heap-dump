package org.neo4j.cypher.internal.runtime.slotted.aggregation;

public final class SlottedNonGroupingAggTable$
{
    public static SlottedNonGroupingAggTable$ MODULE$;

    static
    {
        new SlottedNonGroupingAggTable$();
    }

    private SlottedNonGroupingAggTable$()
    {
        MODULE$ = this;
    }
}
