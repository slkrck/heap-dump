package org.neo4j.cypher.internal.runtime.slotted.aggregation;

import java.util.LinkedHashMap;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTable;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTableFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.aggregation.AggregationFunction;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.values.storable.LongArray;
import org.neo4j.values.storable.Values;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedPrimitiveGroupingAggTable implements AggregationTable
{
    private final SlotConfiguration slots;
    private final int[] readGrouping;
    private final int[] writeGrouping;
    private final QueryState state;
    private final int[] aggregationOffsets;
    private final AggregationExpression[] aggregationExpressions;
    private LinkedHashMap<LongArray,AggregationFunction[]> resultMap;

    public SlottedPrimitiveGroupingAggTable( final SlotConfiguration slots, final int[] readGrouping, final int[] writeGrouping,
            final Map<Object,AggregationExpression> aggregations, final QueryState state )
    {
        this.slots = slots;
        this.readGrouping = readGrouping;
        this.writeGrouping = writeGrouping;
        this.state = state;
        Tuple2 var10 = aggregations.unzip( scala.Predef..MODULE$.$conforms());
        if ( var10 != null )
        {
            Iterable a = (Iterable) var10._1();
            Iterable b = (Iterable) var10._2();
            Tuple2 var7 = new Tuple2( a, b );
            Iterable a = (Iterable) var7._1();
            Iterable b = (Iterable) var7._2();
            Tuple2 var8 = new Tuple2( a.toArray( scala.reflect.ClassTag..MODULE$.Int() ), b.
            toArray( scala.reflect.ClassTag..MODULE$.apply( AggregationExpression.class )));
            if ( var8 != null )
            {
                int[] aggregationOffsets = (int[]) var8._1();
                AggregationExpression[] aggregationExpressions = (AggregationExpression[]) var8._2();
                if ( aggregationOffsets != null && aggregationExpressions != null )
                {
                    Tuple2 var6 = new Tuple2( aggregationOffsets, aggregationExpressions );
                    this.x$2 = var6;
                    this.aggregationOffsets = (int[]) this.x$2._1();
                    this.aggregationExpressions = (AggregationExpression[]) this.x$2._2();
                    return;
                }
            }

            throw new MatchError( var8 );
        }
        else
        {
            throw new MatchError( var10 );
        }
    }

    public LinkedHashMap<LongArray,AggregationFunction[]> resultMap()
    {
        return this.resultMap;
    }

    public void resultMap_$eq( final LinkedHashMap<LongArray,AggregationFunction[]> x$1 )
    {
        this.resultMap = x$1;
    }

    private int[] aggregationOffsets()
    {
        return this.aggregationOffsets;
    }

    private AggregationExpression[] aggregationExpressions()
    {
        return this.aggregationExpressions;
    }

    private LongArray computeGroupingKey( final ExecutionContext row )
    {
        long[] keys = new long[this.readGrouping.length];

        for ( int i = 0; i < this.readGrouping.length; ++i )
        {
            keys[i] = row.getLongAt( this.readGrouping[i] );
        }

        return Values.longArray( keys );
    }

    private void projectGroupingKey( final ExecutionContext ctx, final LongArray key )
    {
        for ( int i = 0; i < this.writeGrouping.length; ++i )
        {
            ctx.setLongAt( this.writeGrouping[i], key.longValue( i ) );
        }
    }

    private ExecutionContext createResultRow( final LongArray groupingKey, final Seq<AggregationFunction> aggregateFunctions )
    {
        SlottedExecutionContext row = new SlottedExecutionContext( this.slots );
        this.projectGroupingKey( row, groupingKey );

        for ( int i = 0; i < aggregateFunctions.length(); ++i )
        {
            row.setRefAt( this.aggregationOffsets()[i], ((AggregationFunction) aggregateFunctions.apply( i )).result( this.state ) );
        }

        return row;
    }

    public void clear()
    {
        this.resultMap_$eq( new LinkedHashMap() );
    }

    public void processRow( final ExecutionContext row )
    {
        LongArray groupingValue = this.computeGroupingKey( row );
        AggregationFunction[] functions = (AggregationFunction[]) this.resultMap().computeIfAbsent( groupingValue, ( x$3 ) -> {
            this.state.memoryTracker().allocated( groupingValue );
            AggregationFunction[] functions = new AggregationFunction[this.aggregationExpressions().length];

            for ( int i = 0; i < this.aggregationExpressions().length; ++i )
            {
                functions[i] = this.aggregationExpressions()[i].createAggregationFunction();
            }

            return functions;
        } );

        for ( int i = 0; i < functions.length; ++i )
        {
            functions[i].apply( row, this.state );
        }
    }

    public Iterator<ExecutionContext> result()
    {
        return ((Iterator) scala.collection.JavaConverters..MODULE$.asScalaIteratorConverter( this.resultMap().entrySet().iterator() ).asScala()).
        map( ( e ) -> {
            return this.createResultRow( (LongArray) e.getKey(), scala.Predef..MODULE$.wrapRefArray( (Object[]) e.getValue() ));
        } );
    }

    public static class Factory implements AggregationTableFactory, Product, Serializable
    {
        private final SlotConfiguration slots;
        private final int[] readGrouping;
        private final int[] writeGrouping;
        private final Map<Object,AggregationExpression> aggregations;

        public Factory( final SlotConfiguration slots, final int[] readGrouping, final int[] writeGrouping,
                final Map<Object,AggregationExpression> aggregations )
        {
            this.slots = slots;
            this.readGrouping = readGrouping;
            this.writeGrouping = writeGrouping;
            this.aggregations = aggregations;
            Product.$init$( this );
        }

        public SlotConfiguration slots()
        {
            return this.slots;
        }

        public int[] readGrouping()
        {
            return this.readGrouping;
        }

        public int[] writeGrouping()
        {
            return this.writeGrouping;
        }

        public Map<Object,AggregationExpression> aggregations()
        {
            return this.aggregations;
        }

        public AggregationTable table( final QueryState state, final ExecutionContextFactory executionContextFactory )
        {
            return new SlottedPrimitiveGroupingAggTable( this.slots(), this.readGrouping(), this.writeGrouping(), this.aggregations(), state );
        }

        public void registerOwningPipe( final Pipe pipe )
        {
            this.aggregations().values().foreach( ( x$4 ) -> {
                $anonfun$registerOwningPipe$1( pipe, x$4 );
                return BoxedUnit.UNIT;
            } );
        }

        public SlottedPrimitiveGroupingAggTable.Factory copy( final SlotConfiguration slots, final int[] readGrouping, final int[] writeGrouping,
                final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedPrimitiveGroupingAggTable.Factory( slots, readGrouping, writeGrouping, aggregations );
        }

        public SlotConfiguration copy$default$1()
        {
            return this.slots();
        }

        public int[] copy$default$2()
        {
            return this.readGrouping();
        }

        public int[] copy$default$3()
        {
            return this.writeGrouping();
        }

        public Map<Object,AggregationExpression> copy$default$4()
        {
            return this.aggregations();
        }

        public String productPrefix()
        {
            return "Factory";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.slots();
                break;
            case 1:
                var10000 = this.readGrouping();
                break;
            case 2:
                var10000 = this.writeGrouping();
                break;
            case 3:
                var10000 = this.aggregations();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedPrimitiveGroupingAggTable.Factory;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label66:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedPrimitiveGroupingAggTable.Factory )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label49:
                        {
                            label48:
                            {
                                SlottedPrimitiveGroupingAggTable.Factory var4 = (SlottedPrimitiveGroupingAggTable.Factory) x$1;
                                SlotConfiguration var10000 = this.slots();
                                SlotConfiguration var5 = var4.slots();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label48;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label48;
                                }

                                if ( this.readGrouping() == var4.readGrouping() && this.writeGrouping() == var4.writeGrouping() )
                                {
                                    label41:
                                    {
                                        Map var7 = this.aggregations();
                                        Map var6 = var4.aggregations();
                                        if ( var7 == null )
                                        {
                                            if ( var6 != null )
                                            {
                                                break label41;
                                            }
                                        }
                                        else if ( !var7.equals( var6 ) )
                                        {
                                            break label41;
                                        }

                                        if ( var4.canEqual( this ) )
                                        {
                                            var8 = true;
                                            break label49;
                                        }
                                    }
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label66;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class Factory$
            extends AbstractFunction4<SlotConfiguration,int[],int[],Map<Object,AggregationExpression>,SlottedPrimitiveGroupingAggTable.Factory>
            implements Serializable
    {
        public static SlottedPrimitiveGroupingAggTable.Factory$ MODULE$;

        static
        {
            new SlottedPrimitiveGroupingAggTable.Factory$();
        }

        public Factory$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "Factory";
        }

        public SlottedPrimitiveGroupingAggTable.Factory apply( final SlotConfiguration slots, final int[] readGrouping, final int[] writeGrouping,
                final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedPrimitiveGroupingAggTable.Factory( slots, readGrouping, writeGrouping, aggregations );
        }

        public Option<Tuple4<SlotConfiguration,int[],int[],Map<Object,AggregationExpression>>> unapply( final SlottedPrimitiveGroupingAggTable.Factory x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :
            new Some( new Tuple4( x$0.slots(), x$0.readGrouping(), x$0.writeGrouping(), x$0.aggregations() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
