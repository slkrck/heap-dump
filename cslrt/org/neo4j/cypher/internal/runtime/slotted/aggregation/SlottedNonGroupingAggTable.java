package org.neo4j.cypher.internal.runtime.slotted.aggregation;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTable;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTableFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.aggregation.AggregationFunction;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedNonGroupingAggTable implements AggregationTable
{
    private final SlotConfiguration slots;
    private final QueryState state;
    private final int[] aggregationOffsets;
    private final AggregationExpression[] aggregationExpressions;
    private final AggregationFunction[] aggregationFunctions;

    public SlottedNonGroupingAggTable( final SlotConfiguration slots, final Map<Object,AggregationExpression> aggregations, final QueryState state )
    {
        this.slots = slots;
        this.state = state;
        Tuple2 var8 = aggregations.unzip( scala.Predef..MODULE$.$conforms());
        if ( var8 != null )
        {
            Iterable a = (Iterable) var8._1();
            Iterable b = (Iterable) var8._2();
            Tuple2 var5 = new Tuple2( a, b );
            Iterable a = (Iterable) var5._1();
            Iterable b = (Iterable) var5._2();
            Tuple2 var6 = new Tuple2( a.toArray( scala.reflect.ClassTag..MODULE$.Int() ), b.
            toArray( scala.reflect.ClassTag..MODULE$.apply( AggregationExpression.class )));
            if ( var6 != null )
            {
                int[] aggregationOffsets = (int[]) var6._1();
                AggregationExpression[] aggregationExpressions = (AggregationExpression[]) var6._2();
                if ( aggregationOffsets != null && aggregationExpressions != null )
                {
                    Tuple2 var4 = new Tuple2( aggregationOffsets, aggregationExpressions );
                    this.x$2 = var4;
                    this.aggregationOffsets = (int[]) this.x$2._1();
                    this.aggregationExpressions = (AggregationExpression[]) this.x$2._2();
                    this.aggregationFunctions = new AggregationFunction[this.aggregationExpressions().length];
                    return;
                }
            }

            throw new MatchError( var6 );
        }
        else
        {
            throw new MatchError( var8 );
        }
    }

    private int[] aggregationOffsets()
    {
        return this.aggregationOffsets;
    }

    private AggregationExpression[] aggregationExpressions()
    {
        return this.aggregationExpressions;
    }

    private AggregationFunction[] aggregationFunctions()
    {
        return this.aggregationFunctions;
    }

    public void clear()
    {
        for ( int i = 0; i < this.aggregationFunctions().length; ++i )
        {
            this.aggregationFunctions()[i] = this.aggregationExpressions()[i].createAggregationFunction();
        }
    }

    public void processRow( final ExecutionContext row )
    {
        for ( int i = 0; i < this.aggregationFunctions().length; ++i )
        {
            this.aggregationFunctions()[i].apply( row, this.state );
        }
    }

    public Iterator<ExecutionContext> result()
    {
        return scala.package..MODULE$.Iterator().single( this.resultRow() );
    }

    public ExecutionContext resultRow()
    {
        SlottedExecutionContext row = new SlottedExecutionContext( this.slots );

        for ( int i = 0; i < this.aggregationFunctions().length; ++i )
        {
            row.setRefAt( this.aggregationOffsets()[i], this.aggregationFunctions()[i].result( this.state ) );
        }

        return row;
    }

    public static class Factory implements AggregationTableFactory, Product, Serializable
    {
        private final SlotConfiguration slots;
        private final Map<Object,AggregationExpression> aggregations;

        public Factory( final SlotConfiguration slots, final Map<Object,AggregationExpression> aggregations )
        {
            this.slots = slots;
            this.aggregations = aggregations;
            Product.$init$( this );
        }

        public SlotConfiguration slots()
        {
            return this.slots;
        }

        public Map<Object,AggregationExpression> aggregations()
        {
            return this.aggregations;
        }

        public AggregationTable table( final QueryState state, final ExecutionContextFactory executionContextFactory )
        {
            return new SlottedNonGroupingAggTable( this.slots(), this.aggregations(), state );
        }

        public void registerOwningPipe( final Pipe pipe )
        {
            this.aggregations().values().foreach( ( x$3 ) -> {
                $anonfun$registerOwningPipe$1( pipe, x$3 );
                return BoxedUnit.UNIT;
            } );
        }

        public SlottedNonGroupingAggTable.Factory copy( final SlotConfiguration slots, final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedNonGroupingAggTable.Factory( slots, aggregations );
        }

        public SlotConfiguration copy$default$1()
        {
            return this.slots();
        }

        public Map<Object,AggregationExpression> copy$default$2()
        {
            return this.aggregations();
        }

        public String productPrefix()
        {
            return "Factory";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.slots();
                break;
            case 1:
                var10000 = this.aggregations();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedNonGroupingAggTable.Factory;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedNonGroupingAggTable.Factory )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedNonGroupingAggTable.Factory var4 = (SlottedNonGroupingAggTable.Factory) x$1;
                                SlotConfiguration var10000 = this.slots();
                                SlotConfiguration var5 = var4.slots();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                Map var7 = this.aggregations();
                                Map var6 = var4.aggregations();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class Factory$ extends AbstractFunction2<SlotConfiguration,Map<Object,AggregationExpression>,SlottedNonGroupingAggTable.Factory>
            implements Serializable
    {
        public static SlottedNonGroupingAggTable.Factory$ MODULE$;

        static
        {
            new SlottedNonGroupingAggTable.Factory$();
        }

        public Factory$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "Factory";
        }

        public SlottedNonGroupingAggTable.Factory apply( final SlotConfiguration slots, final Map<Object,AggregationExpression> aggregations )
        {
            return new SlottedNonGroupingAggTable.Factory( slots, aggregations );
        }

        public Option<Tuple2<SlotConfiguration,Map<Object,AggregationExpression>>> unapply( final SlottedNonGroupingAggTable.Factory x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.slots(), x$0.aggregations() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
