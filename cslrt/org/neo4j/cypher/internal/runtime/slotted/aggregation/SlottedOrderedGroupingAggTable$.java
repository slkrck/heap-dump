package org.neo4j.cypher.internal.runtime.slotted.aggregation;

public final class SlottedOrderedGroupingAggTable$
{
    public static SlottedOrderedGroupingAggTable$ MODULE$;

    static
    {
        new SlottedOrderedGroupingAggTable$();
    }

    private SlottedOrderedGroupingAggTable$()
    {
        MODULE$ = this;
    }
}
