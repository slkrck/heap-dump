package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.InCheckContainer;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.NullPipeDecorator.;
import scala.Option;

public final class SlottedQueryState$
{
    public static SlottedQueryState$ MODULE$;

    static
    {
        new SlottedQueryState$();
    }

    private SlottedQueryState$()
    {
        MODULE$ = this;
    }

    public PipeDecorator $lessinit$greater$default$9()
    {
        return .MODULE$;
    }

    public Option<ExecutionContext> $lessinit$greater$default$10()
    {
        return scala.None..MODULE$;
    }

    public SingleThreadedLRUCache<Object,InCheckContainer> $lessinit$greater$default$11()
    {
        return new SingleThreadedLRUCache( 16 );
    }

    public boolean $lessinit$greater$default$12()
    {
        return false;
    }

    public boolean $lessinit$greater$default$13()
    {
        return false;
    }

    public InputDataStream $lessinit$greater$default$14()
    {
        return org.neo4j.cypher.internal.runtime.NoInput..MODULE$;
    }
}
