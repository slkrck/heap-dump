package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class SlottedExecutionContextFactory$ extends AbstractFunction1<SlotConfiguration,SlottedExecutionContextFactory> implements Serializable
{
    public static SlottedExecutionContextFactory$ MODULE$;

    static
    {
        new SlottedExecutionContextFactory$();
    }

    private SlottedExecutionContextFactory$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedExecutionContextFactory";
    }

    public SlottedExecutionContextFactory apply( final SlotConfiguration slots )
    {
        return new SlottedExecutionContextFactory( slots );
    }

    public Option<SlotConfiguration> unapply( final SlottedExecutionContextFactory x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.slots() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
