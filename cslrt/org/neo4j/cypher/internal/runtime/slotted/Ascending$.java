package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class Ascending$ extends AbstractFunction1<Slot,Ascending> implements Serializable
{
    public static Ascending$ MODULE$;

    static
    {
        new Ascending$();
    }

    private Ascending$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Ascending";
    }

    public Ascending apply( final Slot slot )
    {
        return new Ascending( slot );
    }

    public Option<Slot> unapply( final Ascending x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.slot() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
