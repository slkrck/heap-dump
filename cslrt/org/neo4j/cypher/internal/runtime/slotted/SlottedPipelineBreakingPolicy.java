package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class SlottedPipelineBreakingPolicy
{
    public static SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
    {
        return SlottedPipelineBreakingPolicy$.MODULE$.invoke( var0, var1, var2 );
    }

    public static void onNestedPlanBreak()
    {
        SlottedPipelineBreakingPolicy$.MODULE$.onNestedPlanBreak();
    }

    public static boolean breakOn( final LogicalPlan lp )
    {
        return SlottedPipelineBreakingPolicy$.MODULE$.breakOn( var0 );
    }
}
