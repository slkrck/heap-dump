package org.neo4j.cypher.internal.runtime.slotted;

public final class SlottedExecutionResultBuilderFactory$
{
    public static SlottedExecutionResultBuilderFactory$ MODULE$;

    static
    {
        new SlottedExecutionResultBuilderFactory$();
    }

    private SlottedExecutionResultBuilderFactory$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$11()
    {
        return false;
    }
}
