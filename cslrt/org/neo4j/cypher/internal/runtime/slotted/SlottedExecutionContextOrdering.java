package org.neo4j.cypher.internal.runtime.slotted;

import java.util.Comparator;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import scala.collection.Seq;
import scala.math.Ordering;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class SlottedExecutionContextOrdering
{
    public static Comparator<ExecutionContext> asComparator( final Seq<ColumnOrder> orderBy )
    {
        return SlottedExecutionContextOrdering$.MODULE$.asComparator( var0 );
    }

    public static Ordering<ExecutionContext> comparator( final ColumnOrder order )
    {
        return SlottedExecutionContextOrdering$.MODULE$.comparator( var0 );
    }
}
