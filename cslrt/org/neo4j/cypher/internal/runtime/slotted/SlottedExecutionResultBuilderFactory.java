package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes;
import org.neo4j.cypher.internal.runtime.ExpressionCursors;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.cypher.internal.runtime.MemoryTrackingController;
import org.neo4j.cypher.internal.runtime.ParameterMapping;
import org.neo4j.cypher.internal.runtime.QueryContext;
import org.neo4j.cypher.internal.runtime.QueryIndexes;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.runtime.interpreted.BaseExecutionResultBuilderFactory;
import org.neo4j.cypher.internal.runtime.interpreted.ExecutionResultBuilder;
import org.neo4j.cypher.internal.runtime.interpreted.BaseExecutionResultBuilderFactory.BaseExecutionResultBuilder;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.MapValue;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedExecutionResultBuilderFactory extends BaseExecutionResultBuilderFactory
{
    public final QueryIndexes org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$queryIndexes;
    public final int org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$nExpressionSlots;
    public final ParameterMapping org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$parameterMapping;
    public final boolean org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$lenientCreateRelationship;
    public final MemoryTrackingController org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$memoryTrackingController;
    private volatile SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder$ SlottedExecutionResultBuilder$module;

    public SlottedExecutionResultBuilderFactory( final Pipe pipe, final QueryIndexes queryIndexes, final int nExpressionSlots, final boolean readOnly,
            final Seq<String> columns, final LogicalPlan logicalPlan, final PhysicalPlanningAttributes.SlotConfigurations pipelines,
            final ParameterMapping parameterMapping, final boolean lenientCreateRelationship, final MemoryTrackingController memoryTrackingController,
            final boolean hasLoadCSV )
    {
        super( pipe, readOnly, columns, logicalPlan, hasLoadCSV );
        this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$queryIndexes = queryIndexes;
        this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$nExpressionSlots = nExpressionSlots;
        this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$parameterMapping = parameterMapping;
        this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$lenientCreateRelationship = lenientCreateRelationship;
        this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$memoryTrackingController = memoryTrackingController;
    }

    public static boolean $lessinit$greater$default$11()
    {
        return SlottedExecutionResultBuilderFactory$.MODULE$.$lessinit$greater$default$11();
    }

    public SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder$ SlottedExecutionResultBuilder()
    {
        if ( this.SlottedExecutionResultBuilder$module == null )
        {
            this.SlottedExecutionResultBuilder$lzycompute$1();
        }

        return this.SlottedExecutionResultBuilder$module;
    }

    public ExecutionResultBuilder create( final QueryContext queryContext )
    {
        return new SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder( this, queryContext );
    }

    private final void SlottedExecutionResultBuilder$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.SlottedExecutionResultBuilder$module == null )
            {
                this.SlottedExecutionResultBuilder$module = new SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public class SlottedExecutionResultBuilder extends BaseExecutionResultBuilder implements Product, Serializable
    {
        private final QueryContext queryContext;
        private final ExpressionCursors cursors;

        public SlottedExecutionResultBuilder( final SlottedExecutionResultBuilderFactory $outer, final QueryContext queryContext )
        {
            super( $outer );
            this.queryContext = queryContext;
            Product.$init$( this );
            this.cursors = new ExpressionCursors( queryContext.transactionalContext().cursors() );
            queryContext.resources().trace( this.cursors() );
        }

        public QueryContext queryContext()
        {
            return this.queryContext;
        }

        public ExpressionCursors cursors()
        {
            return this.cursors;
        }

        public SlottedQueryState createQueryState( final MapValue params, final boolean prePopulateResults, final InputDataStream input,
                final QuerySubscriber subscriber )
        {
            QueryContext x$1 = this.queryContext();
            ExternalCSVResource x$2 = this.externalResource();
            AnyValue[] x$3 = org.neo4j.cypher.internal.runtime.createParameterArray..MODULE$.apply( params,
                this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer().org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$parameterMapping );
            ExpressionCursors x$4 = this.cursors();
            IndexReadSession[] x$5 =
                    this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer().org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$queryIndexes.initiateLabelAndSchemaIndexes(
                            this.queryContext() );
            AnyValue[] x$6 =
                    new AnyValue[this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer().org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$nExpressionSlots];
            QueryMemoryTracker x$8 = org.neo4j.cypher.internal.runtime.QueryMemoryTracker..MODULE$.apply(
                this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer().org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$memoryTrackingController.memoryTracking() );
            PipeDecorator x$9 = this.pipeDecorator();
            boolean x$10 =
                    this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer().org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$$lenientCreateRelationship;
            Option x$13 = SlottedQueryState$.MODULE$.$lessinit$greater$default$10();
            SingleThreadedLRUCache x$14 = SlottedQueryState$.MODULE$.$lessinit$greater$default$11();
            return new SlottedQueryState( x$1, x$2, x$3, x$4, x$5, x$6, subscriber, x$8, x$9, x$13, x$14, x$10, prePopulateResults, input );
        }

        public SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder copy( final QueryContext queryContext )
        {
            return this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer().new SlottedExecutionResultBuilder(
                    this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer(), queryContext );
        }

        public QueryContext copy$default$1()
        {
            return this.queryContext();
        }

        public String productPrefix()
        {
            return "SlottedExecutionResultBuilder";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return this.queryContext();
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var6;
            if ( this != x$1 )
            {
                label58:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder &&
                            ((SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder) x$1).org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer() ==
                                    this.org$neo4j$cypher$internal$runtime$slotted$SlottedExecutionResultBuilderFactory$SlottedExecutionResultBuilder$$$outer() )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label35:
                        {
                            label34:
                            {
                                SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder var4 =
                                        (SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder) x$1;
                                QueryContext var10000 = this.queryContext();
                                QueryContext var5 = var4.queryContext();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label34;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label34;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label35;
                                }
                            }

                            var6 = false;
                        }

                        if ( var6 )
                        {
                            break label58;
                        }
                    }

                    var6 = false;
                    return var6;
                }
            }

            var6 = true;
            return var6;
        }
    }

    public class SlottedExecutionResultBuilder$ extends AbstractFunction1<QueryContext,SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder>
            implements Serializable
    {
        public SlottedExecutionResultBuilder$( final SlottedExecutionResultBuilderFactory $outer )
        {
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super();
            }
        }

        public final String toString()
        {
            return "SlottedExecutionResultBuilder";
        }

        public SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder apply( final QueryContext queryContext )
        {
            return this.$outer.new SlottedExecutionResultBuilder( this.$outer, queryContext );
        }

        public Option<QueryContext> unapply( final SlottedExecutionResultBuilderFactory.SlottedExecutionResultBuilder x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( x$0.queryContext() ));
        }
    }
}
