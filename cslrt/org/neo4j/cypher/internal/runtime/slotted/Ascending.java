package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.values.AnyValue;
import org.neo4j.values.AnyValues;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class Ascending implements ColumnOrder, Product, Serializable
{
    private final Slot slot;

    public Ascending( final Slot slot )
    {
        this.slot = slot;
        Product.$init$( this );
    }

    public static Option<Slot> unapply( final Ascending x$0 )
    {
        return Ascending$.MODULE$.unapply( var0 );
    }

    public static Ascending apply( final Slot slot )
    {
        return Ascending$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Slot,A> andThen( final Function1<Ascending,A> g )
    {
        return Ascending$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,Ascending> compose( final Function1<A,Slot> g )
    {
        return Ascending$.MODULE$.compose( var0 );
    }

    public Slot slot()
    {
        return this.slot;
    }

    public int compareValues( final AnyValue a, final AnyValue b )
    {
        return AnyValues.COMPARATOR.compare( a, b );
    }

    public int compareLongs( final long a, final long b )
    {
        return Long.compare( a, b );
    }

    public int compareNullableLongs( final long a, final long b )
    {
        return Long.compareUnsigned( a, b );
    }

    public Ascending copy( final Slot slot )
    {
        return new Ascending( slot );
    }

    public Slot copy$default$1()
    {
        return this.slot();
    }

    public String productPrefix()
    {
        return "Ascending";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.slot();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof Ascending;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof Ascending )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        Ascending var4 = (Ascending) x$1;
                        Slot var10000 = this.slot();
                        Slot var5 = var4.slot();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
