package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ArgumentSlottedPipe implements Pipe, Product, Serializable
{
    private final SlotConfiguration slots;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private ExecutionContextFactory executionContextFactory;

    public ArgumentSlottedPipe( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        this.slots = slots;
        this.argumentSize = argumentSize;
        this.id = id;
        Pipe.$init$( this );
        Product.$init$( this );
    }

    public static int $lessinit$greater$default$3( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return ArgumentSlottedPipe$.MODULE$.$lessinit$greater$default$3( var0, var1 );
    }

    public static int apply$default$3( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return ArgumentSlottedPipe$.MODULE$.apply$default$3( var0, var1 );
    }

    public static Option<Tuple2<SlotConfiguration,SlotConfiguration.Size>> unapply( final ArgumentSlottedPipe x$0 )
    {
        return ArgumentSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ArgumentSlottedPipe apply( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return ArgumentSlottedPipe$.MODULE$.apply( var0, var1, var2 );
    }

    public Iterator<ExecutionContext> createResults( final QueryState state )
    {
        return Pipe.createResults$( this, state );
    }

    public ExecutionContextFactory executionContextFactory()
    {
        return this.executionContextFactory;
    }

    public void executionContextFactory_$eq( final ExecutionContextFactory x$1 )
    {
        this.executionContextFactory = x$1;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<SlottedExecutionContext> internalCreateResults( final QueryState state )
    {
        SlottedExecutionContext context = new SlottedExecutionContext( this.slots() );
        state.copyArgumentStateTo( context, this.argumentSize().nLongs(), this.argumentSize().nReferences() );
        return .MODULE$.Iterator().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new SlottedExecutionContext[]{context}) ));
    }

    public ArgumentSlottedPipe copy( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new ArgumentSlottedPipe( slots, argumentSize, id );
    }

    public SlotConfiguration copy$default$1()
    {
        return this.slots();
    }

    public SlotConfiguration.Size copy$default$2()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "ArgumentSlottedPipe";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.slots();
            break;
        case 1:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ArgumentSlottedPipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof ArgumentSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            ArgumentSlottedPipe var4 = (ArgumentSlottedPipe) x$1;
                            SlotConfiguration var10000 = this.slots();
                            SlotConfiguration var5 = var4.slots();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            SlotConfiguration.Size var7 = this.argumentSize();
                            SlotConfiguration.Size var6 = var4.argumentSize();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
