package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.function.ToLongFunction;

import org.eclipse.collections.api.stack.MutableStack;
import org.eclipse.collections.api.stack.primitive.MutableLongStack;
import org.eclipse.collections.impl.factory.Stacks;
import org.eclipse.collections.impl.factory.primitive.LongStacks;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.physicalplanning.VariablePredicates$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.RelationshipContainer;
import org.neo4j.cypher.internal.runtime.RelationshipIterator;
import org.neo4j.cypher.internal.runtime.RelationshipContainer.;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.exceptions.InternalException;
import org.neo4j.storageengine.api.RelationshipVisitor;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.RelationshipValue;
import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.Tuple16;
import scala.Tuple2;
import scala.Predef..less.colon.less;
import scala.collection.BufferedIterator;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Traversable;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Vector;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassTag;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;
import scala.runtime.Statics;

@JavaDocToJava
public class VarLengthExpandSlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final Slot fromSlot;
    private final int relOffset;
    private final Slot toSlot;
    private final SemanticDirection dir;
    private final SemanticDirection projectedDir;
    private final RelationshipTypes types;
    private final int min;
    private final Option<Object> maxDepth;
    private final boolean shouldExpandAll;
    private final SlotConfiguration slots;
    private final int tempNodeOffset;
    private final int tempRelationshipOffset;
    private final Expression nodePredicate;
    private final Expression relationshipPredicate;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private final ToLongFunction<ExecutionContext> getFromNodeFunction;
    private final ToLongFunction<ExecutionContext> getToNodeFunction;
    private final int org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$toOffset;

    public VarLengthExpandSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option<Object> maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize, final int id )
    {
        super( source );
        this.source = source;
        this.fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.toSlot = toSlot;
        this.dir = dir;
        this.projectedDir = projectedDir;
        this.types = types;
        this.min = min;
        this.maxDepth = maxDepth;
        this.shouldExpandAll = shouldExpandAll;
        this.slots = slots;
        this.tempNodeOffset = tempNodeOffset;
        this.tempRelationshipOffset = tempRelationshipOffset;
        this.nodePredicate = nodePredicate;
        this.relationshipPredicate = relationshipPredicate;
        this.argumentSize = argumentSize;
        this.id = id;
        Product.$init$( this );
        this.getFromNodeFunction = SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( fromSlot, false );
        this.getToNodeFunction = shouldExpandAll ? null : SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( toSlot, false );
        this.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$toOffset = toSlot.offset();
        nodePredicate.registerOwningPipe( this );
        relationshipPredicate.registerOwningPipe( this );
    }

    public static int $lessinit$greater$default$17( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize )
    {
        return VarLengthExpandSlottedPipe$.MODULE$.$lessinit$greater$default$17( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11,
                var12, var13, var14, var15 );
    }

    public static int apply$default$17( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize )
    {
        return VarLengthExpandSlottedPipe$.MODULE$.apply$default$17( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13,
                var14, var15 );
    }

    public static Option<Tuple16<Pipe,Slot,Object,Slot,SemanticDirection,SemanticDirection,RelationshipTypes,Object,Option<Object>,Object,SlotConfiguration,Object,Object,Expression,Expression,SlotConfiguration.Size>> unapply(
            final VarLengthExpandSlottedPipe x$0 )
    {
        return VarLengthExpandSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static VarLengthExpandSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option<Object> maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize, final int id )
    {
        return VarLengthExpandSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15,
                var16 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public Slot fromSlot()
    {
        return this.fromSlot;
    }

    public int relOffset()
    {
        return this.relOffset;
    }

    public Slot toSlot()
    {
        return this.toSlot;
    }

    public SemanticDirection dir()
    {
        return this.dir;
    }

    public SemanticDirection projectedDir()
    {
        return this.projectedDir;
    }

    public RelationshipTypes types()
    {
        return this.types;
    }

    public int min()
    {
        return this.min;
    }

    public Option<Object> maxDepth()
    {
        return this.maxDepth;
    }

    public boolean shouldExpandAll()
    {
        return this.shouldExpandAll;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int tempNodeOffset()
    {
        return this.tempNodeOffset;
    }

    public int tempRelationshipOffset()
    {
        return this.tempRelationshipOffset;
    }

    public Expression nodePredicate()
    {
        return this.nodePredicate;
    }

    public Expression relationshipPredicate()
    {
        return this.relationshipPredicate;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    private ToLongFunction<ExecutionContext> getFromNodeFunction()
    {
        return this.getFromNodeFunction;
    }

    private ToLongFunction<ExecutionContext> getToNodeFunction()
    {
        return this.getToNodeFunction;
    }

    public int org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$toOffset()
    {
        return this.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$toOffset;
    }

    private Iterator<Tuple2<Object,RelationshipContainer>> varLengthExpand( final long node, final QueryState state, final ExecutionContext row )
    {
        MutableLongStack stackOfNodes = LongStacks.mutable.empty();
        MutableStack stackOfRelContainers = Stacks.mutable.empty();
        stackOfNodes.push( node );
        stackOfRelContainers.push(.MODULE$.EMPTY());
        return new Iterator<Tuple2<Object,RelationshipContainer>>( this, state, row, stackOfNodes, stackOfRelContainers )
        {
            public final QueryState state$1;
            private final ExecutionContext row$1;
            private final MutableLongStack stackOfNodes$1;
            private final MutableStack stackOfRelContainers$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.state$1 = state$1;
                    this.row$1 = row$1;
                    this.stackOfNodes$1 = stackOfNodes$1;
                    this.stackOfRelContainers$1 = stackOfRelContainers$1;
                    GenTraversableOnce.$init$( this );
                    TraversableOnce.$init$( this );
                    Iterator.$init$( this );
                }
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> seq()
            {
                return Iterator.seq$( this );
            }

            public boolean isEmpty()
            {
                return Iterator.isEmpty$( this );
            }

            public boolean isTraversableAgain()
            {
                return Iterator.isTraversableAgain$( this );
            }

            public boolean hasDefiniteSize()
            {
                return Iterator.hasDefiniteSize$( this );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> take( final int n )
            {
                return Iterator.take$( this, n );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> drop( final int n )
            {
                return Iterator.drop$( this, n );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> slice( final int from, final int until )
            {
                return Iterator.slice$( this, from, until );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> sliceIterator( final int from, final int until )
            {
                return Iterator.sliceIterator$( this, from, until );
            }

            public <B> Iterator<B> map( final Function1<Tuple2<Object,RelationshipContainer>,B> f )
            {
                return Iterator.map$( this, f );
            }

            public <B> Iterator<B> $plus$plus( final Function0<GenTraversableOnce<B>> that )
            {
                return Iterator.$plus$plus$( this, that );
            }

            public <B> Iterator<B> flatMap( final Function1<Tuple2<Object,RelationshipContainer>,GenTraversableOnce<B>> f )
            {
                return Iterator.flatMap$( this, f );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> filter( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.filter$( this, p );
            }

            public <B> boolean corresponds( final GenTraversableOnce<B> that, final Function2<Tuple2<Object,RelationshipContainer>,B,Object> p )
            {
                return Iterator.corresponds$( this, that, p );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> withFilter( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.withFilter$( this, p );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> filterNot( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.filterNot$( this, p );
            }

            public <B> Iterator<B> collect( final PartialFunction<Tuple2<Object,RelationshipContainer>,B> pf )
            {
                return Iterator.collect$( this, pf );
            }

            public <B> Iterator<B> scanLeft( final B z, final Function2<B,Tuple2<Object,RelationshipContainer>,B> op )
            {
                return Iterator.scanLeft$( this, z, op );
            }

            public <B> Iterator<B> scanRight( final B z, final Function2<Tuple2<Object,RelationshipContainer>,B,B> op )
            {
                return Iterator.scanRight$( this, z, op );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> takeWhile( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.takeWhile$( this, p );
            }

            public Tuple2<Iterator<Tuple2<Object,RelationshipContainer>>,Iterator<Tuple2<Object,RelationshipContainer>>> partition(
                    final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.partition$( this, p );
            }

            public Tuple2<Iterator<Tuple2<Object,RelationshipContainer>>,Iterator<Tuple2<Object,RelationshipContainer>>> span(
                    final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.span$( this, p );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> dropWhile( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.dropWhile$( this, p );
            }

            public <B> Iterator<Tuple2<Tuple2<Object,RelationshipContainer>,B>> zip( final Iterator<B> that )
            {
                return Iterator.zip$( this, that );
            }

            public <A1> Iterator<A1> padTo( final int len, final A1 elem )
            {
                return Iterator.padTo$( this, len, elem );
            }

            public Iterator<Tuple2<Tuple2<Object,RelationshipContainer>,Object>> zipWithIndex()
            {
                return Iterator.zipWithIndex$( this );
            }

            public <B, A1, B1> Iterator<Tuple2<A1,B1>> zipAll( final Iterator<B> that, final A1 thisElem, final B1 thatElem )
            {
                return Iterator.zipAll$( this, that, thisElem, thatElem );
            }

            public <U> void foreach( final Function1<Tuple2<Object,RelationshipContainer>,U> f )
            {
                Iterator.foreach$( this, f );
            }

            public boolean forall( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.forall$( this, p );
            }

            public boolean exists( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.exists$( this, p );
            }

            public boolean contains( final Object elem )
            {
                return Iterator.contains$( this, elem );
            }

            public Option<Tuple2<Object,RelationshipContainer>> find( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.find$( this, p );
            }

            public int indexWhere( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return Iterator.indexWhere$( this, p );
            }

            public int indexWhere( final Function1<Tuple2<Object,RelationshipContainer>,Object> p, final int from )
            {
                return Iterator.indexWhere$( this, p, from );
            }

            public <B> int indexOf( final B elem )
            {
                return Iterator.indexOf$( this, elem );
            }

            public <B> int indexOf( final B elem, final int from )
            {
                return Iterator.indexOf$( this, elem, from );
            }

            public BufferedIterator<Tuple2<Object,RelationshipContainer>> buffered()
            {
                return Iterator.buffered$( this );
            }

            public <B> Iterator<Tuple2<Object,RelationshipContainer>>.GroupedIterator<B> grouped( final int size )
            {
                return Iterator.grouped$( this, size );
            }

            public <B> Iterator<Tuple2<Object,RelationshipContainer>>.GroupedIterator<B> sliding( final int size, final int step )
            {
                return Iterator.sliding$( this, size, step );
            }

            public int length()
            {
                return Iterator.length$( this );
            }

            public Tuple2<Iterator<Tuple2<Object,RelationshipContainer>>,Iterator<Tuple2<Object,RelationshipContainer>>> duplicate()
            {
                return Iterator.duplicate$( this );
            }

            public <B> Iterator<B> patch( final int from, final Iterator<B> patchElems, final int replaced )
            {
                return Iterator.patch$( this, from, patchElems, replaced );
            }

            public <B> void copyToArray( final Object xs, final int start, final int len )
            {
                Iterator.copyToArray$( this, xs, start, len );
            }

            public boolean sameElements( final Iterator<?> that )
            {
                return Iterator.sameElements$( this, that );
            }

            public Traversable<Tuple2<Object,RelationshipContainer>> toTraversable()
            {
                return Iterator.toTraversable$( this );
            }

            public Iterator<Tuple2<Object,RelationshipContainer>> toIterator()
            {
                return Iterator.toIterator$( this );
            }

            public Stream<Tuple2<Object,RelationshipContainer>> toStream()
            {
                return Iterator.toStream$( this );
            }

            public String toString()
            {
                return Iterator.toString$( this );
            }

            public <B> int sliding$default$2()
            {
                return Iterator.sliding$default$2$( this );
            }

            public List<Tuple2<Object,RelationshipContainer>> reversed()
            {
                return TraversableOnce.reversed$( this );
            }

            public int size()
            {
                return TraversableOnce.size$( this );
            }

            public boolean nonEmpty()
            {
                return TraversableOnce.nonEmpty$( this );
            }

            public int count( final Function1<Tuple2<Object,RelationshipContainer>,Object> p )
            {
                return TraversableOnce.count$( this, p );
            }

            public <B> Option<B> collectFirst( final PartialFunction<Tuple2<Object,RelationshipContainer>,B> pf )
            {
                return TraversableOnce.collectFirst$( this, pf );
            }

            public <B> B $div$colon( final B z, final Function2<B,Tuple2<Object,RelationshipContainer>,B> op )
            {
                return TraversableOnce.$div$colon$( this, z, op );
            }

            public <B> B $colon$bslash( final B z, final Function2<Tuple2<Object,RelationshipContainer>,B,B> op )
            {
                return TraversableOnce.$colon$bslash$( this, z, op );
            }

            public <B> B foldLeft( final B z, final Function2<B,Tuple2<Object,RelationshipContainer>,B> op )
            {
                return TraversableOnce.foldLeft$( this, z, op );
            }

            public <B> B foldRight( final B z, final Function2<Tuple2<Object,RelationshipContainer>,B,B> op )
            {
                return TraversableOnce.foldRight$( this, z, op );
            }

            public <B> B reduceLeft( final Function2<B,Tuple2<Object,RelationshipContainer>,B> op )
            {
                return TraversableOnce.reduceLeft$( this, op );
            }

            public <B> B reduceRight( final Function2<Tuple2<Object,RelationshipContainer>,B,B> op )
            {
                return TraversableOnce.reduceRight$( this, op );
            }

            public <B> Option<B> reduceLeftOption( final Function2<B,Tuple2<Object,RelationshipContainer>,B> op )
            {
                return TraversableOnce.reduceLeftOption$( this, op );
            }

            public <B> Option<B> reduceRightOption( final Function2<Tuple2<Object,RelationshipContainer>,B,B> op )
            {
                return TraversableOnce.reduceRightOption$( this, op );
            }

            public <A1> A1 reduce( final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.reduce$( this, op );
            }

            public <A1> Option<A1> reduceOption( final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.reduceOption$( this, op );
            }

            public <A1> A1 fold( final A1 z, final Function2<A1,A1,A1> op )
            {
                return TraversableOnce.fold$( this, z, op );
            }

            public <B> B aggregate( final Function0<B> z, final Function2<B,Tuple2<Object,RelationshipContainer>,B> seqop, final Function2<B,B,B> combop )
            {
                return TraversableOnce.aggregate$( this, z, seqop, combop );
            }

            public <B> B sum( final Numeric<B> num )
            {
                return TraversableOnce.sum$( this, num );
            }

            public <B> B product( final Numeric<B> num )
            {
                return TraversableOnce.product$( this, num );
            }

            public Object min( final Ordering cmp )
            {
                return TraversableOnce.min$( this, cmp );
            }

            public Object max( final Ordering cmp )
            {
                return TraversableOnce.max$( this, cmp );
            }

            public Object maxBy( final Function1 f, final Ordering cmp )
            {
                return TraversableOnce.maxBy$( this, f, cmp );
            }

            public Object minBy( final Function1 f, final Ordering cmp )
            {
                return TraversableOnce.minBy$( this, f, cmp );
            }

            public <B> void copyToBuffer( final Buffer<B> dest )
            {
                TraversableOnce.copyToBuffer$( this, dest );
            }

            public <B> void copyToArray( final Object xs, final int start )
            {
                TraversableOnce.copyToArray$( this, xs, start );
            }

            public <B> void copyToArray( final Object xs )
            {
                TraversableOnce.copyToArray$( this, xs );
            }

            public <B> Object toArray( final ClassTag<B> evidence$1 )
            {
                return TraversableOnce.toArray$( this, evidence$1 );
            }

            public List<Tuple2<Object,RelationshipContainer>> toList()
            {
                return TraversableOnce.toList$( this );
            }

            public Iterable<Tuple2<Object,RelationshipContainer>> toIterable()
            {
                return TraversableOnce.toIterable$( this );
            }

            public Seq<Tuple2<Object,RelationshipContainer>> toSeq()
            {
                return TraversableOnce.toSeq$( this );
            }

            public IndexedSeq<Tuple2<Object,RelationshipContainer>> toIndexedSeq()
            {
                return TraversableOnce.toIndexedSeq$( this );
            }

            public <B> Buffer<B> toBuffer()
            {
                return TraversableOnce.toBuffer$( this );
            }

            public <B> Set<B> toSet()
            {
                return TraversableOnce.toSet$( this );
            }

            public Vector<Tuple2<Object,RelationshipContainer>> toVector()
            {
                return TraversableOnce.toVector$( this );
            }

            public <Col> Col to( final CanBuildFrom<scala.runtime.Nothing .,Tuple2<Object,RelationshipContainer>,Col> cbf )
            {
                return TraversableOnce.to$( this, cbf );
            }

            public <T, U> Map<T,U> toMap( final less<Tuple2<Object,RelationshipContainer>,Tuple2<T,U>> ev )
            {
                return TraversableOnce.toMap$( this, ev );
            }

            public String mkString( final String start, final String sep, final String end )
            {
                return TraversableOnce.mkString$( this, start, sep, end );
            }

            public String mkString( final String sep )
            {
                return TraversableOnce.mkString$( this, sep );
            }

            public String mkString()
            {
                return TraversableOnce.mkString$( this );
            }

            public StringBuilder addString( final StringBuilder b, final String start, final String sep, final String end )
            {
                return TraversableOnce.addString$( this, b, start, sep, end );
            }

            public StringBuilder addString( final StringBuilder b, final String sep )
            {
                return TraversableOnce.addString$( this, b, sep );
            }

            public StringBuilder addString( final StringBuilder b )
            {
                return TraversableOnce.addString$( this, b );
            }

            public int sizeHintIfCheap()
            {
                return GenTraversableOnce.sizeHintIfCheap$( this );
            }

            public Tuple2<Object,RelationshipContainer> next()
            {
                long fromNode = this.stackOfNodes$1.pop();
                RelationshipContainer rels = (RelationshipContainer) this.stackOfRelContainers$1.pop();
                if ( rels.size() < BoxesRunTime.unboxToInt( this.$outer.maxDepth().getOrElse( () -> {
                    return Integer.MAX_VALUE;
                } ) ) )
                {
                    RelationshipIterator relationships = this.state$1.query().getRelationshipsForIdsPrimitive( fromNode, this.$outer.dir(),
                            this.$outer.types().types( this.state$1.query() ) );
                    ObjectRef relationship = ObjectRef.create( (Object) null );
                    RelationshipVisitor relVisitor = new RelationshipVisitor<InternalException>( this, relationship )
                    {
                        private final ObjectRef relationship$1;

                        public
                        {
                            if ( $outer == null )
                            {
                                throw null;
                            }
                            else
                            {
                                this.$outer = $outer;
                                this.relationship$1 = relationship$1;
                            }
                        }

                        public void visit( final long relationshipId, final int typeId, final long startNodeId, final long endNodeId )
                        {
                            this.relationship$1.elem = this.$outer.state$1.query().relationshipById( relationshipId, startNodeId, endNodeId, typeId );
                        }
                    };

                    while ( relationships.hasNext() )
                    {
                        long relId = relationships.next();
                        relationships.relationshipVisit( relId, relVisitor );
                        boolean relationshipIsUniqueInPath = !rels.contains( (RelationshipValue) relationship.elem );
                        if ( relationshipIsUniqueInPath &&
                                this.$outer.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$predicateIsTrue( this.row$1,
                                        this.state$1, this.$outer.tempRelationshipOffset(), this.$outer.relationshipPredicate(),
                                        this.state$1.query().relationshipById( relId ) ) &&
                                this.$outer.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$predicateIsTrue( this.row$1,
                                        this.state$1, this.$outer.tempNodeOffset(), this.$outer.nodePredicate(),
                                        this.state$1.query().nodeById( ((RelationshipValue) relationship.elem).otherNodeId( fromNode ) ) ) )
                        {
                            this.stackOfNodes$1.push( ((RelationshipValue) relationship.elem).otherNodeId( fromNode ) );
                            this.stackOfRelContainers$1.push( rels.append( (RelationshipValue) relationship.elem ) );
                        }
                    }
                }

                RelationshipContainer projectedRels = org.neo4j.cypher.internal.runtime.interpreted.pipes.VarLengthExpandPipe..
                MODULE$.projectBackwards( this.$outer.dir(), this.$outer.projectedDir() ) ? rels.reverse() : rels;
                return new Tuple2( BoxesRunTime.boxToLong( fromNode ), projectedRels );
            }

            public boolean hasNext()
            {
                return !this.stackOfNodes$1.isEmpty();
            }
        };
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.flatMap( ( inputRow ) -> {
            long fromNode = this.getFromNodeFunction().applyAsLong( inputRow );
            Iterator var10000;
            if ( NullChecker$.MODULE$.entityIsNull( fromNode ) )
            {
                var10000 = scala.package..MODULE$.Iterator().empty();
            }
            else if ( this.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$predicateIsTrue( inputRow, state, this.tempNodeOffset(),
                    this.nodePredicate(), state.query().nodeById( fromNode ) ) )
            {
                Iterator paths = this.varLengthExpand( fromNode, state, inputRow );
                var10000 = paths.collect( new Serializable( this, inputRow )
                {
                    public static final long serialVersionUID = 0L;
                    private final ExecutionContext inputRow$1;

                    public
                    {
                        if ( $outer == null )
                        {
                            throw null;
                        }
                        else
                        {
                            this.$outer = $outer;
                            this.inputRow$1 = inputRow$1;
                        }
                    }

                    public final <A1 extends Tuple2<Object,RelationshipContainer>, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                    {
                        Object var3;
                        if ( x1 != null )
                        {
                            long toNode = x1._1$mcJ$sp();
                            RelationshipContainer rels = (RelationshipContainer) x1._2();
                            if ( rels != null && rels.size() >= this.$outer.min() &&
                                    this.$outer.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$isToNodeValid( this.inputRow$1,
                                            toNode ) )
                            {
                                SlottedExecutionContext resultRow = new SlottedExecutionContext( this.$outer.slots() );
                                resultRow.copyFrom( this.inputRow$1, this.$outer.argumentSize().nLongs(), this.$outer.argumentSize().nReferences() );
                                if ( this.$outer.shouldExpandAll() )
                                {
                                    resultRow.setLongAt( this.$outer.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$toOffset(),
                                            toNode );
                                }

                                resultRow.setRefAt( this.$outer.relOffset(), rels.asList() );
                                var3 = resultRow;
                                return var3;
                            }
                        }

                        var3 = var2.apply( x1 );
                        return var3;
                    }

                    public final boolean isDefinedAt( final Tuple2<Object,RelationshipContainer> x1 )
                    {
                        boolean var2;
                        if ( x1 != null )
                        {
                            long toNode = x1._1$mcJ$sp();
                            RelationshipContainer rels = (RelationshipContainer) x1._2();
                            if ( rels != null && rels.size() >= this.$outer.min() &&
                                    this.$outer.org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$isToNodeValid( this.inputRow$1,
                                            toNode ) )
                            {
                                var2 = true;
                                return var2;
                            }
                        }

                        var2 = false;
                        return var2;
                    }
                } );
            }
            else
            {
                var10000 = scala.package..MODULE$.Iterator().empty();
            }

            return var10000;
        } );
    }

    public boolean org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$predicateIsTrue( final ExecutionContext row,
            final QueryState state, final int tempOffset, final Expression predicate, final AnyValue entity )
    {
        boolean var10000;
        if ( tempOffset != VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET() )
        {
            state.expressionVariables()[tempOffset] = entity;
            if ( predicate.apply( row, state ) != Values.TRUE )
            {
                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }

    public boolean org$neo4j$cypher$internal$runtime$slotted$pipes$VarLengthExpandSlottedPipe$$isToNodeValid( final ExecutionContext row, final long node )
    {
        return this.shouldExpandAll() || this.getToNodeFunction().applyAsLong( row ) == node;
    }

    public VarLengthExpandSlottedPipe copy( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option<Object> maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new VarLengthExpandSlottedPipe( source, fromSlot, relOffset, toSlot, dir, projectedDir, types, min, maxDepth, shouldExpandAll, slots,
                tempNodeOffset, tempRelationshipOffset, nodePredicate, relationshipPredicate, argumentSize, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public boolean copy$default$10()
    {
        return this.shouldExpandAll();
    }

    public SlotConfiguration copy$default$11()
    {
        return this.slots();
    }

    public int copy$default$12()
    {
        return this.tempNodeOffset();
    }

    public int copy$default$13()
    {
        return this.tempRelationshipOffset();
    }

    public Expression copy$default$14()
    {
        return this.nodePredicate();
    }

    public Expression copy$default$15()
    {
        return this.relationshipPredicate();
    }

    public SlotConfiguration.Size copy$default$16()
    {
        return this.argumentSize();
    }

    public Slot copy$default$2()
    {
        return this.fromSlot();
    }

    public int copy$default$3()
    {
        return this.relOffset();
    }

    public Slot copy$default$4()
    {
        return this.toSlot();
    }

    public SemanticDirection copy$default$5()
    {
        return this.dir();
    }

    public SemanticDirection copy$default$6()
    {
        return this.projectedDir();
    }

    public RelationshipTypes copy$default$7()
    {
        return this.types();
    }

    public int copy$default$8()
    {
        return this.min();
    }

    public Option<Object> copy$default$9()
    {
        return this.maxDepth();
    }

    public String productPrefix()
    {
        return "VarLengthExpandSlottedPipe";
    }

    public int productArity()
    {
        return 16;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.fromSlot();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.relOffset() );
            break;
        case 3:
            var10000 = this.toSlot();
            break;
        case 4:
            var10000 = this.dir();
            break;
        case 5:
            var10000 = this.projectedDir();
            break;
        case 6:
            var10000 = this.types();
            break;
        case 7:
            var10000 = BoxesRunTime.boxToInteger( this.min() );
            break;
        case 8:
            var10000 = this.maxDepth();
            break;
        case 9:
            var10000 = BoxesRunTime.boxToBoolean( this.shouldExpandAll() );
            break;
        case 10:
            var10000 = this.slots();
            break;
        case 11:
            var10000 = BoxesRunTime.boxToInteger( this.tempNodeOffset() );
            break;
        case 12:
            var10000 = BoxesRunTime.boxToInteger( this.tempRelationshipOffset() );
            break;
        case 13:
            var10000 = this.nodePredicate();
            break;
        case 14:
            var10000 = this.relationshipPredicate();
            break;
        case 15:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof VarLengthExpandSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.source() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fromSlot() ) );
        var1 = Statics.mix( var1, this.relOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.toSlot() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.dir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.projectedDir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.types() ) );
        var1 = Statics.mix( var1, this.min() );
        var1 = Statics.mix( var1, Statics.anyHash( this.maxDepth() ) );
        var1 = Statics.mix( var1, this.shouldExpandAll() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, this.tempNodeOffset() );
        var1 = Statics.mix( var1, this.tempRelationshipOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.nodePredicate() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.relationshipPredicate() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.argumentSize() ) );
        return Statics.finalizeHash( var1, 16 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var23;
        if ( this != x$1 )
        {
            label156:
            {
                boolean var2;
                if ( x$1 instanceof VarLengthExpandSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label136:
                    {
                        label145:
                        {
                            VarLengthExpandSlottedPipe var4 = (VarLengthExpandSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label145;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label145;
                            }

                            Slot var16 = this.fromSlot();
                            Slot var6 = var4.fromSlot();
                            if ( var16 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label145;
                                }
                            }
                            else if ( !var16.equals( var6 ) )
                            {
                                break label145;
                            }

                            if ( this.relOffset() == var4.relOffset() )
                            {
                                label146:
                                {
                                    var16 = this.toSlot();
                                    Slot var7 = var4.toSlot();
                                    if ( var16 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label146;
                                        }
                                    }
                                    else if ( !var16.equals( var7 ) )
                                    {
                                        break label146;
                                    }

                                    SemanticDirection var17 = this.dir();
                                    SemanticDirection var8 = var4.dir();
                                    if ( var17 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label146;
                                        }
                                    }
                                    else if ( !var17.equals( var8 ) )
                                    {
                                        break label146;
                                    }

                                    var17 = this.projectedDir();
                                    SemanticDirection var9 = var4.projectedDir();
                                    if ( var17 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label146;
                                        }
                                    }
                                    else if ( !var17.equals( var9 ) )
                                    {
                                        break label146;
                                    }

                                    RelationshipTypes var18 = this.types();
                                    RelationshipTypes var10 = var4.types();
                                    if ( var18 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label146;
                                        }
                                    }
                                    else if ( !var18.equals( var10 ) )
                                    {
                                        break label146;
                                    }

                                    if ( this.min() == var4.min() )
                                    {
                                        label103:
                                        {
                                            Option var19 = this.maxDepth();
                                            Option var11 = var4.maxDepth();
                                            if ( var19 == null )
                                            {
                                                if ( var11 != null )
                                                {
                                                    break label103;
                                                }
                                            }
                                            else if ( !var19.equals( var11 ) )
                                            {
                                                break label103;
                                            }

                                            if ( this.shouldExpandAll() == var4.shouldExpandAll() )
                                            {
                                                label97:
                                                {
                                                    SlotConfiguration var20 = this.slots();
                                                    SlotConfiguration var12 = var4.slots();
                                                    if ( var20 == null )
                                                    {
                                                        if ( var12 != null )
                                                        {
                                                            break label97;
                                                        }
                                                    }
                                                    else if ( !var20.equals( var12 ) )
                                                    {
                                                        break label97;
                                                    }

                                                    if ( this.tempNodeOffset() == var4.tempNodeOffset() &&
                                                            this.tempRelationshipOffset() == var4.tempRelationshipOffset() )
                                                    {
                                                        label147:
                                                        {
                                                            Expression var21 = this.nodePredicate();
                                                            Expression var13 = var4.nodePredicate();
                                                            if ( var21 == null )
                                                            {
                                                                if ( var13 != null )
                                                                {
                                                                    break label147;
                                                                }
                                                            }
                                                            else if ( !var21.equals( var13 ) )
                                                            {
                                                                break label147;
                                                            }

                                                            var21 = this.relationshipPredicate();
                                                            Expression var14 = var4.relationshipPredicate();
                                                            if ( var21 == null )
                                                            {
                                                                if ( var14 != null )
                                                                {
                                                                    break label147;
                                                                }
                                                            }
                                                            else if ( !var21.equals( var14 ) )
                                                            {
                                                                break label147;
                                                            }

                                                            SlotConfiguration.Size var22 = this.argumentSize();
                                                            SlotConfiguration.Size var15 = var4.argumentSize();
                                                            if ( var22 == null )
                                                            {
                                                                if ( var15 != null )
                                                                {
                                                                    break label147;
                                                                }
                                                            }
                                                            else if ( !var22.equals( var15 ) )
                                                            {
                                                                break label147;
                                                            }

                                                            if ( var4.canEqual( this ) )
                                                            {
                                                                var23 = true;
                                                                break label136;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        var23 = false;
                    }

                    if ( var23 )
                    {
                        break label156;
                    }
                }

                var23 = false;
                return var23;
            }
        }

        var23 = true;
        return var23;
    }
}
