package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple6;
import scala.collection.Seq;
import scala.runtime.BoxesRunTime;

public final class ConditionalApplySlottedPipe$ implements Serializable
{
    public static ConditionalApplySlottedPipe$ MODULE$;

    static
    {
        new ConditionalApplySlottedPipe$();
    }

    private ConditionalApplySlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$7( final Pipe lhs, final Pipe rhs, final Seq<Object> longOffsets, final Seq<Object> refOffsets, final boolean negated,
            final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "ConditionalApplySlottedPipe";
    }

    public ConditionalApplySlottedPipe apply( final Pipe lhs, final Pipe rhs, final Seq<Object> longOffsets, final Seq<Object> refOffsets,
            final boolean negated, final SlotConfiguration slots, final int id )
    {
        return new ConditionalApplySlottedPipe( lhs, rhs, longOffsets, refOffsets, negated, slots, id );
    }

    public int apply$default$7( final Pipe lhs, final Pipe rhs, final Seq<Object> longOffsets, final Seq<Object> refOffsets, final boolean negated,
            final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple6<Pipe,Pipe,Seq<Object>,Seq<Object>,Object,SlotConfiguration>> unapply( final ConditionalApplySlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :
        new Some( new Tuple6( x$0.lhs(), x$0.rhs(), x$0.longOffsets(), x$0.refOffsets(), BoxesRunTime.boxToBoolean( x$0.negated() ), x$0.slots() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
