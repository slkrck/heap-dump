package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple6;
import scala.collection.immutable.Set;
import scala.runtime.BoxesRunTime;

public final class RollUpApplySlottedPipe$ implements Serializable
{
    public static RollUpApplySlottedPipe$ MODULE$;

    static
    {
        new RollUpApplySlottedPipe$();
    }

    private RollUpApplySlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$7( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset,
            final Tuple2<String,Expression> identifierToCollect, final Set<String> nullableIdentifiers, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "RollUpApplySlottedPipe";
    }

    public RollUpApplySlottedPipe apply( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset, final Tuple2<String,Expression> identifierToCollect,
            final Set<String> nullableIdentifiers, final SlotConfiguration slots, final int id )
    {
        return new RollUpApplySlottedPipe( lhs, rhs, collectionRefSlotOffset, identifierToCollect, nullableIdentifiers, slots, id );
    }

    public int apply$default$7( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset, final Tuple2<String,Expression> identifierToCollect,
            final Set<String> nullableIdentifiers, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple6<Pipe,Pipe,Object,Tuple2<String,Expression>,Set<String>,SlotConfiguration>> unapply( final RollUpApplySlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple6( x$0.lhs(), x$0.rhs(), BoxesRunTime.boxToInteger( x$0.collectionRefSlotOffset() ), x$0.identifierToCollect(), x$0.nullableIdentifiers(),
                    x$0.slots() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
