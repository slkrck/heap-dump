package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class EmptyGroupingExpression
{
    public static String toString()
    {
        return EmptyGroupingExpression$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return EmptyGroupingExpression$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return EmptyGroupingExpression$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return EmptyGroupingExpression$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return EmptyGroupingExpression$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return EmptyGroupingExpression$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return EmptyGroupingExpression$.MODULE$.productPrefix();
    }

    public static void project( final ExecutionContext context, final AnyValue groupingKey )
    {
        EmptyGroupingExpression$.MODULE$.project( var0, var1 );
    }

    public static boolean isEmpty()
    {
        return EmptyGroupingExpression$.MODULE$.isEmpty();
    }

    public static AnyValue getGroupingKey( final ExecutionContext context )
    {
        return EmptyGroupingExpression$.MODULE$.getGroupingKey( var0 );
    }

    public static AnyValue computeOrderedGroupingKey( final AnyValue groupingKey )
    {
        return EmptyGroupingExpression$.MODULE$.computeOrderedGroupingKey( var0 );
    }

    public static AnyValue computeGroupingKey( final ExecutionContext context, final QueryState state )
    {
        return EmptyGroupingExpression$.MODULE$.computeGroupingKey( var0, var1 );
    }

    public static void registerOwningPipe( final Pipe pipe )
    {
        EmptyGroupingExpression$.MODULE$.registerOwningPipe( var0 );
    }
}
