package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;

public final class ForeachSlottedPipe$ implements Serializable
{
    public static ForeachSlottedPipe$ MODULE$;

    static
    {
        new ForeachSlottedPipe$();
    }

    private ForeachSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$5( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "ForeachSlottedPipe";
    }

    public ForeachSlottedPipe apply( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression, final int id )
    {
        return new ForeachSlottedPipe( lhs, rhs, innerVariableSlot, expression, id );
    }

    public int apply$default$5( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple4<Pipe,Pipe,Slot,Expression>> unapply( final ForeachSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple4( x$0.lhs(), x$0.rhs(), x$0.innerVariableSlot(), x$0.expression() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
