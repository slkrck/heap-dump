package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class EagerSlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final SlotConfiguration slots;
    private final int id;

    public EagerSlottedPipe( final Pipe source, final SlotConfiguration slots, final int id )
    {
        super( source );
        this.source = source;
        this.slots = slots;
        this.id = id;
        Product.$init$( this );
    }

    public static int $lessinit$greater$default$3( final Pipe source, final SlotConfiguration slots )
    {
        return EagerSlottedPipe$.MODULE$.$lessinit$greater$default$3( var0, var1 );
    }

    public static int apply$default$3( final Pipe source, final SlotConfiguration slots )
    {
        return EagerSlottedPipe$.MODULE$.apply$default$3( var0, var1 );
    }

    public static Option<Tuple2<Pipe,SlotConfiguration>> unapply( final EagerSlottedPipe x$0 )
    {
        return EagerSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static EagerSlottedPipe apply( final Pipe source, final SlotConfiguration slots, final int id )
    {
        return EagerSlottedPipe$.MODULE$.apply( var0, var1, var2 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return state.memoryTracker().memoryTrackingIterator( input.map( ( inputRow ) -> {
            SlottedExecutionContext outputRow = new SlottedExecutionContext( this.slots() );
            inputRow.copyTo( outputRow, inputRow.copyTo$default$2(), inputRow.copyTo$default$3(), inputRow.copyTo$default$4(), inputRow.copyTo$default$5() );
            return outputRow;
        } ) ).toIndexedSeq().iterator();
    }

    public EagerSlottedPipe copy( final Pipe source, final SlotConfiguration slots, final int id )
    {
        return new EagerSlottedPipe( source, slots, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public SlotConfiguration copy$default$2()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "EagerSlottedPipe";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.slots();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EagerSlottedPipe;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof EagerSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            EagerSlottedPipe var4 = (EagerSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            SlotConfiguration var7 = this.slots();
                            SlotConfiguration var6 = var4.slots();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
