package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.BaseCreatePipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.exceptions.InternalException;
import org.neo4j.values.virtual.RelationshipValue;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class EntityCreateSlottedPipe extends BaseCreatePipe
{
    public EntityCreateSlottedPipe( final Pipe source )
    {
        super( source );
    }

    private static final long handleMissingNode$1( final String nodeName, final QueryState state$2, final CreateRelationshipSlottedCommand command$1 )
    {
        if ( state$2.lenientCreateRelationship() )
        {
            return -1L;
        }
        else
        {
            throw new InternalException( org.neo4j.cypher.internal.runtime.LenientCreateRelationship..MODULE$.errorMsg( command$1.relName(), nodeName ));
        }
    }

    public long createNode( final ExecutionContext context, final QueryState state, final CreateNodeSlottedCommand command )
    {
        int[] labelIds = (int[]) ((TraversableOnce) command.labels().map( ( x$1 ) -> {
            return BoxesRunTime.boxToInteger( $anonfun$createNode$1( state, x$1 ) );
        },.MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int());
        long nodeId = state.query().createNodeId( labelIds );
        command.properties().foreach( ( x$2 ) -> {
            $anonfun$createNode$2( this, context, state, nodeId, x$2 );
            return BoxedUnit.UNIT;
        } );
        return nodeId;
    }

    public long createRelationship( final ExecutionContext context, final QueryState state, final CreateRelationshipSlottedCommand command )
    {
        long startNodeId = command.startNodeIdGetter().applyAsLong( context );
        long endNodeId = command.endNodeIdGetter().applyAsLong( context );
        int typeId = command.relType().typ( state.query() );
        long var10000;
        if ( startNodeId == -1L )
        {
            var10000 = handleMissingNode$1( command.startName(), state, command );
        }
        else if ( endNodeId == -1L )
        {
            var10000 = handleMissingNode$1( command.endName(), state, command );
        }
        else
        {
            RelationshipValue relationship = state.query().createRelationship( startNodeId, endNodeId, typeId );
            command.properties().foreach( ( x$3 ) -> {
                $anonfun$createRelationship$1( this, context, state, relationship, x$3 );
                return BoxedUnit.UNIT;
            } );
            var10000 = relationship.id();
        }

        return var10000;
    }
}
