package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class SlotExpression$ extends AbstractFunction3<Slot,Expression,Object,SlotExpression> implements Serializable
{
    public static SlotExpression$ MODULE$;

    static
    {
        new SlotExpression$();
    }

    private SlotExpression$()
    {
        MODULE$ = this;
    }

    public boolean $lessinit$greater$default$3()
    {
        return false;
    }

    public final String toString()
    {
        return "SlotExpression";
    }

    public SlotExpression apply( final Slot slot, final Expression expression, final boolean ordered )
    {
        return new SlotExpression( slot, expression, ordered );
    }

    public boolean apply$default$3()
    {
        return false;
    }

    public Option<Tuple3<Slot,Expression,Object>> unapply( final SlotExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.slot(), x$0.expression(), BoxesRunTime.boxToBoolean( x$0.ordered() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
