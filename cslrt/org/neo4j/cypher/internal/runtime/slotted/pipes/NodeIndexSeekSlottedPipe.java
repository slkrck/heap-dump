package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.logical.plans.MinMaxOrdering;
import org.neo4j.cypher.internal.logical.plans.QueryExpression;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekMode;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.NodeIndexSeeker;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.v4_0.expressions.LabelToken;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.internal.kernel.api.IndexReadSession;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.internal.kernel.api.IndexQuery.ExactPredicate;
import org.neo4j.values.storable.Value;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple9;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.Seq.;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeIndexSeekSlottedPipe implements NodeIndexSeeker, IndexSlottedPipeWithValues, Product, Serializable
{
    private final String ident;
    private final LabelToken label;
    private final IndexedSeq<SlottedIndexedProperty> properties;
    private final int queryIndexId;
    private final QueryExpression<Expression> valueExpr;
    private final IndexSeekMode indexMode;
    private final IndexOrder indexOrder;
    private final SlotConfiguration slots;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private final int offset;
    private final int[] propertyIds;
    private final int[] indexPropertyIndices;
    private final int[] indexPropertySlotOffsets;
    private final boolean needsValues;
    private final MinMaxOrdering<Value> org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE;
    private ExecutionContextFactory executionContextFactory;

    public NodeIndexSeekSlottedPipe( final String ident, final LabelToken label, final IndexedSeq<SlottedIndexedProperty> properties, final int queryIndexId,
            final QueryExpression<Expression> valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        this.ident = ident;
        this.label = label;
        this.properties = properties;
        this.queryIndexId = queryIndexId;
        this.valueExpr = valueExpr;
        this.indexMode = indexMode;
        this.indexOrder = indexOrder;
        this.slots = slots;
        this.argumentSize = argumentSize;
        this.id = id;
        Pipe.$init$( this );
        NodeIndexSeeker.$init$( this );
        IndexSlottedPipeWithValues.$init$( this );
        Product.$init$( this );
        this.offset = slots.getLongOffsetFor( ident );
        this.propertyIds = (int[]) ((TraversableOnce) properties.map( ( x$1 ) -> {
            return BoxesRunTime.boxToInteger( $anonfun$propertyIds$1( x$1 ) );
        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int());
        this.indexPropertyIndices = (int[]) ((TraversableOnce) ((TraversableLike) ((TraversableLike) properties.zipWithIndex(
                scala.collection.IndexedSeq..MODULE$.canBuildFrom())).filter( ( x$2 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$indexPropertyIndices$1( x$2 ) );
        } )).map( ( x$3 ) -> {
            return BoxesRunTime.boxToInteger( $anonfun$indexPropertyIndices$2( x$3 ) );
        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int());
        this.indexPropertySlotOffsets = (int[]) ((TraversableOnce) ((TraversableLike) properties.map( ( x$4 ) -> {
            return x$4.maybeCachedNodePropertySlot();
        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).collect( new Serializable( (NodeIndexSeekSlottedPipe) null )
        {
            public static final long serialVersionUID = 0L;

            public final <A1 extends Option<Object>, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x1 instanceof Some )
                {
                    Some var5 = (Some) x1;
                    int o = BoxesRunTime.unboxToInt( var5.value() );
                    var3 = BoxesRunTime.boxToInteger( o );
                }
                else
                {
                    var3 = var2.apply( x1 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final Option<Object> x1 )
            {
                boolean var2;
                if ( x1 instanceof Some )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        }, scala.collection.IndexedSeq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int());
        this.needsValues = (new ofInt( scala.Predef..MODULE$.intArrayOps( this.indexPropertyIndices() ))).nonEmpty();
        valueExpr.expressions().foreach( ( x$5 ) -> {
            $anonfun$new$1( this, x$5 );
            return BoxedUnit.UNIT;
        } );
    }

    public static int $lessinit$greater$default$10( final String ident, final LabelToken label, final IndexedSeq properties, final int queryIndexId,
            final QueryExpression valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return NodeIndexSeekSlottedPipe$.MODULE$.$lessinit$greater$default$10( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public static IndexSeekMode $lessinit$greater$default$6()
    {
        return NodeIndexSeekSlottedPipe$.MODULE$.$lessinit$greater$default$6();
    }

    public static int apply$default$10( final String ident, final LabelToken label, final IndexedSeq properties, final int queryIndexId,
            final QueryExpression valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return NodeIndexSeekSlottedPipe$.MODULE$.apply$default$10( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public static IndexSeekMode apply$default$6()
    {
        return NodeIndexSeekSlottedPipe$.MODULE$.apply$default$6();
    }

    public static Option<Tuple9<String,LabelToken,IndexedSeq<SlottedIndexedProperty>,Object,QueryExpression<Expression>,IndexSeekMode,IndexOrder,SlotConfiguration,SlotConfiguration.Size>> unapply(
            final NodeIndexSeekSlottedPipe x$0 )
    {
        return NodeIndexSeekSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static NodeIndexSeekSlottedPipe apply( final String ident, final LabelToken label, final IndexedSeq<SlottedIndexedProperty> properties,
            final int queryIndexId, final QueryExpression<Expression> valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return NodeIndexSeekSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8, var9 );
    }

    public <RESULT> NodeValueIndexCursor indexSeek( final QueryState state, final IndexReadSession index, final boolean needsValues,
            final IndexOrder indexOrder, final ExecutionContext baseContext )
    {
        return NodeIndexSeeker.indexSeek$( this, state, index, needsValues, indexOrder, baseContext );
    }

    public Seq<Seq<IndexQuery>> computeIndexQueries( final QueryState state, final ExecutionContext row )
    {
        return NodeIndexSeeker.computeIndexQueries$( this, state, row );
    }

    public Seq<Seq<ExactPredicate>> computeExactQueries( final QueryState state, final ExecutionContext row )
    {
        return NodeIndexSeeker.computeExactQueries$( this, state, row );
    }

    public Iterator<ExecutionContext> createResults( final QueryState state )
    {
        return Pipe.createResults$( this, state );
    }

    public MinMaxOrdering<Value> org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE()
    {
        return this.org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE;
    }

    public final void org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$_setter_$org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE_$eq(
            final MinMaxOrdering<Value> x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$interpreted$pipes$NodeIndexSeeker$$BY_VALUE = x$1;
    }

    public ExecutionContextFactory executionContextFactory()
    {
        return this.executionContextFactory;
    }

    public void executionContextFactory_$eq( final ExecutionContextFactory x$1 )
    {
        this.executionContextFactory = x$1;
    }

    public String ident()
    {
        return this.ident;
    }

    public LabelToken label()
    {
        return this.label;
    }

    public IndexedSeq<SlottedIndexedProperty> properties()
    {
        return this.properties;
    }

    public int queryIndexId()
    {
        return this.queryIndexId;
    }

    public QueryExpression<Expression> valueExpr()
    {
        return this.valueExpr;
    }

    public IndexSeekMode indexMode()
    {
        return this.indexMode;
    }

    public IndexOrder indexOrder()
    {
        return this.indexOrder;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    public int offset()
    {
        return this.offset;
    }

    public int[] propertyIds()
    {
        return this.propertyIds;
    }

    public int[] indexPropertyIndices()
    {
        return this.indexPropertyIndices;
    }

    public int[] indexPropertySlotOffsets()
    {
        return this.indexPropertySlotOffsets;
    }

    private boolean needsValues()
    {
        return this.needsValues;
    }

    public Iterator<ExecutionContext> internalCreateResults( final QueryState state )
    {
        IndexReadSession index = state.queryIndexes()[this.queryIndexId()];
        SlottedExecutionContext context = new SlottedExecutionContext( this.slots() );
        state.copyArgumentStateTo( context, this.argumentSize().nLongs(), this.argumentSize().nReferences() );
        return new IndexSlottedPipeWithValues.SlottedIndexIterator( state, this.slots(),
                this.indexSeek( state, index, this.needsValues(), this.indexOrder(), context ) );
    }

    public boolean canEqual( final Object other )
    {
        return other instanceof NodeIndexSeekSlottedPipe;
    }

    public boolean equals( final Object other )
    {
        boolean var2;
        if ( other instanceof NodeIndexSeekSlottedPipe )
        {
            boolean var18;
            label78:
            {
                NodeIndexSeekSlottedPipe var4 = (NodeIndexSeekSlottedPipe) other;
                if ( var4.canEqual( this ) )
                {
                    label77:
                    {
                        String var10000 = this.ident();
                        String var5 = var4.ident();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label77;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label77;
                        }

                        LabelToken var12 = this.label();
                        LabelToken var6 = var4.label();
                        if ( var12 == null )
                        {
                            if ( var6 != null )
                            {
                                break label77;
                            }
                        }
                        else if ( !var12.equals( var6 ) )
                        {
                            break label77;
                        }

                        IndexedSeq var13 = this.properties();
                        IndexedSeq var7 = var4.properties();
                        if ( var13 == null )
                        {
                            if ( var7 != null )
                            {
                                break label77;
                            }
                        }
                        else if ( !var13.equals( var7 ) )
                        {
                            break label77;
                        }

                        QueryExpression var14 = this.valueExpr();
                        QueryExpression var8 = var4.valueExpr();
                        if ( var14 == null )
                        {
                            if ( var8 != null )
                            {
                                break label77;
                            }
                        }
                        else if ( !var14.equals( var8 ) )
                        {
                            break label77;
                        }

                        IndexSeekMode var15 = this.indexMode();
                        IndexSeekMode var9 = var4.indexMode();
                        if ( var15 == null )
                        {
                            if ( var9 != null )
                            {
                                break label77;
                            }
                        }
                        else if ( !var15.equals( var9 ) )
                        {
                            break label77;
                        }

                        SlotConfiguration var16 = this.slots();
                        SlotConfiguration var10 = var4.slots();
                        if ( var16 == null )
                        {
                            if ( var10 != null )
                            {
                                break label77;
                            }
                        }
                        else if ( !var16.equals( var10 ) )
                        {
                            break label77;
                        }

                        SlotConfiguration.Size var17 = this.argumentSize();
                        SlotConfiguration.Size var11 = var4.argumentSize();
                        if ( var17 == null )
                        {
                            if ( var11 != null )
                            {
                                break label77;
                            }
                        }
                        else if ( !var17.equals( var11 ) )
                        {
                            break label77;
                        }

                        var18 = true;
                        break label78;
                    }
                }

                var18 = false;
            }

            var2 = var18;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public int hashCode()
    {
        Seq state = (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                new Object[]{this.ident(), this.label(), this.properties(), this.valueExpr(), this.indexMode(), this.slots(), this.argumentSize()} ));
        return BoxesRunTime.unboxToInt( ((TraversableOnce) state.map( ( x$6 ) -> {
            return BoxesRunTime.boxToInteger( $anonfun$hashCode$1( x$6 ) );
        },.MODULE$.canBuildFrom()) ).foldLeft( BoxesRunTime.boxToInteger( 0 ), ( a, b ) -> {
            return 31 * a + b;
        } ));
    }

    public NodeIndexSeekSlottedPipe copy( final String ident, final LabelToken label, final IndexedSeq<SlottedIndexedProperty> properties,
            final int queryIndexId, final QueryExpression<Expression> valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new NodeIndexSeekSlottedPipe( ident, label, properties, queryIndexId, valueExpr, indexMode, indexOrder, slots, argumentSize, id );
    }

    public String copy$default$1()
    {
        return this.ident();
    }

    public LabelToken copy$default$2()
    {
        return this.label();
    }

    public IndexedSeq<SlottedIndexedProperty> copy$default$3()
    {
        return this.properties();
    }

    public int copy$default$4()
    {
        return this.queryIndexId();
    }

    public QueryExpression<Expression> copy$default$5()
    {
        return this.valueExpr();
    }

    public IndexSeekMode copy$default$6()
    {
        return this.indexMode();
    }

    public IndexOrder copy$default$7()
    {
        return this.indexOrder();
    }

    public SlotConfiguration copy$default$8()
    {
        return this.slots();
    }

    public SlotConfiguration.Size copy$default$9()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "NodeIndexSeekSlottedPipe";
    }

    public int productArity()
    {
        return 9;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.ident();
            break;
        case 1:
            var10000 = this.label();
            break;
        case 2:
            var10000 = this.properties();
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.queryIndexId() );
            break;
        case 4:
            var10000 = this.valueExpr();
            break;
        case 5:
            var10000 = this.indexMode();
            break;
        case 6:
            var10000 = this.indexOrder();
            break;
        case 7:
            var10000 = this.slots();
            break;
        case 8:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }
}
