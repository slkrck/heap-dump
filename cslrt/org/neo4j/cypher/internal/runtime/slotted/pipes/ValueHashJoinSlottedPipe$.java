package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple8;
import scala.runtime.BoxesRunTime;

public final class ValueHashJoinSlottedPipe$ implements Serializable
{
    public static ValueHashJoinSlottedPipe$ MODULE$;

    static
    {
        new ValueHashJoinSlottedPipe$();
    }

    private ValueHashJoinSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$9( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right,
            final SlotConfiguration slots, final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "ValueHashJoinSlottedPipe";
    }

    public ValueHashJoinSlottedPipe apply( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right,
            final SlotConfiguration slots, final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new ValueHashJoinSlottedPipe( leftSide, rightSide, left, right, slots, longOffset, refsOffset, argumentSize, id );
    }

    public int apply$default$9( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple8<Expression,Expression,Pipe,Pipe,SlotConfiguration,Object,Object,SlotConfiguration.Size>> unapply( final ValueHashJoinSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple8( x$0.leftSide(), x$0.rightSide(), x$0.left(), x$0.right(), x$0.slots(), BoxesRunTime.boxToInteger( x$0.longOffset() ),
                    BoxesRunTime.boxToInteger( x$0.refsOffset() ), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
