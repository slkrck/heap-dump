package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;

public final class EagerSlottedPipe$ implements Serializable
{
    public static EagerSlottedPipe$ MODULE$;

    static
    {
        new EagerSlottedPipe$();
    }

    private EagerSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$3( final Pipe source, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "EagerSlottedPipe";
    }

    public EagerSlottedPipe apply( final Pipe source, final SlotConfiguration slots, final int id )
    {
        return new EagerSlottedPipe( source, slots, id );
    }

    public int apply$default$3( final Pipe source, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple2<Pipe,SlotConfiguration>> unapply( final EagerSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.source(), x$0.slots() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
