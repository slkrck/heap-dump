package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.collection.Seq;

public final class OptionalSlottedPipe$ implements Serializable
{
    public static OptionalSlottedPipe$ MODULE$;

    static
    {
        new OptionalSlottedPipe$();
    }

    private OptionalSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$5( final Pipe source, final Seq<Slot> nullableSlots, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "OptionalSlottedPipe";
    }

    public OptionalSlottedPipe apply( final Pipe source, final Seq<Slot> nullableSlots, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        return new OptionalSlottedPipe( source, nullableSlots, slots, argumentSize, id );
    }

    public int apply$default$5( final Pipe source, final Seq<Slot> nullableSlots, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple4<Pipe,Seq<Slot>,SlotConfiguration,SlotConfiguration.Size>> unapply( final OptionalSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple4( x$0.source(), x$0.nullableSlots(), x$0.slots(), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
