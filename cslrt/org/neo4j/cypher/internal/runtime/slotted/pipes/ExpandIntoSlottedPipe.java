package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.function.ToLongFunction;

import org.eclipse.collections.api.iterator.LongIterator;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrimitiveLongHelper.;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Function0;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple7;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class ExpandIntoSlottedPipe extends PipeWithSource implements PrimitiveCachingExpandInto, Product, Serializable
{
    private final Pipe source;
    private final Slot fromSlot;
    private final int relOffset;
    private final Slot toSlot;
    private final SemanticDirection dir;
    private final RelationshipTypes lazyTypes;
    private final SlotConfiguration slots;
    private final int id;
    private final ToLongFunction<ExecutionContext> getFromNodeFunction;
    private final ToLongFunction<ExecutionContext> getToNodeFunction;
    private boolean org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState;

    public ExpandIntoSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots, final int id )
    {
        super( source );
        this.source = source;
        this.fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.toSlot = toSlot;
        this.dir = dir;
        this.lazyTypes = lazyTypes;
        this.slots = slots;
        this.id = id;
        PrimitiveCachingExpandInto.$init$( this );
        Product.$init$( this );
        this.getFromNodeFunction = SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( fromSlot,
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() );
        this.getToNodeFunction = SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( toSlot,
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() );
    }

    public static int $lessinit$greater$default$8( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots )
    {
        return ExpandIntoSlottedPipe$.MODULE$.$lessinit$greater$default$8( var0, var1, var2, var3, var4, var5, var6 );
    }

    public static int apply$default$8( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots )
    {
        return ExpandIntoSlottedPipe$.MODULE$.apply$default$8( var0, var1, var2, var3, var4, var5, var6 );
    }

    public static Option<Tuple7<Pipe,Slot,Object,Slot,SemanticDirection,RelationshipTypes,SlotConfiguration>> unapply( final ExpandIntoSlottedPipe x$0 )
    {
        return ExpandIntoSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ExpandIntoSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots, final int id )
    {
        return ExpandIntoSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public LongIterator findRelationships( final QueryState state, final long fromNode, final long toNode, final PrimitiveRelationshipsCache relCache,
            final SemanticDirection dir, final Function0<int[]> relTypes )
    {
        return PrimitiveCachingExpandInto.findRelationships$( this, state, fromNode, toNode, relCache, dir, relTypes );
    }

    public boolean org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState()
    {
        return this.org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState;
    }

    public void org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState_$eq( final boolean x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState = x$1;
    }

    public Pipe source()
    {
        return this.source;
    }

    public Slot fromSlot()
    {
        return this.fromSlot;
    }

    public int relOffset()
    {
        return this.relOffset;
    }

    public Slot toSlot()
    {
        return this.toSlot;
    }

    public SemanticDirection dir()
    {
        return this.dir;
    }

    public RelationshipTypes lazyTypes()
    {
        return this.lazyTypes;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int id()
    {
        return this.id;
    }

    private final int CACHE_SIZE()
    {
        return 100000;
    }

    private ToLongFunction<ExecutionContext> getFromNodeFunction()
    {
        return this.getFromNodeFunction;
    }

    private ToLongFunction<ExecutionContext> getToNodeFunction()
    {
        return this.getToNodeFunction;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        PrimitiveRelationshipsCache relCache = new PrimitiveRelationshipsCache( 100000, state.memoryTracker() );
        return input.flatMap( ( inputRow ) -> {
            long fromNode = this.getFromNodeFunction().applyAsLong( inputRow );
            long toNode = this.getToNodeFunction().applyAsLong( inputRow );
            Iterator var10000;
            if ( !NullChecker$.MODULE$.entityIsNull( fromNode ) && !NullChecker$.MODULE$.entityIsNull( toNode ) )
            {
                LongIterator relationships = (LongIterator) relCache.get( fromNode, toNode, this.dir() ).getOrElse( () -> {
                    return this.findRelationships( state, fromNode, toNode, relCache, this.dir(), () -> {
                        return this.lazyTypes().types( state.query() );
                    } );
                } );
                var10000 = .MODULE$.map( relationships, ( relId ) -> {
                return $anonfun$internalCreateResults$4( this, inputRow, BoxesRunTime.unboxToLong( relId ) );
            } );
            }
            else
            {
                var10000 = scala.package..MODULE$.Iterator().empty();
            }

            return var10000;
        } );
    }

    public ExpandIntoSlottedPipe copy( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots, final int id )
    {
        return new ExpandIntoSlottedPipe( source, fromSlot, relOffset, toSlot, dir, lazyTypes, slots, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public Slot copy$default$2()
    {
        return this.fromSlot();
    }

    public int copy$default$3()
    {
        return this.relOffset();
    }

    public Slot copy$default$4()
    {
        return this.toSlot();
    }

    public SemanticDirection copy$default$5()
    {
        return this.dir();
    }

    public RelationshipTypes copy$default$6()
    {
        return this.lazyTypes();
    }

    public SlotConfiguration copy$default$7()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "ExpandIntoSlottedPipe";
    }

    public int productArity()
    {
        return 7;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.fromSlot();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.relOffset() );
            break;
        case 3:
            var10000 = this.toSlot();
            break;
        case 4:
            var10000 = this.dir();
            break;
        case 5:
            var10000 = this.lazyTypes();
            break;
        case 6:
            var10000 = this.slots();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ExpandIntoSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.source() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fromSlot() ) );
        var1 = Statics.mix( var1, this.relOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.toSlot() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.dir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.lazyTypes() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        return Statics.finalizeHash( var1, 7 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var15;
        if ( this != x$1 )
        {
            label102:
            {
                boolean var2;
                if ( x$1 instanceof ExpandIntoSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label83:
                    {
                        label92:
                        {
                            ExpandIntoSlottedPipe var4 = (ExpandIntoSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label92;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label92;
                            }

                            Slot var11 = this.fromSlot();
                            Slot var6 = var4.fromSlot();
                            if ( var11 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label92;
                                }
                            }
                            else if ( !var11.equals( var6 ) )
                            {
                                break label92;
                            }

                            if ( this.relOffset() == var4.relOffset() )
                            {
                                label93:
                                {
                                    var11 = this.toSlot();
                                    Slot var7 = var4.toSlot();
                                    if ( var11 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var11.equals( var7 ) )
                                    {
                                        break label93;
                                    }

                                    SemanticDirection var12 = this.dir();
                                    SemanticDirection var8 = var4.dir();
                                    if ( var12 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var12.equals( var8 ) )
                                    {
                                        break label93;
                                    }

                                    RelationshipTypes var13 = this.lazyTypes();
                                    RelationshipTypes var9 = var4.lazyTypes();
                                    if ( var13 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var13.equals( var9 ) )
                                    {
                                        break label93;
                                    }

                                    SlotConfiguration var14 = this.slots();
                                    SlotConfiguration var10 = var4.slots();
                                    if ( var14 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var14.equals( var10 ) )
                                    {
                                        break label93;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var15 = true;
                                        break label83;
                                    }
                                }
                            }
                        }

                        var15 = false;
                    }

                    if ( var15 )
                    {
                        break label102;
                    }
                }

                var15 = false;
                return var15;
            }
        }

        var15 = true;
        return var15;
    }
}
