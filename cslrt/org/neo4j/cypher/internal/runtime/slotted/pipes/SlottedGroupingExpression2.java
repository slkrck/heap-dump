package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlottedGroupingExpression2 implements GroupingExpression, Product, Serializable
{
    private final SlotExpression groupingExpression1;
    private final SlotExpression groupingExpression2;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> setter1;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> setter2;
    private final Function1<ExecutionContext,AnyValue> getter1;
    private final Function1<ExecutionContext,AnyValue> getter2;
    private final Function1<ListValue,AnyValue> ordered;

    public SlottedGroupingExpression2( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2 )
    {
        this.groupingExpression1 = groupingExpression1;
        this.groupingExpression2 = groupingExpression2;
        Product.$init$( this );
        this.setter1 = SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( groupingExpression1.slot() );
        this.setter2 = SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( groupingExpression2.slot() );
        this.getter1 = SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( groupingExpression1.slot() );
        this.getter2 = SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( groupingExpression2.slot() );
        this.ordered = groupingExpression1.ordered() ? (groupingExpression2.ordered() ? ( x ) -> {
            return (ListValue) scala.Predef..MODULE$.identity( x );
        } : ( l ) -> {
            return l.head();
        }) : (groupingExpression2.ordered() ? ( l ) -> {
            return l.last();
        } : ( x$2 ) -> {
            return Values.NO_VALUE;
        });
    }

    public static Option<Tuple2<SlotExpression,SlotExpression>> unapply( final SlottedGroupingExpression2 x$0 )
    {
        return SlottedGroupingExpression2$.MODULE$.unapply( var0 );
    }

    public static SlottedGroupingExpression2 apply( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2 )
    {
        return SlottedGroupingExpression2$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<SlotExpression,SlotExpression>,SlottedGroupingExpression2> tupled()
    {
        return SlottedGroupingExpression2$.MODULE$.tupled();
    }

    public static Function1<SlotExpression,Function1<SlotExpression,SlottedGroupingExpression2>> curried()
    {
        return SlottedGroupingExpression2$.MODULE$.curried();
    }

    public SlotExpression groupingExpression1()
    {
        return this.groupingExpression1;
    }

    public SlotExpression groupingExpression2()
    {
        return this.groupingExpression2;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit> setter1()
    {
        return this.setter1;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit> setter2()
    {
        return this.setter2;
    }

    private Function1<ExecutionContext,AnyValue> getter1()
    {
        return this.getter1;
    }

    private Function1<ExecutionContext,AnyValue> getter2()
    {
        return this.getter2;
    }

    private Function1<ListValue,AnyValue> ordered()
    {
        return this.ordered;
    }

    public void registerOwningPipe( final Pipe pipe )
    {
        this.groupingExpression1().expression().registerOwningPipe( pipe );
        this.groupingExpression2().expression().registerOwningPipe( pipe );
    }

    public ListValue computeGroupingKey( final ExecutionContext context, final QueryState state )
    {
        return VirtualValues.list( new AnyValue[]{this.groupingExpression1().expression().apply( context, state ),
                this.groupingExpression2().expression().apply( context, state )} );
    }

    public AnyValue computeOrderedGroupingKey( final ListValue groupingKey )
    {
        return (AnyValue) this.ordered().apply( groupingKey );
    }

    public ListValue getGroupingKey( final ExecutionContext context )
    {
        return VirtualValues.list( new AnyValue[]{(AnyValue) this.getter1().apply( context ), (AnyValue) this.getter2().apply( context )} );
    }

    public boolean isEmpty()
    {
        return false;
    }

    public void project( final ExecutionContext context, final ListValue groupingKey )
    {
        this.setter1().apply( context, groupingKey.value( 0 ) );
        this.setter2().apply( context, groupingKey.value( 1 ) );
    }

    public SlottedGroupingExpression2 copy( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2 )
    {
        return new SlottedGroupingExpression2( groupingExpression1, groupingExpression2 );
    }

    public SlotExpression copy$default$1()
    {
        return this.groupingExpression1();
    }

    public SlotExpression copy$default$2()
    {
        return this.groupingExpression2();
    }

    public String productPrefix()
    {
        return "SlottedGroupingExpression2";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        SlotExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.groupingExpression1();
            break;
        case 1:
            var10000 = this.groupingExpression2();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedGroupingExpression2;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof SlottedGroupingExpression2 )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            SlottedGroupingExpression2 var4 = (SlottedGroupingExpression2) x$1;
                            SlotExpression var10000 = this.groupingExpression1();
                            SlotExpression var5 = var4.groupingExpression1();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.groupingExpression2();
                            SlotExpression var6 = var4.groupingExpression2();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
