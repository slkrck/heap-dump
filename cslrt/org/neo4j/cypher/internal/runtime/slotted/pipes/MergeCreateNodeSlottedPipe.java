package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.exceptions.InvalidSemanticsException;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MergeCreateNodeSlottedPipe extends EntityCreateSlottedPipe implements Product, Serializable
{
    private final Pipe source;
    private final CreateNodeSlottedCommand command;
    private final int id;

    public MergeCreateNodeSlottedPipe( final Pipe source, final CreateNodeSlottedCommand command, final int id )
    {
        super( source );
        this.source = source;
        this.command = command;
        this.id = id;
        Product.$init$( this );
        command.properties().foreach( ( x$8 ) -> {
            $anonfun$new$5( this, x$8 );
            return BoxedUnit.UNIT;
        } );
    }

    public static int $lessinit$greater$default$3( final Pipe source, final CreateNodeSlottedCommand command )
    {
        return MergeCreateNodeSlottedPipe$.MODULE$.$lessinit$greater$default$3( var0, var1 );
    }

    public static int apply$default$3( final Pipe source, final CreateNodeSlottedCommand command )
    {
        return MergeCreateNodeSlottedPipe$.MODULE$.apply$default$3( var0, var1 );
    }

    public static Option<Tuple2<Pipe,CreateNodeSlottedCommand>> unapply( final MergeCreateNodeSlottedPipe x$0 )
    {
        return MergeCreateNodeSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static MergeCreateNodeSlottedPipe apply( final Pipe source, final CreateNodeSlottedCommand command, final int id )
    {
        return MergeCreateNodeSlottedPipe$.MODULE$.apply( var0, var1, var2 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public CreateNodeSlottedCommand command()
    {
        return this.command;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.map( ( row ) -> {
            row.setLongAt( this.command().idOffset(), this.createNode( row, state, this.command() ) );
            return row;
        } );
    }

    public void handleNoValue( final String key )
    {
        throw new InvalidSemanticsException( (new StringBuilder( 48 )).append( "Cannot merge node using null property value for " ).append( key ).toString() );
    }

    public MergeCreateNodeSlottedPipe copy( final Pipe source, final CreateNodeSlottedCommand command, final int id )
    {
        return new MergeCreateNodeSlottedPipe( source, command, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public CreateNodeSlottedCommand copy$default$2()
    {
        return this.command();
    }

    public String productPrefix()
    {
        return "MergeCreateNodeSlottedPipe";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.command();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MergeCreateNodeSlottedPipe;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof MergeCreateNodeSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            MergeCreateNodeSlottedPipe var4 = (MergeCreateNodeSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            CreateNodeSlottedCommand var7 = this.command();
                            CreateNodeSlottedCommand var6 = var4.command();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
