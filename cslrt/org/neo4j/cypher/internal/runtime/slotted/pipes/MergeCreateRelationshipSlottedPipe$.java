package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;

public final class MergeCreateRelationshipSlottedPipe$ implements Serializable
{
    public static MergeCreateRelationshipSlottedPipe$ MODULE$;

    static
    {
        new MergeCreateRelationshipSlottedPipe$();
    }

    private MergeCreateRelationshipSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$3( final Pipe source, final CreateRelationshipSlottedCommand command )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "MergeCreateRelationshipSlottedPipe";
    }

    public MergeCreateRelationshipSlottedPipe apply( final Pipe source, final CreateRelationshipSlottedCommand command, final int id )
    {
        return new MergeCreateRelationshipSlottedPipe( source, command, id );
    }

    public int apply$default$3( final Pipe source, final CreateRelationshipSlottedCommand command )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple2<Pipe,CreateRelationshipSlottedCommand>> unapply( final MergeCreateRelationshipSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.source(), x$0.command() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
