package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import org.neo4j.values.storable.LongArray;
import org.neo4j.values.storable.Values;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;

public final class DistinctSlottedPrimitivePipe$ implements Serializable
{
    public static DistinctSlottedPrimitivePipe$ MODULE$;

    static
    {
        new DistinctSlottedPrimitivePipe$();
    }

    private DistinctSlottedPrimitivePipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$5( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public LongArray buildGroupingValue( final ExecutionContext next, final int[] slots )
    {
        long[] keys = new long[slots.length];

        for ( int i = 0; i < slots.length; ++i )
        {
            keys[i] = next.getLongAt( slots[i] );
        }

        return Values.longArray( keys );
    }

    public DistinctSlottedPrimitivePipe apply( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression, final int id )
    {
        return new DistinctSlottedPrimitivePipe( source, slots, primitiveSlots, groupingExpression, id );
    }

    public int apply$default$5( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots, final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple4<Pipe,SlotConfiguration,int[],GroupingExpression>> unapply( final DistinctSlottedPrimitivePipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple4( x$0.source(), x$0.slots(), x$0.primitiveSlots(), x$0.groupingExpression() ) ))
        ;
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
