package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.function.ToLongFunction;

import org.eclipse.collections.api.iterator.LongIterator;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Function0;
import scala.Option;
import scala.collection.Iterator;
import scala.package.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public abstract class OptionalExpandIntoSlottedPipe extends PipeWithSource implements PrimitiveCachingExpandInto
{
    private final int relOffset;
    private final SemanticDirection dir;
    private final RelationshipTypes lazyTypes;
    private final SlotConfiguration slots;
    private final ToLongFunction<ExecutionContext> getFromNodeFunction;
    private final ToLongFunction<ExecutionContext> getToNodeFunction;
    private boolean org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState;

    public OptionalExpandIntoSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots )
    {
        super( source );
        this.relOffset = relOffset;
        this.dir = dir;
        this.lazyTypes = lazyTypes;
        this.slots = slots;
        PrimitiveCachingExpandInto.$init$( this );
        this.getFromNodeFunction = SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( fromSlot,
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() );
        this.getToNodeFunction = SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( toSlot,
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() );
    }

    public static int apply$default$9( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots, final Option maybePredicate )
    {
        return OptionalExpandIntoSlottedPipe$.MODULE$.apply$default$9( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static OptionalExpandIntoSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot,
            final SemanticDirection dir, final RelationshipTypes lazyTypes, final SlotConfiguration slots, final Option<Expression> maybePredicate,
            final int id )
    {
        return OptionalExpandIntoSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public LongIterator findRelationships( final QueryState state, final long fromNode, final long toNode, final PrimitiveRelationshipsCache relCache,
            final SemanticDirection dir, final Function0<int[]> relTypes )
    {
        return PrimitiveCachingExpandInto.findRelationships$( this, state, fromNode, toNode, relCache, dir, relTypes );
    }

    public boolean org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState()
    {
        return this.org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState;
    }

    public void org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState_$eq( final boolean x$1 )
    {
        this.org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState = x$1;
    }

    private final int CACHE_SIZE()
    {
        return 100000;
    }

    private ToLongFunction<ExecutionContext> getFromNodeFunction()
    {
        return this.getFromNodeFunction;
    }

    private ToLongFunction<ExecutionContext> getToNodeFunction()
    {
        return this.getToNodeFunction;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        PrimitiveRelationshipsCache relCache = new PrimitiveRelationshipsCache( 100000, state.memoryTracker() );
        return input.flatMap( ( inputRow ) -> {
            long fromNode = this.getFromNodeFunction().applyAsLong( inputRow );
            long toNode = this.getToNodeFunction().applyAsLong( inputRow );
            Iterator var10000;
            if ( !NullChecker$.MODULE$.entityIsNull( fromNode ) && !NullChecker$.MODULE$.entityIsNull( toNode ) )
            {
                LongIterator relationships = (LongIterator) relCache.get( fromNode, toNode, this.dir ).getOrElse( () -> {
                    return this.findRelationships( state, fromNode, toNode, relCache, this.dir, () -> {
                        return this.lazyTypes.types( state.query() );
                    } );
                } );
                Iterator matchIterator = this.findMatchIterator( inputRow, state, relationships );
                var10000 = matchIterator.isEmpty() ? .
                MODULE$.Iterator().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new SlottedExecutionContext[]{this.withNulls( inputRow )}) )) :
                matchIterator;
            }
            else
            {
                var10000 = .
                MODULE$.Iterator().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new SlottedExecutionContext[]{this.withNulls( inputRow )}) ));
            }

            return var10000;
        } );
    }

    public abstract Iterator<SlottedExecutionContext> findMatchIterator( final ExecutionContext inputRow, final QueryState state,
            final LongIterator relationships );

    private SlottedExecutionContext withNulls( final ExecutionContext inputRow )
    {
        SlottedExecutionContext outputRow = new SlottedExecutionContext( this.slots );
        inputRow.copyTo( outputRow, inputRow.copyTo$default$2(), inputRow.copyTo$default$3(), inputRow.copyTo$default$4(), inputRow.copyTo$default$5() );
        outputRow.setLongAt( this.relOffset, -1L );
        return outputRow;
    }
}
