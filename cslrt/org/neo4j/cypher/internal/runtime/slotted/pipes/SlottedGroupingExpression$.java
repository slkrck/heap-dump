package org.neo4j.cypher.internal.runtime.slotted.pipes;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class SlottedGroupingExpression$ extends AbstractFunction1<SlotExpression[],SlottedGroupingExpression> implements Serializable
{
    public static SlottedGroupingExpression$ MODULE$;

    static
    {
        new SlottedGroupingExpression$();
    }

    private SlottedGroupingExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedGroupingExpression";
    }

    public SlottedGroupingExpression apply( final SlotExpression[] sortedGroupingExpression )
    {
        return new SlottedGroupingExpression( sortedGroupingExpression );
    }

    public Option<SlotExpression[]> unapply( final SlottedGroupingExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.sortedGroupingExpression() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
