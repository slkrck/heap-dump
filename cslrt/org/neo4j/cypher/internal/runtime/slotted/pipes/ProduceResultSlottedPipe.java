package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ValuePopulation;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ProduceResultSlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final Seq<Tuple2<String,Expression>> columns;
    private final int id;
    private final Expression[] columnExpressionArray;

    public ProduceResultSlottedPipe( final Pipe source, final Seq<Tuple2<String,Expression>> columns, final int id )
    {
        super( source );
        this.source = source;
        this.columns = columns;
        this.id = id;
        Product.$init$( this );
        ((IterableLike) columns.map( ( x$1 ) -> {
            return (Expression) x$1._2();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).foreach( ( x$2 ) -> {
        $anonfun$new$2( this, x$2 );
        return BoxedUnit.UNIT;
    } );
        this.columnExpressionArray = (Expression[]) ((TraversableOnce) columns.map( ( x$3 ) -> {
            return (Expression) x$3._2();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ));
    }

    public static int $lessinit$greater$default$3( final Pipe source, final Seq columns )
    {
        return ProduceResultSlottedPipe$.MODULE$.$lessinit$greater$default$3( var0, var1 );
    }

    public static int apply$default$3( final Pipe source, final Seq columns )
    {
        return ProduceResultSlottedPipe$.MODULE$.apply$default$3( var0, var1 );
    }

    public static Option<Tuple2<Pipe,Seq<Tuple2<String,Expression>>>> unapply( final ProduceResultSlottedPipe x$0 )
    {
        return ProduceResultSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ProduceResultSlottedPipe apply( final Pipe source, final Seq<Tuple2<String,Expression>> columns, final int id )
    {
        return ProduceResultSlottedPipe$.MODULE$.apply( var0, var1, var2 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public Seq<Tuple2<String,Expression>> columns()
    {
        return this.columns;
    }

    public int id()
    {
        return this.id;
    }

    private Expression[] columnExpressionArray()
    {
        return this.columnExpressionArray;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return state.prePopulateResults() ? input.map( ( original ) -> {
            this.produceAndPopulate( original, state );
            return original;
        } ) : input.map( ( original ) -> {
            this.produce( original, state );
            return original;
        } );
    }

    private void produceAndPopulate( final ExecutionContext original, final QueryState state )
    {
        QuerySubscriber subscriber = state.subscriber();
        int i = 0;
        subscriber.onRecord();

        while ( i < this.columnExpressionArray().length )
        {
            AnyValue value = this.columnExpressionArray()[i].apply( original, state );
            ValuePopulation.populate( value );
            subscriber.onField( value );
            ++i;
        }

        subscriber.onRecordCompleted();
    }

    private void produce( final ExecutionContext original, final QueryState state )
    {
        QuerySubscriber subscriber = state.subscriber();
        int i = 0;
        subscriber.onRecord();

        while ( i < this.columnExpressionArray().length )
        {
            subscriber.onField( this.columnExpressionArray()[i].apply( original, state ) );
            ++i;
        }

        subscriber.onRecordCompleted();
    }

    public ProduceResultSlottedPipe copy( final Pipe source, final Seq<Tuple2<String,Expression>> columns, final int id )
    {
        return new ProduceResultSlottedPipe( source, columns, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public Seq<Tuple2<String,Expression>> copy$default$2()
    {
        return this.columns();
    }

    public String productPrefix()
    {
        return "ProduceResultSlottedPipe";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.columns();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ProduceResultSlottedPipe;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof ProduceResultSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            ProduceResultSlottedPipe var4 = (ProduceResultSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Seq var7 = this.columns();
                            Seq var6 = var4.columns();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
