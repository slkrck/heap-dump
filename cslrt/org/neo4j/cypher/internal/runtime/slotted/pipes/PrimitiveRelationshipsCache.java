package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.eclipse.collections.api.iterator.LongIterator;
import org.neo4j.cypher.internal.runtime.QueryMemoryTracker;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH.;
import scala.Option;
import scala.Tuple2;
import scala.Tuple2.mcJJ.sp;
import scala.collection.mutable.OpenHashMap;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public final class PrimitiveRelationshipsCache
{
    private final int capacity;
    private final QueryMemoryTracker memoryManager;
    private final OpenHashMap<Tuple2<Object,Object>,long[]> table;

    public PrimitiveRelationshipsCache( final int capacity, final QueryMemoryTracker memoryManager )
    {
        this.capacity = capacity;
        this.memoryManager = memoryManager;
        this.table = new OpenHashMap();
    }

    public OpenHashMap<Tuple2<Object,Object>,long[]> table()
    {
        return this.table;
    }

    public Option<LongIterator> get( final long start, final long end, final SemanticDirection dir )
    {
        return this.table().get( this.key( start, end, dir ) ).map( ( rels ) -> {
            return new LongIterator( (PrimitiveRelationshipsCache) null, rels )
            {
                private final long[] rels$1;
                private int index;

                public
                {
                    this.rels$1 = rels$1;
                    this.index = 0;
                }

                private int index()
                {
                    return this.index;
                }

                private void index_$eq( final int x$1 )
                {
                    this.index = x$1;
                }

                public long next()
                {
                    long r = this.rels$1[this.index()];
                    this.index_$eq( this.index() + 1 );
                    return r;
                }

                public boolean hasNext()
                {
                    return this.index() < this.rels$1.length;
                }
            };
        } );
    }

    public Object put( final long start, final long end, final long[] rels, final SemanticDirection dir )
    {
        Object var10000;
        if ( this.table().size() < this.capacity )
        {
            this.table().put( this.key( start, end, dir ), rels ).isEmpty();
            this.memoryManager.allocated( (long) ((2 + rels.length) * 8) );
            var10000 = BoxedUnit.UNIT;
        }
        else
        {
            var10000 = BoxesRunTime.boxToBoolean( false );
        }

        return var10000;
    }

    private Tuple2<Object,Object> key( final long start, final long end, final SemanticDirection dir )
    {
        sp var10000;
        label28:
        {
            var6 = .MODULE$;
            if ( dir == null )
            {
                if ( var6 != null )
                {
                    break label28;
                }
            }
            else if ( !dir.equals( var6 ) )
            {
                break label28;
            }

            var10000 = start < end ? new sp( start, end ) : new sp( end, start );
            return var10000;
        }

        var10000 = new sp( start, end );
        return var10000;
    }
}
