package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Predef.;
import scala.collection.Iterator;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedGroupingExpression implements GroupingExpression, Product, Serializable
{
    private final SlotExpression[] sortedGroupingExpression;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit>[] setters;
    private final Function1<ExecutionContext,AnyValue>[] getters;
    private final Expression[] expressions;
    private final int numberOfSortedColumns;

    public SlottedGroupingExpression( final SlotExpression[] sortedGroupingExpression )
    {
        this.sortedGroupingExpression = sortedGroupingExpression;
        Product.$init$( this );
        this.setters = (Function2[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) sortedGroupingExpression ))).map( ( e ) -> {
        return SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( e.slot() );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Function2.class )));
        this.getters = (Function1[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) sortedGroupingExpression ))).map( ( e ) -> {
        return SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( e.slot() );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Function1.class )));
        this.expressions = (Expression[]) (new ofRef(.MODULE$.refArrayOps( (Object[]) sortedGroupingExpression ))).map( ( x$4 ) -> {
        return x$4.expression();
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.apply( Expression.class )));
        this.numberOfSortedColumns = (new ofRef(.MODULE$.refArrayOps( (Object[]) sortedGroupingExpression ))).count( ( x$5 ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$numberOfSortedColumns$1( x$5 ) );
    } );
    }

    public static Option<SlotExpression[]> unapply( final SlottedGroupingExpression x$0 )
    {
        return SlottedGroupingExpression$.MODULE$.unapply( var0 );
    }

    public static SlottedGroupingExpression apply( final SlotExpression[] sortedGroupingExpression )
    {
        return SlottedGroupingExpression$.MODULE$.apply( var0 );
    }

    public static <A> Function1<SlotExpression[],A> andThen( final Function1<SlottedGroupingExpression,A> g )
    {
        return SlottedGroupingExpression$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,SlottedGroupingExpression> compose( final Function1<A,SlotExpression[]> g )
    {
        return SlottedGroupingExpression$.MODULE$.compose( var0 );
    }

    public SlotExpression[] sortedGroupingExpression()
    {
        return this.sortedGroupingExpression;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit>[] setters()
    {
        return this.setters;
    }

    private Function1<ExecutionContext,AnyValue>[] getters()
    {
        return this.getters;
    }

    private Expression[] expressions()
    {
        return this.expressions;
    }

    private int numberOfSortedColumns()
    {
        return this.numberOfSortedColumns;
    }

    public void registerOwningPipe( final Pipe pipe )
    {
        (new ofRef(.MODULE$.refArrayOps( (Object[]) this.sortedGroupingExpression() ))).foreach( ( e ) -> {
        $anonfun$registerOwningPipe$1( pipe, e );
        return BoxedUnit.UNIT;
    } );
    }

    public ListValue computeGroupingKey( final ExecutionContext context, final QueryState state )
    {
        AnyValue[] values = new AnyValue[this.expressions().length];

        for ( int i = 0; i < values.length; ++i )
        {
            values[i] = this.expressions()[i].apply( context, state );
        }

        return VirtualValues.list( values );
    }

    public AnyValue computeOrderedGroupingKey( final ListValue groupingKey )
    {
        return groupingKey.take( this.numberOfSortedColumns() );
    }

    public ListValue getGroupingKey( final ExecutionContext context )
    {
        AnyValue[] values = new AnyValue[this.expressions().length];

        for ( int i = 0; i < values.length; ++i )
        {
            values[i] = (AnyValue) this.getters()[i].apply( context );
        }

        return VirtualValues.list( values );
    }

    public boolean isEmpty()
    {
        return false;
    }

    public void project( final ExecutionContext context, final ListValue groupingKey )
    {
        for ( int i = 0; i < groupingKey.size(); ++i )
        {
            this.setters()[i].apply( context, groupingKey.value( i ) );
        }
    }

    public SlottedGroupingExpression copy( final SlotExpression[] sortedGroupingExpression )
    {
        return new SlottedGroupingExpression( sortedGroupingExpression );
    }

    public SlotExpression[] copy$default$1()
    {
        return this.sortedGroupingExpression();
    }

    public String productPrefix()
    {
        return "SlottedGroupingExpression";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.sortedGroupingExpression();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedGroupingExpression;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label49:
            {
                boolean var2;
                if ( x$1 instanceof SlottedGroupingExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    SlottedGroupingExpression var4 = (SlottedGroupingExpression) x$1;
                    if ( this.sortedGroupingExpression() == var4.sortedGroupingExpression() && var4.canEqual( this ) )
                    {
                        break label49;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
