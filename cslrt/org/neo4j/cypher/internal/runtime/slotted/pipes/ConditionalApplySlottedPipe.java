package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.values.storable.Values;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple6;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class ConditionalApplySlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe lhs;
    private final Pipe rhs;
    private final Seq<Object> longOffsets;
    private final Seq<Object> refOffsets;
    private final boolean negated;
    private final SlotConfiguration slots;
    private final int id;

    public ConditionalApplySlottedPipe( final Pipe lhs, final Pipe rhs, final Seq<Object> longOffsets, final Seq<Object> refOffsets, final boolean negated,
            final SlotConfiguration slots, final int id )
    {
        super( lhs );
        this.lhs = lhs;
        this.rhs = rhs;
        this.longOffsets = longOffsets;
        this.refOffsets = refOffsets;
        this.negated = negated;
        this.slots = slots;
        this.id = id;
        Product.$init$( this );
    }

    public static int $lessinit$greater$default$7( final Pipe lhs, final Pipe rhs, final Seq longOffsets, final Seq refOffsets, final boolean negated,
            final SlotConfiguration slots )
    {
        return ConditionalApplySlottedPipe$.MODULE$.$lessinit$greater$default$7( var0, var1, var2, var3, var4, var5 );
    }

    public static int apply$default$7( final Pipe lhs, final Pipe rhs, final Seq longOffsets, final Seq refOffsets, final boolean negated,
            final SlotConfiguration slots )
    {
        return ConditionalApplySlottedPipe$.MODULE$.apply$default$7( var0, var1, var2, var3, var4, var5 );
    }

    public static Option<Tuple6<Pipe,Pipe,Seq<Object>,Seq<Object>,Object,SlotConfiguration>> unapply( final ConditionalApplySlottedPipe x$0 )
    {
        return ConditionalApplySlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ConditionalApplySlottedPipe apply( final Pipe lhs, final Pipe rhs, final Seq<Object> longOffsets, final Seq<Object> refOffsets,
            final boolean negated, final SlotConfiguration slots, final int id )
    {
        return ConditionalApplySlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6 );
    }

    public Pipe lhs()
    {
        return this.lhs;
    }

    public Pipe rhs()
    {
        return this.rhs;
    }

    public Seq<Object> longOffsets()
    {
        return this.longOffsets;
    }

    public Seq<Object> refOffsets()
    {
        return this.refOffsets;
    }

    public boolean negated()
    {
        return this.negated;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.flatMap( ( lhsContext ) -> {
            Iterator var10000;
            if ( this.condition( lhsContext ) )
            {
                QueryState rhsState = state.withInitialContext( lhsContext );
                var10000 = this.rhs().createResults( rhsState );
            }
            else
            {
                SlottedExecutionContext output = new SlottedExecutionContext( this.slots() );
                lhsContext.copyTo( output, lhsContext.copyTo$default$2(), lhsContext.copyTo$default$3(), lhsContext.copyTo$default$4(),
                        lhsContext.copyTo$default$5() );
                var10000 = .MODULE$.Iterator().single( output );
            }

            return var10000;
        } );
    }

    private boolean condition( final ExecutionContext context )
    {
        boolean cond = this.longOffsets().exists( ( offset ) -> {
            return !NullChecker$.MODULE$.entityIsNull( context.getLongAt( offset ) );
        } ) || this.refOffsets().exists( ( x ) -> {
            return context.getRefAt( x ) != Values.NO_VALUE;
        } );
        return this.negated() ? !cond : cond;
    }

    public ConditionalApplySlottedPipe copy( final Pipe lhs, final Pipe rhs, final Seq<Object> longOffsets, final Seq<Object> refOffsets, final boolean negated,
            final SlotConfiguration slots, final int id )
    {
        return new ConditionalApplySlottedPipe( lhs, rhs, longOffsets, refOffsets, negated, slots, id );
    }

    public Pipe copy$default$1()
    {
        return this.lhs();
    }

    public Pipe copy$default$2()
    {
        return this.rhs();
    }

    public Seq<Object> copy$default$3()
    {
        return this.longOffsets();
    }

    public Seq<Object> copy$default$4()
    {
        return this.refOffsets();
    }

    public boolean copy$default$5()
    {
        return this.negated();
    }

    public SlotConfiguration copy$default$6()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "ConditionalApplySlottedPipe";
    }

    public int productArity()
    {
        return 6;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        case 2:
            var10000 = this.longOffsets();
            break;
        case 3:
            var10000 = this.refOffsets();
            break;
        case 4:
            var10000 = BoxesRunTime.boxToBoolean( this.negated() );
            break;
        case 5:
            var10000 = this.slots();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ConditionalApplySlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.lhs() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.rhs() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.longOffsets() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.refOffsets() ) );
        var1 = Statics.mix( var1, this.negated() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        return Statics.finalizeHash( var1, 6 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label92:
            {
                boolean var2;
                if ( x$1 instanceof ConditionalApplySlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label74:
                    {
                        label83:
                        {
                            ConditionalApplySlottedPipe var4 = (ConditionalApplySlottedPipe) x$1;
                            Pipe var10000 = this.lhs();
                            Pipe var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label83;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label83;
                            }

                            var10000 = this.rhs();
                            Pipe var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label83;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label83;
                            }

                            Seq var10 = this.longOffsets();
                            Seq var7 = var4.longOffsets();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label83;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label83;
                            }

                            var10 = this.refOffsets();
                            Seq var8 = var4.refOffsets();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label83;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label83;
                            }

                            if ( this.negated() == var4.negated() )
                            {
                                label52:
                                {
                                    SlotConfiguration var11 = this.slots();
                                    SlotConfiguration var9 = var4.slots();
                                    if ( var11 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label52;
                                        }
                                    }
                                    else if ( !var11.equals( var9 ) )
                                    {
                                        break label52;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var12 = true;
                                        break label74;
                                    }
                                }
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label92;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
