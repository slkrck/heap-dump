package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrefetchingIterator;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.LongArray;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class DistinctSlottedPrimitivePipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final SlotConfiguration slots;
    private final int[] primitiveSlots;
    private final GroupingExpression groupingExpression;
    private final int id;

    public DistinctSlottedPrimitivePipe( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression, final int id )
    {
        super( source );
        this.source = source;
        this.slots = slots;
        this.primitiveSlots = primitiveSlots;
        this.groupingExpression = groupingExpression;
        this.id = id;
        Product.$init$( this );
        groupingExpression.registerOwningPipe( this );
    }

    public static int $lessinit$greater$default$5( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression )
    {
        return DistinctSlottedPrimitivePipe$.MODULE$.$lessinit$greater$default$5( var0, var1, var2, var3 );
    }

    public static int apply$default$5( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression )
    {
        return DistinctSlottedPrimitivePipe$.MODULE$.apply$default$5( var0, var1, var2, var3 );
    }

    public static Option<Tuple4<Pipe,SlotConfiguration,int[],GroupingExpression>> unapply( final DistinctSlottedPrimitivePipe x$0 )
    {
        return DistinctSlottedPrimitivePipe$.MODULE$.unapply( var0 );
    }

    public static DistinctSlottedPrimitivePipe apply( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression, final int id )
    {
        return DistinctSlottedPrimitivePipe$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public static LongArray buildGroupingValue( final ExecutionContext next, final int[] slots )
    {
        return DistinctSlottedPrimitivePipe$.MODULE$.buildGroupingValue( var0, var1 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int[] primitiveSlots()
    {
        return this.primitiveSlots;
    }

    public GroupingExpression groupingExpression()
    {
        return this.groupingExpression;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return new PrefetchingIterator<ExecutionContext>( this, input, state )
        {
            private final MutableSet<LongArray> seen;
            private final Iterator input$1;
            private final QueryState state$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.input$1 = input$1;
                    this.state$1 = state$1;
                    this.seen = Sets.mutable.empty();
                }
            }

            private MutableSet<LongArray> seen()
            {
                return this.seen;
            }

            public Option<ExecutionContext> produceNext()
            {
                while ( true )
                {
                    if ( this.input$1.hasNext() )
                    {
                        ExecutionContext next = (ExecutionContext) this.input$1.next();
                        LongArray groupingValue = DistinctSlottedPrimitivePipe$.MODULE$.buildGroupingValue( next, this.$outer.primitiveSlots() );
                        if ( !this.seen().add( groupingValue ) )
                        {
                            continue;
                        }

                        this.state$1.memoryTracker().allocated( groupingValue );
                        AnyValue key = this.$outer.groupingExpression().computeGroupingKey( next, this.state$1 );
                        this.$outer.groupingExpression().project( next, key );
                        return new Some( next );
                    }

                    return .MODULE$;
                }
            }
        };
    }

    public DistinctSlottedPrimitivePipe copy( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression, final int id )
    {
        return new DistinctSlottedPrimitivePipe( source, slots, primitiveSlots, groupingExpression, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public SlotConfiguration copy$default$2()
    {
        return this.slots();
    }

    public int[] copy$default$3()
    {
        return this.primitiveSlots();
    }

    public GroupingExpression copy$default$4()
    {
        return this.groupingExpression();
    }

    public String productPrefix()
    {
        return "DistinctSlottedPrimitivePipe";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.slots();
            break;
        case 2:
            var10000 = this.primitiveSlots();
            break;
        case 3:
            var10000 = this.groupingExpression();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof DistinctSlottedPrimitivePipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label74:
            {
                boolean var2;
                if ( x$1 instanceof DistinctSlottedPrimitivePipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label56:
                    {
                        label65:
                        {
                            DistinctSlottedPrimitivePipe var4 = (DistinctSlottedPrimitivePipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label65;
                            }

                            SlotConfiguration var8 = this.slots();
                            SlotConfiguration var6 = var4.slots();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label65;
                            }

                            if ( this.primitiveSlots() == var4.primitiveSlots() )
                            {
                                label44:
                                {
                                    GroupingExpression var9 = this.groupingExpression();
                                    GroupingExpression var7 = var4.groupingExpression();
                                    if ( var9 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label44;
                                        }
                                    }
                                    else if ( !var9.equals( var7 ) )
                                    {
                                        break label44;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var10 = true;
                                        break label56;
                                    }
                                }
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label74;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
