package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ListSupport;
import org.neo4j.cypher.internal.runtime.ListSupport.RichSeq;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import scala.Function2;
import scala.Option;
import scala.PartialFunction;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ForeachSlottedPipe extends PipeWithSource implements ListSupport, Product, Serializable
{
    private final Pipe lhs;
    private final Pipe rhs;
    private final Slot innerVariableSlot;
    private final Expression expression;
    private final int id;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> setVariableFun;

    public ForeachSlottedPipe( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression, final int id )
    {
        super( lhs );
        this.lhs = lhs;
        this.rhs = rhs;
        this.innerVariableSlot = innerVariableSlot;
        this.expression = expression;
        this.id = id;
        ListSupport.$init$( this );
        Product.$init$( this );
        this.setVariableFun = SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( innerVariableSlot );
        expression.registerOwningPipe( this );
    }

    public static int $lessinit$greater$default$5( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression )
    {
        return ForeachSlottedPipe$.MODULE$.$lessinit$greater$default$5( var0, var1, var2, var3 );
    }

    public static int apply$default$5( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression )
    {
        return ForeachSlottedPipe$.MODULE$.apply$default$5( var0, var1, var2, var3 );
    }

    public static Option<Tuple4<Pipe,Pipe,Slot,Expression>> unapply( final ForeachSlottedPipe x$0 )
    {
        return ForeachSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ForeachSlottedPipe apply( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression, final int id )
    {
        return ForeachSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public boolean isList( final AnyValue x )
    {
        return ListSupport.isList$( this, x );
    }

    public <T> Option<Iterable<T>> asListOf( final PartialFunction<AnyValue,T> test, final Iterable<AnyValue> input )
    {
        return ListSupport.asListOf$( this, test, input );
    }

    public ListValue makeTraversable( final AnyValue z )
    {
        return ListSupport.makeTraversable$( this, z );
    }

    public PartialFunction<AnyValue,ListValue> castToList()
    {
        return ListSupport.castToList$( this );
    }

    public <T> RichSeq<T> RichSeq( final Seq<T> inner )
    {
        return ListSupport.RichSeq$( this, inner );
    }

    public Pipe lhs()
    {
        return this.lhs;
    }

    public Pipe rhs()
    {
        return this.rhs;
    }

    public Slot innerVariableSlot()
    {
        return this.innerVariableSlot;
    }

    public Expression expression()
    {
        return this.expression;
    }

    public int id()
    {
        return this.id;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit> setVariableFun()
    {
        return this.setVariableFun;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.map( ( outerContext ) -> {
            java.util.Iterator values = this.makeTraversable( this.expression().apply( outerContext, state ) ).iterator();

            while ( values.hasNext() )
            {
                this.setVariableFun().apply( outerContext, values.next() );
                QueryState innerState = state.withInitialContext( outerContext );
                this.rhs().createResults( innerState ).length();
            }

            return outerContext;
        } );
    }

    public ForeachSlottedPipe copy( final Pipe lhs, final Pipe rhs, final Slot innerVariableSlot, final Expression expression, final int id )
    {
        return new ForeachSlottedPipe( lhs, rhs, innerVariableSlot, expression, id );
    }

    public Pipe copy$default$1()
    {
        return this.lhs();
    }

    public Pipe copy$default$2()
    {
        return this.rhs();
    }

    public Slot copy$default$3()
    {
        return this.innerVariableSlot();
    }

    public Expression copy$default$4()
    {
        return this.expression();
    }

    public String productPrefix()
    {
        return "ForeachSlottedPipe";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        case 2:
            var10000 = this.innerVariableSlot();
            break;
        case 3:
            var10000 = this.expression();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ForeachSlottedPipe;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof ForeachSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            ForeachSlottedPipe var4 = (ForeachSlottedPipe) x$1;
                            Pipe var10000 = this.lhs();
                            Pipe var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            var10000 = this.rhs();
                            Pipe var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label72;
                            }

                            Slot var9 = this.innerVariableSlot();
                            Slot var7 = var4.innerVariableSlot();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label72;
                            }

                            Expression var10 = this.expression();
                            Expression var8 = var4.expression();
                            if ( var10 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var11 = true;
                                break label63;
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label81;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
