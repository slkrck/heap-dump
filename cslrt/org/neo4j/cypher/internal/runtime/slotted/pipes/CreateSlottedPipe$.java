package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.collection.IndexedSeq;

public final class CreateSlottedPipe$ implements Serializable
{
    public static CreateSlottedPipe$ MODULE$;

    static
    {
        new CreateSlottedPipe$();
    }

    private CreateSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$4( final Pipe source, final IndexedSeq<CreateNodeSlottedCommand> nodes,
            final IndexedSeq<CreateRelationshipSlottedCommand> relationships )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "CreateSlottedPipe";
    }

    public CreateSlottedPipe apply( final Pipe source, final IndexedSeq<CreateNodeSlottedCommand> nodes,
            final IndexedSeq<CreateRelationshipSlottedCommand> relationships, final int id )
    {
        return new CreateSlottedPipe( source, nodes, relationships, id );
    }

    public int apply$default$4( final Pipe source, final IndexedSeq<CreateNodeSlottedCommand> nodes,
            final IndexedSeq<CreateRelationshipSlottedCommand> relationships )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple3<Pipe,IndexedSeq<CreateNodeSlottedCommand>,IndexedSeq<CreateRelationshipSlottedCommand>>> unapply( final CreateSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.source(), x$0.nodes(), x$0.relationships() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
