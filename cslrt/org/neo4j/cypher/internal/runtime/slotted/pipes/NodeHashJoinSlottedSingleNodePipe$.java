package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple8;
import scala.runtime.BoxesRunTime;

public final class NodeHashJoinSlottedSingleNodePipe$ implements Serializable
{
    public static NodeHashJoinSlottedSingleNodePipe$ MODULE$;

    static
    {
        new NodeHashJoinSlottedSingleNodePipe$();
    }

    private NodeHashJoinSlottedSingleNodePipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$9( final int lhsOffset, final int rhsOffset, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "NodeHashJoinSlottedSingleNodePipe";
    }

    public NodeHashJoinSlottedSingleNodePipe apply( final int lhsOffset, final int rhsOffset, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy,
            final int id )
    {
        return new NodeHashJoinSlottedSingleNodePipe( lhsOffset, rhsOffset, left, right, slots, longsToCopy, refsToCopy, cachedPropertiesToCopy, id );
    }

    public int apply$default$9( final int lhsOffset, final int rhsOffset, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple8<Object,Object,Pipe,Pipe,SlotConfiguration,Tuple2<Object,Object>[],Tuple2<Object,Object>[],Tuple2<Object,Object>[]>> unapply(
            final NodeHashJoinSlottedSingleNodePipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple8( BoxesRunTime.boxToInteger( x$0.lhsOffset() ), BoxesRunTime.boxToInteger( x$0.rhsOffset() ), x$0.left(), x$0.right(), x$0.slots(),
                    x$0.longsToCopy(), x$0.refsToCopy(), x$0.cachedPropertiesToCopy() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
