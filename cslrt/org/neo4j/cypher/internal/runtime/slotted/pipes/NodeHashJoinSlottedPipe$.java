package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.MatchError;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple8;
import scala.Tuple2.mcII.sp;

public final class NodeHashJoinSlottedPipe$ implements Serializable
{
    public static NodeHashJoinSlottedPipe$ MODULE$;

    static
    {
        new NodeHashJoinSlottedPipe$();
    }

    private NodeHashJoinSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$9( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy )
    {
        return .MODULE$.INVALID_ID();
    }

    public void copyDataFromRhs( final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy,
            final Tuple2<Object,Object>[] cachedPropertiesToCopy, final ExecutionContext newRow, final ExecutionContext rhs )
    {
        int i;
        for ( i = 0; i < longsToCopy.length; ++i )
        {
            Tuple2 var11 = longsToCopy[i];
            if ( var11 == null )
            {
                throw new MatchError( var11 );
            }

            int from = var11._1$mcI$sp();
            int to = var11._2$mcI$sp();
            sp var8 = new sp( from, to );
            int from = var8._1$mcI$sp();
            int to = var8._2$mcI$sp();
            newRow.setLongAt( to, rhs.getLongAt( from ) );
        }

        for ( i = 0; i < refsToCopy.length; ++i )
        {
            Tuple2 var17 = refsToCopy[i];
            if ( var17 == null )
            {
                throw new MatchError( var17 );
            }

            int from = var17._1$mcI$sp();
            int to = var17._2$mcI$sp();
            sp var7 = new sp( from, to );
            int from = var7._1$mcI$sp();
            int to = var7._2$mcI$sp();
            newRow.setRefAt( to, rhs.getRefAt( from ) );
        }

        for ( i = 0; i < cachedPropertiesToCopy.length; ++i )
        {
            Tuple2 var23 = cachedPropertiesToCopy[i];
            if ( var23 == null )
            {
                throw new MatchError( var23 );
            }

            int from = var23._1$mcI$sp();
            int to = var23._2$mcI$sp();
            sp var6 = new sp( from, to );
            int from = var6._1$mcI$sp();
            int to = var6._2$mcI$sp();
            newRow.setCachedPropertyAt( to, rhs.getCachedPropertyAt( from ) );
        }
    }

    public void fillKeyArray( final ExecutionContext current, final long[] key, final int[] offsets )
    {
        for ( int i = 0; i < offsets.length; ++i )
        {
            long thisId = current.getLongAt( offsets[i] );
            key[i] = thisId;
            if ( NullChecker$.MODULE$.entityIsNull( thisId ) )
            {
                key[0] = NullChecker$.MODULE$.NULL_ENTITY();
                return;
            }
        }
    }

    public NodeHashJoinSlottedPipe apply( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy,
            final int id )
    {
        return new NodeHashJoinSlottedPipe( lhsOffsets, rhsOffsets, left, right, slots, longsToCopy, refsToCopy, cachedPropertiesToCopy, id );
    }

    public int apply$default$9( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple8<int[],int[],Pipe,Pipe,SlotConfiguration,Tuple2<Object,Object>[],Tuple2<Object,Object>[],Tuple2<Object,Object>[]>> unapply(
            final NodeHashJoinSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple8( x$0.lhsOffsets(), x$0.rhsOffsets(), x$0.left(), x$0.right(), x$0.slots(), x$0.longsToCopy(), x$0.refsToCopy(),
                    x$0.cachedPropertiesToCopy() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
