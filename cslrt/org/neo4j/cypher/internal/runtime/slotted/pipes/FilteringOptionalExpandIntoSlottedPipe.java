package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.eclipse.collections.api.iterator.LongIterator;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrimitiveLongHelper.;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple8;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class FilteringOptionalExpandIntoSlottedPipe extends OptionalExpandIntoSlottedPipe implements Product, Serializable
{
    private final Pipe source;
    private final Slot fromSlot;
    private final int relOffset;
    private final Slot toSlot;
    private final SemanticDirection dir;
    private final RelationshipTypes lazyTypes;
    private final SlotConfiguration slots;
    private final Expression predicate;
    private final int id;

    public FilteringOptionalExpandIntoSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots, final Expression predicate, final int id )
    {
        super( source, fromSlot, relOffset, toSlot, dir, lazyTypes, slots );
        this.source = source;
        this.fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.toSlot = toSlot;
        this.dir = dir;
        this.lazyTypes = lazyTypes;
        this.slots = slots;
        this.predicate = predicate;
        this.id = id;
        Product.$init$( this );
        predicate.registerOwningPipe( this );
    }

    public static Option<Tuple8<Pipe,Slot,Object,Slot,SemanticDirection,RelationshipTypes,SlotConfiguration,Expression>> unapply(
            final FilteringOptionalExpandIntoSlottedPipe x$0 )
    {
        return FilteringOptionalExpandIntoSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static FilteringOptionalExpandIntoSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot,
            final SemanticDirection dir, final RelationshipTypes lazyTypes, final SlotConfiguration slots, final Expression predicate, final int id )
    {
        return FilteringOptionalExpandIntoSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public Slot fromSlot()
    {
        return this.fromSlot;
    }

    public int relOffset()
    {
        return this.relOffset;
    }

    public Slot toSlot()
    {
        return this.toSlot;
    }

    public SemanticDirection dir()
    {
        return this.dir;
    }

    public RelationshipTypes lazyTypes()
    {
        return this.lazyTypes;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public Expression predicate()
    {
        return this.predicate;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<SlottedExecutionContext> findMatchIterator( final ExecutionContext inputRow, final QueryState state, final LongIterator relationships )
    {
        return .MODULE$.map( relationships, ( relId ) -> {
        return $anonfun$findMatchIterator$2( this, inputRow, BoxesRunTime.unboxToLong( relId ) );
    } ).filter( ( ctx ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$findMatchIterator$3( this, state, ctx ) );
    } );
    }

    public FilteringOptionalExpandIntoSlottedPipe copy( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot,
            final SemanticDirection dir, final RelationshipTypes lazyTypes, final SlotConfiguration slots, final Expression predicate, final int id )
    {
        return new FilteringOptionalExpandIntoSlottedPipe( source, fromSlot, relOffset, toSlot, dir, lazyTypes, slots, predicate, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public Slot copy$default$2()
    {
        return this.fromSlot();
    }

    public int copy$default$3()
    {
        return this.relOffset();
    }

    public Slot copy$default$4()
    {
        return this.toSlot();
    }

    public SemanticDirection copy$default$5()
    {
        return this.dir();
    }

    public RelationshipTypes copy$default$6()
    {
        return this.lazyTypes();
    }

    public SlotConfiguration copy$default$7()
    {
        return this.slots();
    }

    public Expression copy$default$8()
    {
        return this.predicate();
    }

    public String productPrefix()
    {
        return "FilteringOptionalExpandIntoSlottedPipe";
    }

    public int productArity()
    {
        return 8;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.fromSlot();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.relOffset() );
            break;
        case 3:
            var10000 = this.toSlot();
            break;
        case 4:
            var10000 = this.dir();
            break;
        case 5:
            var10000 = this.lazyTypes();
            break;
        case 6:
            var10000 = this.slots();
            break;
        case 7:
            var10000 = this.predicate();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FilteringOptionalExpandIntoSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.source() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fromSlot() ) );
        var1 = Statics.mix( var1, this.relOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.toSlot() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.dir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.lazyTypes() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.predicate() ) );
        return Statics.finalizeHash( var1, 8 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var17;
        if ( this != x$1 )
        {
            label111:
            {
                boolean var2;
                if ( x$1 instanceof FilteringOptionalExpandIntoSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label92:
                    {
                        label101:
                        {
                            FilteringOptionalExpandIntoSlottedPipe var4 = (FilteringOptionalExpandIntoSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label101;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label101;
                            }

                            Slot var12 = this.fromSlot();
                            Slot var6 = var4.fromSlot();
                            if ( var12 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label101;
                                }
                            }
                            else if ( !var12.equals( var6 ) )
                            {
                                break label101;
                            }

                            if ( this.relOffset() == var4.relOffset() )
                            {
                                label102:
                                {
                                    var12 = this.toSlot();
                                    Slot var7 = var4.toSlot();
                                    if ( var12 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label102;
                                        }
                                    }
                                    else if ( !var12.equals( var7 ) )
                                    {
                                        break label102;
                                    }

                                    SemanticDirection var13 = this.dir();
                                    SemanticDirection var8 = var4.dir();
                                    if ( var13 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label102;
                                        }
                                    }
                                    else if ( !var13.equals( var8 ) )
                                    {
                                        break label102;
                                    }

                                    RelationshipTypes var14 = this.lazyTypes();
                                    RelationshipTypes var9 = var4.lazyTypes();
                                    if ( var14 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label102;
                                        }
                                    }
                                    else if ( !var14.equals( var9 ) )
                                    {
                                        break label102;
                                    }

                                    SlotConfiguration var15 = this.slots();
                                    SlotConfiguration var10 = var4.slots();
                                    if ( var15 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label102;
                                        }
                                    }
                                    else if ( !var15.equals( var10 ) )
                                    {
                                        break label102;
                                    }

                                    Expression var16 = this.predicate();
                                    Expression var11 = var4.predicate();
                                    if ( var16 == null )
                                    {
                                        if ( var11 != null )
                                        {
                                            break label102;
                                        }
                                    }
                                    else if ( !var16.equals( var11 ) )
                                    {
                                        break label102;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var17 = true;
                                        break label92;
                                    }
                                }
                            }
                        }

                        var17 = false;
                    }

                    if ( var17 )
                    {
                        break label111;
                    }
                }

                var17 = false;
                return var17;
            }
        }

        var17 = true;
        return var17;
    }
}
