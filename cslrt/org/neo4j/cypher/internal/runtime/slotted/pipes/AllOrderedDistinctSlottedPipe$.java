package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;

public final class AllOrderedDistinctSlottedPipe$ implements Serializable
{
    public static AllOrderedDistinctSlottedPipe$ MODULE$;

    static
    {
        new AllOrderedDistinctSlottedPipe$();
    }

    private AllOrderedDistinctSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$4( final Pipe source, final SlotConfiguration slots, final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "AllOrderedDistinctSlottedPipe";
    }

    public AllOrderedDistinctSlottedPipe apply( final Pipe source, final SlotConfiguration slots, final GroupingExpression groupingExpression, final int id )
    {
        return new AllOrderedDistinctSlottedPipe( source, slots, groupingExpression, id );
    }

    public int apply$default$4( final Pipe source, final SlotConfiguration slots, final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple3<Pipe,SlotConfiguration,GroupingExpression>> unapply( final AllOrderedDistinctSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.source(), x$0.slots(), x$0.groupingExpression() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
