package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;

public final class ApplySlottedPipe$ implements Serializable
{
    public static ApplySlottedPipe$ MODULE$;

    static
    {
        new ApplySlottedPipe$();
    }

    private ApplySlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$3( final Pipe lhs, final Pipe rhs )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "ApplySlottedPipe";
    }

    public ApplySlottedPipe apply( final Pipe lhs, final Pipe rhs, final int id )
    {
        return new ApplySlottedPipe( lhs, rhs, id );
    }

    public int apply$default$3( final Pipe lhs, final Pipe rhs )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple2<Pipe,Pipe>> unapply( final ApplySlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.lhs(), x$0.rhs() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
