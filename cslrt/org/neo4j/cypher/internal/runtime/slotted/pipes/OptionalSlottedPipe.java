package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class OptionalSlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final Seq<Slot> nullableSlots;
    private final SlotConfiguration slots;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private final Seq<Function1<ExecutionContext,BoxedUnit>> setNullableSlotToNullFunctions;

    public OptionalSlottedPipe( final Pipe source, final Seq<Slot> nullableSlots, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize,
            final int id )
    {
        super( source );
        this.source = source;
        this.nullableSlots = nullableSlots;
        this.slots = slots;
        this.argumentSize = argumentSize;
        this.id = id;
        Product.$init$( this );
        this.setNullableSlotToNullFunctions = (Seq) nullableSlots.map( ( x0$1 ) -> {
            Function1 var1;
            if ( x0$1 instanceof LongSlot )
            {
                LongSlot var3 = (LongSlot) x0$1;
                int offsetx = var3.offset();
                var1 = ( context ) -> {
                    $anonfun$setNullableSlotToNullFunctions$2( offsetx, context );
                    return BoxedUnit.UNIT;
                };
            }
            else
            {
                if ( !(x0$1 instanceof RefSlot) )
                {
                    throw new MatchError( x0$1 );
                }

                RefSlot var5 = (RefSlot) x0$1;
                int offset = var5.offset();
                var1 = ( context ) -> {
                    $anonfun$setNullableSlotToNullFunctions$3( offset, context );
                    return BoxedUnit.UNIT;
                };
            }

            return var1;
        }, scala.collection.Seq..MODULE$.canBuildFrom());
    }

    public static int $lessinit$greater$default$5( final Pipe source, final Seq nullableSlots, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return OptionalSlottedPipe$.MODULE$.$lessinit$greater$default$5( var0, var1, var2, var3 );
    }

    public static int apply$default$5( final Pipe source, final Seq nullableSlots, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return OptionalSlottedPipe$.MODULE$.apply$default$5( var0, var1, var2, var3 );
    }

    public static Option<Tuple4<Pipe,Seq<Slot>,SlotConfiguration,SlotConfiguration.Size>> unapply( final OptionalSlottedPipe x$0 )
    {
        return OptionalSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static OptionalSlottedPipe apply( final Pipe source, final Seq<Slot> nullableSlots, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        return OptionalSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public Seq<Slot> nullableSlots()
    {
        return this.nullableSlots;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    private Seq<Function1<ExecutionContext,BoxedUnit>> setNullableSlotToNullFunctions()
    {
        return this.setNullableSlotToNullFunctions;
    }

    private void setNullableSlotsToNull( final ExecutionContext context )
    {
        Iterator functions = this.setNullableSlotToNullFunctions().iterator();

        while ( functions.hasNext() )
        {
            ((Function1) functions.next()).apply( context );
        }
    }

    private ExecutionContext notFoundExecutionContext( final QueryState state )
    {
        SlottedExecutionContext context = new SlottedExecutionContext( this.slots() );
        state.copyArgumentStateTo( context, this.argumentSize().nLongs(), this.argumentSize().nReferences() );
        this.setNullableSlotsToNull( context );
        return context;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return !input.hasNext() ? .
        MODULE$.Iterator().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new ExecutionContext[]{this.notFoundExecutionContext( state )}) )) :input;
    }

    public OptionalSlottedPipe copy( final Pipe source, final Seq<Slot> nullableSlots, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize,
            final int id )
    {
        return new OptionalSlottedPipe( source, nullableSlots, slots, argumentSize, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public Seq<Slot> copy$default$2()
    {
        return this.nullableSlots();
    }

    public SlotConfiguration copy$default$3()
    {
        return this.slots();
    }

    public SlotConfiguration.Size copy$default$4()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "OptionalSlottedPipe";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.nullableSlots();
            break;
        case 2:
            var10000 = this.slots();
            break;
        case 3:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof OptionalSlottedPipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof OptionalSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            OptionalSlottedPipe var4 = (OptionalSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            Seq var9 = this.nullableSlots();
                            Seq var6 = var4.nullableSlots();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            SlotConfiguration var10 = this.slots();
                            SlotConfiguration var7 = var4.slots();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            SlotConfiguration.Size var11 = this.argumentSize();
                            SlotConfiguration.Size var8 = var4.argumentSize();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
