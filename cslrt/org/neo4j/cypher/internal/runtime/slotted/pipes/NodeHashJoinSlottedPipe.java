package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.Collections;

import org.eclipse.collections.api.multimap.list.MutableListMultimap;
import org.eclipse.collections.impl.factory.Multimaps;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrefetchingIterator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.values.storable.LongArray;
import org.neo4j.values.storable.Values;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple8;
import scala.collection.Iterator;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodeHashJoinSlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final int[] lhsOffsets;
    private final int[] rhsOffsets;
    private final Pipe left;
    private final Pipe right;
    private final SlotConfiguration slots;
    private final Tuple2<Object,Object>[] longsToCopy;
    private final Tuple2<Object,Object>[] refsToCopy;
    private final Tuple2<Object,Object>[] cachedPropertiesToCopy;
    private final int id;
    private final int org$neo4j$cypher$internal$runtime$slotted$pipes$NodeHashJoinSlottedPipe$$width;

    public NodeHashJoinSlottedPipe( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy,
            final int id )
    {
        super( left );
        this.lhsOffsets = lhsOffsets;
        this.rhsOffsets = rhsOffsets;
        this.left = left;
        this.right = right;
        this.slots = slots;
        this.longsToCopy = longsToCopy;
        this.refsToCopy = refsToCopy;
        this.cachedPropertiesToCopy = cachedPropertiesToCopy;
        this.id = id;
        Product.$init$( this );
        this.org$neo4j$cypher$internal$runtime$slotted$pipes$NodeHashJoinSlottedPipe$$width = lhsOffsets.length;
    }

    public static int $lessinit$greater$default$9( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right,
            final SlotConfiguration slots, final Tuple2[] longsToCopy, final Tuple2[] refsToCopy, final Tuple2[] cachedPropertiesToCopy )
    {
        return NodeHashJoinSlottedPipe$.MODULE$.$lessinit$greater$default$9( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static int apply$default$9( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2[] longsToCopy, final Tuple2[] refsToCopy, final Tuple2[] cachedPropertiesToCopy )
    {
        return NodeHashJoinSlottedPipe$.MODULE$.apply$default$9( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static Option<Tuple8<int[],int[],Pipe,Pipe,SlotConfiguration,Tuple2<Object,Object>[],Tuple2<Object,Object>[],Tuple2<Object,Object>[]>> unapply(
            final NodeHashJoinSlottedPipe x$0 )
    {
        return NodeHashJoinSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static NodeHashJoinSlottedPipe apply( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right,
            final SlotConfiguration slots, final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy,
            final Tuple2<Object,Object>[] cachedPropertiesToCopy, final int id )
    {
        return NodeHashJoinSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public static void fillKeyArray( final ExecutionContext current, final long[] key, final int[] offsets )
    {
        NodeHashJoinSlottedPipe$.MODULE$.fillKeyArray( var0, var1, var2 );
    }

    public static void copyDataFromRhs( final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy,
            final Tuple2<Object,Object>[] cachedPropertiesToCopy, final ExecutionContext newRow, final ExecutionContext rhs )
    {
        NodeHashJoinSlottedPipe$.MODULE$.copyDataFromRhs( var0, var1, var2, var3, var4 );
    }

    public int[] lhsOffsets()
    {
        return this.lhsOffsets;
    }

    public int[] rhsOffsets()
    {
        return this.rhsOffsets;
    }

    public Pipe left()
    {
        return this.left;
    }

    public Pipe right()
    {
        return this.right;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public Tuple2<Object,Object>[] longsToCopy()
    {
        return this.longsToCopy;
    }

    public Tuple2<Object,Object>[] refsToCopy()
    {
        return this.refsToCopy;
    }

    public Tuple2<Object,Object>[] cachedPropertiesToCopy()
    {
        return this.cachedPropertiesToCopy;
    }

    public int id()
    {
        return this.id;
    }

    public int org$neo4j$cypher$internal$runtime$slotted$pipes$NodeHashJoinSlottedPipe$$width()
    {
        return this.org$neo4j$cypher$internal$runtime$slotted$pipes$NodeHashJoinSlottedPipe$$width;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        if ( input.isEmpty() )
        {
            return .MODULE$.Iterator().empty();
        }
        else
        {
            Iterator rhsIterator = this.right().createResults( state );
            if ( rhsIterator.isEmpty() )
            {
                return .MODULE$.Iterator().empty();
            }
            else
            {
                MutableListMultimap table = this.buildProbeTable( state.memoryTracker().memoryTrackingIterator( input ), state );
                return table.isEmpty() ? .MODULE$.Iterator().empty() :this.probeInput( rhsIterator, state, table );
            }
        }
    }

    private MutableListMultimap<LongArray,ExecutionContext> buildProbeTable( final Iterator<ExecutionContext> lhsInput, final QueryState queryState )
    {
        MutableListMultimap table = Multimaps.mutable.list.empty();
        lhsInput.foreach( ( current ) -> {
            long[] key = new long[this.org$neo4j$cypher$internal$runtime$slotted$pipes$NodeHashJoinSlottedPipe$$width()];
            NodeHashJoinSlottedPipe$.MODULE$.fillKeyArray( current, key, this.lhsOffsets() );
            return key[0] != -1L ? BoxesRunTime.boxToBoolean( table.put( Values.longArray( key ), current ) ) : BoxedUnit.UNIT;
        } );
        return table;
    }

    private Iterator<ExecutionContext> probeInput( final Iterator<ExecutionContext> rhsInput, final QueryState queryState,
            final MutableListMultimap<LongArray,ExecutionContext> probeTable )
    {
        return new PrefetchingIterator<ExecutionContext>( this, rhsInput, probeTable )
        {
            private final long[] key;
            private final Iterator rhsInput$1;
            private final MutableListMultimap probeTable$1;
            private java.util.Iterator<ExecutionContext> matches;
            private ExecutionContext currentRhsRow;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.rhsInput$1 = rhsInput$1;
                    this.probeTable$1 = probeTable$1;
                    this.key = new long[$outer.org$neo4j$cypher$internal$runtime$slotted$pipes$NodeHashJoinSlottedPipe$$width()];
                    this.matches = Collections.emptyIterator();
                }
            }

            private long[] key()
            {
                return this.key;
            }

            private java.util.Iterator<ExecutionContext> matches()
            {
                return this.matches;
            }

            private void matches_$eq( final java.util.Iterator<ExecutionContext> x$1 )
            {
                this.matches = x$1;
            }

            private ExecutionContext currentRhsRow()
            {
                return this.currentRhsRow;
            }

            private void currentRhsRow_$eq( final ExecutionContext x$1 )
            {
                this.currentRhsRow = x$1;
            }

            public Option<ExecutionContext> produceNext()
            {
                if ( this.matches().hasNext() )
                {
                    ExecutionContext lhs = (ExecutionContext) this.matches().next();
                    SlottedExecutionContext newRow = new SlottedExecutionContext( this.$outer.slots() );
                    lhs.copyTo( newRow, lhs.copyTo$default$2(), lhs.copyTo$default$3(), lhs.copyTo$default$4(), lhs.copyTo$default$5() );
                    NodeHashJoinSlottedPipe$.MODULE$.copyDataFromRhs( this.$outer.longsToCopy(), this.$outer.refsToCopy(), this.$outer.cachedPropertiesToCopy(),
                            newRow, this.currentRhsRow() );
                    return new Some( newRow );
                }
                else
                {
                    while ( this.rhsInput$1.hasNext() )
                    {
                        this.currentRhsRow_$eq( (ExecutionContext) this.rhsInput$1.next() );
                        NodeHashJoinSlottedPipe$.MODULE$.fillKeyArray( this.currentRhsRow(), this.key(), this.$outer.rhsOffsets() );
                        if ( this.key()[0] != -1L )
                        {
                            this.matches_$eq( this.probeTable$1.get( Values.longArray( this.key() ) ).iterator() );
                            if ( this.matches().hasNext() )
                            {
                                return this.produceNext();
                            }
                        }
                    }

                    return scala.None..MODULE$;
                }
            }
        };
    }

    public NodeHashJoinSlottedPipe copy( final int[] lhsOffsets, final int[] rhsOffsets, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final Tuple2<Object,Object>[] longsToCopy, final Tuple2<Object,Object>[] refsToCopy, final Tuple2<Object,Object>[] cachedPropertiesToCopy,
            final int id )
    {
        return new NodeHashJoinSlottedPipe( lhsOffsets, rhsOffsets, left, right, slots, longsToCopy, refsToCopy, cachedPropertiesToCopy, id );
    }

    public int[] copy$default$1()
    {
        return this.lhsOffsets();
    }

    public int[] copy$default$2()
    {
        return this.rhsOffsets();
    }

    public Pipe copy$default$3()
    {
        return this.left();
    }

    public Pipe copy$default$4()
    {
        return this.right();
    }

    public SlotConfiguration copy$default$5()
    {
        return this.slots();
    }

    public Tuple2<Object,Object>[] copy$default$6()
    {
        return this.longsToCopy();
    }

    public Tuple2<Object,Object>[] copy$default$7()
    {
        return this.refsToCopy();
    }

    public Tuple2<Object,Object>[] copy$default$8()
    {
        return this.cachedPropertiesToCopy();
    }

    public String productPrefix()
    {
        return "NodeHashJoinSlottedPipe";
    }

    public int productArity()
    {
        return 8;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhsOffsets();
            break;
        case 1:
            var10000 = this.rhsOffsets();
            break;
        case 2:
            var10000 = this.left();
            break;
        case 3:
            var10000 = this.right();
            break;
        case 4:
            var10000 = this.slots();
            break;
        case 5:
            var10000 = this.longsToCopy();
            break;
        case 6:
            var10000 = this.refsToCopy();
            break;
        case 7:
            var10000 = this.cachedPropertiesToCopy();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NodeHashJoinSlottedPipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var9;
        if ( this != x$1 )
        {
            label82:
            {
                boolean var2;
                if ( x$1 instanceof NodeHashJoinSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label64:
                    {
                        NodeHashJoinSlottedPipe var4 = (NodeHashJoinSlottedPipe) x$1;
                        if ( this.lhsOffsets() == var4.lhsOffsets() && this.rhsOffsets() == var4.rhsOffsets() )
                        {
                            label73:
                            {
                                Pipe var10000 = this.left();
                                Pipe var5 = var4.left();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label73;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label73;
                                }

                                var10000 = this.right();
                                Pipe var6 = var4.right();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label73;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label73;
                                }

                                SlotConfiguration var8 = this.slots();
                                SlotConfiguration var7 = var4.slots();
                                if ( var8 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label73;
                                    }
                                }
                                else if ( !var8.equals( var7 ) )
                                {
                                    break label73;
                                }

                                if ( this.longsToCopy() == var4.longsToCopy() && this.refsToCopy() == var4.refsToCopy() &&
                                        this.cachedPropertiesToCopy() == var4.cachedPropertiesToCopy() && var4.canEqual( this ) )
                                {
                                    var9 = true;
                                    break label64;
                                }
                            }
                        }

                        var9 = false;
                    }

                    if ( var9 )
                    {
                        break label82;
                    }
                }

                var9 = false;
                return var9;
            }
        }

        var9 = true;
        return var9;
    }
}
