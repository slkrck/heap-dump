package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;

public final class NodesByLabelScanSlottedPipe$ implements Serializable
{
    public static NodesByLabelScanSlottedPipe$ MODULE$;

    static
    {
        new NodesByLabelScanSlottedPipe$();
    }

    private NodesByLabelScanSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$5( final String ident, final LazyLabel label, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "NodesByLabelScanSlottedPipe";
    }

    public NodesByLabelScanSlottedPipe apply( final String ident, final LazyLabel label, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        return new NodesByLabelScanSlottedPipe( ident, label, slots, argumentSize, id );
    }

    public int apply$default$5( final String ident, final LazyLabel label, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple4<String,LazyLabel,SlotConfiguration,SlotConfiguration.Size>> unapply( final NodesByLabelScanSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple4( x$0.ident(), x$0.label(), x$0.slots(), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
