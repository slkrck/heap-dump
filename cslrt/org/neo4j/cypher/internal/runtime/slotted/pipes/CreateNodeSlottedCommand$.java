package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class CreateNodeSlottedCommand$ extends AbstractFunction3<Object,Seq<LazyLabel>,Option<Expression>,CreateNodeSlottedCommand>
        implements Serializable
{
    public static CreateNodeSlottedCommand$ MODULE$;

    static
    {
        new CreateNodeSlottedCommand$();
    }

    private CreateNodeSlottedCommand$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CreateNodeSlottedCommand";
    }

    public CreateNodeSlottedCommand apply( final int idOffset, final Seq<LazyLabel> labels, final Option<Expression> properties )
    {
        return new CreateNodeSlottedCommand( idOffset, labels, properties );
    }

    public Option<Tuple3<Object,Seq<LazyLabel>,Option<Expression>>> unapply( final CreateNodeSlottedCommand x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.idOffset() ), x$0.labels(), x$0.properties() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
