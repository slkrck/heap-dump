package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Seq;

public final class ProduceResultSlottedPipe$ implements Serializable
{
    public static ProduceResultSlottedPipe$ MODULE$;

    static
    {
        new ProduceResultSlottedPipe$();
    }

    private ProduceResultSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$3( final Pipe source, final Seq<Tuple2<String,Expression>> columns )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "ProduceResultSlottedPipe";
    }

    public ProduceResultSlottedPipe apply( final Pipe source, final Seq<Tuple2<String,Expression>> columns, final int id )
    {
        return new ProduceResultSlottedPipe( source, columns, id );
    }

    public int apply$default$3( final Pipe source, final Seq<Tuple2<String,Expression>> columns )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple2<Pipe,Seq<Tuple2<String,Expression>>>> unapply( final ProduceResultSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.source(), x$0.columns() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
