package org.neo4j.cypher.internal.runtime.slotted.pipes;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class SlottedGroupingExpression1$ extends AbstractFunction1<SlotExpression,SlottedGroupingExpression1> implements Serializable
{
    public static SlottedGroupingExpression1$ MODULE$;

    static
    {
        new SlottedGroupingExpression1$();
    }

    private SlottedGroupingExpression1$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedGroupingExpression1";
    }

    public SlottedGroupingExpression1 apply( final SlotExpression groupingExpression )
    {
        return new SlottedGroupingExpression1( groupingExpression );
    }

    public Option<SlotExpression> unapply( final SlottedGroupingExpression1 x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.groupingExpression() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
