package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.v4_0.expressions.LabelToken;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple7;
import scala.collection.Seq;
import scala.runtime.BoxesRunTime;

public final class NodeIndexScanSlottedPipe$ implements Serializable
{
    public static NodeIndexScanSlottedPipe$ MODULE$;

    static
    {
        new NodeIndexScanSlottedPipe$();
    }

    private NodeIndexScanSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$8( final String ident, final LabelToken label, final Seq<SlottedIndexedProperty> properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "NodeIndexScanSlottedPipe";
    }

    public NodeIndexScanSlottedPipe apply( final String ident, final LabelToken label, final Seq<SlottedIndexedProperty> properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new NodeIndexScanSlottedPipe( ident, label, properties, queryIndexId, indexOrder, slots, argumentSize, id );
    }

    public int apply$default$8( final String ident, final LabelToken label, final Seq<SlottedIndexedProperty> properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple7<String,LabelToken,Seq<SlottedIndexedProperty>,Object,IndexOrder,SlotConfiguration,SlotConfiguration.Size>> unapply(
            final NodeIndexScanSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple7( x$0.ident(), x$0.label(), x$0.properties(), BoxesRunTime.boxToInteger( x$0.queryIndexId() ), x$0.indexOrder(), x$0.slots(),
                    x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
