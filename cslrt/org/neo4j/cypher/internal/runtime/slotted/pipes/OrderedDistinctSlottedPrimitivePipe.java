package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrefetchingIterator;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.storable.LongArray;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class OrderedDistinctSlottedPrimitivePipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final SlotConfiguration slots;
    private final int[] primitiveSlots;
    private final int[] orderedPrimitiveSlots;
    private final GroupingExpression groupingExpression;
    private final int id;

    public OrderedDistinctSlottedPrimitivePipe( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots, final int[] orderedPrimitiveSlots,
            final GroupingExpression groupingExpression, final int id )
    {
        super( source );
        this.source = source;
        this.slots = slots;
        this.primitiveSlots = primitiveSlots;
        this.orderedPrimitiveSlots = orderedPrimitiveSlots;
        this.groupingExpression = groupingExpression;
        this.id = id;
        Product.$init$( this );
        groupingExpression.registerOwningPipe( this );
    }

    public static int $lessinit$greater$default$6( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final int[] orderedPrimitiveSlots, final GroupingExpression groupingExpression )
    {
        return OrderedDistinctSlottedPrimitivePipe$.MODULE$.$lessinit$greater$default$6( var0, var1, var2, var3, var4 );
    }

    public static int apply$default$6( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots, final int[] orderedPrimitiveSlots,
            final GroupingExpression groupingExpression )
    {
        return OrderedDistinctSlottedPrimitivePipe$.MODULE$.apply$default$6( var0, var1, var2, var3, var4 );
    }

    public static Option<Tuple5<Pipe,SlotConfiguration,int[],int[],GroupingExpression>> unapply( final OrderedDistinctSlottedPrimitivePipe x$0 )
    {
        return OrderedDistinctSlottedPrimitivePipe$.MODULE$.unapply( var0 );
    }

    public static OrderedDistinctSlottedPrimitivePipe apply( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final int[] orderedPrimitiveSlots, final GroupingExpression groupingExpression, final int id )
    {
        return OrderedDistinctSlottedPrimitivePipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int[] primitiveSlots()
    {
        return this.primitiveSlots;
    }

    public int[] orderedPrimitiveSlots()
    {
        return this.orderedPrimitiveSlots;
    }

    public GroupingExpression groupingExpression()
    {
        return this.groupingExpression;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return new PrefetchingIterator<ExecutionContext>( this, input, state )
        {
            private final Iterator input$1;
            private final QueryState state$1;
            private MutableSet<LongArray> seen;
            private LongArray currentOrderedGroupingValue;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.input$1 = input$1;
                    this.state$1 = state$1;
                    this.seen = Sets.mutable.empty();
                }
            }

            private MutableSet<LongArray> seen()
            {
                return this.seen;
            }

            private void seen_$eq( final MutableSet<LongArray> x$1 )
            {
                this.seen = x$1;
            }

            private LongArray currentOrderedGroupingValue()
            {
                return this.currentOrderedGroupingValue;
            }

            private void currentOrderedGroupingValue_$eq( final LongArray x$1 )
            {
                this.currentOrderedGroupingValue = x$1;
            }

            public Option<ExecutionContext> produceNext()
            {
                ExecutionContext next;
                LongArray groupingValue;
                do
                {
                    if ( !this.input$1.hasNext() )
                    {
                        return .MODULE$;
                    }

                    next = (ExecutionContext) this.input$1.next();
                    groupingValue = DistinctSlottedPrimitivePipe$.MODULE$.buildGroupingValue( next, this.$outer.primitiveSlots() );
                    LongArray orderedGroupingValue = DistinctSlottedPrimitivePipe$.MODULE$.buildGroupingValue( next, this.$outer.orderedPrimitiveSlots() );
                    if ( this.currentOrderedGroupingValue() != null )
                    {
                        LongArray var10000 = this.currentOrderedGroupingValue();
                        if ( var10000 == null )
                        {
                            if ( orderedGroupingValue == null )
                            {
                                continue;
                            }
                        }
                        else if ( var10000.equals( orderedGroupingValue ) )
                        {
                            continue;
                        }
                    }

                    this.currentOrderedGroupingValue_$eq( orderedGroupingValue );
                    this.seen().forEach( ( value ) -> {
                        this.state$1.memoryTracker().deallocated( value );
                    } );
                    this.seen_$eq( Sets.mutable.empty() );
                }
                while ( !this.seen().add( groupingValue ) );

                this.state$1.memoryTracker().allocated( groupingValue );
                this.$outer.groupingExpression().project( next, this.$outer.groupingExpression().computeGroupingKey( next, this.state$1 ) );
                return new Some( next );
            }
        };
    }

    public OrderedDistinctSlottedPrimitivePipe copy( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final int[] orderedPrimitiveSlots, final GroupingExpression groupingExpression, final int id )
    {
        return new OrderedDistinctSlottedPrimitivePipe( source, slots, primitiveSlots, orderedPrimitiveSlots, groupingExpression, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public SlotConfiguration copy$default$2()
    {
        return this.slots();
    }

    public int[] copy$default$3()
    {
        return this.primitiveSlots();
    }

    public int[] copy$default$4()
    {
        return this.orderedPrimitiveSlots();
    }

    public GroupingExpression copy$default$5()
    {
        return this.groupingExpression();
    }

    public String productPrefix()
    {
        return "OrderedDistinctSlottedPrimitivePipe";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.slots();
            break;
        case 2:
            var10000 = this.primitiveSlots();
            break;
        case 3:
            var10000 = this.orderedPrimitiveSlots();
            break;
        case 4:
            var10000 = this.groupingExpression();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof OrderedDistinctSlottedPrimitivePipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label76:
            {
                boolean var2;
                if ( x$1 instanceof OrderedDistinctSlottedPrimitivePipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label58:
                    {
                        label67:
                        {
                            OrderedDistinctSlottedPrimitivePipe var4 = (OrderedDistinctSlottedPrimitivePipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label67;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label67;
                            }

                            SlotConfiguration var8 = this.slots();
                            SlotConfiguration var6 = var4.slots();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label67;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label67;
                            }

                            if ( this.primitiveSlots() == var4.primitiveSlots() && this.orderedPrimitiveSlots() == var4.orderedPrimitiveSlots() )
                            {
                                label45:
                                {
                                    GroupingExpression var9 = this.groupingExpression();
                                    GroupingExpression var7 = var4.groupingExpression();
                                    if ( var9 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label45;
                                        }
                                    }
                                    else if ( !var9.equals( var7 ) )
                                    {
                                        break label45;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var10 = true;
                                        break label58;
                                    }
                                }
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label76;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
