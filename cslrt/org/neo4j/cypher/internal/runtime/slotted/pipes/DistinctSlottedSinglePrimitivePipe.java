package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.eclipse.collections.api.set.primitive.MutableLongSet;
import org.eclipse.collections.impl.factory.primitive.LongSets;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrefetchingIterator;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class DistinctSlottedSinglePrimitivePipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final SlotConfiguration slots;
    private final Slot toSlot;
    private final int offset;
    private final Expression expression;
    private final int id;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> org$neo4j$cypher$internal$runtime$slotted$pipes$DistinctSlottedSinglePrimitivePipe$$setInSlot;

    public DistinctSlottedSinglePrimitivePipe( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset,
            final Expression expression, final int id )
    {
        super( source );
        this.source = source;
        this.slots = slots;
        this.toSlot = toSlot;
        this.offset = offset;
        this.expression = expression;
        this.id = id;
        Product.$init$( this );
        this.org$neo4j$cypher$internal$runtime$slotted$pipes$DistinctSlottedSinglePrimitivePipe$$setInSlot =
                SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( toSlot );
        expression.registerOwningPipe( this );
    }

    public static int $lessinit$greater$default$6( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset,
            final Expression expression )
    {
        return DistinctSlottedSinglePrimitivePipe$.MODULE$.$lessinit$greater$default$6( var0, var1, var2, var3, var4 );
    }

    public static int apply$default$6( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset, final Expression expression )
    {
        return DistinctSlottedSinglePrimitivePipe$.MODULE$.apply$default$6( var0, var1, var2, var3, var4 );
    }

    public static Option<Tuple5<Pipe,SlotConfiguration,Slot,Object,Expression>> unapply( final DistinctSlottedSinglePrimitivePipe x$0 )
    {
        return DistinctSlottedSinglePrimitivePipe$.MODULE$.unapply( var0 );
    }

    public static DistinctSlottedSinglePrimitivePipe apply( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset,
            final Expression expression, final int id )
    {
        return DistinctSlottedSinglePrimitivePipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public Slot toSlot()
    {
        return this.toSlot;
    }

    public int offset()
    {
        return this.offset;
    }

    public Expression expression()
    {
        return this.expression;
    }

    public int id()
    {
        return this.id;
    }

    public Function2<ExecutionContext,AnyValue,BoxedUnit> org$neo4j$cypher$internal$runtime$slotted$pipes$DistinctSlottedSinglePrimitivePipe$$setInSlot()
    {
        return this.org$neo4j$cypher$internal$runtime$slotted$pipes$DistinctSlottedSinglePrimitivePipe$$setInSlot;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return new PrefetchingIterator<ExecutionContext>( this, input, state )
        {
            private final MutableLongSet seen;
            private final Iterator input$1;
            private final QueryState state$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.input$1 = input$1;
                    this.state$1 = state$1;
                    this.seen = LongSets.mutable.empty();
                }
            }

            private MutableLongSet seen()
            {
                return this.seen;
            }

            public Option<ExecutionContext> produceNext()
            {
                while ( true )
                {
                    if ( this.input$1.hasNext() )
                    {
                        ExecutionContext next = (ExecutionContext) this.input$1.next();
                        long id = next.getLongAt( this.$outer.offset() );
                        if ( !this.seen().add( id ) )
                        {
                            continue;
                        }

                        this.state$1.memoryTracker().allocated( 8L );
                        AnyValue outputValue = this.$outer.expression().apply( next, this.state$1 );
                        this.$outer.org$neo4j$cypher$internal$runtime$slotted$pipes$DistinctSlottedSinglePrimitivePipe$$setInSlot().apply( next, outputValue );
                        return new Some( next );
                    }

                    return .MODULE$;
                }
            }
        };
    }

    public DistinctSlottedSinglePrimitivePipe copy( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset,
            final Expression expression, final int id )
    {
        return new DistinctSlottedSinglePrimitivePipe( source, slots, toSlot, offset, expression, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public SlotConfiguration copy$default$2()
    {
        return this.slots();
    }

    public Slot copy$default$3()
    {
        return this.toSlot();
    }

    public int copy$default$4()
    {
        return this.offset();
    }

    public Expression copy$default$5()
    {
        return this.expression();
    }

    public String productPrefix()
    {
        return "DistinctSlottedSinglePrimitivePipe";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.slots();
            break;
        case 2:
            var10000 = this.toSlot();
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 4:
            var10000 = this.expression();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof DistinctSlottedSinglePrimitivePipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.source() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.toSlot() ) );
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.expression() ) );
        return Statics.finalizeHash( var1, 5 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label83:
            {
                boolean var2;
                if ( x$1 instanceof DistinctSlottedSinglePrimitivePipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label65:
                    {
                        label74:
                        {
                            DistinctSlottedSinglePrimitivePipe var4 = (DistinctSlottedSinglePrimitivePipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label74;
                            }

                            SlotConfiguration var9 = this.slots();
                            SlotConfiguration var6 = var4.slots();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label74;
                            }

                            Slot var10 = this.toSlot();
                            Slot var7 = var4.toSlot();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label74;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label74;
                            }

                            if ( this.offset() == var4.offset() )
                            {
                                label48:
                                {
                                    Expression var11 = this.expression();
                                    Expression var8 = var4.expression();
                                    if ( var11 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label48;
                                        }
                                    }
                                    else if ( !var11.equals( var8 ) )
                                    {
                                        break label48;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var12 = true;
                                        break label65;
                                    }
                                }
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label83;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
