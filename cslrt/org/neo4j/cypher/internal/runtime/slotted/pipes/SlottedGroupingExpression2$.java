package org.neo4j.cypher.internal.runtime.slotted.pipes;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class SlottedGroupingExpression2$ extends AbstractFunction2<SlotExpression,SlotExpression,SlottedGroupingExpression2> implements Serializable
{
    public static SlottedGroupingExpression2$ MODULE$;

    static
    {
        new SlottedGroupingExpression2$();
    }

    private SlottedGroupingExpression2$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedGroupingExpression2";
    }

    public SlottedGroupingExpression2 apply( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2 )
    {
        return new SlottedGroupingExpression2( groupingExpression1, groupingExpression2 );
    }

    public Option<Tuple2<SlotExpression,SlotExpression>> unapply( final SlottedGroupingExpression2 x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.groupingExpression1(), x$0.groupingExpression2() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
