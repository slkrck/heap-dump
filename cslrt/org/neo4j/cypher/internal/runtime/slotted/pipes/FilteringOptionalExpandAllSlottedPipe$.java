package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple8;
import scala.None.;
import scala.runtime.BoxesRunTime;

public final class FilteringOptionalExpandAllSlottedPipe$ implements Serializable
{
    public static FilteringOptionalExpandAllSlottedPipe$ MODULE$;

    static
    {
        new FilteringOptionalExpandAllSlottedPipe$();
    }

    private FilteringOptionalExpandAllSlottedPipe$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "FilteringOptionalExpandAllSlottedPipe";
    }

    public FilteringOptionalExpandAllSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset,
            final SemanticDirection dir, final RelationshipTypes types, final SlotConfiguration slots, final Expression predicate, final int id )
    {
        return new FilteringOptionalExpandAllSlottedPipe( source, fromSlot, relOffset, toOffset, dir, types, slots, predicate, id );
    }

    public Option<Tuple8<Pipe,Slot,Object,Object,SemanticDirection,RelationshipTypes,SlotConfiguration,Expression>> unapply(
            final FilteringOptionalExpandAllSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple8( x$0.source(), x$0.fromSlot(), BoxesRunTime.boxToInteger( x$0.relOffset() ), BoxesRunTime.boxToInteger( x$0.toOffset() ), x$0.dir(),
                    x$0.types(), x$0.slots(), x$0.predicate() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
