package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple8;
import scala.None.;
import scala.runtime.AbstractFunction8;
import scala.runtime.BoxesRunTime;

public final class CreateRelationshipSlottedCommand$ extends
        AbstractFunction8<Object,ToLongFunction<ExecutionContext>,LazyType,ToLongFunction<ExecutionContext>,Option<Expression>,String,String,String,CreateRelationshipSlottedCommand>
        implements Serializable
{
    public static CreateRelationshipSlottedCommand$ MODULE$;

    static
    {
        new CreateRelationshipSlottedCommand$();
    }

    private CreateRelationshipSlottedCommand$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CreateRelationshipSlottedCommand";
    }

    public CreateRelationshipSlottedCommand apply( final int relIdOffset, final ToLongFunction<ExecutionContext> startNodeIdGetter, final LazyType relType,
            final ToLongFunction<ExecutionContext> endNodeIdGetter, final Option<Expression> properties, final String relName, final String startName,
            final String endName )
    {
        return new CreateRelationshipSlottedCommand( relIdOffset, startNodeIdGetter, relType, endNodeIdGetter, properties, relName, startName, endName );
    }

    public Option<Tuple8<Object,ToLongFunction<ExecutionContext>,LazyType,ToLongFunction<ExecutionContext>,Option<Expression>,String,String,String>> unapply(
            final CreateRelationshipSlottedCommand x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple8( BoxesRunTime.boxToInteger( x$0.relIdOffset() ), x$0.startNodeIdGetter(), x$0.relType(), x$0.endNodeIdGetter(), x$0.properties(),
                    x$0.relName(), x$0.startName(), x$0.endName() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
