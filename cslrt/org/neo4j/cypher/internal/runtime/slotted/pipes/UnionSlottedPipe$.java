package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;

public final class UnionSlottedPipe$ implements Serializable
{
    public static UnionSlottedPipe$ MODULE$;

    static
    {
        new UnionSlottedPipe$();
    }

    private UnionSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$6( final Pipe lhs, final Pipe rhs, final SlotConfiguration slots, final UnionSlottedPipe.RowMapping lhsMapping,
            final UnionSlottedPipe.RowMapping rhsMapping )
    {
        return .MODULE$.INVALID_ID();
    }

    public UnionSlottedPipe apply( final Pipe lhs, final Pipe rhs, final SlotConfiguration slots, final UnionSlottedPipe.RowMapping lhsMapping,
            final UnionSlottedPipe.RowMapping rhsMapping, final int id )
    {
        return new UnionSlottedPipe( lhs, rhs, slots, lhsMapping, rhsMapping, id );
    }

    public int apply$default$6( final Pipe lhs, final Pipe rhs, final SlotConfiguration slots, final UnionSlottedPipe.RowMapping lhsMapping,
            final UnionSlottedPipe.RowMapping rhsMapping )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple5<Pipe,Pipe,SlotConfiguration,UnionSlottedPipe.RowMapping,UnionSlottedPipe.RowMapping>> unapply( final UnionSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple5( x$0.lhs(), x$0.rhs(), x$0.slots(), x$0.lhsMapping(), x$0.rhsMapping() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
