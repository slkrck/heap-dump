package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.None.;

public final class OptionalExpandAllSlottedPipe$
{
    public static OptionalExpandAllSlottedPipe$ MODULE$;

    static
    {
        new OptionalExpandAllSlottedPipe$();
    }

    private OptionalExpandAllSlottedPipe$()
    {
        MODULE$ = this;
    }

    public OptionalExpandAllSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots, final Option<Expression> maybePredicate, final int id )
    {
        Object var10;
        if ( maybePredicate instanceof Some )
        {
            Some var12 = (Some) maybePredicate;
            Expression predicate = (Expression) var12.value();
            var10 = new FilteringOptionalExpandAllSlottedPipe( source, fromSlot, relOffset, toOffset, dir, types, slots, predicate, id );
        }
        else
        {
            if ( !.MODULE$.equals( maybePredicate )){
            throw new MatchError( maybePredicate );
        }

            var10 = new NonFilteringOptionalExpandAllSlottedPipe( source, fromSlot, relOffset, toOffset, dir, types, slots, id );
        }

        return (OptionalExpandAllSlottedPipe) var10;
    }

    public int apply$default$9( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots, final Option<Expression> maybePredicate )
    {
        return org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID();
    }
}
