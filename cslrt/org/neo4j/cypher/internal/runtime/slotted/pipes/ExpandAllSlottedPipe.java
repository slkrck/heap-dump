package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.RelationshipIterator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.exceptions.InternalException;
import org.neo4j.storageengine.api.RelationshipVisitor;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple7;
import scala.collection.Iterator;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.LongRef;
import scala.runtime.Statics;

@JavaDocToJava
public class ExpandAllSlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe source;
    private final Slot fromSlot;
    private final int relOffset;
    private final int toOffset;
    private final SemanticDirection dir;
    private final RelationshipTypes types;
    private final SlotConfiguration slots;
    private final int id;
    private final ToLongFunction<ExecutionContext> getFromNodeFunction;

    public ExpandAllSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots, final int id )
    {
        super( source );
        this.source = source;
        this.fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.toOffset = toOffset;
        this.dir = dir;
        this.types = types;
        this.slots = slots;
        this.id = id;
        Product.$init$( this );
        this.getFromNodeFunction = SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( fromSlot,
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() );
    }

    public static int $lessinit$greater$default$8( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots )
    {
        return ExpandAllSlottedPipe$.MODULE$.$lessinit$greater$default$8( var0, var1, var2, var3, var4, var5, var6 );
    }

    public static int apply$default$8( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots )
    {
        return ExpandAllSlottedPipe$.MODULE$.apply$default$8( var0, var1, var2, var3, var4, var5, var6 );
    }

    public static Option<Tuple7<Pipe,Slot,Object,Object,SemanticDirection,RelationshipTypes,SlotConfiguration>> unapply( final ExpandAllSlottedPipe x$0 )
    {
        return ExpandAllSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ExpandAllSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots, final int id )
    {
        return ExpandAllSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public Slot fromSlot()
    {
        return this.fromSlot;
    }

    public int relOffset()
    {
        return this.relOffset;
    }

    public int toOffset()
    {
        return this.toOffset;
    }

    public SemanticDirection dir()
    {
        return this.dir;
    }

    public RelationshipTypes types()
    {
        return this.types;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int id()
    {
        return this.id;
    }

    private ToLongFunction<ExecutionContext> getFromNodeFunction()
    {
        return this.getFromNodeFunction;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.flatMap( ( inputRow ) -> {
            long fromNode = this.getFromNodeFunction().applyAsLong( inputRow );
            Iterator var10000;
            if ( NullChecker$.MODULE$.entityIsNull( fromNode ) )
            {
                var10000 = .MODULE$.Iterator().empty();
            }
            else
            {
                RelationshipIterator relationships = state.query().getRelationshipsForIdsPrimitive( fromNode, this.dir(), this.types().types( state.query() ) );
                LongRef otherSide = LongRef.create( 0L );
                RelationshipVisitor relVisitor = new RelationshipVisitor<InternalException>( (ExpandAllSlottedPipe) null, fromNode, otherSide )
                {
                    private final long fromNode$1;
                    private final LongRef otherSide$1;

                    public
                    {
                        this.fromNode$1 = fromNode$1;
                        this.otherSide$1 = otherSide$1;
                    }

                    public void visit( final long relationshipId, final int typeId, final long startNodeId, final long endNodeId )
                    {
                        if ( this.fromNode$1 == startNodeId )
                        {
                            this.otherSide$1.elem = endNodeId;
                        }
                        else
                        {
                            this.otherSide$1.elem = startNodeId;
                        }
                    }
                };
                var10000 = org.neo4j.cypher.internal.runtime.PrimitiveLongHelper..MODULE$.map( relationships, ( relId ) -> {
                return $anonfun$internalCreateResults$2( this, relationships, otherSide, relVisitor, inputRow, BoxesRunTime.unboxToLong( relId ) );
            } );
            }

            return var10000;
        } );
    }

    public ExpandAllSlottedPipe copy( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots, final int id )
    {
        return new ExpandAllSlottedPipe( source, fromSlot, relOffset, toOffset, dir, types, slots, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public Slot copy$default$2()
    {
        return this.fromSlot();
    }

    public int copy$default$3()
    {
        return this.relOffset();
    }

    public int copy$default$4()
    {
        return this.toOffset();
    }

    public SemanticDirection copy$default$5()
    {
        return this.dir();
    }

    public RelationshipTypes copy$default$6()
    {
        return this.types();
    }

    public SlotConfiguration copy$default$7()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "ExpandAllSlottedPipe";
    }

    public int productArity()
    {
        return 7;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.fromSlot();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.relOffset() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.toOffset() );
            break;
        case 4:
            var10000 = this.dir();
            break;
        case 5:
            var10000 = this.types();
            break;
        case 6:
            var10000 = this.slots();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ExpandAllSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.source() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fromSlot() ) );
        var1 = Statics.mix( var1, this.relOffset() );
        var1 = Statics.mix( var1, this.toOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.dir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.types() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        return Statics.finalizeHash( var1, 7 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var14;
        if ( this != x$1 )
        {
            label95:
            {
                boolean var2;
                if ( x$1 instanceof ExpandAllSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label76:
                    {
                        label85:
                        {
                            ExpandAllSlottedPipe var4 = (ExpandAllSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label85;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label85;
                            }

                            Slot var10 = this.fromSlot();
                            Slot var6 = var4.fromSlot();
                            if ( var10 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label85;
                                }
                            }
                            else if ( !var10.equals( var6 ) )
                            {
                                break label85;
                            }

                            if ( this.relOffset() == var4.relOffset() && this.toOffset() == var4.toOffset() )
                            {
                                label86:
                                {
                                    SemanticDirection var11 = this.dir();
                                    SemanticDirection var7 = var4.dir();
                                    if ( var11 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label86;
                                        }
                                    }
                                    else if ( !var11.equals( var7 ) )
                                    {
                                        break label86;
                                    }

                                    RelationshipTypes var12 = this.types();
                                    RelationshipTypes var8 = var4.types();
                                    if ( var12 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label86;
                                        }
                                    }
                                    else if ( !var12.equals( var8 ) )
                                    {
                                        break label86;
                                    }

                                    SlotConfiguration var13 = this.slots();
                                    SlotConfiguration var9 = var4.slots();
                                    if ( var13 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label86;
                                        }
                                    }
                                    else if ( !var13.equals( var9 ) )
                                    {
                                        break label86;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var14 = true;
                                        break label76;
                                    }
                                }
                            }
                        }

                        var14 = false;
                    }

                    if ( var14 )
                    {
                        break label95;
                    }
                }

                var14 = false;
                return var14;
            }
        }

        var14 = true;
        return var14;
    }
}
