package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;

public final class OrderedDistinctSlottedPrimitivePipe$ implements Serializable
{
    public static OrderedDistinctSlottedPrimitivePipe$ MODULE$;

    static
    {
        new OrderedDistinctSlottedPrimitivePipe$();
    }

    private OrderedDistinctSlottedPrimitivePipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$6( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots, final int[] orderedPrimitiveSlots,
            final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "OrderedDistinctSlottedPrimitivePipe";
    }

    public OrderedDistinctSlottedPrimitivePipe apply( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final int[] orderedPrimitiveSlots, final GroupingExpression groupingExpression, final int id )
    {
        return new OrderedDistinctSlottedPrimitivePipe( source, slots, primitiveSlots, orderedPrimitiveSlots, groupingExpression, id );
    }

    public int apply$default$6( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots, final int[] orderedPrimitiveSlots,
            final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple5<Pipe,SlotConfiguration,int[],int[],GroupingExpression>> unapply( final OrderedDistinctSlottedPrimitivePipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :
        new Some( new Tuple5( x$0.source(), x$0.slots(), x$0.primitiveSlots(), x$0.orderedPrimitiveSlots(), x$0.groupingExpression() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
