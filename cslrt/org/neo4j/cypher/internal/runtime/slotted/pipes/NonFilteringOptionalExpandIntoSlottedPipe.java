package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.eclipse.collections.api.iterator.LongIterator;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrimitiveLongHelper.;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple7;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class NonFilteringOptionalExpandIntoSlottedPipe extends OptionalExpandIntoSlottedPipe implements Product, Serializable
{
    private final Pipe source;
    private final Slot fromSlot;
    private final int relOffset;
    private final Slot toSlot;
    private final SemanticDirection dir;
    private final RelationshipTypes lazyTypes;
    private final SlotConfiguration slots;
    private final int id;

    public NonFilteringOptionalExpandIntoSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot,
            final SemanticDirection dir, final RelationshipTypes lazyTypes, final SlotConfiguration slots, final int id )
    {
        super( source, fromSlot, relOffset, toSlot, dir, lazyTypes, slots );
        this.source = source;
        this.fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.toSlot = toSlot;
        this.dir = dir;
        this.lazyTypes = lazyTypes;
        this.slots = slots;
        this.id = id;
        Product.$init$( this );
    }

    public static Option<Tuple7<Pipe,Slot,Object,Slot,SemanticDirection,RelationshipTypes,SlotConfiguration>> unapply(
            final NonFilteringOptionalExpandIntoSlottedPipe x$0 )
    {
        return NonFilteringOptionalExpandIntoSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static NonFilteringOptionalExpandIntoSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot,
            final SemanticDirection dir, final RelationshipTypes lazyTypes, final SlotConfiguration slots, final int id )
    {
        return NonFilteringOptionalExpandIntoSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public Slot fromSlot()
    {
        return this.fromSlot;
    }

    public int relOffset()
    {
        return this.relOffset;
    }

    public Slot toSlot()
    {
        return this.toSlot;
    }

    public SemanticDirection dir()
    {
        return this.dir;
    }

    public RelationshipTypes lazyTypes()
    {
        return this.lazyTypes;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<SlottedExecutionContext> findMatchIterator( final ExecutionContext inputRow, final QueryState state, final LongIterator relationships )
    {
        return .MODULE$.map( relationships, ( relId ) -> {
        return $anonfun$findMatchIterator$1( this, inputRow, BoxesRunTime.unboxToLong( relId ) );
    } );
    }

    public NonFilteringOptionalExpandIntoSlottedPipe copy( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot,
            final SemanticDirection dir, final RelationshipTypes lazyTypes, final SlotConfiguration slots, final int id )
    {
        return new NonFilteringOptionalExpandIntoSlottedPipe( source, fromSlot, relOffset, toSlot, dir, lazyTypes, slots, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public Slot copy$default$2()
    {
        return this.fromSlot();
    }

    public int copy$default$3()
    {
        return this.relOffset();
    }

    public Slot copy$default$4()
    {
        return this.toSlot();
    }

    public SemanticDirection copy$default$5()
    {
        return this.dir();
    }

    public RelationshipTypes copy$default$6()
    {
        return this.lazyTypes();
    }

    public SlotConfiguration copy$default$7()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "NonFilteringOptionalExpandIntoSlottedPipe";
    }

    public int productArity()
    {
        return 7;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.fromSlot();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.relOffset() );
            break;
        case 3:
            var10000 = this.toSlot();
            break;
        case 4:
            var10000 = this.dir();
            break;
        case 5:
            var10000 = this.lazyTypes();
            break;
        case 6:
            var10000 = this.slots();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NonFilteringOptionalExpandIntoSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.source() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fromSlot() ) );
        var1 = Statics.mix( var1, this.relOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.toSlot() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.dir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.lazyTypes() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        return Statics.finalizeHash( var1, 7 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var15;
        if ( this != x$1 )
        {
            label102:
            {
                boolean var2;
                if ( x$1 instanceof NonFilteringOptionalExpandIntoSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label83:
                    {
                        label92:
                        {
                            NonFilteringOptionalExpandIntoSlottedPipe var4 = (NonFilteringOptionalExpandIntoSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label92;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label92;
                            }

                            Slot var11 = this.fromSlot();
                            Slot var6 = var4.fromSlot();
                            if ( var11 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label92;
                                }
                            }
                            else if ( !var11.equals( var6 ) )
                            {
                                break label92;
                            }

                            if ( this.relOffset() == var4.relOffset() )
                            {
                                label93:
                                {
                                    var11 = this.toSlot();
                                    Slot var7 = var4.toSlot();
                                    if ( var11 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var11.equals( var7 ) )
                                    {
                                        break label93;
                                    }

                                    SemanticDirection var12 = this.dir();
                                    SemanticDirection var8 = var4.dir();
                                    if ( var12 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var12.equals( var8 ) )
                                    {
                                        break label93;
                                    }

                                    RelationshipTypes var13 = this.lazyTypes();
                                    RelationshipTypes var9 = var4.lazyTypes();
                                    if ( var13 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var13.equals( var9 ) )
                                    {
                                        break label93;
                                    }

                                    SlotConfiguration var14 = this.slots();
                                    SlotConfiguration var10 = var4.slots();
                                    if ( var14 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var14.equals( var10 ) )
                                    {
                                        break label93;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var15 = true;
                                        break label83;
                                    }
                                }
                            }
                        }

                        var15 = false;
                    }

                    if ( var15 )
                    {
                        break label102;
                    }
                }

                var15 = false;
                return var15;
            }
        }

        var15 = true;
        return var15;
    }
}
