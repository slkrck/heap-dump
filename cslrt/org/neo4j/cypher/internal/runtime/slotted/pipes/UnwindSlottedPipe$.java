package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.runtime.BoxesRunTime;

public final class UnwindSlottedPipe$ implements Serializable
{
    public static UnwindSlottedPipe$ MODULE$;

    static
    {
        new UnwindSlottedPipe$();
    }

    private UnwindSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$5( final Pipe source, final Expression collection, final int offset, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "UnwindSlottedPipe";
    }

    public UnwindSlottedPipe apply( final Pipe source, final Expression collection, final int offset, final SlotConfiguration slots, final int id )
    {
        return new UnwindSlottedPipe( source, collection, offset, slots, id );
    }

    public int apply$default$5( final Pipe source, final Expression collection, final int offset, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple4<Pipe,Expression,Object,SlotConfiguration>> unapply( final UnwindSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :
        new Some( new Tuple4( x$0.source(), x$0.collection(), BoxesRunTime.boxToInteger( x$0.offset() ), x$0.slots() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
