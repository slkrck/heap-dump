package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel.;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NodesByLabelScanSlottedPipe implements Pipe, Product, Serializable
{
    private final String ident;
    private final LazyLabel label;
    private final SlotConfiguration slots;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private final int offset;
    private ExecutionContextFactory executionContextFactory;

    public NodesByLabelScanSlottedPipe( final String ident, final LazyLabel label, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize,
            final int id )
    {
        this.ident = ident;
        this.label = label;
        this.slots = slots;
        this.argumentSize = argumentSize;
        this.id = id;
        Pipe.$init$( this );
        Product.$init$( this );
        this.offset = slots.getLongOffsetFor( ident );
    }

    public static int $lessinit$greater$default$5( final String ident, final LazyLabel label, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return NodesByLabelScanSlottedPipe$.MODULE$.$lessinit$greater$default$5( var0, var1, var2, var3 );
    }

    public static int apply$default$5( final String ident, final LazyLabel label, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return NodesByLabelScanSlottedPipe$.MODULE$.apply$default$5( var0, var1, var2, var3 );
    }

    public static Option<Tuple4<String,LazyLabel,SlotConfiguration,SlotConfiguration.Size>> unapply( final NodesByLabelScanSlottedPipe x$0 )
    {
        return NodesByLabelScanSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static NodesByLabelScanSlottedPipe apply( final String ident, final LazyLabel label, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        return NodesByLabelScanSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public Iterator<ExecutionContext> createResults( final QueryState state )
    {
        return Pipe.createResults$( this, state );
    }

    public ExecutionContextFactory executionContextFactory()
    {
        return this.executionContextFactory;
    }

    public void executionContextFactory_$eq( final ExecutionContextFactory x$1 )
    {
        this.executionContextFactory = x$1;
    }

    public String ident()
    {
        return this.ident;
    }

    public LazyLabel label()
    {
        return this.label;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    private int offset()
    {
        return this.offset;
    }

    public Iterator<ExecutionContext> internalCreateResults( final QueryState state )
    {
        int labelId = this.label().getId( state.query() );
        return labelId == .MODULE$.UNKNOWN() ? scala.package..MODULE$.Iterator().empty() :org.neo4j.cypher.internal.runtime.PrimitiveLongHelper..
        MODULE$.map( state.query().getNodesByLabelPrimitive( labelId ), ( nodeId ) -> {
            return $anonfun$internalCreateResults$1( this, state, BoxesRunTime.unboxToLong( nodeId ) );
        } );
    }

    public NodesByLabelScanSlottedPipe copy( final String ident, final LazyLabel label, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        return new NodesByLabelScanSlottedPipe( ident, label, slots, argumentSize, id );
    }

    public String copy$default$1()
    {
        return this.ident();
    }

    public LazyLabel copy$default$2()
    {
        return this.label();
    }

    public SlotConfiguration copy$default$3()
    {
        return this.slots();
    }

    public SlotConfiguration.Size copy$default$4()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "NodesByLabelScanSlottedPipe";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.ident();
            break;
        case 1:
            var10000 = this.label();
            break;
        case 2:
            var10000 = this.slots();
            break;
        case 3:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NodesByLabelScanSlottedPipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        if ( this != x$1 )
        {
            label81:
            {
                boolean var2;
                if ( x$1 instanceof NodesByLabelScanSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label63:
                    {
                        label72:
                        {
                            NodesByLabelScanSlottedPipe var4 = (NodesByLabelScanSlottedPipe) x$1;
                            String var10000 = this.ident();
                            String var5 = var4.ident();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label72;
                            }

                            LazyLabel var9 = this.label();
                            LazyLabel var6 = var4.label();
                            if ( var9 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var9.equals( var6 ) )
                            {
                                break label72;
                            }

                            SlotConfiguration var10 = this.slots();
                            SlotConfiguration var7 = var4.slots();
                            if ( var10 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var10.equals( var7 ) )
                            {
                                break label72;
                            }

                            SlotConfiguration.Size var11 = this.argumentSize();
                            SlotConfiguration.Size var8 = var4.argumentSize();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label72;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label72;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var12 = true;
                                break label63;
                            }
                        }

                        var12 = false;
                    }

                    if ( var12 )
                    {
                        break label81;
                    }
                }

                var12 = false;
                return var12;
            }
        }

        var12 = true;
        return var12;
    }
}
