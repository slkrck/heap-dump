package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple6;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.reflect.ClassTag.;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class RollUpApplySlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe lhs;
    private final Pipe rhs;
    private final int collectionRefSlotOffset;
    private final Tuple2<String,Expression> identifierToCollect;
    private final Set<String> nullableIdentifiers;
    private final SlotConfiguration slots;
    private final int id;
    private final Function1<QueryState,Function1<ExecutionContext,AnyValue>> getValueToCollectFunction;
    private final Seq<Function1<ExecutionContext,Object>> hasNullValuePredicates;

    public RollUpApplySlottedPipe( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset, final Tuple2<String,Expression> identifierToCollect,
            final Set<String> nullableIdentifiers, final SlotConfiguration slots, final int id )
    {
        super( lhs );
        this.lhs = lhs;
        this.rhs = rhs;
        this.collectionRefSlotOffset = collectionRefSlotOffset;
        this.identifierToCollect = identifierToCollect;
        this.nullableIdentifiers = nullableIdentifiers;
        this.slots = slots;
        this.id = id;
        Product.$init$( this );
        ((Expression) identifierToCollect._2()).registerOwningPipe( this );
        Expression expression = (Expression) identifierToCollect._2();
        this.getValueToCollectFunction = ( state ) -> {
            return ( ctx ) -> {
                return expression.apply( ctx, state );
            };
        };
        this.hasNullValuePredicates = (Seq) nullableIdentifiers.toSeq().map( ( elem ) -> {
            Option elemSlot = this.slots().get( elem );
            boolean var4 = false;
            Some var5 = null;
            Function1 var2;
            if ( elemSlot instanceof Some )
            {
                var4 = true;
                var5 = (Some) elemSlot;
                Slot var7 = (Slot) var5.value();
                if ( var7 instanceof LongSlot )
                {
                    LongSlot var8 = (LongSlot) var7;
                    int offsetx = var8.offset();
                    boolean var10 = var8.nullable();
                    if ( var10 )
                    {
                        var2 = ( ctx ) -> {
                            return BoxesRunTime.boxToBoolean( $anonfun$hasNullValuePredicates$2( offsetx, ctx ) );
                        };
                        return var2;
                    }
                }
            }

            if ( var4 )
            {
                Slot var11 = (Slot) var5.value();
                if ( var11 instanceof RefSlot )
                {
                    RefSlot var12 = (RefSlot) var11;
                    int offset = var12.offset();
                    boolean var14 = var12.nullable();
                    if ( var14 )
                    {
                        var2 = ( ctx ) -> {
                            return BoxesRunTime.boxToBoolean( $anonfun$hasNullValuePredicates$3( offset, ctx ) );
                        };
                        return var2;
                    }
                }
            }

            var2 = ( ctx ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$hasNullValuePredicates$4( ctx ) );
            };
            return var2;
        }, scala.collection.Seq..MODULE$.canBuildFrom());
    }

    public static int $lessinit$greater$default$7( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset, final Tuple2 identifierToCollect,
            final Set nullableIdentifiers, final SlotConfiguration slots )
    {
        return RollUpApplySlottedPipe$.MODULE$.$lessinit$greater$default$7( var0, var1, var2, var3, var4, var5 );
    }

    public static int apply$default$7( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset, final Tuple2 identifierToCollect,
            final Set nullableIdentifiers, final SlotConfiguration slots )
    {
        return RollUpApplySlottedPipe$.MODULE$.apply$default$7( var0, var1, var2, var3, var4, var5 );
    }

    public static Option<Tuple6<Pipe,Pipe,Object,Tuple2<String,Expression>,Set<String>,SlotConfiguration>> unapply( final RollUpApplySlottedPipe x$0 )
    {
        return RollUpApplySlottedPipe$.MODULE$.unapply( var0 );
    }

    public static RollUpApplySlottedPipe apply( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset,
            final Tuple2<String,Expression> identifierToCollect, final Set<String> nullableIdentifiers, final SlotConfiguration slots, final int id )
    {
        return RollUpApplySlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6 );
    }

    public Pipe lhs()
    {
        return this.lhs;
    }

    public Pipe rhs()
    {
        return this.rhs;
    }

    public int collectionRefSlotOffset()
    {
        return this.collectionRefSlotOffset;
    }

    public Tuple2<String,Expression> identifierToCollect()
    {
        return this.identifierToCollect;
    }

    public Set<String> nullableIdentifiers()
    {
        return this.nullableIdentifiers;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int id()
    {
        return this.id;
    }

    private Function1<QueryState,Function1<ExecutionContext,AnyValue>> getValueToCollectFunction()
    {
        return this.getValueToCollectFunction;
    }

    private Seq<Function1<ExecutionContext,Object>> hasNullValuePredicates()
    {
        return this.hasNullValuePredicates;
    }

    private boolean hasNullValue( final ExecutionContext ctx )
    {
        return this.hasNullValuePredicates().exists( ( p ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$hasNullValue$1( ctx, p ) );
        } );
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.map( ( ctx ) -> {
            if ( this.hasNullValue( ctx ) )
            {
                ctx.setRefAt( this.collectionRefSlotOffset(), Values.NO_VALUE );
            }
            else
            {
                QueryState innerState = state.withInitialContext( ctx );
                Iterator innerResults = this.rhs().createResults( innerState );
                ListValue collection = VirtualValues.list(
                        (AnyValue[]) innerResults.map( (Function1) this.getValueToCollectFunction().apply( state ) ).toArray(.MODULE$.apply( AnyValue.class ) ))
                ;
                ctx.setRefAt( this.collectionRefSlotOffset(), collection );
            }

            return ctx;
        } );
    }

    public RollUpApplySlottedPipe copy( final Pipe lhs, final Pipe rhs, final int collectionRefSlotOffset, final Tuple2<String,Expression> identifierToCollect,
            final Set<String> nullableIdentifiers, final SlotConfiguration slots, final int id )
    {
        return new RollUpApplySlottedPipe( lhs, rhs, collectionRefSlotOffset, identifierToCollect, nullableIdentifiers, slots, id );
    }

    public Pipe copy$default$1()
    {
        return this.lhs();
    }

    public Pipe copy$default$2()
    {
        return this.rhs();
    }

    public int copy$default$3()
    {
        return this.collectionRefSlotOffset();
    }

    public Tuple2<String,Expression> copy$default$4()
    {
        return this.identifierToCollect();
    }

    public Set<String> copy$default$5()
    {
        return this.nullableIdentifiers();
    }

    public SlotConfiguration copy$default$6()
    {
        return this.slots();
    }

    public String productPrefix()
    {
        return "RollUpApplySlottedPipe";
    }

    public int productArity()
    {
        return 6;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.collectionRefSlotOffset() );
            break;
        case 3:
            var10000 = this.identifierToCollect();
            break;
        case 4:
            var10000 = this.nullableIdentifiers();
            break;
        case 5:
            var10000 = this.slots();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RollUpApplySlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.lhs() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.rhs() ) );
        var1 = Statics.mix( var1, this.collectionRefSlotOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.identifierToCollect() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.nullableIdentifiers() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        return Statics.finalizeHash( var1, 6 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var13;
        if ( this != x$1 )
        {
            label93:
            {
                boolean var2;
                if ( x$1 instanceof RollUpApplySlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label74:
                    {
                        label83:
                        {
                            RollUpApplySlottedPipe var4 = (RollUpApplySlottedPipe) x$1;
                            Pipe var10000 = this.lhs();
                            Pipe var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label83;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label83;
                            }

                            var10000 = this.rhs();
                            Pipe var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label83;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label83;
                            }

                            if ( this.collectionRefSlotOffset() == var4.collectionRefSlotOffset() )
                            {
                                label84:
                                {
                                    Tuple2 var10 = this.identifierToCollect();
                                    Tuple2 var7 = var4.identifierToCollect();
                                    if ( var10 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label84;
                                        }
                                    }
                                    else if ( !var10.equals( var7 ) )
                                    {
                                        break label84;
                                    }

                                    Set var11 = this.nullableIdentifiers();
                                    Set var8 = var4.nullableIdentifiers();
                                    if ( var11 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label84;
                                        }
                                    }
                                    else if ( !var11.equals( var8 ) )
                                    {
                                        break label84;
                                    }

                                    SlotConfiguration var12 = this.slots();
                                    SlotConfiguration var9 = var4.slots();
                                    if ( var12 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label84;
                                        }
                                    }
                                    else if ( !var12.equals( var9 ) )
                                    {
                                        break label84;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var13 = true;
                                        break label74;
                                    }
                                }
                            }
                        }

                        var13 = false;
                    }

                    if ( var13 )
                    {
                        break label93;
                    }
                }

                var13 = false;
                return var13;
            }
        }

        var13 = true;
        return var13;
    }
}
