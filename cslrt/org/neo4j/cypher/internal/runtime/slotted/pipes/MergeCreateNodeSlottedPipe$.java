package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;

public final class MergeCreateNodeSlottedPipe$ implements Serializable
{
    public static MergeCreateNodeSlottedPipe$ MODULE$;

    static
    {
        new MergeCreateNodeSlottedPipe$();
    }

    private MergeCreateNodeSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$3( final Pipe source, final CreateNodeSlottedCommand command )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "MergeCreateNodeSlottedPipe";
    }

    public MergeCreateNodeSlottedPipe apply( final Pipe source, final CreateNodeSlottedCommand command, final int id )
    {
        return new MergeCreateNodeSlottedPipe( source, command, id );
    }

    public int apply$default$3( final Pipe source, final CreateNodeSlottedCommand command )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple2<Pipe,CreateNodeSlottedCommand>> unapply( final MergeCreateNodeSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.source(), x$0.command() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
