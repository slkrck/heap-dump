package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.RelationshipIterator;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.runtime.slotted.helpers.NullChecker$;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.exceptions.InternalException;
import org.neo4j.storageengine.api.RelationshipVisitor;
import scala.Option;
import scala.collection.Iterator;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.LongRef;

@JavaDocToJava
public abstract class OptionalExpandAllSlottedPipe extends PipeWithSource
{
    private final int relOffset;
    private final int toOffset;
    private final SemanticDirection dir;
    private final RelationshipTypes types;
    private final SlotConfiguration slots;
    private final ToLongFunction<ExecutionContext> getFromNodeFunction;

    public OptionalExpandAllSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots )
    {
        super( source );
        this.relOffset = relOffset;
        this.toOffset = toOffset;
        this.dir = dir;
        this.types = types;
        this.slots = slots;
        this.getFromNodeFunction = SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( fromSlot,
                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() );
    }

    public static int apply$default$9( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots, final Option maybePredicate )
    {
        return OptionalExpandAllSlottedPipe$.MODULE$.apply$default$9( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static OptionalExpandAllSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset,
            final SemanticDirection dir, final RelationshipTypes types, final SlotConfiguration slots, final Option<Expression> maybePredicate, final int id )
    {
        return OptionalExpandAllSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    private ToLongFunction<ExecutionContext> getFromNodeFunction()
    {
        return this.getFromNodeFunction;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.flatMap( ( inputRow ) -> {
            long fromNode = this.getFromNodeFunction().applyAsLong( inputRow );
            Iterator var10000;
            if ( NullChecker$.MODULE$.entityIsNull( fromNode ) )
            {
                var10000 = .
                MODULE$.Iterator().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new SlottedExecutionContext[]{this.withNulls( inputRow )}) ));
            }
            else
            {
                RelationshipIterator relationships = state.query().getRelationshipsForIdsPrimitive( fromNode, this.dir, this.types.types( state.query() ) );
                LongRef otherSide = LongRef.create( 0L );
                RelationshipVisitor relVisitor = new RelationshipVisitor<InternalException>( (OptionalExpandAllSlottedPipe) null, fromNode, otherSide )
                {
                    private final long fromNode$1;
                    private final LongRef otherSide$1;

                    public
                    {
                        this.fromNode$1 = fromNode$1;
                        this.otherSide$1 = otherSide$1;
                    }

                    public void visit( final long relationshipId, final int typeId, final long startNodeId, final long endNodeId )
                    {
                        if ( this.fromNode$1 == startNodeId )
                        {
                            this.otherSide$1.elem = endNodeId;
                        }
                        else
                        {
                            this.otherSide$1.elem = startNodeId;
                        }
                    }
                };
                Iterator matchIterator = this.filter( org.neo4j.cypher.internal.runtime.PrimitiveLongHelper..MODULE$.map( relationships, ( relId ) -> {
                    return $anonfun$internalCreateResults$2( this, relationships, otherSide, relVisitor, inputRow, BoxesRunTime.unboxToLong( relId ) );
            }),state);
                var10000 = matchIterator.isEmpty() ? .
                MODULE$.Iterator().apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new SlottedExecutionContext[]{this.withNulls( inputRow )}) )) :
                matchIterator;
            }

            return var10000;
        } );
    }

    public abstract Iterator<SlottedExecutionContext> filter( final Iterator<SlottedExecutionContext> iterator, final QueryState state );

    private SlottedExecutionContext withNulls( final ExecutionContext inputRow )
    {
        SlottedExecutionContext outputRow = new SlottedExecutionContext( this.slots );
        inputRow.copyTo( outputRow, inputRow.copyTo$default$2(), inputRow.copyTo$default$3(), inputRow.copyTo$default$4(), inputRow.copyTo$default$5() );
        outputRow.setLongAt( this.relOffset, -1L );
        outputRow.setLongAt( this.toOffset, -1L );
        return outputRow;
    }
}
