package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.v4_0.expressions.LabelToken;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple7;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.mutable.ArrayOps.ofInt;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class NodeIndexScanSlottedPipe implements IndexSlottedPipeWithValues, Product, Serializable
{
    private final String ident;
    private final LabelToken label;
    private final Seq<SlottedIndexedProperty> properties;
    private final int queryIndexId;
    private final IndexOrder indexOrder;
    private final SlotConfiguration slots;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private final int offset;
    private final int[] indexPropertySlotOffsets;
    private final int[] indexPropertyIndices;
    private final boolean needsValues;
    private ExecutionContextFactory executionContextFactory;

    public NodeIndexScanSlottedPipe( final String ident, final LabelToken label, final Seq<SlottedIndexedProperty> properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        this.ident = ident;
        this.label = label;
        this.properties = properties;
        this.queryIndexId = queryIndexId;
        this.indexOrder = indexOrder;
        this.slots = slots;
        this.argumentSize = argumentSize;
        this.id = id;
        Pipe.$init$( this );
        IndexSlottedPipeWithValues.$init$( this );
        Product.$init$( this );
        this.offset = slots.getLongOffsetFor( ident );
        this.indexPropertySlotOffsets = (int[]) ((TraversableOnce) properties.flatMap( ( x$1 ) -> {
            return scala.Option..MODULE$.option2Iterable( x$1.maybeCachedNodePropertySlot() );
        }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int());
        this.indexPropertyIndices =
                (int[]) ((TraversableOnce) ((TraversableLike) ((TraversableLike) properties.zipWithIndex( scala.collection.Seq..MODULE$.canBuildFrom())).filter(
                        ( x$2 ) -> {
                            return BoxesRunTime.boxToBoolean( $anonfun$indexPropertyIndices$1( x$2 ) );
                        } )).map( ( x$3 ) -> {
                    return BoxesRunTime.boxToInteger( $anonfun$indexPropertyIndices$2( x$3 ) );
                }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.Int());
        this.needsValues = (new ofInt( scala.Predef..MODULE$.intArrayOps( this.indexPropertyIndices() ))).nonEmpty();
    }

    public static int $lessinit$greater$default$8( final String ident, final LabelToken label, final Seq properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return NodeIndexScanSlottedPipe$.MODULE$.$lessinit$greater$default$8( var0, var1, var2, var3, var4, var5, var6 );
    }

    public static int apply$default$8( final String ident, final LabelToken label, final Seq properties, final int queryIndexId, final IndexOrder indexOrder,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return NodeIndexScanSlottedPipe$.MODULE$.apply$default$8( var0, var1, var2, var3, var4, var5, var6 );
    }

    public static Option<Tuple7<String,LabelToken,Seq<SlottedIndexedProperty>,Object,IndexOrder,SlotConfiguration,SlotConfiguration.Size>> unapply(
            final NodeIndexScanSlottedPipe x$0 )
    {
        return NodeIndexScanSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static NodeIndexScanSlottedPipe apply( final String ident, final LabelToken label, final Seq<SlottedIndexedProperty> properties,
            final int queryIndexId, final IndexOrder indexOrder, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return NodeIndexScanSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public Iterator<ExecutionContext> createResults( final QueryState state )
    {
        return Pipe.createResults$( this, state );
    }

    public ExecutionContextFactory executionContextFactory()
    {
        return this.executionContextFactory;
    }

    public void executionContextFactory_$eq( final ExecutionContextFactory x$1 )
    {
        this.executionContextFactory = x$1;
    }

    public String ident()
    {
        return this.ident;
    }

    public LabelToken label()
    {
        return this.label;
    }

    public Seq<SlottedIndexedProperty> properties()
    {
        return this.properties;
    }

    public int queryIndexId()
    {
        return this.queryIndexId;
    }

    public IndexOrder indexOrder()
    {
        return this.indexOrder;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    public int offset()
    {
        return this.offset;
    }

    public int[] indexPropertySlotOffsets()
    {
        return this.indexPropertySlotOffsets;
    }

    public int[] indexPropertyIndices()
    {
        return this.indexPropertyIndices;
    }

    private boolean needsValues()
    {
        return this.needsValues;
    }

    public Iterator<ExecutionContext> internalCreateResults( final QueryState state )
    {
        NodeValueIndexCursor cursor = state.query().indexScan( state.queryIndexes()[this.queryIndexId()], this.needsValues(), this.indexOrder() );
        return new IndexSlottedPipeWithValues.SlottedIndexIterator( state, this.slots(), cursor );
    }

    public NodeIndexScanSlottedPipe copy( final String ident, final LabelToken label, final Seq<SlottedIndexedProperty> properties, final int queryIndexId,
            final IndexOrder indexOrder, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new NodeIndexScanSlottedPipe( ident, label, properties, queryIndexId, indexOrder, slots, argumentSize, id );
    }

    public String copy$default$1()
    {
        return this.ident();
    }

    public LabelToken copy$default$2()
    {
        return this.label();
    }

    public Seq<SlottedIndexedProperty> copy$default$3()
    {
        return this.properties();
    }

    public int copy$default$4()
    {
        return this.queryIndexId();
    }

    public IndexOrder copy$default$5()
    {
        return this.indexOrder();
    }

    public SlotConfiguration copy$default$6()
    {
        return this.slots();
    }

    public SlotConfiguration.Size copy$default$7()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "NodeIndexScanSlottedPipe";
    }

    public int productArity()
    {
        return 7;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.ident();
            break;
        case 1:
            var10000 = this.label();
            break;
        case 2:
            var10000 = this.properties();
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.queryIndexId() );
            break;
        case 4:
            var10000 = this.indexOrder();
            break;
        case 5:
            var10000 = this.slots();
            break;
        case 6:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NodeIndexScanSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.ident() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.label() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.properties() ) );
        var1 = Statics.mix( var1, this.queryIndexId() );
        var1 = Statics.mix( var1, Statics.anyHash( this.indexOrder() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.argumentSize() ) );
        return Statics.finalizeHash( var1, 7 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var16;
        if ( this != x$1 )
        {
            label102:
            {
                boolean var2;
                if ( x$1 instanceof NodeIndexScanSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label83:
                    {
                        label92:
                        {
                            NodeIndexScanSlottedPipe var4 = (NodeIndexScanSlottedPipe) x$1;
                            String var10000 = this.ident();
                            String var5 = var4.ident();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label92;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label92;
                            }

                            LabelToken var11 = this.label();
                            LabelToken var6 = var4.label();
                            if ( var11 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label92;
                                }
                            }
                            else if ( !var11.equals( var6 ) )
                            {
                                break label92;
                            }

                            Seq var12 = this.properties();
                            Seq var7 = var4.properties();
                            if ( var12 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label92;
                                }
                            }
                            else if ( !var12.equals( var7 ) )
                            {
                                break label92;
                            }

                            if ( this.queryIndexId() == var4.queryIndexId() )
                            {
                                label93:
                                {
                                    IndexOrder var13 = this.indexOrder();
                                    IndexOrder var8 = var4.indexOrder();
                                    if ( var13 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var13.equals( var8 ) )
                                    {
                                        break label93;
                                    }

                                    SlotConfiguration var14 = this.slots();
                                    SlotConfiguration var9 = var4.slots();
                                    if ( var14 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var14.equals( var9 ) )
                                    {
                                        break label93;
                                    }

                                    SlotConfiguration.Size var15 = this.argumentSize();
                                    SlotConfiguration.Size var10 = var4.argumentSize();
                                    if ( var15 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label93;
                                        }
                                    }
                                    else if ( !var15.equals( var10 ) )
                                    {
                                        break label93;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var16 = true;
                                        break label83;
                                    }
                                }
                            }
                        }

                        var16 = false;
                    }

                    if ( var16 )
                    {
                        break label102;
                    }
                }

                var16 = false;
                return var16;
            }
        }

        var16 = true;
        return var16;
    }
}
