package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple7;
import scala.runtime.BoxesRunTime;

public final class ExpandIntoSlottedPipe$ implements Serializable
{
    public static ExpandIntoSlottedPipe$ MODULE$;

    static
    {
        new ExpandIntoSlottedPipe$();
    }

    private ExpandIntoSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$8( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "ExpandIntoSlottedPipe";
    }

    public ExpandIntoSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots, final int id )
    {
        return new ExpandIntoSlottedPipe( source, fromSlot, relOffset, toSlot, dir, lazyTypes, slots, id );
    }

    public int apply$default$8( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final RelationshipTypes lazyTypes, final SlotConfiguration slots )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple7<Pipe,Slot,Object,Slot,SemanticDirection,RelationshipTypes,SlotConfiguration>> unapply( final ExpandIntoSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple7( x$0.source(), x$0.fromSlot(), BoxesRunTime.boxToInteger( x$0.relOffset() ), x$0.toSlot(), x$0.dir(), x$0.lazyTypes(), x$0.slots() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
