package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CreateSlottedPipe extends EntityCreateSlottedPipe implements Product, Serializable
{
    private final Pipe source;
    private final IndexedSeq<CreateNodeSlottedCommand> nodes;
    private final IndexedSeq<CreateRelationshipSlottedCommand> relationships;
    private final int id;

    public CreateSlottedPipe( final Pipe source, final IndexedSeq<CreateNodeSlottedCommand> nodes,
            final IndexedSeq<CreateRelationshipSlottedCommand> relationships, final int id )
    {
        super( source );
        this.source = source;
        this.nodes = nodes;
        this.relationships = relationships;
        this.id = id;
        Product.$init$( this );
        nodes.foreach( ( x$4 ) -> {
            $anonfun$new$1( this, x$4 );
            return BoxedUnit.UNIT;
        } );
        relationships.foreach( ( x$6 ) -> {
            $anonfun$new$3( this, x$6 );
            return BoxedUnit.UNIT;
        } );
    }

    public static int $lessinit$greater$default$4( final Pipe source, final IndexedSeq nodes, final IndexedSeq relationships )
    {
        return CreateSlottedPipe$.MODULE$.$lessinit$greater$default$4( var0, var1, var2 );
    }

    public static int apply$default$4( final Pipe source, final IndexedSeq nodes, final IndexedSeq relationships )
    {
        return CreateSlottedPipe$.MODULE$.apply$default$4( var0, var1, var2 );
    }

    public static Option<Tuple3<Pipe,IndexedSeq<CreateNodeSlottedCommand>,IndexedSeq<CreateRelationshipSlottedCommand>>> unapply( final CreateSlottedPipe x$0 )
    {
        return CreateSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static CreateSlottedPipe apply( final Pipe source, final IndexedSeq<CreateNodeSlottedCommand> nodes,
            final IndexedSeq<CreateRelationshipSlottedCommand> relationships, final int id )
    {
        return CreateSlottedPipe$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public IndexedSeq<CreateNodeSlottedCommand> nodes()
    {
        return this.nodes;
    }

    public IndexedSeq<CreateRelationshipSlottedCommand> relationships()
    {
        return this.relationships;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.map( ( row ) -> {
            int i;
            for ( i = 0; i < this.nodes().length(); ++i )
            {
                CreateNodeSlottedCommand commandx = (CreateNodeSlottedCommand) this.nodes().apply( i );
                long nodeId = this.createNode( row, state, commandx );
                row.setLongAt( commandx.idOffset(), nodeId );
            }

            for ( i = 0; i < this.relationships().length(); ++i )
            {
                CreateRelationshipSlottedCommand command = (CreateRelationshipSlottedCommand) this.relationships().apply( i );
                long relationshipId = this.createRelationship( row, state, command );
                row.setLongAt( command.relIdOffset(), relationshipId );
            }

            return row;
        } );
    }

    public void handleNoValue( final String key )
    {
    }

    public CreateSlottedPipe copy( final Pipe source, final IndexedSeq<CreateNodeSlottedCommand> nodes,
            final IndexedSeq<CreateRelationshipSlottedCommand> relationships, final int id )
    {
        return new CreateSlottedPipe( source, nodes, relationships, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public IndexedSeq<CreateNodeSlottedCommand> copy$default$2()
    {
        return this.nodes();
    }

    public IndexedSeq<CreateRelationshipSlottedCommand> copy$default$3()
    {
        return this.relationships();
    }

    public String productPrefix()
    {
        return "CreateSlottedPipe";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.nodes();
            break;
        case 2:
            var10000 = this.relationships();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CreateSlottedPipe;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var9;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof CreateSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            CreateSlottedPipe var4 = (CreateSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            IndexedSeq var8 = this.nodes();
                            IndexedSeq var6 = var4.nodes();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            var8 = this.relationships();
                            IndexedSeq var7 = var4.relationships();
                            if ( var8 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var9 = true;
                                break label54;
                            }
                        }

                        var9 = false;
                    }

                    if ( var9 )
                    {
                        break label72;
                    }
                }

                var9 = false;
                return var9;
            }
        }

        var9 = true;
        return var9;
    }
}
