package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.logical.plans.QueryExpression;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekMode;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeek.;
import org.neo4j.cypher.internal.v4_0.expressions.LabelToken;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple9;
import scala.collection.IndexedSeq;
import scala.runtime.BoxesRunTime;

public final class NodeIndexSeekSlottedPipe$ implements Serializable
{
    public static NodeIndexSeekSlottedPipe$ MODULE$;

    static
    {
        new NodeIndexSeekSlottedPipe$();
    }

    private NodeIndexSeekSlottedPipe$()
    {
        MODULE$ = this;
    }

    public IndexSeekMode $lessinit$greater$default$6()
    {
        return .MODULE$;
    }

    public int $lessinit$greater$default$10( final String ident, final LabelToken label, final IndexedSeq<SlottedIndexedProperty> properties,
            final int queryIndexId, final QueryExpression<Expression> valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "NodeIndexSeekSlottedPipe";
    }

    public NodeIndexSeekSlottedPipe apply( final String ident, final LabelToken label, final IndexedSeq<SlottedIndexedProperty> properties,
            final int queryIndexId, final QueryExpression<Expression> valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new NodeIndexSeekSlottedPipe( ident, label, properties, queryIndexId, valueExpr, indexMode, indexOrder, slots, argumentSize, id );
    }

    public int apply$default$10( final String ident, final LabelToken label, final IndexedSeq<SlottedIndexedProperty> properties, final int queryIndexId,
            final QueryExpression<Expression> valueExpr, final IndexSeekMode indexMode, final IndexOrder indexOrder, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID();
    }

    public IndexSeekMode apply$default$6()
    {
        return .MODULE$;
    }

    public Option<Tuple9<String,LabelToken,IndexedSeq<SlottedIndexedProperty>,Object,QueryExpression<Expression>,IndexSeekMode,IndexOrder,SlotConfiguration,SlotConfiguration.Size>> unapply(
            final NodeIndexSeekSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple9( x$0.ident(), x$0.label(), x$0.properties(), BoxesRunTime.boxToInteger( x$0.queryIndexId() ), x$0.valueExpr(), x$0.indexMode(),
                    x$0.indexOrder(), x$0.slots(), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
