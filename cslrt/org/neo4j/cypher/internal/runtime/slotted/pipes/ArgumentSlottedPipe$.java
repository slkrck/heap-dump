package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;

public final class ArgumentSlottedPipe$ implements Serializable
{
    public static ArgumentSlottedPipe$ MODULE$;

    static
    {
        new ArgumentSlottedPipe$();
    }

    private ArgumentSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$3( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "ArgumentSlottedPipe";
    }

    public ArgumentSlottedPipe apply( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new ArgumentSlottedPipe( slots, argumentSize, id );
    }

    public int apply$default$3( final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple2<SlotConfiguration,SlotConfiguration.Size>> unapply( final ArgumentSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.slots(), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
