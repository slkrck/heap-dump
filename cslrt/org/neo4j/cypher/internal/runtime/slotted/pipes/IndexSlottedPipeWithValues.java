package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexIteratorBase;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.internal.kernel.api.NodeValueIndexCursor;
import org.neo4j.values.storable.Value;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface IndexSlottedPipeWithValues extends Pipe
{
    static void $init$( final IndexSlottedPipeWithValues $this )
    {
    }

    int offset();

    int[] indexPropertyIndices();

    int[] indexPropertySlotOffsets();

    SlotConfiguration.Size argumentSize();

    public class SlottedIndexIterator extends IndexIteratorBase<ExecutionContext>
    {
        private final QueryState state;
        private final SlotConfiguration slots;

        public SlottedIndexIterator( final IndexSlottedPipeWithValues $outer, final QueryState state, final SlotConfiguration slots,
                final NodeValueIndexCursor cursor )
        {
            this.state = state;
            this.slots = slots;
            if ( $outer == null )
            {
                throw null;
            }
            else
            {
                this.$outer = $outer;
                super( cursor );
            }
        }

        public ExecutionContext fetchNext()
        {
            SlottedExecutionContext var10000;
            if ( super.cursor().next() )
            {
                SlottedExecutionContext slottedContext = new SlottedExecutionContext( this.slots );
                this.state.copyArgumentStateTo( slottedContext,
                        this.org$neo4j$cypher$internal$runtime$slotted$pipes$IndexSlottedPipeWithValues$SlottedIndexIterator$$$outer().argumentSize().nLongs(),
                        this.org$neo4j$cypher$internal$runtime$slotted$pipes$IndexSlottedPipeWithValues$SlottedIndexIterator$$$outer().argumentSize().nReferences() );
                slottedContext.setLongAt(
                        this.org$neo4j$cypher$internal$runtime$slotted$pipes$IndexSlottedPipeWithValues$SlottedIndexIterator$$$outer().offset(),
                        super.cursor().nodeReference() );

                for ( int i = 0; i <
                        this.org$neo4j$cypher$internal$runtime$slotted$pipes$IndexSlottedPipeWithValues$SlottedIndexIterator$$$outer().indexPropertyIndices().length;
                        ++i )
                {
                    Value value = super.cursor().propertyValue(
                            this.org$neo4j$cypher$internal$runtime$slotted$pipes$IndexSlottedPipeWithValues$SlottedIndexIterator$$$outer().indexPropertyIndices()[i] );
                    slottedContext.setCachedPropertyAt(
                            this.org$neo4j$cypher$internal$runtime$slotted$pipes$IndexSlottedPipeWithValues$SlottedIndexIterator$$$outer().indexPropertySlotOffsets()[i],
                            value );
                }

                var10000 = slottedContext;
            }
            else
            {
                var10000 = null;
            }

            return var10000;
        }
    }
}
