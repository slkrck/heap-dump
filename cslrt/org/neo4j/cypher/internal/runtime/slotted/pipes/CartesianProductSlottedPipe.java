package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple6;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CartesianProductSlottedPipe implements Pipe, Product, Serializable
{
    private final Pipe lhs;
    private final Pipe rhs;
    private final int lhsLongCount;
    private final int lhsRefCount;
    private final SlotConfiguration slots;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private ExecutionContextFactory executionContextFactory;

    public CartesianProductSlottedPipe( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        this.lhs = lhs;
        this.rhs = rhs;
        this.lhsLongCount = lhsLongCount;
        this.lhsRefCount = lhsRefCount;
        this.slots = slots;
        this.argumentSize = argumentSize;
        this.id = id;
        Pipe.$init$( this );
        Product.$init$( this );
    }

    public static int $lessinit$greater$default$7( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return CartesianProductSlottedPipe$.MODULE$.$lessinit$greater$default$7( var0, var1, var2, var3, var4, var5 );
    }

    public static int apply$default$7( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return CartesianProductSlottedPipe$.MODULE$.apply$default$7( var0, var1, var2, var3, var4, var5 );
    }

    public static Option<Tuple6<Pipe,Pipe,Object,Object,SlotConfiguration,SlotConfiguration.Size>> unapply( final CartesianProductSlottedPipe x$0 )
    {
        return CartesianProductSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static CartesianProductSlottedPipe apply( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount,
            final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return CartesianProductSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6 );
    }

    public Iterator<ExecutionContext> createResults( final QueryState state )
    {
        return Pipe.createResults$( this, state );
    }

    public ExecutionContextFactory executionContextFactory()
    {
        return this.executionContextFactory;
    }

    public void executionContextFactory_$eq( final ExecutionContextFactory x$1 )
    {
        this.executionContextFactory = x$1;
    }

    public Pipe lhs()
    {
        return this.lhs;
    }

    public Pipe rhs()
    {
        return this.rhs;
    }

    public int lhsLongCount()
    {
        return this.lhsLongCount;
    }

    public int lhsRefCount()
    {
        return this.lhsRefCount;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final QueryState state )
    {
        return this.lhs().createResults( state ).flatMap( ( lhsCtx ) -> {
            return this.rhs().createResults( state ).map( ( rhsCtx ) -> {
                SlottedExecutionContext context = new SlottedExecutionContext( this.slots() );
                lhsCtx.copyTo( context, lhsCtx.copyTo$default$2(), lhsCtx.copyTo$default$3(), lhsCtx.copyTo$default$4(), lhsCtx.copyTo$default$5() );
                rhsCtx.copyTo( context, this.argumentSize().nLongs(), this.argumentSize().nReferences(), this.lhsLongCount(), this.lhsRefCount() );
                return context;
            } );
        } );
    }

    public CartesianProductSlottedPipe copy( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        return new CartesianProductSlottedPipe( lhs, rhs, lhsLongCount, lhsRefCount, slots, argumentSize, id );
    }

    public Pipe copy$default$1()
    {
        return this.lhs();
    }

    public Pipe copy$default$2()
    {
        return this.rhs();
    }

    public int copy$default$3()
    {
        return this.lhsLongCount();
    }

    public int copy$default$4()
    {
        return this.lhsRefCount();
    }

    public SlotConfiguration copy$default$5()
    {
        return this.slots();
    }

    public SlotConfiguration.Size copy$default$6()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "CartesianProductSlottedPipe";
    }

    public int productArity()
    {
        return 6;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.lhsLongCount() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.lhsRefCount() );
            break;
        case 4:
            var10000 = this.slots();
            break;
        case 5:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CartesianProductSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.lhs() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.rhs() ) );
        var1 = Statics.mix( var1, this.lhsLongCount() );
        var1 = Statics.mix( var1, this.lhsRefCount() );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.argumentSize() ) );
        return Statics.finalizeHash( var1, 6 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var11;
        if ( this != x$1 )
        {
            label86:
            {
                boolean var2;
                if ( x$1 instanceof CartesianProductSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label67:
                    {
                        label76:
                        {
                            CartesianProductSlottedPipe var4 = (CartesianProductSlottedPipe) x$1;
                            Pipe var10000 = this.lhs();
                            Pipe var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label76;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label76;
                            }

                            var10000 = this.rhs();
                            Pipe var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label76;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label76;
                            }

                            if ( this.lhsLongCount() == var4.lhsLongCount() && this.lhsRefCount() == var4.lhsRefCount() )
                            {
                                label77:
                                {
                                    SlotConfiguration var9 = this.slots();
                                    SlotConfiguration var7 = var4.slots();
                                    if ( var9 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label77;
                                        }
                                    }
                                    else if ( !var9.equals( var7 ) )
                                    {
                                        break label77;
                                    }

                                    SlotConfiguration.Size var10 = this.argumentSize();
                                    SlotConfiguration.Size var8 = var4.argumentSize();
                                    if ( var10 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label77;
                                        }
                                    }
                                    else if ( !var10.equals( var8 ) )
                                    {
                                        break label77;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var11 = true;
                                        break label67;
                                    }
                                }
                            }
                        }

                        var11 = false;
                    }

                    if ( var11 )
                    {
                        break label86;
                    }
                }

                var11 = false;
                return var11;
            }
        }

        var11 = true;
        return var11;
    }
}
