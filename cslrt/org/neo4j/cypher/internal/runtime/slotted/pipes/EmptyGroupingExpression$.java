package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class EmptyGroupingExpression$ implements GroupingExpression, Product, Serializable
{
    public static EmptyGroupingExpression$ MODULE$;

    static
    {
        new EmptyGroupingExpression$();
    }

    private EmptyGroupingExpression$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public void registerOwningPipe( final Pipe pipe )
    {
    }

    public AnyValue computeGroupingKey( final ExecutionContext context, final QueryState state )
    {
        return Values.NO_VALUE;
    }

    public AnyValue computeOrderedGroupingKey( final AnyValue groupingKey )
    {
        return Values.NO_VALUE;
    }

    public AnyValue getGroupingKey( final ExecutionContext context )
    {
        return Values.NO_VALUE;
    }

    public boolean isEmpty()
    {
        return true;
    }

    public void project( final ExecutionContext context, final AnyValue groupingKey )
    {
    }

    public String productPrefix()
    {
        return "EmptyGroupingExpression";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof EmptyGroupingExpression$;
    }

    public int hashCode()
    {
        return 715465736;
    }

    public String toString()
    {
        return "EmptyGroupingExpression";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
