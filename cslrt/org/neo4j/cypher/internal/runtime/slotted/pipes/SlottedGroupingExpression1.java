package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlottedGroupingExpression1 implements GroupingExpression, Product, Serializable
{
    private final SlotExpression groupingExpression;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> setter;
    private final Function1<ExecutionContext,AnyValue> getter;
    private final Function1<AnyValue,AnyValue> ordered;

    public SlottedGroupingExpression1( final SlotExpression groupingExpression )
    {
        this.groupingExpression = groupingExpression;
        Product.$init$( this );
        this.setter = SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( groupingExpression.slot() );
        this.getter = SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( groupingExpression.slot() );
        this.ordered = groupingExpression.ordered() ? ( x ) -> {
            return (AnyValue) scala.Predef..MODULE$.identity( x );
        } : ( x$1 ) -> {
            return Values.NO_VALUE;
        };
    }

    public static Option<SlotExpression> unapply( final SlottedGroupingExpression1 x$0 )
    {
        return SlottedGroupingExpression1$.MODULE$.unapply( var0 );
    }

    public static SlottedGroupingExpression1 apply( final SlotExpression groupingExpression )
    {
        return SlottedGroupingExpression1$.MODULE$.apply( var0 );
    }

    public static <A> Function1<SlotExpression,A> andThen( final Function1<SlottedGroupingExpression1,A> g )
    {
        return SlottedGroupingExpression1$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,SlottedGroupingExpression1> compose( final Function1<A,SlotExpression> g )
    {
        return SlottedGroupingExpression1$.MODULE$.compose( var0 );
    }

    public SlotExpression groupingExpression()
    {
        return this.groupingExpression;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit> setter()
    {
        return this.setter;
    }

    private Function1<ExecutionContext,AnyValue> getter()
    {
        return this.getter;
    }

    private Function1<AnyValue,AnyValue> ordered()
    {
        return this.ordered;
    }

    public void registerOwningPipe( final Pipe pipe )
    {
        this.groupingExpression().expression().registerOwningPipe( pipe );
    }

    public AnyValue computeGroupingKey( final ExecutionContext context, final QueryState state )
    {
        return this.groupingExpression().expression().apply( context, state );
    }

    public AnyValue computeOrderedGroupingKey( final AnyValue groupingKey )
    {
        return (AnyValue) this.ordered().apply( groupingKey );
    }

    public AnyValue getGroupingKey( final ExecutionContext context )
    {
        return (AnyValue) this.getter().apply( context );
    }

    public boolean isEmpty()
    {
        return false;
    }

    public void project( final ExecutionContext context, final AnyValue groupingKey )
    {
        this.setter().apply( context, groupingKey );
    }

    public SlottedGroupingExpression1 copy( final SlotExpression groupingExpression )
    {
        return new SlottedGroupingExpression1( groupingExpression );
    }

    public SlotExpression copy$default$1()
    {
        return this.groupingExpression();
    }

    public String productPrefix()
    {
        return "SlottedGroupingExpression1";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.groupingExpression();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedGroupingExpression1;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof SlottedGroupingExpression1 )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        SlottedGroupingExpression1 var4 = (SlottedGroupingExpression1) x$1;
                        SlotExpression var10000 = this.groupingExpression();
                        SlotExpression var5 = var4.groupingExpression();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
