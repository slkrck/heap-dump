package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CreateNodeSlottedCommand implements Product, Serializable
{
    private final int idOffset;
    private final Seq<LazyLabel> labels;
    private final Option<Expression> properties;

    public CreateNodeSlottedCommand( final int idOffset, final Seq<LazyLabel> labels, final Option<Expression> properties )
    {
        this.idOffset = idOffset;
        this.labels = labels;
        this.properties = properties;
        Product.$init$( this );
    }

    public static Option<Tuple3<Object,Seq<LazyLabel>,Option<Expression>>> unapply( final CreateNodeSlottedCommand x$0 )
    {
        return CreateNodeSlottedCommand$.MODULE$.unapply( var0 );
    }

    public static CreateNodeSlottedCommand apply( final int idOffset, final Seq<LazyLabel> labels, final Option<Expression> properties )
    {
        return CreateNodeSlottedCommand$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Object,Seq<LazyLabel>,Option<Expression>>,CreateNodeSlottedCommand> tupled()
    {
        return CreateNodeSlottedCommand$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Seq<LazyLabel>,Function1<Option<Expression>,CreateNodeSlottedCommand>>> curried()
    {
        return CreateNodeSlottedCommand$.MODULE$.curried();
    }

    public int idOffset()
    {
        return this.idOffset;
    }

    public Seq<LazyLabel> labels()
    {
        return this.labels;
    }

    public Option<Expression> properties()
    {
        return this.properties;
    }

    public CreateNodeSlottedCommand copy( final int idOffset, final Seq<LazyLabel> labels, final Option<Expression> properties )
    {
        return new CreateNodeSlottedCommand( idOffset, labels, properties );
    }

    public int copy$default$1()
    {
        return this.idOffset();
    }

    public Seq<LazyLabel> copy$default$2()
    {
        return this.labels();
    }

    public Option<Expression> copy$default$3()
    {
        return this.properties();
    }

    public String productPrefix()
    {
        return "CreateNodeSlottedCommand";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.idOffset() );
            break;
        case 1:
            var10000 = this.labels();
            break;
        case 2:
            var10000 = this.properties();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CreateNodeSlottedCommand;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.idOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.labels() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.properties() ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof CreateNodeSlottedCommand )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        CreateNodeSlottedCommand var4 = (CreateNodeSlottedCommand) x$1;
                        if ( this.idOffset() == var4.idOffset() )
                        {
                            label56:
                            {
                                Seq var10000 = this.labels();
                                Seq var5 = var4.labels();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label56;
                                }

                                Option var7 = this.properties();
                                Option var6 = var4.properties();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label56;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label47;
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label65;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
