package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.PrimitiveLongHelper.;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class AllNodesScanSlottedPipe implements Pipe, Product, Serializable
{
    private final String ident;
    private final SlotConfiguration slots;
    private final SlotConfiguration.Size argumentSize;
    private final int id;
    private final int offset;
    private ExecutionContextFactory executionContextFactory;

    public AllNodesScanSlottedPipe( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        this.ident = ident;
        this.slots = slots;
        this.argumentSize = argumentSize;
        this.id = id;
        Pipe.$init$( this );
        Product.$init$( this );
        this.offset = slots.getLongOffsetFor( ident );
    }

    public static int $lessinit$greater$default$4( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return AllNodesScanSlottedPipe$.MODULE$.$lessinit$greater$default$4( var0, var1, var2 );
    }

    public static int apply$default$4( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return AllNodesScanSlottedPipe$.MODULE$.apply$default$4( var0, var1, var2 );
    }

    public static Option<Tuple3<String,SlotConfiguration,SlotConfiguration.Size>> unapply( final AllNodesScanSlottedPipe x$0 )
    {
        return AllNodesScanSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static AllNodesScanSlottedPipe apply( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return AllNodesScanSlottedPipe$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public Iterator<ExecutionContext> createResults( final QueryState state )
    {
        return Pipe.createResults$( this, state );
    }

    public ExecutionContextFactory executionContextFactory()
    {
        return this.executionContextFactory;
    }

    public void executionContextFactory_$eq( final ExecutionContextFactory x$1 )
    {
        this.executionContextFactory = x$1;
    }

    public String ident()
    {
        return this.ident;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    private int offset()
    {
        return this.offset;
    }

    public Iterator<ExecutionContext> internalCreateResults( final QueryState state )
    {
        return .MODULE$.map( state.query().nodeOps().allPrimitive(), ( nodeId ) -> {
        return $anonfun$internalCreateResults$1( this, state, BoxesRunTime.unboxToLong( nodeId ) );
    } );
    }

    public AllNodesScanSlottedPipe copy( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new AllNodesScanSlottedPipe( ident, slots, argumentSize, id );
    }

    public String copy$default$1()
    {
        return this.ident();
    }

    public SlotConfiguration copy$default$2()
    {
        return this.slots();
    }

    public SlotConfiguration.Size copy$default$3()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "AllNodesScanSlottedPipe";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.ident();
            break;
        case 1:
            var10000 = this.slots();
            break;
        case 2:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AllNodesScanSlottedPipe;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof AllNodesScanSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            AllNodesScanSlottedPipe var4 = (AllNodesScanSlottedPipe) x$1;
                            String var10000 = this.ident();
                            String var5 = var4.ident();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            SlotConfiguration var8 = this.slots();
                            SlotConfiguration var6 = var4.slots();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label63;
                            }

                            SlotConfiguration.Size var9 = this.argumentSize();
                            SlotConfiguration.Size var7 = var4.argumentSize();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label54;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label72;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
