package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;

public final class AllNodesScanSlottedPipe$ implements Serializable
{
    public static AllNodesScanSlottedPipe$ MODULE$;

    static
    {
        new AllNodesScanSlottedPipe$();
    }

    private AllNodesScanSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$4( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "AllNodesScanSlottedPipe";
    }

    public AllNodesScanSlottedPipe apply( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new AllNodesScanSlottedPipe( ident, slots, argumentSize, id );
    }

    public int apply$default$4( final String ident, final SlotConfiguration slots, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple3<String,SlotConfiguration,SlotConfiguration.Size>> unapply( final AllNodesScanSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.ident(), x$0.slots(), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
