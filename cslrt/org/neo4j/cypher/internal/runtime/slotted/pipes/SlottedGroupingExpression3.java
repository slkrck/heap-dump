package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlottedGroupingExpression3 implements GroupingExpression, Product, Serializable
{
    private final SlotExpression groupingExpression1;
    private final SlotExpression groupingExpression2;
    private final SlotExpression groupingExpression3;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> setter1;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> setter2;
    private final Function2<ExecutionContext,AnyValue,BoxedUnit> setter3;
    private final Function1<ExecutionContext,AnyValue> getter1;
    private final Function1<ExecutionContext,AnyValue> getter2;
    private final Function1<ExecutionContext,AnyValue> getter3;
    private final Function1<ListValue,AnyValue> ordered;

    public SlottedGroupingExpression3( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2,
            final SlotExpression groupingExpression3 )
    {
        this.groupingExpression1 = groupingExpression1;
        this.groupingExpression2 = groupingExpression2;
        this.groupingExpression3 = groupingExpression3;
        Product.$init$( this );
        this.setter1 = SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( groupingExpression1.slot() );
        this.setter2 = SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( groupingExpression2.slot() );
        this.setter3 = SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( groupingExpression3.slot() );
        this.getter1 = SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( groupingExpression1.slot() );
        this.getter2 = SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( groupingExpression2.slot() );
        this.getter3 = SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( groupingExpression3.slot() );
        this.ordered = groupingExpression1.ordered() ? (groupingExpression2.ordered() ? (groupingExpression3.ordered() ? ( x ) -> {
            return (ListValue) scala.Predef..MODULE$.identity( x );
        } : ( l ) -> {
            return l.take( 2 );
        }) : (groupingExpression3.ordered() ? ( l ) -> {
            return VirtualValues.list( new AnyValue[]{l.head(), l.last()} );
        } : ( l ) -> {
            return l.head();
        })) : (groupingExpression2.ordered() ? (groupingExpression3.ordered() ? ( l ) -> {
            return l.drop( 1 );
        } : ( l ) -> {
            return l.value( 1 );
        }) : (groupingExpression3.ordered() ? ( l ) -> {
            return l.last();
        } : ( x$3 ) -> {
            return Values.NO_VALUE;
        }));
    }

    public static Option<Tuple3<SlotExpression,SlotExpression,SlotExpression>> unapply( final SlottedGroupingExpression3 x$0 )
    {
        return SlottedGroupingExpression3$.MODULE$.unapply( var0 );
    }

    public static SlottedGroupingExpression3 apply( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2,
            final SlotExpression groupingExpression3 )
    {
        return SlottedGroupingExpression3$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<SlotExpression,SlotExpression,SlotExpression>,SlottedGroupingExpression3> tupled()
    {
        return SlottedGroupingExpression3$.MODULE$.tupled();
    }

    public static Function1<SlotExpression,Function1<SlotExpression,Function1<SlotExpression,SlottedGroupingExpression3>>> curried()
    {
        return SlottedGroupingExpression3$.MODULE$.curried();
    }

    public SlotExpression groupingExpression1()
    {
        return this.groupingExpression1;
    }

    public SlotExpression groupingExpression2()
    {
        return this.groupingExpression2;
    }

    public SlotExpression groupingExpression3()
    {
        return this.groupingExpression3;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit> setter1()
    {
        return this.setter1;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit> setter2()
    {
        return this.setter2;
    }

    private Function2<ExecutionContext,AnyValue,BoxedUnit> setter3()
    {
        return this.setter3;
    }

    private Function1<ExecutionContext,AnyValue> getter1()
    {
        return this.getter1;
    }

    private Function1<ExecutionContext,AnyValue> getter2()
    {
        return this.getter2;
    }

    private Function1<ExecutionContext,AnyValue> getter3()
    {
        return this.getter3;
    }

    private Function1<ListValue,AnyValue> ordered()
    {
        return this.ordered;
    }

    public void registerOwningPipe( final Pipe pipe )
    {
        this.groupingExpression1().expression().registerOwningPipe( pipe );
        this.groupingExpression2().expression().registerOwningPipe( pipe );
        this.groupingExpression3().expression().registerOwningPipe( pipe );
    }

    public ListValue computeGroupingKey( final ExecutionContext context, final QueryState state )
    {
        return VirtualValues.list(
                new AnyValue[]{this.groupingExpression1().expression().apply( context, state ), this.groupingExpression2().expression().apply( context, state ),
                        this.groupingExpression3().expression().apply( context, state )} );
    }

    public ListValue getGroupingKey( final ExecutionContext context )
    {
        return VirtualValues.list( new AnyValue[]{(AnyValue) this.getter1().apply( context ), (AnyValue) this.getter2().apply( context ),
                (AnyValue) this.getter3().apply( context )} );
    }

    public AnyValue computeOrderedGroupingKey( final ListValue groupingKey )
    {
        return (AnyValue) this.ordered().apply( groupingKey );
    }

    public boolean isEmpty()
    {
        return false;
    }

    public void project( final ExecutionContext context, final ListValue groupingKey )
    {
        this.setter1().apply( context, groupingKey.value( 0 ) );
        this.setter2().apply( context, groupingKey.value( 1 ) );
        this.setter3().apply( context, groupingKey.value( 2 ) );
    }

    public SlottedGroupingExpression3 copy( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2,
            final SlotExpression groupingExpression3 )
    {
        return new SlottedGroupingExpression3( groupingExpression1, groupingExpression2, groupingExpression3 );
    }

    public SlotExpression copy$default$1()
    {
        return this.groupingExpression1();
    }

    public SlotExpression copy$default$2()
    {
        return this.groupingExpression2();
    }

    public SlotExpression copy$default$3()
    {
        return this.groupingExpression3();
    }

    public String productPrefix()
    {
        return "SlottedGroupingExpression3";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        SlotExpression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.groupingExpression1();
            break;
        case 1:
            var10000 = this.groupingExpression2();
            break;
        case 2:
            var10000 = this.groupingExpression3();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedGroupingExpression3;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label72:
            {
                boolean var2;
                if ( x$1 instanceof SlottedGroupingExpression3 )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label54:
                    {
                        label63:
                        {
                            SlottedGroupingExpression3 var4 = (SlottedGroupingExpression3) x$1;
                            SlotExpression var10000 = this.groupingExpression1();
                            SlotExpression var5 = var4.groupingExpression1();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label63;
                            }

                            var10000 = this.groupingExpression2();
                            SlotExpression var6 = var4.groupingExpression2();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label63;
                            }

                            var10000 = this.groupingExpression3();
                            SlotExpression var7 = var4.groupingExpression3();
                            if ( var10000 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label63;
                                }
                            }
                            else if ( !var10000.equals( var7 ) )
                            {
                                break label63;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label54;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label72;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
