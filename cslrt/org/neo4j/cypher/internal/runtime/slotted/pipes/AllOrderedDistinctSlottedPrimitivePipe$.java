package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;

public final class AllOrderedDistinctSlottedPrimitivePipe$ implements Serializable
{
    public static AllOrderedDistinctSlottedPrimitivePipe$ MODULE$;

    static
    {
        new AllOrderedDistinctSlottedPrimitivePipe$();
    }

    private AllOrderedDistinctSlottedPrimitivePipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$5( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "AllOrderedDistinctSlottedPrimitivePipe";
    }

    public AllOrderedDistinctSlottedPrimitivePipe apply( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots,
            final GroupingExpression groupingExpression, final int id )
    {
        return new AllOrderedDistinctSlottedPrimitivePipe( source, slots, primitiveSlots, groupingExpression, id );
    }

    public int apply$default$5( final Pipe source, final SlotConfiguration slots, final int[] primitiveSlots, final GroupingExpression groupingExpression )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple4<Pipe,SlotConfiguration,int[],GroupingExpression>> unapply( final AllOrderedDistinctSlottedPrimitivePipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple4( x$0.source(), x$0.slots(), x$0.primitiveSlots(), x$0.groupingExpression() ) ))
        ;
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
