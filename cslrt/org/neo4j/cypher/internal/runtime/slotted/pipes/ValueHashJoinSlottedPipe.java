package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple8;
import scala.None.;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class ValueHashJoinSlottedPipe extends AbstractHashJoinPipe<AnyValue,Expression> implements Product, Serializable
{
    private final Expression leftSide;
    private final Expression rightSide;
    private final Pipe left;
    private final Pipe right;
    private final SlotConfiguration slots;
    private final int longOffset;
    private final int refsOffset;
    private final SlotConfiguration.Size argumentSize;
    private final int id;

    public ValueHashJoinSlottedPipe( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize, final int id )
    {
        super( left, right, slots );
        this.leftSide = leftSide;
        this.rightSide = rightSide;
        this.left = left;
        this.right = right;
        this.slots = slots;
        this.longOffset = longOffset;
        this.refsOffset = refsOffset;
        this.argumentSize = argumentSize;
        this.id = id;
        Product.$init$( this );
        leftSide.registerOwningPipe( this );
        rightSide.registerOwningPipe( this );
    }

    public static int $lessinit$greater$default$9( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right,
            final SlotConfiguration slots, final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize )
    {
        return ValueHashJoinSlottedPipe$.MODULE$.$lessinit$greater$default$9( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static int apply$default$9( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right, final SlotConfiguration slots,
            final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize )
    {
        return ValueHashJoinSlottedPipe$.MODULE$.apply$default$9( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static Option<Tuple8<Expression,Expression,Pipe,Pipe,SlotConfiguration,Object,Object,SlotConfiguration.Size>> unapply(
            final ValueHashJoinSlottedPipe x$0 )
    {
        return ValueHashJoinSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ValueHashJoinSlottedPipe apply( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right,
            final SlotConfiguration slots, final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize, final int id )
    {
        return ValueHashJoinSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public Expression leftSide()
    {
        return this.leftSide;
    }

    public Expression rightSide()
    {
        return this.rightSide;
    }

    public Pipe left()
    {
        return this.left;
    }

    public Pipe right()
    {
        return this.right;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public int longOffset()
    {
        return this.longOffset;
    }

    public int refsOffset()
    {
        return this.refsOffset;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public int id()
    {
        return this.id;
    }

    public Option<AnyValue> computeKey( final ExecutionContext context, final Expression keyColumns, final QueryState queryState )
    {
        AnyValue value = keyColumns.apply( context, queryState );
        return (Option) (value == Values.NO_VALUE ?.MODULE$ :new Some( value ));
    }

    public void copyDataFromRhs( final SlottedExecutionContext newRow, final ExecutionContext rhs )
    {
        rhs.copyTo( newRow, this.argumentSize().nLongs(), this.argumentSize().nReferences(), this.longOffset(), this.refsOffset() );
    }

    public ValueHashJoinSlottedPipe copy( final Expression leftSide, final Expression rightSide, final Pipe left, final Pipe right,
            final SlotConfiguration slots, final int longOffset, final int refsOffset, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new ValueHashJoinSlottedPipe( leftSide, rightSide, left, right, slots, longOffset, refsOffset, argumentSize, id );
    }

    public Expression copy$default$1()
    {
        return this.leftSide();
    }

    public Expression copy$default$2()
    {
        return this.rightSide();
    }

    public Pipe copy$default$3()
    {
        return this.left();
    }

    public Pipe copy$default$4()
    {
        return this.right();
    }

    public SlotConfiguration copy$default$5()
    {
        return this.slots();
    }

    public int copy$default$6()
    {
        return this.longOffset();
    }

    public int copy$default$7()
    {
        return this.refsOffset();
    }

    public SlotConfiguration.Size copy$default$8()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "ValueHashJoinSlottedPipe";
    }

    public int productArity()
    {
        return 8;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.leftSide();
            break;
        case 1:
            var10000 = this.rightSide();
            break;
        case 2:
            var10000 = this.left();
            break;
        case 3:
            var10000 = this.right();
            break;
        case 4:
            var10000 = this.slots();
            break;
        case 5:
            var10000 = BoxesRunTime.boxToInteger( this.longOffset() );
            break;
        case 6:
            var10000 = BoxesRunTime.boxToInteger( this.refsOffset() );
            break;
        case 7:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ValueHashJoinSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.leftSide() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.rightSide() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.left() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.right() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, this.longOffset() );
        var1 = Statics.mix( var1, this.refsOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.argumentSize() ) );
        return Statics.finalizeHash( var1, 8 );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var14;
        if ( this != x$1 )
        {
            label103:
            {
                boolean var2;
                if ( x$1 instanceof ValueHashJoinSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label85:
                    {
                        label94:
                        {
                            ValueHashJoinSlottedPipe var4 = (ValueHashJoinSlottedPipe) x$1;
                            Expression var10000 = this.leftSide();
                            Expression var5 = var4.leftSide();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label94;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label94;
                            }

                            var10000 = this.rightSide();
                            Expression var6 = var4.rightSide();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label94;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label94;
                            }

                            Pipe var11 = this.left();
                            Pipe var7 = var4.left();
                            if ( var11 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label94;
                                }
                            }
                            else if ( !var11.equals( var7 ) )
                            {
                                break label94;
                            }

                            var11 = this.right();
                            Pipe var8 = var4.right();
                            if ( var11 == null )
                            {
                                if ( var8 != null )
                                {
                                    break label94;
                                }
                            }
                            else if ( !var11.equals( var8 ) )
                            {
                                break label94;
                            }

                            SlotConfiguration var12 = this.slots();
                            SlotConfiguration var9 = var4.slots();
                            if ( var12 == null )
                            {
                                if ( var9 != null )
                                {
                                    break label94;
                                }
                            }
                            else if ( !var12.equals( var9 ) )
                            {
                                break label94;
                            }

                            if ( this.longOffset() == var4.longOffset() && this.refsOffset() == var4.refsOffset() )
                            {
                                label57:
                                {
                                    SlotConfiguration.Size var13 = this.argumentSize();
                                    SlotConfiguration.Size var10 = var4.argumentSize();
                                    if ( var13 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label57;
                                        }
                                    }
                                    else if ( !var13.equals( var10 ) )
                                    {
                                        break label57;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var14 = true;
                                        break label85;
                                    }
                                }
                            }
                        }

                        var14 = false;
                    }

                    if ( var14 )
                    {
                        break label103;
                    }
                }

                var14 = false;
                return var14;
            }
        }

        var14 = true;
        return var14;
    }
}
