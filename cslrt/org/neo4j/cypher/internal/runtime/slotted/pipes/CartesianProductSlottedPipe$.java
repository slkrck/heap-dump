package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple6;
import scala.runtime.BoxesRunTime;

public final class CartesianProductSlottedPipe$ implements Serializable
{
    public static CartesianProductSlottedPipe$ MODULE$;

    static
    {
        new CartesianProductSlottedPipe$();
    }

    private CartesianProductSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$7( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "CartesianProductSlottedPipe";
    }

    public CartesianProductSlottedPipe apply( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize, final int id )
    {
        return new CartesianProductSlottedPipe( lhs, rhs, lhsLongCount, lhsRefCount, slots, argumentSize, id );
    }

    public int apply$default$7( final Pipe lhs, final Pipe rhs, final int lhsLongCount, final int lhsRefCount, final SlotConfiguration slots,
            final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple6<Pipe,Pipe,Object,Object,SlotConfiguration,SlotConfiguration.Size>> unapply( final CartesianProductSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple6( x$0.lhs(), x$0.rhs(), BoxesRunTime.boxToInteger( x$0.lhsLongCount() ), BoxesRunTime.boxToInteger( x$0.lhsRefCount() ), x$0.slots(),
                    x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
