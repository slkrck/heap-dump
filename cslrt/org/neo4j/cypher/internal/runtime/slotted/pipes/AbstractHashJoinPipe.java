package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import scala.Option;
import scala.collection.Iterator;
import scala.collection.mutable.HashMap;
import scala.collection.mutable.MutableList;
import scala.collection.mutable.Seq;
import scala.package.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public abstract class AbstractHashJoinPipe<Key, T> extends PipeWithSource
{
    private final Pipe right;
    private final SlotConfiguration slots;

    public AbstractHashJoinPipe( final Pipe left, final Pipe right, final SlotConfiguration slots )
    {
        super( left );
        this.right = right;
        this.slots = slots;
    }

    public abstract T leftSide();

    public abstract T rightSide();

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        if ( !input.hasNext() )
        {
            return .MODULE$.Iterator().empty();
        }
        else
        {
            Iterator rhsIterator = this.right.createResults( state );
            if ( rhsIterator.isEmpty() )
            {
                return .MODULE$.Iterator().empty();
            }
            else
            {
                HashMap table = this.buildProbeTable( input, state );
                if ( table.isEmpty() )
                {
                    return .MODULE$.Iterator().empty();
                }
                else
                {
                    Iterator result = rhsIterator.withFilter( ( check$ifrefutable$1 ) -> {
                        return BoxesRunTime.boxToBoolean( $anonfun$internalCreateResults$1( check$ifrefutable$1 ) );
                    } ).flatMap( ( rhs ) -> {
                        return scala.Option..MODULE$.option2Iterable( this.computeKey( rhs, this.rightSide(), state ).map( ( joinKey ) -> {
                            Seq matchesFromLhs = (Seq) table.getOrElse( joinKey, () -> {
                                return (MutableList) scala.collection.mutable.MutableList..MODULE$.empty();
                            } ); return (Seq) matchesFromLhs.map( ( lhs ) -> {
                                SlottedExecutionContext newRow = new SlottedExecutionContext( this.slots );
                                lhs.copyTo( newRow, lhs.copyTo$default$2(), lhs.copyTo$default$3(), lhs.copyTo$default$4(), lhs.copyTo$default$5() );
                                this.copyDataFromRhs( newRow, rhs );
                                return newRow;
                            }, scala.collection.mutable.Seq..MODULE$.canBuildFrom());
                        } ) );
                    } ); return scala.collection.TraversableOnce..MODULE$.flattenTraversableOnce( result, scala.Predef..MODULE$.$conforms()).flatten();
                }
            }
        }
    }

    private HashMap<Key,MutableList<ExecutionContext>> buildProbeTable( final Iterator<ExecutionContext> input, final QueryState queryState )
    {
        HashMap table = new HashMap();
        input.foreach( ( context ) -> {
            $anonfun$buildProbeTable$1( this, queryState, table, context );
            return BoxedUnit.UNIT;
        } );
        return table;
    }

    public abstract Option<Key> computeKey( final ExecutionContext context, final T keyColumns, final QueryState queryState );

    public abstract void copyDataFromRhs( final SlottedExecutionContext newRow, final ExecutionContext rhs );
}
