package org.neo4j.cypher.internal.runtime.slotted.pipes;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;

public final class SlottedGroupingExpression3$ extends AbstractFunction3<SlotExpression,SlotExpression,SlotExpression,SlottedGroupingExpression3>
        implements Serializable
{
    public static SlottedGroupingExpression3$ MODULE$;

    static
    {
        new SlottedGroupingExpression3$();
    }

    private SlottedGroupingExpression3$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedGroupingExpression3";
    }

    public SlottedGroupingExpression3 apply( final SlotExpression groupingExpression1, final SlotExpression groupingExpression2,
            final SlotExpression groupingExpression3 )
    {
        return new SlottedGroupingExpression3( groupingExpression1, groupingExpression2, groupingExpression3 );
    }

    public Option<Tuple3<SlotExpression,SlotExpression,SlotExpression>> unapply( final SlottedGroupingExpression3 x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( x$0.groupingExpression1(), x$0.groupingExpression2(), x$0.groupingExpression3() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
