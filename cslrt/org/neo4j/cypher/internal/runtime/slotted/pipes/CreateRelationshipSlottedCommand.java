package org.neo4j.cypher.internal.runtime.slotted.pipes;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyType;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple8;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CreateRelationshipSlottedCommand implements Product, Serializable
{
    private final int relIdOffset;
    private final ToLongFunction<ExecutionContext> startNodeIdGetter;
    private final LazyType relType;
    private final ToLongFunction<ExecutionContext> endNodeIdGetter;
    private final Option<Expression> properties;
    private final String relName;
    private final String startName;
    private final String endName;

    public CreateRelationshipSlottedCommand( final int relIdOffset, final ToLongFunction<ExecutionContext> startNodeIdGetter, final LazyType relType,
            final ToLongFunction<ExecutionContext> endNodeIdGetter, final Option<Expression> properties, final String relName, final String startName,
            final String endName )
    {
        this.relIdOffset = relIdOffset;
        this.startNodeIdGetter = startNodeIdGetter;
        this.relType = relType;
        this.endNodeIdGetter = endNodeIdGetter;
        this.properties = properties;
        this.relName = relName;
        this.startName = startName;
        this.endName = endName;
        Product.$init$( this );
    }

    public static Option<Tuple8<Object,ToLongFunction<ExecutionContext>,LazyType,ToLongFunction<ExecutionContext>,Option<Expression>,String,String,String>> unapply(
            final CreateRelationshipSlottedCommand x$0 )
    {
        return CreateRelationshipSlottedCommand$.MODULE$.unapply( var0 );
    }

    public static CreateRelationshipSlottedCommand apply( final int relIdOffset, final ToLongFunction<ExecutionContext> startNodeIdGetter,
            final LazyType relType, final ToLongFunction<ExecutionContext> endNodeIdGetter, final Option<Expression> properties, final String relName,
            final String startName, final String endName )
    {
        return CreateRelationshipSlottedCommand$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static Function1<Tuple8<Object,ToLongFunction<ExecutionContext>,LazyType,ToLongFunction<ExecutionContext>,Option<Expression>,String,String,String>,CreateRelationshipSlottedCommand> tupled()
    {
        return CreateRelationshipSlottedCommand$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<ToLongFunction<ExecutionContext>,Function1<LazyType,Function1<ToLongFunction<ExecutionContext>,Function1<Option<Expression>,Function1<String,Function1<String,Function1<String,CreateRelationshipSlottedCommand>>>>>>>> curried()
    {
        return CreateRelationshipSlottedCommand$.MODULE$.curried();
    }

    public int relIdOffset()
    {
        return this.relIdOffset;
    }

    public ToLongFunction<ExecutionContext> startNodeIdGetter()
    {
        return this.startNodeIdGetter;
    }

    public LazyType relType()
    {
        return this.relType;
    }

    public ToLongFunction<ExecutionContext> endNodeIdGetter()
    {
        return this.endNodeIdGetter;
    }

    public Option<Expression> properties()
    {
        return this.properties;
    }

    public String relName()
    {
        return this.relName;
    }

    public String startName()
    {
        return this.startName;
    }

    public String endName()
    {
        return this.endName;
    }

    public CreateRelationshipSlottedCommand copy( final int relIdOffset, final ToLongFunction<ExecutionContext> startNodeIdGetter, final LazyType relType,
            final ToLongFunction<ExecutionContext> endNodeIdGetter, final Option<Expression> properties, final String relName, final String startName,
            final String endName )
    {
        return new CreateRelationshipSlottedCommand( relIdOffset, startNodeIdGetter, relType, endNodeIdGetter, properties, relName, startName, endName );
    }

    public int copy$default$1()
    {
        return this.relIdOffset();
    }

    public ToLongFunction<ExecutionContext> copy$default$2()
    {
        return this.startNodeIdGetter();
    }

    public LazyType copy$default$3()
    {
        return this.relType();
    }

    public ToLongFunction<ExecutionContext> copy$default$4()
    {
        return this.endNodeIdGetter();
    }

    public Option<Expression> copy$default$5()
    {
        return this.properties();
    }

    public String copy$default$6()
    {
        return this.relName();
    }

    public String copy$default$7()
    {
        return this.startName();
    }

    public String copy$default$8()
    {
        return this.endName();
    }

    public String productPrefix()
    {
        return "CreateRelationshipSlottedCommand";
    }

    public int productArity()
    {
        return 8;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.relIdOffset() );
            break;
        case 1:
            var10000 = this.startNodeIdGetter();
            break;
        case 2:
            var10000 = this.relType();
            break;
        case 3:
            var10000 = this.endNodeIdGetter();
            break;
        case 4:
            var10000 = this.properties();
            break;
        case 5:
            var10000 = this.relName();
            break;
        case 6:
            var10000 = this.startName();
            break;
        case 7:
            var10000 = this.endName();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CreateRelationshipSlottedCommand;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.relIdOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.startNodeIdGetter() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.relType() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.endNodeIdGetter() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.properties() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.relName() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.startName() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.endName() ) );
        return Statics.finalizeHash( var1, 8 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var15;
        if ( this != x$1 )
        {
            label110:
            {
                boolean var2;
                if ( x$1 instanceof CreateRelationshipSlottedCommand )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label92:
                    {
                        CreateRelationshipSlottedCommand var4 = (CreateRelationshipSlottedCommand) x$1;
                        if ( this.relIdOffset() == var4.relIdOffset() )
                        {
                            label101:
                            {
                                ToLongFunction var10000 = this.startNodeIdGetter();
                                ToLongFunction var5 = var4.startNodeIdGetter();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label101;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label101;
                                }

                                LazyType var12 = this.relType();
                                LazyType var6 = var4.relType();
                                if ( var12 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label101;
                                    }
                                }
                                else if ( !var12.equals( var6 ) )
                                {
                                    break label101;
                                }

                                var10000 = this.endNodeIdGetter();
                                ToLongFunction var7 = var4.endNodeIdGetter();
                                if ( var10000 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label101;
                                    }
                                }
                                else if ( !var10000.equals( var7 ) )
                                {
                                    break label101;
                                }

                                Option var13 = this.properties();
                                Option var8 = var4.properties();
                                if ( var13 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label101;
                                    }
                                }
                                else if ( !var13.equals( var8 ) )
                                {
                                    break label101;
                                }

                                String var14 = this.relName();
                                String var9 = var4.relName();
                                if ( var14 == null )
                                {
                                    if ( var9 != null )
                                    {
                                        break label101;
                                    }
                                }
                                else if ( !var14.equals( var9 ) )
                                {
                                    break label101;
                                }

                                var14 = this.startName();
                                String var10 = var4.startName();
                                if ( var14 == null )
                                {
                                    if ( var10 != null )
                                    {
                                        break label101;
                                    }
                                }
                                else if ( !var14.equals( var10 ) )
                                {
                                    break label101;
                                }

                                var14 = this.endName();
                                String var11 = var4.endName();
                                if ( var14 == null )
                                {
                                    if ( var11 != null )
                                    {
                                        break label101;
                                    }
                                }
                                else if ( !var14.equals( var11 ) )
                                {
                                    break label101;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var15 = true;
                                    break label92;
                                }
                            }
                        }

                        var15 = false;
                    }

                    if ( var15 )
                    {
                        break label110;
                    }
                }

                var15 = false;
                return var15;
            }
        }

        var15 = true;
        return var15;
    }
}
