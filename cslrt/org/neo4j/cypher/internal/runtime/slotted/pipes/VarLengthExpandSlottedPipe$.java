package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple16;
import scala.runtime.BoxesRunTime;

public final class VarLengthExpandSlottedPipe$ implements Serializable
{
    public static VarLengthExpandSlottedPipe$ MODULE$;

    static
    {
        new VarLengthExpandSlottedPipe$();
    }

    private VarLengthExpandSlottedPipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$17( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option<Object> maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "VarLengthExpandSlottedPipe";
    }

    public VarLengthExpandSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option<Object> maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize, final int id )
    {
        return new VarLengthExpandSlottedPipe( source, fromSlot, relOffset, toSlot, dir, projectedDir, types, min, maxDepth, shouldExpandAll, slots,
                tempNodeOffset, tempRelationshipOffset, nodePredicate, relationshipPredicate, argumentSize, id );
    }

    public int apply$default$17( final Pipe source, final Slot fromSlot, final int relOffset, final Slot toSlot, final SemanticDirection dir,
            final SemanticDirection projectedDir, final RelationshipTypes types, final int min, final Option<Object> maxDepth, final boolean shouldExpandAll,
            final SlotConfiguration slots, final int tempNodeOffset, final int tempRelationshipOffset, final Expression nodePredicate,
            final Expression relationshipPredicate, final SlotConfiguration.Size argumentSize )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple16<Pipe,Slot,Object,Slot,SemanticDirection,SemanticDirection,RelationshipTypes,Object,Option<Object>,Object,SlotConfiguration,Object,Object,Expression,Expression,SlotConfiguration.Size>> unapply(
            final VarLengthExpandSlottedPipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
            new Tuple16( x$0.source(), x$0.fromSlot(), BoxesRunTime.boxToInteger( x$0.relOffset() ), x$0.toSlot(), x$0.dir(), x$0.projectedDir(), x$0.types(),
                    BoxesRunTime.boxToInteger( x$0.min() ), x$0.maxDepth(), BoxesRunTime.boxToBoolean( x$0.shouldExpandAll() ), x$0.slots(),
                    BoxesRunTime.boxToInteger( x$0.tempNodeOffset() ), BoxesRunTime.boxToInteger( x$0.tempRelationshipOffset() ), x$0.nodePredicate(),
                    x$0.relationshipPredicate(), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
