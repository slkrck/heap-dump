package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.eclipse.collections.api.iterator.LongIterator;
import org.neo4j.cypher.internal.runtime.RelationshipIterator;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.exceptions.InternalException;
import org.neo4j.storageengine.api.RelationshipVisitor;
import scala.Function0;
import scala.MatchError;
import scala.Tuple3;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.ArrayBuilder.;
import scala.reflect.ScalaSignature;
import scala.runtime.BooleanRef;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface PrimitiveCachingExpandInto
{
    static void $init$( final PrimitiveCachingExpandInto $this )
    {
        $this.org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState_$eq( false );
    }

    default LongIterator findRelationships( final QueryState state, final long fromNode, final long toNode, final PrimitiveRelationshipsCache relCache,
            final SemanticDirection dir, final Function0<int[]> relTypes )
    {
        boolean fromNodeIsDense = state.query().nodeIsDense( fromNode, state.cursors().nodeCursor() );
        boolean toNodeIsDense = state.query().nodeIsDense( toNode, state.cursors().nodeCursor() );
        LongIterator var10000;
        if ( fromNodeIsDense && toNodeIsDense )
        {
            int fromDegree = this.getDegree( fromNode, (int[]) relTypes.apply(), dir, state );
            if ( fromDegree == 0 )
            {
                return RelationshipIterator.EMPTY;
            }

            int toDegree = this.getDegree( toNode, (int[]) relTypes.apply(), dir.reversed(), state );
            if ( toDegree == 0 )
            {
                return RelationshipIterator.EMPTY;
            }

            var10000 = this.relIterator( state, fromNode, toNode, fromDegree < toDegree, (int[]) relTypes.apply(), relCache, dir );
        }
        else
        {
            var10000 = toNodeIsDense ? this.relIterator( state, fromNode, toNode, true, (int[]) relTypes.apply(), relCache, dir )
                                     : (fromNodeIsDense ? this.relIterator( state, fromNode, toNode, false, (int[]) relTypes.apply(), relCache, dir )
                                                        : this.relIterator( state, fromNode, toNode, this.alternate(), (int[]) relTypes.apply(), relCache,
                                                                dir ));
        }

        return var10000;
    }

    boolean org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState();

    void org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState_$eq( final boolean x$1 );

    private default boolean alternate()
    {
        boolean result = !this.org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState();
        this.org$neo4j$cypher$internal$runtime$slotted$pipes$PrimitiveCachingExpandInto$$alternateState_$eq( result );
        return result;
    }

    private default LongIterator relIterator( final QueryState state, final long fromNode, final long toNode, final boolean preserveDirection,
            final int[] relTypes, final PrimitiveRelationshipsCache relCache, final SemanticDirection dir )
    {
        Tuple3 var12 = preserveDirection ? new Tuple3( BoxesRunTime.boxToLong( fromNode ), dir, BoxesRunTime.boxToLong( toNode ) )
                                         : new Tuple3( BoxesRunTime.boxToLong( toNode ), dir.reversed(), BoxesRunTime.boxToLong( fromNode ) );
        if ( var12 != null )
        {
            long start = BoxesRunTime.unboxToLong( var12._1() );
            SemanticDirection localDirection = (SemanticDirection) var12._2();
            long end = BoxesRunTime.unboxToLong( var12._3() );
            Tuple3 var10 = new Tuple3( BoxesRunTime.boxToLong( start ), localDirection, BoxesRunTime.boxToLong( end ) );
            long start = BoxesRunTime.unboxToLong( var10._1() );
            SemanticDirection localDirection = (SemanticDirection) var10._2();
            long end = BoxesRunTime.unboxToLong( var10._3() );
            RelationshipIterator relationships = state.query().getRelationshipsForIdsPrimitive( start, localDirection, relTypes );
            ArrayBuilder connectedRelationships = .MODULE$.make( scala.reflect.ClassTag..MODULE$.Long());
            BooleanRef connected = BooleanRef.create( false );
            RelationshipVisitor relVisitor =
                    new RelationshipVisitor<InternalException>( (PrimitiveCachingExpandInto) null, start, end, connectedRelationships, connected )
                    {
                        private final long start$1;
                        private final long end$1;
                        private final ArrayBuilder connectedRelationships$1;
                        private final BooleanRef connected$1;

                        public
                        {
                            this.start$1 = start$1;
                            this.end$1 = end$1;
                            this.connectedRelationships$1 = connectedRelationships$1;
                            this.connected$1 = connected$1;
                        }

                        public void visit( final long relationshipId, final int typeId, final long startNodeId, final long endNodeId )
                        {
                            if ( this.end$1 == startNodeId && this.start$1 == endNodeId || this.start$1 == startNodeId && this.end$1 == endNodeId )
                            {
                                this.connectedRelationships$1.$plus$eq( BoxesRunTime.boxToLong( relationshipId ) );
                                this.connected$1.elem = true;
                            }
                        }
                    };
            return new LongIterator( (PrimitiveCachingExpandInto) null, fromNode, toNode, relCache, dir, relationships, connectedRelationships, connected,
                    relVisitor )
            {
                private final long fromNode$1;
                private final long toNode$1;
                private final PrimitiveRelationshipsCache relCache$1;
                private final SemanticDirection dir$1;
                private final RelationshipIterator relationships$1;
                private final ArrayBuilder connectedRelationships$1;
                private final BooleanRef connected$1;
                private final RelationshipVisitor relVisitor$1;
                private long nextRelId;
                private boolean consumed;

                public
                {
                    this.fromNode$1 = fromNode$1;
                    this.toNode$1 = toNode$1;
                    this.relCache$1 = relCache$1;
                    this.dir$1 = dir$1;
                    this.relationships$1 = relationships$1;
                    this.connectedRelationships$1 = connectedRelationships$1;
                    this.connected$1 = connected$1;
                    this.relVisitor$1 = relVisitor$1;
                    this.nextRelId = -1L;
                    this.consumed = true;
                }

                private long nextRelId()
                {
                    return this.nextRelId;
                }

                private void nextRelId_$eq( final long x$1 )
                {
                    this.nextRelId = x$1;
                }

                private boolean consumed()
                {
                    return this.consumed;
                }

                private void consumed_$eq( final boolean x$1 )
                {
                    this.consumed = x$1;
                }

                public long next()
                {
                    this.consumed_$eq( true );
                    return this.nextRelId();
                }

                public boolean hasNext()
                {
                    return !this.consumed() || this.computeNext();
                }

                private boolean computeNext()
                {
                    this.connected$1.elem = false;

                    while ( this.relationships$1.hasNext() && !this.connected$1.elem )
                    {
                        this.nextRelId_$eq( this.relationships$1.next() );
                        this.relationships$1.relationshipVisit( this.nextRelId(), this.relVisitor$1 );
                    }

                    if ( !this.relationships$1.hasNext() )
                    {
                        this.relCache$1.put( this.fromNode$1, this.toNode$1, (long[]) this.connectedRelationships$1.result(), this.dir$1 );
                    }
                    else
                    {
                        BoxedUnit var10000 = BoxedUnit.UNIT;
                    }

                    this.consumed_$eq( !this.connected$1.elem );
                    return this.connected$1.elem;
                }
            };
        }
        else
        {
            throw new MatchError( var12 );
        }
    }

    private default int getDegree( final long node, final int[] relTypes, final SemanticDirection direction, final QueryState state )
    {
        int var10000;
        if ( relTypes == null )
        {
            var10000 = state.query().nodeGetDegree( node, direction, state.cursors().nodeCursor() );
        }
        else
        {
            int i = 0;

            int total;
            for ( total = 0; i < relTypes.length; ++i )
            {
                total += state.query().nodeGetDegree( node, direction, relTypes[i], state.cursors().nodeCursor() );
            }

            var10000 = total;
        }

        return var10000;
    }
}
