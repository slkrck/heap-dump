package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlotExpression implements Product, Serializable
{
    private final Slot slot;
    private final Expression expression;
    private final boolean ordered;

    public SlotExpression( final Slot slot, final Expression expression, final boolean ordered )
    {
        this.slot = slot;
        this.expression = expression;
        this.ordered = ordered;
        Product.$init$( this );
    }

    public static boolean $lessinit$greater$default$3()
    {
        return SlotExpression$.MODULE$.$lessinit$greater$default$3();
    }

    public static boolean apply$default$3()
    {
        return SlotExpression$.MODULE$.apply$default$3();
    }

    public static Option<Tuple3<Slot,Expression,Object>> unapply( final SlotExpression x$0 )
    {
        return SlotExpression$.MODULE$.unapply( var0 );
    }

    public static SlotExpression apply( final Slot slot, final Expression expression, final boolean ordered )
    {
        return SlotExpression$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Slot,Expression,Object>,SlotExpression> tupled()
    {
        return SlotExpression$.MODULE$.tupled();
    }

    public static Function1<Slot,Function1<Expression,Function1<Object,SlotExpression>>> curried()
    {
        return SlotExpression$.MODULE$.curried();
    }

    public Slot slot()
    {
        return this.slot;
    }

    public Expression expression()
    {
        return this.expression;
    }

    public boolean ordered()
    {
        return this.ordered;
    }

    public SlotExpression copy( final Slot slot, final Expression expression, final boolean ordered )
    {
        return new SlotExpression( slot, expression, ordered );
    }

    public Slot copy$default$1()
    {
        return this.slot();
    }

    public Expression copy$default$2()
    {
        return this.expression();
    }

    public boolean copy$default$3()
    {
        return this.ordered();
    }

    public String productPrefix()
    {
        return "SlotExpression";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.slot();
            break;
        case 1:
            var10000 = this.expression();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToBoolean( this.ordered() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlotExpression;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.slot() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.expression() ) );
        var1 = Statics.mix( var1, this.ordered() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof SlotExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        label56:
                        {
                            SlotExpression var4 = (SlotExpression) x$1;
                            Slot var10000 = this.slot();
                            Slot var5 = var4.slot();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label56;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label56;
                            }

                            Expression var7 = this.expression();
                            Expression var6 = var4.expression();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label56;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label56;
                            }

                            if ( this.ordered() == var4.ordered() && var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label47;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label65;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
