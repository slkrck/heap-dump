package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.runtime.BoxesRunTime;

public final class OrderedDistinctSlottedSinglePrimitivePipe$ implements Serializable
{
    public static OrderedDistinctSlottedSinglePrimitivePipe$ MODULE$;

    static
    {
        new OrderedDistinctSlottedSinglePrimitivePipe$();
    }

    private OrderedDistinctSlottedSinglePrimitivePipe$()
    {
        MODULE$ = this;
    }

    public int $lessinit$greater$default$6( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset, final Expression expression )
    {
        return .MODULE$.INVALID_ID();
    }

    public final String toString()
    {
        return "OrderedDistinctSlottedSinglePrimitivePipe";
    }

    public OrderedDistinctSlottedSinglePrimitivePipe apply( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset,
            final Expression expression, final int id )
    {
        return new OrderedDistinctSlottedSinglePrimitivePipe( source, slots, toSlot, offset, expression, id );
    }

    public int apply$default$6( final Pipe source, final SlotConfiguration slots, final Slot toSlot, final int offset, final Expression expression )
    {
        return .MODULE$.INVALID_ID();
    }

    public Option<Tuple5<Pipe,SlotConfiguration,Slot,Object,Expression>> unapply( final OrderedDistinctSlottedSinglePrimitivePipe x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :
        new Some( new Tuple5( x$0.source(), x$0.slots(), x$0.toSlot(), BoxesRunTime.boxToInteger( x$0.offset() ), x$0.expression() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
