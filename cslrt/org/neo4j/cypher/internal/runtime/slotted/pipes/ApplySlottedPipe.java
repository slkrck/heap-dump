package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ApplySlottedPipe extends PipeWithSource implements Product, Serializable
{
    private final Pipe lhs;
    private final Pipe rhs;
    private final int id;

    public ApplySlottedPipe( final Pipe lhs, final Pipe rhs, final int id )
    {
        super( lhs );
        this.lhs = lhs;
        this.rhs = rhs;
        this.id = id;
        Product.$init$( this );
    }

    public static int $lessinit$greater$default$3( final Pipe lhs, final Pipe rhs )
    {
        return ApplySlottedPipe$.MODULE$.$lessinit$greater$default$3( var0, var1 );
    }

    public static int apply$default$3( final Pipe lhs, final Pipe rhs )
    {
        return ApplySlottedPipe$.MODULE$.apply$default$3( var0, var1 );
    }

    public static Option<Tuple2<Pipe,Pipe>> unapply( final ApplySlottedPipe x$0 )
    {
        return ApplySlottedPipe$.MODULE$.unapply( var0 );
    }

    public static ApplySlottedPipe apply( final Pipe lhs, final Pipe rhs, final int id )
    {
        return ApplySlottedPipe$.MODULE$.apply( var0, var1, var2 );
    }

    public Pipe lhs()
    {
        return this.lhs;
    }

    public Pipe rhs()
    {
        return this.rhs;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<ExecutionContext> internalCreateResults( final Iterator<ExecutionContext> input, final QueryState state )
    {
        return input.flatMap( ( lhsContext ) -> {
            QueryState rhsState = state.withInitialContext( lhsContext );
            return this.rhs().createResults( rhsState );
        } );
    }

    public ApplySlottedPipe copy( final Pipe lhs, final Pipe rhs, final int id )
    {
        return new ApplySlottedPipe( lhs, rhs, id );
    }

    public Pipe copy$default$1()
    {
        return this.lhs();
    }

    public Pipe copy$default$2()
    {
        return this.rhs();
    }

    public String productPrefix()
    {
        return "ApplySlottedPipe";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Pipe var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.lhs();
            break;
        case 1:
            var10000 = this.rhs();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ApplySlottedPipe;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof ApplySlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            ApplySlottedPipe var4 = (ApplySlottedPipe) x$1;
                            Pipe var10000 = this.lhs();
                            Pipe var5 = var4.lhs();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.rhs();
                            Pipe var6 = var4.rhs();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
