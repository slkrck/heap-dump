package org.neo4j.cypher.internal.runtime.slotted.pipes;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple8;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class FilteringOptionalExpandAllSlottedPipe extends OptionalExpandAllSlottedPipe implements Product, Serializable
{
    private final Pipe source;
    private final Slot fromSlot;
    private final int relOffset;
    private final int toOffset;
    private final SemanticDirection dir;
    private final RelationshipTypes types;
    private final SlotConfiguration slots;
    private final Expression predicate;
    private final int id;

    public FilteringOptionalExpandAllSlottedPipe( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset, final SemanticDirection dir,
            final RelationshipTypes types, final SlotConfiguration slots, final Expression predicate, final int id )
    {
        super( source, fromSlot, relOffset, toOffset, dir, types, slots );
        this.source = source;
        this.fromSlot = fromSlot;
        this.relOffset = relOffset;
        this.toOffset = toOffset;
        this.dir = dir;
        this.types = types;
        this.slots = slots;
        this.predicate = predicate;
        this.id = id;
        Product.$init$( this );
        predicate.registerOwningPipe( this );
    }

    public static Option<Tuple8<Pipe,Slot,Object,Object,SemanticDirection,RelationshipTypes,SlotConfiguration,Expression>> unapply(
            final FilteringOptionalExpandAllSlottedPipe x$0 )
    {
        return FilteringOptionalExpandAllSlottedPipe$.MODULE$.unapply( var0 );
    }

    public static FilteringOptionalExpandAllSlottedPipe apply( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset,
            final SemanticDirection dir, final RelationshipTypes types, final SlotConfiguration slots, final Expression predicate, final int id )
    {
        return FilteringOptionalExpandAllSlottedPipe$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public Pipe source()
    {
        return this.source;
    }

    public Slot fromSlot()
    {
        return this.fromSlot;
    }

    public int relOffset()
    {
        return this.relOffset;
    }

    public int toOffset()
    {
        return this.toOffset;
    }

    public SemanticDirection dir()
    {
        return this.dir;
    }

    public RelationshipTypes types()
    {
        return this.types;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public Expression predicate()
    {
        return this.predicate;
    }

    public int id()
    {
        return this.id;
    }

    public Iterator<SlottedExecutionContext> filter( final Iterator<SlottedExecutionContext> iterator, final QueryState state )
    {
        return iterator.filter( ( ctx ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$filter$1( this, state, ctx ) );
        } );
    }

    public FilteringOptionalExpandAllSlottedPipe copy( final Pipe source, final Slot fromSlot, final int relOffset, final int toOffset,
            final SemanticDirection dir, final RelationshipTypes types, final SlotConfiguration slots, final Expression predicate, final int id )
    {
        return new FilteringOptionalExpandAllSlottedPipe( source, fromSlot, relOffset, toOffset, dir, types, slots, predicate, id );
    }

    public Pipe copy$default$1()
    {
        return this.source();
    }

    public Slot copy$default$2()
    {
        return this.fromSlot();
    }

    public int copy$default$3()
    {
        return this.relOffset();
    }

    public int copy$default$4()
    {
        return this.toOffset();
    }

    public SemanticDirection copy$default$5()
    {
        return this.dir();
    }

    public RelationshipTypes copy$default$6()
    {
        return this.types();
    }

    public SlotConfiguration copy$default$7()
    {
        return this.slots();
    }

    public Expression copy$default$8()
    {
        return this.predicate();
    }

    public String productPrefix()
    {
        return "FilteringOptionalExpandAllSlottedPipe";
    }

    public int productArity()
    {
        return 8;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.source();
            break;
        case 1:
            var10000 = this.fromSlot();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.relOffset() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.toOffset() );
            break;
        case 4:
            var10000 = this.dir();
            break;
        case 5:
            var10000 = this.types();
            break;
        case 6:
            var10000 = this.slots();
            break;
        case 7:
            var10000 = this.predicate();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof FilteringOptionalExpandAllSlottedPipe;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.source() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fromSlot() ) );
        var1 = Statics.mix( var1, this.relOffset() );
        var1 = Statics.mix( var1, this.toOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.dir() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.types() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.slots() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.predicate() ) );
        return Statics.finalizeHash( var1, 8 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var16;
        if ( this != x$1 )
        {
            label104:
            {
                boolean var2;
                if ( x$1 instanceof FilteringOptionalExpandAllSlottedPipe )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label85:
                    {
                        label94:
                        {
                            FilteringOptionalExpandAllSlottedPipe var4 = (FilteringOptionalExpandAllSlottedPipe) x$1;
                            Pipe var10000 = this.source();
                            Pipe var5 = var4.source();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label94;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label94;
                            }

                            Slot var11 = this.fromSlot();
                            Slot var6 = var4.fromSlot();
                            if ( var11 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label94;
                                }
                            }
                            else if ( !var11.equals( var6 ) )
                            {
                                break label94;
                            }

                            if ( this.relOffset() == var4.relOffset() && this.toOffset() == var4.toOffset() )
                            {
                                label95:
                                {
                                    SemanticDirection var12 = this.dir();
                                    SemanticDirection var7 = var4.dir();
                                    if ( var12 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label95;
                                        }
                                    }
                                    else if ( !var12.equals( var7 ) )
                                    {
                                        break label95;
                                    }

                                    RelationshipTypes var13 = this.types();
                                    RelationshipTypes var8 = var4.types();
                                    if ( var13 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label95;
                                        }
                                    }
                                    else if ( !var13.equals( var8 ) )
                                    {
                                        break label95;
                                    }

                                    SlotConfiguration var14 = this.slots();
                                    SlotConfiguration var9 = var4.slots();
                                    if ( var14 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label95;
                                        }
                                    }
                                    else if ( !var14.equals( var9 ) )
                                    {
                                        break label95;
                                    }

                                    Expression var15 = this.predicate();
                                    Expression var10 = var4.predicate();
                                    if ( var15 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label95;
                                        }
                                    }
                                    else if ( !var15.equals( var10 ) )
                                    {
                                        break label95;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var16 = true;
                                        break label85;
                                    }
                                }
                            }
                        }

                        var16 = false;
                    }

                    if ( var16 )
                    {
                        break label104;
                    }
                }

                var16 = false;
                return var16;
            }
        }

        var16 = true;
        return var16;
    }
}
