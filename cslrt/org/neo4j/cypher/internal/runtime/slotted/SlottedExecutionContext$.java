package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration$;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;

public final class SlottedExecutionContext$ implements Serializable
{
    public static SlottedExecutionContext$ MODULE$;

    static
    {
        new SlottedExecutionContext$();
    }

    private final boolean DEBUG;

    private SlottedExecutionContext$()
    {
        MODULE$ = this;
        this.DEBUG = false;
    }

    public SlottedExecutionContext empty()
    {
        return new SlottedExecutionContext( SlotConfiguration$.MODULE$.empty() );
    }

    public boolean DEBUG()
    {
        return this.DEBUG;
    }

    public SlottedExecutionContext apply( final SlotConfiguration slots )
    {
        return new SlottedExecutionContext( slots );
    }

    public Option<SlotConfiguration> unapply( final SlottedExecutionContext x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.slots() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
