package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.LongSlot;
import org.neo4j.cypher.internal.physicalplanning.RefSlot;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.slotted.expressions.NodeFromSlot;
import org.neo4j.cypher.internal.runtime.slotted.expressions.NullCheck;
import org.neo4j.cypher.internal.runtime.slotted.expressions.ReferenceFromSlot;
import org.neo4j.cypher.internal.runtime.slotted.expressions.RelationshipFromSlot;
import org.neo4j.cypher.internal.runtime.slotted.pipes.UnionSlottedPipe;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.exceptions.InternalException;
import scala.Function3;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.runtime.BoxedUnit;

public final class SlottedPipeMapper$
{
    public static SlottedPipeMapper$ MODULE$;

    static
    {
        new SlottedPipeMapper$();
    }

    private SlottedPipeMapper$()
    {
        MODULE$ = this;
    }

    public Seq<Tuple2<String,Expression>> createProjectionsForResult( final Seq<String> columns, final SlotConfiguration slots )
    {
        Seq runtimeColumns = (Seq) columns.map( ( identifier ) -> {
            return MODULE$.org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$createProjectionForIdentifier( slots, identifier );
        },.MODULE$.canBuildFrom());
        return runtimeColumns;
    }

    public Tuple2<String,Expression> org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$createProjectionForIdentifier( final SlotConfiguration slots,
            final String identifier )
    {
        Slot slot = (Slot) slots.get( identifier ).getOrElse( () -> {
            throw new InternalException(
                    (new StringBuilder( 41 )).append( "Did not find `" ).append( identifier ).append( "` in the slot configuration" ).toString() );
        } );
        return scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( identifier ), this.projectSlotExpression( slot ));
    }

    private Expression projectSlotExpression( final Slot slot )
    {
        Object var2;
        int offset;
        label106:
        {
            boolean var3 = false;
            LongSlot var4 = null;
            NodeType var10000;
            if ( slot instanceof LongSlot )
            {
                var3 = true;
                var4 = (LongSlot) slot;
                offset = var4.offset();
                boolean var7 = var4.nullable();
                CypherType var8 = var4.typ();
                if ( !var7 )
                {
                    var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var8 == null )
                        {
                            break label106;
                        }
                    }
                    else if ( var10000.equals( var8 ) )
                    {
                        break label106;
                    }
                }
            }

            int offset;
            label107:
            {
                if ( var3 )
                {
                    offset = var4.offset();
                    boolean var11 = var4.nullable();
                    CypherType var12 = var4.typ();
                    if ( var11 )
                    {
                        var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                        if ( var10000 == null )
                        {
                            if ( var12 == null )
                            {
                                break label107;
                            }
                        }
                        else if ( var10000.equals( var12 ) )
                        {
                            break label107;
                        }
                    }
                }

                RelationshipType var24;
                if ( var3 )
                {
                    int offset = var4.offset();
                    boolean var15 = var4.nullable();
                    CypherType var16 = var4.typ();
                    if ( !var15 )
                    {
                        label108:
                        {
                            var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                            if ( var24 == null )
                            {
                                if ( var16 != null )
                                {
                                    break label108;
                                }
                            }
                            else if ( !var24.equals( var16 ) )
                            {
                                break label108;
                            }

                            var2 = new RelationshipFromSlot( offset );
                            return (Expression) var2;
                        }
                    }
                }

                int offset;
                label109:
                {
                    if ( var3 )
                    {
                        offset = var4.offset();
                        boolean var19 = var4.nullable();
                        CypherType var20 = var4.typ();
                        if ( var19 )
                        {
                            var24 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                            if ( var24 == null )
                            {
                                if ( var20 == null )
                                {
                                    break label109;
                                }
                            }
                            else if ( var24.equals( var20 ) )
                            {
                                break label109;
                            }
                        }
                    }

                    if ( !(slot instanceof RefSlot) )
                    {
                        throw new InternalException( (new StringBuilder( 27 )).append( "Do not know how to project " ).append( slot ).toString() );
                    }

                    RefSlot var22 = (RefSlot) slot;
                    int offset = var22.offset();
                    var2 = new ReferenceFromSlot( offset );
                    return (Expression) var2;
                }

                var2 = new NullCheck( offset, new RelationshipFromSlot( offset ) );
                return (Expression) var2;
            }

            var2 = new NullCheck( offset, new NodeFromSlot( offset ) );
            return (Expression) var2;
        }

        var2 = new NodeFromSlot( offset );
        return (Expression) var2;
    }

    public UnionSlottedPipe.RowMapping computeUnionMapping( final SlotConfiguration in, final SlotConfiguration out )
    {
        Iterable mapSlots = in.mapSlot( ( x0$14 ) -> {
            Function3 var2;
            if ( x0$14 != null )
            {
                String k = (String) x0$14._1();
                Slot inSlot = (Slot) x0$14._2();
                if ( inSlot instanceof LongSlot )
                {
                    LongSlot var8 = (LongSlot) inSlot;
                    boolean var9 = false;
                    Some var10 = null;
                    Option var11 = out.get( k );
                    Function3 var4;
                    if ( scala.None..MODULE$.equals( var11 )){
                    var4 = ( x$18, x$19, x$20 ) -> {
                        $anonfun$computeUnionMapping$2( x$18, x$19, x$20 );
                        return BoxedUnit.UNIT;
                    };
                } else{
                    label80:
                    {
                        if ( var11 instanceof Some )
                        {
                            var9 = true;
                            var10 = (Some) var11;
                            Slot lx = (Slot) var10.value();
                            if ( lx instanceof LongSlot )
                            {
                                LongSlot var13 = (LongSlot) lx;
                                var4 = ( in, outx, x$21 ) -> {
                                    $anonfun$computeUnionMapping$3( var13, var8, in, outx, x$21 );
                                    return BoxedUnit.UNIT;
                                };
                                break label80;
                            }
                        }

                        if ( !var9 )
                        {
                            throw new MatchError( var11 );
                        }

                        Slot rx = (Slot) var10.value();
                        if ( !(rx instanceof RefSlot) )
                        {
                            throw new MatchError( var11 );
                        }

                        RefSlot var15 = (RefSlot) rx;
                        Expression projectionExpression = MODULE$.projectSlotExpression( var8 );
                        var4 = ( in, outx, state ) -> {
                            $anonfun$computeUnionMapping$4( projectionExpression, var15, in, outx, state );
                            return BoxedUnit.UNIT;
                        };
                    }
                }

                    var2 = var4;
                    return var2;
                }
            }

            if ( x0$14 == null )
            {
                throw new MatchError( x0$14 );
            }
            else
            {
                String kx = (String) x0$14._1();
                Slot inSlotx = (Slot) x0$14._2();
                if ( !(inSlotx instanceof RefSlot) )
                {
                    throw new MatchError( x0$14 );
                }
                else
                {
                    RefSlot var19 = (RefSlot) inSlotx;
                    boolean var20 = false;
                    Some var21 = null;
                    Option var22 = out.get( kx );
                    Function3 var3;
                    if ( scala.None..MODULE$.equals( var22 )){
                    var3 = ( x$22, x$23, x$24 ) -> {
                        $anonfun$computeUnionMapping$5( x$22, x$23, x$24 );
                        return BoxedUnit.UNIT;
                    };
                } else{
                    if ( var22 instanceof Some )
                    {
                        var20 = true;
                        var21 = (Some) var22;
                        Slot l = (Slot) var21.value();
                        if ( l instanceof LongSlot )
                        {
                            LongSlot var24 = (LongSlot) l;
                            throw new IllegalStateException(
                                    (new StringBuilder( 52 )).append( "Expected Union output slot to be a refslot but was: " ).append( var24 ).toString() );
                        }
                    }

                    if ( !var20 )
                    {
                        throw new MatchError( var22 );
                    }

                    Slot r = (Slot) var21.value();
                    if ( !(r instanceof RefSlot) )
                    {
                        throw new MatchError( var22 );
                    }

                    RefSlot var26 = (RefSlot) r;
                    var3 = ( in, outx, x$25 ) -> {
                        $anonfun$computeUnionMapping$6( var26, var19, in, outx, x$25 );
                        return BoxedUnit.UNIT;
                    };
                }

                    var2 = var3;
                    return var2;
                }
            }
        }, ( x0$15 ) -> {
            if ( x0$15 != null )
            {
                ASTCachedProperty cachedProp = (ASTCachedProperty) x0$15._1();
                RefSlot inRefSlot = (RefSlot) x0$15._2();
                Option var7 = out.getCachedPropertySlot( cachedProp );
                Function3 var3;
                if ( var7 instanceof Some )
                {
                    Some var8 = (Some) var7;
                    RefSlot outRefSlot = (RefSlot) var8.value();
                    var3 = ( in, outx, x$26 ) -> {
                        $anonfun$computeUnionMapping$8( inRefSlot, outRefSlot, in, outx, x$26 );
                        return BoxedUnit.UNIT;
                    };
                }
                else
                {
                    if ( !scala.None..MODULE$.equals( var7 )){
                    throw new MatchError( var7 );
                }

                    var3 = ( x$27, x$28, x$29 ) -> {
                        $anonfun$computeUnionMapping$9( x$27, x$28, x$29 );
                        return BoxedUnit.UNIT;
                    };
                }

                return var3;
            }
            else
            {
                throw new MatchError( x0$15 );
            }
        } ); return ( incoming, outgoing, state ) -> {
        mapSlots.foreach( ( f ) -> {
            $anonfun$computeUnionMapping$11( incoming, outgoing, state, f );
            return BoxedUnit.UNIT;
        } );
    };
    }

    public ColumnOrder translateColumnOrder( final SlotConfiguration slots, final org.neo4j.cypher.internal.logical.plans.ColumnOrder s )
    {
        Object var3;
        if ( s instanceof org.neo4j.cypher.internal.logical.plans.Ascending )
        {
            org.neo4j.cypher.internal.logical.plans.Ascending var7 = (org.neo4j.cypher.internal.logical.plans.Ascending) s;
            String name = var7.id();
            Option var9 = slots.get( name );
            if ( !(var9 instanceof Some) )
            {
                if ( scala.None..MODULE$.equals( var9 )){
                throw new InternalException(
                        (new StringBuilder( 43 )).append( "Did not find `" ).append( name ).append( "` in the pipeline information" ).toString() );
            }

                throw new MatchError( var9 );
            }

            Some var10 = (Some) var9;
            Slot slot = (Slot) var10.value();
            Ascending var5 = new Ascending( slot );
            var3 = var5;
        }
        else
        {
            if ( !(s instanceof org.neo4j.cypher.internal.logical.plans.Descending) )
            {
                throw new MatchError( s );
            }

            org.neo4j.cypher.internal.logical.plans.Descending var12 = (org.neo4j.cypher.internal.logical.plans.Descending) s;
            String name = var12.id();
            Option var14 = slots.get( name );
            if ( !(var14 instanceof Some) )
            {
                if ( scala.None..MODULE$.equals( var14 )){
                throw new InternalException(
                        (new StringBuilder( 43 )).append( "Did not find `" ).append( name ).append( "` in the pipeline information" ).toString() );
            }

                throw new MatchError( var14 );
            }

            Some var15 = (Some) var14;
            Slot slot = (Slot) var15.value();
            Descending var4 = new Descending( slot );
            var3 = var4;
        }

        return (ColumnOrder) var3;
    }
}
