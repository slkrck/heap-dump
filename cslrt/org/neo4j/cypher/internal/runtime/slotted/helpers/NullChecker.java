package org.neo4j.cypher.internal.runtime.slotted.helpers;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class NullChecker
{
    public static boolean entityIsNull( final long entityId )
    {
        return NullChecker$.MODULE$.entityIsNull( var0 );
    }

    public static long NULL_ENTITY()
    {
        return NullChecker$.MODULE$.NULL_ENTITY();
    }
}
