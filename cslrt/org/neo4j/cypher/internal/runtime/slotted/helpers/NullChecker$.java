package org.neo4j.cypher.internal.runtime.slotted.helpers;

public final class NullChecker$
{
    public static NullChecker$ MODULE$;

    static
    {
        new NullChecker$();
    }

    private final long NULL_ENTITY;

    private NullChecker$()
    {
        MODULE$ = this;
        this.NULL_ENTITY = -1L;
    }

    public final long NULL_ENTITY()
    {
        return this.NULL_ENTITY;
    }

    public final boolean entityIsNull( final long entityId )
    {
        return entityId == this.NULL_ENTITY();
    }
}
