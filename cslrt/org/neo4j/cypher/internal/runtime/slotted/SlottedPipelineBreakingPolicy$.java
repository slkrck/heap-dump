package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.Eager;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.LeftOuterHashJoin;
import org.neo4j.cypher.internal.logical.plans.LogicalLeafPlan;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.NodeHashJoin;
import org.neo4j.cypher.internal.logical.plans.OptionalExpand;
import org.neo4j.cypher.internal.logical.plans.PruningVarExpand;
import org.neo4j.cypher.internal.logical.plans.RightOuterHashJoin;
import org.neo4j.cypher.internal.logical.plans.Union;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.logical.plans.ValueHashJoin;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import org.neo4j.cypher.internal.physicalplanning.PipelineBreakingPolicy;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;

public final class SlottedPipelineBreakingPolicy$ implements PipelineBreakingPolicy
{
    public static SlottedPipelineBreakingPolicy$ MODULE$;

    static
    {
        new SlottedPipelineBreakingPolicy$();
    }

    private SlottedPipelineBreakingPolicy$()
    {
        MODULE$ = this;
        PipelineBreakingPolicy.$init$( this );
    }

    public SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
    {
        return PipelineBreakingPolicy.invoke$( this, lp, slots, argumentSlots );
    }

    public boolean breakOn( final LogicalPlan lp )
    {
        boolean var2;
        if ( lp instanceof LogicalLeafPlan )
        {
            var2 = true;
        }
        else
        {
            boolean var4;
            if ( lp instanceof Aggregation )
            {
                var4 = true;
            }
            else if ( lp instanceof Expand )
            {
                var4 = true;
            }
            else if ( lp instanceof OptionalExpand )
            {
                var4 = true;
            }
            else if ( lp instanceof VarExpand )
            {
                var4 = true;
            }
            else if ( lp instanceof PruningVarExpand )
            {
                var4 = true;
            }
            else if ( lp instanceof UnwindCollection )
            {
                var4 = true;
            }
            else if ( lp instanceof Eager )
            {
                var4 = true;
            }
            else
            {
                var4 = false;
            }

            if ( var4 )
            {
                var2 = true;
            }
            else
            {
                boolean var3;
                if ( lp instanceof CartesianProduct )
                {
                    var3 = true;
                }
                else if ( lp instanceof RightOuterHashJoin )
                {
                    var3 = true;
                }
                else if ( lp instanceof LeftOuterHashJoin )
                {
                    var3 = true;
                }
                else if ( lp instanceof NodeHashJoin )
                {
                    var3 = true;
                }
                else if ( lp instanceof ValueHashJoin )
                {
                    var3 = true;
                }
                else if ( lp instanceof Union )
                {
                    var3 = true;
                }
                else
                {
                    var3 = false;
                }

                if ( var3 )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }
            }
        }

        return var2;
    }

    public void onNestedPlanBreak()
    {
    }
}
