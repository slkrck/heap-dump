package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class LabelsFromSlot$ extends AbstractFunction1<Object,LabelsFromSlot> implements Serializable
{
    public static LabelsFromSlot$ MODULE$;

    static
    {
        new LabelsFromSlot$();
    }

    private LabelsFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "LabelsFromSlot";
    }

    public LabelsFromSlot apply( final int offset )
    {
        return new LabelsFromSlot( offset );
    }

    public Option<Object> unapply( final LabelsFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.offset() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
