package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.interpreted.CommandProjection;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverter;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Predicate;
import org.neo4j.cypher.internal.runtime.interpreted.commands.values.KeyToken;
import org.neo4j.cypher.internal.runtime.interpreted.commands.values.KeyToken.Unresolved;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.HasLabels;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import org.neo4j.cypher.internal.v4_0.expressions.Property;
import org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class MaterializedEntitiesExpressionConverter implements ExpressionConverter, Product, Serializable
{
    private final TokenContext tokenContext;

    public MaterializedEntitiesExpressionConverter( final TokenContext tokenContext )
    {
        this.tokenContext = tokenContext;
        Product.$init$( this );
    }

    public static Option<TokenContext> unapply( final MaterializedEntitiesExpressionConverter x$0 )
    {
        return MaterializedEntitiesExpressionConverter$.MODULE$.unapply( var0 );
    }

    public static MaterializedEntitiesExpressionConverter apply( final TokenContext tokenContext )
    {
        return MaterializedEntitiesExpressionConverter$.MODULE$.apply( var0 );
    }

    public static <A> Function1<TokenContext,A> andThen( final Function1<MaterializedEntitiesExpressionConverter,A> g )
    {
        return MaterializedEntitiesExpressionConverter$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,MaterializedEntitiesExpressionConverter> compose( final Function1<A,TokenContext> g )
    {
        return MaterializedEntitiesExpressionConverter$.MODULE$.compose( var0 );
    }

    public TokenContext tokenContext()
    {
        return this.tokenContext;
    }

    public Option<Expression> toCommandExpression( final int id, final org.neo4j.cypher.internal.v4_0.expressions.Expression expression,
            final ExpressionConverters self )
    {
        Object var4;
        if ( expression instanceof Property )
        {
            Property var6 = (Property) expression;
            var4 = this.toCommandProperty( id, var6, self );
        }
        else if ( expression instanceof HasLabels )
        {
            HasLabels var7 = (HasLabels) expression;
            var4 = this.hasLabels( id, var7, self );
        }
        else if ( expression instanceof FunctionInvocation )
        {
            FunctionInvocation var8 = (FunctionInvocation) expression;
            var4 = this.toCommandExpression( id, var8.function(), var8, self );
        }
        else
        {
            var4 = .MODULE$;
        }

        return (Option) var4;
    }

    public Option<GroupingExpression> toGroupingExpression( final int id, final Map<String,org.neo4j.cypher.internal.v4_0.expressions.Expression> groupings,
            final Seq<org.neo4j.cypher.internal.v4_0.expressions.Expression> orderToLeverage, final ExpressionConverters self )
    {
        return .MODULE$;
    }

    public Option<CommandProjection> toCommandProjection( final int id, final Map<String,org.neo4j.cypher.internal.v4_0.expressions.Expression> projections,
            final ExpressionConverters self )
    {
        return .MODULE$;
    }

    private Option<Expression> toCommandExpression( final int id, final Function expression, final FunctionInvocation invocation,
            final ExpressionConverters self )
    {
        Tuple2 var6 = new Tuple2( expression, invocation.arguments().head() );
        Object var5;
        if ( var6 != null )
        {
            Function var7 = (Function) var6._1();
            org.neo4j.cypher.internal.v4_0.expressions.Expression property = (org.neo4j.cypher.internal.v4_0.expressions.Expression) var6._2();
            if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Exists..MODULE$.equals( var7 ) && property instanceof Property){
            Property var9 = (Property) property;
            var5 = new Some( new MaterializedEntityPropertyExists( self.toCommandExpression( id, var9.map() ), this.getPropertyKey( var9.propertyKey() ) ) );
            return (Option) var5;
        }
        }

        if ( var6 != null )
        {
            Function var10 = (Function) var6._1();
            org.neo4j.cypher.internal.v4_0.expressions.Expression arg = (org.neo4j.cypher.internal.v4_0.expressions.Expression) var6._2();
            if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Keys..MODULE$.equals( var10 )){
            var5 = new Some( new MaterializedEntityKeysFunction( self.toCommandExpression( id, arg ) ) );
            return (Option) var5;
        }
        }

        if ( var6 != null )
        {
            Function var12 = (Function) var6._1();
            org.neo4j.cypher.internal.v4_0.expressions.Expression arg = (org.neo4j.cypher.internal.v4_0.expressions.Expression) var6._2();
            if ( org.neo4j.cypher.internal.v4_0.expressions.functions.Labels..MODULE$.equals( var12 )){
            var5 = new Some( new MaterializedEntityLabelsFunction( self.toCommandExpression( id, arg ) ) );
            return (Option) var5;
        }
        }

        var5 = .MODULE$;
        return (Option) var5;
    }

    private Option<Expression> toCommandProperty( final int id, final LogicalProperty e, final ExpressionConverters self )
    {
        Object var4;
        if ( e instanceof Property )
        {
            Property var6 = (Property) e;
            org.neo4j.cypher.internal.v4_0.expressions.Expression map = var6.map();
            PropertyKeyName propertyKey = var6.propertyKey();
            var4 = new Some( new MaterializedEntityProperty( self.toCommandExpression( id, map ), this.getPropertyKey( propertyKey ) ) );
        }
        else
        {
            var4 = .MODULE$;
        }

        return (Option) var4;
    }

    private KeyToken getPropertyKey( final PropertyKeyName propertyKey )
    {
        Option var3 = this.tokenContext().getOptPropertyKeyId( propertyKey.name() );
        Object var2;
        if ( var3 instanceof Some )
        {
            Some var4 = (Some) var3;
            int propertyKeyId = BoxesRunTime.unboxToInt( var4.value() );
            var2 = org.neo4j.cypher.internal.runtime.interpreted.commands.values.TokenType.PropertyKey..MODULE$.apply( propertyKey.name(), propertyKeyId );
        }
        else
        {
            var2 = org.neo4j.cypher.internal.runtime.interpreted.commands.values.TokenType.PropertyKey..MODULE$.apply( propertyKey.name() );
        }

        return (KeyToken) var2;
    }

    private Option<Predicate> hasLabels( final int id, final HasLabels e, final ExpressionConverters self )
    {
        return new Some( ((TraversableOnce) e.labels().map( ( l ) -> {
            return new MaterializedEntityHasLabel( self.toCommandExpression( id, e.expression() ),
                    new Unresolved( l.name(), org.neo4j.cypher.internal.runtime.interpreted.commands.values.TokenType.Label..MODULE$ ));
        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).reduceLeft( ( a, b ) -> {
            return org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.And..MODULE$.apply( a, b );
        } ));
    }

    public MaterializedEntitiesExpressionConverter copy( final TokenContext tokenContext )
    {
        return new MaterializedEntitiesExpressionConverter( tokenContext );
    }

    public TokenContext copy$default$1()
    {
        return this.tokenContext();
    }

    public String productPrefix()
    {
        return "MaterializedEntitiesExpressionConverter";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.tokenContext();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MaterializedEntitiesExpressionConverter;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof MaterializedEntitiesExpressionConverter )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        MaterializedEntitiesExpressionConverter var4 = (MaterializedEntitiesExpressionConverter) x$1;
                        TokenContext var10000 = this.tokenContext();
                        TokenContext var5 = var4.tokenContext();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
