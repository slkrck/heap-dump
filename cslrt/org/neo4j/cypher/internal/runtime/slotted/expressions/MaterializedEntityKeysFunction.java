package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.NullInNullOutExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.operations.CypherFunctions;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class MaterializedEntityKeysFunction extends NullInNullOutExpression implements Product, Serializable
{
    private final Expression expr;

    public MaterializedEntityKeysFunction( final Expression expr )
    {
        super( expr );
        this.expr = expr;
        Product.$init$( this );
    }

    public static Option<Expression> unapply( final MaterializedEntityKeysFunction x$0 )
    {
        return MaterializedEntityKeysFunction$.MODULE$.unapply( var0 );
    }

    public static <A> Function1<Expression,A> andThen( final Function1<MaterializedEntityKeysFunction,A> g )
    {
        return MaterializedEntityKeysFunction$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,MaterializedEntityKeysFunction> compose( final Function1<A,Expression> g )
    {
        return MaterializedEntityKeysFunction$.MODULE$.compose( var0 );
    }

    public Expression expr()
    {
        return this.expr;
    }

    public ListValue compute( final AnyValue value, final ExecutionContext ctx, final QueryState state )
    {
        ListValue var4;
        if ( value instanceof NodeValue )
        {
            NodeValue var6 = (NodeValue) value;
            var4 = var6.properties().keys();
        }
        else if ( value instanceof RelationshipValue )
        {
            RelationshipValue var7 = (RelationshipValue) value;
            var4 = var7.properties().keys();
        }
        else
        {
            var4 = CypherFunctions.keys( value, state.query(), state.cursors().nodeCursor(), state.cursors().relationshipScanCursor(),
                    state.cursors().propertyCursor() );
        }

        return var4;
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return (Expression) f.apply( new MaterializedEntityKeysFunction( this.expr().rewrite( f ) ) );
    }

    public Seq<Expression> arguments()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.expr()}) ));
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.expr()}) ));
    }

    public MaterializedEntityKeysFunction copy( final Expression expr )
    {
        return new MaterializedEntityKeysFunction( expr );
    }

    public Expression copy$default$1()
    {
        return this.expr();
    }

    public String productPrefix()
    {
        return "MaterializedEntityKeysFunction";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.expr();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MaterializedEntityKeysFunction;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof MaterializedEntityKeysFunction )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        MaterializedEntityKeysFunction var4 = (MaterializedEntityKeysFunction) x$1;
                        Expression var10000 = this.expr();
                        Expression var5 = var4.expr();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
