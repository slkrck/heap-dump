package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Predicate;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class PrimitiveEquals extends Predicate implements SlottedExpression, Product, Serializable
{
    private final Expression a;
    private final Expression b;

    public PrimitiveEquals( final Expression a, final Expression b )
    {
        this.a = a;
        this.b = b;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<Expression,Expression>> unapply( final PrimitiveEquals x$0 )
    {
        return PrimitiveEquals$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple2<Expression,Expression>,PrimitiveEquals> tupled()
    {
        return PrimitiveEquals$.MODULE$.tupled();
    }

    public static Function1<Expression,Function1<Expression,PrimitiveEquals>> curried()
    {
        return PrimitiveEquals$.MODULE$.curried();
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public Expression a()
    {
        return this.a;
    }

    public Expression b()
    {
        return this.b;
    }

    public Option<Object> isMatch( final ExecutionContext m, final QueryState state )
    {
        Some var10000;
        boolean var10002;
        label17:
        {
            label16:
            {
                AnyValue value1 = this.a().apply( m, state );
                AnyValue value2 = this.b().apply( m, state );
                var10000 = new Some; if ( value1 == null )
            {
                if ( value2 == null )
                {
                    break label16;
                }
            }
            else if ( value1.equals( value2 ) )
            {
                break label16;
            }

                var10002 = false;
                break label17;
            }

            var10002 = true;
        }

        var10000.<init> (BoxesRunTime.boxToBoolean( var10002 ));
        return var10000;
    }

    public boolean containsIsNull()
    {
        return false;
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return (Expression) f.apply( new PrimitiveEquals( this.a().rewrite( f ), this.b().rewrite( f ) ) );
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.a(), this.b()}) ));
    }

    public PrimitiveEquals copy( final Expression a, final Expression b )
    {
        return new PrimitiveEquals( a, b );
    }

    public Expression copy$default$1()
    {
        return this.a();
    }

    public Expression copy$default$2()
    {
        return this.b();
    }

    public String productPrefix()
    {
        return "PrimitiveEquals";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Expression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.a();
            break;
        case 1:
            var10000 = this.b();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof PrimitiveEquals;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof PrimitiveEquals )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            PrimitiveEquals var4 = (PrimitiveEquals) x$1;
                            Expression var10000 = this.a();
                            Expression var5 = var4.a();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.b();
                            Expression var6 = var4.b();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
