package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.Tuple2.mcII.sp;
import scala.runtime.AbstractFunction2;

public final class HasLabelFromSlot$ extends AbstractFunction2<Object,Object,HasLabelFromSlot> implements Serializable
{
    public static HasLabelFromSlot$ MODULE$;

    static
    {
        new HasLabelFromSlot$();
    }

    private HasLabelFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "HasLabelFromSlot";
    }

    public HasLabelFromSlot apply( final int offset, final int resolvedLabelToken )
    {
        return new HasLabelFromSlot( offset, resolvedLabelToken );
    }

    public Option<Tuple2<Object,Object>> unapply( final HasLabelFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new sp( x$0.offset(), x$0.resolvedLabelToken() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
