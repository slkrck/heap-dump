package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.ExpressionVariable;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class NestedPipeSlottedExpression$ extends AbstractFunction4<Pipe,Expression,SlotConfiguration,ExpressionVariable[],NestedPipeSlottedExpression>
        implements Serializable
{
    public static NestedPipeSlottedExpression$ MODULE$;

    static
    {
        new NestedPipeSlottedExpression$();
    }

    private NestedPipeSlottedExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NestedPipeSlottedExpression";
    }

    public NestedPipeSlottedExpression apply( final Pipe pipe, final Expression inner, final SlotConfiguration slots,
            final ExpressionVariable[] availableExpressionVariables )
    {
        return new NestedPipeSlottedExpression( pipe, inner, slots, availableExpressionVariables );
    }

    public Option<Tuple4<Pipe,Expression,SlotConfiguration,ExpressionVariable[]>> unapply( final NestedPipeSlottedExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple4( x$0.pipe(), x$0.inner(), x$0.slots(), x$0.availableExpressionVariables() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
