package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledProjection;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class CompileWrappingProjection$ extends AbstractFunction2<CompiledProjection,Object,CompileWrappingProjection> implements Serializable
{
    public static CompileWrappingProjection$ MODULE$;

    static
    {
        new CompileWrappingProjection$();
    }

    private CompileWrappingProjection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CompileWrappingProjection";
    }

    public CompileWrappingProjection apply( final CompiledProjection projection, final boolean isEmpty )
    {
        return new CompileWrappingProjection( projection, isEmpty );
    }

    public Option<Tuple2<CompiledProjection,Object>> unapply( final CompileWrappingProjection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.projection(), BoxesRunTime.boxToBoolean( x$0.isEmpty() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
