package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.planner.spi.TokenContext;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class MaterializedEntitiesExpressionConverter$ extends AbstractFunction1<TokenContext,MaterializedEntitiesExpressionConverter>
        implements Serializable
{
    public static MaterializedEntitiesExpressionConverter$ MODULE$;

    static
    {
        new MaterializedEntitiesExpressionConverter$();
    }

    private MaterializedEntitiesExpressionConverter$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MaterializedEntitiesExpressionConverter";
    }

    public MaterializedEntitiesExpressionConverter apply( final TokenContext tokenContext )
    {
        return new MaterializedEntitiesExpressionConverter( tokenContext );
    }

    public Option<TokenContext> unapply( final MaterializedEntitiesExpressionConverter x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.tokenContext() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
