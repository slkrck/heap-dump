package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.ExtendedExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class CompileWrappingExpression extends Expression implements ExtendedExpression, Product, Serializable
{
    private final CompiledExpression ce;
    private final Expression legacy;
    private final boolean isDeterministic;

    public CompileWrappingExpression( final CompiledExpression ce, final Expression legacy )
    {
        this.ce = ce;
        this.legacy = legacy;
        Product.$init$( this );
        this.isDeterministic = !legacy.exists( ( x0$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$isDeterministic$1( x0$1 ) );
        } );
    }

    public static Option<Tuple2<CompiledExpression,Expression>> unapply( final CompileWrappingExpression x$0 )
    {
        return CompileWrappingExpression$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple2<CompiledExpression,Expression>,CompileWrappingExpression> tupled()
    {
        return CompileWrappingExpression$.MODULE$.tupled();
    }

    public static Function1<CompiledExpression,Function1<Expression,CompileWrappingExpression>> curried()
    {
        return CompileWrappingExpression$.MODULE$.curried();
    }

    public CompiledExpression ce()
    {
        return this.ce;
    }

    public Expression legacy()
    {
        return this.legacy;
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return (Expression) f.apply( this );
    }

    public Seq<Expression> arguments()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.legacy()}) ));
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.legacy()}) ));
    }

    public AnyValue apply( final ExecutionContext ctx, final QueryState state )
    {
        return this.ce().evaluate( ctx, state.query(), CompiledExpressionConverter$.MODULE$.parametersOrFail( state ), state.cursors(),
                state.expressionVariables() );
    }

    public String toString()
    {
        return this.legacy().toString();
    }

    public boolean isDeterministic()
    {
        return this.isDeterministic;
    }

    public CompileWrappingExpression copy( final CompiledExpression ce, final Expression legacy )
    {
        return new CompileWrappingExpression( ce, legacy );
    }

    public CompiledExpression copy$default$1()
    {
        return this.ce();
    }

    public Expression copy$default$2()
    {
        return this.legacy();
    }

    public String productPrefix()
    {
        return "CompileWrappingExpression";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.ce();
            break;
        case 1:
            var10000 = this.legacy();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CompileWrappingExpression;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof CompileWrappingExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            CompileWrappingExpression var4 = (CompileWrappingExpression) x$1;
                            CompiledExpression var10000 = this.ce();
                            CompiledExpression var5 = var4.ce();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Expression var7 = this.legacy();
                            Expression var6 = var4.legacy();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
