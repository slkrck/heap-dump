package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class ReferenceFromSlot$ extends AbstractFunction1<Object,ReferenceFromSlot> implements Serializable
{
    public static ReferenceFromSlot$ MODULE$;

    static
    {
        new ReferenceFromSlot$();
    }

    private ReferenceFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ReferenceFromSlot";
    }

    public ReferenceFromSlot apply( final int offset )
    {
        return new ReferenceFromSlot( offset );
    }

    public Option<Object> unapply( final ReferenceFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.offset() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
