package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AbstractCachedRelationshipProperty;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.values.storable.Value;
import org.neo4j.values.virtual.VirtualRelationshipValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class SlottedCachedRelationshipPropertyLate extends AbstractCachedRelationshipProperty implements SlottedExpression, Product, Serializable
{
    private final int relationshipOffset;
    private final boolean offsetIsForLongSlot;
    private final String propertyKey;
    private final int cachedPropertyOffset;

    public SlottedCachedRelationshipPropertyLate( final int relationshipOffset, final boolean offsetIsForLongSlot, final String propertyKey,
            final int cachedPropertyOffset )
    {
        this.relationshipOffset = relationshipOffset;
        this.offsetIsForLongSlot = offsetIsForLongSlot;
        this.propertyKey = propertyKey;
        this.cachedPropertyOffset = cachedPropertyOffset;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<Object,Object,String,Object>> unapply( final SlottedCachedRelationshipPropertyLate x$0 )
    {
        return SlottedCachedRelationshipPropertyLate$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple4<Object,Object,String,Object>,SlottedCachedRelationshipPropertyLate> tupled()
    {
        return SlottedCachedRelationshipPropertyLate$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Object,Function1<String,Function1<Object,SlottedCachedRelationshipPropertyLate>>>> curried()
    {
        return SlottedCachedRelationshipPropertyLate$.MODULE$.curried();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int relationshipOffset()
    {
        return this.relationshipOffset;
    }

    public boolean offsetIsForLongSlot()
    {
        return this.offsetIsForLongSlot;
    }

    public String propertyKey()
    {
        return this.propertyKey;
    }

    public int cachedPropertyOffset()
    {
        return this.cachedPropertyOffset;
    }

    public long getId( final ExecutionContext ctx )
    {
        return this.offsetIsForLongSlot() ? ctx.getLongAt( this.relationshipOffset() )
                                          : ((VirtualRelationshipValue) ctx.getRefAt( this.relationshipOffset() )).id();
    }

    public Value getCachedProperty( final ExecutionContext ctx )
    {
        return ctx.getCachedPropertyAt( this.cachedPropertyOffset() );
    }

    public void setCachedProperty( final ExecutionContext ctx, final Value value )
    {
        ctx.setCachedPropertyAt( this.cachedPropertyOffset(), value );
    }

    public int getPropertyKey( final TokenContext tokenContext )
    {
        return BoxesRunTime.unboxToInt( tokenContext.getOptPropertyKeyId( this.propertyKey() ).getOrElse( () -> {
            return -1;
        } ) );
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.empty();
    }

    public SlottedCachedRelationshipPropertyLate copy( final int relationshipOffset, final boolean offsetIsForLongSlot, final String propertyKey,
            final int cachedPropertyOffset )
    {
        return new SlottedCachedRelationshipPropertyLate( relationshipOffset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset );
    }

    public int copy$default$1()
    {
        return this.relationshipOffset();
    }

    public boolean copy$default$2()
    {
        return this.offsetIsForLongSlot();
    }

    public String copy$default$3()
    {
        return this.propertyKey();
    }

    public int copy$default$4()
    {
        return this.cachedPropertyOffset();
    }

    public String productPrefix()
    {
        return "SlottedCachedRelationshipPropertyLate";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.relationshipOffset() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.offsetIsForLongSlot() );
            break;
        case 2:
            var10000 = this.propertyKey();
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.cachedPropertyOffset() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedCachedRelationshipPropertyLate;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.relationshipOffset() );
        var1 = Statics.mix( var1, this.offsetIsForLongSlot() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.propertyKey() ) );
        var1 = Statics.mix( var1, this.cachedPropertyOffset() );
        return Statics.finalizeHash( var1, 4 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label59:
            {
                boolean var2;
                if ( x$1 instanceof SlottedCachedRelationshipPropertyLate )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label42:
                    {
                        SlottedCachedRelationshipPropertyLate var4 = (SlottedCachedRelationshipPropertyLate) x$1;
                        if ( this.relationshipOffset() == var4.relationshipOffset() && this.offsetIsForLongSlot() == var4.offsetIsForLongSlot() )
                        {
                            label39:
                            {
                                String var10000 = this.propertyKey();
                                String var5 = var4.propertyKey();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label39;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label39;
                                }

                                if ( this.cachedPropertyOffset() == var4.cachedPropertyOffset() && var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label42;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label59;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
