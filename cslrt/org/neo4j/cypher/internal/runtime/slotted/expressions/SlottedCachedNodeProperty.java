package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AbstractCachedNodeProperty;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.values.storable.Value;
import org.neo4j.values.virtual.VirtualNodeValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class SlottedCachedNodeProperty extends AbstractCachedNodeProperty implements SlottedExpression, Product, Serializable
{
    private final int nodeOffset;
    private final boolean offsetIsForLongSlot;
    private final int propertyKey;
    private final int cachedPropertyOffset;

    public SlottedCachedNodeProperty( final int nodeOffset, final boolean offsetIsForLongSlot, final int propertyKey, final int cachedPropertyOffset )
    {
        this.nodeOffset = nodeOffset;
        this.offsetIsForLongSlot = offsetIsForLongSlot;
        this.propertyKey = propertyKey;
        this.cachedPropertyOffset = cachedPropertyOffset;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple4<Object,Object,Object,Object>> unapply( final SlottedCachedNodeProperty x$0 )
    {
        return SlottedCachedNodeProperty$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple4<Object,Object,Object,Object>,SlottedCachedNodeProperty> tupled()
    {
        return SlottedCachedNodeProperty$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Object,Function1<Object,Function1<Object,SlottedCachedNodeProperty>>>> curried()
    {
        return SlottedCachedNodeProperty$.MODULE$.curried();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int nodeOffset()
    {
        return this.nodeOffset;
    }

    public boolean offsetIsForLongSlot()
    {
        return this.offsetIsForLongSlot;
    }

    public int propertyKey()
    {
        return this.propertyKey;
    }

    public int cachedPropertyOffset()
    {
        return this.cachedPropertyOffset;
    }

    public long getId( final ExecutionContext ctx )
    {
        return this.offsetIsForLongSlot() ? ctx.getLongAt( this.nodeOffset() ) : ((VirtualNodeValue) ctx.getRefAt( this.nodeOffset() )).id();
    }

    public Value getCachedProperty( final ExecutionContext ctx )
    {
        return ctx.getCachedPropertyAt( this.cachedPropertyOffset() );
    }

    public void setCachedProperty( final ExecutionContext ctx, final Value value )
    {
        ctx.setCachedPropertyAt( this.cachedPropertyOffset(), value );
    }

    public int getPropertyKey( final TokenContext tokenContext )
    {
        return this.propertyKey();
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.empty();
    }

    public SlottedCachedNodeProperty copy( final int nodeOffset, final boolean offsetIsForLongSlot, final int propertyKey, final int cachedPropertyOffset )
    {
        return new SlottedCachedNodeProperty( nodeOffset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset );
    }

    public int copy$default$1()
    {
        return this.nodeOffset();
    }

    public boolean copy$default$2()
    {
        return this.offsetIsForLongSlot();
    }

    public int copy$default$3()
    {
        return this.propertyKey();
    }

    public int copy$default$4()
    {
        return this.cachedPropertyOffset();
    }

    public String productPrefix()
    {
        return "SlottedCachedNodeProperty";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.nodeOffset() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.offsetIsForLongSlot() );
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.propertyKey() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToInteger( this.cachedPropertyOffset() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedCachedNodeProperty;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.nodeOffset() );
        var1 = Statics.mix( var1, this.offsetIsForLongSlot() ? 1231 : 1237 );
        var1 = Statics.mix( var1, this.propertyKey() );
        var1 = Statics.mix( var1, this.cachedPropertyOffset() );
        return Statics.finalizeHash( var1, 4 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        label49:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof SlottedCachedNodeProperty )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label49;
                }

                SlottedCachedNodeProperty var4 = (SlottedCachedNodeProperty) x$1;
                if ( this.nodeOffset() != var4.nodeOffset() || this.offsetIsForLongSlot() != var4.offsetIsForLongSlot() ||
                        this.propertyKey() != var4.propertyKey() || this.cachedPropertyOffset() != var4.cachedPropertyOffset() || !var4.canEqual( this ) )
                {
                    break label49;
                }
            }

            var10000 = true;
            return var10000;
        }

        var10000 = false;
        return var10000;
    }
}
