package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.values.KeyToken;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class MaterializedEntityHasLabel$ extends AbstractFunction2<Expression,KeyToken,MaterializedEntityHasLabel> implements Serializable
{
    public static MaterializedEntityHasLabel$ MODULE$;

    static
    {
        new MaterializedEntityHasLabel$();
    }

    private MaterializedEntityHasLabel$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MaterializedEntityHasLabel";
    }

    public MaterializedEntityHasLabel apply( final Expression entity, final KeyToken label )
    {
        return new MaterializedEntityHasLabel( entity, label );
    }

    public Option<Tuple2<Expression,KeyToken>> unapply( final MaterializedEntityHasLabel x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.entity(), x$0.label() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
