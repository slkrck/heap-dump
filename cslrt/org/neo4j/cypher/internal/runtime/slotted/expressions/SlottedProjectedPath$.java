package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.PathValueBuilder;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.operations.CypherFunctions;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.RelationshipValue;
import scala.Function2;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.immutable.Set;

public final class SlottedProjectedPath$ implements Serializable
{
    public static SlottedProjectedPath$ MODULE$;

    static
    {
        new SlottedProjectedPath$();
    }

    private SlottedProjectedPath$()
    {
        MODULE$ = this;
    }

    public PathValueBuilder org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addIncoming( final AnyValue relValue,
            final QueryState state, final PathValueBuilder builder )
    {
        PathValueBuilder var4;
        if ( relValue instanceof RelationshipValue )
        {
            RelationshipValue var6 = (RelationshipValue) relValue;
            var4 = builder.addRelationship( var6 ).addNode( CypherFunctions.startNode( var6, state.query(), state.cursors().relationshipScanCursor() ) );
        }
        else
        {
            if ( relValue != Values.NO_VALUE )
            {
                throw new CypherTypeException(
                        (new StringBuilder( 35 )).append( "Expected RelationshipValue but got " ).append( relValue.getTypeName() ).toString() );
            }

            var4 = builder.addNoValue();
        }

        return var4;
    }

    public PathValueBuilder org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addOutgoing( final AnyValue relValue,
            final QueryState state, final PathValueBuilder builder )
    {
        PathValueBuilder var4;
        if ( relValue instanceof RelationshipValue )
        {
            RelationshipValue var6 = (RelationshipValue) relValue;
            var4 = builder.addRelationship( var6 ).addNode( CypherFunctions.endNode( var6, state.query(), state.cursors().relationshipScanCursor() ) );
        }
        else
        {
            if ( relValue != Values.NO_VALUE )
            {
                throw new CypherTypeException(
                        (new StringBuilder( 35 )).append( "Expected RelationshipValue but got " ).append( relValue.getTypeName() ).toString() );
            }

            var4 = builder.addNoValue();
        }

        return var4;
    }

    public PathValueBuilder org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addUndirected( final AnyValue relValue,
            final QueryState state, final PathValueBuilder builder )
    {
        PathValueBuilder var4;
        if ( relValue instanceof RelationshipValue )
        {
            RelationshipValue var6 = (RelationshipValue) relValue;
            NodeValue previous = builder.previousNode();
            var4 = builder.addRelationship( var6 ).addNode(
                    CypherFunctions.otherNode( var6, state.query(), previous, state.cursors().relationshipScanCursor() ) );
        }
        else
        {
            if ( relValue != Values.NO_VALUE )
            {
                throw new CypherTypeException(
                        (new StringBuilder( 35 )).append( "Expected RelationshipValue but got " ).append( relValue.getTypeName() ).toString() );
            }

            var4 = builder.addNoValue();
        }

        return var4;
    }

    public PathValueBuilder org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addAllExceptLast( final PathValueBuilder builder,
            final ListValue list, final Function2<PathValueBuilder,AnyValue,PathValueBuilder> f )
    {
        PathValueBuilder aggregated = builder;
        int size = list.size();

        for ( int i = 0; i < size - 1; ++i )
        {
            aggregated = (PathValueBuilder) f.apply( aggregated, list.value( i ) );
        }

        return aggregated;
    }

    public boolean org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$correctDirection( final NodeValue previous,
            final AnyValue first )
    {
        RelationshipValue rel = (RelationshipValue) first;
        boolean correctDirection = rel.startNode().id() == previous.id() || rel.endNode().id() == previous.id();
        return correctDirection;
    }

    public SlottedProjectedPath apply( final Set<String> symbolTableDependencies, final SlottedProjectedPath.Projector projector )
    {
        return new SlottedProjectedPath( symbolTableDependencies, projector );
    }

    public Option<Tuple2<Set<String>,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.symbolTableDependencies(), x$0.projector() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
