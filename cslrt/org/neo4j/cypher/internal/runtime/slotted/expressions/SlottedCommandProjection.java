package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.CommandProjection;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlottedCommandProjection implements CommandProjection, Product, Serializable
{
    private final Map<Object,Expression> introducedExpressions;
    private final Iterable<Function2<ExecutionContext,QueryState,BoxedUnit>> projectionFunctions;

    public SlottedCommandProjection( final Map<Object,Expression> introducedExpressions )
    {
        this.introducedExpressions = introducedExpressions;
        Product.$init$( this );
        this.projectionFunctions = (Iterable) introducedExpressions.map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                int offset = x0$1._1$mcI$sp();
                Expression expression = (Expression) x0$1._2();
                Function2 var1 = ( ctx, state ) -> {
                    $anonfun$projectionFunctions$2( offset, expression, ctx, state );
                    return BoxedUnit.UNIT;
                };
                return var1;
            }
            else
            {
                throw new MatchError( x0$1 );
            }
        }, scala.collection.immutable.Iterable..MODULE$.canBuildFrom());
    }

    public static Option<Map<Object,Expression>> unapply( final SlottedCommandProjection x$0 )
    {
        return SlottedCommandProjection$.MODULE$.unapply( var0 );
    }

    public static SlottedCommandProjection apply( final Map<Object,Expression> introducedExpressions )
    {
        return SlottedCommandProjection$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Map<Object,Expression>,A> andThen( final Function1<SlottedCommandProjection,A> g )
    {
        return SlottedCommandProjection$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,SlottedCommandProjection> compose( final Function1<A,Map<Object,Expression>> g )
    {
        return SlottedCommandProjection$.MODULE$.compose( var0 );
    }

    public Map<Object,Expression> introducedExpressions()
    {
        return this.introducedExpressions;
    }

    public boolean isEmpty()
    {
        return this.introducedExpressions().isEmpty();
    }

    public void registerOwningPipe( final Pipe pipe )
    {
        this.introducedExpressions().values().foreach( ( x$1 ) -> {
            $anonfun$registerOwningPipe$1( pipe, x$1 );
            return BoxedUnit.UNIT;
        } );
    }

    private Iterable<Function2<ExecutionContext,QueryState,BoxedUnit>> projectionFunctions()
    {
        return this.projectionFunctions;
    }

    public void project( final ExecutionContext ctx, final QueryState state )
    {
        this.projectionFunctions().foreach( ( x$2 ) -> {
            $anonfun$project$1( ctx, state, x$2 );
            return BoxedUnit.UNIT;
        } );
    }

    public SlottedCommandProjection copy( final Map<Object,Expression> introducedExpressions )
    {
        return new SlottedCommandProjection( introducedExpressions );
    }

    public Map<Object,Expression> copy$default$1()
    {
        return this.introducedExpressions();
    }

    public String productPrefix()
    {
        return "SlottedCommandProjection";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.introducedExpressions();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedCommandProjection;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof SlottedCommandProjection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        SlottedCommandProjection var4 = (SlottedCommandProjection) x$1;
                        Map var10000 = this.introducedExpressions();
                        Map var5 = var4.introducedExpressions();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
