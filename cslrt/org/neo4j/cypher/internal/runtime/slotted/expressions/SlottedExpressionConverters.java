package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.ast.HasLabelsFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckReferenceProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckVariable;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedPropertyWithPropertyToken;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedPropertyWithoutPropertyToken;
import org.neo4j.cypher.internal.runtime.ast.ExpressionVariable;
import org.neo4j.cypher.internal.runtime.interpreted.CommandProjection;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverter;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Predicate;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.NestedPipeExpression;
import org.neo4j.cypher.internal.runtime.slotted.pipes.EmptyGroupingExpression$;
import org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression;
import org.neo4j.cypher.internal.runtime.slotted.pipes.SlottedGroupingExpression;
import org.neo4j.cypher.internal.runtime.slotted.pipes.SlottedGroupingExpression1;
import org.neo4j.cypher.internal.runtime.slotted.pipes.SlottedGroupingExpression2;
import org.neo4j.cypher.internal.runtime.slotted.pipes.SlottedGroupingExpression3;
import org.neo4j.cypher.internal.v4_0.expressions.EntityType;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.cypher.internal.v4_0.expressions.MultiRelationshipPathStep;
import org.neo4j.cypher.internal.v4_0.expressions.NodePathStep;
import org.neo4j.cypher.internal.v4_0.expressions.PathExpression;
import org.neo4j.cypher.internal.v4_0.expressions.PathStep;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.expressions.SingleRelationshipPathStep;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Predef.ArrowAssoc.;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SetLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable..colon.colon;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedExpressionConverters implements ExpressionConverter, Product, Serializable
{
    private final PhysicalPlan physicalPlan;

    public SlottedExpressionConverters( final PhysicalPlan physicalPlan )
    {
        this.physicalPlan = physicalPlan;
        Product.$init$( this );
    }

    public static Option<PhysicalPlan> unapply( final SlottedExpressionConverters x$0 )
    {
        return SlottedExpressionConverters$.MODULE$.unapply( var0 );
    }

    public static SlottedExpressionConverters apply( final PhysicalPlan physicalPlan )
    {
        return SlottedExpressionConverters$.MODULE$.apply( var0 );
    }

    public static Seq<Tuple3<String,Expression,Object>> orderGroupingKeyExpressions( final Iterable<Tuple2<String,Expression>> groupings,
            final Seq<Expression> orderToLeverage, final SlotConfiguration slots )
    {
        return SlottedExpressionConverters$.MODULE$.orderGroupingKeyExpressions( var0, var1, var2 );
    }

    public PhysicalPlan physicalPlan()
    {
        return this.physicalPlan;
    }

    public Option<CommandProjection> toCommandProjection( final int id, final Map<String,Expression> projections, final ExpressionConverters self )
    {
        SlotConfiguration slots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( id );
        Map projected = (Map) projections.withFilter( ( check$ifrefutable$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$toCommandProjection$1( check$ifrefutable$1 ) );
        } ).withFilter( ( x$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$toCommandProjection$2( slots, x$1 ) );
        } ).map( ( x$2 ) -> {
            if ( x$2 != null )
            {
                String k = (String) x$2._1();
                Expression v = (Expression) x$2._2();
                Tuple2 var4 = .MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                    BoxesRunTime.boxToInteger( slots.apply( k ).offset() ) ), self.toCommandExpression( id, v ));
                return var4;
            }
            else
            {
                throw new MatchError( x$2 );
            }
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
        return new Some( new SlottedCommandProjection( projected ) );
    }

    public Option<GroupingExpression> toGroupingExpression( final int id, final Map<String,Expression> projections, final Seq<Expression> orderToLeverage,
            final ExpressionConverters self )
    {
        SlotConfiguration slots = (SlotConfiguration) this.physicalPlan().slotConfigurations().apply( id );
        Seq orderedGroupings = (Seq) SlottedExpressionConverters$.MODULE$.orderGroupingKeyExpressions( projections, orderToLeverage, slots ).map( ( ex ) -> {
            return new Tuple3( slots.apply( (String) ex._1() ), self.toCommandExpression( id, (Expression) ex._2() ), ex._3() );
        }, scala.collection.Seq..MODULE$.canBuildFrom());
        boolean var8 = false;
        colon var9 = null;
        List var10 = orderedGroupings.toList();
        Some var5;
        if ( scala.collection.immutable.Nil..MODULE$.equals( var10 )){
        var5 = new Some( EmptyGroupingExpression$.MODULE$ );
    } else{
        if ( var10 instanceof colon )
        {
            var8 = true;
            var9 = (colon) var10;
            Tuple3 var11 = (Tuple3) var9.head();
            List var12 = var9.tl$access$1();
            if ( var11 != null )
            {
                Slot slot = (Slot) var11._1();
                org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression e =
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) var11._2();
                boolean ordered = BoxesRunTime.unboxToBoolean( var11._3() );
                if ( scala.collection.immutable.Nil..MODULE$.equals( var12 )){
                var5 = new Some( new SlottedGroupingExpression1( new SlotExpression( slot, e, ordered ) ) );
                return var5;
            }
            }
        }

        if ( var8 )
        {
            Tuple3 var16 = (Tuple3) var9.head();
            List var17 = var9.tl$access$1();
            if ( var16 != null )
            {
                Slot s1 = (Slot) var16._1();
                org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression e1 =
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) var16._2();
                boolean o1 = BoxesRunTime.unboxToBoolean( var16._3() );
                if ( var17 instanceof colon )
                {
                    colon var21 = (colon) var17;
                    Tuple3 var22 = (Tuple3) var21.head();
                    List var23 = var21.tl$access$1();
                    if ( var22 != null )
                    {
                        Slot s2 = (Slot) var22._1();
                        org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression e2 =
                                (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) var22._2();
                        boolean o2 = BoxesRunTime.unboxToBoolean( var22._3() );
                        if ( scala.collection.immutable.Nil..MODULE$.equals( var23 )){
                        var5 = new Some( new SlottedGroupingExpression2( new SlotExpression( s1, e1, o1 ), new SlotExpression( s2, e2, o2 ) ) );
                        return var5;
                    }
                    }
                }
            }
        }

        if ( var8 )
        {
            Tuple3 var27 = (Tuple3) var9.head();
            List var28 = var9.tl$access$1();
            if ( var27 != null )
            {
                Slot s1 = (Slot) var27._1();
                org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression e1 =
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) var27._2();
                boolean o1 = BoxesRunTime.unboxToBoolean( var27._3() );
                if ( var28 instanceof colon )
                {
                    colon var32 = (colon) var28;
                    Tuple3 var33 = (Tuple3) var32.head();
                    List var34 = var32.tl$access$1();
                    if ( var33 != null )
                    {
                        Slot s2 = (Slot) var33._1();
                        org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression e2 =
                                (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) var33._2();
                        boolean o2 = BoxesRunTime.unboxToBoolean( var33._3() );
                        if ( var34 instanceof colon )
                        {
                            colon var38 = (colon) var34;
                            Tuple3 var39 = (Tuple3) var38.head();
                            List var40 = var38.tl$access$1();
                            if ( var39 != null )
                            {
                                Slot s3 = (Slot) var39._1();
                                org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression e3 =
                                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) var39._2();
                                boolean o3 = BoxesRunTime.unboxToBoolean( var39._3() );
                                if ( scala.collection.immutable.Nil..MODULE$.equals( var40 )){
                                var5 = new Some( new SlottedGroupingExpression3( new SlotExpression( s1, e1, o1 ), new SlotExpression( s2, e2, o2 ),
                                        new SlotExpression( s3, e3, o3 ) ) );
                                return var5;
                            }
                            }
                        }
                    }
                }
            }
        }

        var5 = new Some( new SlottedGroupingExpression( (SlotExpression[]) ((TraversableOnce) orderedGroupings.map( ( t ) -> {
            return new SlotExpression( (Slot) t._1(), (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) t._2(),
                    BoxesRunTime.unboxToBoolean( t._3() ) );
        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply( SlotExpression.class ) )));
    }

        return var5;
    }

    public Option<org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression> toCommandExpression( final int id, final Expression expression,
            final ExpressionConverters self )
    {
        boolean var5 = false;
        SlottedCachedPropertyWithPropertyToken var6 = null;
        boolean var7 = false;
        SlottedCachedPropertyWithoutPropertyToken var8 = null;
        Object var4;
        if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.NodeFromSlot )
        {
            org.neo4j.cypher.internal.physicalplanning.ast.NodeFromSlot var10 = (org.neo4j.cypher.internal.physicalplanning.ast.NodeFromSlot) expression;
            int offset = var10.offset();
            var4 = new Some( new NodeFromSlot( offset ) );
        }
        else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.RelationshipFromSlot )
        {
            org.neo4j.cypher.internal.physicalplanning.ast.RelationshipFromSlot var12 =
                    (org.neo4j.cypher.internal.physicalplanning.ast.RelationshipFromSlot) expression;
            int offset = var12.offset();
            var4 = new Some( new RelationshipFromSlot( offset ) );
        }
        else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.ReferenceFromSlot )
        {
            org.neo4j.cypher.internal.physicalplanning.ast.ReferenceFromSlot var14 =
                    (org.neo4j.cypher.internal.physicalplanning.ast.ReferenceFromSlot) expression;
            int offset = var14.offset();
            var4 = new Some( new ReferenceFromSlot( offset ) );
        }
        else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.NodeProperty )
        {
            org.neo4j.cypher.internal.physicalplanning.ast.NodeProperty var16 = (org.neo4j.cypher.internal.physicalplanning.ast.NodeProperty) expression;
            int offset = var16.offset();
            int token = var16.propToken();
            var4 = new Some( new NodeProperty( offset, token ) );
        }
        else
        {
            if ( expression instanceof SlottedCachedPropertyWithPropertyToken )
            {
                var5 = true;
                var6 = (SlottedCachedPropertyWithPropertyToken) expression;
                int offset = var6.offset();
                boolean offsetIsForLongSlot = var6.offsetIsForLongSlot();
                int token = var6.propToken();
                int cachedPropertyOffset = var6.cachedPropertyOffset();
                EntityType var23 = var6.entityType();
                if ( org.neo4j.cypher.internal.v4_0.expressions.NODE_TYPE..MODULE$.equals( var23 )){
                var4 = new Some( new SlottedCachedNodeProperty( offset, offsetIsForLongSlot, token, cachedPropertyOffset ) );
                return (Option) var4;
            }
            }

            if ( var5 )
            {
                int offset = var6.offset();
                boolean offsetIsForLongSlot = var6.offsetIsForLongSlot();
                int token = var6.propToken();
                int cachedPropertyOffset = var6.cachedPropertyOffset();
                EntityType var28 = var6.entityType();
                if ( org.neo4j.cypher.internal.v4_0.expressions.RELATIONSHIP_TYPE..MODULE$.equals( var28 )){
                var4 = new Some( new SlottedCachedRelationshipProperty( offset, offsetIsForLongSlot, token, cachedPropertyOffset ) );
                return (Option) var4;
            }
            }

            if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.RelationshipProperty )
            {
                org.neo4j.cypher.internal.physicalplanning.ast.RelationshipProperty var29 =
                        (org.neo4j.cypher.internal.physicalplanning.ast.RelationshipProperty) expression;
                int offset = var29.offset();
                int token = var29.propToken();
                var4 = new Some( new RelationshipProperty( offset, token ) );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.IdFromSlot )
            {
                org.neo4j.cypher.internal.physicalplanning.ast.IdFromSlot var32 = (org.neo4j.cypher.internal.physicalplanning.ast.IdFromSlot) expression;
                int offset = var32.offset();
                var4 = new Some( new IdFromSlot( offset ) );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.LabelsFromSlot )
            {
                org.neo4j.cypher.internal.physicalplanning.ast.LabelsFromSlot var34 =
                        (org.neo4j.cypher.internal.physicalplanning.ast.LabelsFromSlot) expression;
                int offset = var34.offset();
                var4 = new Some( new LabelsFromSlot( offset ) );
            }
            else if ( expression instanceof HasLabelsFromSlot )
            {
                HasLabelsFromSlot var36 = (HasLabelsFromSlot) expression;
                var4 = new Some( this.hasLabelsFromSlot( id, var36, self ) );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.RelationshipTypeFromSlot )
            {
                org.neo4j.cypher.internal.physicalplanning.ast.RelationshipTypeFromSlot var37 =
                        (org.neo4j.cypher.internal.physicalplanning.ast.RelationshipTypeFromSlot) expression;
                int offset = var37.offset();
                var4 = new Some( new RelationshipTypeFromSlot( offset ) );
            }
            else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyLate )
            {
                org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyLate var39 =
                        (org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyLate) expression;
                int offset = var39.offset();
                String propKey = var39.propKey();
                var4 = new Some( new NodePropertyLate( offset, propKey ) );
            }
            else
            {
                if ( expression instanceof SlottedCachedPropertyWithoutPropertyToken )
                {
                    var7 = true;
                    var8 = (SlottedCachedPropertyWithoutPropertyToken) expression;
                    int offset = var8.offset();
                    boolean offsetIsForLongSlot = var8.offsetIsForLongSlot();
                    String propertyKey = var8.propKey();
                    int cachedPropertyOffset = var8.cachedPropertyOffset();
                    EntityType var46 = var8.entityType();
                    if ( org.neo4j.cypher.internal.v4_0.expressions.NODE_TYPE..MODULE$.equals( var46 )){
                    var4 = new Some( new SlottedCachedNodePropertyLate( offset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset ) );
                    return (Option) var4;
                }
                }

                if ( var7 )
                {
                    int offset = var8.offset();
                    boolean offsetIsForLongSlot = var8.offsetIsForLongSlot();
                    String propertyKey = var8.propKey();
                    int cachedPropertyOffset = var8.cachedPropertyOffset();
                    EntityType var51 = var8.entityType();
                    if ( org.neo4j.cypher.internal.v4_0.expressions.RELATIONSHIP_TYPE..MODULE$.equals( var51 )){
                    var4 = new Some( new SlottedCachedRelationshipPropertyLate( offset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset ) );
                    return (Option) var4;
                }
                }

                if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyLate )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyLate var52 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyLate) expression;
                    int offset = var52.offset();
                    String propKey = var52.propKey();
                    var4 = new Some( new RelationshipPropertyLate( offset, propKey ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.PrimitiveEquals )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.PrimitiveEquals var55 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.PrimitiveEquals) expression;
                    Expression a = var55.a();
                    Expression b = var55.b();
                    org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression lhs = self.toCommandExpression( id, a );
                    org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression rhs = self.toCommandExpression( id, b );
                    var4 = new Some( new PrimitiveEquals( lhs, rhs ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.GetDegreePrimitive )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.GetDegreePrimitive var60 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.GetDegreePrimitive) expression;
                    int offset = var60.offset();
                    Option typ = var60.typ();
                    SemanticDirection direction = var60.direction();
                    var4 = new Some( new GetDegreePrimitive( offset, typ, direction ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExists )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExists var64 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExists) expression;
                    int offset = var64.offset();
                    int token = var64.propToken();
                    var4 = new Some( new NodePropertyExists( offset, token ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExistsLate )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExistsLate var67 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExistsLate) expression;
                    int offset = var67.offset();
                    String token = var67.propKey();
                    var4 = new Some( new NodePropertyExistsLate( offset, token ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExists )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExists var70 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExists) expression;
                    int offset = var70.offset();
                    int token = var70.propToken();
                    var4 = new Some( new RelationshipPropertyExists( offset, token ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExistsLate )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExistsLate var73 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExistsLate) expression;
                    int offset = var73.offset();
                    String token = var73.propKey();
                    var4 = new Some( new RelationshipPropertyExistsLate( offset, token ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.NullCheck )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.NullCheck var76 = (org.neo4j.cypher.internal.physicalplanning.ast.NullCheck) expression;
                    int offset = var76.offset();
                    Expression inner = var76.inner();
                    org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression a = self.toCommandExpression( id, inner );
                    var4 = new Some( new NullCheck( offset, a ) );
                }
                else if ( expression instanceof NullCheckVariable )
                {
                    NullCheckVariable var80 = (NullCheckVariable) expression;
                    int offset = var80.offset();
                    LogicalVariable inner = var80.inner();
                    org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression a = self.toCommandExpression( id, inner );
                    var4 = new Some( new NullCheck( offset, a ) );
                }
                else if ( expression instanceof NullCheckProperty )
                {
                    NullCheckProperty var84 = (NullCheckProperty) expression;
                    int offset = var84.offset();
                    LogicalProperty inner = var84.inner();
                    org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression a = self.toCommandExpression( id, inner );
                    var4 = new Some( new NullCheck( offset, a ) );
                }
                else if ( expression instanceof NullCheckReferenceProperty )
                {
                    NullCheckReferenceProperty var88 = (NullCheckReferenceProperty) expression;
                    int offset = var88.offset();
                    LogicalProperty inner = var88.inner();
                    org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression a = self.toCommandExpression( id, inner );
                    var4 = new Some( new NullCheckReference( offset, a ) );
                }
                else if ( expression instanceof PathExpression )
                {
                    PathExpression var92 = (PathExpression) expression;
                    var4 = new Some( this.toCommandProjectedPath( id, var92, self ) );
                }
                else if ( expression instanceof org.neo4j.cypher.internal.physicalplanning.ast.IsPrimitiveNull )
                {
                    org.neo4j.cypher.internal.physicalplanning.ast.IsPrimitiveNull var93 =
                            (org.neo4j.cypher.internal.physicalplanning.ast.IsPrimitiveNull) expression;
                    int offset = var93.offset();
                    var4 = new Some( new IsPrimitiveNull( offset ) );
                }
                else if ( expression instanceof ExpressionVariable )
                {
                    ExpressionVariable var95 = (ExpressionVariable) expression;
                    var4 = new Some(
                            new org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.ExpressionVariable( var95.offset(), var95.name() ) );
                }
                else if ( expression instanceof NestedPipeExpression )
                {
                    NestedPipeExpression var96 = (NestedPipeExpression) expression;
                    var4 = new Some( new NestedPipeSlottedExpression( var96.pipe(), self.toCommandExpression( id, var96.projection() ),
                            (SlotConfiguration) this.physicalPlan().nestedPlanArgumentConfigurations().apply( var96.pipe().id() ),
                            (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.ExpressionVariable[]) ((TraversableOnce) var96.availableExpressionVariables().map(
                                    ( e ) -> {
                                        return org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.ExpressionVariable..MODULE$.of( e );
                                    }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toArray( scala.reflect.ClassTag..MODULE$.apply(
                            org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.ExpressionVariable.class ) )));
                }
                else
                {
                    var4 = scala.None..MODULE$;
                }
            }
        }

        return (Option) var4;
    }

    private Predicate hasLabelsFromSlot( final int id, final HasLabelsFromSlot e, final ExpressionConverters self )
    {
        Seq preds = (Seq) ((TraversableLike) e.resolvedLabelTokens().map( ( labelId ) -> {
            return $anonfun$hasLabelsFromSlot$1( e, BoxesRunTime.unboxToInt( labelId ) );
        }, scala.collection.Seq..MODULE$.canBuildFrom())).$plus$plus( (GenTraversableOnce) e.lateLabels().map( ( labelName ) -> {
        return new HasLabelFromSlotLate( e.offset(), labelName );
    }, scala.collection.Seq..MODULE$.canBuildFrom() ), scala.collection.Seq..MODULE$.canBuildFrom());
        return org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Ands..MODULE$.apply( preds );
    }

    public SlottedProjectedPath toCommandProjectedPath( final int id, final PathExpression e, final ExpressionConverters self )
    {
        SlottedProjectedPath.Projector projector = this.project$1( e.step(), id, self );
        Set dependencies = (Set) ((SetLike) e.step().dependencies().flatMap( ( x$3 ) -> {
            return x$3.dependencies();
        }, scala.collection.immutable.Set..MODULE$.canBuildFrom())).map( ( x$4 ) -> {
        return x$4.name();
    }, scala.collection.immutable.Set..MODULE$.canBuildFrom());
        return new SlottedProjectedPath( dependencies, projector );
    }

    public SlottedExpressionConverters copy( final PhysicalPlan physicalPlan )
    {
        return new SlottedExpressionConverters( physicalPlan );
    }

    public PhysicalPlan copy$default$1()
    {
        return this.physicalPlan();
    }

    public String productPrefix()
    {
        return "SlottedExpressionConverters";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.physicalPlan();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedExpressionConverters;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public String toString()
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof SlottedExpressionConverters )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        SlottedExpressionConverters var4 = (SlottedExpressionConverters) x$1;
                        PhysicalPlan var10000 = this.physicalPlan();
                        PhysicalPlan var5 = var4.physicalPlan();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }

    private final SlottedProjectedPath.Projector project$1( final PathStep pathStep, final int id$3, final ExpressionConverters self$3 )
    {
        boolean var5 = false;
        SingleRelationshipPathStep var6 = null;
        boolean var7 = false;
        MultiRelationshipPathStep var8 = null;
        Object var4;
        if ( pathStep instanceof NodePathStep )
        {
            NodePathStep var10 = (NodePathStep) pathStep;
            LogicalVariable nodeExpression = var10.node();
            PathStep next = var10.next();
            var4 = new SlottedProjectedPath.singleNodeProjector(
                    (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, nodeExpression,
                            self$3 ).get(), this.project$1( next, id$3, self$3 ) );
        }
        else
        {
            if ( pathStep instanceof SingleRelationshipPathStep )
            {
                var5 = true;
                var6 = (SingleRelationshipPathStep) pathStep;
                LogicalVariable relExpression = var6.rel();
                Option var14 = var6.toNode();
                PathStep next = var6.next();
                if ( var14 instanceof Some )
                {
                    Some var16 = (Some) var14;
                    LogicalVariable targetNodeExpression = (LogicalVariable) var16.value();
                    var4 = new SlottedProjectedPath.singleRelationshipWithKnownTargetProjector(
                            (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                    self$3 ).get(),
                            (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3,
                                    targetNodeExpression, self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                    return (SlottedProjectedPath.Projector) var4;
                }
            }

            if ( var5 )
            {
                LogicalVariable relExpression = var6.rel();
                SemanticDirection var19 = var6.direction();
                PathStep next = var6.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( var19 )){
                var4 = new SlottedProjectedPath.singleIncomingRelationshipProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( var5 )
            {
                LogicalVariable relExpression = var6.rel();
                SemanticDirection var22 = var6.direction();
                PathStep next = var6.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( var22 )){
                var4 = new SlottedProjectedPath.singleOutgoingRelationshipProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( var5 )
            {
                LogicalVariable relExpression = var6.rel();
                SemanticDirection var25 = var6.direction();
                PathStep next = var6.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( var25 )){
                var4 = new SlottedProjectedPath.singleUndirectedRelationshipProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( pathStep instanceof MultiRelationshipPathStep )
            {
                var7 = true;
                var8 = (MultiRelationshipPathStep) pathStep;
                LogicalVariable relExpression = var8.rel();
                SemanticDirection var28 = var8.direction();
                Option var29 = var8.toNode();
                PathStep next = var8.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( var28 ) && var29 instanceof Some){
                Some var31 = (Some) var29;
                LogicalVariable targetNodeExpression = (LogicalVariable) var31.value();
                var4 = new SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(),
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, targetNodeExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( var7 )
            {
                LogicalVariable relExpression = var8.rel();
                SemanticDirection var34 = var8.direction();
                PathStep next = var8.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.INCOMING..MODULE$.equals( var34 )){
                var4 = new SlottedProjectedPath.multiIncomingRelationshipProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( var7 )
            {
                LogicalVariable relExpression = var8.rel();
                SemanticDirection var37 = var8.direction();
                Option var38 = var8.toNode();
                PathStep next = var8.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( var37 ) && var38 instanceof Some){
                Some var40 = (Some) var38;
                LogicalVariable targetNodeExpression = (LogicalVariable) var40.value();
                var4 = new SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(),
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, targetNodeExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( var7 )
            {
                LogicalVariable relExpression = var8.rel();
                SemanticDirection var43 = var8.direction();
                PathStep next = var8.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.OUTGOING..MODULE$.equals( var43 )){
                var4 = new SlottedProjectedPath.multiOutgoingRelationshipProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( var7 )
            {
                LogicalVariable relExpression = var8.rel();
                SemanticDirection var46 = var8.direction();
                Option var47 = var8.toNode();
                PathStep next = var8.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( var46 ) && var47 instanceof Some){
                Some var49 = (Some) var47;
                LogicalVariable targetNodeExpression = (LogicalVariable) var49.value();
                var4 = new SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(),
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, targetNodeExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( var7 )
            {
                LogicalVariable relExpression = var8.rel();
                SemanticDirection var52 = var8.direction();
                PathStep next = var8.next();
                if ( org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection.BOTH..MODULE$.equals( var52 )){
                var4 = new SlottedProjectedPath.multiUndirectedRelationshipProjector(
                        (org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) this.toCommandExpression( id$3, relExpression,
                                self$3 ).get(), this.project$1( next, id$3, self$3 ) );
                return (SlottedProjectedPath.Projector) var4;
            }
            }

            if ( !org.neo4j.cypher.internal.v4_0.expressions.NilPathStep..MODULE$.equals( pathStep )){
            throw new MatchError( pathStep );
        }

            var4 = SlottedProjectedPath.nilProjector$.MODULE$;
        }

        return (SlottedProjectedPath.Projector) var4;
    }
}
