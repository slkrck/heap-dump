package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledGroupingExpression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class CompileWrappingDistinctGroupingExpression$
        extends AbstractFunction2<CompiledGroupingExpression,Object,CompileWrappingDistinctGroupingExpression> implements Serializable
{
    public static CompileWrappingDistinctGroupingExpression$ MODULE$;

    static
    {
        new CompileWrappingDistinctGroupingExpression$();
    }

    private CompileWrappingDistinctGroupingExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CompileWrappingDistinctGroupingExpression";
    }

    public CompileWrappingDistinctGroupingExpression apply( final CompiledGroupingExpression grouping, final boolean isEmpty )
    {
        return new CompileWrappingDistinctGroupingExpression( grouping, isEmpty );
    }

    public Option<Tuple2<CompiledGroupingExpression,Object>> unapply( final CompileWrappingDistinctGroupingExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.grouping(), BoxesRunTime.boxToBoolean( x$0.isEmpty() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
