package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Function1;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SlottedExpression
{
    static void $init$( final SlottedExpression $this )
    {
    }

    default Expression rewrite( final Function1<Expression,Expression> f )
    {
        return (Expression) f.apply( this );
    }

    default Seq<Expression> arguments()
    {
        return (Seq).MODULE$.empty();
    }
}
