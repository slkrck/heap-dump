package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class IsPrimitiveNull$ extends AbstractFunction1<Object,IsPrimitiveNull> implements Serializable
{
    public static IsPrimitiveNull$ MODULE$;

    static
    {
        new IsPrimitiveNull$();
    }

    private IsPrimitiveNull$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "IsPrimitiveNull";
    }

    public IsPrimitiveNull apply( final int offset )
    {
        return new IsPrimitiveNull( offset );
    }

    public Option<Object> unapply( final IsPrimitiveNull x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.offset() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
