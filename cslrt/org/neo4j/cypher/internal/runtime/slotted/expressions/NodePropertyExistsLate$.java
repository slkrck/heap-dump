package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class NodePropertyExistsLate$ extends AbstractFunction2<Object,String,NodePropertyExistsLate> implements Serializable
{
    public static NodePropertyExistsLate$ MODULE$;

    static
    {
        new NodePropertyExistsLate$();
    }

    private NodePropertyExistsLate$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodePropertyExistsLate";
    }

    public NodePropertyExistsLate apply( final int offset, final String propKey )
    {
        return new NodePropertyExistsLate( offset, propKey );
    }

    public Option<Tuple2<Object,String>> unapply( final NodePropertyExistsLate x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.propKey() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
