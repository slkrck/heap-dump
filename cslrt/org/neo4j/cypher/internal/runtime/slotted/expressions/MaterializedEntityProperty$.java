package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.values.KeyToken;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class MaterializedEntityProperty$ extends AbstractFunction2<Expression,KeyToken,MaterializedEntityProperty> implements Serializable
{
    public static MaterializedEntityProperty$ MODULE$;

    static
    {
        new MaterializedEntityProperty$();
    }

    private MaterializedEntityProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MaterializedEntityProperty";
    }

    public MaterializedEntityProperty apply( final Expression mapExpr, final KeyToken propertyKey )
    {
        return new MaterializedEntityProperty( mapExpr, propertyKey );
    }

    public Option<Tuple2<Expression,KeyToken>> unapply( final MaterializedEntityProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.mapExpr(), x$0.propertyKey() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
