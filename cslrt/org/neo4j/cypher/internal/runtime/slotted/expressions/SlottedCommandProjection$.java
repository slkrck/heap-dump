package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction1;

public final class SlottedCommandProjection$ extends AbstractFunction1<Map<Object,Expression>,SlottedCommandProjection> implements Serializable
{
    public static SlottedCommandProjection$ MODULE$;

    static
    {
        new SlottedCommandProjection$();
    }

    private SlottedCommandProjection$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedCommandProjection";
    }

    public SlottedCommandProjection apply( final Map<Object,Expression> introducedExpressions )
    {
        return new SlottedCommandProjection( introducedExpressions );
    }

    public Option<Map<Object,Expression>> unapply( final SlottedCommandProjection x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.introducedExpressions() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
