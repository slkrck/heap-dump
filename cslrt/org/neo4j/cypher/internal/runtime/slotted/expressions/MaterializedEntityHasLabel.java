package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.IsNoValue.;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Predicate;
import org.neo4j.cypher.internal.runtime.interpreted.commands.values.KeyToken;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.NodeValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class MaterializedEntityHasLabel extends Predicate implements Product, Serializable
{
    private final Expression entity;
    private final KeyToken label;

    public MaterializedEntityHasLabel( final Expression entity, final KeyToken label )
    {
        this.entity = entity;
        this.label = label;
        Product.$init$( this );
    }

    public static Option<Tuple2<Expression,KeyToken>> unapply( final MaterializedEntityHasLabel x$0 )
    {
        return MaterializedEntityHasLabel$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple2<Expression,KeyToken>,MaterializedEntityHasLabel> tupled()
    {
        return MaterializedEntityHasLabel$.MODULE$.tupled();
    }

    public static Function1<Expression,Function1<KeyToken,MaterializedEntityHasLabel>> curried()
    {
        return MaterializedEntityHasLabel$.MODULE$.curried();
    }

    public Expression entity()
    {
        return this.entity;
    }

    public KeyToken label()
    {
        return this.label;
    }

    public Option<Object> isMatch( final ExecutionContext m, final QueryState state )
    {
        AnyValue var4 = this.entity().apply( m, state );
        Object var3;
        if (.MODULE$.unapply( var4 )){
        var3 = scala.None..MODULE$;
    } else{
        NodeValue node = (NodeValue) org.neo4j.cypher.internal.runtime.CastSupport..
        MODULE$.castOrFail( var4, scala.reflect.ClassTag..MODULE$.apply( NodeValue.class ));

        for ( int i = 0; i < node.labels().length(); ++i )
        {
            if ( node.labels().stringValue( i ).equals( this.label().name() ) )
            {
                return new Some( BoxesRunTime.boxToBoolean( true ) );
            }
        }

        var3 = new Some( BoxesRunTime.boxToBoolean( false ) );
    }

        return (Option) var3;
    }

    public String toString()
    {
        return (new StringBuilder( 1 )).append( this.entity() ).append( ":" ).append( this.label().name() ).toString();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return (Expression) f.apply( new MaterializedEntityHasLabel( this.entity().rewrite( f ),
                (KeyToken) this.label().typedRewrite( f, scala.reflect.ClassTag..MODULE$.apply( KeyToken.class ) ) ));
    }

    public Seq<Expression> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.label(), this.entity()}) ));
    }

    public Seq<Expression> arguments()
    {
        return (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.entity()}) ));
    }

    public boolean containsIsNull()
    {
        return false;
    }

    public MaterializedEntityHasLabel copy( final Expression entity, final KeyToken label )
    {
        return new MaterializedEntityHasLabel( entity, label );
    }

    public Expression copy$default$1()
    {
        return this.entity();
    }

    public KeyToken copy$default$2()
    {
        return this.label();
    }

    public String productPrefix()
    {
        return "MaterializedEntityHasLabel";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.entity();
            break;
        case 1:
            var10000 = this.label();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MaterializedEntityHasLabel;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof MaterializedEntityHasLabel )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            MaterializedEntityHasLabel var4 = (MaterializedEntityHasLabel) x$1;
                            Expression var10000 = this.entity();
                            Expression var5 = var4.entity();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            KeyToken var7 = this.label();
                            KeyToken var6 = var4.label();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
