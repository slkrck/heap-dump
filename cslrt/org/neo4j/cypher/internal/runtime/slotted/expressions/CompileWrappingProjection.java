package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledProjection;
import org.neo4j.cypher.internal.runtime.interpreted.CommandProjection;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CompileWrappingProjection implements CommandProjection, Product, Serializable
{
    private final CompiledProjection projection;
    private final boolean isEmpty;

    public CompileWrappingProjection( final CompiledProjection projection, final boolean isEmpty )
    {
        this.projection = projection;
        this.isEmpty = isEmpty;
        Product.$init$( this );
    }

    public static Option<Tuple2<CompiledProjection,Object>> unapply( final CompileWrappingProjection x$0 )
    {
        return CompileWrappingProjection$.MODULE$.unapply( var0 );
    }

    public static CompileWrappingProjection apply( final CompiledProjection projection, final boolean isEmpty )
    {
        return CompileWrappingProjection$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CompiledProjection,Object>,CompileWrappingProjection> tupled()
    {
        return CompileWrappingProjection$.MODULE$.tupled();
    }

    public static Function1<CompiledProjection,Function1<Object,CompileWrappingProjection>> curried()
    {
        return CompileWrappingProjection$.MODULE$.curried();
    }

    public CompiledProjection projection()
    {
        return this.projection;
    }

    public boolean isEmpty()
    {
        return this.isEmpty;
    }

    public void registerOwningPipe( final Pipe pipe )
    {
    }

    public void project( final ExecutionContext ctx, final QueryState state )
    {
        this.projection().project( ctx, state.query(), CompiledExpressionConverter$.MODULE$.parametersOrFail( state ), state.cursors(),
                state.expressionVariables() );
    }

    public CompileWrappingProjection copy( final CompiledProjection projection, final boolean isEmpty )
    {
        return new CompileWrappingProjection( projection, isEmpty );
    }

    public CompiledProjection copy$default$1()
    {
        return this.projection();
    }

    public boolean copy$default$2()
    {
        return this.isEmpty();
    }

    public String productPrefix()
    {
        return "CompileWrappingProjection";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.projection();
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.isEmpty() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CompileWrappingProjection;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.projection() ) );
        var1 = Statics.mix( var1, this.isEmpty() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 2 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof CompileWrappingProjection )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        label37:
                        {
                            CompileWrappingProjection var4 = (CompileWrappingProjection) x$1;
                            CompiledProjection var10000 = this.projection();
                            CompiledProjection var5 = var4.projection();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label37;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label37;
                            }

                            if ( this.isEmpty() == var4.isEmpty() && var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label38;
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
