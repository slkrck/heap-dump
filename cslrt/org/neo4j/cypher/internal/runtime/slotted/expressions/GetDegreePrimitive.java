package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class GetDegreePrimitive extends Expression implements SlottedExpression, Product, Serializable
{
    private final int offset;
    private final Option<String> typ;
    private final SemanticDirection direction;

    public GetDegreePrimitive( final int offset, final Option<String> typ, final SemanticDirection direction )
    {
        this.offset = offset;
        this.typ = typ;
        this.direction = direction;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple3<Object,Option<String>,SemanticDirection>> unapply( final GetDegreePrimitive x$0 )
    {
        return GetDegreePrimitive$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple3<Object,Option<String>,SemanticDirection>,GetDegreePrimitive> tupled()
    {
        return GetDegreePrimitive$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Option<String>,Function1<SemanticDirection,GetDegreePrimitive>>> curried()
    {
        return GetDegreePrimitive$.MODULE$.curried();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public Option<String> typ()
    {
        return this.typ;
    }

    public SemanticDirection direction()
    {
        return this.direction;
    }

    public AnyValue apply( final ExecutionContext ctx, final QueryState state )
    {
        Option var5 = this.typ();
        Object var3;
        if (.MODULE$.equals( var5 )){
        var3 = Values.longValue( (long) state.query().nodeGetDegree( ctx.getLongAt( this.offset() ), this.direction(), state.cursors().nodeCursor() ) );
    } else{
        if ( !(var5 instanceof Some) )
        {
            throw new MatchError( var5 );
        }

        Some var6 = (Some) var5;
        String t = (String) var6.value();
        Option var8 = state.query().getOptRelTypeId( t );
        Object var4;
        if (.MODULE$.equals( var8 )){
            var4 = Values.ZERO_INT;
        } else{
            if ( !(var8 instanceof Some) )
            {
                throw new MatchError( var8 );
            }

            Some var9 = (Some) var8;
            int relTypeId = BoxesRunTime.unboxToInt( var9.value() );
            var4 = Values.longValue(
                    (long) state.query().nodeGetDegree( ctx.getLongAt( this.offset() ), this.direction(), relTypeId, state.cursors().nodeCursor() ) );
        }

        var3 = var4;
    }

        return (AnyValue) var3;
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public GetDegreePrimitive copy( final int offset, final Option<String> typ, final SemanticDirection direction )
    {
        return new GetDegreePrimitive( offset, typ, direction );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public Option<String> copy$default$2()
    {
        return this.typ();
    }

    public SemanticDirection copy$default$3()
    {
        return this.direction();
    }

    public String productPrefix()
    {
        return "GetDegreePrimitive";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = this.typ();
            break;
        case 2:
            var10000 = this.direction();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof GetDegreePrimitive;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.typ() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.direction() ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof GetDegreePrimitive )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        GetDegreePrimitive var4 = (GetDegreePrimitive) x$1;
                        if ( this.offset() == var4.offset() )
                        {
                            label56:
                            {
                                Option var10000 = this.typ();
                                Option var5 = var4.typ();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label56;
                                }

                                SemanticDirection var7 = this.direction();
                                SemanticDirection var6 = var4.direction();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label56;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label47;
                                }
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label65;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
