package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledGroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class CompileWrappingDistinctGroupingExpression implements GroupingExpression, Product, Serializable
{
    private final CompiledGroupingExpression grouping;
    private final boolean isEmpty;

    public CompileWrappingDistinctGroupingExpression( final CompiledGroupingExpression grouping, final boolean isEmpty )
    {
        this.grouping = grouping;
        this.isEmpty = isEmpty;
        Product.$init$( this );
    }

    public static Option<Tuple2<CompiledGroupingExpression,Object>> unapply( final CompileWrappingDistinctGroupingExpression x$0 )
    {
        return CompileWrappingDistinctGroupingExpression$.MODULE$.unapply( var0 );
    }

    public static CompileWrappingDistinctGroupingExpression apply( final CompiledGroupingExpression grouping, final boolean isEmpty )
    {
        return CompileWrappingDistinctGroupingExpression$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<CompiledGroupingExpression,Object>,CompileWrappingDistinctGroupingExpression> tupled()
    {
        return CompileWrappingDistinctGroupingExpression$.MODULE$.tupled();
    }

    public static Function1<CompiledGroupingExpression,Function1<Object,CompileWrappingDistinctGroupingExpression>> curried()
    {
        return CompileWrappingDistinctGroupingExpression$.MODULE$.curried();
    }

    public CompiledGroupingExpression grouping()
    {
        return this.grouping;
    }

    public boolean isEmpty()
    {
        return this.isEmpty;
    }

    public void registerOwningPipe( final Pipe pipe )
    {
    }

    public AnyValue computeGroupingKey( final ExecutionContext context, final QueryState state )
    {
        return this.grouping().computeGroupingKey( context, state.query(), CompiledExpressionConverter$.MODULE$.parametersOrFail( state ), state.cursors(),
                state.expressionVariables() );
    }

    public AnyValue computeOrderedGroupingKey( final AnyValue groupingKey )
    {
        throw new IllegalStateException( "Compiled expressions do not support this yet." );
    }

    public AnyValue getGroupingKey( final ExecutionContext context )
    {
        return this.grouping().getGroupingKey( context );
    }

    public void project( final ExecutionContext context, final AnyValue groupingKey )
    {
        this.grouping().projectGroupingKey( context, groupingKey );
    }

    public CompileWrappingDistinctGroupingExpression copy( final CompiledGroupingExpression grouping, final boolean isEmpty )
    {
        return new CompileWrappingDistinctGroupingExpression( grouping, isEmpty );
    }

    public CompiledGroupingExpression copy$default$1()
    {
        return this.grouping();
    }

    public boolean copy$default$2()
    {
        return this.isEmpty();
    }

    public String productPrefix()
    {
        return "CompileWrappingDistinctGroupingExpression";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.grouping();
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.isEmpty() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof CompileWrappingDistinctGroupingExpression;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.grouping() ) );
        var1 = Statics.mix( var1, this.isEmpty() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 2 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof CompileWrappingDistinctGroupingExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        label37:
                        {
                            CompileWrappingDistinctGroupingExpression var4 = (CompileWrappingDistinctGroupingExpression) x$1;
                            CompiledGroupingExpression var10000 = this.grouping();
                            CompiledGroupingExpression var5 = var4.grouping();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label37;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label37;
                            }

                            if ( this.isEmpty() == var4.isEmpty() && var4.canEqual( this ) )
                            {
                                var6 = true;
                                break label38;
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
