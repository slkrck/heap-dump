package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class MaterializedEntityKeysFunction$ extends AbstractFunction1<Expression,MaterializedEntityKeysFunction> implements Serializable
{
    public static MaterializedEntityKeysFunction$ MODULE$;

    static
    {
        new MaterializedEntityKeysFunction$();
    }

    private MaterializedEntityKeysFunction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MaterializedEntityKeysFunction";
    }

    public MaterializedEntityKeysFunction apply( final Expression expr )
    {
        return new MaterializedEntityKeysFunction( expr );
    }

    public Option<Expression> unapply( final MaterializedEntityKeysFunction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.expr() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
