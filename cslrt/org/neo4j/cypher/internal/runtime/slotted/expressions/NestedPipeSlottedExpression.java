package org.neo4j.cypher.internal.runtime.slotted.expressions;

import java.util.ArrayList;

import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.ExpressionVariable;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.immutable.List;
import scala.collection.immutable..colon.colon;
import scala.collection.immutable.Nil.;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class NestedPipeSlottedExpression extends Expression implements Product, Serializable
{
    private final Pipe pipe;
    private final Expression inner;
    private final SlotConfiguration slots;
    private final ExpressionVariable[] availableExpressionVariables;
    private final int[] expVarSlotsInNestedPlan;

    public NestedPipeSlottedExpression( final Pipe pipe, final Expression inner, final SlotConfiguration slots,
            final ExpressionVariable[] availableExpressionVariables )
    {
        this.pipe = pipe;
        this.inner = inner;
        this.slots = slots;
        this.availableExpressionVariables = availableExpressionVariables;
        Product.$init$( this );
        this.expVarSlotsInNestedPlan = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) availableExpressionVariables ))).map( ( ev ) -> {
        return BoxesRunTime.boxToInteger( $anonfun$expVarSlotsInNestedPlan$1( this, ev ) );
    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
    }

    public static Option<Tuple4<Pipe,Expression,SlotConfiguration,ExpressionVariable[]>> unapply( final NestedPipeSlottedExpression x$0 )
    {
        return NestedPipeSlottedExpression$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple4<Pipe,Expression,SlotConfiguration,ExpressionVariable[]>,NestedPipeSlottedExpression> tupled()
    {
        return NestedPipeSlottedExpression$.MODULE$.tupled();
    }

    public static Function1<Pipe,Function1<Expression,Function1<SlotConfiguration,Function1<ExpressionVariable[],NestedPipeSlottedExpression>>>> curried()
    {
        return NestedPipeSlottedExpression$.MODULE$.curried();
    }

    public Pipe pipe()
    {
        return this.pipe;
    }

    public Expression inner()
    {
        return this.inner;
    }

    public SlotConfiguration slots()
    {
        return this.slots;
    }

    public ExpressionVariable[] availableExpressionVariables()
    {
        return this.availableExpressionVariables;
    }

    private int[] expVarSlotsInNestedPlan()
    {
        return this.expVarSlotsInNestedPlan;
    }

    public AnyValue apply( final ExecutionContext ctx, final QueryState state )
    {
        SlottedExecutionContext initialContext = this.createInitialContext( ctx, state );
        QueryState innerState = state.withInitialContext( initialContext ).withDecorator( state.decorator().innerDecorator( this.owningPipe() ) );
        Iterator results = this.pipe().createResults( innerState );
        ArrayList all = new ArrayList();

        while ( results.hasNext() )
        {
            all.add( this.inner().apply( (ExecutionContext) results.next(), state ) );
        }

        return VirtualValues.fromList( all );
    }

    private SlottedExecutionContext createInitialContext( final ExecutionContext ctx, final QueryState state )
    {
        SlottedExecutionContext initialContext = new SlottedExecutionContext( this.slots() );
        initialContext.copyFrom( ctx, this.slots().numberOfLongs(), this.slots().numberOfReferences() - this.expVarSlotsInNestedPlan().length );

        for ( int i = 0; i < this.expVarSlotsInNestedPlan().length; ++i )
        {
            ExpressionVariable expVar = this.availableExpressionVariables()[i];
            initialContext.setRefAt( this.expVarSlotsInNestedPlan()[i], state.expressionVariables()[expVar.offset()] );
        }

        return initialContext;
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return (Expression) f.apply(
                new NestedPipeSlottedExpression( this.pipe(), this.inner().rewrite( f ), this.slots(), this.availableExpressionVariables() ) );
    }

    public List<Expression> arguments()
    {
        return new colon( this.inner(),.MODULE$);
    }

    public String toString()
    {
        return "NestedExpression()";
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.inner()}) ))).
        $plus$plus( new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) this.availableExpressionVariables() ) ), scala.collection.Seq..
        MODULE$.canBuildFrom());
    }

    public NestedPipeSlottedExpression copy( final Pipe pipe, final Expression inner, final SlotConfiguration slots,
            final ExpressionVariable[] availableExpressionVariables )
    {
        return new NestedPipeSlottedExpression( pipe, inner, slots, availableExpressionVariables );
    }

    public Pipe copy$default$1()
    {
        return this.pipe();
    }

    public Expression copy$default$2()
    {
        return this.inner();
    }

    public SlotConfiguration copy$default$3()
    {
        return this.slots();
    }

    public ExpressionVariable[] copy$default$4()
    {
        return this.availableExpressionVariables();
    }

    public String productPrefix()
    {
        return "NestedPipeSlottedExpression";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.pipe();
            break;
        case 1:
            var10000 = this.inner();
            break;
        case 2:
            var10000 = this.slots();
            break;
        case 3:
            var10000 = this.availableExpressionVariables();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NestedPipeSlottedExpression;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label74:
            {
                boolean var2;
                if ( x$1 instanceof NestedPipeSlottedExpression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label56:
                    {
                        label65:
                        {
                            NestedPipeSlottedExpression var4 = (NestedPipeSlottedExpression) x$1;
                            Pipe var10000 = this.pipe();
                            Pipe var5 = var4.pipe();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label65;
                            }

                            Expression var8 = this.inner();
                            Expression var6 = var4.inner();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label65;
                            }

                            SlotConfiguration var9 = this.slots();
                            SlotConfiguration var7 = var4.slots();
                            if ( var9 == null )
                            {
                                if ( var7 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var9.equals( var7 ) )
                            {
                                break label65;
                            }

                            if ( this.availableExpressionVariables() == var4.availableExpressionVariables() && var4.canEqual( this ) )
                            {
                                var10 = true;
                                break label56;
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label74;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
