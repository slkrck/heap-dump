package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Predicate;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class RelationshipPropertyExists extends Predicate implements SlottedExpression, Product, Serializable
{
    private final int offset;
    private final int token;

    public RelationshipPropertyExists( final int offset, final int token )
    {
        this.offset = offset;
        this.token = token;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<Object,Object>> unapply( final RelationshipPropertyExists x$0 )
    {
        return RelationshipPropertyExists$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple2<Object,Object>,RelationshipPropertyExists> tupled()
    {
        return RelationshipPropertyExists$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Object,RelationshipPropertyExists>> curried()
    {
        return RelationshipPropertyExists$.MODULE$.curried();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public int token()
    {
        return this.token;
    }

    public Option<Object> isMatch( final ExecutionContext m, final QueryState state )
    {
        return new Some( BoxesRunTime.boxToBoolean(
                state.query().relationshipOps().hasProperty( m.getLongAt( this.offset() ), this.token(), state.cursors().relationshipScanCursor(),
                        state.cursors().propertyCursor() ) ) );
    }

    public boolean containsIsNull()
    {
        return false;
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.empty();
    }

    public RelationshipPropertyExists copy( final int offset, final int token )
    {
        return new RelationshipPropertyExists( offset, token );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public int copy$default$2()
    {
        return this.token();
    }

    public String productPrefix()
    {
        return "RelationshipPropertyExists";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Integer var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToInteger( this.token() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipPropertyExists;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, this.token() );
        return Statics.finalizeHash( var1, 2 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label51:
            {
                boolean var2;
                if ( x$1 instanceof RelationshipPropertyExists )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    RelationshipPropertyExists var4 = (RelationshipPropertyExists) x$1;
                    if ( this.offset() == var4.offset() && this.token() == var4.token() && var4.canEqual( this ) )
                    {
                        break label51;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
