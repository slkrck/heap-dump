package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class PrimitiveEquals$ extends AbstractFunction2<Expression,Expression,PrimitiveEquals> implements Serializable
{
    public static PrimitiveEquals$ MODULE$;

    static
    {
        new PrimitiveEquals$();
    }

    private PrimitiveEquals$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "PrimitiveEquals";
    }

    public PrimitiveEquals apply( final Expression a, final Expression b )
    {
        return new PrimitiveEquals( a, b );
    }

    public Option<Tuple2<Expression,Expression>> unapply( final PrimitiveEquals x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.a(), x$0.b() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
