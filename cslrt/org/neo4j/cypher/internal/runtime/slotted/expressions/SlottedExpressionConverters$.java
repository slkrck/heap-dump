package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple2.mcZI.sp;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Seq.;
import scala.runtime.BoxesRunTime;

public final class SlottedExpressionConverters$ implements Serializable
{
    public static SlottedExpressionConverters$ MODULE$;

    static
    {
        new SlottedExpressionConverters$();
    }

    private SlottedExpressionConverters$()
    {
        MODULE$ = this;
    }

    public Seq<Tuple3<String,Expression,Object>> orderGroupingKeyExpressions( final Iterable<Tuple2<String,Expression>> groupings,
            final Seq<Expression> orderToLeverage, final SlotConfiguration slots )
    {
        return (Seq) ((SeqLike) groupings.toSeq().map( ( a ) -> {
            return new Tuple3( a._1(), a._2(), BoxesRunTime.boxToBoolean( orderToLeverage.contains( a._2() ) ) );
        },.MODULE$.canBuildFrom())).sortBy( ( b ) -> {
        return new sp( !BoxesRunTime.unboxToBoolean( b._3() ), slots.apply( (String) b._1() ).offset() );
    }, scala.math.Ordering..MODULE$.Tuple2( scala.math.Ordering.Boolean..MODULE$, scala.math.Ordering.Int..MODULE$));
    }

    public SlottedExpressionConverters apply( final PhysicalPlan physicalPlan )
    {
        return new SlottedExpressionConverters( physicalPlan );
    }

    public Option<PhysicalPlan> unapply( final SlottedExpressionConverters x$0 )
    {
        return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( x$0.physicalPlan() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
