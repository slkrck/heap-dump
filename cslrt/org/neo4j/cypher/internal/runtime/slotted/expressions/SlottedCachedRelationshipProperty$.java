package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class SlottedCachedRelationshipProperty$ extends AbstractFunction4<Object,Object,Object,Object,SlottedCachedRelationshipProperty>
        implements Serializable
{
    public static SlottedCachedRelationshipProperty$ MODULE$;

    static
    {
        new SlottedCachedRelationshipProperty$();
    }

    private SlottedCachedRelationshipProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedCachedRelationshipProperty";
    }

    public SlottedCachedRelationshipProperty apply( final int relationshipOffset, final boolean offsetIsForLongSlot, final int propertyKey,
            final int cachedPropertyOffset )
    {
        return new SlottedCachedRelationshipProperty( relationshipOffset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset );
    }

    public Option<Tuple4<Object,Object,Object,Object>> unapply( final SlottedCachedRelationshipProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple4( BoxesRunTime.boxToInteger( x$0.relationshipOffset() ), BoxesRunTime.boxToBoolean( x$0.offsetIsForLongSlot() ),
                    BoxesRunTime.boxToInteger( x$0.propertyKey() ), BoxesRunTime.boxToInteger( x$0.cachedPropertyOffset() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
