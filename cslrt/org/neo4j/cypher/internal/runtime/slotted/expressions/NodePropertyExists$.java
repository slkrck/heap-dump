package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.Tuple2.mcII.sp;
import scala.runtime.AbstractFunction2;

public final class NodePropertyExists$ extends AbstractFunction2<Object,Object,NodePropertyExists> implements Serializable
{
    public static NodePropertyExists$ MODULE$;

    static
    {
        new NodePropertyExists$();
    }

    private NodePropertyExists$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodePropertyExists";
    }

    public NodePropertyExists apply( final int offset, final int token )
    {
        return new NodePropertyExists( offset, token );
    }

    public Option<Tuple2<Object,Object>> unapply( final NodePropertyExists x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new sp( x$0.offset(), x$0.token() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
