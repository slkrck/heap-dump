package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class IdFromSlot$ extends AbstractFunction1<Object,IdFromSlot> implements Serializable
{
    public static IdFromSlot$ MODULE$;

    static
    {
        new IdFromSlot$();
    }

    private IdFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "IdFromSlot";
    }

    public IdFromSlot apply( final int offset )
    {
        return new IdFromSlot( offset );
    }

    public Option<Object> unapply( final IdFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.offset() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
