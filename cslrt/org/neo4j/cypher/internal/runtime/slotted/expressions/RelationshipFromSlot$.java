package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class RelationshipFromSlot$ extends AbstractFunction1<Object,RelationshipFromSlot> implements Serializable
{
    public static RelationshipFromSlot$ MODULE$;

    static
    {
        new RelationshipFromSlot$();
    }

    private RelationshipFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipFromSlot";
    }

    public RelationshipFromSlot apply( final int offset )
    {
        return new RelationshipFromSlot( offset );
    }

    public Option<Object> unapply( final RelationshipFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.offset() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
