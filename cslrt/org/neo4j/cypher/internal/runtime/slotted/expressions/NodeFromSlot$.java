package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class NodeFromSlot$ extends AbstractFunction1<Object,NodeFromSlot> implements Serializable
{
    public static NodeFromSlot$ MODULE$;

    static
    {
        new NodeFromSlot$();
    }

    private NodeFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodeFromSlot";
    }

    public NodeFromSlot apply( final int offset )
    {
        return new NodeFromSlot( offset );
    }

    public Option<Object> unapply( final NodeFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.offset() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
