package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class SlottedCachedNodePropertyLate$ extends AbstractFunction4<Object,Object,String,Object,SlottedCachedNodePropertyLate> implements Serializable
{
    public static SlottedCachedNodePropertyLate$ MODULE$;

    static
    {
        new SlottedCachedNodePropertyLate$();
    }

    private SlottedCachedNodePropertyLate$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedCachedNodePropertyLate";
    }

    public SlottedCachedNodePropertyLate apply( final int nodeOffset, final boolean offsetIsForLongSlot, final String propertyKey,
            final int cachedPropertyOffset )
    {
        return new SlottedCachedNodePropertyLate( nodeOffset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset );
    }

    public Option<Tuple4<Object,Object,String,Object>> unapply( final SlottedCachedNodePropertyLate x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple4( BoxesRunTime.boxToInteger( x$0.nodeOffset() ), BoxesRunTime.boxToBoolean( x$0.offsetIsForLongSlot() ), x$0.propertyKey(),
                    BoxesRunTime.boxToInteger( x$0.cachedPropertyOffset() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
