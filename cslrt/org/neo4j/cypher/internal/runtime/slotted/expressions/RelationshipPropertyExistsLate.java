package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Predicate;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class RelationshipPropertyExistsLate extends Predicate implements SlottedExpression, Product, Serializable
{
    private final int offset;
    private final String propKey;

    public RelationshipPropertyExistsLate( final int offset, final String propKey )
    {
        this.offset = offset;
        this.propKey = propKey;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<Object,String>> unapply( final RelationshipPropertyExistsLate x$0 )
    {
        return RelationshipPropertyExistsLate$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple2<Object,String>,RelationshipPropertyExistsLate> tupled()
    {
        return RelationshipPropertyExistsLate$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<String,RelationshipPropertyExistsLate>> curried()
    {
        return RelationshipPropertyExistsLate$.MODULE$.curried();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public String propKey()
    {
        return this.propKey;
    }

    public Option<Object> isMatch( final ExecutionContext m, final QueryState state )
    {
        Option maybeToken = state.query().getOptPropertyKeyId( this.propKey() );
        boolean result = maybeToken.isEmpty() ? false : state.query().relationshipOps().hasProperty( m.getLongAt( this.offset() ),
                BoxesRunTime.unboxToInt( maybeToken.get() ), state.cursors().relationshipScanCursor(), state.cursors().propertyCursor() );
        return new Some( BoxesRunTime.boxToBoolean( result ) );
    }

    public boolean containsIsNull()
    {
        return false;
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.empty();
    }

    public RelationshipPropertyExistsLate copy( final int offset, final String propKey )
    {
        return new RelationshipPropertyExistsLate( offset, propKey );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public String copy$default$2()
    {
        return this.propKey();
    }

    public String productPrefix()
    {
        return "RelationshipPropertyExistsLate";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = this.propKey();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipPropertyExistsLate;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.propKey() ) );
        return Statics.finalizeHash( var1, 2 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof RelationshipPropertyExistsLate )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        RelationshipPropertyExistsLate var4 = (RelationshipPropertyExistsLate) x$1;
                        if ( this.offset() == var4.offset() )
                        {
                            label36:
                            {
                                String var10000 = this.propKey();
                                String var5 = var4.propKey();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label36;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label36;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label38;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
