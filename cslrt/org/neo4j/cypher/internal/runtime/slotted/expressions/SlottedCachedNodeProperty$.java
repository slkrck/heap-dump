package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class SlottedCachedNodeProperty$ extends AbstractFunction4<Object,Object,Object,Object,SlottedCachedNodeProperty> implements Serializable
{
    public static SlottedCachedNodeProperty$ MODULE$;

    static
    {
        new SlottedCachedNodeProperty$();
    }

    private SlottedCachedNodeProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedCachedNodeProperty";
    }

    public SlottedCachedNodeProperty apply( final int nodeOffset, final boolean offsetIsForLongSlot, final int propertyKey, final int cachedPropertyOffset )
    {
        return new SlottedCachedNodeProperty( nodeOffset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset );
    }

    public Option<Tuple4<Object,Object,Object,Object>> unapply( final SlottedCachedNodeProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple4( BoxesRunTime.boxToInteger( x$0.nodeOffset() ), BoxesRunTime.boxToBoolean( x$0.offsetIsForLongSlot() ),
                    BoxesRunTime.boxToInteger( x$0.propertyKey() ), BoxesRunTime.boxToInteger( x$0.cachedPropertyOffset() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
