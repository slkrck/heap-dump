package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState;
import org.neo4j.exceptions.InternalException;
import org.neo4j.values.AnyValue;

public final class CompiledExpressionConverter$
{
    public static CompiledExpressionConverter$ MODULE$;

    static
    {
        new CompiledExpressionConverter$();
    }

    private final int org$neo4j$cypher$internal$runtime$slotted$expressions$CompiledExpressionConverter$$COMPILE_LIMIT;

    private CompiledExpressionConverter$()
    {
        MODULE$ = this;
        this.org$neo4j$cypher$internal$runtime$slotted$expressions$CompiledExpressionConverter$$COMPILE_LIMIT = 2;
    }

    public boolean $lessinit$greater$default$6()
    {
        return false;
    }

    public int org$neo4j$cypher$internal$runtime$slotted$expressions$CompiledExpressionConverter$$COMPILE_LIMIT()
    {
        return this.org$neo4j$cypher$internal$runtime$slotted$expressions$CompiledExpressionConverter$$COMPILE_LIMIT;
    }

    public AnyValue[] parametersOrFail( final QueryState state )
    {
        if ( state instanceof SlottedQueryState )
        {
            SlottedQueryState var4 = (SlottedQueryState) state;
            AnyValue[] var2 = var4.params();
            return var2;
        }
        else
        {
            throw new InternalException( "Expected a slotted query state" );
        }
    }
}
