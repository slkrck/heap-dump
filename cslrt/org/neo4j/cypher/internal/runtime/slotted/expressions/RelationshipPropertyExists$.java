package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.Tuple2.mcII.sp;
import scala.runtime.AbstractFunction2;

public final class RelationshipPropertyExists$ extends AbstractFunction2<Object,Object,RelationshipPropertyExists> implements Serializable
{
    public static RelationshipPropertyExists$ MODULE$;

    static
    {
        new RelationshipPropertyExists$();
    }

    private RelationshipPropertyExists$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipPropertyExists";
    }

    public RelationshipPropertyExists apply( final int offset, final int token )
    {
        return new RelationshipPropertyExists( offset, token );
    }

    public Option<Tuple2<Object,Object>> unapply( final RelationshipPropertyExists x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new sp( x$0.offset(), x$0.token() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
