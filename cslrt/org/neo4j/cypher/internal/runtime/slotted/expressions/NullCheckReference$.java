package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class NullCheckReference$ extends AbstractFunction2<Object,Expression,NullCheckReference> implements Serializable
{
    public static NullCheckReference$ MODULE$;

    static
    {
        new NullCheckReference$();
    }

    private NullCheckReference$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NullCheckReference";
    }

    public NullCheckReference apply( final int offset, final Expression inner )
    {
        return new NullCheckReference( offset, inner );
    }

    public Option<Tuple2<Object,Expression>> unapply( final NullCheckReference x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.inner() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
