package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.Predicate;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class HasLabelFromSlot extends Predicate implements SlottedExpression, Product, Serializable
{
    private final int offset;
    private final int resolvedLabelToken;

    public HasLabelFromSlot( final int offset, final int resolvedLabelToken )
    {
        this.offset = offset;
        this.resolvedLabelToken = resolvedLabelToken;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<Object,Object>> unapply( final HasLabelFromSlot x$0 )
    {
        return HasLabelFromSlot$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple2<Object,Object>,HasLabelFromSlot> tupled()
    {
        return HasLabelFromSlot$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Object,HasLabelFromSlot>> curried()
    {
        return HasLabelFromSlot$.MODULE$.curried();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public int resolvedLabelToken()
    {
        return this.resolvedLabelToken;
    }

    public Option<Object> isMatch( final ExecutionContext m, final QueryState state )
    {
        return new Some( BoxesRunTime.boxToBoolean(
                state.query().isLabelSetOnNode( this.resolvedLabelToken(), m.getLongAt( this.offset() ), state.cursors().nodeCursor() ) ) );
    }

    public boolean containsIsNull()
    {
        return false;
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.empty();
    }

    public HasLabelFromSlot copy( final int offset, final int resolvedLabelToken )
    {
        return new HasLabelFromSlot( offset, resolvedLabelToken );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public int copy$default$2()
    {
        return this.resolvedLabelToken();
    }

    public String productPrefix()
    {
        return "HasLabelFromSlot";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Integer var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToInteger( this.resolvedLabelToken() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof HasLabelFromSlot;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, this.resolvedLabelToken() );
        return Statics.finalizeHash( var1, 2 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label51:
            {
                boolean var2;
                if ( x$1 instanceof HasLabelFromSlot )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    HasLabelFromSlot var4 = (HasLabelFromSlot) x$1;
                    if ( this.offset() == var4.offset() && this.resolvedLabelToken() == var4.resolvedLabelToken() && var4.canEqual( this ) )
                    {
                        break label51;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
