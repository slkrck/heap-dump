package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class RelationshipTypeFromSlot extends Expression implements SlottedExpression, Product, Serializable
{
    private final int offset;

    public RelationshipTypeFromSlot( final int offset )
    {
        this.offset = offset;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Object> unapply( final RelationshipTypeFromSlot x$0 )
    {
        return RelationshipTypeFromSlot$.MODULE$.unapply( var0 );
    }

    public static <A> Function1<Object,A> andThen( final Function1<RelationshipTypeFromSlot,A> g )
    {
        return RelationshipTypeFromSlot$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,RelationshipTypeFromSlot> compose( final Function1<A,Object> g )
    {
        return RelationshipTypeFromSlot$.MODULE$.compose( var0 );
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public AnyValue apply( final ExecutionContext ctx, final QueryState state )
    {
        return state.query().getTypeForRelationship( ctx.getLongAt( this.offset() ), state.cursors().relationshipScanCursor() );
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.empty();
    }

    public RelationshipTypeFromSlot copy( final int offset )
    {
        return new RelationshipTypeFromSlot( offset );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public String productPrefix()
    {
        return "RelationshipTypeFromSlot";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return BoxesRunTime.boxToInteger( this.offset() );
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipTypeFromSlot;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        return Statics.finalizeHash( var1, 1 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label49:
            {
                boolean var2;
                if ( x$1 instanceof RelationshipTypeFromSlot )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    RelationshipTypeFromSlot var4 = (RelationshipTypeFromSlot) x$1;
                    if ( this.offset() == var4.offset() && var4.canEqual( this ) )
                    {
                        break label49;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
