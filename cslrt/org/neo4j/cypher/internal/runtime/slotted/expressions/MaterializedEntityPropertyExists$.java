package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.values.KeyToken;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class MaterializedEntityPropertyExists$ extends AbstractFunction2<Expression,KeyToken,MaterializedEntityPropertyExists> implements Serializable
{
    public static MaterializedEntityPropertyExists$ MODULE$;

    static
    {
        new MaterializedEntityPropertyExists$();
    }

    private MaterializedEntityPropertyExists$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MaterializedEntityPropertyExists";
    }

    public MaterializedEntityPropertyExists apply( final Expression variable, final KeyToken propertyKey )
    {
        return new MaterializedEntityPropertyExists( variable, propertyKey );
    }

    public Option<Tuple2<Expression,KeyToken>> unapply( final MaterializedEntityPropertyExists x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.variable(), x$0.propertyKey() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
