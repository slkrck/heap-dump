package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class HasLabelFromSlotLate$ extends AbstractFunction2<Object,String,HasLabelFromSlotLate> implements Serializable
{
    public static HasLabelFromSlotLate$ MODULE$;

    static
    {
        new HasLabelFromSlotLate$();
    }

    private HasLabelFromSlotLate$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "HasLabelFromSlotLate";
    }

    public HasLabelFromSlotLate apply( final int offset, final String labelName )
    {
        return new HasLabelFromSlotLate( offset, labelName );
    }

    public Option<Tuple2<Object,String>> unapply( final HasLabelFromSlotLate x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.labelName() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
