package org.neo4j.cypher.internal.runtime.slotted.expressions;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class SlottedCachedRelationshipPropertyLate$ extends AbstractFunction4<Object,Object,String,Object,SlottedCachedRelationshipPropertyLate>
        implements Serializable
{
    public static SlottedCachedRelationshipPropertyLate$ MODULE$;

    static
    {
        new SlottedCachedRelationshipPropertyLate$();
    }

    private SlottedCachedRelationshipPropertyLate$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedCachedRelationshipPropertyLate";
    }

    public SlottedCachedRelationshipPropertyLate apply( final int relationshipOffset, final boolean offsetIsForLongSlot, final String propertyKey,
            final int cachedPropertyOffset )
    {
        return new SlottedCachedRelationshipPropertyLate( relationshipOffset, offsetIsForLongSlot, propertyKey, cachedPropertyOffset );
    }

    public Option<Tuple4<Object,Object,String,Object>> unapply( final SlottedCachedRelationshipPropertyLate x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple4( BoxesRunTime.boxToInteger( x$0.relationshipOffset() ), BoxesRunTime.boxToBoolean( x$0.offsetIsForLongSlot() ), x$0.propertyKey(),
                    BoxesRunTime.boxToInteger( x$0.cachedPropertyOffset() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
