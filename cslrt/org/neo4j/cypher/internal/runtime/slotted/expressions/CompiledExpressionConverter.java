package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode;
import org.neo4j.cypher.internal.Assertion;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler$;
import org.neo4j.cypher.internal.runtime.interpreted.CommandProjection;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.CommunityExpressionConverter;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverter;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.functions.AggregatingFunction;
import org.neo4j.logging.Log;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.None.;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class CompiledExpressionConverter implements ExpressionConverter
{
    private final Log log;
    private final PhysicalPlan physicalPlan;
    private final boolean readOnly;
    private final CodeGenerationMode codeGenerationMode;
    private final boolean neverFail;
    private final ExpressionConverters inner;

    public CompiledExpressionConverter( final Log log, final PhysicalPlan physicalPlan, final TokenContext tokenContext, final boolean readOnly,
            final CodeGenerationMode codeGenerationMode, final boolean neverFail )
    {
        this.log = log;
        this.physicalPlan = physicalPlan;
        this.readOnly = readOnly;
        this.codeGenerationMode = codeGenerationMode;
        this.neverFail = neverFail;
        this.inner = new ExpressionConverters( scala.Predef..MODULE$.wrapRefArray(
                (Object[]) (new ExpressionConverter[]{new SlottedExpressionConverters( physicalPlan ), new CommunityExpressionConverter( tokenContext )}) ));
    }

    public static boolean $lessinit$greater$default$6()
    {
        return CompiledExpressionConverter$.MODULE$.$lessinit$greater$default$6();
    }

    public static AnyValue[] parametersOrFail( final QueryState state )
    {
        return CompiledExpressionConverter$.MODULE$.parametersOrFail( var0 );
    }

    private ExpressionConverters inner()
    {
        return this.inner;
    }

    public Option<Expression> toCommandExpression( final int id, final org.neo4j.cypher.internal.v4_0.expressions.Expression expression,
            final ExpressionConverters self )
    {
        org.neo4j.cypher.internal.v4_0.expressions.Expression var6 = expression;
        Object var4;
        if ( expression instanceof FunctionInvocation )
        {
            FunctionInvocation var7 = (FunctionInvocation) expression;
            if ( var7.function() instanceof AggregatingFunction )
            {
                var4 = .MODULE$;
                return (Option) var4;
            }
        }

        if ( this.sizeOf( expression ) >
                CompiledExpressionConverter$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$CompiledExpressionConverter$$COMPILE_LIMIT() )
        {
            Object var10000;
            try
            {
                this.log.debug( (new StringBuilder( 22 )).append( "Compiling expression: " ).append( expression ).toString() );
                var10000 = ExpressionCompiler$.MODULE$.defaultGenerator( (SlotConfiguration) this.physicalPlan.slotConfigurations().apply( id ), this.readOnly,
                        this.codeGenerationMode, ExpressionCompiler$.MODULE$.defaultGenerator$default$4() ).compileExpression( var6 ).map( ( x$1 ) -> {
                    return new CompileWrappingExpression( x$1, this.inner().toCommandExpression( id, expression ) );
                } );
            }
            catch ( Throwable var12 )
            {
                Option var10 = org.neo4j.cypher.internal.NonFatalCypherError..MODULE$.unapply( var12 );
                if ( var10.isEmpty() )
                {
                    throw var12;
                }

                Throwable t = (Throwable) var10.get();
                if ( this.shouldThrow() )
                {
                    throw t;
                }

                this.log.debug( (new StringBuilder( 30 )).append( "Failed to compile expression: " ).append( expression ).toString(), t );
                var5 = .MODULE$;
                var10000 = var5;
            }

            var4 = var10000;
        }
        else
        {
            var4 = .MODULE$;
        }

        return (Option) var4;
    }

    private int sizeOf( final org.neo4j.cypher.internal.v4_0.expressions.Expression expression )
    {
        return org.neo4j.cypher.internal.v4_0.util.Foldable.FoldableAny..
        MODULE$.treeCount$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( expression ), new Serializable(
                (CompiledExpressionConverter) null )
        {
            public static final long serialVersionUID = 0L;

            public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x1 instanceof org.neo4j.cypher.internal.v4_0.expressions.Expression )
                {
                    var3 = BoxesRunTime.boxToBoolean( true );
                }
                else
                {
                    var3 = var2.apply( x1 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final Object x1 )
            {
                boolean var2;
                if ( x1 instanceof org.neo4j.cypher.internal.v4_0.expressions.Expression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        });
    }

    public Option<CommandProjection> toCommandProjection( final int id, final Map<String,org.neo4j.cypher.internal.v4_0.expressions.Expression> projections,
            final ExpressionConverters self )
    {
        Object var10000;
        try
        {
            int totalSize = BoxesRunTime.unboxToInt( projections.values().foldLeft( BoxesRunTime.boxToInteger( 0 ), ( acc, current ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$toCommandProjection$1( this, BoxesRunTime.unboxToInt( acc ), current ) );
            } ) );
            if ( totalSize >
                    CompiledExpressionConverter$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$CompiledExpressionConverter$$COMPILE_LIMIT() )
            {
                this.log.debug( (new StringBuilder( 23 )).append( " Compiling projection: " ).append( projections ).toString() );
                var10000 = ExpressionCompiler$.MODULE$.defaultGenerator( (SlotConfiguration) this.physicalPlan.slotConfigurations().apply( id ), this.readOnly,
                        this.codeGenerationMode, ExpressionCompiler$.MODULE$.defaultGenerator$default$4() ).compileProjection( projections ).map( ( x$2 ) -> {
                    return new CompileWrappingProjection( x$2, projections.isEmpty() );
                } );
            }
            else
            {
                var10000 = .MODULE$;
            }
        }
        catch ( Throwable var10 )
        {
            Option var8 = org.neo4j.cypher.internal.NonFatalCypherError..MODULE$.unapply( var10 );
            if ( var8.isEmpty() )
            {
                throw var10;
            }

            Throwable t = (Throwable) var8.get();
            if ( this.shouldThrow() )
            {
                throw t;
            }

            this.log.debug( (new StringBuilder( 30 )).append( "Failed to compile projection: " ).append( projections ).toString(), t );
            var4 = .MODULE$;
            var10000 = var4;
        }

        return (Option) var10000;
    }

    public Option<GroupingExpression> toGroupingExpression( final int id, final Map<String,org.neo4j.cypher.internal.v4_0.expressions.Expression> projections,
            final Seq<org.neo4j.cypher.internal.v4_0.expressions.Expression> orderToLeverage, final ExpressionConverters self )
    {
        Object var10000;
        try
        {
            if ( orderToLeverage.nonEmpty() )
            {
                var10000 = .MODULE$;
            }
            else
            {
                int totalSize = BoxesRunTime.unboxToInt( projections.values().foldLeft( BoxesRunTime.boxToInteger( 0 ), ( acc, current ) -> {
                    return BoxesRunTime.boxToInteger( $anonfun$toGroupingExpression$1( this, BoxesRunTime.unboxToInt( acc ), current ) );
                } ) );
                if ( totalSize >
                        CompiledExpressionConverter$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$CompiledExpressionConverter$$COMPILE_LIMIT() )
                {
                    this.log.debug( (new StringBuilder( 32 )).append( " Compiling grouping expression: " ).append( projections ).toString() );
                    var10000 =
                            ExpressionCompiler$.MODULE$.defaultGenerator( (SlotConfiguration) this.physicalPlan.slotConfigurations().apply( id ), this.readOnly,
                                    this.codeGenerationMode, ExpressionCompiler$.MODULE$.defaultGenerator$default$4() ).compileGrouping( ( slots ) -> {
                                return SlottedExpressionConverters$.MODULE$.orderGroupingKeyExpressions( projections, orderToLeverage, slots );
                            } ).map( ( x$3 ) -> {
                                return new CompileWrappingDistinctGroupingExpression( x$3, projections.isEmpty() );
                            } );
                }
                else
                {
                    var10000 = .MODULE$;
                }
            }
        }
        catch ( Throwable var11 )
        {
            Option var9 = org.neo4j.cypher.internal.NonFatalCypherError..MODULE$.unapply( var11 );
            if ( var9.isEmpty() )
            {
                throw var11;
            }

            Throwable t = (Throwable) var9.get();
            if ( this.shouldThrow() )
            {
                throw t;
            }

            this.log.debug( (new StringBuilder( 39 )).append( "Failed to compile grouping expression: " ).append( projections ).toString(), t );
            var5 = .MODULE$;
            var10000 = var5;
        }

        return (Option) var10000;
    }

    private boolean shouldThrow()
    {
        return !this.neverFail && Assertion.assertionsEnabled();
    }
}
