package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Seq.;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class NullCheckReference extends Expression implements SlottedExpression, Product, Serializable
{
    private final int offset;
    private final Expression inner;

    public NullCheckReference( final int offset, final Expression inner )
    {
        this.offset = offset;
        this.inner = inner;
        SlottedExpression.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<Object,Expression>> unapply( final NullCheckReference x$0 )
    {
        return NullCheckReference$.MODULE$.unapply( var0 );
    }

    public static Function1<Tuple2<Object,Expression>,NullCheckReference> tupled()
    {
        return NullCheckReference$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Expression,NullCheckReference>> curried()
    {
        return NullCheckReference$.MODULE$.curried();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return SlottedExpression.rewrite$( this, f );
    }

    public Seq<Expression> arguments()
    {
        return SlottedExpression.arguments$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public Expression inner()
    {
        return this.inner;
    }

    public AnyValue apply( final ExecutionContext ctx, final QueryState state )
    {
        return (AnyValue) (ctx.getRefAt( this.offset() ) == Values.NO_VALUE ? Values.NO_VALUE : this.inner().apply( ctx, state ));
    }

    public Seq<AstNode<?>> children()
    {
        return (Seq).MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.inner()}) ));
    }

    public NullCheckReference copy( final int offset, final Expression inner )
    {
        return new NullCheckReference( offset, inner );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public Expression copy$default$2()
    {
        return this.inner();
    }

    public String productPrefix()
    {
        return "NullCheckReference";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = this.inner();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NullCheckReference;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.inner() ) );
        return Statics.finalizeHash( var1, 2 );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof NullCheckReference )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        NullCheckReference var4 = (NullCheckReference) x$1;
                        if ( this.offset() == var4.offset() )
                        {
                            label36:
                            {
                                Expression var10000 = this.inner();
                                Expression var5 = var4.inner();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label36;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label36;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label38;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
