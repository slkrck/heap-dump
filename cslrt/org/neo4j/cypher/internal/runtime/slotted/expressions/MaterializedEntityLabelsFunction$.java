package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class MaterializedEntityLabelsFunction$ extends AbstractFunction1<Expression,MaterializedEntityLabelsFunction> implements Serializable
{
    public static MaterializedEntityLabelsFunction$ MODULE$;

    static
    {
        new MaterializedEntityLabelsFunction$();
    }

    private MaterializedEntityLabelsFunction$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MaterializedEntityLabelsFunction";
    }

    public MaterializedEntityLabelsFunction apply( final Expression nodeExpr )
    {
        return new MaterializedEntityLabelsFunction( nodeExpr );
    }

    public Option<Expression> unapply( final MaterializedEntityLabelsFunction x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.nodeExpr() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
