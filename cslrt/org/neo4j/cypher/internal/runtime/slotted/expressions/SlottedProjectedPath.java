package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.interpreted.commands.AstNode;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.PathValueBuilder;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction2;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class SlottedProjectedPath extends Expression implements Product, Serializable
{
    private final Set<String> symbolTableDependencies;
    private final SlottedProjectedPath.Projector projector;

    public SlottedProjectedPath( final Set<String> symbolTableDependencies, final SlottedProjectedPath.Projector projector )
    {
        this.symbolTableDependencies = symbolTableDependencies;
        this.projector = projector;
        Product.$init$( this );
    }

    public static Option<Tuple2<Set<String>,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath x$0 )
    {
        return SlottedProjectedPath$.MODULE$.unapply( var0 );
    }

    public Set<String> symbolTableDependencies()
    {
        return this.symbolTableDependencies;
    }

    public SlottedProjectedPath.Projector projector()
    {
        return this.projector;
    }

    public AnyValue apply( final ExecutionContext ctx, final QueryState state )
    {
        return this.projector().apply( ctx, state, state.clearPathValueBuilder() ).result();
    }

    public Seq<Expression> arguments()
    {
        return (Seq) scala.collection.Seq..MODULE$.empty();
    }

    public Expression rewrite( final Function1<Expression,Expression> f )
    {
        return (Expression) f.apply( this );
    }

    public Seq<AstNode<?>> children()
    {
        return this.projector().arguments();
    }

    public SlottedProjectedPath copy( final Set<String> symbolTableDependencies, final SlottedProjectedPath.Projector projector )
    {
        return new SlottedProjectedPath( symbolTableDependencies, projector );
    }

    public Set<String> copy$default$1()
    {
        return this.symbolTableDependencies();
    }

    public SlottedProjectedPath.Projector copy$default$2()
    {
        return this.projector();
    }

    public String productPrefix()
    {
        return "SlottedProjectedPath";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.symbolTableDependencies();
            break;
        case 1:
            var10000 = this.projector();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedProjectedPath;
    }

    public int hashCode()
    {
        return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof SlottedProjectedPath )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            SlottedProjectedPath var4 = (SlottedProjectedPath) x$1;
                            Set var10000 = this.symbolTableDependencies();
                            Set var5 = var4.symbolTableDependencies();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            SlottedProjectedPath.Projector var7 = this.projector();
                            SlottedProjectedPath.Projector var6 = var4.projector();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }

    public interface Projector
    {
        PathValueBuilder apply( final ExecutionContext context, final QueryState state, final PathValueBuilder builder );

        Seq<Expression> arguments();
    }

    public static class multiIncomingRelationshipProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final SlottedProjectedPath.Projector tailProjector;

        public multiIncomingRelationshipProjector( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            AnyValue relListValue = this.rel().apply( ctx, state );
            return this.tailProjector().apply( ctx, state, builder.addIncomingRelationships( relListValue ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.rel()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.multiIncomingRelationshipProjector copy( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiIncomingRelationshipProjector( rel, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public SlottedProjectedPath.Projector copy$default$2()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "multiIncomingRelationshipProjector";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.multiIncomingRelationshipProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.multiIncomingRelationshipProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedProjectedPath.multiIncomingRelationshipProjector var4 = (SlottedProjectedPath.multiIncomingRelationshipProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlottedProjectedPath.Projector var7 = this.tailProjector();
                                SlottedProjectedPath.Projector var6 = var4.tailProjector();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class multiIncomingRelationshipProjector$
            extends AbstractFunction2<Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.multiIncomingRelationshipProjector> implements Serializable
    {
        public static SlottedProjectedPath.multiIncomingRelationshipProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.multiIncomingRelationshipProjector$();
        }

        public multiIncomingRelationshipProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "multiIncomingRelationshipProjector";
        }

        public SlottedProjectedPath.multiIncomingRelationshipProjector apply( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiIncomingRelationshipProjector( rel, tailProjector );
        }

        public Option<Tuple2<Expression,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath.multiIncomingRelationshipProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.rel(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class multiIncomingRelationshipWithKnownTargetProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final Expression node;
        private final SlottedProjectedPath.Projector tailProjector;

        public multiIncomingRelationshipWithKnownTargetProjector( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.node = node;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public Expression node()
        {
            return this.node;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            boolean var5 = false;
            ListValue var6 = null;
            AnyValue var7 = this.rel().apply( ctx, state );
            PathValueBuilder var4;
            if ( var7 instanceof ListValue )
            {
                var5 = true;
                var6 = (ListValue) var7;
                if ( var6.nonEmpty() )
                {
                    PathValueBuilder aggregated =
                            SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addAllExceptLast( builder,
                                    var6, ( b, v ) -> {
                                        return b.addIncomingRelationship( v );
                                    } );
                    var4 = this.tailProjector().apply( ctx, state, aggregated.addRelationship( var6.last() ).addNode( this.node().apply( ctx, state ) ) );
                    return var4;
                }
            }

            if ( var5 )
            {
                var4 = this.tailProjector().apply( ctx, state, builder );
            }
            else
            {
                if ( var7 != Values.NO_VALUE )
                {
                    throw new CypherTypeException( (new StringBuilder( 27 )).append( "Expected ListValue but got " ).append( var7.getTypeName() ).toString() );
                }

                var4 = this.tailProjector().apply( ctx, state, builder.addNoValue() );
            }

            return var4;
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Expression[]{this.rel(), this.node()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector copy( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector( rel, node, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public Expression copy$default$2()
        {
            return this.node();
        }

        public SlottedProjectedPath.Projector copy$default$3()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "multiIncomingRelationshipWithKnownTargetProjector";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.node();
                break;
            case 2:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector var4 =
                                        (SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                var10000 = this.node();
                                Expression var6 = var4.node();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label63;
                                }

                                SlottedProjectedPath.Projector var8 = this.tailProjector();
                                SlottedProjectedPath.Projector var7 = var4.tailProjector();
                                if ( var8 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var9 = true;
                                    break label54;
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label72;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }

    public static class multiIncomingRelationshipWithKnownTargetProjector$ extends
            AbstractFunction3<Expression,Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector$();
        }

        public multiIncomingRelationshipWithKnownTargetProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "multiIncomingRelationshipWithKnownTargetProjector";
        }

        public SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector apply( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector( rel, node, tailProjector );
        }

        public Option<Tuple3<Expression,Expression,SlottedProjectedPath.Projector>> unapply(
                final SlottedProjectedPath.multiIncomingRelationshipWithKnownTargetProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.rel(), x$0.node(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class multiOutgoingRelationshipProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final SlottedProjectedPath.Projector tailProjector;

        public multiOutgoingRelationshipProjector( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            AnyValue relListValue = this.rel().apply( ctx, state );
            return this.tailProjector().apply( ctx, state, builder.addOutgoingRelationships( relListValue ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.rel()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.multiOutgoingRelationshipProjector copy( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiOutgoingRelationshipProjector( rel, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public SlottedProjectedPath.Projector copy$default$2()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "multiOutgoingRelationshipProjector";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.multiOutgoingRelationshipProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.multiOutgoingRelationshipProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedProjectedPath.multiOutgoingRelationshipProjector var4 = (SlottedProjectedPath.multiOutgoingRelationshipProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlottedProjectedPath.Projector var7 = this.tailProjector();
                                SlottedProjectedPath.Projector var6 = var4.tailProjector();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class multiOutgoingRelationshipProjector$
            extends AbstractFunction2<Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.multiOutgoingRelationshipProjector> implements Serializable
    {
        public static SlottedProjectedPath.multiOutgoingRelationshipProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.multiOutgoingRelationshipProjector$();
        }

        public multiOutgoingRelationshipProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "multiOutgoingRelationshipProjector";
        }

        public SlottedProjectedPath.multiOutgoingRelationshipProjector apply( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiOutgoingRelationshipProjector( rel, tailProjector );
        }

        public Option<Tuple2<Expression,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath.multiOutgoingRelationshipProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.rel(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class multiOutgoingRelationshipWithKnownTargetProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final Expression node;
        private final SlottedProjectedPath.Projector tailProjector;

        public multiOutgoingRelationshipWithKnownTargetProjector( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.node = node;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public Expression node()
        {
            return this.node;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            boolean var5 = false;
            ListValue var6 = null;
            AnyValue var7 = this.rel().apply( ctx, state );
            PathValueBuilder var4;
            if ( var7 instanceof ListValue )
            {
                var5 = true;
                var6 = (ListValue) var7;
                if ( var6.nonEmpty() )
                {
                    PathValueBuilder aggregated =
                            SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addAllExceptLast( builder,
                                    var6, ( b, v ) -> {
                                        return b.addOutgoingRelationship( v );
                                    } );
                    var4 = this.tailProjector().apply( ctx, state, aggregated.addRelationship( var6.last() ).addNode( this.node().apply( ctx, state ) ) );
                    return var4;
                }
            }

            if ( var5 )
            {
                var4 = this.tailProjector().apply( ctx, state, builder );
            }
            else
            {
                if ( var7 != Values.NO_VALUE )
                {
                    throw new CypherTypeException( (new StringBuilder( 27 )).append( "Expected ListValue but got " ).append( var7.getTypeName() ).toString() );
                }

                var4 = this.tailProjector().apply( ctx, state, builder.addNoValue() );
            }

            return var4;
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Expression[]{this.rel(), this.node()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector copy( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector( rel, node, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public Expression copy$default$2()
        {
            return this.node();
        }

        public SlottedProjectedPath.Projector copy$default$3()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "multiOutgoingRelationshipWithKnownTargetProjector";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.node();
                break;
            case 2:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector var4 =
                                        (SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                var10000 = this.node();
                                Expression var6 = var4.node();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label63;
                                }

                                SlottedProjectedPath.Projector var8 = this.tailProjector();
                                SlottedProjectedPath.Projector var7 = var4.tailProjector();
                                if ( var8 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var9 = true;
                                    break label54;
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label72;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }

    public static class multiOutgoingRelationshipWithKnownTargetProjector$ extends
            AbstractFunction3<Expression,Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector$();
        }

        public multiOutgoingRelationshipWithKnownTargetProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "multiOutgoingRelationshipWithKnownTargetProjector";
        }

        public SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector apply( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector( rel, node, tailProjector );
        }

        public Option<Tuple3<Expression,Expression,SlottedProjectedPath.Projector>> unapply(
                final SlottedProjectedPath.multiOutgoingRelationshipWithKnownTargetProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.rel(), x$0.node(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class multiUndirectedRelationshipProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final SlottedProjectedPath.Projector tailProjector;

        public multiUndirectedRelationshipProjector( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            AnyValue relListValue = this.rel().apply( ctx, state );
            return this.tailProjector().apply( ctx, state, builder.addUndirectedRelationships( relListValue ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.rel()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.multiUndirectedRelationshipProjector copy( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiUndirectedRelationshipProjector( rel, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public SlottedProjectedPath.Projector copy$default$2()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "multiUndirectedRelationshipProjector";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.multiUndirectedRelationshipProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.multiUndirectedRelationshipProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedProjectedPath.multiUndirectedRelationshipProjector var4 =
                                        (SlottedProjectedPath.multiUndirectedRelationshipProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlottedProjectedPath.Projector var7 = this.tailProjector();
                                SlottedProjectedPath.Projector var6 = var4.tailProjector();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class multiUndirectedRelationshipProjector$
            extends AbstractFunction2<Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.multiUndirectedRelationshipProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.multiUndirectedRelationshipProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.multiUndirectedRelationshipProjector$();
        }

        public multiUndirectedRelationshipProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "multiUndirectedRelationshipProjector";
        }

        public SlottedProjectedPath.multiUndirectedRelationshipProjector apply( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiUndirectedRelationshipProjector( rel, tailProjector );
        }

        public Option<Tuple2<Expression,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath.multiUndirectedRelationshipProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.rel(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class multiUndirectedRelationshipWithKnownTargetProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final Expression node;
        private final SlottedProjectedPath.Projector tailProjector;

        public multiUndirectedRelationshipWithKnownTargetProjector( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.node = node;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public Expression node()
        {
            return this.node;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            boolean var5 = false;
            ListValue var6 = null;
            AnyValue var7 = this.rel().apply( ctx, state );
            PathValueBuilder var4;
            if ( var7 instanceof ListValue )
            {
                var5 = true;
                var6 = (ListValue) var7;
                if ( var6.nonEmpty() )
                {
                    PathValueBuilder var10000;
                    if ( SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$correctDirection(
                            builder.previousNode(), var6.head() ) )
                    {
                        PathValueBuilder aggregated =
                                SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addAllExceptLast(
                                        builder, var6, ( b, v ) -> {
                                            return b.addUndirectedRelationship( v );
                                        } );
                        var10000 =
                                this.tailProjector().apply( ctx, state, aggregated.addRelationship( var6.last() ).addNode( this.node().apply( ctx, state ) ) );
                    }
                    else
                    {
                        ListValue reversed = var6.reverse();
                        PathValueBuilder aggregated =
                                SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addAllExceptLast(
                                        builder, reversed, ( b, v ) -> {
                                            return b.addUndirectedRelationship( v );
                                        } );
                        var10000 = this.tailProjector().apply( ctx, state,
                                aggregated.addRelationship( reversed.last() ).addNode( this.node().apply( ctx, state ) ) );
                    }

                    var4 = var10000;
                    return var4;
                }
            }

            if ( var5 )
            {
                var4 = this.tailProjector().apply( ctx, state, builder );
            }
            else
            {
                if ( var7 != Values.NO_VALUE )
                {
                    throw new CypherTypeException( (new StringBuilder( 27 )).append( "Expected ListValue but got " ).append( var7.getTypeName() ).toString() );
                }

                var4 = this.tailProjector().apply( ctx, state, builder.addNoValue() );
            }

            return var4;
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Expression[]{this.rel(), this.node()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector copy( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector( rel, node, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public Expression copy$default$2()
        {
            return this.node();
        }

        public SlottedProjectedPath.Projector copy$default$3()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "multiUndirectedRelationshipWithKnownTargetProjector";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.node();
                break;
            case 2:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector var4 =
                                        (SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                var10000 = this.node();
                                Expression var6 = var4.node();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label63;
                                }

                                SlottedProjectedPath.Projector var8 = this.tailProjector();
                                SlottedProjectedPath.Projector var7 = var4.tailProjector();
                                if ( var8 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var9 = true;
                                    break label54;
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label72;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }

    public static class multiUndirectedRelationshipWithKnownTargetProjector$ extends
            AbstractFunction3<Expression,Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector$();
        }

        public multiUndirectedRelationshipWithKnownTargetProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "multiUndirectedRelationshipWithKnownTargetProjector";
        }

        public SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector apply( final Expression rel, final Expression node,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector( rel, node, tailProjector );
        }

        public Option<Tuple3<Expression,Expression,SlottedProjectedPath.Projector>> unapply(
                final SlottedProjectedPath.multiUndirectedRelationshipWithKnownTargetProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.rel(), x$0.node(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class nilProjector$ implements SlottedProjectedPath.Projector
    {
        public static SlottedProjectedPath.nilProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.nilProjector$();
        }

        public nilProjector$()
        {
            MODULE$ = this;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            return builder;
        }

        public Seq<Expression> arguments()
        {
            return (Seq) scala.collection.Seq..MODULE$.empty();
        }
    }

    public static class singleIncomingRelationshipProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final SlottedProjectedPath.Projector tailProjector;

        public singleIncomingRelationshipProjector( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            return this.tailProjector().apply( ctx, state,
                    SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addIncoming(
                            this.rel().apply( ctx, state ), state, builder ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.rel()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.singleIncomingRelationshipProjector copy( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleIncomingRelationshipProjector( rel, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public SlottedProjectedPath.Projector copy$default$2()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "singleIncomingRelationshipProjector";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.singleIncomingRelationshipProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.singleIncomingRelationshipProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedProjectedPath.singleIncomingRelationshipProjector var4 = (SlottedProjectedPath.singleIncomingRelationshipProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlottedProjectedPath.Projector var7 = this.tailProjector();
                                SlottedProjectedPath.Projector var6 = var4.tailProjector();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class singleIncomingRelationshipProjector$
            extends AbstractFunction2<Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.singleIncomingRelationshipProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.singleIncomingRelationshipProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.singleIncomingRelationshipProjector$();
        }

        public singleIncomingRelationshipProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "singleIncomingRelationshipProjector";
        }

        public SlottedProjectedPath.singleIncomingRelationshipProjector apply( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleIncomingRelationshipProjector( rel, tailProjector );
        }

        public Option<Tuple2<Expression,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath.singleIncomingRelationshipProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.rel(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class singleNodeProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression node;
        private final SlottedProjectedPath.Projector tailProjector;

        public singleNodeProjector( final Expression node, final SlottedProjectedPath.Projector tailProjector )
        {
            this.node = node;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression node()
        {
            return this.node;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            AnyValue nodeValue = this.node().apply( ctx, state );
            return this.tailProjector().apply( ctx, state, builder.addNode( nodeValue ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Expression[]{this.node()}) ))).$plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.singleNodeProjector copy( final Expression node, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleNodeProjector( node, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.node();
        }

        public SlottedProjectedPath.Projector copy$default$2()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "singleNodeProjector";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.node();
                break;
            case 1:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.singleNodeProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.singleNodeProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedProjectedPath.singleNodeProjector var4 = (SlottedProjectedPath.singleNodeProjector) x$1;
                                Expression var10000 = this.node();
                                Expression var5 = var4.node();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlottedProjectedPath.Projector var7 = this.tailProjector();
                                SlottedProjectedPath.Projector var6 = var4.tailProjector();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class singleNodeProjector$ extends AbstractFunction2<Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.singleNodeProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.singleNodeProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.singleNodeProjector$();
        }

        public singleNodeProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "singleNodeProjector";
        }

        public SlottedProjectedPath.singleNodeProjector apply( final Expression node, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleNodeProjector( node, tailProjector );
        }

        public Option<Tuple2<Expression,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath.singleNodeProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.node(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class singleOutgoingRelationshipProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final SlottedProjectedPath.Projector tailProjector;

        public singleOutgoingRelationshipProjector( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            return this.tailProjector().apply( ctx, state,
                    SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addOutgoing(
                            this.rel().apply( ctx, state ), state, builder ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.rel()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.singleOutgoingRelationshipProjector copy( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleOutgoingRelationshipProjector( rel, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public SlottedProjectedPath.Projector copy$default$2()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "singleOutgoingRelationshipProjector";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.singleOutgoingRelationshipProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.singleOutgoingRelationshipProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedProjectedPath.singleOutgoingRelationshipProjector var4 = (SlottedProjectedPath.singleOutgoingRelationshipProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlottedProjectedPath.Projector var7 = this.tailProjector();
                                SlottedProjectedPath.Projector var6 = var4.tailProjector();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class singleOutgoingRelationshipProjector$
            extends AbstractFunction2<Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.singleOutgoingRelationshipProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.singleOutgoingRelationshipProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.singleOutgoingRelationshipProjector$();
        }

        public singleOutgoingRelationshipProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "singleOutgoingRelationshipProjector";
        }

        public SlottedProjectedPath.singleOutgoingRelationshipProjector apply( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleOutgoingRelationshipProjector( rel, tailProjector );
        }

        public Option<Tuple2<Expression,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath.singleOutgoingRelationshipProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.rel(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class singleRelationshipWithKnownTargetProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final Expression target;
        private final SlottedProjectedPath.Projector tailProjector;

        public singleRelationshipWithKnownTargetProjector( final Expression rel, final Expression target, final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.target = target;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public Expression target()
        {
            return this.target;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            AnyValue relValue = this.rel().apply( ctx, state );
            AnyValue nodeValue = this.target().apply( ctx, state );
            return this.tailProjector().apply( ctx, state, builder.addRelationship( relValue ).addNode( nodeValue ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray(
                    (Object[]) (new Expression[]{this.rel(), this.target()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.singleRelationshipWithKnownTargetProjector copy( final Expression rel, final Expression target,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleRelationshipWithKnownTargetProjector( rel, target, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public Expression copy$default$2()
        {
            return this.target();
        }

        public SlottedProjectedPath.Projector copy$default$3()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "singleRelationshipWithKnownTargetProjector";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.target();
                break;
            case 2:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.singleRelationshipWithKnownTargetProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var9;
            if ( this != x$1 )
            {
                label72:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.singleRelationshipWithKnownTargetProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label54:
                        {
                            label63:
                            {
                                SlottedProjectedPath.singleRelationshipWithKnownTargetProjector var4 =
                                        (SlottedProjectedPath.singleRelationshipWithKnownTargetProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label63;
                                }

                                var10000 = this.target();
                                Expression var6 = var4.target();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label63;
                                }

                                SlottedProjectedPath.Projector var8 = this.tailProjector();
                                SlottedProjectedPath.Projector var7 = var4.tailProjector();
                                if ( var8 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label63;
                                    }
                                }
                                else if ( !var8.equals( var7 ) )
                                {
                                    break label63;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var9 = true;
                                    break label54;
                                }
                            }

                            var9 = false;
                        }

                        if ( var9 )
                        {
                            break label72;
                        }
                    }

                    var9 = false;
                    return var9;
                }
            }

            var9 = true;
            return var9;
        }
    }

    public static class singleRelationshipWithKnownTargetProjector$
            extends AbstractFunction3<Expression,Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.singleRelationshipWithKnownTargetProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.singleRelationshipWithKnownTargetProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.singleRelationshipWithKnownTargetProjector$();
        }

        public singleRelationshipWithKnownTargetProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "singleRelationshipWithKnownTargetProjector";
        }

        public SlottedProjectedPath.singleRelationshipWithKnownTargetProjector apply( final Expression rel, final Expression target,
                final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleRelationshipWithKnownTargetProjector( rel, target, tailProjector );
        }

        public Option<Tuple3<Expression,Expression,SlottedProjectedPath.Projector>> unapply(
                final SlottedProjectedPath.singleRelationshipWithKnownTargetProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple3( x$0.rel(), x$0.target(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class singleUndirectedRelationshipProjector implements SlottedProjectedPath.Projector, Product, Serializable
    {
        private final Expression rel;
        private final SlottedProjectedPath.Projector tailProjector;

        public singleUndirectedRelationshipProjector( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            this.rel = rel;
            this.tailProjector = tailProjector;
            Product.$init$( this );
        }

        public Expression rel()
        {
            return this.rel;
        }

        public SlottedProjectedPath.Projector tailProjector()
        {
            return this.tailProjector;
        }

        public PathValueBuilder apply( final ExecutionContext ctx, final QueryState state, final PathValueBuilder builder )
        {
            return this.tailProjector().apply( ctx, state,
                    SlottedProjectedPath$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$expressions$SlottedProjectedPath$$addUndirected(
                            this.rel().apply( ctx, state ), state, builder ) );
        }

        public Seq<Expression> arguments()
        {
            return (Seq) ((TraversableLike) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new Expression[]{this.rel()}) ))).
            $plus$plus( this.tailProjector().arguments(), scala.collection.Seq..MODULE$.canBuildFrom());
        }

        public SlottedProjectedPath.singleUndirectedRelationshipProjector copy( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleUndirectedRelationshipProjector( rel, tailProjector );
        }

        public Expression copy$default$1()
        {
            return this.rel();
        }

        public SlottedProjectedPath.Projector copy$default$2()
        {
            return this.tailProjector();
        }

        public String productPrefix()
        {
            return "singleUndirectedRelationshipProjector";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.rel();
                break;
            case 1:
                var10000 = this.tailProjector();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlottedProjectedPath.singleUndirectedRelationshipProjector;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label63:
                {
                    boolean var2;
                    if ( x$1 instanceof SlottedProjectedPath.singleUndirectedRelationshipProjector )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label45:
                        {
                            label54:
                            {
                                SlottedProjectedPath.singleUndirectedRelationshipProjector var4 =
                                        (SlottedProjectedPath.singleUndirectedRelationshipProjector) x$1;
                                Expression var10000 = this.rel();
                                Expression var5 = var4.rel();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label54;
                                }

                                SlottedProjectedPath.Projector var7 = this.tailProjector();
                                SlottedProjectedPath.Projector var6 = var4.tailProjector();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label54;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label54;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label45;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label63;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class singleUndirectedRelationshipProjector$
            extends AbstractFunction2<Expression,SlottedProjectedPath.Projector,SlottedProjectedPath.singleUndirectedRelationshipProjector>
            implements Serializable
    {
        public static SlottedProjectedPath.singleUndirectedRelationshipProjector$ MODULE$;

        static
        {
            new SlottedProjectedPath.singleUndirectedRelationshipProjector$();
        }

        public singleUndirectedRelationshipProjector$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "singleUndirectedRelationshipProjector";
        }

        public SlottedProjectedPath.singleUndirectedRelationshipProjector apply( final Expression rel, final SlottedProjectedPath.Projector tailProjector )
        {
            return new SlottedProjectedPath.singleUndirectedRelationshipProjector( rel, tailProjector );
        }

        public Option<Tuple2<Expression,SlottedProjectedPath.Projector>> unapply( final SlottedProjectedPath.singleUndirectedRelationshipProjector x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new Tuple2( x$0.rel(), x$0.tailProjector() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
