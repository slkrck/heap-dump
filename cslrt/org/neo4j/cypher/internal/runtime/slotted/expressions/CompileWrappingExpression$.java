package org.neo4j.cypher.internal.runtime.slotted.expressions;

import org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class CompileWrappingExpression$ extends AbstractFunction2<CompiledExpression,Expression,CompileWrappingExpression> implements Serializable
{
    public static CompileWrappingExpression$ MODULE$;

    static
    {
        new CompileWrappingExpression$();
    }

    private CompileWrappingExpression$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "CompileWrappingExpression";
    }

    public CompileWrappingExpression apply( final CompiledExpression ce, final Expression legacy )
    {
        return new CompileWrappingExpression( ce, legacy );
    }

    public Option<Tuple2<CompiledExpression,Expression>> unapply( final CompileWrappingExpression x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.ce(), x$0.legacy() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
