package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.physicalplanning.Slot;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class Descending$ extends AbstractFunction1<Slot,Descending> implements Serializable
{
    public static Descending$ MODULE$;

    static
    {
        new Descending$();
    }

    private Descending$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "Descending";
    }

    public Descending apply( final Slot slot )
    {
        return new Descending( slot );
    }

    public Option<Slot> unapply( final Descending x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.slot() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
