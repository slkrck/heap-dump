package org.neo4j.cypher.internal.runtime.slotted;

import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.ValuePopulation;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.mutable.OpenHashMap;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ArrayResultExecutionContextFactory implements Product, Serializable
{
    private final Seq<Tuple2<String,Expression>> columns;
    private final Expression[] columnExpressionArray;
    private final int columnArraySize;
    private final OpenHashMap<String,Object> columnIndexMap;
    private ArrayResultExecutionContext freeExecutionContextInstance;

    public ArrayResultExecutionContextFactory( final Seq<Tuple2<String,Expression>> columns )
    {
        this.columns = columns;
        Product.$init$( this );
        this.columnExpressionArray = (Expression[]) ((TraversableOnce) columns.map( ( x$1 ) -> {
            return (Expression) x$1._2();
        }, scala.collection.Seq..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Expression.class ));
        this.columnArraySize = this.columnExpressionArray().length;
        OpenHashMap m = new OpenHashMap( columns.length() );
        IntRef index = IntRef.create( 0 );
        columns.foreach( ( x0$1 ) -> {
            $anonfun$columnIndexMap$1( m, index, x0$1 );
            return BoxedUnit.UNIT;
        } );
        this.columnIndexMap = m;
    }

    public static Option<Seq<Tuple2<String,Expression>>> unapply( final ArrayResultExecutionContextFactory x$0 )
    {
        return ArrayResultExecutionContextFactory$.MODULE$.unapply( var0 );
    }

    public static ArrayResultExecutionContextFactory apply( final Seq<Tuple2<String,Expression>> columns )
    {
        return ArrayResultExecutionContextFactory$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Seq<Tuple2<String,Expression>>,A> andThen( final Function1<ArrayResultExecutionContextFactory,A> g )
    {
        return ArrayResultExecutionContextFactory$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ArrayResultExecutionContextFactory> compose( final Function1<A,Seq<Tuple2<String,Expression>>> g )
    {
        return ArrayResultExecutionContextFactory$.MODULE$.compose( var0 );
    }

    public Seq<Tuple2<String,Expression>> columns()
    {
        return this.columns;
    }

    private Expression[] columnExpressionArray()
    {
        return this.columnExpressionArray;
    }

    private int columnArraySize()
    {
        return this.columnArraySize;
    }

    private OpenHashMap<String,Object> columnIndexMap()
    {
        return this.columnIndexMap;
    }

    public ArrayResultExecutionContext newResult( final ExecutionContext context, final QueryState state, final boolean prePopulate )
    {
        ArrayResultExecutionContext result = this.allocateExecutionContext();
        AnyValue[] resultArray = result.resultArray();
        int index = 0;
        if ( prePopulate )
        {
            while ( index < this.columnArraySize() )
            {
                AnyValue value = this.columnExpressionArray()[index].apply( context, state );
                ValuePopulation.populate( value );
                resultArray[index] = value;
                ++index;
            }
        }
        else
        {
            while ( index < this.columnArraySize() )
            {
                resultArray[index] = this.columnExpressionArray()[index].apply( context, state );
                ++index;
            }
        }

        return result;
    }

    private ArrayResultExecutionContext freeExecutionContextInstance()
    {
        return this.freeExecutionContextInstance;
    }

    private void freeExecutionContextInstance_$eq( final ArrayResultExecutionContext x$1 )
    {
        this.freeExecutionContextInstance = x$1;
    }

    private ArrayResultExecutionContext allocateExecutionContext()
    {
        ArrayResultExecutionContext var10000;
        if ( this.freeExecutionContextInstance() != null )
        {
            ArrayResultExecutionContext context = this.freeExecutionContextInstance();
            this.freeExecutionContextInstance_$eq( (ArrayResultExecutionContext) null );
            var10000 = context;
        }
        else
        {
            var10000 = this.createNewExecutionContext();
        }

        return var10000;
    }

    public void releaseExecutionContext( final ArrayResultExecutionContext executionContext )
    {
        this.freeExecutionContextInstance_$eq( executionContext );
    }

    private ArrayResultExecutionContext createNewExecutionContext()
    {
        AnyValue[] resultArray = new AnyValue[this.columnArraySize()];
        return new ArrayResultExecutionContext( resultArray, this.columnIndexMap(), this );
    }

    public ArrayResultExecutionContextFactory copy( final Seq<Tuple2<String,Expression>> columns )
    {
        return new ArrayResultExecutionContextFactory( columns );
    }

    public Seq<Tuple2<String,Expression>> copy$default$1()
    {
        return this.columns();
    }

    public String productPrefix()
    {
        return "ArrayResultExecutionContextFactory";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.columns();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ArrayResultExecutionContextFactory;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof ArrayResultExecutionContextFactory )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        ArrayResultExecutionContextFactory var4 = (ArrayResultExecutionContextFactory) x$1;
                        Seq var10000 = this.columns();
                        Seq var5 = var4.columns();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
