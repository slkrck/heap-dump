package org.neo4j.cypher.internal.runtime.slotted;

import java.util.Comparator;

import org.neo4j.cypher.internal.ir.VarPatternLength;
import org.neo4j.cypher.internal.logical.plans.AbstractSelectOrSemiApply;
import org.neo4j.cypher.internal.logical.plans.AbstractSemiApply;
import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.AllNodesScan;
import org.neo4j.cypher.internal.logical.plans.AntiConditionalApply;
import org.neo4j.cypher.internal.logical.plans.Apply;
import org.neo4j.cypher.internal.logical.plans.Argument;
import org.neo4j.cypher.internal.logical.plans.AssertSameNode;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.ConditionalApply;
import org.neo4j.cypher.internal.logical.plans.Create;
import org.neo4j.cypher.internal.logical.plans.DeleteExpression;
import org.neo4j.cypher.internal.logical.plans.DeleteNode;
import org.neo4j.cypher.internal.logical.plans.DeletePath;
import org.neo4j.cypher.internal.logical.plans.DeleteRelationship;
import org.neo4j.cypher.internal.logical.plans.DetachDeleteExpression;
import org.neo4j.cypher.internal.logical.plans.DetachDeleteNode;
import org.neo4j.cypher.internal.logical.plans.DetachDeletePath;
import org.neo4j.cypher.internal.logical.plans.Distinct;
import org.neo4j.cypher.internal.logical.plans.DropResult;
import org.neo4j.cypher.internal.logical.plans.Eager;
import org.neo4j.cypher.internal.logical.plans.EmptyResult;
import org.neo4j.cypher.internal.logical.plans.ErrorPlan;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.ForeachApply;
import org.neo4j.cypher.internal.logical.plans.IndexOrder;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LockNodes;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.MergeCreateNode;
import org.neo4j.cypher.internal.logical.plans.MergeCreateRelationship;
import org.neo4j.cypher.internal.logical.plans.NodeByLabelScan;
import org.neo4j.cypher.internal.logical.plans.NodeHashJoin;
import org.neo4j.cypher.internal.logical.plans.NodeIndexScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexSeek;
import org.neo4j.cypher.internal.logical.plans.NodeUniqueIndexSeek;
import org.neo4j.cypher.internal.logical.plans.Optional;
import org.neo4j.cypher.internal.logical.plans.OptionalExpand;
import org.neo4j.cypher.internal.logical.plans.OrderedAggregation;
import org.neo4j.cypher.internal.logical.plans.OrderedDistinct;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.logical.plans.QueryExpression;
import org.neo4j.cypher.internal.logical.plans.RemoveLabels;
import org.neo4j.cypher.internal.logical.plans.RollUpApply;
import org.neo4j.cypher.internal.logical.plans.Selection;
import org.neo4j.cypher.internal.logical.plans.SetLabels;
import org.neo4j.cypher.internal.logical.plans.SetNodePropertiesFromMap;
import org.neo4j.cypher.internal.logical.plans.SetNodeProperty;
import org.neo4j.cypher.internal.logical.plans.SetPropertiesFromMap;
import org.neo4j.cypher.internal.logical.plans.SetProperty;
import org.neo4j.cypher.internal.logical.plans.SetRelationshipPropertiesFromMap;
import org.neo4j.cypher.internal.logical.plans.SetRelationshipProperty;
import org.neo4j.cypher.internal.logical.plans.Skip;
import org.neo4j.cypher.internal.logical.plans.Sort;
import org.neo4j.cypher.internal.logical.plans.Ties;
import org.neo4j.cypher.internal.logical.plans.Top;
import org.neo4j.cypher.internal.logical.plans.Union;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.logical.plans.ValueHashJoin;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlan;
import org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes;
import org.neo4j.cypher.internal.physicalplanning.Slot;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration;
import org.neo4j.cypher.internal.physicalplanning.SlotConfiguration$;
import org.neo4j.cypher.internal.physicalplanning.SlotConfigurationUtils$;
import org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty$;
import org.neo4j.cypher.internal.physicalplanning.VariablePredicates$;
import org.neo4j.cypher.internal.physicalplanning.ast.NodeFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckVariable;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipFromSlot;
import org.neo4j.cypher.internal.runtime.QueryIndexRegistrator;
import org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression;
import org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.True;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.DropResultPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.EagerAggregationPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.EmptyResultPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekMode;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.IndexSeekModeFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyType;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.OrderedAggregationPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.OrderedAggregationTableFactory;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeMapper;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.ProjectionPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.SortPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Top1Pipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.Top1WithTiesPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.TopNPipe;
import org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTableFactory;
import org.neo4j.cypher.internal.runtime.slotted.aggregation.SlottedGroupingAggTable;
import org.neo4j.cypher.internal.runtime.slotted.aggregation.SlottedNonGroupingAggTable;
import org.neo4j.cypher.internal.runtime.slotted.aggregation.SlottedOrderedGroupingAggTable;
import org.neo4j.cypher.internal.runtime.slotted.aggregation.SlottedOrderedNonGroupingAggTable;
import org.neo4j.cypher.internal.runtime.slotted.aggregation.SlottedPrimitiveGroupingAggTable;
import org.neo4j.cypher.internal.runtime.slotted.pipes.AllNodesScanSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.AllOrderedDistinctSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.AllOrderedDistinctSlottedPrimitivePipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ApplySlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ArgumentSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.CartesianProductSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ConditionalApplySlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.CreateNodeSlottedCommand;
import org.neo4j.cypher.internal.runtime.slotted.pipes.CreateRelationshipSlottedCommand;
import org.neo4j.cypher.internal.runtime.slotted.pipes.CreateSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.DistinctSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.DistinctSlottedPrimitivePipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.DistinctSlottedSinglePrimitivePipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.EagerSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ExpandAllSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ExpandIntoSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ForeachSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.MergeCreateNodeSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.MergeCreateRelationshipSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.NodeHashJoinSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.NodeHashJoinSlottedSingleNodePipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.NodeIndexScanSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.NodeIndexSeekSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.NodesByLabelScanSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.OptionalExpandAllSlottedPipe$;
import org.neo4j.cypher.internal.runtime.slotted.pipes.OptionalExpandIntoSlottedPipe$;
import org.neo4j.cypher.internal.runtime.slotted.pipes.OptionalSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.OrderedDistinctSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.OrderedDistinctSlottedPrimitivePipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.OrderedDistinctSlottedSinglePrimitivePipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ProduceResultSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.RollUpApplySlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.UnionSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.UnwindSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.ValueHashJoinSlottedPipe;
import org.neo4j.cypher.internal.runtime.slotted.pipes.VarLengthExpandSlottedPipe;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.expressions.Equals;
import org.neo4j.cypher.internal.v4_0.expressions.LabelName;
import org.neo4j.cypher.internal.v4_0.expressions.LabelToken;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.cypher.internal.v4_0.expressions.RelTypeName;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.expressions.SignedDecimalIntegerLiteral;
import org.neo4j.exceptions.InternalException;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.GenIterable;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ArrayOps.ofRef;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;
import scala.runtime.LazyRef;
import scala.runtime.NonLocalReturnControl;

@JavaDocToJava
public class SlottedPipeMapper implements PipeMapper
{
    private final PipeMapper fallback;
    private final ExpressionConverters expressionConverters;
    private final PhysicalPlan physicalPlan;
    private final boolean readOnly;
    private final QueryIndexRegistrator indexRegistrator;
    private final SemanticTable semanticTable;

    public SlottedPipeMapper( final PipeMapper fallback, final ExpressionConverters expressionConverters, final PhysicalPlan physicalPlan,
            final boolean readOnly, final QueryIndexRegistrator indexRegistrator, final SemanticTable semanticTable )
    {
        this.fallback = fallback;
        this.expressionConverters = expressionConverters;
        this.physicalPlan = physicalPlan;
        this.readOnly = readOnly;
        this.indexRegistrator = indexRegistrator;
        this.semanticTable = semanticTable;
    }

    public static UnionSlottedPipe.RowMapping computeUnionMapping( final SlotConfiguration in, final SlotConfiguration out )
    {
        return SlottedPipeMapper$.MODULE$.computeUnionMapping( var0, var1 );
    }

    public static Seq<Tuple2<String,Expression>> createProjectionsForResult( final Seq<String> columns, final SlotConfiguration slots )
    {
        return SlottedPipeMapper$.MODULE$.createProjectionsForResult( var0, var1 );
    }

    private static final boolean checkSharedSlots$1( final Seq slots, final int expectedSlots )
    {
        Object var2 = new Object();

        boolean var10000;
        try
        {
            Seq sorted = (Seq) slots.sortBy( ( x$15 ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$verifyOnlyArgumentsAreSharedSlots$4( x$15 ) );
            }, scala.math.Ordering.Int..MODULE$);
            IntRef prevOffset = IntRef.create( -1 );
            sorted.foreach( ( slot ) -> {
                $anonfun$verifyOnlyArgumentsAreSharedSlots$5( prevOffset, var2, slot );
                return BoxedUnit.UNIT;
            } );
            var10000 = prevOffset.elem + 1 == expectedSlots;
        }
        catch ( NonLocalReturnControl var6 )
        {
            if ( var6.key() != var2 )
            {
                throw var6;
            }

            var10000 = var6.value$mcZ$sp();
        }

        return var10000;
    }

    private static final boolean sameSlotsInOrder$1( final Seq a, final Seq b )
    {
        return ((IterableLike) ((IterableLike) a.sortBy( ( x$16 ) -> {
            return (String) x$16._1();
        }, scala.math.Ordering.String..MODULE$)).zip( (GenIterable) b.sortBy( ( x$17 ) -> {
            return (String) x$17._1();
        }, scala.math.Ordering.String..MODULE$ ),scala.collection.Seq..MODULE$.canBuildFrom())).forall( ( x0$13 ) -> {
        return BoxesRunTime.boxToBoolean( $anonfun$verifyArgumentsAreTheSameOnBothSides$7( x0$13 ) );
    } );
    }

    public Pipe onLeaf( final LogicalPlan plan )
    {
        int id = plan.id();
        Function1 convertExpressions = ( e ) -> {
            return this.expressionConverters.toCommandExpression( id, e );
        };
        SlotConfiguration slots = (SlotConfiguration) this.physicalPlan.slotConfigurations().apply( id );
        SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan.argumentSizes().apply( id );
        SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( slots );
        Object var2;
        if ( plan instanceof AllNodesScan )
        {
            AllNodesScan var9 = (AllNodesScan) plan;
            String column = var9.idName();
            var2 = new AllNodesScanSlottedPipe( column, slots, argumentSize, id );
        }
        else if ( plan instanceof NodeIndexScan )
        {
            NodeIndexScan var11 = (NodeIndexScan) plan;
            String column = var11.idName();
            LabelToken label = var11.label();
            Seq properties = var11.properties();
            IndexOrder indexOrder = var11.indexOrder();
            var2 = new NodeIndexScanSlottedPipe( column, label, (Seq) properties.map( ( x$1 ) -> {
                return SlottedIndexedProperty$.MODULE$.apply( column, x$1, slots );
            }, scala.collection.Seq..MODULE$.canBuildFrom() ), this.indexRegistrator.registerQueryIndex( label,
                    properties ), indexOrder, slots, argumentSize, id);
        }
        else if ( plan instanceof NodeIndexSeek )
        {
            NodeIndexSeek var16 = (NodeIndexSeek) plan;
            String column = var16.idName();
            LabelToken label = var16.label();
            Seq properties = var16.properties();
            QueryExpression valueExpr = var16.valueExpr();
            IndexOrder indexOrder = var16.indexOrder();
            IndexSeekMode indexSeekMode = (new IndexSeekModeFactory( false, this.readOnly )).fromQueryExpression( valueExpr );
            var2 = new NodeIndexSeekSlottedPipe( column, label, ((TraversableOnce) properties.map( ( x$2 ) -> {
                return SlottedIndexedProperty$.MODULE$.apply( column, x$2, slots );
            }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toIndexedSeq(), this.indexRegistrator.registerQueryIndex( label, properties ), valueExpr.map(
                    convertExpressions ), indexSeekMode, indexOrder, slots, argumentSize, id);
        }
        else if ( plan instanceof NodeUniqueIndexSeek )
        {
            NodeUniqueIndexSeek var23 = (NodeUniqueIndexSeek) plan;
            String column = var23.idName();
            LabelToken label = var23.label();
            Seq properties = var23.properties();
            QueryExpression valueExpr = var23.valueExpr();
            IndexOrder indexOrder = var23.indexOrder();
            IndexSeekMode indexSeekMode = (new IndexSeekModeFactory( true, this.readOnly )).fromQueryExpression( valueExpr );
            var2 = new NodeIndexSeekSlottedPipe( column, label, ((TraversableOnce) properties.map( ( x$3 ) -> {
                return SlottedIndexedProperty$.MODULE$.apply( column, x$3, slots );
            }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toIndexedSeq(), this.indexRegistrator.registerQueryIndex( label, properties ), valueExpr.map(
                    convertExpressions ), indexSeekMode, indexOrder, slots, argumentSize, id);
        }
        else if ( plan instanceof NodeByLabelScan )
        {
            NodeByLabelScan var30 = (NodeByLabelScan) plan;
            String column = var30.idName();
            LabelName label = var30.label();
            this.indexRegistrator.registerLabelScan();
            var2 = new NodesByLabelScanSlottedPipe( column, org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.apply( label,
                    this.semanticTable ), slots, argumentSize, id);
        }
        else if ( plan instanceof Argument )
        {
            var2 = new ArgumentSlottedPipe( slots, argumentSize, id );
        }
        else
        {
            var2 = this.fallback.onLeaf( plan );
        }

        ((Pipe) var2).executionContextFactory_$eq( new SlottedExecutionContextFactory( slots ) );
        return (Pipe) var2;
    }

    public Pipe onOneChildPlan( final LogicalPlan plan, final Pipe source )
    {
        int id = plan.id();
        Function1 convertExpressions = ( e ) -> {
            return this.expressionConverters.toCommandExpression( id, e );
        };
        SlotConfiguration slots = (SlotConfiguration) this.physicalPlan.slotConfigurations().apply( id );
        SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( slots );
        boolean var14 = false;
        Expand var15 = null;
        boolean var16 = false;
        OptionalExpand var17 = null;
        boolean var18 = false;
        Top var19 = null;
        Object var3;
        if ( plan instanceof ProduceResult )
        {
            ProduceResult var21 = (ProduceResult) plan;
            Seq columns = var21.columns();
            Seq runtimeColumns = SlottedPipeMapper$.MODULE$.createProjectionsForResult( columns, slots );
            var3 = new ProduceResultSlottedPipe( source, runtimeColumns, id );
        }
        else
        {
            label304:
            {
                if ( plan instanceof Expand )
                {
                    var14 = true;
                    var15 = (Expand) plan;
                    String from = var15.from();
                    SemanticDirection dir = var15.dir();
                    Seq types = var15.types();
                    String to = var15.to();
                    String relName = var15.relName();
                    ExpansionMode var29 = var15.mode();
                    if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( var29 )){
                    Slot fromSlot = slots.apply( from );
                    int relOffset = slots.getLongOffsetFor( relName );
                    int toOffset = slots.getLongOffsetFor( to );
                    var3 = new ExpandAllSlottedPipe( source, fromSlot, relOffset, toOffset, dir,
                            org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..MODULE$.apply(
                            (RelTypeName[]) types.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable),slots, id);
                    break label304;
                }
                }

                if ( var14 )
                {
                    String from = var15.from();
                    SemanticDirection dir = var15.dir();
                    Seq types = var15.types();
                    String to = var15.to();
                    String relName = var15.relName();
                    ExpansionMode var38 = var15.mode();
                    if ( org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$.equals( var38 )){
                    Slot fromSlot = slots.apply( from );
                    int relOffset = slots.getLongOffsetFor( relName );
                    Slot toSlot = slots.apply( to );
                    var3 = new ExpandIntoSlottedPipe( source, fromSlot, relOffset, toSlot, dir,
                            org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..MODULE$.apply(
                            (RelTypeName[]) types.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable),slots, id);
                    break label304;
                }
                }

                if ( plan instanceof OptionalExpand )
                {
                    var16 = true;
                    var17 = (OptionalExpand) plan;
                    String fromName = var17.from();
                    SemanticDirection dir = var17.dir();
                    Seq types = var17.types();
                    String toName = var17.to();
                    String relName = var17.relName();
                    ExpansionMode var47 = var17.mode();
                    Option predicate = var17.predicate();
                    if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( var47 )){
                    Slot fromSlot = slots.apply( fromName );
                    int relOffset = slots.getLongOffsetFor( relName );
                    int toOffset = slots.getLongOffsetFor( toName );
                    var3 = OptionalExpandAllSlottedPipe$.MODULE$.apply( source, fromSlot, relOffset, toOffset, dir,
                            org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..MODULE$.apply(
                            (RelTypeName[]) types.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable),
                    slots, predicate.map( convertExpressions ), id);
                    break label304;
                }
                }

                if ( var16 )
                {
                    String fromName = var17.from();
                    SemanticDirection dir = var17.dir();
                    Seq types = var17.types();
                    String toName = var17.to();
                    String relName = var17.relName();
                    ExpansionMode var57 = var17.mode();
                    Option predicate = var17.predicate();
                    if ( org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$.equals( var57 )){
                    Slot fromSlot = slots.apply( fromName );
                    int relOffset = slots.getLongOffsetFor( relName );
                    Slot toSlot = slots.apply( toName );
                    var3 = OptionalExpandIntoSlottedPipe$.MODULE$.apply( source, fromSlot, relOffset, toSlot, dir,
                            org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..MODULE$.apply(
                            (RelTypeName[]) types.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable),
                    slots, predicate.map( convertExpressions ), id);
                    break label304;
                }
                }

                if ( plan instanceof VarExpand )
                {
                    VarExpand var62 = (VarExpand) plan;
                    LogicalPlan sourcePlan = var62.source();
                    String fromName = var62.from();
                    SemanticDirection dir = var62.dir();
                    SemanticDirection projectedDir = var62.projectedDir();
                    Seq types = var62.types();
                    String toName = var62.to();
                    String relName = var62.relName();
                    VarPatternLength var70 = var62.length();
                    ExpansionMode expansionMode = var62.mode();
                    Option nodePredicate = var62.nodePredicate();
                    Option relationshipPredicate = var62.relationshipPredicate();
                    if ( var70 != null )
                    {
                        int min = var70.min();
                        Option max = var70.max();
                        boolean var9;
                        if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( expansionMode )){
                        var9 = true;
                    } else{
                        if ( !org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$.equals( expansionMode )){
                            throw new MatchError( expansionMode );
                        }

                        var9 = false;
                    }

                        Slot fromSlot = slots.apply( fromName );
                        int relOffset = slots.getReferenceOffsetFor( relName );
                        Slot toSlot = slots.apply( toName );
                        SlotConfiguration sourceSlots = (SlotConfiguration) this.physicalPlan.slotConfigurations().apply( sourcePlan.id() );
                        int tempNodeOffset = VariablePredicates$.MODULE$.expressionSlotForPredicate( nodePredicate );
                        int tempRelationshipOffset = VariablePredicates$.MODULE$.expressionSlotForPredicate( relationshipPredicate );
                        SlotConfiguration.Size argumentSize = new SlotConfiguration.Size( sourceSlots.numberOfLongs(), sourceSlots.numberOfReferences() );
                        var3 = new VarLengthExpandSlottedPipe( source, fromSlot, relOffset, toSlot, dir, projectedDir,
                                org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes..MODULE$.apply(
                                (RelTypeName[]) types.toArray( scala.reflect.ClassTag..MODULE$.apply( RelTypeName.class ) ), this.semanticTable),
                        min, max, var9, slots, tempNodeOffset, tempRelationshipOffset, (Expression) nodePredicate.map( ( x ) -> {
                            return this.expressionConverters.toCommandExpression( id, x.predicate() );
                        } ).getOrElse( () -> {
                            return new True();
                        } ), (Expression) relationshipPredicate.map( ( x ) -> {
                            return this.expressionConverters.toCommandExpression( id, x.predicate() );
                        } ).getOrElse( () -> {
                            return new True();
                        } ), argumentSize, id);
                        break label304;
                    }
                }

                if ( plan instanceof Optional )
                {
                    Optional var85 = (Optional) plan;
                    LogicalPlan inner = var85.source();
                    Set symbols = var85.protectedSymbols();
                    Set nullableKeys = (Set) inner.availableSymbols().$minus$minus( symbols );
                    Slot[] nullableSlots = (Slot[]) ((TraversableOnce) nullableKeys.map( ( k ) -> {
                        return (Slot) slots.get( k ).get();
                    }, scala.collection.immutable.Set..MODULE$.canBuildFrom())).toArray( scala.reflect.ClassTag..MODULE$.apply( Slot.class ));
                    SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan.argumentSizes().apply( plan.id() );
                    var3 = new OptionalSlottedPipe( source, scala.Predef..MODULE$.wrapRefArray( (Object[]) nullableSlots ), slots, argumentSize, id);
                }
                else if ( plan instanceof Projection )
                {
                    Projection var91 = (Projection) plan;
                    Map expressions = var91.projectExpressions();
                    Map toProject = (Map) expressions.collect( new Serializable( (SlottedPipeMapper) null, slots )
                    {
                        public static final long serialVersionUID = 0L;
                        private final SlotConfiguration slots$2;

                        public
                        {
                            this.slots$2 = slots$2;
                        }

                        public final <A1 extends Tuple2<String,org.neo4j.cypher.internal.v4_0.expressions.Expression>, B1> B1 applyOrElse( final A1 x1, final
                                Function1<A1,B1> default )
                        {
                            Object var3;
                            if ( x1 != null )
                            {
                                String k = (String) x1._1();
                                org.neo4j.cypher.internal.v4_0.expressions.Expression e = (org.neo4j.cypher.internal.v4_0.expressions.Expression) x1._2();
                                if ( SlotConfiguration$.MODULE$.isRefSlotAndNotAlias( this.slots$2, k ) )
                                {
                                    var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( k ), e);
                                    return var3;
                                }
                            }

                            var3 = var2.apply( x1 );
                            return var3;
                        }

                        public final boolean isDefinedAt( final Tuple2<String,org.neo4j.cypher.internal.v4_0.expressions.Expression> x1 )
                        {
                            boolean var2;
                            if ( x1 != null )
                            {
                                String k = (String) x1._1();
                                if ( SlotConfiguration$.MODULE$.isRefSlotAndNotAlias( this.slots$2, k ) )
                                {
                                    var2 = true;
                                    return var2;
                                }
                            }

                            var2 = false;
                            return var2;
                        }
                    }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                    var3 = new ProjectionPipe( source, this.expressionConverters.toCommandProjection( id, toProject ), id );
                }
                else if ( plan instanceof Create )
                {
                    Create var94 = (Create) plan;
                    Seq nodes = var94.nodes();
                    Seq relationships = var94.relationships();
                    var3 = new CreateSlottedPipe( source, ((TraversableOnce) nodes.map( ( n ) -> {
                        return new CreateNodeSlottedCommand( slots.getLongOffsetFor( n.idName() ), (Seq) n.labels().map( ( name ) -> {
                            return org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.apply( name, this.semanticTable );
                        }, scala.collection.Seq..MODULE$.canBuildFrom() ),n.properties().map( convertExpressions ));
                    }, scala.collection.Seq..MODULE$.canBuildFrom()) ).toIndexedSeq(), ((TraversableOnce) relationships.map( ( r ) -> {
                        return new CreateRelationshipSlottedCommand( slots.getLongOffsetFor( r.idName() ),
                                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( slots.apply( r.startNode() ),
                                        SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() ),
                                new LazyType( r.relType().name() ),
                                SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( slots.apply( r.endNode() ),
                                        SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() ),
                                r.properties().map( convertExpressions ), r.idName(), r.startNode(), r.endNode() );
                    }, scala.collection.Seq..MODULE$.canBuildFrom())).toIndexedSeq(), id);
                }
                else if ( plan instanceof MergeCreateNode )
                {
                    MergeCreateNode var97 = (MergeCreateNode) plan;
                    String idName = var97.idName();
                    Seq labels = var97.labels();
                    Option properties = var97.properties();
                    var3 = new MergeCreateNodeSlottedPipe( source,
                            new CreateNodeSlottedCommand( slots.getLongOffsetFor( idName ), (Seq) labels.map( ( namex ) -> {
                                return org.neo4j.cypher.internal.runtime.interpreted.pipes.LazyLabel..MODULE$.apply( namex, this.semanticTable );
                            }, scala.collection.Seq..MODULE$.canBuildFrom() ), properties.map( convertExpressions ) ), id);
                }
                else if ( plan instanceof MergeCreateRelationship )
                {
                    MergeCreateRelationship var101 = (MergeCreateRelationship) plan;
                    String idName = var101.idName();
                    String startNode = var101.startNode();
                    RelTypeName relType = var101.typ();
                    String endNode = var101.endNode();
                    Option properties = var101.properties();
                    var3 = new MergeCreateRelationshipSlottedPipe( source, new CreateRelationshipSlottedCommand( slots.getLongOffsetFor( idName ),
                            SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( slots.apply( startNode ),
                                    SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() ), new LazyType( relType.name() ),
                            SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( slots.apply( endNode ),
                                    SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2() ), properties.map( convertExpressions ),
                            idName, startNode, endNode ), id );
                }
                else if ( plan instanceof EmptyResult )
                {
                    var3 = new EmptyResultPipe( source, id );
                }
                else if ( plan instanceof DropResult )
                {
                    var3 = new DropResultPipe( source, id );
                }
                else if ( plan instanceof UnwindCollection )
                {
                    UnwindCollection var107 = (UnwindCollection) plan;
                    String name = var107.variable();
                    org.neo4j.cypher.internal.v4_0.expressions.Expression expression = var107.expression();
                    int offset = slots.getReferenceOffsetFor( name );
                    var3 = new UnwindSlottedPipe( source, (Expression) convertExpressions.apply( expression ), offset, slots, id );
                }
                else if ( plan instanceof Aggregation )
                {
                    Aggregation var111 = (Aggregation) plan;
                    Map groupingExpressions = var111.groupingExpressions();
                    Map aggregationExpression = var111.aggregationExpression();
                    Map aggregation = (Map) aggregationExpression.map( ( x0$1 ) -> {
                        if ( x0$1 != null )
                        {
                            String key = (String) x0$1._1();
                            org.neo4j.cypher.internal.v4_0.expressions.Expression expression =
                                    (org.neo4j.cypher.internal.v4_0.expressions.Expression) x0$1._2();
                            Tuple2 var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                BoxesRunTime.boxToInteger( slots.getReferenceOffsetFor( key ) ) ), (AggregationExpression) convertExpressions.apply(
                                expression ));
                            return var3;
                        }
                        else
                        {
                            throw new MatchError( x0$1 );
                        }
                    }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                    String[] keys = (String[]) groupingExpressions.keys().toArray( scala.reflect.ClassTag..MODULE$.apply( String.class ));
                    int[] longSlotGroupingValues = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) keys ))).
                    collect( new Serializable( (SlottedPipeMapper) null, slots, groupingExpressions )
                    {
                        public static final long serialVersionUID = 0L;
                        private final SlotConfiguration slots$2;
                        private final Map groupingExpressions$1;

                        public
                        {
                            this.slots$2 = slots$2;
                            this.groupingExpressions$1 = groupingExpressions$1;
                        }

                        public final <A1 extends String, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
                        {
                            Object var3;
                            if ( !this.slots$2.apply( x2 ).isLongSlot() )
                            {
                                var3 = var2.apply( x2 );
                            }
                            else
                            {
                                boolean var6 = false;
                                NullCheckVariable var7 = null;
                                org.neo4j.cypher.internal.v4_0.expressions.Expression var8 =
                                        (org.neo4j.cypher.internal.v4_0.expressions.Expression) this.groupingExpressions$1.apply( x2 );
                                int var4;
                                if ( var8 instanceof NodeFromSlot )
                                {
                                    NodeFromSlot var9 = (NodeFromSlot) var8;
                                    int offsetxxx = var9.offset();
                                    var4 = offsetxxx;
                                }
                                else if ( var8 instanceof RelationshipFromSlot )
                                {
                                    RelationshipFromSlot var11 = (RelationshipFromSlot) var8;
                                    int offset = var11.offset();
                                    var4 = offset;
                                }
                                else
                                {
                                    label45:
                                    {
                                        if ( var8 instanceof NullCheckVariable )
                                        {
                                            var6 = true;
                                            var7 = (NullCheckVariable) var8;
                                            LogicalVariable var13 = var7.inner();
                                            if ( var13 instanceof NodeFromSlot )
                                            {
                                                NodeFromSlot var14 = (NodeFromSlot) var13;
                                                int offsetx = var14.offset();
                                                var4 = offsetx;
                                                break label45;
                                            }
                                        }

                                        if ( !var6 )
                                        {
                                            throw new InternalException( (new StringBuilder( 71 )).append(
                                                    "Cannot build slotted aggregation pipe. Unexpected grouping expression: " ).append( var8 ).toString() );
                                        }

                                        LogicalVariable var16 = var7.inner();
                                        if ( !(var16 instanceof RelationshipFromSlot) )
                                        {
                                            throw new InternalException( (new StringBuilder( 71 )).append(
                                                    "Cannot build slotted aggregation pipe. Unexpected grouping expression: " ).append( var8 ).toString() );
                                        }

                                        RelationshipFromSlot var17 = (RelationshipFromSlot) var16;
                                        int offsetxx = var17.offset();
                                        var4 = offsetxx;
                                    }
                                }

                                var3 = BoxesRunTime.boxToInteger( var4 );
                            }

                            return var3;
                        }

                        public final boolean isDefinedAt( final String x2 )
                        {
                            boolean var2;
                            if ( this.slots$2.apply( x2 ).isLongSlot() )
                            {
                                var2 = true;
                            }
                            else
                            {
                                var2 = false;
                            }

                            return var2;
                        }
                    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
                    int[] longSlotGroupingKeys = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) keys ))).
                    collect( new Serializable( (SlottedPipeMapper) null, slots )
                    {
                        public static final long serialVersionUID = 0L;
                        private final SlotConfiguration slots$2;

                        public
                        {
                            this.slots$2 = slots$2;
                        }

                        public final <A1 extends String, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
                        {
                            Object var3;
                            if ( this.slots$2.apply( x3 ).isLongSlot() )
                            {
                                var3 = BoxesRunTime.boxToInteger( this.slots$2.apply( x3 ).offset() );
                            }
                            else
                            {
                                var3 = var2.apply( x3 );
                            }

                            return var3;
                        }

                        public final boolean isDefinedAt( final String x3 )
                        {
                            boolean var2;
                            if ( this.slots$2.apply( x3 ).isLongSlot() )
                            {
                                var2 = true;
                            }
                            else
                            {
                                var2 = false;
                            }

                            return var2;
                        }
                    }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
                    Product tableFactory = groupingExpressions.isEmpty() ? new SlottedNonGroupingAggTable.Factory( slots, aggregation ) : (
                            longSlotGroupingValues.length == groupingExpressions.size() && longSlotGroupingValues.length == longSlotGroupingKeys.length
                            ? new SlottedPrimitiveGroupingAggTable.Factory( slots, longSlotGroupingValues, longSlotGroupingKeys, aggregation )
                            : new SlottedGroupingAggTable.Factory( slots,
                                    this.expressionConverters.toGroupingExpression( id, groupingExpressions, (Seq) scala.collection.Seq..MODULE$.empty() ),
                            aggregation));
                    var3 = new EagerAggregationPipe( source, (AggregationTableFactory) tableFactory, id );
                }
                else if ( plan instanceof OrderedAggregation )
                {
                    OrderedAggregation var119 = (OrderedAggregation) plan;
                    Map groupingExpressions = var119.groupingExpressions();
                    Map aggregationExpression = var119.aggregationExpression();
                    Seq orderToLeverage = var119.orderToLeverage();
                    Map aggregation = (Map) aggregationExpression.map( ( x0$2 ) -> {
                        if ( x0$2 != null )
                        {
                            String key = (String) x0$2._1();
                            org.neo4j.cypher.internal.v4_0.expressions.Expression expression =
                                    (org.neo4j.cypher.internal.v4_0.expressions.Expression) x0$2._2();
                            Tuple2 var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                BoxesRunTime.boxToInteger( slots.getReferenceOffsetFor( key ) ) ), (AggregationExpression) convertExpressions.apply(
                                expression ));
                            return var3;
                        }
                        else
                        {
                            throw new MatchError( x0$2 );
                        }
                    }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                    Tuple2 var125 = groupingExpressions.partition( ( x0$3 ) -> {
                        return BoxesRunTime.boxToBoolean( $anonfun$onOneChildPlan$13( orderToLeverage, x0$3 ) );
                    } );
                    if ( var125 == null )
                    {
                        throw new MatchError( var125 );
                    }

                    Map orderedGroupingExpressions = (Map) var125._1();
                    Map unorderedGroupingExpressions = (Map) var125._2();
                    Tuple2 var8 = new Tuple2( orderedGroupingExpressions, unorderedGroupingExpressions );
                    Map orderedGroupingExpressions = (Map) var8._1();
                    Map unorderedGroupingExpressions = (Map) var8._2();
                    GroupingExpression orderedGroupingColumns =
                            this.expressionConverters.toGroupingExpression( id, orderedGroupingExpressions, orderToLeverage );
                    GroupingExpression unorderedGroupingColumns =
                            this.expressionConverters.toGroupingExpression( id, unorderedGroupingExpressions, orderToLeverage );
                    OrderedAggregationTableFactory tableFactory =
                            unorderedGroupingColumns.isEmpty() ? new SlottedOrderedNonGroupingAggTable.Factory( slots, orderedGroupingColumns, aggregation )
                                                               : new SlottedOrderedGroupingAggTable.Factory( slots, orderedGroupingColumns,
                                                                       unorderedGroupingColumns, aggregation );
                    var3 = new OrderedAggregationPipe( source, (OrderedAggregationTableFactory) tableFactory, id );
                }
                else if ( plan instanceof Distinct )
                {
                    Distinct var133 = (Distinct) plan;
                    Map groupingExpressions = var133.groupingExpressions();
                    var3 = this.chooseDistinctPipe( groupingExpressions, (Seq) scala.collection.Seq..MODULE$.empty(), slots, source, id);
                }
                else if ( plan instanceof OrderedDistinct )
                {
                    OrderedDistinct var135 = (OrderedDistinct) plan;
                    Map groupingExpressions = var135.groupingExpressions();
                    Seq orderToLeverage = var135.orderToLeverage();
                    var3 = this.chooseDistinctPipe( groupingExpressions, orderToLeverage, slots, source, id );
                }
                else
                {
                    label317:
                    {
                        if ( plan instanceof Top )
                        {
                            var18 = true;
                            var19 = (Top) plan;
                            Seq sortItems = var19.sortItems();
                            if ( sortItems.isEmpty() )
                            {
                                var3 = source;
                                break label317;
                            }
                        }

                        if ( var18 )
                        {
                            Seq sortItems = var19.sortItems();
                            org.neo4j.cypher.internal.v4_0.expressions.Expression var140 = var19.limit();
                            if ( var140 instanceof SignedDecimalIntegerLiteral )
                            {
                                SignedDecimalIntegerLiteral var141 = (SignedDecimalIntegerLiteral) var140;
                                String var142 = var141.stringVal();
                                if ( "1".equals( var142 ) )
                                {
                                    var3 = new Top1Pipe( source, SlottedExecutionContextOrdering$.MODULE$.asComparator( (Seq) sortItems.map( ( x$5 ) -> {
                                        return this.translateColumnOrder( slots, x$5 );
                                    }, scala.collection.Seq..MODULE$.canBuildFrom() ) ), id);
                                    break label317;
                                }
                            }
                        }

                        if ( var18 )
                        {
                            Seq sortItems = var19.sortItems();
                            org.neo4j.cypher.internal.v4_0.expressions.Expression limit = var19.limit();
                            var3 = new TopNPipe( source, (Expression) convertExpressions.apply( limit ),
                                    SlottedExecutionContextOrdering$.MODULE$.asComparator( (Seq) sortItems.map( ( x$6 ) -> {
                                        return this.translateColumnOrder( slots, x$6 );
                                    }, scala.collection.Seq..MODULE$.canBuildFrom() ) ), id);
                        }
                        else
                        {
                            label318:
                            {
                                if ( plan instanceof Limit )
                                {
                                    Limit var145 = (Limit) plan;
                                    org.neo4j.cypher.internal.v4_0.expressions.Expression count = var145.count();
                                    Ties var147 = var145.ties();
                                    if ( org.neo4j.cypher.internal.logical.plans.IncludeTies..MODULE$.equals( var147 )){
                                    Tuple2 var148 = new Tuple2( source, count );
                                    if ( var148 != null )
                                    {
                                        Pipe var149 = (Pipe) var148._1();
                                        org.neo4j.cypher.internal.v4_0.expressions.Expression var150 =
                                                (org.neo4j.cypher.internal.v4_0.expressions.Expression) var148._2();
                                        if ( var149 instanceof SortPipe )
                                        {
                                            SortPipe var151 = (SortPipe) var149;
                                            Pipe inner = var151.source();
                                            Comparator comparator = var151.comparator();
                                            if ( var150 instanceof SignedDecimalIntegerLiteral )
                                            {
                                                SignedDecimalIntegerLiteral var154 = (SignedDecimalIntegerLiteral) var150;
                                                String var155 = var154.stringVal();
                                                if ( "1".equals( var155 ) )
                                                {
                                                    Top1WithTiesPipe var7 = new Top1WithTiesPipe( inner, comparator, id );
                                                    var3 = var7;
                                                    break label318;
                                                }
                                            }
                                        }
                                    }

                                    throw new InternalException( "Including ties is only supported for very specific plans" );
                                }
                                }

                                boolean var6;
                                if ( plan instanceof Selection )
                                {
                                    var6 = true;
                                }
                                else if ( plan instanceof Limit )
                                {
                                    var6 = true;
                                }
                                else if ( plan instanceof ErrorPlan )
                                {
                                    var6 = true;
                                }
                                else if ( plan instanceof Skip )
                                {
                                    var6 = true;
                                }
                                else
                                {
                                    var6 = false;
                                }

                                if ( var6 )
                                {
                                    var3 = this.fallback.onOneChildPlan( plan, source );
                                }
                                else if ( plan instanceof Sort )
                                {
                                    Sort var156 = (Sort) plan;
                                    Seq sortItems = var156.sortItems();
                                    var3 = new SortPipe( source, SlottedExecutionContextOrdering$.MODULE$.asComparator( (Seq) sortItems.map( ( x$7 ) -> {
                                        return this.translateColumnOrder( slots, x$7 );
                                    }, scala.collection.Seq..MODULE$.canBuildFrom() ) ), id);
                                }
                                else if ( plan instanceof Eager )
                                {
                                    var3 = new EagerSlottedPipe( source, slots, id );
                                }
                                else
                                {
                                    boolean var5;
                                    if ( plan instanceof DeleteNode )
                                    {
                                        var5 = true;
                                    }
                                    else if ( plan instanceof DeleteRelationship )
                                    {
                                        var5 = true;
                                    }
                                    else if ( plan instanceof DeletePath )
                                    {
                                        var5 = true;
                                    }
                                    else if ( plan instanceof DeleteExpression )
                                    {
                                        var5 = true;
                                    }
                                    else if ( plan instanceof DetachDeleteNode )
                                    {
                                        var5 = true;
                                    }
                                    else if ( plan instanceof DetachDeletePath )
                                    {
                                        var5 = true;
                                    }
                                    else if ( plan instanceof DetachDeleteExpression )
                                    {
                                        var5 = true;
                                    }
                                    else
                                    {
                                        var5 = false;
                                    }

                                    if ( var5 )
                                    {
                                        var3 = this.fallback.onOneChildPlan( plan, source );
                                    }
                                    else
                                    {
                                        boolean var4;
                                        if ( plan instanceof SetLabels )
                                        {
                                            var4 = true;
                                        }
                                        else if ( plan instanceof SetNodeProperty )
                                        {
                                            var4 = true;
                                        }
                                        else if ( plan instanceof SetNodePropertiesFromMap )
                                        {
                                            var4 = true;
                                        }
                                        else if ( plan instanceof SetRelationshipProperty )
                                        {
                                            var4 = true;
                                        }
                                        else if ( plan instanceof SetRelationshipPropertiesFromMap )
                                        {
                                            var4 = true;
                                        }
                                        else if ( plan instanceof SetProperty )
                                        {
                                            var4 = true;
                                        }
                                        else if ( plan instanceof SetPropertiesFromMap )
                                        {
                                            var4 = true;
                                        }
                                        else if ( plan instanceof RemoveLabels )
                                        {
                                            var4 = true;
                                        }
                                        else
                                        {
                                            var4 = false;
                                        }

                                        if ( var4 )
                                        {
                                            var3 = this.fallback.onOneChildPlan( plan, source );
                                        }
                                        else if ( plan instanceof LockNodes )
                                        {
                                            var3 = this.fallback.onOneChildPlan( plan, source );
                                        }
                                        else
                                        {
                                            var3 = this.fallback.onOneChildPlan( plan, source );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        ((Pipe) var3).executionContextFactory_$eq( new SlottedExecutionContextFactory( slots ) );
        return (Pipe) var3;
    }

    private ColumnOrder translateColumnOrder( final SlotConfiguration slots, final org.neo4j.cypher.internal.logical.plans.ColumnOrder s )
    {
        Object var3;
        if ( s instanceof org.neo4j.cypher.internal.logical.plans.Ascending )
        {
            org.neo4j.cypher.internal.logical.plans.Ascending var7 = (org.neo4j.cypher.internal.logical.plans.Ascending) s;
            String name = var7.id();
            Option var9 = slots.get( name );
            if ( !(var9 instanceof Some) )
            {
                if ( scala.None..MODULE$.equals( var9 )){
                throw new InternalException(
                        (new StringBuilder( 41 )).append( "Did not find `" ).append( name ).append( "` in the slot configuration" ).toString() );
            }

                throw new MatchError( var9 );
            }

            Some var10 = (Some) var9;
            Slot slot = (Slot) var10.value();
            Ascending var5 = new Ascending( slot );
            var3 = var5;
        }
        else
        {
            if ( !(s instanceof org.neo4j.cypher.internal.logical.plans.Descending) )
            {
                throw new MatchError( s );
            }

            org.neo4j.cypher.internal.logical.plans.Descending var12 = (org.neo4j.cypher.internal.logical.plans.Descending) s;
            String name = var12.id();
            Option var14 = slots.get( name );
            if ( !(var14 instanceof Some) )
            {
                if ( scala.None..MODULE$.equals( var14 )){
                throw new InternalException(
                        (new StringBuilder( 41 )).append( "Did not find `" ).append( name ).append( "` in the slot configuration" ).toString() );
            }

                throw new MatchError( var14 );
            }

            Some var15 = (Some) var14;
            Slot slot = (Slot) var15.value();
            Descending var4 = new Descending( slot );
            var3 = var4;
        }

        return (ColumnOrder) var3;
    }

    public Pipe onTwoChildPlan( final LogicalPlan plan, final Pipe lhs, final Pipe rhs )
    {
        PhysicalPlanningAttributes.SlotConfigurations slotConfigs = this.physicalPlan.slotConfigurations();
        int id = plan.id();
        Function1 convertExpressions = ( e ) -> {
            return this.expressionConverters.toCommandExpression( id, e );
        };
        SlotConfiguration slots = (SlotConfiguration) slotConfigs.apply( id );
        SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( slots );
        Object var4;
        if ( plan instanceof Apply )
        {
            var4 = new ApplySlottedPipe( lhs, rhs, id );
        }
        else
        {
            boolean var8;
            if ( plan instanceof AbstractSemiApply )
            {
                var8 = true;
            }
            else if ( plan instanceof AbstractSelectOrSemiApply )
            {
                var8 = true;
            }
            else
            {
                var8 = false;
            }

            if ( var8 )
            {
                var4 = this.fallback.onTwoChildPlan( plan, lhs, rhs );
            }
            else if ( plan instanceof RollUpApply )
            {
                RollUpApply var15 = (RollUpApply) plan;
                LogicalPlan rhsPlan = var15.right();
                String collectionName = var15.collectionName();
                String identifierToCollect = var15.variableToCollect();
                Set nullables = var15.nullableVariables();
                SlotConfiguration rhsSlots = (SlotConfiguration) slotConfigs.apply( rhsPlan.id() );
                Tuple2 identifierToCollectExpression =
                        SlottedPipeMapper$.MODULE$.org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$createProjectionForIdentifier( rhsSlots,
                                identifierToCollect );
                int collectionRefSlotOffset = slots.getReferenceOffsetFor( collectionName );
                var4 = new RollUpApplySlottedPipe( lhs, rhs, collectionRefSlotOffset, identifierToCollectExpression, nullables, slots, id );
            }
            else if ( plan instanceof CartesianProduct )
            {
                SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan.argumentSizes().apply( plan.id() );
                LogicalPlan lhsPlan = (LogicalPlan) plan.lhs().get();
                SlotConfiguration lhsSlots = (SlotConfiguration) slotConfigs.apply( lhsPlan.id() );
                org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.ifAssertionsEnabled( () -> {
                this.verifyOnlyArgumentsAreSharedSlots( plan, this.physicalPlan );
            } );
                var4 = new CartesianProductSlottedPipe( lhs, rhs, lhsSlots.numberOfLongs(), lhsSlots.numberOfReferences(), slots, argumentSize, id );
            }
            else if ( plan instanceof NodeHashJoin )
            {
                NodeHashJoin var26 = (NodeHashJoin) plan;
                SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan.argumentSizes().apply( plan.id() );
                String[] nodes = (String[]) var26.nodes().toArray( scala.reflect.ClassTag..MODULE$.apply( String.class ));
                int[] leftNodes = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) nodes ))).map( ( k ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$onTwoChildPlan$3( slots, k ) );
            }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
                SlotConfiguration rhsSlots = (SlotConfiguration) slotConfigs.apply( var26.right().id() );
                int[] rightNodes = (int[]) (new ofRef( scala.Predef..MODULE$.refArrayOps( (Object[]) nodes ))).map( ( k ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$onTwoChildPlan$4( rhsSlots, k ) );
            }, scala.Array..MODULE$.canBuildFrom( scala.reflect.ClassTag..MODULE$.Int()));
                org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.ifAssertionsEnabled( () -> {
                this.verifyArgumentsAreTheSameOnBothSides( plan, this.physicalPlan );
            } );
                Tuple3 var33 = this.computeSlotsToCopy( rhsSlots, argumentSize, slots );
                if ( var33 == null )
                {
                    throw new MatchError( var33 );
                }

                Tuple2[] longsToCopy = (Tuple2[]) var33._1();
                Tuple2[] refsToCopy = (Tuple2[]) var33._2();
                Tuple2[] cachedPropertiesToCopy = (Tuple2[]) var33._3();
                Tuple3 var7 = new Tuple3( longsToCopy, refsToCopy, cachedPropertiesToCopy );
                Tuple2[] longsToCopy = (Tuple2[]) var7._1();
                Tuple2[] refsToCopy = (Tuple2[]) var7._2();
                Tuple2[] cachedPropertiesToCopy = (Tuple2[]) var7._3();
                var4 = leftNodes.length == 1 ? new NodeHashJoinSlottedSingleNodePipe( leftNodes[0], rightNodes[0], lhs, rhs, slots, longsToCopy, refsToCopy,
                        cachedPropertiesToCopy, id ) : new NodeHashJoinSlottedPipe( leftNodes, rightNodes, lhs, rhs, slots, longsToCopy, refsToCopy,
                        cachedPropertiesToCopy, id );
            }
            else
            {
                label81:
                {
                    if ( plan instanceof ValueHashJoin )
                    {
                        ValueHashJoin var40 = (ValueHashJoin) plan;
                        LogicalPlan lhsPlan = var40.left();
                        Equals var42 = var40.join();
                        if ( var42 != null )
                        {
                            org.neo4j.cypher.internal.v4_0.expressions.Expression lhsAstExp = var42.lhs();
                            org.neo4j.cypher.internal.v4_0.expressions.Expression rhsAstExp = var42.rhs();
                            SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) this.physicalPlan.argumentSizes().apply( plan.id() );
                            Expression lhsCmdExp = (Expression) convertExpressions.apply( lhsAstExp );
                            Expression rhsCmdExp = (Expression) convertExpressions.apply( rhsAstExp );
                            SlotConfiguration lhsSlots = (SlotConfiguration) slotConfigs.apply( lhsPlan.id() );
                            int longOffset = lhsSlots.numberOfLongs();
                            int refOffset = lhsSlots.numberOfReferences();
                            org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.ifAssertionsEnabled( () -> {
                            this.verifyOnlyArgumentsAreSharedSlots( plan, this.physicalPlan );
                        } );
                            var4 = new ValueHashJoinSlottedPipe( lhsCmdExp, rhsCmdExp, lhs, rhs, slots, longOffset, refOffset, argumentSize, id );
                            break label81;
                        }
                    }

                    if ( plan instanceof ConditionalApply )
                    {
                        ConditionalApply var51 = (ConditionalApply) plan;
                        Seq items = var51.items();
                        Tuple2 var54 = items.partition( ( idName ) -> {
                            return BoxesRunTime.boxToBoolean( $anonfun$onTwoChildPlan$7( slots, idName ) );
                        } );
                        if ( var54 == null )
                        {
                            throw new MatchError( var54 );
                        }

                        Seq longIds = (Seq) var54._1();
                        Seq refIds = (Seq) var54._2();
                        Tuple2 var6 = new Tuple2( longIds, refIds );
                        Seq longIds = (Seq) var6._1();
                        Seq refIds = (Seq) var6._2();
                        Seq longOffsets = (Seq) longIds.map( ( e ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$onTwoChildPlan$8( slots, e ) );
                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                        Seq refOffsets = (Seq) refIds.map( ( e ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$onTwoChildPlan$9( slots, e ) );
                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                        var4 = new ConditionalApplySlottedPipe( lhs, rhs, longOffsets, refOffsets, false, slots, id );
                    }
                    else if ( plan instanceof AntiConditionalApply )
                    {
                        AntiConditionalApply var61 = (AntiConditionalApply) plan;
                        Seq items = var61.items();
                        Tuple2 var64 = items.partition( ( idName ) -> {
                            return BoxesRunTime.boxToBoolean( $anonfun$onTwoChildPlan$10( slots, idName ) );
                        } );
                        if ( var64 == null )
                        {
                            throw new MatchError( var64 );
                        }

                        Seq longIds = (Seq) var64._1();
                        Seq refIds = (Seq) var64._2();
                        Tuple2 var5 = new Tuple2( longIds, refIds );
                        Seq longIds = (Seq) var5._1();
                        Seq refIds = (Seq) var5._2();
                        Seq longOffsets = (Seq) longIds.map( ( e ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$onTwoChildPlan$11( slots, e ) );
                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                        Seq refOffsets = (Seq) refIds.map( ( e ) -> {
                            return BoxesRunTime.boxToInteger( $anonfun$onTwoChildPlan$12( slots, e ) );
                        }, scala.collection.Seq..MODULE$.canBuildFrom());
                        var4 = new ConditionalApplySlottedPipe( lhs, rhs, longOffsets, refOffsets, true, slots, id );
                    }
                    else if ( plan instanceof ForeachApply )
                    {
                        ForeachApply var71 = (ForeachApply) plan;
                        String variable = var71.variable();
                        org.neo4j.cypher.internal.v4_0.expressions.Expression expression = var71.expression();
                        Slot innerVariableSlot = (Slot) slots.get( variable ).getOrElse( () -> {
                            throw new InternalException(
                                    (new StringBuilder( 31 )).append( "Foreach variable '" ).append( variable ).append( "' has no slot" ).toString() );
                        } );
                        var4 = new ForeachSlottedPipe( lhs, rhs, innerVariableSlot, (Expression) convertExpressions.apply( expression ), id );
                    }
                    else if ( plan instanceof Union )
                    {
                        SlotConfiguration lhsSlots = (SlotConfiguration) slotConfigs.apply( lhs.id() );
                        SlotConfiguration rhsSlots = (SlotConfiguration) slotConfigs.apply( rhs.id() );
                        var4 = new UnionSlottedPipe( lhs, rhs, slots, SlottedPipeMapper$.MODULE$.computeUnionMapping( lhsSlots, slots ),
                                SlottedPipeMapper$.MODULE$.computeUnionMapping( rhsSlots, slots ), id );
                    }
                    else if ( plan instanceof AssertSameNode )
                    {
                        var4 = this.fallback.onTwoChildPlan( plan, lhs, rhs );
                    }
                    else
                    {
                        var4 = this.fallback.onTwoChildPlan( plan, lhs, rhs );
                    }
                }
            }
        }

        ((Pipe) var4).executionContextFactory_$eq( new SlottedExecutionContextFactory( slots ) );
        return (Pipe) var4;
    }

    private Tuple3<Tuple2<Object,Object>[],Tuple2<Object,Object>[],Tuple2<Object,Object>[]> computeSlotsToCopy( final SlotConfiguration rhsSlots,
            final SlotConfiguration.Size argumentSize, final SlotConfiguration slots )
    {
        Builder copyLongsFromRHS = scala.collection.mutable.ArrayBuffer..MODULE$.newBuilder();
        Builder copyRefsFromRHS = scala.collection.mutable.ArrayBuffer..MODULE$.newBuilder();
        Builder copyCachedPropertiesFromRHS = scala.collection.mutable.ArrayBuffer..MODULE$.newBuilder();
        rhsSlots.foreachSlotOrdered( ( x0$4, x1$1 ) -> {
            $anonfun$computeSlotsToCopy$1( argumentSize, slots, copyLongsFromRHS, copyRefsFromRHS, x0$4, x1$1 );
            return BoxedUnit.UNIT;
        }, ( cnp ) -> {
            $anonfun$computeSlotsToCopy$2( rhsSlots, argumentSize, slots, copyCachedPropertiesFromRHS, cnp );
            return BoxedUnit.UNIT;
        }, rhsSlots.foreachSlotOrdered$default$3(), rhsSlots.foreachSlotOrdered$default$4() );
        return new Tuple3( ((TraversableOnce) copyLongsFromRHS.result()).toArray( scala.reflect.ClassTag..MODULE$.apply( Tuple2.class ) ),
        ((TraversableOnce) copyRefsFromRHS.result()).toArray( scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )),
        ((TraversableOnce) copyCachedPropertiesFromRHS.result()).toArray( scala.reflect.ClassTag..MODULE$.apply( Tuple2.class )));
    }

    private Pipe chooseDistinctPipe( final Map<String,org.neo4j.cypher.internal.v4_0.expressions.Expression> groupingExpressions,
            final Seq<org.neo4j.cypher.internal.v4_0.expressions.Expression> orderToLeverage, final SlotConfiguration slots, final Pipe source, final int id )
    {
        LazyRef AllPrimitive$module = new LazyRef();
        LazyRef References$module = new LazyRef();
        Function1 convertExpressions = ( e ) -> {
            return this.expressionConverters.toCommandExpression( id, e );
        };
        Map runtimeProjections = (Map) groupingExpressions.map( ( x0$5 ) -> {
            if ( x0$5 != null )
            {
                String key = (String) x0$5._1();
                org.neo4j.cypher.internal.v4_0.expressions.Expression expression = (org.neo4j.cypher.internal.v4_0.expressions.Expression) x0$5._2();
                Tuple2 var3 = scala.Predef.ArrowAssoc..
                MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( slots.apply( key ) ), convertExpressions.apply( expression ));
                return var3;
            }
            else
            {
                throw new MatchError( x0$5 );
            }
        }, scala.collection.immutable.Map..MODULE$.canBuildFrom());

        abstract class DistinctPhysicalOp$1
        {
            DistinctPhysicalOp$1 addExpression( final org.neo4j.cypher.internal.v4_0.expressions.Expression e, final boolean ordered );
        }

        DistinctPhysicalOp$1 physicalDistinctOp = (DistinctPhysicalOp$1) groupingExpressions.foldLeft(
                this.org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$AllPrimitive$2( AllPrimitive$module, References$module ).apply(
                        (Seq) scala.collection.Seq..MODULE$.empty(), (Seq) scala.collection.Seq..MODULE$.empty()),( x0$6, x1$2 ) -> {
        Tuple2 var4 = new Tuple2( x0$6, x1$2 );
        if ( var4 != null )
        {
            DistinctPhysicalOp$1 acc = (DistinctPhysicalOp$1) var4._1();
            Tuple2 var6 = (Tuple2) var4._2();
            if ( acc != null && var6 != null )
            {
                org.neo4j.cypher.internal.v4_0.expressions.Expression expression = (org.neo4j.cypher.internal.v4_0.expressions.Expression) var6._2();
                DistinctPhysicalOp$1 var3 = acc.addExpression( expression, orderToLeverage.contains( expression ) );
                return var3;
            }
        }

        throw new MatchError( var4 );
    });
        boolean var14 = false;

        class AllPrimitive$3 implements DistinctPhysicalOp$1, Product, Serializable
        {
            private final Seq<Object> offsets;
            private final Seq<Object> orderedOffsets;
            private final LazyRef AllPrimitive$module$1;
            private final LazyRef References$module$1;

            public AllPrimitive$3( final SlottedPipeMapper $outer, final Seq offsets, final Seq orderedOffsets, final LazyRef AllPrimitive$module$1,
                    final LazyRef References$module$1 )
            {
                this.offsets = offsets;
                this.orderedOffsets = orderedOffsets;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.AllPrimitive$module$1 = AllPrimitive$module$1;
                    this.References$module$1 = References$module$1;
                    super();
                    Product.$init$( this );
                }
            }

            public Seq<Object> offsets()
            {
                return this.offsets;
            }

            public Seq<Object> orderedOffsets()
            {
                return this.orderedOffsets;
            }

            public DistinctPhysicalOp$1 addExpression( final org.neo4j.cypher.internal.v4_0.expressions.Expression e, final boolean ordered )
            {
                Object var3;
                if ( e instanceof NodeFromSlot )
                {
                    NodeFromSlot var5 = (NodeFromSlot) e;
                    Seq oo = ordered ? (Seq) this.orderedOffsets().$colon$plus( BoxesRunTime.boxToInteger( var5.offset() ),
                            scala.collection.Seq..MODULE$.canBuildFrom()) :this.orderedOffsets();
                    var3 = this.$outer.org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$AllPrimitive$2( this.AllPrimitive$module$1,
                            this.References$module$1 ).apply(
                            (Seq) this.offsets().$colon$plus( BoxesRunTime.boxToInteger( var5.offset() ), scala.collection.Seq..MODULE$.canBuildFrom() ), oo);
                }
                else if ( e instanceof RelationshipFromSlot )
                {
                    RelationshipFromSlot var7 = (RelationshipFromSlot) e;
                    Seq oox = ordered ? (Seq) this.orderedOffsets().$colon$plus( BoxesRunTime.boxToInteger( var7.offset() ),
                            scala.collection.Seq..MODULE$.canBuildFrom()) :this.orderedOffsets();
                    var3 = this.$outer.org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$AllPrimitive$2( this.AllPrimitive$module$1,
                            this.References$module$1 ).apply(
                            (Seq) this.offsets().$colon$plus( BoxesRunTime.boxToInteger( var7.offset() ), scala.collection.Seq..MODULE$.canBuildFrom() ), oox);
                }
                else
                {
                    var3 = this.$outer.org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$References$1( this.References$module$1 );
                }

                return (DistinctPhysicalOp$1) var3;
            }

            public AllPrimitive$3 copy( final Seq<Object> offsets, final Seq<Object> orderedOffsets )
            {
                return new AllPrimitive$3( this.$outer, offsets, orderedOffsets, this.AllPrimitive$module$1, this.References$module$1 );
            }

            public Seq<Object> copy$default$1()
            {
                return this.offsets();
            }

            public Seq<Object> copy$default$2()
            {
                return this.orderedOffsets();
            }

            public String productPrefix()
            {
                return "AllPrimitive";
            }

            public int productArity()
            {
                return 2;
            }

            public Object productElement( final int x$1 )
            {
                Seq var10000;
                switch ( x$1 )
                {
                case 0:
                    var10000 = this.offsets();
                    break;
                case 1:
                    var10000 = this.orderedOffsets();
                    break;
                default:
                    throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
                }

                return var10000;
            }

            public Iterator<Object> productIterator()
            {
                return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
            }

            public boolean canEqual( final Object x$1 )
            {
                return x$1 instanceof AllPrimitive$3;
            }

            public int hashCode()
            {
                return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
            }

            public String toString()
            {
                return scala.runtime.ScalaRunTime..MODULE$._toString( this );
            }

            public boolean equals( final Object x$1 )
            {
                boolean var7;
                if ( this != x$1 )
                {
                    label63:
                    {
                        boolean var2;
                        if ( x$1 instanceof AllPrimitive$3 )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        if ( var2 )
                        {
                            label45:
                            {
                                label54:
                                {
                                    AllPrimitive$3 var4 = (AllPrimitive$3) x$1;
                                    Seq var10000 = this.offsets();
                                    Seq var5 = var4.offsets();
                                    if ( var10000 == null )
                                    {
                                        if ( var5 != null )
                                        {
                                            break label54;
                                        }
                                    }
                                    else if ( !var10000.equals( var5 ) )
                                    {
                                        break label54;
                                    }

                                    var10000 = this.orderedOffsets();
                                    Seq var6 = var4.orderedOffsets();
                                    if ( var10000 == null )
                                    {
                                        if ( var6 != null )
                                        {
                                            break label54;
                                        }
                                    }
                                    else if ( !var10000.equals( var6 ) )
                                    {
                                        break label54;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var7 = true;
                                        break label45;
                                    }
                                }

                                var7 = false;
                            }

                            if ( var7 )
                            {
                                break label63;
                            }
                        }

                        var7 = false;
                        return var7;
                    }
                }

                var7 = true;
                return var7;
            }
        }

        AllPrimitive$3 var15 = null;
        Object var6;
        if ( physicalDistinctOp instanceof AllPrimitive$3 )
        {
            var14 = true;
            var15 = (AllPrimitive$3) physicalDistinctOp;
            Seq offsets = var15.offsets();
            Seq orderedOffsets = var15.orderedOffsets();
            if ( offsets.size() == 1 && orderedOffsets.isEmpty() )
            {
                Tuple2 var20 = (Tuple2) runtimeProjections.head();
                if ( var20 == null )
                {
                    throw new MatchError( var20 );
                }

                Slot toSlot = (Slot) var20._1();
                Expression runtimeExpression = (Expression) var20._2();
                Tuple2 var8 = new Tuple2( toSlot, runtimeExpression );
                Slot toSlot = (Slot) var8._1();
                Expression runtimeExpression = (Expression) var8._2();
                var6 = new DistinctSlottedSinglePrimitivePipe( source, slots, toSlot, BoxesRunTime.unboxToInt( offsets.head() ), runtimeExpression, id );
                return (Pipe) var6;
            }
        }

        if ( var14 )
        {
            Seq offsets = var15.offsets();
            Seq orderedOffsets = var15.orderedOffsets();
            if ( offsets.size() == 1 )
            {
                label107:
                {
                    if ( orderedOffsets == null )
                    {
                        if ( offsets != null )
                        {
                            break label107;
                        }
                    }
                    else if ( !orderedOffsets.equals( offsets ) )
                    {
                        break label107;
                    }

                    Tuple2 var29 = (Tuple2) runtimeProjections.head();
                    if ( var29 == null )
                    {
                        throw new MatchError( var29 );
                    }

                    Slot toSlot = (Slot) var29._1();
                    Expression runtimeExpression = (Expression) var29._2();
                    Tuple2 var7 = new Tuple2( toSlot, runtimeExpression );
                    Slot toSlot = (Slot) var7._1();
                    Expression runtimeExpression = (Expression) var7._2();
                    var6 = new OrderedDistinctSlottedSinglePrimitivePipe( source, slots, toSlot, BoxesRunTime.unboxToInt( offsets.head() ), runtimeExpression,
                            id );
                    return (Pipe) var6;
                }
            }
        }

        if ( var14 )
        {
            Seq offsets = var15.offsets();
            Seq orderedOffsets = var15.orderedOffsets();
            Object var10000;
            if ( orderToLeverage.isEmpty() )
            {
                var10000 =
                        new DistinctSlottedPrimitivePipe( source, slots, (int[]) ((TraversableOnce) offsets.sorted( scala.math.Ordering.Int..MODULE$) ).toArray(
                                scala.reflect.ClassTag..MODULE$.Int()),
                this.expressionConverters.toGroupingExpression( id, groupingExpressions, orderToLeverage ), id);
            }
            else
            {
                label92:
                {
                    label64:
                    {
                        if ( orderedOffsets == null )
                        {
                            if ( offsets != null )
                            {
                                break label64;
                            }
                        }
                        else if ( !orderedOffsets.equals( offsets ) )
                        {
                            break label64;
                        }

                        var10000 = new AllOrderedDistinctSlottedPrimitivePipe( source, slots,
                                (int[]) ((TraversableOnce) offsets.sorted( scala.math.Ordering.Int..MODULE$) ).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                        this.expressionConverters.toGroupingExpression( id, groupingExpressions, orderToLeverage ), id);
                        break label92;
                    }

                    var10000 = new OrderedDistinctSlottedPrimitivePipe( source, slots,
                            (int[]) ((TraversableOnce) offsets.sorted( scala.math.Ordering.Int..MODULE$) ).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                    (int[]) ((TraversableOnce) orderedOffsets.sorted( scala.math.Ordering.Int..MODULE$)).toArray( scala.reflect.ClassTag..MODULE$.Int()),
                    this.expressionConverters.toGroupingExpression( id, groupingExpressions, orderToLeverage ), id);
                }
            }

            var6 = var10000;
        }
        else
        {
            if ( !this.org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$References$1( References$module ).equals( physicalDistinctOp ) )
            {
                throw new MatchError( physicalDistinctOp );
            }

            var6 = orderToLeverage.isEmpty() ? new DistinctSlottedPipe( source, slots,
                    this.expressionConverters.toGroupingExpression( id, groupingExpressions, orderToLeverage ), id )
                                             : (groupingExpressions.values().forall( ( elem ) -> {
                                                 return BoxesRunTime.boxToBoolean( $anonfun$chooseDistinctPipe$4( orderToLeverage, elem ) );
                                             } ) ? new AllOrderedDistinctSlottedPipe( source, slots,
                                                     this.expressionConverters.toGroupingExpression( id, groupingExpressions, orderToLeverage ), id )
                                                 : new OrderedDistinctSlottedPipe( source, slots,
                                                         this.expressionConverters.toGroupingExpression( id, groupingExpressions, orderToLeverage ), id ));
        }

        return (Pipe) var6;
    }

    private void verifyOnlyArgumentsAreSharedSlots( final LogicalPlan plan, final PhysicalPlan physicalPlan )
    {
        SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) physicalPlan.argumentSizes().apply( plan.id() );
        LogicalPlan lhsPlan = (LogicalPlan) plan.lhs().get();
        LogicalPlan rhsPlan = (LogicalPlan) plan.rhs().get();
        SlotConfiguration lhsSlots = (SlotConfiguration) physicalPlan.slotConfigurations().apply( lhsPlan.id() );
        SlotConfiguration rhsSlots = (SlotConfiguration) physicalPlan.slotConfigurations().apply( rhsPlan.id() );
        Iterable sharedSlots = rhsSlots.filterSlots( ( x0$7 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$verifyOnlyArgumentsAreSharedSlots$1( lhsSlots, x0$7 ) );
        }, ( x0$8 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$verifyOnlyArgumentsAreSharedSlots$2( argumentSize, lhsSlots, x0$8 ) );
        } );
        Tuple2 var11 = sharedSlots.partition( ( x$13 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$verifyOnlyArgumentsAreSharedSlots$3( x$13 ) );
        } );
        if ( var11 != null )
        {
            Iterable sharedLongSlots = (Iterable) var11._1();
            Iterable sharedRefSlots = (Iterable) var11._2();
            Tuple2 var3 = new Tuple2( sharedLongSlots, sharedRefSlots );
            Iterable sharedLongSlots = (Iterable) var3._1();
            Iterable sharedRefSlots = (Iterable) var3._2();
            boolean longSlotsOk = checkSharedSlots$1( sharedLongSlots.toSeq(), argumentSize.nLongs() );
            boolean refSlotsOk = checkSharedSlots$1( sharedRefSlots.toSeq(), argumentSize.nReferences() );
            if ( !longSlotsOk || !refSlotsOk )
            {
                String longSlotsMessage = longSlotsOk ? "" : (new StringBuilder( 37 )).append( "#long arguments=" ).append( argumentSize.nLongs() ).append(
                        " shared long slots: " ).append( sharedLongSlots ).append( " " ).toString();
                String refSlotsMessage = refSlotsOk ? "" : (new StringBuilder( 35 )).append( "#ref arguments=" ).append( argumentSize.nReferences() ).append(
                        " shared ref slots: " ).append( sharedRefSlots ).append( " " ).toString();
                throw new InternalException(
                        (new StringBuilder( 75 )).append( "Unexpected slot configuration. Shared slots not only within argument size: " ).append(
                                longSlotsMessage ).append( refSlotsMessage ).toString() );
            }
        }
        else
        {
            throw new MatchError( var11 );
        }
    }

    private void verifyArgumentsAreTheSameOnBothSides( final LogicalPlan plan, final PhysicalPlan physicalPlan )
    {
        SlotConfiguration.Size argumentSize = (SlotConfiguration.Size) physicalPlan.argumentSizes().apply( plan.id() );
        LogicalPlan lhsPlan = (LogicalPlan) plan.lhs().get();
        LogicalPlan rhsPlan = (LogicalPlan) plan.rhs().get();
        SlotConfiguration lhsSlots = (SlotConfiguration) physicalPlan.slotConfigurations().apply( lhsPlan.id() );
        SlotConfiguration rhsSlots = (SlotConfiguration) physicalPlan.slotConfigurations().apply( rhsPlan.id() );
        ArrayBuffer lhsArgLongSlots = (ArrayBuffer) scala.collection.mutable.ArrayBuffer..MODULE$.empty();
        ArrayBuffer lhsArgRefSlots = (ArrayBuffer) scala.collection.mutable.ArrayBuffer..MODULE$.empty();
        ArrayBuffer rhsArgLongSlots = (ArrayBuffer) scala.collection.mutable.ArrayBuffer..MODULE$.empty();
        ArrayBuffer rhsArgRefSlots = (ArrayBuffer) scala.collection.mutable.ArrayBuffer..MODULE$.empty();
        lhsSlots.foreachSlot( ( x0$9 ) -> {
            if ( x0$9 == null )
            {
                throw new MatchError( x0$9 );
            }
            else
            {
                String key = (String) x0$9._1();
                Slot slot = (Slot) x0$9._2();
                Object var4 = slot.isLongSlot() && slot.offset() < argumentSize.nLongs() ? lhsArgLongSlots.$plus$eq(
                        scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( key ), slot)) :
                (!slot.isLongSlot() && slot.offset() < argumentSize.nReferences() ? lhsArgRefSlots.$plus$eq(
                        scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( key ), slot)) :BoxedUnit.UNIT);
                return var4;
            }
        }, ( x0$10 ) -> {
            $anonfun$verifyArgumentsAreTheSameOnBothSides$2( argumentSize, lhsArgRefSlots, x0$10 );
            return BoxedUnit.UNIT;
        } ); rhsSlots.foreachSlot( ( x0$11 ) -> {
        if ( x0$11 == null )
        {
            throw new MatchError( x0$11 );
        }
        else
        {
            String key = (String) x0$11._1();
            Slot slot = (Slot) x0$11._2();
            Object var4 = slot.isLongSlot() && slot.offset() < argumentSize.nLongs() ? rhsArgLongSlots.$plus$eq(
                    scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( key ), slot)) :
            (!slot.isLongSlot() && slot.offset() < argumentSize.nReferences() ? rhsArgRefSlots.$plus$eq(
                    scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( key ), slot)) :BoxedUnit.UNIT);
            return var4;
        }
    }, ( x0$12 ) -> {
        $anonfun$verifyArgumentsAreTheSameOnBothSides$4( argumentSize, rhsArgRefSlots, x0$12 );
        return BoxedUnit.UNIT;
    } ); boolean longSlotsOk = lhsArgLongSlots.size() == rhsArgLongSlots.size() && sameSlotsInOrder$1( lhsArgLongSlots, rhsArgLongSlots );
        boolean refSlotsOk = lhsArgRefSlots.size() == rhsArgRefSlots.size() && sameSlotsInOrder$1( lhsArgRefSlots, rhsArgRefSlots );
        if ( !longSlotsOk || !refSlotsOk )
        {
            String longSlotsMessage = longSlotsOk ? "" : (new StringBuilder( 29 )).append( "#long arguments=" ).append( argumentSize.nLongs() ).append(
                    " lhs: " ).append( lhsArgLongSlots ).append( " rhs: " ).append( rhsArgLongSlots ).append( " " ).toString();
            String refSlotsMessage = refSlotsOk ? "" : (new StringBuilder( 28 )).append( "#ref arguments=" ).append( argumentSize.nReferences() ).append(
                    " lhs: " ).append( lhsArgRefSlots ).append( " rhs: " ).append( rhsArgRefSlots ).append( " " ).toString();
            throw new InternalException( (new StringBuilder( 69 )).append( "Unexpected slot configuration. Arguments differ between lhs and rhs: " ).append(
                    longSlotsMessage ).append( refSlotsMessage ).toString() );
        }
    }

    public final AllPrimitive$4$ org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$AllPrimitive$2( final LazyRef AllPrimitive$module$1,
            final LazyRef References$module$1 )
    {
        return AllPrimitive$module$1.initialized() ? (AllPrimitive$4$) AllPrimitive$module$1.value()
                                                   : this.AllPrimitive$lzycompute$1( AllPrimitive$module$1, References$module$1 );
    }

    public final References$2$ org$neo4j$cypher$internal$runtime$slotted$SlottedPipeMapper$$References$1( final LazyRef References$module$1 )
    {
        return References$module$1.initialized() ? (References$2$) References$module$1.value() : this.References$lzycompute$1( References$module$1 );
    }
}
