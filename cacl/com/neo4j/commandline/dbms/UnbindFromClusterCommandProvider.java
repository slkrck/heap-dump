package com.neo4j.commandline.dbms;

import org.neo4j.cli.CommandProvider;
import org.neo4j.cli.ExecutionContext;

public class UnbindFromClusterCommandProvider implements CommandProvider<UnbindFromClusterCommand>
{
    public UnbindFromClusterCommand createCommand( ExecutionContext ctx )
    {
        return new UnbindFromClusterCommand( ctx );
    }
}
