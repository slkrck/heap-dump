package com.neo4j.commandline.dbms;

import com.neo4j.causalclustering.core.state.ClusterStateLayout;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.neo4j.cli.AbstractCommand;
import org.neo4j.cli.CommandFailedException;
import org.neo4j.cli.ExecutionContext;
import org.neo4j.commandline.dbms.LockChecker;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.layout.Neo4jLayout;
import org.neo4j.kernel.internal.locker.FileLockException;
import picocli.CommandLine.Command;

@Command( name = "unbind", header = {"Removes cluster state data for the specified database."}, description = {
        "Removes cluster state data for the specified database, so that the instance can rebind to a new or recovered cluster."} )
class UnbindFromClusterCommand extends AbstractCommand
{
    UnbindFromClusterCommand( ExecutionContext ctx )
    {
        super( ctx );
    }

    private static Config loadNeo4jConfig( Path homeDir, Path configDir )
    {
        return Config.newBuilder().fromFileNoThrow( configDir.resolve( "neo4j.conf" ) ).set( GraphDatabaseSettings.neo4j_home, homeDir ).build();
    }

    public void execute()
    {
        try
        {
            Config config = loadNeo4jConfig( this.ctx.homeDir(), this.ctx.confDir() );
            Neo4jLayout neo4jLayout = Neo4jLayout.of( config );
            Closeable ignored = LockChecker.checkDbmsLock( neo4jLayout );

            try
            {
                File clusterStateDirectory =
                        ClusterStateLayout.of( ((Path) config.get( GraphDatabaseSettings.data_directory )).toFile() ).getClusterStateDirectory();
                if ( this.ctx.fs().fileExists( clusterStateDirectory ) )
                {
                    this.deleteClusterStateIn( clusterStateDirectory );
                }
                else
                {
                    this.ctx.err().println( "This instance was not bound. No work performed." );
                }
            }
            catch ( Throwable var7 )
            {
                if ( ignored != null )
                {
                    try
                    {
                        ignored.close();
                    }
                    catch ( Throwable var6 )
                    {
                        var7.addSuppressed( var6 );
                    }
                }

                throw var7;
            }

            if ( ignored != null )
            {
                ignored.close();
            }
        }
        catch ( FileLockException var8 )
        {
            throw new CommandFailedException( "Database is currently locked. Please shutdown database.", var8 );
        }
        catch ( Exception var9 )
        {
            throw new CommandFailedException( var9.getMessage(), var9 );
        }
    }

    private void deleteClusterStateIn( File target ) throws UnbindFromClusterCommand.UnbindFailureException
    {
        try
        {
            this.ctx.fs().deleteRecursively( target );
        }
        catch ( IOException var3 )
        {
            throw new UnbindFromClusterCommand.UnbindFailureException( var3 );
        }
    }

    private static class UnbindFailureException extends Exception
    {
        UnbindFailureException( Exception e )
        {
            super( e );
        }
    }
}
