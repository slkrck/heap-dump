package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.core.consensus.ContinuousJob;
import com.neo4j.causalclustering.core.consensus.LeaderAvailabilityHandler;
import com.neo4j.causalclustering.core.consensus.LeaderAvailabilityTimers;
import com.neo4j.causalclustering.core.consensus.RaftGroup;
import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.core.consensus.RaftMessageMonitoringHandler;
import com.neo4j.causalclustering.core.consensus.RaftMessageTimerResetMonitor;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.state.CommandApplicationProcess;
import com.neo4j.causalclustering.core.state.RaftMessageApplier;
import com.neo4j.causalclustering.core.state.snapshot.CoreDownloaderService;
import com.neo4j.causalclustering.error_handling.DatabasePanicker;
import com.neo4j.causalclustering.messaging.ComposableMessageHandler;
import com.neo4j.causalclustering.messaging.LifecycleMessageHandler;

import java.time.Clock;
import java.util.Objects;

import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

class RaftMessageHandlerChainFactory
{
    private final RaftMessageDispatcher raftMessageDispatcher;
    private final CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider;
    private final DatabasePanicker panicker;
    private final JobScheduler jobScheduler;
    private final Clock clock;
    private final LogProvider logProvider;
    private final Monitors monitors;
    private final Config config;
    private NamedDatabaseId namedDatabaseId;

    RaftMessageHandlerChainFactory( JobScheduler jobScheduler, Clock clock, LogProvider logProvider, Monitors monitors, Config config,
            RaftMessageDispatcher raftMessageDispatcher, CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider,
            DatabasePanicker panicker, NamedDatabaseId namedDatabaseId )
    {
        this.jobScheduler = jobScheduler;
        this.clock = clock;
        this.logProvider = logProvider;
        this.monitors = monitors;
        this.config = config;
        this.raftMessageDispatcher = raftMessageDispatcher;
        this.catchupAddressProvider = catchupAddressProvider;
        this.panicker = panicker;
        this.namedDatabaseId = namedDatabaseId;
    }

    LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> createMessageHandlerChain( RaftGroup raftGroup,
            CoreDownloaderService downloaderService, CommandApplicationProcess commandApplicationProcess )
    {
        RaftMessageApplier messageApplier =
                new RaftMessageApplier( this.logProvider, raftGroup.raftMachine(), downloaderService, commandApplicationProcess, this.catchupAddressProvider,
                        this.panicker );
        ComposableMessageHandler monitoringHandler = RaftMessageMonitoringHandler.composable( this.clock, this.monitors );
        ComposableMessageHandler batchingMessageHandler = this.createBatchingHandler();
        LeaderAvailabilityTimers var10000 = raftGroup.getLeaderAvailabilityTimers();
        RaftMessageTimerResetMonitor var10001 = (RaftMessageTimerResetMonitor) this.monitors.newMonitor( RaftMessageTimerResetMonitor.class, new String[0] );
        RaftMachine var10002 = raftGroup.raftMachine();
        Objects.requireNonNull( var10002 );
        ComposableMessageHandler leaderAvailabilityHandler = LeaderAvailabilityHandler.composable( var10000, var10001, var10002::term );
        ComposableMessageHandler clusterBindingHandler = ClusterBindingHandler.composable( this.raftMessageDispatcher, this.logProvider );
        return (LifecycleMessageHandler) clusterBindingHandler.compose( leaderAvailabilityHandler ).compose( batchingMessageHandler ).compose(
                monitoringHandler ).apply( messageApplier );
    }

    private ComposableMessageHandler createBatchingHandler()
    {
        BoundedPriorityQueue.Config inQueueConfig = new BoundedPriorityQueue.Config( (Integer) this.config.get( CausalClusteringSettings.raft_in_queue_size ),
                (Long) this.config.get( CausalClusteringSettings.raft_in_queue_max_bytes ) );
        BatchingMessageHandler.Config batchConfig =
                new BatchingMessageHandler.Config( (Integer) this.config.get( CausalClusteringSettings.raft_in_queue_max_batch ),
                        (Long) this.config.get( CausalClusteringSettings.raft_in_queue_max_batch_bytes ) );
        return BatchingMessageHandler.composable( inQueueConfig, batchConfig, this::jobFactory, this.logProvider );
    }

    private ContinuousJob jobFactory( Runnable runnable )
    {
        return new ContinuousJob( this.jobScheduler.threadFactory( Group.RAFT_BATCH_HANDLER ), runnable, this.logProvider, this.namedDatabaseId.toString() );
    }
}
