package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.consensus.RaftMessageNettyHandler;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.protocol.v2.RaftProtocolServerInstallerV2;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.logging.RaftMessageLogger;
import com.neo4j.causalclustering.messaging.LoggingInbound;
import com.neo4j.causalclustering.net.BootstrapConfiguration;
import com.neo4j.causalclustering.net.Server;
import com.neo4j.causalclustering.protocol.ModifierProtocolInstaller;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ProtocolInstaller;
import com.neo4j.causalclustering.protocol.ProtocolInstallerRepository;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.handshake.ApplicationProtocolRepository;
import com.neo4j.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import com.neo4j.causalclustering.protocol.handshake.HandshakeServerInitializer;
import com.neo4j.causalclustering.protocol.handshake.ModifierProtocolRepository;
import com.neo4j.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import com.neo4j.causalclustering.protocol.init.ServerChannelInitializer;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocols;
import io.netty.channel.ChannelInboundHandler;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executor;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;

public class RaftServerFactory
{
    public static final String RAFT_SERVER_NAME = "raft-server";
    private final GlobalModule globalModule;
    private final IdentityModule identityModule;
    private final ApplicationSupportedProtocols supportedApplicationProtocol;
    private final RaftMessageLogger<MemberId> raftMessageLogger;
    private final LogProvider logProvider;
    private final NettyPipelineBuilderFactory pipelineBuilderFactory;
    private final Collection<ModifierSupportedProtocols> supportedModifierProtocols;

    RaftServerFactory( GlobalModule globalModule, IdentityModule identityModule, NettyPipelineBuilderFactory pipelineBuilderFactory,
            RaftMessageLogger<MemberId> raftMessageLogger, ApplicationSupportedProtocols supportedApplicationProtocol,
            Collection<ModifierSupportedProtocols> supportedModifierProtocols )
    {
        this.globalModule = globalModule;
        this.identityModule = identityModule;
        this.supportedApplicationProtocol = supportedApplicationProtocol;
        this.raftMessageLogger = raftMessageLogger;
        this.logProvider = globalModule.getLogService().getInternalLogProvider();
        this.pipelineBuilderFactory = pipelineBuilderFactory;
        this.supportedModifierProtocols = supportedModifierProtocols;
    }

    Server createRaftServer( RaftMessageDispatcher raftMessageDispatcher, ChannelInboundHandler installedProtocolsHandler )
    {
        Config config = this.globalModule.getGlobalConfig();
        ApplicationProtocolRepository applicationProtocolRepository =
                new ApplicationProtocolRepository( ApplicationProtocols.values(), this.supportedApplicationProtocol );
        ModifierProtocolRepository modifierProtocolRepository = new ModifierProtocolRepository( ModifierProtocols.values(), this.supportedModifierProtocols );
        RaftMessageNettyHandler nettyHandler = new RaftMessageNettyHandler( this.logProvider );
        RaftProtocolServerInstallerV2.Factory raftProtocolServerInstallerV2 =
                new RaftProtocolServerInstallerV2.Factory( nettyHandler, this.pipelineBuilderFactory, this.logProvider );
        ProtocolInstallerRepository<ProtocolInstaller.Orientation.Server> protocolInstallerRepository =
                new ProtocolInstallerRepository( List.of( raftProtocolServerInstallerV2 ), ModifierProtocolInstaller.allServerInstallers );
        HandshakeServerInitializer handshakeInitializer =
                new HandshakeServerInitializer( applicationProtocolRepository, modifierProtocolRepository, protocolInstallerRepository,
                        this.pipelineBuilderFactory, this.logProvider, config );
        Duration handshakeTimeout = (Duration) config.get( CausalClusteringSettings.handshake_timeout );
        ServerChannelInitializer channelInitializer =
                new ServerChannelInitializer( handshakeInitializer, this.pipelineBuilderFactory, handshakeTimeout, this.logProvider, config );
        SocketAddress raftListenAddress = (SocketAddress) config.get( CausalClusteringSettings.raft_listen_address );
        Executor raftServerExecutor = this.globalModule.getJobScheduler().executor( Group.RAFT_SERVER );
        Server raftServer = new Server( channelInitializer, installedProtocolsHandler, this.logProvider, this.globalModule.getLogService().getUserLogProvider(),
                raftListenAddress, "raft-server", raftServerExecutor, this.globalModule.getConnectorPortRegister(),
                BootstrapConfiguration.serverConfig( config ) );
        LoggingInbound<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> loggingRaftInbound =
                new LoggingInbound( nettyHandler, this.raftMessageLogger, this.identityModule.myself() );
        loggingRaftInbound.registerHandler( raftMessageDispatcher );
        return raftServer;
    }
}
