package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.common.ClusteringEditionModule;
import com.neo4j.causalclustering.discovery.DiscoveryServiceFactory;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.graphdb.facade.DatabaseManagementServiceFactory;
import org.neo4j.graphdb.facade.ExternalDependencies;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.impl.factory.DatabaseInfo;

public class CoreGraphDatabase
{
    private final DatabaseManagementService managementService;

    public CoreGraphDatabase( Config config, ExternalDependencies dependencies, DiscoveryServiceFactory discoveryServiceFactory,
            CoreGraphDatabase.CoreEditionModuleFactory editionModuleFactory )
    {
        this.managementService = this.createManagementService( config, dependencies, discoveryServiceFactory, editionModuleFactory );
    }

    protected DatabaseManagementService createManagementService( Config config, ExternalDependencies dependencies,
            DiscoveryServiceFactory discoveryServiceFactory, CoreGraphDatabase.CoreEditionModuleFactory editionModuleFactory )
    {
        return (new DatabaseManagementServiceFactory( DatabaseInfo.CORE, ( globalModule ) -> {
            return editionModuleFactory.create( globalModule, discoveryServiceFactory );
        } )).build( config, dependencies );
    }

    public DatabaseManagementService getManagementService()
    {
        return this.managementService;
    }

    public interface CoreEditionModuleFactory
    {
        ClusteringEditionModule create( GlobalModule var1, DiscoveryServiceFactory var2 );
    }
}
