package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.routing.load_balancing.LeaderService;
import com.neo4j.causalclustering.routing.load_balancing.LoadBalancingPluginLoader;
import com.neo4j.causalclustering.routing.load_balancing.LoadBalancingProcessor;
import com.neo4j.causalclustering.routing.load_balancing.procedure.GetRoutingTableProcedureForMultiDC;
import com.neo4j.causalclustering.routing.load_balancing.procedure.GetRoutingTableProcedureForSingleDC;

import java.util.List;

import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.api.procedure.CallableProcedure;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.BaseRoutingProcedureInstaller;

public class CoreRoutingProcedureInstaller extends BaseRoutingProcedureInstaller
{
    private final TopologyService topologyService;
    private final LeaderService leaderService;
    private final DatabaseManager<?> databaseManager;
    private final Config config;
    private final LogProvider logProvider;

    public CoreRoutingProcedureInstaller( TopologyService topologyService, LeaderService leaderService, DatabaseManager<?> databaseManager, Config config,
            LogProvider logProvider )
    {
        this.topologyService = topologyService;
        this.leaderService = leaderService;
        this.databaseManager = databaseManager;
        this.config = config;
        this.logProvider = logProvider;
    }

    protected CallableProcedure createProcedure( List<String> namespace )
    {
        if ( (Boolean) this.config.get( CausalClusteringSettings.multi_dc_license ) )
        {
            LoadBalancingProcessor loadBalancingProcessor = this.loadLoadBalancingProcessor();
            return new GetRoutingTableProcedureForMultiDC( namespace, loadBalancingProcessor, this.databaseManager, this.config, this.logProvider );
        }
        else
        {
            return new GetRoutingTableProcedureForSingleDC( namespace, this.topologyService, this.leaderService, this.databaseManager, this.config,
                    this.logProvider );
        }
    }

    private LoadBalancingProcessor loadLoadBalancingProcessor()
    {
        try
        {
            return LoadBalancingPluginLoader.load( this.topologyService, this.leaderService, this.logProvider, this.config );
        }
        catch ( Throwable var2 )
        {
            throw new RuntimeException( var2 );
        }
    }
}
