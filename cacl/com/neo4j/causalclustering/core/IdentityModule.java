package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.common.state.ClusterStateStorageFactory;
import com.neo4j.causalclustering.core.state.storage.SimpleStorage;
import com.neo4j.causalclustering.identity.MemberId;

import java.io.IOException;
import java.util.UUID;

import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.logging.Log;

public class IdentityModule
{
    private MemberId myself;

    IdentityModule( GlobalModule globalModule, ClusterStateStorageFactory storageFactory )
    {
        Log log = globalModule.getLogService().getInternalLogProvider().getLog( this.getClass() );
        SimpleStorage memberIdStorage = storageFactory.createMemberIdStorage();

        try
        {
            if ( memberIdStorage.exists() )
            {
                this.myself = (MemberId) memberIdStorage.readState();
                if ( this.myself == null )
                {
                    throw new RuntimeException( "I was null" );
                }
            }
            else
            {
                UUID uuid = UUID.randomUUID();
                this.myself = new MemberId( uuid );
                memberIdStorage.writeState( this.myself );
                log.info( String.format( "Generated new id: %s (%s)", this.myself, uuid ) );
            }
        }
        catch ( IOException var6 )
        {
            throw new RuntimeException( var6 );
        }

        globalModule.getJobScheduler().setTopLevelGroupName( "Core " + this.myself );
    }

    public MemberId myself()
    {
        return this.myself;
    }
}
