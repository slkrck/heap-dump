package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.routing.load_balancing.LoadBalancingPluginLoader;
import com.neo4j.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;

import java.io.File;
import java.nio.file.Path;
import java.util.Map;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.GroupSettingValidator;
import org.neo4j.configuration.connectors.BoltConnector;
import org.neo4j.graphdb.config.Setting;
import org.neo4j.logging.Log;

public class CausalClusterConfigurationValidator implements GroupSettingValidator
{
    public String getPrefix()
    {
        return "causal_clustering";
    }

    public String getDescription()
    {
        return "Validates causal clustering settings";
    }

    public void validate( Map<Setting<?>,Object> values, Config config )
    {
        EnterpriseEditionSettings.Mode mode = (EnterpriseEditionSettings.Mode) config.get( EnterpriseEditionSettings.mode );
        if ( mode.equals( EnterpriseEditionSettings.Mode.CORE ) || mode.equals( EnterpriseEditionSettings.Mode.READ_REPLICA ) )
        {
            this.validateInitialDiscoveryMembers( config );
            this.validateBoltConnector( config );
            LoadBalancingPluginLoader.validate( config, (Log) null );
            this.validateDeclaredClusterSizes( config );
            this.validateMiddleware( config );
        }
    }

    private void validateMiddleware( Config config )
    {
        Path akkaConfig = (Path) config.get( CausalClusteringSettings.middleware_akka_external_config );
        if ( akkaConfig != null )
        {
            File akkaConfigFile = akkaConfig.toFile();
            if ( !akkaConfigFile.exists() || !akkaConfigFile.isFile() )
            {
                throw new IllegalArgumentException(
                        String.format( "'%s' must be a file or empty", CausalClusteringSettings.middleware_akka_external_config.name() ) );
            }

            try
            {
                ConfigFactory.parseFileAnySyntax( akkaConfigFile );
            }
            catch ( ConfigException var5 )
            {
                throw new IllegalArgumentException( String.format( "'%s' could not be parsed", akkaConfig ), var5 );
            }
        }
    }

    private void validateDeclaredClusterSizes( Config config )
    {
        int startup = (Integer) config.get( CausalClusteringSettings.minimum_core_cluster_size_at_formation );
        int runtime = (Integer) config.get( CausalClusteringSettings.minimum_core_cluster_size_at_runtime );
        if ( runtime > startup )
        {
            throw new IllegalArgumentException(
                    String.format( "'%s' must be set greater than or equal to '%s'", CausalClusteringSettings.minimum_core_cluster_size_at_formation.name(),
                            CausalClusteringSettings.minimum_core_cluster_size_at_runtime.name() ) );
        }
    }

    private void validateBoltConnector( Config config )
    {
        if ( !(Boolean) config.get( BoltConnector.enabled ) )
        {
            throw new IllegalArgumentException( "A Bolt connector must be configured to run a cluster" );
        }
    }

    private void validateInitialDiscoveryMembers( Config config )
    {
        DiscoveryType discoveryType = (DiscoveryType) config.get( CausalClusteringSettings.discovery_type );
        discoveryType.requiredSettings().forEach( ( setting ) -> {
            if ( !config.isExplicitlySet( setting ) )
            {
                throw new IllegalArgumentException( String.format( "Missing value for '%s', which is mandatory with '%s=%s'", setting.name(),
                        CausalClusteringSettings.discovery_type.name(), discoveryType ) );
            }
        } );
    }
}
