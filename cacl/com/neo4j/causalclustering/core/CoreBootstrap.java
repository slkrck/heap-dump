package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.state.BootstrapSaver;
import com.neo4j.causalclustering.core.state.CoreSnapshotService;
import com.neo4j.causalclustering.core.state.snapshot.CoreDownloaderService;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;
import com.neo4j.causalclustering.core.state.storage.SimpleStorage;
import com.neo4j.causalclustering.identity.BoundState;
import com.neo4j.causalclustering.identity.RaftBinder;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.LifecycleMessageHandler;
import com.neo4j.dbms.ClusterInternalDbmsOperator;
import com.neo4j.dbms.DatabaseStartAborter;

import java.time.Duration;
import java.util.Optional;

import org.neo4j.kernel.database.Database;
import org.neo4j.scheduler.JobHandle;

class CoreBootstrap
{
    private final Database kernelDatabase;
    private final RaftBinder raftBinder;
    private final LifecycleMessageHandler<?> raftMessageHandler;
    private final CoreSnapshotService snapshotService;
    private final CoreDownloaderService downloadService;
    private final ClusterInternalDbmsOperator clusterInternalOperator;
    private final DatabaseStartAborter databaseStartAborter;
    private final SimpleStorage<RaftId> raftIdStorage;
    private final BootstrapSaver bootstrapSaver;
    private final TempBootstrapDir tempBootstrapDir;

    CoreBootstrap( Database kernelDatabase, RaftBinder raftBinder, LifecycleMessageHandler<?> raftMessageHandler, CoreSnapshotService snapshotService,
            CoreDownloaderService downloadService, ClusterInternalDbmsOperator clusterInternalOperator, DatabaseStartAborter databaseStartAborter,
            SimpleStorage<RaftId> raftIdStorage, BootstrapSaver bootstrapSaver, TempBootstrapDir tempBootstrapDir )
    {
        this.kernelDatabase = kernelDatabase;
        this.raftBinder = raftBinder;
        this.raftMessageHandler = raftMessageHandler;
        this.snapshotService = snapshotService;
        this.downloadService = downloadService;
        this.clusterInternalOperator = clusterInternalOperator;
        this.databaseStartAborter = databaseStartAborter;
        this.raftIdStorage = raftIdStorage;
        this.bootstrapSaver = bootstrapSaver;
        this.tempBootstrapDir = tempBootstrapDir;
    }

    public void perform() throws Exception
    {
        ClusterInternalDbmsOperator.BootstrappingHandle bootstrapHandle = this.clusterInternalOperator.bootstrap( this.kernelDatabase.getNamedDatabaseId() );

        try
        {
            this.bindAndStartMessageHandler();
        }
        finally
        {
            bootstrapHandle.release();
            this.databaseStartAborter.started( this.kernelDatabase.getNamedDatabaseId() );
        }
    }

    private void bindAndStartMessageHandler() throws Exception
    {
        this.bootstrapSaver.restore( this.kernelDatabase.getDatabaseLayout() );
        this.tempBootstrapDir.delete();
        BoundState boundState = this.raftBinder.bindToRaft( this.databaseStartAborter );
        if ( boundState.snapshot().isPresent() )
        {
            this.snapshotService.installSnapshot( (CoreSnapshot) boundState.snapshot().get() );
            this.raftMessageHandler.start( boundState.raftId() );
        }
        else
        {
            this.raftMessageHandler.start( boundState.raftId() );

            try
            {
                this.awaitState();
                this.bootstrapSaver.clean( this.kernelDatabase.getDatabaseLayout() );
            }
            catch ( Exception var3 )
            {
                this.raftMessageHandler.stop();
                throw var3;
            }
        }

        if ( this.raftIdStorage.exists() )
        {
            RaftId raftIdStore = (RaftId) this.raftIdStorage.readState();
            if ( !raftIdStore.equals( boundState.raftId() ) )
            {
                throw new IllegalStateException(
                        String.format( "Exiting raft id '%s' is different from bound state '%s'.", raftIdStore.uuid(), boundState.raftId().uuid() ) );
            }
        }
        else
        {
            this.raftIdStorage.writeState( boundState.raftId() );
        }
    }

    private void awaitState() throws Exception
    {
        Duration waitTime = Duration.ofSeconds( 1L );
        this.snapshotService.awaitState( this.databaseStartAborter, waitTime );
        Optional<JobHandle> downloadJob = this.downloadService.downloadJob();
        if ( downloadJob.isPresent() )
        {
            ((JobHandle) downloadJob.get()).waitTermination();
        }
    }
}
