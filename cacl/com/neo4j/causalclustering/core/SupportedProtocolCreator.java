package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.protocol.Protocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolCategory;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import com.neo4j.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocolCategory;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocols;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.configuration.Config;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SupportedProtocolCreator
{
    private final Config config;
    private final Log log;

    public SupportedProtocolCreator( Config config, LogProvider logProvider )
    {
        this.config = config;
        this.log = logProvider.getLog( this.getClass() );
    }

    public ApplicationSupportedProtocols getSupportedCatchupProtocolsFromConfiguration()
    {
        return this.getApplicationSupportedProtocols( (List) this.config.get( CausalClusteringSettings.catchup_implementations ),
                ApplicationProtocolCategory.CATCHUP );
    }

    public ApplicationSupportedProtocols getSupportedRaftProtocolsFromConfiguration()
    {
        return this.getApplicationSupportedProtocols( (List) this.config.get( CausalClusteringSettings.raft_implementations ),
                ApplicationProtocolCategory.RAFT );
    }

    private ApplicationSupportedProtocols getApplicationSupportedProtocols( List<ApplicationProtocolVersion> configVersions,
            ApplicationProtocolCategory category )
    {
        if ( configVersions.isEmpty() )
        {
            return new ApplicationSupportedProtocols( category, Collections.emptyList() );
        }
        else
        {
            List<ApplicationProtocolVersion> knownVersions = this.protocolsForConfig( category, configVersions, ( version ) -> {
                return ApplicationProtocols.find( category, version );
            } );
            if ( knownVersions.isEmpty() )
            {
                throw new IllegalArgumentException(
                        String.format( "None of configured %s implementations %s are known", category.canonicalName(), configVersions ) );
            }
            else
            {
                return new ApplicationSupportedProtocols( category, knownVersions );
            }
        }
    }

    public List<ModifierSupportedProtocols> createSupportedModifierProtocols()
    {
        ModifierSupportedProtocols supportedCompression = this.compressionProtocolVersions();
        return (List) Stream.of( supportedCompression ).filter( ( supportedProtocols ) -> {
            return !supportedProtocols.versions().isEmpty();
        } ).collect( Collectors.toList() );
    }

    private ModifierSupportedProtocols compressionProtocolVersions()
    {
        List<String> implementations =
                this.protocolsForConfig( ModifierProtocolCategory.COMPRESSION, (List) this.config.get( CausalClusteringSettings.compression_implementations ),
                        ( implementation ) -> {
                            return ModifierProtocols.find( ModifierProtocolCategory.COMPRESSION, implementation );
                        } );
        return new ModifierSupportedProtocols( ModifierProtocolCategory.COMPRESSION, implementations );
    }

    private <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> List<IMPL> protocolsForConfig( Protocol.Category<T> category, List<IMPL> implementations,
            Function<IMPL,Optional<T>> finder )
    {
        return (List) implementations.stream().map( ( impl ) -> {
            return Pair.of( impl, (Optional) finder.apply( impl ) );
        } ).peek( ( protocolWithImplementation ) -> {
            this.logUnknownProtocol( category, protocolWithImplementation );
        } ).map( Pair::other ).flatMap( Optional::stream ).map( Protocol::implementation ).collect( Collectors.toList() );
    }

    private <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> void logUnknownProtocol( Protocol.Category<T> category,
            Pair<IMPL,Optional<T>> protocolWithImplementation )
    {
        if ( ((Optional) protocolWithImplementation.other()).isEmpty() )
        {
            this.log.warn( "Configured %s protocol implementation %s unknown. Ignoring.", new Object[]{category, protocolWithImplementation.first()} );
        }
    }
}
