package com.neo4j.causalclustering.core;

import org.neo4j.configuration.GroupSetting;

public abstract class LoadBalancingPluginGroup extends GroupSetting
{
    private final String pluginName;

    protected LoadBalancingPluginGroup( String name, String pluginName )
    {
        super( name );
        this.pluginName = pluginName;
    }

    public String getPrefix()
    {
        return "causal_clustering.load_balancing.config." + this.pluginName;
    }
}
