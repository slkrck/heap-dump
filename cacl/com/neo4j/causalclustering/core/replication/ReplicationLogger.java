package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.identity.MemberId;
import org.neo4j.logging.Log;

class ReplicationLogger
{
    private final Log log;
    private int attempts;

    ReplicationLogger( Log log )
    {
        this.log = log;
    }

    void newAttempt( DistributedOperation operation, MemberId leader )
    {
        ++this.attempts;
        if ( this.attempts > 1 )
        {
            this.log.info( String.format( "Replication attempt %d to leader %s: %s", this.attempts, leader, operation ) );
        }
    }

    void success( DistributedOperation operation )
    {
        if ( this.attempts > 1 )
        {
            this.log.info( String.format( "Successfully replicated after attempt %d: %s", this.attempts, operation ) );
        }
    }
}
