package com.neo4j.causalclustering.core.replication;

public interface Replicator
{
    ReplicationResult replicate( ReplicatedContent var1 );
}
