package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.state.StateMachineResult;

public interface ProgressTracker
{
    Progress start( DistributedOperation var1 );

    void trackReplication( DistributedOperation var1 );

    void trackResult( DistributedOperation var1, StateMachineResult var2 );

    void abort( DistributedOperation var1 );

    void triggerReplicationEvent();

    int inProgressCount();
}
