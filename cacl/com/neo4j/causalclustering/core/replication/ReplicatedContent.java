package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;

import java.io.IOException;
import java.util.OptionalLong;

public interface ReplicatedContent
{
    default OptionalLong size()
    {
        return OptionalLong.empty();
    }

    void dispatch( ReplicatedContentHandler var1 ) throws IOException;
}
