package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.state.machines.dummy.DummyRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.logging.Log;
import org.neo4j.procedure.Admin;
import org.neo4j.procedure.Context;
import org.neo4j.procedure.Description;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;

public class ReplicationBenchmarkProcedure
{
    private static long startTime;
    private static List<ReplicationBenchmarkProcedure.Worker> workers;
    @Context
    public Replicator replicator;
    @Context
    public SecurityContext securityContext;
    @Context
    public Log log;

    @Admin
    @Description( "Start the benchmark." )
    @Procedure( name = "dbms.cluster.benchmark.start", mode = Mode.DBMS )
    public synchronized void start( @Name( "nThreads" ) Long nThreads, @Name( "blockSize" ) Long blockSize )
    {
        if ( workers != null )
        {
            throw new IllegalStateException( "Already running." );
        }
        else
        {
            this.log.info( "Starting replication benchmark procedure" );
            startTime = System.currentTimeMillis();
            workers = new ArrayList( Math.toIntExact( nThreads ) );

            for ( int i = 0; (long) i < nThreads; ++i )
            {
                ReplicationBenchmarkProcedure.Worker worker = new ReplicationBenchmarkProcedure.Worker( Math.toIntExact( blockSize ) );
                workers.add( worker );
                worker.start();
            }
        }
    }

    @Admin
    @Description( "Stop a running benchmark." )
    @Procedure( name = "dbms.cluster.benchmark.stop", mode = Mode.DBMS )
    public synchronized Stream<BenchmarkResult> stop() throws InterruptedException
    {
        if ( workers == null )
        {
            throw new IllegalStateException( "Not running." );
        }
        else
        {
            this.log.info( "Stopping replication benchmark procedure" );
            Iterator var1 = workers.iterator();

            ReplicationBenchmarkProcedure.Worker worker;
            while ( var1.hasNext() )
            {
                worker = (ReplicationBenchmarkProcedure.Worker) var1.next();
                worker.stop();
            }

            var1 = workers.iterator();

            while ( var1.hasNext() )
            {
                worker = (ReplicationBenchmarkProcedure.Worker) var1.next();
                worker.join();
            }

            long runTime = System.currentTimeMillis() - startTime;
            long totalRequests = 0L;
            long totalBytes = 0L;

            ReplicationBenchmarkProcedure.Worker worker;
            for ( Iterator var7 = workers.iterator(); var7.hasNext(); totalBytes += worker.totalBytes )
            {
                worker = (ReplicationBenchmarkProcedure.Worker) var7.next();
                totalRequests += worker.totalRequests;
            }

            workers = null;
            return Stream.of( new BenchmarkResult( totalRequests, totalBytes, runTime ) );
        }
    }

    private class Worker implements Runnable
    {
        private final int blockSize;
        long totalRequests;
        long totalBytes;
        private Thread t;
        private volatile boolean stopped;

        Worker( int blockSize )
        {
            this.blockSize = blockSize;
        }

        void start()
        {
            this.t = new Thread( this );
            this.t.start();
        }

        public void run()
        {
            while ( true )
            {
                try
                {
                    if ( !this.stopped )
                    {
                        ReplicationResult replicationResult =
                                ReplicationBenchmarkProcedure.this.replicator.replicate( new DummyRequest( new byte[this.blockSize] ) );
                        if ( replicationResult.outcome() != ReplicationResult.Outcome.APPLIED )
                        {
                            throw new RuntimeException( "Failed to replicate", replicationResult.failure() );
                        }

                        DummyRequest request = (DummyRequest) replicationResult.stateMachineResult().consume();
                        ++this.totalRequests;
                        this.totalBytes += request.byteCount();
                        continue;
                    }
                }
                catch ( Throwable var3 )
                {
                    ReplicationBenchmarkProcedure.this.log.error( "Worker exception", var3 );
                }

                return;
            }
        }

        void stop()
        {
            this.stopped = true;
        }

        void join() throws InterruptedException
        {
            this.t.join();
        }
    }
}
