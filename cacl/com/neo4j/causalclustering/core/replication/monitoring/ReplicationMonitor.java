package com.neo4j.causalclustering.core.replication.monitoring;

public interface ReplicationMonitor
{
    void clientRequest();

    void replicationAttempt();

    void notReplicated();

    void maybeReplicated();

    void successfullyReplicated();
}
