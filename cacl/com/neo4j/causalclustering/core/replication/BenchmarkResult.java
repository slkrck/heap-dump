package com.neo4j.causalclustering.core.replication;

public class BenchmarkResult
{
    public Long totalRequests;
    public Long totalBytes;
    public Long timeMillis;
    public Double opsPerMilli;
    public Double mbPerSecond;

    BenchmarkResult( long totalRequests, long totalBytes, long totalTimeMillis )
    {
        this.totalRequests = totalRequests;
        this.totalBytes = totalBytes;
        this.timeMillis = totalTimeMillis;
        this.opsPerMilli = (double) totalRequests / (double) totalTimeMillis;
        this.mbPerSecond = (double) totalBytes / (double) totalTimeMillis / 1048576.0D * 1000.0D;
    }
}
