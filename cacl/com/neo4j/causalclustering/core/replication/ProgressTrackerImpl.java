package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.replication.session.GlobalSession;
import com.neo4j.causalclustering.core.replication.session.LocalOperationId;
import com.neo4j.causalclustering.core.state.StateMachineResult;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class ProgressTrackerImpl implements ProgressTracker
{
    private final Map<LocalOperationId,Progress> tracker = new ConcurrentHashMap();
    private final GlobalSession myGlobalSession;

    public ProgressTrackerImpl( GlobalSession myGlobalSession )
    {
        this.myGlobalSession = myGlobalSession;
    }

    public Progress start( DistributedOperation operation )
    {
        assert operation.globalSession().equals( this.myGlobalSession );

        Progress progress = new Progress();
        this.tracker.put( operation.operationId(), progress );
        return progress;
    }

    public void trackReplication( DistributedOperation operation )
    {
        if ( operation.globalSession().equals( this.myGlobalSession ) )
        {
            Progress progress = (Progress) this.tracker.get( operation.operationId() );
            if ( progress != null )
            {
                progress.setReplicated();
            }
        }
    }

    public void trackResult( DistributedOperation operation, StateMachineResult result )
    {
        Objects.requireNonNull( result, "Illegal result for operation: " + operation );
        if ( operation.globalSession().equals( this.myGlobalSession ) )
        {
            Progress progress = (Progress) this.tracker.remove( operation.operationId() );
            if ( progress != null )
            {
                progress.registerResult( result );
            }
        }
    }

    public void abort( DistributedOperation operation )
    {
        this.tracker.remove( operation.operationId() );
    }

    public void triggerReplicationEvent()
    {
        this.tracker.forEach( ( ignored, progress ) -> {
            progress.triggerReplicationEvent();
        } );
    }

    public int inProgressCount()
    {
        return this.tracker.size();
    }
}
