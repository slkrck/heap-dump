package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.state.StateMachineResult;

import java.util.Objects;

public class ReplicationResult
{
    private final ReplicationResult.Outcome outcome;
    private final Throwable failure;
    private final StateMachineResult stateMachineResult;

    private ReplicationResult( ReplicationResult.Outcome outcome, StateMachineResult stateMachineResult )
    {
        Objects.requireNonNull( stateMachineResult, "Illegal result: " + stateMachineResult );
        this.outcome = outcome;
        this.failure = null;
        this.stateMachineResult = stateMachineResult;
    }

    private ReplicationResult( ReplicationResult.Outcome outcome, Throwable failure )
    {
        Objects.requireNonNull( failure, "Illegal failure: " + failure );
        this.outcome = outcome;
        this.stateMachineResult = null;
        this.failure = failure;
    }

    public static ReplicationResult notReplicated( Throwable failure )
    {
        return new ReplicationResult( ReplicationResult.Outcome.NOT_REPLICATED, failure );
    }

    public static ReplicationResult maybeReplicated( Throwable failure )
    {
        return new ReplicationResult( ReplicationResult.Outcome.MAYBE_REPLICATED, failure );
    }

    public static ReplicationResult applied( StateMachineResult stateMachineResult )
    {
        return new ReplicationResult( ReplicationResult.Outcome.APPLIED, stateMachineResult );
    }

    public ReplicationResult.Outcome outcome()
    {
        return this.outcome;
    }

    public Throwable failure()
    {
        return this.failure;
    }

    public StateMachineResult stateMachineResult()
    {
        return this.stateMachineResult;
    }

    public static enum Outcome
    {
        NOT_REPLICATED,
        MAYBE_REPLICATED,
        APPLIED;
    }
}
