package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.state.StateMachineResult;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Progress
{
    private final Semaphore replicationSignal = new Semaphore( 0 );
    private final Semaphore resultSignal = new Semaphore( 0 );
    private volatile boolean isReplicated;
    private volatile StateMachineResult result;

    public void triggerReplicationEvent()
    {
        this.replicationSignal.release();
    }

    public void setReplicated()
    {
        this.isReplicated = true;
        this.replicationSignal.release();
    }

    public void awaitReplication( long timeoutMillis ) throws InterruptedException
    {
        if ( !this.isReplicated )
        {
            this.replicationSignal.tryAcquire( timeoutMillis, TimeUnit.MILLISECONDS );
        }
    }

    public void awaitResult() throws InterruptedException
    {
        if ( this.result == null )
        {
            this.resultSignal.acquire();
        }
    }

    public boolean isReplicated()
    {
        return this.isReplicated;
    }

    void registerResult( StateMachineResult result )
    {
        this.result = result;
        this.resultSignal.release();
    }

    public StateMachineResult result()
    {
        return this.result;
    }
}
