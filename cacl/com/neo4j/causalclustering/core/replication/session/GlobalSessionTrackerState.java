package com.neo4j.causalclustering.core.replication.session;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.Map.Entry;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class GlobalSessionTrackerState
{
    private Map<MemberId,GlobalSessionTrackerState.LocalSessionTracker> sessionTrackers = new HashMap();
    private long logIndex = -1L;

    public boolean validateOperation( GlobalSession globalSession, LocalOperationId localOperationId )
    {
        GlobalSessionTrackerState.LocalSessionTracker existingSessionTracker =
                (GlobalSessionTrackerState.LocalSessionTracker) this.sessionTrackers.get( globalSession.owner() );
        return this.isNewSession( globalSession, existingSessionTracker ) ? this.isFirstOperation( localOperationId )
                                                                          : existingSessionTracker.isValidOperation( localOperationId );
    }

    public void update( GlobalSession globalSession, LocalOperationId localOperationId, long logIndex )
    {
        GlobalSessionTrackerState.LocalSessionTracker localSessionTracker = this.validateGlobalSessionAndGetLocalSessionTracker( globalSession );
        localSessionTracker.validateAndTrackOperation( localOperationId );
        this.logIndex = logIndex;
    }

    private boolean isNewSession( GlobalSession globalSession, GlobalSessionTrackerState.LocalSessionTracker existingSessionTracker )
    {
        return existingSessionTracker == null || !existingSessionTracker.globalSessionId.equals( globalSession.sessionId() );
    }

    private boolean isFirstOperation( LocalOperationId id )
    {
        return id.sequenceNumber() == 0L;
    }

    public long logIndex()
    {
        return this.logIndex;
    }

    private GlobalSessionTrackerState.LocalSessionTracker validateGlobalSessionAndGetLocalSessionTracker( GlobalSession globalSession )
    {
        GlobalSessionTrackerState.LocalSessionTracker localSessionTracker =
                (GlobalSessionTrackerState.LocalSessionTracker) this.sessionTrackers.get( globalSession.owner() );
        if ( localSessionTracker == null || !localSessionTracker.globalSessionId.equals( globalSession.sessionId() ) )
        {
            localSessionTracker = new GlobalSessionTrackerState.LocalSessionTracker( globalSession.sessionId(), new HashMap() );
            this.sessionTrackers.put( globalSession.owner(), localSessionTracker );
        }

        return localSessionTracker;
    }

    public GlobalSessionTrackerState newInstance()
    {
        GlobalSessionTrackerState copy = new GlobalSessionTrackerState();
        copy.logIndex = this.logIndex;
        Iterator var2 = this.sessionTrackers.entrySet().iterator();

        while ( var2.hasNext() )
        {
            Entry<MemberId,GlobalSessionTrackerState.LocalSessionTracker> entry = (Entry) var2.next();
            copy.sessionTrackers.put( (MemberId) entry.getKey(), ((GlobalSessionTrackerState.LocalSessionTracker) entry.getValue()).newInstance() );
        }

        return copy;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            GlobalSessionTrackerState that = (GlobalSessionTrackerState) o;
            return this.logIndex == that.logIndex && Objects.equals( this.sessionTrackers, that.sessionTrackers );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.sessionTrackers, this.logIndex} );
    }

    public String toString()
    {
        return String.format( "GlobalSessionTrackerState{sessionTrackers=%s, logIndex=%d}", this.sessionTrackers, this.logIndex );
    }

    private static class LocalSessionTracker
    {
        final UUID globalSessionId;
        final Map<Long,Long> lastSequenceNumberPerSession;

        LocalSessionTracker( UUID globalSessionId, Map<Long,Long> lastSequenceNumberPerSession )
        {
            this.globalSessionId = globalSessionId;
            this.lastSequenceNumberPerSession = lastSequenceNumberPerSession;
        }

        boolean validateAndTrackOperation( LocalOperationId operationId )
        {
            if ( !this.isValidOperation( operationId ) )
            {
                return false;
            }
            else
            {
                this.lastSequenceNumberPerSession.put( operationId.localSessionId(), operationId.sequenceNumber() );
                return true;
            }
        }

        private boolean isValidOperation( LocalOperationId operationId )
        {
            Long lastSequenceNumber = (Long) this.lastSequenceNumberPerSession.get( operationId.localSessionId() );
            if ( lastSequenceNumber == null )
            {
                if ( operationId.sequenceNumber() != 0L )
                {
                    return false;
                }
            }
            else if ( operationId.sequenceNumber() != lastSequenceNumber + 1L )
            {
                return false;
            }

            return true;
        }

        public GlobalSessionTrackerState.LocalSessionTracker newInstance()
        {
            return new GlobalSessionTrackerState.LocalSessionTracker( this.globalSessionId, new HashMap( this.lastSequenceNumberPerSession ) );
        }

        public String toString()
        {
            return String.format( "LocalSessionTracker{globalSessionId=%s, lastSequenceNumberPerSession=%s}", this.globalSessionId,
                    this.lastSequenceNumberPerSession );
        }
    }

    public static class Marshal extends SafeStateMarshal<GlobalSessionTrackerState>
    {
        private final ChannelMarshal<MemberId> memberMarshal;

        public Marshal()
        {
            this.memberMarshal = MemberId.Marshal.INSTANCE;
        }

        public void marshal( GlobalSessionTrackerState target, WritableChannel channel ) throws IOException
        {
            Map<MemberId,GlobalSessionTrackerState.LocalSessionTracker> sessionTrackers = target.sessionTrackers;
            channel.putLong( target.logIndex );
            channel.putInt( sessionTrackers.size() );
            Iterator var4 = sessionTrackers.entrySet().iterator();

            while ( var4.hasNext() )
            {
                Entry<MemberId,GlobalSessionTrackerState.LocalSessionTracker> entry = (Entry) var4.next();
                this.memberMarshal.marshal( (MemberId) entry.getKey(), channel );
                GlobalSessionTrackerState.LocalSessionTracker localSessionTracker = (GlobalSessionTrackerState.LocalSessionTracker) entry.getValue();
                UUID uuid = localSessionTracker.globalSessionId;
                channel.putLong( uuid.getMostSignificantBits() );
                channel.putLong( uuid.getLeastSignificantBits() );
                Map<Long,Long> map = localSessionTracker.lastSequenceNumberPerSession;
                channel.putInt( map.size() );
                Iterator var9 = map.entrySet().iterator();

                while ( var9.hasNext() )
                {
                    Entry<Long,Long> sessionSequence = (Entry) var9.next();
                    channel.putLong( (Long) sessionSequence.getKey() );
                    channel.putLong( (Long) sessionSequence.getValue() );
                }
            }
        }

        public GlobalSessionTrackerState unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
        {
            long logIndex = channel.getLong();
            int sessionTrackerSize = channel.getInt();
            Map<MemberId,GlobalSessionTrackerState.LocalSessionTracker> sessionTrackers = new HashMap();

            for ( int i = 0; i < sessionTrackerSize; ++i )
            {
                MemberId member = (MemberId) this.memberMarshal.unmarshal( channel );
                if ( member == null )
                {
                    throw new IllegalStateException( "Null member" );
                }

                long mostSigBits = channel.getLong();
                long leastSigBits = channel.getLong();
                UUID globalSessionId = new UUID( mostSigBits, leastSigBits );
                int localSessionTrackerSize = channel.getInt();
                Map<Long,Long> lastSequenceNumberPerSession = new HashMap();

                for ( int j = 0; j < localSessionTrackerSize; ++j )
                {
                    long localSessionId = channel.getLong();
                    long sequenceNumber = channel.getLong();
                    lastSequenceNumberPerSession.put( localSessionId, sequenceNumber );
                }

                GlobalSessionTrackerState.LocalSessionTracker localSessionTracker =
                        new GlobalSessionTrackerState.LocalSessionTracker( globalSessionId, lastSequenceNumberPerSession );
                sessionTrackers.put( member, localSessionTracker );
            }

            GlobalSessionTrackerState result = new GlobalSessionTrackerState();
            result.sessionTrackers = sessionTrackers;
            result.logIndex = logIndex;
            return result;
        }

        public GlobalSessionTrackerState startState()
        {
            return new GlobalSessionTrackerState();
        }

        public long ordinal( GlobalSessionTrackerState state )
        {
            return state.logIndex();
        }
    }
}
