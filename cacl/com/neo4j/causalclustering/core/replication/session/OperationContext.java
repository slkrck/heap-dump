package com.neo4j.causalclustering.core.replication.session;

public class OperationContext
{
    private final GlobalSession globalSession;
    private final LocalOperationId localOperationId;
    private final LocalSession localSession;

    public OperationContext( GlobalSession globalSession, LocalOperationId localOperationId, LocalSession localSession )
    {
        this.globalSession = globalSession;
        this.localOperationId = localOperationId;
        this.localSession = localSession;
    }

    public GlobalSession globalSession()
    {
        return this.globalSession;
    }

    public LocalOperationId localOperationId()
    {
        return this.localOperationId;
    }

    protected LocalSession localSession()
    {
        return this.localSession;
    }

    public String toString()
    {
        return "OperationContext{globalSession=" + this.globalSession + ", localOperationId=" + this.localOperationId + "}";
    }
}
