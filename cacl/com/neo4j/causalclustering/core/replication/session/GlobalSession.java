package com.neo4j.causalclustering.core.replication.session;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.UUID;

public class GlobalSession
{
    private final UUID sessionId;
    private final MemberId owner;

    public GlobalSession( UUID sessionId, MemberId owner )
    {
        this.sessionId = sessionId;
        this.owner = owner;
    }

    public UUID sessionId()
    {
        return this.sessionId;
    }

    public MemberId owner()
    {
        return this.owner;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            GlobalSession that = (GlobalSession) o;
            return !this.sessionId.equals( that.sessionId ) ? false : this.owner.equals( that.owner );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.sessionId.hashCode();
        result = 31 * result + this.owner.hashCode();
        return result;
    }

    public String toString()
    {
        return String.format( "GlobalSession{sessionId=%s, owner=%s}", this.sessionId, this.owner );
    }
}
