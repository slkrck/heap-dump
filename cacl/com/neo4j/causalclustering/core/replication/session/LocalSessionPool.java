package com.neo4j.causalclustering.core.replication.session;

import java.util.ArrayDeque;
import java.util.Deque;

public class LocalSessionPool
{
    private final Deque<LocalSession> sessionStack = new ArrayDeque();
    private final GlobalSession globalSession;
    private long nextLocalSessionId;

    public LocalSessionPool( GlobalSession globalSession )
    {
        this.globalSession = globalSession;
    }

    private LocalSession createSession()
    {
        return new LocalSession( (long) (this.nextLocalSessionId++) );
    }

    public GlobalSession getGlobalSession()
    {
        return this.globalSession;
    }

    public synchronized OperationContext acquireSession()
    {
        LocalSession localSession = this.sessionStack.isEmpty() ? this.createSession() : (LocalSession) this.sessionStack.pop();
        return new OperationContext( this.globalSession, localSession.nextOperationId(), localSession );
    }

    public synchronized void releaseSession( OperationContext operationContext )
    {
        this.sessionStack.push( operationContext.localSession() );
    }

    public synchronized long openSessionCount()
    {
        return this.nextLocalSessionId - (long) this.sessionStack.size();
    }
}
