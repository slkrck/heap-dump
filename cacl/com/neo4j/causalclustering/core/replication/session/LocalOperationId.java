package com.neo4j.causalclustering.core.replication.session;

public class LocalOperationId
{
    private final long localSessionId;
    private final long sequenceNumber;

    public LocalOperationId( long localSessionId, long sequenceNumber )
    {
        this.localSessionId = localSessionId;
        this.sequenceNumber = sequenceNumber;
    }

    public long localSessionId()
    {
        return this.localSessionId;
    }

    public long sequenceNumber()
    {
        return this.sequenceNumber;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            LocalOperationId that = (LocalOperationId) o;
            if ( this.localSessionId != that.localSessionId )
            {
                return false;
            }
            else
            {
                return this.sequenceNumber == that.sequenceNumber;
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = (int) (this.localSessionId ^ this.localSessionId >>> 32);
        result = 31 * result + (int) (this.sequenceNumber ^ this.sequenceNumber >>> 32);
        return result;
    }

    public String toString()
    {
        return String.format( "LocalOperationId{localSessionId=%d, sequenceNumber=%d}", this.localSessionId, this.sequenceNumber );
    }
}
