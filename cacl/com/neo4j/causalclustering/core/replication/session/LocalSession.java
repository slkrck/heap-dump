package com.neo4j.causalclustering.core.replication.session;

public class LocalSession
{
    private long localSessionId;
    private long currentSequenceNumber;

    public LocalSession( long localSessionId )
    {
        this.localSessionId = localSessionId;
    }

    protected LocalOperationId nextOperationId()
    {
        return new LocalOperationId( this.localSessionId, (long) (this.currentSequenceNumber++) );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            LocalSession that = (LocalSession) o;
            return this.localSessionId == that.localSessionId;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return (int) (this.localSessionId ^ this.localSessionId >>> 32);
    }

    public String toString()
    {
        return String.format( "LocalSession{localSessionId=%d, sequenceNumber=%d}", this.localSessionId, this.currentSequenceNumber );
    }
}
