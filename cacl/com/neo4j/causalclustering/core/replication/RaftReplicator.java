package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.consensus.LeaderListener;
import com.neo4j.causalclustering.core.consensus.LeaderLocator;
import com.neo4j.causalclustering.core.consensus.NoLeaderFoundException;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.replication.monitoring.ReplicationMonitor;
import com.neo4j.causalclustering.core.replication.session.LocalSessionPool;
import com.neo4j.causalclustering.core.replication.session.OperationContext;
import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Outbound;
import com.neo4j.dbms.database.ClusteredDatabaseContext;

import java.time.Duration;

import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.kernel.availability.UnavailableException;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;

public class RaftReplicator implements Replicator, LeaderListener
{
    private final NamedDatabaseId namedDatabaseId;
    private final MemberId me;
    private final Outbound<MemberId,RaftMessages.RaftMessage> outbound;
    private final ProgressTracker progressTracker;
    private final LocalSessionPool sessionPool;
    private final TimeoutStrategy progressTimeoutStrategy;
    private final Log log;
    private final DatabaseManager<ClusteredDatabaseContext> databaseManager;
    private final ReplicationMonitor replicationMonitor;
    private final long availabilityTimeoutMillis;
    private final LeaderProvider leaderProvider;

    public RaftReplicator( NamedDatabaseId namedDatabaseId, LeaderLocator leaderLocator, MemberId me, Outbound<MemberId,RaftMessages.RaftMessage> outbound,
            LocalSessionPool sessionPool, ProgressTracker progressTracker, TimeoutStrategy progressTimeoutStrategy, long availabilityTimeoutMillis,
            LogProvider logProvider, DatabaseManager<ClusteredDatabaseContext> databaseManager, Monitors monitors, Duration leaderAwaitDuration )
    {
        this.namedDatabaseId = namedDatabaseId;
        this.me = me;
        this.outbound = outbound;
        this.progressTracker = progressTracker;
        this.sessionPool = sessionPool;
        this.progressTimeoutStrategy = progressTimeoutStrategy;
        this.availabilityTimeoutMillis = availabilityTimeoutMillis;
        this.log = logProvider.getLog( this.getClass() );
        this.databaseManager = databaseManager;
        this.replicationMonitor = (ReplicationMonitor) monitors.newMonitor( ReplicationMonitor.class, new String[0] );
        this.leaderProvider = new LeaderProvider( leaderAwaitDuration );
        leaderLocator.registerListener( this );
    }

    public ReplicationResult replicate( ReplicatedContent command )
    {
        this.replicationMonitor.clientRequest();

        try
        {
            this.assertDatabaseAvailable();
        }
        catch ( UnavailableException var11 )
        {
            this.replicationMonitor.notReplicated();
            return ReplicationResult.notReplicated( var11 );
        }

        MemberId leader;
        try
        {
            leader = this.leaderProvider.awaitLeaderOrThrow();
        }
        catch ( InterruptedException var9 )
        {
            Thread.currentThread().interrupt();
            this.replicationMonitor.notReplicated();
            return ReplicationResult.notReplicated( var9 );
        }
        catch ( NoLeaderFoundException var10 )
        {
            this.replicationMonitor.notReplicated();
            return ReplicationResult.notReplicated( var10 );
        }

        OperationContext session = this.sessionPool.acquireSession();
        DistributedOperation operation = new DistributedOperation( command, session.globalSession(), session.localOperationId() );
        StateMachineResult stateMachineResult = null;
        Progress progress = this.progressTracker.start( operation );

        try
        {
            Timeout progressTimeout = this.progressTimeoutStrategy.newTimeout();
            ReplicationLogger logger = new ReplicationLogger( this.log );

            do
            {
                logger.newAttempt( operation, leader );
                if ( this.tryReplicate( leader, operation, progress, progressTimeout ) )
                {
                    this.sessionPool.releaseSession( session );
                    this.replicationMonitor.successfullyReplicated();
                    logger.success( operation );
                    progress.awaitResult();
                    stateMachineResult = progress.result();
                }
                else
                {
                    this.assertDatabaseAvailable();
                    leader = this.leaderProvider.awaitLeaderOrThrow();
                }
            }
            while ( stateMachineResult == null );
        }
        catch ( Throwable var12 )
        {
            if ( var12 instanceof InterruptedException )
            {
                Thread.currentThread().interrupt();
            }

            this.progressTracker.abort( operation );
            this.replicationMonitor.maybeReplicated();
            return ReplicationResult.maybeReplicated( var12 );
        }

        return ReplicationResult.applied( stateMachineResult );
    }

    private boolean tryReplicate( MemberId leader, DistributedOperation operation, Progress progress, Timeout replicationTimeout ) throws InterruptedException
    {
        this.replicationMonitor.replicationAttempt();
        this.outbound.send( leader, new RaftMessages.NewEntry.Request( this.me, operation ), true );
        progress.awaitReplication( replicationTimeout.getAndIncrement() );
        return progress.isReplicated();
    }

    public void onLeaderSwitch( LeaderInfo leaderInfo )
    {
        this.progressTracker.triggerReplicationEvent();
        MemberId newLeader = leaderInfo.memberId();
        MemberId oldLeader = this.leaderProvider.currentLeader();
        if ( newLeader == null && oldLeader != null )
        {
            this.log.info( "Lost previous leader '%s'. Currently no available leader", new Object[]{oldLeader} );
        }
        else if ( newLeader != null && oldLeader == null )
        {
            this.log.info( "A new leader has been detected: '%s'", new Object[]{newLeader} );
        }

        this.leaderProvider.setLeader( newLeader );
    }

    private void assertDatabaseAvailable() throws UnavailableException
    {
        Database database = (Database) this.databaseManager.getDatabaseContext( this.namedDatabaseId ).map( DatabaseContext::database ).orElseThrow(
                IllegalStateException::new );
        database.getDatabaseAvailabilityGuard().await( this.availabilityTimeoutMillis );
        if ( !database.getDatabaseHealth().isHealthy() )
        {
            throw new UnavailableException( "Database is not healthy." );
        }
    }
}
