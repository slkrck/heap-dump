package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.consensus.NoLeaderFoundException;
import com.neo4j.causalclustering.identity.MemberId;

import java.time.Duration;

class LeaderProvider
{
    private final long timeoutMillis;
    private volatile MemberId currentLeader;

    LeaderProvider( Duration leaderAwaitTimeout )
    {
        this.timeoutMillis = leaderAwaitTimeout.toMillis();
    }

    MemberId awaitLeaderOrThrow() throws InterruptedException, NoLeaderFoundException
    {
        MemberId leader = this.currentLeader;
        if ( leader != null )
        {
            return leader;
        }
        else
        {
            leader = this.awaitLeader();
            if ( leader == null )
            {
                throw new NoLeaderFoundException();
            }
            else
            {
                return leader;
            }
        }
    }

    private synchronized MemberId awaitLeader() throws InterruptedException
    {
        if ( this.currentLeader == null )
        {
            this.wait( this.timeoutMillis );
        }

        return this.currentLeader;
    }

    synchronized void setLeader( MemberId leader )
    {
        this.currentLeader = leader;
        if ( leader != null )
        {
            this.notifyAll();
        }
    }

    MemberId currentLeader()
    {
        return this.currentLeader;
    }
}
