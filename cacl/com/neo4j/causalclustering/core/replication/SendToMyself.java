package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Outbound;

public class SendToMyself
{
    private final MemberId myself;
    private final Outbound<MemberId,RaftMessages.RaftMessage> outbound;

    public SendToMyself( MemberId myself, Outbound<MemberId,RaftMessages.RaftMessage> outbound )
    {
        this.myself = myself;
        this.outbound = outbound;
    }

    public void replicate( ReplicatedContent content )
    {
        this.outbound.send( this.myself, new RaftMessages.NewEntry.Request( this.myself, content ) );
    }
}
