package com.neo4j.causalclustering.core.replication;

import com.neo4j.causalclustering.core.replication.session.GlobalSession;
import com.neo4j.causalclustering.core.replication.session.LocalOperationId;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ContentBuilder;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;

import java.io.IOException;
import java.util.Objects;
import java.util.OptionalLong;
import java.util.UUID;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class DistributedOperation implements ReplicatedContent
{
    private final ReplicatedContent content;
    private final GlobalSession globalSession;
    private final LocalOperationId operationId;

    public DistributedOperation( ReplicatedContent content, GlobalSession globalSession, LocalOperationId operationId )
    {
        this.content = content;
        this.globalSession = globalSession;
        this.operationId = operationId;
    }

    public static ContentBuilder<ReplicatedContent> deserialize( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        long mostSigBits = channel.getLong();
        long leastSigBits = channel.getLong();
        MemberId owner = (MemberId) (new MemberId.Marshal()).unmarshal( channel );
        GlobalSession globalSession = new GlobalSession( new UUID( mostSigBits, leastSigBits ), owner );
        long localSessionId = channel.getLong();
        long sequenceNumber = channel.getLong();
        LocalOperationId localOperationId = new LocalOperationId( localSessionId, sequenceNumber );
        return ContentBuilder.unfinished( ( subContent ) -> {
            return new DistributedOperation( subContent, globalSession, localOperationId );
        } );
    }

    public GlobalSession globalSession()
    {
        return this.globalSession;
    }

    public LocalOperationId operationId()
    {
        return this.operationId;
    }

    public ReplicatedContent content()
    {
        return this.content;
    }

    public OptionalLong size()
    {
        return this.content.size();
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
        this.content().dispatch( contentHandler );
    }

    public void marshalMetaData( WritableChannel channel ) throws IOException
    {
        channel.putLong( this.globalSession().sessionId().getMostSignificantBits() );
        channel.putLong( this.globalSession().sessionId().getLeastSignificantBits() );
        (new MemberId.Marshal()).marshal( this.globalSession().owner(), channel );
        channel.putLong( this.operationId.localSessionId() );
        channel.putLong( this.operationId.sequenceNumber() );
    }

    public String toString()
    {
        return "DistributedOperation{content=" + this.content + ", globalSession=" + this.globalSession + ", operationId=" + this.operationId + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            DistributedOperation that = (DistributedOperation) o;
            return Objects.equals( this.content, that.content ) && Objects.equals( this.globalSession, that.globalSession ) &&
                    Objects.equals( this.operationId, that.operationId );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.content, this.globalSession, this.operationId} );
    }
}
