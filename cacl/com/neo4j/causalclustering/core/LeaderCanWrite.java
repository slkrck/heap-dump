package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.core.consensus.roles.Role;
import org.neo4j.graphdb.WriteOperationsNotAllowedException;
import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.impl.factory.AccessCapability;

public class LeaderCanWrite implements AccessCapability
{
    public static final String NOT_LEADER_ERROR_MSG =
            "No write operations are allowed directly on this database. Writes must pass through the leader. The role of this server is: %s";
    private final RaftMachine raftMachine;

    public LeaderCanWrite( RaftMachine raftMachine )
    {
        this.raftMachine = raftMachine;
    }

    public void assertCanWrite()
    {
        Role currentRole = this.raftMachine.currentRole();
        if ( !currentRole.equals( Role.LEADER ) )
        {
            throw new WriteOperationsNotAllowedException( String.format(
                    "No write operations are allowed directly on this database. Writes must pass through the leader. The role of this server is: %s",
                    currentRole ), Cluster.NotALeader );
        }
    }
}
