package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.consensus.LeaderLocator;
import com.neo4j.causalclustering.core.consensus.NoLeaderFoundException;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class LeaderProvider
{
    private final LeaderLocator leaderLocator;
    private final TopologyService topologyService;

    public LeaderProvider( LeaderLocator leaderLocator, TopologyService topologyService )
    {
        this.leaderLocator = leaderLocator;
        this.topologyService = topologyService;
    }

    public MemberId getLeader( NamedDatabaseId databaseId ) throws NoLeaderFoundException
    {
        LeaderInfo leadMemberFromDiscovery = this.topologyService.getLeader( databaseId );
        LeaderInfo leadMember = null;

        try
        {
            leadMember = this.leaderLocator.getLeaderInfo();
            if ( leadMemberFromDiscovery != null && leadMemberFromDiscovery.term() > leadMember.term() )
            {
                leadMember = leadMemberFromDiscovery;
            }
        }
        catch ( NoLeaderFoundException var5 )
        {
            if ( leadMemberFromDiscovery == null || leadMemberFromDiscovery.memberId() == null )
            {
                throw new NoLeaderFoundException( "Neither Raft nor Discovery has leader", var5 );
            }

            leadMember = leadMemberFromDiscovery;
        }

        return leadMember.memberId();
    }
}
