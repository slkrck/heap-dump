package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCacheFactory;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collections;
import java.util.List;

import org.neo4j.configuration.Description;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.Internal;
import org.neo4j.configuration.SettingConstraints;
import org.neo4j.configuration.SettingImpl;
import org.neo4j.configuration.SettingValueParser;
import org.neo4j.configuration.SettingValueParsers;
import org.neo4j.configuration.SettingsDeclaration;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.graphdb.config.Setting;
import org.neo4j.io.ByteUnit;
import org.neo4j.logging.Level;

@Description( "Settings for Causal Clustering" )
public class CausalClusteringSettings implements SettingsDeclaration
{
    public static final String TEMP_STORE_COPY_DIRECTORY_NAME = "temp-copy";
    public static final String TEMP_BOOTSTRAP_DIRECTORY_NAME = "temp-bootstrap";
    public static final String TEMP_SAVE_DIRECTORY_NAME = "temp-save";
    @Description( "Time out for a new member to catch up" )
    public static final Setting<Duration> join_catch_up_timeout;
    @Description( "The time limit within which a new leader election will occur if no messages are received." )
    public static final Setting<Duration> leader_election_timeout;
    @Internal
    @Description( "Configures the time after which we give up trying to bind to a cluster formed of the other initial discovery members." )
    public static final Setting<Duration> cluster_binding_timeout;
    @Internal
    @Description( "Configures the time after which we retry binding to a cluster. Only applies to Akka discovery. A discovery type of DNS/SRV/K8S will be queried again on retry." )
    public static final Setting<Duration> cluster_binding_retry_timeout;
    @Description( "Prevents the current instance from volunteering to become Raft leader. Defaults to false, and should only be used in exceptional circumstances by expert users. Using this can result in reduced availability for the cluster." )
    public static final Setting<Boolean> refuse_to_be_leader;
    @Description( "Enable pre-voting extension to the Raft protocol (this is breaking and must match between the core cluster members)" )
    public static final Setting<Boolean> enable_pre_voting;
    @Description( "The maximum batch size when catching up (in unit of entries)" )
    public static final Setting<Integer> catchup_batch_size;
    @Description( "The maximum lag allowed before log shipping pauses (in unit of entries)" )
    public static final Setting<Integer> log_shipping_max_lag;
    @Internal
    @Description( "Maximum number of entries in the RAFT in-queue" )
    public static final Setting<Integer> raft_in_queue_size;
    @Description( "Maximum number of bytes in the RAFT in-queue" )
    public static final Setting<Long> raft_in_queue_max_bytes;
    @Internal
    @Description( "Largest batch processed by RAFT in number of entries" )
    public static final Setting<Integer> raft_in_queue_max_batch;
    @Description( "Largest batch processed by RAFT in bytes" )
    public static final Setting<Long> raft_in_queue_max_batch_bytes;
    @Description( "Minimum number of Core machines initially required to form a cluster. The cluster will form when at least this many Core members have discovered each other." )
    public static final Setting<Integer> minimum_core_cluster_size_at_formation;
    @Description( "The minimum size of the dynamically adjusted voting set (which only core members may be a part of). Adjustments to the voting set happen automatically as the availability of core members changes, due to explicit operations such as starting or stopping a member, or unintended issues such as network partitions. Note that this dynamic scaling of the voting set is generally desirable as under some circumstances it can increase the number of instance failures which may be tolerated. A majority of the voting set must be available before voting in or out members." )
    public static final Setting<Integer> minimum_core_cluster_size_at_runtime;
    public static final int DEFAULT_DISCOVERY_PORT = 5000;
    public static final int DEFAULT_TRANSACTION_PORT = 6000;
    public static final int DEFAULT_RAFT_PORT = 7000;
    @Description( "Network interface and port for the transaction shipping server to listen on. Please note that it is also possible to run the backup client against this port so always limit access to it via the firewall and configure an ssl policy." )
    public static final Setting<SocketAddress> transaction_listen_address;
    @Description( "Advertised hostname/IP address and port for the transaction shipping server." )
    public static final Setting<SocketAddress> transaction_advertised_address;
    @Description( "Network interface and port for the RAFT server to listen on." )
    public static final Setting<SocketAddress> raft_listen_address;
    @Description( "Advertised hostname/IP address and port for the RAFT server." )
    public static final Setting<SocketAddress> raft_advertised_address;
    @Description( "Host and port to bind the cluster member discovery management communication." )
    public static final Setting<SocketAddress> discovery_listen_address;
    @Description( "Advertised cluster member discovery management communication." )
    public static final Setting<SocketAddress> discovery_advertised_address;
    @Description( "A comma-separated list of other members of the cluster to join." )
    public static final Setting<List<SocketAddress>> initial_discovery_members;
    @Internal
    @Description( "Use native transport if available. Epoll for Linux or Kqueue for MacOS/BSD. If this setting is set to false, or if native transport is not available, Nio transport will be used." )
    public static final Setting<Boolean> use_native_transport;
    @Description( "Type of in-flight cache." )
    public static final Setting<InFlightCacheFactory.Type> in_flight_cache_type;
    @Description( "The maximum number of entries in the in-flight cache." )
    public static final Setting<Integer> in_flight_cache_max_entries;
    @Description( "The maximum number of bytes in the in-flight cache." )
    public static final Setting<Long> in_flight_cache_max_bytes;
    @Description( "Address for Kubernetes API" )
    public static final Setting<SocketAddress> kubernetes_address;
    @Description( "File location of token for Kubernetes API" )
    public static final Setting<Path> kubernetes_token;
    @Description( "File location of namespace for Kubernetes API" )
    public static final Setting<Path> kubernetes_namespace;
    @Description( "File location of CA certificate for Kubernetes API" )
    public static final Setting<Path> kubernetes_ca_crt;
    @Description( "LabelSelector for Kubernetes API" )
    public static final Setting<String> kubernetes_label_selector;
    @Description( "Service port name for discovery for Kubernetes API" )
    public static final Setting<String> kubernetes_service_port_name;
    @Internal
    @Description( "Configures the time taken attempting to publish a cluster id to the discovery service before potentially retrying." )
    public static final Setting<Duration> raft_id_publish_timeout;
    @Internal
    @Description( "The polling interval when attempting to resolve initial_discovery_members from DNS and SRV records." )
    public static final Setting<Duration> discovery_resolution_retry_interval;
    @Internal
    @Description( "Configures the time after which we give up trying to resolve a DNS/SRV record into a list of initial discovery members." )
    public static final Setting<Duration> discovery_resolution_timeout;
    @Description( "Configure the discovery type used for cluster name resolution" )
    public static final Setting<DiscoveryType> discovery_type;
    @Description( "The level of middleware logging" )
    public static final Setting<Level> middleware_logging_level;
    @Internal
    @Description( "External config file for Akka" )
    public static final Setting<Path> middleware_akka_external_config;
    @Internal
    @Description( "Parallelism level of default dispatcher used by Akka based cluster topology discovery, including cluster, replicator, and discovery actors" )
    public static final Setting<Integer> middleware_akka_default_parallelism_level;
    @Internal
    @Description( "Parallelism level of dispatcher used for communication from Akka based cluster topology discovery " )
    public static final Setting<Integer> middleware_akka_sink_parallelism_level;
    @Internal
    @Description( "Timeout for Akka socket binding" )
    public static final Setting<Duration> akka_bind_timeout;
    @Internal
    @Description( "Timeout for Akka connection" )
    public static final Setting<Duration> akka_connection_timeout;
    @Internal
    @Description( "Timeout for Akka handshake" )
    public static final Setting<Duration> akka_handshake_timeout;
    @Internal
    @Description( "Akka cluster phi accrual failure detector. How often keep-alive heartbeat messages should be sent to each connection." )
    public static final Setting<Duration> akka_failure_detector_heartbeat_interval;
    @Internal
    @Description( "Akka cluster phi accrual failure detector. Defines the failure detector threshold. A low threshold is prone to generate many wrong suspicions but ensures a quick detection in the event of a real crash. Conversely, a high threshold generates fewer mistakes but needs more time to detect actual crashes." )
    public static final Setting<Double> akka_failure_detector_threshold;
    @Internal
    @Description( "Akka cluster phi accrual failure detector. Number of the samples of inter-heartbeat arrival times to adaptively calculate the failure timeout for connections." )
    public static final Setting<Integer> akka_failure_detector_max_sample_size;
    @Internal
    @Description( "Akka cluster phi accrual failure detector. Minimum standard deviation to use for the normal distribution in AccrualFailureDetector. Too low standard deviation might result in too much sensitivity for sudden, but normal, deviations in heartbeat inter arrival times." )
    public static final Setting<Duration> akka_failure_detector_min_std_deviation;
    @Internal
    @Description( "Akka cluster phi accrual failure detector. Number of potentially lost/delayed heartbeats that will be accepted before considering it to be an anomaly. This margin is important to be able to survive sudden, occasional, pauses in heartbeat arrivals, due to for example garbage collect or network drop." )
    public static final Setting<Duration> akka_failure_detector_acceptable_heartbeat_pause;
    @Internal
    @Description( "Akka cluster phi accrual failure detector. Number of member nodes that each member will send heartbeat messages to, i.e. each node will be monitored by this number of other nodes." )
    public static final Setting<Integer> akka_failure_detector_monitored_by_nr_of_members;
    @Internal
    @Description( "Akka cluster phi accrual failure detector. After the heartbeat request has been sent the first failure detection will start after this period, even though no heartbeat message has been received." )
    public static final Setting<Duration> akka_failure_detector_expected_response_after;
    @Description( "The maximum file size before the storage file is rotated (in unit of entries)" )
    public static final Setting<Integer> last_flushed_state_size;
    @Description( "The maximum file size before the membership state file is rotated (in unit of entries)" )
    public static final Setting<Integer> raft_membership_state_size;
    @Description( "The maximum file size before the vote state file is rotated (in unit of entries)" )
    public static final Setting<Integer> vote_state_size;
    @Description( "The maximum file size before the term state file is rotated (in unit of entries)" )
    public static final Setting<Integer> term_state_size;
    @Description( "The maximum file size before the global session tracker state file is rotated (in unit of entries)" )
    public static final Setting<Integer> global_session_tracker_state_size;
    @Description( "The maximum file size before the replicated lease state file is rotated (in unit of entries)" )
    public static final Setting<Integer> replicated_lease_state_size;
    @Description( "The initial timeout until replication is retried. The timeout will increase exponentially." )
    public static final Setting<Duration> replication_retry_timeout_base;
    @Description( "The upper limit for the exponentially incremented retry timeout." )
    public static final Setting<Duration> replication_retry_timeout_limit;
    @Description( "The duration for which the replicator will await a new leader." )
    public static final Setting<Duration> replication_leader_await_timeout;
    @Description( "The number of operations to be processed before the state machines flush to disk" )
    public static final Setting<Integer> state_machine_flush_window_size;
    @Description( "The maximum number of operations to be batched during applications of operations in the state machines" )
    public static final Setting<Integer> state_machine_apply_max_batch_size;
    @Description( "RAFT log pruning strategy" )
    public static final Setting<String> raft_log_pruning_strategy;
    @Description( "RAFT log implementation" )
    public static final Setting<String> raft_log_implementation;
    @Description( "RAFT log rotation size" )
    public static final Setting<Long> raft_log_rotation_size;
    @Description( "RAFT log reader pool size" )
    public static final Setting<Integer> raft_log_reader_pool_size;
    @Description( "RAFT log pruning frequency" )
    public static final Setting<Duration> raft_log_pruning_frequency;
    @Description( "Enable or disable the dump of all network messages pertaining to the RAFT protocol" )
    @Internal
    public static final Setting<Boolean> raft_messages_log_enable;
    @Description( "Path to RAFT messages log." )
    @Internal
    public static final Setting<Path> raft_messages_log_path;
    @Description( "Interval of pulling updates from cores." )
    public static final Setting<Duration> pull_interval;
    @Description( "The catch up protocol times out if the given duration elapses with no network activity. Every message received by the client from the server extends the time out duration." )
    public static final Setting<Duration> catch_up_client_inactivity_timeout;
    @Description( "Maximum retry time per request during store copy. Regular store files and indexes are downloaded in separate requests during store copy. This configures the maximum time failed requests are allowed to resend. " )
    public static final Setting<Duration> store_copy_max_retry_time_per_request;
    @Description( "Maximum backoff timeout for store copy requests" )
    @Internal
    public static final Setting<Duration> store_copy_backoff_max_wait;
    @Description( "Store copy chunk size" )
    public static final Setting<Integer> store_copy_chunk_size;
    @Description( "Throttle limit for logging unknown cluster member address" )
    public static final Setting<Duration> unknown_address_logging_throttle;
    @Description( "Maximum transaction batch size for read replicas when applying transactions pulled from core servers." )
    @Internal
    public static final Setting<Integer> read_replica_transaction_applier_batch_size;
    @Description( "Configure if the `dbms.routing.getRoutingTable()` procedure should include followers as read endpoints or return only read replicas. Note: if there are no read replicas in the cluster, followers are returned as read end points regardless the value of this setting. Defaults to true so that followers are available for read-only queries in a typical heterogeneous setup." )
    public static final Setting<Boolean> cluster_allow_reads_on_followers;
    @Description( "Time between scanning the cluster to refresh current server's view of topology" )
    public static final Setting<Duration> cluster_topology_refresh;
    @Description( "An ordered list in descending preference of the strategy which read replicas use to choose the upstream server from which to pull transactional updates." )
    public static final Setting<List<String>> upstream_selection_strategy;
    @Description( "Configuration of a user-defined upstream selection strategy. The user-defined strategy is used if the list of strategies (`causal_clustering.upstream_selection_strategy`) includes the value `user_defined`. " )
    public static final Setting<String> user_defined_upstream_selection_strategy;
    @Description( "Comma separated list of groups to be used by the connect-randomly-to-server-group selection strategy. The connect-randomly-to-server-group strategy is used if the list of strategies (`causal_clustering.upstream_selection_strategy`) includes the value `connect-randomly-to-server-group`. " )
    public static final Setting<List<String>> connect_randomly_to_server_group_strategy;
    @Description( "A list of group names for the server used when configuring load balancing and replication policies." )
    public static final Setting<List<String>> server_groups;
    @Description( "The load balancing plugin to use." )
    public static final Setting<String> load_balancing_plugin;
    @Description( "Time out for protocol negotiation handshake" )
    public static final Setting<Duration> handshake_timeout;
    @Description( "Enables shuffling of the returned load balancing result." )
    public static final Setting<Boolean> load_balancing_shuffle;
    @Description( "Require authorization for access to the Causal Clustering status endpoints." )
    public static final Setting<Boolean> status_auth_enabled;
    @Description( "Sampling window for throughput estimate reported in the status endpoint." )
    public static final Setting<Duration> status_throughput_window;
    @Description( "Enable multi-data center features. Requires appropriate licensing." )
    public static final Setting<Boolean> multi_dc_license;
    @Description( "Raft protocol implementation versions that this instance will allow in negotiation as a comma-separated list. Order is not relevant: the greatest value will be preferred. An empty list will allow all supported versions. Example value: \"1.0, 1.3, 2.0, 2.1\"" )
    public static final Setting<List<ApplicationProtocolVersion>> raft_implementations;
    @Description( "Catchup protocol implementation versions that this instance will allow in negotiation as a comma-separated list. Order is not relevant: the greatest value will be preferred. An empty list will allow all supported versions. Example value: \"1.1, 1.2, 2.1, 2.2\"" )
    public static final Setting<List<ApplicationProtocolVersion>> catchup_implementations;
    @Description( "Network compression algorithms that this instance will allow in negotiation as a comma-separated list. Listed in descending order of preference for incoming connections. An empty list implies no compression. For outgoing connections this merely specifies the allowed set of algorithms and the preference of the  remote peer will be used for making the decision. Allowable values: [Gzip, Snappy, Snappy_validating, LZ4, LZ4_high_compression, LZ_validating, LZ4_high_compression_validating]" )
    public static final Setting<List<String>> compression_implementations;
    @Internal
    public static final Setting<Boolean> inbound_connection_initialization_logging_enabled;
    private static final SettingValueParser<ApplicationProtocolVersion> APP_PROTOCOL_VER;

    static
    {
        join_catch_up_timeout =
                SettingImpl.newBuilder( "causal_clustering.join_catch_up_timeout", SettingValueParsers.DURATION, Duration.ofMinutes( 10L ) ).build();
        leader_election_timeout =
                SettingImpl.newBuilder( "causal_clustering.leader_election_timeout", SettingValueParsers.DURATION, Duration.ofSeconds( 7L ) ).build();
        cluster_binding_timeout =
                SettingImpl.newBuilder( "causal_clustering.cluster_binding_timeout", SettingValueParsers.DURATION, Duration.ofMinutes( 5L ) ).build();
        cluster_binding_retry_timeout =
                SettingImpl.newBuilder( "causal_clustering.cluster_binding_retry_timeout", SettingValueParsers.DURATION, Duration.ofMinutes( 1L ) ).build();
        refuse_to_be_leader = SettingImpl.newBuilder( "causal_clustering.refuse_to_be_leader", SettingValueParsers.BOOL, false ).build();
        enable_pre_voting = SettingImpl.newBuilder( "causal_clustering.enable_pre_voting", SettingValueParsers.BOOL, true ).build();
        catchup_batch_size = SettingImpl.newBuilder( "causal_clustering.catchup_batch_size", SettingValueParsers.INT, 64 ).build();
        log_shipping_max_lag = SettingImpl.newBuilder( "causal_clustering.log_shipping_max_lag", SettingValueParsers.INT, 256 ).build();
        raft_in_queue_size = SettingImpl.newBuilder( "causal_clustering.raft_in_queue_size", SettingValueParsers.INT, 1024 ).build();
        raft_in_queue_max_bytes =
                SettingImpl.newBuilder( "causal_clustering.raft_in_queue_max_bytes", SettingValueParsers.BYTES, ByteUnit.gibiBytes( 2L ) ).build();
        raft_in_queue_max_batch = SettingImpl.newBuilder( "causal_clustering.raft_in_queue_max_batch", SettingValueParsers.INT, 128 ).build();
        raft_in_queue_max_batch_bytes =
                SettingImpl.newBuilder( "causal_clustering.raft_in_queue_max_batch_bytes", SettingValueParsers.BYTES, ByteUnit.mebiBytes( 8L ) ).build();
        minimum_core_cluster_size_at_formation =
                SettingImpl.newBuilder( "causal_clustering.minimum_core_cluster_size_at_formation", SettingValueParsers.INT, 3 ).addConstraint(
                        SettingConstraints.min( 2 ) ).build();
        minimum_core_cluster_size_at_runtime =
                SettingImpl.newBuilder( "causal_clustering.minimum_core_cluster_size_at_runtime", SettingValueParsers.INT, 3 ).addConstraint(
                        SettingConstraints.min( 2 ) ).build();
        transaction_listen_address = SettingImpl.newBuilder( "causal_clustering.transaction_listen_address", SettingValueParsers.SOCKET_ADDRESS,
                new SocketAddress( 6000 ) ).setDependency( GraphDatabaseSettings.default_listen_address ).build();
        transaction_advertised_address = SettingImpl.newBuilder( "causal_clustering.transaction_advertised_address", SettingValueParsers.SOCKET_ADDRESS,
                new SocketAddress( 6000 ) ).setDependency( GraphDatabaseSettings.default_advertised_address ).build();
        raft_listen_address =
                SettingImpl.newBuilder( "causal_clustering.raft_listen_address", SettingValueParsers.SOCKET_ADDRESS, new SocketAddress( 7000 ) ).setDependency(
                        GraphDatabaseSettings.default_listen_address ).build();
        raft_advertised_address = SettingImpl.newBuilder( "causal_clustering.raft_advertised_address", SettingValueParsers.SOCKET_ADDRESS,
                new SocketAddress( 7000 ) ).setDependency( GraphDatabaseSettings.default_advertised_address ).build();
        discovery_listen_address = SettingImpl.newBuilder( "causal_clustering.discovery_listen_address", SettingValueParsers.SOCKET_ADDRESS,
                new SocketAddress( 5000 ) ).setDependency( GraphDatabaseSettings.default_listen_address ).build();
        discovery_advertised_address = SettingImpl.newBuilder( "causal_clustering.discovery_advertised_address", SettingValueParsers.SOCKET_ADDRESS,
                new SocketAddress( 5000 ) ).setDependency( GraphDatabaseSettings.default_advertised_address ).build();
        initial_discovery_members =
                SettingImpl.newBuilder( "causal_clustering.initial_discovery_members", SettingValueParsers.listOf( SettingValueParsers.SOCKET_ADDRESS ),
                        (Object) null ).build();
        use_native_transport = SettingImpl.newBuilder( "causal_clustering.use_native_transport", SettingValueParsers.BOOL, true ).build();
        in_flight_cache_type = SettingImpl.newBuilder( "causal_clustering.in_flight_cache.type", SettingValueParsers.ofEnum( InFlightCacheFactory.Type.class ),
                InFlightCacheFactory.Type.CONSECUTIVE ).build();
        in_flight_cache_max_entries = SettingImpl.newBuilder( "causal_clustering.in_flight_cache.max_entries", SettingValueParsers.INT, 1024 ).build();
        in_flight_cache_max_bytes =
                SettingImpl.newBuilder( "causal_clustering.in_flight_cache.max_bytes", SettingValueParsers.BYTES, ByteUnit.gibiBytes( 2L ) ).build();
        kubernetes_address = SettingImpl.newBuilder( "causal_clustering.kubernetes.address", SettingValueParsers.SOCKET_ADDRESS,
                new SocketAddress( "kubernetes.default.svc", 443 ) ).build();
        kubernetes_token = pathUnixAbsolute( "causal_clustering.kubernetes.token", "/var/run/secrets/kubernetes.io/serviceaccount/token" );
        kubernetes_namespace = pathUnixAbsolute( "causal_clustering.kubernetes.namespace", "/var/run/secrets/kubernetes.io/serviceaccount/namespace" );
        kubernetes_ca_crt = pathUnixAbsolute( "causal_clustering.kubernetes.ca_crt", "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt" );
        kubernetes_label_selector = SettingImpl.newBuilder( "causal_clustering.kubernetes.label_selector", SettingValueParsers.STRING, (Object) null ).build();
        kubernetes_service_port_name =
                SettingImpl.newBuilder( "causal_clustering.kubernetes.service_port_name", SettingValueParsers.STRING, (Object) null ).build();
        raft_id_publish_timeout =
                SettingImpl.newBuilder( "causal_clustering.cluster_id_publish_timeout", SettingValueParsers.DURATION, Duration.ofSeconds( 15L ) ).build();
        discovery_resolution_retry_interval = SettingImpl.newBuilder( "causal_clustering.discovery_resolution_retry_interval", SettingValueParsers.DURATION,
                Duration.ofSeconds( 5L ) ).build();
        discovery_resolution_timeout =
                SettingImpl.newBuilder( "causal_clustering.discovery_resolution_timeout", SettingValueParsers.DURATION, Duration.ofMinutes( 5L ) ).build();
        discovery_type =
                SettingImpl.newBuilder( "causal_clustering.discovery_type", SettingValueParsers.ofEnum( DiscoveryType.class ), DiscoveryType.LIST ).build();
        middleware_logging_level =
                SettingImpl.newBuilder( "causal_clustering.middleware.logging.level", SettingValueParsers.ofEnum( Level.class ), Level.WARN ).build();
        middleware_akka_external_config =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.external_config", SettingValueParsers.PATH, (Object) null ).build();
        middleware_akka_default_parallelism_level =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.default-parallelism", SettingValueParsers.INT, 4 ).build();
        middleware_akka_sink_parallelism_level =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.sink-parallelism", SettingValueParsers.INT, 2 ).build();
        akka_bind_timeout =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.bind-timeout", SettingValueParsers.DURATION, Duration.ofSeconds( 10L ) ).build();
        akka_connection_timeout = SettingImpl.newBuilder( "causal_clustering.middleware.akka.connection-timeout", SettingValueParsers.DURATION,
                Duration.ofSeconds( 10L ) ).build();
        akka_handshake_timeout = SettingImpl.newBuilder( "causal_clustering.middleware.akka.handshake-timeout", SettingValueParsers.DURATION,
                Duration.ofSeconds( 30L ) ).build();
        akka_failure_detector_heartbeat_interval =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.failure_detector.heartbeat_interval", SettingValueParsers.DURATION,
                        Duration.ofSeconds( 1L ) ).build();
        akka_failure_detector_threshold =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.failure_detector.threshold", SettingValueParsers.DOUBLE, 10.0D ).build();
        akka_failure_detector_max_sample_size =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.failure_detector.max_sample_size", SettingValueParsers.INT, 1000 ).build();
        akka_failure_detector_min_std_deviation =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.failure_detector.min_std_deviation", SettingValueParsers.DURATION,
                        Duration.ofMillis( 100L ) ).build();
        akka_failure_detector_acceptable_heartbeat_pause =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.failure_detector.acceptable_heartbeat_pause", SettingValueParsers.DURATION,
                        Duration.ofSeconds( 4L ) ).build();
        akka_failure_detector_monitored_by_nr_of_members =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.failure_detector.monitored_by_nr_of_members", SettingValueParsers.INT, 5 ).build();
        akka_failure_detector_expected_response_after =
                SettingImpl.newBuilder( "causal_clustering.middleware.akka.failure_detector.expected_response_after", SettingValueParsers.DURATION,
                        Duration.ofSeconds( 1L ) ).build();
        last_flushed_state_size = SettingImpl.newBuilder( "causal_clustering.last_applied_state_size", SettingValueParsers.INT, 1000 ).build();
        raft_membership_state_size = SettingImpl.newBuilder( "causal_clustering.raft_membership_state_size", SettingValueParsers.INT, 1000 ).build();
        vote_state_size = SettingImpl.newBuilder( "causal_clustering.raft_vote_state_size", SettingValueParsers.INT, 1000 ).build();
        term_state_size = SettingImpl.newBuilder( "causal_clustering.raft_term_state_size", SettingValueParsers.INT, 1000 ).build();
        global_session_tracker_state_size =
                SettingImpl.newBuilder( "causal_clustering.global_session_tracker_state_size", SettingValueParsers.INT, 1000 ).build();
        replicated_lease_state_size = SettingImpl.newBuilder( "causal_clustering.replicated_lease_state_size", SettingValueParsers.INT, 1000 ).build();
        replication_retry_timeout_base =
                SettingImpl.newBuilder( "causal_clustering.replication_retry_timeout_base", SettingValueParsers.DURATION, Duration.ofSeconds( 10L ) ).build();
        replication_retry_timeout_limit =
                SettingImpl.newBuilder( "causal_clustering.replication_retry_timeout_limit", SettingValueParsers.DURATION, Duration.ofSeconds( 60L ) ).build();
        replication_leader_await_timeout =
                SettingImpl.newBuilder( "causal_clustering.replication_leader_await_timeout", SettingValueParsers.DURATION, Duration.ofSeconds( 10L ) ).build();
        state_machine_flush_window_size = SettingImpl.newBuilder( "causal_clustering.state_machine_flush_window_size", SettingValueParsers.INT, 4096 ).build();
        state_machine_apply_max_batch_size =
                SettingImpl.newBuilder( "causal_clustering.state_machine_apply_max_batch_size", SettingValueParsers.INT, 16 ).build();
        raft_log_pruning_strategy = SettingImpl.newBuilder( "causal_clustering.raft_log_prune_strategy", SettingValueParsers.STRING, "1g size" ).build();
        raft_log_implementation = SettingImpl.newBuilder( "causal_clustering.raft_log_implementation", SettingValueParsers.STRING, "SEGMENTED" ).build();
        raft_log_rotation_size =
                SettingImpl.newBuilder( "causal_clustering.raft_log_rotation_size", SettingValueParsers.BYTES, ByteUnit.mebiBytes( 250L ) ).addConstraint(
                        SettingConstraints.min( 1024L ) ).build();
        raft_log_reader_pool_size = SettingImpl.newBuilder( "causal_clustering.raft_log_reader_pool_size", SettingValueParsers.INT, 8 ).build();
        raft_log_pruning_frequency =
                SettingImpl.newBuilder( "causal_clustering.raft_log_pruning_frequency", SettingValueParsers.DURATION, Duration.ofMinutes( 10L ) ).build();
        raft_messages_log_enable = SettingImpl.newBuilder( "causal_clustering.raft_messages_log_enable", SettingValueParsers.BOOL, false ).build();
        raft_messages_log_path =
                SettingImpl.newBuilder( "causal_clustering.raft_messages_log_path", SettingValueParsers.PATH, Path.of( "raft-messages.log" ) ).setDependency(
                        GraphDatabaseSettings.logs_directory ).immutable().build();
        pull_interval = SettingImpl.newBuilder( "causal_clustering.pull_interval", SettingValueParsers.DURATION, Duration.ofSeconds( 1L ) ).build();
        catch_up_client_inactivity_timeout = SettingImpl.newBuilder( "causal_clustering.catch_up_client_inactivity_timeout", SettingValueParsers.DURATION,
                Duration.ofMinutes( 10L ) ).build();
        store_copy_max_retry_time_per_request = SettingImpl.newBuilder( "causal_clustering.store_copy_max_retry_time_per_request", SettingValueParsers.DURATION,
                Duration.ofMinutes( 20L ) ).build();
        store_copy_backoff_max_wait =
                SettingImpl.newBuilder( "causal_clustering.store_copy_backoff_max_wait", SettingValueParsers.DURATION, Duration.ofSeconds( 5L ) ).build();
        store_copy_chunk_size =
                SettingImpl.newBuilder( "causal_clustering.store_copy_chunk_size", SettingValueParsers.INT, (int) ByteUnit.kibiBytes( 32L ) ).addConstraint(
                        SettingConstraints.range( (int) ByteUnit.kibiBytes( 4L ), (int) ByteUnit.mebiBytes( 1L ) ) ).build();
        unknown_address_logging_throttle = SettingImpl.newBuilder( "causal_clustering.unknown_address_logging_throttle", SettingValueParsers.DURATION,
                Duration.ofMillis( 10000L ) ).build();
        read_replica_transaction_applier_batch_size =
                SettingImpl.newBuilder( "causal_clustering.read_replica_transaction_applier_batch_size", SettingValueParsers.INT, 64 ).build();
        cluster_allow_reads_on_followers =
                SettingImpl.newBuilder( "causal_clustering.cluster_allow_reads_on_followers", SettingValueParsers.BOOL, true ).build();
        cluster_topology_refresh =
                SettingImpl.newBuilder( "causal_clustering.cluster_topology_refresh", SettingValueParsers.DURATION, Duration.ofSeconds( 5L ) ).addConstraint(
                        SettingConstraints.min( Duration.ofSeconds( 1L ) ) ).build();
        upstream_selection_strategy =
                SettingImpl.newBuilder( "causal_clustering.upstream_selection_strategy", SettingValueParsers.listOf( SettingValueParsers.STRING ),
                        List.of( "default" ) ).build();
        user_defined_upstream_selection_strategy =
                SettingImpl.newBuilder( "causal_clustering.user_defined_upstream_strategy", SettingValueParsers.STRING, "" ).build();
        connect_randomly_to_server_group_strategy =
                SettingImpl.newBuilder( "causal_clustering.connect-randomly-to-server-group", SettingValueParsers.listOf( SettingValueParsers.STRING ),
                        Collections.emptyList() ).build();
        server_groups = SettingImpl.newBuilder( "causal_clustering.server_groups", SettingValueParsers.listOf( SettingValueParsers.STRING ),
                Collections.emptyList() ).build();
        load_balancing_plugin = SettingImpl.newBuilder( "causal_clustering.load_balancing.plugin", SettingValueParsers.STRING, "server_policies" ).build();
        handshake_timeout = SettingImpl.newBuilder( "causal_clustering.handshake_timeout", SettingValueParsers.DURATION, Duration.ofSeconds( 20L ) ).build();
        load_balancing_shuffle = SettingImpl.newBuilder( "causal_clustering.load_balancing.shuffle", SettingValueParsers.BOOL, true ).build();
        status_auth_enabled = SettingImpl.newBuilder( "dbms.security.causal_clustering_status_auth_enabled", SettingValueParsers.BOOL, true ).build();
        status_throughput_window =
                SettingImpl.newBuilder( "causal_clustering.status_throughput_window", SettingValueParsers.DURATION, Duration.ofSeconds( 5L ) ).addConstraint(
                        SettingConstraints.range( Duration.ofSeconds( 1L ), Duration.ofMinutes( 5L ) ) ).build();
        multi_dc_license = SettingImpl.newBuilder( "causal_clustering.multi_dc_license", SettingValueParsers.BOOL, false ).build();
        APP_PROTOCOL_VER = new SettingValueParser<ApplicationProtocolVersion>()
        {
            public ApplicationProtocolVersion parse( String value )
            {
                return ApplicationProtocolVersion.parse( value );
            }

            public String getDescription()
            {
                return "an application protocol version";
            }

            public Class<ApplicationProtocolVersion> getType()
            {
                return ApplicationProtocolVersion.class;
            }
        };
        raft_implementations = SettingImpl.newBuilder( "causal_clustering.protocol_implementations.raft", SettingValueParsers.listOf( APP_PROTOCOL_VER ),
                Collections.emptyList() ).build();
        catchup_implementations = SettingImpl.newBuilder( "causal_clustering.protocol_implementations.catchup", SettingValueParsers.listOf( APP_PROTOCOL_VER ),
                Collections.emptyList() ).build();
        compression_implementations =
                SettingImpl.newBuilder( "causal_clustering.protocol_implementations.compression", SettingValueParsers.listOf( SettingValueParsers.STRING ),
                        Collections.emptyList() ).build();
        inbound_connection_initialization_logging_enabled =
                SettingImpl.newBuilder( "unsupported.causal_clustering.inbound_connection_initialization_logging_enabled", SettingValueParsers.BOOL,
                        true ).dynamic().build();
    }

    private static Setting<Path> pathUnixAbsolute( String name, String path )
    {
        File[] roots = File.listRoots();
        Path root = roots.length > 0 ? roots[0].toPath() : Paths.get( "//" );
        return SettingImpl.newBuilder( name, SettingValueParsers.PATH, root.resolve( path ) ).build();
    }
}
