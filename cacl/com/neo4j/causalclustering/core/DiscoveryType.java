package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.discovery.DnsHostnameResolver;
import com.neo4j.causalclustering.discovery.DomainNameResolverImpl;
import com.neo4j.causalclustering.discovery.KubernetesResolver;
import com.neo4j.causalclustering.discovery.NoOpHostnameResolver;
import com.neo4j.causalclustering.discovery.RemoteMembersResolver;
import com.neo4j.causalclustering.discovery.SrvHostnameResolver;
import com.neo4j.causalclustering.discovery.SrvRecordResolverImpl;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiFunction;

import org.neo4j.configuration.Config;
import org.neo4j.graphdb.config.Setting;
import org.neo4j.logging.internal.LogService;

public enum DiscoveryType
{
    DNS( ( logService, conf ) -> {
        return DnsHostnameResolver.resolver( logService, new DomainNameResolverImpl(), conf );
    }, new Setting[]{CausalClusteringSettings.initial_discovery_members} ),
    LIST( ( logService, conf ) -> {
        return NoOpHostnameResolver.resolver( conf );
    }, new Setting[]{CausalClusteringSettings.initial_discovery_members} ),
    SRV( ( logService, conf ) -> {
        return SrvHostnameResolver.resolver( logService, new SrvRecordResolverImpl(), conf );
    }, new Setting[]{CausalClusteringSettings.initial_discovery_members} ),
    K8S( KubernetesResolver::resolver,
            new Setting[]{CausalClusteringSettings.kubernetes_label_selector, CausalClusteringSettings.kubernetes_service_port_name} );

    private final BiFunction<LogService,Config,RemoteMembersResolver> resolverSupplier;
    private final Collection<Setting<?>> requiredSettings;

    private DiscoveryType( BiFunction<LogService,Config,RemoteMembersResolver> resolverSupplier, Setting<?>... requiredSettings )
    {
        this.resolverSupplier = resolverSupplier;
        this.requiredSettings = Arrays.asList( requiredSettings );
    }

    public RemoteMembersResolver getHostnameResolver( LogService logService, Config config )
    {
        return (RemoteMembersResolver) this.resolverSupplier.apply( logService, config );
    }

    public Collection<Setting<?>> requiredSettings()
    {
        return this.requiredSettings;
    }
}
