package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.common.ClusteredDatabase;
import com.neo4j.causalclustering.common.DatabaseTopologyNotifier;
import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.core.state.CommandApplicationProcess;
import com.neo4j.causalclustering.core.state.snapshot.CoreDownloaderService;
import com.neo4j.causalclustering.messaging.LifecycleMessageHandler;

import java.util.Objects;

import org.neo4j.function.ThrowingAction;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.kernel.recovery.RecoveryFacade;

class CoreDatabase extends ClusteredDatabase
{
    CoreDatabase( RaftMachine raftMachine, Database kernelDatabase, CommandApplicationProcess commandApplicationProcess,
            LifecycleMessageHandler<?> raftMessageHandler, CoreDownloaderService downloadService, RecoveryFacade recoveryFacade, Lifecycle clusterComponents,
            CorePanicHandlers panicHandler, CoreBootstrap bootstrap, DatabaseTopologyNotifier topologyNotifier )
    {
        this.addComponent( panicHandler );
        this.addComponent( clusterComponents );
        this.addComponent( LifecycleAdapter.onStart( () -> {
            recoveryFacade.recovery( kernelDatabase.getDatabaseLayout() );
        } ) );
        this.addComponent( topologyNotifier );
        Objects.requireNonNull( bootstrap );
        this.addComponent( LifecycleAdapter.onStart( bootstrap::perform ) );
        this.addComponent( kernelDatabase );
        Objects.requireNonNull( commandApplicationProcess );
        ThrowingAction var10001 = commandApplicationProcess::start;
        Objects.requireNonNull( commandApplicationProcess );
        this.addComponent( LifecycleAdapter.simpleLife( var10001, commandApplicationProcess::stop ) );
        Objects.requireNonNull( raftMachine );
        this.addComponent( LifecycleAdapter.onStart( raftMachine::postRecoveryActions ) );
        Objects.requireNonNull( raftMessageHandler );
        this.addComponent( LifecycleAdapter.onStop( raftMessageHandler::stop ) );
        Objects.requireNonNull( raftMachine );
        this.addComponent( LifecycleAdapter.onStop( raftMachine::stopTimers ) );
        this.addComponent( downloadService );
    }
}
