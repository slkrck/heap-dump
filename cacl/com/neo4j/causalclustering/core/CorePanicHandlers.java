package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.common.DatabasePanicHandlers;
import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.core.state.CommandApplicationProcess;
import com.neo4j.causalclustering.error_handling.DatabasePanicEventHandler;
import com.neo4j.causalclustering.error_handling.PanicService;
import com.neo4j.dbms.ClusterInternalDbmsOperator;

import java.util.List;

import org.neo4j.kernel.database.Database;

class CorePanicHandlers extends DatabasePanicHandlers
{
    CorePanicHandlers( RaftMachine raftMachine, Database kernelDatabase, CommandApplicationProcess applicationProcess,
            ClusterInternalDbmsOperator clusterInternalOperator, PanicService panicService )
    {
        super( panicService, kernelDatabase.getNamedDatabaseId(),
                List.of( DatabasePanicEventHandler.raiseAvailabilityGuard( kernelDatabase ), DatabasePanicEventHandler.markUnhealthy( kernelDatabase ),
                        applicationProcess, raftMachine, DatabasePanicEventHandler.stopDatabase( kernelDatabase, clusterInternalOperator ) ) );
    }
}
