package com.neo4j.causalclustering.core;

import java.io.File;
import java.io.IOException;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;

public class TempBootstrapDir
{
    protected final FileSystemAbstraction fs;
    protected final File tempBootstrapDir;

    public TempBootstrapDir( FileSystemAbstraction fs, DatabaseLayout layout )
    {
        this.fs = fs;
        this.tempBootstrapDir = new File( layout.databaseDirectory(), "temp-bootstrap" );
    }

    public static TempBootstrapDir.Resource cleanBeforeAndAfter( FileSystemAbstraction fs, DatabaseLayout layout ) throws IOException
    {
        return new TempBootstrapDir.Resource( fs, layout );
    }

    public File get()
    {
        return this.tempBootstrapDir;
    }

    public void delete() throws IOException
    {
        this.fs.deleteRecursively( this.tempBootstrapDir );
    }

    public static class Resource extends TempBootstrapDir implements AutoCloseable
    {
        private Resource( FileSystemAbstraction fs, DatabaseLayout layout ) throws IOException
        {
            super( fs, layout );
            fs.deleteRecursively( this.get() );
        }

        public void close() throws IOException
        {
            this.fs.deleteRecursively( this.get() );
        }
    }
}
