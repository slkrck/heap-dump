package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.ComposableMessageHandler;
import com.neo4j.causalclustering.messaging.LifecycleMessageHandler;

import java.util.Objects;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClusterBindingHandler implements LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>
{
    private final LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler;
    private final RaftMessageDispatcher raftMessageDispatcher;
    private final Log log;
    private volatile RaftId boundRaftId;

    public ClusterBindingHandler( RaftMessageDispatcher raftMessageDispatcher,
            LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler, LogProvider logProvider )
    {
        this.delegateHandler = delegateHandler;
        this.raftMessageDispatcher = raftMessageDispatcher;
        this.log = logProvider.getLog( this.getClass() );
    }

    public static ComposableMessageHandler composable( RaftMessageDispatcher raftMessageDispatcher, LogProvider logProvider )
    {
        return ( delegate ) -> {
            return new ClusterBindingHandler( raftMessageDispatcher, delegate, logProvider );
        };
    }

    public void start( RaftId raftId ) throws Exception
    {
        this.boundRaftId = raftId;
        this.delegateHandler.start( raftId );
        this.raftMessageDispatcher.registerHandlerChain( raftId, this );
    }

    public void stop() throws Exception
    {
        try
        {
            if ( this.boundRaftId != null )
            {
                this.raftMessageDispatcher.deregisterHandlerChain( this.boundRaftId );
                this.boundRaftId = null;
            }
        }
        finally
        {
            this.delegateHandler.stop();
        }
    }

    public void handle( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message )
    {
        RaftId raftId = this.boundRaftId;
        if ( Objects.isNull( raftId ) )
        {
            this.log.debug( "Message handling has been stopped, dropping the message: %s", new Object[]{message.message()} );
        }
        else if ( !Objects.equals( raftId, message.raftId() ) )
        {
            this.log.info( "Discarding message[%s] owing to mismatched raftId. Expected: %s, Encountered: %s",
                    new Object[]{message.message(), raftId, message.raftId()} );
        }
        else
        {
            this.delegateHandler.handle( message );
        }
    }
}
