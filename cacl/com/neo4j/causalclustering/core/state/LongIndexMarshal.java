package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class LongIndexMarshal extends SafeStateMarshal<Long>
{
    public Long startState()
    {
        return -1L;
    }

    public long ordinal( Long index )
    {
        return index;
    }

    public void marshal( Long index, WritableChannel channel ) throws IOException
    {
        channel.putLong( index );
    }

    protected Long unmarshal0( ReadableChannel channel ) throws IOException
    {
        return channel.getLong();
    }
}
