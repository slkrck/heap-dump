package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.replication.DistributedOperation;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.function.ThrowingBiConsumer;

class CommandBatcher
{
    private final ThrowingBiConsumer<Long,List<DistributedOperation>,Exception> applier;
    private List<DistributedOperation> batch;
    private int maxBatchSize;
    private long lastIndex;

    CommandBatcher( int maxBatchSize, ThrowingBiConsumer<Long,List<DistributedOperation>,Exception> applier )
    {
        this.batch = new ArrayList( maxBatchSize );
        this.maxBatchSize = maxBatchSize;
        this.applier = applier;
    }

    void add( long index, DistributedOperation operation ) throws Exception
    {
        assert this.batch.size() <= 0 || index == this.lastIndex + 1L;

        this.batch.add( operation );
        this.lastIndex = index;
        if ( this.batch.size() == this.maxBatchSize )
        {
            this.flush();
        }
    }

    void flush() throws Exception
    {
        this.applier.accept( this.lastIndex, this.batch );
        this.batch.clear();
    }
}
