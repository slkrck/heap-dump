package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.state.storage.SimpleStorage;
import com.neo4j.causalclustering.core.state.version.ClusterStateVersion;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.ArrayUtils;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClusterStateMigrator extends LifecycleAdapter
{
    private static final ClusterStateVersion CURRENT_VERSION = new ClusterStateVersion( 1, 0 );
    private final ClusterStateLayout clusterStateLayout;
    private final SimpleStorage<ClusterStateVersion> clusterStateVersionStorage;
    private final FileSystemAbstraction fs;
    private final Log log;

    public ClusterStateMigrator( FileSystemAbstraction fs, ClusterStateLayout clusterStateLayout, SimpleStorage<ClusterStateVersion> clusterStateVersionStorage,
            LogProvider logProvider )
    {
        this.clusterStateLayout = clusterStateLayout;
        this.clusterStateVersionStorage = clusterStateVersionStorage;
        this.fs = fs;
        this.log = logProvider.getLog( this.getClass() );
    }

    private static void validatePersistedClusterStateVersion( ClusterStateVersion persistedVersion )
    {
        if ( !Objects.equals( persistedVersion, CURRENT_VERSION ) )
        {
            throw new IllegalStateException( "Illegal cluster state version: " + persistedVersion + ". Migration for this version does not exist" );
        }
    }

    public void init()
    {
        ClusterStateVersion persistedVersion = this.readClusterStateVersion();
        this.log.info( "Persisted cluster state version is: %s", new Object[]{persistedVersion} );
        if ( persistedVersion == null )
        {
            this.migrateWhenClusterStateVersionIsAbsent();
        }
        else
        {
            validatePersistedClusterStateVersion( persistedVersion );
        }
    }

    private void migrateWhenClusterStateVersionIsAbsent()
    {
        try
        {
            File[] oldClusterStateFiles = this.fs.listFiles( this.clusterStateLayout.getClusterStateDirectory(), this::isNotMemberIdStorage );
            if ( ArrayUtils.isNotEmpty( oldClusterStateFiles ) )
            {
                File[] var2 = oldClusterStateFiles;
                int var3 = oldClusterStateFiles.length;

                for ( int var4 = 0; var4 < var3; ++var4 )
                {
                    File oldClusterStateFile = var2[var4];
                    this.fs.deleteRecursively( oldClusterStateFile );
                }

                this.log.info( "Deleted old cluster state entries %s", new Object[]{Arrays.toString( oldClusterStateFiles )} );
            }

            this.clusterStateVersionStorage.writeState( CURRENT_VERSION );
            this.log.info( "Created a version storage for version %s", new Object[]{CURRENT_VERSION} );
        }
        catch ( IOException var6 )
        {
            throw new UncheckedIOException( "Unable to migrate the cluster state directory", var6 );
        }
    }

    private boolean isNotMemberIdStorage( File parentDir, String name )
    {
        File clusterStateDir = this.clusterStateLayout.getClusterStateDirectory();
        String memberIdDir = this.clusterStateLayout.memberIdStateFile().getParentFile().getName();
        return !parentDir.equals( clusterStateDir ) || !name.equals( memberIdDir );
    }

    private ClusterStateVersion readClusterStateVersion()
    {
        if ( this.clusterStateVersionStorage.exists() )
        {
            try
            {
                return (ClusterStateVersion) this.clusterStateVersionStorage.readState();
            }
            catch ( IOException var2 )
            {
                throw new UncheckedIOException( "Unable to read cluster state version", var2 );
            }
        }
        else
        {
            return null;
        }
    }
}
