package com.neo4j.causalclustering.core.state.machines.lease;

import com.neo4j.causalclustering.core.consensus.LeaderLocator;
import com.neo4j.causalclustering.core.consensus.NoLeaderFoundException;
import com.neo4j.causalclustering.core.replication.ReplicationResult;
import com.neo4j.causalclustering.core.replication.Replicator;
import com.neo4j.causalclustering.identity.MemberId;
import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.api.exceptions.Status.General;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.api.LeaseException;
import org.neo4j.kernel.impl.api.LeaseService;

public class ClusterLeaseCoordinator implements LeaseService
{
    private static final String NOT_ON_LEADER_ERROR_MESSAGE = "Should only attempt to acquire lease when leader.";
    private final MemberId myself;
    private final Replicator replicator;
    private final LeaderLocator leaderLocator;
    private final ReplicatedLeaseStateMachine leaseStateMachine;
    private final NamedDatabaseId namedDatabaseId;
    private volatile int invalidLeaseId = -1;
    private volatile int myLeaseId = -1;

    public ClusterLeaseCoordinator( MemberId myself, Replicator replicator, LeaderLocator leaderLocator, ReplicatedLeaseStateMachine leaseStateMachine,
            NamedDatabaseId namedDatabaseId )
    {
        this.myself = myself;
        this.replicator = replicator;
        this.leaderLocator = leaderLocator;
        this.leaseStateMachine = leaseStateMachine;
        this.namedDatabaseId = namedDatabaseId;
    }

    public LeaseClient newClient()
    {
        return new ClusterLeaseClient( this );
    }

    public boolean isInvalid( int leaseId )
    {
        if ( leaseId == -1 )
        {
            throw new IllegalArgumentException( "Not a lease" );
        }
        else
        {
            return leaseId == this.invalidLeaseId || leaseId != this.leaseStateMachine.leaseId() || leaseId != this.myLeaseId;
        }
    }

    public synchronized void invalidateLease( int leaseId )
    {
        if ( leaseId != -1 && leaseId == this.leaseStateMachine.leaseId() )
        {
            this.invalidLeaseId = leaseId;
        }
    }

    private Lease currentLease()
    {
        ReplicatedLeaseState state = this.leaseStateMachine.snapshot();
        return new ReplicatedLeaseRequest( state, this.namedDatabaseId );
    }

    synchronized int acquireLeaseOrThrow() throws LeaseException
    {
        Lease currentLease = this.currentLease();
        if ( this.myself.equals( currentLease.owner() ) && !this.isInvalid( currentLease.id() ) )
        {
            return currentLease.id();
        }
        else
        {
            this.ensureLeader();
            ReplicatedLeaseRequest leaseRequest =
                    new ReplicatedLeaseRequest( this.myself, Lease.nextCandidateId( currentLease.id() ), this.namedDatabaseId.databaseId() );
            ReplicationResult replicationResult = this.replicator.replicate( leaseRequest );
            if ( replicationResult.outcome() != ReplicationResult.Outcome.APPLIED )
            {
                throw new LeaseException( "Failed to acquire lease", replicationResult.failure(), Cluster.ReplicationFailure );
            }
            else
            {
                boolean leaseAcquired;
                try
                {
                    leaseAcquired = (Boolean) replicationResult.stateMachineResult().consume();
                }
                catch ( Exception var6 )
                {
                    throw new LeaseException( "Unexpected exception", var6, General.UnknownError );
                }

                if ( !leaseAcquired )
                {
                    throw new LeaseException( "Failed to acquire lease since it was taken by another candidate", Cluster.NotALeader );
                }
                else
                {
                    this.invalidLeaseId = currentLease.id();
                    return this.myLeaseId = leaseRequest.id();
                }
            }
        }
    }

    private void ensureLeader() throws LeaseException
    {
        MemberId leader = null;

        try
        {
            leader = this.leaderLocator.getLeaderInfo().memberId();
        }
        catch ( NoLeaderFoundException var3 )
        {
        }

        if ( !this.myself.equals( leader ) )
        {
            throw new LeaseException( "Should only attempt to acquire lease when leader.", Cluster.NotALeader );
        }
    }
}
