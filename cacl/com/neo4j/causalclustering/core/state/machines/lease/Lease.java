package com.neo4j.causalclustering.core.state.machines.lease;

interface Lease
{
    static int nextCandidateId( int currentId )
    {
        int candidateId = currentId + 1;
        if ( candidateId == -1 )
        {
            ++candidateId;
        }

        return candidateId;
    }

    int id();

    Object owner();
}
