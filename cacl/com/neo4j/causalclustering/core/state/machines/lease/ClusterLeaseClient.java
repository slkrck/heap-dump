package com.neo4j.causalclustering.core.state.machines.lease;

import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.api.LeaseException;

public class ClusterLeaseClient implements LeaseClient
{
    private final ClusterLeaseCoordinator coordinator;
    private int leaseId = -1;

    ClusterLeaseClient( ClusterLeaseCoordinator coordinator )
    {
        this.coordinator = coordinator;
    }

    public int leaseId()
    {
        return this.leaseId;
    }

    public void ensureValid() throws LeaseException
    {
        if ( this.leaseId == -1 )
        {
            this.leaseId = this.coordinator.acquireLeaseOrThrow();
        }
        else if ( this.coordinator.isInvalid( this.leaseId ) )
        {
            throw new LeaseException( "Local instance lost lease.", Cluster.NotALeader );
        }
    }
}
