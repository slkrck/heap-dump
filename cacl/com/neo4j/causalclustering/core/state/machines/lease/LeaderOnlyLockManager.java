package com.neo4j.causalclustering.core.state.machines.lease;

import java.util.stream.Stream;

import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.locking.ActiveLock;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.kernel.impl.locking.Locks.Client;
import org.neo4j.kernel.impl.locking.Locks.Visitor;
import org.neo4j.lock.AcquireLockTimeoutException;
import org.neo4j.lock.LockTracer;
import org.neo4j.lock.ResourceType;

public class LeaderOnlyLockManager implements Locks
{
    private final Locks localLocks;

    public LeaderOnlyLockManager( Locks localLocks )
    {
        this.localLocks = localLocks;
    }

    public Client newClient()
    {
        return new LeaderOnlyLockManager.LeaderOnlyLockClient( this.localLocks.newClient() );
    }

    public void accept( Visitor visitor )
    {
        this.localLocks.accept( visitor );
    }

    public void close()
    {
        this.localLocks.close();
    }

    private static class LeaderOnlyLockClient implements Client
    {
        private final Client localClient;
        private LeaseClient leaseClient;

        LeaderOnlyLockClient( Client localClient )
        {
            this.localClient = localClient;
        }

        public void initialize( LeaseClient leaseClient )
        {
            this.leaseClient = leaseClient;
        }

        public void acquireShared( LockTracer tracer, ResourceType resourceType, long... resourceId ) throws AcquireLockTimeoutException
        {
            this.localClient.acquireShared( tracer, resourceType, resourceId );
        }

        public void acquireExclusive( LockTracer tracer, ResourceType resourceType, long... resourceId ) throws AcquireLockTimeoutException
        {
            this.ensureExclusiveLockCanBeAcquired();
            this.localClient.acquireExclusive( tracer, resourceType, resourceId );
        }

        public boolean tryExclusiveLock( ResourceType resourceType, long resourceId )
        {
            this.ensureExclusiveLockCanBeAcquired();
            return this.localClient.tryExclusiveLock( resourceType, resourceId );
        }

        public boolean trySharedLock( ResourceType resourceType, long resourceId )
        {
            return this.localClient.trySharedLock( resourceType, resourceId );
        }

        public boolean reEnterShared( ResourceType resourceType, long resourceId )
        {
            return this.localClient.reEnterShared( resourceType, resourceId );
        }

        public boolean reEnterExclusive( ResourceType resourceType, long resourceId )
        {
            this.ensureExclusiveLockCanBeAcquired();
            return this.localClient.reEnterExclusive( resourceType, resourceId );
        }

        public void releaseShared( ResourceType resourceType, long... resourceIds )
        {
            this.localClient.releaseShared( resourceType, resourceIds );
        }

        public void releaseExclusive( ResourceType resourceType, long... resourceIds )
        {
            this.localClient.releaseExclusive( resourceType, resourceIds );
        }

        public void prepare()
        {
            this.localClient.prepare();
        }

        public void stop()
        {
            this.localClient.stop();
        }

        public void close()
        {
            this.localClient.close();
        }

        public int getLockSessionId()
        {
            return this.leaseClient.leaseId();
        }

        public Stream<? extends ActiveLock> activeLocks()
        {
            return this.localClient.activeLocks();
        }

        public long activeLockCount()
        {
            return this.localClient.activeLockCount();
        }

        void ensureExclusiveLockCanBeAcquired()
        {
            this.leaseClient.ensureValid();
        }
    }
}
