package com.neo4j.causalclustering.core.state.machines.lease;

import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.StateMachine;
import com.neo4j.causalclustering.core.state.storage.StateStorage;

import java.io.IOException;
import java.util.function.Consumer;

import org.neo4j.util.concurrent.Runnables;

public class ReplicatedLeaseStateMachine implements StateMachine<ReplicatedLeaseRequest>
{
    private final StateStorage<ReplicatedLeaseState> storage;
    private final Runnable runOnLeaseChange;
    private ReplicatedLeaseState state;
    private volatile int leaseId;

    public ReplicatedLeaseStateMachine( StateStorage<ReplicatedLeaseState> storage )
    {
        this( storage, Runnables.EMPTY_RUNNABLE );
    }

    public ReplicatedLeaseStateMachine( StateStorage<ReplicatedLeaseState> storage, Runnable runOnLeaseChange )
    {
        this.leaseId = -1;
        this.storage = storage;
        this.runOnLeaseChange = runOnLeaseChange;
    }

    public synchronized void applyCommand( ReplicatedLeaseRequest leaseRequest, long commandIndex, Consumer<StateMachineResult> callback )
    {
        if ( commandIndex > this.state().ordinal() )
        {
            boolean requestAccepted = leaseRequest.id() == Lease.nextCandidateId( this.state.leaseId() );
            if ( requestAccepted )
            {
                this.state = new ReplicatedLeaseState( commandIndex, leaseRequest );
                this.leaseId = this.state.leaseId();
                this.runOnLeaseChange.run();
            }

            callback.accept( StateMachineResult.of( (Object) requestAccepted ) );
        }
    }

    public synchronized void flush() throws IOException
    {
        this.storage.writeState( this.state() );
    }

    public long lastAppliedIndex()
    {
        return this.state().ordinal();
    }

    private ReplicatedLeaseState state()
    {
        if ( this.state == null )
        {
            this.state = (ReplicatedLeaseState) this.storage.getInitialState();
            this.leaseId = this.state.leaseId();
        }

        return this.state;
    }

    public synchronized ReplicatedLeaseState snapshot()
    {
        return this.state().newInstance();
    }

    int leaseId()
    {
        return this.leaseId;
    }

    public synchronized void installSnapshot( ReplicatedLeaseState snapshot )
    {
        this.state = snapshot;
    }
}
