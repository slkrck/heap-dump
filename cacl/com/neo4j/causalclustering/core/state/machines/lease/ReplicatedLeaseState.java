package com.neo4j.causalclustering.core.state.machines.lease;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.Objects;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ReplicatedLeaseState
{
    public static final ReplicatedLeaseState INITIAL_LEASE_STATE;

    static
    {
        INITIAL_LEASE_STATE = new ReplicatedLeaseState( -1L, ReplicatedLeaseRequest.INVALID_LEASE_REQUEST );
    }

    private final long ordinal;
    private final MemberId owner;
    private final int leaseId;

    public ReplicatedLeaseState( long ordinal, ReplicatedLeaseRequest leaseRequest )
    {
        this( ordinal, leaseRequest.id(), leaseRequest.owner() );
    }

    private ReplicatedLeaseState( long ordinal, int leaseId, MemberId owner )
    {
        this.ordinal = ordinal;
        this.leaseId = leaseId;
        this.owner = owner;
    }

    public int leaseId()
    {
        return this.leaseId;
    }

    public MemberId owner()
    {
        return this.owner;
    }

    long ordinal()
    {
        return this.ordinal;
    }

    public String toString()
    {
        return String.format( "ReplicatedLeaseState{leaseId=%s, owner=%s, ordinal=%d}", this.leaseId, this.owner, this.ordinal );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReplicatedLeaseState that = (ReplicatedLeaseState) o;
            return this.ordinal == that.ordinal && this.leaseId == that.leaseId && Objects.equals( this.owner, that.owner );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.ordinal, this.owner, this.leaseId} );
    }

    ReplicatedLeaseState newInstance()
    {
        return new ReplicatedLeaseState( this.ordinal, this.leaseId, this.owner );
    }

    public static class Marshal extends SafeStateMarshal<ReplicatedLeaseState>
    {
        private final ChannelMarshal<MemberId> memberMarshal;

        public Marshal()
        {
            this.memberMarshal = MemberId.Marshal.INSTANCE;
        }

        public void marshal( ReplicatedLeaseState state, WritableChannel channel ) throws IOException
        {
            channel.putLong( state.ordinal );
            channel.putInt( state.leaseId() );
            this.memberMarshal.marshal( state.owner(), channel );
        }

        public ReplicatedLeaseState unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
        {
            long logIndex = channel.getLong();
            int leaseId = channel.getInt();
            MemberId member = (MemberId) this.memberMarshal.unmarshal( channel );
            return new ReplicatedLeaseState( logIndex, leaseId, member );
        }

        public ReplicatedLeaseState startState()
        {
            return ReplicatedLeaseState.INITIAL_LEASE_STATE;
        }

        public long ordinal( ReplicatedLeaseState state )
        {
            return state.ordinal();
        }
    }
}
