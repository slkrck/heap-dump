package com.neo4j.causalclustering.core.state.machines.lease;

import com.neo4j.causalclustering.core.state.CommandDispatcher;
import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.tx.CoreReplicatedContent;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class ReplicatedLeaseRequest implements CoreReplicatedContent, Lease
{
    static final ReplicatedLeaseRequest INVALID_LEASE_REQUEST = new ReplicatedLeaseRequest( (MemberId) null, -1, (DatabaseId) null );
    private final MemberId owner;
    private final int leaseId;
    private final DatabaseId databaseId;

    public ReplicatedLeaseRequest( ReplicatedLeaseState state, NamedDatabaseId namedDatabaseId )
    {
        this( state.owner(), state.leaseId(), namedDatabaseId.databaseId() );
    }

    public ReplicatedLeaseRequest( MemberId owner, int leaseId, DatabaseId databaseId )
    {
        this.owner = owner;
        this.leaseId = leaseId;
        this.databaseId = databaseId;
    }

    public int id()
    {
        return this.leaseId;
    }

    public MemberId owner()
    {
        return this.owner;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReplicatedLeaseRequest that = (ReplicatedLeaseRequest) o;
            return this.leaseId == that.leaseId && Objects.equals( this.owner, that.owner );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.owner, this.leaseId} );
    }

    public String toString()
    {
        return String.format( "ReplicatedLeaseRequest{owner=%s, leaseId=%d}", this.owner, this.leaseId );
    }

    public void dispatch( CommandDispatcher commandDispatcher, long commandIndex, Consumer<StateMachineResult> callback )
    {
        commandDispatcher.dispatch( this, commandIndex, callback );
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }
}
