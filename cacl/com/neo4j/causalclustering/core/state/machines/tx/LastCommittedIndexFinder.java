package com.neo4j.causalclustering.core.state.machines.tx;

import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.LogicalTransactionStore;
import org.neo4j.kernel.impl.transaction.log.NoSuchTransactionException;
import org.neo4j.kernel.impl.transaction.log.TransactionCursor;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.TransactionIdStore;

public class LastCommittedIndexFinder
{
    private final TransactionIdStore transactionIdStore;
    private final LogicalTransactionStore transactionStore;
    private final Log log;

    public LastCommittedIndexFinder( TransactionIdStore transactionIdStore, LogicalTransactionStore transactionStore, LogProvider logProvider )
    {
        this.transactionIdStore = transactionIdStore;
        this.transactionStore = transactionStore;
        this.log = logProvider.getLog( this.getClass() );
    }

    public long getLastCommittedIndex()
    {
        long lastTxId = this.transactionIdStore.getLastCommittedTransactionId();
        this.log.info( "Last transaction id in metadata store %d", new Object[]{lastTxId} );
        CommittedTransactionRepresentation lastTx = null;

        try
        {
            TransactionCursor transactions = this.transactionStore.getTransactions( lastTxId );

            try
            {
                while ( transactions.next() )
                {
                    lastTx = (CommittedTransactionRepresentation) transactions.get();
                }
            }
            catch ( Throwable var10 )
            {
                if ( transactions != null )
                {
                    try
                    {
                        transactions.close();
                    }
                    catch ( Throwable var9 )
                    {
                        var10.addSuppressed( var9 );
                    }
                }

                throw var10;
            }

            if ( transactions != null )
            {
                transactions.close();
            }
        }
        catch ( NoSuchTransactionException var11 )
        {
            if ( lastTx != null )
            {
                throw new IllegalStateException( var11 );
            }
        }
        catch ( Exception var12 )
        {
            throw new RuntimeException( var12 );
        }

        if ( lastTx == null )
        {
            throw new RuntimeException( "We must have at least one transaction telling us where we are at in the consensus log." );
        }
        else
        {
            this.log.info( "Start id of last committed transaction in transaction log %d",
                    new Object[]{lastTx.getStartEntry().getLastCommittedTxWhenTransactionStarted()} );
            this.log.info( "Last committed transaction id in transaction log %d", new Object[]{lastTx.getCommitEntry().getTxId()} );
            byte[] lastHeaderFound = lastTx.getStartEntry().getAdditionalHeader();
            long lastConsensusIndex = LogIndexTxHeaderEncoding.decodeLogIndexFromTxHeader( lastHeaderFound );
            this.log.info( "Last committed consensus log index committed into tx log %d", new Object[]{lastConsensusIndex} );
            return lastConsensusIndex;
        }
    }
}
