package com.neo4j.causalclustering.core.state.machines.tx;

import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.ByteBufBacked;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.OutputStreamWritableChannel;
import io.netty.buffer.ByteBuf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public class ReplicatedTransactionMarshalV2
{
    private ReplicatedTransactionMarshalV2()
    {
    }

    public static void marshal( WritableChannel writableChannel, ByteArrayReplicatedTransaction replicatedTransaction ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( replicatedTransaction.databaseId(), writableChannel );
        int length = replicatedTransaction.getTxBytes().length;
        writableChannel.putInt( length );
        writableChannel.put( replicatedTransaction.getTxBytes(), length );
    }

    public static ReplicatedTransaction unmarshal( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
        int txBytesLength = channel.getInt();
        byte[] txBytes = new byte[txBytesLength];
        channel.get( txBytes, txBytesLength );
        return ReplicatedTransaction.from( txBytes, databaseId );
    }

    public static void marshal( WritableChannel writableChannel, TransactionRepresentationReplicatedTransaction replicatedTransaction ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( replicatedTransaction.databaseId(), writableChannel );
        int txStartIndex;
        if ( writableChannel instanceof ByteBufBacked )
        {
            ByteBuf buffer = ((ByteBufBacked) writableChannel).byteBuf();
            int metaDataIndex = buffer.writerIndex();
            txStartIndex = metaDataIndex + 4;
            buffer.writerIndex( txStartIndex );
            writeTx( writableChannel, replicatedTransaction.tx() );
            int txLength = buffer.writerIndex() - txStartIndex;
            buffer.setInt( metaDataIndex, txLength );
        }
        else
        {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream( 1024 );
            OutputStreamWritableChannel outputStreamWritableChannel = new OutputStreamWritableChannel( outputStream );
            writeTx( outputStreamWritableChannel, replicatedTransaction.tx() );
            txStartIndex = outputStream.size();
            writableChannel.putInt( txStartIndex );
            writableChannel.put( outputStream.toByteArray(), txStartIndex );
        }
    }

    private static void writeTx( WritableChannel writableChannel, TransactionRepresentation tx ) throws IOException
    {
        ReplicatedTransactionFactory.TransactionRepresentationWriter txWriter = ReplicatedTransactionFactory.transactionalRepresentationWriter( tx );

        while ( txWriter.canWrite() )
        {
            txWriter.write( writableChannel );
        }
    }
}
