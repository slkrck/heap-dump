package com.neo4j.causalclustering.core.state.machines.tx;

import com.neo4j.causalclustering.messaging.marshalling.ByteArrayTransactionChunker;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import io.netty.buffer.ByteBuf;
import io.netty.handler.stream.ChunkedInput;

import java.io.IOException;
import java.util.Arrays;
import java.util.OptionalLong;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public class ByteArrayReplicatedTransaction extends ReplicatedTransaction
{
    private final byte[] txBytes;
    private final DatabaseId databaseId;

    ByteArrayReplicatedTransaction( byte[] txBytes, DatabaseId databaseId )
    {
        super( databaseId );
        this.txBytes = txBytes;
        this.databaseId = databaseId;
    }

    public OptionalLong size()
    {
        return OptionalLong.of( (long) this.txBytes.length );
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
    }

    public byte[] getTxBytes()
    {
        return this.txBytes;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ByteArrayReplicatedTransaction that = (ByteArrayReplicatedTransaction) o;
            return Arrays.equals( this.txBytes, that.txBytes );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Arrays.hashCode( this.txBytes );
    }

    public ChunkedInput<ByteBuf> encode()
    {
        return new ByteArrayTransactionChunker( this );
    }

    public TransactionRepresentation extract( TransactionRepresentationExtractor extractor )
    {
        return extractor.extract( this );
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }

    public String toString()
    {
        return "ByteArrayReplicatedTransaction{txBytes.length=" + this.txBytes.length + "}";
    }
}
