package com.neo4j.causalclustering.core.state.machines.tx;

import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public interface TransactionRepresentationExtractor
{
    TransactionRepresentation extract( TransactionRepresentationReplicatedTransaction var1 );

    TransactionRepresentation extract( ByteArrayReplicatedTransaction var1 );
}
