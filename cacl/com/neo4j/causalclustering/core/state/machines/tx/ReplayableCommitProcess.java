package com.neo4j.causalclustering.core.state.machines.tx;

import java.util.concurrent.atomic.AtomicLong;

import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.storageengine.api.TransactionApplicationMode;

class ReplayableCommitProcess implements TransactionCommitProcess
{
    private final AtomicLong lastLocalTxId = new AtomicLong( 1L );
    private final TransactionCommitProcess localCommitProcess;
    private final TransactionCounter transactionCounter;

    ReplayableCommitProcess( TransactionCommitProcess localCommitProcess, TransactionCounter transactionCounter )
    {
        this.localCommitProcess = localCommitProcess;
        this.transactionCounter = transactionCounter;
    }

    public long commit( TransactionToApply batch, CommitEvent commitEvent, TransactionApplicationMode mode ) throws TransactionFailureException
    {
        long txId = this.lastLocalTxId.incrementAndGet();
        return txId > this.transactionCounter.lastCommittedTransactionId() ? this.localCommitProcess.commit( batch, commitEvent, mode ) : txId;
    }
}
