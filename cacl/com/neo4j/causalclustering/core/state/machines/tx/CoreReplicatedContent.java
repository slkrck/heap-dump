package com.neo4j.causalclustering.core.state.machines.tx;

import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.core.state.CommandDispatcher;
import com.neo4j.causalclustering.core.state.StateMachineResult;

import java.util.function.Consumer;

import org.neo4j.kernel.database.DatabaseId;

public interface CoreReplicatedContent extends ReplicatedContent
{
    void dispatch( CommandDispatcher var1, long var2, Consumer<StateMachineResult> var4 );

    DatabaseId databaseId();
}
