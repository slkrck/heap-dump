package com.neo4j.causalclustering.core.state.machines.tx;

interface TransactionCounter
{
    long lastCommittedTransactionId();
}
