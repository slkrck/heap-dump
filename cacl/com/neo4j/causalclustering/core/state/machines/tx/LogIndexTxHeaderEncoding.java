package com.neo4j.causalclustering.core.state.machines.tx;

public class LogIndexTxHeaderEncoding
{
    private LogIndexTxHeaderEncoding()
    {
    }

    public static byte[] encodeLogIndexAsTxHeader( long logIndex )
    {
        byte[] b = new byte[8];

        for ( int i = 7; i > 0; --i )
        {
            b[i] = (byte) ((int) logIndex);
            logIndex >>>= 8;
        }

        b[0] = (byte) ((int) logIndex);
        return b;
    }

    public static long decodeLogIndexFromTxHeader( byte[] bytes )
    {
        if ( bytes.length < 8 )
        {
            throw new IllegalArgumentException( "Unable to decode RAFT log index from transaction header" );
        }
        else
        {
            long logIndex = 0L;

            for ( int i = 0; i < 8; ++i )
            {
                logIndex <<= 8;
                logIndex ^= (long) (bytes[i] & 255);
            }

            return logIndex;
        }
    }
}
