package com.neo4j.causalclustering.core.state.machines.tx;

import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import io.netty.buffer.ByteBuf;
import io.netty.handler.stream.ChunkedInput;

import java.io.IOException;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public class TransactionRepresentationReplicatedTransaction extends ReplicatedTransaction
{
    private final TransactionRepresentation tx;
    private final DatabaseId databaseId;

    public TransactionRepresentationReplicatedTransaction( TransactionRepresentation tx, DatabaseId databaseId )
    {
        super( databaseId );
        this.tx = tx;
        this.databaseId = databaseId;
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }

    public ChunkedInput<ByteBuf> encode()
    {
        return new ChunkedTransaction( this );
    }

    public TransactionRepresentation extract( TransactionRepresentationExtractor extractor )
    {
        return extractor.extract( this );
    }

    public TransactionRepresentation tx()
    {
        return this.tx;
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
    }

    public String toString()
    {
        return "TransactionRepresentationReplicatedTransaction{tx=" + this.tx + "}";
    }
}
