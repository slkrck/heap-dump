package com.neo4j.causalclustering.core.state.machines.tx;

import java.util.function.Supplier;

import org.neo4j.kernel.impl.transaction.log.LogicalTransactionStore;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.TransactionIdStore;

public class RecoverConsensusLogIndex
{
    private final LogProvider logProvider;
    private final Supplier<TransactionIdStore> txIdStore;
    private final Supplier<LogicalTransactionStore> txStore;

    public RecoverConsensusLogIndex( Supplier<TransactionIdStore> txIdStore, Supplier<LogicalTransactionStore> txStore, LogProvider logProvider )
    {
        this.txIdStore = txIdStore;
        this.txStore = txStore;
        this.logProvider = logProvider;
    }

    public long findLastAppliedIndex()
    {
        return (new LastCommittedIndexFinder( (TransactionIdStore) this.txIdStore.get(), (LogicalTransactionStore) this.txStore.get(),
                this.logProvider )).getLastCommittedIndex();
    }
}
