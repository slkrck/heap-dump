package com.neo4j.causalclustering.core.state.machines.tx;

import com.neo4j.causalclustering.core.replication.ReplicationResult;
import com.neo4j.causalclustering.core.replication.Replicator;
import com.neo4j.causalclustering.core.state.machines.lease.ClusterLeaseCoordinator;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.api.exceptions.Status.General;
import org.neo4j.kernel.api.exceptions.Status.Transaction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.storageengine.api.TransactionApplicationMode;

public class ReplicatedTransactionCommitProcess implements TransactionCommitProcess
{
    private final Replicator replicator;
    private final NamedDatabaseId namedDatabaseId;
    private final ClusterLeaseCoordinator leaseCoordinator;

    public ReplicatedTransactionCommitProcess( Replicator replicator, NamedDatabaseId namedDatabaseId, ClusterLeaseCoordinator leaseCoordinator )
    {
        this.replicator = replicator;
        this.namedDatabaseId = namedDatabaseId;
        this.leaseCoordinator = leaseCoordinator;
    }

    public long commit( TransactionToApply tx, CommitEvent commitEvent, TransactionApplicationMode mode ) throws TransactionFailureException
    {
        TransactionRepresentation txRepresentation = tx.transactionRepresentation();
        int leaseId = txRepresentation.getLeaseId();
        if ( leaseId != -1 && this.leaseCoordinator.isInvalid( leaseId ) )
        {
            throw new TransactionFailureException( Transaction.LeaseExpired, "The lease has been invalidated", new Object[0] );
        }
        else
        {
            TransactionRepresentationReplicatedTransaction transaction = ReplicatedTransaction.from( txRepresentation, this.namedDatabaseId );

            ReplicationResult replicationResult;
            try
            {
                replicationResult = this.replicator.replicate( transaction );
            }
            catch ( Throwable var11 )
            {
                this.leaseCoordinator.invalidateLease( leaseId );
                throw new TransactionFailureException( Cluster.ReplicationFailure, var11 );
            }

            switch ( replicationResult.outcome() )
            {
            case MAYBE_REPLICATED:
                this.leaseCoordinator.invalidateLease( leaseId );
            case NOT_REPLICATED:
                throw new TransactionFailureException( Cluster.ReplicationFailure, replicationResult.failure() );
            case APPLIED:
                try
                {
                    return (Long) replicationResult.stateMachineResult().consume();
                }
                catch ( TransactionFailureException var9 )
                {
                    throw var9;
                }
                catch ( Throwable var10 )
                {
                    throw new TransactionFailureException( General.UnknownError, "Unexpected exception", new Object[]{var10} );
                }
            default:
                throw new TransactionFailureException( General.UnknownError, "Unexpected outcome: " + replicationResult.outcome(), new Object[0] );
            }
        }
    }
}
