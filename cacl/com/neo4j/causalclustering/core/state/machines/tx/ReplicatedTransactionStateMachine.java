package com.neo4j.causalclustering.core.state.machines.tx;

import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.StateMachine;
import com.neo4j.causalclustering.core.state.machines.StateMachineCommitHelper;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseStateMachine;

import java.util.function.Consumer;
import java.util.function.LongConsumer;

import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.api.exceptions.Status.Transaction;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionQueue;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryReader;
import org.neo4j.kernel.impl.transaction.log.entry.VersionAwareLogEntryReader;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ReplicatedTransactionStateMachine implements StateMachine<ReplicatedTransaction>
{
    private final StateMachineCommitHelper commitHelper;
    private final ReplicatedLeaseStateMachine leaseStateMachine;
    private final int maxBatchSize;
    private final Log log;
    private final LogEntryReader reader = new VersionAwareLogEntryReader();
    private TransactionQueue queue;
    private long lastCommittedIndex = -1L;

    public ReplicatedTransactionStateMachine( StateMachineCommitHelper commitHelper, ReplicatedLeaseStateMachine leaseStateMachine, int maxBatchSize,
            LogProvider logProvider )
    {
        this.commitHelper = commitHelper;
        this.leaseStateMachine = leaseStateMachine;
        this.maxBatchSize = maxBatchSize;
        this.log = logProvider.getLog( this.getClass() );
    }

    public synchronized void installCommitProcess( TransactionCommitProcess commitProcess, long lastCommittedIndex )
    {
        this.lastCommittedIndex = lastCommittedIndex;
        this.commitHelper.updateLastAppliedCommandIndex( lastCommittedIndex );
        this.log.info( String.format( "Updated lastCommittedIndex to %d", lastCommittedIndex ) );
        this.queue = new TransactionQueue( this.maxBatchSize, ( first, last ) -> {
            this.commitHelper.commit( commitProcess, first );
        } );
    }

    public synchronized void applyCommand( ReplicatedTransaction replicatedTx, long commandIndex, Consumer<StateMachineResult> callback )
    {
        if ( commandIndex <= this.lastCommittedIndex )
        {
            this.log.debug( "Ignoring transaction at log index %d since already committed up to %d", new Object[]{commandIndex, this.lastCommittedIndex} );
        }
        else
        {
            byte[] extraHeader = LogIndexTxHeaderEncoding.encodeLogIndexAsTxHeader( commandIndex );
            TransactionRepresentation tx = ReplicatedTransactionFactory.extractTransactionRepresentation( replicatedTx, extraHeader, this.reader );
            int currentLeaseId = this.leaseStateMachine.snapshot().leaseId();
            int leaseId = tx.getLeaseId();
            if ( currentLeaseId != leaseId && leaseId != -1 )
            {
                callback.accept( StateMachineResult.of( (Exception) (new TransactionFailureException( Transaction.LeaseExpired,
                        "The lease used for the transaction has expired: [current lease id:%d, transaction lease id:%d]",
                        new Object[]{currentLeaseId, leaseId} )) ) );
            }
            else
            {
                try
                {
                    LongConsumer txCommittedCallback = ( committedTxId ) -> {
                        callback.accept( StateMachineResult.of( (Object) committedTxId ) );
                    };
                    TransactionToApply transaction = this.commitHelper.newTransactionToApply( tx, commandIndex, txCommittedCallback );
                    this.queue.queue( transaction );
                }
                catch ( Exception var11 )
                {
                    throw this.panicException( var11 );
                }
            }
        }
    }

    public void flush()
    {
    }

    public long lastAppliedIndex()
    {
        if ( this.queue == null )
        {
            throw new IllegalStateException( "Value has not been installed" );
        }
        else
        {
            return this.lastCommittedIndex;
        }
    }

    public synchronized void ensuredApplied()
    {
        try
        {
            this.queue.empty();
        }
        catch ( Exception var2 )
        {
            throw this.panicException( var2 );
        }
    }

    private IllegalStateException panicException( Exception e )
    {
        return new IllegalStateException(
                "Failed to locally commit a transaction that has already been committed to the RAFT log. This server cannot process later transactions and needs to be restarted once the underlying cause has been addressed.",
                e );
    }
}
