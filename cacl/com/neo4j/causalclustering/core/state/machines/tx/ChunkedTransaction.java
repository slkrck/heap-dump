package com.neo4j.causalclustering.core.state.machines.tx;

import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.helper.ErrorHandler;
import com.neo4j.causalclustering.messaging.ChunkingNetworkChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import io.netty.util.ReferenceCountUtil;

import java.util.LinkedList;
import java.util.Queue;

import org.neo4j.kernel.database.DatabaseId;

public class ChunkedTransaction implements ChunkedInput<ByteBuf>
{
    private static final int CHUNK_SIZE = 32768;
    private final ReplicatedTransactionFactory.TransactionRepresentationWriter txWriter;
    private final DatabaseId databaseId;
    private ChunkingNetworkChannel channel;
    private Queue<ByteBuf> chunks = new LinkedList();

    ChunkedTransaction( TransactionRepresentationReplicatedTransaction tx )
    {
        this.txWriter = ReplicatedTransactionFactory.transactionalRepresentationWriter( tx.tx() );
        this.databaseId = tx.databaseId();
    }

    public boolean isEndOfInput()
    {
        return this.channel != null && this.channel.closed() && this.chunks.isEmpty();
    }

    public void close()
    {
        ErrorHandler errorHandler = new ErrorHandler( "Closing chunked transaction" );

        try
        {
            if ( this.channel != null )
            {
                errorHandler.execute( () -> {
                    this.channel.close();
                } );
            }

            this.chunks.forEach( ( byteBuf ) -> {
                errorHandler.execute( () -> {
                    ReferenceCountUtil.release( byteBuf );
                } );
            } );
        }
        catch ( Throwable var5 )
        {
            try
            {
                errorHandler.close();
            }
            catch ( Throwable var4 )
            {
                var5.addSuppressed( var4 );
            }

            throw var5;
        }

        errorHandler.close();
    }

    public ByteBuf readChunk( ChannelHandlerContext ctx ) throws Exception
    {
        return this.readChunk( ctx.alloc() );
    }

    public ByteBuf readChunk( ByteBufAllocator allocator ) throws Exception
    {
        if ( this.isEndOfInput() )
        {
            return null;
        }
        else
        {
            if ( this.channel == null )
            {
                this.channel = new ChunkingNetworkChannel( allocator, 32768, this.chunks );
                DatabaseIdWithoutNameMarshal.INSTANCE.marshal( (DatabaseId) this.databaseId, this.channel );
            }

            while ( this.txWriter.canWrite() && this.chunks.isEmpty() )
            {
                this.txWriter.write( this.channel );
            }

            if ( this.chunks.isEmpty() )
            {
                this.channel.close();
            }

            return (ByteBuf) this.chunks.poll();
        }
    }

    public long length()
    {
        return -1L;
    }

    public long progress()
    {
        return 0L;
    }
}
