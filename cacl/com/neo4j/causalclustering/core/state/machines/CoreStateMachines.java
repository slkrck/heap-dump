package com.neo4j.causalclustering.core.state.machines;

import com.neo4j.causalclustering.core.state.CommandDispatcher;
import com.neo4j.causalclustering.core.state.CoreStateFiles;
import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.dummy.DummyMachine;
import com.neo4j.causalclustering.core.state.machines.dummy.DummyRequest;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseState;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseStateMachine;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenStateMachine;
import com.neo4j.causalclustering.core.state.machines.tx.RecoverConsensusLogIndex;
import com.neo4j.causalclustering.core.state.machines.tx.ReplicatedTransaction;
import com.neo4j.causalclustering.core.state.machines.tx.ReplicatedTransactionStateMachine;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;

import java.io.IOException;
import java.util.function.Consumer;

import org.neo4j.kernel.impl.api.TransactionCommitProcess;

public class CoreStateMachines
{
    private final ReplicatedTransactionStateMachine replicatedTxStateMachine;
    private final ReplicatedTokenStateMachine labelTokenStateMachine;
    private final ReplicatedTokenStateMachine relationshipTypeTokenStateMachine;
    private final ReplicatedTokenStateMachine propertyKeyTokenStateMachine;
    private final ReplicatedLeaseStateMachine replicatedLeaseStateMachine;
    private final DummyMachine benchmarkMachine;
    private final RecoverConsensusLogIndex consensusLogIndexRecovery;
    private final CommandDispatcher dispatcher;

    public CoreStateMachines( ReplicatedTransactionStateMachine replicatedTxStateMachine, ReplicatedTokenStateMachine labelTokenStateMachine,
            ReplicatedTokenStateMachine relationshipTypeTokenStateMachine, ReplicatedTokenStateMachine propertyKeyTokenStateMachine,
            ReplicatedLeaseStateMachine replicatedLeaseStateMachine, DummyMachine benchmarkMachine, RecoverConsensusLogIndex consensusLogIndexRecovery )
    {
        this.replicatedTxStateMachine = replicatedTxStateMachine;
        this.labelTokenStateMachine = labelTokenStateMachine;
        this.relationshipTypeTokenStateMachine = relationshipTypeTokenStateMachine;
        this.propertyKeyTokenStateMachine = propertyKeyTokenStateMachine;
        this.replicatedLeaseStateMachine = replicatedLeaseStateMachine;
        this.benchmarkMachine = benchmarkMachine;
        this.consensusLogIndexRecovery = consensusLogIndexRecovery;
        this.dispatcher = new CoreStateMachines.StateMachineCommandDispatcher();
    }

    public CommandDispatcher commandDispatcher()
    {
        return this.dispatcher;
    }

    public long getLastAppliedIndex()
    {
        return this.replicatedLeaseStateMachine.lastAppliedIndex();
    }

    public void flush() throws IOException
    {
        this.replicatedTxStateMachine.flush();
        this.labelTokenStateMachine.flush();
        this.relationshipTypeTokenStateMachine.flush();
        this.propertyKeyTokenStateMachine.flush();
        this.replicatedLeaseStateMachine.flush();
    }

    public void augmentSnapshot( CoreSnapshot coreSnapshot )
    {
        coreSnapshot.add( CoreStateFiles.LEASE, this.replicatedLeaseStateMachine.snapshot() );
    }

    public void installSnapshot( CoreSnapshot coreSnapshot )
    {
        this.replicatedLeaseStateMachine.installSnapshot( (ReplicatedLeaseState) coreSnapshot.get( CoreStateFiles.LEASE ) );
    }

    public void installCommitProcess( TransactionCommitProcess localCommit )
    {
        long lastAppliedIndex = this.consensusLogIndexRecovery.findLastAppliedIndex();
        this.replicatedTxStateMachine.installCommitProcess( localCommit, lastAppliedIndex );
        this.labelTokenStateMachine.installCommitProcess( localCommit, lastAppliedIndex );
        this.relationshipTypeTokenStateMachine.installCommitProcess( localCommit, lastAppliedIndex );
        this.propertyKeyTokenStateMachine.installCommitProcess( localCommit, lastAppliedIndex );
    }

    private class StateMachineCommandDispatcher implements CommandDispatcher
    {
        public void dispatch( ReplicatedTransaction transaction, long commandIndex, Consumer<StateMachineResult> callback )
        {
            CoreStateMachines.this.replicatedTxStateMachine.applyCommand( transaction, commandIndex, callback );
        }

        public void dispatch( ReplicatedTokenRequest tokenRequest, long commandIndex, Consumer<StateMachineResult> callback )
        {
            CoreStateMachines.this.replicatedTxStateMachine.ensuredApplied();
            switch ( tokenRequest.type() )
            {
            case PROPERTY:
                CoreStateMachines.this.propertyKeyTokenStateMachine.applyCommand( tokenRequest, commandIndex, callback );
                break;
            case RELATIONSHIP:
                CoreStateMachines.this.relationshipTypeTokenStateMachine.applyCommand( tokenRequest, commandIndex, callback );
                break;
            case LABEL:
                CoreStateMachines.this.labelTokenStateMachine.applyCommand( tokenRequest, commandIndex, callback );
                break;
            default:
                throw new IllegalStateException();
            }
        }

        public void dispatch( ReplicatedLeaseRequest leaseRequest, long commandIndex, Consumer<StateMachineResult> callback )
        {
            CoreStateMachines.this.replicatedTxStateMachine.ensuredApplied();
            CoreStateMachines.this.replicatedLeaseStateMachine.applyCommand( leaseRequest, commandIndex, callback );
        }

        public void dispatch( DummyRequest dummyRequest, long commandIndex, Consumer<StateMachineResult> callback )
        {
            CoreStateMachines.this.benchmarkMachine.applyCommand( dummyRequest, commandIndex, callback );
        }

        public void close()
        {
            CoreStateMachines.this.replicatedTxStateMachine.ensuredApplied();
        }
    }
}
