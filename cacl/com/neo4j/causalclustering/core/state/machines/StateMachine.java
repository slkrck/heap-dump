package com.neo4j.causalclustering.core.state.machines;

import com.neo4j.causalclustering.core.state.StateMachineResult;

import java.io.IOException;
import java.util.function.Consumer;

public interface StateMachine<Command>
{
    void applyCommand( Command var1, long var2, Consumer<StateMachineResult> var4 );

    void flush() throws IOException;

    long lastAppliedIndex();
}
