package com.neo4j.causalclustering.core.state.machines;

public class CommandIndexTracker
{
    private volatile long appliedCommandIndex;

    public long getAppliedCommandIndex()
    {
        return this.appliedCommandIndex;
    }

    public void setAppliedCommandIndex( long appliedCommandIndex )
    {
        this.appliedCommandIndex = appliedCommandIndex;
    }
}
