package com.neo4j.causalclustering.core.state.machines;

import com.neo4j.dbms.ReplicatedDatabaseEventService;

import java.util.function.LongConsumer;

import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracer;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContext;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContextSupplier;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.storageengine.api.TransactionApplicationMode;

public class StateMachineCommitHelper
{
    private static final LongConsumer NO_OP_COMMIT_CALLBACK = ( ignore ) -> {
    };
    private final CommandIndexTracker commandIndexTracker;
    private final PageCursorTracerSupplier pageCursorTracerSupplier;
    private final VersionContextSupplier versionContextSupplier;
    private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;

    public StateMachineCommitHelper( CommandIndexTracker commandIndexTracker, PageCursorTracerSupplier pageCursorTracerSupplier,
            VersionContextSupplier versionContextSupplier, ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch )
    {
        this.commandIndexTracker = commandIndexTracker;
        this.pageCursorTracerSupplier = pageCursorTracerSupplier;
        this.versionContextSupplier = versionContextSupplier;
        this.databaseEventDispatch = databaseEventDispatch;
    }

    public void updateLastAppliedCommandIndex( long commandIndex )
    {
        this.commandIndexTracker.setAppliedCommandIndex( commandIndex );
    }

    public void commit( TransactionCommitProcess commitProcess, TransactionRepresentation tx, long commandIndex ) throws TransactionFailureException
    {
        TransactionToApply txToApply = this.newTransactionToApply( tx, commandIndex, NO_OP_COMMIT_CALLBACK );
        this.commit( commitProcess, txToApply );
    }

    public void commit( TransactionCommitProcess commitProcess, TransactionToApply txToApply ) throws TransactionFailureException
    {
        commitProcess.commit( txToApply, CommitEvent.NULL, TransactionApplicationMode.EXTERNAL );
        ((PageCursorTracer) this.pageCursorTracerSupplier.get()).reportEvents();
    }

    public TransactionToApply newTransactionToApply( TransactionRepresentation txRepresentation, long commandIndex, LongConsumer txCommittedCallback )
    {
        VersionContext versionContext = this.versionContextSupplier.getVersionContext();
        TransactionToApply txToApply = new TransactionToApply( txRepresentation, versionContext );
        txToApply.onClose( ( committedTxId ) -> {
            long latestCommittedTxIdWhenStarted = txRepresentation.getLatestCommittedTxWhenStarted();
            if ( latestCommittedTxIdWhenStarted >= committedTxId )
            {
                throw new IllegalStateException(
                        String.format( "Out of order transaction. Expected that %d < %d", latestCommittedTxIdWhenStarted, committedTxId ) );
            }
            else
            {
                txCommittedCallback.accept( committedTxId );
                this.databaseEventDispatch.fireTransactionCommitted( committedTxId );
                this.updateLastAppliedCommandIndex( commandIndex );
            }
        } );
        return txToApply;
    }
}
