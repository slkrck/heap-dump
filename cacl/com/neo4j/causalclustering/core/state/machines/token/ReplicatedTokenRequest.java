package com.neo4j.causalclustering.core.state.machines.token;

import com.neo4j.causalclustering.core.state.CommandDispatcher;
import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.tx.CoreReplicatedContent;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;

import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;

import org.neo4j.kernel.database.DatabaseId;

public class ReplicatedTokenRequest implements CoreReplicatedContent
{
    private final TokenType type;
    private final String tokenName;
    private final byte[] commandBytes;
    private final DatabaseId databaseId;

    public ReplicatedTokenRequest( DatabaseId databaseId, TokenType type, String tokenName, byte[] commandBytes )
    {
        this.type = type;
        this.tokenName = tokenName;
        this.commandBytes = commandBytes;
        this.databaseId = databaseId;
    }

    public TokenType type()
    {
        return this.type;
    }

    String tokenName()
    {
        return this.tokenName;
    }

    byte[] commandBytes()
    {
        return this.commandBytes;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReplicatedTokenRequest that = (ReplicatedTokenRequest) o;
            if ( this.type != that.type )
            {
                return false;
            }
            else
            {
                return !this.tokenName.equals( that.tokenName ) ? false : Arrays.equals( this.commandBytes, that.commandBytes );
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.type.hashCode();
        result = 31 * result + this.tokenName.hashCode();
        result = 31 * result + Arrays.hashCode( this.commandBytes );
        return result;
    }

    public String toString()
    {
        return String.format( "ReplicatedTokenRequest{type='%s', name='%s'}", this.type, this.tokenName );
    }

    public void dispatch( CommandDispatcher commandDispatcher, long commandIndex, Consumer<StateMachineResult> callback )
    {
        commandDispatcher.dispatch( this, commandIndex, callback );
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }
}
