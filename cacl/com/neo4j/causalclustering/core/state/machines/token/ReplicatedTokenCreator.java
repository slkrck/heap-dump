package com.neo4j.causalclustering.core.state.machines.token;

import org.neo4j.kernel.api.txstate.TransactionState;

public interface ReplicatedTokenCreator
{
    void createToken( TransactionState var1, String var2, boolean var3, int var4 );
}
