package com.neo4j.causalclustering.core.state.machines.token;

import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.StateMachine;
import com.neo4j.causalclustering.core.state.machines.StateMachineCommitHelper;
import com.neo4j.causalclustering.core.state.machines.tx.LogIndexTxHeaderEncoding;

import java.util.Collection;
import java.util.function.Consumer;

import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.transaction.log.PhysicalTransactionRepresentation;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StorageCommand;
import org.neo4j.storageengine.api.StorageCommand.TokenCommand;
import org.neo4j.token.TokenRegistry;

public class ReplicatedTokenStateMachine implements StateMachine<ReplicatedTokenRequest>
{
    private final StateMachineCommitHelper commitHelper;
    private final TokenRegistry tokenRegistry;
    private final Log log;
    private TransactionCommitProcess commitProcess;
    private long lastCommittedIndex = -1L;

    public ReplicatedTokenStateMachine( StateMachineCommitHelper commitHelper, TokenRegistry tokenRegistry, LogProvider logProvider )
    {
        this.commitHelper = commitHelper;
        this.tokenRegistry = tokenRegistry;
        this.log = logProvider.getLog( this.getClass() );
    }

    public synchronized void installCommitProcess( TransactionCommitProcess commitProcess, long lastCommittedIndex )
    {
        this.commitProcess = commitProcess;
        this.lastCommittedIndex = lastCommittedIndex;
        this.commitHelper.updateLastAppliedCommandIndex( lastCommittedIndex );
        this.log.info( String.format( "(%s) Updated lastCommittedIndex to %d", this.tokenRegistry.getTokenType(), lastCommittedIndex ) );
    }

    public synchronized void applyCommand( ReplicatedTokenRequest tokenRequest, long commandIndex, Consumer<StateMachineResult> callback )
    {
        if ( commandIndex <= this.lastCommittedIndex )
        {
            this.log.warn( String.format( "Ignored %s because already committed (%d <= %d).", tokenRequest, commandIndex, this.lastCommittedIndex ) );
        }
        else
        {
            Collection<StorageCommand> commands = StorageCommandMarshal.bytesToCommands( tokenRequest.commandBytes() );
            int newTokenId = this.extractTokenId( commands );
            boolean internal = this.isInternal( commands );
            String name = tokenRequest.tokenName();
            Integer existingTokenId = internal ? this.tokenRegistry.getIdInternal( name ) : this.tokenRegistry.getId( name );
            if ( existingTokenId == null )
            {
                this.log.info( String.format( "Applying %s with newTokenId=%d", tokenRequest, newTokenId ) );
                this.applyToStore( commands, commandIndex );
                callback.accept( StateMachineResult.of( (Object) newTokenId ) );
            }
            else
            {
                this.log.warn( String.format( "Ignored %s (newTokenId=%d) since it already exists with existingTokenId=%d", tokenRequest, newTokenId,
                        existingTokenId ) );
                callback.accept( StateMachineResult.of( (Object) existingTokenId ) );
            }
        }
    }

    private void applyToStore( Collection<StorageCommand> commands, long logIndex )
    {
        PhysicalTransactionRepresentation representation = new PhysicalTransactionRepresentation( commands );
        representation.setHeader( LogIndexTxHeaderEncoding.encodeLogIndexAsTxHeader( logIndex ), 0L, 0L, 0L, 0 );

        try
        {
            this.commitHelper.commit( this.commitProcess, representation, logIndex );
        }
        catch ( TransactionFailureException var6 )
        {
            throw new RuntimeException( var6 );
        }
    }

    private int extractTokenId( Collection<StorageCommand> commands )
    {
        TokenCommand tokenCommand = this.getSingleTokenCommand( commands );
        return tokenCommand.tokenId();
    }

    private boolean isInternal( Collection<StorageCommand> commands )
    {
        TokenCommand tokenCommand = this.getSingleTokenCommand( commands );
        return tokenCommand.isInternal();
    }

    private TokenCommand getSingleTokenCommand( Collection<StorageCommand> commands )
    {
        StorageCommand command = (StorageCommand) Iterables.single( commands );
        if ( !(command instanceof TokenCommand) )
        {
            throw new IllegalArgumentException( "Was expecting a single token command, got " + command );
        }
        else
        {
            return (TokenCommand) command;
        }
    }

    public synchronized void flush()
    {
    }

    public long lastAppliedIndex()
    {
        if ( this.commitProcess == null )
        {
            throw new IllegalStateException( "Value has not been installed" );
        }
        else
        {
            return this.lastCommittedIndex;
        }
    }
}
