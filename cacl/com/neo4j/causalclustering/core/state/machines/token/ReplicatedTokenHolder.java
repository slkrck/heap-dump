package com.neo4j.causalclustering.core.state.machines.token;

import com.neo4j.causalclustering.core.replication.ReplicationResult;
import com.neo4j.causalclustering.core.replication.Replicator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;

import org.neo4j.exceptions.KernelException;
import org.neo4j.graphdb.TransactionFailureException;
import org.neo4j.internal.id.IdGeneratorFactory;
import org.neo4j.internal.id.IdType;
import org.neo4j.kernel.api.txstate.TransactionState;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.state.TxState;
import org.neo4j.lock.ResourceLocker;
import org.neo4j.storageengine.api.CommandCreationContext;
import org.neo4j.storageengine.api.StorageCommand;
import org.neo4j.storageengine.api.StorageEngine;
import org.neo4j.storageengine.api.StorageReader;
import org.neo4j.storageengine.api.txstate.TxStateVisitor;
import org.neo4j.token.AbstractTokenHolderBase;
import org.neo4j.token.TokenRegistry;

public class ReplicatedTokenHolder extends AbstractTokenHolderBase
{
    private final Replicator replicator;
    private final IdGeneratorFactory idGeneratorFactory;
    private final IdType tokenIdType;
    private final TokenType type;
    private final Supplier<StorageEngine> storageEngineSupplier;
    private final ReplicatedTokenCreator tokenCreator;
    private final DatabaseId databaseId;

    ReplicatedTokenHolder( NamedDatabaseId namedDatabaseId, TokenRegistry tokenRegistry, Replicator replicator, IdGeneratorFactory idGeneratorFactory,
            IdType tokenIdType, Supplier<StorageEngine> storageEngineSupplier, TokenType type, ReplicatedTokenCreator tokenCreator )
    {
        super( tokenRegistry );
        this.replicator = replicator;
        this.idGeneratorFactory = idGeneratorFactory;
        this.tokenIdType = tokenIdType;
        this.type = type;
        this.storageEngineSupplier = storageEngineSupplier;
        this.tokenCreator = tokenCreator;
        this.databaseId = namedDatabaseId.databaseId();
    }

    public void getOrCreateIds( String[] names, int[] ids ) throws KernelException
    {
        for ( int i = 0; i < names.length; ++i )
        {
            ids[i] = this.innerGetOrCreateId( names[i], false );
        }
    }

    public void getOrCreateInternalIds( String[] names, int[] ids ) throws KernelException
    {
        for ( int i = 0; i < names.length; ++i )
        {
            ids[i] = this.innerGetOrCreateId( names[i], true );
        }
    }

    protected int createToken( String tokenName, boolean internal )
    {
        ReplicatedTokenRequest tokenRequest = new ReplicatedTokenRequest( this.databaseId, this.type, tokenName, this.createCommands( tokenName, internal ) );
        ReplicationResult replicationResult = this.replicator.replicate( tokenRequest );
        if ( replicationResult.outcome() != ReplicationResult.Outcome.APPLIED )
        {
            throw new TransactionFailureException( "Could not replicate token for " + this.databaseId, replicationResult.failure() );
        }
        else
        {
            try
            {
                return (Integer) replicationResult.stateMachineResult().consume();
            }
            catch ( Exception var6 )
            {
                throw new IllegalStateException( var6 );
            }
        }
    }

    private byte[] createCommands( String tokenName, boolean internal )
    {
        StorageEngine storageEngine = (StorageEngine) this.storageEngineSupplier.get();
        Collection<StorageCommand> commands = new ArrayList();
        TransactionState txState = new TxState();
        int tokenId = Math.toIntExact( this.idGeneratorFactory.get( this.tokenIdType ).nextId() );
        this.tokenCreator.createToken( txState, tokenName, internal, tokenId );

        try
        {
            StorageReader reader = storageEngine.newReader();

            try
            {
                CommandCreationContext creationContext = storageEngine.newCommandCreationContext();

                try
                {
                    storageEngine.createCommands( commands, txState, reader, creationContext, ResourceLocker.PREVENT, Long.MAX_VALUE,
                            TxStateVisitor.NO_DECORATION );
                }
                catch ( Throwable var13 )
                {
                    if ( creationContext != null )
                    {
                        try
                        {
                            creationContext.close();
                        }
                        catch ( Throwable var12 )
                        {
                            var13.addSuppressed( var12 );
                        }
                    }

                    throw var13;
                }

                if ( creationContext != null )
                {
                    creationContext.close();
                }
            }
            catch ( Throwable var14 )
            {
                if ( reader != null )
                {
                    try
                    {
                        reader.close();
                    }
                    catch ( Throwable var11 )
                    {
                        var14.addSuppressed( var11 );
                    }
                }

                throw var14;
            }

            if ( reader != null )
            {
                reader.close();
            }
        }
        catch ( KernelException var15 )
        {
            throw new RuntimeException( "Unable to create token '" + tokenName + "' for " + this.databaseId, var15 );
        }

        return StorageCommandMarshal.commandsToBytes( commands );
    }
}
