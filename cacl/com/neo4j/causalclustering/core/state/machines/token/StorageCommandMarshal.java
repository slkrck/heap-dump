package com.neo4j.causalclustering.core.state.machines.token;

import com.neo4j.causalclustering.messaging.BoundedNetworkWritableChannel;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import org.neo4j.kernel.impl.transaction.log.entry.LogEntryCommand;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryReader;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryWriter;
import org.neo4j.kernel.impl.transaction.log.entry.VersionAwareLogEntryReader;
import org.neo4j.storageengine.api.StorageCommand;

public class StorageCommandMarshal
{
    public static byte[] commandsToBytes( Collection<StorageCommand> commands )
    {
        ByteBuf commandBuffer = Unpooled.buffer();
        BoundedNetworkWritableChannel channel = new BoundedNetworkWritableChannel( commandBuffer );

        try
        {
            (new LogEntryWriter( channel )).serialize( commands );
        }
        catch ( IOException var4 )
        {
            throw new IllegalStateException( "This should not be possible since it is all in memory." );
        }

        byte[] commandsBytes = Arrays.copyOf( commandBuffer.array(), commandBuffer.writerIndex() );
        commandBuffer.release();
        return commandsBytes;
    }

    static Collection<StorageCommand> bytesToCommands( byte[] commandBytes )
    {
        ByteBuf txBuffer = Unpooled.wrappedBuffer( commandBytes );
        NetworkReadableChannel channel = new NetworkReadableChannel( txBuffer );
        LogEntryReader reader = new VersionAwareLogEntryReader();
        LinkedList commands = new LinkedList();

        try
        {
            LogEntryCommand entryRead;
            while ( (entryRead = (LogEntryCommand) reader.readLogEntry( channel )) != null )
            {
                commands.add( entryRead.getCommand() );
            }

            return commands;
        }
        catch ( IOException var7 )
        {
            throw new IllegalStateException( "This should not be possible since it is all in memory." );
        }
    }
}
