package com.neo4j.causalclustering.core.state.machines.token;

public enum TokenType
{
    PROPERTY,
    RELATIONSHIP,
    LABEL;
}
