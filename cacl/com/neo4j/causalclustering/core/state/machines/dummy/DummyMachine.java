package com.neo4j.causalclustering.core.state.machines.dummy;

import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.StateMachine;

import java.util.function.Consumer;

public class DummyMachine implements StateMachine<DummyRequest>
{
    public void applyCommand( DummyRequest dummyRequest, long commandIndex, Consumer<StateMachineResult> callback )
    {
        callback.accept( StateMachineResult.of( (Object) dummyRequest ) );
    }

    public void flush()
    {
    }

    public long lastAppliedIndex()
    {
        return 0L;
    }
}
