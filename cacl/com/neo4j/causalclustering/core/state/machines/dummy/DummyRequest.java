package com.neo4j.causalclustering.core.state.machines.dummy;

import com.neo4j.causalclustering.core.state.CommandDispatcher;
import com.neo4j.causalclustering.core.state.StateMachineResult;
import com.neo4j.causalclustering.core.state.machines.tx.CoreReplicatedContent;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.marshalling.ByteArrayChunkedEncoder;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import io.netty.buffer.ByteBuf;
import io.netty.handler.stream.ChunkedInput;

import java.io.IOException;
import java.util.Arrays;
import java.util.OptionalLong;
import java.util.function.Consumer;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DummyRequest implements CoreReplicatedContent
{
    private final byte[] data;

    public DummyRequest( byte[] data )
    {
        this.data = data;
    }

    public static DummyRequest decode( ByteBuf byteBuf )
    {
        int length = byteBuf.readableBytes();
        byte[] array = new byte[length];
        byteBuf.readBytes( array );
        return new DummyRequest( array );
    }

    public OptionalLong size()
    {
        return OptionalLong.of( (long) this.data.length );
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
    }

    public long byteCount()
    {
        return this.data != null ? (long) this.data.length : 0L;
    }

    public void dispatch( CommandDispatcher commandDispatcher, long commandIndex, Consumer<StateMachineResult> callback )
    {
        commandDispatcher.dispatch( this, commandIndex, callback );
    }

    public DatabaseId databaseId()
    {
        return null;
    }

    public ChunkedInput<ByteBuf> encoder()
    {
        byte[] array = this.data;
        if ( array == null )
        {
            array = new byte[0];
        }

        return new ByteArrayChunkedEncoder( array );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            DummyRequest that = (DummyRequest) o;
            return Arrays.equals( this.data, that.data );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Arrays.hashCode( this.data );
    }

    public static class Marshal extends SafeChannelMarshal<DummyRequest>
    {
        public static final DummyRequest.Marshal INSTANCE = new DummyRequest.Marshal();

        public void marshal( DummyRequest dummy, WritableChannel channel ) throws IOException
        {
            if ( dummy.data != null )
            {
                channel.putInt( dummy.data.length );
                channel.put( dummy.data, dummy.data.length );
            }
            else
            {
                channel.putInt( 0 );
            }
        }

        protected DummyRequest unmarshal0( ReadableChannel channel ) throws IOException
        {
            int length = channel.getInt();
            byte[] data;
            if ( length > 0 )
            {
                data = new byte[length];
                channel.get( data, length );
            }
            else
            {
                data = null;
            }

            return new DummyRequest( data );
        }
    }
}
