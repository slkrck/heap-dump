package com.neo4j.causalclustering.core.state.storage;

import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.File;
import java.io.IOException;

import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.memory.BufferScope;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SimpleFileStorage<T> implements SimpleStorage<T>
{
    private final FileSystemAbstraction fileSystem;
    private final ChannelMarshal<T> marshal;
    private final File file;
    private final Log log;

    public SimpleFileStorage( FileSystemAbstraction fileSystem, File file, ChannelMarshal<T> marshal, LogProvider logProvider )
    {
        this.fileSystem = fileSystem;
        this.log = logProvider.getLog( this.getClass() );
        this.file = file;
        this.marshal = marshal;
    }

    public boolean exists()
    {
        return this.fileSystem.fileExists( this.file );
    }

    public T readState() throws IOException
    {
        try
        {
            BufferScope bufferScope = new BufferScope( ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE );

            Object var3;
            try
            {
                ReadAheadChannel channel = new ReadAheadChannel( this.fileSystem.read( this.file ), bufferScope.buffer );

                try
                {
                    var3 = this.marshal.unmarshal( channel );
                }
                catch ( Throwable var7 )
                {
                    try
                    {
                        channel.close();
                    }
                    catch ( Throwable var6 )
                    {
                        var7.addSuppressed( var6 );
                    }

                    throw var7;
                }

                channel.close();
            }
            catch ( Throwable var8 )
            {
                try
                {
                    bufferScope.close();
                }
                catch ( Throwable var5 )
                {
                    var8.addSuppressed( var5 );
                }

                throw var8;
            }

            bufferScope.close();
            return var3;
        }
        catch ( EndOfStreamException var9 )
        {
            this.log.error( "End of stream reached: " + this.file );
            throw new IOException( var9 );
        }
    }

    public void writeState( T state ) throws IOException
    {
        if ( this.file.getParentFile() != null )
        {
            this.fileSystem.mkdirs( this.file.getParentFile() );
        }

        this.fileSystem.deleteFile( this.file );
        BufferScope bufferScope = new BufferScope( Math.toIntExact( ByteUnit.kibiBytes( 512L ) ) );

        try
        {
            PhysicalFlushableChannel channel = new PhysicalFlushableChannel( this.fileSystem.write( this.file ), bufferScope.buffer );

            try
            {
                this.marshal.marshal( state, channel );
            }
            catch ( Throwable var8 )
            {
                try
                {
                    channel.close();
                }
                catch ( Throwable var7 )
                {
                    var8.addSuppressed( var7 );
                }

                throw var8;
            }

            channel.close();
        }
        catch ( Throwable var9 )
        {
            try
            {
                bufferScope.close();
            }
            catch ( Throwable var6 )
            {
                var9.addSuppressed( var6 );
            }

            throw var9;
        }

        bufferScope.close();
    }
}
