package com.neo4j.causalclustering.core.state.storage;

public abstract class SafeStateMarshal<STATE> extends SafeChannelMarshal<STATE> implements StateMarshal<STATE>
{
}
