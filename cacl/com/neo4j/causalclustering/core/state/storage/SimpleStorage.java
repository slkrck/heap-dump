package com.neo4j.causalclustering.core.state.storage;

import java.io.IOException;

public interface SimpleStorage<T> extends StateStorage<T>
{
    T readState() throws IOException;

    default T getInitialState()
    {
        return null;
    }
}
