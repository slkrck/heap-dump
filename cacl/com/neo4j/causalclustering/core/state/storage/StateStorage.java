package com.neo4j.causalclustering.core.state.storage;

import java.io.IOException;

public interface StateStorage<STATE>
{
    STATE getInitialState();

    void writeState( STATE var1 ) throws IOException;

    boolean exists();
}
