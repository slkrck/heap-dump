package com.neo4j.causalclustering.core.state.storage;

import com.neo4j.causalclustering.core.state.CoreStateFiles;
import com.neo4j.causalclustering.core.state.StateRecoveryManager;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class DurableStateStorage<STATE> extends LifecycleAdapter implements StateStorage<STATE>
{
    private final StateRecoveryManager<STATE> recoveryManager;
    private final Log log;
    private final File fileA;
    private final File fileB;
    private final FileSystemAbstraction fsa;
    private final CoreStateFiles<STATE> fileType;
    private final StateMarshal<STATE> marshal;
    private final int numberOfEntriesBeforeRotation;
    private STATE initialState;
    private int numberOfEntriesWrittenInActiveFile;
    private File currentStoreFile;
    private PhysicalFlushableChannel currentStoreChannel;

    public DurableStateStorage( FileSystemAbstraction fsa, File baseDir, CoreStateFiles<STATE> fileType, int numberOfEntriesBeforeRotation,
            LogProvider logProvider )
    {
        this.fsa = fsa;
        this.fileType = fileType;
        this.marshal = fileType.marshal();
        this.numberOfEntriesBeforeRotation = numberOfEntriesBeforeRotation;
        this.log = logProvider.getLog( this.getClass() );
        this.recoveryManager = new StateRecoveryManager( fsa, this.marshal );
        this.fileA = new File( baseDir, fileType.name() + ".a" );
        this.fileB = new File( baseDir, fileType.name() + ".b" );
    }

    public boolean exists()
    {
        return this.fsa.fileExists( this.fileA ) && this.fsa.fileExists( this.fileB );
    }

    private void create() throws IOException
    {
        this.ensureExists( this.fileA );
        this.ensureExists( this.fileB );
    }

    private void ensureExists( File file ) throws IOException
    {
        if ( !this.fsa.fileExists( file ) )
        {
            this.fsa.mkdirs( file.getParentFile() );
            PhysicalFlushableChannel channel = this.channelForFile( file );

            try
            {
                this.marshal.marshal( this.marshal.startState(), channel );
            }
            catch ( Throwable var6 )
            {
                if ( channel != null )
                {
                    try
                    {
                        channel.close();
                    }
                    catch ( Throwable var5 )
                    {
                        var6.addSuppressed( var5 );
                    }
                }

                throw var6;
            }

            if ( channel != null )
            {
                channel.close();
            }
        }
    }

    private void recover() throws IOException
    {
        StateRecoveryManager.RecoveryStatus<STATE> recoveryStatus = this.recoveryManager.recover( this.fileA, this.fileB );
        this.currentStoreFile = recoveryStatus.activeFile();
        this.currentStoreChannel = this.resetStoreFile( this.currentStoreFile );
        this.initialState = recoveryStatus.recoveredState();
        this.log.info( "%s state restored, up to ordinal %d", new Object[]{this.fileType, this.marshal.ordinal( this.initialState )} );
    }

    public STATE getInitialState()
    {
        assert this.initialState != null;

        return this.initialState;
    }

    public void init() throws IOException
    {
        this.create();
        this.recover();
    }

    public synchronized void shutdown() throws IOException
    {
        this.currentStoreChannel.close();
        this.currentStoreChannel = null;
    }

    public synchronized void writeState( STATE state ) throws IOException
    {
        if ( this.numberOfEntriesWrittenInActiveFile >= this.numberOfEntriesBeforeRotation )
        {
            this.switchStoreFile();
            this.numberOfEntriesWrittenInActiveFile = 0;
        }

        this.marshal.marshal( state, this.currentStoreChannel );
        this.currentStoreChannel.prepareForFlush().flush();
        ++this.numberOfEntriesWrittenInActiveFile;
    }

    public String toString()
    {
        return "DurableStateStorage{fileType=" + this.fileType + "}";
    }

    private void switchStoreFile() throws IOException
    {
        this.currentStoreChannel.close();
        if ( this.currentStoreFile.equals( this.fileA ) )
        {
            this.currentStoreChannel = this.resetStoreFile( this.fileB );
            this.currentStoreFile = this.fileB;
        }
        else
        {
            this.currentStoreChannel = this.resetStoreFile( this.fileA );
            this.currentStoreFile = this.fileA;
        }
    }

    private PhysicalFlushableChannel resetStoreFile( File nextStore ) throws IOException
    {
        this.fsa.truncate( nextStore, 0L );
        return this.channelForFile( nextStore );
    }

    private PhysicalFlushableChannel channelForFile( File file ) throws IOException
    {
        ByteBuffer buffer = ByteBuffer.allocate( Math.toIntExact( ByteUnit.kibiBytes( 512L ) ) );
        return new PhysicalFlushableChannel( this.fsa.write( file ), buffer );
    }
}
