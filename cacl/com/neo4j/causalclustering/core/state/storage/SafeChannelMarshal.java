package com.neo4j.causalclustering.core.state.storage;

import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadPastEndException;
import org.neo4j.io.fs.ReadableChannel;

public abstract class SafeChannelMarshal<STATE> implements ChannelMarshal<STATE>
{
    public final STATE unmarshal( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        try
        {
            return this.unmarshal0( channel );
        }
        catch ( ReadPastEndException var3 )
        {
            throw new EndOfStreamException( var3 );
        }
    }

    protected abstract STATE unmarshal0( ReadableChannel var1 ) throws IOException, EndOfStreamException;
}
