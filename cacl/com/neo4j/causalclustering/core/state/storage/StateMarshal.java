package com.neo4j.causalclustering.core.state.storage;

import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

public interface StateMarshal<STATE> extends ChannelMarshal<STATE>
{
    STATE startState();

    long ordinal( STATE var1 );
}
