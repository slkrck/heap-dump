package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.common.state.ClusterStateStorageFactory;
import com.neo4j.causalclustering.core.state.storage.SimpleStorage;
import com.neo4j.causalclustering.core.state.storage.StateStorage;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Optional;

import org.neo4j.configuration.Config;
import org.neo4j.io.fs.DefaultFileSystemAbstraction;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;

public class DumpClusterState
{
    private final ClusterStateStorageFactory storageFactory;
    private final PrintStream out;
    private final String databaseToDump;

    DumpClusterState( FileSystemAbstraction fs, File dataDirectory, PrintStream out, String databaseToDump )
    {
        this.storageFactory = newCoreStateStorageService( fs, dataDirectory );
        this.out = out;
        this.databaseToDump = databaseToDump;
    }

    public static void main( String[] args )
    {
        File dataDirectory;
        Optional databaseToDumpOpt;
        Optional databaseNameOpt;
        if ( args.length == 1 )
        {
            dataDirectory = new File( args[0] );
            databaseToDumpOpt = Optional.empty();
            databaseNameOpt = Optional.empty();
        }
        else if ( args.length == 2 )
        {
            dataDirectory = new File( args[0] );
            databaseToDumpOpt = Optional.ofNullable( args[1] );
            databaseNameOpt = Optional.empty();
        }
        else
        {
            if ( args.length != 3 )
            {
                System.out.println( "usage: DumpClusterState <data directory> ?<database to dump> ?<default database name>" );
                System.exit( 1 );
                return;
            }

            dataDirectory = new File( args[0] );
            databaseToDumpOpt = Optional.ofNullable( args[1] );
            databaseNameOpt = Optional.ofNullable( args[2] );
        }

        try
        {
            DefaultFileSystemAbstraction fileSystem = new DefaultFileSystemAbstraction();

            try
            {
                String databaseName = (String) databaseNameOpt.orElse( "neo4j" );
                String databaseToDump = (String) databaseToDumpOpt.orElse( databaseName );
                DumpClusterState dumpTool = new DumpClusterState( fileSystem, dataDirectory, System.out, databaseToDump );
                dumpTool.dump();
            }
            catch ( Throwable var9 )
            {
                try
                {
                    fileSystem.close();
                }
                catch ( Throwable var8 )
                {
                    var9.addSuppressed( var8 );
                }

                throw var9;
            }

            fileSystem.close();
        }
        catch ( Exception var10 )
        {
            System.out.println( "[ERROR] We were unable to properly dump cluster state." );
            System.out.println( "[ERROR] This usually indicates that the cluster-state folder structure is incomplete or otherwise corrupt." );
        }
    }

    private static ClusterStateStorageFactory newCoreStateStorageService( FileSystemAbstraction fs, File dataDirectory )
    {
        ClusterStateLayout layout = ClusterStateLayout.of( dataDirectory );
        return new ClusterStateStorageFactory( fs, layout, NullLogProvider.getInstance(), Config.defaults() );
    }

    void dump()
    {
        LifeSupport life = new LifeSupport();
        life.start();

        try
        {
            this.dumpSimpleState( CoreStateFiles.CORE_MEMBER_ID, this.storageFactory.createMemberIdStorage() );
            this.dumpSimpleState( CoreStateFiles.RAFT_ID,
                    this.storageFactory.createRaftIdStorage( this.databaseToDump, DatabaseLogProvider.nullDatabaseLogProvider() ) );
            this.dumpState( CoreStateFiles.LAST_FLUSHED,
                    this.storageFactory.createLastFlushedStorage( this.databaseToDump, life, DatabaseLogProvider.nullDatabaseLogProvider() ) );
            this.dumpState( CoreStateFiles.LEASE,
                    this.storageFactory.createLeaseStorage( this.databaseToDump, life, DatabaseLogProvider.nullDatabaseLogProvider() ) );
            this.dumpState( CoreStateFiles.SESSION_TRACKER,
                    this.storageFactory.createSessionTrackerStorage( this.databaseToDump, life, DatabaseLogProvider.nullDatabaseLogProvider() ) );
            this.dumpState( CoreStateFiles.RAFT_MEMBERSHIP,
                    this.storageFactory.createRaftMembershipStorage( this.databaseToDump, life, DatabaseLogProvider.nullDatabaseLogProvider() ) );
            this.dumpState( CoreStateFiles.RAFT_TERM,
                    this.storageFactory.createRaftTermStorage( this.databaseToDump, life, DatabaseLogProvider.nullDatabaseLogProvider() ) );
            this.dumpState( CoreStateFiles.RAFT_VOTE,
                    this.storageFactory.createRaftVoteStorage( this.databaseToDump, life, DatabaseLogProvider.nullDatabaseLogProvider() ) );
        }
        finally
        {
            life.shutdown();
        }
    }

    private <E> void dumpState( CoreStateFiles<E> fileType, StateStorage<E> storage )
    {
        if ( storage.exists() )
        {
            this.out.println( String.format( "%s: %s", fileType, storage.getInitialState() ) );
        }
    }

    private <E> void dumpSimpleState( CoreStateFiles<E> fileType, SimpleStorage<E> storage )
    {
        if ( storage.exists() )
        {
            String stateStr;
            try
            {
                stateStr = storage.readState().toString();
            }
            catch ( IOException var5 )
            {
                stateStr = String.format( "Error, state unreadable. %s", var5.getMessage() );
            }

            this.out.println( String.format( "%s: %s", fileType, stateStr ) );
        }
    }
}
