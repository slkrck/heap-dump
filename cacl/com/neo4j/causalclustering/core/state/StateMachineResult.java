package com.neo4j.causalclustering.core.state;

public class StateMachineResult
{
    private final Exception exception;
    private final Object result;

    private StateMachineResult( Exception exception )
    {
        this.exception = exception;
        this.result = null;
    }

    private StateMachineResult( Object result )
    {
        this.result = result;
        this.exception = null;
    }

    public static StateMachineResult of( Object result )
    {
        return new StateMachineResult( result );
    }

    public static StateMachineResult of( Exception exception )
    {
        return new StateMachineResult( exception );
    }

    public <T> T consume() throws Exception
    {
        if ( this.exception != null )
        {
            throw this.exception;
        }
        else
        {
            return this.result;
        }
    }

    public String toString()
    {
        return "Result{exception=" + this.exception + ", result=" + this.result + "}";
    }
}
