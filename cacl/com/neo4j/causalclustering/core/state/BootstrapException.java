package com.neo4j.causalclustering.core.state;

import org.neo4j.kernel.database.NamedDatabaseId;

class BootstrapException extends RuntimeException
{
    BootstrapException( NamedDatabaseId namedDatabaseId, Exception cause )
    {
        super( "Failed to bootstrap database " + namedDatabaseId.name(), cause );
    }
}
