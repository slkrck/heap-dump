package com.neo4j.causalclustering.core.state.version;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ClusterStateVersionMarshal extends SafeStateMarshal<ClusterStateVersion>
{
    public void marshal( ClusterStateVersion version, WritableChannel channel ) throws IOException
    {
        channel.putInt( version.major() );
        channel.putInt( version.minor() );
    }

    protected ClusterStateVersion unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        int major = channel.getInt();
        int minor = channel.getInt();
        return new ClusterStateVersion( major, minor );
    }

    public ClusterStateVersion startState()
    {
        return null;
    }

    public long ordinal( ClusterStateVersion clusterStateVersion )
    {
        throw new UnsupportedOperationException( "Recovery for " + ClusterStateVersion.class.getSimpleName() + " storage is not required" );
    }
}
