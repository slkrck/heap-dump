package com.neo4j.causalclustering.core.state.version;

import java.util.Objects;

public class ClusterStateVersion
{
    private final int major;
    private final int minor;

    public ClusterStateVersion( int major, int minor )
    {
        this.major = major;
        this.minor = minor;
    }

    public int major()
    {
        return this.major;
    }

    public int minor()
    {
        return this.minor;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ClusterStateVersion that = (ClusterStateVersion) o;
            return this.major == that.major && this.minor == that.minor;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.major, this.minor} );
    }

    public String toString()
    {
        return "ClusterStateVersion{major=" + this.major + ", minor=" + this.minor + "}";
    }
}
