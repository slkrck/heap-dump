package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.TempBootstrapDir;
import com.neo4j.causalclustering.core.consensus.membership.MembershipEntry;
import com.neo4j.causalclustering.core.replication.session.GlobalSessionTrackerState;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseState;
import com.neo4j.causalclustering.core.state.machines.tx.LogIndexTxHeaderEncoding;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;
import com.neo4j.causalclustering.core.state.snapshot.RaftCoreState;
import com.neo4j.causalclustering.helper.TemporaryDatabase;
import com.neo4j.causalclustering.helper.TemporaryDatabaseFactory;
import com.neo4j.causalclustering.identity.MemberId;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabasePageCache;
import org.neo4j.graphdb.factory.module.DatabaseInitializer;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.tracing.cursor.context.EmptyVersionContextSupplier;
import org.neo4j.kernel.impl.store.MetaDataStore;
import org.neo4j.kernel.impl.transaction.log.FlushablePositionAwareChecksumChannel;
import org.neo4j.kernel.impl.transaction.log.LogPositionMarker;
import org.neo4j.kernel.impl.transaction.log.PhysicalTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.TransactionLogWriter;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryWriter;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.Lifespan;
import org.neo4j.kernel.recovery.Recovery;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreId;
import org.neo4j.storageengine.api.TransactionIdStore;
import org.neo4j.storageengine.api.TransactionMetaDataStore;

public class RaftBootstrapper
{
    private static final long FIRST_INDEX = 0L;
    private static final long FIRST_TERM = 0L;
    private final BootstrapContext bootstrapContext;
    private final TemporaryDatabaseFactory tempDatabaseFactory;
    private final DatabaseInitializer databaseInitializer;
    private final PageCache pageCache;
    private final FileSystemAbstraction fs;
    private final Log log;
    private final StorageEngineFactory storageEngineFactory;
    private final Config config;
    private final BootstrapSaver bootstrapSaver;

    public RaftBootstrapper( BootstrapContext bootstrapContext, TemporaryDatabaseFactory tempDatabaseFactory, DatabaseInitializer databaseInitializer,
            PageCache pageCache, FileSystemAbstraction fs, LogProvider logProvider, StorageEngineFactory storageEngineFactory, Config config,
            BootstrapSaver bootstrapSaver )
    {
        this.bootstrapContext = bootstrapContext;
        this.tempDatabaseFactory = tempDatabaseFactory;
        this.databaseInitializer = databaseInitializer;
        this.pageCache = pageCache;
        this.fs = fs;
        this.log = logProvider.getLog( this.getClass() );
        this.storageEngineFactory = storageEngineFactory;
        this.config = config;
        this.bootstrapSaver = bootstrapSaver;
    }

    public CoreSnapshot bootstrap( Set<MemberId> members )
    {
        return this.bootstrap( members, (StoreId) null );
    }

    public CoreSnapshot bootstrap( Set<MemberId> members, StoreId storeId )
    {
        try
        {
            Log var10000 = this.log;
            String var10001 = this.bootstrapContext.databaseId().name();
            var10000.info( "Bootstrapping database " + var10001 + " for members " + members );
            if ( this.isStorePresent() )
            {
                this.ensureRecoveredOrThrow( this.bootstrapContext, this.config );
                if ( this.bootstrapContext.databaseId().isSystemDatabase() )
                {
                    this.bootstrapExistingSystemDatabase();
                }
            }
            else
            {
                this.createStore( storeId );
            }

            this.appendNullTransactionLogEntryToSetRaftIndexToMinusOne( this.bootstrapContext );
            CoreSnapshot snapshot = this.buildCoreSnapshot( members );
            var10000 = this.log;
            var10001 = this.bootstrapContext.databaseId().name();
            var10000.info( "Bootstrapping of the database " + var10001 + " completed " + snapshot );
            return snapshot;
        }
        catch ( Exception var4 )
        {
            throw new BootstrapException( this.bootstrapContext.databaseId(), var4 );
        }
    }

    public void saveStore() throws IOException
    {
        DatabaseLayout databaseLayout = this.bootstrapContext.databaseLayout();
        this.bootstrapSaver.save( databaseLayout );
    }

    private void bootstrapExistingSystemDatabase() throws IOException
    {
        TempBootstrapDir.Resource bootstrapRootDir = TempBootstrapDir.cleanBeforeAndAfter( this.fs, this.bootstrapContext.databaseLayout() );

        try
        {
            File tempDefaultDatabaseDir = new File( bootstrapRootDir.get(), "neo4j" );
            this.fs.copyRecursively( this.bootstrapContext.databaseLayout().databaseDirectory(), tempDefaultDatabaseDir );
            this.fs.copyRecursively( this.bootstrapContext.databaseLayout().getTransactionLogsDirectory(), tempDefaultDatabaseDir );
            DatabaseLayout tempDatabaseLayout = this.initializeStoreUsingTempDatabase( bootstrapRootDir.get() );
            this.bootstrapContext.replaceWith( tempDatabaseLayout.databaseDirectory() );
        }
        catch ( Throwable var5 )
        {
            if ( bootstrapRootDir != null )
            {
                try
                {
                    bootstrapRootDir.close();
                }
                catch ( Throwable var4 )
                {
                    var5.addSuppressed( var4 );
                }
            }

            throw var5;
        }

        if ( bootstrapRootDir != null )
        {
            bootstrapRootDir.close();
        }
    }

    private boolean isStorePresent()
    {
        return this.storageEngineFactory.storageExists( this.fs, this.bootstrapContext.databaseLayout(), this.pageCache );
    }

    private void createStore( StoreId storeId ) throws IOException
    {
        TempBootstrapDir.Resource bootstrapRootDir = TempBootstrapDir.cleanBeforeAndAfter( this.fs, this.bootstrapContext.databaseLayout() );

        try
        {
            String databaseName = this.bootstrapContext.databaseId().name();
            this.log.info( "Initializing the store for database " + databaseName + " using a temporary database in " + bootstrapRootDir );
            DatabaseLayout bootstrapDatabaseLayout = this.initializeStoreUsingTempDatabase( bootstrapRootDir.get() );
            if ( storeId != null )
            {
                this.log.info( "Changing store ID of bootstrapped database to " + storeId );
                MetaDataStore.setStoreId( this.pageCache, bootstrapDatabaseLayout.metadataStore(), storeId, -559063315L, 0L );
            }

            this.log.info( "Moving created store files from " + bootstrapDatabaseLayout + " to " + this.bootstrapContext.databaseLayout() );
            this.bootstrapContext.replaceWith( bootstrapDatabaseLayout.databaseDirectory() );
            this.bootstrapContext.removeTransactionLogs();
        }
        catch ( Throwable var6 )
        {
            if ( bootstrapRootDir != null )
            {
                try
                {
                    bootstrapRootDir.close();
                }
                catch ( Throwable var5 )
                {
                    var6.addSuppressed( var5 );
                }
            }

            throw var6;
        }

        if ( bootstrapRootDir != null )
        {
            bootstrapRootDir.close();
        }
    }

    private DatabaseLayout initializeStoreUsingTempDatabase( File bootstrapRootDir )
    {
        TemporaryDatabase tempDatabase = this.tempDatabaseFactory.startTemporaryDatabase( bootstrapRootDir, this.config );

        DatabaseLayout databaseLayout;
        try
        {
            this.databaseInitializer.initialize( tempDatabase.graphDatabaseService() );
            databaseLayout = tempDatabase.defaultDatabaseDirectory();
        }
        catch ( Throwable var7 )
        {
            if ( tempDatabase != null )
            {
                try
                {
                    tempDatabase.close();
                }
                catch ( Throwable var6 )
                {
                    var7.addSuppressed( var6 );
                }
            }

            throw var7;
        }

        if ( tempDatabase != null )
        {
            tempDatabase.close();
        }

        return databaseLayout;
    }

    private void ensureRecoveredOrThrow( BootstrapContext bootstrapContext, Config config ) throws Exception
    {
        if ( Recovery.isRecoveryRequired( this.fs, bootstrapContext.databaseLayout(), config ) )
        {
            String message = "Cannot bootstrap database " + bootstrapContext.databaseId().name() +
                    ". Recovery is required. Please ensure that the store being seeded comes from a cleanly shutdown instance of Neo4j or a Neo4j backup";
            this.log.error( message );
            throw new IllegalStateException( message );
        }
    }

    private CoreSnapshot buildCoreSnapshot( Set<MemberId> members )
    {
        RaftCoreState raftCoreState = new RaftCoreState( new MembershipEntry( 0L, members ) );
        GlobalSessionTrackerState sessionTrackerState = new GlobalSessionTrackerState();
        CoreSnapshot coreSnapshot = new CoreSnapshot( 0L, 0L );
        coreSnapshot.add( CoreStateFiles.RAFT_CORE_STATE, raftCoreState );
        coreSnapshot.add( CoreStateFiles.SESSION_TRACKER, sessionTrackerState );
        coreSnapshot.add( CoreStateFiles.LEASE, ReplicatedLeaseState.INITIAL_LEASE_STATE );
        return coreSnapshot;
    }

    private void appendNullTransactionLogEntryToSetRaftIndexToMinusOne( BootstrapContext bootstrapContext ) throws IOException
    {
        DatabaseLayout layout = bootstrapContext.databaseLayout();
        DatabasePageCache databasePageCache = new DatabasePageCache( this.pageCache, EmptyVersionContextSupplier.EMPTY );

        try
        {
            StoreId storeId = this.storageEngineFactory.storeId( layout, this.pageCache );
            TransactionIdStore readOnlyTransactionIdStore = this.storageEngineFactory.readOnlyTransactionIdStore( this.fs, layout, databasePageCache );
            LogFiles logFiles = LogFilesBuilder.activeFilesBuilder( layout, this.fs, databasePageCache ).withConfig( this.config ).withStoreId(
                    storeId ).withLastCommittedTransactionIdSupplier( () -> {
                return readOnlyTransactionIdStore.getLastClosedTransactionId() - 1L;
            } ).build();
            LogPositionMarker logPositionMarker = new LogPositionMarker();
            Lifespan ignored = new Lifespan( new Lifecycle[]{logFiles} );

            long dummyTransactionId;
            try
            {
                FlushablePositionAwareChecksumChannel channel = logFiles.getLogFile().getWriter();
                TransactionLogWriter writer = new TransactionLogWriter( new LogEntryWriter( channel ) );
                long lastCommittedTransactionId = readOnlyTransactionIdStore.getLastCommittedTransactionId();
                PhysicalTransactionRepresentation tx = new PhysicalTransactionRepresentation( Collections.emptyList() );
                byte[] txHeaderBytes = LogIndexTxHeaderEncoding.encodeLogIndexAsTxHeader( -1L );
                tx.setHeader( txHeaderBytes, -1L, lastCommittedTransactionId, -1L, -1 );
                dummyTransactionId = lastCommittedTransactionId + 1L;
                channel.getCurrentPosition( logPositionMarker );
                writer.append( tx, dummyTransactionId, -559063315 );
                channel.prepareForFlush().flush();
            }
            catch ( Throwable var20 )
            {
                try
                {
                    ignored.close();
                }
                catch ( Throwable var18 )
                {
                    var20.addSuppressed( var18 );
                }

                throw var20;
            }

            ignored.close();
            TransactionMetaDataStore transactionMetaDataStore =
                    this.storageEngineFactory.transactionMetaDataStore( this.fs, layout, this.config, databasePageCache );

            try
            {
                transactionMetaDataStore.setLastCommittedAndClosedTransactionId( dummyTransactionId, 0, System.currentTimeMillis(),
                        logPositionMarker.getByteOffset(), logPositionMarker.getLogVersion() );
            }
            catch ( Throwable var21 )
            {
                if ( transactionMetaDataStore != null )
                {
                    try
                    {
                        transactionMetaDataStore.close();
                    }
                    catch ( Throwable var19 )
                    {
                        var21.addSuppressed( var19 );
                    }
                }

                throw var21;
            }

            if ( transactionMetaDataStore != null )
            {
                transactionMetaDataStore.close();
            }
        }
        catch ( Throwable var22 )
        {
            try
            {
                databasePageCache.close();
            }
            catch ( Throwable var17 )
            {
                var22.addSuppressed( var17 );
            }

            throw var22;
        }

        databasePageCache.close();
    }
}
