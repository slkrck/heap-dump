package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class CoreSnapshotEncoder extends MessageToByteEncoder<CoreSnapshot>
{
    protected void encode( ChannelHandlerContext ctx, CoreSnapshot coreSnapshot, ByteBuf out ) throws Exception
    {
        (new CoreSnapshot.Marshal()).marshal( (CoreSnapshot) coreSnapshot, new NetworkWritableChannel( out ) );
    }
}
