package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.identity.MemberId;

public class TopologyLookupException extends Exception
{
    public TopologyLookupException( Throwable cause )
    {
        super( cause );
    }

    public TopologyLookupException( MemberId memberId )
    {
        super( String.format( "Cannot find the target member %s socket address", memberId ) );
    }
}
