package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.core.state.CoreStateFiles;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class CoreSnapshot
{
    private final long prevIndex;
    private final long prevTerm;
    private final Map<CoreStateFiles<?>,Object> snapshotCollection = new HashMap();

    public CoreSnapshot( long prevIndex, long prevTerm )
    {
        this.prevIndex = prevIndex;
        this.prevTerm = prevTerm;
    }

    public long prevIndex()
    {
        return this.prevIndex;
    }

    public long prevTerm()
    {
        return this.prevTerm;
    }

    public <T> void add( CoreStateFiles<T> type, T state )
    {
        this.snapshotCollection.put( type, state );
    }

    public <T> T get( CoreStateFiles<T> type )
    {
        return this.snapshotCollection.get( type );
    }

    public int size()
    {
        return this.snapshotCollection.size();
    }

    public String toString()
    {
        return String.format( "CoreSnapshot{prevIndex=%d, prevTerm=%d, snapshotCollection=%s}", this.prevIndex, this.prevTerm, this.snapshotCollection );
    }

    public static class Marshal extends SafeChannelMarshal<CoreSnapshot>
    {
        public void marshal( CoreSnapshot coreSnapshot, WritableChannel channel ) throws IOException
        {
            channel.putLong( coreSnapshot.prevIndex );
            channel.putLong( coreSnapshot.prevTerm );
            channel.putInt( coreSnapshot.size() );
            Iterator var3 = coreSnapshot.snapshotCollection.entrySet().iterator();

            while ( var3.hasNext() )
            {
                Entry<CoreStateFiles<?>,Object> entry = (Entry) var3.next();
                CoreStateFiles type = (CoreStateFiles) entry.getKey();
                Object state = entry.getValue();
                channel.putInt( type.typeId() );
                type.marshal().marshal( state, channel );
            }
        }

        public CoreSnapshot unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
        {
            long prevIndex = channel.getLong();
            long prevTerm = channel.getLong();
            CoreSnapshot coreSnapshot = new CoreSnapshot( prevIndex, prevTerm );
            int snapshotCount = channel.getInt();

            for ( int i = 0; i < snapshotCount; ++i )
            {
                int typeOrdinal = channel.getInt();
                CoreStateFiles type = (CoreStateFiles) CoreStateFiles.values().get( typeOrdinal );
                Object state = type.marshal().unmarshal( channel );
                coreSnapshot.add( type, state );
            }

            return coreSnapshot;
        }
    }
}
