package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class CoreSnapshotDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out ) throws Exception
    {
        out.add( (new CoreSnapshot.Marshal()).unmarshal( new NetworkReadableChannel( msg ) ) );
    }
}
