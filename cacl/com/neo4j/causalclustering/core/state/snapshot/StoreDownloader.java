package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.CatchupComponentsRepository;
import com.neo4j.causalclustering.catchup.storecopy.DatabaseShutdownException;
import com.neo4j.causalclustering.catchup.storecopy.RemoteStore;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFailedException;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyProcess;
import com.neo4j.causalclustering.catchup.storecopy.StoreIdDownloadFailedException;

import java.io.IOException;
import java.util.Optional;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

public class StoreDownloader
{
    private final CatchupComponentsRepository componentsRepo;
    private final Log log;

    public StoreDownloader( CatchupComponentsRepository componentsRepo, LogProvider logProvider )
    {
        this.componentsRepo = componentsRepo;
        this.log = logProvider.getLog( this.getClass() );
    }

    boolean bringUpToDate( StoreDownloadContext context, SocketAddress primaryAddress, CatchupAddressProvider addressProvider )
            throws IOException, DatabaseShutdownException
    {
        CatchupComponentsRepository.CatchupComponents components = this.getCatchupComponents( context.databaseId() );
        Optional<StoreId> validStoreId = this.validateStoreId( context, components.remoteStore(), primaryAddress );
        if ( validStoreId.isEmpty() )
        {
            return false;
        }
        else
        {
            if ( !context.isEmpty() )
            {
                if ( this.tryCatchup( context, addressProvider, components.remoteStore() ) )
                {
                    return true;
                }

                context.delete();
            }

            return this.replaceWithStore( (StoreId) validStoreId.get(), addressProvider, components.storeCopyProcess() );
        }
    }

    private Optional<StoreId> validateStoreId( StoreDownloadContext context, RemoteStore remoteStore, SocketAddress address )
    {
        StoreId remoteStoreId;
        try
        {
            remoteStoreId = remoteStore.getStoreId( address );
        }
        catch ( StoreIdDownloadFailedException var6 )
        {
            this.log.warn( "Store copy failed", var6 );
            return Optional.empty();
        }

        if ( !context.isEmpty() && !remoteStoreId.equals( context.storeId() ) )
        {
            this.log.error( "Store copy failed due to store ID mismatch" );
            return Optional.empty();
        }
        else
        {
            return Optional.ofNullable( remoteStoreId );
        }
    }

    private boolean tryCatchup( StoreDownloadContext context, CatchupAddressProvider addressProvider, RemoteStore remoteStore ) throws IOException
    {
        try
        {
            remoteStore.tryCatchingUp( addressProvider, context.storeId(), context.databaseLayout(), false, false );
            return true;
        }
        catch ( StoreCopyFailedException var5 )
        {
            this.log.warn( "Failed to catch up", var5 );
            return false;
        }
    }

    private boolean replaceWithStore( StoreId remoteStoreId, CatchupAddressProvider addressProvider, StoreCopyProcess storeCopy )
            throws IOException, DatabaseShutdownException
    {
        try
        {
            storeCopy.replaceWithStoreFrom( addressProvider, remoteStoreId );
            return true;
        }
        catch ( StoreCopyFailedException var5 )
        {
            this.log.warn( "Failed to copy and replace store", var5 );
            return false;
        }
    }

    private CatchupComponentsRepository.CatchupComponents getCatchupComponents( NamedDatabaseId namedDatabaseId )
    {
        return (CatchupComponentsRepository.CatchupComponents) this.componentsRepo.componentsFor( namedDatabaseId ).orElseThrow( () -> {
            return new IllegalStateException( String.format( "There are no catchup components for the database %s.", namedDatabaseId ) );
        } );
    }
}
