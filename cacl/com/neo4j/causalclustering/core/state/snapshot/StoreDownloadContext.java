package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.catchup.storecopy.StoreFiles;
import com.neo4j.dbms.ClusterInternalDbmsOperator;

import java.io.IOException;

import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.logging.Log;
import org.neo4j.storageengine.api.StoreId;

public class StoreDownloadContext
{
    private final Database kernelDatabase;
    private final StoreFiles storeFiles;
    private final LogFiles transactionLogs;
    private final Log log;
    private final ClusterInternalDbmsOperator internalOperator;
    private volatile StoreId storeId;

    public StoreDownloadContext( Database kernelDatabase, StoreFiles storeFiles, LogFiles transactionLogs, ClusterInternalDbmsOperator internalOperator )
    {
        this.kernelDatabase = kernelDatabase;
        this.storeFiles = storeFiles;
        this.transactionLogs = transactionLogs;
        this.log = kernelDatabase.getInternalLogProvider().getLog( this.getClass() );
        this.internalOperator = internalOperator;
    }

    NamedDatabaseId databaseId()
    {
        return this.kernelDatabase.getNamedDatabaseId();
    }

    DatabaseLayout databaseLayout()
    {
        return this.kernelDatabase.getDatabaseLayout();
    }

    public StoreId storeId()
    {
        if ( this.storeId == null )
        {
            this.storeId = this.readStoreIdFromDisk();
        }

        return this.storeId;
    }

    private StoreId readStoreIdFromDisk()
    {
        try
        {
            return this.storeFiles.readStoreId( this.databaseLayout() );
        }
        catch ( IOException var2 )
        {
            this.log.warn( "Failure reading store id", var2 );
            return null;
        }
    }

    boolean isEmpty()
    {
        return this.storeFiles.isEmpty( this.databaseLayout() );
    }

    void delete() throws IOException
    {
        this.storeFiles.delete( this.databaseLayout(), this.transactionLogs );
    }

    ClusterInternalDbmsOperator.StoreCopyHandle stopForStoreCopy()
    {
        return this.internalOperator.stopForStoreCopy( this.kernelDatabase.getNamedDatabaseId() );
    }

    public Database kernelDatabase()
    {
        return this.kernelDatabase;
    }
}
