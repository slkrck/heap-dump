package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class CoreSnapshotResponseHandler extends SimpleChannelInboundHandler<CoreSnapshot>
{
    private final CatchupClientProtocol protocol;
    private final CatchupResponseHandler listener;

    public CoreSnapshotResponseHandler( CatchupClientProtocol protocol, CatchupResponseHandler listener )
    {
        this.protocol = protocol;
        this.listener = listener;
    }

    protected void channelRead0( ChannelHandlerContext ctx, CoreSnapshot coreSnapshot )
    {
        if ( this.protocol.isExpecting( CatchupClientProtocol.State.CORE_SNAPSHOT ) )
        {
            this.listener.onCoreSnapshot( coreSnapshot );
            this.protocol.expect( CatchupClientProtocol.State.MESSAGE_TYPE );
        }
        else
        {
            ctx.fireChannelRead( coreSnapshot );
        }
    }
}
