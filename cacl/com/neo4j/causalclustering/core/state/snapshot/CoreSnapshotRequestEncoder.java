package com.neo4j.causalclustering.core.state.snapshot;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class CoreSnapshotRequestEncoder extends MessageToByteEncoder<CoreSnapshotRequest>
{
    protected void encode( ChannelHandlerContext ctx, CoreSnapshotRequest msg, ByteBuf out )
    {
        out.writeByte( 0 );
    }
}
