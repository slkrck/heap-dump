package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.CatchupAddressResolutionException;
import com.neo4j.causalclustering.catchup.storecopy.DatabaseShutdownException;

import java.io.IOException;
import java.util.Optional;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class CoreDownloader
{
    private final SnapshotDownloader snapshotDownloader;
    private final StoreDownloader storeDownloader;
    private final Log log;

    public CoreDownloader( SnapshotDownloader snapshotDownloader, StoreDownloader storeDownloader, LogProvider logProvider )
    {
        this.snapshotDownloader = snapshotDownloader;
        this.storeDownloader = storeDownloader;
        this.log = logProvider.getLog( this.getClass() );
    }

    Optional<CoreSnapshot> downloadSnapshotAndStore( StoreDownloadContext context, CatchupAddressProvider addressProvider )
            throws IOException, DatabaseShutdownException
    {
        Optional<SocketAddress> primaryOpt = this.lookupPrimary( context.databaseId(), addressProvider );
        if ( primaryOpt.isEmpty() )
        {
            return Optional.empty();
        }
        else
        {
            SocketAddress primaryAddress = (SocketAddress) primaryOpt.get();
            Optional<CoreSnapshot> coreSnapshot = this.snapshotDownloader.getCoreSnapshot( context.databaseId(), primaryAddress );
            if ( coreSnapshot.isEmpty() )
            {
                return Optional.empty();
            }
            else
            {
                return !this.storeDownloader.bringUpToDate( context, primaryAddress, addressProvider ) ? Optional.empty() : coreSnapshot;
            }
        }
    }

    private Optional<SocketAddress> lookupPrimary( NamedDatabaseId namedDatabaseId, CatchupAddressProvider addressProvider )
    {
        try
        {
            return Optional.of( addressProvider.primary( namedDatabaseId ) );
        }
        catch ( CatchupAddressResolutionException var4 )
        {
            this.log.warn( "Store copy failed, as we're unable to find the target catchup address. [Message: %s]", new Object[]{var4.getMessage()} );
            return Optional.empty();
        }
    }
}
