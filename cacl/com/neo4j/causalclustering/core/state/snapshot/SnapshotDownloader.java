package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.catchup.CatchupClientFactory;
import com.neo4j.causalclustering.catchup.CatchupResponseAdaptor;
import com.neo4j.causalclustering.catchup.VersionedCatchupClients;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SnapshotDownloader
{
    private final CatchupClientFactory catchupClientFactory;
    private final Log log;

    public SnapshotDownloader( LogProvider logProvider, CatchupClientFactory catchupClientFactory )
    {
        this.log = logProvider.getLog( this.getClass() );
        this.catchupClientFactory = catchupClientFactory;
    }

    Optional<CoreSnapshot> getCoreSnapshot( NamedDatabaseId namedDatabaseId, SocketAddress address )
    {
        this.log.info( "Downloading snapshot from core server at %s", new Object[]{address} );

        CoreSnapshot coreSnapshot;
        try
        {
            VersionedCatchupClients client = this.catchupClientFactory.getClient( address, this.log );
            CatchupResponseAdaptor<CoreSnapshot> responseHandler = new CatchupResponseAdaptor<CoreSnapshot>()
            {
                public void onCoreSnapshot( CompletableFuture<CoreSnapshot> signal, CoreSnapshot response )
                {
                    signal.complete( response );
                }
            };
            coreSnapshot = (CoreSnapshot) client.v3( ( c ) -> {
                return c.getCoreSnapshot( namedDatabaseId );
            } ).withResponseHandler( responseHandler ).request();
        }
        catch ( Exception var6 )
        {
            this.log.warn( "Store copy failed", var6 );
            return Optional.empty();
        }

        return Optional.of( coreSnapshot );
    }
}
