package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.storecopy.DatabaseShutdownException;
import com.neo4j.causalclustering.core.state.CommandApplicationProcess;
import com.neo4j.causalclustering.core.state.CoreSnapshotService;
import com.neo4j.causalclustering.error_handling.DatabasePanicker;
import com.neo4j.dbms.ClusterInternalDbmsOperator;
import com.neo4j.dbms.ReplicatedDatabaseEventService;

import java.io.IOException;
import java.util.Optional;

import org.neo4j.collection.Dependencies;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.TransactionIdStore;

public class PersistentSnapshotDownloader implements Runnable
{
    static final String DOWNLOAD_SNAPSHOT = "download of snapshot";
    static final String SHUTDOWN = "shutdown";
    private final StoreDownloadContext context;
    private final CommandApplicationProcess applicationProcess;
    private final CatchupAddressProvider addressProvider;
    private final CoreDownloader downloader;
    private final CoreSnapshotService snapshotService;
    private final ReplicatedDatabaseEventService databaseEventService;
    private final Log log;
    private final TimeoutStrategy backoffStrategy;
    private final DatabasePanicker panicker;
    private final PersistentSnapshotDownloader.Monitor monitor;
    private volatile PersistentSnapshotDownloader.State state;
    private volatile boolean stopped;

    PersistentSnapshotDownloader( CatchupAddressProvider addressProvider, CommandApplicationProcess applicationProcess, CoreDownloader downloader,
            CoreSnapshotService snapshotService, ReplicatedDatabaseEventService databaseEventService, StoreDownloadContext context, Log log,
            TimeoutStrategy backoffStrategy, DatabasePanicker panicker, Monitors monitors )
    {
        this.applicationProcess = applicationProcess;
        this.addressProvider = addressProvider;
        this.downloader = downloader;
        this.snapshotService = snapshotService;
        this.databaseEventService = databaseEventService;
        this.context = context;
        this.log = log;
        this.backoffStrategy = backoffStrategy;
        this.panicker = panicker;
        this.monitor = (PersistentSnapshotDownloader.Monitor) monitors.newMonitor( PersistentSnapshotDownloader.Monitor.class, new String[0] );
        this.state = PersistentSnapshotDownloader.State.INITIATED;
    }

    public void run()
    {
        if ( this.moveToRunningState() )
        {
            NamedDatabaseId databaseId = this.context.databaseId();
            boolean panicked = false;

            try
            {
                this.monitor.startedDownloadingSnapshot( databaseId );
                this.applicationProcess.pauseApplier( "download of snapshot" );
                this.log.info( "Stopping kernel before store copy" );
                ClusterInternalDbmsOperator.StoreCopyHandle stoppedKernel = this.context.stopForStoreCopy();
                boolean incomplete = false;
                Optional<CoreSnapshot> snapshot = this.downloadSnapshotAndStore( this.context );
                if ( snapshot.isPresent() )
                {
                    this.log.info( "Core snapshot downloaded: %s", new Object[]{snapshot.get()} );
                }
                else
                {
                    this.log.warn( "Core snapshot could not be downloaded" );
                    incomplete = true;
                }

                if ( incomplete )
                {
                    this.log.warn( "Not starting kernel after failed store copy" );
                }
                else if ( this.stopped )
                {
                    this.log.warn( "Not starting kernel after store copy because database has been requested to stop" );
                }
                else
                {
                    this.snapshotService.installSnapshot( (CoreSnapshot) snapshot.get() );
                    this.log.info( "Attempting kernel start after store copy" );
                    boolean reconcilerTriggered = stoppedKernel.release();
                    if ( !reconcilerTriggered )
                    {
                        this.log.info( "Reconciler could not be triggered at this time. This is expected during bootstrapping." );
                    }
                    else if ( !this.context.kernelDatabase().isStarted() )
                    {
                        this.log.warn(
                                "Kernel did not start properly after the store copy. This might be because of unexpected errors or normal early-exit paths." );
                    }
                    else
                    {
                        this.log.info( "Kernel started after store copy" );
                        this.notifyStoreCopied( this.context.kernelDatabase() );
                    }
                }
            }
            catch ( InterruptedException var12 )
            {
                Thread.currentThread().interrupt();
                this.log.warn( "Persistent snapshot downloader was interrupted" );
            }
            catch ( DatabaseShutdownException var13 )
            {
                this.log.warn( "Store copy aborted due to shut down", var13 );
            }
            catch ( Throwable var14 )
            {
                this.log.error( "Unrecoverable error during store copy", var14 );
                panicked = true;
                this.panicker.panic( var14 );
            }
            finally
            {
                if ( !panicked )
                {
                    this.applicationProcess.resumeApplier( "download of snapshot" );
                    this.monitor.downloadSnapshotComplete( databaseId );
                }

                this.state = PersistentSnapshotDownloader.State.COMPLETED;
            }
        }
    }

    private void notifyStoreCopied( Database database )
    {
        Dependencies dependencyResolver = database.getDependencyResolver();
        TransactionIdStore txIdStore = (TransactionIdStore) dependencyResolver.resolveDependency( TransactionIdStore.class );
        long lastCommittedTransactionId = txIdStore.getLastCommittedTransactionId();

        assert lastCommittedTransactionId == txIdStore.getLastClosedTransactionId();

        this.databaseEventService.getDatabaseEventDispatch( this.context.databaseId() ).fireStoreReplaced( lastCommittedTransactionId );
    }

    private Optional<CoreSnapshot> downloadSnapshotAndStore( StoreDownloadContext context ) throws IOException, DatabaseShutdownException, InterruptedException
    {
        Timeout backoff = this.backoffStrategy.newTimeout();

        while ( !this.stopped )
        {
            Optional<CoreSnapshot> optionalSnapshot = this.downloader.downloadSnapshotAndStore( context, this.addressProvider );
            if ( optionalSnapshot.isPresent() )
            {
                return optionalSnapshot;
            }

            Thread.sleep( backoff.getAndIncrement() );
        }

        this.log.info( "Persistent snapshot downloader was stopped before download succeeded" );
        return Optional.empty();
    }

    private synchronized boolean moveToRunningState()
    {
        if ( this.state != PersistentSnapshotDownloader.State.INITIATED )
        {
            return false;
        }
        else
        {
            this.state = PersistentSnapshotDownloader.State.RUNNING;
            return true;
        }
    }

    void stop() throws InterruptedException
    {
        this.applicationProcess.pauseApplier( "shutdown" );
        this.stopped = true;

        while ( !this.hasCompleted() )
        {
            Thread.sleep( 100L );
        }
    }

    boolean hasCompleted()
    {
        return this.state == PersistentSnapshotDownloader.State.COMPLETED;
    }

    private static enum State
    {
        INITIATED,
        RUNNING,
        COMPLETED;
    }

    public interface Monitor
    {
        void startedDownloadingSnapshot( NamedDatabaseId var1 );

        void downloadSnapshotComplete( NamedDatabaseId var1 );
    }
}
