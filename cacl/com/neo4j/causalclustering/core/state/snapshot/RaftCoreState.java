package com.neo4j.causalclustering.core.state.snapshot;

import com.neo4j.causalclustering.core.consensus.membership.MembershipEntry;
import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class RaftCoreState
{
    private MembershipEntry committed;

    public RaftCoreState( MembershipEntry committed )
    {
        this.committed = committed;
    }

    public MembershipEntry committed()
    {
        return this.committed;
    }

    public String toString()
    {
        return "RaftCoreState{committed=" + this.committed + "}";
    }

    public static class Marshal extends SafeStateMarshal<RaftCoreState>
    {
        private static final MembershipEntry.Marshal MEMBERSHIP_MARSHAL = new MembershipEntry.Marshal();

        public RaftCoreState startState()
        {
            return null;
        }

        public long ordinal( RaftCoreState raftCoreState )
        {
            return 0L;
        }

        public void marshal( RaftCoreState raftCoreState, WritableChannel channel ) throws IOException
        {
            MEMBERSHIP_MARSHAL.marshal( raftCoreState.committed(), channel );
        }

        protected RaftCoreState unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
        {
            return new RaftCoreState( (MembershipEntry) MEMBERSHIP_MARSHAL.unmarshal( channel ) );
        }
    }
}
