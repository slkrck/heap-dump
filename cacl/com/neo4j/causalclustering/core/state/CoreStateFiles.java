package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.membership.RaftMembershipState;
import com.neo4j.causalclustering.core.consensus.term.TermState;
import com.neo4j.causalclustering.core.consensus.vote.VoteState;
import com.neo4j.causalclustering.core.replication.session.GlobalSessionTrackerState;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseState;
import com.neo4j.causalclustering.core.state.snapshot.RaftCoreState;
import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.core.state.version.ClusterStateVersion;
import com.neo4j.causalclustering.core.state.version.ClusterStateVersionMarshal;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.neo4j.configuration.Config;
import org.neo4j.graphdb.config.Setting;

public class CoreStateFiles<STATE>
{
    public static final CoreStateFiles<ClusterStateVersion> VERSION;
    public static final CoreStateFiles<MemberId> CORE_MEMBER_ID;
    public static final CoreStateFiles<ReplicatedLeaseState> LEASE;
    public static final CoreStateFiles<GlobalSessionTrackerState> SESSION_TRACKER;
    public static final CoreStateFiles<RaftCoreState> RAFT_CORE_STATE;
    public static final CoreStateFiles<RaftId> RAFT_ID;
    public static final CoreStateFiles<RaftLog> RAFT_LOG;
    public static final CoreStateFiles<TermState> RAFT_TERM;
    public static final CoreStateFiles<VoteState> RAFT_VOTE;
    public static final CoreStateFiles<RaftMembershipState> RAFT_MEMBERSHIP;
    public static final CoreStateFiles<Long> LAST_FLUSHED;
    private static final List<CoreStateFiles<?>> VALUES;

    static
    {
        VERSION = new CoreStateFiles( "version", CoreStateFiles.Scope.GLOBAL, new ClusterStateVersionMarshal(), CoreStateType.VERSION );
        CORE_MEMBER_ID = new CoreStateFiles( "core-member-id", CoreStateFiles.Scope.GLOBAL, new MemberId.Marshal(), CoreStateType.CORE_MEMBER_ID );
        LEASE = new CoreStateFiles( "lease", CoreStateFiles.Scope.DATABASE, new ReplicatedLeaseState.Marshal(),
                CausalClusteringSettings.replicated_lease_state_size, CoreStateType.LEASE );
        SESSION_TRACKER = new CoreStateFiles( "session-tracker", CoreStateFiles.Scope.DATABASE, new GlobalSessionTrackerState.Marshal(),
                CausalClusteringSettings.global_session_tracker_state_size, CoreStateType.SESSION_TRACKER );
        RAFT_CORE_STATE = new CoreStateFiles( "core", CoreStateFiles.Scope.DATABASE, new RaftCoreState.Marshal(), CoreStateType.RAFT_CORE_STATE );
        RAFT_ID = new CoreStateFiles( "raft-id", CoreStateFiles.Scope.DATABASE, new RaftId.Marshal(), CoreStateType.RAFT_ID );
        RAFT_LOG = new CoreStateFiles( "raft-log", CoreStateFiles.Scope.DATABASE, (SafeStateMarshal) null, CoreStateType.RAFT_LOG );
        RAFT_TERM = new CoreStateFiles( "term", CoreStateFiles.Scope.DATABASE, new TermState.Marshal(), CausalClusteringSettings.term_state_size,
                CoreStateType.RAFT_TERM );
        RAFT_VOTE = new CoreStateFiles( "vote", CoreStateFiles.Scope.DATABASE, new VoteState.Marshal(), CausalClusteringSettings.vote_state_size,
                CoreStateType.RAFT_VOTE );
        RAFT_MEMBERSHIP = new CoreStateFiles( "membership", CoreStateFiles.Scope.DATABASE, new RaftMembershipState.Marshal(),
                CausalClusteringSettings.raft_membership_state_size, CoreStateType.RAFT_MEMBERSHIP );
        LAST_FLUSHED =
                new CoreStateFiles( "last-flushed", CoreStateFiles.Scope.DATABASE, new LongIndexMarshal(), CausalClusteringSettings.last_flushed_state_size,
                        CoreStateType.LAST_FLUSHED );
        List<CoreStateFiles<?>> all =
                Arrays.asList( VERSION, LEASE, RAFT_ID, CORE_MEMBER_ID, RAFT_LOG, RAFT_TERM, RAFT_VOTE, RAFT_MEMBERSHIP, RAFT_CORE_STATE, LAST_FLUSHED,
                        SESSION_TRACKER );
        all.sort( Comparator.comparingInt( CoreStateFiles::typeId ) );
        VALUES = Collections.unmodifiableList( all );
    }

    private final String name;
    private final CoreStateFiles.Scope scope;
    private final SafeStateMarshal<STATE> marshal;
    private final Setting<Integer> rotationSizeSetting;
    private final CoreStateType typeId;

    private CoreStateFiles( String name, CoreStateFiles.Scope scope, SafeStateMarshal<STATE> marshal, CoreStateType typeId )
    {
        this( name, scope, marshal, (Setting) null, typeId );
    }

    private CoreStateFiles( String name, CoreStateFiles.Scope scope, SafeStateMarshal<STATE> marshal, Setting<Integer> rotationSizeSetting,
            CoreStateType typeId )
    {
        this.name = name;
        this.scope = scope;
        this.marshal = marshal;
        this.typeId = typeId;
        this.rotationSizeSetting = rotationSizeSetting;
    }

    public static <S> CoreStateFiles<S> DUMMY( SafeStateMarshal<S> marshal )
    {
        return new CoreStateFiles( "dummy", CoreStateFiles.Scope.DATABASE, marshal, CoreStateType.DUMMY );
    }

    public static List<CoreStateFiles<?>> values()
    {
        return VALUES;
    }

    public String name()
    {
        return this.name;
    }

    public CoreStateFiles.Scope scope()
    {
        return this.scope;
    }

    public int rotationSize( Config config )
    {
        if ( this.rotationSizeSetting == null )
        {
            throw new UnsupportedOperationException( "This type does not rotate and thus has no rotation size setting: " + this );
        }
        else
        {
            return (Integer) config.get( this.rotationSizeSetting );
        }
    }

    public SafeStateMarshal<STATE> marshal()
    {
        if ( this.marshal == null )
        {
            throw new UnsupportedOperationException( "This type does not have a marshal registered." + this );
        }
        else
        {
            return this.marshal;
        }
    }

    public int typeId()
    {
        return this.typeId.typeId();
    }

    public String toString()
    {
        return this.name();
    }

    static enum Scope
    {
        GLOBAL,
        DATABASE;
    }
}
