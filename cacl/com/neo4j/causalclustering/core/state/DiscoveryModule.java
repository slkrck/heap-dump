package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.CoreTopologyService;
import com.neo4j.causalclustering.discovery.DiscoveryServiceFactory;
import com.neo4j.causalclustering.discovery.RemoteMembersResolver;
import com.neo4j.causalclustering.discovery.ResolutionResolverFactory;
import com.neo4j.causalclustering.discovery.RetryStrategy;
import com.neo4j.causalclustering.discovery.member.DiscoveryMemberFactory;
import com.neo4j.causalclustering.identity.MemberId;

import java.time.Duration;

import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.config.SslPolicyLoader;
import org.neo4j.time.SystemNanoClock;

public class DiscoveryModule
{
    private final SystemNanoClock clock;
    private final JobScheduler jobScheduler;
    private final LogProvider userLog;
    private final LogProvider debugLog;
    private final CoreTopologyService topologyService;

    public DiscoveryModule( MemberId myself, DiscoveryServiceFactory discoveryServiceFactory, DiscoveryMemberFactory discoveryMemberFactory,
            GlobalModule globalModule, SslPolicyLoader sslPolicyLoader )
    {
        LifeSupport globalLife = globalModule.getGlobalLife();
        Config globalConfig = globalModule.getGlobalConfig();
        LogService logService = globalModule.getLogService();
        Dependencies globalDependencies = globalModule.getGlobalDependencies();
        this.clock = globalModule.getGlobalClock();
        this.jobScheduler = globalModule.getJobScheduler();
        Monitors globalMonitors = globalModule.getGlobalMonitors();
        this.debugLog = logService.getInternalLogProvider();
        this.userLog = logService.getUserLogProvider();
        this.topologyService =
                this.createDiscoveryService( myself, discoveryServiceFactory, discoveryMemberFactory, sslPolicyLoader, globalLife, globalConfig, logService,
                        globalMonitors, globalDependencies );
    }

    private static RetryStrategy resolveStrategy( Config config )
    {
        long refreshPeriodMillis = ((Duration) config.get( CausalClusteringSettings.cluster_topology_refresh )).toMillis();
        int pollingFrequencyWithinRefreshWindow = 2;
        int numberOfRetries = pollingFrequencyWithinRefreshWindow + 1;
        long delayInMillis = refreshPeriodMillis / (long) pollingFrequencyWithinRefreshWindow;
        return new RetryStrategy( delayInMillis, (long) numberOfRetries );
    }

    private CoreTopologyService createDiscoveryService( MemberId myself, DiscoveryServiceFactory discoveryServiceFactory,
            DiscoveryMemberFactory discoveryMemberFactory, SslPolicyLoader sslPolicyLoader, LifeSupport life, Config config, LogService logService,
            Monitors monitors, Dependencies dependencies )
    {
        RemoteMembersResolver remoteMembersResolver = ResolutionResolverFactory.chooseResolver( config, logService );
        CoreTopologyService topologyService =
                discoveryServiceFactory.coreTopologyService( config, myself, this.jobScheduler, this.debugLog, this.userLog, remoteMembersResolver,
                        resolveStrategy( config ), sslPolicyLoader, discoveryMemberFactory, monitors, this.clock );
        life.add( topologyService );
        dependencies.satisfyDependency( topologyService );
        return topologyService;
    }

    public CoreTopologyService topologyService()
    {
        return this.topologyService;
    }
}
