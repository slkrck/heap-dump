package com.neo4j.causalclustering.core.state;

import java.util.function.Supplier;

import org.neo4j.internal.id.IdGeneratorFactory;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.transaction.log.LogicalTransactionStore;
import org.neo4j.storageengine.api.StorageEngine;
import org.neo4j.storageengine.api.TransactionIdStore;

public class CoreKernelResolvers
{
    private Database database;

    public void registerDatabase( Database database )
    {
        this.database = database;
    }

    public Supplier<StorageEngine> storageEngine()
    {
        return () -> {
            return (StorageEngine) this.database.getDependencyResolver().resolveDependency( StorageEngine.class );
        };
    }

    public Supplier<TransactionIdStore> txIdStore()
    {
        return () -> {
            return (TransactionIdStore) this.database.getDependencyResolver().resolveDependency( TransactionIdStore.class );
        };
    }

    public Supplier<LogicalTransactionStore> txStore()
    {
        return () -> {
            return (LogicalTransactionStore) this.database.getDependencyResolver().resolveDependency( LogicalTransactionStore.class );
        };
    }

    public Supplier<IdGeneratorFactory> idGeneratorFactory()
    {
        return () -> {
            return (IdGeneratorFactory) this.database.getDependencyResolver().resolveDependency( IdGeneratorFactory.class );
        };
    }
}
