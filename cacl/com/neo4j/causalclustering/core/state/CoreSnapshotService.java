package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.CoreState;
import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;
import com.neo4j.causalclustering.core.state.snapshot.RaftCoreState;
import com.neo4j.dbms.DatabaseStartAborter;

import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.neo4j.dbms.database.DatabaseStartAbortedException;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.internal.CappedLogger;
import org.neo4j.logging.internal.LogService;

public class CoreSnapshotService
{
    private static final String OPERATION_NAME = "snapshot request";
    private final CommandApplicationProcess applicationProcess;
    private final CoreState coreState;
    private final RaftLog raftLog;
    private final RaftMachine raftMachine;
    private final NamedDatabaseId namedDatabaseId;
    private final CappedLogger logger;

    public CoreSnapshotService( CommandApplicationProcess applicationProcess, RaftLog raftLog, CoreState coreState, RaftMachine raftMachine,
            NamedDatabaseId namedDatabaseId, LogService logService, Clock clock )
    {
        this.applicationProcess = applicationProcess;
        this.coreState = coreState;
        this.raftLog = raftLog;
        this.raftMachine = raftMachine;
        this.namedDatabaseId = namedDatabaseId;
        this.logger = (new CappedLogger( logService.getInternalLog( this.getClass() ) )).setTimeLimit( 10L, TimeUnit.SECONDS, clock );
    }

    public synchronized CoreSnapshot snapshot() throws Exception
    {
        this.applicationProcess.pauseApplier( "snapshot request" );

        CoreSnapshot var6;
        try
        {
            long lastApplied = this.applicationProcess.lastApplied();
            long prevTerm = this.raftLog.readEntryTerm( lastApplied );
            CoreSnapshot coreSnapshot = new CoreSnapshot( lastApplied, prevTerm );
            this.coreState.augmentSnapshot( coreSnapshot );
            coreSnapshot.add( CoreStateFiles.RAFT_CORE_STATE, this.raftMachine.coreState() );
            var6 = coreSnapshot;
        }
        finally
        {
            this.applicationProcess.resumeApplier( "snapshot request" );
        }

        return var6;
    }

    public synchronized void installSnapshot( CoreSnapshot coreSnapshot ) throws IOException
    {
        long snapshotPrevIndex = coreSnapshot.prevIndex();
        this.raftLog.skip( snapshotPrevIndex, coreSnapshot.prevTerm() );
        this.raftMachine.installCoreState( (RaftCoreState) coreSnapshot.get( CoreStateFiles.RAFT_CORE_STATE ) );
        this.coreState.installSnapshot( coreSnapshot );
        this.coreState.flush( snapshotPrevIndex );
        this.applicationProcess.installSnapshot( coreSnapshot );
        this.notifyAll();
    }

    public synchronized void awaitState( DatabaseStartAborter startAborter, Duration waitTime ) throws InterruptedException, DatabaseStartAbortedException
    {
        while ( this.raftMachine.state().appendIndex() < 0L )
        {
            this.logger.info( "Waiting for another raft group member to publish a core state snapshot", new Object[0] );
            if ( startAborter.shouldAbort( this.namedDatabaseId ) )
            {
                throw new DatabaseStartAbortedException( this.namedDatabaseId );
            }

            this.wait( waitTime.toMillis() );
        }
    }
}
