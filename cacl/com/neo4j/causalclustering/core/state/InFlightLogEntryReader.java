package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.consensus.log.RaftLogCursor;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;

import java.io.IOException;

public class InFlightLogEntryReader implements AutoCloseable
{
    private final ReadableRaftLog raftLog;
    private final InFlightCache inFlightCache;
    private final boolean pruneAfterRead;
    private RaftLogCursor cursor;
    private boolean useCache = true;

    public InFlightLogEntryReader( ReadableRaftLog raftLog, InFlightCache inFlightCache, boolean pruneAfterRead )
    {
        this.raftLog = raftLog;
        this.inFlightCache = inFlightCache;
        this.pruneAfterRead = pruneAfterRead;
    }

    public RaftLogEntry get( long logIndex ) throws IOException
    {
        RaftLogEntry entry = null;
        if ( this.useCache )
        {
            entry = this.inFlightCache.get( logIndex );
        }
        else
        {
            this.inFlightCache.reportSkippedCacheAccess();
        }

        if ( entry == null )
        {
            this.useCache = false;
            entry = this.getUsingCursor( logIndex );
        }

        if ( this.pruneAfterRead )
        {
            this.inFlightCache.prune( logIndex );
        }

        return entry;
    }

    private RaftLogEntry getUsingCursor( long logIndex ) throws IOException
    {
        if ( this.cursor == null )
        {
            this.cursor = this.raftLog.getEntryCursor( logIndex );
        }

        if ( this.cursor.next() )
        {
            if ( this.cursor.index() != logIndex )
            {
                throw new IllegalStateException( String.format( "expected index %d but was %s", logIndex, this.cursor.index() ) );
            }
            else
            {
                return (RaftLogEntry) this.cursor.get();
            }
        }
        else
        {
            return null;
        }
    }

    public void close() throws IOException
    {
        if ( this.cursor != null )
        {
            this.cursor.close();
        }
    }
}
