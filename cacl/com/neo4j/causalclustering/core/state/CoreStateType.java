package com.neo4j.causalclustering.core.state;

public enum CoreStateType
{
    DUMMY( -1 ),
    VERSION( 0 ),
    SESSION_TRACKER( 1 ),
    LEASE( 2 ),
    RAFT_CORE_STATE( 3 ),
    DB_NAME( 4 ),
    RAFT_ID( 5 ),
    CORE_MEMBER_ID( 6 ),
    RAFT_LOG( 7 ),
    RAFT_TERM( 8 ),
    RAFT_VOTE( 9 ),
    RAFT_MEMBERSHIP( 10 ),
    LAST_FLUSHED( 11 );

    private final int typeId;

    private CoreStateType( int typeId )
    {
        this.typeId = typeId;
    }

    public int typeId()
    {
        return this.typeId;
    }
}
