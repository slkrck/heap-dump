package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.state.storage.StateMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.File;
import java.io.IOException;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.memory.BufferScope;

public class StateRecoveryManager<STATE>
{
    protected final FileSystemAbstraction fileSystem;
    private final StateMarshal<STATE> marshal;

    public StateRecoveryManager( FileSystemAbstraction fileSystem, StateMarshal<STATE> marshal )
    {
        this.fileSystem = fileSystem;
        this.marshal = marshal;
    }

    public StateRecoveryManager.RecoveryStatus<STATE> recover( File fileA, File fileB ) throws IOException
    {
        assert fileA != null && fileB != null;

        STATE a = this.readLastEntryFrom( fileA );
        STATE b = this.readLastEntryFrom( fileB );
        if ( a == null && b == null )
        {
            throw new IllegalStateException( "no recoverable state" );
        }
        else if ( a == null )
        {
            return new StateRecoveryManager.RecoveryStatus( fileA, b );
        }
        else if ( b == null )
        {
            return new StateRecoveryManager.RecoveryStatus( fileB, a );
        }
        else
        {
            return this.marshal.ordinal( a ) > this.marshal.ordinal( b ) ? new StateRecoveryManager.RecoveryStatus( fileB, a )
                                                                         : new StateRecoveryManager.RecoveryStatus( fileA, b );
        }
    }

    private STATE readLastEntryFrom( File file ) throws IOException
    {
        BufferScope bufferScope = new BufferScope( ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE );

        Object var6;
        try
        {
            ReadAheadChannel channel = new ReadAheadChannel( this.fileSystem.read( file ), bufferScope.buffer );

            try
            {
                Object result = null;

                while ( true )
                {
                    try
                    {
                        Object lastRead;
                        if ( (lastRead = this.marshal.unmarshal( channel )) != null )
                        {
                            result = lastRead;
                            continue;
                        }
                    }
                    catch ( EndOfStreamException var9 )
                    {
                    }

                    var6 = result;
                    break;
                }
            }
            catch ( Throwable var10 )
            {
                try
                {
                    channel.close();
                }
                catch ( Throwable var8 )
                {
                    var10.addSuppressed( var8 );
                }

                throw var10;
            }

            channel.close();
        }
        catch ( Throwable var11 )
        {
            try
            {
                bufferScope.close();
            }
            catch ( Throwable var7 )
            {
                var11.addSuppressed( var7 );
            }

            throw var11;
        }

        bufferScope.close();
        return var6;
    }

    public static class RecoveryStatus<STATE>
    {
        private final File activeFile;
        private final STATE recoveredState;

        RecoveryStatus( File activeFile, STATE recoveredState )
        {
            this.activeFile = activeFile;
            this.recoveredState = recoveredState;
        }

        public STATE recoveredState()
        {
            return this.recoveredState;
        }

        public File activeFile()
        {
            return this.activeFile;
        }
    }
}
