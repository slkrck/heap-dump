package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.catchup.storecopy.StoreFiles;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.util.Arrays;
import java.util.UUID;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class BootstrapSaver
{
    private final FileSystemAbstraction fileSystem;
    private final Log log;

    public BootstrapSaver( FileSystemAbstraction fileSystem, LogProvider logProvider )
    {
        this.fileSystem = fileSystem;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void save( DatabaseLayout databaseLayout ) throws IOException
    {
        this.saveFiles( databaseLayout.databaseDirectory() );
        this.saveFiles( databaseLayout.getTransactionLogsDirectory() );
    }

    public void restore( DatabaseLayout databaseLayout ) throws IOException
    {
        this.restoreFiles( databaseLayout.databaseDirectory() );
        this.restoreFiles( databaseLayout.getTransactionLogsDirectory() );
    }

    public void clean( DatabaseLayout databaseLayout ) throws IOException
    {
        this.clean( databaseLayout.databaseDirectory() );
        this.clean( databaseLayout.getTransactionLogsDirectory() );
    }

    private void saveFiles( File directory ) throws IOException
    {
        if ( this.hasFiles( directory ) )
        {
            this.log.info( "Saving: " + directory );
            this.saveInSelf( directory );
            this.assertExistsAndEmpty( directory );
        }
    }

    private boolean hasFiles( File databaseDirectory )
    {
        File[] dbFsNodes = this.fileSystem.listFiles( databaseDirectory, StoreFiles.EXCLUDE_TEMPORARY_DIRS );
        return dbFsNodes != null && dbFsNodes.length > 0;
    }

    private void assertExistsAndEmpty( File databaseDirectory )
    {
        File[] dbFsNodes = this.fileSystem.listFiles( databaseDirectory, StoreFiles.EXCLUDE_TEMPORARY_DIRS );
        if ( dbFsNodes == null || dbFsNodes.length != 0 )
        {
            throw new IllegalStateException( "Expected empty directory: " + databaseDirectory );
        }
    }

    private void saveInSelf( File baseDir ) throws IOException
    {
        File tempSavedDir = new File( baseDir, "temp-save" );
        if ( this.fileSystem.fileExists( tempSavedDir ) )
        {
            throw new IllegalStateException( "Directory not expected to exist: " + tempSavedDir );
        }
        else
        {
            File tempRenameDir = this.tempRenameDir( baseDir );
            this.fileSystem.renameFile( baseDir, tempRenameDir, new CopyOption[0] );
            if ( !this.fileSystem.mkdir( baseDir ) )
            {
                throw new IllegalStateException( "Failed to create: " + baseDir );
            }
            else
            {
                this.fileSystem.renameFile( tempRenameDir, tempSavedDir, new CopyOption[0] );
            }
        }
    }

    private void restoreFiles( File directory ) throws IOException
    {
        File tempSaveDir = new File( directory, "temp-save" );
        if ( this.fileSystem.fileExists( tempSaveDir ) )
        {
            File[] dbFsNodes = this.fileSystem.listFiles( directory, StoreFiles.EXCLUDE_TEMPORARY_DIRS );
            if ( dbFsNodes != null )
            {
                if ( dbFsNodes.length > 0 )
                {
                    throw new IllegalStateException( "Unexpected files in directory: " + directory );
                }
                else
                {
                    this.log.info( "Restoring: " + tempSaveDir );
                    this.restoreFromSelf( directory );
                }
            }
        }
    }

    private void restoreFromSelf( File directory ) throws IOException
    {
        File tempRenameDir = this.tempRenameDir( directory );
        File tempSavedDir = new File( directory, "temp-save" );
        this.fileSystem.renameFile( tempSavedDir, tempRenameDir, new CopyOption[0] );
        File[] fsNodes = this.fileSystem.listFiles( directory, StoreFiles.EXCLUDE_TEMPORARY_DIRS );
        if ( fsNodes != null && fsNodes.length == 0 )
        {
            this.fileSystem.deleteRecursively( directory );
            this.fileSystem.renameFile( tempRenameDir, directory, new CopyOption[0] );
        }
        else
        {
            throw new IllegalStateException( "Unexpected state of directory: " + Arrays.toString( fsNodes ) );
        }
    }

    private File tempRenameDir( File baseDir )
    {
        String var10002 = baseDir.getPath();
        File tempRenameDir = new File( var10002 + "-" + UUID.randomUUID().toString().substring( 0, 8 ) );
        if ( this.fileSystem.fileExists( tempRenameDir ) )
        {
            throw new IllegalStateException( "Directory conflict: " + tempRenameDir );
        }
        else
        {
            return tempRenameDir;
        }
    }

    private void clean( File dbDir ) throws IOException
    {
        File tempSavedDir = new File( dbDir, "temp-save" );
        this.log.info( "Cleaning: " + tempSavedDir );
        this.fileSystem.deleteRecursively( tempSavedDir );
    }
}
