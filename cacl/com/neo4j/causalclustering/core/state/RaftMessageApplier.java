package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.outcome.ConsensusOutcome;
import com.neo4j.causalclustering.core.consensus.outcome.SnapshotRequirement;
import com.neo4j.causalclustering.core.state.snapshot.CoreDownloaderService;
import com.neo4j.causalclustering.error_handling.DatabasePanicker;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.LifecycleMessageHandler;

import java.util.Optional;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.JobHandle;

public class RaftMessageApplier implements LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>
{
    private final Log log;
    private final RaftMachine raftMachine;
    private final CoreDownloaderService downloadService;
    private final CommandApplicationProcess applicationProcess;
    private final DatabasePanicker panicker;
    private CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider;
    private boolean stopped;

    public RaftMessageApplier( LogProvider logProvider, RaftMachine raftMachine, CoreDownloaderService downloadService,
            CommandApplicationProcess applicationProcess, CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider,
            DatabasePanicker panicker )
    {
        this.log = logProvider.getLog( this.getClass() );
        this.raftMachine = raftMachine;
        this.downloadService = downloadService;
        this.applicationProcess = applicationProcess;
        this.catchupAddressProvider = catchupAddressProvider;
        this.panicker = panicker;
    }

    public synchronized void handle( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> wrappedMessage )
    {
        if ( !this.stopped )
        {
            try
            {
                ConsensusOutcome outcome = this.raftMachine.handle( wrappedMessage.message() );
                if ( outcome.snapshotRequirement().isPresent() )
                {
                    SnapshotRequirement snapshotRequirement = (SnapshotRequirement) outcome.snapshotRequirement().get();
                    this.log.info( String.format( "Scheduling download because of %s", snapshotRequirement ) );
                    Optional<JobHandle> downloadJob = this.downloadService.scheduleDownload( this.catchupAddressProvider );
                    if ( downloadJob.isPresent() )
                    {
                        ((JobHandle) downloadJob.get()).waitTermination();
                    }
                }
                else
                {
                    this.notifyCommitted( outcome.getCommitIndex() );
                }
            }
            catch ( Throwable var5 )
            {
                this.log.error( "Error handling message", var5 );
                this.panicker.panic( var5 );
                this.stop();
            }
        }
    }

    public synchronized void start( RaftId raftId )
    {
        this.stopped = false;
    }

    public synchronized void stop()
    {
        this.stopped = true;
    }

    private void notifyCommitted( long commitIndex )
    {
        this.applicationProcess.notifyCommitted( commitIndex );
    }
}
