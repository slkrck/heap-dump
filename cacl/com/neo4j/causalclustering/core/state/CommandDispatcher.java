package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.state.machines.dummy.DummyRequest;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import com.neo4j.causalclustering.core.state.machines.tx.ReplicatedTransaction;

import java.util.function.Consumer;

public interface CommandDispatcher extends AutoCloseable
{
    void dispatch( ReplicatedTransaction var1, long var2, Consumer<StateMachineResult> var4 );

    void dispatch( ReplicatedTokenRequest var1, long var2, Consumer<StateMachineResult> var4 );

    void dispatch( ReplicatedLeaseRequest var1, long var2, Consumer<StateMachineResult> var4 );

    void dispatch( DummyRequest var1, long var2, Consumer<StateMachineResult> var4 );

    void close();
}
