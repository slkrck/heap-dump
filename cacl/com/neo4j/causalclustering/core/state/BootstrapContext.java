package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.catchup.storecopy.StoreFiles;

import java.io.File;
import java.io.IOException;

import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;

public class BootstrapContext
{
    private final NamedDatabaseId namedDatabaseId;
    private final DatabaseLayout databaseLayout;
    private final StoreFiles storeFiles;
    private final LogFiles transactionLogs;

    public BootstrapContext( NamedDatabaseId namedDatabaseId, DatabaseLayout databaseLayout, StoreFiles storeFiles, LogFiles transactionLogs )
    {
        this.namedDatabaseId = namedDatabaseId;
        this.databaseLayout = databaseLayout;
        this.storeFiles = storeFiles;
        this.transactionLogs = transactionLogs;
    }

    NamedDatabaseId databaseId()
    {
        return this.namedDatabaseId;
    }

    DatabaseLayout databaseLayout()
    {
        return this.databaseLayout;
    }

    void replaceWith( File sourceDir ) throws IOException
    {
        this.storeFiles.delete( this.databaseLayout, this.transactionLogs );
        this.storeFiles.moveTo( sourceDir, this.databaseLayout, this.transactionLogs );
    }

    void removeTransactionLogs()
    {
        this.storeFiles.delete( this.transactionLogs );
    }
}
