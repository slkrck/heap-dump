package com.neo4j.causalclustering.core.state;

import java.io.File;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.util.Preconditions;

public class ClusterStateLayout
{
    private static final String CLUSTER_STATE_DIRECTORY_NAME = "cluster-state";
    private static final String DB_DIRECTORY_NAME = "db";
    private static final String STATE_DIRECTORY_SUFFIX = "-state";
    private final File clusterStateDirectory;

    private ClusterStateLayout( File clusterStateDirectory )
    {
        this.clusterStateDirectory = clusterStateDirectory;
    }

    public static ClusterStateLayout of( File parentDirectory )
    {
        return new ClusterStateLayout( new File( parentDirectory, "cluster-state" ) );
    }

    private static String stateDirectoryName( CoreStateFiles<?> coreStateFiles )
    {
        return coreStateFiles == CoreStateFiles.RAFT_LOG ? coreStateFiles.name() : coreStateFiles.name() + "-state";
    }

    private static void checkScope( CoreStateFiles<?> coreStateFiles, CoreStateFiles.Scope scope )
    {
        Preconditions.checkArgument( coreStateFiles.scope() == scope, "Illegal scope: " + coreStateFiles.scope() );
    }

    public File getClusterStateDirectory()
    {
        return this.clusterStateDirectory;
    }

    public File clusterStateVersionFile()
    {
        return this.globalClusterStateFile( CoreStateFiles.VERSION );
    }

    public File raftIdStateFile( String databaseName )
    {
        return this.databaseClusterStateFile( CoreStateFiles.RAFT_ID, databaseName );
    }

    public File memberIdStateFile()
    {
        return this.globalClusterStateFile( CoreStateFiles.CORE_MEMBER_ID );
    }

    public File leaseStateDirectory( String databaseName )
    {
        return this.databaseClusterStateDirectory( CoreStateFiles.LEASE, databaseName );
    }

    public File lastFlushedStateDirectory( String databaseName )
    {
        return this.databaseClusterStateDirectory( CoreStateFiles.LAST_FLUSHED, databaseName );
    }

    public File raftMembershipStateDirectory( String databaseName )
    {
        return this.databaseClusterStateDirectory( CoreStateFiles.RAFT_MEMBERSHIP, databaseName );
    }

    public File raftLogDirectory( String databaseName )
    {
        return this.databaseClusterStateDirectory( CoreStateFiles.RAFT_LOG, databaseName );
    }

    public File sessionTrackerDirectory( String databaseName )
    {
        return this.databaseClusterStateDirectory( CoreStateFiles.SESSION_TRACKER, databaseName );
    }

    public File raftTermStateDirectory( String databaseName )
    {
        return this.databaseClusterStateDirectory( CoreStateFiles.RAFT_TERM, databaseName );
    }

    public File raftVoteStateDirectory( String databaseName )
    {
        return this.databaseClusterStateDirectory( CoreStateFiles.RAFT_VOTE, databaseName );
    }

    public Set<File> listGlobalAndDatabaseDirectories( String databaseName, Predicate<CoreStateFiles<?>> stateFilesFilter )
    {
        Stream<File> globalDirectories = CoreStateFiles.values().stream().filter( ( type ) -> {
            return type.scope() == CoreStateFiles.Scope.GLOBAL;
        } ).filter( stateFilesFilter ).map( this::globalClusterStateDirectory );
        Stream<File> databaseDirectories = CoreStateFiles.values().stream().filter( ( type ) -> {
            return type.scope() == CoreStateFiles.Scope.DATABASE;
        } ).filter( stateFilesFilter ).map( ( type ) -> {
            return this.databaseClusterStateDirectory( type, databaseName );
        } );
        return (Set) Stream.concat( globalDirectories, databaseDirectories ).collect( Collectors.toSet() );
    }

    private File globalClusterStateFile( CoreStateFiles<?> coreStateFiles )
    {
        checkScope( coreStateFiles, CoreStateFiles.Scope.GLOBAL );
        File directory = this.globalClusterStateDirectory( coreStateFiles );
        return new File( directory, coreStateFiles.name() );
    }

    private File databaseClusterStateFile( CoreStateFiles<?> coreStateFiles, String databaseName )
    {
        checkScope( coreStateFiles, CoreStateFiles.Scope.DATABASE );
        File directory = this.databaseClusterStateDirectory( coreStateFiles, databaseName );
        return new File( directory, coreStateFiles.name() );
    }

    private File globalClusterStateDirectory( CoreStateFiles<?> coreStateFiles )
    {
        checkScope( coreStateFiles, CoreStateFiles.Scope.GLOBAL );
        return new File( this.clusterStateDirectory, stateDirectoryName( coreStateFiles ) );
    }

    private File databaseClusterStateDirectory( CoreStateFiles<?> coreStateFiles, String databaseName )
    {
        checkScope( coreStateFiles, CoreStateFiles.Scope.DATABASE );
        File databaseDirectory = new File( this.dbDirectory(), databaseName );
        return new File( databaseDirectory, stateDirectoryName( coreStateFiles ) );
    }

    public File raftGroupDir( String databaseName )
    {
        return new File( this.dbDirectory(), databaseName );
    }

    private File dbDirectory()
    {
        return new File( this.clusterStateDirectory, "db" );
    }
}
