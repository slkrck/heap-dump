package com.neo4j.causalclustering.core.state;

import com.neo4j.causalclustering.core.state.machines.CoreStateMachines;
import org.neo4j.graphdb.factory.module.id.DatabaseIdContext;
import org.neo4j.kernel.impl.api.CommitProcessFactory;
import org.neo4j.kernel.impl.api.LeaseService;
import org.neo4j.kernel.impl.factory.AccessCapabilityFactory;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.token.TokenHolders;

public class CoreEditionKernelComponents
{
    private final CommitProcessFactory commitProcessFactory;
    private final Locks lockManager;
    private final TokenHolders tokenHolders;
    private final DatabaseIdContext idContext;
    private final CoreStateMachines stateMachines;
    private final AccessCapabilityFactory accessCapabilityFactory;
    private final LeaseService leaseService;

    public CoreEditionKernelComponents( CommitProcessFactory commitProcessFactory, Locks lockManager, TokenHolders tokenHolders, DatabaseIdContext idContext,
            CoreStateMachines stateMachines, AccessCapabilityFactory accessCapabilityFactory, LeaseService leaseService )
    {
        this.commitProcessFactory = commitProcessFactory;
        this.lockManager = lockManager;
        this.tokenHolders = tokenHolders;
        this.idContext = idContext;
        this.stateMachines = stateMachines;
        this.accessCapabilityFactory = accessCapabilityFactory;
        this.leaseService = leaseService;
    }

    public DatabaseIdContext idContext()
    {
        return this.idContext;
    }

    public CommitProcessFactory commitProcessFactory()
    {
        return this.commitProcessFactory;
    }

    public TokenHolders tokenHolders()
    {
        return this.tokenHolders;
    }

    public Locks lockManager()
    {
        return this.lockManager;
    }

    public CoreStateMachines stateMachines()
    {
        return this.stateMachines;
    }

    public AccessCapabilityFactory accessCapabilityFactory()
    {
        return this.accessCapabilityFactory;
    }

    public LeaseService leaseService()
    {
        return this.leaseService;
    }
}
