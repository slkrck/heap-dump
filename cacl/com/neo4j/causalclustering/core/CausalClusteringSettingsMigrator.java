package com.neo4j.causalclustering.core;

import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.SettingMigrator;
import org.neo4j.configuration.SettingMigrators;
import org.neo4j.logging.Level;
import org.neo4j.logging.Log;

public class CausalClusteringSettingsMigrator implements SettingMigrator
{
    private static Level parseLevel( String value )
    {
        int levelInt = Integer.parseInt( value );
        if ( levelInt == java.util.logging.Level.OFF.intValue() )
        {
            return Level.NONE;
        }
        else if ( levelInt <= java.util.logging.Level.FINE.intValue() )
        {
            return Level.DEBUG;
        }
        else if ( levelInt <= java.util.logging.Level.INFO.intValue() )
        {
            return Level.INFO;
        }
        else
        {
            return levelInt <= java.util.logging.Level.WARNING.intValue() ? Level.WARN : Level.ERROR;
        }
    }

    public void migrate( Map<String,String> values, Map<String,String> defaultValues, Log log )
    {
        this.migrateRoutingTtl( values, log );
        this.migrateDisableMiddlewareLogging( values, log );
        this.migrateMiddlewareLoggingLevelSetting( values, log );
        this.migrateMiddlewareLoggingLevelValue( values, log );
        this.migrateAdvertisedAddresses( values, defaultValues, log );
    }

    private void migrateRoutingTtl( Map<String,String> values, Log log )
    {
        SettingMigrators.migrateSettingNameChange( values, log, "causal_clustering.cluster_routing_ttl", GraphDatabaseSettings.routing_ttl );
    }

    private void migrateDisableMiddlewareLogging( Map<String,String> values, Log log )
    {
        String oldSetting = "causal_clustering.disable_middleware_logging";
        String newSetting = CausalClusteringSettings.middleware_logging_level.name();
        String value = (String) values.remove( oldSetting );
        if ( Objects.equals( "true", value ) )
        {
            log.warn( "Use of deprecated setting %s. It is replaced by %s", new Object[]{oldSetting, newSetting} );
            values.put( newSetting, Level.NONE.toString() );
        }
    }

    private void migrateMiddlewareLoggingLevelSetting( Map<String,String> input, Log log )
    {
        String oldSetting = "causal_clustering.middleware_logging.level";
        String newSetting = CausalClusteringSettings.middleware_logging_level.name();
        String value = (String) input.remove( oldSetting );
        if ( StringUtils.isNotBlank( value ) && NumberUtils.isParsable( value ) )
        {
            log.warn( "Use of deprecated setting %s. It is replaced by %s", new Object[]{oldSetting, newSetting} );
            input.put( newSetting, value );
        }
    }

    private void migrateMiddlewareLoggingLevelValue( Map<String,String> values, Log log )
    {
        String setting = CausalClusteringSettings.middleware_logging_level.name();
        String value = (String) values.get( setting );
        if ( StringUtils.isNotBlank( value ) && NumberUtils.isParsable( value ) )
        {
            String level = parseLevel( value ).toString();
            log.warn( "Old value format in %s used. %s migrated to %s", new Object[]{setting, value, level} );
            values.put( setting, level );
        }
    }

    private void migrateAdvertisedAddresses( Map<String,String> values, Map<String,String> defaultValues, Log log )
    {
        SettingMigrators.migrateAdvertisedAddressInheritanceChange( values, defaultValues, log, CausalClusteringSettings.transaction_listen_address.name(),
                CausalClusteringSettings.transaction_advertised_address.name() );
        SettingMigrators.migrateAdvertisedAddressInheritanceChange( values, defaultValues, log, CausalClusteringSettings.raft_listen_address.name(),
                CausalClusteringSettings.raft_advertised_address.name() );
        SettingMigrators.migrateAdvertisedAddressInheritanceChange( values, defaultValues, log, CausalClusteringSettings.discovery_listen_address.name(),
                CausalClusteringSettings.discovery_advertised_address.name() );
    }
}
