package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.Inbound;

import java.time.Clock;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.CappedLogger;

public class RaftMessageDispatcher implements Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>
{
    private final Map<RaftId,Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>> handlersById = new ConcurrentHashMap();
    private final CappedLogger log;

    RaftMessageDispatcher( LogProvider logProvider, Clock clock )
    {
        this.log = this.createCappedLogger( logProvider, clock );
    }

    public void handle( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message )
    {
        RaftId id = message.raftId();
        Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> head = (Inbound.MessageHandler) this.handlersById.get( id );
        if ( head == null )
        {
            this.log.warn( "Unable to process message %s because handler for Raft ID %s is not installed", new Object[]{message, id} );
        }
        else
        {
            head.handle( message );
        }
    }

    void registerHandlerChain( RaftId id, Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> head )
    {
        Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> existingHead =
                (Inbound.MessageHandler) this.handlersById.putIfAbsent( id, head );
        if ( existingHead != null )
        {
            throw new IllegalArgumentException( "Handler chain for raft ID " + id + " is already registered" );
        }
    }

    void deregisterHandlerChain( RaftId id )
    {
        this.handlersById.remove( id );
    }

    private CappedLogger createCappedLogger( LogProvider logProvider, Clock clock )
    {
        CappedLogger logger = new CappedLogger( logProvider.getLog( this.getClass() ) );
        logger.setTimeLimit( 5L, TimeUnit.SECONDS, clock );
        return logger;
    }
}
