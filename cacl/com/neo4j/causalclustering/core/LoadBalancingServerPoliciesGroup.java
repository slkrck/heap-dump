package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.routing.load_balancing.filters.Filter;
import com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies.FilterConfigParser;
import com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies.InvalidFilterSpecification;
import com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies.ServerInfo;
import org.neo4j.configuration.SettingValueParser;
import org.neo4j.graphdb.config.Setting;

public final class LoadBalancingServerPoliciesGroup extends LoadBalancingPluginGroup
{
    private static final SettingValueParser<Filter<ServerInfo>> PARSER = new SettingValueParser<Filter<ServerInfo>>()
    {
        public Filter<ServerInfo> parse( String value )
        {
            try
            {
                return FilterConfigParser.parse( value );
            }
            catch ( InvalidFilterSpecification var3 )
            {
                throw new IllegalArgumentException( "Not a valid filter", var3 );
            }
        }

        public String getDescription()
        {
            return "a load-balancing filter";
        }

        public Class<Filter<ServerInfo>> getType()
        {
            return Filter.class;
        }
    };
    public final Setting<Filter<ServerInfo>> value;

    public LoadBalancingServerPoliciesGroup()
    {
        super( (String) null, (String) null );
        this.value = this.getBuilder( PARSER, (Object) null ).build();
    }

    private LoadBalancingServerPoliciesGroup( String name )
    {
        super( name, "server_policies" );
        this.value = this.getBuilder( PARSER, (Object) null ).build();
    }

    public static LoadBalancingServerPoliciesGroup group( String name )
    {
        return new LoadBalancingServerPoliciesGroup( name );
    }
}
