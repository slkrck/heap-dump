package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.core.replication.Replicator;
import com.neo4j.causalclustering.core.state.machines.CoreStateMachines;
import com.neo4j.causalclustering.core.state.machines.lease.ClusterLeaseCoordinator;
import com.neo4j.causalclustering.core.state.machines.tx.ReplicatedTransactionCommitProcess;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.CommitProcessFactory;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionRepresentationCommitProcess;
import org.neo4j.kernel.impl.transaction.log.TransactionAppender;
import org.neo4j.storageengine.api.StorageEngine;

public class CoreCommitProcessFactory implements CommitProcessFactory
{
    private final NamedDatabaseId namedDatabaseId;
    private final Replicator replicator;
    private final CoreStateMachines coreStateMachines;
    private final ClusterLeaseCoordinator leaseCoordinator;

    CoreCommitProcessFactory( NamedDatabaseId namedDatabaseId, Replicator replicator, CoreStateMachines coreStateMachines,
            ClusterLeaseCoordinator leaseCoordinator )
    {
        this.namedDatabaseId = namedDatabaseId;
        this.replicator = replicator;
        this.coreStateMachines = coreStateMachines;
        this.leaseCoordinator = leaseCoordinator;
    }

    public TransactionCommitProcess create( TransactionAppender appender, StorageEngine storageEngine, Config config )
    {
        this.initializeCommitProcessForStateMachines( appender, storageEngine );
        return new ReplicatedTransactionCommitProcess( this.replicator, this.namedDatabaseId, this.leaseCoordinator );
    }

    private void initializeCommitProcessForStateMachines( TransactionAppender appender, StorageEngine storageEngine )
    {
        TransactionRepresentationCommitProcess commitProcess = new TransactionRepresentationCommitProcess( appender, storageEngine );
        this.coreStateMachines.installCommitProcess( commitProcess );
    }
}
