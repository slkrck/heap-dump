package com.neo4j.causalclustering.core.consensus;

public class NoLeaderFoundException extends Exception
{
    public NoLeaderFoundException()
    {
    }

    public NoLeaderFoundException( Throwable cause )
    {
        super( cause );
    }

    public NoLeaderFoundException( String message )
    {
        super( message );
    }

    public NoLeaderFoundException( String message, Throwable cause )
    {
        super( message, cause );
    }
}
