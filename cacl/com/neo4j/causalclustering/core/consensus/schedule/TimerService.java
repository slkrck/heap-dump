package com.neo4j.causalclustering.core.consensus.schedule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class TimerService
{
    protected final JobScheduler scheduler;
    private final Collection<Timer> timers = new ArrayList();
    private final Log log;

    public TimerService( JobScheduler scheduler, LogProvider logProvider )
    {
        this.scheduler = scheduler;
        this.log = logProvider.getLog( this.getClass() );
    }

    public synchronized Timer create( TimerService.TimerName name, Group group, TimeoutHandler handler )
    {
        Timer timer = new Timer( name, this.scheduler, this.log, group, handler );
        this.timers.add( timer );
        return timer;
    }

    public synchronized Collection<Timer> getTimers( TimerService.TimerName name )
    {
        return (Collection) this.timers.stream().filter( ( timer ) -> {
            return timer.name().equals( name );
        } ).collect( Collectors.toList() );
    }

    public synchronized void invoke( TimerService.TimerName name )
    {
        this.getTimers( name ).forEach( Timer::invoke );
    }

    public interface TimerName
    {
        String name();
    }
}
