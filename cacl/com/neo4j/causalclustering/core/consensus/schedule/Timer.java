package com.neo4j.causalclustering.core.consensus.schedule;

import org.neo4j.logging.Log;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

public class Timer
{
    private final TimerService.TimerName name;
    private final JobScheduler scheduler;
    private final Log log;
    private final Group group;
    private final TimeoutHandler handler;
    private Timeout timeout;
    private Delay delay;
    private JobHandle job;
    private long activeJobId;
    private boolean isDead;
    private volatile boolean isRunning;

    Timer( TimerService.TimerName name, JobScheduler scheduler, Log log, Group group, TimeoutHandler handler )
    {
        this.name = name;
        this.scheduler = scheduler;
        this.log = log;
        this.group = group;
        this.handler = handler;
    }

    public synchronized boolean set( Timeout newTimeout )
    {
        if ( this.isDead )
        {
            return false;
        }
        else
        {
            this.delay = newTimeout.next();
            this.timeout = newTimeout;
            long jobId = this.newJobId();
            this.job = this.scheduler.schedule( this.group, () -> {
                this.handle( jobId );
            }, this.delay.amount(), this.delay.unit() );
            return true;
        }
    }

    private long newJobId()
    {
        ++this.activeJobId;
        return this.activeJobId;
    }

    private void handle( long jobId )
    {
        synchronized ( this )
        {
            if ( this.activeJobId != jobId )
            {
                return;
            }

            this.isRunning = true;
        }

        try
        {
            this.handler.onTimeout( this );
        }
        catch ( Throwable var9 )
        {
            this.log.error( String.format( "[%s] Handler threw exception", this.canonicalName() ), var9 );
        }
        finally
        {
            this.isRunning = false;
        }
    }

    public synchronized boolean reset()
    {
        if ( this.timeout == null )
        {
            throw new IllegalStateException( "You can't reset until you have set a timeout" );
        }
        else
        {
            return this.set( this.timeout );
        }
    }

    public void cancel( Timer.CancelMode cancelMode )
    {
        JobHandle job;
        synchronized ( this )
        {
            ++this.activeJobId;
            job = this.isRunning ? this.job : null;
        }

        if ( job != null )
        {
            try
            {
                if ( cancelMode == Timer.CancelMode.SYNC_WAIT )
                {
                    job.waitTermination();
                }
                else if ( cancelMode == Timer.CancelMode.ASYNC_INTERRUPT )
                {
                    job.cancel();
                }
            }
            catch ( Exception var5 )
            {
                this.log.warn( String.format( "[%s] Cancelling timer threw exception", this.canonicalName() ), var5 );
            }
        }
    }

    public void kill( Timer.CancelMode cancelMode )
    {
        synchronized ( this )
        {
            this.isDead = true;
        }

        this.cancel( cancelMode );
    }

    public synchronized void invoke()
    {
        long jobId = this.newJobId();
        this.job = this.scheduler.schedule( this.group, () -> {
            this.handle( jobId );
        } );
    }

    synchronized Delay delay()
    {
        return this.delay;
    }

    public TimerService.TimerName name()
    {
        return this.name;
    }

    private String canonicalName()
    {
        String var10000 = this.name.getClass().getCanonicalName();
        return var10000 + "." + this.name.name();
    }

    public static enum CancelMode
    {
        ASYNC,
        ASYNC_INTERRUPT,
        SYNC_WAIT;
    }
}
