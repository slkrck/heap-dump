package com.neo4j.causalclustering.core.consensus.schedule;

import java.util.concurrent.TimeUnit;

public class TimeoutFactory
{
    public static Timeout fixedTimeout( long delay, TimeUnit unit )
    {
        return new FixedTimeout( delay, unit );
    }

    public static Timeout uniformRandomTimeout( long minDelay, long maxDelay, TimeUnit unit )
    {
        return new UniformRandomTimeout( minDelay, maxDelay, unit );
    }
}
