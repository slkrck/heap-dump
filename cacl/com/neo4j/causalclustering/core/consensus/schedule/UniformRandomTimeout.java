package com.neo4j.causalclustering.core.consensus.schedule;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class UniformRandomTimeout implements Timeout
{
    private final long minDelay;
    private final long maxDelay;
    private final TimeUnit unit;

    UniformRandomTimeout( long minDelay, long maxDelay, TimeUnit unit )
    {
        this.minDelay = minDelay;
        this.maxDelay = maxDelay;
        this.unit = unit;
    }

    public Delay next()
    {
        long delay = ThreadLocalRandom.current().nextLong( this.minDelay, this.maxDelay + 1L );
        return new Delay( delay, this.unit );
    }
}
