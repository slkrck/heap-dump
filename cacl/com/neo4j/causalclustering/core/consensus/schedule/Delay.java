package com.neo4j.causalclustering.core.consensus.schedule;

import java.util.concurrent.TimeUnit;

public class Delay
{
    private final long amount;
    private final TimeUnit unit;

    Delay( long amount, TimeUnit unit )
    {
        this.amount = amount;
        this.unit = unit;
    }

    public long amount()
    {
        return this.amount;
    }

    public TimeUnit unit()
    {
        return this.unit;
    }
}
