package com.neo4j.causalclustering.core.consensus.schedule;

import java.util.concurrent.TimeUnit;

public class FixedTimeout implements Timeout
{
    private final Delay delay;

    FixedTimeout( long amount, TimeUnit unit )
    {
        this.delay = new Delay( amount, unit );
    }

    public Delay next()
    {
        return this.delay;
    }
}
