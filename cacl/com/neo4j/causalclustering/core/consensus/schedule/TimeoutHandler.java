package com.neo4j.causalclustering.core.consensus.schedule;

@FunctionalInterface
public interface TimeoutHandler
{
    void onTimeout( Timer var1 ) throws Exception;
}
