package com.neo4j.causalclustering.core.consensus.schedule;

public interface Timeout
{
    Delay next();
}
