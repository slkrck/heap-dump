package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.ComposableMessageHandler;
import com.neo4j.causalclustering.messaging.LifecycleMessageHandler;

import java.util.function.LongSupplier;

public class LeaderAvailabilityHandler implements LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>
{
    private final LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler;
    private final LeaderAvailabilityTimers leaderAvailabilityTimers;
    private final LeaderAvailabilityHandler.ShouldRenewElectionTimeout shouldRenewElectionTimeout;
    private final RaftMessageTimerResetMonitor raftMessageTimerResetMonitor;

    public LeaderAvailabilityHandler( LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler,
            LeaderAvailabilityTimers leaderAvailabilityTimers, RaftMessageTimerResetMonitor raftMessageTimerResetMonitor, LongSupplier term )
    {
        this.delegateHandler = delegateHandler;
        this.leaderAvailabilityTimers = leaderAvailabilityTimers;
        this.shouldRenewElectionTimeout = new LeaderAvailabilityHandler.ShouldRenewElectionTimeout( term );
        this.raftMessageTimerResetMonitor = raftMessageTimerResetMonitor;
    }

    public static ComposableMessageHandler composable( LeaderAvailabilityTimers leaderAvailabilityTimers,
            RaftMessageTimerResetMonitor raftMessageTimerResetMonitor, LongSupplier term )
    {
        return ( delegate ) -> {
            return new LeaderAvailabilityHandler( delegate, leaderAvailabilityTimers, raftMessageTimerResetMonitor, term );
        };
    }

    public synchronized void start( RaftId raftId ) throws Exception
    {
        this.delegateHandler.start( raftId );
    }

    public synchronized void stop() throws Exception
    {
        this.delegateHandler.stop();
    }

    public void handle( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message )
    {
        this.handleTimeouts( message );
        this.delegateHandler.handle( message );
    }

    private void handleTimeouts( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message )
    {
        if ( (Boolean) message.dispatch( this.shouldRenewElectionTimeout ) )
        {
            this.raftMessageTimerResetMonitor.timerReset();
            this.leaderAvailabilityTimers.renewElection();
        }
    }

    private static class ShouldRenewElectionTimeout implements RaftMessages.Handler<Boolean,RuntimeException>
    {
        private final LongSupplier term;

        private ShouldRenewElectionTimeout( LongSupplier term )
        {
            this.term = term;
        }

        public Boolean handle( RaftMessages.AppendEntries.Request request )
        {
            return request.leaderTerm() >= this.term.getAsLong();
        }

        public Boolean handle( RaftMessages.Heartbeat heartbeat )
        {
            return heartbeat.leaderTerm() >= this.term.getAsLong();
        }

        public Boolean handle( RaftMessages.Vote.Request request )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.Vote.Response response )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.PreVote.Request request )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.PreVote.Response response )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.AppendEntries.Response response )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.LogCompactionInfo logCompactionInfo )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.HeartbeatResponse heartbeatResponse )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.Timeout.Election election )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.Timeout.Heartbeat heartbeat )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.NewEntry.Request request )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.NewEntry.BatchRequest batchRequest )
        {
            return Boolean.FALSE;
        }

        public Boolean handle( RaftMessages.PruneRequest pruneRequest )
        {
            return Boolean.FALSE;
        }
    }
}
