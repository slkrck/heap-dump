package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;

import java.io.IOException;

public class NewLeaderBarrier implements ReplicatedContent
{
    public String toString()
    {
        return "NewLeaderBarrier";
    }

    public int hashCode()
    {
        return 1;
    }

    public boolean equals( Object obj )
    {
        return obj instanceof NewLeaderBarrier;
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
    }
}
