package com.neo4j.causalclustering.core.consensus;

import java.time.Duration;

public interface RaftMessageProcessingMonitor
{
    void setDelay( Duration var1 );

    void updateTimer( RaftMessages.Type var1, Duration var2 );
}
