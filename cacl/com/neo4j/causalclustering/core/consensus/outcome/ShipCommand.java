package com.neo4j.causalclustering.core.consensus.outcome;

import com.neo4j.causalclustering.core.consensus.LeaderContext;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.shipping.RaftLogShipper;

import java.util.Arrays;

public abstract class ShipCommand
{
    public abstract void applyTo( RaftLogShipper var1, LeaderContext var2 );

    public static class CommitUpdate extends ShipCommand
    {
        public void applyTo( RaftLogShipper raftLogShipper, LeaderContext leaderContext )
        {
            raftLogShipper.onCommitUpdate( leaderContext );
        }

        public String toString()
        {
            return "CommitUpdate{}";
        }
    }

    public static class NewEntries extends ShipCommand
    {
        private final long prevLogIndex;
        private final long prevLogTerm;
        private final RaftLogEntry[] newLogEntries;

        public NewEntries( long prevLogIndex, long prevLogTerm, RaftLogEntry[] newLogEntries )
        {
            this.prevLogIndex = prevLogIndex;
            this.prevLogTerm = prevLogTerm;
            this.newLogEntries = newLogEntries;
        }

        public void applyTo( RaftLogShipper raftLogShipper, LeaderContext leaderContext )
        {
            raftLogShipper.onNewEntries( this.prevLogIndex, this.prevLogTerm, this.newLogEntries, leaderContext );
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                ShipCommand.NewEntries newEntries = (ShipCommand.NewEntries) o;
                if ( this.prevLogIndex != newEntries.prevLogIndex )
                {
                    return false;
                }
                else
                {
                    return this.prevLogTerm != newEntries.prevLogTerm ? false : Arrays.equals( this.newLogEntries, newEntries.newLogEntries );
                }
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            int result = (int) (this.prevLogIndex ^ this.prevLogIndex >>> 32);
            result = 31 * result + (int) (this.prevLogTerm ^ this.prevLogTerm >>> 32);
            result = 31 * result + Arrays.hashCode( this.newLogEntries );
            return result;
        }

        public String toString()
        {
            return String.format( "NewEntry{prevLogIndex=%d, prevLogTerm=%d, newLogEntry=%s}", this.prevLogIndex, this.prevLogTerm,
                    Arrays.toString( this.newLogEntries ) );
        }
    }

    public static class Match extends ShipCommand
    {
        private final long newMatchIndex;
        private final Object target;

        public Match( long newMatchIndex, Object target )
        {
            this.newMatchIndex = newMatchIndex;
            this.target = target;
        }

        public void applyTo( RaftLogShipper raftLogShipper, LeaderContext leaderContext )
        {
            if ( raftLogShipper.identity().equals( this.target ) )
            {
                raftLogShipper.onMatch( this.newMatchIndex, leaderContext );
            }
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                ShipCommand.Match match = (ShipCommand.Match) o;
                return this.newMatchIndex != match.newMatchIndex ? false : this.target.equals( match.target );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            int result = (int) (this.newMatchIndex ^ this.newMatchIndex >>> 32);
            result = 31 * result + this.target.hashCode();
            return result;
        }

        public String toString()
        {
            return String.format( "Match{newMatchIndex=%d, target=%s}", this.newMatchIndex, this.target );
        }
    }

    public static class Mismatch extends ShipCommand
    {
        private final long lastRemoteAppendIndex;
        private final Object target;

        public Mismatch( long lastRemoteAppendIndex, Object target )
        {
            this.lastRemoteAppendIndex = lastRemoteAppendIndex;
            this.target = target;
        }

        public void applyTo( RaftLogShipper raftLogShipper, LeaderContext leaderContext )
        {
            if ( raftLogShipper.identity().equals( this.target ) )
            {
                raftLogShipper.onMismatch( this.lastRemoteAppendIndex, leaderContext );
            }
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                ShipCommand.Mismatch mismatch = (ShipCommand.Mismatch) o;
                return this.lastRemoteAppendIndex != mismatch.lastRemoteAppendIndex ? false : this.target.equals( mismatch.target );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            int result = (int) (this.lastRemoteAppendIndex ^ this.lastRemoteAppendIndex >>> 32);
            result = 31 * result + this.target.hashCode();
            return result;
        }

        public String toString()
        {
            return String.format( "Mismatch{lastRemoteAppendIndex=%d, target=%s}", this.lastRemoteAppendIndex, this.target );
        }
    }
}
