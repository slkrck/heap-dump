package com.neo4j.causalclustering.core.consensus.outcome;

import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;

import java.io.IOException;

import org.neo4j.logging.Log;

public interface RaftLogCommand
{
    void dispatch( RaftLogCommand.Handler var1 ) throws IOException;

    void applyTo( RaftLog var1, Log var2 ) throws IOException;

    void applyTo( InFlightCache var1, Log var2 );

    public interface Handler
    {
        void append( long var1, RaftLogEntry... var3 ) throws IOException;

        void truncate( long var1 ) throws IOException;

        void prune( long var1 );
    }
}
