package com.neo4j.causalclustering.core.consensus.outcome;

public class SnapshotRequirement
{
    private final long leaderPrevIndex;
    private final long localAppendIndex;

    SnapshotRequirement( long leaderPrevIndex, long localAppendIndex )
    {
        this.leaderPrevIndex = leaderPrevIndex;
        this.localAppendIndex = localAppendIndex;
    }

    public long leaderPrevIndex()
    {
        return this.leaderPrevIndex;
    }

    public long localAppendIndex()
    {
        return this.localAppendIndex;
    }

    public String toString()
    {
        return "SnapshotRequirement{leaderPrevIndex=" + this.leaderPrevIndex + ", localAppendIndex=" + this.localAppendIndex + "}";
    }
}
