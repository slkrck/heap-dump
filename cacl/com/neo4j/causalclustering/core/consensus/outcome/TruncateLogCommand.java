package com.neo4j.causalclustering.core.consensus.outcome;

import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;

import java.io.IOException;
import java.util.Objects;

import org.neo4j.logging.Log;

public class TruncateLogCommand implements RaftLogCommand
{
    public final long fromIndex;

    public TruncateLogCommand( long fromIndex )
    {
        this.fromIndex = fromIndex;
    }

    public void dispatch( RaftLogCommand.Handler handler ) throws IOException
    {
        handler.truncate( this.fromIndex );
    }

    public void applyTo( RaftLog raftLog, Log log ) throws IOException
    {
        raftLog.truncate( this.fromIndex );
    }

    public void applyTo( InFlightCache inFlightCache, Log log )
    {
        log.debug( "Start truncating in-flight-map from index %d. Current map:%n%s", new Object[]{this.fromIndex, inFlightCache} );
        inFlightCache.truncate( this.fromIndex );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            TruncateLogCommand that = (TruncateLogCommand) o;
            return this.fromIndex == that.fromIndex;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.fromIndex} );
    }

    public String toString()
    {
        return "TruncateLogCommand{fromIndex=" + this.fromIndex + "}";
    }
}
