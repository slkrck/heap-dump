package com.neo4j.causalclustering.core.consensus.outcome;

import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;

import java.io.IOException;
import java.util.Objects;

import org.neo4j.logging.Log;

public class AppendLogEntry implements RaftLogCommand
{
    public final long index;
    public final RaftLogEntry entry;

    public AppendLogEntry( long index, RaftLogEntry entry )
    {
        this.index = index;
        this.entry = entry;
    }

    public void applyTo( RaftLog raftLog, Log log ) throws IOException
    {
        if ( this.index <= raftLog.appendIndex() )
        {
            throw new IllegalStateException( "Attempted to append over an existing entry at index " + this.index );
        }
        else
        {
            raftLog.append( this.entry );
        }
    }

    public void applyTo( InFlightCache inFlightCache, Log log )
    {
        inFlightCache.put( this.index, this.entry );
    }

    public void dispatch( RaftLogCommand.Handler handler ) throws IOException
    {
        handler.append( this.index, this.entry );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            AppendLogEntry that = (AppendLogEntry) o;
            return this.index == that.index && Objects.equals( this.entry, that.entry );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.index, this.entry} );
    }

    public String toString()
    {
        return "AppendLogEntry{index=" + this.index + ", entry=" + this.entry + "}";
    }
}
