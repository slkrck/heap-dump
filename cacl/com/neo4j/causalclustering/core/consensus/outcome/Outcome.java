package com.neo4j.causalclustering.core.consensus.outcome;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.roles.Role;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerStates;
import com.neo4j.causalclustering.core.consensus.state.ReadableRaftState;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Message;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.Set;

public class Outcome implements Message, ConsensusOutcome
{
    private Role nextRole;
    private long term;
    private MemberId leader;
    private long leaderCommit;
    private Collection<RaftLogCommand> logCommands = new ArrayList();
    private Collection<RaftMessages.Directed> outgoingMessages = new ArrayList();
    private long commitIndex;
    private MemberId votedFor;
    private boolean renewElectionTimeout;
    private SnapshotRequirement snapshotRequirement;
    private boolean isPreElection;
    private Set<MemberId> preVotesForMe;
    private Set<MemberId> votesForMe;
    private long lastLogIndexBeforeWeBecameLeader;
    private FollowerStates<MemberId> followerStates;
    private Collection<ShipCommand> shipCommands = new ArrayList();
    private boolean electedLeader;
    private OptionalLong steppingDownInTerm;
    private Set<MemberId> heartbeatResponses;

    public Outcome( Role currentRole, ReadableRaftState ctx )
    {
        this.defaults( currentRole, ctx );
    }

    public Outcome( Role nextRole, long term, MemberId leader, long leaderCommit, MemberId votedFor, Set<MemberId> votesForMe, Set<MemberId> preVotesForMe,
            long lastLogIndexBeforeWeBecameLeader, FollowerStates<MemberId> followerStates, boolean renewElectionTimeout,
            Collection<RaftLogCommand> logCommands, Collection<RaftMessages.Directed> outgoingMessages, Collection<ShipCommand> shipCommands, long commitIndex,
            Set<MemberId> heartbeatResponses, boolean isPreElection )
    {
        this.nextRole = nextRole;
        this.term = term;
        this.leader = leader;
        this.leaderCommit = leaderCommit;
        this.votedFor = votedFor;
        this.votesForMe = new HashSet( votesForMe );
        this.preVotesForMe = new HashSet( preVotesForMe );
        this.lastLogIndexBeforeWeBecameLeader = lastLogIndexBeforeWeBecameLeader;
        this.followerStates = followerStates;
        this.renewElectionTimeout = renewElectionTimeout;
        this.heartbeatResponses = new HashSet( heartbeatResponses );
        this.logCommands.addAll( logCommands );
        this.outgoingMessages.addAll( outgoingMessages );
        this.shipCommands.addAll( shipCommands );
        this.commitIndex = commitIndex;
        this.isPreElection = isPreElection;
        this.steppingDownInTerm = OptionalLong.empty();
    }

    private void defaults( Role currentRole, ReadableRaftState ctx )
    {
        this.nextRole = currentRole;
        this.term = ctx.term();
        this.leader = ctx.leader();
        this.leaderCommit = ctx.leaderCommit();
        this.votedFor = ctx.votedFor();
        this.renewElectionTimeout = false;
        this.isPreElection = currentRole == Role.FOLLOWER && ctx.isPreElection();
        this.steppingDownInTerm = OptionalLong.empty();
        this.preVotesForMe = (Set) (this.isPreElection ? new HashSet( ctx.preVotesForMe() ) : Collections.emptySet());
        this.votesForMe = (Set) (currentRole == Role.CANDIDATE ? new HashSet( ctx.votesForMe() ) : Collections.emptySet());
        this.heartbeatResponses = (Set) (currentRole == Role.LEADER ? new HashSet( ctx.heartbeatResponses() ) : Collections.emptySet());
        this.lastLogIndexBeforeWeBecameLeader = currentRole == Role.LEADER ? ctx.lastLogIndexBeforeWeBecameLeader() : -1L;
        this.followerStates = currentRole == Role.LEADER ? ctx.followerStates() : new FollowerStates();
        this.commitIndex = ctx.commitIndex();
    }

    public void setNextRole( Role nextRole )
    {
        this.nextRole = nextRole;
    }

    public void setNextTerm( long nextTerm )
    {
        this.term = nextTerm;
    }

    public void addLogCommand( RaftLogCommand logCommand )
    {
        this.logCommands.add( logCommand );
    }

    public void addOutgoingMessage( RaftMessages.Directed message )
    {
        this.outgoingMessages.add( message );
    }

    public void renewElectionTimeout()
    {
        this.renewElectionTimeout = true;
    }

    public void markNeedForFreshSnapshot( long leaderPrevIndex, long localAppendIndex )
    {
        this.snapshotRequirement = new SnapshotRequirement( leaderPrevIndex, localAppendIndex );
    }

    public void addVoteForMe( MemberId voteFrom )
    {
        this.votesForMe.add( voteFrom );
    }

    public void replaceFollowerStates( FollowerStates<MemberId> followerStates )
    {
        this.followerStates = followerStates;
    }

    public void addShipCommand( ShipCommand shipCommand )
    {
        this.shipCommands.add( shipCommand );
    }

    public void electedLeader()
    {
        assert !this.isSteppingDown();

        this.electedLeader = true;
    }

    public void steppingDown( long stepDownTerm )
    {
        assert !this.electedLeader;

        this.steppingDownInTerm = OptionalLong.of( stepDownTerm );
    }

    public String toString()
    {
        return "Outcome{nextRole=" + this.nextRole + ", term=" + this.term + ", leader=" + this.leader + ", leaderCommit=" + this.leaderCommit +
                ", logCommands=" + this.logCommands + ", outgoingMessages=" + this.outgoingMessages + ", commitIndex=" + this.commitIndex + ", votedFor=" +
                this.votedFor + ", renewElectionTimeout=" + this.renewElectionTimeout + ", snapshotRequirement=" + this.snapshotRequirement + ", votesForMe=" +
                this.votesForMe + ", preVotesForMe=" + this.preVotesForMe + ", lastLogIndexBeforeWeBecameLeader=" + this.lastLogIndexBeforeWeBecameLeader +
                ", followerStates=" + this.followerStates + ", shipCommands=" + this.shipCommands + ", electedLeader=" + this.electedLeader +
                ", steppingDownInTerm=" + this.steppingDownInTerm + "}";
    }

    public Role getRole()
    {
        return this.nextRole;
    }

    public long getTerm()
    {
        return this.term;
    }

    public MemberId getLeader()
    {
        return this.leader;
    }

    public void setLeader( MemberId leader )
    {
        this.leader = leader;
    }

    public long getLeaderCommit()
    {
        return this.leaderCommit;
    }

    public void setLeaderCommit( long leaderCommit )
    {
        this.leaderCommit = leaderCommit;
    }

    public Collection<RaftLogCommand> getLogCommands()
    {
        return this.logCommands;
    }

    public Collection<RaftMessages.Directed> getOutgoingMessages()
    {
        return this.outgoingMessages;
    }

    public MemberId getVotedFor()
    {
        return this.votedFor;
    }

    public void setVotedFor( MemberId votedFor )
    {
        this.votedFor = votedFor;
    }

    public boolean electionTimeoutRenewed()
    {
        return this.renewElectionTimeout;
    }

    public Optional<SnapshotRequirement> snapshotRequirement()
    {
        return Optional.ofNullable( this.snapshotRequirement );
    }

    public Set<MemberId> getVotesForMe()
    {
        return this.votesForMe;
    }

    public long getLastLogIndexBeforeWeBecameLeader()
    {
        return this.lastLogIndexBeforeWeBecameLeader;
    }

    public void setLastLogIndexBeforeWeBecameLeader( long lastLogIndexBeforeWeBecameLeader )
    {
        this.lastLogIndexBeforeWeBecameLeader = lastLogIndexBeforeWeBecameLeader;
    }

    public FollowerStates<MemberId> getFollowerStates()
    {
        return this.followerStates;
    }

    public Collection<ShipCommand> getShipCommands()
    {
        return this.shipCommands;
    }

    public boolean isElectedLeader()
    {
        return this.electedLeader;
    }

    public boolean isSteppingDown()
    {
        return this.steppingDownInTerm.isPresent();
    }

    public OptionalLong stepDownTerm()
    {
        return this.steppingDownInTerm;
    }

    public long getCommitIndex()
    {
        return this.commitIndex;
    }

    public void setCommitIndex( long commitIndex )
    {
        this.commitIndex = commitIndex;
    }

    public void addHeartbeatResponse( MemberId from )
    {
        this.heartbeatResponses.add( from );
    }

    public Set<MemberId> getHeartbeatResponses()
    {
        return this.heartbeatResponses;
    }

    public boolean isPreElection()
    {
        return this.isPreElection;
    }

    public void setPreElection( boolean isPreElection )
    {
        this.isPreElection = isPreElection;
    }

    public void addPreVoteForMe( MemberId from )
    {
        this.preVotesForMe.add( from );
    }

    public Set<MemberId> getPreVotesForMe()
    {
        return this.preVotesForMe;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            Outcome outcome = (Outcome) o;
            return this.term == outcome.term && this.leaderCommit == outcome.leaderCommit && this.commitIndex == outcome.commitIndex &&
                    this.renewElectionTimeout == outcome.renewElectionTimeout && Objects.equals( this.snapshotRequirement, outcome.snapshotRequirement ) &&
                    this.isPreElection == outcome.isPreElection && this.lastLogIndexBeforeWeBecameLeader == outcome.lastLogIndexBeforeWeBecameLeader &&
                    this.electedLeader == outcome.electedLeader && this.nextRole == outcome.nextRole &&
                    Objects.equals( this.steppingDownInTerm, outcome.steppingDownInTerm ) && Objects.equals( this.leader, outcome.leader ) &&
                    Objects.equals( this.logCommands, outcome.logCommands ) && Objects.equals( this.outgoingMessages, outcome.outgoingMessages ) &&
                    Objects.equals( this.votedFor, outcome.votedFor ) && Objects.equals( this.preVotesForMe, outcome.preVotesForMe ) &&
                    Objects.equals( this.votesForMe, outcome.votesForMe ) && Objects.equals( this.followerStates, outcome.followerStates ) &&
                    Objects.equals( this.shipCommands, outcome.shipCommands ) && Objects.equals( this.heartbeatResponses, outcome.heartbeatResponses );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash(
                new Object[]{this.nextRole, this.term, this.leader, this.leaderCommit, this.logCommands, this.outgoingMessages, this.commitIndex, this.votedFor,
                        this.renewElectionTimeout, this.snapshotRequirement, this.isPreElection, this.preVotesForMe, this.votesForMe,
                        this.lastLogIndexBeforeWeBecameLeader, this.followerStates, this.shipCommands, this.electedLeader, this.steppingDownInTerm,
                        this.heartbeatResponses} );
    }
}
