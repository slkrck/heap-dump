package com.neo4j.causalclustering.core.consensus.outcome;

import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import org.neo4j.logging.Log;

public class BatchAppendLogEntries implements RaftLogCommand
{
    public final long baseIndex;
    public final int offset;
    public final RaftLogEntry[] entries;

    public BatchAppendLogEntries( long baseIndex, int offset, RaftLogEntry[] entries )
    {
        this.baseIndex = baseIndex;
        this.offset = offset;
        this.entries = entries;
    }

    public void dispatch( RaftLogCommand.Handler handler ) throws IOException
    {
        handler.append( this.baseIndex + (long) this.offset, (RaftLogEntry[]) Arrays.copyOfRange( this.entries, this.offset, this.entries.length ) );
    }

    public void applyTo( RaftLog raftLog, Log log ) throws IOException
    {
        long lastIndex = this.baseIndex + (long) this.offset;
        if ( lastIndex <= raftLog.appendIndex() )
        {
            throw new IllegalStateException( "Attempted to append over an existing entry starting at index " + lastIndex );
        }
        else
        {
            raftLog.append( (RaftLogEntry[]) Arrays.copyOfRange( this.entries, this.offset, this.entries.length ) );
        }
    }

    public void applyTo( InFlightCache inFlightCache, Log log )
    {
        for ( int i = this.offset; i < this.entries.length; ++i )
        {
            inFlightCache.put( this.baseIndex + (long) i, this.entries[i] );
        }
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            BatchAppendLogEntries that = (BatchAppendLogEntries) o;
            return this.baseIndex == that.baseIndex && this.offset == that.offset && Arrays.equals( this.entries, that.entries );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.baseIndex, this.offset, Arrays.hashCode( this.entries )} );
    }

    public String toString()
    {
        return String.format( "BatchAppendLogEntries{baseIndex=%d, offset=%d, entries=%s}", this.baseIndex, this.offset, Arrays.toString( this.entries ) );
    }
}
