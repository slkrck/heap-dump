package com.neo4j.causalclustering.core.consensus.outcome;

import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;

import java.io.IOException;
import java.util.Objects;

import org.neo4j.logging.Log;

public class PruneLogCommand implements RaftLogCommand
{
    private final long pruneIndex;

    public PruneLogCommand( long pruneIndex )
    {
        this.pruneIndex = pruneIndex;
    }

    public void dispatch( RaftLogCommand.Handler handler )
    {
        handler.prune( this.pruneIndex );
    }

    public void applyTo( RaftLog raftLog, Log log ) throws IOException
    {
        raftLog.prune( this.pruneIndex );
    }

    public void applyTo( InFlightCache inFlightCache, Log log )
    {
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            PruneLogCommand that = (PruneLogCommand) o;
            return this.pruneIndex == that.pruneIndex;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.pruneIndex} );
    }

    public String toString()
    {
        return "PruneLogCommand{pruneIndex=" + this.pruneIndex + "}";
    }
}
