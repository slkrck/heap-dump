package com.neo4j.causalclustering.core.consensus.outcome;

import java.util.Optional;

public interface ConsensusOutcome
{
    Optional<SnapshotRequirement> snapshotRequirement();

    long getCommitIndex();
}
