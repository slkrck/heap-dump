package com.neo4j.causalclustering.core.consensus;

public interface LeaderLocator
{
    LeaderInfo getLeaderInfo() throws NoLeaderFoundException;

    void registerListener( LeaderListener var1 );

    void unregisterListener( LeaderListener var1 );
}
