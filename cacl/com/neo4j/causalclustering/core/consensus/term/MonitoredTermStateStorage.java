package com.neo4j.causalclustering.core.consensus.term;

import com.neo4j.causalclustering.core.consensus.log.monitoring.RaftTermMonitor;
import com.neo4j.causalclustering.core.state.storage.StateStorage;

import java.io.IOException;

import org.neo4j.monitoring.Monitors;

public class MonitoredTermStateStorage implements StateStorage<TermState>
{
    private static final String TERM_TAG = "term";
    private final StateStorage<TermState> delegate;
    private final RaftTermMonitor termMonitor;

    public MonitoredTermStateStorage( StateStorage<TermState> delegate, Monitors monitors )
    {
        this.delegate = delegate;
        this.termMonitor = (RaftTermMonitor) monitors.newMonitor( RaftTermMonitor.class, new String[]{this.getClass().getName(), "term"} );
    }

    public TermState getInitialState()
    {
        return (TermState) this.delegate.getInitialState();
    }

    public void writeState( TermState state ) throws IOException
    {
        this.delegate.writeState( state );
        this.termMonitor.term( state.currentTerm() );
    }

    public boolean exists()
    {
        return this.delegate.exists();
    }
}
