package com.neo4j.causalclustering.core.consensus.term;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class TermState
{
    private volatile long term;

    public TermState()
    {
    }

    private TermState( long term )
    {
        this.term = term;
    }

    public long currentTerm()
    {
        return this.term;
    }

    public boolean update( long newTerm )
    {
        this.failIfInvalid( newTerm );
        boolean changed = this.term != newTerm;
        this.term = newTerm;
        return changed;
    }

    private void failIfInvalid( long newTerm )
    {
        if ( newTerm < this.term )
        {
            throw new IllegalArgumentException( "Cannot move to a lower term" );
        }
    }

    public String toString()
    {
        return "TermState{term=" + this.term + "}";
    }

    public static class Marshal extends SafeStateMarshal<TermState>
    {
        public void marshal( TermState termState, WritableChannel channel ) throws IOException
        {
            channel.putLong( termState.currentTerm() );
        }

        protected TermState unmarshal0( ReadableChannel channel ) throws IOException
        {
            return new TermState( channel.getLong() );
        }

        public TermState startState()
        {
            return new TermState();
        }

        public long ordinal( TermState state )
        {
            return state.currentTerm();
        }
    }
}
