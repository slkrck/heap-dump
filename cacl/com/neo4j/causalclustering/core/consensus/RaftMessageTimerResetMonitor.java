package com.neo4j.causalclustering.core.consensus;

public interface RaftMessageTimerResetMonitor
{
    void timerReset();
}
