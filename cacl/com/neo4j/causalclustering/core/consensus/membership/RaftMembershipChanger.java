package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.roles.Role;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerStates;
import com.neo4j.causalclustering.identity.MemberId;

import java.time.Clock;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class RaftMembershipChanger
{
    private final Log log;
    private final ReadableRaftLog raftLog;
    private final Clock clock;
    private final long electionTimeout;
    private final RaftMembershipManager membershipManager;
    public RaftMembershipStateMachineEventHandler state = new RaftMembershipChanger.Inactive();
    private long catchupTimeout;
    private MemberId catchingUpMember;

    RaftMembershipChanger( ReadableRaftLog raftLog, Clock clock, long electionTimeout, LogProvider logProvider, long catchupTimeout,
            RaftMembershipManager membershipManager )
    {
        this.raftLog = raftLog;
        this.clock = clock;
        this.electionTimeout = electionTimeout;
        this.catchupTimeout = catchupTimeout;
        this.membershipManager = membershipManager;
        this.log = logProvider.getLog( this.getClass() );
    }

    private synchronized void handleState( RaftMembershipStateMachineEventHandler newState )
    {
        RaftMembershipStateMachineEventHandler oldState = this.state;
        this.state = newState;
        if ( oldState != newState )
        {
            oldState.onExit();
            newState.onEntry();
            this.log.info( newState.toString() );
            this.membershipManager.stateChanged();
        }
    }

    void onRole( Role role )
    {
        this.handleState( this.state.onRole( role ) );
    }

    void onRaftGroupCommitted()
    {
        this.handleState( this.state.onRaftGroupCommitted() );
    }

    void onFollowerStateChange( FollowerStates<MemberId> followerStates )
    {
        this.handleState( this.state.onFollowerStateChange( followerStates ) );
    }

    void onMissingMember( MemberId member )
    {
        this.handleState( this.state.onMissingMember( member ) );
    }

    void onSuperfluousMember( MemberId member )
    {
        this.handleState( this.state.onSuperfluousMember( member ) );
    }

    void onTargetChanged( Set<MemberId> targetMembers )
    {
        this.handleState( this.state.onTargetChanged( targetMembers ) );
    }

    private class ConsensusInProgress extends RaftMembershipChanger.ActiveBaseState
    {
        private ConsensusInProgress()
        {
            super();
        }

        public RaftMembershipStateMachineEventHandler onRaftGroupCommitted()
        {
            return RaftMembershipChanger.this.new Idle();
        }

        public void onEntry()
        {
        }

        public void onExit()
        {
            RaftMembershipChanger.this.membershipManager.removeAdditionalReplicationMember( RaftMembershipChanger.this.catchingUpMember );
            RaftMembershipChanger.this.log.info( "Removing replication member: " + RaftMembershipChanger.this.catchingUpMember );
        }

        public String toString()
        {
            return "ConsensusInProgress{}";
        }
    }

    private class CatchingUp extends RaftMembershipChanger.ActiveBaseState
    {
        private final CatchupGoalTracker catchupGoalTracker;
        boolean movingToConsensus;

        CatchingUp( MemberId member )
        {
            super();
            this.catchupGoalTracker =
                    new CatchupGoalTracker( RaftMembershipChanger.this.raftLog, RaftMembershipChanger.this.clock, RaftMembershipChanger.this.electionTimeout,
                            RaftMembershipChanger.this.catchupTimeout );
            RaftMembershipChanger.this.catchingUpMember = member;
        }

        public void onEntry()
        {
            RaftMembershipChanger.this.membershipManager.addAdditionalReplicationMember( RaftMembershipChanger.this.catchingUpMember );
            RaftMembershipChanger.this.log.info( "Adding replication member: " + RaftMembershipChanger.this.catchingUpMember );
        }

        public void onExit()
        {
            if ( !this.movingToConsensus )
            {
                RaftMembershipChanger.this.membershipManager.removeAdditionalReplicationMember( RaftMembershipChanger.this.catchingUpMember );
                RaftMembershipChanger.this.log.info( "Removing replication member: " + RaftMembershipChanger.this.catchingUpMember );
            }
        }

        public RaftMembershipStateMachineEventHandler onRole( Role role )
        {
            return (RaftMembershipStateMachineEventHandler) (role != Role.LEADER ? RaftMembershipChanger.this.new Inactive() : this);
        }

        public RaftMembershipStateMachineEventHandler onFollowerStateChange( FollowerStates<MemberId> followerStates )
        {
            this.catchupGoalTracker.updateProgress( followerStates.get( RaftMembershipChanger.this.catchingUpMember ) );
            if ( this.catchupGoalTracker.isFinished() )
            {
                if ( this.catchupGoalTracker.isGoalAchieved() )
                {
                    Set<MemberId> updatedVotingMembers = new HashSet( RaftMembershipChanger.this.membershipManager.votingMembers() );
                    updatedVotingMembers.add( RaftMembershipChanger.this.catchingUpMember );
                    RaftMembershipChanger.this.membershipManager.doConsensus( updatedVotingMembers );
                    this.movingToConsensus = true;
                    return RaftMembershipChanger.this.new ConsensusInProgress();
                }
                else
                {
                    return RaftMembershipChanger.this.new Idle();
                }
            }
            else
            {
                return this;
            }
        }

        public RaftMembershipStateMachineEventHandler onTargetChanged( Set targetMembers )
        {
            return (RaftMembershipStateMachineEventHandler) (!targetMembers.contains( RaftMembershipChanger.this.catchingUpMember )
                                                             ? RaftMembershipChanger.this.new Idle() : this);
        }

        public String toString()
        {
            return String.format( "CatchingUp{catchupGoalTracker=%s, catchingUpMember=%s}", this.catchupGoalTracker,
                    RaftMembershipChanger.this.catchingUpMember );
        }
    }

    private class Idle extends RaftMembershipChanger.ActiveBaseState
    {
        private Idle()
        {
            super();
        }

        public RaftMembershipStateMachineEventHandler onMissingMember( MemberId member )
        {
            return RaftMembershipChanger.this.new CatchingUp( member );
        }

        public RaftMembershipStateMachineEventHandler onSuperfluousMember( MemberId member )
        {
            Set<MemberId> updatedVotingMembers = new HashSet( RaftMembershipChanger.this.membershipManager.votingMembers() );
            updatedVotingMembers.remove( member );
            RaftMembershipChanger.this.membershipManager.doConsensus( updatedVotingMembers );
            return RaftMembershipChanger.this.new ConsensusInProgress();
        }

        public String toString()
        {
            return "Idle{}";
        }
    }

    abstract class ActiveBaseState extends RaftMembershipStateMachineEventHandler.Adapter
    {
        public RaftMembershipStateMachineEventHandler onRole( Role role )
        {
            return (RaftMembershipStateMachineEventHandler) (role != Role.LEADER ? RaftMembershipChanger.this.new Inactive() : this);
        }
    }

    private class Inactive extends RaftMembershipStateMachineEventHandler.Adapter
    {
        public RaftMembershipStateMachineEventHandler onRole( Role role )
        {
            if ( role == Role.LEADER )
            {
                return (RaftMembershipStateMachineEventHandler) (RaftMembershipChanger.this.membershipManager.uncommittedMemberChangeInLog()
                                                                 ? RaftMembershipChanger.this.new ConsensusInProgress()
                                                                 : RaftMembershipChanger.this.new Idle());
            }
            else
            {
                return this;
            }
        }

        public String toString()
        {
            return "Inactive{}";
        }
    }
}
