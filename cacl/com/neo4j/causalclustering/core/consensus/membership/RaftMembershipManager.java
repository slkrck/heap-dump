package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.consensus.log.RaftLogCursor;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.outcome.RaftLogCommand;
import com.neo4j.causalclustering.core.consensus.roles.Role;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerStates;
import com.neo4j.causalclustering.core.replication.SendToMyself;
import com.neo4j.causalclustering.core.state.storage.StateStorage;
import com.neo4j.causalclustering.identity.MemberId;

import java.io.IOException;
import java.time.Clock;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.LongSupplier;

import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftMembershipManager extends LifecycleAdapter implements RaftMembership, RaftLogCommand.Handler
{
    private final SendToMyself sendToMyself;
    private final MemberId myself;
    private final RaftMembers.Builder<MemberId> memberSetBuilder;
    private final ReadableRaftLog raftLog;
    private final Log log;
    private final StateStorage<RaftMembershipState> storage;
    private final int minimumConsensusGroupSize;
    private RaftMembershipChanger membershipChanger;
    private Set<MemberId> targetMembers;
    private LongSupplier recoverFromIndexSupplier;
    private RaftMembershipState state;
    private volatile Set<MemberId> votingMembers = Collections.unmodifiableSet( new HashSet() );
    private volatile Set<MemberId> replicationMembers = Collections.unmodifiableSet( new HashSet() );
    private Set<RaftMembership.Listener> listeners = new HashSet();
    private Set<MemberId> additionalReplicationMembers = new HashSet();

    public RaftMembershipManager( SendToMyself sendToMyself, MemberId myself, RaftMembers.Builder<MemberId> memberSetBuilder, ReadableRaftLog raftLog,
            LogProvider logProvider, int minimumConsensusGroupSize, long electionTimeout, Clock clock, long catchupTimeout,
            StateStorage<RaftMembershipState> membershipStorage )
    {
        this.sendToMyself = sendToMyself;
        this.myself = myself;
        this.memberSetBuilder = memberSetBuilder;
        this.raftLog = raftLog;
        this.minimumConsensusGroupSize = minimumConsensusGroupSize;
        this.storage = membershipStorage;
        this.log = logProvider.getLog( this.getClass() );
        this.membershipChanger = new RaftMembershipChanger( raftLog, clock, electionTimeout, logProvider, catchupTimeout, this );
    }

    public void setRecoverFromIndexSupplier( LongSupplier recoverFromIndexSupplier )
    {
        this.recoverFromIndexSupplier = recoverFromIndexSupplier;
    }

    public void start() throws IOException
    {
        this.state = (RaftMembershipState) this.storage.getInitialState();
        long recoverFromIndex = this.recoverFromIndexSupplier.getAsLong();
        this.log.info( "Membership state before recovery: " + this.state );
        this.log.info( "Recovering from: " + recoverFromIndex + " to: " + this.raftLog.appendIndex() );
        RaftLogCursor cursor = this.raftLog.getEntryCursor( recoverFromIndex );

        try
        {
            while ( cursor.next() )
            {
                this.append( cursor.index(), (RaftLogEntry) cursor.get() );
            }
        }
        catch ( Throwable var7 )
        {
            if ( cursor != null )
            {
                try
                {
                    cursor.close();
                }
                catch ( Throwable var6 )
                {
                    var7.addSuppressed( var6 );
                }
            }

            throw var7;
        }

        if ( cursor != null )
        {
            cursor.close();
        }

        this.log.info( "Membership state after recovery: " + this.state );
        this.updateMemberSets();
    }

    public void setTargetMembershipSet( Set<MemberId> targetMembers )
    {
        boolean targetMembershipChanged = !targetMembers.equals( this.targetMembers );
        this.targetMembers = new HashSet( targetMembers );
        if ( targetMembershipChanged )
        {
            this.log.info( "Target membership: " + targetMembers );
        }

        this.membershipChanger.onTargetChanged( targetMembers );
        HashSet<MemberId> intersection = new HashSet( targetMembers );
        Set<MemberId> votingMembers = this.votingMembers();
        intersection.retainAll( votingMembers );
        int majority = votingMembers.size() / 2 + 1;
        if ( intersection.size() < majority )
        {
            this.log.warn(
                    "Target membership %s does not contain a majority of existing raft members %s. It is likely an operator removed too many core members too quickly. If not, this issue should be transient.",
                    new Object[]{this.targetMembers, this.votingMembers} );
        }

        this.checkForStartCondition();
    }

    private Set<MemberId> missingMembers()
    {
        if ( this.targetMembers != null && this.votingMembers() != null )
        {
            Set<MemberId> missingMembers = new HashSet( this.targetMembers );
            missingMembers.removeAll( this.votingMembers() );
            return missingMembers;
        }
        else
        {
            return Collections.emptySet();
        }
    }

    private void updateMemberSets()
    {
        this.votingMembers = Collections.unmodifiableSet( this.state.getLatest() );
        HashSet<MemberId> newReplicationMembers = new HashSet( this.votingMembers );
        newReplicationMembers.addAll( this.additionalReplicationMembers );
        this.replicationMembers = Collections.unmodifiableSet( newReplicationMembers );
        this.listeners.forEach( RaftMembership.Listener::onMembershipChanged );
    }

    void addAdditionalReplicationMember( MemberId member )
    {
        this.additionalReplicationMembers.add( member );
        this.updateMemberSets();
    }

    void removeAdditionalReplicationMember( MemberId member )
    {
        this.additionalReplicationMembers.remove( member );
        this.updateMemberSets();
    }

    private boolean isSafeToRemoveMember()
    {
        Set<MemberId> votingMembers = this.votingMembers();
        boolean safeToRemoveMember = votingMembers != null && votingMembers.size() > this.minimumConsensusGroupSize;
        if ( !safeToRemoveMember )
        {
            Set<MemberId> membersToRemove = this.superfluousMembers();
            this.log.info(
                    "Not safe to remove %s %s because it would reduce the number of voting members below the expected cluster size of %d. Voting members: %s",
                    new Object[]{membersToRemove.size() > 1 ? "members" : "member", membersToRemove, this.minimumConsensusGroupSize, votingMembers} );
        }

        return safeToRemoveMember;
    }

    private Set<MemberId> superfluousMembers()
    {
        if ( this.targetMembers != null && this.votingMembers() != null )
        {
            Set<MemberId> superfluousMembers = new HashSet( this.votingMembers() );
            superfluousMembers.removeAll( this.targetMembers );
            superfluousMembers.remove( this.myself );
            return superfluousMembers;
        }
        else
        {
            return Collections.emptySet();
        }
    }

    private void checkForStartCondition()
    {
        if ( this.missingMembers().size() > 0 )
        {
            this.membershipChanger.onMissingMember( (MemberId) Iterables.first( this.missingMembers() ) );
        }
        else if ( this.superfluousMembers().size() > 0 && this.isSafeToRemoveMember() )
        {
            this.membershipChanger.onSuperfluousMember( (MemberId) Iterables.first( this.superfluousMembers() ) );
        }
    }

    void doConsensus( Set<MemberId> newVotingMemberSet )
    {
        this.log.info( "Getting consensus on new voting member set %s", new Object[]{newVotingMemberSet} );
        this.sendToMyself.replicate( this.memberSetBuilder.build( newVotingMemberSet ) );
    }

    void stateChanged()
    {
        this.checkForStartCondition();
    }

    public void onFollowerStateChange( FollowerStates<MemberId> followerStates )
    {
        this.membershipChanger.onFollowerStateChange( followerStates );
    }

    public void onRole( Role role )
    {
        this.membershipChanger.onRole( role );
    }

    public Set<MemberId> votingMembers()
    {
        return this.votingMembers;
    }

    public Set<MemberId> replicationMembers()
    {
        return this.replicationMembers;
    }

    public void registerListener( RaftMembership.Listener listener )
    {
        this.listeners.add( listener );
    }

    boolean uncommittedMemberChangeInLog()
    {
        return this.state.uncommittedMemberChangeInLog();
    }

    public void processLog( long commitIndex, Collection<RaftLogCommand> logCommands ) throws IOException
    {
        Iterator var4 = logCommands.iterator();

        while ( var4.hasNext() )
        {
            RaftLogCommand logCommand = (RaftLogCommand) var4.next();
            logCommand.dispatch( this );
        }

        if ( this.state.commit( commitIndex ) )
        {
            this.membershipChanger.onRaftGroupCommitted();
            this.storage.writeState( this.state );
            this.updateMemberSets();
        }
    }

    public void append( long baseIndex, RaftLogEntry... entries ) throws IOException
    {
        RaftLogEntry[] var4 = entries;
        int var5 = entries.length;

        for ( int var6 = 0; var6 < var5; ++var6 )
        {
            RaftLogEntry entry = var4[var6];
            if ( entry.content() instanceof RaftMembers )
            {
                RaftMembers<MemberId> raftMembers = (RaftMembers) entry.content();
                if ( this.state.uncommittedMemberChangeInLog() )
                {
                    this.log.warn( "Appending with uncommitted membership change in log" );
                }

                if ( this.state.append( baseIndex, new HashSet( raftMembers.getMembers() ) ) )
                {
                    this.log.info( "Appending new member set %s", new Object[]{this.state} );
                    this.storage.writeState( this.state );
                    this.updateMemberSets();
                }
                else
                {
                    this.log.warn( "Appending member set was ignored. Current state: %s, Appended set: %s, Log index: %d%n",
                            new Object[]{this.state, raftMembers, baseIndex} );
                }
            }

            ++baseIndex;
        }
    }

    public void truncate( long fromIndex ) throws IOException
    {
        if ( this.state.truncate( fromIndex ) )
        {
            this.storage.writeState( this.state );
            this.updateMemberSets();
        }
    }

    public void prune( long pruneIndex )
    {
    }

    public MembershipEntry getCommitted()
    {
        return this.state.committed();
    }

    public void install( MembershipEntry committed ) throws IOException
    {
        this.state = new RaftMembershipState( committed.logIndex(), committed, (MembershipEntry) null );
        this.storage.writeState( this.state );
        this.updateMemberSets();
    }
}
