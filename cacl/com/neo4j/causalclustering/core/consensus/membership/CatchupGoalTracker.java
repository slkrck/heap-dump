package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerState;

import java.time.Clock;

class CatchupGoalTracker
{
    static final long MAX_ROUNDS = 10L;
    private final ReadableRaftLog raftLog;
    private final Clock clock;
    private final long roundTimeout;
    private long startTime;
    private long roundStartTime;
    private long roundCount;
    private long catchupTimeout;
    private long targetIndex;
    private boolean finished;
    private boolean goalAchieved;

    CatchupGoalTracker( ReadableRaftLog raftLog, Clock clock, long roundTimeout, long catchupTimeout )
    {
        this.raftLog = raftLog;
        this.clock = clock;
        this.roundTimeout = roundTimeout;
        this.catchupTimeout = catchupTimeout;
        this.targetIndex = raftLog.appendIndex();
        this.startTime = clock.millis();
        this.roundStartTime = clock.millis();
        this.roundCount = 1L;
    }

    void updateProgress( FollowerState followerState )
    {
        if ( !this.finished )
        {
            boolean achievedTarget = followerState.getMatchIndex() >= this.targetIndex;
            if ( achievedTarget && this.clock.millis() - this.roundStartTime <= this.roundTimeout )
            {
                this.goalAchieved = true;
                this.finished = true;
            }
            else if ( this.clock.millis() > this.startTime + this.catchupTimeout )
            {
                this.finished = true;
            }
            else if ( achievedTarget )
            {
                if ( this.roundCount < 10L )
                {
                    ++this.roundCount;
                    this.roundStartTime = this.clock.millis();
                    this.targetIndex = this.raftLog.appendIndex();
                }
                else
                {
                    this.finished = true;
                }
            }
        }
    }

    boolean isFinished()
    {
        return this.finished;
    }

    boolean isGoalAchieved()
    {
        return this.goalAchieved;
    }

    public String toString()
    {
        return String.format(
                "CatchupGoalTracker{startTime=%d, roundStartTime=%d, roundTimeout=%d, roundCount=%d, catchupTimeout=%d, targetIndex=%d, finished=%s, goalAchieved=%s}",
                this.startTime, this.roundStartTime, this.roundTimeout, this.roundCount, this.catchupTimeout, this.targetIndex, this.finished,
                this.goalAchieved );
    }
}
