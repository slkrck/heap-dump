package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.consensus.roles.Role;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerStates;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;

interface RaftMembershipStateMachineEventHandler
{
    RaftMembershipStateMachineEventHandler onRole( Role var1 );

    RaftMembershipStateMachineEventHandler onRaftGroupCommitted();

    RaftMembershipStateMachineEventHandler onFollowerStateChange( FollowerStates<MemberId> var1 );

    RaftMembershipStateMachineEventHandler onMissingMember( MemberId var1 );

    RaftMembershipStateMachineEventHandler onSuperfluousMember( MemberId var1 );

    RaftMembershipStateMachineEventHandler onTargetChanged( Set<MemberId> var1 );

    void onExit();

    void onEntry();

    public abstract static class Adapter implements RaftMembershipStateMachineEventHandler
    {
        public RaftMembershipStateMachineEventHandler onRole( Role role )
        {
            return this;
        }

        public RaftMembershipStateMachineEventHandler onRaftGroupCommitted()
        {
            return this;
        }

        public RaftMembershipStateMachineEventHandler onMissingMember( MemberId member )
        {
            return this;
        }

        public RaftMembershipStateMachineEventHandler onSuperfluousMember( MemberId member )
        {
            return this;
        }

        public RaftMembershipStateMachineEventHandler onFollowerStateChange( FollowerStates<MemberId> followerStates )
        {
            return this;
        }

        public RaftMembershipStateMachineEventHandler onTargetChanged( Set targetMembers )
        {
            return this;
        }

        public void onExit()
        {
        }

        public void onEntry()
        {
        }
    }
}
