package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.replication.ReplicatedContent;

import java.util.Set;

public interface RaftMembers<MEMBER> extends ReplicatedContent
{
    Set<MEMBER> getMembers();

    public interface Builder<MEMBER>
    {
        RaftMembers<MEMBER> build( Set<MEMBER> var1 );
    }
}
