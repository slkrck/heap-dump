package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;

public interface RaftMembership
{
    Set<MemberId> votingMembers();

    Set<MemberId> replicationMembers();

    void registerListener( RaftMembership.Listener var1 );

    public interface Listener
    {
        void onMembershipChanged();
    }
}
