package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

public class RaftMembershipState extends LifecycleAdapter
{
    private MembershipEntry committed;
    private MembershipEntry appended;
    private long ordinal;

    public RaftMembershipState()
    {
        this( -1L, (MembershipEntry) null, (MembershipEntry) null );
    }

    RaftMembershipState( long ordinal, MembershipEntry committed, MembershipEntry appended )
    {
        this.ordinal = ordinal;
        this.committed = committed;
        this.appended = appended;
    }

    public boolean append( long logIndex, Set<MemberId> members )
    {
        if ( this.appended != null && logIndex <= this.appended.logIndex() )
        {
            return false;
        }
        else if ( this.committed != null && logIndex <= this.committed.logIndex() )
        {
            return false;
        }
        else
        {
            if ( this.appended != null && (this.committed == null || this.appended.logIndex() > this.committed.logIndex()) )
            {
                this.committed = this.appended;
            }

            ++this.ordinal;
            this.appended = new MembershipEntry( logIndex, members );
            return true;
        }
    }

    public boolean truncate( long fromIndex )
    {
        if ( this.committed != null && fromIndex <= this.committed.logIndex() )
        {
            throw new IllegalStateException( "Truncating committed entry" );
        }
        else if ( this.appended != null && fromIndex <= this.appended.logIndex() )
        {
            ++this.ordinal;
            this.appended = null;
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean commit( long commitIndex )
    {
        if ( this.appended != null && commitIndex >= this.appended.logIndex() )
        {
            ++this.ordinal;
            this.committed = this.appended;
            this.appended = null;
            return true;
        }
        else
        {
            return false;
        }
    }

    boolean uncommittedMemberChangeInLog()
    {
        return this.appended != null;
    }

    Set<MemberId> getLatest()
    {
        return (Set) (this.appended != null ? this.appended.members() : (this.committed != null ? this.committed.members() : new HashSet()));
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            RaftMembershipState that = (RaftMembershipState) o;
            return this.ordinal == that.ordinal && Objects.equals( this.committed, that.committed ) && Objects.equals( this.appended, that.appended );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.committed, this.appended, this.ordinal} );
    }

    public String toString()
    {
        return "RaftMembershipState{committed=" + this.committed + ", appended=" + this.appended + ", ordinal=" + this.ordinal + "}";
    }

    public RaftMembershipState newInstance()
    {
        return new RaftMembershipState( this.ordinal, this.committed, this.appended );
    }

    public MembershipEntry committed()
    {
        return this.committed;
    }

    public long getOrdinal()
    {
        return this.ordinal;
    }

    public static class Marshal extends SafeStateMarshal<RaftMembershipState>
    {
        MembershipEntry.Marshal entryMarshal = new MembershipEntry.Marshal();

        public RaftMembershipState startState()
        {
            return new RaftMembershipState();
        }

        public long ordinal( RaftMembershipState state )
        {
            return state.ordinal;
        }

        public void marshal( RaftMembershipState state, WritableChannel channel ) throws IOException
        {
            channel.putLong( state.ordinal );
            this.entryMarshal.marshal( state.committed, channel );
            this.entryMarshal.marshal( state.appended, channel );
        }

        public RaftMembershipState unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
        {
            long ordinal = channel.getLong();
            MembershipEntry committed = (MembershipEntry) this.entryMarshal.unmarshal( channel );
            MembershipEntry appended = (MembershipEntry) this.entryMarshal.unmarshal( channel );
            return new RaftMembershipState( ordinal, committed, appended );
        }
    }
}
