package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentHandler;

import java.io.IOException;
import java.util.Set;

public class MemberIdSet implements RaftMembers<MemberId>
{
    private final Set<MemberId> members;

    public MemberIdSet( Set<MemberId> members )
    {
        this.members = members;
    }

    public String toString()
    {
        return "MemberIdSet{ members=" + this.members + "}";
    }

    public Set<MemberId> getMembers()
    {
        return this.members;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            boolean var10000;
            label35:
            {
                MemberIdSet that = (MemberIdSet) o;
                if ( this.members != null )
                {
                    if ( this.members.equals( that.members ) )
                    {
                        break label35;
                    }
                }
                else if ( that.members == null )
                {
                    break label35;
                }

                var10000 = false;
                return var10000;
            }

            var10000 = true;
            return var10000;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.members != null ? this.members.hashCode() : 0;
    }

    public void dispatch( ReplicatedContentHandler contentHandler ) throws IOException
    {
        contentHandler.handle( this );
    }
}
