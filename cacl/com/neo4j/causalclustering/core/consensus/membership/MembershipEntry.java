package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class MembershipEntry
{
    private long logIndex;
    private Set<MemberId> members;

    public MembershipEntry( long logIndex, Set<MemberId> members )
    {
        this.members = members;
        this.logIndex = logIndex;
    }

    public long logIndex()
    {
        return this.logIndex;
    }

    public Set<MemberId> members()
    {
        return this.members;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            MembershipEntry that = (MembershipEntry) o;
            return this.logIndex == that.logIndex && Objects.equals( this.members, that.members );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.logIndex, this.members} );
    }

    public String toString()
    {
        return "MembershipEntry{logIndex=" + this.logIndex + ", members=" + this.members + "}";
    }

    public static class Marshal extends SafeStateMarshal<MembershipEntry>
    {
        MemberId.Marshal memberMarshal = new MemberId.Marshal();

        public MembershipEntry startState()
        {
            return null;
        }

        public long ordinal( MembershipEntry entry )
        {
            return entry.logIndex;
        }

        public void marshal( MembershipEntry entry, WritableChannel channel ) throws IOException
        {
            if ( entry == null )
            {
                channel.putInt( 0 );
            }
            else
            {
                channel.putInt( 1 );
                channel.putLong( entry.logIndex );
                channel.putInt( entry.members.size() );
                Iterator var3 = entry.members.iterator();

                while ( var3.hasNext() )
                {
                    MemberId member = (MemberId) var3.next();
                    this.memberMarshal.marshal( member, channel );
                }
            }
        }

        protected MembershipEntry unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
        {
            int hasEntry = channel.getInt();
            if ( hasEntry == 0 )
            {
                return null;
            }
            else
            {
                long logIndex = channel.getLong();
                int memberCount = channel.getInt();
                Set<MemberId> members = new HashSet();

                for ( int i = 0; i < memberCount; ++i )
                {
                    members.add( (MemberId) this.memberMarshal.unmarshal( channel ) );
                }

                return new MembershipEntry( logIndex, members );
            }
        }
    }
}
