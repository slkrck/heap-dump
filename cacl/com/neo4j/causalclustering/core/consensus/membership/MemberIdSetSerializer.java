package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class MemberIdSetSerializer
{
    private MemberIdSetSerializer()
    {
    }

    public static void marshal( MemberIdSet memberSet, WritableChannel channel ) throws IOException
    {
        Set<MemberId> members = memberSet.getMembers();
        channel.putInt( members.size() );
        MemberId.Marshal memberIdMarshal = new MemberId.Marshal();
        Iterator var4 = members.iterator();

        while ( var4.hasNext() )
        {
            MemberId member = (MemberId) var4.next();
            memberIdMarshal.marshal( member, channel );
        }
    }

    public static MemberIdSet unmarshal( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        HashSet<MemberId> members = new HashSet();
        int memberCount = channel.getInt();
        MemberId.Marshal memberIdMarshal = new MemberId.Marshal();

        for ( int i = 0; i < memberCount; ++i )
        {
            members.add( (MemberId) memberIdMarshal.unmarshal( channel ) );
        }

        return new MemberIdSet( members );
    }
}
