package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerState;

import java.time.Clock;

class CatchupGoal
{
    private static final long MAX_ROUNDS = 10L;
    private final ReadableRaftLog raftLog;
    private final Clock clock;
    private final long electionTimeout;
    private long targetIndex;
    private long roundCount;
    private long startTime;

    CatchupGoal( ReadableRaftLog raftLog, Clock clock, long electionTimeout )
    {
        this.raftLog = raftLog;
        this.clock = clock;
        this.electionTimeout = electionTimeout;
        this.targetIndex = raftLog.appendIndex();
        this.startTime = clock.millis();
        this.roundCount = 1L;
    }

    boolean achieved( FollowerState followerState )
    {
        if ( followerState.getMatchIndex() >= this.targetIndex )
        {
            if ( this.clock.millis() - this.startTime <= this.electionTimeout )
            {
                return true;
            }

            if ( this.roundCount < 10L )
            {
                ++this.roundCount;
                this.startTime = this.clock.millis();
                this.targetIndex = this.raftLog.appendIndex();
            }
        }

        return false;
    }
}
