package com.neo4j.causalclustering.core.consensus.membership;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;

public class MemberIdSetBuilder implements RaftMembers.Builder<MemberId>
{
    public RaftMembers<MemberId> build( Set<MemberId> members )
    {
        return new MemberIdSet( members );
    }
}
