package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.core.consensus.membership.RaftMembershipManager;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.roles.Role;
import com.neo4j.causalclustering.core.consensus.shipping.RaftLogShippingManager;
import com.neo4j.causalclustering.core.consensus.state.RaftState;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Outbound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class RaftOutcomeApplier
{
    private final RaftState state;
    private final Log log;
    private final Outbound<MemberId,RaftMessages.RaftMessage> outbound;
    private final RaftMessageTimerResetMonitor raftMessageTimerResetMonitor;
    private final LeaderAvailabilityTimers leaderAvailabilityTimers;
    private final RaftLogShippingManager logShipping;
    private final RaftMembershipManager membershipManager;
    private final Collection<LeaderListener> leaderListeners = new ArrayList();
    private volatile LeaderInfo leaderInfo;

    RaftOutcomeApplier( RaftState state, Outbound<MemberId,RaftMessages.RaftMessage> outbound, LeaderAvailabilityTimers leaderAvailabilityTimers,
            RaftMessageTimerResetMonitor raftMessageTimerResetMonitor, RaftLogShippingManager logShipping, RaftMembershipManager membershipManager,
            LogProvider logProvider )
    {
        this.state = state;
        this.outbound = outbound;
        this.leaderAvailabilityTimers = leaderAvailabilityTimers;
        this.raftMessageTimerResetMonitor = raftMessageTimerResetMonitor;
        this.logShipping = logShipping;
        this.membershipManager = membershipManager;
        this.log = logProvider.getLog( this.getClass() );
    }

    synchronized Role handle( Outcome outcome ) throws IOException
    {
        boolean newLeaderWasElected = this.leaderChanged( outcome, this.state.leader() );
        this.state.update( outcome );
        this.sendMessages( outcome );
        this.handleTimers( outcome );
        this.handleLogShipping( outcome );
        this.leaderInfo = new LeaderInfo( outcome.getLeader(), outcome.getTerm() );
        if ( newLeaderWasElected )
        {
            this.notifyLeaderChanges( outcome );
        }

        this.driveMembership( outcome );
        return outcome.getRole();
    }

    private boolean leaderChanged( Outcome outcome, MemberId oldLeader )
    {
        return !Objects.equals( oldLeader, outcome.getLeader() );
    }

    private void sendMessages( Outcome outcome )
    {
        Iterator var2 = outcome.getOutgoingMessages().iterator();

        while ( var2.hasNext() )
        {
            RaftMessages.Directed outgoingMessage = (RaftMessages.Directed) var2.next();

            try
            {
                this.outbound.send( outgoingMessage.to(), outgoingMessage.message() );
            }
            catch ( Exception var5 )
            {
                this.log.warn( String.format( "Failed to send message %s.", outgoingMessage ), var5 );
            }
        }
    }

    private void handleTimers( Outcome outcome )
    {
        if ( outcome.electionTimeoutRenewed() )
        {
            this.raftMessageTimerResetMonitor.timerReset();
            this.leaderAvailabilityTimers.renewElection();
        }
        else if ( outcome.isSteppingDown() )
        {
            this.raftMessageTimerResetMonitor.timerReset();
        }
    }

    private void handleLogShipping( Outcome outcome )
    {
        LeaderContext leaderContext = new LeaderContext( outcome.getTerm(), outcome.getLeaderCommit() );
        if ( outcome.isElectedLeader() )
        {
            this.logShipping.resume( leaderContext );
        }
        else if ( outcome.isSteppingDown() )
        {
            this.logShipping.pause();
        }

        if ( outcome.getRole() == Role.LEADER )
        {
            this.logShipping.handleCommands( outcome.getShipCommands(), leaderContext );
        }
    }

    private void notifyLeaderChanges( Outcome outcome )
    {
        Iterator var2 = this.leaderListeners.iterator();

        while ( var2.hasNext() )
        {
            LeaderListener listener = (LeaderListener) var2.next();
            listener.onLeaderEvent( outcome );
        }
    }

    private void driveMembership( Outcome outcome ) throws IOException
    {
        this.membershipManager.processLog( outcome.getCommitIndex(), outcome.getLogCommands() );
        Role nextRole = outcome.getRole();
        this.membershipManager.onRole( nextRole );
        if ( nextRole == Role.LEADER )
        {
            this.membershipManager.onFollowerStateChange( outcome.getFollowerStates() );
        }
    }

    Optional<LeaderInfo> getLeaderInfo()
    {
        return Optional.ofNullable( this.leaderInfo );
    }

    synchronized void registerListener( LeaderListener listener )
    {
        this.leaderListeners.add( listener );
        listener.onLeaderSwitch( this.state.leaderInfo() );
    }

    synchronized void unregisterListener( LeaderListener listener )
    {
        this.leaderListeners.remove( listener );
    }
}
