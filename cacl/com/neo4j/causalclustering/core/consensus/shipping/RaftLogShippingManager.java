package com.neo4j.causalclustering.core.consensus.shipping;

import com.neo4j.causalclustering.core.consensus.LeaderContext;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;
import com.neo4j.causalclustering.core.consensus.membership.RaftMembership;
import com.neo4j.causalclustering.core.consensus.outcome.ShipCommand;
import com.neo4j.causalclustering.core.consensus.schedule.TimerService;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Outbound;

import java.time.Clock;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.LogProvider;

public class RaftLogShippingManager extends LifecycleAdapter implements RaftMembership.Listener
{
    private final Outbound<MemberId,RaftMessages.RaftMessage> outbound;
    private final LogProvider logProvider;
    private final ReadableRaftLog raftLog;
    private final Clock clock;
    private final MemberId myself;
    private final RaftMembership membership;
    private final long retryTimeMillis;
    private final int catchupBatchSize;
    private final int maxAllowedShippingLag;
    private final InFlightCache inFlightCache;
    private Map<MemberId,RaftLogShipper> logShippers = new HashMap();
    private LeaderContext lastLeaderContext;
    private boolean running;
    private boolean stopped;
    private TimerService timerService;

    public RaftLogShippingManager( Outbound<MemberId,RaftMessages.RaftMessage> outbound, LogProvider logProvider, ReadableRaftLog raftLog,
            TimerService timerService, Clock clock, MemberId myself, RaftMembership membership, long retryTimeMillis, int catchupBatchSize,
            int maxAllowedShippingLag, InFlightCache inFlightCache )
    {
        this.outbound = outbound;
        this.logProvider = logProvider;
        this.raftLog = raftLog;
        this.timerService = timerService;
        this.clock = clock;
        this.myself = myself;
        this.membership = membership;
        this.retryTimeMillis = retryTimeMillis;
        this.catchupBatchSize = catchupBatchSize;
        this.maxAllowedShippingLag = maxAllowedShippingLag;
        this.inFlightCache = inFlightCache;
        membership.registerListener( this );
    }

    public synchronized void pause()
    {
        this.running = false;
        this.logShippers.values().forEach( RaftLogShipper::stop );
        this.logShippers.clear();
    }

    public synchronized void resume( LeaderContext initialLeaderContext )
    {
        if ( !this.stopped )
        {
            this.running = true;
            Iterator var2 = this.membership.replicationMembers().iterator();

            while ( var2.hasNext() )
            {
                MemberId member = (MemberId) var2.next();
                this.ensureLogShipperRunning( member, initialLeaderContext );
            }

            this.lastLeaderContext = initialLeaderContext;
        }
    }

    public synchronized void stop()
    {
        this.pause();
        this.stopped = true;
    }

    private void ensureLogShipperRunning( MemberId member, LeaderContext leaderContext )
    {
        RaftLogShipper logShipper = (RaftLogShipper) this.logShippers.get( member );
        if ( logShipper == null && !member.equals( this.myself ) )
        {
            logShipper =
                    new RaftLogShipper( this.outbound, this.logProvider, this.raftLog, this.clock, this.timerService, this.myself, member, leaderContext.term,
                            leaderContext.commitIndex, this.retryTimeMillis, this.catchupBatchSize, this.maxAllowedShippingLag, this.inFlightCache );
            this.logShippers.put( member, logShipper );
            logShipper.start();
        }
    }

    public synchronized void handleCommands( Iterable<ShipCommand> shipCommands, LeaderContext leaderContext )
    {
        Iterator var3 = shipCommands.iterator();

        while ( var3.hasNext() )
        {
            ShipCommand shipCommand = (ShipCommand) var3.next();
            Iterator var5 = this.logShippers.values().iterator();

            while ( var5.hasNext() )
            {
                RaftLogShipper logShipper = (RaftLogShipper) var5.next();
                shipCommand.applyTo( logShipper, leaderContext );
            }
        }

        this.lastLeaderContext = leaderContext;
    }

    public synchronized void onMembershipChanged()
    {
        if ( this.lastLeaderContext != null && this.running )
        {
            HashSet<MemberId> toBeRemoved = new HashSet( this.logShippers.keySet() );
            toBeRemoved.removeAll( this.membership.replicationMembers() );
            Iterator var2 = toBeRemoved.iterator();

            MemberId replicationMember;
            while ( var2.hasNext() )
            {
                replicationMember = (MemberId) var2.next();
                RaftLogShipper logShipper = (RaftLogShipper) this.logShippers.remove( replicationMember );
                if ( logShipper != null )
                {
                    logShipper.stop();
                }
            }

            var2 = this.membership.replicationMembers().iterator();

            while ( var2.hasNext() )
            {
                replicationMember = (MemberId) var2.next();
                this.ensureLogShipperRunning( replicationMember, this.lastLeaderContext );
            }
        }
    }

    public String toString()
    {
        return String.format( "RaftLogShippingManager{logShippers=%s, myself=%s}", this.logShippers, this.myself );
    }
}
