package com.neo4j.causalclustering.core.consensus.shipping;

import com.neo4j.causalclustering.core.consensus.LeaderContext;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;
import com.neo4j.causalclustering.core.consensus.schedule.TimeoutFactory;
import com.neo4j.causalclustering.core.consensus.schedule.Timer;
import com.neo4j.causalclustering.core.consensus.schedule.TimerService;
import com.neo4j.causalclustering.core.state.InFlightLogEntryReader;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Outbound;

import java.io.IOException;
import java.time.Clock;
import java.util.concurrent.TimeUnit;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;

public class RaftLogShipper
{
    private static final long MIN_INDEX = 1L;
    private final int TIMER_INACTIVE = 0;
    private final Outbound<MemberId,RaftMessages.RaftMessage> outbound;
    private final Log log;
    private final ReadableRaftLog raftLog;
    private final Clock clock;
    private final MemberId follower;
    private final MemberId leader;
    private final long retryTimeMillis;
    private final int catchupBatchSize;
    private final int maxAllowedShippingLag;
    private final InFlightCache inFlightCache;
    private TimerService timerService;
    private Timer timer;
    private long timeoutAbsoluteMillis;
    private long lastSentIndex;
    private long matchIndex = -1L;
    private LeaderContext lastLeaderContext;
    private RaftLogShipper.Mode mode;

    RaftLogShipper( Outbound<MemberId,RaftMessages.RaftMessage> outbound, LogProvider logProvider, ReadableRaftLog raftLog, Clock clock,
            TimerService timerService, MemberId leader, MemberId follower, long leaderTerm, long leaderCommit, long retryTimeMillis, int catchupBatchSize,
            int maxAllowedShippingLag, InFlightCache inFlightCache )
    {
        this.mode = RaftLogShipper.Mode.MISMATCH;
        this.outbound = outbound;
        this.timerService = timerService;
        this.catchupBatchSize = catchupBatchSize;
        this.maxAllowedShippingLag = maxAllowedShippingLag;
        this.log = logProvider.getLog( this.getClass() );
        this.raftLog = raftLog;
        this.clock = clock;
        this.follower = follower;
        this.leader = leader;
        this.retryTimeMillis = retryTimeMillis;
        this.lastLeaderContext = new LeaderContext( leaderTerm, leaderCommit );
        this.inFlightCache = inFlightCache;
    }

    public Object identity()
    {
        return this.follower;
    }

    public synchronized void start()
    {
        this.log.info( "Starting log shipper: %s", new Object[]{this.statusAsString()} );
        this.sendEmpty( this.raftLog.appendIndex(), this.lastLeaderContext );
    }

    public synchronized void stop()
    {
        this.log.info( "Stopping log shipper %s", new Object[]{this.statusAsString()} );
        this.abortTimeout();
    }

    public synchronized void onMismatch( long lastRemoteAppendIndex, LeaderContext leaderContext )
    {
        switch ( this.mode )
        {
        case MISMATCH:
            long logIndex = Long.max( Long.min( this.lastSentIndex - 1L, lastRemoteAppendIndex ), 1L );
            this.sendEmpty( logIndex, leaderContext );
            break;
        case PIPELINE:
        case CATCHUP:
            this.log.info( "%s: mismatch in mode %s from follower %s, moving to MISMATCH mode", new Object[]{this.statusAsString(), this.mode, this.follower} );
            this.mode = RaftLogShipper.Mode.MISMATCH;
            this.sendEmpty( this.lastSentIndex, leaderContext );
            break;
        default:
            throw new IllegalStateException( "Unknown mode: " + this.mode );
        }

        this.lastLeaderContext = leaderContext;
    }

    public synchronized void onMatch( long newMatchIndex, LeaderContext leaderContext )
    {
        boolean progress = newMatchIndex > this.matchIndex;
        if ( newMatchIndex > this.matchIndex )
        {
            this.matchIndex = newMatchIndex;
        }
        else
        {
            this.log.warn( "%s: match index not progressing. This should be transient.", new Object[]{this.statusAsString()} );
        }

        switch ( this.mode )
        {
        case MISMATCH:
            if ( this.sendNextBatchAfterMatch( leaderContext ) )
            {
                this.log.info( "%s: caught up after mismatch, moving to PIPELINE mode", new Object[]{this.statusAsString()} );
                this.mode = RaftLogShipper.Mode.PIPELINE;
            }
            else
            {
                this.log.info( "%s: starting catch up after mismatch, moving to CATCHUP mode", new Object[]{this.statusAsString()} );
                this.mode = RaftLogShipper.Mode.CATCHUP;
            }
            break;
        case PIPELINE:
            if ( this.matchIndex == this.lastSentIndex )
            {
                this.abortTimeout();
            }
            else if ( progress )
            {
                this.scheduleTimeout( this.retryTimeMillis );
            }
            break;
        case CATCHUP:
            if ( this.matchIndex >= this.lastSentIndex && this.sendNextBatchAfterMatch( leaderContext ) )
            {
                this.log.info( "%s: caught up, moving to PIPELINE mode", new Object[]{this.statusAsString()} );
                this.mode = RaftLogShipper.Mode.PIPELINE;
            }
            break;
        default:
            throw new IllegalStateException( "Unknown mode: " + this.mode );
        }

        this.lastLeaderContext = leaderContext;
    }

    public synchronized void onNewEntries( long prevLogIndex, long prevLogTerm, RaftLogEntry[] newLogEntries, LeaderContext leaderContext )
    {
        if ( this.mode == RaftLogShipper.Mode.PIPELINE )
        {
            while ( this.lastSentIndex <= prevLogIndex )
            {
                if ( prevLogIndex - this.matchIndex > (long) this.maxAllowedShippingLag )
                {
                    this.log.info( "%s: follower has fallen behind (target prevLogIndex was %d, maxAllowedShippingLag is %d), moving to CATCHUP mode",
                            new Object[]{this.statusAsString(), prevLogIndex, this.maxAllowedShippingLag} );
                    this.mode = RaftLogShipper.Mode.CATCHUP;
                    break;
                }

                this.sendNewEntries( prevLogIndex, prevLogTerm, newLogEntries, leaderContext );
            }
        }

        this.lastLeaderContext = leaderContext;
    }

    public synchronized void onCommitUpdate( LeaderContext leaderContext )
    {
        if ( this.mode == RaftLogShipper.Mode.PIPELINE )
        {
            this.sendCommitUpdate( leaderContext );
        }

        this.lastLeaderContext = leaderContext;
    }

    private synchronized void onScheduledTimeoutExpiry()
    {
        if ( this.timedOut() )
        {
            this.onTimeout();
        }
        else if ( this.timeoutAbsoluteMillis != 0L )
        {
            long timeLeft = this.timeoutAbsoluteMillis - this.clock.millis();
            if ( timeLeft > 0L )
            {
                this.scheduleTimeout( timeLeft );
            }
            else
            {
                this.onTimeout();
            }
        }
    }

    void onTimeout()
    {
        if ( this.mode == RaftLogShipper.Mode.PIPELINE )
        {
            this.log.info( "%s: timed out, moving to CATCHUP mode", new Object[]{this.statusAsString()} );
            this.mode = RaftLogShipper.Mode.CATCHUP;
            this.scheduleTimeout( this.retryTimeMillis );
        }
        else if ( this.mode == RaftLogShipper.Mode.CATCHUP )
        {
            this.log.info( "%s: timed out, moving to MISMATCH mode", new Object[]{this.statusAsString()} );
            this.mode = RaftLogShipper.Mode.MISMATCH;
        }

        if ( this.lastLeaderContext != null )
        {
            this.sendEmpty( this.lastSentIndex, this.lastLeaderContext );
        }
    }

    private boolean timedOut()
    {
        return this.timeoutAbsoluteMillis != 0L && this.clock.millis() - this.timeoutAbsoluteMillis >= 0L;
    }

    private void scheduleTimeout( long deltaMillis )
    {
        this.timeoutAbsoluteMillis = this.clock.millis() + deltaMillis;
        if ( this.timer == null )
        {
            this.timer = this.timerService.create( RaftLogShipper.Timeouts.RESEND, Group.RAFT_TIMER, ( timeout ) -> {
                this.onScheduledTimeoutExpiry();
            } );
        }

        this.timer.set( TimeoutFactory.fixedTimeout( deltaMillis, TimeUnit.MILLISECONDS ) );
    }

    private void abortTimeout()
    {
        if ( this.timer != null )
        {
            this.timer.cancel( Timer.CancelMode.ASYNC );
        }

        this.timeoutAbsoluteMillis = 0L;
    }

    private boolean sendNextBatchAfterMatch( LeaderContext leaderContext )
    {
        long lastIndex = this.raftLog.appendIndex();
        if ( lastIndex > this.matchIndex )
        {
            long endIndex = Long.min( lastIndex, this.matchIndex + (long) this.catchupBatchSize );
            this.scheduleTimeout( this.retryTimeMillis );
            this.sendRange( this.matchIndex + 1L, endIndex, leaderContext );
            return endIndex == lastIndex;
        }
        else
        {
            return true;
        }
    }

    private void sendCommitUpdate( LeaderContext leaderContext )
    {
        RaftMessages.Heartbeat appendRequest = new RaftMessages.Heartbeat( this.leader, leaderContext.term, leaderContext.commitIndex, leaderContext.term );
        this.outbound.send( this.follower, appendRequest );
    }

    private void sendNewEntries( long prevLogIndex, long prevLogTerm, RaftLogEntry[] newEntries, LeaderContext leaderContext )
    {
        this.scheduleTimeout( this.retryTimeMillis );
        this.lastSentIndex = prevLogIndex + 1L;
        RaftMessages.AppendEntries.Request appendRequest =
                new RaftMessages.AppendEntries.Request( this.leader, leaderContext.term, prevLogIndex, prevLogTerm, newEntries, leaderContext.commitIndex );
        this.outbound.send( this.follower, appendRequest );
    }

    private void sendEmpty( long logIndex, LeaderContext leaderContext )
    {
        this.scheduleTimeout( this.retryTimeMillis );
        logIndex = Long.max( this.raftLog.prevIndex() + 1L, logIndex );
        this.lastSentIndex = logIndex;

        try
        {
            long prevLogIndex = logIndex - 1L;
            long prevLogTerm = this.raftLog.readEntryTerm( prevLogIndex );
            if ( prevLogTerm > leaderContext.term )
            {
                this.log.warn( "%s: aborting send. Not leader anymore? %s, prevLogTerm=%d", new Object[]{this.statusAsString(), leaderContext, prevLogTerm} );
                return;
            }

            if ( this.doesNotExistInLog( prevLogIndex, prevLogTerm ) )
            {
                this.log.warn( "%s: Entry was pruned when sending empty (prevLogIndex=%d, prevLogTerm=%d)",
                        new Object[]{this.statusAsString(), prevLogIndex, prevLogTerm} );
                return;
            }

            RaftMessages.AppendEntries.Request appendRequest =
                    new RaftMessages.AppendEntries.Request( this.leader, leaderContext.term, prevLogIndex, prevLogTerm, RaftLogEntry.empty,
                            leaderContext.commitIndex );
            this.outbound.send( this.follower, appendRequest );
        }
        catch ( IOException var9 )
        {
            this.log.warn( this.statusAsString() + " exception during empty send", var9 );
        }
    }

    private void sendRange( long startIndex, long endIndex, LeaderContext leaderContext )
    {
        if ( startIndex <= endIndex )
        {
            this.lastSentIndex = endIndex;

            try
            {
                int batchSize = (int) (endIndex - startIndex + 1L);
                RaftLogEntry[] entries = new RaftLogEntry[batchSize];
                long prevLogIndex = startIndex - 1L;
                long prevLogTerm = this.raftLog.readEntryTerm( prevLogIndex );
                if ( prevLogTerm > leaderContext.term )
                {
                    this.log.warn( "%s aborting send. Not leader anymore? %s, prevLogTerm=%d",
                            new Object[]{this.statusAsString(), leaderContext, prevLogTerm} );
                    return;
                }

                boolean entryMissing = false;
                InFlightLogEntryReader logEntrySupplier = new InFlightLogEntryReader( this.raftLog, this.inFlightCache, false );

                label62:
                {
                    try
                    {
                        int offset = 0;

                        while ( true )
                        {
                            if ( offset >= batchSize )
                            {
                                break label62;
                            }

                            entries[offset] = logEntrySupplier.get( startIndex + (long) offset );
                            if ( entries[offset] == null )
                            {
                                entryMissing = true;
                                break label62;
                            }

                            if ( entries[offset].term() > leaderContext.term )
                            {
                                this.log.warn( "%s aborting send. Not leader anymore? %s, entryTerm=%d",
                                        new Object[]{this.statusAsString(), leaderContext, entries[offset].term()} );
                                break;
                            }

                            ++offset;
                        }
                    }
                    catch ( Throwable var17 )
                    {
                        try
                        {
                            logEntrySupplier.close();
                        }
                        catch ( Throwable var16 )
                        {
                            var17.addSuppressed( var16 );
                        }

                        throw var17;
                    }

                    logEntrySupplier.close();
                    return;
                }

                logEntrySupplier.close();
                if ( !entryMissing && !this.doesNotExistInLog( prevLogIndex, prevLogTerm ) )
                {
                    RaftMessages.AppendEntries.Request appendRequest =
                            new RaftMessages.AppendEntries.Request( this.leader, leaderContext.term, prevLogIndex, prevLogTerm, entries,
                                    leaderContext.commitIndex );
                    this.outbound.send( this.follower, appendRequest );
                }
                else if ( this.raftLog.prevIndex() >= prevLogIndex )
                {
                    this.sendLogCompactionInfo( leaderContext );
                }
                else
                {
                    this.log.error( "%s: Could not send compaction info and entries were missing, but log is not behind.",
                            new Object[]{this.statusAsString()} );
                }
            }
            catch ( IOException var18 )
            {
                this.log.warn( this.statusAsString() + " exception during batch send", var18 );
            }
        }
    }

    private boolean doesNotExistInLog( long logIndex, long logTerm )
    {
        return logTerm == -1L && logIndex != -1L;
    }

    private void sendLogCompactionInfo( LeaderContext leaderContext )
    {
        this.log.warn( "Sending log compaction info. Log pruned? Status=%s, LeaderContext=%s", new Object[]{this.statusAsString(), leaderContext} );
        this.outbound.send( this.follower, new RaftMessages.LogCompactionInfo( this.leader, leaderContext.term, this.raftLog.prevIndex() ) );
    }

    private String statusAsString()
    {
        return String.format( "%s[matchIndex: %d, lastSentIndex: %d, localAppendIndex: %d, mode: %s]", this.follower, this.matchIndex, this.lastSentIndex,
                this.raftLog.appendIndex(), this.mode );
    }

    public static enum Timeouts implements TimerService.TimerName
    {
        RESEND;
    }

    static enum Mode
    {
        MISMATCH,
        CATCHUP,
        PIPELINE;
    }
}
