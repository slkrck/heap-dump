package com.neo4j.causalclustering.core.consensus.state;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.log.ReadableRaftLog;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;
import com.neo4j.causalclustering.core.consensus.membership.RaftMembership;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.outcome.RaftLogCommand;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerStates;
import com.neo4j.causalclustering.core.consensus.term.TermState;
import com.neo4j.causalclustering.core.consensus.vote.VoteState;
import com.neo4j.causalclustering.core.state.storage.StateStorage;
import com.neo4j.causalclustering.identity.MemberId;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftState implements ReadableRaftState
{
    private final MemberId myself;
    private final StateStorage<TermState> termStorage;
    private final StateStorage<VoteState> voteStorage;
    private final RaftMembership membership;
    private final Log log;
    private final RaftLog entryLog;
    private final InFlightCache inFlightCache;
    private final boolean supportPreVoting;
    private final boolean refuseToBeLeader;
    private TermState termState;
    private VoteState voteState;
    private MemberId leader;
    private LeaderInfo leaderInfo;
    private Set<MemberId> votesForMe;
    private Set<MemberId> preVotesForMe;
    private Set<MemberId> heartbeatResponses;
    private FollowerStates<MemberId> followerStates;
    private long leaderCommit;
    private long commitIndex;
    private long lastLogIndexBeforeWeBecameLeader;
    private boolean isPreElection;

    public RaftState( MemberId myself, StateStorage<TermState> termStorage, RaftMembership membership, RaftLog entryLog, StateStorage<VoteState> voteStorage,
            InFlightCache inFlightCache, LogProvider logProvider, boolean supportPreVoting, boolean refuseToBeLeader )
    {
        this.leaderInfo = LeaderInfo.INITIAL;
        this.votesForMe = new HashSet();
        this.preVotesForMe = new HashSet();
        this.heartbeatResponses = new HashSet();
        this.followerStates = new FollowerStates();
        this.leaderCommit = -1L;
        this.commitIndex = -1L;
        this.lastLogIndexBeforeWeBecameLeader = -1L;
        this.myself = myself;
        this.termStorage = termStorage;
        this.voteStorage = voteStorage;
        this.membership = membership;
        this.entryLog = entryLog;
        this.inFlightCache = inFlightCache;
        this.supportPreVoting = supportPreVoting;
        this.log = logProvider.getLog( this.getClass() );
        this.isPreElection = supportPreVoting;
        this.refuseToBeLeader = refuseToBeLeader;
    }

    public MemberId myself()
    {
        return this.myself;
    }

    public Set<MemberId> votingMembers()
    {
        return this.membership.votingMembers();
    }

    public Set<MemberId> replicationMembers()
    {
        return this.membership.replicationMembers();
    }

    public long term()
    {
        return this.termState().currentTerm();
    }

    private TermState termState()
    {
        if ( this.termState == null )
        {
            this.termState = (TermState) this.termStorage.getInitialState();
        }

        return this.termState;
    }

    public MemberId leader()
    {
        return this.leader;
    }

    public LeaderInfo leaderInfo()
    {
        return this.leaderInfo;
    }

    public long leaderCommit()
    {
        return this.leaderCommit;
    }

    public MemberId votedFor()
    {
        return this.voteState().votedFor();
    }

    private VoteState voteState()
    {
        if ( this.voteState == null )
        {
            this.voteState = (VoteState) this.voteStorage.getInitialState();
        }

        return this.voteState;
    }

    public Set<MemberId> votesForMe()
    {
        return this.votesForMe;
    }

    public Set<MemberId> heartbeatResponses()
    {
        return this.heartbeatResponses;
    }

    public long lastLogIndexBeforeWeBecameLeader()
    {
        return this.lastLogIndexBeforeWeBecameLeader;
    }

    public FollowerStates<MemberId> followerStates()
    {
        return this.followerStates;
    }

    public ReadableRaftLog entryLog()
    {
        return this.entryLog;
    }

    public long commitIndex()
    {
        return this.commitIndex;
    }

    public boolean supportPreVoting()
    {
        return this.supportPreVoting;
    }

    public boolean isPreElection()
    {
        return this.isPreElection;
    }

    public Set<MemberId> preVotesForMe()
    {
        return this.preVotesForMe;
    }

    public boolean refusesToBeLeader()
    {
        return this.refuseToBeLeader;
    }

    public void update( Outcome outcome ) throws IOException
    {
        if ( this.termState().update( outcome.getTerm() ) )
        {
            this.termStorage.writeState( this.termState() );
        }

        if ( this.voteState().update( outcome.getVotedFor(), outcome.getTerm() ) )
        {
            this.voteStorage.writeState( this.voteState() );
        }

        this.logIfLeaderChanged( outcome.getLeader() );
        this.leader = outcome.getLeader();
        this.leaderInfo = new LeaderInfo( outcome.getLeader(), outcome.getTerm() );
        this.leaderCommit = outcome.getLeaderCommit();
        this.votesForMe = outcome.getVotesForMe();
        this.preVotesForMe = outcome.getPreVotesForMe();
        this.heartbeatResponses = outcome.getHeartbeatResponses();
        this.lastLogIndexBeforeWeBecameLeader = outcome.getLastLogIndexBeforeWeBecameLeader();
        this.followerStates = outcome.getFollowerStates();
        this.isPreElection = outcome.isPreElection();
        Iterator var2 = outcome.getLogCommands().iterator();

        while ( var2.hasNext() )
        {
            RaftLogCommand logCommand = (RaftLogCommand) var2.next();
            logCommand.applyTo( this.entryLog, this.log );
            logCommand.applyTo( this.inFlightCache, this.log );
        }

        this.commitIndex = outcome.getCommitIndex();
    }

    private void logIfLeaderChanged( MemberId leader )
    {
        if ( this.leader == null )
        {
            if ( leader != null )
            {
                this.log.info( "First leader elected: %s", new Object[]{leader} );
            }
        }
        else
        {
            if ( !this.leader.equals( leader ) )
            {
                this.log.info( "Leader changed from %s to %s", new Object[]{this.leader, leader} );
            }
        }
    }

    public ExposedRaftState copy()
    {
        return new RaftState.ReadOnlyRaftState( this.leaderCommit(), this.commitIndex(), this.entryLog().appendIndex(), this.lastLogIndexBeforeWeBecameLeader(),
                this.term(), this.votingMembers() );
    }

    private class ReadOnlyRaftState implements ExposedRaftState
    {
        final long leaderCommit;
        final long commitIndex;
        final long appendIndex;
        final long lastLogIndexBeforeWeBecameLeader;
        final long term;
        final Set<MemberId> votingMembers;

        private ReadOnlyRaftState( long leaderCommit, long commitIndex, long appendIndex, long lastLogIndexBeforeWeBecameLeader, long term,
                Set<MemberId> votingMembers )
        {
            this.leaderCommit = leaderCommit;
            this.commitIndex = commitIndex;
            this.appendIndex = appendIndex;
            this.lastLogIndexBeforeWeBecameLeader = lastLogIndexBeforeWeBecameLeader;
            this.term = term;
            this.votingMembers = votingMembers;
        }

        public long lastLogIndexBeforeWeBecameLeader()
        {
            return this.lastLogIndexBeforeWeBecameLeader;
        }

        public long leaderCommit()
        {
            return this.leaderCommit;
        }

        public long commitIndex()
        {
            return this.commitIndex;
        }

        public long appendIndex()
        {
            return this.appendIndex;
        }

        public long term()
        {
            return this.term;
        }

        public Set<MemberId> votingMembers()
        {
            return this.votingMembers;
        }
    }
}
