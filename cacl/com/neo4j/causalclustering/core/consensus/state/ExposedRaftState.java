package com.neo4j.causalclustering.core.consensus.state;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;

public interface ExposedRaftState
{
    long lastLogIndexBeforeWeBecameLeader();

    long leaderCommit();

    long commitIndex();

    long appendIndex();

    long term();

    Set<MemberId> votingMembers();
}
