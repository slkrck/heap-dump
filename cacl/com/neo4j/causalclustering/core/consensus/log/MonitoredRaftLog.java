package com.neo4j.causalclustering.core.consensus.log;

import com.neo4j.causalclustering.core.consensus.log.monitoring.RaftLogAppendIndexMonitor;

import java.io.IOException;

import org.neo4j.monitoring.Monitors;

public class MonitoredRaftLog extends DelegatingRaftLog
{
    private final RaftLogAppendIndexMonitor appendIndexMonitor;

    public MonitoredRaftLog( RaftLog delegate, Monitors monitors )
    {
        super( delegate );
        this.appendIndexMonitor = (RaftLogAppendIndexMonitor) monitors.newMonitor( RaftLogAppendIndexMonitor.class, new String[]{this.getClass().getName()} );
    }

    public long append( RaftLogEntry... entries ) throws IOException
    {
        long appendIndex = super.append( entries );
        this.appendIndexMonitor.appendIndex( appendIndex );
        return appendIndex;
    }

    public void truncate( long fromIndex ) throws IOException
    {
        super.truncate( fromIndex );
        this.appendIndexMonitor.appendIndex( super.appendIndex() );
    }
}
