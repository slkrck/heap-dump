package com.neo4j.causalclustering.core.consensus.log.cache;

import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;

public interface InFlightCache
{
    void enable();

    void put( long var1, RaftLogEntry var3 );

    RaftLogEntry get( long var1 );

    void truncate( long var1 );

    void prune( long var1 );

    long totalBytes();

    int elementCount();

    void reportSkippedCacheAccess();
}
