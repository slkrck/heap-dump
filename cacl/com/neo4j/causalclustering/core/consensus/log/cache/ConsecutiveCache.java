package com.neo4j.causalclustering.core.consensus.log.cache;

import com.neo4j.collection.CircularBuffer;

class ConsecutiveCache<V>
{
    private final CircularBuffer<V> circle;
    private long endIndex = -1L;

    ConsecutiveCache( int capacity )
    {
        this.circle = new CircularBuffer( capacity );
    }

    private long firstIndex()
    {
        return this.endIndex - (long) this.circle.size() + 1L;
    }

    void put( long idx, V e, V[] evictions )
    {
        if ( idx < 0L )
        {
            throw new IllegalArgumentException( String.format( "Index must be >= 0 (was %d)", idx ) );
        }
        else if ( e == null )
        {
            throw new IllegalArgumentException( "Null entries are not accepted" );
        }
        else
        {
            if ( idx == this.endIndex + 1L )
            {
                evictions[0] = this.circle.append( e );
                ++this.endIndex;
            }
            else
            {
                this.circle.clear( evictions );
                this.circle.append( e );
                this.endIndex = idx;
            }
        }
    }

    V get( long idx )
    {
        if ( idx < 0L )
        {
            throw new IllegalArgumentException( String.format( "Index must be >= 0 (was %d)", idx ) );
        }
        else
        {
            return idx <= this.endIndex && idx >= this.firstIndex() ? this.circle.read( Math.toIntExact( idx - this.firstIndex() ) ) : null;
        }
    }

    public void clear( V[] evictions )
    {
        this.circle.clear( evictions );
    }

    public int size()
    {
        return this.circle.size();
    }

    public void prune( long upToIndex, V[] evictions )
    {
        long index = this.firstIndex();

        for ( int i = 0; index <= Math.min( upToIndex, this.endIndex ); ++index )
        {
            evictions[i] = this.circle.remove();

            assert evictions[i] != null;

            ++i;
        }
    }

    public V remove()
    {
        return this.circle.remove();
    }

    public void truncate( long fromIndex, V[] evictions )
    {
        if ( fromIndex <= this.endIndex )
        {
            long index = Math.max( fromIndex, this.firstIndex() );

            for ( int var6 = 0; index <= this.endIndex; ++index )
            {
                evictions[var6++] = this.circle.removeHead();
            }

            this.endIndex = fromIndex - 1L;
        }
    }
}
