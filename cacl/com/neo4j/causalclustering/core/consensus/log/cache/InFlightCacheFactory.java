package com.neo4j.causalclustering.core.consensus.log.cache;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import org.neo4j.configuration.Config;
import org.neo4j.monitoring.Monitors;

public class InFlightCacheFactory
{
    public static InFlightCache create( Config config, Monitors monitors )
    {
        return ((InFlightCacheFactory.Type) config.get( CausalClusteringSettings.in_flight_cache_type )).create( config, monitors );
    }

    public static enum Type
    {
        NONE
                {
                    InFlightCache create( Config config, Monitors monitors )
                    {
                        return new VoidInFlightCache();
                    }
                },
        CONSECUTIVE
                {
                    InFlightCache create( Config config, Monitors monitors )
                    {
                        return new ConsecutiveInFlightCache( (Integer) config.get( CausalClusteringSettings.in_flight_cache_max_entries ),
                                (Long) config.get( CausalClusteringSettings.in_flight_cache_max_bytes ),
                                (InFlightCacheMonitor) monitors.newMonitor( InFlightCacheMonitor.class, new String[0] ), false );
                    }
                },
        UNBOUNDED
                {
                    InFlightCache create( Config config, Monitors monitors )
                    {
                        return new UnboundedInFlightCache();
                    }
                };

        abstract InFlightCache create( Config var1, Monitors var2 );
    }
}
