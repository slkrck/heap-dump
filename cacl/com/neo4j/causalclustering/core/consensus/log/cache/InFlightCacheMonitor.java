package com.neo4j.causalclustering.core.consensus.log.cache;

public interface InFlightCacheMonitor
{
    InFlightCacheMonitor VOID = new InFlightCacheMonitor()
    {
        public void miss()
        {
        }

        public void hit()
        {
        }

        public void setMaxBytes( long maxBytes )
        {
        }

        public void setTotalBytes( long totalBytes )
        {
        }

        public void setMaxElements( int maxElements )
        {
        }

        public void setElementCount( int elementCount )
        {
        }
    };

    void miss();

    void hit();

    void setMaxBytes( long var1 );

    void setTotalBytes( long var1 );

    void setMaxElements( int var1 );

    void setElementCount( int var1 );
}
