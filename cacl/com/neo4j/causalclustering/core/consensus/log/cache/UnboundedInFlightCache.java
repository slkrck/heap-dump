package com.neo4j.causalclustering.core.consensus.log.cache;

import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;

import java.util.HashMap;
import java.util.Map;

public class UnboundedInFlightCache implements InFlightCache
{
    private Map<Long,RaftLogEntry> map = new HashMap();
    private boolean enabled;

    public synchronized void enable()
    {
        this.enabled = true;
    }

    public synchronized void put( long logIndex, RaftLogEntry entry )
    {
        if ( this.enabled )
        {
            this.map.put( logIndex, entry );
        }
    }

    public synchronized RaftLogEntry get( long logIndex )
    {
        return !this.enabled ? null : (RaftLogEntry) this.map.get( logIndex );
    }

    public synchronized void truncate( long fromIndex )
    {
        if ( this.enabled )
        {
            this.map.keySet().removeIf( ( idx ) -> {
                return idx >= fromIndex;
            } );
        }
    }

    public synchronized void prune( long upToIndex )
    {
        if ( this.enabled )
        {
            this.map.keySet().removeIf( ( idx ) -> {
                return idx <= upToIndex;
            } );
        }
    }

    public synchronized long totalBytes()
    {
        return 0L;
    }

    public synchronized int elementCount()
    {
        return 0;
    }

    public void reportSkippedCacheAccess()
    {
    }
}
