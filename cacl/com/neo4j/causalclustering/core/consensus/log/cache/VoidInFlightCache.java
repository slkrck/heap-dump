package com.neo4j.causalclustering.core.consensus.log.cache;

import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;

public class VoidInFlightCache implements InFlightCache
{
    public void enable()
    {
    }

    public void put( long logIndex, RaftLogEntry entry )
    {
    }

    public RaftLogEntry get( long logIndex )
    {
        return null;
    }

    public void truncate( long fromIndex )
    {
    }

    public void prune( long upToIndex )
    {
    }

    public long totalBytes()
    {
        return 0L;
    }

    public int elementCount()
    {
        return 0;
    }

    public void reportSkippedCacheAccess()
    {
    }
}
