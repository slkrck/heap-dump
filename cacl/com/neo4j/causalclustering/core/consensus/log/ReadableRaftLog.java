package com.neo4j.causalclustering.core.consensus.log;

import java.io.IOException;

public interface ReadableRaftLog
{
    long appendIndex();

    long prevIndex();

    long readEntryTerm( long var1 ) throws IOException;

    RaftLogCursor getEntryCursor( long var1 ) throws IOException;
}
