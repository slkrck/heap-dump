package com.neo4j.causalclustering.core.consensus.log.segmented;

class SegmentedRaftLogPruner
{
    private final CoreLogPruningStrategy pruningStrategy;

    SegmentedRaftLogPruner( CoreLogPruningStrategy pruningStrategy )
    {
        this.pruningStrategy = pruningStrategy;
    }

    long getIndexToPruneFrom( long safeIndex, Segments segments )
    {
        return Math.min( safeIndex, this.pruningStrategy.getIndexToKeep( segments ) );
    }
}
