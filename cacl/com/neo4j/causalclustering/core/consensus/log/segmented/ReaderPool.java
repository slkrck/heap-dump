package com.neo4j.causalclustering.core.consensus.log.segmented;

import java.io.IOException;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class ReaderPool
{
    private final int maxSize;
    private final Log log;
    private final FileNames fileNames;
    private final FileSystemAbstraction fsa;
    private final Clock clock;
    private ArrayList<Reader> pool;

    ReaderPool( int maxSize, LogProvider logProvider, FileNames fileNames, FileSystemAbstraction fsa, Clock clock )
    {
        this.pool = new ArrayList( maxSize );
        this.maxSize = maxSize;
        this.log = logProvider.getLog( this.getClass() );
        this.fileNames = fileNames;
        this.fsa = fsa;
        this.clock = clock;
    }

    Reader acquire( long version, long byteOffset ) throws IOException
    {
        Reader reader = this.getFromPool( version );
        if ( reader == null )
        {
            reader = this.createFor( version );
        }

        reader.channel().position( byteOffset );
        return reader;
    }

    void release( Reader reader )
    {
        reader.setTimeStamp( this.clock.millis() );
        Optional<Reader> optionalOverflow = this.putInPool( reader );
        optionalOverflow.ifPresent( this::dispose );
    }

    private synchronized Reader getFromPool( long version )
    {
        Iterator itr = this.pool.iterator();

        Reader reader;
        do
        {
            if ( !itr.hasNext() )
            {
                return null;
            }

            reader = (Reader) itr.next();
        }
        while ( reader.version() != version );

        itr.remove();
        return reader;
    }

    private synchronized Optional<Reader> putInPool( Reader reader )
    {
        this.pool.add( reader );
        return this.pool.size() > this.maxSize ? Optional.of( (Reader) this.pool.remove( 0 ) ) : Optional.empty();
    }

    private Reader createFor( long version ) throws IOException
    {
        return new Reader( this.fsa, this.fileNames.getForSegment( version ), version );
    }

    synchronized void prune( long maxAge, TimeUnit unit )
    {
        if ( this.pool != null )
        {
            long endTimeMillis = this.clock.millis() - unit.toMillis( maxAge );
            Iterator itr = this.pool.iterator();

            while ( itr.hasNext() )
            {
                Reader reader = (Reader) itr.next();
                if ( reader.getTimeStamp() < endTimeMillis )
                {
                    this.dispose( reader );
                    itr.remove();
                }
            }
        }
    }

    private void dispose( Reader reader )
    {
        try
        {
            reader.close();
        }
        catch ( IOException var3 )
        {
            this.log.error( "Failed to close reader", var3 );
        }
    }

    synchronized void close() throws IOException
    {
        Iterator var1 = this.pool.iterator();

        while ( var1.hasNext() )
        {
            Reader reader = (Reader) var1.next();
            reader.close();
        }

        this.pool.clear();
        this.pool = null;
    }

    public synchronized void prune( long version )
    {
        if ( this.pool != null )
        {
            Iterator itr = this.pool.iterator();

            while ( itr.hasNext() )
            {
                Reader reader = (Reader) itr.next();
                if ( reader.version() == version )
                {
                    this.dispose( reader );
                    itr.remove();
                }
            }
        }
    }
}
