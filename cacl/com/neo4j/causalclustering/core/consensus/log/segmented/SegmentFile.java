package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.consensus.log.EntryRecord;
import com.neo4j.causalclustering.core.consensus.log.LogPosition;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.neo4j.cursor.EmptyIOCursor;
import org.neo4j.cursor.IOCursor;
import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.StoreChannel;
import org.neo4j.io.memory.ByteBuffers;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class SegmentFile implements AutoCloseable
{
    private static final SegmentHeader.Marshal headerMarshal = new SegmentHeader.Marshal();
    private final Log log;
    private final FileSystemAbstraction fileSystem;
    private final File file;
    private final ReaderPool readerPool;
    private final ChannelMarshal<ReplicatedContent> contentMarshal;
    private final PositionCache positionCache;
    private final ReferenceCounter refCount;
    private final SegmentHeader header;
    private final long version;
    private PhysicalFlushableChannel bufferedWriter;
    private ByteBuffer writeBuffer;

    SegmentFile( FileSystemAbstraction fileSystem, File file, ReaderPool readerPool, long version, ChannelMarshal<ReplicatedContent> contentMarshal,
            LogProvider logProvider, SegmentHeader header )
    {
        this.fileSystem = fileSystem;
        this.file = file;
        this.readerPool = readerPool;
        this.contentMarshal = contentMarshal;
        this.header = header;
        this.version = version;
        this.positionCache = new PositionCache( header.recordOffset() );
        this.refCount = new ReferenceCounter();
        this.log = logProvider.getLog( this.getClass() );
    }

    static SegmentFile create( FileSystemAbstraction fileSystem, File file, ReaderPool readerPool, long version,
            ChannelMarshal<ReplicatedContent> contentMarshal, LogProvider logProvider, SegmentHeader header ) throws IOException
    {
        if ( fileSystem.fileExists( file ) )
        {
            throw new IllegalStateException( "File was not expected to exist" );
        }
        else
        {
            SegmentFile segment = new SegmentFile( fileSystem, file, readerPool, version, contentMarshal, logProvider, header );
            headerMarshal.marshal( (SegmentHeader) header, segment.getOrCreateWriter() );
            segment.flush();
            return segment;
        }
    }

    IOCursor<EntryRecord> getCursor( long logIndex ) throws IOException, DisposedException
    {
        assert logIndex > this.header.prevIndex();

        if ( !this.refCount.increase() )
        {
            throw new DisposedException();
        }
        else
        {
            long offsetIndex = logIndex - (this.header.prevIndex() + 1L);
            LogPosition position = this.positionCache.lookup( offsetIndex );
            Reader reader = this.readerPool.acquire( this.version, position.byteOffset );

            try
            {
                long currentIndex = position.logIndex;
                return new EntryRecordCursor( reader, this.contentMarshal, currentIndex, offsetIndex, this );
            }
            catch ( EndOfStreamException var9 )
            {
                this.readerPool.release( reader );
                this.refCount.decrease();
                return EmptyIOCursor.empty();
            }
            catch ( IOException var10 )
            {
                reader.close();
                this.refCount.decrease();
                throw var10;
            }
        }
    }

    private synchronized PhysicalFlushableChannel getOrCreateWriter() throws IOException
    {
        if ( this.bufferedWriter == null )
        {
            if ( !this.refCount.increase() )
            {
                throw new IOException( "Writer has been closed" );
            }

            StoreChannel channel = this.fileSystem.write( this.file );
            channel.position( channel.size() );
            this.writeBuffer = ByteBuffers.allocateDirect( Math.toIntExact( ByteUnit.kibiBytes( 512L ) ) );
            this.bufferedWriter = new PhysicalFlushableChannel( channel, this.writeBuffer );
        }

        return this.bufferedWriter;
    }

    synchronized long position() throws IOException
    {
        return this.getOrCreateWriter().position();
    }

    synchronized void closeWriter()
    {
        if ( this.bufferedWriter != null )
        {
            try
            {
                this.flush();
                this.bufferedWriter.close();
            }
            catch ( IOException var5 )
            {
                this.log.error( "Failed to close writer for: " + this.file, var5 );
            }
            finally
            {
                ByteBuffers.releaseBuffer( this.writeBuffer );
                this.writeBuffer = null;
                this.bufferedWriter = null;
                this.refCount.decrease();
            }
        }
    }

    public synchronized void write( long logIndex, RaftLogEntry entry ) throws IOException
    {
        EntryRecord.write( this.getOrCreateWriter(), this.contentMarshal, logIndex, entry.term(), entry.content() );
    }

    synchronized void flush() throws IOException
    {
        this.bufferedWriter.prepareForFlush().flush();
    }

    public boolean delete()
    {
        return this.fileSystem.deleteFile( this.file );
    }

    public SegmentHeader header()
    {
        return this.header;
    }

    public long size()
    {
        return this.fileSystem.getFileSize( this.file );
    }

    String getFilename()
    {
        return this.file.getName();
    }

    boolean tryClose()
    {
        if ( this.refCount.tryDispose() )
        {
            this.close();
            return true;
        }
        else
        {
            return false;
        }
    }

    public void close()
    {
        this.closeWriter();
        this.readerPool.prune( this.version );
        if ( !this.refCount.tryDispose() )
        {
            throw new IllegalStateException( String.format( "Segment still referenced. Value: %d", this.refCount.get() ) );
        }
    }

    public String toString()
    {
        String var10000 = this.file.getName();
        return "SegmentFile{file=" + var10000 + ", header=" + this.header + "}";
    }

    ReferenceCounter refCount()
    {
        return this.refCount;
    }

    PositionCache positionCache()
    {
        return this.positionCache;
    }

    public ReaderPool readerPool()
    {
        return this.readerPool;
    }
}
