package com.neo4j.causalclustering.core.consensus.log.segmented;

import java.util.concurrent.atomic.AtomicInteger;

class ReferenceCounter
{
    private static final int DISPOSED_VALUE = -1;
    private AtomicInteger count = new AtomicInteger();

    boolean increase()
    {
        int pre;
        do
        {
            pre = this.count.get();
            if ( pre == -1 )
            {
                return false;
            }
        }
        while ( !this.count.compareAndSet( pre, pre + 1 ) );

        return true;
    }

    void decrease()
    {
        int pre;
        do
        {
            pre = this.count.get();
            if ( pre <= 0 )
            {
                throw new IllegalStateException( "Illegal count: " + pre );
            }
        }
        while ( !this.count.compareAndSet( pre, pre - 1 ) );
    }

    boolean tryDispose()
    {
        return this.count.get() == -1 || this.count.compareAndSet( 0, -1 );
    }

    public int get()
    {
        return this.count.get();
    }
}
