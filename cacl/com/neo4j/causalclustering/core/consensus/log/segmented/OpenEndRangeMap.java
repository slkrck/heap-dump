package com.neo4j.causalclustering.core.consensus.log.segmented;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

class OpenEndRangeMap<K extends Comparable<K>, V>
{
    private final TreeMap<K,V> tree = new TreeMap();
    private K endKey;
    private V endValue;

    Collection<V> replaceFrom( K from, V value )
    {
        Collection<V> removed = new ArrayList();
        Iterator itr = this.tree.tailMap( from ).values().iterator();

        while ( itr.hasNext() )
        {
            removed.add( itr.next() );
            itr.remove();
        }

        this.tree.put( from, value );
        this.endKey = from;
        this.endValue = value;
        return removed;
    }

    OpenEndRangeMap.ValueRange<K,V> lookup( K at )
    {
        if ( this.endKey != null && this.endKey.compareTo( at ) <= 0 )
        {
            return new OpenEndRangeMap.ValueRange( (Object) null, this.endValue );
        }
        else
        {
            Entry<K,V> entry = this.tree.floorEntry( at );
            return new OpenEndRangeMap.ValueRange( (Comparable) this.tree.higherKey( at ), entry != null ? entry.getValue() : null );
        }
    }

    public V last()
    {
        return this.endValue;
    }

    public Set<Entry<K,V>> entrySet()
    {
        return this.tree.entrySet();
    }

    public Collection<V> remove( K lessThan )
    {
        Collection<V> removed = new ArrayList();
        K floor = (Comparable) this.tree.floorKey( lessThan );
        Iterator itr = this.tree.headMap( floor, false ).values().iterator();

        while ( itr.hasNext() )
        {
            removed.add( itr.next() );
            itr.remove();
        }

        if ( this.tree.isEmpty() )
        {
            this.endKey = null;
            this.endValue = null;
        }

        return removed;
    }

    static class ValueRange<K, V>
    {
        private final K limit;
        private final V value;

        ValueRange( K limit, V value )
        {
            this.limit = limit;
            this.value = value;
        }

        Optional<K> limit()
        {
            return Optional.ofNullable( this.limit );
        }

        Optional<V> value()
        {
            return Optional.ofNullable( this.value );
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                OpenEndRangeMap.ValueRange<?,?> that = (OpenEndRangeMap.ValueRange) o;
                return Objects.equals( this.limit, that.limit ) && Objects.equals( this.value, that.value );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return Objects.hash( new Object[]{this.limit, this.value} );
        }
    }
}
