package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.consensus.log.LogPosition;

class PositionCache
{
    static final int CACHE_SIZE = 8;
    private final LogPosition first;
    private LogPosition[] cache = new LogPosition[8];
    private int pos;

    PositionCache( long recordOffset )
    {
        this.first = new LogPosition( 0L, recordOffset );

        for ( int i = 0; i < this.cache.length; ++i )
        {
            this.cache[i] = this.first;
        }
    }

    public synchronized void put( LogPosition position )
    {
        this.cache[this.pos] = position;
        this.pos = (this.pos + 1) % 8;
    }

    synchronized LogPosition lookup( long offsetIndex )
    {
        if ( offsetIndex == 0L )
        {
            return this.first;
        }
        else
        {
            LogPosition best = this.first;

            for ( int i = 0; i < 8; ++i )
            {
                if ( this.cache[i].logIndex <= offsetIndex && this.cache[i].logIndex > best.logIndex )
                {
                    best = this.cache[i];
                }
            }

            return best;
        }
    }
}
