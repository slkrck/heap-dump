package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.consensus.log.EntryRecord;
import com.neo4j.causalclustering.core.consensus.log.RaftLogCursor;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;

import java.io.IOException;

import org.neo4j.cursor.CursorValue;
import org.neo4j.cursor.IOCursor;

class SegmentedRaftLogCursor implements RaftLogCursor
{
    private final IOCursor<EntryRecord> inner;
    private CursorValue<RaftLogEntry> current;
    private long index;

    SegmentedRaftLogCursor( long fromIndex, IOCursor<EntryRecord> inner )
    {
        this.inner = inner;
        this.current = new CursorValue();
        this.index = fromIndex - 1L;
    }

    public boolean next() throws IOException
    {
        boolean hasNext = this.inner.next();
        if ( hasNext )
        {
            this.current.set( ((EntryRecord) this.inner.get()).logEntry() );
            ++this.index;
        }
        else
        {
            this.current.invalidate();
        }

        return hasNext;
    }

    public void close() throws IOException
    {
        this.inner.close();
    }

    public long index()
    {
        return this.index;
    }

    public RaftLogEntry get()
    {
        return (RaftLogEntry) this.current.get();
    }
}
