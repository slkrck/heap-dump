package com.neo4j.causalclustering.core.consensus.log.segmented;

import org.neo4j.function.Factory;
import org.neo4j.kernel.impl.transaction.log.pruning.ThresholdConfigParser;
import org.neo4j.kernel.impl.transaction.log.pruning.ThresholdConfigParser.ThresholdConfigValue;
import org.neo4j.logging.LogProvider;

public class CoreLogPruningStrategyFactory implements Factory<CoreLogPruningStrategy>
{
    private final String pruningStrategyConfig;
    private final LogProvider logProvider;

    public CoreLogPruningStrategyFactory( String pruningStrategyConfig, LogProvider logProvider )
    {
        this.pruningStrategyConfig = pruningStrategyConfig;
        this.logProvider = logProvider;
    }

    public CoreLogPruningStrategy newInstance()
    {
        ThresholdConfigValue thresholdConfigValue = ThresholdConfigParser.parse( this.pruningStrategyConfig );
        String type = thresholdConfigValue.type;
        long value = thresholdConfigValue.value;
        byte var6 = -1;
        switch ( type.hashCode() )
        {
        case -1591573360:
            if ( type.equals( "entries" ) )
            {
                var6 = 2;
            }
            break;
        case 115311:
            if ( type.equals( "txs" ) )
            {
                var6 = 1;
            }
            break;
        case 3076183:
            if ( type.equals( "days" ) )
            {
                var6 = 4;
            }
            break;
        case 3530753:
            if ( type.equals( "size" ) )
            {
                var6 = 0;
            }
            break;
        case 97196323:
            if ( type.equals( "false" ) )
            {
                var6 = 5;
            }
            break;
        case 99469071:
            if ( type.equals( "hours" ) )
            {
                var6 = 3;
            }
        }

        switch ( var6 )
        {
        case 0:
            return new SizeBasedLogPruningStrategy( value );
        case 1:
        case 2:
            return new EntryBasedLogPruningStrategy( value, this.logProvider );
        case 3:
        case 4:
            throw new IllegalArgumentException( "Time based pruning not supported yet for the segmented raft log, got '" + type + "'." );
        case 5:
            return new NoPruningPruningStrategy();
        default:
            throw new IllegalArgumentException(
                    "Invalid log pruning configuration value '" + value + "'. Invalid type '" + type + "', valid are files, size, txs, entries, hours, days." );
        }
    }
}
