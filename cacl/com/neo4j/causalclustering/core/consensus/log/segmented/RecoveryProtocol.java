package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.consensus.log.EntryRecord;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.Map.Entry;
import java.util.function.Function;

import org.neo4j.cursor.IOCursor;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.fs.StoreChannel;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class RecoveryProtocol
{
    private static final SegmentHeader.Marshal headerMarshal = new SegmentHeader.Marshal();
    private final FileSystemAbstraction fileSystem;
    private final FileNames fileNames;
    private final Function<Integer,ChannelMarshal<ReplicatedContent>> marshalSelector;
    private final LogProvider logProvider;
    private final Log log;
    private final ReaderPool readerPool;

    RecoveryProtocol( FileSystemAbstraction fileSystem, FileNames fileNames, ReaderPool readerPool,
            Function<Integer,ChannelMarshal<ReplicatedContent>> marshalSelector, LogProvider logProvider )
    {
        this.fileSystem = fileSystem;
        this.fileNames = fileNames;
        this.readerPool = readerPool;
        this.marshalSelector = marshalSelector;
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
    }

    private static SegmentHeader loadHeader( FileSystemAbstraction fileSystem, File file ) throws IOException, EndOfStreamException
    {
        StoreChannel channel = fileSystem.read( file );

        SegmentHeader var4;
        try
        {
            ByteBuffer buffer = ByteBuffer.allocate( SegmentHeader.CURRENT_RECORD_OFFSET );
            var4 = (SegmentHeader) headerMarshal.unmarshal( new ReadAheadChannel( channel, buffer ) );
        }
        catch ( Throwable var6 )
        {
            if ( channel != null )
            {
                try
                {
                    channel.close();
                }
                catch ( Throwable var5 )
                {
                    var6.addSuppressed( var5 );
                }
            }

            throw var6;
        }

        if ( channel != null )
        {
            channel.close();
        }

        return var4;
    }

    private static void writeHeader( FileSystemAbstraction fileSystem, File file, SegmentHeader header ) throws IOException
    {
        StoreChannel channel = fileSystem.write( file );

        try
        {
            channel.position( 0L );
            ByteBuffer buffer = ByteBuffer.allocate( SegmentHeader.CURRENT_RECORD_OFFSET );
            PhysicalFlushableChannel writer = new PhysicalFlushableChannel( channel, buffer );
            headerMarshal.marshal( (SegmentHeader) header, writer );
            writer.prepareForFlush().flush();
        }
        catch ( Throwable var7 )
        {
            if ( channel != null )
            {
                try
                {
                    channel.close();
                }
                catch ( Throwable var6 )
                {
                    var7.addSuppressed( var6 );
                }
            }

            throw var7;
        }

        if ( channel != null )
        {
            channel.close();
        }
    }

    private static void checkSegmentNumberSequence( long fileNameSegmentNumber, long expectedSegmentNumber ) throws DamagedLogStorageException
    {
        if ( fileNameSegmentNumber != expectedSegmentNumber )
        {
            throw new DamagedLogStorageException( "Segment numbers not strictly monotonic. Expected: %d but found: %d",
                    new Object[]{expectedSegmentNumber, fileNameSegmentNumber} );
        }
    }

    private static void checkSegmentNumberMatches( long headerSegmentNumber, long fileNameSegmentNumber ) throws DamagedLogStorageException
    {
        if ( headerSegmentNumber != fileNameSegmentNumber )
        {
            throw new DamagedLogStorageException( "File segment number does not match header. Expected: %d but found: %d",
                    new Object[]{headerSegmentNumber, fileNameSegmentNumber} );
        }
    }

    State run() throws IOException, DamagedLogStorageException, DisposedException
    {
        State state = new State();
        SortedMap<Long,File> files = this.fileNames.getAllFiles( this.fileSystem, this.log );
        if ( files.entrySet().isEmpty() )
        {
            state.segments =
                    new Segments( this.fileSystem, this.fileNames, this.readerPool, Collections.emptyList(), this.marshalSelector, this.logProvider, -1L );
            state.segments.rotate( -1L, -1L, -1L );
            state.terms = new Terms( -1L, -1L );
            return state;
        }
        else
        {
            List<SegmentFile> segmentFiles = new ArrayList();
            SegmentFile segment = null;
            long expectedSegmentNumber = (Long) files.firstKey();
            boolean mustRecoverLastHeader = false;
            boolean skip = true;

            for ( Iterator var9 = files.entrySet().iterator(); var9.hasNext(); ++expectedSegmentNumber )
            {
                Entry<Long,File> entry = (Entry) var9.next();
                long fileSegmentNumber = (Long) entry.getKey();
                File file = (File) entry.getValue();
                checkSegmentNumberSequence( fileSegmentNumber, expectedSegmentNumber );

                SegmentHeader header;
                try
                {
                    header = loadHeader( this.fileSystem, file );
                    checkSegmentNumberMatches( header.segmentNumber(), fileSegmentNumber );
                }
                catch ( EndOfStreamException var18 )
                {
                    if ( (Long) files.lastKey() != fileSegmentNumber )
                    {
                        throw new DamagedLogStorageException( var18, "Intermediate file with incomplete or no header found: %s", new Object[]{file} );
                    }

                    if ( files.size() == 1 )
                    {
                        throw new DamagedLogStorageException( var18, "Single file with incomplete or no header found: %s", new Object[]{file} );
                    }

                    mustRecoverLastHeader = true;
                    break;
                }

                segment = new SegmentFile( this.fileSystem, file, this.readerPool, fileSegmentNumber,
                        (ChannelMarshal) this.marshalSelector.apply( header.formatVersion() ), this.logProvider, header );
                segmentFiles.add( segment );
                if ( segment.header().prevIndex() != segment.header().prevFileLastIndex() )
                {
                    this.log.info( String.format( "Skipping from index %d to %d.", segment.header().prevFileLastIndex(), segment.header().prevIndex() + 1L ) );
                    skip = true;
                }

                if ( skip )
                {
                    state.prevIndex = segment.header().prevIndex();
                    state.prevTerm = segment.header().prevTerm();
                    skip = false;
                }
            }

            assert segment != null;

            state.appendIndex = segment.header().prevIndex();
            state.terms = new Terms( segment.header().prevIndex(), segment.header().prevTerm() );
            IOCursor cursor = segment.getCursor( segment.header().prevIndex() + 1L );

            try
            {
                while ( cursor.next() )
                {
                    EntryRecord entry = (EntryRecord) cursor.get();
                    state.appendIndex = entry.logIndex();
                    state.terms.append( state.appendIndex, entry.logEntry().term() );
                }
            }
            catch ( Throwable var17 )
            {
                if ( cursor != null )
                {
                    try
                    {
                        cursor.close();
                    }
                    catch ( Throwable var16 )
                    {
                        var17.addSuppressed( var16 );
                    }
                }

                throw var17;
            }

            if ( cursor != null )
            {
                cursor.close();
            }

            if ( mustRecoverLastHeader )
            {
                SegmentHeader header = new SegmentHeader( state.appendIndex, expectedSegmentNumber, state.appendIndex, state.terms.latest() );
                this.log.warn( "Recovering last file based on next-to-last file. " + header );
                File file = this.fileNames.getForSegment( expectedSegmentNumber );
                writeHeader( this.fileSystem, file, header );
                segment = new SegmentFile( this.fileSystem, file, this.readerPool, expectedSegmentNumber,
                        (ChannelMarshal) this.marshalSelector.apply( header.formatVersion() ), this.logProvider, header );
                segmentFiles.add( segment );
            }

            state.segments = new Segments( this.fileSystem, this.fileNames, this.readerPool, segmentFiles, this.marshalSelector, this.logProvider,
                    segment.header().segmentNumber() );
            return state;
        }
    }
}
