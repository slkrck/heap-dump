package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Function;

import org.neo4j.internal.helpers.collection.Visitor;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class Segments implements AutoCloseable
{
    private final OpenEndRangeMap<Long,SegmentFile> rangeMap = new OpenEndRangeMap();
    private final List<SegmentFile> allSegments;
    private final Log log;
    private final FileNames fileNames;
    private final Function<Integer,ChannelMarshal<ReplicatedContent>> marshalSelector;
    private final LogProvider logProvider;
    private final ReaderPool readerPool;
    private FileSystemAbstraction fileSystem;
    private long currentVersion;

    Segments( FileSystemAbstraction fileSystem, FileNames fileNames, ReaderPool readerPool, List<SegmentFile> allSegments,
            Function<Integer,ChannelMarshal<ReplicatedContent>> marshalSelector, LogProvider logProvider, long currentVersion )
    {
        this.fileSystem = fileSystem;
        this.fileNames = fileNames;
        this.allSegments = new ArrayList( allSegments );
        this.marshalSelector = marshalSelector;
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
        this.currentVersion = currentVersion;
        this.readerPool = readerPool;
        this.populateRangeMap();
    }

    private void populateRangeMap()
    {
        Iterator var1 = this.allSegments.iterator();

        while ( var1.hasNext() )
        {
            SegmentFile segment = (SegmentFile) var1.next();
            this.rangeMap.replaceFrom( segment.header().prevIndex() + 1L, segment );
        }
    }

    synchronized SegmentFile truncate( long prevFileLastIndex, long prevIndex, long prevTerm ) throws IOException
    {
        if ( prevFileLastIndex < prevIndex )
        {
            throw new IllegalArgumentException(
                    String.format( "Cannot truncate at index %d which is after current append index %d", prevIndex, prevFileLastIndex ) );
        }
        else
        {
            if ( prevFileLastIndex == prevIndex )
            {
                this.log.warn( String.format( "Truncating at current log append index %d", prevIndex ) );
            }

            return this.createNext( prevFileLastIndex, prevIndex, prevTerm );
        }
    }

    synchronized SegmentFile rotate( long prevFileLastIndex, long prevIndex, long prevTerm ) throws IOException
    {
        if ( prevFileLastIndex != prevIndex )
        {
            throw new IllegalArgumentException( String.format(
                    "Cannot rotate file and have append index go from %d to %d. Going backwards is a truncation operation, going forwards is a skip operation.",
                    prevFileLastIndex, prevIndex ) );
        }
        else
        {
            return this.createNext( prevFileLastIndex, prevIndex, prevTerm );
        }
    }

    synchronized SegmentFile skip( long prevFileLastIndex, long prevIndex, long prevTerm ) throws IOException
    {
        if ( prevFileLastIndex > prevIndex )
        {
            throw new IllegalArgumentException( String.format( "Cannot skip from index %d backwards to index %d", prevFileLastIndex, prevIndex ) );
        }
        else
        {
            if ( prevFileLastIndex == prevIndex )
            {
                this.log.warn( String.format( "Skipping at current log append index %d", prevIndex ) );
            }

            return this.createNext( prevFileLastIndex, prevIndex, prevTerm );
        }
    }

    private synchronized SegmentFile createNext( long prevFileLastIndex, long prevIndex, long prevTerm ) throws IOException
    {
        ++this.currentVersion;
        SegmentHeader header = new SegmentHeader( prevFileLastIndex, this.currentVersion, prevIndex, prevTerm );
        File file = this.fileNames.getForSegment( this.currentVersion );
        ChannelMarshal<ReplicatedContent> contentMarshal = (ChannelMarshal) this.marshalSelector.apply( header.formatVersion() );
        SegmentFile segment = SegmentFile.create( this.fileSystem, file, this.readerPool, this.currentVersion, contentMarshal, this.logProvider, header );
        segment.flush();
        this.allSegments.add( segment );
        this.rangeMap.replaceFrom( prevIndex + 1L, segment );
        return segment;
    }

    synchronized OpenEndRangeMap.ValueRange<Long,SegmentFile> getForIndex( long logIndex )
    {
        return this.rangeMap.lookup( logIndex );
    }

    synchronized SegmentFile last()
    {
        return (SegmentFile) this.rangeMap.last();
    }

    public synchronized SegmentFile prune( long pruneIndex )
    {
        Iterator<SegmentFile> itr = this.allSegments.iterator();
        SegmentFile notDisposed = (SegmentFile) itr.next();

        int firstRemaining;
        SegmentFile current;
        for ( firstRemaining = 0; itr.hasNext(); notDisposed = current )
        {
            current = (SegmentFile) itr.next();
            if ( current.header().prevFileLastIndex() > pruneIndex || !notDisposed.tryClose() )
            {
                break;
            }

            this.log.info( "Pruning %s", new Object[]{notDisposed} );
            if ( !notDisposed.delete() )
            {
                this.log.error( "Failed to delete %s", new Object[]{notDisposed} );
                break;
            }

            ++firstRemaining;
        }

        this.rangeMap.remove( notDisposed.header().prevIndex() + 1L );
        this.allSegments.subList( 0, firstRemaining ).clear();
        return notDisposed;
    }

    synchronized void visit( Visitor<SegmentFile,RuntimeException> visitor )
    {
        ListIterator<SegmentFile> itr = this.allSegments.listIterator();

        for ( boolean terminate = false; itr.hasNext() && !terminate; terminate = visitor.visit( (SegmentFile) itr.next() ) )
        {
        }
    }

    synchronized void visitBackwards( Visitor<SegmentFile,RuntimeException> visitor )
    {
        ListIterator<SegmentFile> itr = this.allSegments.listIterator( this.allSegments.size() );

        for ( boolean terminate = false; itr.hasPrevious() && !terminate; terminate = visitor.visit( (SegmentFile) itr.previous() ) )
        {
        }
    }

    public synchronized void close()
    {
        RuntimeException error = null;
        Iterator var2 = this.allSegments.iterator();

        while ( var2.hasNext() )
        {
            SegmentFile segment = (SegmentFile) var2.next();

            try
            {
                segment.close();
            }
            catch ( RuntimeException var5 )
            {
                if ( error == null )
                {
                    error = var5;
                }
                else
                {
                    error.addSuppressed( var5 );
                }
            }
        }

        if ( error != null )
        {
            throw error;
        }
    }
}
