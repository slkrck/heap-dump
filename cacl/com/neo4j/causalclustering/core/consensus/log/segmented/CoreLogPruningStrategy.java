package com.neo4j.causalclustering.core.consensus.log.segmented;

public interface CoreLogPruningStrategy
{
    long getIndexToKeep( Segments var1 );
}
