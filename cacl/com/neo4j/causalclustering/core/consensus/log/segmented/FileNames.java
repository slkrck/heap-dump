package com.neo4j.causalclustering.core.consensus.log.segmented;

import java.io.File;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.Log;

public class FileNames
{
    static final String BASE_FILE_NAME = "raft.log.";
    private static final String VERSION_MATCH = "(0|[1-9]\\d*)";
    private final File baseDirectory;
    private final Pattern logFilePattern;

    public FileNames( File baseDirectory )
    {
        this.baseDirectory = baseDirectory;
        this.logFilePattern = Pattern.compile( "raft.log.(0|[1-9]\\d*)" );
    }

    File getForSegment( long version )
    {
        return new File( this.baseDirectory, "raft.log." + version );
    }

    public SortedMap<Long,File> getAllFiles( FileSystemAbstraction fileSystem, Log log )
    {
        SortedMap<Long,File> versionFileMap = new TreeMap();
        File[] var4 = fileSystem.listFiles( this.baseDirectory );
        int var5 = var4.length;

        for ( int var6 = 0; var6 < var5; ++var6 )
        {
            File file = var4[var6];
            Matcher matcher = this.logFilePattern.matcher( file.getName() );
            if ( !matcher.matches() )
            {
                log.warn( "Found out of place file: " + file.getName() );
            }
            else
            {
                versionFileMap.put( Long.valueOf( matcher.group( 1 ) ), file );
            }
        }

        return versionFileMap;
    }
}
