package com.neo4j.causalclustering.core.consensus.log.segmented;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.StoreChannel;

public class Reader implements Closeable
{
    private final long version;
    private final StoreChannel storeChannel;
    private long timeStamp;

    Reader( FileSystemAbstraction fsa, File file, long version ) throws IOException
    {
        this.storeChannel = fsa.read( file );
        this.version = version;
    }

    public long version()
    {
        return this.version;
    }

    public StoreChannel channel()
    {
        return this.storeChannel;
    }

    public void close() throws IOException
    {
        this.storeChannel.close();
    }

    long getTimeStamp()
    {
        return this.timeStamp;
    }

    void setTimeStamp( long timeStamp )
    {
        this.timeStamp = timeStamp;
    }
}
