package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.consensus.log.EntryRecord;
import com.neo4j.causalclustering.core.consensus.log.LogPosition;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.neo4j.cursor.CursorValue;
import org.neo4j.cursor.IOCursor;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.fs.StoreChannel;
import org.neo4j.io.memory.ByteBuffers;

class EntryRecordCursor implements IOCursor<EntryRecord>
{
    private final ByteBuffer buffer;
    private final LogPosition position;
    private final CursorValue<EntryRecord> currentRecord = new CursorValue();
    private final Reader reader;
    private final SegmentFile segment;
    private ReadAheadChannel<StoreChannel> bufferedReader;
    private ChannelMarshal<ReplicatedContent> contentMarshal;
    private boolean hadError;
    private boolean closed;

    EntryRecordCursor( Reader reader, ChannelMarshal<ReplicatedContent> contentMarshal, long currentIndex, long wantedIndex, SegmentFile segment )
            throws IOException, EndOfStreamException
    {
        this.buffer = ByteBuffers.allocateDirect( ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE );
        this.bufferedReader = new ReadAheadChannel( reader.channel(), this.buffer );
        this.reader = reader;
        this.contentMarshal = contentMarshal;

        for ( this.segment = segment; currentIndex < wantedIndex; ++currentIndex )
        {
            EntryRecord.read( this.bufferedReader, contentMarshal );
        }

        this.position = new LogPosition( currentIndex, this.bufferedReader.position() );
    }

    public boolean next() throws IOException
    {
        EntryRecord entryRecord;
        try
        {
            entryRecord = EntryRecord.read( this.bufferedReader, this.contentMarshal );
        }
        catch ( EndOfStreamException var3 )
        {
            this.currentRecord.invalidate();
            return false;
        }
        catch ( IOException var4 )
        {
            this.hadError = true;
            throw var4;
        }

        this.currentRecord.set( entryRecord );
        this.position.byteOffset = this.bufferedReader.position();
        ++this.position.logIndex;
        return true;
    }

    public void close() throws IOException
    {
        if ( this.closed )
        {
            throw new IllegalStateException( "Already closed" );
        }
        else
        {
            this.bufferedReader = null;
            ByteBuffers.releaseBuffer( this.buffer );
            this.closed = true;
            this.segment.refCount().decrease();
            if ( this.hadError )
            {
                this.reader.close();
            }
            else
            {
                this.segment.positionCache().put( this.position );
                this.segment.readerPool().release( this.reader );
            }
        }
    }

    public EntryRecord get()
    {
        return (EntryRecord) this.currentRecord.get();
    }
}
