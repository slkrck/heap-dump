package com.neo4j.causalclustering.core.consensus.log.segmented;

public class State
{
    Segments segments;
    Terms terms;
    long prevIndex = -1L;
    long prevTerm = -1L;
    long appendIndex = -1L;

    public String toString()
    {
        return "State{prevIndex=" + this.prevIndex + ", prevTerm=" + this.prevTerm + ", appendIndex=" + this.appendIndex + "}";
    }
}
