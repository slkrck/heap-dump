package com.neo4j.causalclustering.core.consensus.log.segmented;

import org.neo4j.internal.helpers.collection.Visitor;

class SizeBasedLogPruningStrategy implements CoreLogPruningStrategy, Visitor<SegmentFile,RuntimeException>
{
    private final long bytesToKeep;
    private long accumulatedSize;
    private SegmentFile file;

    SizeBasedLogPruningStrategy( long bytesToKeep )
    {
        this.bytesToKeep = bytesToKeep;
    }

    public synchronized long getIndexToKeep( Segments segments )
    {
        this.accumulatedSize = 0L;
        this.file = null;
        segments.visitBackwards( this );
        return this.file != null ? this.file.header().prevIndex() + 1L : -1L;
    }

    public boolean visit( SegmentFile segment ) throws RuntimeException
    {
        if ( this.accumulatedSize < this.bytesToKeep )
        {
            this.file = segment;
            this.accumulatedSize += this.file.size();
            return false;
        }
        else
        {
            return true;
        }
    }
}
