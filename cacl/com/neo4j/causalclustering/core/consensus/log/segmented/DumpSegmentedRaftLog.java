package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.consensus.log.EntryRecord;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;
import com.neo4j.causalclustering.messaging.marshalling.CoreReplicatedContentMarshalV2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;

import org.neo4j.cursor.IOCursor;
import org.neo4j.internal.helpers.Args;
import org.neo4j.io.fs.DefaultFileSystemAbstraction;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.time.Clocks;

class DumpSegmentedRaftLog
{
    private static final String TO_FILE = "tofile";
    private static final DumpSegmentedRaftLog.Printer SYSTEM_OUT_PRINTER = new DumpSegmentedRaftLog.Printer()
    {
        public PrintStream getFor( String file )
        {
            return System.out;
        }

        public void close()
        {
        }
    };
    private final FileSystemAbstraction fileSystem;
    private ChannelMarshal<ReplicatedContent> marshal;

    private DumpSegmentedRaftLog( FileSystemAbstraction fileSystem, ChannelMarshal<ReplicatedContent> marshal )
    {
        this.fileSystem = fileSystem;
        this.marshal = marshal;
    }

    public static void main( String[] args )
    {
        Args arguments = Args.withFlags( new String[]{"tofile"} ).parse( args );
        DumpSegmentedRaftLog.Printer printer = getPrinter( arguments );

        try
        {
            Iterator var3 = arguments.orphans().iterator();

            while ( var3.hasNext() )
            {
                String fileAsString = (String) var3.next();
                System.out.println( "Reading file " + fileAsString );

                try
                {
                    DefaultFileSystemAbstraction fileSystem = new DefaultFileSystemAbstraction();

                    try
                    {
                        (new DumpSegmentedRaftLog( fileSystem, new CoreReplicatedContentMarshalV2() )).dump( fileAsString, printer.getFor( fileAsString ) );
                    }
                    catch ( Throwable var10 )
                    {
                        try
                        {
                            fileSystem.close();
                        }
                        catch ( Throwable var9 )
                        {
                            var10.addSuppressed( var9 );
                        }

                        throw var10;
                    }

                    fileSystem.close();
                }
                catch ( DisposedException | DamagedLogStorageException | IOException var11 )
                {
                    var11.printStackTrace();
                }
            }
        }
        catch ( Throwable var12 )
        {
            if ( printer != null )
            {
                try
                {
                    printer.close();
                }
                catch ( Throwable var8 )
                {
                    var12.addSuppressed( var8 );
                }
            }

            throw var12;
        }

        if ( printer != null )
        {
            printer.close();
        }
    }

    private static DumpSegmentedRaftLog.Printer getPrinter( Args args )
    {
        boolean toFile = args.getBoolean( "tofile", false, true );
        return (DumpSegmentedRaftLog.Printer) (toFile ? new DumpSegmentedRaftLog.FilePrinter() : SYSTEM_OUT_PRINTER);
    }

    private int dump( String filenameOrDirectory, PrintStream out ) throws IOException, DamagedLogStorageException, DisposedException
    {
        LogProvider logProvider = NullLogProvider.getInstance();
        int[] logsFound = new int[]{0};
        FileNames fileNames = new FileNames( new File( filenameOrDirectory ) );
        ReaderPool readerPool = new ReaderPool( 0, logProvider, fileNames, this.fileSystem, Clocks.systemClock() );
        RecoveryProtocol recoveryProtocol = new RecoveryProtocol( this.fileSystem, fileNames, readerPool, ( ignored ) -> {
            return this.marshal;
        }, logProvider );
        Segments segments = recoveryProtocol.run().segments;
        segments.visit( ( segment ) -> {
            int var10002 = logsFound[0]++;
            out.println( "=== " + segment.getFilename() + " ===" );
            SegmentHeader header = segment.header();
            out.println( header.toString() );

            try
            {
                IOCursor cursor = segment.getCursor( header.prevIndex() + 1L );

                try
                {
                    while ( cursor.next() )
                    {
                        out.println( ((EntryRecord) cursor.get()).toString() );
                    }
                }
                catch ( Throwable var8 )
                {
                    if ( cursor != null )
                    {
                        try
                        {
                            cursor.close();
                        }
                        catch ( Throwable var7 )
                        {
                            var8.addSuppressed( var7 );
                        }
                    }

                    throw var8;
                }

                if ( cursor != null )
                {
                    cursor.close();
                }

                return false;
            }
            catch ( IOException | DisposedException var9 )
            {
                var9.printStackTrace();
                System.exit( -1 );
                return true;
            }
        } );
        return logsFound[0];
    }

    interface Printer extends AutoCloseable
    {
        PrintStream getFor( String var1 ) throws FileNotFoundException;

        void close();
    }

    private static class FilePrinter implements DumpSegmentedRaftLog.Printer
    {
        private File directory;
        private PrintStream out;

        public PrintStream getFor( String file ) throws FileNotFoundException
        {
            File absoluteFile = (new File( file )).getAbsoluteFile();
            File dir = absoluteFile.isDirectory() ? absoluteFile : absoluteFile.getParentFile();
            if ( !dir.equals( this.directory ) )
            {
                this.close();
                File dumpFile = new File( dir, "dump-logical-log.txt" );
                System.out.println( "Redirecting the output to " + dumpFile.getPath() );
                this.out = new PrintStream( dumpFile );
                this.directory = dir;
            }

            return this.out;
        }

        public void close()
        {
            if ( this.out != null )
            {
                this.out.close();
            }
        }
    }
}
