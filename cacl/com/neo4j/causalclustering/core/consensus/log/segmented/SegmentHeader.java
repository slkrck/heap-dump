package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class SegmentHeader
{
    static final int CURRENT_RECORD_OFFSET;
    private static final int LEGACY_VERSION = 1;
    private static final int CURRENT_VERSION = 2;
    private static final int RECORD_OFFSET_V1 = 32;
    private static final int RECORD_OFFSET_V2 = "_NEO4J_RAFT_LOG_".length() + 8 + 32;

    static
    {
        CURRENT_RECORD_OFFSET = RECORD_OFFSET_V2;
    }

    private final int formatVersion;
    private final int recordOffset;
    private final long prevFileLastIndex;
    private final long segmentNumber;
    private final long prevIndex;
    private final long prevTerm;

    SegmentHeader( long prevFileLastIndex, long segmentNumber, long prevIndex, long prevTerm )
    {
        this( 2, CURRENT_RECORD_OFFSET, prevFileLastIndex, segmentNumber, prevIndex, prevTerm );
    }

    SegmentHeader( int formatVersion, int recordOffset, long prevFileLastIndex, long segmentNumber, long prevIndex, long prevTerm )
    {
        this.formatVersion = formatVersion;
        this.recordOffset = recordOffset;
        this.prevFileLastIndex = prevFileLastIndex;
        this.segmentNumber = segmentNumber;
        this.prevIndex = prevIndex;
        this.prevTerm = prevTerm;
    }

    int formatVersion()
    {
        return this.formatVersion;
    }

    long recordOffset()
    {
        return (long) this.recordOffset;
    }

    long prevFileLastIndex()
    {
        return this.prevFileLastIndex;
    }

    long segmentNumber()
    {
        return this.segmentNumber;
    }

    public long prevIndex()
    {
        return this.prevIndex;
    }

    public long prevTerm()
    {
        return this.prevTerm;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            SegmentHeader that = (SegmentHeader) o;
            return this.formatVersion == that.formatVersion && this.recordOffset == that.recordOffset && this.prevFileLastIndex == that.prevFileLastIndex &&
                    this.segmentNumber == that.segmentNumber && this.prevIndex == that.prevIndex && this.prevTerm == that.prevTerm;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.formatVersion, this.recordOffset, this.prevFileLastIndex, this.segmentNumber, this.prevIndex, this.prevTerm} );
    }

    public String toString()
    {
        return "SegmentHeader{formatVersion=" + this.formatVersion + ", recordOffset=" + this.recordOffset + ", prevFileLastIndex=" + this.prevFileLastIndex +
                ", segmentNumber=" + this.segmentNumber + ", prevIndex=" + this.prevIndex + ", prevTerm=" + this.prevTerm + "}";
    }

    static class Marshal extends SafeChannelMarshal<SegmentHeader>
    {
        public void marshal( SegmentHeader header, WritableChannel channel ) throws IOException
        {
            if ( header.formatVersion < 2 )
            {
                throw new IllegalArgumentException( String.format( "This software does not support writing version %s headers.", header.formatVersion ) );
            }
            else
            {
                channel.put( SegmentHeader.Magic.BYTES, SegmentHeader.Magic.BYTES.length );
                channel.putInt( header.formatVersion );
                channel.putInt( SegmentHeader.CURRENT_RECORD_OFFSET );
                channel.putLong( header.prevFileLastIndex );
                channel.putLong( header.segmentNumber );
                channel.putLong( header.prevIndex );
                channel.putLong( header.prevTerm );
            }
        }

        public SegmentHeader unmarshal0( ReadableChannel channel ) throws IOException
        {
            byte[] headBytes = new byte[SegmentHeader.Magic.LENGTH];
            channel.get( headBytes, SegmentHeader.Magic.LENGTH );
            ByteBuffer headBytesBuffer = ByteBuffer.wrap( headBytes );
            int formatVersion;
            int recordOffset;
            long prevFileLastIndex;
            long segmentNumber;
            if ( Arrays.equals( headBytesBuffer.array(), SegmentHeader.Magic.BYTES ) )
            {
                formatVersion = channel.getInt();
                recordOffset = channel.getInt();
                this.ensureValid( formatVersion, recordOffset );
                prevFileLastIndex = channel.getLong();
                segmentNumber = channel.getLong();
            }
            else
            {
                formatVersion = 1;
                recordOffset = 32;
                prevFileLastIndex = headBytesBuffer.getLong();
                segmentNumber = headBytesBuffer.getLong();
            }

            long prevIndex = channel.getLong();
            long prevTerm = channel.getLong();
            return new SegmentHeader( formatVersion, recordOffset, prevFileLastIndex, segmentNumber, prevIndex, prevTerm );
        }

        private void ensureValid( int formatVersion, int recordOffset )
        {
            if ( formatVersion != 2 )
            {
                throw new IllegalStateException( String.format( "Unsupported format version %s", formatVersion ) );
            }
            else if ( recordOffset != SegmentHeader.CURRENT_RECORD_OFFSET )
            {
                throw new IllegalStateException( "Invalid record offset" );
            }
        }
    }

    private static final class Magic
    {
        static final String STRING = "_NEO4J_RAFT_LOG_";
        static final byte[] BYTES;
        static final int LENGTH;

        static
        {
            BYTES = "_NEO4J_RAFT_LOG_".getBytes( StandardCharsets.UTF_8 );
            LENGTH = BYTES.length;
        }
    }
}
