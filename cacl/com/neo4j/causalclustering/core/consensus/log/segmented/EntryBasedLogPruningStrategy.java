package com.neo4j.causalclustering.core.consensus.log.segmented;

import org.neo4j.internal.helpers.collection.Visitor;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class EntryBasedLogPruningStrategy implements CoreLogPruningStrategy
{
    private final long entriesToKeep;
    private final Log log;

    EntryBasedLogPruningStrategy( long entriesToKeep, LogProvider logProvider )
    {
        this.entriesToKeep = entriesToKeep;
        this.log = logProvider.getLog( this.getClass() );
    }

    public long getIndexToKeep( Segments segments )
    {
        EntryBasedLogPruningStrategy.SegmentVisitor visitor = new EntryBasedLogPruningStrategy.SegmentVisitor();
        segments.visitBackwards( visitor );
        if ( visitor.visitedCount == 0L )
        {
            this.log.warn(
                    "No log files found during the prune operation. This state should resolve on its own, but if this warning continues, you may want to look for other errors in the user log." );
        }

        return visitor.prevIndex;
    }

    private class SegmentVisitor implements Visitor<SegmentFile,RuntimeException>
    {
        long visitedCount;
        long accumulated;
        long prevIndex = -1L;
        long lastPrevIndex = -1L;

        public boolean visit( SegmentFile segment ) throws RuntimeException
        {
            ++this.visitedCount;
            if ( this.lastPrevIndex == -1L )
            {
                this.lastPrevIndex = segment.header().prevIndex();
                return false;
            }
            else
            {
                this.prevIndex = segment.header().prevIndex();
                this.accumulated += this.lastPrevIndex - this.prevIndex;
                this.lastPrevIndex = this.prevIndex;
                return this.accumulated >= EntryBasedLogPruningStrategy.this.entriesToKeep;
            }
        }
    }
}
