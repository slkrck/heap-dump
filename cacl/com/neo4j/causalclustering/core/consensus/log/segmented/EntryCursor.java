package com.neo4j.causalclustering.core.consensus.log.segmented;

import com.neo4j.causalclustering.core.consensus.log.EntryRecord;

import java.io.IOException;
import java.util.Optional;

import org.neo4j.cursor.CursorValue;
import org.neo4j.cursor.IOCursor;

class EntryCursor implements IOCursor<EntryRecord>
{
    private final Segments segments;
    private IOCursor<EntryRecord> cursor;
    private OpenEndRangeMap.ValueRange<Long,SegmentFile> segmentRange;
    private long currentIndex;
    private long limit = Long.MAX_VALUE;
    private CursorValue<EntryRecord> currentRecord = new CursorValue();

    EntryCursor( Segments segments, long logIndex )
    {
        this.segments = segments;
        this.currentIndex = logIndex - 1L;
    }

    public boolean next() throws IOException
    {
        ++this.currentIndex;
        if ( (this.segmentRange == null || this.currentIndex >= this.limit) && !this.nextSegment() )
        {
            return false;
        }
        else if ( this.cursor.next() )
        {
            this.currentRecord.set( (EntryRecord) this.cursor.get() );
            return true;
        }
        else
        {
            this.currentRecord.invalidate();
            return false;
        }
    }

    private boolean nextSegment() throws IOException
    {
        this.segmentRange = this.segments.getForIndex( this.currentIndex );
        Optional<SegmentFile> optionalFile = this.segmentRange.value();
        if ( !optionalFile.isPresent() )
        {
            this.currentRecord.invalidate();
            return false;
        }
        else
        {
            SegmentFile file = (SegmentFile) optionalFile.get();
            IOCursor oldCursor = this.cursor;

            try
            {
                this.cursor = file.getCursor( this.currentIndex );
            }
            catch ( DisposedException var5 )
            {
                this.currentRecord.invalidate();
                return false;
            }

            if ( oldCursor != null )
            {
                oldCursor.close();
            }

            this.limit = (Long) this.segmentRange.limit().orElse( Long.MAX_VALUE );
            return true;
        }
    }

    public void close() throws IOException
    {
        if ( this.cursor != null )
        {
            this.cursor.close();
        }
    }

    public EntryRecord get()
    {
        return (EntryRecord) this.currentRecord.get();
    }
}
