package com.neo4j.causalclustering.core.consensus.log.segmented;

class NoPruningPruningStrategy implements CoreLogPruningStrategy
{
    public long getIndexToKeep( Segments segments )
    {
        return -1L;
    }
}
