package com.neo4j.causalclustering.core.consensus.log.segmented;

import org.neo4j.exceptions.KernelException;
import org.neo4j.kernel.api.exceptions.Status.General;

public class DamagedLogStorageException extends KernelException
{
    public DamagedLogStorageException( String format, Object... args )
    {
        super( General.StorageDamageDetected, format, args );
    }

    public DamagedLogStorageException( Throwable cause, String format, Object... args )
    {
        super( General.StorageDamageDetected, cause, format, args );
    }
}
