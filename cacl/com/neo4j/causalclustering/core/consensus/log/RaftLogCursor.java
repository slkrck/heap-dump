package com.neo4j.causalclustering.core.consensus.log;

import java.io.IOException;

import org.neo4j.cursor.RawCursor;

public interface RaftLogCursor extends RawCursor<RaftLogEntry,Exception>
{
    static RaftLogCursor empty()
    {
        return new RaftLogCursor()
        {
            public boolean next()
            {
                return false;
            }

            public void close()
            {
            }

            public long index()
            {
                return -1L;
            }

            public RaftLogEntry get()
            {
                throw new IllegalStateException();
            }
        };
    }

    boolean next() throws IOException;

    void close() throws IOException;

    long index();
}
