package com.neo4j.causalclustering.core.consensus.log;

import java.util.Objects;
import java.util.Set;
import java.util.function.LongPredicate;

import org.neo4j.internal.helpers.collection.LruCache;

public class RaftLogMetadataCache
{
    private final LruCache<Long,RaftLogMetadataCache.RaftLogEntryMetadata> raftLogEntryCache;

    public RaftLogMetadataCache( int logEntryCacheSize )
    {
        this.raftLogEntryCache = new LruCache( "Raft log entry cache", logEntryCacheSize );
    }

    public void clear()
    {
        this.raftLogEntryCache.clear();
    }

    public RaftLogMetadataCache.RaftLogEntryMetadata getMetadata( long logIndex )
    {
        return (RaftLogMetadataCache.RaftLogEntryMetadata) this.raftLogEntryCache.get( logIndex );
    }

    public RaftLogMetadataCache.RaftLogEntryMetadata cacheMetadata( long logIndex, long entryTerm, org.neo4j.kernel.impl.transaction.log.LogPosition position )
    {
        RaftLogMetadataCache.RaftLogEntryMetadata result = new RaftLogMetadataCache.RaftLogEntryMetadata( entryTerm, position );
        this.raftLogEntryCache.put( logIndex, result );
        return result;
    }

    public void removeUpTo( long upTo )
    {
        this.remove( ( key ) -> {
            return key <= upTo;
        } );
    }

    public void removeUpwardsFrom( long startingFrom )
    {
        this.remove( ( key ) -> {
            return key >= startingFrom;
        } );
    }

    private void remove( LongPredicate predicate )
    {
        Set var10000 = this.raftLogEntryCache.keySet();
        Objects.requireNonNull( predicate );
        var10000.removeIf( predicate::test );
    }

    public static class RaftLogEntryMetadata
    {
        private final long entryTerm;
        private final org.neo4j.kernel.impl.transaction.log.LogPosition startPosition;

        public RaftLogEntryMetadata( long entryTerm, org.neo4j.kernel.impl.transaction.log.LogPosition startPosition )
        {
            Objects.requireNonNull( startPosition );
            this.entryTerm = entryTerm;
            this.startPosition = startPosition;
        }

        public long getEntryTerm()
        {
            return this.entryTerm;
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                RaftLogMetadataCache.RaftLogEntryMetadata that = (RaftLogMetadataCache.RaftLogEntryMetadata) o;
                return this.entryTerm != that.entryTerm ? false : this.startPosition.equals( that.startPosition );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            int result = (int) (this.entryTerm ^ this.entryTerm >>> 32);
            result = 31 * result + this.startPosition.hashCode();
            return result;
        }

        public String toString()
        {
            return "RaftLogEntryMetadata{entryTerm=" + this.entryTerm + ", startPosition=" + this.startPosition + "}";
        }
    }
}
