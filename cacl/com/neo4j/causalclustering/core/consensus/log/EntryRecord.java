package com.neo4j.causalclustering.core.consensus.log;

import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadPastEndException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class EntryRecord
{
    private final long logIndex;
    private final RaftLogEntry logEntry;

    public EntryRecord( long logIndex, RaftLogEntry logEntry )
    {
        this.logIndex = logIndex;
        this.logEntry = logEntry;
    }

    public static EntryRecord read( ReadableChannel channel, ChannelMarshal<ReplicatedContent> contentMarshal ) throws IOException, EndOfStreamException
    {
        try
        {
            long appendIndex = channel.getLong();
            long term = channel.getLong();
            ReplicatedContent content = (ReplicatedContent) contentMarshal.unmarshal( channel );
            return new EntryRecord( appendIndex, new RaftLogEntry( term, content ) );
        }
        catch ( ReadPastEndException var7 )
        {
            throw new EndOfStreamException( var7 );
        }
    }

    public static void write( WritableChannel channel, ChannelMarshal<ReplicatedContent> contentMarshal, long logIndex, long term, ReplicatedContent content )
            throws IOException
    {
        channel.putLong( logIndex );
        channel.putLong( term );
        contentMarshal.marshal( content, channel );
    }

    public RaftLogEntry logEntry()
    {
        return this.logEntry;
    }

    public long logIndex()
    {
        return this.logIndex;
    }

    public String toString()
    {
        return String.format( "%d: %s", this.logIndex, this.logEntry );
    }
}
