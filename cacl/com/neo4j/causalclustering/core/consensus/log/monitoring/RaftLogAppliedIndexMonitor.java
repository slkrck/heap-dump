package com.neo4j.causalclustering.core.consensus.log.monitoring;

public interface RaftLogAppliedIndexMonitor
{
    long appliedIndex();

    void appliedIndex( long var1 );
}
