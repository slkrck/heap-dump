package com.neo4j.causalclustering.core.consensus.log.monitoring;

public interface RaftLogAppendIndexMonitor
{
    long appendIndex();

    void appendIndex( long var1 );
}
