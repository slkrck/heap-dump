package com.neo4j.causalclustering.core.consensus.log.monitoring;

public interface RaftLogCommitIndexMonitor
{
    long commitIndex();

    void commitIndex( long var1 );
}
