package com.neo4j.causalclustering.core.consensus.log.monitoring;

public interface RaftTermMonitor
{
    long term();

    void term( long var1 );
}
