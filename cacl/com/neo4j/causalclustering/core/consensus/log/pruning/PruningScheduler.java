package com.neo4j.causalclustering.core.consensus.log.pruning;

import com.neo4j.causalclustering.core.state.RaftLogPruner;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;

import org.neo4j.exceptions.UnderlyingStorageException;
import org.neo4j.function.Predicates;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

public class PruningScheduler extends LifecycleAdapter
{
    private final RaftLogPruner logPruner;
    private final JobScheduler scheduler;
    private final long recurringPeriodMillis;
    private final Log log;
    private volatile JobHandle handle;
    private volatile boolean stopped;
    private volatile boolean checkPointing;
    private final Runnable job = new Runnable()
    {
        public void run()
        {
            try
            {
                PruningScheduler.this.checkPointing = true;
                if ( PruningScheduler.this.stopped )
                {
                    return;
                }

                PruningScheduler.this.logPruner.prune();
            }
            catch ( IOException var5 )
            {
                throw new UnderlyingStorageException( var5 );
            }
            finally
            {
                PruningScheduler.this.checkPointing = false;
            }

            if ( !PruningScheduler.this.stopped )
            {
                PruningScheduler.this.handle = PruningScheduler.this.scheduler.schedule( Group.RAFT_LOG_PRUNING, PruningScheduler.this.job,
                        PruningScheduler.this.recurringPeriodMillis, TimeUnit.MILLISECONDS );
            }
        }
    };
    private final BooleanSupplier checkPointingCondition = new BooleanSupplier()
    {
        public boolean getAsBoolean()
        {
            return !PruningScheduler.this.checkPointing;
        }
    };

    public PruningScheduler( RaftLogPruner logPruner, JobScheduler scheduler, long recurringPeriodMillis, LogProvider logProvider )
    {
        this.logPruner = logPruner;
        this.scheduler = scheduler;
        this.recurringPeriodMillis = recurringPeriodMillis;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void start()
    {
        this.handle = this.scheduler.schedule( Group.RAFT_LOG_PRUNING, this.job, this.recurringPeriodMillis, TimeUnit.MILLISECONDS );
    }

    public void stop()
    {
        this.log.info( "PruningScheduler stopping" );
        this.stopped = true;
        if ( this.handle != null )
        {
            this.handle.cancel();
        }

        Predicates.awaitForever( this.checkPointingCondition, 100L, TimeUnit.MILLISECONDS );
    }
}
