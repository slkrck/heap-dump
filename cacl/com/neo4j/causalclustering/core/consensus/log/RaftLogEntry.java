package com.neo4j.causalclustering.core.consensus.log;

import com.neo4j.causalclustering.core.replication.ReplicatedContent;

import java.util.Objects;

public class RaftLogEntry
{
    public static final RaftLogEntry[] empty = new RaftLogEntry[0];
    private final long term;
    private final ReplicatedContent content;

    public RaftLogEntry( long term, ReplicatedContent content )
    {
        Objects.requireNonNull( content );
        this.term = term;
        this.content = content;
    }

    public long term()
    {
        return this.term;
    }

    public ReplicatedContent content()
    {
        return this.content;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            RaftLogEntry that = (RaftLogEntry) o;
            return this.term == that.term && this.content.equals( that.content );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = (int) (this.term ^ this.term >>> 32);
        result = 31 * result + this.content.hashCode();
        return result;
    }

    public String toString()
    {
        return String.format( "{term=%d, content=%s}", this.term, this.content );
    }
}
