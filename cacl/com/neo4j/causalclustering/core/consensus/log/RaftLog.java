package com.neo4j.causalclustering.core.consensus.log;

import java.io.IOException;

public interface RaftLog extends ReadableRaftLog
{
    long append( RaftLogEntry... var1 ) throws IOException;

    void truncate( long var1 ) throws IOException;

    long prune( long var1 ) throws IOException;

    long skip( long var1, long var3 ) throws IOException;
}
