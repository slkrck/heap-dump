package com.neo4j.causalclustering.core.consensus.log;

import java.io.IOException;

public class DelegatingRaftLog implements RaftLog
{
    private final RaftLog inner;

    public DelegatingRaftLog( RaftLog inner )
    {
        this.inner = inner;
    }

    public long append( RaftLogEntry... entry ) throws IOException
    {
        return this.inner.append( entry );
    }

    public void truncate( long fromIndex ) throws IOException
    {
        this.inner.truncate( fromIndex );
    }

    public long prune( long safeIndex ) throws IOException
    {
        return this.inner.prune( safeIndex );
    }

    public long skip( long index, long term ) throws IOException
    {
        return this.inner.skip( index, term );
    }

    public long appendIndex()
    {
        return this.inner.appendIndex();
    }

    public long prevIndex()
    {
        return this.inner.prevIndex();
    }

    public long readEntryTerm( long logIndex ) throws IOException
    {
        return this.inner.readEntryTerm( logIndex );
    }

    public RaftLogCursor getEntryCursor( long fromIndex ) throws IOException
    {
        return this.inner.getEntryCursor( fromIndex );
    }
}
