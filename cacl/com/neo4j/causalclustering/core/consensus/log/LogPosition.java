package com.neo4j.causalclustering.core.consensus.log;

import java.util.Objects;

public class LogPosition
{
    public long logIndex;
    public long byteOffset;

    public LogPosition( long logIndex, long byteOffset )
    {
        this.logIndex = logIndex;
        this.byteOffset = byteOffset;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            LogPosition position = (LogPosition) o;
            return this.logIndex == position.logIndex && this.byteOffset == position.byteOffset;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.logIndex, this.byteOffset} );
    }
}
