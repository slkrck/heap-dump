package com.neo4j.causalclustering.core.consensus.log;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class InMemoryRaftLog implements RaftLog
{
    private final Map<Long,RaftLogEntry> raftLog = new HashMap();
    private long prevIndex = -1L;
    private long prevTerm = -1L;
    private long appendIndex = -1L;
    private long commitIndex = -1L;
    private long term = -1L;

    public synchronized long append( RaftLogEntry... entries )
    {
        long newAppendIndex = this.appendIndex;
        RaftLogEntry[] var4 = entries;
        int var5 = entries.length;

        for ( int var6 = 0; var6 < var5; ++var6 )
        {
            RaftLogEntry entry = var4[var6];
            newAppendIndex = this.appendSingle( entry );
        }

        return newAppendIndex;
    }

    private synchronized long appendSingle( RaftLogEntry logEntry )
    {
        Objects.requireNonNull( logEntry );
        if ( logEntry.term() >= this.term )
        {
            this.term = logEntry.term();
            ++this.appendIndex;
            this.raftLog.put( this.appendIndex, logEntry );
            return this.appendIndex;
        }
        else
        {
            throw new IllegalStateException(
                    String.format( "Non-monotonic term %d for in entry %s in term %d", logEntry.term(), logEntry.toString(), this.term ) );
        }
    }

    public synchronized long prune( long safeIndex )
    {
        if ( safeIndex > this.prevIndex )
        {
            long removeIndex = this.prevIndex + 1L;
            this.prevTerm = this.readEntryTerm( safeIndex );
            this.prevIndex = safeIndex;

            do
            {
                this.raftLog.remove( removeIndex );
                ++removeIndex;
            }
            while ( removeIndex <= safeIndex );
        }

        return this.prevIndex;
    }

    public synchronized long appendIndex()
    {
        return this.appendIndex;
    }

    public synchronized long prevIndex()
    {
        return this.prevIndex;
    }

    public synchronized long readEntryTerm( long logIndex )
    {
        if ( logIndex == this.prevIndex )
        {
            return this.prevTerm;
        }
        else
        {
            return logIndex >= this.prevIndex && logIndex <= this.appendIndex ? ((RaftLogEntry) this.raftLog.get( logIndex )).term() : -1L;
        }
    }

    public synchronized void truncate( long fromIndex )
    {
        if ( fromIndex <= this.commitIndex )
        {
            throw new IllegalArgumentException( "cannot truncate before the commit index" );
        }
        else if ( fromIndex > this.appendIndex )
        {
            throw new IllegalArgumentException( "Cannot truncate at index " + fromIndex + " when append index is " + this.appendIndex );
        }
        else
        {
            if ( fromIndex <= this.prevIndex )
            {
                this.prevIndex = -1L;
                this.prevTerm = -1L;
            }

            for ( long i = this.appendIndex; i >= fromIndex; --i )
            {
                this.raftLog.remove( i );
            }

            if ( this.appendIndex >= fromIndex )
            {
                this.appendIndex = fromIndex - 1L;
            }

            this.term = this.readEntryTerm( this.appendIndex );
        }
    }

    public synchronized RaftLogCursor getEntryCursor( final long fromIndex )
    {
        return new RaftLogCursor()
        {
            RaftLogEntry current;
            private long currentIndex = fromIndex - 1L;

            public boolean next()
            {
                ++this.currentIndex;
                synchronized ( InMemoryRaftLog.this )
                {
                    boolean hasNext = this.currentIndex <= InMemoryRaftLog.this.appendIndex;
                    if ( hasNext )
                    {
                        if ( this.currentIndex <= InMemoryRaftLog.this.prevIndex || this.currentIndex > InMemoryRaftLog.this.appendIndex )
                        {
                            return false;
                        }

                        this.current = (RaftLogEntry) InMemoryRaftLog.this.raftLog.get( this.currentIndex );
                    }
                    else
                    {
                        this.current = null;
                    }

                    return hasNext;
                }
            }

            public void close()
            {
            }

            public long index()
            {
                return this.currentIndex;
            }

            public RaftLogEntry get()
            {
                return this.current;
            }
        };
    }

    public synchronized long skip( long index, long term )
    {
        if ( index > this.appendIndex )
        {
            this.raftLog.clear();
            this.appendIndex = index;
            this.prevIndex = index;
            this.prevTerm = term;
        }

        return this.appendIndex;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            InMemoryRaftLog that = (InMemoryRaftLog) o;
            return Objects.equals( this.appendIndex, that.appendIndex ) && Objects.equals( this.commitIndex, that.commitIndex ) &&
                    Objects.equals( this.term, that.term ) && Objects.equals( this.raftLog, that.raftLog );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.raftLog, this.appendIndex, this.commitIndex, this.term} );
    }
}
