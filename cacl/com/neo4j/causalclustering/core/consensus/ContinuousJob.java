package com.neo4j.causalclustering.core.consensus;

import java.util.concurrent.ThreadFactory;

import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ContinuousJob extends LifecycleAdapter
{
    private final ContinuousJob.AbortableJob abortableJob;
    private final Log log;
    private final Thread thread;

    public ContinuousJob( ThreadFactory threadFactory, Runnable task, LogProvider logProvider, String threadNameSuffix )
    {
        this.abortableJob = new ContinuousJob.AbortableJob( task );
        this.thread = threadFactory.newThread( this.abortableJob );
        Thread var10000 = this.thread;
        String var10001 = this.thread.getName();
        var10000.setName( var10001 + "-" + threadNameSuffix );
        this.log = logProvider.getLog( this.getClass() );
    }

    public void start()
    {
        this.abortableJob.keepRunning = true;
        this.thread.start();
    }

    public void stop() throws Exception
    {
        this.log.info( "ContinuousJob " + this.thread.getName() + " stopping" );
        this.abortableJob.keepRunning = false;
        this.thread.join();
    }

    private static class AbortableJob implements Runnable
    {
        private final Runnable task;
        private volatile boolean keepRunning;

        AbortableJob( Runnable task )
        {
            this.task = task;
        }

        public void run()
        {
            while ( this.keepRunning )
            {
                this.task.run();
            }
        }
    }
}
