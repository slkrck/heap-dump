package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Objects;

public class LeaderInfo
{
    public static final LeaderInfo INITIAL = new LeaderInfo( (MemberId) null, -1L );
    private final MemberId memberId;
    private final long term;
    private final boolean isSteppingDown;

    public LeaderInfo( MemberId memberId, long term )
    {
        this( memberId, term, false );
    }

    private LeaderInfo( MemberId memberId, long term, boolean isSteppingDown )
    {
        this.memberId = memberId;
        this.term = term;
        this.isSteppingDown = isSteppingDown;
    }

    public LeaderInfo stepDown()
    {
        return new LeaderInfo( (MemberId) null, this.term, true );
    }

    public boolean isSteppingDown()
    {
        return this.isSteppingDown;
    }

    public MemberId memberId()
    {
        return this.memberId;
    }

    public long term()
    {
        return this.term;
    }

    public String toString()
    {
        return "LeaderInfo{" + this.memberId + ", term=" + this.term + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            LeaderInfo that = (LeaderInfo) o;
            return this.term == that.term && this.isSteppingDown == that.isSteppingDown && Objects.equals( this.memberId, that.memberId );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.memberId, this.term, this.isSteppingDown} );
    }
}
