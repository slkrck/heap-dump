package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.common.RaftLogImplementation;
import com.neo4j.causalclustering.common.state.ClusterStateStorageFactory;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.core.consensus.log.InMemoryRaftLog;
import com.neo4j.causalclustering.core.consensus.log.MonitoredRaftLog;
import com.neo4j.causalclustering.core.consensus.log.RaftLog;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;
import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCacheFactory;
import com.neo4j.causalclustering.core.consensus.log.segmented.CoreLogPruningStrategy;
import com.neo4j.causalclustering.core.consensus.log.segmented.CoreLogPruningStrategyFactory;
import com.neo4j.causalclustering.core.consensus.log.segmented.SegmentedRaftLog;
import com.neo4j.causalclustering.core.consensus.membership.MemberIdSetBuilder;
import com.neo4j.causalclustering.core.consensus.membership.RaftMembershipManager;
import com.neo4j.causalclustering.core.consensus.membership.RaftMembershipState;
import com.neo4j.causalclustering.core.consensus.schedule.TimerService;
import com.neo4j.causalclustering.core.consensus.shipping.RaftLogShippingManager;
import com.neo4j.causalclustering.core.consensus.state.RaftState;
import com.neo4j.causalclustering.core.consensus.term.MonitoredTermStateStorage;
import com.neo4j.causalclustering.core.consensus.term.TermState;
import com.neo4j.causalclustering.core.consensus.vote.VoteState;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.core.replication.SendToMyself;
import com.neo4j.causalclustering.core.state.ClusterStateLayout;
import com.neo4j.causalclustering.core.state.storage.StateStorage;
import com.neo4j.causalclustering.discovery.CoreTopologyService;
import com.neo4j.causalclustering.discovery.RaftCoreTopologyConnector;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Outbound;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;
import com.neo4j.causalclustering.messaging.marshalling.CoreReplicatedContentMarshalV2;

import java.io.File;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;

import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.time.Clocks;
import org.neo4j.time.SystemNanoClock;

public class RaftGroup
{
    private final MonitoredRaftLog raftLog;
    private final RaftMachine raftMachine;
    private final RaftMembershipManager raftMembershipManager;
    private final InFlightCache inFlightCache;
    private final LeaderAvailabilityTimers leaderAvailabilityTimers;

    RaftGroup( Config config, DatabaseLogService logService, FileSystemAbstraction fileSystem, JobScheduler jobScheduler, SystemNanoClock clock,
            MemberId myself, LifeSupport life, Monitors monitors, Dependencies dependencies, Outbound<MemberId,RaftMessages.RaftMessage> outbound,
            ClusterStateLayout clusterState, CoreTopologyService topologyService, ClusterStateStorageFactory storageFactory, NamedDatabaseId namedDatabaseId )
    {
        DatabaseLogProvider logProvider = logService.getInternalLogProvider();
        TimerService timerService = new TimerService( jobScheduler, logProvider );
        Map<Integer,ChannelMarshal<ReplicatedContent>> marshals = Map.of( 2, new CoreReplicatedContentMarshalV2() );
        RaftLog underlyingLog = createRaftLog( config, life, fileSystem, clusterState, marshals, logProvider, jobScheduler, namedDatabaseId );
        this.raftLog = new MonitoredRaftLog( underlyingLog, monitors );
        StateStorage<TermState> durableTermState = storageFactory.createRaftTermStorage( namedDatabaseId.name(), life, logProvider );
        StateStorage<TermState> termState = new MonitoredTermStateStorage( durableTermState, monitors );
        StateStorage<VoteState> voteState = storageFactory.createRaftVoteStorage( namedDatabaseId.name(), life, logProvider );
        StateStorage<RaftMembershipState> raftMembershipStorage = storageFactory.createRaftMembershipStorage( namedDatabaseId.name(), life, logProvider );
        this.leaderAvailabilityTimers = createElectionTiming( config, timerService, logProvider );
        SendToMyself leaderOnlyReplicator = new SendToMyself( myself, outbound );
        Integer minimumConsensusGroupSize = (Integer) config.get( CausalClusteringSettings.minimum_core_cluster_size_at_runtime );
        MemberIdSetBuilder memberSetBuilder = new MemberIdSetBuilder();
        this.raftMembershipManager =
                new RaftMembershipManager( leaderOnlyReplicator, myself, memberSetBuilder, this.raftLog, logProvider, minimumConsensusGroupSize,
                        this.leaderAvailabilityTimers.getElectionTimeoutMillis(), Clocks.systemClock(),
                        ((Duration) config.get( CausalClusteringSettings.join_catch_up_timeout )).toMillis(), raftMembershipStorage );
        dependencies.satisfyDependency( this.raftMembershipManager );
        life.add( this.raftMembershipManager );
        this.inFlightCache = InFlightCacheFactory.create( config, monitors );
        RaftLogShippingManager logShipping =
                new RaftLogShippingManager( outbound, logProvider, this.raftLog, timerService, Clocks.systemClock(), myself, this.raftMembershipManager,
                        this.leaderAvailabilityTimers.getElectionTimeoutMillis(), (Integer) config.get( CausalClusteringSettings.catchup_batch_size ),
                        (Integer) config.get( CausalClusteringSettings.log_shipping_max_lag ), this.inFlightCache );
        boolean supportsPreVoting = (Boolean) config.get( CausalClusteringSettings.enable_pre_voting );
        RaftState state =
                new RaftState( myself, termState, this.raftMembershipManager, this.raftLog, voteState, this.inFlightCache, logProvider, supportsPreVoting,
                        (Boolean) config.get( CausalClusteringSettings.refuse_to_be_leader ) );
        RaftMessageTimerResetMonitor raftMessageTimerResetMonitor =
                (RaftMessageTimerResetMonitor) monitors.newMonitor( RaftMessageTimerResetMonitor.class, new String[0] );
        RaftOutcomeApplier raftOutcomeApplier =
                new RaftOutcomeApplier( state, outbound, this.leaderAvailabilityTimers, raftMessageTimerResetMonitor, logShipping, this.raftMembershipManager,
                        logProvider );
        this.raftMachine =
                new RaftMachine( myself, this.leaderAvailabilityTimers, logProvider, this.raftMembershipManager, this.inFlightCache, raftOutcomeApplier,
                        state );
        DurationSinceLastMessageMonitor durationSinceLastMessageMonitor = new DurationSinceLastMessageMonitor( clock );
        monitors.addMonitorListener( durationSinceLastMessageMonitor, new String[0] );
        dependencies.satisfyDependency( durationSinceLastMessageMonitor );
        life.add( new RaftCoreTopologyConnector( topologyService, this.raftMachine, namedDatabaseId ) );
        life.add( logShipping );
    }

    private static LeaderAvailabilityTimers createElectionTiming( Config config, TimerService timerService, LogProvider logProvider )
    {
        Duration electionTimeout = (Duration) config.get( CausalClusteringSettings.leader_election_timeout );
        return new LeaderAvailabilityTimers( electionTimeout, electionTimeout.dividedBy( 3L ), Clocks.systemClock(), timerService, logProvider );
    }

    private static RaftLog createRaftLog( Config config, LifeSupport life, FileSystemAbstraction fileSystem, ClusterStateLayout layout,
            Map<Integer,ChannelMarshal<ReplicatedContent>> marshalSelector, LogProvider logProvider, JobScheduler scheduler, NamedDatabaseId namedDatabaseId )
    {
        RaftLogImplementation raftLogImplementation = RaftLogImplementation.valueOf( (String) config.get( CausalClusteringSettings.raft_log_implementation ) );
        switch ( raftLogImplementation )
        {
        case IN_MEMORY:
            return new InMemoryRaftLog();
        case SEGMENTED:
            long rotateAtSize = (Long) config.get( CausalClusteringSettings.raft_log_rotation_size );
            int readerPoolSize = (Integer) config.get( CausalClusteringSettings.raft_log_reader_pool_size );
            CoreLogPruningStrategy pruningStrategy =
                    (new CoreLogPruningStrategyFactory( (String) config.get( CausalClusteringSettings.raft_log_pruning_strategy ), logProvider )).newInstance();
            File directory = layout.raftLogDirectory( namedDatabaseId.name() );
            Objects.requireNonNull( marshalSelector );
            return (RaftLog) life.add(
                    new SegmentedRaftLog( fileSystem, directory, rotateAtSize, marshalSelector::get, logProvider, readerPoolSize, Clocks.systemClock(),
                            scheduler, pruningStrategy ) );
        default:
            throw new IllegalStateException( "Unknown raft log implementation: " + raftLogImplementation );
        }
    }

    public RaftLog raftLog()
    {
        return this.raftLog;
    }

    public RaftMachine raftMachine()
    {
        return this.raftMachine;
    }

    public RaftMembershipManager raftMembershipManager()
    {
        return this.raftMembershipManager;
    }

    public InFlightCache inFlightCache()
    {
        return this.inFlightCache;
    }

    public LeaderAvailabilityTimers getLeaderAvailabilityTimers()
    {
        return this.leaderAvailabilityTimers;
    }
}
