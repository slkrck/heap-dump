package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.core.consensus.outcome.Outcome;

public interface LeaderListener
{
    default void onLeaderStepDown( long stepDownTerm )
    {
    }

    void onLeaderSwitch( LeaderInfo var1 );

    default void onLeaderEvent( Outcome outcome )
    {
        outcome.stepDownTerm().ifPresent( this::onLeaderStepDown );
        this.onLeaderSwitch( new LeaderInfo( outcome.getLeader(), outcome.getTerm() ) );
    }
}
