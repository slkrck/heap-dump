package com.neo4j.causalclustering.core.consensus.roles;

import com.neo4j.causalclustering.core.consensus.MajorityIncludingSelfQuorum;
import com.neo4j.causalclustering.core.consensus.NewLeaderBarrier;
import com.neo4j.causalclustering.core.consensus.RaftMessageHandler;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.state.ReadableRaftState;

import java.io.IOException;

import org.neo4j.logging.Log;

class Candidate implements RaftMessageHandler
{
    public Outcome handle( RaftMessages.RaftMessage message, ReadableRaftState ctx, Log log ) throws IOException
    {
        return (Outcome) message.dispatch( new Candidate.Handler( ctx, log ) );
    }

    private static class Handler implements RaftMessages.Handler<Outcome,IOException>
    {
        private final ReadableRaftState ctx;
        private final Log log;
        private final Outcome outcome;

        private Handler( ReadableRaftState ctx, Log log )
        {
            this.ctx = ctx;
            this.log = log;
            this.outcome = new Outcome( Role.CANDIDATE, ctx );
        }

        public Outcome handle( RaftMessages.Heartbeat req ) throws IOException
        {
            if ( req.leaderTerm() < this.ctx.term() )
            {
                return this.outcome;
            }
            else
            {
                this.outcome.setNextRole( Role.FOLLOWER );
                this.log.info( "Moving to FOLLOWER state after receiving heartbeat from %s at term %d (I am at %d)",
                        new Object[]{req.from(), req.leaderTerm(), this.ctx.term()} );
                Heart.beat( this.ctx, this.outcome, req, this.log );
                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.AppendEntries.Request req ) throws IOException
        {
            if ( req.leaderTerm() < this.ctx.term() )
            {
                RaftMessages.AppendEntries.Response appendResponse =
                        new RaftMessages.AppendEntries.Response( this.ctx.myself(), this.ctx.term(), false, req.prevLogIndex(),
                                this.ctx.entryLog().appendIndex() );
                this.outcome.addOutgoingMessage( new RaftMessages.Directed( req.from(), appendResponse ) );
                return this.outcome;
            }
            else
            {
                this.outcome.setNextRole( Role.FOLLOWER );
                this.log.info( "Moving to FOLLOWER state after receiving append entries request from %s at term %d (I am at %d)n",
                        new Object[]{req.from(), req.leaderTerm(), this.ctx.term()} );
                Appending.handleAppendEntriesRequest( this.ctx, this.outcome, req, this.log );
                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.Vote.Response res ) throws IOException
        {
            if ( res.term() > this.ctx.term() )
            {
                this.outcome.setNextTerm( res.term() );
                this.outcome.setNextRole( Role.FOLLOWER );
                this.log.info( "Moving to FOLLOWER state after receiving vote response from %s at term %d (I am at %d)",
                        new Object[]{res.from(), res.term(), this.ctx.term()} );
                return this.outcome;
            }
            else if ( res.term() >= this.ctx.term() && res.voteGranted() )
            {
                if ( !res.from().equals( this.ctx.myself() ) )
                {
                    this.outcome.addVoteForMe( res.from() );
                }

                if ( MajorityIncludingSelfQuorum.isQuorum( this.ctx.votingMembers(), this.outcome.getVotesForMe() ) )
                {
                    this.outcome.setLeader( this.ctx.myself() );
                    Appending.appendNewEntry( this.ctx, this.outcome, new NewLeaderBarrier() );
                    Leader.sendHeartbeats( this.ctx, this.outcome );
                    this.outcome.setLastLogIndexBeforeWeBecameLeader( this.ctx.entryLog().appendIndex() );
                    this.outcome.electedLeader();
                    this.outcome.renewElectionTimeout();
                    this.outcome.setNextRole( Role.LEADER );
                    this.log.info( "Moving to LEADER state at term %d (I am %s), voted for by %s",
                            new Object[]{this.ctx.term(), this.ctx.myself(), this.outcome.getVotesForMe()} );
                }

                return this.outcome;
            }
            else
            {
                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.Vote.Request req ) throws IOException
        {
            if ( req.term() > this.ctx.term() )
            {
                this.outcome.getVotesForMe().clear();
                this.outcome.setNextRole( Role.FOLLOWER );
                this.log.info( "Moving to FOLLOWER state after receiving vote request from %s at term %d (I am at %d)",
                        new Object[]{req.from(), req.term(), this.ctx.term()} );
                Voting.handleVoteRequest( this.ctx, this.outcome, req, this.log );
                return this.outcome;
            }
            else
            {
                this.outcome.addOutgoingMessage(
                        new RaftMessages.Directed( req.from(), new RaftMessages.Vote.Response( this.ctx.myself(), this.outcome.getTerm(), false ) ) );
                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.Timeout.Election election ) throws IOException
        {
            this.log.info( "Failed to get elected. Got votes from: %s", new Object[]{this.ctx.votesForMe()} );
            if ( !Election.startRealElection( this.ctx, this.outcome, this.log ) )
            {
                this.log.info( "Moving to FOLLOWER state after failing to start election" );
                this.outcome.setNextRole( Role.FOLLOWER );
            }

            return this.outcome;
        }

        public Outcome handle( RaftMessages.PreVote.Request req ) throws IOException
        {
            if ( this.ctx.supportPreVoting() )
            {
                if ( req.term() > this.ctx.term() )
                {
                    this.outcome.getVotesForMe().clear();
                    this.outcome.setNextRole( Role.FOLLOWER );
                    this.log.info( "Moving to FOLLOWER state after receiving pre vote request from %s at term %d (I am at %d)",
                            new Object[]{req.from(), req.term(), this.ctx.term()} );
                }

                Voting.declinePreVoteRequest( this.ctx, this.outcome, req );
            }

            return this.outcome;
        }

        public Outcome handle( RaftMessages.PreVote.Response response )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.AppendEntries.Response response )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.LogCompactionInfo logCompactionInfo )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.HeartbeatResponse heartbeatResponse )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.Timeout.Heartbeat heartbeat )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.NewEntry.Request request )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.NewEntry.BatchRequest batchRequest )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.PruneRequest pruneRequest )
        {
            Pruning.handlePruneRequest( this.outcome, pruneRequest );
            return this.outcome;
        }
    }
}
