package com.neo4j.causalclustering.core.consensus.roles;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.state.ReadableRaftState;

import java.io.IOException;

import org.neo4j.logging.Log;

class Heart
{
    private Heart()
    {
    }

    static void beat( ReadableRaftState state, Outcome outcome, RaftMessages.Heartbeat request, Log log ) throws IOException
    {
        if ( request.leaderTerm() >= state.term() )
        {
            outcome.setPreElection( false );
            outcome.setNextTerm( request.leaderTerm() );
            outcome.setLeader( request.from() );
            outcome.setLeaderCommit( request.commitIndex() );
            outcome.addOutgoingMessage( new RaftMessages.Directed( request.from(), new RaftMessages.HeartbeatResponse( state.myself() ) ) );
            if ( Follower.logHistoryMatches( state, request.commitIndex(), request.commitIndexTerm() ) )
            {
                Follower.commitToLogOnUpdate( state, request.commitIndex(), request.commitIndex(), outcome );
            }
        }
    }
}
