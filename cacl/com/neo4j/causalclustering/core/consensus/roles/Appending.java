package com.neo4j.causalclustering.core.consensus.roles;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.consensus.outcome.AppendLogEntry;
import com.neo4j.causalclustering.core.consensus.outcome.BatchAppendLogEntries;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.outcome.ShipCommand;
import com.neo4j.causalclustering.core.consensus.outcome.TruncateLogCommand;
import com.neo4j.causalclustering.core.consensus.state.ReadableRaftState;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;

import java.io.IOException;
import java.util.Collection;

import org.neo4j.logging.Log;

class Appending
{
    private Appending()
    {
    }

    static void handleAppendEntriesRequest( ReadableRaftState state, Outcome outcome, RaftMessages.AppendEntries.Request request, Log log ) throws IOException
    {
        RaftMessages.AppendEntries.Response appendResponse;
        if ( request.leaderTerm() < state.term() )
        {
            appendResponse = new RaftMessages.AppendEntries.Response( state.myself(), state.term(), false, -1L, state.entryLog().appendIndex() );
            outcome.addOutgoingMessage( new RaftMessages.Directed( request.from(), appendResponse ) );
        }
        else
        {
            outcome.setPreElection( false );
            outcome.setNextTerm( request.leaderTerm() );
            outcome.setLeader( request.from() );
            outcome.setLeaderCommit( request.leaderCommit() );
            if ( !Follower.logHistoryMatches( state, request.prevLogIndex(), request.prevLogTerm() ) )
            {
                assert request.prevLogIndex() > -1L && request.prevLogTerm() > -1L;

                appendResponse = new RaftMessages.AppendEntries.Response( state.myself(), request.leaderTerm(), false, -1L, state.entryLog().appendIndex() );
                outcome.addOutgoingMessage( new RaftMessages.Directed( request.from(), appendResponse ) );
            }
            else
            {
                long baseIndex = request.prevLogIndex() + 1L;

                int offset;
                long logIndex;
                for ( offset = 0; offset < request.entries().length; ++offset )
                {
                    logIndex = baseIndex + (long) offset;
                    long logTerm = state.entryLog().readEntryTerm( logIndex );
                    if ( logIndex > state.entryLog().appendIndex() )
                    {
                        break;
                    }

                    if ( logIndex >= state.entryLog().prevIndex() && logTerm != request.entries()[offset].term() )
                    {
                        if ( logIndex <= state.commitIndex() )
                        {
                            throw new IllegalStateException(
                                    String.format( "Cannot truncate entry at index %d with term %d when commit index is at %d", logIndex, logTerm,
                                            state.commitIndex() ) );
                        }

                        outcome.addLogCommand( new TruncateLogCommand( logIndex ) );
                        break;
                    }
                }

                if ( offset < request.entries().length )
                {
                    outcome.addLogCommand( new BatchAppendLogEntries( baseIndex, offset, request.entries() ) );
                }

                Follower.commitToLogOnUpdate( state, request.prevLogIndex() + (long) request.entries().length, request.leaderCommit(), outcome );
                logIndex = request.prevLogIndex() + (long) request.entries().length;
                RaftMessages.AppendEntries.Response appendResponse =
                        new RaftMessages.AppendEntries.Response( state.myself(), request.leaderTerm(), true, logIndex, logIndex );
                outcome.addOutgoingMessage( new RaftMessages.Directed( request.from(), appendResponse ) );
            }
        }
    }

    static void appendNewEntry( ReadableRaftState ctx, Outcome outcome, ReplicatedContent content ) throws IOException
    {
        long prevLogIndex = ctx.entryLog().appendIndex();
        long prevLogTerm =
                prevLogIndex == -1L ? -1L : (prevLogIndex > ctx.lastLogIndexBeforeWeBecameLeader() ? ctx.term() : ctx.entryLog().readEntryTerm( prevLogIndex ));
        RaftLogEntry newLogEntry = new RaftLogEntry( ctx.term(), content );
        outcome.addShipCommand( new ShipCommand.NewEntries( prevLogIndex, prevLogTerm, new RaftLogEntry[]{newLogEntry} ) );
        outcome.addLogCommand( new AppendLogEntry( prevLogIndex + 1L, newLogEntry ) );
    }

    static void appendNewEntries( ReadableRaftState ctx, Outcome outcome, Collection<ReplicatedContent> contents ) throws IOException
    {
        long prevLogIndex = ctx.entryLog().appendIndex();
        long prevLogTerm =
                prevLogIndex == -1L ? -1L : (prevLogIndex > ctx.lastLogIndexBeforeWeBecameLeader() ? ctx.term() : ctx.entryLog().readEntryTerm( prevLogIndex ));
        RaftLogEntry[] raftLogEntries = (RaftLogEntry[]) contents.stream().map( ( content ) -> {
            return new RaftLogEntry( ctx.term(), content );
        } ).toArray( ( x$0 ) -> {
            return new RaftLogEntry[x$0];
        } );
        outcome.addShipCommand( new ShipCommand.NewEntries( prevLogIndex, prevLogTerm, raftLogEntries ) );
        outcome.addLogCommand( new BatchAppendLogEntries( prevLogIndex + 1L, 0, raftLogEntries ) );
    }
}
