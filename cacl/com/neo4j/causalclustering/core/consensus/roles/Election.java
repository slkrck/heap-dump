package com.neo4j.causalclustering.core.consensus.roles;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.state.ReadableRaftState;
import com.neo4j.causalclustering.identity.MemberId;

import java.io.IOException;
import java.util.Set;

import org.neo4j.logging.Log;

public class Election
{
    private Election()
    {
    }

    public static boolean startRealElection( ReadableRaftState ctx, Outcome outcome, Log log ) throws IOException
    {
        Set<MemberId> currentMembers = ctx.votingMembers();
        if ( currentMembers != null && currentMembers.contains( ctx.myself() ) )
        {
            outcome.setNextTerm( ctx.term() + 1L );
            RaftMessages.Vote.Request voteForMe = new RaftMessages.Vote.Request( ctx.myself(), outcome.getTerm(), ctx.myself(), ctx.entryLog().appendIndex(),
                    ctx.entryLog().readEntryTerm( ctx.entryLog().appendIndex() ) );
            currentMembers.stream().filter( ( member ) -> {
                return !member.equals( ctx.myself() );
            } ).forEach( ( member ) -> {
                outcome.addOutgoingMessage( new RaftMessages.Directed( member, voteForMe ) );
            } );
            outcome.setVotedFor( ctx.myself() );
            log.info( "Election started with vote request: %s and members: %s", new Object[]{voteForMe, currentMembers} );
            return true;
        }
        else
        {
            log.info( "Election attempted but not started, current members are %s, I am %s", new Object[]{currentMembers, ctx.myself()} );
            return false;
        }
    }

    public static boolean startPreElection( ReadableRaftState ctx, Outcome outcome, Log log ) throws IOException
    {
        Set<MemberId> currentMembers = ctx.votingMembers();
        if ( currentMembers != null && currentMembers.contains( ctx.myself() ) )
        {
            RaftMessages.PreVote.Request preVoteForMe =
                    new RaftMessages.PreVote.Request( ctx.myself(), outcome.getTerm(), ctx.myself(), ctx.entryLog().appendIndex(),
                            ctx.entryLog().readEntryTerm( ctx.entryLog().appendIndex() ) );
            currentMembers.stream().filter( ( member ) -> {
                return !member.equals( ctx.myself() );
            } ).forEach( ( member ) -> {
                outcome.addOutgoingMessage( new RaftMessages.Directed( member, preVoteForMe ) );
            } );
            log.info( "Pre-election started with: %s and members: %s", new Object[]{preVoteForMe, currentMembers} );
            return true;
        }
        else
        {
            log.info( "Pre-election attempted but not started, current members are %s, I am %s", new Object[]{currentMembers, ctx.myself()} );
            return false;
        }
    }
}
