package com.neo4j.causalclustering.core.consensus.roles;

import com.neo4j.causalclustering.core.consensus.Followers;
import com.neo4j.causalclustering.core.consensus.MajorityIncludingSelfQuorum;
import com.neo4j.causalclustering.core.consensus.RaftMessageHandler;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.outcome.ShipCommand;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerState;
import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerStates;
import com.neo4j.causalclustering.core.consensus.state.ReadableRaftState;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.identity.MemberId;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import org.neo4j.internal.helpers.collection.FilteringIterable;
import org.neo4j.logging.Log;

public class Leader implements RaftMessageHandler
{
    private static Iterable<MemberId> replicationTargets( ReadableRaftState ctx )
    {
        return new FilteringIterable( ctx.replicationMembers(), ( member ) -> {
            return !member.equals( ctx.myself() );
        } );
    }

    static void sendHeartbeats( ReadableRaftState ctx, Outcome outcome ) throws IOException
    {
        long commitIndex = ctx.commitIndex();
        long commitIndexTerm = ctx.entryLog().readEntryTerm( commitIndex );
        RaftMessages.Heartbeat heartbeat = new RaftMessages.Heartbeat( ctx.myself(), ctx.term(), commitIndex, commitIndexTerm );
        Iterator var7 = replicationTargets( ctx ).iterator();

        while ( var7.hasNext() )
        {
            MemberId to = (MemberId) var7.next();
            outcome.addOutgoingMessage( new RaftMessages.Directed( to, heartbeat ) );
        }
    }

    public Outcome handle( RaftMessages.RaftMessage message, ReadableRaftState ctx, Log log ) throws IOException
    {
        return (Outcome) message.dispatch( new Leader.Handler( ctx, log ) );
    }

    private static class Handler implements RaftMessages.Handler<Outcome,IOException>
    {
        private final ReadableRaftState ctx;
        private final Log log;
        private final Outcome outcome;

        Handler( ReadableRaftState ctx, Log log )
        {
            this.ctx = ctx;
            this.log = log;
            this.outcome = new Outcome( Role.LEADER, ctx );
        }

        public Outcome handle( RaftMessages.Heartbeat heartbeat ) throws IOException
        {
            if ( heartbeat.leaderTerm() < this.ctx.term() )
            {
                return this.outcome;
            }
            else
            {
                this.stepDownToFollower( this.outcome, this.ctx );
                this.log.info( "Moving to FOLLOWER state after receiving heartbeat at term %d (my term is %d) from %s",
                        new Object[]{heartbeat.leaderTerm(), this.ctx.term(), heartbeat.from()} );
                Heart.beat( this.ctx, this.outcome, heartbeat, this.log );
                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.Timeout.Heartbeat heartbeat ) throws IOException
        {
            Leader.sendHeartbeats( this.ctx, this.outcome );
            return this.outcome;
        }

        public Outcome handle( RaftMessages.HeartbeatResponse heartbeatResponse )
        {
            this.outcome.addHeartbeatResponse( heartbeatResponse.from() );
            return this.outcome;
        }

        public Outcome handle( RaftMessages.Timeout.Election election )
        {
            if ( !MajorityIncludingSelfQuorum.isQuorum( this.ctx.votingMembers().size(), this.ctx.heartbeatResponses().size() ) )
            {
                this.stepDownToFollower( this.outcome, this.ctx );
                this.log.info( "Moving to FOLLOWER state after not receiving heartbeat responses in this election timeout period. Heartbeats received: %s",
                        new Object[]{this.ctx.heartbeatResponses()} );
            }

            this.outcome.getHeartbeatResponses().clear();
            return this.outcome;
        }

        public Outcome handle( RaftMessages.AppendEntries.Request req ) throws IOException
        {
            if ( req.leaderTerm() < this.ctx.term() )
            {
                RaftMessages.AppendEntries.Response appendResponse =
                        new RaftMessages.AppendEntries.Response( this.ctx.myself(), this.ctx.term(), false, -1L, this.ctx.entryLog().appendIndex() );
                this.outcome.addOutgoingMessage( new RaftMessages.Directed( req.from(), appendResponse ) );
                return this.outcome;
            }
            else if ( req.leaderTerm() == this.ctx.term() )
            {
                throw new IllegalStateException( "Two leaders in the same term." );
            }
            else
            {
                this.stepDownToFollower( this.outcome, this.ctx );
                this.log.info( "Moving to FOLLOWER state after receiving append request at term %d (my term is %d) from %s",
                        new Object[]{req.leaderTerm(), this.ctx.term(), req.from()} );
                Appending.handleAppendEntriesRequest( this.ctx, this.outcome, req, this.log );
                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.AppendEntries.Response response ) throws IOException
        {
            if ( response.term() < this.ctx.term() )
            {
                return this.outcome;
            }
            else if ( response.term() > this.ctx.term() )
            {
                this.outcome.setNextTerm( response.term() );
                this.stepDownToFollower( this.outcome, this.ctx );
                this.log.info( "Moving to FOLLOWER state after receiving append response at term %d (my term is %d) from %s",
                        new Object[]{response.term(), this.ctx.term(), response.from()} );
                this.outcome.replaceFollowerStates( new FollowerStates() );
                return this.outcome;
            }
            else
            {
                FollowerState follower = this.ctx.followerStates().get( response.from() );
                if ( response.success() )
                {
                    assert response.matchIndex() <= this.ctx.entryLog().appendIndex();

                    boolean followerProgressed = response.matchIndex() > follower.getMatchIndex();
                    this.outcome.replaceFollowerStates( this.outcome.getFollowerStates().onSuccessResponse( response.from(),
                            Math.max( response.matchIndex(), follower.getMatchIndex() ) ) );
                    this.outcome.addShipCommand( new ShipCommand.Match( response.matchIndex(), response.from() ) );
                    boolean matchInCurrentTerm = this.ctx.entryLog().readEntryTerm( response.matchIndex() ) == this.ctx.term();
                    if ( followerProgressed && matchInCurrentTerm )
                    {
                        long quorumAppendIndex = Followers.quorumAppendIndex( this.ctx.votingMembers(), this.outcome.getFollowerStates() );
                        if ( quorumAppendIndex > this.ctx.commitIndex() )
                        {
                            this.outcome.setLeaderCommit( quorumAppendIndex );
                            this.outcome.setCommitIndex( quorumAppendIndex );
                            this.outcome.addShipCommand( new ShipCommand.CommitUpdate() );
                        }
                    }
                }
                else if ( response.appendIndex() > -1L && response.appendIndex() >= this.ctx.entryLog().prevIndex() )
                {
                    this.outcome.addShipCommand( new ShipCommand.Mismatch( response.appendIndex(), response.from() ) );
                }
                else
                {
                    RaftMessages.LogCompactionInfo compactionInfo =
                            new RaftMessages.LogCompactionInfo( this.ctx.myself(), this.ctx.term(), this.ctx.entryLog().prevIndex() );
                    RaftMessages.Directed directedCompactionInfo = new RaftMessages.Directed( response.from(), compactionInfo );
                    this.outcome.addOutgoingMessage( directedCompactionInfo );
                }

                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.Vote.Request req ) throws IOException
        {
            if ( req.term() > this.ctx.term() )
            {
                this.stepDownToFollower( this.outcome, this.ctx );
                this.log.info( "Moving to FOLLOWER state after receiving vote request at term %d (my term is %d) from %s",
                        new Object[]{req.term(), this.ctx.term(), req.from()} );
                Voting.handleVoteRequest( this.ctx, this.outcome, req, this.log );
                return this.outcome;
            }
            else
            {
                this.outcome.addOutgoingMessage(
                        new RaftMessages.Directed( req.from(), new RaftMessages.Vote.Response( this.ctx.myself(), this.ctx.term(), false ) ) );
                return this.outcome;
            }
        }

        public Outcome handle( RaftMessages.NewEntry.Request req ) throws IOException
        {
            ReplicatedContent content = req.content();
            Appending.appendNewEntry( this.ctx, this.outcome, content );
            return this.outcome;
        }

        public Outcome handle( RaftMessages.NewEntry.BatchRequest req ) throws IOException
        {
            Collection<ReplicatedContent> contents = req.contents();
            Appending.appendNewEntries( this.ctx, this.outcome, contents );
            return this.outcome;
        }

        public Outcome handle( RaftMessages.PruneRequest pruneRequest )
        {
            Pruning.handlePruneRequest( this.outcome, pruneRequest );
            return this.outcome;
        }

        public Outcome handle( RaftMessages.Vote.Response response )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.PreVote.Request req ) throws IOException
        {
            if ( this.ctx.supportPreVoting() )
            {
                if ( req.term() > this.ctx.term() )
                {
                    this.stepDownToFollower( this.outcome, this.ctx );
                    this.log.info( "Moving to FOLLOWER state after receiving pre vote request from %s at term %d (I am at %d)",
                            new Object[]{req.from(), req.term(), this.ctx.term()} );
                }

                Voting.declinePreVoteRequest( this.ctx, this.outcome, req );
            }

            return this.outcome;
        }

        public Outcome handle( RaftMessages.PreVote.Response response )
        {
            return this.outcome;
        }

        public Outcome handle( RaftMessages.LogCompactionInfo logCompactionInfo )
        {
            return this.outcome;
        }

        private void stepDownToFollower( Outcome outcome, ReadableRaftState raftState )
        {
            outcome.steppingDown( raftState.term() );
            outcome.setNextRole( Role.FOLLOWER );
            outcome.setLeader( (MemberId) null );
        }
    }
}
