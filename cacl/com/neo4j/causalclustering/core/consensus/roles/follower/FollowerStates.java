package com.neo4j.causalclustering.core.consensus.roles.follower;

import java.util.HashMap;
import java.util.Map;

public class FollowerStates<MEMBER>
{
    private final Map<MEMBER,FollowerState> states;

    public FollowerStates( FollowerStates<MEMBER> original, MEMBER updatedMember, FollowerState updatedState )
    {
        this.states = new HashMap( original.states );
        this.states.put( updatedMember, updatedState );
    }

    public FollowerStates()
    {
        this.states = new HashMap();
    }

    public FollowerState get( MEMBER member )
    {
        FollowerState result = (FollowerState) this.states.get( member );
        if ( result == null )
        {
            result = new FollowerState();
        }

        return result;
    }

    public String toString()
    {
        return String.format( "FollowerStates%s", this.states );
    }

    public int size()
    {
        return this.states.size();
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            boolean var10000;
            label35:
            {
                FollowerStates that = (FollowerStates) o;
                if ( this.states != null )
                {
                    if ( this.states.equals( that.states ) )
                    {
                        break label35;
                    }
                }
                else if ( that.states == null )
                {
                    break label35;
                }

                var10000 = false;
                return var10000;
            }

            var10000 = true;
            return var10000;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.states != null ? this.states.hashCode() : 0;
    }

    public FollowerStates<MEMBER> onSuccessResponse( MEMBER member, long newMatchIndex )
    {
        return new FollowerStates( this, member, this.get( member ).onSuccessResponse( newMatchIndex ) );
    }
}
