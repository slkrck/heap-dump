package com.neo4j.causalclustering.core.consensus.roles.follower;

public class FollowerState
{
    private final long matchIndex;

    public FollowerState()
    {
        this( -1L );
    }

    private FollowerState( long matchIndex )
    {
        assert matchIndex >= -1L : String.format( "Match index can never be less than -1. Was %d", matchIndex );

        this.matchIndex = matchIndex;
    }

    public long getMatchIndex()
    {
        return this.matchIndex;
    }

    public FollowerState onSuccessResponse( long newMatchIndex )
    {
        return new FollowerState( newMatchIndex );
    }

    public String toString()
    {
        return String.format( "State{matchIndex=%d}", this.matchIndex );
    }
}
