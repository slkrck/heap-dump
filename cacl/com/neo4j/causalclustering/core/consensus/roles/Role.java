package com.neo4j.causalclustering.core.consensus.roles;

import com.neo4j.causalclustering.core.consensus.RaftMessageHandler;

public enum Role
{
    FOLLOWER( new Follower() ),
    CANDIDATE( new Candidate() ),
    LEADER( new Leader() );

    public final RaftMessageHandler handler;

    private Role( RaftMessageHandler handler )
    {
        this.handler = handler;
    }
}
