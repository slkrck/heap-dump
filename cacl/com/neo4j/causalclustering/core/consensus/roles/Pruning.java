package com.neo4j.causalclustering.core.consensus.roles;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.outcome.PruneLogCommand;

class Pruning
{
    private Pruning()
    {
    }

    static void handlePruneRequest( Outcome outcome, RaftMessages.PruneRequest pruneRequest )
    {
        outcome.addLogCommand( new PruneLogCommand( pruneRequest.pruneIndex() ) );
    }
}
