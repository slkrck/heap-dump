package com.neo4j.causalclustering.core.consensus.roles;

@FunctionalInterface
public interface RoleProvider
{
    Role currentRole();
}
