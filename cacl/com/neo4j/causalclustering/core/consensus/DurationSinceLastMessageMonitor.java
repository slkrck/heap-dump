package com.neo4j.causalclustering.core.consensus;

import java.time.Duration;

import org.neo4j.time.Stopwatch;
import org.neo4j.time.SystemNanoClock;

public class DurationSinceLastMessageMonitor implements RaftMessageTimerResetMonitor
{
    private final SystemNanoClock clock;
    private Stopwatch lastMessage;

    public DurationSinceLastMessageMonitor( SystemNanoClock clock )
    {
        this.clock = clock;
    }

    public void timerReset()
    {
        this.lastMessage = this.clock.startStopWatch();
    }

    public Duration durationSinceLastMessage()
    {
        return this.lastMessage == null ? null : this.lastMessage.elapsed();
    }
}
