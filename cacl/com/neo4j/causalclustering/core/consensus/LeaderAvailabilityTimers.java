package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.core.consensus.schedule.TimeoutFactory;
import com.neo4j.causalclustering.core.consensus.schedule.TimeoutHandler;
import com.neo4j.causalclustering.core.consensus.schedule.Timer;
import com.neo4j.causalclustering.core.consensus.schedule.TimerService;

import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.neo4j.function.ThrowingAction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;

class LeaderAvailabilityTimers
{
    private final long electionIntervalMillis;
    private final long heartbeatIntervalMillis;
    private final Clock clock;
    private final TimerService timerService;
    private final Log log;
    private volatile long lastElectionRenewalMillis;
    private volatile Timer heartbeatTimer;
    private volatile Timer electionTimer;

    LeaderAvailabilityTimers( Duration electionTimeout, Duration heartbeatInterval, Clock clock, TimerService timerService, LogProvider logProvider )
    {
        this.electionIntervalMillis = electionTimeout.toMillis();
        this.heartbeatIntervalMillis = heartbeatInterval.toMillis();
        this.clock = clock;
        this.timerService = timerService;
        this.log = logProvider.getLog( this.getClass() );
        if ( this.electionIntervalMillis < this.heartbeatIntervalMillis )
        {
            throw new IllegalArgumentException(
                    String.format( "Election timeout %s should not be shorter than heartbeat interval %s", this.electionIntervalMillis,
                            this.heartbeatIntervalMillis ) );
        }
    }

    void start( ThrowingAction<Exception> electionAction, ThrowingAction<Exception> heartbeatAction )
    {
        this.electionTimer = this.timerService.create( RaftMachine.Timeouts.ELECTION, Group.RAFT_TIMER, this.renewing( electionAction ) );
        this.electionTimer.set( TimeoutFactory.uniformRandomTimeout( this.electionIntervalMillis, this.electionIntervalMillis * 2L, TimeUnit.MILLISECONDS ) );
        this.heartbeatTimer = this.timerService.create( RaftMachine.Timeouts.HEARTBEAT, Group.RAFT_TIMER, this.renewing( heartbeatAction ) );
        this.heartbeatTimer.set( TimeoutFactory.fixedTimeout( this.heartbeatIntervalMillis, TimeUnit.MILLISECONDS ) );
        this.lastElectionRenewalMillis = this.clock.millis();
    }

    void stop()
    {
        if ( this.electionTimer != null )
        {
            this.electionTimer.kill( Timer.CancelMode.SYNC_WAIT );
        }

        if ( this.heartbeatTimer != null )
        {
            this.heartbeatTimer.kill( Timer.CancelMode.SYNC_WAIT );
        }
    }

    void renewElection()
    {
        this.lastElectionRenewalMillis = this.clock.millis();
        if ( this.electionTimer != null )
        {
            this.electionTimer.reset();
        }
    }

    boolean isElectionTimedOut()
    {
        return this.clock.millis() - this.lastElectionRenewalMillis >= this.electionIntervalMillis;
    }

    long getElectionTimeoutMillis()
    {
        return this.electionIntervalMillis;
    }

    private TimeoutHandler renewing( ThrowingAction<Exception> action )
    {
        return ( timeout ) -> {
            try
            {
                action.apply();
            }
            catch ( Exception var4 )
            {
                this.log.error( "Failed to process timeout.", var4 );
            }

            timeout.reset();
        };
    }
}
