package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.ComposableMessageHandler;
import com.neo4j.causalclustering.messaging.LifecycleMessageHandler;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import org.neo4j.monitoring.Monitors;

public class RaftMessageMonitoringHandler implements LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>
{
    private final LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> raftMessageHandler;
    private final Clock clock;
    private final RaftMessageProcessingMonitor raftMessageDelayMonitor;

    public RaftMessageMonitoringHandler( LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> raftMessageHandler, Clock clock,
            Monitors monitors )
    {
        this.raftMessageHandler = raftMessageHandler;
        this.clock = clock;
        this.raftMessageDelayMonitor = (RaftMessageProcessingMonitor) monitors.newMonitor( RaftMessageProcessingMonitor.class, new String[0] );
    }

    public static ComposableMessageHandler composable( Clock clock, Monitors monitors )
    {
        return ( delegate ) -> {
            return new RaftMessageMonitoringHandler( delegate, clock, monitors );
        };
    }

    public synchronized void handle( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> incomingMessage )
    {
        Instant start = this.clock.instant();
        this.logDelay( incomingMessage, start );
        this.timeHandle( incomingMessage, start );
    }

    private void timeHandle( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> incomingMessage, Instant start )
    {
        boolean var7 = false;

        try
        {
            var7 = true;
            this.raftMessageHandler.handle( incomingMessage );
            var7 = false;
        }
        finally
        {
            if ( var7 )
            {
                Duration duration = Duration.between( start, this.clock.instant() );
                this.raftMessageDelayMonitor.updateTimer( incomingMessage.type(), duration );
            }
        }

        Duration duration = Duration.between( start, this.clock.instant() );
        this.raftMessageDelayMonitor.updateTimer( incomingMessage.type(), duration );
    }

    private void logDelay( RaftMessages.ReceivedInstantRaftIdAwareMessage<?> incomingMessage, Instant start )
    {
        Duration delay = Duration.between( incomingMessage.receivedAt(), start );
        this.raftMessageDelayMonitor.setDelay( delay );
    }

    public void start( RaftId raftId ) throws Exception
    {
        this.raftMessageHandler.start( raftId );
    }

    public void stop() throws Exception
    {
        this.raftMessageHandler.stop();
    }
}
