package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.messaging.Inbound;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.ChannelHandler.Sharable;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

@Sharable
public class RaftMessageNettyHandler extends SimpleChannelInboundHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>
        implements Inbound<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>
{
    private Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> actual;
    private Log log;

    public RaftMessageNettyHandler( LogProvider logProvider )
    {
        this.log = logProvider.getLog( this.getClass() );
    }

    public void registerHandler( Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> actual )
    {
        this.actual = actual;
    }

    protected void channelRead0( ChannelHandlerContext channelHandlerContext, RaftMessages.ReceivedInstantRaftIdAwareMessage<?> incomingMessage )
    {
        try
        {
            this.actual.handle( incomingMessage );
        }
        catch ( Exception var4 )
        {
            this.log.error( String.format( "Failed to process message %s", incomingMessage ), var4 );
        }
    }
}
