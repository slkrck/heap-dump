package com.neo4j.causalclustering.core.consensus;

public interface CoreMetaData
{
    boolean isLeader();
}
