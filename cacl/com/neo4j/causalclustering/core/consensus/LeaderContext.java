package com.neo4j.causalclustering.core.consensus;

public class LeaderContext
{
    public final long term;
    public final long commitIndex;

    public LeaderContext( long term, long commitIndex )
    {
        this.term = term;
        this.commitIndex = commitIndex;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            LeaderContext that = (LeaderContext) o;
            if ( this.term != that.term )
            {
                return false;
            }
            else
            {
                return this.commitIndex == that.commitIndex;
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = (int) (this.term ^ this.term >>> 32);
        result = 31 * result + (int) (this.commitIndex ^ this.commitIndex >>> 32);
        return result;
    }

    public String toString()
    {
        return String.format( "LeaderContext{term=%d, commitIndex=%d}", this.term, this.commitIndex );
    }
}
