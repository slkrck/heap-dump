package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.core.consensus.roles.follower.FollowerStates;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Followers
{
    private Followers()
    {
    }

    public static <MEMBER> long quorumAppendIndex( Set<MEMBER> votingMembers, FollowerStates<MEMBER> states )
    {
        TreeMap<Long,Integer> appendedCounts = new TreeMap();
        Iterator var3 = votingMembers.iterator();

        while ( var3.hasNext() )
        {
            MEMBER member = var3.next();
            long txId = states.get( member ).getMatchIndex();
            appendedCounts.merge( txId, 1, ( a, b ) -> {
                return a + b;
            } );
        }

        int total = 0;
        Iterator var8 = appendedCounts.descendingMap().entrySet().iterator();

        Entry entry;
        do
        {
            if ( !var8.hasNext() )
            {
                return -1L;
            }

            entry = (Entry) var8.next();
            total += (Integer) entry.getValue();
        }
        while ( !MajorityIncludingSelfQuorum.isQuorum( votingMembers.size(), total ) );

        return (Long) entry.getKey();
    }
}
