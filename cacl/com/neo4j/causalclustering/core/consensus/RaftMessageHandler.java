package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.state.ReadableRaftState;

import java.io.IOException;

import org.neo4j.logging.Log;

public interface RaftMessageHandler
{
    Outcome handle( RaftMessages.RaftMessage var1, ReadableRaftState var2, Log var3 ) throws IOException;
}
