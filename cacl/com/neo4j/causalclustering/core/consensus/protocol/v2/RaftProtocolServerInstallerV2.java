package com.neo4j.causalclustering.core.consensus.protocol.v2;

import com.neo4j.causalclustering.messaging.marshalling.v2.ContentTypeProtocol;
import com.neo4j.causalclustering.messaging.marshalling.v2.decoding.ContentTypeDispatcher;
import com.neo4j.causalclustering.messaging.marshalling.v2.decoding.DecodingDispatcher;
import com.neo4j.causalclustering.messaging.marshalling.v2.decoding.RaftMessageComposer;
import com.neo4j.causalclustering.messaging.marshalling.v2.decoding.ReplicatedContentDecoder;
import com.neo4j.causalclustering.protocol.ModifierProtocolInstaller;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ProtocolInstaller;
import com.neo4j.causalclustering.protocol.ServerNettyPipelineBuilder;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInboundHandler;

import java.time.Clock;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftProtocolServerInstallerV2 implements ProtocolInstaller<ProtocolInstaller.Orientation.Server>
{
    private static final ApplicationProtocols APPLICATION_PROTOCOL;

    static
    {
        APPLICATION_PROTOCOL = ApplicationProtocols.RAFT_2_0;
    }

    private final LogProvider logProvider;
    private final ChannelInboundHandler raftMessageHandler;
    private final NettyPipelineBuilderFactory pipelineBuilderFactory;
    private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers;
    private final Log log;

    public RaftProtocolServerInstallerV2( ChannelInboundHandler raftMessageHandler, NettyPipelineBuilderFactory pipelineBuilderFactory,
            List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers, LogProvider logProvider )
    {
        this.raftMessageHandler = raftMessageHandler;
        this.pipelineBuilderFactory = pipelineBuilderFactory;
        this.modifiers = modifiers;
        this.logProvider = logProvider;
        this.log = this.logProvider.getLog( this.getClass() );
    }

    public void install( Channel channel ) throws Exception
    {
        ContentTypeProtocol contentTypeProtocol = new ContentTypeProtocol();
        DecodingDispatcher decodingDispatcher = new DecodingDispatcher( contentTypeProtocol, this.logProvider );
        ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) this.pipelineBuilderFactory.server(
                channel, this.log ).modify( this.modifiers )).addFraming().add( "raft_content_type_dispatcher",
                new ChannelHandler[]{new ContentTypeDispatcher( contentTypeProtocol )} )).add( "raft_component_decoder",
                new ChannelHandler[]{decodingDispatcher} )).add( "raft_content_decoder",
                new ChannelHandler[]{new ReplicatedContentDecoder( contentTypeProtocol )} )).add( "raft_message_composer",
                new ChannelHandler[]{new RaftMessageComposer( Clock.systemUTC() )} )).add( "raft_handler",
                new ChannelHandler[]{this.raftMessageHandler} )).install();
    }

    public ApplicationProtocol applicationProtocol()
    {
        return APPLICATION_PROTOCOL;
    }

    public Collection<Collection<ModifierProtocol>> modifiers()
    {
        return (Collection) this.modifiers.stream().map( ModifierProtocolInstaller::protocols ).collect( Collectors.toList() );
    }

    public static class Factory extends ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Server,RaftProtocolServerInstallerV2>
    {
        public Factory( ChannelInboundHandler raftMessageHandler, NettyPipelineBuilderFactory pipelineBuilderFactory, LogProvider logProvider )
        {
            super( RaftProtocolServerInstallerV2.APPLICATION_PROTOCOL, ( modifiers ) -> {
                return new RaftProtocolServerInstallerV2( raftMessageHandler, pipelineBuilderFactory, modifiers, logProvider );
            } );
        }
    }
}
