package com.neo4j.causalclustering.core.consensus.protocol.v2;

import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentCodec;
import com.neo4j.causalclustering.messaging.marshalling.v2.encoding.ContentTypeEncoder;
import com.neo4j.causalclustering.messaging.marshalling.v2.encoding.RaftMessageContentEncoder;
import com.neo4j.causalclustering.messaging.marshalling.v2.encoding.RaftMessageEncoder;
import com.neo4j.causalclustering.protocol.ClientNettyPipelineBuilder;
import com.neo4j.causalclustering.protocol.ModifierProtocolInstaller;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ProtocolInstaller;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftProtocolClientInstallerV2 implements ProtocolInstaller<ProtocolInstaller.Orientation.Client>
{
    private static final ApplicationProtocols APPLICATION_PROTOCOL;

    static
    {
        APPLICATION_PROTOCOL = ApplicationProtocols.RAFT_2_0;
    }

    private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers;
    private final Log log;
    private final NettyPipelineBuilderFactory clientPipelineBuilderFactory;

    public RaftProtocolClientInstallerV2( NettyPipelineBuilderFactory clientPipelineBuilderFactory,
            List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers, LogProvider logProvider )
    {
        this.modifiers = modifiers;
        this.log = logProvider.getLog( this.getClass() );
        this.clientPipelineBuilderFactory = clientPipelineBuilderFactory;
    }

    public void install( Channel channel ) throws Exception
    {
        ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) this.clientPipelineBuilderFactory.client(
                channel, this.log ).modify( this.modifiers )).addFraming().add( "raft_message_encoder", new ChannelHandler[]{new RaftMessageEncoder()} )).add(
                "raft_content_type_encoder", new ChannelHandler[]{new ContentTypeEncoder()} )).add( "raft_chunked_writer",
                new ChannelHandler[]{new ChunkedWriteHandler()} )).add( "raft_message_content_encoder",
                new ChannelHandler[]{new RaftMessageContentEncoder( new ReplicatedContentCodec() )} )).install();
    }

    public ApplicationProtocol applicationProtocol()
    {
        return APPLICATION_PROTOCOL;
    }

    public Collection<Collection<ModifierProtocol>> modifiers()
    {
        return (Collection) this.modifiers.stream().map( ModifierProtocolInstaller::protocols ).collect( Collectors.toList() );
    }

    public static class Factory extends ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Client,RaftProtocolClientInstallerV2>
    {
        public Factory( NettyPipelineBuilderFactory clientPipelineBuilderFactory, LogProvider logProvider )
        {
            super( RaftProtocolClientInstallerV2.APPLICATION_PROTOCOL, ( mods ) -> {
                return new RaftProtocolClientInstallerV2( clientPipelineBuilderFactory, mods, logProvider );
            } );
        }
    }
}
