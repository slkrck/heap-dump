package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.common.state.ClusterStateStorageFactory;
import com.neo4j.causalclustering.core.state.ClusterStateLayout;
import com.neo4j.causalclustering.discovery.CoreTopologyService;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.Outbound;
import org.neo4j.collection.Dependencies;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.monitoring.Monitors;

public class RaftGroupFactory
{
    private final MemberId myself;
    private final GlobalModule globalModule;
    private final ClusterStateLayout clusterState;
    private final CoreTopologyService topologyService;
    private final ClusterStateStorageFactory storageFactory;

    public RaftGroupFactory( MemberId myself, GlobalModule globalModule, ClusterStateLayout clusterState, CoreTopologyService topologyService,
            ClusterStateStorageFactory storageFactory )
    {
        this.myself = myself;
        this.globalModule = globalModule;
        this.clusterState = clusterState;
        this.topologyService = topologyService;
        this.storageFactory = storageFactory;
    }

    public RaftGroup create( NamedDatabaseId namedDatabaseId, Outbound<MemberId,RaftMessages.RaftMessage> outbound, LifeSupport life, Monitors monitors,
            Dependencies dependencies, DatabaseLogService logService )
    {
        return new RaftGroup( this.globalModule.getGlobalConfig(), logService, this.globalModule.getFileSystem(), this.globalModule.getJobScheduler(),
                this.globalModule.getGlobalClock(), this.myself, life, monitors, dependencies, outbound, this.clusterState, this.topologyService,
                this.storageFactory, namedDatabaseId );
    }
}
