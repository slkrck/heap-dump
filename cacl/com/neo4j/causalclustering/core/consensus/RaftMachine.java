package com.neo4j.causalclustering.core.consensus;

import com.neo4j.causalclustering.core.consensus.log.cache.InFlightCache;
import com.neo4j.causalclustering.core.consensus.membership.RaftMembershipManager;
import com.neo4j.causalclustering.core.consensus.outcome.ConsensusOutcome;
import com.neo4j.causalclustering.core.consensus.outcome.Outcome;
import com.neo4j.causalclustering.core.consensus.roles.Role;
import com.neo4j.causalclustering.core.consensus.roles.RoleProvider;
import com.neo4j.causalclustering.core.consensus.schedule.TimerService;
import com.neo4j.causalclustering.core.consensus.state.ExposedRaftState;
import com.neo4j.causalclustering.core.consensus.state.RaftState;
import com.neo4j.causalclustering.core.state.snapshot.RaftCoreState;
import com.neo4j.causalclustering.error_handling.DatabasePanicEventHandler;
import com.neo4j.causalclustering.identity.MemberId;

import java.io.IOException;
import java.util.Set;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftMachine implements LeaderLocator, CoreMetaData, DatabasePanicEventHandler, RoleProvider
{
    private final InFlightCache inFlightCache;
    private final RaftOutcomeApplier outcomeApplier;
    private final RaftState state;
    private final MemberId myself;
    private final LeaderAvailabilityTimers leaderAvailabilityTimers;
    private final RaftMembershipManager membershipManager;
    private final Log log;
    private volatile Role currentRole;

    public RaftMachine( MemberId myself, LeaderAvailabilityTimers leaderAvailabilityTimers, LogProvider logProvider, RaftMembershipManager membershipManager,
            InFlightCache inFlightCache, RaftOutcomeApplier outcomeApplier, RaftState state )
    {
        this.currentRole = Role.FOLLOWER;
        this.myself = myself;
        this.leaderAvailabilityTimers = leaderAvailabilityTimers;
        this.log = logProvider.getLog( this.getClass() );
        this.membershipManager = membershipManager;
        this.inFlightCache = inFlightCache;
        this.outcomeApplier = outcomeApplier;
        this.state = state;
    }

    public void onPanic( Throwable cause )
    {
        this.stopTimers();
    }

    public void postRecoveryActions()
    {
        this.leaderAvailabilityTimers.start( this::electionTimeout, () -> {
            this.handle( new RaftMessages.Timeout.Heartbeat( this.myself ) );
        } );
        this.inFlightCache.enable();
    }

    public void stopTimers()
    {
        this.leaderAvailabilityTimers.stop();
    }

    private void electionTimeout() throws IOException
    {
        if ( this.leaderAvailabilityTimers.isElectionTimedOut() )
        {
            this.triggerElection();
        }
    }

    public void triggerElection() throws IOException
    {
        this.handle( new RaftMessages.Timeout.Election( this.myself ) );
    }

    public synchronized RaftCoreState coreState()
    {
        return new RaftCoreState( this.membershipManager.getCommitted() );
    }

    public synchronized void installCoreState( RaftCoreState coreState ) throws IOException
    {
        this.membershipManager.install( coreState.committed() );
    }

    public synchronized void setTargetMembershipSet( Set<MemberId> targetMembers )
    {
        this.membershipManager.setTargetMembershipSet( targetMembers );
        if ( this.currentRole == Role.LEADER )
        {
            this.membershipManager.onFollowerStateChange( this.state.followerStates() );
        }
    }

    public LeaderInfo getLeaderInfo() throws NoLeaderFoundException
    {
        return (LeaderInfo) this.outcomeApplier.getLeaderInfo().orElseThrow( NoLeaderFoundException::new );
    }

    public void registerListener( LeaderListener listener )
    {
        this.outcomeApplier.registerListener( listener );
    }

    public void unregisterListener( LeaderListener listener )
    {
        this.outcomeApplier.unregisterListener( listener );
    }

    public synchronized ExposedRaftState state()
    {
        return this.state.copy();
    }

    public synchronized ConsensusOutcome handle( RaftMessages.RaftMessage incomingMessage ) throws IOException
    {
        Outcome outcome = this.currentRole.handler.handle( incomingMessage, this.state, this.log );
        this.currentRole = this.outcomeApplier.handle( outcome );
        return outcome;
    }

    public boolean isLeader()
    {
        return this.currentRole == Role.LEADER;
    }

    public Role currentRole()
    {
        return this.currentRole;
    }

    public MemberId memberId()
    {
        return this.myself;
    }

    public String toString()
    {
        MemberId var10000 = this.myself;
        return "RaftMachine{myself=" + var10000 + ", currentRole=" + this.currentRole + ", term=" + this.term() + ", votingMembers=" + this.votingMembers() +
                ", leader=" + this.outcomeApplier.getLeaderInfo() + "}";
    }

    public long term()
    {
        return this.state.term();
    }

    public Set<MemberId> votingMembers()
    {
        return this.membershipManager.votingMembers();
    }

    public Set<MemberId> replicationMembers()
    {
        return this.membershipManager.replicationMembers();
    }

    public static enum Timeouts implements TimerService.TimerName
    {
        ELECTION,
        HEARTBEAT;
    }
}
