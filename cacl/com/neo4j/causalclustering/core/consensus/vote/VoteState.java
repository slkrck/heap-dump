package com.neo4j.causalclustering.core.consensus.vote;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class VoteState
{
    private MemberId votedFor;
    private long term = -1L;

    public VoteState()
    {
    }

    private VoteState( MemberId votedFor, long term )
    {
        this.term = term;
        this.votedFor = votedFor;
    }

    public MemberId votedFor()
    {
        return this.votedFor;
    }

    public boolean update( MemberId votedFor, long term )
    {
        if ( this.termChanged( term ) )
        {
            this.votedFor = votedFor;
            this.term = term;
            return true;
        }
        else
        {
            if ( this.votedFor == null )
            {
                if ( votedFor != null )
                {
                    this.votedFor = votedFor;
                    return true;
                }
            }
            else if ( !this.votedFor.equals( votedFor ) )
            {
                throw new IllegalArgumentException( "Can only vote once per term." );
            }

            return false;
        }
    }

    private boolean termChanged( long term )
    {
        return term != this.term;
    }

    public long term()
    {
        return this.term;
    }

    public String toString()
    {
        return "VoteState{votedFor=" + this.votedFor + ", term=" + this.term + "}";
    }

    public static class Marshal extends SafeStateMarshal<VoteState>
    {
        private final ChannelMarshal<MemberId> memberMarshal;

        public Marshal()
        {
            this.memberMarshal = MemberId.Marshal.INSTANCE;
        }

        public void marshal( VoteState state, WritableChannel channel ) throws IOException
        {
            channel.putLong( state.term );
            this.memberMarshal.marshal( state.votedFor(), channel );
        }

        public VoteState unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
        {
            long term = channel.getLong();
            MemberId member = (MemberId) this.memberMarshal.unmarshal( channel );
            return new VoteState( member, term );
        }

        public VoteState startState()
        {
            return new VoteState();
        }

        public long ordinal( VoteState state )
        {
            return state.term();
        }
    }
}
