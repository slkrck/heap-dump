package com.neo4j.causalclustering.core.consensus;

import java.util.Collection;

public class MajorityIncludingSelfQuorum
{
    private static final int MIN_QUORUM = 2;

    private MajorityIncludingSelfQuorum()
    {
    }

    public static boolean isQuorum( Collection<?> cluster, Collection<?> countNotIncludingMyself )
    {
        return isQuorum( cluster.size(), countNotIncludingMyself.size() );
    }

    public static boolean isQuorum( int clusterSize, int countNotIncludingSelf )
    {
        return isQuorum( 2, clusterSize, countNotIncludingSelf );
    }

    public static boolean isQuorum( int minQuorum, int clusterSize, int countNotIncludingSelf )
    {
        return countNotIncludingSelf + 1 >= minQuorum && countNotIncludingSelf >= clusterSize / 2;
    }
}
