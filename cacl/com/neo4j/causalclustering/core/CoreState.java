package com.neo4j.causalclustering.core;

import com.neo4j.causalclustering.SessionTracker;
import com.neo4j.causalclustering.core.replication.session.GlobalSessionTrackerState;
import com.neo4j.causalclustering.core.state.CommandDispatcher;
import com.neo4j.causalclustering.core.state.CoreStateFiles;
import com.neo4j.causalclustering.core.state.machines.CoreStateMachines;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;
import com.neo4j.causalclustering.core.state.storage.StateStorage;

import java.io.IOException;

public class CoreState
{
    private final SessionTracker sessionTracker;
    private final StateStorage<Long> lastFlushedStorage;
    private final CoreStateMachines stateMachines;

    CoreState( SessionTracker sessionTracker, StateStorage<Long> lastFlushedStorage, CoreStateMachines stateMachines )
    {
        this.sessionTracker = sessionTracker;
        this.lastFlushedStorage = lastFlushedStorage;
        this.stateMachines = stateMachines;
    }

    public void augmentSnapshot( CoreSnapshot coreSnapshot )
    {
        this.stateMachines.augmentSnapshot( coreSnapshot );
        coreSnapshot.add( CoreStateFiles.SESSION_TRACKER, this.sessionTracker.snapshot() );
    }

    public void installSnapshot( CoreSnapshot coreSnapshot )
    {
        this.stateMachines.installSnapshot( coreSnapshot );
        this.sessionTracker.installSnapshot( (GlobalSessionTrackerState) coreSnapshot.get( CoreStateFiles.SESSION_TRACKER ) );
    }

    public void flush( long lastApplied ) throws IOException
    {
        this.stateMachines.flush();
        this.sessionTracker.flush();
        this.lastFlushedStorage.writeState( lastApplied );
    }

    public CommandDispatcher commandDispatcher()
    {
        return this.stateMachines.commandDispatcher();
    }

    public long getLastAppliedIndex()
    {
        long lastAppliedIndex = this.stateMachines.getLastAppliedIndex();
        long maxFromSession = this.sessionTracker.getLastAppliedIndex();
        return Long.max( lastAppliedIndex, maxFromSession );
    }

    public long getLastFlushed()
    {
        return (Long) this.lastFlushedStorage.getInitialState();
    }
}
