package com.neo4j.causalclustering.identity;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.util.Id;

public final class RaftId
{
    private final Id id;

    RaftId( UUID uuid )
    {
        this.id = new Id( uuid );
    }

    public static RaftId from( DatabaseId databaseId )
    {
        return new RaftId( databaseId.uuid() );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            RaftId raftId = (RaftId) o;
            return Objects.equals( this.id, raftId.id );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.id} );
    }

    public UUID uuid()
    {
        return this.id.uuid();
    }

    public String toString()
    {
        return "RaftId{" + this.id + "}";
    }

    public static class Marshal extends SafeStateMarshal<RaftId>
    {
        public static final RaftId.Marshal INSTANCE = new RaftId.Marshal();
        private static final UUID NIL = new UUID( 0L, 0L );

        public void marshal( RaftId raftId, WritableChannel channel ) throws IOException
        {
            UUID uuid = raftId == null ? NIL : raftId.uuid();
            channel.putLong( uuid.getMostSignificantBits() );
            channel.putLong( uuid.getLeastSignificantBits() );
        }

        public RaftId unmarshal0( ReadableChannel channel ) throws IOException
        {
            long mostSigBits = channel.getLong();
            long leastSigBits = channel.getLong();
            UUID uuid = new UUID( mostSigBits, leastSigBits );
            return uuid.equals( NIL ) ? null : new RaftId( uuid );
        }

        public RaftId startState()
        {
            return null;
        }

        public long ordinal( RaftId raftId )
        {
            return raftId == null ? 0L : 1L;
        }
    }
}
