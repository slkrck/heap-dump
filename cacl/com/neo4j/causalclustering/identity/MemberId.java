package com.neo4j.causalclustering.identity;

import com.neo4j.causalclustering.core.state.storage.SafeStateMarshal;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.util.Id;

public class MemberId
{
    private final Id id;

    public MemberId( UUID uuid )
    {
        this.id = new Id( uuid );
    }

    public UUID getUuid()
    {
        return this.id.uuid();
    }

    public String toString()
    {
        return String.format( "MemberId{%s}", this.id );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            MemberId that = (MemberId) o;
            return Objects.equals( this.id, that.id );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hashCode( this.id );
    }

    public static class Marshal extends SafeStateMarshal<MemberId>
    {
        public static final MemberId.Marshal INSTANCE = new MemberId.Marshal();

        public void marshal( MemberId memberId, WritableChannel channel ) throws IOException
        {
            if ( memberId == null )
            {
                channel.put( (byte) 0 );
            }
            else
            {
                channel.put( (byte) 1 );
                channel.putLong( memberId.getUuid().getMostSignificantBits() );
                channel.putLong( memberId.getUuid().getLeastSignificantBits() );
            }
        }

        public MemberId unmarshal0( ReadableChannel channel ) throws IOException
        {
            byte nullMarker = channel.get();
            if ( nullMarker == 0 )
            {
                return null;
            }
            else
            {
                long mostSigBits = channel.getLong();
                long leastSigBits = channel.getLong();
                return new MemberId( new UUID( mostSigBits, leastSigBits ) );
            }
        }

        public MemberId startState()
        {
            return null;
        }

        public long ordinal( MemberId memberId )
        {
            return memberId == null ? 0L : 1L;
        }
    }
}
