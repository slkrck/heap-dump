package com.neo4j.causalclustering.identity;

import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;

import java.util.Optional;

public class BoundState
{
    private final RaftId raftId;
    private final CoreSnapshot coreSnapshot;

    public BoundState( RaftId raftId )
    {
        this( raftId, (CoreSnapshot) null );
    }

    BoundState( RaftId raftId, CoreSnapshot coreSnapshot )
    {
        this.raftId = raftId;
        this.coreSnapshot = coreSnapshot;
    }

    public RaftId raftId()
    {
        return this.raftId;
    }

    public Optional<CoreSnapshot> snapshot()
    {
        return Optional.ofNullable( this.coreSnapshot );
    }
}
