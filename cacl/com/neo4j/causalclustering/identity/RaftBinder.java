package com.neo4j.causalclustering.identity;

import com.neo4j.causalclustering.core.state.RaftBootstrapper;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;
import com.neo4j.causalclustering.core.state.storage.SimpleStorage;
import com.neo4j.causalclustering.discovery.CoreTopologyService;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.PublishRaftIdOutcome;
import com.neo4j.dbms.ClusterSystemGraphDbmsModel;
import com.neo4j.dbms.DatabaseStartAborter;

import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.neo4j.dbms.database.DatabaseStartAbortedException;
import org.neo4j.function.ThrowingAction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class RaftBinder implements Supplier<Optional<RaftId>>
{
    private final NamedDatabaseId namedDatabaseId;
    private final SimpleStorage<RaftId> raftIdStorage;
    private final CoreTopologyService topologyService;
    private final ClusterSystemGraphDbmsModel systemGraph;
    private final RaftBootstrapper raftBootstrapper;
    private final MemberId myIdentity;
    private final RaftBinder.Monitor monitor;
    private final Clock clock;
    private final ThrowingAction<InterruptedException> retryWaiter;
    private final Duration timeout;
    private final int minCoreHosts;
    private RaftId raftId;

    public RaftBinder( NamedDatabaseId namedDatabaseId, MemberId myIdentity, SimpleStorage<RaftId> raftIdStorage, CoreTopologyService topologyService,
            ClusterSystemGraphDbmsModel systemGraph, Clock clock, ThrowingAction<InterruptedException> retryWaiter, Duration timeout,
            RaftBootstrapper raftBootstrapper, int minCoreHosts, Monitors monitors )
    {
        this.namedDatabaseId = namedDatabaseId;
        this.myIdentity = myIdentity;
        this.systemGraph = systemGraph;
        this.monitor = (RaftBinder.Monitor) monitors.newMonitor( RaftBinder.Monitor.class, new String[0] );
        this.raftIdStorage = raftIdStorage;
        this.topologyService = topologyService;
        this.raftBootstrapper = raftBootstrapper;
        this.clock = clock;
        this.retryWaiter = retryWaiter;
        this.timeout = timeout;
        this.minCoreHosts = minCoreHosts;
    }

    private static void validateRaftId( RaftId raftId, NamedDatabaseId namedDatabaseId )
    {
        if ( !Objects.equals( raftId.uuid(), namedDatabaseId.databaseId().uuid() ) )
        {
            throw new IllegalStateException( String.format(
                    "Pre-existing cluster state found with an unexpected id %s. The id for this database is %s. This may indicate a previous DROP operation for %s did not complete.",
                    raftId.uuid(), namedDatabaseId.databaseId().uuid(), namedDatabaseId.name() ) );
        }
    }

    private boolean hostShouldBootstrapRaft( DatabaseCoreTopology coreTopology )
    {
        int memberCount = coreTopology.members().size();
        if ( memberCount < this.minCoreHosts )
        {
            this.monitor.waitingForCoreMembers( this.namedDatabaseId, this.minCoreHosts );
            return false;
        }
        else if ( !this.topologyService.canBootstrapRaftGroup( this.namedDatabaseId ) )
        {
            this.monitor.waitingForBootstrap( this.namedDatabaseId );
            return false;
        }
        else
        {
            return true;
        }
    }

    public BoundState bindToRaft( DatabaseStartAborter databaseStartAborter ) throws Exception
    {
        RaftBinder.BindingConditions bindingConditions = new RaftBinder.BindingConditions( databaseStartAborter, this.clock, this.timeout );
        return this.raftIdStorage.exists() ? this.bindToRaftIdFromDisk( bindingConditions ) : this.getBoundState( bindingConditions );
    }

    private BoundState getBoundState( RaftBinder.BindingConditions bindingConditions ) throws Exception
    {
        if ( this.isInitialDatabase() )
        {
            return this.bindToInitialRaftGroup( bindingConditions );
        }
        else
        {
            Set<MemberId> initialMemberIds =
                    (Set) this.systemGraph.getInitialMembers( this.namedDatabaseId ).stream().map( MemberId::new ).collect( Collectors.toSet() );
            return this.bindToRaftGroupNotPartOfInitialDatabases( bindingConditions, initialMemberIds );
        }
    }

    private BoundState bindToRaftIdFromDisk( RaftBinder.BindingConditions bindingConditions ) throws Exception
    {
        this.raftId = (RaftId) this.raftIdStorage.readState();
        validateRaftId( this.raftId, this.namedDatabaseId );
        this.awaitPublishRaftId( this.namedDatabaseId, bindingConditions, this.raftId );
        this.monitor.boundToRaftFromDisk( this.namedDatabaseId, this.raftId );
        return new BoundState( this.raftId );
    }

    private BoundState bindToInitialRaftGroup( RaftBinder.BindingConditions bindingConditions ) throws Exception
    {
        while ( true )
        {
            DatabaseCoreTopology topology = this.topologyService.coreTopologyForDatabase( this.namedDatabaseId );
            if ( this.isAlreadyBootstrapped( topology ) )
            {
                validateRaftId( topology.raftId(), this.namedDatabaseId );
                return this.handleBootstrapByOther( topology );
            }

            if ( this.hostShouldBootstrapRaft( topology ) )
            {
                RaftId raftId = RaftId.from( this.namedDatabaseId.databaseId() );
                this.monitor.logBootstrapAttemptWithDiscoveryService();
                PublishRaftIdOutcome outcome = this.publishRaftId( raftId, bindingConditions );
                if ( RaftBinder.Publisher.successfullyPublishedBy( RaftBinder.Publisher.ME, outcome ) )
                {
                    this.raftId = raftId;
                    CoreSnapshot snapshot = this.raftBootstrapper.bootstrap( topology.members().keySet() );
                    this.monitor.bootstrapped( snapshot, this.namedDatabaseId, raftId );
                    return new BoundState( raftId, snapshot );
                }
            }

            bindingConditions.allowContinueBinding( this.namedDatabaseId, topology );
            this.retryWaiter.apply();
        }
    }

    private BoundState awaitBootstrapByOther( RaftBinder.BindingConditions bindingConditions )
            throws IOException, TimeoutException, DatabaseStartAbortedException
    {
        while ( true )
        {
            DatabaseCoreTopology topology = this.topologyService.coreTopologyForDatabase( this.namedDatabaseId );
            if ( this.isAlreadyBootstrapped( topology ) )
            {
                validateRaftId( topology.raftId(), this.namedDatabaseId );
                return this.handleBootstrapByOther( topology );
            }

            bindingConditions.allowContinueBinding( this.namedDatabaseId, topology );
        }
    }

    private BoundState bindToRaftGroupNotPartOfInitialDatabases( RaftBinder.BindingConditions bindingConditions, Set<MemberId> initialMemberIds )
            throws Exception
    {
        if ( !initialMemberIds.contains( this.myIdentity ) )
        {
            return this.awaitBootstrapByOther( bindingConditions );
        }
        else
        {
            StoreId storeId = this.systemGraph.getStoreId( this.namedDatabaseId );
            RaftId raftId = RaftId.from( this.namedDatabaseId.databaseId() );
            this.monitor.logBootstrapWithInitialMembersAndStoreID( initialMemberIds, storeId );
            PublishRaftIdOutcome outcome = this.awaitPublishRaftId( this.namedDatabaseId, bindingConditions, raftId );
            this.raftId = raftId;
            if ( RaftBinder.Publisher.successfullyPublishedBy( RaftBinder.Publisher.ME, outcome ) )
            {
                CoreSnapshot snapshot = this.raftBootstrapper.bootstrap( initialMemberIds, storeId );
                this.monitor.bootstrapped( snapshot, this.namedDatabaseId, raftId );
                return new BoundState( raftId, snapshot );
            }
            else
            {
                return new BoundState( raftId );
            }
        }
    }

    private boolean isInitialDatabase()
    {
        return this.namedDatabaseId.isSystemDatabase() || this.systemGraph.getInitialMembers( this.namedDatabaseId ).isEmpty();
    }

    private boolean isAlreadyBootstrapped( DatabaseCoreTopology topology )
    {
        return topology.raftId() != null;
    }

    private BoundState handleBootstrapByOther( DatabaseCoreTopology topology ) throws IOException
    {
        this.saveSystemDatabase();
        this.monitor.boundToRaftThroughTopology( this.namedDatabaseId, this.raftId );
        this.raftId = topology.raftId();
        return new BoundState( this.raftId );
    }

    private void saveSystemDatabase() throws IOException
    {
        if ( this.namedDatabaseId.isSystemDatabase() )
        {
            this.monitor.logSaveSystemDatabase();
            this.raftBootstrapper.saveStore();
        }
    }

    public Optional<RaftId> get()
    {
        return Optional.ofNullable( this.raftId );
    }

    private PublishRaftIdOutcome awaitPublishRaftId( NamedDatabaseId namedDatabaseId, RaftBinder.BindingConditions bindingConditions, RaftId raftId )
            throws BindingException, InterruptedException, TimeoutException, DatabaseStartAbortedException
    {
        while ( true )
        {
            PublishRaftIdOutcome outcome = this.publishRaftId( raftId, bindingConditions );
            if ( outcome != PublishRaftIdOutcome.FAILED_PUBLISH )
            {
                return outcome;
            }

            this.monitor.retryPublishRaftId( namedDatabaseId, raftId );
            this.retryWaiter.apply();
            bindingConditions.allowContinuePublishing( namedDatabaseId );
        }
    }

    private PublishRaftIdOutcome publishRaftId( RaftId localRaftId, RaftBinder.BindingConditions bindingConditions )
            throws BindingException, TimeoutException, DatabaseStartAbortedException
    {
        PublishRaftIdOutcome outcome;
        do
        {
            try
            {
                outcome = this.topologyService.publishRaftId( localRaftId );
            }
            catch ( TimeoutException var5 )
            {
                outcome = null;
            }
            catch ( Throwable var6 )
            {
                throw new BindingException( String.format( "Failed to publish raftId %s", localRaftId ), var6 );
            }
        }
        while ( outcome == null && bindingConditions.allowContinuePublishing( this.namedDatabaseId ) );

        return outcome;
    }

    static enum Publisher
    {
        ME
                {
                    boolean validate( PublishRaftIdOutcome outcome )
                    {
                        return outcome == PublishRaftIdOutcome.SUCCESSFUL_PUBLISH_BY_ME;
                    }
                },
        ANYONE
                {
                    boolean validate( PublishRaftIdOutcome outcome )
                    {
                        return outcome == PublishRaftIdOutcome.SUCCESSFUL_PUBLISH_BY_ME || outcome == PublishRaftIdOutcome.SUCCESSFUL_PUBLISH_BY_OTHER;
                    }
                };

        public static boolean successfullyPublishedBy( RaftBinder.Publisher publisher, PublishRaftIdOutcome outcome )
        {
            return publisher.validate( outcome );
        }

        abstract boolean validate( PublishRaftIdOutcome var1 );
    }

    public interface Monitor
    {
        void waitingForCoreMembers( NamedDatabaseId var1, int var2 );

        void waitingForBootstrap( NamedDatabaseId var1 );

        void bootstrapped( CoreSnapshot var1, NamedDatabaseId var2, RaftId var3 );

        void boundToRaftFromDisk( NamedDatabaseId var1, RaftId var2 );

        void boundToRaftThroughTopology( NamedDatabaseId var1, RaftId var2 );

        void retryPublishRaftId( NamedDatabaseId var1, RaftId var2 );

        void logSaveSystemDatabase();

        void logBootstrapAttemptWithDiscoveryService();

        void logBootstrapWithInitialMembersAndStoreID( Set<MemberId> var1, StoreId var2 );
    }

    private static class BindingConditions
    {
        private final DatabaseStartAborter startAborter;
        private final Clock clock;
        private final long endTime;

        BindingConditions( DatabaseStartAborter startAborter, Clock clock, Duration timeout )
        {
            this.startAborter = startAborter;
            this.clock = clock;
            this.endTime = clock.millis() + timeout.toMillis();
        }

        boolean allowContinueBinding( NamedDatabaseId namedDatabaseId, DatabaseCoreTopology topology ) throws TimeoutException, DatabaseStartAbortedException
        {
            String message = String.format( "Failed to join or bootstrap a raft group with id %s and members %s in time. Please restart the cluster.",
                    RaftId.from( namedDatabaseId.databaseId() ), topology );
            return this.allowContinue( namedDatabaseId, message );
        }

        boolean allowContinuePublishing( NamedDatabaseId namedDatabaseId ) throws TimeoutException, DatabaseStartAbortedException
        {
            String message = String.format( "Failed to publish raftId %s in time. Please restart the cluster.", RaftId.from( namedDatabaseId.databaseId() ) );
            return this.allowContinue( namedDatabaseId, message );
        }

        private boolean allowContinue( NamedDatabaseId namedDatabaseId, String timeoutMessage ) throws TimeoutException, DatabaseStartAbortedException
        {
            boolean shouldAbort = this.startAborter.shouldAbort( namedDatabaseId );
            boolean timedOut = this.endTime < this.clock.millis();
            if ( shouldAbort )
            {
                throw new DatabaseStartAbortedException( namedDatabaseId );
            }
            else if ( timedOut )
            {
                throw new TimeoutException( timeoutMessage );
            }
            else
            {
                return true;
            }
        }
    }
}
