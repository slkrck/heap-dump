package com.neo4j.causalclustering.common;

import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import org.neo4j.configuration.ssl.SslPolicyScope;
import org.neo4j.ssl.SslPolicy;
import org.neo4j.ssl.config.SslPolicyLoader;

public final class PipelineBuilders
{
    private final NettyPipelineBuilderFactory clientPipelineBuilderFactory;
    private final NettyPipelineBuilderFactory serverPipelineBuilderFactory;
    private final NettyPipelineBuilderFactory backupServerPipelineBuilderFactory;

    public PipelineBuilders( SslPolicyLoader sslPolicyLoader )
    {
        SslPolicy clusterSslPolicy = sslPolicyLoader.hasPolicyForSource( SslPolicyScope.CLUSTER ) ? sslPolicyLoader.getPolicy( SslPolicyScope.CLUSTER ) : null;
        SslPolicy backupSslPolicy = sslPolicyLoader.hasPolicyForSource( SslPolicyScope.BACKUP ) ? sslPolicyLoader.getPolicy( SslPolicyScope.BACKUP ) : null;
        this.clientPipelineBuilderFactory = new NettyPipelineBuilderFactory( clusterSslPolicy );
        this.serverPipelineBuilderFactory = new NettyPipelineBuilderFactory( clusterSslPolicy );
        this.backupServerPipelineBuilderFactory = new NettyPipelineBuilderFactory( backupSslPolicy );
    }

    public NettyPipelineBuilderFactory client()
    {
        return this.clientPipelineBuilderFactory;
    }

    public NettyPipelineBuilderFactory server()
    {
        return this.serverPipelineBuilderFactory;
    }

    public NettyPipelineBuilderFactory backupServer()
    {
        return this.backupServerPipelineBuilderFactory;
    }
}
