package com.neo4j.causalclustering.common;

import com.neo4j.causalclustering.catchup.CatchupServerBuilder;
import com.neo4j.causalclustering.catchup.CatchupServerHandler;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.net.BootstrapConfiguration;
import com.neo4j.causalclustering.net.Server;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import com.neo4j.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import com.neo4j.kernel.impl.enterprise.configuration.OnlineBackupSettings;
import io.netty.channel.ChannelInboundHandler;

import java.time.Duration;
import java.util.Collection;
import java.util.Optional;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.JobScheduler;

public class TransactionBackupServiceProvider
{
    public static final String BACKUP_SERVER_NAME = "backup-server";
    private final LogProvider logProvider;
    private final ChannelInboundHandler parentHandler;
    private final ApplicationSupportedProtocols catchupProtocols;
    private final Collection<ModifierSupportedProtocols> supportedModifierProtocols;
    private final NettyPipelineBuilderFactory serverPipelineBuilderFactory;
    private final CatchupServerHandler catchupServerHandler;
    private final JobScheduler scheduler;
    private final ConnectorPortRegister portRegister;

    public TransactionBackupServiceProvider( LogProvider logProvider, ApplicationSupportedProtocols catchupProtocols,
            Collection<ModifierSupportedProtocols> supportedModifierProtocols, NettyPipelineBuilderFactory serverPipelineBuilderFactory,
            CatchupServerHandler catchupServerHandler, ChannelInboundHandler parentHandler, JobScheduler scheduler, ConnectorPortRegister portRegister )
    {
        this.logProvider = logProvider;
        this.parentHandler = parentHandler;
        this.catchupProtocols = catchupProtocols;
        this.supportedModifierProtocols = supportedModifierProtocols;
        this.serverPipelineBuilderFactory = serverPipelineBuilderFactory;
        this.catchupServerHandler = catchupServerHandler;
        this.scheduler = scheduler;
        this.portRegister = portRegister;
    }

    public Optional<Server> resolveIfBackupEnabled( Config config )
    {
        if ( (Boolean) config.get( OnlineBackupSettings.online_backup_enabled ) )
        {
            SocketAddress backupAddress = (SocketAddress) config.get( OnlineBackupSettings.online_backup_listen_address );
            this.logProvider.getLog( TransactionBackupServiceProvider.class ).info( "Binding backup service on address %s", new Object[]{backupAddress} );
            Server catchupServer = CatchupServerBuilder.builder().catchupServerHandler( this.catchupServerHandler ).catchupProtocols(
                    this.catchupProtocols ).modifierProtocols( this.supportedModifierProtocols ).pipelineBuilder(
                    this.serverPipelineBuilderFactory ).installedProtocolsHandler( this.parentHandler ).listenAddress( backupAddress ).scheduler(
                    this.scheduler ).config( config ).bootstrapConfig( BootstrapConfiguration.serverConfig( config ) ).portRegister(
                    this.portRegister ).debugLogProvider( this.logProvider ).serverName( "backup-server" ).handshakeTimeout(
                    (Duration) config.get( CausalClusteringSettings.handshake_timeout ) ).build();
            return Optional.of( catchupServer );
        }
        else
        {
            return Optional.empty();
        }
    }
}
