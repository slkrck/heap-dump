package com.neo4j.causalclustering.common;

import com.neo4j.causalclustering.discovery.TopologyService;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

public class DatabaseTopologyNotifier extends LifecycleAdapter
{
    private final NamedDatabaseId namedDatabaseId;
    private final TopologyService topologyService;

    public DatabaseTopologyNotifier( NamedDatabaseId namedDatabaseId, TopologyService topologyService )
    {
        this.namedDatabaseId = namedDatabaseId;
        this.topologyService = topologyService;
    }

    public void start()
    {
        this.topologyService.onDatabaseStart( this.namedDatabaseId );
    }

    public void stop()
    {
        this.topologyService.onDatabaseStop( this.namedDatabaseId );
    }
}
