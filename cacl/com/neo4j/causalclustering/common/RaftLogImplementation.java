package com.neo4j.causalclustering.common;

public enum RaftLogImplementation
{
    IN_MEMORY,
    SEGMENTED;
}
