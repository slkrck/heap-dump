package com.neo4j.causalclustering.common;

import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.LifecycleException;

public class ClusteredDatabase
{
    private final LifeSupport components = new LifeSupport();
    private boolean hasBeenStarted;

    public final void start()
    {
        if ( this.hasBeenStarted )
        {
            throw new IllegalStateException( "Clustered databases do not support component reuse." );
        }
        else
        {
            this.hasBeenStarted = true;

            try
            {
                this.components.start();
            }
            catch ( LifecycleException var4 )
            {
                try
                {
                    this.components.shutdown();
                }
                catch ( Throwable var3 )
                {
                    var4.addSuppressed( var3 );
                }

                throw var4;
            }
        }
    }

    public final void stop()
    {
        this.components.shutdown();
    }

    public final boolean hasBeenStarted()
    {
        return this.hasBeenStarted;
    }

    protected void addComponent( Lifecycle component )
    {
        this.components.add( component );
    }
}
