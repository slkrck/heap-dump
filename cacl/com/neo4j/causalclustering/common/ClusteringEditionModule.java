package com.neo4j.causalclustering.common;

import com.neo4j.kernel.impl.enterprise.EnterpriseConstraintSemantics;
import com.neo4j.kernel.impl.enterprise.transaction.log.checkpoint.ConfigurableIOLimiter;
import com.neo4j.kernel.impl.net.DefaultNetworkConnectionTracker;

import java.time.Duration;
import java.util.function.Predicate;

import org.neo4j.bolt.dbapi.BoltGraphDatabaseManagementServiceSPI;
import org.neo4j.bolt.dbapi.impl.BoltKernelDatabaseManagementServiceProvider;
import org.neo4j.bolt.txtracking.DefaultReconciledTransactionTracker;
import org.neo4j.bolt.txtracking.ReconciledTransactionTracker;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.function.Predicates;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.AbstractEditionModule;
import org.neo4j.kernel.api.net.NetworkConnectionTracker;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.time.SystemNanoClock;

public abstract class ClusteringEditionModule extends AbstractEditionModule
{
    protected final ReconciledTransactionTracker reconciledTxTracker;

    protected ClusteringEditionModule( GlobalModule globalModule )
    {
        this.reconciledTxTracker = new DefaultReconciledTransactionTracker( globalModule.getLogService() );
    }

    public static Predicate<String> fileWatcherFileNameFilter()
    {
        return Predicates.any( new Predicate[]{( fileName ) -> {
            return fileName.startsWith( "neostore.transaction.db" );
        }, ( filename ) -> {
            return filename.endsWith( ".cacheprof" );
        }} );
    }

    protected void editionInvariants( GlobalModule globalModule, Dependencies dependencies )
    {
        this.ioLimiter = new ConfigurableIOLimiter( globalModule.getGlobalConfig() );
        this.constraintSemantics = new EnterpriseConstraintSemantics();
        this.connectionTracker = (NetworkConnectionTracker) dependencies.satisfyDependency( this.createConnectionTracker() );
    }

    protected NetworkConnectionTracker createConnectionTracker()
    {
        return new DefaultNetworkConnectionTracker();
    }

    public BoltGraphDatabaseManagementServiceSPI createBoltDatabaseManagementServiceProvider( Dependencies dependencies,
            DatabaseManagementService managementService, Monitors monitors, SystemNanoClock clock, LogService logService )
    {
        Config config = (Config) dependencies.resolveDependency( Config.class );
        Duration bookmarkAwaitDuration = (Duration) config.get( GraphDatabaseSettings.bookmark_ready_timeout );
        return new BoltKernelDatabaseManagementServiceProvider( managementService, this.reconciledTxTracker, monitors, clock, bookmarkAwaitDuration );
    }
}
