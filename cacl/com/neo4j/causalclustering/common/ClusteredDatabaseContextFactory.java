package com.neo4j.causalclustering.common;

import com.neo4j.causalclustering.catchup.CatchupComponentsFactory;
import com.neo4j.causalclustering.catchup.storecopy.StoreFiles;
import com.neo4j.dbms.database.ClusteredDatabaseContext;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;

@FunctionalInterface
public interface ClusteredDatabaseContextFactory
{
    ClusteredDatabaseContext create( Database var1, GraphDatabaseFacade var2, LogFiles var3, StoreFiles var4, LogProvider var5, CatchupComponentsFactory var6,
            ClusteredDatabase var7, Monitors var8 );
}
