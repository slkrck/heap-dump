package com.neo4j.causalclustering.common;

import org.neo4j.collection.Dependencies;
import org.neo4j.monitoring.Monitors;

public class ClusterMonitors extends Monitors
{
    private ClusterMonitors( Monitors parent )
    {
        super( parent );
    }

    public static ClusterMonitors create( Monitors globalMonitors, Dependencies clusterDependencies )
    {
        ClusterMonitors monitors = new ClusterMonitors( globalMonitors );
        clusterDependencies.satisfyDependency( monitors );
        return monitors;
    }
}
