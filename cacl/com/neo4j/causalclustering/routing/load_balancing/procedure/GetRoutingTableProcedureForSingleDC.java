package com.neo4j.causalclustering.routing.load_balancing.procedure;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.ClientConnector;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.routing.load_balancing.LeaderService;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.BaseGetRoutingTableProcedure;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.values.virtual.MapValue;

public class GetRoutingTableProcedureForSingleDC extends BaseGetRoutingTableProcedure
{
    private static final String DESCRIPTION = "Returns cluster endpoints and their capabilities for single data center setup.";
    private final TopologyService topologyService;
    private final LeaderService leaderService;

    public GetRoutingTableProcedureForSingleDC( List<String> namespace, TopologyService topologyService, LeaderService leaderService,
            DatabaseManager<?> databaseManager, Config config, LogProvider logProvider )
    {
        super( namespace, databaseManager, config, logProvider );
        this.topologyService = topologyService;
        this.leaderService = leaderService;
    }

    protected String description()
    {
        return "Returns cluster endpoints and their capabilities for single data center setup.";
    }

    protected RoutingResult invoke( NamedDatabaseId namedDatabaseId, MapValue routingContext )
    {
        List<SocketAddress> routeEndpoints = this.routeEndpoints( namedDatabaseId );
        List<SocketAddress> writeEndpoints = this.writeEndpoints( namedDatabaseId );
        List<SocketAddress> readEndpoints = this.readEndpoints( namedDatabaseId );
        long timeToLiveMillis = ((Duration) this.config.get( GraphDatabaseSettings.routing_ttl )).toMillis();
        return new RoutingResult( routeEndpoints, writeEndpoints, readEndpoints, timeToLiveMillis );
    }

    private List<SocketAddress> routeEndpoints( NamedDatabaseId namedDatabaseId )
    {
        List<SocketAddress> routers = (List) this.coreServersFor( namedDatabaseId ).stream().map( ClientConnector::boltAddress ).collect( Collectors.toList() );
        Collections.shuffle( routers );
        return routers;
    }

    private List<SocketAddress> writeEndpoints( NamedDatabaseId namedDatabaseId )
    {
        Optional<SocketAddress> optionalLeaderAddress = this.leaderService.getLeaderBoltAddress( namedDatabaseId );
        if ( optionalLeaderAddress.isEmpty() )
        {
            this.log.debug( "No leader server found. This can happen during a leader switch. No write end points available" );
        }

        return (List) optionalLeaderAddress.stream().collect( Collectors.toList() );
    }

    private List<SocketAddress> readEndpoints( NamedDatabaseId namedDatabaseId )
    {
        List<SocketAddress> readReplicas =
                (List) this.readReplicasFor( namedDatabaseId ).stream().map( ClientConnector::boltAddress ).collect( Collectors.toList() );
        boolean allowReadsOnFollowers = readReplicas.isEmpty() || (Boolean) this.config.get( CausalClusteringSettings.cluster_allow_reads_on_followers );
        Stream<SocketAddress> coreReadEndPoints = allowReadsOnFollowers ? this.coreReadEndPoints( namedDatabaseId ) : Stream.empty();
        List<SocketAddress> readEndPoints = (List) Stream.concat( readReplicas.stream(), coreReadEndPoints ).collect( Collectors.toList() );
        Collections.shuffle( readEndPoints );
        return readEndPoints;
    }

    private Stream<SocketAddress> coreReadEndPoints( NamedDatabaseId namedDatabaseId )
    {
        Optional<SocketAddress> optionalLeaderAddress = this.leaderService.getLeaderBoltAddress( namedDatabaseId );
        Collection<CoreServerInfo> coreServerInfos = this.coreServersFor( namedDatabaseId );
        Stream<SocketAddress> coreAddresses = coreServerInfos.stream().map( ClientConnector::boltAddress );
        if ( optionalLeaderAddress.isPresent() && coreServerInfos.size() > 1 )
        {
            SocketAddress leaderAddress = (SocketAddress) optionalLeaderAddress.get();
            return coreAddresses.filter( ( address ) -> {
                return !leaderAddress.equals( address );
            } );
        }
        else
        {
            return coreAddresses;
        }
    }

    private Collection<CoreServerInfo> coreServersFor( NamedDatabaseId namedDatabaseId )
    {
        return this.topologyService.coreTopologyForDatabase( namedDatabaseId ).members().values();
    }

    private Collection<ReadReplicaInfo> readReplicasFor( NamedDatabaseId namedDatabaseId )
    {
        return this.topologyService.readReplicaTopologyForDatabase( namedDatabaseId ).members().values();
    }
}
