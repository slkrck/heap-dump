package com.neo4j.causalclustering.routing.load_balancing.procedure;

import java.util.Collections;
import java.util.List;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.procedure.builtin.routing.SingleInstanceGetRoutingTableProcedure;

public class ReadReplicaGetRoutingTableProcedure extends SingleInstanceGetRoutingTableProcedure
{
    public ReadReplicaGetRoutingTableProcedure( List<String> namespace, DatabaseManager<?> databaseManager, ConnectorPortRegister portRegister, Config config,
            LogProvider logProvider )
    {
        super( namespace, databaseManager, portRegister, config, logProvider );
    }

    protected RoutingResult createRoutingResult( SocketAddress address, long routingTableTtl )
    {
        List<SocketAddress> addresses = Collections.singletonList( address );
        return new RoutingResult( addresses, Collections.emptyList(), addresses, routingTableTtl );
    }
}
