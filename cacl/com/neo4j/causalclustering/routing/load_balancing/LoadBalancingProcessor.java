package com.neo4j.causalclustering.routing.load_balancing;

import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.values.virtual.MapValue;

public interface LoadBalancingProcessor
{
    RoutingResult run( NamedDatabaseId var1, MapValue var2 ) throws ProcedureException;
}
