package com.neo4j.causalclustering.routing.load_balancing.plugins;

import com.neo4j.causalclustering.routing.load_balancing.LoadBalancingProcessor;

import java.util.Collections;

import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.values.virtual.MapValue;

public class ServerShufflingProcessor implements LoadBalancingProcessor
{
    private final LoadBalancingProcessor delegate;

    public ServerShufflingProcessor( LoadBalancingProcessor delegate )
    {
        this.delegate = delegate;
    }

    public RoutingResult run( NamedDatabaseId namedDatabaseId, MapValue context ) throws ProcedureException
    {
        RoutingResult result = this.delegate.run( namedDatabaseId, context );
        Collections.shuffle( result.routeEndpoints() );
        Collections.shuffle( result.writeEndpoints() );
        Collections.shuffle( result.readEndpoints() );
        return result;
    }

    public LoadBalancingProcessor delegate()
    {
        return this.delegate;
    }
}
