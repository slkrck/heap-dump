package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import com.neo4j.causalclustering.routing.load_balancing.filters.Filter;

import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.neo4j.internal.helpers.collection.Iterators;

public class AnyGroupFilter implements Filter<ServerInfo>
{
    private final Predicate<ServerInfo> matchesAnyGroup;
    private final Set<String> groups;

    AnyGroupFilter( String... groups )
    {
        this( Iterators.asSet( groups ) );
    }

    AnyGroupFilter( Set<String> groups )
    {
        this.matchesAnyGroup = ( serverInfo ) -> {
            Iterator var2 = serverInfo.groups().iterator();

            String group;
            do
            {
                if ( !var2.hasNext() )
                {
                    return false;
                }

                group = (String) var2.next();
            }
            while ( !groups.contains( group ) );

            return true;
        };
        this.groups = groups;
    }

    public Set<ServerInfo> apply( Set<ServerInfo> data )
    {
        return (Set) data.stream().filter( this.matchesAnyGroup ).collect( Collectors.toSet() );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            AnyGroupFilter that = (AnyGroupFilter) o;
            return Objects.equals( this.groups, that.groups );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.groups} );
    }

    public String toString()
    {
        return "AnyGroupFilter{groups=" + this.groups + "}";
    }
}
