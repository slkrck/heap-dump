package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

public class InvalidFilterSpecification extends Exception
{
    InvalidFilterSpecification( String message )
    {
        super( message );
    }

    InvalidFilterSpecification( String message, NumberFormatException cause )
    {
        super( message, cause );
    }
}
