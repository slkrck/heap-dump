package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import java.util.Set;

public interface Policy
{
    Set<ServerInfo> apply( Set<ServerInfo> var1 );
}
