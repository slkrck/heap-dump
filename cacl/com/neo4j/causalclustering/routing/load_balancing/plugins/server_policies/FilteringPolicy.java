package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import com.neo4j.causalclustering.routing.load_balancing.filters.Filter;

import java.util.Objects;
import java.util.Set;

public class FilteringPolicy implements Policy
{
    private final Filter<ServerInfo> filter;

    FilteringPolicy( Filter<ServerInfo> filter )
    {
        this.filter = filter;
    }

    public Set<ServerInfo> apply( Set<ServerInfo> data )
    {
        return this.filter.apply( data );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FilteringPolicy that = (FilteringPolicy) o;
            return Objects.equals( this.filter, that.filter );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.filter} );
    }

    public String toString()
    {
        return "FilteringPolicy{filter=" + this.filter + "}";
    }
}
