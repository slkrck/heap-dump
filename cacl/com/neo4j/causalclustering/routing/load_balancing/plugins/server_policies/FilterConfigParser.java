package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import com.neo4j.causalclustering.routing.load_balancing.filters.Filter;
import com.neo4j.causalclustering.routing.load_balancing.filters.FilterChain;
import com.neo4j.causalclustering.routing.load_balancing.filters.FirstValidRule;
import com.neo4j.causalclustering.routing.load_balancing.filters.IdentityFilter;
import com.neo4j.causalclustering.routing.load_balancing.filters.MinimumCountFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FilterConfigParser
{
    private FilterConfigParser()
    {
    }

    private static Filter<ServerInfo> filterFor( String filterName, String[] args ) throws InvalidFilterSpecification
    {
        byte var3 = -1;
        switch ( filterName.hashCode() )
        {
        case -1237460524:
            if ( filterName.equals( "groups" ) )
            {
                var3 = 0;
            }
            break;
        case 96673:
            if ( filterName.equals( "all" ) )
            {
                var3 = 2;
            }
            break;
        case 108114:
            if ( filterName.equals( "min" ) )
            {
                var3 = 1;
            }
            break;
        case 3194945:
            if ( filterName.equals( "halt" ) )
            {
                var3 = 3;
            }
        }

        switch ( var3 )
        {
        case 0:
            if ( args.length < 1 )
            {
                throw new InvalidFilterSpecification( String.format( "Invalid number of arguments for filter '%s': %d", filterName, args.length ) );
            }
            else
            {
                String[] var9 = args;
                int var5 = args.length;

                for ( int var6 = 0; var6 < var5; ++var6 )
                {
                    String group = var9[var6];
                    if ( group.matches( "\\W" ) )
                    {
                        throw new InvalidFilterSpecification( String.format( "Invalid group for filter '%s': '%s'", filterName, group ) );
                    }
                }

                return new AnyGroupFilter( args );
            }
        case 1:
            if ( args.length != 1 )
            {
                throw new InvalidFilterSpecification( String.format( "Invalid number of arguments for filter '%s': %d", filterName, args.length ) );
            }
            else
            {
                int minCount;
                try
                {
                    minCount = Integer.parseInt( args[0] );
                }
                catch ( NumberFormatException var8 )
                {
                    throw new InvalidFilterSpecification( String.format( "Invalid argument for filter '%s': '%s'", filterName, args[0] ), var8 );
                }

                return new MinimumCountFilter( minCount );
            }
        case 2:
            if ( args.length != 0 )
            {
                throw new InvalidFilterSpecification( String.format( "Invalid number of arguments for filter '%s': %d", filterName, args.length ) );
            }

            return IdentityFilter.as();
        case 3:
            if ( args.length != 0 )
            {
                throw new InvalidFilterSpecification( String.format( "Invalid number of arguments for filter '%s': %d", filterName, args.length ) );
            }

            return HaltFilter.INSTANCE;
        default:
            throw new InvalidFilterSpecification( "Unknown filter: " + filterName );
        }
    }

    public static Filter<ServerInfo> parse( String filterConfig ) throws InvalidFilterSpecification
    {
        if ( filterConfig.length() == 0 )
        {
            throw new InvalidFilterSpecification( "Filter config is empty" );
        }
        else
        {
            List<FilterChain<ServerInfo>> rules = new ArrayList();
            String[] ruleSpecs = filterConfig.split( ";" );
            if ( ruleSpecs.length == 0 )
            {
                throw new InvalidFilterSpecification( "No rules specified" );
            }
            else
            {
                boolean haltFilterEncountered = false;
                String[] var4 = ruleSpecs;
                int var5 = ruleSpecs.length;

                for ( int var6 = 0; var6 < var5; ++var6 )
                {
                    String ruleSpec = var4[var6];
                    ruleSpec = ruleSpec.trim();
                    List<Filter<ServerInfo>> filterChain = new ArrayList();
                    String[] filterSpecs = ruleSpec.split( "->" );
                    boolean allFilterEncountered = false;
                    String[] var11 = filterSpecs;
                    int var12 = filterSpecs.length;

                    for ( int var13 = 0; var13 < var12; ++var13 )
                    {
                        String filterSpec = var11[var13];
                        filterSpec = filterSpec.trim();
                        String[] nameAndArgs = filterSpec.split( "\\(", 0 );
                        if ( nameAndArgs.length != 2 )
                        {
                            throw new InvalidFilterSpecification( String.format( "Syntax error filter specification: '%s'", filterSpec ) );
                        }

                        String namePart = nameAndArgs[0].trim();
                        String argsPart = nameAndArgs[1].trim();
                        if ( !argsPart.endsWith( ")" ) )
                        {
                            throw new InvalidFilterSpecification( String.format( "No closing parenthesis: '%s'", filterSpec ) );
                        }

                        argsPart = argsPart.substring( 0, argsPart.length() - 1 );
                        String filterName = namePart.trim();
                        if ( !filterName.matches( "\\w+" ) )
                        {
                            throw new InvalidFilterSpecification( String.format( "Syntax error filter name: '%s'", filterName ) );
                        }

                        String[] nonEmptyArgs = (String[]) ((List) Arrays.stream( argsPart.split( "," ) ).map( String::trim ).filter( ( s ) -> {
                            return !s.isEmpty();
                        } ).collect( Collectors.toList() )).toArray( new String[0] );
                        String[] var19 = nonEmptyArgs;
                        int var20 = nonEmptyArgs.length;

                        for ( int var21 = 0; var21 < var20; ++var21 )
                        {
                            String arg = var19[var21];
                            if ( !arg.matches( "[\\w-]+" ) )
                            {
                                throw new InvalidFilterSpecification( String.format( "Syntax error argument: '%s'", arg ) );
                            }
                        }

                        if ( haltFilterEncountered )
                        {
                            if ( filterChain.size() > 0 )
                            {
                                throw new InvalidFilterSpecification( String.format( "Filter 'halt' may not be followed by other filters: '%s'", ruleSpec ) );
                            }

                            throw new InvalidFilterSpecification( String.format( "Rule 'halt' may not followed by other rules: '%s'", filterConfig ) );
                        }

                        Filter<ServerInfo> filter = filterFor( filterName, nonEmptyArgs );
                        if ( filter == HaltFilter.INSTANCE )
                        {
                            if ( filterChain.size() != 0 )
                            {
                                throw new InvalidFilterSpecification( String.format( "Filter 'halt' must be the only filter in a rule: '%s'", ruleSpec ) );
                            }

                            haltFilterEncountered = true;
                        }
                        else if ( filter == IdentityFilter.INSTANCE )
                        {
                            if ( allFilterEncountered || filterChain.size() != 0 )
                            {
                                throw new InvalidFilterSpecification(
                                        String.format( "Filter 'all' is implicit but allowed only first in a rule: '%s'", ruleSpec ) );
                            }

                            allFilterEncountered = true;
                        }
                        else
                        {
                            filterChain.add( filter );
                        }
                    }

                    if ( filterChain.size() > 0 )
                    {
                        rules.add( new FilterChain( filterChain ) );
                    }
                }

                if ( !haltFilterEncountered )
                {
                    rules.add( new FilterChain( Collections.singletonList( IdentityFilter.as() ) ) );
                }

                return new FirstValidRule( rules );
            }
        }
    }
}
