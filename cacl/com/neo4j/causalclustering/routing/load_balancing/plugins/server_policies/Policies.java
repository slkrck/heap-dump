package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import com.neo4j.causalclustering.routing.load_balancing.filters.IdentityFilter;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.api.exceptions.Status.Procedure;
import org.neo4j.logging.Log;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.MapValue;

public class Policies
{
    public static final String POLICY_KEY = "policy";
    static final String DEFAULT_POLICY_NAME = "default";
    static final Policy DEFAULT_POLICY = new FilteringPolicy( IdentityFilter.as() );
    private final Map<String,Policy> policies = new HashMap();
    private final Log log;

    Policies( Log log )
    {
        this.log = log;
    }

    void addPolicy( String policyName, Policy policy )
    {
        Policy oldPolicy = (Policy) this.policies.putIfAbsent( policyName, policy );
        if ( oldPolicy != null )
        {
            this.log.error( String.format( "Policy name conflict for '%s'.", policyName ) );
        }
    }

    Policy selectFor( MapValue context ) throws ProcedureException
    {
        AnyValue policyName = context.get( "policy" );
        if ( policyName == Values.NO_VALUE )
        {
            return this.defaultPolicy();
        }
        else
        {
            Policy selectedPolicy = (Policy) this.policies.get( ((TextValue) policyName).stringValue() );
            if ( selectedPolicy == null )
            {
                throw new ProcedureException( Procedure.ProcedureCallFailed, String.format( "Policy definition for '%s' could not be found.", policyName ),
                        new Object[0] );
            }
            else
            {
                return selectedPolicy;
            }
        }
    }

    private Policy defaultPolicy()
    {
        Policy registeredDefault = (Policy) this.policies.get( "default" );
        return registeredDefault != null ? registeredDefault : DEFAULT_POLICY;
    }
}
