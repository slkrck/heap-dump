package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import com.neo4j.causalclustering.core.LoadBalancingServerPoliciesGroup;
import com.neo4j.causalclustering.routing.load_balancing.filters.Filter;
import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;

class FilteringPolicyLoader
{
    private FilteringPolicyLoader()
    {
    }

    static Policies loadServerPolicies( Config config, Log log )
    {
        Policies policies = new Policies( log );
        config.getGroups( LoadBalancingServerPoliciesGroup.class ).forEach( ( id, policy ) -> {
            policies.addPolicy( policy.name(), new FilteringPolicy( (Filter) config.get( policy.value ) ) );
        } );
        return policies;
    }
}
