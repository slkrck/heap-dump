package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import com.neo4j.causalclustering.routing.load_balancing.filters.Filter;

import java.util.Set;

class HaltFilter implements Filter<ServerInfo>
{
    public static final HaltFilter INSTANCE = new HaltFilter();

    private HaltFilter()
    {
    }

    public Set<ServerInfo> apply( Set<ServerInfo> data )
    {
        throw new UnsupportedOperationException();
    }
}
