package com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Objects;
import java.util.Set;

import org.neo4j.configuration.helpers.SocketAddress;

public class ServerInfo
{
    private final SocketAddress boltAddress;
    private MemberId memberId;
    private Set<String> groups;

    public ServerInfo( SocketAddress boltAddress, MemberId memberId, Set<String> groups )
    {
        this.boltAddress = boltAddress;
        this.memberId = memberId;
        this.groups = groups;
    }

    public MemberId memberId()
    {
        return this.memberId;
    }

    SocketAddress boltAddress()
    {
        return this.boltAddress;
    }

    Set<String> groups()
    {
        return this.groups;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ServerInfo that = (ServerInfo) o;
            return Objects.equals( this.boltAddress, that.boltAddress ) && Objects.equals( this.memberId, that.memberId ) &&
                    Objects.equals( this.groups, that.groups );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.boltAddress, this.memberId, this.groups} );
    }

    public String toString()
    {
        return "ServerInfo{boltAddress=" + this.boltAddress + ", memberId=" + this.memberId + ", groups=" + this.groups + "}";
    }
}
