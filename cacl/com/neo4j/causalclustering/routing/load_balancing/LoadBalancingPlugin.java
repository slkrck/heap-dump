package com.neo4j.causalclustering.routing.load_balancing;

import com.neo4j.causalclustering.discovery.TopologyService;
import org.neo4j.annotations.service.Service;
import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

@Service
public interface LoadBalancingPlugin extends LoadBalancingProcessor
{
    void validate( Config var1, Log var2 );

    void init( TopologyService var1, LeaderService var2, LogProvider var3, Config var4 ) throws Throwable;

    String pluginName();

    default boolean isShufflingPlugin()
    {
        return false;
    }
}
