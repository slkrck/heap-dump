package com.neo4j.causalclustering.routing.load_balancing;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.consensus.LeaderLocator;
import com.neo4j.causalclustering.core.consensus.NoLeaderFoundException;
import com.neo4j.causalclustering.discovery.ClientConnector;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.RoleInfo;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Map;
import java.util.Optional;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class DefaultLeaderService implements LeaderService
{
    private final LeaderLocatorForDatabase leaderLocatorForDatabase;
    private final TopologyService topologyService;
    private final Log log;

    public DefaultLeaderService( LeaderLocatorForDatabase leaderLocatorForDatabase, TopologyService topologyService, LogProvider logProvider )
    {
        this.leaderLocatorForDatabase = leaderLocatorForDatabase;
        this.topologyService = topologyService;
        this.log = logProvider.getLog( this.getClass() );
    }

    private static Optional<MemberId> getLeaderIdFromLeaderLocator( LeaderLocator leaderLocator )
    {
        try
        {
            return Optional.of( leaderLocator.getLeaderInfo() ).map( LeaderInfo::memberId );
        }
        catch ( NoLeaderFoundException var2 )
        {
            return Optional.empty();
        }
    }

    public Optional<MemberId> getLeaderId( NamedDatabaseId namedDatabaseId )
    {
        Optional<LeaderLocator> leaderLocator = this.leaderLocatorForDatabase.getLeader( namedDatabaseId );
        Optional leaderId;
        if ( leaderLocator.isPresent() )
        {
            leaderId = getLeaderIdFromLeaderLocator( (LeaderLocator) leaderLocator.get() );
            this.log.debug( "Leader locator %s for database %s returned leader ID %s", new Object[]{leaderLocator, namedDatabaseId, leaderId} );
            return leaderId;
        }
        else
        {
            leaderId = this.getLeaderIdFromTopologyService( namedDatabaseId );
            this.log.debug( "Topology service for database %s returned leader ID %s", new Object[]{namedDatabaseId, leaderId} );
            return leaderId;
        }
    }

    public Optional<SocketAddress> getLeaderBoltAddress( NamedDatabaseId namedDatabaseId )
    {
        Optional<SocketAddress> leaderBoltAddress = this.getLeaderId( namedDatabaseId ).flatMap( this::resolveBoltAddress );
        this.log.debug( "Leader for database %s has Bolt address %s", new Object[]{namedDatabaseId, leaderBoltAddress} );
        return leaderBoltAddress;
    }

    private Optional<MemberId> getLeaderIdFromTopologyService( NamedDatabaseId namedDatabaseId )
    {
        return this.topologyService.coreTopologyForDatabase( namedDatabaseId ).members().keySet().stream().filter( ( memberId ) -> {
            return this.topologyService.lookupRole( namedDatabaseId, memberId ) == RoleInfo.LEADER;
        } ).findFirst();
    }

    private Optional<SocketAddress> resolveBoltAddress( MemberId memberId )
    {
        Map<MemberId,CoreServerInfo> coresById = this.topologyService.allCoreServers();
        this.log.debug( "Resolving Bolt address for member %s using %s", new Object[]{memberId, coresById} );
        return Optional.ofNullable( (CoreServerInfo) coresById.get( memberId ) ).map( ClientConnector::boltAddress );
    }
}
