package com.neo4j.causalclustering.routing.load_balancing;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Optional;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;

public interface LeaderService
{
    Optional<MemberId> getLeaderId( NamedDatabaseId var1 );

    Optional<SocketAddress> getLeaderBoltAddress( NamedDatabaseId var1 );
}
