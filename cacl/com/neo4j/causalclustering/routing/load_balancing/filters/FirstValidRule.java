package com.neo4j.causalclustering.routing.load_balancing.filters;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class FirstValidRule<T> implements Filter<T>
{
    private List<FilterChain<T>> rules;

    public FirstValidRule( List<FilterChain<T>> rules )
    {
        this.rules = rules;
    }

    public Set<T> apply( Set<T> input )
    {
        Set<T> output = input;
        Iterator var3 = this.rules.iterator();

        while ( var3.hasNext() )
        {
            Filter<T> chain = (Filter) var3.next();
            output = chain.apply( input );
            if ( !output.isEmpty() )
            {
                break;
            }
        }

        return output;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FirstValidRule<?> that = (FirstValidRule) o;
            return Objects.equals( this.rules, that.rules );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.rules} );
    }

    public String toString()
    {
        return "FirstValidRule{rules=" + this.rules + "}";
    }
}
