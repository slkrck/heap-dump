package com.neo4j.causalclustering.routing.load_balancing.filters;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class FilterChain<T> implements Filter<T>
{
    private List<Filter<T>> chain;

    public FilterChain( List<Filter<T>> chain )
    {
        this.chain = chain;
    }

    public Set<T> apply( Set<T> data )
    {
        Filter filter;
        for ( Iterator var2 = this.chain.iterator(); var2.hasNext(); data = filter.apply( data ) )
        {
            filter = (Filter) var2.next();
        }

        return data;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FilterChain<?> that = (FilterChain) o;
            return Objects.equals( this.chain, that.chain );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.chain} );
    }

    public String toString()
    {
        return "FilterChain{chain=" + this.chain + "}";
    }
}
