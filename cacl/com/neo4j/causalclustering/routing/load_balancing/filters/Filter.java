package com.neo4j.causalclustering.routing.load_balancing.filters;

import java.util.Set;

@FunctionalInterface
public interface Filter<T>
{
    Set<T> apply( Set<T> var1 );
}
