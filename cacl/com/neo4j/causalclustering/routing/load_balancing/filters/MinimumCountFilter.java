package com.neo4j.causalclustering.routing.load_balancing.filters;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class MinimumCountFilter<T> implements Filter<T>
{
    private final int minCount;

    public MinimumCountFilter( int minCount )
    {
        this.minCount = minCount;
    }

    public Set<T> apply( Set<T> data )
    {
        return data.size() >= this.minCount ? data : Collections.emptySet();
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            MinimumCountFilter<?> that = (MinimumCountFilter) o;
            return this.minCount == that.minCount;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.minCount} );
    }

    public String toString()
    {
        return "MinimumCountFilter{minCount=" + this.minCount + "}";
    }
}
