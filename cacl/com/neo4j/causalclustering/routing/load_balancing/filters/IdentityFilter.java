package com.neo4j.causalclustering.routing.load_balancing.filters;

import java.util.Set;

public class IdentityFilter<T> implements Filter<T>
{
    public static final IdentityFilter INSTANCE = new IdentityFilter();

    private IdentityFilter()
    {
    }

    public static <T> IdentityFilter<T> as()
    {
        return INSTANCE;
    }

    public Set<T> apply( Set<T> data )
    {
        return data;
    }

    public String toString()
    {
        return "IdentityFilter{}";
    }
}
