package com.neo4j.causalclustering.routing.load_balancing;

import com.neo4j.causalclustering.core.consensus.LeaderLocator;

import java.util.Optional;

import org.neo4j.kernel.database.NamedDatabaseId;

public interface LeaderLocatorForDatabase
{
    Optional<LeaderLocator> getLeader( NamedDatabaseId var1 );
}
