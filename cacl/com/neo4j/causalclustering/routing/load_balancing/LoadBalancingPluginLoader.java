package com.neo4j.causalclustering.routing.load_balancing;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.routing.load_balancing.plugins.ServerShufflingProcessor;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.service.Services;

public class LoadBalancingPluginLoader
{
    private LoadBalancingPluginLoader()
    {
    }

    public static void validate( Config config, Log log )
    {
        LoadBalancingPlugin plugin = findPlugin( config );
        plugin.validate( config, log );
    }

    public static LoadBalancingProcessor load( TopologyService topologyService, LeaderService leaderService, LogProvider logProvider, Config config )
            throws Throwable
    {
        LoadBalancingPlugin plugin = findPlugin( config );
        plugin.init( topologyService, leaderService, logProvider, config );
        return (LoadBalancingProcessor) ((Boolean) config.get( CausalClusteringSettings.load_balancing_shuffle ) && !plugin.isShufflingPlugin()
                                         ? new ServerShufflingProcessor( plugin ) : plugin);
    }

    private static LoadBalancingPlugin findPlugin( Config config )
    {
        Set<String> availableOptions = new HashSet();
        Iterable<LoadBalancingPlugin> allImplementationsOnClasspath = Services.loadAll( LoadBalancingPlugin.class );
        String configuredName = (String) config.get( CausalClusteringSettings.load_balancing_plugin );
        Iterator var4 = allImplementationsOnClasspath.iterator();

        while ( var4.hasNext() )
        {
            LoadBalancingPlugin plugin = (LoadBalancingPlugin) var4.next();
            if ( plugin.pluginName().equals( configuredName ) )
            {
                return plugin;
            }

            availableOptions.add( plugin.pluginName() );
        }

        throw new IllegalArgumentException(
                String.format( "Could not find load balancing plugin with name: '%s' among available options: %s", configuredName, availableOptions ) );
    }
}
