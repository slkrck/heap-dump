package com.neo4j.causalclustering.protocol;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Protocol<IMPL extends Comparable<IMPL>>
{
    static <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> Optional<T> find( T[] values, Protocol.Category<T> category, IMPL implementation,
            Function<IMPL,IMPL> normalise )
    {
        return Stream.of( values ).filter( ( protocol ) -> {
            return Objects.equals( protocol.category(), category.canonicalName() );
        } ).filter( ( protocol ) -> {
            return Objects.equals( normalise.apply( protocol.implementation() ), normalise.apply( implementation ) );
        } ).findFirst();
    }

    static <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> List<T> filterCategory( T[] values, Protocol.Category<T> category,
            Predicate<IMPL> implPredicate )
    {
        return (List) Stream.of( values ).filter( ( protocol ) -> {
            return Objects.equals( protocol.category(), category.canonicalName() );
        } ).filter( ( protocol ) -> {
            return implPredicate.test( protocol.implementation() );
        } ).collect( Collectors.toList() );
    }

    String category();

    IMPL implementation();

    public interface Category<T extends Protocol<?>>
    {
        String canonicalName();
    }
}
