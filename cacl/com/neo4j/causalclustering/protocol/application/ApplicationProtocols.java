package com.neo4j.causalclustering.protocol.application;

import com.neo4j.causalclustering.protocol.Protocol;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.neo4j.function.Predicates;

public enum ApplicationProtocols implements ApplicationProtocol
{
    RAFT_2_0( ApplicationProtocolCategory.RAFT, new ApplicationProtocolVersion( 2, 0 ) ),
    CATCHUP_3_0( ApplicationProtocolCategory.CATCHUP, new ApplicationProtocolVersion( 3, 0 ) );

    private final ApplicationProtocolVersion version;
    private final ApplicationProtocolCategory identifier;

    private ApplicationProtocols( ApplicationProtocolCategory identifier, ApplicationProtocolVersion version )
    {
        this.identifier = identifier;
        this.version = version;
    }

    public static Optional<ApplicationProtocol> find( ApplicationProtocolCategory category, ApplicationProtocolVersion version )
    {
        return Protocol.find( values(), category, version, Function.identity() );
    }

    public static List<ApplicationProtocol> withCategory( ApplicationProtocolCategory category )
    {
        return Protocol.filterCategory( values(), category, Predicates.alwaysTrue() );
    }

    public String category()
    {
        return this.identifier.canonicalName();
    }

    public ApplicationProtocolVersion implementation()
    {
        return this.version;
    }
}
