package com.neo4j.causalclustering.protocol.application;

import com.neo4j.causalclustering.protocol.Protocol;

public enum ApplicationProtocolCategory implements Protocol.Category<ApplicationProtocol>
{
    RAFT,
    CATCHUP;

    public String canonicalName()
    {
        return this.name().toLowerCase();
    }
}
