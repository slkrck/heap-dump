package com.neo4j.causalclustering.protocol.application;

import com.neo4j.causalclustering.protocol.Protocol;

public interface ApplicationProtocol extends Protocol<ApplicationProtocolVersion>
{
}
