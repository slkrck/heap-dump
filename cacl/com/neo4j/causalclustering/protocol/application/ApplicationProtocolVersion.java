package com.neo4j.causalclustering.protocol.application;

import io.netty.buffer.ByteBuf;

import java.util.Objects;

import org.neo4j.util.Preconditions;

public class ApplicationProtocolVersion implements Comparable<ApplicationProtocolVersion>
{
    private final int major;
    private final int minor;

    public ApplicationProtocolVersion( int major, int minor )
    {
        this.major = Preconditions.requireNonNegative( major );
        this.minor = Preconditions.requireNonNegative( minor );
    }

    public static ApplicationProtocolVersion parse( String value )
    {
        String[] split = value.split( "\\." );
        if ( split.length != 2 )
        {
            throw illegalStringValue( value );
        }
        else
        {
            try
            {
                int major = Integer.parseInt( split[0] );
                int minor = Integer.parseInt( split[1] );
                return new ApplicationProtocolVersion( major, minor );
            }
            catch ( NumberFormatException var4 )
            {
                throw illegalStringValue( value );
            }
        }
    }

    public static ApplicationProtocolVersion decode( ByteBuf buf )
    {
        return new ApplicationProtocolVersion( buf.readInt(), buf.readInt() );
    }

    private static RuntimeException illegalStringValue( String value )
    {
        return new IllegalArgumentException( "Illegal protocol version string: " + value +
                ". Expected a value in format '<major>.<minor>' where both major and minor are non-negative integer values" );
    }

    public int major()
    {
        return this.major;
    }

    public int minor()
    {
        return this.minor;
    }

    public void encode( ByteBuf buf )
    {
        buf.writeInt( this.major );
        buf.writeInt( this.minor );
    }

    public int compareTo( ApplicationProtocolVersion other )
    {
        int majorComparison = Integer.compare( this.major, other.major );
        return majorComparison == 0 ? Integer.compare( this.minor, other.minor ) : majorComparison;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ApplicationProtocolVersion that = (ApplicationProtocolVersion) o;
            return this.major == that.major && this.minor == that.minor;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.major, this.minor} );
    }

    public String toString()
    {
        return this.major + "." + this.minor;
    }
}
