package com.neo4j.causalclustering.protocol;

import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public interface ProtocolInstaller<O extends ProtocolInstaller.Orientation>
{
    void install( Channel var1 ) throws Exception;

    ApplicationProtocol applicationProtocol();

    Collection<Collection<ModifierProtocol>> modifiers();

    public interface Orientation
    {
        public interface Client extends ProtocolInstaller.Orientation
        {
            String OUTBOUND = "outbound";
        }

        public interface Server extends ProtocolInstaller.Orientation
        {
            String INBOUND = "inbound";
        }
    }

    public abstract static class Factory<O extends ProtocolInstaller.Orientation, I extends ProtocolInstaller<O>>
    {
        private final ApplicationProtocol applicationProtocol;
        private final Function<List<ModifierProtocolInstaller<O>>,I> constructor;

        protected Factory( ApplicationProtocol applicationProtocol, Function<List<ModifierProtocolInstaller<O>>,I> constructor )
        {
            this.applicationProtocol = applicationProtocol;
            this.constructor = constructor;
        }

        I create( List<ModifierProtocolInstaller<O>> modifiers )
        {
            return (ProtocolInstaller) this.constructor.apply( modifiers );
        }

        public ApplicationProtocol applicationProtocol()
        {
            return this.applicationProtocol;
        }
    }
}
