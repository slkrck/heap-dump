package com.neo4j.causalclustering.protocol;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

import javax.net.ssl.SSLException;

import org.neo4j.logging.Log;
import org.neo4j.ssl.SslPolicy;

public class ClientNettyPipelineBuilder extends NettyPipelineBuilder<ProtocolInstaller.Orientation.Client,ClientNettyPipelineBuilder>
{
    private static final int LENGTH_FIELD_BYTES = 4;

    ClientNettyPipelineBuilder( ChannelPipeline pipeline, SslPolicy sslPolicy, Log log )
    {
        super( pipeline, sslPolicy, log );
    }

    public ClientNettyPipelineBuilder addFraming()
    {
        this.add( "frame_encoder", new ChannelHandler[]{new LengthFieldPrepender( 4 )} );
        this.add( "frame_decoder", new ChannelHandler[]{new LengthFieldBasedFrameDecoder( Integer.MAX_VALUE, 0, 4, 0, 4 )} );
        return this;
    }

    ChannelHandler createSslHandler( Channel channel, SslPolicy sslPolicy ) throws SSLException
    {
        return sslPolicy.nettyClientHandler( channel );
    }
}
