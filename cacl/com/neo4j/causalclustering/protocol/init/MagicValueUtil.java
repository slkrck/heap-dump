package com.neo4j.causalclustering.protocol.init;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import io.netty.buffer.UnpooledByteBufAllocator;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

final class MagicValueUtil
{
    private static final String MAGIC_VALUE = "NEO4J_CLUSTER";
    private static final ByteBuf MAGIC_VALUE_BUF = createMagicValueBuf();

    private MagicValueUtil()
    {
    }

    static ByteBuf magicValueBuf()
    {
        return MAGIC_VALUE_BUF.duplicate();
    }

    static String readMagicValue( ByteBuf buf )
    {
        int length = MAGIC_VALUE_BUF.writerIndex();
        return buf.readCharSequence( length, StandardCharsets.US_ASCII ).toString();
    }

    static boolean isCorrectMagicValue( String value )
    {
        return Objects.equals( "NEO4J_CLUSTER", value );
    }

    private static ByteBuf createMagicValueBuf()
    {
        ByteBuf buf = ByteBufUtil.writeAscii( UnpooledByteBufAllocator.DEFAULT, "NEO4J_CLUSTER" );
        ByteBuf unreleasableBuf = Unpooled.unreleasableBuffer( buf );
        return unreleasableBuf.asReadOnly();
    }
}
