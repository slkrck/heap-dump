package com.neo4j.causalclustering.protocol.init;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.net.ChildInitializer;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ServerNettyPipelineBuilder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.ReadTimeoutHandler;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ServerChannelInitializer implements ChildInitializer
{
    private final ChannelInitializer<?> handshakeInitializer;
    private final NettyPipelineBuilderFactory pipelineBuilderFactory;
    private final Duration timeout;
    private final LogProvider logProvider;
    private final Log log;
    private final Config config;

    public ServerChannelInitializer( ChannelInitializer<?> handshakeInitializer, NettyPipelineBuilderFactory pipelineBuilderFactory, Duration timeout,
            LogProvider logProvider, Config config )
    {
        this.handshakeInitializer = handshakeInitializer;
        this.pipelineBuilderFactory = pipelineBuilderFactory;
        this.timeout = timeout;
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
        this.config = config;
    }

    public void initChannel( Channel channel )
    {
        if ( (Boolean) this.config.get( CausalClusteringSettings.inbound_connection_initialization_logging_enabled ) )
        {
            this.log.info( "Initializing server channel %s", new Object[]{channel} );
        }

        ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) this.pipelineBuilderFactory.server( channel, this.log ).add( "read_timeout_handler",
                new ChannelHandler[]{new ReadTimeoutHandler( this.timeout.toMillis(), TimeUnit.MILLISECONDS )} )).add( "init_server_handler",
                new ChannelHandler[]{new InitServerHandler( this.handshakeInitializer, this.pipelineBuilderFactory, this.logProvider )} )).install();
    }
}
