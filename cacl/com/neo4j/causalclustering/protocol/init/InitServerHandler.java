package com.neo4j.causalclustering.protocol.init;

import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ServerNettyPipelineBuilder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.DecoderException;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class InitServerHandler extends ReplayingDecoder<Void>
{
    static final String NAME = "init_server_handler";
    private final ChannelInitializer<?> handshakeInitializer;
    private final NettyPipelineBuilderFactory pipelineBuilderFactory;
    private final LogProvider logProvider;
    private final Log log;

    InitServerHandler( ChannelInitializer<?> handshakeInitializer, NettyPipelineBuilderFactory pipelineBuilderFactory, LogProvider logProvider )
    {
        this.handshakeInitializer = handshakeInitializer;
        this.pipelineBuilderFactory = pipelineBuilderFactory;
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out )
    {
        String receivedMagicValue = MagicValueUtil.readMagicValue( in );
        if ( MagicValueUtil.isCorrectMagicValue( receivedMagicValue ) )
        {
            Channel channel = ctx.channel();
            this.log.debug( "Channel %s received a correct magic message", new Object[]{channel} );
            channel.writeAndFlush( MagicValueUtil.magicValueBuf(), channel.voidPromise() );
            ((ServerNettyPipelineBuilder) this.pipelineBuilderFactory.server( channel,
                    this.logProvider.getLog( this.handshakeInitializer.getClass() ) ).addFraming().add( "handshake_initializer",
                    new ChannelHandler[]{this.handshakeInitializer} )).install();
        }
        else
        {
            ctx.close();
            throw new DecoderException( "Wrong magic value: '" + receivedMagicValue + "'" );
        }
    }
}
