package com.neo4j.causalclustering.protocol.init;

import com.neo4j.causalclustering.protocol.ClientNettyPipelineBuilder;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.DecoderException;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class InitClientHandler extends ReplayingDecoder<Void>
{
    static final String NAME = "init_client_handler";
    private final ChannelInitializer<?> handshakeInitializer;
    private final NettyPipelineBuilderFactory pipelineBuilderFactory;
    private final LogProvider logProvider;
    private final Log log;

    InitClientHandler( ChannelInitializer<?> handshakeInitializer, NettyPipelineBuilderFactory pipelineBuilderFactory, LogProvider logProvider )
    {
        this.handshakeInitializer = handshakeInitializer;
        this.pipelineBuilderFactory = pipelineBuilderFactory;
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
        this.setSingleDecode( true );
    }

    public void channelActive( ChannelHandlerContext ctx )
    {
        ctx.writeAndFlush( MagicValueUtil.magicValueBuf(), ctx.voidPromise() );
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out )
    {
        String receivedMagicValue = MagicValueUtil.readMagicValue( in );
        if ( MagicValueUtil.isCorrectMagicValue( receivedMagicValue ) )
        {
            this.log.debug( "Channel %s received a correct magic message", new Object[]{ctx.channel()} );
            ((ClientNettyPipelineBuilder) this.pipelineBuilderFactory.client( ctx.channel(),
                    this.logProvider.getLog( this.handshakeInitializer.getClass() ) ).addFraming().add( "handshake_initializer",
                    new ChannelHandler[]{this.handshakeInitializer} )).install();
        }
        else
        {
            ctx.close();
            throw new DecoderException( "Wrong magic value: '" + receivedMagicValue + "'" );
        }
    }
}
