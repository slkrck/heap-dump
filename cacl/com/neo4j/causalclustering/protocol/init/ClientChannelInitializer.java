package com.neo4j.causalclustering.protocol.init;

import com.neo4j.causalclustering.protocol.ClientNettyPipelineBuilder;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.handshake.ChannelAttribute;
import com.neo4j.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.ReadTimeoutHandler;

import java.nio.channels.ClosedChannelException;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClientChannelInitializer extends ChannelInitializer<Channel>
{
    private final ChannelInitializer<?> handshakeInitializer;
    private final NettyPipelineBuilderFactory pipelineBuilderFactory;
    private final Duration timeout;
    private final LogProvider logProvider;
    private final Log log;

    public ClientChannelInitializer( ChannelInitializer<?> handshakeInitializer, NettyPipelineBuilderFactory pipelineBuilderFactory, Duration timeout,
            LogProvider logProvider )
    {
        this.handshakeInitializer = handshakeInitializer;
        this.pipelineBuilderFactory = pipelineBuilderFactory;
        this.timeout = timeout;
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void initChannel( Channel channel )
    {
        this.log.info( "Initializing client channel %s", new Object[]{channel} );
        CompletableFuture<ProtocolStack> protocolFuture = new CompletableFuture();
        channel.attr( ChannelAttribute.PROTOCOL_STACK ).set( protocolFuture );
        channel.closeFuture().addListener( ( ignore ) -> {
            protocolFuture.completeExceptionally( new ClosedChannelException() );
        } );
        ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) this.pipelineBuilderFactory.client( channel, this.log ).add( "read_timeout_handler",
                new ChannelHandler[]{new ReadTimeoutHandler( this.timeout.toMillis(), TimeUnit.MILLISECONDS )} )).add( "init_client_handler",
                new ChannelHandler[]{new InitClientHandler( this.handshakeInitializer, this.pipelineBuilderFactory, this.logProvider )} )).install();
    }
}
