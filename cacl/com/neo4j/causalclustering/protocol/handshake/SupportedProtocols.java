package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.Protocol;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class SupportedProtocols<U extends Comparable<U>, T extends Protocol<U>>
{
    private final Protocol.Category<T> category;
    private final List<U> versions;

    SupportedProtocols( Protocol.Category<T> category, List<U> versions )
    {
        this.category = category;
        this.versions = Collections.unmodifiableList( versions );
    }

    public Set<U> mutuallySupportedVersionsFor( Set<U> requestedVersions )
    {
        if ( this.versions().isEmpty() )
        {
            return requestedVersions;
        }
        else
        {
            Stream var10000 = requestedVersions.stream();
            List var10001 = this.versions();
            Objects.requireNonNull( var10001 );
            return (Set) var10000.filter( var10001::contains ).collect( Collectors.toSet() );
        }
    }

    public Protocol.Category<T> identifier()
    {
        return this.category;
    }

    public List<U> versions()
    {
        return this.versions;
    }
}
