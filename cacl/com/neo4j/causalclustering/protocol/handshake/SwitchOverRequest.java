package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;

import java.util.List;
import java.util.Objects;

import org.neo4j.internal.helpers.collection.Pair;

public class SwitchOverRequest implements ServerMessage
{
    static final int MESSAGE_CODE = 3;
    private final String protocolName;
    private final ApplicationProtocolVersion version;
    private final List<Pair<String,String>> modifierProtocols;

    public SwitchOverRequest( String applicationProtocolName, ApplicationProtocolVersion applicationProtocolVersion,
            List<Pair<String,String>> modifierProtocols )
    {
        this.protocolName = applicationProtocolName;
        this.version = applicationProtocolVersion;
        this.modifierProtocols = modifierProtocols;
    }

    public void dispatch( ServerMessageHandler handler )
    {
        handler.handle( this );
    }

    public String protocolName()
    {
        return this.protocolName;
    }

    public List<Pair<String,String>> modifierProtocols()
    {
        return this.modifierProtocols;
    }

    public ApplicationProtocolVersion version()
    {
        return this.version;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            SwitchOverRequest that = (SwitchOverRequest) o;
            return Objects.equals( this.version, that.version ) && Objects.equals( this.protocolName, that.protocolName ) &&
                    Objects.equals( this.modifierProtocols, that.modifierProtocols );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.protocolName, this.version, this.modifierProtocols} );
    }

    public String toString()
    {
        return "SwitchOverRequest{protocolName='" + this.protocolName + "', version=" + this.version + ", modifierProtocols=" + this.modifierProtocols + "}";
    }
}
