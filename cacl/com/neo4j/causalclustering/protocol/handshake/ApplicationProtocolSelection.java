package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;

import java.util.Set;

public class ApplicationProtocolSelection extends ProtocolSelection<ApplicationProtocolVersion,ApplicationProtocol>
{
    ApplicationProtocolSelection( String identifier, Set<ApplicationProtocolVersion> versions )
    {
        super( identifier, versions );
    }
}
