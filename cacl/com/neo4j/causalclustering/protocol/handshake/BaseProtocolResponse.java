package com.neo4j.causalclustering.protocol.handshake;

import java.util.Objects;

public abstract class BaseProtocolResponse<IMPL extends Comparable<IMPL>> implements ClientMessage
{
    private final StatusCode statusCode;
    private final String protocolName;
    private final IMPL version;

    BaseProtocolResponse( StatusCode statusCode, String protocolName, IMPL version )
    {
        this.statusCode = statusCode;
        this.protocolName = protocolName;
        this.version = version;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            BaseProtocolResponse that = (BaseProtocolResponse) o;
            return Objects.equals( this.version, that.version ) && Objects.equals( this.protocolName, that.protocolName );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.protocolName, this.version} );
    }

    public StatusCode statusCode()
    {
        return this.statusCode;
    }

    public String protocolName()
    {
        return this.protocolName;
    }

    public IMPL version()
    {
        return this.version;
    }

    public String toString()
    {
        return "BaseProtocolResponse{statusCode=" + this.statusCode + ", protocolName='" + this.protocolName + "', version=" + this.version + "}";
    }
}
