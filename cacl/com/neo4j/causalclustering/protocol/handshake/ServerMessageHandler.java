package com.neo4j.causalclustering.protocol.handshake;

public interface ServerMessageHandler
{
    void handle( ApplicationProtocolRequest var1 );

    void handle( ModifierProtocolRequest var1 );

    void handle( SwitchOverRequest var1 );
}
