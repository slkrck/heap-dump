package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;

public class ApplicationProtocolRepository extends ProtocolRepository<ApplicationProtocolVersion,ApplicationProtocol>
{
    private final ApplicationSupportedProtocols supportedProtocol;

    public ApplicationProtocolRepository( ApplicationProtocol[] protocols, ApplicationSupportedProtocols supportedProtocol )
    {
        super( protocols, ( ignored ) -> {
            return versionNumberComparator();
        }, ApplicationProtocolSelection::new );
        this.supportedProtocol = supportedProtocol;
    }

    ApplicationSupportedProtocols supportedProtocol()
    {
        return this.supportedProtocol;
    }
}
