package com.neo4j.causalclustering.protocol.handshake;

public class ModifierProtocolResponse extends BaseProtocolResponse<String>
{
    static final int MESSAGE_CODE = 1;

    ModifierProtocolResponse( StatusCode statusCode, String protocolName, String implementation )
    {
        super( statusCode, protocolName, implementation );
    }

    static ModifierProtocolResponse failure( String protocolName )
    {
        return new ModifierProtocolResponse( StatusCode.FAILURE, protocolName, "" );
    }

    public void dispatch( ClientMessageHandler handler )
    {
        handler.handle( this );
    }
}
