package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.internal.helpers.collection.Pair;

public class ServerMessageDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out )
    {
        int messageCode = in.readInt();
        switch ( messageCode )
        {
        case 1:
            ApplicationProtocolRequest applicationProtocolRequest =
                    (ApplicationProtocolRequest) this.decodeProtocolRequest( ApplicationProtocolRequest::new, in, ApplicationProtocolVersion::decode );
            out.add( applicationProtocolRequest );
            return;
        case 2:
            ModifierProtocolRequest modifierProtocolRequest =
                    (ModifierProtocolRequest) this.decodeProtocolRequest( ModifierProtocolRequest::new, in, StringMarshal::unmarshal );
            out.add( modifierProtocolRequest );
            return;
        case 3:
            String protocolName = StringMarshal.unmarshal( in );
            ApplicationProtocolVersion version = ApplicationProtocolVersion.decode( in );
            int numberOfModifierProtocols = in.readInt();
            List<Pair<String,String>> modifierProtocols = (List) Stream.generate( () -> {
                return Pair.of( StringMarshal.unmarshal( in ), StringMarshal.unmarshal( in ) );
            } ).limit( (long) numberOfModifierProtocols ).collect( Collectors.toList() );
            out.add( new SwitchOverRequest( protocolName, version, modifierProtocols ) );
            return;
        default:
            throw new IllegalStateException();
        }
    }

    private <U extends Comparable<U>, T extends BaseProtocolRequest<U>> T decodeProtocolRequest( BiFunction<String,Set<U>,T> constructor, ByteBuf in,
            Function<ByteBuf,U> versionDecoder )
    {
        String protocolName = StringMarshal.unmarshal( in );
        int versionArrayLength = in.readInt();
        Set<U> versions = (Set) Stream.generate( () -> {
            return (Comparable) versionDecoder.apply( in );
        } ).limit( (long) versionArrayLength ).collect( Collectors.toSet() );
        return (BaseProtocolRequest) constructor.apply( protocolName, versions );
    }
}
