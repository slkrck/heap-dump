package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;

import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;

import org.neo4j.internal.helpers.collection.Pair;

public class ClientHandshakeException extends Exception
{
    public ClientHandshakeException( String message )
    {
        super( message );
    }

    public ClientHandshakeException( String message, @Nullable ApplicationProtocol negotiatedApplicationProtocol,
            List<Pair<String,Optional<ModifierProtocol>>> negotiatedModifierProtocols )
    {
        super( message + " Negotiated application protocol: " + negotiatedApplicationProtocol + " Negotiated modifier protocols: " +
                negotiatedModifierProtocols );
    }
}
