package com.neo4j.causalclustering.protocol.handshake;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class NettyHandshakeClient extends SimpleChannelInboundHandler<ClientMessage>
{
    private final HandshakeClient handler;

    public NettyHandshakeClient( HandshakeClient handler )
    {
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, ClientMessage msg )
    {
        msg.dispatch( this.handler );
    }
}
