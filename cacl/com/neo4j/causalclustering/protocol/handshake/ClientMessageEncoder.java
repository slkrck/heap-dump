package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.util.function.BiConsumer;

public class ClientMessageEncoder extends MessageToByteEncoder<ServerMessage>
{
    protected void encode( ChannelHandlerContext ctx, ServerMessage msg, ByteBuf out )
    {
        msg.dispatch( new ClientMessageEncoder.Encoder( out ) );
    }

    class Encoder implements ServerMessageHandler
    {
        private final ByteBuf out;

        Encoder( ByteBuf out )
        {
            this.out = out;
        }

        public void handle( ApplicationProtocolRequest applicationProtocolRequest )
        {
            this.out.writeInt( 1 );
            this.encodeProtocolRequest( applicationProtocolRequest, ( buf, version ) -> {
                version.encode( buf );
            } );
        }

        public void handle( ModifierProtocolRequest modifierProtocolRequest )
        {
            this.out.writeInt( 2 );
            this.encodeProtocolRequest( modifierProtocolRequest, StringMarshal::marshal );
        }

        public void handle( SwitchOverRequest switchOverRequest )
        {
            this.out.writeInt( 3 );
            StringMarshal.marshal( this.out, switchOverRequest.protocolName() );
            switchOverRequest.version().encode( this.out );
            this.out.writeInt( switchOverRequest.modifierProtocols().size() );
            switchOverRequest.modifierProtocols().forEach( ( pair ) -> {
                StringMarshal.marshal( this.out, (String) pair.first() );
                StringMarshal.marshal( this.out, (String) pair.other() );
            } );
        }

        private <U extends Comparable<U>> void encodeProtocolRequest( BaseProtocolRequest<U> protocolRequest, BiConsumer<ByteBuf,U> writer )
        {
            StringMarshal.marshal( this.out, protocolRequest.protocolName() );
            this.out.writeInt( protocolRequest.versions().size() );
            protocolRequest.versions().forEach( ( version ) -> {
                writer.accept( this.out, version );
            } );
        }
    }
}
