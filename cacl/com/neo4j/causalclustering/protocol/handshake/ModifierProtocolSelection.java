package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;

import java.util.Set;

public class ModifierProtocolSelection extends ProtocolSelection<String,ModifierProtocol>
{
    public ModifierProtocolSelection( String identifier, Set<String> versions )
    {
        super( identifier, versions );
    }
}
