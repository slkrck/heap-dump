package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.Protocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ProtocolStack
{
    private final ApplicationProtocol applicationProtocol;
    private final List<ModifierProtocol> modifierProtocols;

    public ProtocolStack( ApplicationProtocol applicationProtocol, List<ModifierProtocol> modifierProtocols )
    {
        this.applicationProtocol = applicationProtocol;
        this.modifierProtocols = Collections.unmodifiableList( modifierProtocols );
    }

    public static ProtocolStack.Builder builder()
    {
        return new ProtocolStack.Builder();
    }

    public ApplicationProtocol applicationProtocol()
    {
        return this.applicationProtocol;
    }

    public List<ModifierProtocol> modifierProtocols()
    {
        return this.modifierProtocols;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ProtocolStack that = (ProtocolStack) o;
            return Objects.equals( this.applicationProtocol, that.applicationProtocol ) && Objects.equals( this.modifierProtocols, that.modifierProtocols );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.applicationProtocol, this.modifierProtocols} );
    }

    public String toString()
    {
        String desc = String.format( "%s version:%s", this.applicationProtocol.category(), this.applicationProtocol.implementation() );
        List<String> modifierNames = (List) this.modifierProtocols.stream().map( Protocol::implementation ).collect( Collectors.toList() );
        if ( !modifierNames.isEmpty() )
        {
            desc = String.format( "%s (%s)", desc, String.join( ", ", modifierNames ) );
        }

        return desc;
    }

    public static class Builder
    {
        private final List<ModifierProtocol> modifierProtocols = new ArrayList();
        private ApplicationProtocol applicationProtocol;

        private Builder()
        {
        }

        public ProtocolStack.Builder modifier( ModifierProtocol modifierProtocol )
        {
            this.modifierProtocols.add( modifierProtocol );
            return this;
        }

        public ProtocolStack.Builder application( ApplicationProtocol applicationProtocol )
        {
            this.applicationProtocol = applicationProtocol;
            return this;
        }

        ProtocolStack build()
        {
            return new ProtocolStack( this.applicationProtocol, this.modifierProtocols );
        }

        public String toString()
        {
            return "Builder{applicationProtocol=" + this.applicationProtocol + ", modifierProtocols=" + this.modifierProtocols + "}";
        }
    }
}
