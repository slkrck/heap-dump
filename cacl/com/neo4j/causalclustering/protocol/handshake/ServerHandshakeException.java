package com.neo4j.causalclustering.protocol.handshake;

public class ServerHandshakeException extends Exception
{
    public ServerHandshakeException( String message, ProtocolStack.Builder protocolStackBuilder )
    {
        super( message + " Negotiated protocols: " + protocolStackBuilder );
    }
}
