package com.neo4j.causalclustering.protocol.handshake;

import java.util.Set;

public class ModifierProtocolRequest extends BaseProtocolRequest<String>
{
    static final int MESSAGE_CODE = 2;

    ModifierProtocolRequest( String protocolName, Set<String> versions )
    {
        super( protocolName, versions );
    }

    public void dispatch( ServerMessageHandler handler )
    {
        handler.handle( this );
    }
}
