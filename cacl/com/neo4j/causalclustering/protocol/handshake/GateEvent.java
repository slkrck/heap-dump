package com.neo4j.causalclustering.protocol.handshake;

import java.util.Objects;

public class GateEvent
{
    private static final GateEvent SUCCESS = new GateEvent( true );
    private static final GateEvent FAILURE = new GateEvent( false );
    private final boolean isSuccess;

    private GateEvent( boolean isSuccess )
    {
        this.isSuccess = isSuccess;
    }

    public static GateEvent getSuccess()
    {
        return SUCCESS;
    }

    public static GateEvent getFailure()
    {
        return FAILURE;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            GateEvent that = (GateEvent) o;
            return this.isSuccess == that.isSuccess;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.isSuccess} );
    }
}
