package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.messaging.Channel;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.neo4j.internal.helpers.collection.Pair;

public class HandshakeClient implements ClientMessageHandler
{
    private final CompletableFuture<ProtocolStack> fProtocol;
    private Channel channel;
    private ApplicationProtocolRepository applicationProtocolRepository;
    private ApplicationSupportedProtocols supportedApplicationProtocol;
    private ModifierProtocolRepository modifierProtocolRepository;
    private Collection<ModifierSupportedProtocols> supportedModifierProtocols;
    private ApplicationProtocol negotiatedApplicationProtocol;
    private List<Pair<String,Optional<ModifierProtocol>>> negotiatedModifierProtocols;
    private ProtocolStack protocolStack;

    HandshakeClient( CompletableFuture<ProtocolStack> protocolFuture )
    {
        this.fProtocol = protocolFuture;
    }

    public void initiate( Channel channel, ApplicationProtocolRepository applicationProtocolRepository, ModifierProtocolRepository modifierProtocolRepository )
    {
        this.channel = channel;
        this.applicationProtocolRepository = applicationProtocolRepository;
        this.supportedApplicationProtocol = applicationProtocolRepository.supportedProtocol();
        this.modifierProtocolRepository = modifierProtocolRepository;
        this.supportedModifierProtocols = modifierProtocolRepository.supportedProtocols();
        this.negotiatedModifierProtocols = new ArrayList( this.supportedModifierProtocols.size() );
        this.sendProtocolRequests( channel, this.supportedApplicationProtocol, this.supportedModifierProtocols );
    }

    private void sendProtocolRequests( Channel channel, ApplicationSupportedProtocols applicationProtocols,
            Collection<ModifierSupportedProtocols> supportedModifierProtocols )
    {
        supportedModifierProtocols.forEach( ( modifierProtocol ) -> {
            ProtocolSelection<String,ModifierProtocol> protocolSelection =
                    this.modifierProtocolRepository.getAll( modifierProtocol.identifier(), modifierProtocol.versions() );
            channel.write( new ModifierProtocolRequest( protocolSelection.identifier(), protocolSelection.versions() ) );
        } );
        ProtocolSelection<ApplicationProtocolVersion,ApplicationProtocol> applicationProtocolSelection =
                this.applicationProtocolRepository.getAll( applicationProtocols.identifier(), applicationProtocols.versions() );
        channel.writeAndFlush( new ApplicationProtocolRequest( applicationProtocolSelection.identifier(), applicationProtocolSelection.versions() ) );
    }

    public void handle( ApplicationProtocolResponse applicationProtocolResponse )
    {
        if ( applicationProtocolResponse.statusCode() != StatusCode.SUCCESS )
        {
            this.fail( "Unsuccessful application protocol response" );
        }
        else
        {
            Optional<ApplicationProtocol> protocol = this.applicationProtocolRepository.select( applicationProtocolResponse.protocolName(),
                    (ApplicationProtocolVersion) applicationProtocolResponse.version() );
            if ( protocol.isEmpty() )
            {
                ProtocolSelection<ApplicationProtocolVersion,ApplicationProtocol> knownApplicationProtocolVersions =
                        this.applicationProtocolRepository.getAll( this.supportedApplicationProtocol.identifier(),
                                this.supportedApplicationProtocol.versions() );
                this.fail( String.format(
                        "Mismatch of application protocols between client and server: Server protocol %s version %s: Client protocol %s versions %s",
                        applicationProtocolResponse.protocolName(), applicationProtocolResponse.version(), knownApplicationProtocolVersions.identifier(),
                        knownApplicationProtocolVersions.versions() ) );
            }
            else
            {
                this.negotiatedApplicationProtocol = (ApplicationProtocol) protocol.get();
                this.sendSwitchOverRequestIfReady();
            }
        }
    }

    public void handle( ModifierProtocolResponse modifierProtocolResponse )
    {
        if ( modifierProtocolResponse.statusCode() == StatusCode.SUCCESS )
        {
            Optional<ModifierProtocol> selectedModifierProtocol =
                    this.modifierProtocolRepository.select( modifierProtocolResponse.protocolName(), (String) modifierProtocolResponse.version() );
            this.negotiatedModifierProtocols.add( Pair.of( modifierProtocolResponse.protocolName(), selectedModifierProtocol ) );
        }
        else
        {
            this.negotiatedModifierProtocols.add( Pair.of( modifierProtocolResponse.protocolName(), Optional.empty() ) );
        }

        this.sendSwitchOverRequestIfReady();
    }

    private void sendSwitchOverRequestIfReady()
    {
        if ( this.negotiatedApplicationProtocol != null && this.negotiatedModifierProtocols.size() == this.supportedModifierProtocols.size() )
        {
            List<ModifierProtocol> agreedModifierProtocols =
                    (List) this.negotiatedModifierProtocols.stream().map( Pair::other ).flatMap( Optional::stream ).collect( Collectors.toList() );
            this.protocolStack = new ProtocolStack( this.negotiatedApplicationProtocol, agreedModifierProtocols );
            List<Pair<String,String>> switchOverModifierProtocols = (List) agreedModifierProtocols.stream().map( ( protocol ) -> {
                return Pair.of( protocol.category(), (String) protocol.implementation() );
            } ).collect( Collectors.toList() );
            this.channel.writeAndFlush( new SwitchOverRequest( this.negotiatedApplicationProtocol.category(),
                    (ApplicationProtocolVersion) this.negotiatedApplicationProtocol.implementation(), switchOverModifierProtocols ) );
        }
    }

    public void handle( SwitchOverResponse response )
    {
        if ( this.protocolStack == null )
        {
            this.fail( "Attempted to switch over when protocol stack not established" );
        }
        else if ( response.status() != StatusCode.SUCCESS )
        {
            this.fail( "Server failed to switch over" );
        }
        else
        {
            this.fProtocol.complete( this.protocolStack );
        }
    }

    private void fail( String message )
    {
        this.fProtocol.completeExceptionally( new ClientHandshakeException( message, this.negotiatedApplicationProtocol, this.negotiatedModifierProtocols ) );
    }

    public CompletableFuture<ProtocolStack> protocol()
    {
        return this.fProtocol;
    }
}
