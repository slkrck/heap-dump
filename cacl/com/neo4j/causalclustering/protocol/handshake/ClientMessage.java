package com.neo4j.causalclustering.protocol.handshake;

public interface ClientMessage
{
    void dispatch( ClientMessageHandler var1 );
}
