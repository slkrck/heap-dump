package com.neo4j.causalclustering.protocol.handshake;

import org.neo4j.configuration.helpers.SocketAddress;

public interface ServerHandshakeFinishedEvent
{
    public static class Closed
    {
        public final SocketAddress advertisedSocketAddress;

        public Closed( SocketAddress advertisedSocketAddress )
        {
            this.advertisedSocketAddress = advertisedSocketAddress;
        }
    }

    public static class Created
    {
        public final SocketAddress advertisedSocketAddress;
        public final ProtocolStack protocolStack;

        public Created( SocketAddress advertisedSocketAddress, ProtocolStack protocolStack )
        {
            this.advertisedSocketAddress = advertisedSocketAddress;
            this.protocolStack = protocolStack;
        }
    }
}
