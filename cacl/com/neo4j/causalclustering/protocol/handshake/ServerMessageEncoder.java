package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.util.function.BiConsumer;

public class ServerMessageEncoder extends MessageToByteEncoder<ClientMessage>
{
    protected void encode( ChannelHandlerContext ctx, ClientMessage msg, ByteBuf out )
    {
        msg.dispatch( new ServerMessageEncoder.Encoder( out ) );
    }

    class Encoder implements ClientMessageHandler
    {
        private final ByteBuf out;

        Encoder( ByteBuf out )
        {
            this.out = out;
        }

        public void handle( ApplicationProtocolResponse applicationProtocolResponse )
        {
            this.out.writeInt( 0 );
            this.encodeProtocolResponse( applicationProtocolResponse, ( buf, version ) -> {
                version.encode( buf );
            } );
        }

        public void handle( ModifierProtocolResponse modifierProtocolResponse )
        {
            this.out.writeInt( 1 );
            this.encodeProtocolResponse( modifierProtocolResponse, StringMarshal::marshal );
        }

        public void handle( SwitchOverResponse switchOverResponse )
        {
            this.out.writeInt( 2 );
            this.out.writeInt( switchOverResponse.status().codeValue() );
        }

        private <U extends Comparable<U>> void encodeProtocolResponse( BaseProtocolResponse<U> protocolResponse, BiConsumer<ByteBuf,U> writer )
        {
            this.out.writeInt( protocolResponse.statusCode().codeValue() );
            StringMarshal.marshal( this.out, protocolResponse.protocolName() );
            writer.accept( this.out, protocolResponse.version() );
        }
    }
}
