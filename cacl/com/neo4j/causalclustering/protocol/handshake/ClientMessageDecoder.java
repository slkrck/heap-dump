package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.neo4j.function.TriFunction;

public class ClientMessageDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out ) throws ClientHandshakeException
    {
        int messageCode = in.readInt();
        switch ( messageCode )
        {
        case 0:
            ApplicationProtocolResponse applicationProtocolResponse =
                    (ApplicationProtocolResponse) this.decodeProtocolResponse( ApplicationProtocolResponse::new, ApplicationProtocolVersion::decode, in );
            out.add( applicationProtocolResponse );
            return;
        case 1:
            ModifierProtocolResponse modifierProtocolResponse =
                    (ModifierProtocolResponse) this.decodeProtocolResponse( ModifierProtocolResponse::new, StringMarshal::unmarshal, in );
            out.add( modifierProtocolResponse );
            return;
        case 2:
            int statusCodeValue = in.readInt();
            Optional<StatusCode> statusCode = StatusCode.fromCodeValue( statusCodeValue );
            if ( statusCode.isPresent() )
            {
                out.add( new SwitchOverResponse( (StatusCode) statusCode.get() ) );
            }

            return;
        default:
        }
    }

    private <U extends Comparable<U>, T extends BaseProtocolResponse<U>> T decodeProtocolResponse( TriFunction<StatusCode,String,U,T> constructor,
            Function<ByteBuf,U> reader, ByteBuf in ) throws ClientHandshakeException
    {
        int statusCodeValue = in.readInt();
        String identifier = StringMarshal.unmarshal( in );
        U version = (Comparable) reader.apply( in );
        Optional<StatusCode> statusCode = StatusCode.fromCodeValue( statusCodeValue );
        return (BaseProtocolResponse) statusCode.map( ( status ) -> {
            return (BaseProtocolResponse) constructor.apply( status, identifier, version );
        } ).orElseThrow( () -> {
            return new ClientHandshakeException( String.format( "Unknown status code %s for protocol %s version %s", statusCodeValue, identifier, version ) );
        } );
    }
}
