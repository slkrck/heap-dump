package com.neo4j.causalclustering.protocol.handshake;

public interface ClientMessageHandler
{
    void handle( ApplicationProtocolResponse var1 );

    void handle( ModifierProtocolResponse var1 );

    void handle( SwitchOverResponse var1 );
}
