package com.neo4j.causalclustering.protocol.handshake;

public interface ServerMessage
{
    void dispatch( ServerMessageHandler var1 );
}
