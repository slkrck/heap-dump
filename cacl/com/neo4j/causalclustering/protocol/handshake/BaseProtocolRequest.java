package com.neo4j.causalclustering.protocol.handshake;

import java.util.Objects;
import java.util.Set;

public abstract class BaseProtocolRequest<IMPL extends Comparable<IMPL>> implements ServerMessage
{
    private final String protocolName;
    private final Set<IMPL> versions;

    BaseProtocolRequest( String protocolName, Set<IMPL> versions )
    {
        this.protocolName = protocolName;
        this.versions = versions;
    }

    public String protocolName()
    {
        return this.protocolName;
    }

    public Set<IMPL> versions()
    {
        return this.versions;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            BaseProtocolRequest that = (BaseProtocolRequest) o;
            return Objects.equals( this.protocolName, that.protocolName ) && Objects.equals( this.versions, that.versions );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.protocolName, this.versions} );
    }

    public String toString()
    {
        return "BaseProtocolRequest{protocolName='" + this.protocolName + "', versions=" + this.versions + "}";
    }
}
