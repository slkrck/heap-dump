package com.neo4j.causalclustering.protocol.handshake;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum StatusCode
{
    SUCCESS( 0 ),
    ONGOING( 1 ),
    FAILURE( -1 );

    private static final AtomicReference<Map<Integer,StatusCode>> CODE_MAP = new AtomicReference();
    private final int codeValue;

    private StatusCode( int codeValue )
    {
        this.codeValue = codeValue;
    }

    public static Optional<StatusCode> fromCodeValue( int codeValue )
    {
        Map<Integer,StatusCode> map = (Map) CODE_MAP.get();
        if ( map == null )
        {
            map = (Map) Stream.of( values() ).collect( Collectors.toMap( StatusCode::codeValue, Function.identity() ) );
            CODE_MAP.compareAndSet( (Object) null, map );
        }

        return Optional.ofNullable( (StatusCode) map.get( codeValue ) );
    }

    public int codeValue()
    {
        return this.codeValue;
    }
}
