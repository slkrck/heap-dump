package com.neo4j.causalclustering.protocol.handshake;

import java.util.Objects;

public class SwitchOverResponse implements ClientMessage
{
    public static final SwitchOverResponse FAILURE;
    static final int MESSAGE_CODE = 2;

    static
    {
        FAILURE = new SwitchOverResponse( StatusCode.FAILURE );
    }

    private final StatusCode status;

    SwitchOverResponse( StatusCode status )
    {
        this.status = status;
    }

    public void dispatch( ClientMessageHandler handler )
    {
        handler.handle( this );
    }

    public StatusCode status()
    {
        return this.status;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            SwitchOverResponse that = (SwitchOverResponse) o;
            return this.status == that.status;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.status} );
    }

    public String toString()
    {
        return "SwitchOverResponse{status=" + this.status + "}";
    }
}
