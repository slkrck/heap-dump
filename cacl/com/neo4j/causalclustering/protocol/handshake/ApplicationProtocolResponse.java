package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;

public class ApplicationProtocolResponse extends BaseProtocolResponse<ApplicationProtocolVersion>
{
    static final int MESSAGE_CODE = 0;
    static final ApplicationProtocolResponse NO_PROTOCOL;

    static
    {
        NO_PROTOCOL = new ApplicationProtocolResponse( StatusCode.FAILURE, "", new ApplicationProtocolVersion( 0, 0 ) );
    }

    ApplicationProtocolResponse( StatusCode statusCode, String protocolName, ApplicationProtocolVersion version )
    {
        super( statusCode, protocolName, version );
    }

    public void dispatch( ClientMessageHandler handler )
    {
        handler.handle( this );
    }
}
