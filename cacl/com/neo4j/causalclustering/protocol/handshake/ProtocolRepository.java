package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.Protocol;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.neo4j.internal.helpers.collection.Pair;

public abstract class ProtocolRepository<U extends Comparable<U>, T extends Protocol<U>>
{
    private final Map<Pair<String,U>,T> protocolMap;
    private final BiFunction<String,Set<U>,ProtocolSelection<U,T>> protocolSelectionFactory;
    private Function<String,Comparator<T>> comparator;

    public ProtocolRepository( T[] protocols, Function<String,Comparator<T>> comparators,
            BiFunction<String,Set<U>,ProtocolSelection<U,T>> protocolSelectionFactory )
    {
        this.protocolSelectionFactory = protocolSelectionFactory;
        Map<Pair<String,U>,T> map = new HashMap();
        Protocol[] var5 = protocols;
        int var6 = protocols.length;

        for ( int var7 = 0; var7 < var6; ++var7 )
        {
            T protocol = var5[var7];
            Protocol<U> previous = (Protocol) map.put( Pair.of( protocol.category(), protocol.implementation() ), protocol );
            if ( previous != null )
            {
                throw new IllegalArgumentException(
                        String.format( "Multiple protocols with same identifier and version supplied: %s and %s", protocol, previous ) );
            }
        }

        this.protocolMap = Collections.unmodifiableMap( map );
        this.comparator = comparators;
    }

    static <U extends Comparable<U>, T extends Protocol<U>> Comparator<T> versionNumberComparator()
    {
        return Comparator.comparing( Protocol::implementation );
    }

    Optional<T> select( String protocolName, U version )
    {
        return Optional.ofNullable( (Protocol) this.protocolMap.get( Pair.of( protocolName, version ) ) );
    }

    Optional<T> select( String protocolName, Set<U> versions )
    {
        return versions.stream().map( ( version ) -> {
            return this.select( protocolName, version );
        } ).flatMap( Optional::stream ).max( (Comparator) this.comparator.apply( protocolName ) );
    }

    public ProtocolSelection<U,T> getAll( Protocol.Category<T> category, Collection<U> versions )
    {
        Set<U> selectedVersions = (Set) this.protocolMap.keySet().stream().filter( ( pair ) -> {
            return ((String) pair.first()).equals( category.canonicalName() );
        } ).map( Pair::other ).filter( ( version ) -> {
            return versions.isEmpty() || versions.contains( version );
        } ).collect( Collectors.toSet() );
        if ( selectedVersions.isEmpty() )
        {
            throw new IllegalArgumentException(
                    String.format( "Attempted to select protocols for %s versions %s but no match in known protocols %s", category, versions,
                            this.protocolMap ) );
        }
        else
        {
            return (ProtocolSelection) this.protocolSelectionFactory.apply( category.canonicalName(), selectedVersions );
        }
    }
}
