package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;

import java.util.Set;

public class ApplicationProtocolRequest extends BaseProtocolRequest<ApplicationProtocolVersion>
{
    static final int MESSAGE_CODE = 1;

    ApplicationProtocolRequest( String protocolName, Set<ApplicationProtocolVersion> versions )
    {
        super( protocolName, versions );
    }

    public void dispatch( ServerMessageHandler handler )
    {
        handler.handle( this );
    }
}
