package com.neo4j.causalclustering.protocol.handshake;

import com.neo4j.causalclustering.messaging.Channel;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class HandshakeServer implements ServerMessageHandler
{
    private final Channel channel;
    private final ApplicationProtocolRepository applicationProtocolRepository;
    private final ModifierProtocolRepository modifierProtocolRepository;
    private final SupportedProtocols<ApplicationProtocolVersion,ApplicationProtocol> supportedApplicationProtocol;
    private final ProtocolStack.Builder protocolStackBuilder = ProtocolStack.builder();
    private final CompletableFuture<ProtocolStack> protocolStackFuture = new CompletableFuture();

    HandshakeServer( ApplicationProtocolRepository applicationProtocolRepository, ModifierProtocolRepository modifierProtocolRepository, Channel channel )
    {
        this.channel = channel;
        this.applicationProtocolRepository = applicationProtocolRepository;
        this.modifierProtocolRepository = modifierProtocolRepository;
        this.supportedApplicationProtocol = applicationProtocolRepository.supportedProtocol();
    }

    public void handle( ApplicationProtocolRequest request )
    {
        ApplicationProtocolResponse response;
        if ( !request.protocolName().equals( this.supportedApplicationProtocol.identifier().canonicalName() ) )
        {
            response = ApplicationProtocolResponse.NO_PROTOCOL;
            this.channel.writeAndFlush( response );
            this.decline( String.format( "Requested protocol %s not supported", request.protocolName() ) );
        }
        else
        {
            Optional<ApplicationProtocol> selected = this.applicationProtocolRepository.select( request.protocolName(), this.supportedVersionsFor( request ) );
            if ( selected.isPresent() )
            {
                ApplicationProtocol selectedProtocol = (ApplicationProtocol) selected.get();
                this.protocolStackBuilder.application( selectedProtocol );
                response = new ApplicationProtocolResponse( StatusCode.SUCCESS, selectedProtocol.category(),
                        (ApplicationProtocolVersion) selectedProtocol.implementation() );
                this.channel.writeAndFlush( response );
            }
            else
            {
                response = ApplicationProtocolResponse.NO_PROTOCOL;
                this.channel.writeAndFlush( response );
                this.decline( String.format( "Do not support requested protocol %s versions %s", request.protocolName(), request.versions() ) );
            }
        }
    }

    public void handle( ModifierProtocolRequest modifierProtocolRequest )
    {
        Optional<ModifierProtocol> selected =
                this.modifierProtocolRepository.select( modifierProtocolRequest.protocolName(), this.supportedVersionsFor( modifierProtocolRequest ) );
        ModifierProtocolResponse response;
        if ( selected.isPresent() )
        {
            ModifierProtocol modifierProtocol = (ModifierProtocol) selected.get();
            this.protocolStackBuilder.modifier( modifierProtocol );
            response = new ModifierProtocolResponse( StatusCode.SUCCESS, modifierProtocol.category(), (String) modifierProtocol.implementation() );
        }
        else
        {
            response = ModifierProtocolResponse.failure( modifierProtocolRequest.protocolName() );
        }

        this.channel.writeAndFlush( response );
    }

    public void handle( SwitchOverRequest switchOverRequest )
    {
        ProtocolStack protocolStack = this.protocolStackBuilder.build();
        Optional<ApplicationProtocol> switchOverProtocol =
                this.applicationProtocolRepository.select( switchOverRequest.protocolName(), switchOverRequest.version() );
        List<ModifierProtocol> switchOverModifiers = (List) switchOverRequest.modifierProtocols().stream().map( ( pair ) -> {
            return this.modifierProtocolRepository.select( (String) pair.first(), (String) pair.other() );
        } ).flatMap( Optional::stream ).collect( Collectors.toList() );
        if ( switchOverProtocol.isEmpty() )
        {
            this.channel.writeAndFlush( SwitchOverResponse.FAILURE );
            this.decline( String.format( "Cannot switch to protocol %s version %s", switchOverRequest.protocolName(), switchOverRequest.version() ) );
        }
        else if ( protocolStack.applicationProtocol() == null )
        {
            this.channel.writeAndFlush( SwitchOverResponse.FAILURE );
            this.decline( String.format( "Attempted to switch to protocol %s version %s before negotiation complete", switchOverRequest.protocolName(),
                    switchOverRequest.version() ) );
        }
        else if ( !((ApplicationProtocol) switchOverProtocol.get()).equals( protocolStack.applicationProtocol() ) )
        {
            this.channel.writeAndFlush( SwitchOverResponse.FAILURE );
            this.decline( String.format( "Switch over mismatch: requested %s version %s but negotiated %s version %s", switchOverRequest.protocolName(),
                    switchOverRequest.version(), protocolStack.applicationProtocol().category(), protocolStack.applicationProtocol().implementation() ) );
        }
        else if ( !switchOverModifiers.equals( protocolStack.modifierProtocols() ) )
        {
            this.channel.writeAndFlush( SwitchOverResponse.FAILURE );
            this.decline( String.format( "Switch over mismatch: requested modifiers %s but negotiated %s", switchOverRequest.modifierProtocols(),
                    protocolStack.modifierProtocols() ) );
        }
        else
        {
            SwitchOverResponse response = new SwitchOverResponse( StatusCode.SUCCESS );
            this.channel.writeAndFlush( response );
            this.protocolStackFuture.complete( protocolStack );
        }
    }

    private Set<String> supportedVersionsFor( ModifierProtocolRequest request )
    {
        return (Set) this.modifierProtocolRepository.supportedProtocolFor( request.protocolName() ).map( ( supported ) -> {
            return supported.mutuallySupportedVersionsFor( request.versions() );
        } ).orElse( Collections.emptySet() );
    }

    private Set<ApplicationProtocolVersion> supportedVersionsFor( ApplicationProtocolRequest request )
    {
        return this.supportedApplicationProtocol.mutuallySupportedVersionsFor( request.versions() );
    }

    private void decline( String message )
    {
        this.channel.dispose();
        this.protocolStackFuture.completeExceptionally( new ServerHandshakeException( message, this.protocolStackBuilder ) );
    }

    CompletableFuture<ProtocolStack> protocolStackFuture()
    {
        return this.protocolStackFuture;
    }
}
