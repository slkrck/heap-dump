package com.neo4j.causalclustering.protocol.handshake;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class NettyHandshakeServer extends SimpleChannelInboundHandler<ServerMessage>
{
    private final HandshakeServer handler;

    public NettyHandshakeServer( HandshakeServer handler )
    {
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, ServerMessage msg )
    {
        msg.dispatch( this.handler );
    }
}
