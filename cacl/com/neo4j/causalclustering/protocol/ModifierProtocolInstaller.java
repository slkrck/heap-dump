package com.neo4j.causalclustering.protocol;

import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocols;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.handler.codec.compression.JdkZlibDecoder;
import io.netty.handler.codec.compression.JdkZlibEncoder;
import io.netty.handler.codec.compression.Lz4FrameDecoder;
import io.netty.handler.codec.compression.Lz4FrameEncoder;
import io.netty.handler.codec.compression.SnappyFrameDecoder;
import io.netty.handler.codec.compression.SnappyFrameEncoder;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

public interface ModifierProtocolInstaller<O extends ProtocolInstaller.Orientation>
{
    List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> serverCompressionInstallers =
            Arrays.asList( new ModifierProtocolInstaller.SnappyServer(), new ModifierProtocolInstaller.SnappyValidatingServer(),
                    new ModifierProtocolInstaller.LZ4Server(), new ModifierProtocolInstaller.LZ4ValidatingServer(),
                    new ModifierProtocolInstaller.GzipServer() );
    List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> clientCompressionInstallers =
            Arrays.asList( new ModifierProtocolInstaller.SnappyClient(), new ModifierProtocolInstaller.LZ4Client(),
                    new ModifierProtocolInstaller.LZ4HighCompressionClient(), new ModifierProtocolInstaller.GzipClient() );
    List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> allClientInstallers = clientCompressionInstallers;
    List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> allServerInstallers = serverCompressionInstallers;

    Collection<ModifierProtocol> protocols();

    <BUILDER extends NettyPipelineBuilder<O,BUILDER>> void apply( NettyPipelineBuilder<O,BUILDER> var1 );

    public static class GzipServer extends ModifierProtocolInstaller.BaseServerModifier
    {
        GzipServer()
        {
            super( "zlib_decoder", JdkZlibDecoder::new, ModifierProtocols.COMPRESSION_GZIP );
        }
    }

    public static class GzipClient extends ModifierProtocolInstaller.BaseClientModifier
    {
        GzipClient()
        {
            super( "zlib_encoder", JdkZlibEncoder::new, ModifierProtocols.COMPRESSION_GZIP );
        }
    }

    public static class LZ4ValidatingServer extends ModifierProtocolInstaller.BaseServerModifier
    {
        LZ4ValidatingServer()
        {
            super( "lz4_decoder", () -> {
                return new Lz4FrameDecoder( true );
            }, ModifierProtocols.COMPRESSION_LZ4_VALIDATING, ModifierProtocols.COMPRESSION_LZ4_HIGH_COMPRESSION_VALIDATING );
        }
    }

    public static class LZ4Server extends ModifierProtocolInstaller.BaseServerModifier
    {
        LZ4Server()
        {
            super( "lz4_decoder", Lz4FrameDecoder::new, ModifierProtocols.COMPRESSION_LZ4, ModifierProtocols.COMPRESSION_LZ4_HIGH_COMPRESSION );
        }
    }

    public static class LZ4HighCompressionClient extends ModifierProtocolInstaller.BaseClientModifier
    {
        LZ4HighCompressionClient()
        {
            super( "lz4_encoder", () -> {
                return new Lz4FrameEncoder( true );
            }, ModifierProtocols.COMPRESSION_LZ4_HIGH_COMPRESSION, ModifierProtocols.COMPRESSION_LZ4_HIGH_COMPRESSION_VALIDATING );
        }
    }

    public static class LZ4Client extends ModifierProtocolInstaller.BaseClientModifier
    {
        LZ4Client()
        {
            super( "lz4_encoder", Lz4FrameEncoder::new, ModifierProtocols.COMPRESSION_LZ4, ModifierProtocols.COMPRESSION_LZ4_VALIDATING );
        }
    }

    public static class SnappyValidatingServer extends ModifierProtocolInstaller.BaseServerModifier
    {
        SnappyValidatingServer()
        {
            super( "snappy_validating_decoder", () -> {
                return new SnappyFrameDecoder( true );
            }, ModifierProtocols.COMPRESSION_SNAPPY_VALIDATING );
        }
    }

    public static class SnappyServer extends ModifierProtocolInstaller.BaseServerModifier
    {
        SnappyServer()
        {
            super( "snappy_decoder", SnappyFrameDecoder::new, ModifierProtocols.COMPRESSION_SNAPPY );
        }
    }

    public static class SnappyClient extends ModifierProtocolInstaller.BaseClientModifier
    {
        SnappyClient()
        {
            super( "snappy_encoder", SnappyFrameEncoder::new, ModifierProtocols.COMPRESSION_SNAPPY, ModifierProtocols.COMPRESSION_SNAPPY_VALIDATING );
        }
    }

    public abstract static class BaseServerModifier implements ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>
    {
        private final String pipelineDecoderName;
        private final Supplier<ByteToMessageDecoder> decoder;
        private final Collection<ModifierProtocol> modifierProtocols;

        protected BaseServerModifier( String pipelineDecoderName, Supplier<ByteToMessageDecoder> decoder, ModifierProtocol... modifierProtocols )
        {
            this.pipelineDecoderName = pipelineDecoderName;
            this.decoder = decoder;
            this.modifierProtocols = Arrays.asList( modifierProtocols );
        }

        public final Collection<ModifierProtocol> protocols()
        {
            return this.modifierProtocols;
        }

        public final <BUILDER extends NettyPipelineBuilder<ProtocolInstaller.Orientation.Server,BUILDER>> void apply(
                NettyPipelineBuilder<ProtocolInstaller.Orientation.Server,BUILDER> nettyPipelineBuilder )
        {
            nettyPipelineBuilder.add( this.pipelineDecoderName, (ChannelHandler) this.decoder.get() );
        }
    }

    public abstract static class BaseClientModifier implements ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>
    {
        private final String pipelineEncoderName;
        private final Supplier<MessageToByteEncoder<ByteBuf>> encoder;
        private final Collection<ModifierProtocol> modifierProtocols;

        protected BaseClientModifier( String pipelineEncoderName, Supplier<MessageToByteEncoder<ByteBuf>> encoder, ModifierProtocol... modifierProtocols )
        {
            this.pipelineEncoderName = pipelineEncoderName;
            this.encoder = encoder;
            this.modifierProtocols = Arrays.asList( modifierProtocols );
        }

        public final Collection<ModifierProtocol> protocols()
        {
            return this.modifierProtocols;
        }

        public final <BUILDER extends NettyPipelineBuilder<ProtocolInstaller.Orientation.Client,BUILDER>> void apply(
                NettyPipelineBuilder<ProtocolInstaller.Orientation.Client,BUILDER> nettyPipelineBuilder )
        {
            nettyPipelineBuilder.add( this.pipelineEncoderName, (ChannelHandler) this.encoder.get() );
        }
    }
}
