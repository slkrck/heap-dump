package com.neo4j.causalclustering.protocol;

import com.neo4j.causalclustering.messaging.MessageGate;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import io.netty.util.ReferenceCountUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;
import javax.net.ssl.SSLException;

import org.neo4j.function.ThrowingAction;
import org.neo4j.logging.Log;
import org.neo4j.ssl.SslPolicy;
import org.neo4j.util.FeatureToggles;

public abstract class NettyPipelineBuilder<O extends ProtocolInstaller.Orientation, BUILDER extends NettyPipelineBuilder<O,BUILDER>>
{
    static final String MESSAGE_GATE_NAME = "message_gate";
    static final String ERROR_HANDLER_TAIL = "error_handler_tail";
    static final String ERROR_HANDLER_HEAD = "error_handler_head";
    static final String SSL_HANDLER_NAME = "ssl_handler";
    private static final boolean DEBUG = FeatureToggles.flag( NettyPipelineBuilder.class, "DEBUG", false );
    private final ChannelPipeline pipeline;
    private final SslPolicy sslPolicy;
    private final Log log;
    private final List<NettyPipelineBuilder.HandlerInfo> handlerInfos = new ArrayList();
    private Predicate<Object> gatePredicate;
    private BUILDER self = this;

    NettyPipelineBuilder( ChannelPipeline pipeline, SslPolicy sslPolicy, Log log )
    {
        this.pipeline = pipeline;
        this.sslPolicy = sslPolicy;
        this.log = log;
    }

    public static ClientNettyPipelineBuilder client( ChannelPipeline pipeline, SslPolicy sslPolicy, Log log )
    {
        return new ClientNettyPipelineBuilder( pipeline, sslPolicy, log );
    }

    public static ServerNettyPipelineBuilder server( ChannelPipeline pipeline, SslPolicy sslPolicy, Log log )
    {
        return new ServerNettyPipelineBuilder( pipeline, sslPolicy, log );
    }

    private static void swallow( ThrowingAction<Exception> action )
    {
        try
        {
            action.apply();
        }
        catch ( Throwable var2 )
        {
        }
    }

    private static void logByteBuf( ChannelHandlerContext ctx, String prefix, Object msg, Log log )
    {
        if ( DEBUG )
        {
            log.info( prefix + ": " + ctx.channel() + "\n" + msg.toString() + "\n" + ByteBufUtil.prettyHexDump( (ByteBuf) msg ) );
        }
    }

    public abstract BUILDER addFraming();

    abstract ChannelHandler createSslHandler( Channel var1, SslPolicy var2 ) throws SSLException;

    public BUILDER modify( ModifierProtocolInstaller<O> modifier )
    {
        modifier.apply( this );
        return this.self;
    }

    public BUILDER modify( List<ModifierProtocolInstaller<O>> modifiers )
    {
        modifiers.forEach( this::modify );
        return this.self;
    }

    public BUILDER add( String name, List<ChannelHandler> newHandlers )
    {
        Stream var10000 = newHandlers.stream().map( ( handler ) -> {
            return new NettyPipelineBuilder.HandlerInfo( name, handler );
        } );
        List var10001 = this.handlerInfos;
        Objects.requireNonNull( var10001 );
        var10000.forEachOrdered( var10001::add );
        return this.self;
    }

    public BUILDER add( String name, ChannelHandler... newHandlers )
    {
        return this.add( name, Arrays.asList( newHandlers ) );
    }

    public BUILDER addGate( Predicate<Object> gatePredicate )
    {
        if ( this.gatePredicate != null )
        {
            throw new IllegalStateException( "Cannot have more than one gate." );
        }
        else
        {
            this.gatePredicate = gatePredicate;
            return this.self;
        }
    }

    public void install()
    {
        this.ensureErrorHandling();
        this.ensureSslHandlerInstalled();
        this.installGate();
        this.clearUserHandlers();
        String userHead = this.findUserHandlerHead();

        NettyPipelineBuilder.HandlerInfo info;
        for ( Iterator var2 = this.handlerInfos.iterator(); var2.hasNext(); userHead = info.name )
        {
            info = (NettyPipelineBuilder.HandlerInfo) var2.next();
            this.pipeline.addAfter( userHead, info.name, info.handler );
        }
    }

    private void installGate()
    {
        if ( this.pipeline.get( "message_gate" ) != null && this.gatePredicate != null )
        {
            throw new IllegalStateException( "Cannot have more than one gate." );
        }
        else
        {
            if ( this.gatePredicate != null )
            {
                this.pipeline.addBefore( "error_handler_tail", "message_gate", new MessageGate( this.gatePredicate ) );
            }
        }
    }

    private void clearUserHandlers()
    {
        Stream var10000 = this.pipeline.names().stream().filter( this::isNotDefault ).filter( this::isNotErrorHandler ).filter( this::isNotGate ).filter(
                this::isNotSslHandler );
        ChannelPipeline var10001 = this.pipeline;
        Objects.requireNonNull( var10001 );
        var10000.forEach( var10001::remove );
    }

    private boolean isNotDefault( String name )
    {
        return this.pipeline.get( name ) != null;
    }

    private boolean isNotErrorHandler( String name )
    {
        return !name.equals( "error_handler_head" ) && !name.equals( "error_handler_tail" );
    }

    private boolean isNotGate( String name )
    {
        return !name.equals( "message_gate" );
    }

    private boolean isNotSslHandler( String name )
    {
        return !name.equals( "ssl_handler" );
    }

    private void ensureErrorHandling()
    {
        int size = this.pipeline.names().size();
        if ( ((String) this.pipeline.names().get( 0 )).equals( "error_handler_head" ) )
        {
            if ( !((String) this.pipeline.names().get( size - 2 )).equals( "error_handler_tail" ) )
            {
                throw new IllegalStateException( "Both error handlers must exist." );
            }
        }
        else
        {
            this.pipeline.addLast( "error_handler_tail", new NettyPipelineBuilder.ErrorHandlerTail( this.log ) );
            this.pipeline.addFirst( "error_handler_head", new NettyPipelineBuilder.ErrorHandlerHead( this.log ) );
        }
    }

    private void ensureSslHandlerInstalled()
    {
        if ( this.sslPolicy != null && this.pipeline.get( "ssl_handler" ) == null )
        {
            try
            {
                ChannelHandler sslHandler = this.createSslHandler( this.pipeline.channel(), this.sslPolicy );
                this.pipeline.addAfter( "error_handler_head", "ssl_handler", sslHandler );
            }
            catch ( SSLException var2 )
            {
                throw new IllegalStateException( "Unable to create an SSL handler", var2 );
            }
        }
    }

    private String findUserHandlerHead()
    {
        return this.sslPolicy != null ? "ssl_handler" : "error_handler_head";
    }

    private static class ErrorHandlerTail extends ChannelDuplexHandler
    {
        private final Log log;

        ErrorHandlerTail( Log log )
        {
            this.log = log;
        }

        public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause )
        {
            NettyPipelineBuilder.swallow( () -> {
                this.log.error( String.format( "Exception in inbound for channel: %s", ctx.channel() ), cause );
            } );
            ReferenceCountUtil.release( cause );
            Objects.requireNonNull( ctx );
            NettyPipelineBuilder.swallow( ctx::close );
        }

        public void channelRead( ChannelHandlerContext ctx, Object msg )
        {
            this.log.error( "Unhandled inbound message: %s for channel: %s", new Object[]{msg, ctx.channel()} );
            ReferenceCountUtil.release( msg );
            ctx.close();
        }

        public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise )
        {
            if ( NettyPipelineBuilder.DEBUG )
            {
                this.log.info( "OUTBOUND: " + msg );
            }

            if ( !promise.isVoid() )
            {
                promise.addListener( ( future ) -> {
                    if ( !future.isSuccess() )
                    {
                        NettyPipelineBuilder.swallow( () -> {
                            this.log.error( String.format( "Exception in outbound for channel: %s", future.channel() ), future.cause() );
                        } );
                        Objects.requireNonNull( ctx );
                        NettyPipelineBuilder.swallow( ctx::close );
                    }
                } );
            }

            ctx.write( msg, promise );
        }
    }

    private static class ErrorHandlerHead extends ChannelDuplexHandler
    {
        private final Log log;

        ErrorHandlerHead( Log log )
        {
            this.log = log;
        }

        public void channelRead( ChannelHandlerContext ctx, Object msg )
        {
            NettyPipelineBuilder.logByteBuf( ctx, "INBOUND", msg, this.log );
            ctx.fireChannelRead( msg );
        }

        public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause )
        {
            NettyPipelineBuilder.swallow( () -> {
                this.log.error( String.format( "Exception in outbound for channel: %s", ctx.channel() ), cause );
            } );
            ReferenceCountUtil.release( cause );
            Objects.requireNonNull( ctx );
            NettyPipelineBuilder.swallow( ctx::close );
        }

        public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise )
        {
            if ( !(msg instanceof ByteBuf) )
            {
                this.log.error( "Unhandled outbound message: %s for channel: %s", new Object[]{msg, ctx.channel()} );
                ReferenceCountUtil.release( msg );
                ctx.close();
            }
            else
            {
                NettyPipelineBuilder.logByteBuf( ctx, "OUTBOUND", msg, this.log );
                ctx.write( msg, promise );
            }
        }
    }

    private static class HandlerInfo
    {
        private final String name;
        private final ChannelHandler handler;

        HandlerInfo( String name, ChannelHandler handler )
        {
            this.name = name;
            this.handler = handler;
        }
    }
}
