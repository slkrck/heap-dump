package com.neo4j.causalclustering.protocol;

import io.netty.channel.Channel;
import org.neo4j.logging.Log;
import org.neo4j.ssl.SslPolicy;

public class NettyPipelineBuilderFactory
{
    private final SslPolicy sslPolicy;

    public NettyPipelineBuilderFactory( SslPolicy sslPolicy )
    {
        this.sslPolicy = sslPolicy;
    }

    public static NettyPipelineBuilderFactory insecure()
    {
        return new NettyPipelineBuilderFactory( (SslPolicy) null );
    }

    public ClientNettyPipelineBuilder client( Channel channel, Log log )
    {
        return NettyPipelineBuilder.client( channel.pipeline(), this.sslPolicy, log );
    }

    public ServerNettyPipelineBuilder server( Channel channel, Log log )
    {
        return NettyPipelineBuilder.server( channel.pipeline(), this.sslPolicy, log );
    }
}
