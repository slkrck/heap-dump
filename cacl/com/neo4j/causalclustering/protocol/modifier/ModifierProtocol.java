package com.neo4j.causalclustering.protocol.modifier;

import com.neo4j.causalclustering.protocol.Protocol;

public interface ModifierProtocol extends Protocol<String>
{
}
