package com.neo4j.causalclustering.protocol.modifier;

import com.neo4j.causalclustering.protocol.Protocol;

import java.util.Optional;

public enum ModifierProtocols implements ModifierProtocol
{
    COMPRESSION_GZIP( ModifierProtocolCategory.COMPRESSION, "Gzip" ),
    COMPRESSION_SNAPPY( ModifierProtocolCategory.COMPRESSION, "Snappy" ),
    COMPRESSION_SNAPPY_VALIDATING( ModifierProtocolCategory.COMPRESSION, "Snappy_validating" ),
    COMPRESSION_LZ4( ModifierProtocolCategory.COMPRESSION, "LZ4" ),
    COMPRESSION_LZ4_HIGH_COMPRESSION( ModifierProtocolCategory.COMPRESSION, "LZ4_high_compression" ),
    COMPRESSION_LZ4_VALIDATING( ModifierProtocolCategory.COMPRESSION, "LZ_validating" ),
    COMPRESSION_LZ4_HIGH_COMPRESSION_VALIDATING( ModifierProtocolCategory.COMPRESSION, "LZ4_high_compression_validating" );

    public static final String ALLOWED_VALUES_STRING =
            "[Gzip, Snappy, Snappy_validating, LZ4, LZ4_high_compression, LZ_validating, LZ4_high_compression_validating]";
    private final String friendlyName;
    private final ModifierProtocolCategory identifier;

    private ModifierProtocols( ModifierProtocolCategory identifier, String friendlyName )
    {
        this.identifier = identifier;
        this.friendlyName = friendlyName;
    }

    public static Optional<ModifierProtocol> find( ModifierProtocolCategory category, String friendlyName )
    {
        return Protocol.find( values(), category, friendlyName, String::toLowerCase );
    }

    public String implementation()
    {
        return this.friendlyName;
    }

    public String category()
    {
        return this.identifier.canonicalName();
    }

    private static class Implementations
    {
        static final String GZIP = "Gzip";
        static final String SNAPPY = "Snappy";
        static final String SNAPPY_VALIDATING = "Snappy_validating";
        static final String LZ4 = "LZ4";
        static final String LZ4_HIGH_COMPRESSION = "LZ4_high_compression";
        static final String LZ_VALIDATING = "LZ_validating";
        static final String LZ4_HIGH_COMPRESSION_VALIDATING = "LZ4_high_compression_validating";
    }
}
