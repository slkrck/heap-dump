package com.neo4j.causalclustering.protocol.modifier;

import com.neo4j.causalclustering.protocol.Protocol;

public enum ModifierProtocolCategory implements Protocol.Category<ModifierProtocol>
{
    COMPRESSION,
    GRATUITOUS_OBFUSCATION;

    public String canonicalName()
    {
        return this.name().toLowerCase();
    }
}
