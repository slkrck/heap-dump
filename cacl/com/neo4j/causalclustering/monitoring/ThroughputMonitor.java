package com.neo4j.causalclustering.monitoring;

import com.neo4j.collection.CircularBuffer;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.util.VisibleForTesting;

public class ThroughputMonitor extends LifecycleAdapter
{
    @VisibleForTesting
    static final int SAMPLING_WINDOW_DIVISOR = 8;
    private final Log log;
    private final Clock clock;
    private final JobScheduler scheduler;
    private final Semaphore samplingTaskLock = new Semaphore( 0, true );
    private final Duration samplingWindow;
    private final Duration samplingInterval;
    private final QualitySampler<Long> qualitySampler;
    private final CircularBuffer<Sample<Long>> samples = new CircularBuffer( 9 );
    private JobHandle job;

    public ThroughputMonitor( LogProvider logProvider, Clock clock, JobScheduler scheduler, Duration samplingWindow, Supplier<Long> valueSupplier )
    {
        this.log = logProvider.getLog( this.getClass() );
        this.clock = clock;
        this.scheduler = scheduler;
        this.samplingWindow = samplingWindow;
        this.samplingInterval = samplingWindow.dividedBy( 8L );
        Duration samplingTolerance = this.samplingInterval.dividedBy( 2L );
        this.qualitySampler = new QualitySampler( clock, samplingTolerance, valueSupplier );
    }

    public void start()
    {
        this.samplingTaskLock.release();
        this.job = this.scheduler.scheduleRecurring( Group.THROUGHPUT_MONITOR, this::samplingTask, this.samplingInterval.toMillis(), TimeUnit.MILLISECONDS );
    }

    public void stop()
    {
        this.job.cancel();
        this.samplingTaskLock.acquireUninterruptibly();
    }

    private boolean tooEarly()
    {
        if ( this.samples.size() == 0 )
        {
            return false;
        }
        else
        {
            Sample<Long> lastSample = (Sample) this.samples.read( this.samples.size() - 1 );
            return Duration.between( lastSample.instant(), this.clock.instant() ).compareTo( this.samplingInterval ) < 0;
        }
    }

    private void samplingTask()
    {
        if ( this.samplingTaskLock.tryAcquire() )
        {
            try
            {
                this.samplingTask0();
            }
            catch ( Throwable var5 )
            {
                this.log.error( "Sampling task failed exceptionally", var5 );
            }
            finally
            {
                this.samplingTaskLock.release();
            }
        }
    }

    private void samplingTask0()
    {
        if ( !this.tooEarly() )
        {
            synchronized ( this )
            {
                Optional<Sample<Long>> sample = this.qualitySampler.sample();
                if ( sample.isPresent() )
                {
                    this.samples.append( (Sample) sample.get() );
                }
                else
                {
                    this.log.warn( "Sampling task failed" );
                }
            }
        }
    }

    public synchronized Optional<Double> throughput()
    {
        Optional<Sample<Long>> optSample = this.qualitySampler.sample();
        if ( optSample.isEmpty() )
        {
            this.log.warn( "Sampling for throughput failed" );
            return Optional.empty();
        }
        else
        {
            Sample<Long> latestSample = (Sample) optSample.get();
            Instant origin = latestSample.instant().minus( this.samplingWindow );
            Sample<Long> bestOldSample = this.findClosestSample( origin );
            if ( bestOldSample == null )
            {
                this.log.warn( "Throughput estimation failed due to lack of older sample" );
                return Optional.empty();
            }
            else
            {
                Duration acceptableDelta = this.samplingWindow.dividedBy( 2L );
                if ( Duration.between( bestOldSample.instant(), origin ).abs().compareTo( acceptableDelta ) > 0 )
                {
                    this.log.warn( "Throughput estimation failed due to lack of acceptable older sample" );
                    return Optional.empty();
                }
                else
                {
                    long timeDiffMillis = Duration.between( bestOldSample.instant(), latestSample.instant() ).toMillis();
                    long valueDiff = (Long) latestSample.value() - (Long) bestOldSample.value();
                    double ratePerSecond = (double) valueDiff / (double) timeDiffMillis * 1000.0D;
                    return Optional.of( ratePerSecond );
                }
            }
        }
    }

    private Sample<Long> findClosestSample( Instant origin )
    {
        Sample<Long> closestSample = null;

        for ( int i = 0; i < this.samples.size(); ++i )
        {
            Sample<Long> other = (Sample) this.samples.read( i );
            if ( other == null )
            {
                break;
            }

            if ( closestSample == null )
            {
                closestSample = other;
            }
            else
            {
                Duration currentClosest = Duration.between( closestSample.instant(), origin ).abs();
                Duration otherDistance = Duration.between( other.instant(), origin ).abs();
                if ( otherDistance.compareTo( currentClosest ) < 0 )
                {
                    closestSample = other;
                }
            }
        }

        return closestSample;
    }
}
