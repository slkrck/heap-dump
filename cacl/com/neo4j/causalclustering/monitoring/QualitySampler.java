package com.neo4j.causalclustering.monitoring;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Supplier;

import org.neo4j.util.Preconditions;

class QualitySampler<T>
{
    private static final int DEFAULT_MAXIMUM_ATTEMPTS = 16;
    private final Clock clock;
    private final Duration samplingTolerance;
    private final int maximumAttempts;
    private final Supplier<T> sampler;

    QualitySampler( Clock clock, Duration samplingTolerance, Supplier<T> sampler )
    {
        this( clock, samplingTolerance, 16, sampler );
    }

    QualitySampler( Clock clock, Duration samplingTolerance, int maximumAttempts, Supplier<T> sampler )
    {
        Preconditions.requirePositive( maximumAttempts );
        this.clock = clock;
        this.samplingTolerance = samplingTolerance;
        this.maximumAttempts = maximumAttempts;
        this.sampler = sampler;
    }

    public Optional<Sample<T>> sample()
    {
        int attempt = 0;

        Sample sample;
        for ( sample = null; sample == null && attempt < this.maximumAttempts; ++attempt )
        {
            sample = this.sample0();
        }

        return Optional.ofNullable( sample );
    }

    private Sample<T> sample0()
    {
        Instant before = this.clock.instant();
        T sample = this.sampler.get();
        Instant after = this.clock.instant();
        Duration samplingDuration = Duration.between( before, after );
        if ( samplingDuration.compareTo( this.samplingTolerance ) > 0 )
        {
            return null;
        }
        else
        {
            Instant sampleTime = before.plus( samplingDuration.dividedBy( 2L ) );
            return new Sample( sampleTime, sample );
        }
    }
}
