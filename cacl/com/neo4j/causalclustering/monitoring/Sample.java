package com.neo4j.causalclustering.monitoring;

import java.time.Instant;
import java.util.Objects;

class Sample<T>
{
    private final Instant instant;
    private final T value;

    Sample( Instant instant, T value )
    {
        this.instant = instant;
        this.value = value;
    }

    Instant instant()
    {
        return this.instant;
    }

    T value()
    {
        return this.value;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            Sample<?> sample = (Sample) o;
            return Objects.equals( this.instant, sample.instant ) && Objects.equals( this.value, sample.value );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.instant, this.value} );
    }

    public String toString()
    {
        return "Sample{instant=" + this.instant + ", value=" + this.value + "}";
    }
}
