package com.neo4j.causalclustering.diagnostics;

import com.neo4j.causalclustering.core.consensus.log.segmented.FileNames;
import com.neo4j.causalclustering.core.state.ClusterStateLayout;
import com.neo4j.causalclustering.core.state.CoreStateFiles;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.diagnostics.DiagnosticsOfflineReportProvider;
import org.neo4j.kernel.diagnostics.DiagnosticsReportSource;
import org.neo4j.kernel.diagnostics.DiagnosticsReportSources;
import org.neo4j.logging.NullLog;

public class ClusterDiagnosticsOfflineReportProvider extends DiagnosticsOfflineReportProvider
{
    private FileSystemAbstraction fs;
    private ClusterStateLayout clusterStateLayout;
    private String defaultDatabaseName;

    public ClusterDiagnosticsOfflineReportProvider()
    {
        super( "raft", new String[]{"ccstate"} );
    }

    public void init( FileSystemAbstraction fs, String defaultDatabaseName, Config config, File storeDirectory )
    {
        this.fs = fs;
        this.clusterStateLayout = ClusterStateLayout.of( ((Path) config.get( GraphDatabaseSettings.data_directory )).toFile() );
        this.defaultDatabaseName = defaultDatabaseName;
    }

    protected List<DiagnosticsReportSource> provideSources( Set<String> classifiers )
    {
        List<DiagnosticsReportSource> sources = new ArrayList();
        if ( classifiers.contains( "raft" ) )
        {
            this.getRaftLogs( sources );
        }

        if ( classifiers.contains( "ccstate" ) )
        {
            this.getClusterState( sources );
        }

        return sources;
    }

    private void getRaftLogs( List<DiagnosticsReportSource> sources )
    {
        File raftLogDirectory = this.clusterStateLayout.raftLogDirectory( this.defaultDatabaseName );
        FileNames fileNames = new FileNames( raftLogDirectory );
        SortedMap<Long,File> allFiles = fileNames.getAllFiles( this.fs, NullLog.getInstance() );
        Iterator var5 = allFiles.values().iterator();

        while ( var5.hasNext() )
        {
            File logFile = (File) var5.next();
            sources.add( DiagnosticsReportSources.newDiagnosticsFile( "raft/" + logFile.getName(), this.fs, logFile ) );
        }
    }

    private void getClusterState( List<DiagnosticsReportSource> sources )
    {
        Set<File> directories = this.clusterStateLayout.listGlobalAndDatabaseDirectories( this.defaultDatabaseName, ( type ) -> {
            return type != CoreStateFiles.RAFT_LOG;
        } );
        Iterator var3 = directories.iterator();

        while ( var3.hasNext() )
        {
            File directory = (File) var3.next();
            this.addDirectory( "ccstate", directory, sources );
        }
    }

    private void addDirectory( String path, File dir, List<DiagnosticsReportSource> sources )
    {
        String currentLevel = path + File.separator + dir.getName();
        if ( this.fs.isDirectory( dir ) )
        {
            File[] files = this.fs.listFiles( dir );
            if ( files != null )
            {
                File[] var6 = files;
                int var7 = files.length;

                for ( int var8 = 0; var8 < var7; ++var8 )
                {
                    File file = var6[var8];
                    this.addDirectory( currentLevel, file, sources );
                }
            }
        }
        else
        {
            sources.add( DiagnosticsReportSources.newDiagnosticsFile( currentLevel, this.fs, dir ) );
        }
    }
}
