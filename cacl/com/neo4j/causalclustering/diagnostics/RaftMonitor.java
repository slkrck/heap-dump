package com.neo4j.causalclustering.diagnostics;

import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;
import com.neo4j.causalclustering.core.state.snapshot.PersistentSnapshotDownloader;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftBinder;
import com.neo4j.causalclustering.identity.RaftId;

import java.time.Clock;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.CappedLogger;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class RaftMonitor implements RaftBinder.Monitor, PersistentSnapshotDownloader.Monitor
{
    private final Log debug;
    private final Log user;
    private final CappedLogger coreMemberWaitLog;
    private final CappedLogger bootstrapWaitLog;
    private final CappedLogger publishRaftIdLog;
    private final CappedLogger discoveryServiceAttemptLog;
    private final CappedLogger initialMembersAttempLog;

    private RaftMonitor( LogService logService, Clock clock )
    {
        this.debug = logService.getInternalLogProvider().getLog( this.getClass() );
        this.user = logService.getUserLogProvider().getLog( this.getClass() );
        this.coreMemberWaitLog = (new CappedLogger( this.user )).setTimeLimit( 10L, TimeUnit.SECONDS, clock );
        this.bootstrapWaitLog = (new CappedLogger( this.user )).setTimeLimit( 10L, TimeUnit.SECONDS, clock );
        this.publishRaftIdLog = (new CappedLogger( this.debug )).setTimeLimit( 5L, TimeUnit.SECONDS, clock );
        this.discoveryServiceAttemptLog = (new CappedLogger( this.debug )).setTimeLimit( 10L, TimeUnit.SECONDS, clock );
        this.initialMembersAttempLog = (new CappedLogger( this.debug )).setTimeLimit( 10L, TimeUnit.SECONDS, clock );
    }

    public static void register( LogService logService, Monitors monitors, Clock clock )
    {
        RaftMonitor raftMonitor = new RaftMonitor( logService, clock );
        monitors.addMonitorListener( raftMonitor, new String[0] );
    }

    public void waitingForCoreMembers( NamedDatabaseId namedDatabaseId, int minimumCount )
    {
        this.coreMemberWaitLog.info( "Database '%s' is waiting for a total of %d core members...", new Object[]{namedDatabaseId.name(), minimumCount} );
    }

    public void waitingForBootstrap( NamedDatabaseId namedDatabaseId )
    {
        this.bootstrapWaitLog.info( "Database '%s' is waiting for bootstrap by other instance...", new Object[]{namedDatabaseId.name()} );
    }

    public void bootstrapped( CoreSnapshot snapshot, NamedDatabaseId namedDatabaseId, RaftId raftId )
    {
        this.user.info( String.format( "This instance bootstrapped a raft for database '%s'.", namedDatabaseId.name() ) );
        this.debug.info( String.format( "Bootstrapped %s with %s using %s", namedDatabaseId, raftId, snapshot ) );
    }

    public void boundToRaftFromDisk( NamedDatabaseId namedDatabaseId, RaftId raftId )
    {
        this.user.info( String.format( "Bound database '%s' to raft with id '%s', found on disk.", namedDatabaseId.name(), raftId.uuid() ) );
    }

    public void boundToRaftThroughTopology( NamedDatabaseId namedDatabaseId, RaftId raftId )
    {
        this.user.info( String.format( "Bound database '%s' to raft with id '%s'.", namedDatabaseId.name(), raftId.uuid() ) );
    }

    public void startedDownloadingSnapshot( NamedDatabaseId namedDatabaseId )
    {
        this.user.info( "Started downloading snapshot for database '%s'...", new Object[]{namedDatabaseId.name()} );
    }

    public void downloadSnapshotComplete( NamedDatabaseId namedDatabaseId )
    {
        this.user.info( "Download of snapshot for database '%s' complete.", new Object[]{namedDatabaseId.name()} );
    }

    public void retryPublishRaftId( NamedDatabaseId namedDatabaseId, RaftId raftId )
    {
        this.publishRaftIdLog.info( "Failed to publish RaftId %s for database %s. Retrying", new Object[]{raftId, namedDatabaseId.name()} );
    }

    public void logSaveSystemDatabase()
    {
        this.debug.info( "Temporarily moving system database to force store copy" );
    }

    public void logBootstrapAttemptWithDiscoveryService()
    {
        this.discoveryServiceAttemptLog.info( "Trying bootstrap using discovery service method", new Object[0] );
    }

    public void logBootstrapWithInitialMembersAndStoreID( Set<MemberId> initialMembers, StoreId storeId )
    {
        this.initialMembersAttempLog.info( "Trying bootstrap using initial members %s and store ID %s", new Object[]{initialMembers, storeId} );
    }
}
