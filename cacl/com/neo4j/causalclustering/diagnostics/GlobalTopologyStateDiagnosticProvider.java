package com.neo4j.causalclustering.diagnostics;

import com.neo4j.causalclustering.discovery.TopologyService;
import org.neo4j.internal.diagnostics.DiagnosticsProvider;
import org.neo4j.internal.helpers.Strings;
import org.neo4j.logging.Logger;

public class GlobalTopologyStateDiagnosticProvider implements DiagnosticsProvider
{
    private final TopologyService topologyService;

    public GlobalTopologyStateDiagnosticProvider( TopologyService topologyService )
    {
        this.topologyService = topologyService;
    }

    private static String newPaddedLIne()
    {
        return System.lineSeparator() + "  ";
    }

    public String getDiagnosticsName()
    {
        return "Global topology state";
    }

    public void dump( Logger logger )
    {
        logger.log( "Current core topology:%s%s", new Object[]{newPaddedLIne(), Strings.printMap( this.topologyService.allCoreServers(), newPaddedLIne() )} );
        logger.log( "Current read replica topology:%s%s",
                new Object[]{newPaddedLIne(), Strings.printMap( this.topologyService.allReadReplicas(), newPaddedLIne() )} );
    }
}
