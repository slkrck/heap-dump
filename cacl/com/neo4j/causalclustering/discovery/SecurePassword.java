package com.neo4j.causalclustering.discovery;

import java.security.SecureRandom;
import java.util.Arrays;

public class SecurePassword implements AutoCloseable
{
    private static final int lowerBound = 32;
    private static final int upperBound = 126;
    private static final int range = 94;
    private final char[] password;

    public SecurePassword( int length, SecureRandom random )
    {
        this.password = new char[length];

        for ( int i = 0; i < this.password.length; ++i )
        {
            this.password[i] = (char) (random.nextInt( 94 ) + 32);
        }
    }

    public void close()
    {
        Arrays.fill( this.password, '\u0000' );
    }

    public char[] password()
    {
        return this.password;
    }
}
