package com.neo4j.causalclustering.discovery;

import akka.remote.artery.tcp.SSLEngineProvider;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;

import org.neo4j.configuration.ssl.ClientAuth;
import org.neo4j.ssl.ClientSideHostnameVerificationEngineModification;
import org.neo4j.ssl.EssentialEngineModifications;
import org.neo4j.ssl.SslPolicy;
import scala.Option;

public class AkkaDiscoverySSLEngineProvider implements SSLEngineProvider
{
    private final SslPolicy sslPolicy;
    private final SSLContext sslContext;

    public AkkaDiscoverySSLEngineProvider( SslPolicy sslPolicy )
    {
        this.sslPolicy = sslPolicy;
        this.sslContext = (new DiscoverySSLContextFactory( this.sslPolicy )).sslContext();
    }

    public SSLEngine createServerSSLEngine( String hostname, int port )
    {
        SSLEngine sslEngine = this.createSSLEngine( false, hostname, port );
        sslEngine.setWantClientAuth( ClientAuth.OPTIONAL.equals( this.sslPolicy.getClientAuth() ) );
        sslEngine.setNeedClientAuth( ClientAuth.REQUIRE.equals( this.sslPolicy.getClientAuth() ) );
        return sslEngine;
    }

    public SSLEngine createClientSSLEngine( String hostname, int port )
    {
        SSLEngine sslEngine = this.createSSLEngine( true, hostname, port );
        if ( this.sslPolicy.isVerifyHostname() )
        {
            sslEngine = (new ClientSideHostnameVerificationEngineModification()).apply( sslEngine );
        }

        return sslEngine;
    }

    private SSLEngine createSSLEngine( boolean isClient, String hostname, int port )
    {
        SSLEngine sslEngine = this.sslContext.createSSLEngine( hostname, port );
        sslEngine = (new EssentialEngineModifications( this.sslPolicy.getTlsVersions(), isClient )).apply( sslEngine );
        if ( this.sslPolicy.getCipherSuites() != null )
        {
            sslEngine.setEnabledCipherSuites( (String[]) this.sslPolicy.getCipherSuites().toArray( new String[0] ) );
        }

        return sslEngine;
    }

    public Option<Throwable> verifyClientSession( String hostname, SSLSession session )
    {
        return Option.apply( (Object) null );
    }

    public Option<Throwable> verifyServerSession( String hostname, SSLSession session )
    {
        return Option.apply( (Object) null );
    }
}
