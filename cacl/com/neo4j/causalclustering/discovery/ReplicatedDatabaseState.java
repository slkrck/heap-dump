package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.neo4j.kernel.database.DatabaseId;

public class ReplicatedDatabaseState
{
    private final DatabaseId databaseId;
    private final Map<MemberId,DiscoveryDatabaseState> memberStates;
    private final boolean coreStates;
    private final String name;

    private ReplicatedDatabaseState( DatabaseId databaseId, Map<MemberId,DiscoveryDatabaseState> memberStates, boolean isCoreStates )
    {
        this.databaseId = databaseId;
        this.memberStates = memberStates;
        this.coreStates = isCoreStates;
        this.name = isCoreStates ? "CoreReplicatedDatabaseState" : "ReadReplicaReplicatedDatabaseState";
    }

    public static ReplicatedDatabaseState ofCores( DatabaseId databaseId, Map<MemberId,DiscoveryDatabaseState> memberStates )
    {
        return new ReplicatedDatabaseState( databaseId, memberStates, true );
    }

    public static ReplicatedDatabaseState ofReadReplicas( DatabaseId databaseId, Map<MemberId,DiscoveryDatabaseState> memberStates )
    {
        return new ReplicatedDatabaseState( databaseId, memberStates, false );
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }

    public Map<MemberId,DiscoveryDatabaseState> memberStates()
    {
        return this.memberStates;
    }

    public Optional<DiscoveryDatabaseState> stateFor( MemberId memberId )
    {
        return Optional.ofNullable( (DiscoveryDatabaseState) this.memberStates.get( memberId ) );
    }

    public boolean isEmpty()
    {
        return this.memberStates.isEmpty();
    }

    public boolean containsCoreStates()
    {
        return this.coreStates;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReplicatedDatabaseState that = (ReplicatedDatabaseState) o;
            return this.coreStates == that.coreStates && Objects.equals( this.databaseId, that.databaseId ) &&
                    Objects.equals( this.memberStates, that.memberStates );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseId, this.memberStates, this.coreStates} );
    }

    public String toString()
    {
        return String.format( "%s{%s}", this.name, this.memberStates );
    }
}
