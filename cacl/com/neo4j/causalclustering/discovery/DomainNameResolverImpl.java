package com.neo4j.causalclustering.discovery;

import java.net.InetAddress;

public class DomainNameResolverImpl implements DomainNameResolver
{
    public InetAddress[] resolveDomainName( String hostname ) throws UnknownHostException
    {
        try
        {
            return InetAddress.getAllByName( hostname );
        }
        catch ( java.net.UnknownHostException var3 )
        {
            throw new UnknownHostException( var3 );
        }
    }
}
