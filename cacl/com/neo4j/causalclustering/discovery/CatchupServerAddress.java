package com.neo4j.causalclustering.discovery;

import org.neo4j.configuration.helpers.SocketAddress;

public interface CatchupServerAddress
{
    SocketAddress catchupServer();
}
