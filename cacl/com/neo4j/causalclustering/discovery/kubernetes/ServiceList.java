package com.neo4j.causalclustering.discovery.kubernetes;

import java.util.List;

public class ServiceList extends KubernetesType
{
    private List<ServiceList.Service> items;

    public List<ServiceList.Service> items()
    {
        return this.items;
    }

    public void setItems( List<ServiceList.Service> items )
    {
        this.items = items;
    }

    public <T> T handle( KubernetesType.Visitor<T> visitor )
    {
        return visitor.visit( this );
    }

    public static class Service
    {
        private ObjectMetadata metadata;
        private ServiceList.Service.ServiceSpec spec;

        public ObjectMetadata metadata()
        {
            return this.metadata;
        }

        public ServiceList.Service.ServiceSpec spec()
        {
            return this.spec;
        }

        public void setMetadata( ObjectMetadata metadata )
        {
            this.metadata = metadata;
        }

        public void setSpec( ServiceList.Service.ServiceSpec spec )
        {
            this.spec = spec;
        }

        public static class ServiceSpec
        {
            private List<ServiceList.Service.ServiceSpec.ServicePort> ports;

            public List<ServiceList.Service.ServiceSpec.ServicePort> ports()
            {
                return this.ports;
            }

            public void setPorts( List<ServiceList.Service.ServiceSpec.ServicePort> ports )
            {
                this.ports = ports;
            }

            public static class ServicePort
            {
                private String name;
                private int port;
                private String protocol;

                public String name()
                {
                    return this.name;
                }

                public int port()
                {
                    return this.port;
                }

                public String protocol()
                {
                    return this.protocol;
                }

                public void setName( String name )
                {
                    this.name = name;
                }

                public void setPort( int port )
                {
                    this.port = port;
                }

                public void setProtocol( String protocol )
                {
                    this.protocol = protocol;
                }
            }
        }
    }
}
