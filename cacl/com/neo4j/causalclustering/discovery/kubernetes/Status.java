package com.neo4j.causalclustering.discovery.kubernetes;

public class Status extends KubernetesType
{
    private String status;
    private String message;
    private String reason;
    private int code;

    public String status()
    {
        return this.status;
    }

    public void setStatus( String status )
    {
        this.status = status;
    }

    public String message()
    {
        return this.message;
    }

    public void setMessage( String message )
    {
        this.message = message;
    }

    public String reason()
    {
        return this.reason;
    }

    public void setReason( String reason )
    {
        this.reason = reason;
    }

    public int code()
    {
        return this.code;
    }

    public void setCode( int code )
    {
        this.code = code;
    }

    public <T> T handle( KubernetesType.Visitor<T> visitor )
    {
        return visitor.visit( this );
    }

    public String toString()
    {
        return "Status{status='" + this.status + "', message='" + this.message + "', reason='" + this.reason + "', code=" + this.code + "}";
    }
}
