package com.neo4j.causalclustering.discovery.kubernetes;

public class ObjectMetadata
{
    private String deletionTimestamp;
    private String name;

    public String deletionTimestamp()
    {
        return this.deletionTimestamp;
    }

    public String name()
    {
        return this.name;
    }

    public void setDeletionTimestamp( String deletionTimestamp )
    {
        this.deletionTimestamp = deletionTimestamp;
    }

    public void setName( String name )
    {
        this.name = name;
    }
}
