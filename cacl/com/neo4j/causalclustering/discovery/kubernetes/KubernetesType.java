package com.neo4j.causalclustering.discovery.kubernetes;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo( use = Id.NAME, property = "kind" )
@JsonSubTypes( {@Type( value = ServiceList.class, name = "ServiceList" ), @Type( value = Status.class, name = "Status" )} )
public abstract class KubernetesType
{
    private String kind;

    public String kind()
    {
        return this.kind;
    }

    public void setKind( String kind )
    {
        this.kind = kind;
    }

    public abstract <T> T handle( KubernetesType.Visitor<T> var1 );

    public interface Visitor<T>
    {
        T visit( Status var1 );

        T visit( ServiceList var1 );
    }
}
