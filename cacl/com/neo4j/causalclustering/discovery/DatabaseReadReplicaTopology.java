package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class DatabaseReadReplicaTopology implements Topology<ReadReplicaInfo>
{
    private final DatabaseId databaseId;
    private final Map<MemberId,ReadReplicaInfo> readReplicaMembers;

    public DatabaseReadReplicaTopology( DatabaseId databaseId, Map<MemberId,ReadReplicaInfo> readReplicaMembers )
    {
        this.databaseId = (DatabaseId) Objects.requireNonNull( databaseId );
        this.readReplicaMembers = readReplicaMembers;
    }

    public static DatabaseReadReplicaTopology empty( DatabaseId databaseId )
    {
        return new DatabaseReadReplicaTopology( databaseId, Collections.emptyMap() );
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }

    public Map<MemberId,ReadReplicaInfo> members()
    {
        return this.readReplicaMembers;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            DatabaseReadReplicaTopology that = (DatabaseReadReplicaTopology) o;
            return Objects.equals( this.databaseId, that.databaseId ) && Objects.equals( this.readReplicaMembers, that.readReplicaMembers );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseId, this.readReplicaMembers} );
    }

    public String toString()
    {
        return String.format( "DatabaseReadReplicaTopology{%s %s}", this.databaseId, this.readReplicaMembers.keySet() );
    }
}
