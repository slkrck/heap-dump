package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class DatabaseCoreTopology implements Topology<CoreServerInfo>
{
    private final DatabaseId databaseId;
    private final RaftId raftId;
    private final Map<MemberId,CoreServerInfo> coreMembers;

    public DatabaseCoreTopology( DatabaseId databaseId, RaftId raftId, Map<MemberId,CoreServerInfo> coreMembers )
    {
        this.databaseId = (DatabaseId) Objects.requireNonNull( databaseId );
        this.raftId = raftId;
        this.coreMembers = Map.copyOf( coreMembers );
    }

    public static DatabaseCoreTopology empty( DatabaseId databaseId )
    {
        return new DatabaseCoreTopology( databaseId, (RaftId) null, Collections.emptyMap() );
    }

    public Map<MemberId,CoreServerInfo> members()
    {
        return this.coreMembers;
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }

    public RaftId raftId()
    {
        return this.raftId;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            DatabaseCoreTopology that = (DatabaseCoreTopology) o;
            return Objects.equals( this.databaseId, that.databaseId ) && Objects.equals( this.raftId, that.raftId ) &&
                    Objects.equals( this.coreMembers, that.coreMembers );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseId, this.raftId, this.coreMembers} );
    }

    public String toString()
    {
        return String.format( "DatabaseCoreTopology{%s %s}", this.databaseId, this.coreMembers.keySet() );
    }
}
