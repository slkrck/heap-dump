package com.neo4j.causalclustering.discovery;

import java.util.Set;

import org.neo4j.kernel.database.DatabaseId;

public interface DiscoveryServerInfo extends CatchupServerAddress, ClientConnector, GroupedServer
{
    Set<DatabaseId> startedDatabaseIds();
}
