package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Map;

import org.neo4j.kernel.database.DatabaseId;

public interface Topology<T extends DiscoveryServerInfo>
{
    DatabaseId databaseId();

    Map<MemberId,T> members();
}
