package com.neo4j.causalclustering.discovery;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.neo4j.ssl.SslPolicy;

public class DiscoverySSLContextFactory
{
    public static final String PROTOCOL = "TLS";
    private final SslPolicy sslPolicy;

    public DiscoverySSLContextFactory( SslPolicy sslPolicy )
    {
        this.sslPolicy = sslPolicy;
    }

    public SSLContext sslContext()
    {
        try
        {
            SecurePassword securePassword = new SecurePassword( 32, new SecureRandom() );

            SSLContext var8;
            try
            {
                char[] password = securePassword.password();
                KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance( KeyManagerFactory.getDefaultAlgorithm() );
                KeyStore keyStore = this.sslPolicy.getKeyStore( password, password );
                keyManagerFactory.init( keyStore, password );
                SSLContext sslContext = SSLContext.getInstance( "TLS" );
                KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
                TrustManager[] trustManagers = this.sslPolicy.getTrustManagerFactory().getTrustManagers();
                sslContext.init( keyManagers, trustManagers, (SecureRandom) null );
                var8 = sslContext;
            }
            catch ( Throwable var10 )
            {
                try
                {
                    securePassword.close();
                }
                catch ( Throwable var9 )
                {
                    var10.addSuppressed( var9 );
                }

                throw var10;
            }

            securePassword.close();
            return var8;
        }
        catch ( KeyManagementException | UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException var11 )
        {
            throw new RuntimeException( "Error creating SSL context", var11 );
        }
    }
}
