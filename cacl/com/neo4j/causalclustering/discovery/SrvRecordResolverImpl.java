package com.neo4j.causalclustering.discovery;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Optional;
import java.util.Properties;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;

public class SrvRecordResolverImpl extends SrvRecordResolver
{
    private final String[] SRV_RECORDS = new String[]{"SRV"};
    private final String SRV_ATTR = "srv";
    private Optional<InitialDirContext> _idc = Optional.empty();

    private static <T> Stream<T> enumerationAsStream( final Enumeration<T> e )
    {
        return StreamSupport.stream( Spliterators.spliteratorUnknownSize( new Iterator<T>()
        {
            public T next()
            {
                return e.nextElement();
            }

            public boolean hasNext()
            {
                return e.hasMoreElements();
            }
        }, 16 ), false );
    }

    public Stream<SrvRecordResolver.SrvRecord> resolveSrvRecord( String url ) throws NamingException
    {
        Attributes attrs = ((InitialDirContext) this._idc.orElseGet( this::setIdc )).getAttributes( url, this.SRV_RECORDS );
        return enumerationAsStream( attrs.get( "srv" ).getAll() ).map( SrvRecordResolver.SrvRecord::parse );
    }

    private synchronized InitialDirContext setIdc()
    {
        return (InitialDirContext) this._idc.orElseGet( () -> {
            Properties env = new Properties();
            env.put( "java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory" );

            try
            {
                this._idc = Optional.of( new InitialDirContext( env ) );
                return (InitialDirContext) this._idc.get();
            }
            catch ( NamingException var3 )
            {
                throw new RuntimeException( var3 );
            }
        } );
    }
}
