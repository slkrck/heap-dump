package com.neo4j.causalclustering.discovery.akka;

import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;

public interface DatabaseStateUpdateSink
{
    void onDbStateUpdate( ReplicatedDatabaseState var1 );
}
