package com.neo4j.causalclustering.discovery.akka;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.DiscoveryServerInfo;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;
import com.neo4j.causalclustering.discovery.RoleInfo;
import com.neo4j.causalclustering.discovery.Topology;
import com.neo4j.causalclustering.discovery.akka.coretopology.BootstrapState;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.Strings;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class GlobalTopologyState implements TopologyUpdateSink, DirectoryUpdateSink, BootstrapStateUpdateSink, DatabaseStateUpdateSink
{
    private final Log log;
    private final Consumer<DatabaseCoreTopology> callback;
    private final Map<DatabaseId,DatabaseCoreTopology> coreTopologiesByDatabase;
    private final Map<DatabaseId,DatabaseReadReplicaTopology> readReplicaTopologiesByDatabase;
    private final Map<DatabaseId,ReplicatedDatabaseState> coreStatesByDatabase;
    private final Map<DatabaseId,ReplicatedDatabaseState> readReplicaStatesByDatabase;
    private volatile Map<MemberId,CoreServerInfo> coresByMemberId;
    private volatile Map<MemberId,ReadReplicaInfo> readReplicasByMemberId;
    private volatile Map<DatabaseId,LeaderInfo> remoteDbLeaderMap;
    private volatile BootstrapState bootstrapState;

    GlobalTopologyState( LogProvider logProvider, Consumer<DatabaseCoreTopology> listener )
    {
        this.bootstrapState = BootstrapState.EMPTY;
        this.coreTopologiesByDatabase = new ConcurrentHashMap();
        this.readReplicaTopologiesByDatabase = new ConcurrentHashMap();
        this.coreStatesByDatabase = new ConcurrentHashMap();
        this.readReplicaStatesByDatabase = new ConcurrentHashMap();
        this.log = logProvider.getLog( this.getClass() );
        this.coresByMemberId = Collections.emptyMap();
        this.readReplicasByMemberId = Collections.emptyMap();
        this.remoteDbLeaderMap = Collections.emptyMap();
        this.callback = listener;
    }

    private static String printLeaderInfoMap( Map<DatabaseId,LeaderInfo> leaderInfoMap, Map<DatabaseId,LeaderInfo> oldDbLeaderMap )
    {
        HashSet<DatabaseId> allDatabaseIds = new HashSet( leaderInfoMap.keySet() );
        allDatabaseIds.addAll( oldDbLeaderMap.keySet() );
        return allDatabaseIds.isEmpty() ? "No leader information was detected for any database" : (String) allDatabaseIds.stream().map( ( dbId ) -> {
            LeaderInfo oldLeader = (LeaderInfo) oldDbLeaderMap.get( dbId );
            LeaderInfo newLeader = (LeaderInfo) leaderInfoMap.get( dbId );
            if ( oldLeader == null )
            {
                return newLeader == null ? String.format( "No leader for database %s", dbId )
                                         : String.format( "Discovered leader %s in term %d for database %s", newLeader.memberId(), newLeader.term(), dbId );
            }
            else
            {
                return newLeader == null ? String.format( "Database %s lost its leader. Previous leader was %s", dbId, oldLeader.memberId() )
                                         : String.format( "Database %s switch leader from %s to %s in term %d", dbId, oldLeader.memberId(),
                                                 newLeader.memberId(), newLeader.term() );
            }
        } ).collect( Collectors.joining( newPaddedLine() ) );
    }

    private static String newPaddedLine()
    {
        return System.lineSeparator() + "  ";
    }

    private static <T extends DiscoveryServerInfo> Map<MemberId,T> extractServerInfos( Map<DatabaseId,? extends Topology<T>> topologies )
    {
        Map<MemberId,T> result = new HashMap();
        Iterator var2 = topologies.values().iterator();

        while ( var2.hasNext() )
        {
            Topology<T> topology = (Topology) var2.next();
            Iterator var4 = topology.members().entrySet().iterator();

            while ( var4.hasNext() )
            {
                Entry<MemberId,T> entry = (Entry) var4.next();
                result.put( (MemberId) entry.getKey(), (DiscoveryServerInfo) entry.getValue() );
            }
        }

        return Collections.unmodifiableMap( result );
    }

    private static boolean hasNoMembers( Topology<?> topology )
    {
        return topology.members().isEmpty();
    }

    public void onTopologyUpdate( DatabaseCoreTopology newCoreTopology )
    {
        DatabaseId databaseId = newCoreTopology.databaseId();
        DatabaseCoreTopology currentCoreTopology = (DatabaseCoreTopology) this.coreTopologiesByDatabase.put( databaseId, newCoreTopology );
        if ( !Objects.equals( currentCoreTopology, newCoreTopology ) )
        {
            if ( currentCoreTopology == null )
            {
                currentCoreTopology = DatabaseCoreTopology.empty( databaseId );
            }

            this.coresByMemberId = extractServerInfos( this.coreTopologiesByDatabase );
            this.logTopologyChange( "Core topology", newCoreTopology, databaseId, currentCoreTopology );
            this.callback.accept( newCoreTopology );
        }

        if ( hasNoMembers( newCoreTopology ) )
        {
            this.coreTopologiesByDatabase.remove( databaseId, newCoreTopology );
        }
    }

    public void onTopologyUpdate( DatabaseReadReplicaTopology newReadReplicaTopology )
    {
        DatabaseId databaseId = newReadReplicaTopology.databaseId();
        DatabaseReadReplicaTopology currentReadReplicaTopology =
                (DatabaseReadReplicaTopology) this.readReplicaTopologiesByDatabase.put( databaseId, newReadReplicaTopology );
        if ( !Objects.equals( currentReadReplicaTopology, newReadReplicaTopology ) )
        {
            if ( currentReadReplicaTopology == null )
            {
                currentReadReplicaTopology = DatabaseReadReplicaTopology.empty( databaseId );
            }

            this.readReplicasByMemberId = extractServerInfos( this.readReplicaTopologiesByDatabase );
            this.logTopologyChange( "Read replica topology", newReadReplicaTopology, databaseId, currentReadReplicaTopology );
        }

        if ( hasNoMembers( newReadReplicaTopology ) )
        {
            this.readReplicaTopologiesByDatabase.remove( databaseId, newReadReplicaTopology );
        }
    }

    public void onDbLeaderUpdate( Map<DatabaseId,LeaderInfo> leaderInfoMap )
    {
        if ( !leaderInfoMap.equals( this.remoteDbLeaderMap ) )
        {
            this.log.info( "Database leader(s) update:%s%s", new Object[]{newPaddedLine(), printLeaderInfoMap( leaderInfoMap, this.remoteDbLeaderMap )} );
            this.remoteDbLeaderMap = leaderInfoMap;
        }
    }

    public void onDbStateUpdate( ReplicatedDatabaseState newState )
    {
        String role = newState.containsCoreStates() ? "core" : "read_replica";
        Map<DatabaseId,ReplicatedDatabaseState> statesByDatabase = newState.containsCoreStates() ? this.coreStatesByDatabase : this.readReplicaStatesByDatabase;
        DatabaseId databaseId = newState.databaseId();
        ReplicatedDatabaseState previousState = (ReplicatedDatabaseState) statesByDatabase.put( databaseId, newState );
        if ( !Objects.equals( previousState, newState ) )
        {
            StringBuilder stringBuilder = (new StringBuilder( String.format( "The %s replicated states for database %s changed", role, databaseId ) )).append(
                    System.lineSeparator() );
            if ( previousState == null )
            {
                stringBuilder.append( "previous state was empty" );
            }
            else
            {
                stringBuilder.append( "previous state was:" ).append( newPaddedLine() ).append(
                        Strings.printMap( previousState.memberStates(), newPaddedLine() ) );
            }

            stringBuilder.append( System.lineSeparator() );
            if ( newState.isEmpty() )
            {
                stringBuilder.append( "current state is empty" );
            }
            else
            {
                stringBuilder.append( "current state is:" ).append( newPaddedLine() ).append( Strings.printMap( newState.memberStates(), newPaddedLine() ) );
            }

            this.log.info( stringBuilder.toString() );
        }

        if ( newState.isEmpty() )
        {
            statesByDatabase.remove( databaseId, newState );
        }
    }

    public void onBootstrapStateUpdate( BootstrapState newBootstrapState )
    {
        this.bootstrapState = (BootstrapState) Objects.requireNonNull( newBootstrapState );
    }

    private void logTopologyChange( String topologyDescription, Topology<?> newTopology, DatabaseId databaseId, Topology<?> oldTopology )
    {
        Set<MemberId> allMembers = Collections.unmodifiableSet( newTopology.members().keySet() );
        HashSet<MemberId> lostMembers = new HashSet( oldTopology.members().keySet() );
        lostMembers.removeAll( allMembers );
        HashMap<MemberId,? extends DiscoveryServerInfo> newMembers = new HashMap( newTopology.members() );
        newMembers.keySet().removeAll( oldTopology.members().keySet() );
        String var10000 = String.format( "%s for database %s is now: %s", topologyDescription, databaseId, allMembers.isEmpty() ? "empty" : allMembers );
        String logLine =
                var10000 + System.lineSeparator() + (lostMembers.isEmpty() ? "No members where lost" : String.format( "Lost members :%s", lostMembers )) +
                        System.lineSeparator() + (newMembers.isEmpty() ? "No new members" : String.format( "New members: %s%s", newPaddedLine(),
                        Strings.printMap( newMembers, newPaddedLine() ) ));
        this.log.info( logLine );
    }

    public RoleInfo role( NamedDatabaseId namedDatabaseId, MemberId memberId )
    {
        DatabaseId databaseId = namedDatabaseId.databaseId();
        DatabaseReadReplicaTopology rrTopology = (DatabaseReadReplicaTopology) this.readReplicaTopologiesByDatabase.get( databaseId );
        DatabaseCoreTopology coreTopology = (DatabaseCoreTopology) this.coreTopologiesByDatabase.get( databaseId );
        if ( coreTopology != null && coreTopology.members().containsKey( memberId ) )
        {
            LeaderInfo leaderInfo = (LeaderInfo) this.remoteDbLeaderMap.getOrDefault( databaseId, LeaderInfo.INITIAL );
            return Objects.equals( memberId, leaderInfo.memberId() ) ? RoleInfo.LEADER : RoleInfo.FOLLOWER;
        }
        else
        {
            return rrTopology != null && rrTopology.members().containsKey( memberId ) ? RoleInfo.READ_REPLICA : RoleInfo.UNKNOWN;
        }
    }

    public Map<MemberId,CoreServerInfo> allCoreServers()
    {
        return this.coresByMemberId;
    }

    public Map<MemberId,ReadReplicaInfo> allReadReplicas()
    {
        return this.readReplicasByMemberId;
    }

    public DatabaseCoreTopology coreTopologyForDatabase( NamedDatabaseId namedDatabaseId )
    {
        DatabaseId databaseId = namedDatabaseId.databaseId();
        DatabaseCoreTopology topology = (DatabaseCoreTopology) this.coreTopologiesByDatabase.get( databaseId );
        return topology != null ? topology : DatabaseCoreTopology.empty( databaseId );
    }

    public DatabaseReadReplicaTopology readReplicaTopologyForDatabase( NamedDatabaseId namedDatabaseId )
    {
        DatabaseId databaseId = namedDatabaseId.databaseId();
        DatabaseReadReplicaTopology topology = (DatabaseReadReplicaTopology) this.readReplicaTopologiesByDatabase.get( databaseId );
        return topology != null ? topology : DatabaseReadReplicaTopology.empty( databaseId );
    }

    public LeaderInfo getLeader( NamedDatabaseId namedDatabaseId )
    {
        return (LeaderInfo) this.remoteDbLeaderMap.get( namedDatabaseId.databaseId() );
    }

    SocketAddress retrieveCatchupServerAddress( MemberId memberId )
    {
        CoreServerInfo coreServerInfo = (CoreServerInfo) this.coresByMemberId.get( memberId );
        if ( coreServerInfo != null )
        {
            SocketAddress address = coreServerInfo.catchupServer();
            this.log.debug( "Catchup address for core %s was %s", new Object[]{memberId, address} );
            return coreServerInfo.catchupServer();
        }
        else
        {
            ReadReplicaInfo readReplicaInfo = (ReadReplicaInfo) this.readReplicasByMemberId.get( memberId );
            if ( readReplicaInfo != null )
            {
                SocketAddress address = readReplicaInfo.catchupServer();
                this.log.debug( "Catchup address for read replica %s was %s", new Object[]{memberId, address} );
                return address;
            }
            else
            {
                this.log.debug( "Catchup address for member %s not found", new Object[]{memberId} );
                return null;
            }
        }
    }

    DiscoveryDatabaseState stateFor( MemberId memberId, NamedDatabaseId namedDatabaseId )
    {
        DatabaseId databaseId = namedDatabaseId.databaseId();
        return (DiscoveryDatabaseState) this.lookupState( memberId, databaseId, this.coreStatesByDatabase ).or( () -> {
            return this.lookupState( memberId, databaseId, this.readReplicaStatesByDatabase );
        } ).orElse( DiscoveryDatabaseState.unknown( namedDatabaseId.databaseId() ) );
    }

    ReplicatedDatabaseState coreStatesForDatabase( NamedDatabaseId namedDatabaseId )
    {
        DatabaseId databaseId = namedDatabaseId.databaseId();
        return (ReplicatedDatabaseState) this.coreStatesByDatabase.getOrDefault( databaseId, ReplicatedDatabaseState.ofCores( databaseId, Map.of() ) );
    }

    ReplicatedDatabaseState readReplicaStatesForDatabase( NamedDatabaseId namedDatabaseId )
    {
        DatabaseId databaseId = namedDatabaseId.databaseId();
        return (ReplicatedDatabaseState) this.readReplicaStatesByDatabase.getOrDefault( databaseId,
                ReplicatedDatabaseState.ofReadReplicas( databaseId, Map.of() ) );
    }

    private Optional<DiscoveryDatabaseState> lookupState( MemberId memberId, DatabaseId databaseId, Map<DatabaseId,ReplicatedDatabaseState> states )
    {
        return Optional.ofNullable( (ReplicatedDatabaseState) states.get( databaseId ) ).flatMap( ( replicated ) -> {
            return replicated.stateFor( memberId );
        } );
    }

    BootstrapState bootstrapState()
    {
        return this.bootstrapState;
    }
}
