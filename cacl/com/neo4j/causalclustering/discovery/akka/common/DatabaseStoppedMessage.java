package com.neo4j.causalclustering.discovery.akka.common;

import java.util.Objects;

import org.neo4j.kernel.database.NamedDatabaseId;

public class DatabaseStoppedMessage
{
    private final NamedDatabaseId namedDatabaseId;

    public DatabaseStoppedMessage( NamedDatabaseId namedDatabaseId )
    {
        this.namedDatabaseId = (NamedDatabaseId) Objects.requireNonNull( namedDatabaseId );
    }

    public NamedDatabaseId namedDatabaseId()
    {
        return this.namedDatabaseId;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            DatabaseStoppedMessage that = (DatabaseStoppedMessage) o;
            return Objects.equals( this.namedDatabaseId, that.namedDatabaseId );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.namedDatabaseId} );
    }

    public String toString()
    {
        return "DatabaseStoppedMessage{databaseId=" + this.namedDatabaseId + "}";
    }
}
