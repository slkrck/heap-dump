package com.neo4j.causalclustering.discovery.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.client.ClusterClient;
import akka.cluster.client.ClusterClientSettings;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.neo4j.causalclustering.catchup.CatchupAddressResolutionException;
import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;
import com.neo4j.causalclustering.discovery.RoleInfo;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import com.neo4j.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.discovery.akka.readreplicatopology.ClientTopologyActor;
import com.neo4j.causalclustering.discovery.akka.system.ActorSystemLifecycle;
import com.neo4j.causalclustering.discovery.member.DiscoveryMember;
import com.neo4j.causalclustering.discovery.member.DiscoveryMemberFactory;
import com.neo4j.causalclustering.identity.MemberId;

import java.time.Clock;
import java.util.Map;
import java.util.Objects;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.DatabaseState;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.SafeLifecycle;
import org.neo4j.logging.LogProvider;
import org.neo4j.util.VisibleForTesting;

public class AkkaTopologyClient extends SafeLifecycle implements TopologyService
{
    private final Config config;
    private final ActorSystemLifecycle actorSystemLifecycle;
    private final DiscoveryMemberFactory discoveryMemberFactory;
    private final MemberId myself;
    private final LogProvider logProvider;
    private final Clock clock;
    private volatile ActorRef clientTopologyActorRef;
    private volatile GlobalTopologyState globalTopologyState;

    AkkaTopologyClient( Config config, LogProvider logProvider, MemberId myself, ActorSystemLifecycle actorSystemLifecycle,
            DiscoveryMemberFactory discoveryMemberFactory, Clock clock )
    {
        this.config = config;
        this.myself = myself;
        this.actorSystemLifecycle = actorSystemLifecycle;
        this.discoveryMemberFactory = discoveryMemberFactory;
        this.logProvider = logProvider;
        this.globalTopologyState = newGlobalTopologyState( logProvider );
        this.clock = clock;
    }

    private static GlobalTopologyState newGlobalTopologyState( LogProvider logProvider )
    {
        return new GlobalTopologyState( logProvider, ( ignored ) -> {
        } );
    }

    public void start0()
    {
        this.actorSystemLifecycle.createClientActorSystem();
        this.startTopologyActors();
    }

    private void startTopologyActors()
    {
        ClusterClientSettings clusterClientSettings = this.actorSystemLifecycle.clusterClientSettings();
        ActorRef clusterClient = this.actorSystemLifecycle.systemActorOf( ClusterClient.props( clusterClientSettings ), "cluster-client" );
        ActorSystemLifecycle var10000 = this.actorSystemLifecycle;
        GlobalTopologyState var10001 = this.globalTopologyState;
        Objects.requireNonNull( var10001 );
        SourceQueueWithComplete<DatabaseCoreTopology> coreTopologySink = var10000.queueMostRecent( var10001::onTopologyUpdate );
        var10000 = this.actorSystemLifecycle;
        var10001 = this.globalTopologyState;
        Objects.requireNonNull( var10001 );
        SourceQueueWithComplete<DatabaseReadReplicaTopology> rrTopologySink = var10000.queueMostRecent( var10001::onTopologyUpdate );
        var10000 = this.actorSystemLifecycle;
        var10001 = this.globalTopologyState;
        Objects.requireNonNull( var10001 );
        SourceQueueWithComplete<Map<DatabaseId,LeaderInfo>> directorySink = var10000.queueMostRecent( var10001::onDbLeaderUpdate );
        var10000 = this.actorSystemLifecycle;
        var10001 = this.globalTopologyState;
        Objects.requireNonNull( var10001 );
        SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink = var10000.queueMostRecent( var10001::onDbStateUpdate );
        DiscoveryMember discoveryMember = this.discoveryMemberFactory.create( this.myself );
        Props clientTopologyProps =
                ClientTopologyActor.props( discoveryMember, coreTopologySink, rrTopologySink, directorySink, databaseStateSink, clusterClient, this.config,
                        this.logProvider, this.clock );
        this.clientTopologyActorRef = this.actorSystemLifecycle.applicationActorOf( clientTopologyProps, "cc-client-topology-actor" );
    }

    public void stop0() throws Exception
    {
        this.clientTopologyActorRef = null;
        this.actorSystemLifecycle.shutdown();
        this.globalTopologyState = newGlobalTopologyState( this.logProvider );
    }

    public void onDatabaseStart( NamedDatabaseId namedDatabaseId )
    {
        ActorRef clientTopologyActor = this.clientTopologyActorRef;
        if ( clientTopologyActor != null )
        {
            clientTopologyActor.tell( new DatabaseStartedMessage( namedDatabaseId ), ActorRef.noSender() );
        }
    }

    public void onDatabaseStop( NamedDatabaseId namedDatabaseId )
    {
        ActorRef clientTopologyActor = this.clientTopologyActorRef;
        if ( clientTopologyActor != null )
        {
            clientTopologyActor.tell( new DatabaseStoppedMessage( namedDatabaseId ), ActorRef.noSender() );
        }
    }

    public void stateChange( DatabaseState previousState, DatabaseState newState )
    {
        ActorRef clientTopologyActor = this.clientTopologyActorRef;
        if ( clientTopologyActor != null )
        {
            clientTopologyActor.tell( DiscoveryDatabaseState.from( newState ), ActorRef.noSender() );
        }
    }

    public Map<MemberId,CoreServerInfo> allCoreServers()
    {
        return this.globalTopologyState.allCoreServers();
    }

    public DatabaseCoreTopology coreTopologyForDatabase( NamedDatabaseId namedDatabaseId )
    {
        return this.globalTopologyState.coreTopologyForDatabase( namedDatabaseId );
    }

    public Map<MemberId,ReadReplicaInfo> allReadReplicas()
    {
        return this.globalTopologyState.allReadReplicas();
    }

    public DatabaseReadReplicaTopology readReplicaTopologyForDatabase( NamedDatabaseId namedDatabaseId )
    {
        return this.globalTopologyState.readReplicaTopologyForDatabase( namedDatabaseId );
    }

    public SocketAddress lookupCatchupAddress( MemberId upstream ) throws CatchupAddressResolutionException
    {
        SocketAddress advertisedSocketAddress = this.globalTopologyState.retrieveCatchupServerAddress( upstream );
        if ( advertisedSocketAddress == null )
        {
            throw new CatchupAddressResolutionException( upstream );
        }
        else
        {
            return advertisedSocketAddress;
        }
    }

    public RoleInfo lookupRole( NamedDatabaseId namedDatabaseId, MemberId memberId )
    {
        return this.globalTopologyState.role( namedDatabaseId, memberId );
    }

    public LeaderInfo getLeader( NamedDatabaseId namedDatabaseId )
    {
        return this.globalTopologyState.getLeader( namedDatabaseId );
    }

    public MemberId memberId()
    {
        return this.myself;
    }

    public DiscoveryDatabaseState lookupDatabaseState( NamedDatabaseId namedDatabaseId, MemberId memberId )
    {
        return this.globalTopologyState.stateFor( memberId, namedDatabaseId );
    }

    public Map<MemberId,DiscoveryDatabaseState> allCoreStatesForDatabase( NamedDatabaseId namedDatabaseId )
    {
        return Map.copyOf( this.globalTopologyState.coreStatesForDatabase( namedDatabaseId ).memberStates() );
    }

    public Map<MemberId,DiscoveryDatabaseState> allReadReplicaStatesForDatabase( NamedDatabaseId namedDatabaseId )
    {
        return Map.copyOf( this.globalTopologyState.readReplicaStatesForDatabase( namedDatabaseId ).memberStates() );
    }

    public boolean isHealthy()
    {
        return true;
    }

    @VisibleForTesting
    GlobalTopologyState topologyState()
    {
        return this.globalTopologyState;
    }
}
