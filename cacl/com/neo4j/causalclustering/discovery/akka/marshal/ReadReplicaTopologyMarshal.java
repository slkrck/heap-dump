package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaTopologyMarshal extends SafeChannelMarshal<DatabaseReadReplicaTopology>
{
    private final ChannelMarshal<ReadReplicaInfo> readReplicaInfoMarshal = new ReadReplicaInfoMarshal();
    private final ChannelMarshal<MemberId> memberIdMarshal = new MemberId.Marshal();

    protected DatabaseReadReplicaTopology unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
        int size = channel.getInt();
        HashMap<MemberId,ReadReplicaInfo> replicas = new HashMap( size );

        for ( int i = 0; i < size; ++i )
        {
            MemberId memberId = (MemberId) this.memberIdMarshal.unmarshal( channel );
            ReadReplicaInfo readReplicaInfo = (ReadReplicaInfo) this.readReplicaInfoMarshal.unmarshal( channel );
            replicas.put( memberId, readReplicaInfo );
        }

        return new DatabaseReadReplicaTopology( databaseId, replicas );
    }

    public void marshal( DatabaseReadReplicaTopology readReplicaTopology, WritableChannel channel ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( readReplicaTopology.databaseId(), channel );
        channel.putInt( readReplicaTopology.members().size() );
        Iterator var3 = readReplicaTopology.members().entrySet().iterator();

        while ( var3.hasNext() )
        {
            Entry<MemberId,ReadReplicaInfo> entry = (Entry) var3.next();
            MemberId memberId = (MemberId) entry.getKey();
            ReadReplicaInfo readReplicaInfo = (ReadReplicaInfo) entry.getValue();
            this.memberIdMarshal.marshal( memberId, channel );
            this.readReplicaInfoMarshal.marshal( readReplicaInfo, channel );
        }
    }
}
