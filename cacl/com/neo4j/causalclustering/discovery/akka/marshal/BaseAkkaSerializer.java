package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.remote.MessageSerializer.SerializationException;
import akka.serialization.JSerializer;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;
import com.neo4j.causalclustering.messaging.marshalling.InputStreamReadableChannel;
import com.neo4j.causalclustering.messaging.marshalling.OutputStreamWritableChannel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class BaseAkkaSerializer<T> extends JSerializer
{
    static final int LEADER_INFO = 1000;
    static final int RAFT_ID = 1001;
    static final int UNIQUE_ADDRESS = 1002;
    static final int CORE_SERVER_INFO_FOR_MEMBER_ID = 1003;
    static final int READ_REPLICA_INFO_FOR_MEMBER_ID = 1004;
    static final int MEMBER_ID = 1005;
    static final int READ_REPLICA_INFO = 1006;
    static final int CORE_TOPOLOGY = 1007;
    static final int READ_REPLICA_REMOVAL = 1008;
    static final int READ_REPLICA_TOPOLOGY = 1009;
    static final int DB_LEADER_INFO = 1010;
    static final int REPLICATED_LEADER_INFO = 1011;
    static final int DATABASE_ID = 1012;
    static final int REPLICATED_DATABASE_STATE = 1013;
    static final int DATABASE_TO_MEMBER = 1014;
    static final int DATABASE_STATE = 1015;
    private final ChannelMarshal<T> marshal;
    private final int id;
    private final int sizeHint;

    protected BaseAkkaSerializer( ChannelMarshal<T> marshal, int id, int sizeHint )
    {
        this.marshal = marshal;
        this.id = id;
        this.sizeHint = sizeHint;
    }

    public byte[] toBinary( Object o )
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( this.sizeHint );
        OutputStreamWritableChannel channel = new OutputStreamWritableChannel( outputStream );

        try
        {
            this.marshal.marshal( o, channel );
        }
        catch ( IOException var5 )
        {
            throw new SerializationException( "Failed to serialize", var5 );
        }

        return outputStream.toByteArray();
    }

    public Object fromBinaryJava( byte[] bytes, Class<?> manifest )
    {
        ByteArrayInputStream inputStream = new ByteArrayInputStream( bytes );
        InputStreamReadableChannel channel = new InputStreamReadableChannel( inputStream );

        try
        {
            return this.marshal.unmarshal( channel );
        }
        catch ( EndOfStreamException | IOException var6 )
        {
            throw new SerializationException( "Failed to deserialize", var6 );
        }
    }

    int sizeHint()
    {
        return this.sizeHint;
    }

    public int identifier()
    {
        return this.id;
    }

    public boolean includeManifest()
    {
        return false;
    }
}
