package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class LeaderInfoMarshal extends SafeChannelMarshal<LeaderInfo>
{
    private MemberId.Marshal memberIdMarshal = new MemberId.Marshal();

    protected LeaderInfo unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        MemberId memberId = (MemberId) this.memberIdMarshal.unmarshal( channel );
        long term = channel.getLong();
        return new LeaderInfo( memberId, term );
    }

    public void marshal( LeaderInfo leaderInfo, WritableChannel channel ) throws IOException
    {
        this.memberIdMarshal.marshal( leaderInfo.memberId(), channel );
        channel.putLong( leaderInfo.term() );
    }
}
