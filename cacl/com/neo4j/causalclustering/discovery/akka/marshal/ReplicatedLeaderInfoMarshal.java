package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.akka.directory.ReplicatedLeaderInfo;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ReplicatedLeaderInfoMarshal extends SafeChannelMarshal<ReplicatedLeaderInfo>
{
    private final LeaderInfoMarshal leaderInfoMarshal = new LeaderInfoMarshal();

    protected ReplicatedLeaderInfo unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        return new ReplicatedLeaderInfo( (LeaderInfo) this.leaderInfoMarshal.unmarshal( channel ) );
    }

    public void marshal( ReplicatedLeaderInfo replicatedLeaderInfo, WritableChannel channel ) throws IOException
    {
        this.leaderInfoMarshal.marshal( replicatedLeaderInfo.leaderInfo(), channel );
    }
}
