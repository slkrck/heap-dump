package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.ClientConnectorAddresses;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.BooleanMarshal;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.Set;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class CoreServerInfoMarshal extends DiscoveryServerInfoMarshal<CoreServerInfo>
{
    private final ChannelMarshal<ClientConnectorAddresses> clientConnectorAddressesMarshal = new ClientConnectorAddresses.Marshal();
    private final ChannelMarshal<SocketAddress> advertisedSocketAddressMarshal = new AdvertisedSocketAddressMarshal();

    protected CoreServerInfo unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        SocketAddress raftServer = (SocketAddress) this.advertisedSocketAddressMarshal.unmarshal( channel );
        SocketAddress catchupServer = (SocketAddress) this.advertisedSocketAddressMarshal.unmarshal( channel );
        ClientConnectorAddresses clientConnectorAddresses = (ClientConnectorAddresses) this.clientConnectorAddressesMarshal.unmarshal( channel );
        Set<String> groups = unmarshalGroups( channel );
        Set<DatabaseId> databaseIds = unmarshalDatabaseIds( channel );
        boolean refuseToBeLeader = BooleanMarshal.unmarshal( channel );
        return new CoreServerInfo( raftServer, catchupServer, clientConnectorAddresses, groups, databaseIds, refuseToBeLeader );
    }

    public void marshal( CoreServerInfo coreServerInfo, WritableChannel channel ) throws IOException
    {
        this.advertisedSocketAddressMarshal.marshal( coreServerInfo.getRaftServer(), channel );
        this.advertisedSocketAddressMarshal.marshal( coreServerInfo.catchupServer(), channel );
        this.clientConnectorAddressesMarshal.marshal( coreServerInfo.connectors(), channel );
        marshalGroups( coreServerInfo, channel );
        marshalDatabaseIds( coreServerInfo, channel );
        BooleanMarshal.marshal( channel, coreServerInfo.refusesToBeLeader() );
    }
}
