package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.ClientConnectorAddresses;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.Set;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaInfoMarshal extends DiscoveryServerInfoMarshal<ReadReplicaInfo>
{
    private final ChannelMarshal<ClientConnectorAddresses> clientConnectorAddressesMarshal = new ClientConnectorAddresses.Marshal();
    private final ChannelMarshal<SocketAddress> advertisedSocketAddressMarshal = new AdvertisedSocketAddressMarshal();

    protected ReadReplicaInfo unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        ClientConnectorAddresses clientConnectorAddresses = (ClientConnectorAddresses) this.clientConnectorAddressesMarshal.unmarshal( channel );
        SocketAddress catchupServer = (SocketAddress) this.advertisedSocketAddressMarshal.unmarshal( channel );
        Set<String> groups = unmarshalGroups( channel );
        Set<DatabaseId> databaseIds = unmarshalDatabaseIds( channel );
        return new ReadReplicaInfo( clientConnectorAddresses, catchupServer, groups, databaseIds );
    }

    public void marshal( ReadReplicaInfo readReplicaInfo, WritableChannel channel ) throws IOException
    {
        this.clientConnectorAddressesMarshal.marshal( readReplicaInfo.connectors(), channel );
        this.advertisedSocketAddressMarshal.marshal( readReplicaInfo.catchupServer(), channel );
        marshalGroups( readReplicaInfo, channel );
        marshalDatabaseIds( readReplicaInfo, channel );
    }
}
