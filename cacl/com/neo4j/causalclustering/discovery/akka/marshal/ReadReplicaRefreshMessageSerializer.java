package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.actor.ExtendedActorSystem;
import com.neo4j.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRefreshMessage;

public class ReadReplicaRefreshMessageSerializer extends BaseAkkaSerializer<ReadReplicaRefreshMessage>
{
    protected ReadReplicaRefreshMessageSerializer( ExtendedActorSystem system )
    {
        super( new ReadReplicaRefreshMessageMarshal( system ), 1004, 256 );
    }
}
